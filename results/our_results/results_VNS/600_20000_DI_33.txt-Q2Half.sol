Capicity Bin: 16400
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8212 Color: 1
Size: 6828 Color: 1
Size: 1360 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 10228 Color: 1
Size: 3820 Color: 1
Size: 1280 Color: 0
Size: 1072 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8986 Color: 1
Size: 6678 Color: 1
Size: 736 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9254 Color: 1
Size: 5994 Color: 1
Size: 1152 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 1
Size: 5916 Color: 1
Size: 1176 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9484 Color: 1
Size: 5764 Color: 1
Size: 1152 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 1
Size: 5082 Color: 1
Size: 1012 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10338 Color: 1
Size: 5322 Color: 1
Size: 740 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10888 Color: 1
Size: 4600 Color: 1
Size: 912 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10984 Color: 1
Size: 5000 Color: 1
Size: 416 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10987 Color: 1
Size: 4511 Color: 1
Size: 902 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10996 Color: 1
Size: 4508 Color: 1
Size: 896 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11067 Color: 1
Size: 4445 Color: 1
Size: 888 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 1
Size: 4364 Color: 1
Size: 872 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 1
Size: 4554 Color: 1
Size: 574 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11672 Color: 1
Size: 4280 Color: 1
Size: 448 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 1
Size: 4264 Color: 1
Size: 356 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11820 Color: 1
Size: 3682 Color: 1
Size: 898 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11916 Color: 1
Size: 3852 Color: 1
Size: 632 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11954 Color: 1
Size: 3740 Color: 1
Size: 706 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11986 Color: 1
Size: 3506 Color: 1
Size: 908 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 1
Size: 3682 Color: 1
Size: 716 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12008 Color: 1
Size: 3586 Color: 1
Size: 806 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12034 Color: 1
Size: 3642 Color: 1
Size: 724 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12098 Color: 1
Size: 3666 Color: 1
Size: 636 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12101 Color: 1
Size: 3531 Color: 1
Size: 768 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 1
Size: 3944 Color: 1
Size: 352 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12130 Color: 1
Size: 3562 Color: 1
Size: 708 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12146 Color: 1
Size: 3546 Color: 1
Size: 708 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12163 Color: 1
Size: 3049 Color: 1
Size: 1188 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12171 Color: 1
Size: 3525 Color: 1
Size: 704 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12178 Color: 1
Size: 3522 Color: 1
Size: 700 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12194 Color: 1
Size: 3746 Color: 1
Size: 460 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12210 Color: 1
Size: 3202 Color: 1
Size: 988 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12312 Color: 1
Size: 3700 Color: 1
Size: 388 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12514 Color: 1
Size: 3242 Color: 1
Size: 644 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12553 Color: 1
Size: 3207 Color: 1
Size: 640 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12562 Color: 1
Size: 3172 Color: 1
Size: 666 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 1
Size: 3174 Color: 1
Size: 632 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12596 Color: 1
Size: 3148 Color: 1
Size: 656 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12628 Color: 1
Size: 2974 Color: 1
Size: 798 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 1
Size: 2920 Color: 1
Size: 848 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12743 Color: 1
Size: 2961 Color: 1
Size: 696 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12801 Color: 1
Size: 3001 Color: 1
Size: 598 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12849 Color: 1
Size: 2321 Color: 1
Size: 1230 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12877 Color: 1
Size: 2931 Color: 1
Size: 592 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12885 Color: 1
Size: 2989 Color: 1
Size: 526 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 3144 Color: 1
Size: 352 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12996 Color: 1
Size: 2844 Color: 1
Size: 560 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 1
Size: 2728 Color: 1
Size: 652 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13046 Color: 1
Size: 2820 Color: 1
Size: 534 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13078 Color: 1
Size: 2962 Color: 1
Size: 360 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13144 Color: 1
Size: 2512 Color: 1
Size: 744 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13179 Color: 1
Size: 2637 Color: 1
Size: 584 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13210 Color: 1
Size: 2770 Color: 1
Size: 420 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13237 Color: 1
Size: 2573 Color: 1
Size: 590 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13300 Color: 1
Size: 2588 Color: 1
Size: 512 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 1
Size: 2332 Color: 1
Size: 748 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13321 Color: 1
Size: 2567 Color: 1
Size: 512 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 1
Size: 2564 Color: 1
Size: 512 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13392 Color: 1
Size: 2448 Color: 1
Size: 560 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13441 Color: 1
Size: 2467 Color: 1
Size: 492 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13462 Color: 1
Size: 2450 Color: 1
Size: 488 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 1
Size: 2328 Color: 1
Size: 576 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13558 Color: 1
Size: 2370 Color: 1
Size: 472 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13591 Color: 1
Size: 2293 Color: 1
Size: 516 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13604 Color: 1
Size: 2424 Color: 1
Size: 372 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13617 Color: 1
Size: 2159 Color: 1
Size: 624 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13624 Color: 1
Size: 2224 Color: 1
Size: 552 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13633 Color: 1
Size: 2307 Color: 1
Size: 460 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13638 Color: 1
Size: 2248 Color: 1
Size: 514 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13676 Color: 1
Size: 2262 Color: 1
Size: 462 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13686 Color: 1
Size: 2302 Color: 1
Size: 412 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13701 Color: 1
Size: 1939 Color: 1
Size: 760 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 1
Size: 2152 Color: 1
Size: 528 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13798 Color: 1
Size: 2170 Color: 1
Size: 432 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 1
Size: 2032 Color: 1
Size: 552 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13847 Color: 1
Size: 1929 Color: 1
Size: 624 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13859 Color: 1
Size: 1805 Color: 1
Size: 736 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13875 Color: 1
Size: 2095 Color: 1
Size: 430 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 1
Size: 2168 Color: 1
Size: 336 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13966 Color: 1
Size: 2238 Color: 1
Size: 196 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13998 Color: 1
Size: 2002 Color: 1
Size: 400 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 14036 Color: 1
Size: 2084 Color: 1
Size: 280 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 1
Size: 1964 Color: 1
Size: 384 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 14063 Color: 1
Size: 1979 Color: 1
Size: 358 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 14082 Color: 1
Size: 1934 Color: 1
Size: 384 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 14180 Color: 1
Size: 1852 Color: 1
Size: 368 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 14183 Color: 1
Size: 1791 Color: 1
Size: 426 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 1
Size: 1864 Color: 1
Size: 336 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 14204 Color: 1
Size: 1836 Color: 1
Size: 360 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 14214 Color: 1
Size: 1822 Color: 1
Size: 364 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 14235 Color: 1
Size: 1717 Color: 1
Size: 448 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 14251 Color: 1
Size: 2131 Color: 1
Size: 18 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14252 Color: 1
Size: 1716 Color: 1
Size: 432 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 14262 Color: 1
Size: 1796 Color: 1
Size: 342 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 14310 Color: 1
Size: 1742 Color: 1
Size: 348 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 14348 Color: 1
Size: 1768 Color: 1
Size: 284 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 14358 Color: 1
Size: 1782 Color: 1
Size: 260 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14383 Color: 1
Size: 1681 Color: 1
Size: 336 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 14438 Color: 1
Size: 1702 Color: 1
Size: 260 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 14446 Color: 1
Size: 1630 Color: 1
Size: 324 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 14453 Color: 1
Size: 1599 Color: 1
Size: 348 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 1
Size: 1688 Color: 1
Size: 228 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 14510 Color: 1
Size: 1578 Color: 1
Size: 312 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 1
Size: 1716 Color: 1
Size: 160 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14531 Color: 1
Size: 1559 Color: 1
Size: 310 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14552 Color: 1
Size: 1524 Color: 1
Size: 324 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14558 Color: 1
Size: 1538 Color: 1
Size: 304 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14571 Color: 1
Size: 1525 Color: 1
Size: 304 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14572 Color: 1
Size: 1434 Color: 1
Size: 394 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 1
Size: 1512 Color: 1
Size: 288 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14621 Color: 1
Size: 1483 Color: 1
Size: 296 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14640 Color: 1
Size: 1472 Color: 1
Size: 288 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14669 Color: 1
Size: 1443 Color: 1
Size: 288 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14676 Color: 1
Size: 1444 Color: 1
Size: 280 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14682 Color: 1
Size: 1414 Color: 1
Size: 304 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14706 Color: 1
Size: 1544 Color: 1
Size: 150 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14708 Color: 1
Size: 1374 Color: 1
Size: 318 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14724 Color: 1
Size: 1404 Color: 1
Size: 272 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14750 Color: 1
Size: 1378 Color: 1
Size: 272 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14754 Color: 1
Size: 1412 Color: 1
Size: 234 Color: 0

Bin 123: 1 of cap free
Amount of items: 3
Items: 
Size: 9009 Color: 1
Size: 7022 Color: 1
Size: 368 Color: 0

Bin 124: 1 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 1
Size: 5723 Color: 1
Size: 716 Color: 0

Bin 125: 1 of cap free
Amount of items: 3
Items: 
Size: 10107 Color: 1
Size: 5148 Color: 1
Size: 1144 Color: 0

Bin 126: 1 of cap free
Amount of items: 3
Items: 
Size: 10428 Color: 1
Size: 4947 Color: 1
Size: 1024 Color: 0

Bin 127: 1 of cap free
Amount of items: 3
Items: 
Size: 10465 Color: 1
Size: 5118 Color: 1
Size: 816 Color: 0

Bin 128: 1 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 1
Size: 3991 Color: 1
Size: 1196 Color: 0

Bin 129: 1 of cap free
Amount of items: 3
Items: 
Size: 11611 Color: 1
Size: 4324 Color: 1
Size: 464 Color: 0

Bin 130: 1 of cap free
Amount of items: 3
Items: 
Size: 11906 Color: 1
Size: 3583 Color: 1
Size: 910 Color: 0

Bin 131: 1 of cap free
Amount of items: 3
Items: 
Size: 12079 Color: 1
Size: 3416 Color: 1
Size: 904 Color: 0

Bin 132: 1 of cap free
Amount of items: 3
Items: 
Size: 12155 Color: 1
Size: 3300 Color: 1
Size: 944 Color: 0

Bin 133: 1 of cap free
Amount of items: 3
Items: 
Size: 12444 Color: 1
Size: 3539 Color: 1
Size: 416 Color: 0

Bin 134: 1 of cap free
Amount of items: 3
Items: 
Size: 12483 Color: 1
Size: 3494 Color: 1
Size: 422 Color: 0

Bin 135: 1 of cap free
Amount of items: 3
Items: 
Size: 12834 Color: 1
Size: 2685 Color: 1
Size: 880 Color: 0

Bin 136: 1 of cap free
Amount of items: 3
Items: 
Size: 13077 Color: 1
Size: 2798 Color: 1
Size: 524 Color: 0

Bin 137: 1 of cap free
Amount of items: 3
Items: 
Size: 13313 Color: 1
Size: 2568 Color: 1
Size: 518 Color: 0

Bin 138: 1 of cap free
Amount of items: 3
Items: 
Size: 13628 Color: 1
Size: 2119 Color: 1
Size: 652 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 13811 Color: 1
Size: 2396 Color: 1
Size: 192 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 13843 Color: 1
Size: 2088 Color: 1
Size: 468 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 13868 Color: 1
Size: 2251 Color: 1
Size: 280 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 1
Size: 2105 Color: 1
Size: 376 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 1
Size: 2171 Color: 1
Size: 240 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 1
Size: 2132 Color: 1
Size: 272 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 14027 Color: 1
Size: 2276 Color: 1
Size: 96 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 14150 Color: 1
Size: 1849 Color: 1
Size: 400 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 14155 Color: 1
Size: 2116 Color: 1
Size: 128 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 14201 Color: 1
Size: 1878 Color: 1
Size: 320 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 14309 Color: 1
Size: 1638 Color: 1
Size: 452 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 14341 Color: 1
Size: 1040 Color: 0
Size: 1018 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 14376 Color: 1
Size: 1743 Color: 1
Size: 280 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 14483 Color: 1
Size: 1612 Color: 1
Size: 304 Color: 0

Bin 153: 1 of cap free
Amount of items: 4
Items: 
Size: 8202 Color: 1
Size: 7621 Color: 1
Size: 304 Color: 0
Size: 272 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 8205 Color: 1
Size: 6834 Color: 1
Size: 1360 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 8664 Color: 1
Size: 6557 Color: 1
Size: 1178 Color: 0

Bin 156: 2 of cap free
Amount of items: 3
Items: 
Size: 8264 Color: 1
Size: 6836 Color: 1
Size: 1298 Color: 0

Bin 157: 2 of cap free
Amount of items: 3
Items: 
Size: 9000 Color: 1
Size: 6032 Color: 1
Size: 1366 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 9321 Color: 1
Size: 6161 Color: 1
Size: 916 Color: 0

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 9432 Color: 1
Size: 6022 Color: 1
Size: 944 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 1
Size: 4776 Color: 1
Size: 732 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 10970 Color: 1
Size: 4980 Color: 1
Size: 448 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 11007 Color: 1
Size: 4495 Color: 1
Size: 896 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 11687 Color: 1
Size: 4031 Color: 1
Size: 680 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 11868 Color: 1
Size: 3922 Color: 1
Size: 608 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 12050 Color: 1
Size: 3852 Color: 1
Size: 496 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 12066 Color: 1
Size: 3626 Color: 1
Size: 706 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 13088 Color: 1
Size: 2638 Color: 1
Size: 672 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 14280 Color: 1
Size: 2070 Color: 1
Size: 48 Color: 0

Bin 169: 2 of cap free
Amount of items: 7
Items: 
Size: 4526 Color: 1
Size: 3706 Color: 1
Size: 3265 Color: 1
Size: 2341 Color: 1
Size: 992 Color: 0
Size: 784 Color: 0
Size: 784 Color: 0

Bin 170: 3 of cap free
Amount of items: 3
Items: 
Size: 9533 Color: 1
Size: 5816 Color: 1
Size: 1048 Color: 0

Bin 171: 3 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 1
Size: 5245 Color: 1
Size: 744 Color: 0

Bin 172: 3 of cap free
Amount of items: 3
Items: 
Size: 11563 Color: 1
Size: 4102 Color: 1
Size: 732 Color: 0

Bin 173: 3 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 1
Size: 3929 Color: 1
Size: 556 Color: 0

Bin 174: 3 of cap free
Amount of items: 3
Items: 
Size: 13286 Color: 1
Size: 2771 Color: 1
Size: 340 Color: 0

Bin 175: 3 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 1949 Color: 1
Size: 280 Color: 0

Bin 176: 4 of cap free
Amount of items: 3
Items: 
Size: 10938 Color: 1
Size: 4594 Color: 1
Size: 864 Color: 0

Bin 177: 4 of cap free
Amount of items: 3
Items: 
Size: 11964 Color: 1
Size: 3752 Color: 1
Size: 680 Color: 0

Bin 178: 5 of cap free
Amount of items: 3
Items: 
Size: 10014 Color: 1
Size: 5901 Color: 1
Size: 480 Color: 0

Bin 179: 5 of cap free
Amount of items: 3
Items: 
Size: 11482 Color: 1
Size: 4081 Color: 1
Size: 832 Color: 0

Bin 180: 5 of cap free
Amount of items: 3
Items: 
Size: 13285 Color: 1
Size: 2598 Color: 1
Size: 512 Color: 0

Bin 181: 6 of cap free
Amount of items: 3
Items: 
Size: 11694 Color: 1
Size: 3976 Color: 1
Size: 724 Color: 0

Bin 182: 7 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 1
Size: 6833 Color: 1
Size: 1344 Color: 0

Bin 183: 7 of cap free
Amount of items: 3
Items: 
Size: 9356 Color: 1
Size: 6501 Color: 1
Size: 536 Color: 0

Bin 184: 9 of cap free
Amount of items: 3
Items: 
Size: 9273 Color: 1
Size: 5982 Color: 1
Size: 1136 Color: 0

Bin 185: 10 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 1
Size: 3152 Color: 0

Bin 186: 11 of cap free
Amount of items: 3
Items: 
Size: 10285 Color: 1
Size: 5368 Color: 1
Size: 736 Color: 0

Bin 187: 14 of cap free
Amount of items: 3
Items: 
Size: 8933 Color: 1
Size: 6461 Color: 1
Size: 992 Color: 0

Bin 188: 14 of cap free
Amount of items: 3
Items: 
Size: 9222 Color: 1
Size: 6300 Color: 1
Size: 864 Color: 0

Bin 189: 14 of cap free
Amount of items: 3
Items: 
Size: 9270 Color: 1
Size: 6416 Color: 1
Size: 700 Color: 0

Bin 190: 14 of cap free
Amount of items: 4
Items: 
Size: 7784 Color: 1
Size: 5958 Color: 1
Size: 1364 Color: 0
Size: 1280 Color: 0

Bin 191: 15 of cap free
Amount of items: 3
Items: 
Size: 8601 Color: 1
Size: 6536 Color: 1
Size: 1248 Color: 0

Bin 192: 18 of cap free
Amount of items: 3
Items: 
Size: 8704 Color: 1
Size: 7310 Color: 1
Size: 368 Color: 0

Bin 193: 18 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 1
Size: 6792 Color: 1
Size: 416 Color: 0

Bin 194: 42 of cap free
Amount of items: 3
Items: 
Size: 8698 Color: 1
Size: 6456 Color: 1
Size: 1204 Color: 0

Bin 195: 62 of cap free
Amount of items: 3
Items: 
Size: 8210 Color: 1
Size: 7216 Color: 1
Size: 912 Color: 0

Bin 196: 136 of cap free
Amount of items: 3
Items: 
Size: 10680 Color: 1
Size: 4520 Color: 1
Size: 1064 Color: 0

Bin 197: 2315 of cap free
Amount of items: 3
Items: 
Size: 8204 Color: 1
Size: 5097 Color: 1
Size: 784 Color: 0

Bin 198: 5298 of cap free
Amount of items: 3
Items: 
Size: 8201 Color: 1
Size: 2597 Color: 1
Size: 304 Color: 0

Bin 199: 8288 of cap free
Amount of items: 1
Items: 
Size: 8112 Color: 1

Total size: 3247200
Total free space: 16400

