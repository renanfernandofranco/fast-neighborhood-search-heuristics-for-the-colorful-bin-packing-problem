Capicity Bin: 16400
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8264 Color: 3
Size: 7784 Color: 13
Size: 352 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 16
Size: 6536 Color: 4
Size: 556 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9432 Color: 1
Size: 6456 Color: 4
Size: 512 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10014 Color: 3
Size: 6022 Color: 13
Size: 364 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 2
Size: 4520 Color: 1
Size: 608 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11687 Color: 15
Size: 3525 Color: 0
Size: 1188 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11694 Color: 2
Size: 3642 Color: 10
Size: 1064 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12050 Color: 9
Size: 3706 Color: 17
Size: 644 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11964 Color: 17
Size: 3976 Color: 4
Size: 460 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11986 Color: 5
Size: 3666 Color: 2
Size: 748 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 8
Size: 3494 Color: 13
Size: 904 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12066 Color: 9
Size: 3682 Color: 8
Size: 652 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12098 Color: 9
Size: 3562 Color: 16
Size: 740 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12101 Color: 7
Size: 3583 Color: 11
Size: 716 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12163 Color: 3
Size: 3531 Color: 0
Size: 706 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12194 Color: 17
Size: 3922 Color: 10
Size: 284 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12312 Color: 7
Size: 3700 Color: 9
Size: 388 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12885 Color: 12
Size: 2931 Color: 19
Size: 584 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 18
Size: 2084 Color: 4
Size: 1412 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12996 Color: 16
Size: 3144 Color: 12
Size: 260 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13078 Color: 12
Size: 2962 Color: 9
Size: 360 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 19
Size: 2844 Color: 4
Size: 318 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13285 Color: 0
Size: 1979 Color: 13
Size: 1136 Color: 10

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 0
Size: 2568 Color: 2
Size: 512 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13321 Color: 4
Size: 2567 Color: 15
Size: 512 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 18
Size: 2588 Color: 13
Size: 488 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13441 Color: 16
Size: 2293 Color: 10
Size: 666 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13462 Color: 3
Size: 2238 Color: 7
Size: 700 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13617 Color: 8
Size: 1791 Color: 10
Size: 992 Color: 12

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13624 Color: 1
Size: 2262 Color: 8
Size: 514 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13628 Color: 13
Size: 2512 Color: 16
Size: 260 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13686 Color: 13
Size: 2564 Color: 2
Size: 150 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13701 Color: 2
Size: 2251 Color: 3
Size: 448 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 17
Size: 1782 Color: 15
Size: 898 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 16
Size: 1852 Color: 8
Size: 732 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13811 Color: 11
Size: 1681 Color: 13
Size: 908 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13843 Color: 8
Size: 2105 Color: 0
Size: 452 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 18
Size: 2276 Color: 7
Size: 228 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 13
Size: 2070 Color: 9
Size: 412 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14027 Color: 2
Size: 1849 Color: 9
Size: 524 Color: 13

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 16
Size: 1434 Color: 3
Size: 798 Color: 10

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14204 Color: 6
Size: 1472 Color: 14
Size: 724 Color: 12

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14235 Color: 16
Size: 1743 Color: 17
Size: 422 Color: 6

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14262 Color: 14
Size: 1378 Color: 4
Size: 760 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14280 Color: 19
Size: 1072 Color: 14
Size: 1048 Color: 5

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14358 Color: 14
Size: 1702 Color: 14
Size: 340 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14376 Color: 7
Size: 1280 Color: 7
Size: 744 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14383 Color: 5
Size: 1483 Color: 6
Size: 534 Color: 11

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14531 Color: 16
Size: 1559 Color: 11
Size: 310 Color: 6

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14446 Color: 19
Size: 1248 Color: 15
Size: 706 Color: 8

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14453 Color: 19
Size: 1599 Color: 8
Size: 348 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14552 Color: 16
Size: 1512 Color: 18
Size: 336 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14510 Color: 0
Size: 1538 Color: 7
Size: 352 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 4
Size: 1360 Color: 14
Size: 516 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14571 Color: 16
Size: 1525 Color: 14
Size: 304 Color: 17

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14621 Color: 8
Size: 1443 Color: 13
Size: 336 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14640 Color: 1
Size: 1360 Color: 16
Size: 400 Color: 18

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14706 Color: 5
Size: 910 Color: 8
Size: 784 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14708 Color: 18
Size: 1404 Color: 8
Size: 288 Color: 19

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14724 Color: 4
Size: 1364 Color: 14
Size: 312 Color: 10

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 9321 Color: 11
Size: 6678 Color: 15
Size: 400 Color: 16

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 16
Size: 5901 Color: 4
Size: 192 Color: 11

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11611 Color: 7
Size: 4508 Color: 2
Size: 280 Color: 13

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 12
Size: 3991 Color: 9
Size: 496 Color: 1

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12553 Color: 17
Size: 2302 Color: 10
Size: 1544 Color: 0

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12562 Color: 13
Size: 2095 Color: 16
Size: 1742 Color: 12

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12743 Color: 19
Size: 3416 Color: 3
Size: 240 Color: 8

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12801 Color: 16
Size: 3172 Color: 19
Size: 426 Color: 18

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 12877 Color: 1
Size: 3522 Color: 2

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13847 Color: 8
Size: 1768 Color: 16
Size: 784 Color: 12

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 14155 Color: 17
Size: 1964 Color: 10
Size: 280 Color: 8

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 14682 Color: 18
Size: 1717 Color: 13

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 9270 Color: 16
Size: 6792 Color: 9
Size: 336 Color: 11

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 13
Size: 4980 Color: 5
Size: 528 Color: 13

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 11906 Color: 7
Size: 3148 Color: 15
Size: 1344 Color: 11

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 11916 Color: 18
Size: 3586 Color: 17
Size: 896 Color: 9

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 12034 Color: 16
Size: 4364 Color: 18

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 12079 Color: 12
Size: 4031 Color: 18
Size: 288 Color: 14

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 12210 Color: 5
Size: 3820 Color: 18
Size: 368 Color: 16

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 17
Size: 2598 Color: 16
Size: 304 Color: 0

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14150 Color: 8
Size: 2248 Color: 4

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 7
Size: 2088 Color: 2

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 14750 Color: 4
Size: 1630 Color: 18
Size: 18 Color: 16

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 8933 Color: 9
Size: 6828 Color: 18
Size: 636 Color: 0

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 9000 Color: 19
Size: 6501 Color: 6
Size: 896 Color: 3

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 10285 Color: 16
Size: 5764 Color: 17
Size: 348 Color: 9

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 11482 Color: 1
Size: 4495 Color: 16
Size: 420 Color: 12

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 12155 Color: 6
Size: 3682 Color: 16
Size: 560 Color: 8

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 13088 Color: 3
Size: 2573 Color: 16
Size: 736 Color: 17

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 14309 Color: 6
Size: 1144 Color: 16
Size: 944 Color: 15

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 11954 Color: 8
Size: 2637 Color: 18
Size: 1805 Color: 4

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 12146 Color: 18
Size: 3626 Color: 6
Size: 624 Color: 15

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 14036 Color: 12
Size: 1688 Color: 16
Size: 672 Color: 11

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 1
Size: 1796 Color: 7

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 12849 Color: 11
Size: 3546 Color: 7

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 13798 Color: 10
Size: 2597 Color: 6

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 14063 Color: 6
Size: 2332 Color: 19

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 14483 Color: 18
Size: 1864 Color: 7
Size: 48 Color: 4

Bin 99: 6 of cap free
Amount of items: 3
Items: 
Size: 9222 Color: 16
Size: 5994 Color: 18
Size: 1178 Color: 8

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 11868 Color: 11
Size: 4526 Color: 9

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 12130 Color: 16
Size: 4264 Color: 0

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 13998 Color: 3
Size: 2396 Color: 17

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 14558 Color: 12
Size: 1836 Color: 6

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 14572 Color: 14
Size: 1822 Color: 12

Bin 105: 7 of cap free
Amount of items: 3
Items: 
Size: 10107 Color: 19
Size: 5982 Color: 15
Size: 304 Color: 10

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 13392 Color: 14
Size: 3001 Color: 9

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 14052 Color: 1
Size: 2341 Color: 4

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 14676 Color: 2
Size: 1716 Color: 11

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 14754 Color: 18
Size: 1638 Color: 7

Bin 110: 9 of cap free
Amount of items: 3
Items: 
Size: 10888 Color: 16
Size: 4511 Color: 17
Size: 992 Color: 9

Bin 111: 9 of cap free
Amount of items: 3
Items: 
Size: 10996 Color: 15
Size: 4947 Color: 19
Size: 448 Color: 13

Bin 112: 9 of cap free
Amount of items: 3
Items: 
Size: 11007 Color: 4
Size: 5000 Color: 3
Size: 384 Color: 7

Bin 113: 10 of cap free
Amount of items: 2
Items: 
Size: 9174 Color: 4
Size: 7216 Color: 17

Bin 114: 10 of cap free
Amount of items: 2
Items: 
Size: 13966 Color: 1
Size: 2424 Color: 9

Bin 115: 11 of cap free
Amount of items: 3
Items: 
Size: 8698 Color: 18
Size: 6461 Color: 5
Size: 1230 Color: 16

Bin 116: 11 of cap free
Amount of items: 2
Items: 
Size: 10228 Color: 13
Size: 6161 Color: 9

Bin 117: 11 of cap free
Amount of items: 2
Items: 
Size: 11067 Color: 17
Size: 5322 Color: 14

Bin 118: 11 of cap free
Amount of items: 2
Items: 
Size: 13237 Color: 0
Size: 3152 Color: 1

Bin 119: 11 of cap free
Amount of items: 2
Items: 
Size: 13591 Color: 8
Size: 2798 Color: 10

Bin 120: 11 of cap free
Amount of items: 2
Items: 
Size: 14082 Color: 9
Size: 2307 Color: 0

Bin 121: 12 of cap free
Amount of items: 3
Items: 
Size: 10938 Color: 8
Size: 5082 Color: 14
Size: 368 Color: 7

Bin 122: 12 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 5
Size: 3506 Color: 18
Size: 288 Color: 9

Bin 123: 12 of cap free
Amount of items: 3
Items: 
Size: 13286 Color: 15
Size: 2974 Color: 8
Size: 128 Color: 16

Bin 124: 13 of cap free
Amount of items: 3
Items: 
Size: 10987 Color: 4
Size: 4594 Color: 6
Size: 806 Color: 16

Bin 125: 13 of cap free
Amount of items: 2
Items: 
Size: 14438 Color: 4
Size: 1949 Color: 10

Bin 126: 14 of cap free
Amount of items: 7
Items: 
Size: 8205 Color: 0
Size: 2370 Color: 16
Size: 2224 Color: 18
Size: 2119 Color: 6
Size: 916 Color: 1
Size: 280 Color: 15
Size: 272 Color: 7

Bin 127: 14 of cap free
Amount of items: 3
Items: 
Size: 9254 Color: 10
Size: 6300 Color: 1
Size: 832 Color: 7

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 10428 Color: 7
Size: 5958 Color: 15

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 13144 Color: 12
Size: 3242 Color: 18

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 13179 Color: 5
Size: 3207 Color: 3

Bin 131: 15 of cap free
Amount of items: 3
Items: 
Size: 9356 Color: 1
Size: 6833 Color: 13
Size: 196 Color: 3

Bin 132: 15 of cap free
Amount of items: 3
Items: 
Size: 13300 Color: 17
Size: 2989 Color: 11
Size: 96 Color: 13

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 14669 Color: 3
Size: 1716 Color: 8

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 4
Size: 4280 Color: 3

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 12632 Color: 3
Size: 3752 Color: 13

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 13210 Color: 10
Size: 3174 Color: 3

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 14214 Color: 1
Size: 2170 Color: 4

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 14252 Color: 15
Size: 2132 Color: 11

Bin 139: 18 of cap free
Amount of items: 8
Items: 
Size: 8212 Color: 1
Size: 1298 Color: 7
Size: 1280 Color: 5
Size: 1204 Color: 10
Size: 1196 Color: 17
Size: 1152 Color: 18
Size: 1152 Color: 12
Size: 888 Color: 0

Bin 140: 18 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 3
Size: 3202 Color: 4
Size: 160 Color: 2

Bin 141: 18 of cap free
Amount of items: 2
Items: 
Size: 14251 Color: 2
Size: 2131 Color: 17

Bin 142: 19 of cap free
Amount of items: 2
Items: 
Size: 10465 Color: 9
Size: 5916 Color: 8

Bin 143: 20 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 15
Size: 3944 Color: 9
Size: 656 Color: 14

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 14348 Color: 1
Size: 2032 Color: 11

Bin 145: 22 of cap free
Amount of items: 2
Items: 
Size: 13558 Color: 14
Size: 2820 Color: 4

Bin 146: 23 of cap free
Amount of items: 2
Items: 
Size: 13077 Color: 7
Size: 3300 Color: 19

Bin 147: 24 of cap free
Amount of items: 2
Items: 
Size: 9960 Color: 15
Size: 6416 Color: 13

Bin 148: 25 of cap free
Amount of items: 3
Items: 
Size: 10680 Color: 18
Size: 5097 Color: 19
Size: 598 Color: 13

Bin 149: 25 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 1
Size: 2771 Color: 18

Bin 150: 26 of cap free
Amount of items: 2
Items: 
Size: 11820 Color: 9
Size: 4554 Color: 15

Bin 151: 26 of cap free
Amount of items: 2
Items: 
Size: 12628 Color: 11
Size: 3746 Color: 6

Bin 152: 27 of cap free
Amount of items: 2
Items: 
Size: 12444 Color: 2
Size: 3929 Color: 10

Bin 153: 27 of cap free
Amount of items: 2
Items: 
Size: 12834 Color: 11
Size: 3539 Color: 2

Bin 154: 28 of cap free
Amount of items: 2
Items: 
Size: 14201 Color: 5
Size: 2171 Color: 1

Bin 155: 29 of cap free
Amount of items: 10
Items: 
Size: 8201 Color: 19
Size: 1176 Color: 5
Size: 1040 Color: 8
Size: 1024 Color: 8
Size: 1018 Color: 15
Size: 1012 Color: 0
Size: 988 Color: 17
Size: 768 Color: 7
Size: 680 Color: 11
Size: 464 Color: 12

Bin 156: 30 of cap free
Amount of items: 2
Items: 
Size: 10338 Color: 7
Size: 6032 Color: 5

Bin 157: 31 of cap free
Amount of items: 4
Items: 
Size: 8210 Color: 16
Size: 4445 Color: 11
Size: 2770 Color: 14
Size: 944 Color: 13

Bin 158: 32 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 13
Size: 2168 Color: 9

Bin 159: 33 of cap free
Amount of items: 2
Items: 
Size: 9533 Color: 12
Size: 6834 Color: 19

Bin 160: 34 of cap free
Amount of items: 2
Items: 
Size: 12514 Color: 0
Size: 3852 Color: 5

Bin 161: 34 of cap free
Amount of items: 2
Items: 
Size: 13638 Color: 5
Size: 2728 Color: 11

Bin 162: 38 of cap free
Amount of items: 2
Items: 
Size: 13313 Color: 13
Size: 3049 Color: 18

Bin 163: 38 of cap free
Amount of items: 2
Items: 
Size: 14484 Color: 8
Size: 1878 Color: 2

Bin 164: 40 of cap free
Amount of items: 2
Items: 
Size: 11212 Color: 1
Size: 5148 Color: 3

Bin 165: 41 of cap free
Amount of items: 3
Items: 
Size: 8986 Color: 18
Size: 6557 Color: 10
Size: 816 Color: 7

Bin 166: 48 of cap free
Amount of items: 2
Items: 
Size: 10984 Color: 9
Size: 5368 Color: 13

Bin 167: 49 of cap free
Amount of items: 6
Items: 
Size: 8204 Color: 17
Size: 2116 Color: 19
Size: 1929 Color: 14
Size: 1612 Color: 10
Size: 1578 Color: 11
Size: 912 Color: 10

Bin 168: 49 of cap free
Amount of items: 3
Items: 
Size: 8601 Color: 9
Size: 5816 Color: 8
Size: 1934 Color: 1

Bin 169: 57 of cap free
Amount of items: 2
Items: 
Size: 14341 Color: 6
Size: 2002 Color: 1

Bin 170: 58 of cap free
Amount of items: 2
Items: 
Size: 13875 Color: 13
Size: 2467 Color: 6

Bin 171: 58 of cap free
Amount of items: 2
Items: 
Size: 14183 Color: 9
Size: 2159 Color: 3

Bin 172: 60 of cap free
Amount of items: 34
Items: 
Size: 680 Color: 6
Size: 640 Color: 14
Size: 632 Color: 18
Size: 632 Color: 6
Size: 624 Color: 7
Size: 592 Color: 16
Size: 576 Color: 9
Size: 560 Color: 17
Size: 552 Color: 16
Size: 552 Color: 1
Size: 536 Color: 16
Size: 526 Color: 3
Size: 518 Color: 19
Size: 512 Color: 5
Size: 492 Color: 7
Size: 480 Color: 5
Size: 472 Color: 2
Size: 468 Color: 5
Size: 462 Color: 3
Size: 448 Color: 3
Size: 432 Color: 13
Size: 432 Color: 11
Size: 430 Color: 12
Size: 416 Color: 18
Size: 416 Color: 11
Size: 416 Color: 9
Size: 394 Color: 6
Size: 384 Color: 15
Size: 372 Color: 10
Size: 368 Color: 4
Size: 360 Color: 8
Size: 342 Color: 12
Size: 320 Color: 1
Size: 304 Color: 4

Bin 173: 61 of cap free
Amount of items: 2
Items: 
Size: 11563 Color: 10
Size: 4776 Color: 17

Bin 174: 64 of cap free
Amount of items: 2
Items: 
Size: 12596 Color: 15
Size: 3740 Color: 19

Bin 175: 65 of cap free
Amount of items: 2
Items: 
Size: 12483 Color: 18
Size: 3852 Color: 16

Bin 176: 68 of cap free
Amount of items: 2
Items: 
Size: 12008 Color: 4
Size: 4324 Color: 2

Bin 177: 68 of cap free
Amount of items: 2
Items: 
Size: 14180 Color: 5
Size: 2152 Color: 8

Bin 178: 72 of cap free
Amount of items: 2
Items: 
Size: 8216 Color: 10
Size: 8112 Color: 14

Bin 179: 74 of cap free
Amount of items: 3
Items: 
Size: 8664 Color: 6
Size: 5723 Color: 15
Size: 1939 Color: 15

Bin 180: 75 of cap free
Amount of items: 2
Items: 
Size: 8704 Color: 7
Size: 7621 Color: 10

Bin 181: 77 of cap free
Amount of items: 2
Items: 
Size: 13995 Color: 9
Size: 2328 Color: 17

Bin 182: 80 of cap free
Amount of items: 2
Items: 
Size: 9484 Color: 10
Size: 6836 Color: 12

Bin 183: 81 of cap free
Amount of items: 2
Items: 
Size: 9009 Color: 3
Size: 7310 Color: 2

Bin 184: 82 of cap free
Amount of items: 2
Items: 
Size: 13633 Color: 8
Size: 2685 Color: 10

Bin 185: 82 of cap free
Amount of items: 2
Items: 
Size: 13868 Color: 1
Size: 2450 Color: 3

Bin 186: 86 of cap free
Amount of items: 2
Items: 
Size: 13676 Color: 10
Size: 2638 Color: 18

Bin 187: 89 of cap free
Amount of items: 2
Items: 
Size: 13046 Color: 6
Size: 3265 Color: 3

Bin 188: 91 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 0
Size: 2321 Color: 8

Bin 189: 93 of cap free
Amount of items: 2
Items: 
Size: 13859 Color: 10
Size: 2448 Color: 0

Bin 190: 105 of cap free
Amount of items: 2
Items: 
Size: 9273 Color: 2
Size: 7022 Color: 0

Bin 191: 111 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 2
Size: 2961 Color: 8
Size: 2920 Color: 7

Bin 192: 118 of cap free
Amount of items: 2
Items: 
Size: 11164 Color: 4
Size: 5118 Color: 1

Bin 193: 120 of cap free
Amount of items: 2
Items: 
Size: 12178 Color: 0
Size: 4102 Color: 18

Bin 194: 128 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 6
Size: 4600 Color: 0

Bin 195: 148 of cap free
Amount of items: 2
Items: 
Size: 12171 Color: 19
Size: 4081 Color: 4

Bin 196: 158 of cap free
Amount of items: 22
Items: 
Size: 912 Color: 19
Size: 902 Color: 5
Size: 880 Color: 16
Size: 872 Color: 0
Size: 864 Color: 1
Size: 848 Color: 1
Size: 784 Color: 8
Size: 744 Color: 5
Size: 736 Color: 17
Size: 736 Color: 3
Size: 732 Color: 18
Size: 724 Color: 18
Size: 716 Color: 7
Size: 708 Color: 10
Size: 708 Color: 2
Size: 704 Color: 6
Size: 700 Color: 7
Size: 696 Color: 9
Size: 652 Color: 10
Size: 590 Color: 12
Size: 574 Color: 11
Size: 460 Color: 6

Bin 197: 185 of cap free
Amount of items: 2
Items: 
Size: 10970 Color: 5
Size: 5245 Color: 16

Bin 198: 212 of cap free
Amount of items: 7
Items: 
Size: 8202 Color: 15
Size: 1524 Color: 13
Size: 1444 Color: 3
Size: 1414 Color: 17
Size: 1374 Color: 9
Size: 1366 Color: 19
Size: 864 Color: 1

Bin 199: 12148 of cap free
Amount of items: 14
Items: 
Size: 376 Color: 13
Size: 358 Color: 18
Size: 356 Color: 10
Size: 324 Color: 14
Size: 324 Color: 12
Size: 304 Color: 11
Size: 304 Color: 8
Size: 296 Color: 1
Size: 280 Color: 15
Size: 280 Color: 11
Size: 272 Color: 12
Size: 272 Color: 7
Size: 272 Color: 4
Size: 234 Color: 13

Total size: 3247200
Total free space: 16400

