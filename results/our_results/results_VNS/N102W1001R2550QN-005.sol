Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 102

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 90
Size: 294 Color: 45
Size: 275 Color: 26

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 97
Size: 265 Color: 15
Size: 252 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 75
Size: 343 Color: 59
Size: 279 Color: 31

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 69
Size: 355 Color: 63
Size: 279 Color: 30

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 85
Size: 305 Color: 50
Size: 284 Color: 36

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 82
Size: 309 Color: 51
Size: 284 Color: 37

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 94
Size: 271 Color: 22
Size: 251 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 89
Size: 290 Color: 40
Size: 280 Color: 34

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 81
Size: 325 Color: 56
Size: 278 Color: 29

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 100
Size: 261 Color: 11
Size: 250 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 95
Size: 266 Color: 18
Size: 252 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 74
Size: 362 Color: 68
Size: 263 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 73
Size: 355 Color: 64
Size: 271 Color: 23

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 98
Size: 265 Color: 16
Size: 250 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 87
Size: 315 Color: 54
Size: 265 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 83
Size: 299 Color: 46
Size: 292 Color: 42

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 88
Size: 301 Color: 47
Size: 273 Color: 24

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 92
Size: 280 Color: 32
Size: 262 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 76
Size: 372 Color: 72
Size: 250 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 93
Size: 269 Color: 20
Size: 269 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 101
Size: 250 Color: 0
Size: 253 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 91
Size: 290 Color: 41
Size: 278 Color: 28

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 77
Size: 323 Color: 55
Size: 293 Color: 44

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 79
Size: 338 Color: 57
Size: 274 Color: 25

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 86
Size: 302 Color: 48
Size: 285 Color: 38

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 80
Size: 312 Color: 53
Size: 293 Color: 43

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 71
Size: 344 Color: 60
Size: 287 Color: 39

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 78
Size: 338 Color: 58
Size: 275 Color: 27

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 67
Size: 359 Color: 65
Size: 280 Color: 33

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 96
Size: 263 Color: 14
Size: 254 Color: 9

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 99
Size: 261 Color: 10
Size: 253 Color: 8

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 62
Size: 349 Color: 61
Size: 303 Color: 49

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 70
Size: 361 Color: 66
Size: 271 Color: 21

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 84
Size: 309 Color: 52
Size: 281 Color: 35

Total size: 34034
Total free space: 0

