Capicity Bin: 9808
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 1
Size: 4408 Color: 0
Size: 480 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6158 Color: 0
Size: 3444 Color: 1
Size: 206 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 1
Size: 2908 Color: 1
Size: 608 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 0
Size: 2932 Color: 1
Size: 536 Color: 0

Bin 5: 0 of cap free
Amount of items: 5
Items: 
Size: 7151 Color: 1
Size: 1184 Color: 0
Size: 1063 Color: 0
Size: 262 Color: 0
Size: 148 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7904 Color: 0
Size: 1644 Color: 1
Size: 260 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7990 Color: 1
Size: 1186 Color: 0
Size: 632 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7996 Color: 1
Size: 1544 Color: 1
Size: 268 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8136 Color: 1
Size: 864 Color: 0
Size: 808 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8182 Color: 1
Size: 1350 Color: 0
Size: 276 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8214 Color: 1
Size: 1326 Color: 0
Size: 268 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 8150 Color: 0
Size: 1358 Color: 0
Size: 300 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8324 Color: 1
Size: 1316 Color: 0
Size: 168 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 8375 Color: 0
Size: 1195 Color: 0
Size: 238 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8434 Color: 1
Size: 1074 Color: 1
Size: 300 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8436 Color: 0
Size: 932 Color: 0
Size: 440 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 8762 Color: 1
Size: 572 Color: 0
Size: 474 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 8820 Color: 1
Size: 708 Color: 1
Size: 280 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 5557 Color: 0
Size: 3818 Color: 1
Size: 432 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 6101 Color: 0
Size: 3514 Color: 1
Size: 192 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 0
Size: 2491 Color: 1
Size: 326 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 0
Size: 2643 Color: 1
Size: 152 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 7283 Color: 1
Size: 2184 Color: 0
Size: 340 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 7318 Color: 1
Size: 2215 Color: 0
Size: 274 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 7461 Color: 0
Size: 2050 Color: 1
Size: 296 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 8005 Color: 0
Size: 1650 Color: 1
Size: 152 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 8155 Color: 1
Size: 980 Color: 0
Size: 672 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 8163 Color: 1
Size: 828 Color: 0
Size: 816 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 8195 Color: 1
Size: 1440 Color: 1
Size: 172 Color: 0

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 8407 Color: 0
Size: 1400 Color: 1

Bin 31: 2 of cap free
Amount of items: 16
Items: 
Size: 800 Color: 0
Size: 708 Color: 1
Size: 700 Color: 1
Size: 700 Color: 1
Size: 696 Color: 0
Size: 680 Color: 0
Size: 680 Color: 0
Size: 640 Color: 1
Size: 608 Color: 1
Size: 592 Color: 1
Size: 584 Color: 0
Size: 576 Color: 1
Size: 560 Color: 0
Size: 528 Color: 0
Size: 498 Color: 0
Size: 256 Color: 1

Bin 32: 2 of cap free
Amount of items: 8
Items: 
Size: 4904 Color: 0
Size: 816 Color: 0
Size: 816 Color: 0
Size: 816 Color: 0
Size: 808 Color: 1
Size: 752 Color: 1
Size: 708 Color: 1
Size: 186 Color: 1

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 6103 Color: 1
Size: 3543 Color: 0
Size: 160 Color: 1

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 1
Size: 2892 Color: 1
Size: 328 Color: 0

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 1
Size: 2678 Color: 0
Size: 288 Color: 1

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 7290 Color: 0
Size: 2516 Color: 1

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 7594 Color: 1
Size: 2212 Color: 0

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 7785 Color: 0
Size: 2021 Color: 1

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 7960 Color: 0
Size: 1702 Color: 0
Size: 144 Color: 1

Bin 40: 2 of cap free
Amount of items: 4
Items: 
Size: 8227 Color: 1
Size: 1419 Color: 0
Size: 88 Color: 0
Size: 72 Color: 1

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 8742 Color: 1
Size: 1064 Color: 0

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 0
Size: 2869 Color: 1
Size: 188 Color: 0

Bin 43: 3 of cap free
Amount of items: 4
Items: 
Size: 7156 Color: 1
Size: 1279 Color: 0
Size: 1194 Color: 0
Size: 176 Color: 1

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 7701 Color: 1
Size: 1874 Color: 1
Size: 230 Color: 0

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 7836 Color: 1
Size: 1757 Color: 0
Size: 212 Color: 1

Bin 46: 3 of cap free
Amount of items: 2
Items: 
Size: 8536 Color: 1
Size: 1269 Color: 0

Bin 47: 3 of cap free
Amount of items: 2
Items: 
Size: 8563 Color: 1
Size: 1242 Color: 0

Bin 48: 4 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 0
Size: 4240 Color: 0
Size: 616 Color: 1

Bin 49: 4 of cap free
Amount of items: 3
Items: 
Size: 7752 Color: 0
Size: 1804 Color: 0
Size: 248 Color: 1

Bin 50: 4 of cap free
Amount of items: 3
Items: 
Size: 7830 Color: 1
Size: 1846 Color: 0
Size: 128 Color: 1

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 7965 Color: 0
Size: 1371 Color: 0
Size: 468 Color: 1

Bin 52: 5 of cap free
Amount of items: 3
Items: 
Size: 5559 Color: 0
Size: 3712 Color: 0
Size: 532 Color: 1

Bin 53: 5 of cap free
Amount of items: 2
Items: 
Size: 5716 Color: 0
Size: 4087 Color: 1

Bin 54: 5 of cap free
Amount of items: 3
Items: 
Size: 6963 Color: 0
Size: 2488 Color: 1
Size: 352 Color: 1

Bin 55: 5 of cap free
Amount of items: 3
Items: 
Size: 8236 Color: 1
Size: 1503 Color: 0
Size: 64 Color: 1

Bin 56: 5 of cap free
Amount of items: 2
Items: 
Size: 8287 Color: 1
Size: 1516 Color: 0

Bin 57: 5 of cap free
Amount of items: 2
Items: 
Size: 8399 Color: 0
Size: 1404 Color: 1

Bin 58: 5 of cap free
Amount of items: 2
Items: 
Size: 8484 Color: 1
Size: 1319 Color: 0

Bin 59: 6 of cap free
Amount of items: 2
Items: 
Size: 8124 Color: 1
Size: 1678 Color: 0

Bin 60: 6 of cap free
Amount of items: 4
Items: 
Size: 8483 Color: 0
Size: 1175 Color: 1
Size: 112 Color: 1
Size: 32 Color: 0

Bin 61: 8 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 0
Size: 4410 Color: 1
Size: 160 Color: 1

Bin 62: 8 of cap free
Amount of items: 2
Items: 
Size: 5748 Color: 1
Size: 4052 Color: 0

Bin 63: 8 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 0
Size: 3042 Color: 0
Size: 160 Color: 1

Bin 64: 8 of cap free
Amount of items: 2
Items: 
Size: 8310 Color: 0
Size: 1490 Color: 1

Bin 65: 8 of cap free
Amount of items: 2
Items: 
Size: 8692 Color: 1
Size: 1108 Color: 0

Bin 66: 9 of cap free
Amount of items: 6
Items: 
Size: 4905 Color: 0
Size: 1856 Color: 1
Size: 856 Color: 0
Size: 846 Color: 0
Size: 808 Color: 1
Size: 528 Color: 1

Bin 67: 9 of cap free
Amount of items: 3
Items: 
Size: 6367 Color: 1
Size: 3272 Color: 0
Size: 160 Color: 1

Bin 68: 9 of cap free
Amount of items: 2
Items: 
Size: 7477 Color: 1
Size: 2322 Color: 0

Bin 69: 9 of cap free
Amount of items: 2
Items: 
Size: 8760 Color: 0
Size: 1039 Color: 1

Bin 70: 10 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 0
Size: 4044 Color: 0
Size: 200 Color: 1

Bin 71: 10 of cap free
Amount of items: 2
Items: 
Size: 8392 Color: 1
Size: 1406 Color: 0

Bin 72: 11 of cap free
Amount of items: 3
Items: 
Size: 7766 Color: 0
Size: 1943 Color: 1
Size: 88 Color: 1

Bin 73: 11 of cap free
Amount of items: 2
Items: 
Size: 8547 Color: 1
Size: 1250 Color: 0

Bin 74: 12 of cap free
Amount of items: 2
Items: 
Size: 6408 Color: 0
Size: 3388 Color: 1

Bin 75: 12 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 1
Size: 2840 Color: 1
Size: 160 Color: 0

Bin 76: 12 of cap free
Amount of items: 3
Items: 
Size: 7741 Color: 1
Size: 1295 Color: 0
Size: 760 Color: 0

Bin 77: 12 of cap free
Amount of items: 2
Items: 
Size: 7887 Color: 0
Size: 1909 Color: 1

Bin 78: 12 of cap free
Amount of items: 2
Items: 
Size: 8578 Color: 0
Size: 1218 Color: 1

Bin 79: 13 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 1
Size: 2708 Color: 0
Size: 272 Color: 0

Bin 80: 13 of cap free
Amount of items: 2
Items: 
Size: 8626 Color: 1
Size: 1169 Color: 0

Bin 81: 14 of cap free
Amount of items: 2
Items: 
Size: 7806 Color: 1
Size: 1988 Color: 0

Bin 82: 14 of cap free
Amount of items: 2
Items: 
Size: 8107 Color: 1
Size: 1687 Color: 0

Bin 83: 15 of cap free
Amount of items: 4
Items: 
Size: 4916 Color: 1
Size: 3541 Color: 1
Size: 986 Color: 0
Size: 350 Color: 0

Bin 84: 15 of cap free
Amount of items: 3
Items: 
Size: 7496 Color: 1
Size: 1925 Color: 1
Size: 372 Color: 0

Bin 85: 15 of cap free
Amount of items: 2
Items: 
Size: 8275 Color: 0
Size: 1518 Color: 1

Bin 86: 16 of cap free
Amount of items: 2
Items: 
Size: 8122 Color: 0
Size: 1670 Color: 1

Bin 87: 16 of cap free
Amount of items: 2
Items: 
Size: 8255 Color: 1
Size: 1537 Color: 0

Bin 88: 16 of cap free
Amount of items: 2
Items: 
Size: 8410 Color: 0
Size: 1382 Color: 1

Bin 89: 17 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 0
Size: 2495 Color: 1
Size: 274 Color: 0

Bin 90: 17 of cap free
Amount of items: 2
Items: 
Size: 8190 Color: 1
Size: 1601 Color: 0

Bin 91: 19 of cap free
Amount of items: 4
Items: 
Size: 4908 Color: 0
Size: 3519 Color: 0
Size: 874 Color: 1
Size: 488 Color: 1

Bin 92: 20 of cap free
Amount of items: 2
Items: 
Size: 7192 Color: 0
Size: 2596 Color: 1

Bin 93: 22 of cap free
Amount of items: 4
Items: 
Size: 4910 Color: 0
Size: 3522 Color: 0
Size: 890 Color: 1
Size: 464 Color: 1

Bin 94: 24 of cap free
Amount of items: 3
Items: 
Size: 4936 Color: 0
Size: 4584 Color: 1
Size: 264 Color: 0

Bin 95: 24 of cap free
Amount of items: 2
Items: 
Size: 8280 Color: 1
Size: 1504 Color: 0

Bin 96: 24 of cap free
Amount of items: 2
Items: 
Size: 8748 Color: 1
Size: 1036 Color: 0

Bin 97: 30 of cap free
Amount of items: 2
Items: 
Size: 7428 Color: 1
Size: 2350 Color: 0

Bin 98: 30 of cap free
Amount of items: 2
Items: 
Size: 8490 Color: 0
Size: 1288 Color: 1

Bin 99: 31 of cap free
Amount of items: 2
Items: 
Size: 8533 Color: 0
Size: 1244 Color: 1

Bin 100: 33 of cap free
Amount of items: 4
Items: 
Size: 4907 Color: 1
Size: 3540 Color: 1
Size: 908 Color: 0
Size: 420 Color: 0

Bin 101: 34 of cap free
Amount of items: 2
Items: 
Size: 7124 Color: 0
Size: 2650 Color: 1

Bin 102: 35 of cap free
Amount of items: 2
Items: 
Size: 7087 Color: 0
Size: 2686 Color: 1

Bin 103: 36 of cap free
Amount of items: 2
Items: 
Size: 8442 Color: 0
Size: 1330 Color: 1

Bin 104: 40 of cap free
Amount of items: 2
Items: 
Size: 5684 Color: 1
Size: 4084 Color: 0

Bin 105: 42 of cap free
Amount of items: 2
Items: 
Size: 8574 Color: 0
Size: 1192 Color: 1

Bin 106: 42 of cap free
Amount of items: 2
Items: 
Size: 8664 Color: 0
Size: 1102 Color: 1

Bin 107: 44 of cap free
Amount of items: 2
Items: 
Size: 4964 Color: 1
Size: 4800 Color: 0

Bin 108: 46 of cap free
Amount of items: 2
Items: 
Size: 8794 Color: 0
Size: 968 Color: 1

Bin 109: 48 of cap free
Amount of items: 2
Items: 
Size: 8572 Color: 0
Size: 1188 Color: 1

Bin 110: 50 of cap free
Amount of items: 29
Items: 
Size: 544 Color: 1
Size: 496 Color: 0
Size: 480 Color: 0
Size: 412 Color: 0
Size: 408 Color: 1
Size: 392 Color: 0
Size: 388 Color: 0
Size: 384 Color: 1
Size: 384 Color: 1
Size: 368 Color: 0
Size: 336 Color: 1
Size: 336 Color: 1
Size: 332 Color: 1
Size: 332 Color: 0
Size: 328 Color: 1
Size: 328 Color: 0
Size: 320 Color: 1
Size: 306 Color: 1
Size: 304 Color: 0
Size: 296 Color: 1
Size: 282 Color: 1
Size: 280 Color: 1
Size: 258 Color: 0
Size: 254 Color: 1
Size: 252 Color: 0
Size: 244 Color: 1
Size: 240 Color: 0
Size: 240 Color: 0
Size: 234 Color: 0

Bin 111: 50 of cap free
Amount of items: 2
Items: 
Size: 8792 Color: 0
Size: 966 Color: 1

Bin 112: 51 of cap free
Amount of items: 2
Items: 
Size: 7652 Color: 0
Size: 2105 Color: 1

Bin 113: 51 of cap free
Amount of items: 2
Items: 
Size: 8378 Color: 0
Size: 1379 Color: 1

Bin 114: 53 of cap free
Amount of items: 2
Items: 
Size: 8650 Color: 1
Size: 1105 Color: 0

Bin 115: 56 of cap free
Amount of items: 2
Items: 
Size: 7562 Color: 0
Size: 2190 Color: 1

Bin 116: 59 of cap free
Amount of items: 2
Items: 
Size: 8322 Color: 1
Size: 1427 Color: 0

Bin 117: 62 of cap free
Amount of items: 2
Items: 
Size: 8644 Color: 0
Size: 1102 Color: 1

Bin 118: 66 of cap free
Amount of items: 2
Items: 
Size: 8022 Color: 0
Size: 1720 Color: 1

Bin 119: 73 of cap free
Amount of items: 3
Items: 
Size: 5272 Color: 0
Size: 3412 Color: 1
Size: 1051 Color: 1

Bin 120: 78 of cap free
Amount of items: 4
Items: 
Size: 4906 Color: 0
Size: 3494 Color: 1
Size: 888 Color: 0
Size: 442 Color: 1

Bin 121: 82 of cap free
Amount of items: 2
Items: 
Size: 7798 Color: 1
Size: 1928 Color: 0

Bin 122: 84 of cap free
Amount of items: 2
Items: 
Size: 4932 Color: 0
Size: 4792 Color: 1

Bin 123: 86 of cap free
Amount of items: 2
Items: 
Size: 5636 Color: 0
Size: 4086 Color: 1

Bin 124: 87 of cap free
Amount of items: 2
Items: 
Size: 6630 Color: 0
Size: 3091 Color: 1

Bin 125: 87 of cap free
Amount of items: 2
Items: 
Size: 7350 Color: 0
Size: 2371 Color: 1

Bin 126: 107 of cap free
Amount of items: 2
Items: 
Size: 7623 Color: 0
Size: 2078 Color: 1

Bin 127: 116 of cap free
Amount of items: 2
Items: 
Size: 6146 Color: 1
Size: 3546 Color: 0

Bin 128: 117 of cap free
Amount of items: 2
Items: 
Size: 6637 Color: 1
Size: 3054 Color: 0

Bin 129: 122 of cap free
Amount of items: 2
Items: 
Size: 5618 Color: 1
Size: 4068 Color: 0

Bin 130: 128 of cap free
Amount of items: 2
Items: 
Size: 5896 Color: 1
Size: 3784 Color: 0

Bin 131: 129 of cap free
Amount of items: 2
Items: 
Size: 5594 Color: 0
Size: 4085 Color: 1

Bin 132: 138 of cap free
Amount of items: 2
Items: 
Size: 5586 Color: 0
Size: 4084 Color: 1

Bin 133: 6778 of cap free
Amount of items: 15
Items: 
Size: 236 Color: 1
Size: 232 Color: 1
Size: 224 Color: 0
Size: 220 Color: 1
Size: 216 Color: 1
Size: 216 Color: 0
Size: 210 Color: 0
Size: 208 Color: 0
Size: 196 Color: 1
Size: 186 Color: 0
Size: 184 Color: 1
Size: 184 Color: 1
Size: 184 Color: 0
Size: 176 Color: 0
Size: 158 Color: 0

Total size: 1294656
Total free space: 9808

