Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 10002

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 373067 Color: 7243
Size: 368066 Color: 7109
Size: 258868 Color: 1202

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 349357 Color: 6437
Size: 335630 Color: 5887
Size: 315014 Color: 4947

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 347622 Color: 6375
Size: 329692 Color: 5616
Size: 322687 Color: 5305

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 379398 Color: 7448
Size: 342757 Color: 6178
Size: 277846 Color: 2772

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 350612 Color: 6484
Size: 336197 Color: 5909
Size: 313192 Color: 4858

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 396983 Color: 7976
Size: 323679 Color: 5346
Size: 279339 Color: 2884

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 372408 Color: 7226
Size: 347445 Color: 6369
Size: 280148 Color: 2931

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 381195 Color: 7499
Size: 340842 Color: 6103
Size: 277964 Color: 2783

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 395729 Color: 7941
Size: 311782 Color: 4777
Size: 292490 Color: 3722

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 386363 Color: 7649
Size: 334626 Color: 5830
Size: 279012 Color: 2859

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 386699 Color: 7662
Size: 343750 Color: 6218
Size: 269552 Color: 2125

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 386894 Color: 7668
Size: 342775 Color: 6179
Size: 270332 Color: 2191

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 387500 Color: 7683
Size: 339295 Color: 6031
Size: 273206 Color: 2424

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 411378 Color: 8331
Size: 335489 Color: 5883
Size: 253134 Color: 509

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 364384 Color: 6990
Size: 359638 Color: 6828
Size: 275979 Color: 2638

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 361774 Color: 6900
Size: 346953 Color: 6351
Size: 291274 Color: 3643

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 384652 Color: 7597
Size: 340907 Color: 6106
Size: 274442 Color: 2520

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 389571 Color: 7744
Size: 329566 Color: 5609
Size: 280864 Color: 2978

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 393724 Color: 7875
Size: 340747 Color: 6099
Size: 265530 Color: 1785

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 394119 Color: 7894
Size: 339876 Color: 6058
Size: 266006 Color: 1830

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 388960 Color: 7718
Size: 340374 Color: 6085
Size: 270667 Color: 2220

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 389772 Color: 7749
Size: 339138 Color: 6030
Size: 271091 Color: 2255

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 395422 Color: 7926
Size: 338541 Color: 6013
Size: 266038 Color: 1836

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 368999 Color: 7135
Size: 355851 Color: 6687
Size: 275151 Color: 2584

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 387941 Color: 7697
Size: 326721 Color: 5490
Size: 285339 Color: 3261

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 350291 Color: 6471
Size: 339544 Color: 6043
Size: 310166 Color: 4685

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 374533 Color: 7292
Size: 340347 Color: 6084
Size: 285121 Color: 3241

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 383239 Color: 7561
Size: 341747 Color: 6140
Size: 275015 Color: 2566

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 382590 Color: 7540
Size: 354813 Color: 6652
Size: 262598 Color: 1560

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 405869 Color: 8205
Size: 340202 Color: 6079
Size: 253930 Color: 608

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 389023 Color: 7722
Size: 334179 Color: 5810
Size: 276799 Color: 2707

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 393727 Color: 7876
Size: 335414 Color: 5879
Size: 270860 Color: 2234

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 356261 Color: 6713
Size: 355912 Color: 6691
Size: 287828 Color: 3428

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 372856 Color: 7238
Size: 360753 Color: 6859
Size: 266392 Color: 1860

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 374768 Color: 7297
Size: 349225 Color: 6433
Size: 276008 Color: 2641

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 381905 Color: 7524
Size: 342477 Color: 6164
Size: 275619 Color: 2611

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 365239 Color: 7015
Size: 317634 Color: 5079
Size: 317128 Color: 5051

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 383984 Color: 7579
Size: 348606 Color: 6409
Size: 267411 Color: 1942

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 368208 Color: 7111
Size: 358056 Color: 6768
Size: 273737 Color: 2460

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 358057 Color: 6770
Size: 351995 Color: 6551
Size: 289949 Color: 3557

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 365894 Color: 7029
Size: 345037 Color: 6276
Size: 289070 Color: 3504

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 353928 Color: 6619
Size: 353453 Color: 6600
Size: 292620 Color: 3731

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 375467 Color: 7322
Size: 332735 Color: 5746
Size: 291799 Color: 3686

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 379443 Color: 7449
Size: 362504 Color: 6930
Size: 258054 Color: 1116

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 378811 Color: 7429
Size: 343167 Color: 6196
Size: 278023 Color: 2790

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 377264 Color: 7379
Size: 337908 Color: 5988
Size: 284829 Color: 3224

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 378757 Color: 7427
Size: 337802 Color: 5982
Size: 283442 Color: 3139

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 390658 Color: 7776
Size: 351771 Color: 6540
Size: 257572 Color: 1059

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 397281 Color: 7982
Size: 328985 Color: 5579
Size: 273735 Color: 2459

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 389822 Color: 7751
Size: 338539 Color: 6012
Size: 271640 Color: 2305

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 367956 Color: 7100
Size: 355201 Color: 6658
Size: 276844 Color: 2711

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 367662 Color: 7084
Size: 352085 Color: 6554
Size: 280254 Color: 2941

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 380421 Color: 7474
Size: 351924 Color: 6544
Size: 267656 Color: 1959

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 368579 Color: 7124
Size: 324978 Color: 5403
Size: 306444 Color: 4480

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 387179 Color: 7673
Size: 348682 Color: 6414
Size: 264140 Color: 1676

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 387108 Color: 7671
Size: 361187 Color: 6879
Size: 251706 Color: 303

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 385902 Color: 7632
Size: 357281 Color: 6740
Size: 256818 Color: 973

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 386752 Color: 7664
Size: 350326 Color: 6472
Size: 262923 Color: 1583

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 397870 Color: 8001
Size: 348612 Color: 6411
Size: 253519 Color: 557

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 390168 Color: 7760
Size: 347308 Color: 6364
Size: 262525 Color: 1554

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 371379 Color: 7192
Size: 348285 Color: 6393
Size: 280337 Color: 2945

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 381680 Color: 7517
Size: 351497 Color: 6526
Size: 266824 Color: 1895

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 377611 Color: 7388
Size: 326221 Color: 5465
Size: 296169 Color: 3936

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 369114 Color: 7140
Size: 346144 Color: 6318
Size: 284743 Color: 3217

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 366784 Color: 7056
Size: 359756 Color: 6835
Size: 273461 Color: 2445

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 399799 Color: 8052
Size: 333289 Color: 5771
Size: 266913 Color: 1905

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 378313 Color: 7414
Size: 352362 Color: 6567
Size: 269326 Color: 2105

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 389358 Color: 7737
Size: 337255 Color: 5955
Size: 273388 Color: 2439

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 363617 Color: 6966
Size: 324052 Color: 5361
Size: 312332 Color: 4806

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 379905 Color: 7461
Size: 343028 Color: 6190
Size: 277068 Color: 2724

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 389143 Color: 7727
Size: 346792 Color: 6344
Size: 264066 Color: 1670

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 381621 Color: 7513
Size: 328383 Color: 5557
Size: 289997 Color: 3560

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 402185 Color: 8102
Size: 322065 Color: 5282
Size: 275751 Color: 2620

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 404852 Color: 8175
Size: 335631 Color: 5888
Size: 259518 Color: 1267

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 375124 Color: 7309
Size: 359768 Color: 6837
Size: 265109 Color: 1752

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 383833 Color: 7578
Size: 341357 Color: 6126
Size: 274811 Color: 2553

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 394078 Color: 7891
Size: 339863 Color: 6057
Size: 266060 Color: 1838

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 396835 Color: 7974
Size: 344512 Color: 6254
Size: 258654 Color: 1172

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 381923 Color: 7526
Size: 340285 Color: 6081
Size: 277793 Color: 2770

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 345185 Color: 6280
Size: 333545 Color: 5779
Size: 321271 Color: 5239

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 409641 Color: 8285
Size: 338445 Color: 6008
Size: 251915 Color: 336

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 386388 Color: 7650
Size: 333839 Color: 5795
Size: 279774 Color: 2901

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 367507 Color: 7079
Size: 351989 Color: 6550
Size: 280505 Color: 2960

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 405847 Color: 8203
Size: 333627 Color: 5784
Size: 260527 Color: 1378

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 393853 Color: 7879
Size: 328980 Color: 5578
Size: 277168 Color: 2732

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 379850 Color: 7458
Size: 343014 Color: 6189
Size: 277137 Color: 2727

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 341113 Color: 6116
Size: 330744 Color: 5655
Size: 328144 Color: 5545

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 389358 Color: 7736
Size: 339375 Color: 6035
Size: 271268 Color: 2270

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 377643 Color: 7390
Size: 329376 Color: 5598
Size: 292982 Color: 3753

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 366768 Color: 7055
Size: 354793 Color: 6650
Size: 278440 Color: 2823

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 393617 Color: 7872
Size: 331969 Color: 5710
Size: 274415 Color: 2516

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 382819 Color: 7553
Size: 342132 Color: 6155
Size: 275050 Color: 2574

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 364580 Color: 6995
Size: 345519 Color: 6291
Size: 289902 Color: 3556

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 373954 Color: 7281
Size: 314429 Color: 4925
Size: 311618 Color: 4763

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 372638 Color: 7233
Size: 348960 Color: 6424
Size: 278403 Color: 2818

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 343003 Color: 6187
Size: 328713 Color: 5571
Size: 328285 Color: 5551

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 384994 Color: 7606
Size: 341363 Color: 6127
Size: 273644 Color: 2454

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 364061 Color: 6976
Size: 345013 Color: 6274
Size: 290927 Color: 3620

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 366549 Color: 7048
Size: 347355 Color: 6365
Size: 286097 Color: 3320

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 382807 Color: 7552
Size: 361161 Color: 6875
Size: 256033 Color: 868

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 386991 Color: 7670
Size: 354628 Color: 6644
Size: 258382 Color: 1150

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 394060 Color: 7890
Size: 339312 Color: 6032
Size: 266629 Color: 1874

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 380994 Color: 7491
Size: 365973 Color: 7030
Size: 253034 Color: 490

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 365496 Color: 7022
Size: 337973 Color: 5992
Size: 296532 Color: 3957

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 371367 Color: 7191
Size: 356924 Color: 6731
Size: 271710 Color: 2309

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 385340 Color: 7617
Size: 342797 Color: 6180
Size: 271864 Color: 2320

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 389353 Color: 7735
Size: 334631 Color: 5832
Size: 276017 Color: 2642

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 380982 Color: 7490
Size: 352355 Color: 6566
Size: 266664 Color: 1877

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 381539 Color: 7512
Size: 345960 Color: 6307
Size: 272502 Color: 2371

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 376931 Color: 7372
Size: 357578 Color: 6753
Size: 265492 Color: 1783

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 399083 Color: 8034
Size: 339405 Color: 6037
Size: 261513 Color: 1468

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 397247 Color: 7981
Size: 347667 Color: 6378
Size: 255087 Color: 745

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 399472 Color: 8045
Size: 312250 Color: 4804
Size: 288279 Color: 3460

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 395878 Color: 7950
Size: 329779 Color: 5618
Size: 274344 Color: 2506

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 380848 Color: 7487
Size: 347906 Color: 6384
Size: 271247 Color: 2266

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 357864 Color: 6762
Size: 334843 Color: 5841
Size: 307294 Color: 4522

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 378512 Color: 7420
Size: 325525 Color: 5437
Size: 295964 Color: 3927

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 381289 Color: 7502
Size: 361891 Color: 6904
Size: 256821 Color: 975

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 378203 Color: 7412
Size: 330942 Color: 5662
Size: 290856 Color: 3614

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 411152 Color: 8325
Size: 338309 Color: 6003
Size: 250540 Color: 99

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 409424 Color: 8281
Size: 305957 Color: 4459
Size: 284620 Color: 3211

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 375447 Color: 7321
Size: 366106 Color: 7034
Size: 258448 Color: 1156

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 395285 Color: 7923
Size: 330744 Color: 5656
Size: 273972 Color: 2486

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 410856 Color: 8316
Size: 299783 Color: 4119
Size: 289362 Color: 3526

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 366023 Color: 7032
Size: 344924 Color: 6270
Size: 289054 Color: 3501

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 385697 Color: 7629
Size: 347514 Color: 6372
Size: 266790 Color: 1890

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 397299 Color: 7983
Size: 331055 Color: 5670
Size: 271647 Color: 2306

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 377919 Color: 7399
Size: 359651 Color: 6829
Size: 262431 Color: 1545

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 369380 Color: 7147
Size: 348005 Color: 6389
Size: 282616 Color: 3093

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 386918 Color: 7669
Size: 323308 Color: 5329
Size: 289775 Color: 3550

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 380328 Color: 7471
Size: 341093 Color: 6114
Size: 278580 Color: 2833

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 388838 Color: 7715
Size: 353680 Color: 6607
Size: 257483 Color: 1050

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 382901 Color: 7554
Size: 342566 Color: 6167
Size: 274534 Color: 2527

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 409084 Color: 8272
Size: 337184 Color: 5952
Size: 253733 Color: 579

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 394350 Color: 7902
Size: 339777 Color: 6053
Size: 265874 Color: 1813

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 395110 Color: 7917
Size: 339965 Color: 6063
Size: 264926 Color: 1730

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 388017 Color: 7699
Size: 344777 Color: 6264
Size: 267207 Color: 1928

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 388608 Color: 7713
Size: 341453 Color: 6130
Size: 269940 Color: 2163

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 381171 Color: 7498
Size: 354037 Color: 6622
Size: 264793 Color: 1720

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 380675 Color: 7483
Size: 356185 Color: 6710
Size: 263141 Color: 1604

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 367507 Color: 7078
Size: 360216 Color: 6850
Size: 272278 Color: 2356

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 410953 Color: 8319
Size: 338529 Color: 6010
Size: 250519 Color: 91

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 411383 Color: 8332
Size: 332581 Color: 5735
Size: 256037 Color: 869

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 382257 Color: 7532
Size: 355294 Color: 6669
Size: 262450 Color: 1547

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 397594 Color: 7993
Size: 333493 Color: 5776
Size: 268914 Color: 2075

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 389547 Color: 7743
Size: 339368 Color: 6034
Size: 271086 Color: 2254

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 388422 Color: 7709
Size: 339894 Color: 6060
Size: 271685 Color: 2308

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 410649 Color: 8305
Size: 325298 Color: 5420
Size: 264054 Color: 1669

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 367925 Color: 7098
Size: 341200 Color: 6119
Size: 290876 Color: 3618

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 394099 Color: 7892
Size: 306068 Color: 4465
Size: 299834 Color: 4121

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 388003 Color: 7698
Size: 327055 Color: 5502
Size: 284943 Color: 3231

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 382324 Color: 7535
Size: 341900 Color: 6148
Size: 275777 Color: 2622

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 390295 Color: 7764
Size: 339087 Color: 6027
Size: 270619 Color: 2217

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 377406 Color: 7382
Size: 358428 Color: 6783
Size: 264167 Color: 1680

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 378462 Color: 7419
Size: 360827 Color: 6863
Size: 260712 Color: 1391

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 368857 Color: 7132
Size: 355275 Color: 6666
Size: 275869 Color: 2628

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 385416 Color: 7624
Size: 346902 Color: 6349
Size: 267683 Color: 1963

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 367324 Color: 7074
Size: 328963 Color: 5577
Size: 303714 Color: 4331

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 374076 Color: 7284
Size: 373525 Color: 7263
Size: 252400 Color: 404

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 397783 Color: 7998
Size: 350611 Color: 6483
Size: 251607 Color: 284

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 379720 Color: 7454
Size: 342927 Color: 6184
Size: 277354 Color: 2743

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 374964 Color: 7306
Size: 368419 Color: 7119
Size: 256618 Color: 948

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 387571 Color: 7684
Size: 347756 Color: 6381
Size: 264674 Color: 1712

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 398173 Color: 8009
Size: 344330 Color: 6245
Size: 257498 Color: 1053

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 369720 Color: 7154
Size: 361208 Color: 6880
Size: 269073 Color: 2082

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 369542 Color: 7152
Size: 362737 Color: 6936
Size: 267722 Color: 1970

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 367745 Color: 7088
Size: 363265 Color: 6950
Size: 268991 Color: 2077

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 405159 Color: 8182
Size: 339992 Color: 6064
Size: 254850 Color: 723

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 381485 Color: 7510
Size: 334380 Color: 5821
Size: 284136 Color: 3183

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 368023 Color: 7106
Size: 361662 Color: 6896
Size: 270316 Color: 2189

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 411480 Color: 8334
Size: 328546 Color: 5565
Size: 259975 Color: 1311

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 388944 Color: 7717
Size: 356253 Color: 6712
Size: 254804 Color: 715

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 388035 Color: 7700
Size: 341010 Color: 6110
Size: 270956 Color: 2244

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 337107 Color: 5948
Size: 332708 Color: 5745
Size: 330186 Color: 5636

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 393970 Color: 7887
Size: 340077 Color: 6072
Size: 265954 Color: 1824

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 400839 Color: 8076
Size: 348985 Color: 6425
Size: 250177 Color: 35

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 385406 Color: 7622
Size: 357547 Color: 6751
Size: 257048 Color: 1003

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 374916 Color: 7304
Size: 348905 Color: 6422
Size: 276180 Color: 2655

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 350784 Color: 6493
Size: 337351 Color: 5959
Size: 311866 Color: 4781

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 377729 Color: 7392
Size: 361467 Color: 6887
Size: 260805 Color: 1404

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 365646 Color: 7025
Size: 361260 Color: 6882
Size: 273095 Color: 2413

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 387296 Color: 7677
Size: 319261 Color: 5149
Size: 293444 Color: 3778

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 383459 Color: 7567
Size: 343461 Color: 6209
Size: 273081 Color: 2412

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 384011 Color: 7580
Size: 341240 Color: 6123
Size: 274750 Color: 2544

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 374912 Color: 7303
Size: 344431 Color: 6249
Size: 280658 Color: 2966

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 390840 Color: 7779
Size: 337686 Color: 5976
Size: 271475 Color: 2288

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 383824 Color: 7577
Size: 341109 Color: 6115
Size: 275068 Color: 2577

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 410901 Color: 8318
Size: 336854 Color: 5934
Size: 252246 Color: 383

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 411371 Color: 8330
Size: 338420 Color: 6007
Size: 250210 Color: 42

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 402769 Color: 8124
Size: 334292 Color: 5816
Size: 262940 Color: 1584

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 404523 Color: 8167
Size: 339036 Color: 6024
Size: 256442 Color: 930

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 374875 Color: 7301
Size: 344449 Color: 6251
Size: 280677 Color: 2969

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 374900 Color: 7302
Size: 363007 Color: 6941
Size: 262094 Color: 1515

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 369107 Color: 7139
Size: 337674 Color: 5974
Size: 293220 Color: 3767

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 408498 Color: 8263
Size: 321920 Color: 5275
Size: 269583 Color: 2127

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 394204 Color: 7899
Size: 332981 Color: 5760
Size: 272816 Color: 2394

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 389206 Color: 7728
Size: 339404 Color: 6036
Size: 271391 Color: 2281

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 387843 Color: 7694
Size: 328104 Color: 5543
Size: 284054 Color: 3178

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 389832 Color: 7752
Size: 339035 Color: 6023
Size: 271134 Color: 2261

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 366718 Color: 7054
Size: 332014 Color: 5714
Size: 301269 Color: 4201

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 384401 Color: 7587
Size: 345378 Color: 6286
Size: 270222 Color: 2181

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 367277 Color: 7066
Size: 365435 Color: 7021
Size: 267289 Color: 1933

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 402043 Color: 8094
Size: 328277 Color: 5550
Size: 269681 Color: 2137

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 367322 Color: 7073
Size: 364886 Color: 7005
Size: 267793 Color: 1974

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 373497 Color: 7261
Size: 329058 Color: 5581
Size: 297446 Color: 4007

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 399448 Color: 8044
Size: 320303 Color: 5195
Size: 280250 Color: 2940

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 366853 Color: 7059
Size: 365186 Color: 7011
Size: 267962 Color: 1989

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 364899 Color: 7006
Size: 337630 Color: 5972
Size: 297472 Color: 4010

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 409766 Color: 8287
Size: 317741 Color: 5085
Size: 272494 Color: 2369

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 377513 Color: 7387
Size: 343789 Color: 6220
Size: 278699 Color: 2843

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 403604 Color: 8142
Size: 325823 Color: 5452
Size: 270574 Color: 2215

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 410767 Color: 8312
Size: 338725 Color: 6019
Size: 250509 Color: 88

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 379380 Color: 7446
Size: 341243 Color: 6124
Size: 279378 Color: 2885

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 390561 Color: 7774
Size: 338665 Color: 6015
Size: 270775 Color: 2229

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 367299 Color: 7068
Size: 348509 Color: 6401
Size: 284193 Color: 3186

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 361407 Color: 6885
Size: 346616 Color: 6338
Size: 291978 Color: 3698

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 411026 Color: 8320
Size: 327487 Color: 5520
Size: 261488 Color: 1464

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 381757 Color: 7522
Size: 342182 Color: 6157
Size: 276062 Color: 2646

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 410669 Color: 8309
Size: 332941 Color: 5755
Size: 256391 Color: 925

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 402624 Color: 8118
Size: 326996 Color: 5501
Size: 270381 Color: 2194

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 389866 Color: 7753
Size: 346815 Color: 6345
Size: 263320 Color: 1615

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 410716 Color: 8311
Size: 336556 Color: 5926
Size: 252729 Color: 449

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 388407 Color: 7708
Size: 339644 Color: 6048
Size: 271950 Color: 2326

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 410450 Color: 8301
Size: 338283 Color: 6001
Size: 251268 Color: 234

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 385488 Color: 7625
Size: 340541 Color: 6094
Size: 273972 Color: 2485

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 389233 Color: 7729
Size: 337135 Color: 5949
Size: 273633 Color: 2453

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 390076 Color: 7756
Size: 357790 Color: 6759
Size: 252135 Color: 368

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 379589 Color: 7450
Size: 357551 Color: 6752
Size: 262861 Color: 1579

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 348601 Color: 6408
Size: 338105 Color: 5997
Size: 313295 Color: 4863

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 383684 Color: 7573
Size: 358119 Color: 6774
Size: 258198 Color: 1136

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 410487 Color: 8303
Size: 337620 Color: 5971
Size: 251894 Color: 333

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 411056 Color: 8321
Size: 323211 Color: 5325
Size: 265734 Color: 1804

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 401720 Color: 8089
Size: 325352 Color: 5422
Size: 272929 Color: 2401

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 387413 Color: 7682
Size: 312096 Color: 4796
Size: 300492 Color: 4159

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 381749 Color: 7521
Size: 363369 Color: 6955
Size: 254883 Color: 729

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 400215 Color: 8065
Size: 344037 Color: 6231
Size: 255749 Color: 831

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 410664 Color: 8307
Size: 311182 Color: 4746
Size: 278155 Color: 2798

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 410378 Color: 8299
Size: 335728 Color: 5892
Size: 253895 Color: 600

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 383471 Color: 7568
Size: 348420 Color: 6399
Size: 268110 Color: 2003

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 395601 Color: 7936
Size: 352649 Color: 6575
Size: 251751 Color: 315

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 386235 Color: 7644
Size: 316138 Color: 5003
Size: 297628 Color: 4017

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 406586 Color: 8230
Size: 332531 Color: 5733
Size: 260884 Color: 1412

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 383453 Color: 7566
Size: 341505 Color: 6133
Size: 275043 Color: 2571

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 409223 Color: 8276
Size: 339997 Color: 6066
Size: 250781 Color: 143

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 396764 Color: 7970
Size: 327245 Color: 5514
Size: 275992 Color: 2639

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 409826 Color: 8289
Size: 337353 Color: 5960
Size: 252822 Color: 460

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 409379 Color: 8280
Size: 314861 Color: 4941
Size: 275761 Color: 2621

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 369226 Color: 7143
Size: 356292 Color: 6715
Size: 274483 Color: 2524

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 409967 Color: 8293
Size: 329520 Color: 5605
Size: 260514 Color: 1375

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 403977 Color: 8152
Size: 332470 Color: 5731
Size: 263554 Color: 1636

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 409752 Color: 8286
Size: 339315 Color: 6033
Size: 250934 Color: 176

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 410238 Color: 8296
Size: 337299 Color: 5958
Size: 252464 Color: 413

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 368005 Color: 7105
Size: 341180 Color: 6118
Size: 290816 Color: 3610

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 400019 Color: 8060
Size: 306242 Color: 4471
Size: 293740 Color: 3790

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 404245 Color: 8158
Size: 336905 Color: 5940
Size: 258851 Color: 1198

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 410884 Color: 8317
Size: 335217 Color: 5869
Size: 253900 Color: 602

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 409432 Color: 8282
Size: 338674 Color: 6016
Size: 251895 Color: 334

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 409919 Color: 8291
Size: 337159 Color: 5950
Size: 252923 Color: 477

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 408021 Color: 8255
Size: 328300 Color: 5552
Size: 263680 Color: 1642

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 344673 Color: 6258
Size: 338306 Color: 6002
Size: 317022 Color: 5046

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 411143 Color: 8324
Size: 334607 Color: 5829
Size: 254251 Color: 652

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 394507 Color: 7903
Size: 353240 Color: 6591
Size: 252254 Color: 384

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 407108 Color: 8237
Size: 340827 Color: 6101
Size: 252066 Color: 362

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 337956 Color: 5990
Size: 335063 Color: 5856
Size: 326982 Color: 5500

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 399706 Color: 8051
Size: 346070 Color: 6313
Size: 254225 Color: 646

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 408822 Color: 8268
Size: 336923 Color: 5942
Size: 254256 Color: 653

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 410141 Color: 8294
Size: 325700 Color: 5448
Size: 264160 Color: 1677

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 408564 Color: 8266
Size: 337677 Color: 5975
Size: 253760 Color: 583

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 371242 Color: 7189
Size: 351943 Color: 6546
Size: 276816 Color: 2709

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 408562 Color: 8265
Size: 340021 Color: 6069
Size: 251418 Color: 247

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 395608 Color: 7938
Size: 333851 Color: 5796
Size: 270542 Color: 2212

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 335206 Color: 5865
Size: 332586 Color: 5736
Size: 332209 Color: 5722

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 408315 Color: 8261
Size: 336432 Color: 5924
Size: 255254 Color: 768

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 408191 Color: 8259
Size: 332750 Color: 5747
Size: 259060 Color: 1218

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 405582 Color: 8197
Size: 332294 Color: 5725
Size: 262125 Color: 1519

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 402585 Color: 8116
Size: 327788 Color: 5532
Size: 269628 Color: 2131

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 397912 Color: 8003
Size: 351639 Color: 6534
Size: 250450 Color: 77

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 408086 Color: 8256
Size: 332059 Color: 5718
Size: 259856 Color: 1300

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 352332 Color: 6562
Size: 325700 Color: 5447
Size: 321969 Color: 5276

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 404427 Color: 8165
Size: 338090 Color: 5995
Size: 257484 Color: 1051

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 407995 Color: 8254
Size: 338118 Color: 5998
Size: 253888 Color: 597

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 406498 Color: 8225
Size: 336292 Color: 5916
Size: 257211 Color: 1022

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 404159 Color: 8157
Size: 339811 Color: 6054
Size: 256031 Color: 867

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 372593 Color: 7231
Size: 356178 Color: 6709
Size: 271230 Color: 2265

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 407945 Color: 8252
Size: 338256 Color: 6000
Size: 253800 Color: 588

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 410852 Color: 8315
Size: 331407 Color: 5685
Size: 257742 Color: 1079

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 410814 Color: 8314
Size: 330016 Color: 5627
Size: 259171 Color: 1232

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 407869 Color: 8250
Size: 325205 Color: 5416
Size: 266927 Color: 1907

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 406514 Color: 8226
Size: 337229 Color: 5954
Size: 256258 Color: 894

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 406459 Color: 8224
Size: 320100 Color: 5187
Size: 273442 Color: 2442

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 400278 Color: 8068
Size: 333776 Color: 5792
Size: 265947 Color: 1822

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 408949 Color: 8270
Size: 332870 Color: 5753
Size: 258182 Color: 1132

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 380111 Color: 7464
Size: 361540 Color: 6892
Size: 258350 Color: 1146

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 410698 Color: 8310
Size: 335307 Color: 5872
Size: 253996 Color: 619

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 407779 Color: 8248
Size: 334916 Color: 5848
Size: 257306 Color: 1031

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 396208 Color: 7960
Size: 334377 Color: 5819
Size: 269416 Color: 2110

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 405030 Color: 8179
Size: 344072 Color: 6234
Size: 250899 Color: 167

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 407636 Color: 8245
Size: 302046 Color: 4244
Size: 290319 Color: 3576

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 409293 Color: 8278
Size: 338035 Color: 5993
Size: 252673 Color: 446

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 403658 Color: 8145
Size: 324681 Color: 5386
Size: 271662 Color: 2307

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 407090 Color: 8235
Size: 341216 Color: 6120
Size: 251695 Color: 300

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 367936 Color: 7099
Size: 356039 Color: 6704
Size: 276026 Color: 2643

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 394773 Color: 7906
Size: 354250 Color: 6631
Size: 250978 Color: 190

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 406182 Color: 8218
Size: 337586 Color: 5967
Size: 256233 Color: 892

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 407238 Color: 8239
Size: 337891 Color: 5987
Size: 254872 Color: 725

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 406134 Color: 8214
Size: 337560 Color: 5966
Size: 256307 Color: 910

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 397649 Color: 7994
Size: 325285 Color: 5418
Size: 277067 Color: 2723

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 409539 Color: 8283
Size: 329439 Color: 5601
Size: 261023 Color: 1427

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 404018 Color: 8154
Size: 325280 Color: 5417
Size: 270703 Color: 2223

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 410597 Color: 8304
Size: 335212 Color: 5867
Size: 254192 Color: 642

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 405356 Color: 8187
Size: 324775 Color: 5391
Size: 269870 Color: 2156

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 405358 Color: 8188
Size: 337078 Color: 5947
Size: 257565 Color: 1058

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 405163 Color: 8183
Size: 343367 Color: 6205
Size: 251471 Color: 260

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 409248 Color: 8277
Size: 330693 Color: 5653
Size: 260060 Color: 1319

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 400093 Color: 8063
Size: 335980 Color: 5899
Size: 263928 Color: 1661

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 343006 Color: 6188
Size: 333581 Color: 5780
Size: 323414 Color: 5334

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 406134 Color: 8215
Size: 337603 Color: 5969
Size: 256264 Color: 897

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 404619 Color: 8168
Size: 334636 Color: 5834
Size: 260746 Color: 1393

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 390495 Color: 7773
Size: 355522 Color: 6676
Size: 253984 Color: 615

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 411233 Color: 8327
Size: 336926 Color: 5943
Size: 251842 Color: 329

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 399930 Color: 8056
Size: 332697 Color: 5743
Size: 267374 Color: 1940

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 407100 Color: 8236
Size: 334872 Color: 5844
Size: 258029 Color: 1114

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 398117 Color: 8008
Size: 345118 Color: 6278
Size: 256766 Color: 965

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 405338 Color: 8186
Size: 326381 Color: 5469
Size: 268282 Color: 2025

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 397575 Color: 7992
Size: 333803 Color: 5793
Size: 268623 Color: 2045

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 405377 Color: 8190
Size: 336042 Color: 5903
Size: 258582 Color: 1167

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 405385 Color: 8192
Size: 336999 Color: 5945
Size: 257617 Color: 1063

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 405487 Color: 8194
Size: 333391 Color: 5775
Size: 261123 Color: 1432

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 404755 Color: 8172
Size: 334898 Color: 5846
Size: 260348 Color: 1358

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 405550 Color: 8196
Size: 343926 Color: 6225
Size: 250525 Color: 95

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 405248 Color: 8185
Size: 335097 Color: 5857
Size: 259656 Color: 1288

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 405447 Color: 8193
Size: 340067 Color: 6071
Size: 254487 Color: 683

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 407984 Color: 8253
Size: 335354 Color: 5874
Size: 256663 Color: 951

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 405139 Color: 8181
Size: 337762 Color: 5979
Size: 257100 Color: 1008

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 400783 Color: 8074
Size: 342849 Color: 6181
Size: 256369 Color: 922

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 406035 Color: 8210
Size: 337840 Color: 5983
Size: 256126 Color: 877

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 408894 Color: 8269
Size: 338693 Color: 6017
Size: 252414 Color: 406

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 366642 Color: 7050
Size: 317517 Color: 5072
Size: 315842 Color: 4985

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 405369 Color: 8189
Size: 343372 Color: 6206
Size: 251260 Color: 230

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 407471 Color: 8242
Size: 336203 Color: 5910
Size: 256327 Color: 916

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 407666 Color: 8246
Size: 330119 Color: 5634
Size: 262216 Color: 1526

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 402043 Color: 8093
Size: 324845 Color: 5397
Size: 273113 Color: 2415

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 405971 Color: 8209
Size: 335357 Color: 5875
Size: 258673 Color: 1176

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 405593 Color: 8199
Size: 327493 Color: 5521
Size: 266915 Color: 1906

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 404639 Color: 8170
Size: 320230 Color: 5191
Size: 275132 Color: 2583

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 405591 Color: 8198
Size: 323086 Color: 5312
Size: 271324 Color: 2275

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 407327 Color: 8241
Size: 335040 Color: 5854
Size: 257634 Color: 1066

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 408161 Color: 8257
Size: 338077 Color: 5994
Size: 253763 Color: 584

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 405849 Color: 8204
Size: 335474 Color: 5882
Size: 258678 Color: 1177

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 404781 Color: 8173
Size: 344526 Color: 6255
Size: 250694 Color: 127

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 389887 Color: 7754
Size: 347161 Color: 6359
Size: 262953 Color: 1586

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 405801 Color: 8202
Size: 342617 Color: 6170
Size: 251583 Color: 281

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 407268 Color: 8240
Size: 333350 Color: 5773
Size: 259383 Color: 1255

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 374118 Color: 7285
Size: 366328 Color: 7039
Size: 259555 Color: 1271

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 387126 Color: 7672
Size: 356505 Color: 6720
Size: 256370 Color: 923

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 409629 Color: 8284
Size: 332980 Color: 5758
Size: 257392 Color: 1040

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 403142 Color: 8132
Size: 340536 Color: 6093
Size: 256323 Color: 911

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 406213 Color: 8219
Size: 342749 Color: 6177
Size: 251039 Color: 197

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 395072 Color: 7914
Size: 341979 Color: 6150
Size: 262950 Color: 1585

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 384134 Color: 7582
Size: 313720 Color: 4887
Size: 302147 Color: 4251

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 404272 Color: 8161
Size: 336065 Color: 5904
Size: 259664 Color: 1289

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 399142 Color: 8035
Size: 335142 Color: 5862
Size: 265717 Color: 1802

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 382528 Color: 7538
Size: 348373 Color: 6397
Size: 269100 Color: 2085

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 405877 Color: 8206
Size: 332971 Color: 5756
Size: 261153 Color: 1435

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 406697 Color: 8232
Size: 334668 Color: 5835
Size: 258636 Color: 1170

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 406581 Color: 8228
Size: 335672 Color: 5891
Size: 257748 Color: 1083

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 406405 Color: 8223
Size: 334549 Color: 5827
Size: 259047 Color: 1217

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 404081 Color: 8156
Size: 342581 Color: 6168
Size: 253339 Color: 532

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 403858 Color: 8147
Size: 331766 Color: 5699
Size: 264377 Color: 1697

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 399268 Color: 8039
Size: 334955 Color: 5851
Size: 265778 Color: 1807

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 403954 Color: 8151
Size: 317675 Color: 5082
Size: 278372 Color: 2815

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 398333 Color: 8017
Size: 345312 Color: 6285
Size: 256356 Color: 921

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 406165 Color: 8216
Size: 342329 Color: 6162
Size: 251507 Color: 267

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 403953 Color: 8150
Size: 345080 Color: 6277
Size: 250968 Color: 187

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 396750 Color: 7969
Size: 337384 Color: 5961
Size: 265867 Color: 1812

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 397187 Color: 7979
Size: 338721 Color: 6018
Size: 264093 Color: 1673

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 384533 Color: 7591
Size: 343325 Color: 6203
Size: 272143 Color: 2346

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 396133 Color: 7958
Size: 331801 Color: 5700
Size: 272067 Color: 2340

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 406106 Color: 8212
Size: 340616 Color: 6097
Size: 253279 Color: 526

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 384687 Color: 7598
Size: 341724 Color: 6139
Size: 273590 Color: 2449

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 380602 Color: 7479
Size: 330937 Color: 5661
Size: 288462 Color: 3468

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 386114 Color: 7641
Size: 319350 Color: 5152
Size: 294537 Color: 3842

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 408163 Color: 8258
Size: 334210 Color: 5812
Size: 257628 Color: 1065

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 396831 Color: 7973
Size: 334356 Color: 5818
Size: 268814 Color: 2067

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 403573 Color: 8140
Size: 336363 Color: 5921
Size: 260065 Color: 1321

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 410401 Color: 8300
Size: 309778 Color: 4661
Size: 279822 Color: 2905

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 396305 Color: 7962
Size: 349636 Color: 6449
Size: 254060 Color: 627

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 403298 Color: 8136
Size: 343137 Color: 6193
Size: 253566 Color: 561

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 403239 Color: 8135
Size: 336854 Color: 5935
Size: 259908 Color: 1303

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 405927 Color: 8208
Size: 337916 Color: 5989
Size: 256158 Color: 883

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 403160 Color: 8133
Size: 336871 Color: 5937
Size: 259970 Color: 1310

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 360694 Color: 6855
Size: 360131 Color: 6847
Size: 279176 Color: 2874

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 409353 Color: 8279
Size: 340431 Color: 6088
Size: 250217 Color: 43

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 407540 Color: 8244
Size: 333590 Color: 5782
Size: 258871 Color: 1203

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 402967 Color: 8129
Size: 336855 Color: 5936
Size: 260179 Color: 1336

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 402934 Color: 8128
Size: 343721 Color: 6216
Size: 253346 Color: 533

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 402933 Color: 8127
Size: 334016 Color: 5800
Size: 263052 Color: 1598

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 395761 Color: 7943
Size: 350368 Color: 6473
Size: 253872 Color: 594

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 395840 Color: 7947
Size: 345229 Color: 6282
Size: 258932 Color: 1207

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 379841 Color: 7456
Size: 364150 Color: 6980
Size: 256010 Color: 864

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 410660 Color: 8306
Size: 313335 Color: 4865
Size: 276006 Color: 2640

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 359409 Color: 6820
Size: 346972 Color: 6352
Size: 293620 Color: 3784

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 402648 Color: 8119
Size: 306667 Color: 4491
Size: 290686 Color: 3597

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 408562 Color: 8264
Size: 336284 Color: 5915
Size: 255155 Color: 754

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 402482 Color: 8112
Size: 334115 Color: 5805
Size: 263404 Color: 1623

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 402281 Color: 8104
Size: 336258 Color: 5913
Size: 261462 Color: 1462

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 373634 Color: 7266
Size: 331987 Color: 5712
Size: 294380 Color: 3827

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 402076 Color: 8097
Size: 343982 Color: 6229
Size: 253943 Color: 610

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 402603 Color: 8117
Size: 334377 Color: 5820
Size: 263021 Color: 1595

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 402661 Color: 8120
Size: 335455 Color: 5880
Size: 261885 Color: 1496

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 403921 Color: 8149
Size: 333634 Color: 5785
Size: 262446 Color: 1546

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 395136 Color: 7918
Size: 345670 Color: 6296
Size: 259195 Color: 1237

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 402511 Color: 8113
Size: 333619 Color: 5783
Size: 263871 Color: 1659

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 402879 Color: 8125
Size: 336812 Color: 5932
Size: 260310 Color: 1356

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 395380 Color: 7925
Size: 316701 Color: 5028
Size: 287920 Color: 3435

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 401997 Color: 8092
Size: 333022 Color: 5762
Size: 264982 Color: 1736

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 402217 Color: 8103
Size: 336410 Color: 5923
Size: 261374 Color: 1454

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 390204 Color: 7761
Size: 329179 Color: 5588
Size: 280618 Color: 2964

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 381916 Color: 7525
Size: 344281 Color: 6243
Size: 273804 Color: 2464

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 356247 Color: 6711
Size: 354849 Color: 6653
Size: 288905 Color: 3490

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 400042 Color: 8061
Size: 336124 Color: 5905
Size: 263835 Color: 1653

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 402067 Color: 8096
Size: 334041 Color: 5801
Size: 263893 Color: 1660

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 388359 Color: 7707
Size: 355451 Color: 6675
Size: 256191 Color: 887

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 409080 Color: 8271
Size: 328778 Color: 5572
Size: 262143 Color: 1520

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 402136 Color: 8100
Size: 346987 Color: 6353
Size: 250878 Color: 162

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 402181 Color: 8101
Size: 333261 Color: 5770
Size: 264559 Color: 1706

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 402760 Color: 8123
Size: 340420 Color: 6087
Size: 256821 Color: 974

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 405199 Color: 8184
Size: 335944 Color: 5896
Size: 258858 Color: 1200

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 380326 Color: 7470
Size: 356907 Color: 6728
Size: 262768 Color: 1571

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 401149 Color: 8078
Size: 346145 Color: 6319
Size: 252707 Color: 448

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 364138 Color: 6978
Size: 350683 Color: 6489
Size: 285180 Color: 3246

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 397909 Color: 8002
Size: 340103 Color: 6075
Size: 261989 Color: 1507

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 401587 Color: 8085
Size: 343227 Color: 6197
Size: 255187 Color: 760

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 401581 Color: 8084
Size: 339500 Color: 6041
Size: 258920 Color: 1205

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 395443 Color: 7928
Size: 342288 Color: 6158
Size: 262270 Color: 1532

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 401352 Color: 8081
Size: 336834 Color: 5933
Size: 261815 Color: 1491

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 401388 Color: 8082
Size: 345997 Color: 6309
Size: 252616 Color: 439

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 401699 Color: 8088
Size: 333131 Color: 5767
Size: 265171 Color: 1758

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 401220 Color: 8079
Size: 332756 Color: 5748
Size: 266025 Color: 1832

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 407871 Color: 8251
Size: 321030 Color: 5227
Size: 271100 Color: 2256

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 400706 Color: 8073
Size: 335128 Color: 5861
Size: 264167 Color: 1681

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 400661 Color: 8072
Size: 332591 Color: 5738
Size: 266749 Color: 1884

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 400897 Color: 8077
Size: 342624 Color: 6171
Size: 256480 Color: 935

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 400801 Color: 8075
Size: 339840 Color: 6055
Size: 259360 Color: 1251

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 400594 Color: 8071
Size: 332614 Color: 5739
Size: 266793 Color: 1891

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 407763 Color: 8247
Size: 313544 Color: 4875
Size: 278694 Color: 2841

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 411364 Color: 8329
Size: 334150 Color: 5808
Size: 254487 Color: 684

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 400312 Color: 8070
Size: 349492 Color: 6443
Size: 250197 Color: 40

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 400297 Color: 8069
Size: 346115 Color: 6316
Size: 253589 Color: 564

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 365406 Color: 7019
Size: 351904 Color: 6543
Size: 282691 Color: 3097

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 397518 Color: 7991
Size: 341557 Color: 6136
Size: 260926 Color: 1414

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 407849 Color: 8249
Size: 332501 Color: 5732
Size: 259651 Color: 1287

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 382293 Color: 7533
Size: 349995 Color: 6456
Size: 267713 Color: 1968

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 400054 Color: 8062
Size: 334546 Color: 5826
Size: 265401 Color: 1777

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 400114 Color: 8064
Size: 332373 Color: 5728
Size: 267514 Color: 1951

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 403559 Color: 8139
Size: 334239 Color: 5813
Size: 262203 Color: 1523

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 399947 Color: 8057
Size: 308610 Color: 4593
Size: 291444 Color: 3651

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 399899 Color: 8055
Size: 338500 Color: 6009
Size: 261602 Color: 1477

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 401930 Color: 8091
Size: 333065 Color: 5765
Size: 265006 Color: 1740

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 399881 Color: 8054
Size: 332033 Color: 5716
Size: 268087 Color: 2000

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 368814 Color: 7130
Size: 353960 Color: 6620
Size: 277227 Color: 2734

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 399385 Color: 8042
Size: 331848 Color: 5704
Size: 268768 Color: 2058

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 399961 Color: 8059
Size: 336759 Color: 5931
Size: 263281 Color: 1613

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 399578 Color: 8048
Size: 331635 Color: 5695
Size: 268788 Color: 2060

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 402131 Color: 8098
Size: 337222 Color: 5953
Size: 260648 Color: 1384

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 409883 Color: 8290
Size: 321701 Color: 5258
Size: 268417 Color: 2031

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 395995 Color: 7954
Size: 346646 Color: 6339
Size: 257360 Color: 1037

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 404064 Color: 8155
Size: 335041 Color: 5855
Size: 260896 Color: 1413

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 361581 Color: 6894
Size: 327554 Color: 5524
Size: 310866 Color: 4729

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 404319 Color: 8162
Size: 333774 Color: 5791
Size: 261908 Color: 1498

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 398329 Color: 8016
Size: 332976 Color: 5757
Size: 268696 Color: 2051

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 410317 Color: 8298
Size: 335580 Color: 5886
Size: 254104 Color: 632

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 347984 Color: 6388
Size: 337767 Color: 5980
Size: 314250 Color: 4918

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 400249 Color: 8066
Size: 339532 Color: 6042
Size: 260220 Color: 1340

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 388444 Color: 7710
Size: 335196 Color: 5864
Size: 276361 Color: 2673

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 396780 Color: 7972
Size: 337602 Color: 5968
Size: 265619 Color: 1795

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 408770 Color: 8267
Size: 329628 Color: 5613
Size: 261603 Color: 1478

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 404695 Color: 8171
Size: 327153 Color: 5509
Size: 268153 Color: 2012

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 386031 Color: 7639
Size: 341421 Color: 6128
Size: 272549 Color: 2376

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 399004 Color: 8031
Size: 345896 Color: 6304
Size: 255101 Color: 747

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 402545 Color: 8114
Size: 327832 Color: 5536
Size: 269624 Color: 2130

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 398805 Color: 8027
Size: 342897 Color: 6183
Size: 258299 Color: 1142

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 398945 Color: 8030
Size: 331298 Color: 5678
Size: 269758 Color: 2143

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 398774 Color: 8025
Size: 331388 Color: 5683
Size: 269839 Color: 2150

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 373369 Color: 7254
Size: 356545 Color: 6721
Size: 270087 Color: 2172

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 398512 Color: 8021
Size: 331250 Color: 5677
Size: 270239 Color: 2183

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 398296 Color: 8015
Size: 332701 Color: 5744
Size: 269004 Color: 2078

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 398422 Color: 8018
Size: 344275 Color: 6242
Size: 257304 Color: 1030

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 398437 Color: 8019
Size: 331191 Color: 5676
Size: 270373 Color: 2193

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 405377 Color: 8191
Size: 325768 Color: 5450
Size: 268856 Color: 2070

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 398835 Color: 8028
Size: 331097 Color: 5673
Size: 270069 Color: 2171

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 396368 Color: 7963
Size: 330361 Color: 5642
Size: 273272 Color: 2430

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 395081 Color: 7915
Size: 344062 Color: 6233
Size: 260858 Color: 1408

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 398744 Color: 8024
Size: 334043 Color: 5802
Size: 267214 Color: 1929

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 398589 Color: 8023
Size: 341788 Color: 6143
Size: 259624 Color: 1281

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 398509 Color: 8020
Size: 330741 Color: 5654
Size: 270751 Color: 2227

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 398209 Color: 8011
Size: 333739 Color: 5788
Size: 268053 Color: 1995

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 402547 Color: 8115
Size: 329252 Color: 5592
Size: 268202 Color: 2017

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 406690 Color: 8231
Size: 316867 Color: 5035
Size: 276444 Color: 2677

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 398246 Color: 8012
Size: 331045 Color: 5668
Size: 270710 Color: 2224

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 397971 Color: 8005
Size: 351052 Color: 6507
Size: 250978 Color: 189

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 373457 Color: 7257
Size: 361008 Color: 6868
Size: 265536 Color: 1786

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 402049 Color: 8095
Size: 331995 Color: 5713
Size: 265957 Color: 1825

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 403574 Color: 8141
Size: 330683 Color: 5652
Size: 265744 Color: 1805

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 397931 Color: 8004
Size: 336306 Color: 5918
Size: 265764 Color: 1806

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 397766 Color: 7997
Size: 330871 Color: 5660
Size: 271364 Color: 2279

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 343951 Color: 6228
Size: 331817 Color: 5702
Size: 324233 Color: 5368

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 397976 Color: 8006
Size: 343054 Color: 6191
Size: 258971 Color: 1208

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 402135 Color: 8099
Size: 342300 Color: 6159
Size: 255566 Color: 803

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 397862 Color: 8000
Size: 330583 Color: 5650
Size: 271556 Color: 2298

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 397378 Color: 7985
Size: 332165 Color: 5721
Size: 270458 Color: 2202

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 397707 Color: 7995
Size: 336357 Color: 5920
Size: 265937 Color: 1820

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 405909 Color: 8207
Size: 327799 Color: 5533
Size: 266293 Color: 1848

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 366690 Color: 7052
Size: 357629 Color: 6757
Size: 275682 Color: 2613

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 397472 Color: 7989
Size: 335759 Color: 5893
Size: 266770 Color: 1886

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 383625 Color: 7572
Size: 312429 Color: 4815
Size: 303947 Color: 4344

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 397491 Color: 7990
Size: 336178 Color: 5908
Size: 266332 Color: 1855

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 371781 Color: 7209
Size: 366121 Color: 7035
Size: 262099 Color: 1517

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 397445 Color: 7988
Size: 350197 Color: 6469
Size: 252359 Color: 398

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 386521 Color: 7652
Size: 350943 Color: 6500
Size: 262537 Color: 1555

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 406537 Color: 8227
Size: 339909 Color: 6061
Size: 253555 Color: 559

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 398262 Color: 8013
Size: 320246 Color: 5192
Size: 281493 Color: 3015

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 399059 Color: 8033
Size: 333037 Color: 5764
Size: 267905 Color: 1981

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 403622 Color: 8143
Size: 330059 Color: 5631
Size: 266320 Color: 1851

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 403640 Color: 8144
Size: 333521 Color: 5777
Size: 262840 Color: 1577

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 375535 Color: 7324
Size: 364596 Color: 6997
Size: 259870 Color: 1301

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 405541 Color: 8195
Size: 328269 Color: 5549
Size: 266191 Color: 1842

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 363371 Color: 6956
Size: 319592 Color: 5162
Size: 317038 Color: 5048

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 397138 Color: 7978
Size: 340389 Color: 6086
Size: 262474 Color: 1550

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 404248 Color: 8160
Size: 329891 Color: 5620
Size: 265862 Color: 1811

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 402340 Color: 8107
Size: 327473 Color: 5519
Size: 270188 Color: 2177

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 396995 Color: 7977
Size: 328306 Color: 5553
Size: 274700 Color: 2540

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 396727 Color: 7968
Size: 335800 Color: 5894
Size: 267474 Color: 1948

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 396594 Color: 7967
Size: 331912 Color: 5708
Size: 271495 Color: 2289

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 411080 Color: 8322
Size: 331029 Color: 5666
Size: 257892 Color: 1100

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 386036 Color: 7640
Size: 347134 Color: 6358
Size: 266831 Color: 1896

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 396183 Color: 7959
Size: 350403 Color: 6478
Size: 253415 Color: 543

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 404787 Color: 8174
Size: 327985 Color: 5538
Size: 267229 Color: 1930

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 395532 Color: 7932
Size: 346344 Color: 6327
Size: 258125 Color: 1125

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 398067 Color: 8007
Size: 336701 Color: 5930
Size: 265233 Color: 1763

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 396114 Color: 7957
Size: 347644 Color: 6376
Size: 256243 Color: 893

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 390587 Color: 7775
Size: 332850 Color: 5752
Size: 276564 Color: 2689

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 404343 Color: 8163
Size: 323538 Color: 5341
Size: 272120 Color: 2345

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 396380 Color: 7964
Size: 329631 Color: 5614
Size: 273990 Color: 2487

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 367183 Color: 7063
Size: 345565 Color: 6294
Size: 287253 Color: 3387

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 395839 Color: 7946
Size: 333658 Color: 5787
Size: 270504 Color: 2207

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 410291 Color: 8297
Size: 316096 Color: 5000
Size: 273614 Color: 2450

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 403033 Color: 8130
Size: 329287 Color: 5595
Size: 267681 Color: 1962

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 395907 Color: 7951
Size: 336637 Color: 5929
Size: 267457 Color: 1947

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 406293 Color: 8221
Size: 334963 Color: 5852
Size: 258745 Color: 1187

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 395868 Color: 7949
Size: 329921 Color: 5621
Size: 274212 Color: 2493

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 395363 Color: 7924
Size: 329475 Color: 5603
Size: 275163 Color: 2585

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 403184 Color: 8134
Size: 330476 Color: 5648
Size: 266341 Color: 1856

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 402396 Color: 8109
Size: 315722 Color: 4972
Size: 281883 Color: 3047

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 398794 Color: 8026
Size: 336157 Color: 5906
Size: 265050 Color: 1745

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 395664 Color: 7939
Size: 336450 Color: 5925
Size: 267887 Color: 1980

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 403043 Color: 8131
Size: 329272 Color: 5593
Size: 267686 Color: 1965

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 395717 Color: 7940
Size: 316949 Color: 5038
Size: 287335 Color: 3396

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 395562 Color: 7934
Size: 329496 Color: 5604
Size: 274943 Color: 2559

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 376487 Color: 7356
Size: 355386 Color: 6673
Size: 268128 Color: 2007

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 395577 Color: 7935
Size: 344903 Color: 6267
Size: 259521 Color: 1269

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 406133 Color: 8213
Size: 329224 Color: 5591
Size: 264644 Color: 1710

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 395791 Color: 7945
Size: 329182 Color: 5589
Size: 275028 Color: 2569

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 365883 Color: 7028
Size: 329286 Color: 5594
Size: 304832 Color: 4403

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 363999 Color: 6973
Size: 329153 Color: 5587
Size: 306849 Color: 4501

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 395546 Color: 7933
Size: 328591 Color: 5566
Size: 275864 Color: 2627

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 395461 Color: 7929
Size: 335952 Color: 5897
Size: 268588 Color: 2043

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 395495 Color: 7931
Size: 336379 Color: 5922
Size: 268127 Color: 2006

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 395158 Color: 7920
Size: 339586 Color: 6045
Size: 265257 Color: 1765

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 373913 Color: 7277
Size: 371760 Color: 7208
Size: 254328 Color: 658

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 395933 Color: 7952
Size: 339934 Color: 6062
Size: 264134 Color: 1675

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 402376 Color: 8108
Size: 328929 Color: 5576
Size: 268696 Color: 2052

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 373843 Color: 7274
Size: 362343 Color: 6924
Size: 263815 Color: 1650

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 395155 Color: 7919
Size: 329415 Color: 5600
Size: 275431 Color: 2599

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 363847 Color: 6970
Size: 342381 Color: 6163
Size: 293773 Color: 3791

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 343275 Color: 6201
Size: 338155 Color: 5999
Size: 318571 Color: 5120

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 366531 Color: 7047
Size: 318540 Color: 5118
Size: 314930 Color: 4944

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 394005 Color: 7889
Size: 325370 Color: 5423
Size: 280626 Color: 2965

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 394889 Color: 7910
Size: 345905 Color: 6305
Size: 259207 Color: 1239

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 376491 Color: 7358
Size: 345517 Color: 6290
Size: 277993 Color: 2786

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 408363 Color: 8262
Size: 328458 Color: 5561
Size: 263180 Color: 1606

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 361390 Color: 6884
Size: 343541 Color: 6211
Size: 295070 Color: 3876

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 394859 Color: 7908
Size: 328364 Color: 5556
Size: 276778 Color: 2706

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 394878 Color: 7909
Size: 345440 Color: 6287
Size: 259683 Color: 1291

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 363317 Color: 6952
Size: 349998 Color: 6457
Size: 286686 Color: 3358

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 373508 Color: 7262
Size: 363036 Color: 6942
Size: 263457 Color: 1629

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 396492 Color: 7965
Size: 334629 Color: 5831
Size: 268880 Color: 2072

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 396066 Color: 7956
Size: 350050 Color: 6460
Size: 253885 Color: 596

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 407537 Color: 8243
Size: 342024 Color: 6151
Size: 250440 Color: 75

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 404499 Color: 8166
Size: 325524 Color: 5436
Size: 269978 Color: 2164

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 348263 Color: 6392
Size: 338912 Color: 6020
Size: 312826 Color: 4840

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 394315 Color: 7900
Size: 328160 Color: 5546
Size: 277526 Color: 2754

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 394348 Color: 7901
Size: 335984 Color: 5900
Size: 269669 Color: 2136

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 388609 Color: 7714
Size: 327558 Color: 5525
Size: 283834 Color: 3155

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 366404 Color: 7041
Size: 320842 Color: 5218
Size: 312755 Color: 4835

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 390945 Color: 7781
Size: 343778 Color: 6219
Size: 265278 Color: 1768

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 393975 Color: 7888
Size: 356023 Color: 6700
Size: 250003 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 394132 Color: 7895
Size: 328100 Color: 5542
Size: 277769 Color: 2768

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 403486 Color: 8138
Size: 332056 Color: 5717
Size: 264459 Color: 1701

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 394115 Color: 7893
Size: 336016 Color: 5902
Size: 269870 Color: 2155

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 394133 Color: 7896
Size: 334765 Color: 5838
Size: 271103 Color: 2258

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 396251 Color: 7961
Size: 330184 Color: 5635
Size: 273566 Color: 2448

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 393915 Color: 7881
Size: 331814 Color: 5701
Size: 274272 Color: 2500

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 393888 Color: 7880
Size: 327496 Color: 5522
Size: 278617 Color: 2835

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 397421 Color: 7987
Size: 327622 Color: 5527
Size: 274958 Color: 2560

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 346723 Color: 6341
Size: 327242 Color: 5513
Size: 326036 Color: 5459

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 409944 Color: 8292
Size: 323727 Color: 5348
Size: 266330 Color: 1854

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 352669 Color: 6576
Size: 323811 Color: 5353
Size: 323521 Color: 5340

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 360588 Color: 6854
Size: 337614 Color: 5970
Size: 301799 Color: 4230

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 372353 Color: 7224
Size: 324103 Color: 5362
Size: 303545 Color: 4324

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 349407 Color: 6440
Size: 329552 Color: 5607
Size: 321042 Color: 5228

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 373668 Color: 7267
Size: 313966 Color: 4902
Size: 312367 Color: 4811

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 372706 Color: 7236
Size: 362036 Color: 6910
Size: 265259 Color: 1766

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 336318 Color: 5919
Size: 332628 Color: 5741
Size: 331055 Color: 5669

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 365591 Color: 7024
Size: 323462 Color: 5337
Size: 310948 Color: 4732

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 344095 Color: 6235
Size: 334161 Color: 5809
Size: 321745 Color: 5260

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 364661 Color: 7001
Size: 333113 Color: 5766
Size: 302227 Color: 4257

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 401252 Color: 8080
Size: 331424 Color: 5686
Size: 267325 Color: 1935

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 365238 Color: 7014
Size: 324222 Color: 5365
Size: 310541 Color: 4708

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 405006 Color: 8177
Size: 327647 Color: 5528
Size: 267348 Color: 1936

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 365196 Color: 7012
Size: 325120 Color: 5408
Size: 309685 Color: 4652

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 374129 Color: 7286
Size: 316039 Color: 4997
Size: 309833 Color: 4665

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 397800 Color: 7999
Size: 309798 Color: 4664
Size: 292403 Color: 3718

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 365089 Color: 7009
Size: 325194 Color: 5413
Size: 309718 Color: 4655

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 372687 Color: 7235
Size: 318481 Color: 5115
Size: 308833 Color: 4605

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 376497 Color: 7359
Size: 342487 Color: 6165
Size: 281017 Color: 2987

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 371693 Color: 7204
Size: 331628 Color: 5694
Size: 296680 Color: 3966

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 376482 Color: 7355
Size: 349379 Color: 6438
Size: 274140 Color: 2491

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 376405 Color: 7352
Size: 343596 Color: 6213
Size: 280000 Color: 2918

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 376398 Color: 7351
Size: 341804 Color: 6145
Size: 281799 Color: 3043

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 382734 Color: 7548
Size: 331682 Color: 5696
Size: 285585 Color: 3279

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 376275 Color: 7349
Size: 343663 Color: 6215
Size: 280063 Color: 2924

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 365725 Color: 7026
Size: 325550 Color: 5438
Size: 308726 Color: 4597

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 375192 Color: 7310
Size: 327808 Color: 5534
Size: 297001 Color: 3981

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 385905 Color: 7633
Size: 337732 Color: 5977
Size: 276364 Color: 2674

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 374859 Color: 7299
Size: 326385 Color: 5470
Size: 298757 Color: 4081

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 364608 Color: 6998
Size: 354726 Color: 6649
Size: 280667 Color: 2967

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 348855 Color: 6421
Size: 337175 Color: 5951
Size: 313971 Color: 4904

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 407224 Color: 8238
Size: 331402 Color: 5684
Size: 261375 Color: 1455

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 354391 Color: 6635
Size: 346435 Color: 6333
Size: 299175 Color: 4091

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 395102 Color: 7916
Size: 342634 Color: 6172
Size: 262265 Color: 1529

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 375245 Color: 7314
Size: 343141 Color: 6194
Size: 281615 Color: 3025

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 383308 Color: 7562
Size: 358906 Color: 6806
Size: 257787 Color: 1090

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 365250 Color: 7016
Size: 317443 Color: 5068
Size: 317308 Color: 5062

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 373468 Color: 7259
Size: 344431 Color: 6250
Size: 282102 Color: 3062

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 379891 Color: 7459
Size: 331571 Color: 5692
Size: 288539 Color: 3473

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 373192 Color: 7248
Size: 344198 Color: 6239
Size: 282611 Color: 3092

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 377931 Color: 7400
Size: 344615 Color: 6257
Size: 277455 Color: 2749

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 378952 Color: 7434
Size: 324176 Color: 5363
Size: 296873 Color: 3976

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 342146 Color: 6156
Size: 339696 Color: 6050
Size: 318159 Color: 5101

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 373144 Color: 7247
Size: 322794 Color: 5309
Size: 304063 Color: 4355

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 395771 Color: 7944
Size: 325576 Color: 5439
Size: 278654 Color: 2838

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 388354 Color: 7706
Size: 347225 Color: 6362
Size: 264422 Color: 1698

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 342977 Color: 6185
Size: 333329 Color: 5772
Size: 323695 Color: 5347

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 366854 Color: 7060
Size: 345821 Color: 6301
Size: 287326 Color: 3394

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 402291 Color: 8105
Size: 329562 Color: 5608
Size: 268148 Color: 2011

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 373293 Color: 7249
Size: 371944 Color: 7214
Size: 254764 Color: 710

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 373464 Color: 7258
Size: 325887 Color: 5456
Size: 300650 Color: 4163

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 385196 Color: 7613
Size: 345774 Color: 6300
Size: 269031 Color: 2079

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 372867 Color: 7239
Size: 344096 Color: 6236
Size: 283038 Color: 3115

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 368364 Color: 7117
Size: 363566 Color: 6963
Size: 268071 Color: 1998

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 340565 Color: 6096
Size: 330357 Color: 5641
Size: 329079 Color: 5583

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 373294 Color: 7250
Size: 363195 Color: 6946
Size: 263512 Color: 1634

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 342649 Color: 6173
Size: 329932 Color: 5622
Size: 327420 Color: 5516

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 360059 Color: 6845
Size: 335651 Color: 5890
Size: 304291 Color: 4366

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 373820 Color: 7273
Size: 372596 Color: 7232
Size: 253585 Color: 562

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 372476 Color: 7229
Size: 372251 Color: 7222
Size: 255274 Color: 769

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 364216 Color: 6983
Size: 335123 Color: 5859
Size: 300662 Color: 4164

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 346021 Color: 6310
Size: 329596 Color: 5612
Size: 324384 Color: 5373

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 372280 Color: 7223
Size: 343874 Color: 6224
Size: 283847 Color: 3158

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 372200 Color: 7220
Size: 333358 Color: 5774
Size: 294443 Color: 3835

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 369142 Color: 7141
Size: 328072 Color: 5541
Size: 302787 Color: 4291

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 372452 Color: 7228
Size: 360857 Color: 6864
Size: 266692 Color: 1879

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 362395 Color: 6928
Size: 340853 Color: 6104
Size: 296753 Color: 3968

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 371721 Color: 7205
Size: 315211 Color: 4954
Size: 313069 Color: 4852

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 373115 Color: 7245
Size: 327706 Color: 5529
Size: 299180 Color: 4092

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 371545 Color: 7200
Size: 337970 Color: 5991
Size: 290486 Color: 3588

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 371479 Color: 7195
Size: 346783 Color: 6343
Size: 281739 Color: 3034

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 371825 Color: 7210
Size: 369054 Color: 7138
Size: 259122 Color: 1225

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 371551 Color: 7201
Size: 340958 Color: 6108
Size: 287492 Color: 3403

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 372101 Color: 7217
Size: 371486 Color: 7196
Size: 256414 Color: 927

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 383805 Color: 7576
Size: 356920 Color: 6730
Size: 259276 Color: 1247

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 384784 Color: 7600
Size: 335115 Color: 5858
Size: 280102 Color: 2927

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 375611 Color: 7325
Size: 362102 Color: 6913
Size: 262288 Color: 1534

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 397360 Color: 7984
Size: 350164 Color: 6467
Size: 252477 Color: 414

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 368849 Color: 7131
Size: 367875 Color: 7094
Size: 263277 Color: 1612

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 371488 Color: 7197
Size: 342881 Color: 6182
Size: 285632 Color: 3285

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 358903 Color: 6805
Size: 354804 Color: 6651
Size: 286294 Color: 3332

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 371839 Color: 7211
Size: 362131 Color: 6916
Size: 266031 Color: 1833

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 366428 Color: 7042
Size: 331040 Color: 5667
Size: 302533 Color: 4277

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 364195 Color: 6982
Size: 328650 Color: 5568
Size: 307156 Color: 4515

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 369941 Color: 7161
Size: 346075 Color: 6314
Size: 283985 Color: 3170

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 394511 Color: 7904
Size: 329398 Color: 5599
Size: 276092 Color: 2648

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 361033 Color: 6871
Size: 329468 Color: 5602
Size: 309500 Color: 4639

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 343110 Color: 6192
Size: 335032 Color: 5853
Size: 321859 Color: 5268

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 350131 Color: 6463
Size: 345821 Color: 6302
Size: 304049 Color: 4354

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 376709 Color: 7367
Size: 354081 Color: 6625
Size: 269211 Color: 2095

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 399628 Color: 8050
Size: 349184 Color: 6430
Size: 251189 Color: 216

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 395425 Color: 7927
Size: 324511 Color: 5379
Size: 280065 Color: 2925

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 383408 Color: 7565
Size: 334745 Color: 5837
Size: 281848 Color: 3045

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 368520 Color: 7122
Size: 356270 Color: 6714
Size: 275211 Color: 2590

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 349524 Color: 6446
Size: 344675 Color: 6260
Size: 305802 Color: 4448

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 371266 Color: 7190
Size: 368457 Color: 7120
Size: 260278 Color: 1348

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 381622 Color: 7514
Size: 344484 Color: 6252
Size: 273895 Color: 2480

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 357334 Color: 6742
Size: 325489 Color: 5431
Size: 317178 Color: 5055

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 368234 Color: 7112
Size: 321320 Color: 5241
Size: 310447 Color: 4703

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 369047 Color: 7137
Size: 321168 Color: 5233
Size: 309786 Color: 4663

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 350885 Color: 6497
Size: 340506 Color: 6091
Size: 308610 Color: 4594

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 380786 Color: 7485
Size: 367414 Color: 7077
Size: 251801 Color: 323

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 364032 Color: 6975
Size: 362058 Color: 6911
Size: 273911 Color: 2481

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 368044 Color: 7108
Size: 339136 Color: 6029
Size: 292821 Color: 3744

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 365409 Color: 7020
Size: 320872 Color: 5221
Size: 313720 Color: 4886

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 337798 Color: 5981
Size: 335991 Color: 5901
Size: 326212 Color: 5464

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 368273 Color: 7114
Size: 321888 Color: 5272
Size: 309840 Color: 4666

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 367959 Color: 7101
Size: 324702 Color: 5389
Size: 307340 Color: 4529

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 393727 Color: 7877
Size: 335209 Color: 5866
Size: 271065 Color: 2252

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 335188 Color: 5863
Size: 332783 Color: 5749
Size: 332030 Color: 5715

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 350876 Color: 6496
Size: 337466 Color: 5962
Size: 311659 Color: 4765

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 411341 Color: 8328
Size: 328236 Color: 5548
Size: 260424 Color: 1369

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 376719 Color: 7368
Size: 357247 Color: 6738
Size: 266035 Color: 1835

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 364007 Color: 6974
Size: 330012 Color: 5625
Size: 305982 Color: 4461

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 410179 Color: 8295
Size: 305839 Color: 4451
Size: 283983 Color: 3169

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 363590 Color: 6964
Size: 319408 Color: 5154
Size: 317003 Color: 5042

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 372002 Color: 7216
Size: 334913 Color: 5847
Size: 293086 Color: 3757

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 369183 Color: 7142
Size: 345131 Color: 6279
Size: 285687 Color: 3290

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 353437 Color: 6598
Size: 337494 Color: 5963
Size: 309070 Color: 4615

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 376801 Color: 7370
Size: 329970 Color: 5623
Size: 293230 Color: 3768

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 394162 Color: 7898
Size: 341578 Color: 6137
Size: 264261 Color: 1689

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 358920 Color: 6809
Size: 351260 Color: 6516
Size: 289821 Color: 3552

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 363379 Color: 6957
Size: 341752 Color: 6141
Size: 294870 Color: 3860

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 357945 Color: 6764
Size: 336226 Color: 5912
Size: 305830 Color: 4450

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 398209 Color: 8010
Size: 334950 Color: 5850
Size: 266842 Color: 1898

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 339428 Color: 6039
Size: 338319 Color: 6004
Size: 322254 Color: 5287

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 351924 Color: 6545
Size: 343245 Color: 6198
Size: 304832 Color: 4404

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 363771 Color: 6967
Size: 353470 Color: 6601
Size: 282760 Color: 3099

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 341531 Color: 6135
Size: 336987 Color: 5944
Size: 321483 Color: 5249

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 363295 Color: 6951
Size: 346349 Color: 6328
Size: 290357 Color: 3582

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 369364 Color: 7146
Size: 348365 Color: 6396
Size: 282272 Color: 3073

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 365829 Color: 7027
Size: 357464 Color: 6748
Size: 276708 Color: 2702

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 346234 Color: 6323
Size: 345712 Color: 6297
Size: 308055 Color: 4564

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 376555 Color: 7361
Size: 339610 Color: 6046
Size: 283836 Color: 3156

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 405004 Color: 8176
Size: 312334 Color: 4807
Size: 282663 Color: 3096

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 372381 Color: 7225
Size: 346041 Color: 6312
Size: 281579 Color: 3022

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 368236 Color: 7113
Size: 351041 Color: 6505
Size: 280724 Color: 2971

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 367990 Color: 7103
Size: 367240 Color: 7064
Size: 264771 Color: 1718

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 363593 Color: 6965
Size: 345029 Color: 6275
Size: 291379 Color: 3650

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 366317 Color: 7038
Size: 359824 Color: 6842
Size: 273860 Color: 2471

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 372893 Color: 7240
Size: 363865 Color: 6971
Size: 263243 Color: 1610

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 364071 Color: 6977
Size: 362312 Color: 6920
Size: 273618 Color: 2451

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 368688 Color: 7125
Size: 359555 Color: 6827
Size: 271758 Color: 2313

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 362398 Color: 6929
Size: 335238 Color: 5871
Size: 302365 Color: 4262

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 362329 Color: 6923
Size: 335231 Color: 5870
Size: 302441 Color: 4267

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 395465 Color: 7930
Size: 330199 Color: 5637
Size: 274337 Color: 2505

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 358203 Color: 6775
Size: 330640 Color: 5651
Size: 311158 Color: 4744

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 368364 Color: 7116
Size: 338389 Color: 6006
Size: 293248 Color: 3769

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 362805 Color: 6937
Size: 328460 Color: 5562
Size: 308736 Color: 4599

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 356028 Color: 6701
Size: 330334 Color: 5640
Size: 313639 Color: 4882

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 385916 Color: 7634
Size: 362109 Color: 6914
Size: 251976 Color: 344

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 393707 Color: 7874
Size: 322446 Color: 5293
Size: 283848 Color: 3159

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 382319 Color: 7534
Size: 364349 Color: 6989
Size: 253333 Color: 531

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 372203 Color: 7221
Size: 334001 Color: 5799
Size: 293797 Color: 3793

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 361937 Color: 6906
Size: 336915 Color: 5941
Size: 301149 Color: 4194

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 347370 Color: 6366
Size: 333769 Color: 5790
Size: 318862 Color: 5132

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 385144 Color: 7610
Size: 344924 Color: 6271
Size: 269933 Color: 2162

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 356038 Color: 6703
Size: 351288 Color: 6518
Size: 292675 Color: 3734

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 380407 Color: 7473
Size: 335636 Color: 5889
Size: 283958 Color: 3167

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 363045 Color: 6943
Size: 350149 Color: 6465
Size: 286807 Color: 3365

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 380234 Color: 7467
Size: 361688 Color: 6897
Size: 258079 Color: 1121

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 361581 Color: 6895
Size: 361524 Color: 6889
Size: 276896 Color: 2715

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 373431 Color: 7255
Size: 337260 Color: 5956
Size: 289310 Color: 3521

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 359687 Color: 6830
Size: 333017 Color: 5761
Size: 307297 Color: 4523

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 366377 Color: 7040
Size: 351388 Color: 6521
Size: 282236 Color: 3071

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 361961 Color: 6907
Size: 330384 Color: 5643
Size: 307656 Color: 4547

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 362355 Color: 6926
Size: 334859 Color: 5843
Size: 302787 Color: 4290

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 361179 Color: 6877
Size: 340841 Color: 6102
Size: 297981 Color: 4035

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 371502 Color: 7198
Size: 344897 Color: 6266
Size: 283602 Color: 3151

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 360793 Color: 6861
Size: 323901 Color: 5356
Size: 315307 Color: 4957

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 361214 Color: 6881
Size: 341136 Color: 6117
Size: 297651 Color: 4018

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 409105 Color: 8273
Size: 316238 Color: 5011
Size: 274658 Color: 2535

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 395974 Color: 7953
Size: 329217 Color: 5590
Size: 274810 Color: 2551

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 355992 Color: 6699
Size: 346350 Color: 6329
Size: 297659 Color: 4019

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 361015 Color: 6869
Size: 340323 Color: 6083
Size: 298663 Color: 4076

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 366802 Color: 7057
Size: 360477 Color: 6853
Size: 272722 Color: 2389

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 370081 Color: 7163
Size: 350736 Color: 6491
Size: 279184 Color: 2875

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 373495 Color: 7260
Size: 339580 Color: 6044
Size: 286926 Color: 3369

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 385412 Color: 7623
Size: 355880 Color: 6689
Size: 258709 Color: 1179

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 401774 Color: 8090
Size: 303807 Color: 4334
Size: 294420 Color: 3830

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 367844 Color: 7093
Size: 358268 Color: 6778
Size: 273889 Color: 2479

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 364297 Color: 6985
Size: 355254 Color: 6662
Size: 280450 Color: 2956

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 375524 Color: 7323
Size: 347097 Color: 6357
Size: 277380 Color: 2745

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 359787 Color: 6839
Size: 359714 Color: 6832
Size: 280500 Color: 2959

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 359799 Color: 6841
Size: 359784 Color: 6838
Size: 280418 Color: 2954

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 360900 Color: 6865
Size: 347371 Color: 6367
Size: 291730 Color: 3678

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 360702 Color: 6856
Size: 358907 Color: 6807
Size: 280392 Color: 2949

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 359793 Color: 6840
Size: 359730 Color: 6834
Size: 280478 Color: 2958

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 378844 Color: 7430
Size: 359695 Color: 6831
Size: 261462 Color: 1463

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 360327 Color: 6851
Size: 359764 Color: 6836
Size: 279910 Color: 2912

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 360109 Color: 6846
Size: 343939 Color: 6226
Size: 295953 Color: 3926

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 365035 Color: 7007
Size: 355118 Color: 6657
Size: 279848 Color: 2907

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 359451 Color: 6824
Size: 359415 Color: 6822
Size: 281135 Color: 2995

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 377828 Color: 7394
Size: 368513 Color: 7121
Size: 253660 Color: 573

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 364840 Color: 7003
Size: 359412 Color: 6821
Size: 275749 Color: 2619

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 365583 Color: 7023
Size: 345535 Color: 6293
Size: 288883 Color: 3489

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 361747 Color: 6899
Size: 355356 Color: 6672
Size: 282898 Color: 3108

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 367805 Color: 7089
Size: 350055 Color: 6461
Size: 282141 Color: 3066

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 369352 Color: 7145
Size: 358424 Color: 6782
Size: 272225 Color: 2352

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 363465 Color: 6961
Size: 353230 Color: 6588
Size: 283306 Color: 3133

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 366225 Color: 7037
Size: 363216 Color: 6947
Size: 270560 Color: 2213

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 372671 Color: 7234
Size: 358837 Color: 6802
Size: 268493 Color: 2035

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 358853 Color: 6803
Size: 358766 Color: 6798
Size: 282382 Color: 3078

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 358756 Color: 6797
Size: 358637 Color: 6789
Size: 282608 Color: 3091

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 383321 Color: 7563
Size: 364852 Color: 7004
Size: 251828 Color: 326

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 406173 Color: 8217
Size: 304659 Color: 4392
Size: 289169 Color: 3514

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 387930 Color: 7696
Size: 320800 Color: 5216
Size: 291271 Color: 3642

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 376408 Color: 7353
Size: 358655 Color: 6792
Size: 264938 Color: 1731

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 358568 Color: 6785
Size: 358567 Color: 6784
Size: 282866 Color: 3103

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 384784 Color: 7601
Size: 347078 Color: 6356
Size: 268139 Color: 2008

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 367359 Color: 7075
Size: 349255 Color: 6434
Size: 283387 Color: 3137

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 406339 Color: 8222
Size: 330256 Color: 5639
Size: 263406 Color: 1624

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 362260 Color: 6918
Size: 358095 Color: 6772
Size: 279646 Color: 2895

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 358041 Color: 6767
Size: 358031 Color: 6766
Size: 283929 Color: 3164

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 349219 Color: 6432
Size: 333640 Color: 5786
Size: 317142 Color: 5054

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 357790 Color: 6758
Size: 351951 Color: 6547
Size: 290260 Color: 3574

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 357806 Color: 6761
Size: 339665 Color: 6049
Size: 302530 Color: 4276

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 381057 Color: 7493
Size: 348683 Color: 6415
Size: 270261 Color: 2185

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 368689 Color: 7127
Size: 357972 Color: 6765
Size: 273340 Color: 2435

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 366652 Color: 7051
Size: 349278 Color: 6435
Size: 284071 Color: 3180

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 361436 Color: 6886
Size: 356934 Color: 6732
Size: 281631 Color: 3026

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 368086 Color: 7110
Size: 345973 Color: 6308
Size: 285942 Color: 3307

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 371535 Color: 7199
Size: 358630 Color: 6788
Size: 269836 Color: 2149

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 358087 Color: 6771
Size: 329651 Color: 5615
Size: 312263 Color: 4805

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 361528 Color: 6890
Size: 353231 Color: 6589
Size: 285242 Color: 3251

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 399255 Color: 8038
Size: 334841 Color: 5840
Size: 265905 Color: 1818

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 358056 Color: 6769
Size: 357089 Color: 6735
Size: 284856 Color: 3228

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 356920 Color: 6729
Size: 339099 Color: 6028
Size: 303982 Color: 4347

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 371753 Color: 7207
Size: 341827 Color: 6146
Size: 286421 Color: 3342

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 360173 Color: 6849
Size: 350708 Color: 6490
Size: 289120 Color: 3508

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 365402 Color: 7018
Size: 347796 Color: 6382
Size: 286803 Color: 3364

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 377618 Color: 7389
Size: 335379 Color: 5877
Size: 287004 Color: 3374

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 362288 Color: 6919
Size: 356447 Color: 6719
Size: 281266 Color: 3002

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 356360 Color: 6716
Size: 355938 Color: 6692
Size: 287703 Color: 3415

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 365350 Color: 7017
Size: 358607 Color: 6787
Size: 276044 Color: 2645

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 355595 Color: 6678
Size: 355570 Color: 6677
Size: 288836 Color: 3487

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 351634 Color: 6533
Size: 334584 Color: 5828
Size: 313783 Color: 4892

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 396779 Color: 7971
Size: 336556 Color: 5927
Size: 266666 Color: 1878

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 353486 Color: 6602
Size: 344897 Color: 6265
Size: 301618 Color: 4218

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 351610 Color: 6532
Size: 351597 Color: 6531
Size: 296794 Color: 3970

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 351053 Color: 6508
Size: 334634 Color: 5833
Size: 314314 Color: 4921

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 371995 Color: 7215
Size: 347480 Color: 6371
Size: 280526 Color: 2961

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 355981 Color: 6698
Size: 353081 Color: 6585
Size: 290939 Color: 3622

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 355204 Color: 6659
Size: 350030 Color: 6459
Size: 294767 Color: 3857

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 357065 Color: 6733
Size: 355244 Color: 6660
Size: 287692 Color: 3414

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 377046 Color: 7374
Size: 361023 Color: 6870
Size: 261932 Color: 1500

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 361866 Color: 6903
Size: 356890 Color: 6727
Size: 281245 Color: 2999

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 355784 Color: 6684
Size: 355724 Color: 6681
Size: 288493 Color: 3470

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 360737 Color: 6858
Size: 357607 Color: 6756
Size: 281657 Color: 3028

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 367891 Color: 7096
Size: 355268 Color: 6665
Size: 276842 Color: 2710

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 368689 Color: 7126
Size: 342312 Color: 6160
Size: 289000 Color: 3497

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 361532 Color: 6891
Size: 359935 Color: 6843
Size: 278534 Color: 2830

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 355345 Color: 6670
Size: 355290 Color: 6668
Size: 289366 Color: 3527

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 367014 Color: 7062
Size: 355261 Color: 6664
Size: 277726 Color: 2766

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 374920 Color: 7305
Size: 332981 Color: 5759
Size: 292100 Color: 3703

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 355956 Color: 6693
Size: 355276 Color: 6667
Size: 288769 Color: 3483

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 382088 Color: 7527
Size: 351588 Color: 6529
Size: 266325 Color: 1853

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 352972 Color: 6582
Size: 350224 Color: 6470
Size: 296805 Color: 3974

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 356608 Color: 6723
Size: 354381 Color: 6634
Size: 289012 Color: 3498

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 367987 Color: 7102
Size: 340083 Color: 6073
Size: 291931 Color: 3696

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 354628 Color: 6643
Size: 354622 Color: 6642
Size: 290751 Color: 3605

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 354662 Color: 6646
Size: 354606 Color: 6641
Size: 290733 Color: 3603

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 354587 Color: 6640
Size: 354555 Color: 6638
Size: 290859 Color: 3615

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 354530 Color: 6637
Size: 354504 Color: 6636
Size: 290967 Color: 3625

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 385219 Color: 7614
Size: 353588 Color: 6605
Size: 261194 Color: 1437

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 378940 Color: 7433
Size: 360794 Color: 6862
Size: 260267 Color: 1346

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 354180 Color: 6629
Size: 354121 Color: 6628
Size: 291700 Color: 3672

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 354120 Color: 6627
Size: 354074 Color: 6624
Size: 291807 Color: 3688

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 345188 Color: 6281
Size: 331506 Color: 5689
Size: 323307 Color: 5328

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 350396 Color: 6477
Size: 326041 Color: 5460
Size: 323564 Color: 5342

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 363811 Color: 6968
Size: 352111 Color: 6556
Size: 284079 Color: 3181

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 359082 Color: 6815
Size: 353811 Color: 6612
Size: 287108 Color: 3381

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 353857 Color: 6615
Size: 353817 Color: 6613
Size: 292327 Color: 3717

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 349107 Color: 6428
Size: 348526 Color: 6403
Size: 302368 Color: 4264

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 353702 Color: 6609
Size: 352208 Color: 6559
Size: 294091 Color: 3814

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 357581 Color: 6754
Size: 349511 Color: 6445
Size: 292909 Color: 3749

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 353623 Color: 6606
Size: 353532 Color: 6604
Size: 292846 Color: 3747

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 373880 Color: 7275
Size: 350758 Color: 6492
Size: 275363 Color: 2597

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 358783 Color: 6800
Size: 353490 Color: 6603
Size: 287728 Color: 3418

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 353235 Color: 6590
Size: 350104 Color: 6462
Size: 296662 Color: 3963

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 357148 Color: 6736
Size: 353380 Color: 6596
Size: 289473 Color: 3534

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 376693 Color: 7366
Size: 350377 Color: 6475
Size: 272931 Color: 2402

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 366842 Color: 7058
Size: 362387 Color: 6927
Size: 270772 Color: 2228

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 355256 Color: 6663
Size: 350512 Color: 6480
Size: 294233 Color: 3823

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 352898 Color: 6581
Size: 333252 Color: 5769
Size: 313851 Color: 4897

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 352620 Color: 6573
Size: 352511 Color: 6571
Size: 294870 Color: 3861

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 352609 Color: 6572
Size: 352495 Color: 6570
Size: 294897 Color: 3864

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 362185 Color: 6917
Size: 342559 Color: 6166
Size: 295257 Color: 3887

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 353724 Color: 6610
Size: 350802 Color: 6494
Size: 295475 Color: 3900

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 378571 Color: 7421
Size: 360142 Color: 6848
Size: 261288 Color: 1447

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 352872 Color: 6580
Size: 350935 Color: 6498
Size: 296194 Color: 3940

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 357799 Color: 6760
Size: 345907 Color: 6306
Size: 296295 Color: 3944

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 381047 Color: 7492
Size: 355976 Color: 6696
Size: 262978 Color: 1591

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 384368 Color: 7586
Size: 350965 Color: 6503
Size: 264668 Color: 1711

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 388286 Color: 7705
Size: 325455 Color: 5429
Size: 286260 Color: 3330

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 344691 Color: 6261
Size: 331345 Color: 5681
Size: 323965 Color: 5358

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 351673 Color: 6535
Size: 350938 Color: 6499
Size: 297390 Color: 4003

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 351903 Color: 6542
Size: 351175 Color: 6514
Size: 296923 Color: 3977

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 406584 Color: 8229
Size: 306298 Color: 4474
Size: 287119 Color: 3382

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 371387 Color: 7194
Size: 348633 Color: 6413
Size: 279981 Color: 2915

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 398914 Color: 8029
Size: 349543 Color: 6447
Size: 251544 Color: 273

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 367293 Color: 7067
Size: 350496 Color: 6479
Size: 282212 Color: 3069

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 375108 Color: 7308
Size: 354237 Color: 6630
Size: 270656 Color: 2218

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 373926 Color: 7279
Size: 356658 Color: 6724
Size: 269417 Color: 2111

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 363424 Color: 6959
Size: 357913 Color: 6763
Size: 278664 Color: 2840

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 362688 Color: 6934
Size: 337652 Color: 5973
Size: 299661 Color: 4110

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 351160 Color: 6513
Size: 326776 Color: 5494
Size: 322065 Color: 5283

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 350159 Color: 6466
Size: 350144 Color: 6464
Size: 299698 Color: 4112

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 350166 Color: 6468
Size: 349840 Color: 6452
Size: 299995 Color: 4128

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 368579 Color: 7123
Size: 349999 Color: 6458
Size: 281423 Color: 3012

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 335466 Color: 5881
Size: 332560 Color: 5734
Size: 331975 Color: 5711

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 390954 Color: 7783
Size: 331469 Color: 5688
Size: 277578 Color: 2758

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 358780 Color: 6799
Size: 341315 Color: 6125
Size: 299906 Color: 4126

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 361971 Color: 6908
Size: 352631 Color: 6574
Size: 285399 Color: 3266

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 360348 Color: 6852
Size: 353149 Color: 6587
Size: 286504 Color: 3350

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 349435 Color: 6442
Size: 349396 Color: 6439
Size: 301170 Color: 4197

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 377797 Color: 7393
Size: 355680 Color: 6679
Size: 266524 Color: 1866

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 411154 Color: 8326
Size: 305615 Color: 4438
Size: 283232 Color: 3125

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 358686 Color: 6794
Size: 349121 Color: 6429
Size: 292194 Color: 3708

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 351102 Color: 6510
Size: 346221 Color: 6320
Size: 302678 Color: 4283

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 357302 Color: 6741
Size: 351130 Color: 6512
Size: 291569 Color: 3662

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 379310 Color: 7443
Size: 348833 Color: 6420
Size: 271858 Color: 2319

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 363530 Color: 6962
Size: 324002 Color: 5359
Size: 312469 Color: 4818

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 355976 Color: 6695
Size: 329866 Color: 5619
Size: 314159 Color: 4909

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 402439 Color: 8110
Size: 346563 Color: 6336
Size: 250999 Color: 193

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 363111 Color: 6944
Size: 346126 Color: 6317
Size: 290764 Color: 3606

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 359004 Color: 6812
Size: 353283 Color: 6593
Size: 287714 Color: 3417

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 363382 Color: 6958
Size: 346279 Color: 6324
Size: 290340 Color: 3580

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 354276 Color: 6632
Size: 352004 Color: 6553
Size: 293721 Color: 3788

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 364332 Color: 6987
Size: 344405 Color: 6247
Size: 291264 Color: 3641

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 366144 Color: 7036
Size: 344417 Color: 6248
Size: 289440 Color: 3531

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 373093 Color: 7244
Size: 340915 Color: 6107
Size: 285993 Color: 3313

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 346468 Color: 6334
Size: 327710 Color: 5530
Size: 325823 Color: 5453

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 349929 Color: 6453
Size: 327514 Color: 5523
Size: 322558 Color: 5299

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 360983 Color: 6867
Size: 351978 Color: 6548
Size: 287040 Color: 3377

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 361178 Color: 6876
Size: 358265 Color: 6777
Size: 280558 Color: 2963

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 361045 Color: 6872
Size: 352000 Color: 6552
Size: 286956 Color: 3371

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 378625 Color: 7422
Size: 337878 Color: 5985
Size: 283498 Color: 3145

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 406073 Color: 8211
Size: 340056 Color: 6070
Size: 253872 Color: 595

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 364303 Color: 6986
Size: 358639 Color: 6790
Size: 277059 Color: 2722

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 367300 Color: 7070
Size: 346082 Color: 6315
Size: 286619 Color: 3355

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 383718 Color: 7574
Size: 329579 Color: 5611
Size: 286704 Color: 3360

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 361566 Color: 6893
Size: 356088 Color: 6707
Size: 282347 Color: 3077

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 377338 Color: 7380
Size: 355079 Color: 6656
Size: 267584 Color: 1955

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 368353 Color: 7115
Size: 361112 Color: 6874
Size: 270536 Color: 2209

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 364645 Color: 7000
Size: 350374 Color: 6474
Size: 284982 Color: 3234

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 379845 Color: 7457
Size: 353371 Color: 6595
Size: 266785 Color: 1888

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 398547 Color: 8022
Size: 348709 Color: 6417
Size: 252745 Color: 451

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 363894 Color: 6972
Size: 351512 Color: 6527
Size: 284595 Color: 3208

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 371860 Color: 7212
Size: 359389 Color: 6819
Size: 268752 Color: 2057

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 363261 Color: 6949
Size: 352094 Color: 6555
Size: 284646 Color: 3214

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 373432 Color: 7256
Size: 324534 Color: 5382
Size: 302035 Color: 4243

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 350946 Color: 6501
Size: 325117 Color: 5407
Size: 323938 Color: 5357

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 354108 Color: 6626
Size: 346339 Color: 6326
Size: 299554 Color: 4105

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 349009 Color: 6426
Size: 348937 Color: 6423
Size: 302055 Color: 4246

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 363234 Color: 6948
Size: 358338 Color: 6780
Size: 278429 Color: 2821

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 374491 Color: 7290
Size: 341524 Color: 6134
Size: 283986 Color: 3171

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 351030 Color: 6504
Size: 348576 Color: 6407
Size: 300395 Color: 4151

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 348626 Color: 6412
Size: 348439 Color: 6400
Size: 302936 Color: 4299

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 393952 Color: 7885
Size: 329754 Color: 5617
Size: 276295 Color: 2664

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 339881 Color: 6059
Size: 330106 Color: 5632
Size: 330014 Color: 5626

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 397729 Color: 7996
Size: 327461 Color: 5518
Size: 274811 Color: 2552

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 361068 Color: 6873
Size: 333587 Color: 5781
Size: 305346 Color: 4428

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 380346 Color: 7472
Size: 367705 Color: 7086
Size: 251950 Color: 340

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 405790 Color: 8201
Size: 329982 Color: 5624
Size: 264229 Color: 1684

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 361523 Color: 6888
Size: 346761 Color: 6342
Size: 291717 Color: 3673

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 395604 Color: 7937
Size: 352168 Color: 6557
Size: 252229 Color: 380

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 402933 Color: 8126
Size: 306669 Color: 4492
Size: 290399 Color: 3585

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 347920 Color: 6385
Size: 347470 Color: 6370
Size: 304611 Color: 4389

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 384731 Color: 7599
Size: 308209 Color: 4574
Size: 307061 Color: 4513

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 348536 Color: 6404
Size: 335214 Color: 5868
Size: 316251 Color: 5013

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 348694 Color: 6416
Size: 346329 Color: 6325
Size: 304978 Color: 4413

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 367992 Color: 7104
Size: 350645 Color: 6487
Size: 281364 Color: 3008

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 364679 Color: 7002
Size: 356169 Color: 6708
Size: 279153 Color: 2871

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 362313 Color: 6921
Size: 352790 Color: 6578
Size: 284898 Color: 3229

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 362681 Color: 6933
Size: 331859 Color: 5706
Size: 305461 Color: 4432

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 349560 Color: 6448
Size: 340562 Color: 6095
Size: 309879 Color: 4668

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 374865 Color: 7300
Size: 347036 Color: 6354
Size: 278100 Color: 2796

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 352696 Color: 6577
Size: 346852 Color: 6348
Size: 300453 Color: 4157

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 367300 Color: 7069
Size: 318533 Color: 5117
Size: 314168 Color: 4912

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 367396 Color: 7076
Size: 316833 Color: 5033
Size: 315772 Color: 4977

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 367575 Color: 7081
Size: 318061 Color: 5097
Size: 314365 Color: 4923

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 367596 Color: 7083
Size: 316551 Color: 5023
Size: 315854 Color: 4987

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 368025 Color: 7107
Size: 318681 Color: 5126
Size: 313295 Color: 4864

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 368371 Color: 7118
Size: 317398 Color: 5065
Size: 314232 Color: 4917

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 368908 Color: 7134
Size: 318103 Color: 5099
Size: 312990 Color: 4848

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 369024 Color: 7136
Size: 315831 Color: 4983
Size: 315146 Color: 4952

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 369256 Color: 7144
Size: 318219 Color: 5104
Size: 312526 Color: 4820

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 369421 Color: 7148
Size: 316613 Color: 5024
Size: 313967 Color: 4903

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 369508 Color: 7150
Size: 318254 Color: 5106
Size: 312239 Color: 4803

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 369682 Color: 7153
Size: 318177 Color: 5103
Size: 312142 Color: 4799

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 369742 Color: 7155
Size: 344744 Color: 6263
Size: 285515 Color: 3275

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 369769 Color: 7157
Size: 317677 Color: 5083
Size: 312555 Color: 4823

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 369775 Color: 7158
Size: 322505 Color: 5294
Size: 307721 Color: 4550

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 369935 Color: 7160
Size: 320879 Color: 5222
Size: 309187 Color: 4622

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 369971 Color: 7162
Size: 321718 Color: 5259
Size: 308312 Color: 4578

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 370090 Color: 7164
Size: 316440 Color: 5018
Size: 313471 Color: 4872

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 370101 Color: 7165
Size: 315979 Color: 4996
Size: 313921 Color: 4899

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 370144 Color: 7167
Size: 319285 Color: 5151
Size: 310572 Color: 4710

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 370218 Color: 7168
Size: 318660 Color: 5123
Size: 311123 Color: 4741

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 370257 Color: 7169
Size: 318665 Color: 5125
Size: 311079 Color: 4738

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 370281 Color: 7170
Size: 315517 Color: 4963
Size: 314203 Color: 4915

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 370303 Color: 7171
Size: 319240 Color: 5148
Size: 310458 Color: 4705

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 370335 Color: 7172
Size: 321018 Color: 5226
Size: 308648 Color: 4595

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 370336 Color: 7173
Size: 320025 Color: 5181
Size: 309640 Color: 4648

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 370341 Color: 7174
Size: 321114 Color: 5231
Size: 308546 Color: 4587

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 370396 Color: 7175
Size: 321779 Color: 5263
Size: 307826 Color: 4554

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 370438 Color: 7176
Size: 315953 Color: 4994
Size: 313610 Color: 4879

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 370443 Color: 7177
Size: 316909 Color: 5036
Size: 312649 Color: 4831

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 370464 Color: 7178
Size: 316924 Color: 5037
Size: 312613 Color: 4830

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 370473 Color: 7179
Size: 315853 Color: 4986
Size: 313675 Color: 4883

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 370503 Color: 7180
Size: 314752 Color: 4933
Size: 314746 Color: 4932

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 370507 Color: 7181
Size: 315915 Color: 4991
Size: 313579 Color: 4877

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 370528 Color: 7182
Size: 317523 Color: 5073
Size: 311950 Color: 4786

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 370646 Color: 7183
Size: 322346 Color: 5290
Size: 307009 Color: 4511

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 370683 Color: 7184
Size: 319900 Color: 5175
Size: 309418 Color: 4636

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 370821 Color: 7185
Size: 321835 Color: 5266
Size: 307345 Color: 4531

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 370846 Color: 7186
Size: 321278 Color: 5240
Size: 307877 Color: 4556

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 370924 Color: 7187
Size: 315185 Color: 4953
Size: 313892 Color: 4898

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 373580 Color: 7264
Size: 318311 Color: 5110
Size: 308110 Color: 4568

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 373604 Color: 7265
Size: 320757 Color: 5212
Size: 305640 Color: 4440

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 373702 Color: 7268
Size: 316267 Color: 5014
Size: 310032 Color: 4677

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 373708 Color: 7269
Size: 321165 Color: 5232
Size: 305128 Color: 4421

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 373719 Color: 7270
Size: 318691 Color: 5128
Size: 307591 Color: 4541

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 373727 Color: 7271
Size: 321416 Color: 5246
Size: 304858 Color: 4405

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 373926 Color: 7278
Size: 319106 Color: 5138
Size: 306969 Color: 4508

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 373940 Color: 7280
Size: 323809 Color: 5352
Size: 302252 Color: 4259

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 373971 Color: 7282
Size: 325313 Color: 5421
Size: 300717 Color: 4168

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 373978 Color: 7283
Size: 324179 Color: 5364
Size: 301844 Color: 4235

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 374201 Color: 7287
Size: 316389 Color: 5016
Size: 309411 Color: 4634

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 374267 Color: 7288
Size: 319133 Color: 5141
Size: 306601 Color: 4488

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 374279 Color: 7289
Size: 320086 Color: 5184
Size: 305636 Color: 4439

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 374631 Color: 7293
Size: 321246 Color: 5237
Size: 304124 Color: 4357

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 374665 Color: 7294
Size: 320578 Color: 5206
Size: 304758 Color: 4397

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 374689 Color: 7295
Size: 323105 Color: 5315
Size: 302207 Color: 4254

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 375088 Color: 7307
Size: 323109 Color: 5317
Size: 301804 Color: 4231

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 375211 Color: 7311
Size: 321857 Color: 5267
Size: 302933 Color: 4297

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 375219 Color: 7312
Size: 313794 Color: 4894
Size: 310988 Color: 4734

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 375243 Color: 7313
Size: 326507 Color: 5477
Size: 298251 Color: 4051

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 375308 Color: 7316
Size: 315143 Color: 4951
Size: 309550 Color: 4642

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 375336 Color: 7317
Size: 313466 Color: 4870
Size: 311199 Color: 4748

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 375341 Color: 7318
Size: 324239 Color: 5369
Size: 300421 Color: 4154

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 375370 Color: 7319
Size: 323790 Color: 5351
Size: 300841 Color: 4176

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 375702 Color: 7326
Size: 319697 Color: 5167
Size: 304602 Color: 4388

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 375710 Color: 7327
Size: 344201 Color: 6240
Size: 280090 Color: 2926

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 375739 Color: 7328
Size: 324663 Color: 5385
Size: 299599 Color: 4107

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 375784 Color: 7330
Size: 322393 Color: 5291
Size: 301824 Color: 4233

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 375806 Color: 7331
Size: 317608 Color: 5077
Size: 306587 Color: 4485

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 375820 Color: 7332
Size: 324859 Color: 5398
Size: 299322 Color: 4100

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 375877 Color: 7333
Size: 313117 Color: 4854
Size: 311007 Color: 4735

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 375928 Color: 7334
Size: 313543 Color: 4874
Size: 310530 Color: 4707

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 375940 Color: 7335
Size: 318853 Color: 5131
Size: 305208 Color: 4424

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 375981 Color: 7336
Size: 314896 Color: 4942
Size: 309124 Color: 4619

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 376047 Color: 7337
Size: 321415 Color: 5245
Size: 302539 Color: 4278

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 376071 Color: 7338
Size: 321860 Color: 5269
Size: 302070 Color: 4247

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 376085 Color: 7339
Size: 315958 Color: 4995
Size: 307958 Color: 4559

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 376087 Color: 7340
Size: 313029 Color: 4850
Size: 310885 Color: 4730

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 376142 Color: 7341
Size: 316990 Color: 5040
Size: 306869 Color: 4504

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 376144 Color: 7342
Size: 322261 Color: 5288
Size: 301596 Color: 4216

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 376166 Color: 7343
Size: 312358 Color: 4809
Size: 311477 Color: 4760

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 376167 Color: 7344
Size: 323448 Color: 5336
Size: 300386 Color: 4150

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 376167 Color: 7345
Size: 322550 Color: 5298
Size: 301284 Color: 4203

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 376187 Color: 7346
Size: 314159 Color: 4907
Size: 309655 Color: 4650

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 376216 Color: 7347
Size: 326191 Color: 5463
Size: 297594 Color: 4014

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 376218 Color: 7348
Size: 322033 Color: 5277
Size: 301750 Color: 4225

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 376605 Color: 7362
Size: 325291 Color: 5419
Size: 298105 Color: 4041

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 376614 Color: 7363
Size: 315556 Color: 4965
Size: 307831 Color: 4555

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 376621 Color: 7364
Size: 323204 Color: 5324
Size: 300176 Color: 4134

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 376637 Color: 7365
Size: 317788 Color: 5086
Size: 305576 Color: 4436

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 376789 Color: 7369
Size: 316616 Color: 5025
Size: 306596 Color: 4486

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 376824 Color: 7371
Size: 344060 Color: 6232
Size: 279117 Color: 2866

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 376947 Color: 7373
Size: 326106 Color: 5461
Size: 296948 Color: 3979

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 377056 Color: 7375
Size: 343942 Color: 6227
Size: 279003 Color: 2857

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 377079 Color: 7376
Size: 321891 Color: 5273
Size: 301031 Color: 4181

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 377093 Color: 7377
Size: 314966 Color: 4946
Size: 307942 Color: 4557

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 377186 Color: 7378
Size: 316132 Color: 5001
Size: 306683 Color: 4494

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 377374 Color: 7381
Size: 326619 Color: 5485
Size: 296008 Color: 3931

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 377481 Color: 7384
Size: 316243 Color: 5012
Size: 306277 Color: 4473

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 377488 Color: 7385
Size: 320856 Color: 5219
Size: 301657 Color: 4219

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 377510 Color: 7386
Size: 325404 Color: 5426
Size: 297087 Color: 3989

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 377671 Color: 7391
Size: 343810 Color: 6221
Size: 278520 Color: 2827

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 377852 Color: 7395
Size: 316469 Color: 5021
Size: 305680 Color: 4443

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 377864 Color: 7396
Size: 317189 Color: 5056
Size: 304948 Color: 4411

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 377907 Color: 7397
Size: 326510 Color: 5478
Size: 295584 Color: 3911

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 377914 Color: 7398
Size: 318079 Color: 5098
Size: 304008 Color: 4350

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 378049 Color: 7401
Size: 312366 Color: 4810
Size: 309586 Color: 4645

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 378056 Color: 7402
Size: 313136 Color: 4857
Size: 308809 Color: 4604

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 378064 Color: 7403
Size: 312017 Color: 4789
Size: 309920 Color: 4673

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 378067 Color: 7404
Size: 322431 Color: 5292
Size: 299503 Color: 4104

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 378067 Color: 7405
Size: 311157 Color: 4743
Size: 310777 Color: 4725

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 378073 Color: 7406
Size: 343592 Color: 6212
Size: 278336 Color: 2811

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 378100 Color: 7407
Size: 343647 Color: 6214
Size: 278254 Color: 2803

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 378173 Color: 7408
Size: 317009 Color: 5043
Size: 304819 Color: 4401

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 378177 Color: 7409
Size: 314823 Color: 4938
Size: 307001 Color: 4510

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 378179 Color: 7410
Size: 314337 Color: 4922
Size: 307485 Color: 4535

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 378183 Color: 7411
Size: 316138 Color: 5002
Size: 305680 Color: 4444

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 378258 Color: 7413
Size: 324608 Color: 5384
Size: 297135 Color: 3991

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 378359 Color: 7415
Size: 313212 Color: 4859
Size: 308430 Color: 4583

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 378418 Color: 7416
Size: 320441 Color: 5200
Size: 301142 Color: 4192

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 378429 Color: 7417
Size: 327203 Color: 5511
Size: 294369 Color: 3826

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 378456 Color: 7418
Size: 320551 Color: 5204
Size: 300994 Color: 4180

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 378687 Color: 7424
Size: 317220 Color: 5059
Size: 304094 Color: 4356

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 378691 Color: 7425
Size: 312583 Color: 4828
Size: 308727 Color: 4598

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 378724 Color: 7426
Size: 343320 Color: 6202
Size: 277957 Color: 2781

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 378932 Color: 7432
Size: 312092 Color: 4795
Size: 308977 Color: 4609

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 379124 Color: 7436
Size: 325514 Color: 5434
Size: 295363 Color: 3893

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 379176 Color: 7437
Size: 311301 Color: 4751
Size: 309524 Color: 4641

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 379183 Color: 7438
Size: 326976 Color: 5498
Size: 293842 Color: 3803

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 379247 Color: 7439
Size: 311337 Color: 4753
Size: 309417 Color: 4635

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 379256 Color: 7440
Size: 318289 Color: 5109
Size: 302456 Color: 4269

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 379258 Color: 7441
Size: 327116 Color: 5508
Size: 293627 Color: 3785

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 379308 Color: 7442
Size: 318331 Color: 5111
Size: 302362 Color: 4261

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 379328 Color: 7444
Size: 319606 Color: 5164
Size: 301067 Color: 4185

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 379372 Color: 7445
Size: 313764 Color: 4889
Size: 306865 Color: 4503

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 379657 Color: 7451
Size: 313134 Color: 4856
Size: 307210 Color: 4517

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 379692 Color: 7452
Size: 310321 Color: 4694
Size: 309988 Color: 4676

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 379718 Color: 7453
Size: 313963 Color: 4901
Size: 306320 Color: 4476

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 379839 Color: 7455
Size: 324433 Color: 5375
Size: 295729 Color: 3918

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 380065 Color: 7462
Size: 321772 Color: 5262
Size: 298164 Color: 4048

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 380080 Color: 7463
Size: 312427 Color: 4813
Size: 307494 Color: 4536

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 380119 Color: 7465
Size: 318287 Color: 5108
Size: 301595 Color: 4215

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 380209 Color: 7466
Size: 309905 Color: 4672
Size: 309887 Color: 4669

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 380266 Color: 7468
Size: 310952 Color: 4733
Size: 308783 Color: 4602

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 380309 Color: 7469
Size: 320468 Color: 5202
Size: 299224 Color: 4095

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 380475 Color: 7475
Size: 324488 Color: 5378
Size: 295038 Color: 3871

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 380481 Color: 7476
Size: 312071 Color: 4793
Size: 307449 Color: 4534

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 380571 Color: 7478
Size: 312568 Color: 4826
Size: 306862 Color: 4502

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 380620 Color: 7480
Size: 325512 Color: 5433
Size: 293869 Color: 3805

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 380651 Color: 7481
Size: 325635 Color: 5443
Size: 293715 Color: 3787

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 380652 Color: 7482
Size: 316628 Color: 5026
Size: 302721 Color: 4287

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 380753 Color: 7484
Size: 321420 Color: 5247
Size: 297828 Color: 4028

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 380828 Color: 7486
Size: 323660 Color: 5345
Size: 295513 Color: 3904

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 380859 Color: 7488
Size: 342656 Color: 6174
Size: 276486 Color: 2683

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 380926 Color: 7489
Size: 319732 Color: 5168
Size: 299343 Color: 4101

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 381085 Color: 7494
Size: 324416 Color: 5374
Size: 294500 Color: 3839

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 381125 Color: 7495
Size: 311301 Color: 4752
Size: 307575 Color: 4538

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 381143 Color: 7496
Size: 320503 Color: 5203
Size: 298355 Color: 4058

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 381144 Color: 7497
Size: 312558 Color: 4824
Size: 306299 Color: 4475

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 381254 Color: 7500
Size: 324720 Color: 5390
Size: 294027 Color: 3812

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 381288 Color: 7501
Size: 323175 Color: 5323
Size: 295538 Color: 3907

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 381360 Color: 7503
Size: 312965 Color: 4847
Size: 305676 Color: 4442

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 381422 Color: 7504
Size: 317404 Color: 5066
Size: 301175 Color: 4198

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 381432 Color: 7505
Size: 326261 Color: 5466
Size: 292308 Color: 3713

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 381449 Color: 7507
Size: 313822 Color: 4896
Size: 304730 Color: 4395

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 381449 Color: 7506
Size: 315920 Color: 4992
Size: 302632 Color: 4281

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 381453 Color: 7508
Size: 325721 Color: 5449
Size: 292827 Color: 3745

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 381469 Color: 7509
Size: 318526 Color: 5116
Size: 300006 Color: 4129

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 381535 Color: 7511
Size: 312414 Color: 4812
Size: 306052 Color: 4464

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 381678 Color: 7516
Size: 323411 Color: 5333
Size: 294912 Color: 3865

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 381678 Color: 7515
Size: 309896 Color: 4670
Size: 308427 Color: 4582

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 381706 Color: 7518
Size: 316092 Color: 4999
Size: 302203 Color: 4253

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 381707 Color: 7519
Size: 322605 Color: 5301
Size: 295689 Color: 3916

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 382171 Color: 7529
Size: 325937 Color: 5457
Size: 291893 Color: 3694

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 382198 Color: 7530
Size: 316077 Color: 4998
Size: 301726 Color: 4223

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 382205 Color: 7531
Size: 327082 Color: 5504
Size: 290714 Color: 3600

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 382494 Color: 7536
Size: 315458 Color: 4960
Size: 302049 Color: 4245

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 382522 Color: 7537
Size: 317139 Color: 5053
Size: 300340 Color: 4145

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 382570 Color: 7539
Size: 314094 Color: 4906
Size: 303337 Color: 4312

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 382633 Color: 7541
Size: 312848 Color: 4843
Size: 304520 Color: 4382

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 382637 Color: 7542
Size: 317021 Color: 5045
Size: 300343 Color: 4146

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 382638 Color: 7543
Size: 309774 Color: 4659
Size: 307589 Color: 4540

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 382674 Color: 7544
Size: 315744 Color: 4973
Size: 301583 Color: 4214

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 382714 Color: 7545
Size: 326552 Color: 5480
Size: 290735 Color: 3604

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 382730 Color: 7546
Size: 316853 Color: 5034
Size: 300418 Color: 4153

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 382732 Color: 7547
Size: 309187 Color: 4623
Size: 308082 Color: 4566

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 382762 Color: 7549
Size: 310414 Color: 4701
Size: 306825 Color: 4498

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 382766 Color: 7550
Size: 313711 Color: 4885
Size: 303524 Color: 4321

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 382774 Color: 7551
Size: 311035 Color: 4736
Size: 306192 Color: 4469

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 382961 Color: 7555
Size: 319229 Color: 5147
Size: 297811 Color: 4026

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 383024 Color: 7556
Size: 313809 Color: 4895
Size: 303168 Color: 4307

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 383053 Color: 7557
Size: 319268 Color: 5150
Size: 297680 Color: 4020

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 383237 Color: 7560
Size: 310831 Color: 4728
Size: 305933 Color: 4458

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 383401 Color: 7564
Size: 315372 Color: 4958
Size: 301228 Color: 4200

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 383515 Color: 7570
Size: 341476 Color: 6131
Size: 275010 Color: 2565

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 383565 Color: 7571
Size: 341450 Color: 6129
Size: 274986 Color: 2561

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 384305 Color: 7583
Size: 314603 Color: 4930
Size: 301093 Color: 4190

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 384353 Color: 7584
Size: 311349 Color: 4754
Size: 304299 Color: 4368

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 384358 Color: 7585
Size: 312197 Color: 4802
Size: 303446 Color: 4319

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 384455 Color: 7588
Size: 323733 Color: 5349
Size: 291813 Color: 3690

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 384463 Color: 7589
Size: 320962 Color: 5224
Size: 294576 Color: 3846

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 384489 Color: 7590
Size: 313388 Color: 4868
Size: 302124 Color: 4249

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 384548 Color: 7592
Size: 315509 Color: 4962
Size: 299944 Color: 4127

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 384579 Color: 7594
Size: 341046 Color: 6113
Size: 274376 Color: 2510

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 384616 Color: 7595
Size: 309516 Color: 4640
Size: 305869 Color: 4454

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 384620 Color: 7596
Size: 341031 Color: 6112
Size: 274350 Color: 2508

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 384897 Color: 7602
Size: 324695 Color: 5388
Size: 290409 Color: 3586

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 384901 Color: 7603
Size: 320015 Color: 5180
Size: 295085 Color: 3877

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 384929 Color: 7604
Size: 317947 Color: 5091
Size: 297125 Color: 3990

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 384968 Color: 7605
Size: 315770 Color: 4976
Size: 299263 Color: 4099

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 385015 Color: 7607
Size: 315806 Color: 4981
Size: 299180 Color: 4093

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 385057 Color: 7608
Size: 314159 Color: 4908
Size: 300785 Color: 4172

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 385087 Color: 7609
Size: 315522 Color: 4964
Size: 299392 Color: 4102

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 385163 Color: 7611
Size: 317849 Color: 5087
Size: 296989 Color: 3980

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 385255 Color: 7615
Size: 326688 Color: 5488
Size: 288058 Color: 3444

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 385267 Color: 7616
Size: 308300 Color: 4577
Size: 306434 Color: 4479

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 385381 Color: 7618
Size: 316681 Color: 5027
Size: 297939 Color: 4032

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 385385 Color: 7619
Size: 317584 Color: 5076
Size: 297032 Color: 3985

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 385386 Color: 7620
Size: 320088 Color: 5185
Size: 294527 Color: 3840

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 385393 Color: 7621
Size: 321101 Color: 5230
Size: 293507 Color: 3781

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 385584 Color: 7627
Size: 340679 Color: 6098
Size: 273738 Color: 2461

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 385844 Color: 7630
Size: 309144 Color: 4620
Size: 305013 Color: 4416

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 385866 Color: 7631
Size: 327164 Color: 5510
Size: 286971 Color: 3372

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 385990 Color: 7635
Size: 324487 Color: 5377
Size: 289524 Color: 3535

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 386001 Color: 7636
Size: 323476 Color: 5338
Size: 290524 Color: 3591

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 386002 Color: 7637
Size: 319517 Color: 5156
Size: 294482 Color: 3837

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 386210 Color: 7642
Size: 312026 Color: 4790
Size: 301765 Color: 4228

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 386231 Color: 7643
Size: 327061 Color: 5503
Size: 286709 Color: 3361

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 386290 Color: 7645
Size: 317457 Color: 5070
Size: 296254 Color: 3943

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 386293 Color: 7646
Size: 319894 Color: 5174
Size: 293814 Color: 3795

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 386305 Color: 7648
Size: 325100 Color: 5406
Size: 288596 Color: 3476

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 386510 Color: 7651
Size: 313755 Color: 4888
Size: 299736 Color: 4115

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 386570 Color: 7653
Size: 310356 Color: 4695
Size: 303075 Color: 4304

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 386605 Color: 7655
Size: 314163 Color: 4911
Size: 299233 Color: 4096

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 386627 Color: 7656
Size: 319225 Color: 5145
Size: 294149 Color: 3816

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 386641 Color: 7657
Size: 323255 Color: 5327
Size: 290105 Color: 3567

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 386642 Color: 7658
Size: 308956 Color: 4608
Size: 304403 Color: 4375

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 386658 Color: 7659
Size: 310422 Color: 4702
Size: 302921 Color: 4296

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 386668 Color: 7660
Size: 317916 Color: 5088
Size: 295417 Color: 3896

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 386696 Color: 7661
Size: 311087 Color: 4739
Size: 302218 Color: 4255

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 386724 Color: 7663
Size: 307155 Color: 4514
Size: 306122 Color: 4466

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 386860 Color: 7666
Size: 312064 Color: 4792
Size: 301077 Color: 4186

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 387232 Color: 7674
Size: 319225 Color: 5146
Size: 293544 Color: 3783

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 387252 Color: 7675
Size: 321257 Color: 5238
Size: 291492 Color: 3655

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 387340 Color: 7678
Size: 320871 Color: 5220
Size: 291790 Color: 3685

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 387364 Color: 7679
Size: 314858 Color: 4940
Size: 297779 Color: 4025

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 387371 Color: 7680
Size: 317932 Color: 5089
Size: 294698 Color: 3851

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 387380 Color: 7681
Size: 339996 Color: 6065
Size: 272625 Color: 2381

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 387571 Color: 7685
Size: 318984 Color: 5136
Size: 293446 Color: 3779

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 387635 Color: 7686
Size: 306756 Color: 4495
Size: 305610 Color: 4437

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 387641 Color: 7687
Size: 313276 Color: 4862
Size: 299084 Color: 4089

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 387681 Color: 7688
Size: 326467 Color: 5475
Size: 285853 Color: 3297

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 387753 Color: 7689
Size: 308583 Color: 4591
Size: 303665 Color: 4327

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 387787 Color: 7690
Size: 309014 Color: 4612
Size: 303200 Color: 4309

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 387789 Color: 7691
Size: 324224 Color: 5366
Size: 287988 Color: 3439

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 387791 Color: 7692
Size: 323166 Color: 5321
Size: 289044 Color: 3499

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 387828 Color: 7693
Size: 314783 Color: 4935
Size: 297390 Color: 4002

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 387887 Color: 7695
Size: 311730 Color: 4773
Size: 300384 Color: 4149

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 388107 Color: 7701
Size: 310622 Color: 4717
Size: 301272 Color: 4202

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 388136 Color: 7702
Size: 311065 Color: 4737
Size: 300800 Color: 4175

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 388237 Color: 7703
Size: 309297 Color: 4627
Size: 302467 Color: 4271

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 388873 Color: 7716
Size: 324843 Color: 5396
Size: 286285 Color: 3331

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 388997 Color: 7719
Size: 307987 Color: 4562
Size: 303017 Color: 4300

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 389005 Color: 7720
Size: 314691 Color: 4931
Size: 296305 Color: 3945

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 389016 Color: 7721
Size: 321898 Color: 5274
Size: 289087 Color: 3506

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 389055 Color: 7723
Size: 319780 Color: 5169
Size: 291166 Color: 3638

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 389077 Color: 7724
Size: 317549 Color: 5074
Size: 293375 Color: 3773

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 389120 Color: 7725
Size: 307963 Color: 4561
Size: 302918 Color: 4295

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 389140 Color: 7726
Size: 325390 Color: 5425
Size: 285471 Color: 3270

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 389247 Color: 7730
Size: 339431 Color: 6040
Size: 271323 Color: 2274

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 389275 Color: 7731
Size: 319138 Color: 5142
Size: 291588 Color: 3664

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 389288 Color: 7732
Size: 312805 Color: 4837
Size: 297908 Color: 4030

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 389332 Color: 7733
Size: 309986 Color: 4675
Size: 300683 Color: 4167

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 389346 Color: 7734
Size: 306652 Color: 4490
Size: 304003 Color: 4349

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 389442 Color: 7738
Size: 308785 Color: 4603
Size: 301774 Color: 4229

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 389479 Color: 7739
Size: 308140 Color: 4570
Size: 302382 Color: 4265

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 389512 Color: 7740
Size: 325483 Color: 5430
Size: 285006 Color: 3235

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 389527 Color: 7741
Size: 310388 Color: 4698
Size: 300086 Color: 4133

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 389543 Color: 7742
Size: 306255 Color: 4472
Size: 304203 Color: 4359

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 389652 Color: 7746
Size: 307215 Color: 4518
Size: 303134 Color: 4305

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 389723 Color: 7747
Size: 322316 Color: 5289
Size: 287962 Color: 3438

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 389764 Color: 7748
Size: 325412 Color: 5427
Size: 284825 Color: 3223

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 389783 Color: 7750
Size: 307600 Color: 4542
Size: 302618 Color: 4279

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 390120 Color: 7757
Size: 339030 Color: 6022
Size: 270851 Color: 2233

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 390122 Color: 7758
Size: 324918 Color: 5400
Size: 284961 Color: 3233

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 390163 Color: 7759
Size: 321769 Color: 5261
Size: 288069 Color: 3446

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 390230 Color: 7762
Size: 311148 Color: 4742
Size: 298623 Color: 4074

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 390266 Color: 7763
Size: 313073 Color: 4853
Size: 296662 Color: 3964

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 390341 Color: 7765
Size: 320286 Color: 5194
Size: 289374 Color: 3528

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 390342 Color: 7766
Size: 308577 Color: 4590
Size: 301082 Color: 4187

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 390355 Color: 7768
Size: 321436 Color: 5248
Size: 288210 Color: 3456

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 390355 Color: 7767
Size: 310404 Color: 4700
Size: 299242 Color: 4097

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 390364 Color: 7769
Size: 312428 Color: 4814
Size: 297209 Color: 3994

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 390424 Color: 7770
Size: 319917 Color: 5177
Size: 289660 Color: 3540

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 390444 Color: 7771
Size: 307433 Color: 4533
Size: 302124 Color: 4250

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 390814 Color: 7778
Size: 310226 Color: 4689
Size: 298961 Color: 4085

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 391056 Color: 7784
Size: 306791 Color: 4497
Size: 302154 Color: 4252

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 391067 Color: 7785
Size: 322519 Color: 5296
Size: 286415 Color: 3341

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 391103 Color: 7786
Size: 305082 Color: 4419
Size: 303816 Color: 4335

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 391131 Color: 7787
Size: 323656 Color: 5344
Size: 285214 Color: 3250

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 391261 Color: 7788
Size: 321390 Color: 5244
Size: 287350 Color: 3397

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 391264 Color: 7789
Size: 310313 Color: 4693
Size: 298424 Color: 4063

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 391343 Color: 7790
Size: 307613 Color: 4543
Size: 301045 Color: 4183

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 391365 Color: 7791
Size: 319405 Color: 5153
Size: 289231 Color: 3520

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 391378 Color: 7792
Size: 318786 Color: 5129
Size: 289837 Color: 3554

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 391409 Color: 7793
Size: 320340 Color: 5196
Size: 288252 Color: 3459

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 391424 Color: 7794
Size: 306152 Color: 4467
Size: 302425 Color: 4266

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 391454 Color: 7795
Size: 322042 Color: 5278
Size: 286505 Color: 3351

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 391487 Color: 7796
Size: 326448 Color: 5473
Size: 282066 Color: 3060

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 391520 Color: 7797
Size: 321218 Color: 5236
Size: 287263 Color: 3389

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 391537 Color: 7798
Size: 313707 Color: 4884
Size: 294757 Color: 3856

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 391564 Color: 7799
Size: 307959 Color: 4560
Size: 300478 Color: 4158

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 391596 Color: 7800
Size: 317682 Color: 5084
Size: 290723 Color: 3602

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 391612 Color: 7801
Size: 322216 Color: 5286
Size: 286173 Color: 3327

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 391646 Color: 7802
Size: 304491 Color: 4380
Size: 303864 Color: 4340

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 391660 Color: 7803
Size: 310200 Color: 4687
Size: 298141 Color: 4045

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 391696 Color: 7804
Size: 322518 Color: 5295
Size: 285787 Color: 3294

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 391709 Color: 7805
Size: 315831 Color: 4982
Size: 292461 Color: 3721

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 391731 Color: 7806
Size: 306937 Color: 4507
Size: 301333 Color: 4206

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 391733 Color: 7807
Size: 311858 Color: 4779
Size: 296410 Color: 3948

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 391745 Color: 7808
Size: 318248 Color: 5105
Size: 290008 Color: 3563

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 391775 Color: 7809
Size: 310728 Color: 4722
Size: 297498 Color: 4011

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 391784 Color: 7810
Size: 317411 Color: 5067
Size: 290806 Color: 3609

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 391821 Color: 7811
Size: 309642 Color: 4649
Size: 298538 Color: 4070

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 391872 Color: 7812
Size: 311477 Color: 4761
Size: 296652 Color: 3962

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 391897 Color: 7813
Size: 324576 Color: 5383
Size: 283528 Color: 3148

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 391905 Color: 7814
Size: 307336 Color: 4528
Size: 300760 Color: 4170

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 391908 Color: 7815
Size: 307769 Color: 4553
Size: 300324 Color: 4144

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 391909 Color: 7816
Size: 316462 Color: 5020
Size: 291630 Color: 3666

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 391942 Color: 7817
Size: 317096 Color: 5050
Size: 290963 Color: 3624

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 391953 Color: 7818
Size: 320204 Color: 5190
Size: 287844 Color: 3430

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 391972 Color: 7819
Size: 315584 Color: 4967
Size: 292445 Color: 3720

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 391991 Color: 7820
Size: 307333 Color: 4527
Size: 300677 Color: 4165

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 392004 Color: 7821
Size: 316439 Color: 5017
Size: 291558 Color: 3659

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 392028 Color: 7822
Size: 309718 Color: 4656
Size: 298255 Color: 4052

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 392038 Color: 7823
Size: 323392 Color: 5330
Size: 284571 Color: 3206

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 392046 Color: 7824
Size: 322168 Color: 5285
Size: 285787 Color: 3295

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 392079 Color: 7825
Size: 310310 Color: 4692
Size: 297612 Color: 4016

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 392084 Color: 7826
Size: 311420 Color: 4757
Size: 296497 Color: 3953

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 392106 Color: 7827
Size: 313366 Color: 4867
Size: 294529 Color: 3841

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 392122 Color: 7828
Size: 311979 Color: 4788
Size: 295900 Color: 3924

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 392235 Color: 7829
Size: 324230 Color: 5367
Size: 283536 Color: 3150

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 392241 Color: 7830
Size: 312099 Color: 4797
Size: 295661 Color: 3914

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 392283 Color: 7831
Size: 316532 Color: 5022
Size: 291186 Color: 3639

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 392304 Color: 7832
Size: 309012 Color: 4611
Size: 298685 Color: 4078

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 392361 Color: 7833
Size: 326388 Color: 5471
Size: 281252 Color: 3000

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 392385 Color: 7834
Size: 320034 Color: 5182
Size: 287582 Color: 3407

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 392401 Color: 7835
Size: 313782 Color: 4891
Size: 293818 Color: 3796

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 392463 Color: 7836
Size: 311684 Color: 4768
Size: 295854 Color: 3921

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 392486 Color: 7838
Size: 304829 Color: 4402
Size: 302686 Color: 4284

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 392486 Color: 7837
Size: 319642 Color: 5165
Size: 287873 Color: 3432

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 392491 Color: 7839
Size: 306615 Color: 4489
Size: 300895 Color: 4178

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 392513 Color: 7840
Size: 315283 Color: 4956
Size: 292205 Color: 3709

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 392523 Color: 7841
Size: 324532 Color: 5381
Size: 282946 Color: 3110

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 392556 Color: 7842
Size: 310387 Color: 4697
Size: 297058 Color: 3987

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 392595 Color: 7843
Size: 304896 Color: 4407
Size: 302510 Color: 4275

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 392620 Color: 7844
Size: 304621 Color: 4391
Size: 302760 Color: 4289

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 392743 Color: 7845
Size: 325051 Color: 5404
Size: 282207 Color: 3068

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 392751 Color: 7846
Size: 320359 Color: 5197
Size: 286891 Color: 3367

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 392770 Color: 7847
Size: 312347 Color: 4808
Size: 294884 Color: 3862

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 392771 Color: 7848
Size: 323781 Color: 5350
Size: 283449 Color: 3140

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 392845 Color: 7849
Size: 323838 Color: 5354
Size: 283318 Color: 3134

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 392876 Color: 7850
Size: 321667 Color: 5256
Size: 285458 Color: 3269

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 392878 Color: 7851
Size: 314457 Color: 4926
Size: 292666 Color: 3733

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 392886 Color: 7852
Size: 312812 Color: 4838
Size: 294303 Color: 3825

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 392892 Color: 7853
Size: 325385 Color: 5424
Size: 281724 Color: 3033

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 392899 Color: 7854
Size: 311210 Color: 4749
Size: 295892 Color: 3923

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 392933 Color: 7855
Size: 312607 Color: 4829
Size: 294461 Color: 3836

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 392951 Color: 7856
Size: 309707 Color: 4654
Size: 297343 Color: 4000

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 393068 Color: 7857
Size: 318871 Color: 5133
Size: 288062 Color: 3445

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 393086 Color: 7858
Size: 304461 Color: 4378
Size: 302454 Color: 4268

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 393098 Color: 7859
Size: 326450 Color: 5474
Size: 280453 Color: 2957

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 393100 Color: 7860
Size: 310100 Color: 4680
Size: 296801 Color: 3972

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 393196 Color: 7861
Size: 311747 Color: 4774
Size: 295058 Color: 3874

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 393207 Color: 7862
Size: 320804 Color: 5217
Size: 285990 Color: 3312

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 393232 Color: 7863
Size: 326718 Color: 5489
Size: 280051 Color: 2923

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 393273 Color: 7864
Size: 308189 Color: 4572
Size: 298539 Color: 4071

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 393278 Color: 7865
Size: 316717 Color: 5029
Size: 290006 Color: 3562

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 393282 Color: 7866
Size: 310045 Color: 4678
Size: 296674 Color: 3965

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 393294 Color: 7867
Size: 319123 Color: 5140
Size: 287584 Color: 3408

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 393363 Color: 7868
Size: 326427 Color: 5472
Size: 280211 Color: 2938

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 393382 Color: 7869
Size: 311189 Color: 4747
Size: 295430 Color: 3897

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 393385 Color: 7870
Size: 326604 Color: 5484
Size: 280012 Color: 2919

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 393453 Color: 7871
Size: 305760 Color: 4446
Size: 300788 Color: 4174

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 410450 Color: 8302
Size: 337761 Color: 5978
Size: 251790 Color: 320

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 411529 Color: 8335
Size: 318558 Color: 5119
Size: 269914 Color: 2159

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 411539 Color: 8336
Size: 303712 Color: 4330
Size: 284750 Color: 3219

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 411646 Color: 8337
Size: 334124 Color: 5806
Size: 254231 Color: 648

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 411697 Color: 8338
Size: 313974 Color: 4905
Size: 274330 Color: 2503

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 411700 Color: 8339
Size: 323500 Color: 5339
Size: 264801 Color: 1721

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 411713 Color: 8340
Size: 305840 Color: 4452
Size: 282448 Color: 3080

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 411734 Color: 8341
Size: 312559 Color: 4825
Size: 275708 Color: 2615

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 411741 Color: 8342
Size: 325201 Color: 5415
Size: 263059 Color: 1599

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 411771 Color: 8343
Size: 313244 Color: 4861
Size: 274986 Color: 2562

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 411781 Color: 8344
Size: 295024 Color: 3870
Size: 293196 Color: 3765

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 411785 Color: 8345
Size: 294997 Color: 3868
Size: 293219 Color: 3766

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 412047 Color: 8346
Size: 325586 Color: 5440
Size: 262368 Color: 1541

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 412102 Color: 8347
Size: 318283 Color: 5107
Size: 269616 Color: 2129

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 412134 Color: 8348
Size: 332223 Color: 5723
Size: 255644 Color: 816

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 412178 Color: 8349
Size: 294009 Color: 3810
Size: 293814 Color: 3794

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 412182 Color: 8350
Size: 308341 Color: 4580
Size: 279478 Color: 2888

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 412223 Color: 8351
Size: 332931 Color: 5754
Size: 254847 Color: 722

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 412245 Color: 8352
Size: 315936 Color: 4993
Size: 271820 Color: 2318

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 412270 Color: 8353
Size: 315066 Color: 4950
Size: 272665 Color: 2383

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 412270 Color: 8354
Size: 313941 Color: 4900
Size: 273790 Color: 2463

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 412318 Color: 8355
Size: 329308 Color: 5596
Size: 258375 Color: 1148

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 412405 Color: 8356
Size: 332307 Color: 5726
Size: 255289 Color: 771

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 412425 Color: 8357
Size: 321195 Color: 5234
Size: 266381 Color: 1859

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 412436 Color: 8358
Size: 333525 Color: 5778
Size: 254040 Color: 625

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 412447 Color: 8359
Size: 326762 Color: 5492
Size: 260792 Color: 1402

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 412449 Color: 8360
Size: 330835 Color: 5658
Size: 256717 Color: 959

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 412492 Color: 8361
Size: 332696 Color: 5742
Size: 254813 Color: 718

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 412556 Color: 8362
Size: 327312 Color: 5515
Size: 260133 Color: 1330

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 412558 Color: 8363
Size: 323174 Color: 5322
Size: 264269 Color: 1691

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 412632 Color: 8364
Size: 332328 Color: 5727
Size: 255041 Color: 743

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 412659 Color: 8365
Size: 320731 Color: 5210
Size: 266611 Color: 1871

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 412679 Color: 8366
Size: 330457 Color: 5645
Size: 256865 Color: 981

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 412810 Color: 8367
Size: 325518 Color: 5435
Size: 261673 Color: 1484

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 412857 Color: 8368
Size: 331760 Color: 5698
Size: 255384 Color: 781

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 412989 Color: 8369
Size: 298452 Color: 4066
Size: 288560 Color: 3474

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 413052 Color: 8370
Size: 319825 Color: 5170
Size: 267124 Color: 1922

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 413062 Color: 8371
Size: 326683 Color: 5487
Size: 260256 Color: 1344

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 413156 Color: 8372
Size: 307643 Color: 4546
Size: 279202 Color: 2877

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 413165 Color: 8373
Size: 327599 Color: 5526
Size: 259237 Color: 1241

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 413182 Color: 8374
Size: 331928 Color: 5709
Size: 254891 Color: 731

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 413236 Color: 8375
Size: 310606 Color: 4713
Size: 276159 Color: 2651

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 413257 Color: 8376
Size: 294619 Color: 3848
Size: 292125 Color: 3704

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 413296 Color: 8377
Size: 297021 Color: 3984
Size: 289684 Color: 3544

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 413316 Color: 8378
Size: 295971 Color: 3928
Size: 290714 Color: 3599

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 413385 Color: 8379
Size: 316344 Color: 5015
Size: 270272 Color: 2186

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 413390 Color: 8380
Size: 311752 Color: 4775
Size: 274859 Color: 2554

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 413407 Color: 8381
Size: 295556 Color: 3909
Size: 291038 Color: 3630

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 413407 Color: 8382
Size: 299707 Color: 4114
Size: 286887 Color: 3366

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 413443 Color: 8383
Size: 311392 Color: 4755
Size: 275166 Color: 2586

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 413526 Color: 8384
Size: 336271 Color: 5914
Size: 250204 Color: 41

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 413543 Color: 8386
Size: 299460 Color: 4103
Size: 286998 Color: 3373

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 413543 Color: 8385
Size: 322660 Color: 5304
Size: 263798 Color: 1648

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 413551 Color: 8387
Size: 317030 Color: 5047
Size: 269420 Color: 2112

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 413567 Color: 8388
Size: 316203 Color: 5006
Size: 270231 Color: 2182

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 413568 Color: 8389
Size: 332809 Color: 5750
Size: 253624 Color: 568

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 413586 Color: 8390
Size: 308757 Color: 4601
Size: 277658 Color: 2764

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 413594 Color: 8391
Size: 295476 Color: 3901
Size: 290931 Color: 3621

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 413623 Color: 8392
Size: 302002 Color: 4241
Size: 284376 Color: 3197

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 413643 Color: 8393
Size: 299668 Color: 4111
Size: 286690 Color: 3359

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 413662 Color: 8394
Size: 327089 Color: 5505
Size: 259250 Color: 1244

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 413670 Color: 8395
Size: 324828 Color: 5395
Size: 261503 Color: 1467

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 413730 Color: 8396
Size: 323396 Color: 5331
Size: 262875 Color: 1581

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 413853 Color: 8397
Size: 320596 Color: 5207
Size: 265552 Color: 1787

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 413886 Color: 8398
Size: 332125 Color: 5719
Size: 253990 Color: 616

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 413905 Color: 8399
Size: 335346 Color: 5873
Size: 250750 Color: 134

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 413927 Color: 8400
Size: 307423 Color: 4532
Size: 278651 Color: 2837

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 413990 Color: 8401
Size: 316237 Color: 5010
Size: 269774 Color: 2144

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 414033 Color: 8402
Size: 326489 Color: 5476
Size: 259479 Color: 1263

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 414064 Color: 8403
Size: 300265 Color: 4138
Size: 285672 Color: 3288

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 414086 Color: 8404
Size: 326904 Color: 5496
Size: 259011 Color: 1213

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 414093 Color: 8405
Size: 294435 Color: 3832
Size: 291473 Color: 3653

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 414144 Color: 8406
Size: 296184 Color: 3939
Size: 289673 Color: 3542

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 414149 Color: 8407
Size: 313129 Color: 4855
Size: 272723 Color: 2390

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 414169 Color: 8408
Size: 331022 Color: 5665
Size: 254810 Color: 717

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 414352 Color: 8409
Size: 309004 Color: 4610
Size: 276645 Color: 2695

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 414446 Color: 8410
Size: 316237 Color: 5009
Size: 269318 Color: 2103

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 414490 Color: 8411
Size: 310644 Color: 4720
Size: 274867 Color: 2556

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 414509 Color: 8413
Size: 315758 Color: 4974
Size: 269734 Color: 2141

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 414509 Color: 8412
Size: 309583 Color: 4644
Size: 275909 Color: 2630

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 414622 Color: 8414
Size: 299748 Color: 4117
Size: 285631 Color: 3284

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 414741 Color: 8415
Size: 293424 Color: 3775
Size: 291836 Color: 3692

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 414804 Color: 8416
Size: 331454 Color: 5687
Size: 253743 Color: 581

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 414918 Color: 8417
Size: 301051 Color: 4184
Size: 284032 Color: 3175

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 414933 Color: 8418
Size: 296109 Color: 3934
Size: 288959 Color: 3494

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 414981 Color: 8419
Size: 302485 Color: 4273
Size: 282535 Color: 3085

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 415029 Color: 8420
Size: 315774 Color: 4978
Size: 269198 Color: 2094

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 415034 Color: 8421
Size: 294436 Color: 3833
Size: 290531 Color: 3592

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 415048 Color: 8422
Size: 317190 Color: 5057
Size: 267763 Color: 1973

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 415085 Color: 8423
Size: 300679 Color: 4166
Size: 284237 Color: 3188

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 415170 Color: 8424
Size: 292520 Color: 3724
Size: 292311 Color: 3715

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 415212 Color: 8425
Size: 326869 Color: 5495
Size: 257920 Color: 1103

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 415218 Color: 8426
Size: 310449 Color: 4704
Size: 274334 Color: 2504

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 415231 Color: 8427
Size: 325158 Color: 5411
Size: 259612 Color: 1278

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 415303 Color: 8428
Size: 295038 Color: 3872
Size: 289660 Color: 3541

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 415321 Color: 8429
Size: 314497 Color: 4928
Size: 270183 Color: 2176

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 415423 Color: 8430
Size: 312543 Color: 4822
Size: 272035 Color: 2337

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 415452 Color: 8431
Size: 331513 Color: 5690
Size: 253036 Color: 492

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 415591 Color: 8432
Size: 295707 Color: 3917
Size: 288703 Color: 3479

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 415698 Color: 8433
Size: 305901 Color: 4457
Size: 278402 Color: 2817

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 415710 Color: 8434
Size: 330117 Color: 5633
Size: 254174 Color: 640

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 415777 Color: 8435
Size: 303277 Color: 4311
Size: 280947 Color: 2985

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 415819 Color: 8436
Size: 322840 Color: 5310
Size: 261342 Color: 1452

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 415923 Color: 8437
Size: 324890 Color: 5399
Size: 259188 Color: 1235

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 415932 Color: 8438
Size: 330993 Color: 5664
Size: 253076 Color: 502

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 415995 Color: 8439
Size: 330850 Color: 5659
Size: 253156 Color: 516

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 415997 Color: 8440
Size: 333966 Color: 5798
Size: 250038 Color: 8

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 416142 Color: 8441
Size: 314227 Color: 4916
Size: 269632 Color: 2132

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 416160 Color: 8442
Size: 302503 Color: 4274
Size: 281338 Color: 3006

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 416192 Color: 8443
Size: 311718 Color: 4770
Size: 272091 Color: 2343

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 416226 Color: 8444
Size: 313770 Color: 4890
Size: 270005 Color: 2167

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 416267 Color: 8445
Size: 328909 Color: 5575
Size: 254825 Color: 720

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 416280 Color: 8446
Size: 292028 Color: 3700
Size: 291693 Color: 3670

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 416337 Color: 8447
Size: 298348 Color: 4057
Size: 285316 Color: 3258

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 416370 Color: 8448
Size: 325638 Color: 5444
Size: 257993 Color: 1108

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 416393 Color: 8449
Size: 307053 Color: 4512
Size: 276555 Color: 2687

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 416495 Color: 8450
Size: 311632 Color: 4764
Size: 271874 Color: 2321

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 416531 Color: 8451
Size: 301142 Color: 4193
Size: 282328 Color: 3075

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 416538 Color: 8452
Size: 315393 Color: 4959
Size: 268070 Color: 1997

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 416591 Color: 8453
Size: 307584 Color: 4539
Size: 275826 Color: 2624

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 416633 Color: 8454
Size: 324518 Color: 5380
Size: 258850 Color: 1197

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 416642 Color: 8455
Size: 291807 Color: 3689
Size: 291552 Color: 3658

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 416712 Color: 8456
Size: 327102 Color: 5506
Size: 256187 Color: 886

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 416750 Color: 8457
Size: 326763 Color: 5493
Size: 256488 Color: 936

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 416828 Color: 8458
Size: 317268 Color: 5061
Size: 265905 Color: 1819

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 416902 Color: 8459
Size: 305008 Color: 4415
Size: 278091 Color: 2794

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 416939 Color: 8460
Size: 310514 Color: 4706
Size: 272548 Color: 2375

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 416979 Color: 8461
Size: 330041 Color: 5629
Size: 252981 Color: 486

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 417101 Color: 8462
Size: 297467 Color: 4009
Size: 285433 Color: 3267

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 417174 Color: 8463
Size: 327111 Color: 5507
Size: 255716 Color: 828

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 417213 Color: 8464
Size: 323421 Color: 5335
Size: 259367 Color: 1252

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 417326 Color: 8465
Size: 310364 Color: 4696
Size: 272311 Color: 2358

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 417341 Color: 8466
Size: 304430 Color: 4377
Size: 278230 Color: 2802

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 417378 Color: 8467
Size: 295543 Color: 3908
Size: 287080 Color: 3378

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 417389 Color: 8468
Size: 296368 Color: 3947
Size: 286244 Color: 3329

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 417406 Color: 8469
Size: 300637 Color: 4162
Size: 281958 Color: 3052

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 417470 Color: 8470
Size: 321559 Color: 5252
Size: 260972 Color: 1419

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 417516 Color: 8471
Size: 315776 Color: 4979
Size: 266709 Color: 1882

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 417636 Color: 8472
Size: 309868 Color: 4667
Size: 272497 Color: 2370

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 417646 Color: 8473
Size: 291567 Color: 3661
Size: 290788 Color: 3608

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 417651 Color: 8474
Size: 294607 Color: 3847
Size: 287743 Color: 3420

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 417707 Color: 8475
Size: 293528 Color: 3782
Size: 288766 Color: 3482

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 417719 Color: 8476
Size: 318021 Color: 5094
Size: 264261 Color: 1690

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 417733 Color: 8477
Size: 294488 Color: 3838
Size: 287780 Color: 3421

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 417904 Color: 8478
Size: 311683 Color: 4767
Size: 270414 Color: 2198

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 417991 Color: 8479
Size: 331372 Color: 5682
Size: 250638 Color: 116

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 418052 Color: 8480
Size: 316823 Color: 5032
Size: 265126 Color: 1756

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 418078 Color: 8481
Size: 295498 Color: 3903
Size: 286425 Color: 3344

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 418168 Color: 8482
Size: 296522 Color: 3955
Size: 285311 Color: 3256

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 418221 Color: 8483
Size: 306918 Color: 4506
Size: 274862 Color: 2555

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 418239 Color: 8484
Size: 311511 Color: 4762
Size: 270251 Color: 2184

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 418316 Color: 8485
Size: 319568 Color: 5160
Size: 262117 Color: 1518

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 418379 Color: 8486
Size: 298499 Color: 4068
Size: 283123 Color: 3120

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 418394 Color: 8487
Size: 301979 Color: 4240
Size: 279628 Color: 2894

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 418444 Color: 8488
Size: 328068 Color: 5540
Size: 253489 Color: 554

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 418474 Color: 8489
Size: 305229 Color: 4425
Size: 276298 Color: 2667

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 418476 Color: 8490
Size: 316815 Color: 5031
Size: 264710 Color: 1715

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 418554 Color: 8491
Size: 310123 Color: 4683
Size: 271324 Color: 2276

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 418609 Color: 8492
Size: 325085 Color: 5405
Size: 256307 Color: 909

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 418776 Color: 8493
Size: 328710 Color: 5570
Size: 252515 Color: 424

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 418858 Color: 8494
Size: 304141 Color: 4358
Size: 277002 Color: 2719

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 418907 Color: 8495
Size: 302935 Color: 4298
Size: 278159 Color: 2799

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 418913 Color: 8496
Size: 325421 Color: 5428
Size: 255667 Color: 819

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 419014 Color: 8497
Size: 310106 Color: 4681
Size: 270881 Color: 2237

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 419038 Color: 8498
Size: 318981 Color: 5135
Size: 261982 Color: 1506

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 419123 Color: 8499
Size: 312825 Color: 4839
Size: 268053 Color: 1996

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 419152 Color: 8500
Size: 304366 Color: 4373
Size: 276483 Color: 2682

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 419171 Color: 8501
Size: 309729 Color: 4658
Size: 271101 Color: 2257

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 419179 Color: 8502
Size: 302094 Color: 4248
Size: 278728 Color: 2845

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 419200 Color: 8503
Size: 306870 Color: 4505
Size: 273931 Color: 2484

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 419243 Color: 8504
Size: 326599 Color: 5483
Size: 254159 Color: 636

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 419246 Color: 8505
Size: 308230 Color: 4575
Size: 272525 Color: 2373

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 419353 Color: 8506
Size: 292689 Color: 3735
Size: 287959 Color: 3437

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 419430 Color: 8507
Size: 314920 Color: 4943
Size: 265651 Color: 1797

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 419451 Color: 8508
Size: 300537 Color: 4161
Size: 280013 Color: 2922

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 419563 Color: 8510
Size: 291684 Color: 3669
Size: 288754 Color: 3481

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 419563 Color: 8509
Size: 312736 Color: 4834
Size: 267702 Color: 1967

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 419572 Color: 8511
Size: 327766 Color: 5531
Size: 252663 Color: 445

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 419579 Color: 8512
Size: 301902 Color: 4238
Size: 278520 Color: 2828

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 419588 Color: 8513
Size: 291538 Color: 3657
Size: 288875 Color: 3488

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 419589 Color: 8514
Size: 325666 Color: 5445
Size: 254746 Color: 704

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 419638 Color: 8515
Size: 328308 Color: 5554
Size: 252055 Color: 359

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 419674 Color: 8516
Size: 304600 Color: 4387
Size: 275727 Color: 2618

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 419720 Color: 8517
Size: 302720 Color: 4286
Size: 277561 Color: 2756

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 419796 Color: 8518
Size: 317938 Color: 5090
Size: 262267 Color: 1530

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 419800 Color: 8519
Size: 298219 Color: 4049
Size: 281982 Color: 3053

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 419829 Color: 8520
Size: 323849 Color: 5355
Size: 256323 Color: 912

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 419854 Color: 8521
Size: 314759 Color: 4934
Size: 265388 Color: 1776

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 419857 Color: 8522
Size: 303909 Color: 4341
Size: 276235 Color: 2662

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 419865 Color: 8523
Size: 308136 Color: 4569
Size: 272000 Color: 2333

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 419873 Color: 8524
Size: 324948 Color: 5401
Size: 255180 Color: 757

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 419877 Color: 8525
Size: 309176 Color: 4621
Size: 270948 Color: 2243

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 419907 Color: 8526
Size: 309081 Color: 4616
Size: 271013 Color: 2247

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 419973 Color: 8527
Size: 306840 Color: 4500
Size: 273188 Color: 2422

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 419987 Color: 8528
Size: 325770 Color: 5451
Size: 254244 Color: 650

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 420005 Color: 8529
Size: 304980 Color: 4414
Size: 275016 Color: 2567

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 420051 Color: 8530
Size: 319122 Color: 5139
Size: 260828 Color: 1406

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 420126 Color: 8531
Size: 310786 Color: 4726
Size: 269089 Color: 2084

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 420248 Color: 8532
Size: 305059 Color: 4418
Size: 274694 Color: 2538

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 420271 Color: 8533
Size: 290050 Color: 3565
Size: 289680 Color: 3543

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 420291 Color: 8534
Size: 292613 Color: 3730
Size: 287097 Color: 3380

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 420319 Color: 8535
Size: 320181 Color: 5189
Size: 259501 Color: 1264

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 420348 Color: 8536
Size: 310795 Color: 4727
Size: 268858 Color: 2071

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 420412 Color: 8537
Size: 298097 Color: 4040
Size: 281492 Color: 3014

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 420469 Color: 8538
Size: 298328 Color: 4056
Size: 281204 Color: 2996

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 420525 Color: 8539
Size: 311887 Color: 4782
Size: 267589 Color: 1956

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 420603 Color: 8540
Size: 326553 Color: 5481
Size: 252845 Color: 465

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 420660 Color: 8541
Size: 301323 Color: 4205
Size: 278018 Color: 2789

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 420791 Color: 8542
Size: 297681 Color: 4021
Size: 281529 Color: 3017

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 420796 Color: 8543
Size: 312672 Color: 4832
Size: 266533 Color: 1867

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 420824 Color: 8544
Size: 291697 Color: 3671
Size: 287480 Color: 3402

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 420951 Color: 8545
Size: 322781 Color: 5308
Size: 256269 Color: 899

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 420962 Color: 8546
Size: 308013 Color: 4563
Size: 271026 Color: 2249

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 421016 Color: 8547
Size: 319556 Color: 5159
Size: 259429 Color: 1260

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 421100 Color: 8548
Size: 327211 Color: 5512
Size: 251690 Color: 299

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 421133 Color: 8549
Size: 320069 Color: 5183
Size: 258799 Color: 1191

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 421204 Color: 8550
Size: 302640 Color: 4282
Size: 276157 Color: 2650

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 421332 Color: 8551
Size: 311727 Color: 4772
Size: 266942 Color: 1908

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 421436 Color: 8552
Size: 315898 Color: 4990
Size: 262667 Color: 1564

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 421465 Color: 8553
Size: 314372 Color: 4924
Size: 264164 Color: 1679

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 421509 Color: 8554
Size: 289424 Color: 3530
Size: 289068 Color: 3503

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 421536 Color: 8555
Size: 292597 Color: 3728
Size: 285868 Color: 3298

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 421546 Color: 8556
Size: 326320 Color: 5467
Size: 252135 Color: 369

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 421598 Color: 8557
Size: 298419 Color: 4062
Size: 279984 Color: 2916

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 421656 Color: 8558
Size: 293164 Color: 3761
Size: 285181 Color: 3247

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 421716 Color: 8559
Size: 305146 Color: 4423
Size: 273139 Color: 2418

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 421766 Color: 8560
Size: 293428 Color: 3777
Size: 284807 Color: 3221

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 421815 Color: 8561
Size: 306827 Color: 4499
Size: 271359 Color: 2278

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 421885 Color: 8562
Size: 315647 Color: 4968
Size: 262469 Color: 1549

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 421910 Color: 8563
Size: 290500 Color: 3590
Size: 287591 Color: 3410

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 421914 Color: 8564
Size: 289126 Color: 3509
Size: 288961 Color: 3495

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 421994 Color: 8565
Size: 310945 Color: 4731
Size: 267062 Color: 1917

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 422036 Color: 8566
Size: 325883 Color: 5455
Size: 252082 Color: 364

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 422149 Color: 8567
Size: 320393 Color: 5198
Size: 257459 Color: 1048

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 422232 Color: 8568
Size: 314503 Color: 4929
Size: 263266 Color: 1611

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 422239 Color: 8569
Size: 306000 Color: 4462
Size: 271762 Color: 2315

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 422362 Color: 8570
Size: 304387 Color: 4374
Size: 273252 Color: 2428

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 422691 Color: 8571
Size: 302225 Color: 4256
Size: 275085 Color: 2579

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 422769 Color: 8572
Size: 316443 Color: 5019
Size: 260789 Color: 1401

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 422913 Color: 8573
Size: 292601 Color: 3729
Size: 284487 Color: 3202

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 422939 Color: 8574
Size: 320791 Color: 5215
Size: 256271 Color: 900

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 422940 Color: 8575
Size: 306562 Color: 4484
Size: 270499 Color: 2206

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 423030 Color: 8576
Size: 291491 Color: 3654
Size: 285480 Color: 3272

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 423192 Color: 8577
Size: 320910 Color: 5223
Size: 255899 Color: 851

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 423262 Color: 8578
Size: 315868 Color: 4989
Size: 260871 Color: 1409

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 423356 Color: 8579
Size: 308333 Color: 4579
Size: 268312 Color: 2027

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 423478 Color: 8580
Size: 319430 Color: 5155
Size: 257093 Color: 1007

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 423536 Color: 8581
Size: 292139 Color: 3705
Size: 284326 Color: 3193

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 423563 Color: 8582
Size: 315839 Color: 4984
Size: 260599 Color: 1382

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 423604 Color: 8583
Size: 303676 Color: 4329
Size: 272721 Color: 2388

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 423637 Color: 8584
Size: 312083 Color: 4794
Size: 264281 Color: 1694

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 423665 Color: 8585
Size: 325865 Color: 5454
Size: 250471 Color: 79

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 423673 Color: 8586
Size: 311110 Color: 4740
Size: 265218 Color: 1760

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 423687 Color: 8587
Size: 322055 Color: 5280
Size: 254259 Color: 654

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 423692 Color: 8588
Size: 293830 Color: 3801
Size: 282479 Color: 3081

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 423701 Color: 8589
Size: 289825 Color: 3553
Size: 286475 Color: 3349

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 423760 Color: 8590
Size: 294976 Color: 3866
Size: 281265 Color: 3001

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 423794 Color: 8591
Size: 321590 Color: 5253
Size: 254617 Color: 694

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 423802 Color: 8592
Size: 309222 Color: 4624
Size: 266977 Color: 1911

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 423805 Color: 8593
Size: 301755 Color: 4226
Size: 274441 Color: 2519

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 423812 Color: 8594
Size: 308250 Color: 4576
Size: 267939 Color: 1985

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 423848 Color: 8595
Size: 306598 Color: 4487
Size: 269555 Color: 2126

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 423901 Color: 8596
Size: 325615 Color: 5442
Size: 250485 Color: 82

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 423946 Color: 8597
Size: 318039 Color: 5095
Size: 258016 Color: 1111

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 423957 Color: 8598
Size: 291721 Color: 3676
Size: 284323 Color: 3192

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 423960 Color: 8599
Size: 319892 Color: 5173
Size: 256149 Color: 881

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 424012 Color: 8600
Size: 302331 Color: 4260
Size: 273658 Color: 2455

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 424029 Color: 8601
Size: 312539 Color: 4821
Size: 263433 Color: 1625

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 424045 Color: 8602
Size: 299779 Color: 4118
Size: 276177 Color: 2654

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 424106 Color: 8603
Size: 303037 Color: 4302
Size: 272858 Color: 2397

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 424113 Color: 8604
Size: 307628 Color: 4544
Size: 268260 Color: 2023

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 424159 Color: 8605
Size: 299250 Color: 4098
Size: 276592 Color: 2691

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 424160 Color: 8606
Size: 291098 Color: 3634
Size: 284743 Color: 3218

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 424175 Color: 8607
Size: 309120 Color: 4618
Size: 266706 Color: 1880

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 424220 Color: 8608
Size: 309776 Color: 4660
Size: 266005 Color: 1829

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 424224 Color: 8609
Size: 290389 Color: 3584
Size: 285388 Color: 3264

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 424238 Color: 8610
Size: 297867 Color: 4029
Size: 277896 Color: 2778

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 424271 Color: 8611
Size: 317012 Color: 5044
Size: 258718 Color: 1180

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 424274 Color: 8612
Size: 320576 Color: 5205
Size: 255151 Color: 752

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 424294 Color: 8613
Size: 304203 Color: 4360
Size: 271504 Color: 2291

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 424357 Color: 8614
Size: 320968 Color: 5225
Size: 254676 Color: 698

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 424387 Color: 8615
Size: 320165 Color: 5188
Size: 255449 Color: 790

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 424400 Color: 8616
Size: 297002 Color: 3982
Size: 278599 Color: 2834

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 424497 Color: 8617
Size: 320002 Color: 5179
Size: 255502 Color: 797

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 424648 Color: 8618
Size: 319684 Color: 5166
Size: 255669 Color: 820

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 424670 Color: 8619
Size: 316205 Color: 5007
Size: 259126 Color: 1226

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 424743 Color: 8620
Size: 314292 Color: 4919
Size: 260966 Color: 1418

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 424793 Color: 8621
Size: 318656 Color: 5122
Size: 256552 Color: 939

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 424822 Color: 8622
Size: 300295 Color: 4142
Size: 274884 Color: 2557

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 424863 Color: 8623
Size: 311700 Color: 4769
Size: 263438 Color: 1626

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 424906 Color: 8624
Size: 321657 Color: 5255
Size: 253438 Color: 548

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 424911 Color: 8625
Size: 322053 Color: 5279
Size: 253037 Color: 493

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 424940 Color: 8626
Size: 311916 Color: 4785
Size: 263145 Color: 1605

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 425037 Color: 8627
Size: 307639 Color: 4545
Size: 267325 Color: 1934

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 425070 Color: 8628
Size: 288947 Color: 3492
Size: 285984 Color: 3311

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 425087 Color: 8629
Size: 294558 Color: 3844
Size: 280356 Color: 2946

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 425098 Color: 8630
Size: 319580 Color: 5161
Size: 255323 Color: 775

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 425156 Color: 8631
Size: 291739 Color: 3680
Size: 283106 Color: 3117

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 425272 Color: 8632
Size: 314199 Color: 4914
Size: 260530 Color: 1380

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 425302 Color: 8633
Size: 289171 Color: 3515
Size: 285528 Color: 3276

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 425326 Color: 8634
Size: 293251 Color: 3770
Size: 281424 Color: 3013

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 425328 Color: 8635
Size: 289614 Color: 3539
Size: 285059 Color: 3237

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 425335 Color: 8636
Size: 300062 Color: 4131
Size: 274604 Color: 2533

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 425340 Color: 8637
Size: 307527 Color: 4537
Size: 267134 Color: 1923

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 425365 Color: 8638
Size: 320738 Color: 5211
Size: 253898 Color: 601

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 425453 Color: 8639
Size: 294407 Color: 3828
Size: 280141 Color: 2930

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 425536 Color: 8640
Size: 317483 Color: 5071
Size: 256982 Color: 995

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 425543 Color: 8641
Size: 303976 Color: 4345
Size: 270482 Color: 2203

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 425596 Color: 8642
Size: 298686 Color: 4079
Size: 275719 Color: 2617

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 425647 Color: 8643
Size: 307326 Color: 4525
Size: 267028 Color: 1913

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 425730 Color: 8644
Size: 309390 Color: 4633
Size: 264881 Color: 1726

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 425792 Color: 8645
Size: 291636 Color: 3667
Size: 282573 Color: 3088

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 425803 Color: 8646
Size: 322062 Color: 5281
Size: 252136 Color: 370

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 425804 Color: 8647
Size: 308545 Color: 4586
Size: 265652 Color: 1798

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 425841 Color: 8648
Size: 309104 Color: 4617
Size: 265056 Color: 1747

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 425851 Color: 8649
Size: 290383 Color: 3583
Size: 283767 Color: 3154

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 425869 Color: 8650
Size: 304279 Color: 4365
Size: 269853 Color: 2154

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 425873 Color: 8651
Size: 300244 Color: 4136
Size: 273884 Color: 2476

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 425883 Color: 8652
Size: 322628 Color: 5303
Size: 251490 Color: 263

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 425960 Color: 8653
Size: 323217 Color: 5326
Size: 250824 Color: 154

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 426155 Color: 8654
Size: 298269 Color: 4054
Size: 275577 Color: 2606

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 426195 Color: 8655
Size: 318653 Color: 5121
Size: 255153 Color: 753

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 426212 Color: 8656
Size: 301525 Color: 4213
Size: 272264 Color: 2355

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 426275 Color: 8657
Size: 322607 Color: 5302
Size: 251119 Color: 207

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 426310 Color: 8658
Size: 313606 Color: 4878
Size: 260085 Color: 1324

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 426370 Color: 8660
Size: 310617 Color: 4716
Size: 263014 Color: 1594

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 426370 Color: 8659
Size: 317308 Color: 5063
Size: 256323 Color: 913

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 426397 Color: 8661
Size: 294769 Color: 3859
Size: 278835 Color: 2852

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 426433 Color: 8662
Size: 323120 Color: 5319
Size: 250448 Color: 76

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 426439 Color: 8663
Size: 312575 Color: 4827
Size: 260987 Color: 1422

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 426464 Color: 8664
Size: 322749 Color: 5307
Size: 250788 Color: 144

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 426492 Color: 8665
Size: 317608 Color: 5078
Size: 255901 Color: 852

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 426536 Color: 8666
Size: 290419 Color: 3587
Size: 283046 Color: 3116

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 426608 Color: 8667
Size: 290600 Color: 3593
Size: 282793 Color: 3101

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 426640 Color: 8668
Size: 321624 Color: 5254
Size: 251737 Color: 311

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 426712 Color: 8669
Size: 312052 Color: 4791
Size: 261237 Color: 1441

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 426785 Color: 8670
Size: 298667 Color: 4077
Size: 274549 Color: 2530

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 426865 Color: 8671
Size: 295522 Color: 3906
Size: 277614 Color: 2759

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 426944 Color: 8672
Size: 295403 Color: 3894
Size: 277654 Color: 2762

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 426981 Color: 8673
Size: 287855 Color: 3431
Size: 285165 Color: 3244

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 427019 Color: 8674
Size: 311721 Color: 4771
Size: 261261 Color: 1445

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 427053 Color: 8675
Size: 301502 Color: 4212
Size: 271446 Color: 2283

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 427083 Color: 8676
Size: 310188 Color: 4686
Size: 262730 Color: 1567

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 427098 Color: 8677
Size: 297303 Color: 3999
Size: 275600 Color: 2609

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 427104 Color: 8678
Size: 302739 Color: 4288
Size: 270158 Color: 2175

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 427113 Color: 8679
Size: 321699 Color: 5257
Size: 251189 Color: 215

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 427120 Color: 8680
Size: 311863 Color: 4780
Size: 261018 Color: 1426

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 427182 Color: 8681
Size: 306167 Color: 4468
Size: 266652 Color: 1876

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 427218 Color: 8682
Size: 318409 Color: 5113
Size: 254374 Color: 666

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 427309 Color: 8683
Size: 311889 Color: 4783
Size: 260803 Color: 1403

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 427321 Color: 8684
Size: 294703 Color: 3852
Size: 277977 Color: 2784

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 427448 Color: 8685
Size: 287911 Color: 3434
Size: 284642 Color: 3213

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 427543 Color: 8686
Size: 301709 Color: 4220
Size: 270749 Color: 2226

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 427549 Color: 8687
Size: 293676 Color: 3786
Size: 278776 Color: 2847

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 427599 Color: 8688
Size: 288509 Color: 3472
Size: 283893 Color: 3161

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 427630 Color: 8689
Size: 311407 Color: 4756
Size: 260964 Color: 1417

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 427639 Color: 8690
Size: 297763 Color: 4023
Size: 274599 Color: 2532

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 427655 Color: 8691
Size: 313615 Color: 4880
Size: 258731 Color: 1183

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 427706 Color: 8692
Size: 317444 Color: 5069
Size: 254851 Color: 724

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 427741 Color: 8693
Size: 298568 Color: 4073
Size: 273692 Color: 2457

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 427891 Color: 8694
Size: 306534 Color: 4482
Size: 265576 Color: 1790

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 427922 Color: 8695
Size: 286147 Color: 3324
Size: 285932 Color: 3305

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 427923 Color: 8696
Size: 319856 Color: 5171
Size: 252222 Color: 378

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 427925 Color: 8697
Size: 290308 Color: 3575
Size: 281768 Color: 3040

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 427950 Color: 8698
Size: 286437 Color: 3346
Size: 285614 Color: 3283

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 427974 Color: 8699
Size: 300294 Color: 4141
Size: 271733 Color: 2311

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 428011 Color: 8700
Size: 317362 Color: 5064
Size: 254628 Color: 695

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 428025 Color: 8701
Size: 301435 Color: 4210
Size: 270541 Color: 2211

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 428031 Color: 8702
Size: 286031 Color: 3317
Size: 285939 Color: 3306

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 428094 Color: 8703
Size: 314183 Color: 4913
Size: 257724 Color: 1078

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 428115 Color: 8704
Size: 295456 Color: 3899
Size: 276430 Color: 2676

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 428116 Color: 8705
Size: 317134 Color: 5052
Size: 254751 Color: 705

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 428277 Color: 8706
Size: 317665 Color: 5081
Size: 254059 Color: 626

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 428328 Color: 8707
Size: 294188 Color: 3820
Size: 277485 Color: 2752

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 428483 Color: 8708
Size: 307279 Color: 4521
Size: 264239 Color: 1687

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 428524 Color: 8709
Size: 304932 Color: 4410
Size: 266545 Color: 1869

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 428715 Color: 8710
Size: 298709 Color: 4080
Size: 272577 Color: 2379

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 428766 Color: 8711
Size: 303666 Color: 4328
Size: 267569 Color: 1952

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 428863 Color: 8712
Size: 315720 Color: 4971
Size: 255418 Color: 786

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 428865 Color: 8713
Size: 303165 Color: 4306
Size: 267971 Color: 1990

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 428871 Color: 8714
Size: 304815 Color: 4400
Size: 266315 Color: 1850

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 428922 Color: 8715
Size: 310679 Color: 4721
Size: 260400 Color: 1368

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 428983 Color: 8716
Size: 292748 Color: 3738
Size: 278270 Color: 2804

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 428986 Color: 8717
Size: 306023 Color: 4463
Size: 264992 Color: 1739

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 429075 Color: 8718
Size: 310555 Color: 4709
Size: 260371 Color: 1364

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 429137 Color: 8719
Size: 301828 Color: 4234
Size: 269036 Color: 2080

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 429171 Color: 8720
Size: 289146 Color: 3512
Size: 281684 Color: 3031

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 429299 Color: 8721
Size: 291922 Color: 3695
Size: 278780 Color: 2849

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 429377 Color: 8722
Size: 288947 Color: 3493
Size: 281677 Color: 3029

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 429383 Color: 8723
Size: 315218 Color: 4955
Size: 255400 Color: 783

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 429387 Color: 8724
Size: 310307 Color: 4691
Size: 260307 Color: 1354

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 429400 Color: 8725
Size: 304203 Color: 4361
Size: 266398 Color: 1861

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 429403 Color: 8726
Size: 301497 Color: 4211
Size: 269101 Color: 2086

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 429404 Color: 8727
Size: 296730 Color: 3967
Size: 273867 Color: 2472

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 429476 Color: 8728
Size: 286026 Color: 3316
Size: 284499 Color: 3203

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 429559 Color: 8729
Size: 309046 Color: 4614
Size: 261396 Color: 1457

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 429610 Color: 8730
Size: 298039 Color: 4039
Size: 272352 Color: 2360

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 429660 Color: 8731
Size: 313552 Color: 4876
Size: 256789 Color: 970

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 429765 Color: 8732
Size: 307254 Color: 4520
Size: 262982 Color: 1592

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 429768 Color: 8733
Size: 292038 Color: 3701
Size: 278195 Color: 2800

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 429796 Color: 8734
Size: 286116 Color: 3322
Size: 284089 Color: 3182

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 429827 Color: 8735
Size: 301365 Color: 4207
Size: 268809 Color: 2066

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 429854 Color: 8736
Size: 309321 Color: 4629
Size: 260826 Color: 1405

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 429938 Color: 8737
Size: 297608 Color: 4015
Size: 272455 Color: 2367

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 430019 Color: 8738
Size: 318664 Color: 5124
Size: 251318 Color: 238

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 430021 Color: 8739
Size: 319519 Color: 5157
Size: 250461 Color: 78

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 430122 Color: 8740
Size: 318404 Color: 5112
Size: 251475 Color: 262

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 430229 Color: 8741
Size: 295319 Color: 3889
Size: 274453 Color: 2522

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 430255 Color: 8742
Size: 300892 Color: 4177
Size: 268854 Color: 2069

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 430312 Color: 8743
Size: 289471 Color: 3533
Size: 280218 Color: 2939

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 430410 Color: 8744
Size: 289176 Color: 3516
Size: 280415 Color: 2953

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 430414 Color: 8745
Size: 304010 Color: 4351
Size: 265577 Color: 1792

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 430482 Color: 8746
Size: 291558 Color: 3660
Size: 277961 Color: 2782

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 430494 Color: 8747
Size: 314310 Color: 4920
Size: 255197 Color: 763

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 430514 Color: 8748
Size: 285123 Color: 3242
Size: 284364 Color: 3195

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 430669 Color: 8749
Size: 319068 Color: 5137
Size: 250264 Color: 50

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 430752 Color: 8750
Size: 312957 Color: 4846
Size: 256292 Color: 904

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 430756 Color: 8751
Size: 312952 Color: 4845
Size: 256293 Color: 906

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 430877 Color: 8752
Size: 312458 Color: 4817
Size: 256666 Color: 952

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 430882 Color: 8753
Size: 288181 Color: 3454
Size: 280938 Color: 2984

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 430886 Color: 8754
Size: 285638 Color: 3286
Size: 283477 Color: 3142

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 430976 Color: 8755
Size: 315704 Color: 4970
Size: 253321 Color: 530

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 431062 Color: 8756
Size: 292767 Color: 3739
Size: 276172 Color: 2653

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 431063 Color: 8757
Size: 297411 Color: 4004
Size: 271527 Color: 2293

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 431100 Color: 8758
Size: 292944 Color: 3750
Size: 275957 Color: 2634

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 431201 Color: 8759
Size: 306407 Color: 4478
Size: 262393 Color: 1542

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 431237 Color: 8760
Size: 298380 Color: 4061
Size: 270384 Color: 2195

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 431248 Color: 8761
Size: 312487 Color: 4819
Size: 256266 Color: 898

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 431400 Color: 8762
Size: 300755 Color: 4169
Size: 267846 Color: 1978

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 431419 Color: 8763
Size: 301086 Color: 4189
Size: 267496 Color: 1950

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 431446 Color: 8764
Size: 309379 Color: 4632
Size: 259176 Color: 1234

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 431454 Color: 8765
Size: 302478 Color: 4272
Size: 266069 Color: 1839

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 431490 Color: 8766
Size: 313042 Color: 4851
Size: 255469 Color: 794

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 431547 Color: 8767
Size: 303399 Color: 4316
Size: 265055 Color: 1746

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 431575 Color: 8768
Size: 290338 Color: 3579
Size: 278088 Color: 2793

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 431597 Color: 8769
Size: 300425 Color: 4155
Size: 267979 Color: 1991

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 431653 Color: 8770
Size: 288335 Color: 3465
Size: 280013 Color: 2921

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 431782 Color: 8771
Size: 285501 Color: 3273
Size: 282718 Color: 3098

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 431788 Color: 8772
Size: 314829 Color: 4939
Size: 253384 Color: 538

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 431880 Color: 8773
Size: 298632 Color: 4075
Size: 269489 Color: 2119

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 431908 Color: 8774
Size: 308078 Color: 4565
Size: 260015 Color: 1313

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 431944 Color: 8775
Size: 309328 Color: 4630
Size: 258729 Color: 1182

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 431961 Color: 8776
Size: 284310 Color: 3190
Size: 283730 Color: 3153

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 431971 Color: 8777
Size: 315800 Color: 4980
Size: 252230 Color: 381

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 431993 Color: 8778
Size: 304566 Color: 4384
Size: 263442 Color: 1628

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 432024 Color: 8779
Size: 315046 Color: 4949
Size: 252931 Color: 479

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 432064 Color: 8780
Size: 317068 Color: 5049
Size: 250869 Color: 161

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 432130 Color: 8781
Size: 314955 Color: 4945
Size: 252916 Color: 474

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 432160 Color: 8782
Size: 309900 Color: 4671
Size: 257941 Color: 1105

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 432164 Color: 8783
Size: 312194 Color: 4801
Size: 255643 Color: 815

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 432405 Color: 8784
Size: 304620 Color: 4390
Size: 262976 Color: 1590

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 432436 Color: 8785
Size: 303726 Color: 4332
Size: 263839 Color: 1654

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 432463 Color: 8786
Size: 305890 Color: 4455
Size: 261648 Color: 1481

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 432610 Color: 8787
Size: 287030 Color: 3376
Size: 280361 Color: 2947

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 432674 Color: 8788
Size: 304511 Color: 4381
Size: 262816 Color: 1574

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 432722 Color: 8789
Size: 315574 Color: 4966
Size: 251705 Color: 302

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 432764 Color: 8790
Size: 303843 Color: 4336
Size: 263394 Color: 1620

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 432770 Color: 8791
Size: 312829 Color: 4841
Size: 254402 Color: 670

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 432910 Color: 8792
Size: 301972 Color: 4239
Size: 265119 Color: 1753

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 432920 Color: 8793
Size: 300296 Color: 4143
Size: 266785 Color: 1889

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 432983 Color: 8794
Size: 287452 Color: 3400
Size: 279566 Color: 2893

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 433010 Color: 8795
Size: 290634 Color: 3595
Size: 276357 Color: 2672

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 433016 Color: 8796
Size: 312694 Color: 4833
Size: 254291 Color: 655

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 433068 Color: 8797
Size: 293925 Color: 3806
Size: 273008 Color: 2407

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 433104 Color: 8798
Size: 304490 Color: 4379
Size: 262407 Color: 1543

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 433171 Color: 8799
Size: 306988 Color: 4509
Size: 259842 Color: 1299

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 433196 Color: 8800
Size: 297262 Color: 3997
Size: 269543 Color: 2124

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 433215 Color: 8801
Size: 285189 Color: 3248
Size: 281597 Color: 3023

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 433249 Color: 8802
Size: 291069 Color: 3633
Size: 275683 Color: 2614

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 433320 Color: 8803
Size: 297228 Color: 3995
Size: 269453 Color: 2115

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 433342 Color: 8804
Size: 287615 Color: 3412
Size: 279044 Color: 2861

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 433446 Color: 8805
Size: 307699 Color: 4549
Size: 258856 Color: 1199

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 433463 Color: 8806
Size: 291777 Color: 3684
Size: 274761 Color: 2546

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 433548 Color: 8807
Size: 299011 Color: 4088
Size: 267442 Color: 1946

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 433550 Color: 8808
Size: 315697 Color: 4969
Size: 250754 Color: 137

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 433588 Color: 8809
Size: 291719 Color: 3675
Size: 274694 Color: 2537

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 433612 Color: 8810
Size: 291195 Color: 3640
Size: 275194 Color: 2588

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 433627 Color: 8811
Size: 295253 Color: 3886
Size: 271121 Color: 2259

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 433655 Color: 8812
Size: 291327 Color: 3645
Size: 275019 Color: 2568

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 433737 Color: 8813
Size: 288333 Color: 3463
Size: 277931 Color: 2780

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 433781 Color: 8814
Size: 300504 Color: 4160
Size: 265716 Color: 1801

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 433813 Color: 8815
Size: 283890 Color: 3160
Size: 282298 Color: 3074

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 433820 Color: 8816
Size: 297373 Color: 4001
Size: 268808 Color: 2064

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 433832 Color: 8817
Size: 297436 Color: 4006
Size: 268733 Color: 2055

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 433853 Color: 8818
Size: 287784 Color: 3422
Size: 278364 Color: 2814

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 433890 Color: 8819
Size: 304873 Color: 4406
Size: 261238 Color: 1442

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 433937 Color: 8820
Size: 289765 Color: 3549
Size: 276299 Color: 2668

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 433938 Color: 8821
Size: 286654 Color: 3357
Size: 279409 Color: 2886

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 433988 Color: 8822
Size: 289551 Color: 3536
Size: 276462 Color: 2679

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 434153 Color: 8823
Size: 284624 Color: 3212
Size: 281224 Color: 2997

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 434170 Color: 8824
Size: 306669 Color: 4493
Size: 259162 Color: 1231

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 434183 Color: 8825
Size: 300438 Color: 4156
Size: 265380 Color: 1775

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 434187 Color: 8826
Size: 304292 Color: 4367
Size: 261522 Color: 1470

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 434237 Color: 8827
Size: 286653 Color: 3356
Size: 279111 Color: 2865

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 434274 Color: 8828
Size: 287824 Color: 3427
Size: 277903 Color: 2779

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 434351 Color: 8829
Size: 298473 Color: 4067
Size: 267177 Color: 1926

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 434388 Color: 8830
Size: 295052 Color: 3873
Size: 270561 Color: 2214

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 434393 Color: 8831
Size: 311678 Color: 4766
Size: 253930 Color: 607

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 434413 Color: 8832
Size: 283944 Color: 3166
Size: 281644 Color: 3027

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 434470 Color: 8833
Size: 298149 Color: 4046
Size: 267382 Color: 1941

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 434584 Color: 8834
Size: 296758 Color: 3969
Size: 268659 Color: 2049

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 434604 Color: 8835
Size: 282881 Color: 3106
Size: 282516 Color: 3083

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 434680 Color: 8836
Size: 307946 Color: 4558
Size: 257375 Color: 1039

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 434727 Color: 8837
Size: 310244 Color: 4690
Size: 255030 Color: 740

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 434754 Color: 8838
Size: 300263 Color: 4137
Size: 264984 Color: 1737

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 434791 Color: 8839
Size: 299898 Color: 4125
Size: 265312 Color: 1771

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 434803 Color: 8840
Size: 304950 Color: 4412
Size: 260248 Color: 1343

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 434824 Color: 8841
Size: 300897 Color: 4179
Size: 264280 Color: 1693

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 434845 Color: 8842
Size: 304023 Color: 4352
Size: 261133 Color: 1433

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 434888 Color: 8843
Size: 314162 Color: 4910
Size: 250951 Color: 182

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 434900 Color: 8844
Size: 298530 Color: 4069
Size: 266571 Color: 1870

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 434927 Color: 8845
Size: 313018 Color: 4849
Size: 252056 Color: 360

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 434949 Color: 8846
Size: 287923 Color: 3436
Size: 277129 Color: 2726

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 434985 Color: 8847
Size: 309366 Color: 4631
Size: 255650 Color: 818

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 435065 Color: 8848
Size: 292522 Color: 3725
Size: 272414 Color: 2365

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 435067 Color: 8849
Size: 297249 Color: 3996
Size: 267685 Color: 1964

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 435118 Color: 8850
Size: 296328 Color: 3946
Size: 268555 Color: 2041

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 435220 Color: 8851
Size: 297204 Color: 3993
Size: 267577 Color: 1953

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 435232 Color: 8852
Size: 285693 Color: 3291
Size: 279076 Color: 2862

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 435294 Color: 8853
Size: 285162 Color: 3243
Size: 279545 Color: 2892

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 435329 Color: 8854
Size: 288110 Color: 3450
Size: 276562 Color: 2688

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 435343 Color: 8855
Size: 283286 Color: 3129
Size: 281372 Color: 3009

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 435343 Color: 8856
Size: 291598 Color: 3665
Size: 273060 Color: 2410

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 435429 Color: 8857
Size: 290949 Color: 3623
Size: 273623 Color: 2452

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 435465 Color: 8858
Size: 294743 Color: 3855
Size: 269793 Color: 2146

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 435472 Color: 8859
Size: 313339 Color: 4866
Size: 251190 Color: 217

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 435522 Color: 8860
Size: 290228 Color: 3572
Size: 274251 Color: 2498

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 435537 Color: 8861
Size: 307302 Color: 4524
Size: 257162 Color: 1018

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 435546 Color: 8862
Size: 304278 Color: 4364
Size: 260177 Color: 1334

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 435562 Color: 8863
Size: 300365 Color: 4147
Size: 264074 Color: 1672

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 435578 Color: 8864
Size: 308401 Color: 4581
Size: 256022 Color: 866

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 435663 Color: 8865
Size: 285114 Color: 3240
Size: 279224 Color: 2878

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 435699 Color: 8866
Size: 282446 Color: 3079
Size: 281856 Color: 3046

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 435710 Color: 8867
Size: 304353 Color: 4372
Size: 259938 Color: 1307

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 435769 Color: 8868
Size: 303200 Color: 4310
Size: 261032 Color: 1428

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 435793 Color: 8869
Size: 293821 Color: 3798
Size: 270387 Color: 2196

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 435876 Color: 8870
Size: 304417 Color: 4376
Size: 259708 Color: 1293

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 435913 Color: 8871
Size: 290976 Color: 3626
Size: 273112 Color: 2414

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 435967 Color: 8872
Size: 282643 Color: 3094
Size: 281391 Color: 3010

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 435970 Color: 8873
Size: 298157 Color: 4047
Size: 265874 Color: 1814

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 435984 Color: 8874
Size: 285733 Color: 3292
Size: 278284 Color: 2805

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 436015 Color: 8875
Size: 292246 Color: 3711
Size: 271740 Color: 2312

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 436050 Color: 8876
Size: 297048 Color: 3986
Size: 266903 Color: 1902

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 436106 Color: 8877
Size: 304271 Color: 4363
Size: 259624 Color: 1282

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 436142 Color: 8878
Size: 286396 Color: 3339
Size: 277463 Color: 2750

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 436202 Color: 8879
Size: 282239 Color: 3072
Size: 281560 Color: 3021

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 436265 Color: 8880
Size: 295560 Color: 3910
Size: 268176 Color: 2013

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 436359 Color: 8881
Size: 311899 Color: 4784
Size: 251743 Color: 314

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 436512 Color: 8882
Size: 287836 Color: 3429
Size: 275653 Color: 2612

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 436526 Color: 8883
Size: 286594 Color: 3354
Size: 276881 Color: 2714

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 436580 Color: 8884
Size: 293120 Color: 3758
Size: 270301 Color: 2188

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 436591 Color: 8885
Size: 310766 Color: 4723
Size: 252644 Color: 442

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 436600 Color: 8886
Size: 305561 Color: 4435
Size: 257840 Color: 1095

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 436630 Color: 8887
Size: 286391 Color: 3337
Size: 276980 Color: 2718

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 436678 Color: 8888
Size: 294690 Color: 3850
Size: 268633 Color: 2046

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 436727 Color: 8889
Size: 296565 Color: 3959
Size: 266709 Color: 1881

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 436743 Color: 8890
Size: 287809 Color: 3425
Size: 275449 Color: 2600

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 436783 Color: 8891
Size: 301812 Color: 4232
Size: 261406 Color: 1458

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 436792 Color: 8892
Size: 310583 Color: 4712
Size: 252626 Color: 440

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 436808 Color: 8893
Size: 309560 Color: 4643
Size: 253633 Color: 569

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 436883 Color: 8894
Size: 305539 Color: 4434
Size: 257579 Color: 1062

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 436945 Color: 8895
Size: 310127 Color: 4684
Size: 252929 Color: 478

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 437066 Color: 8896
Size: 294711 Color: 3853
Size: 268224 Color: 2020

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 437067 Color: 8897
Size: 283226 Color: 3124
Size: 279708 Color: 2896

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 437068 Color: 8898
Size: 297983 Color: 4036
Size: 264950 Color: 1733

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 437112 Color: 8899
Size: 289591 Color: 3538
Size: 273298 Color: 2431

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 437233 Color: 8900
Size: 286470 Color: 3348
Size: 276298 Color: 2666

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 437250 Color: 8901
Size: 284444 Color: 3201
Size: 278307 Color: 2807

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 437276 Color: 8902
Size: 295995 Color: 3930
Size: 266730 Color: 1883

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 437297 Color: 8903
Size: 300080 Color: 4132
Size: 262624 Color: 1562

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 437308 Color: 8904
Size: 306789 Color: 4496
Size: 255904 Color: 853

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 437346 Color: 8905
Size: 291114 Color: 3635
Size: 271541 Color: 2296

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 437347 Color: 8906
Size: 309490 Color: 4638
Size: 253164 Color: 519

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 437369 Color: 8907
Size: 293823 Color: 3799
Size: 268809 Color: 2065

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 437374 Color: 8908
Size: 297286 Color: 3998
Size: 265341 Color: 1774

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 437396 Color: 8909
Size: 305244 Color: 4426
Size: 257361 Color: 1038

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 437556 Color: 8910
Size: 310078 Color: 4679
Size: 252367 Color: 399

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 437587 Color: 8911
Size: 285346 Color: 3263
Size: 277068 Color: 2725

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 437589 Color: 8912
Size: 294416 Color: 3829
Size: 267996 Color: 1993

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 437629 Color: 8913
Size: 281604 Color: 3024
Size: 280768 Color: 2974

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 437652 Color: 8914
Size: 303939 Color: 4343
Size: 258410 Color: 1151

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 437724 Color: 8915
Size: 295666 Color: 3915
Size: 266611 Color: 1872

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 437726 Color: 8916
Size: 297576 Color: 4012
Size: 264699 Color: 1714

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 437739 Color: 8917
Size: 289807 Color: 3551
Size: 272455 Color: 2368

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 437754 Color: 8918
Size: 310637 Color: 4718
Size: 251610 Color: 285

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 437758 Color: 8919
Size: 282051 Color: 3058
Size: 280192 Color: 2934

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 437762 Color: 8920
Size: 311805 Color: 4778
Size: 250434 Color: 73

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 437765 Color: 8921
Size: 301711 Color: 4221
Size: 260525 Color: 1376

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 437817 Color: 8922
Size: 292535 Color: 3726
Size: 269649 Color: 2134

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 437862 Color: 8923
Size: 285928 Color: 3304
Size: 276211 Color: 2658

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 437888 Color: 8924
Size: 303911 Color: 4342
Size: 258202 Color: 1137

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 437910 Color: 8925
Size: 284775 Color: 3220
Size: 277316 Color: 2740

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 437922 Color: 8926
Size: 299743 Color: 4116
Size: 262336 Color: 1539

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 437953 Color: 8927
Size: 294981 Color: 3867
Size: 267067 Color: 1918

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 437964 Color: 8928
Size: 295944 Color: 3925
Size: 266093 Color: 1840

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 437969 Color: 8929
Size: 290862 Color: 3617
Size: 271170 Color: 2264

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 438110 Color: 8930
Size: 309031 Color: 4613
Size: 252860 Color: 468

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 438212 Color: 8931
Size: 300373 Color: 4148
Size: 261416 Color: 1459

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 438236 Color: 8932
Size: 310107 Color: 4682
Size: 251658 Color: 294

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 438240 Color: 8933
Size: 294120 Color: 3815
Size: 267641 Color: 1957

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 438365 Color: 8934
Size: 295163 Color: 3881
Size: 266473 Color: 1863

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 438456 Color: 8935
Size: 301713 Color: 4222
Size: 259832 Color: 1298

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 438474 Color: 8936
Size: 308095 Color: 4567
Size: 253432 Color: 547

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 438495 Color: 8937
Size: 293426 Color: 3776
Size: 268080 Color: 1999

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 438575 Color: 8938
Size: 301298 Color: 4204
Size: 260128 Color: 1329

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 438592 Color: 8939
Size: 308914 Color: 4607
Size: 252495 Color: 419

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 438595 Color: 8940
Size: 288407 Color: 3467
Size: 272999 Color: 2406

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 438601 Color: 8941
Size: 287222 Color: 3384
Size: 274178 Color: 2492

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 438629 Color: 8942
Size: 296564 Color: 3958
Size: 264808 Color: 1722

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 438707 Color: 8943
Size: 308489 Color: 4585
Size: 252805 Color: 457

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 438709 Color: 8944
Size: 298433 Color: 4064
Size: 262859 Color: 1578

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 438826 Color: 8945
Size: 303410 Color: 4317
Size: 257765 Color: 1085

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 438889 Color: 8946
Size: 289127 Color: 3510
Size: 271985 Color: 2331

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 438934 Color: 8947
Size: 304687 Color: 4393
Size: 256380 Color: 924

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 438935 Color: 8948
Size: 289177 Color: 3517
Size: 271889 Color: 2322

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 438943 Color: 8949
Size: 302716 Color: 4285
Size: 258342 Color: 1144

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 438974 Color: 8950
Size: 301597 Color: 4217
Size: 259430 Color: 1261

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 439007 Color: 8951
Size: 309726 Color: 4657
Size: 251268 Color: 235

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 439030 Color: 8952
Size: 303860 Color: 4339
Size: 257111 Color: 1010

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 439166 Color: 8953
Size: 288820 Color: 3485
Size: 272015 Color: 2336

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 439367 Color: 8954
Size: 292770 Color: 3740
Size: 267864 Color: 1979

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 439445 Color: 8955
Size: 309456 Color: 4637
Size: 251100 Color: 203

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 439457 Color: 8956
Size: 286407 Color: 3340
Size: 274137 Color: 2490

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 439470 Color: 8957
Size: 282970 Color: 3111
Size: 277561 Color: 2757

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 439508 Color: 8958
Size: 287325 Color: 3393
Size: 273168 Color: 2419

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 439509 Color: 8959
Size: 290860 Color: 3616
Size: 269632 Color: 2133

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 439525 Color: 8960
Size: 304034 Color: 4353
Size: 256442 Color: 931

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 439575 Color: 8961
Size: 306235 Color: 4470
Size: 254191 Color: 641

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 439592 Color: 8962
Size: 304708 Color: 4394
Size: 255701 Color: 824

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 439621 Color: 8963
Size: 296569 Color: 3961
Size: 263811 Color: 1649

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 439648 Color: 8964
Size: 303373 Color: 4313
Size: 256980 Color: 994

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 439697 Color: 8965
Size: 301854 Color: 4237
Size: 258450 Color: 1157

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 439717 Color: 8966
Size: 286044 Color: 3318
Size: 274240 Color: 2496

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 439725 Color: 8967
Size: 303983 Color: 4348
Size: 256293 Color: 905

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 439726 Color: 8968
Size: 284904 Color: 3230
Size: 275371 Color: 2598

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 439734 Color: 8969
Size: 293723 Color: 3789
Size: 266544 Color: 1868

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 439736 Color: 8970
Size: 301158 Color: 4195
Size: 259107 Color: 1223

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 439741 Color: 8971
Size: 282514 Color: 3082
Size: 277746 Color: 2767

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 439795 Color: 8972
Size: 290785 Color: 3607
Size: 269421 Color: 2113

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 439851 Color: 8973
Size: 295023 Color: 3869
Size: 265127 Color: 1757

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 439879 Color: 8974
Size: 280400 Color: 2951
Size: 279722 Color: 2899

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 440004 Color: 8975
Size: 295251 Color: 3885
Size: 264746 Color: 1717

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 440065 Color: 8976
Size: 291468 Color: 3652
Size: 268468 Color: 2034

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 440209 Color: 8977
Size: 305477 Color: 4433
Size: 254315 Color: 657

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 440268 Color: 8978
Size: 296173 Color: 3937
Size: 263560 Color: 1637

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 440273 Color: 8979
Size: 280821 Color: 2977
Size: 278907 Color: 2854

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 440652 Color: 8980
Size: 290215 Color: 3571
Size: 269134 Color: 2088

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 440672 Color: 8981
Size: 280129 Color: 2929
Size: 279200 Color: 2876

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 440699 Color: 8982
Size: 308721 Color: 4596
Size: 250581 Color: 106

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 440708 Color: 8983
Size: 282541 Color: 3086
Size: 276752 Color: 2704

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 440730 Color: 8984
Size: 284824 Color: 3222
Size: 274447 Color: 2521

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 440788 Color: 8985
Size: 308556 Color: 4589
Size: 250657 Color: 121

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 440840 Color: 8986
Size: 303169 Color: 4308
Size: 255992 Color: 861

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 440856 Color: 8987
Size: 286932 Color: 3370
Size: 272213 Color: 2351

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 440858 Color: 8988
Size: 304907 Color: 4408
Size: 254236 Color: 649

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 440860 Color: 8989
Size: 286164 Color: 3326
Size: 272977 Color: 2404

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 441002 Color: 8990
Size: 291826 Color: 3691
Size: 267173 Color: 1925

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 441075 Color: 8991
Size: 304598 Color: 4386
Size: 254328 Color: 659

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 441089 Color: 8992
Size: 281762 Color: 3038
Size: 277150 Color: 2730

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 441142 Color: 8993
Size: 293834 Color: 3802
Size: 265025 Color: 1742

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 441357 Color: 8994
Size: 280208 Color: 2936
Size: 278436 Color: 2822

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 441368 Color: 8995
Size: 296016 Color: 3932
Size: 262617 Color: 1561

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 441377 Color: 8996
Size: 287892 Color: 3433
Size: 270732 Color: 2225

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 441458 Color: 8997
Size: 298442 Color: 4065
Size: 260101 Color: 1327

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 441473 Color: 8998
Size: 303977 Color: 4346
Size: 254551 Color: 691

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 441577 Color: 8999
Size: 300408 Color: 4152
Size: 258016 Color: 1110

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 441588 Color: 9000
Size: 292178 Color: 3707
Size: 266235 Color: 1845

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 441627 Color: 9001
Size: 296829 Color: 3975
Size: 261545 Color: 1471

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 441682 Color: 9002
Size: 292793 Color: 3742
Size: 265526 Color: 1784

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 441733 Color: 9003
Size: 296923 Color: 3978
Size: 261345 Color: 1453

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 441735 Color: 9004
Size: 306540 Color: 4483
Size: 251726 Color: 309

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 441806 Color: 9005
Size: 303022 Color: 4301
Size: 255173 Color: 755

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 441822 Color: 9006
Size: 292843 Color: 3746
Size: 265336 Color: 1773

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 441856 Color: 9007
Size: 284847 Color: 3226
Size: 273298 Color: 2432

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 441899 Color: 9008
Size: 287604 Color: 3411
Size: 270498 Color: 2205

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 441953 Color: 9009
Size: 289344 Color: 3525
Size: 268704 Color: 2054

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 442008 Color: 9010
Size: 300237 Color: 4135
Size: 257756 Color: 1084

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 442045 Color: 9011
Size: 303586 Color: 4326
Size: 254370 Color: 664

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 442054 Color: 9012
Size: 299811 Color: 4120
Size: 258136 Color: 1127

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 442129 Color: 9013
Size: 297693 Color: 4022
Size: 260179 Color: 1335

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 442138 Color: 9014
Size: 298037 Color: 4038
Size: 259826 Color: 1297

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 442158 Color: 9015
Size: 291842 Color: 3693
Size: 266001 Color: 1828

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 442174 Color: 9016
Size: 289128 Color: 3511
Size: 268699 Color: 2053

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 442228 Color: 9017
Size: 286387 Color: 3335
Size: 271386 Color: 2280

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 442301 Color: 9018
Size: 295754 Color: 3919
Size: 261946 Color: 1503

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 442363 Color: 9019
Size: 285252 Color: 3252
Size: 272386 Color: 2362

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 442392 Color: 9020
Size: 290243 Color: 3573
Size: 267366 Color: 1938

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 442438 Color: 9021
Size: 305118 Color: 4420
Size: 252445 Color: 411

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 442455 Color: 9022
Size: 305795 Color: 4447
Size: 251751 Color: 316

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 442489 Color: 9023
Size: 284595 Color: 3209
Size: 272917 Color: 2399

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 442525 Color: 9024
Size: 285390 Color: 3265
Size: 272086 Color: 2342

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 442579 Color: 9025
Size: 300766 Color: 4171
Size: 256656 Color: 950

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 442662 Color: 9026
Size: 302366 Color: 4263
Size: 254973 Color: 736

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 442726 Color: 9027
Size: 282870 Color: 3104
Size: 274405 Color: 2514

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 442772 Color: 9028
Size: 291638 Color: 3668
Size: 265591 Color: 1793

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 442783 Color: 9029
Size: 301205 Color: 4199
Size: 256013 Color: 865

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 442835 Color: 9030
Size: 304316 Color: 4369
Size: 252850 Color: 467

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 442858 Color: 9031
Size: 289960 Color: 3558
Size: 267183 Color: 1927

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 442866 Color: 9032
Size: 295634 Color: 3913
Size: 261501 Color: 1466

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 442881 Color: 9033
Size: 281408 Color: 3011
Size: 275712 Color: 2616

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 442918 Color: 9034
Size: 300271 Color: 4139
Size: 256812 Color: 972

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 443016 Color: 9035
Size: 304913 Color: 4409
Size: 252072 Color: 363

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 443052 Color: 9036
Size: 301758 Color: 4227
Size: 255191 Color: 762

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 443126 Color: 9037
Size: 296095 Color: 3933
Size: 260780 Color: 1400

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 443132 Color: 9038
Size: 291799 Color: 3687
Size: 265070 Color: 1748

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 443174 Color: 9039
Size: 306515 Color: 4481
Size: 250312 Color: 57

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 443220 Color: 9040
Size: 282897 Color: 3107
Size: 273884 Color: 2477

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 443226 Color: 9041
Size: 292310 Color: 3714
Size: 264465 Color: 1703

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 443267 Color: 9042
Size: 305717 Color: 4445
Size: 251017 Color: 194

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 443281 Color: 9043
Size: 296225 Color: 3941
Size: 260495 Color: 1373

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 443437 Color: 9044
Size: 278918 Color: 2855
Size: 277646 Color: 2761

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 443437 Color: 9045
Size: 285309 Color: 3255
Size: 271255 Color: 2269

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 443457 Color: 9046
Size: 282106 Color: 3063
Size: 274438 Color: 2518

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 443510 Color: 9047
Size: 298125 Color: 4044
Size: 258366 Color: 1147

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 443643 Color: 9048
Size: 284369 Color: 3196
Size: 271989 Color: 2332

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 443672 Color: 9049
Size: 292293 Color: 3712
Size: 264036 Color: 1667

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 443682 Color: 9050
Size: 303541 Color: 4322
Size: 252778 Color: 455

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 443744 Color: 9051
Size: 286331 Color: 3333
Size: 269926 Color: 2160

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 443769 Color: 9052
Size: 285312 Color: 3257
Size: 270920 Color: 2239

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 443851 Color: 9053
Size: 283351 Color: 3135
Size: 272799 Color: 2392

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 443877 Color: 9054
Size: 293956 Color: 3808
Size: 262168 Color: 1521

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 443886 Color: 9055
Size: 293818 Color: 3797
Size: 262297 Color: 1536

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 443926 Color: 9056
Size: 305424 Color: 4431
Size: 250651 Color: 120

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 443966 Color: 9057
Size: 303548 Color: 4325
Size: 252487 Color: 416

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 443992 Color: 9058
Size: 282796 Color: 3102
Size: 273213 Color: 2426

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 444036 Color: 9059
Size: 278657 Color: 2839
Size: 277308 Color: 2739

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 444092 Color: 9060
Size: 287793 Color: 3423
Size: 268116 Color: 2004

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 444147 Color: 9061
Size: 289206 Color: 3518
Size: 266648 Color: 1875

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 444159 Color: 9062
Size: 303422 Color: 4318
Size: 252420 Color: 407

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 444184 Color: 9063
Size: 288971 Color: 3496
Size: 266846 Color: 1900

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 444194 Color: 9064
Size: 305419 Color: 4429
Size: 250388 Color: 67

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 444197 Color: 9065
Size: 299843 Color: 4123
Size: 255961 Color: 857

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 444203 Color: 9066
Size: 278637 Color: 2836
Size: 277161 Color: 2731

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 444208 Color: 9067
Size: 280992 Color: 2986
Size: 274801 Color: 2549

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 444261 Color: 9068
Size: 298018 Color: 4037
Size: 257722 Color: 1077

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 444350 Color: 9069
Size: 281558 Color: 3020
Size: 274093 Color: 2489

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 444453 Color: 9070
Size: 286019 Color: 3315
Size: 269529 Color: 2122

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 444517 Color: 9071
Size: 289162 Color: 3513
Size: 266322 Color: 1852

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 444527 Color: 9072
Size: 278761 Color: 2846
Size: 276713 Color: 2703

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 444546 Color: 9073
Size: 279009 Color: 2858
Size: 276446 Color: 2678

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 444563 Color: 9074
Size: 300014 Color: 4130
Size: 255424 Color: 789

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 444616 Color: 9075
Size: 279799 Color: 2902
Size: 275586 Color: 2607

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 444631 Color: 9076
Size: 279026 Color: 2860
Size: 276344 Color: 2671

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 444696 Color: 9077
Size: 294185 Color: 3819
Size: 261120 Color: 1431

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 444719 Color: 9078
Size: 299638 Color: 4109
Size: 255644 Color: 817

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 444725 Color: 9079
Size: 300787 Color: 4173
Size: 254489 Color: 686

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 444774 Color: 9080
Size: 297456 Color: 4008
Size: 257771 Color: 1087

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 444844 Color: 9081
Size: 298368 Color: 4060
Size: 256789 Color: 971

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 444882 Color: 9082
Size: 301085 Color: 4188
Size: 254034 Color: 624

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 445144 Color: 9083
Size: 295620 Color: 3912
Size: 259237 Color: 1242

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 445160 Color: 9084
Size: 304249 Color: 4362
Size: 250592 Color: 109

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 445175 Color: 9085
Size: 285592 Color: 3281
Size: 269234 Color: 2098

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 445336 Color: 9086
Size: 296518 Color: 3954
Size: 258147 Color: 1129

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 445447 Color: 9087
Size: 280185 Color: 2933
Size: 274369 Color: 2509

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 445500 Color: 9088
Size: 294194 Color: 3821
Size: 260307 Color: 1355

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 445508 Color: 9089
Size: 295119 Color: 3879
Size: 259374 Color: 1254

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 445531 Color: 9090
Size: 302911 Color: 4294
Size: 251559 Color: 277

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 445592 Color: 9091
Size: 277790 Color: 2769
Size: 276619 Color: 2694

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 445593 Color: 9092
Size: 285060 Color: 3238
Size: 269348 Color: 2108

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 445613 Color: 9093
Size: 281126 Color: 2994
Size: 273262 Color: 2429

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 445666 Color: 9094
Size: 289044 Color: 3500
Size: 265291 Color: 1769

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 445679 Color: 9095
Size: 296180 Color: 3938
Size: 258142 Color: 1128

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 445733 Color: 9096
Size: 278776 Color: 2848
Size: 275492 Color: 2602

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 445734 Color: 9097
Size: 285441 Color: 3268
Size: 268826 Color: 2068

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 445782 Color: 9098
Size: 294154 Color: 3817
Size: 260065 Color: 1322

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 445837 Color: 9099
Size: 279937 Color: 2914
Size: 274227 Color: 2495

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 445842 Color: 9100
Size: 298972 Color: 4087
Size: 255187 Color: 761

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 445855 Color: 9101
Size: 283654 Color: 3152
Size: 270492 Color: 2204

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 445912 Color: 9102
Size: 291757 Color: 3682
Size: 262332 Color: 1538

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 445969 Color: 9103
Size: 284189 Color: 3185
Size: 269843 Color: 2151

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 445972 Color: 9104
Size: 278463 Color: 2825
Size: 275566 Color: 2605

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 445984 Color: 9105
Size: 296485 Color: 3951
Size: 257532 Color: 1056

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 446102 Color: 9106
Size: 298268 Color: 4053
Size: 255631 Color: 813

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 446135 Color: 9107
Size: 292852 Color: 3748
Size: 261014 Color: 1425

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 446177 Color: 9108
Size: 287321 Color: 3392
Size: 266503 Color: 1865

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 446254 Color: 9109
Size: 302628 Color: 4280
Size: 251119 Color: 206

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 446348 Color: 9110
Size: 287236 Color: 3385
Size: 266417 Color: 1862

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 446460 Color: 9111
Size: 301039 Color: 4182
Size: 252502 Color: 422

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 446488 Color: 9112
Size: 287255 Color: 3388
Size: 266258 Color: 1847

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 446542 Color: 9113
Size: 303394 Color: 4315
Size: 250065 Color: 13

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 446673 Color: 9114
Size: 292816 Color: 3743
Size: 260512 Color: 1374

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 446763 Color: 9115
Size: 297922 Color: 4031
Size: 255316 Color: 773

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 446837 Color: 9116
Size: 288093 Color: 3449
Size: 265071 Color: 1749

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 446851 Color: 9117
Size: 283929 Color: 3165
Size: 269221 Color: 2096

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 446864 Color: 9118
Size: 277003 Color: 2720
Size: 276134 Color: 2649

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 446941 Color: 9119
Size: 290836 Color: 3613
Size: 262224 Color: 1527

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 446954 Color: 9120
Size: 299835 Color: 4122
Size: 253212 Color: 522

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 446968 Color: 9121
Size: 281272 Color: 3003
Size: 271761 Color: 2314

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 447056 Color: 9122
Size: 298109 Color: 4042
Size: 254836 Color: 721

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 447089 Color: 9123
Size: 301747 Color: 4224
Size: 251165 Color: 213

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 447137 Color: 9124
Size: 281239 Color: 2998
Size: 271625 Color: 2303

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 447144 Color: 9125
Size: 279313 Color: 2883
Size: 273544 Color: 2447

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 447163 Color: 9126
Size: 282554 Color: 3087
Size: 270284 Color: 2187

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 447180 Color: 9127
Size: 280676 Color: 2968
Size: 272145 Color: 2347

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 447230 Color: 9128
Size: 281060 Color: 2991
Size: 271711 Color: 2310

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 447273 Color: 9129
Size: 289077 Color: 3505
Size: 263651 Color: 1641

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 447335 Color: 9130
Size: 279300 Color: 2882
Size: 273366 Color: 2438

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 447421 Color: 9131
Size: 285546 Color: 3277
Size: 267034 Color: 1914

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 447533 Color: 9132
Size: 277674 Color: 2765
Size: 274794 Color: 2548

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 447556 Color: 9133
Size: 289340 Color: 3524
Size: 263105 Color: 1602

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 447557 Color: 9134
Size: 282005 Color: 3056
Size: 270439 Color: 2200

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 447678 Color: 9135
Size: 276802 Color: 2708
Size: 275521 Color: 2603

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 447752 Color: 9136
Size: 293023 Color: 3755
Size: 259226 Color: 1240

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 447795 Color: 9137
Size: 301159 Color: 4196
Size: 251047 Color: 198

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 447967 Color: 9138
Size: 290195 Color: 3570
Size: 261839 Color: 1493

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 448025 Color: 9139
Size: 280439 Color: 2955
Size: 271537 Color: 2295

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 448085 Color: 9140
Size: 286906 Color: 3368
Size: 265010 Color: 1741

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 448118 Color: 9141
Size: 277535 Color: 2755
Size: 274348 Color: 2507

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 448203 Color: 9142
Size: 287276 Color: 3390
Size: 264522 Color: 1704

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 448291 Color: 9143
Size: 288640 Color: 3477
Size: 263070 Color: 1600

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 448310 Color: 9144
Size: 298243 Color: 4050
Size: 253448 Color: 549

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 448315 Color: 9145
Size: 286562 Color: 3353
Size: 265124 Color: 1755

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 448323 Color: 9146
Size: 288143 Color: 3451
Size: 263535 Color: 1635

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 448324 Color: 9147
Size: 298308 Color: 4055
Size: 253369 Color: 535

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 448340 Color: 9148
Size: 281784 Color: 3042
Size: 269877 Color: 2157

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 448381 Color: 9149
Size: 286189 Color: 3328
Size: 265431 Color: 1780

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 448515 Color: 9150
Size: 290715 Color: 3601
Size: 260771 Color: 1397

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 448604 Color: 9151
Size: 298903 Color: 4083
Size: 252494 Color: 418

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 448641 Color: 9152
Size: 286372 Color: 3334
Size: 264988 Color: 1738

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 448792 Color: 9153
Size: 283491 Color: 3143
Size: 267718 Color: 1969

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 448882 Color: 9154
Size: 291724 Color: 3677
Size: 259395 Color: 1257

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 448962 Color: 9155
Size: 291375 Color: 3649
Size: 259664 Color: 1290

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 449015 Color: 9156
Size: 291067 Color: 3632
Size: 259919 Color: 1304

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 449093 Color: 9157
Size: 288909 Color: 3491
Size: 261999 Color: 1508

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 449155 Color: 9158
Size: 295335 Color: 3891
Size: 255511 Color: 799

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 449190 Color: 9159
Size: 283990 Color: 3172
Size: 266821 Color: 1894

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 449208 Color: 9160
Size: 282342 Color: 3076
Size: 268451 Color: 2033

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 449270 Color: 9161
Size: 295060 Color: 3875
Size: 255671 Color: 821

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 449311 Color: 9162
Size: 295802 Color: 3920
Size: 254888 Color: 730

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 449511 Color: 9163
Size: 275945 Color: 2633
Size: 274545 Color: 2529

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 449536 Color: 9164
Size: 298111 Color: 4043
Size: 252354 Color: 397

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 449592 Color: 9165
Size: 290048 Color: 3564
Size: 260361 Color: 1362

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 449626 Color: 9166
Size: 293418 Color: 3774
Size: 256957 Color: 990

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 449678 Color: 9167
Size: 296461 Color: 3949
Size: 253862 Color: 593

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 449705 Color: 9168
Size: 296231 Color: 3942
Size: 254065 Color: 629

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 449723 Color: 9169
Size: 284053 Color: 3177
Size: 266225 Color: 1844

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 449764 Color: 9170
Size: 282873 Color: 3105
Size: 267364 Color: 1937

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 449814 Color: 9171
Size: 276859 Color: 2712
Size: 273328 Color: 2433

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 449898 Color: 9172
Size: 284068 Color: 3179
Size: 266035 Color: 1834

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 449929 Color: 9173
Size: 284022 Color: 3174
Size: 266050 Color: 1837

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 449937 Color: 9174
Size: 277498 Color: 2753
Size: 272566 Color: 2378

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 449940 Color: 9175
Size: 288562 Color: 3475
Size: 261499 Color: 1465

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 449976 Color: 9176
Size: 288173 Color: 3453
Size: 261852 Color: 1495

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 449979 Color: 9177
Size: 284298 Color: 3189
Size: 265724 Color: 1803

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 450000 Color: 9178
Size: 275293 Color: 2592
Size: 274708 Color: 2541

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 450088 Color: 9179
Size: 295147 Color: 3880
Size: 254766 Color: 711

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 450176 Color: 9180
Size: 281540 Color: 3018
Size: 268285 Color: 2026

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 450219 Color: 9181
Size: 285675 Color: 3289
Size: 264107 Color: 1674

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 450263 Color: 9182
Size: 281745 Color: 3035
Size: 267993 Color: 1992

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 450313 Color: 9183
Size: 276658 Color: 2697
Size: 273030 Color: 2408

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 450406 Color: 9184
Size: 288146 Color: 3452
Size: 261449 Color: 1460

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 450406 Color: 9185
Size: 289066 Color: 3502
Size: 260529 Color: 1379

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 450463 Color: 9186
Size: 285912 Color: 3303
Size: 263626 Color: 1638

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 450540 Color: 9187
Size: 290321 Color: 3577
Size: 259140 Color: 1228

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 450562 Color: 9188
Size: 275052 Color: 2575
Size: 274387 Color: 2511

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 450668 Color: 9189
Size: 289219 Color: 3519
Size: 260114 Color: 1328

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 450806 Color: 9190
Size: 294014 Color: 3811
Size: 255181 Color: 759

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 450872 Color: 9191
Size: 285341 Color: 3262
Size: 263788 Color: 1647

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 450896 Color: 9192
Size: 285873 Color: 3300
Size: 263232 Color: 1609

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 450925 Color: 9193
Size: 285994 Color: 3314
Size: 263082 Color: 1601

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 451003 Color: 9194
Size: 291015 Color: 3629
Size: 257983 Color: 1106

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 451032 Color: 9195
Size: 294230 Color: 3822
Size: 254739 Color: 703

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 451060 Color: 9196
Size: 295518 Color: 3905
Size: 253423 Color: 545

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 451072 Color: 9197
Size: 291135 Color: 3637
Size: 257794 Color: 1091

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 451104 Color: 9198
Size: 279100 Color: 2864
Size: 269797 Color: 2147

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 451230 Color: 9199
Size: 279298 Color: 2881
Size: 269473 Color: 2117

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 451234 Color: 9200
Size: 291352 Color: 3647
Size: 257415 Color: 1044

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 451323 Color: 9201
Size: 297774 Color: 4024
Size: 250904 Color: 170

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 451331 Color: 9202
Size: 295975 Color: 3929
Size: 252695 Color: 447

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 451338 Color: 9203
Size: 276650 Color: 2696
Size: 272013 Color: 2334

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 451396 Color: 9204
Size: 281026 Color: 2990
Size: 267579 Color: 1954

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 451464 Color: 9205
Size: 292954 Color: 3751
Size: 255583 Color: 807

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 451469 Color: 9206
Size: 298358 Color: 4059
Size: 250174 Color: 34

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 451473 Color: 9207
Size: 287331 Color: 3395
Size: 261197 Color: 1439

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 451552 Color: 9208
Size: 276222 Color: 2660
Size: 272227 Color: 2353

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 451557 Color: 9209
Size: 289722 Color: 3547
Size: 258722 Color: 1181

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 451599 Color: 9210
Size: 277804 Color: 2771
Size: 270598 Color: 2216

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 451683 Color: 9211
Size: 293288 Color: 3771
Size: 255030 Color: 741

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 451824 Color: 9212
Size: 285608 Color: 3282
Size: 262569 Color: 1557

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 451841 Color: 9213
Size: 289377 Color: 3529
Size: 258783 Color: 1190

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 451861 Color: 9214
Size: 294887 Color: 3863
Size: 253253 Color: 524

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 451885 Color: 9215
Size: 274711 Color: 2542
Size: 273405 Color: 2440

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 451988 Color: 9216
Size: 275850 Color: 2625
Size: 272163 Color: 2348

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 452014 Color: 9217
Size: 296149 Color: 3935
Size: 251838 Color: 328

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 452023 Color: 9218
Size: 276469 Color: 2680
Size: 271509 Color: 2292

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 452031 Color: 9219
Size: 290661 Color: 3596
Size: 257309 Color: 1032

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 452054 Color: 9220
Size: 279990 Color: 2917
Size: 267957 Color: 1987

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 452080 Color: 9221
Size: 295170 Color: 3882
Size: 252751 Color: 452

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 452106 Color: 9222
Size: 291997 Color: 3699
Size: 255898 Color: 850

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 452112 Color: 9223
Size: 294429 Color: 3831
Size: 253460 Color: 552

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 452252 Color: 9224
Size: 276695 Color: 2701
Size: 271054 Color: 2251

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 452379 Color: 9225
Size: 279411 Color: 2887
Size: 268211 Color: 2018

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 452445 Color: 9226
Size: 295098 Color: 3878
Size: 252458 Color: 412

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 452534 Color: 9227
Size: 277448 Color: 2747
Size: 270019 Color: 2168

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 452615 Color: 9228
Size: 274248 Color: 2497
Size: 273138 Color: 2416

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 452656 Color: 9229
Size: 275812 Color: 2623
Size: 271533 Color: 2294

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 452667 Color: 9230
Size: 295289 Color: 3888
Size: 252045 Color: 357

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 452689 Color: 9231
Size: 275959 Color: 2635
Size: 271353 Color: 2277

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 452715 Color: 9232
Size: 278095 Color: 2795
Size: 269191 Color: 2093

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 452798 Color: 9233
Size: 290877 Color: 3619
Size: 256326 Color: 915

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 452870 Color: 9234
Size: 288393 Color: 3466
Size: 258738 Color: 1184

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 452886 Color: 9235
Size: 288035 Color: 3443
Size: 259080 Color: 1219

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 452897 Color: 9236
Size: 288004 Color: 3440
Size: 259100 Color: 1221

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 453028 Color: 9237
Size: 276038 Color: 2644
Size: 270935 Color: 2240

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 453074 Color: 9238
Size: 289440 Color: 3532
Size: 257487 Color: 1052

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 453095 Color: 9239
Size: 281687 Color: 3032
Size: 265219 Color: 1761

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 453126 Color: 9240
Size: 284608 Color: 3210
Size: 262267 Color: 1531

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 453135 Color: 9241
Size: 284558 Color: 3205
Size: 262308 Color: 1537

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 453157 Color: 9242
Size: 291285 Color: 3644
Size: 255559 Color: 802

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 453246 Color: 9243
Size: 293158 Color: 3760
Size: 253597 Color: 566

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 453283 Color: 9244
Size: 286767 Color: 3362
Size: 259951 Color: 1308

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 453329 Color: 9245
Size: 290829 Color: 3612
Size: 255843 Color: 843

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 453374 Color: 9246
Size: 291718 Color: 3674
Size: 254909 Color: 732

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 453394 Color: 9247
Size: 277629 Color: 2760
Size: 268978 Color: 2076

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 453557 Color: 9248
Size: 288321 Color: 3462
Size: 258123 Color: 1124

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 453575 Color: 9249
Size: 287186 Color: 3383
Size: 259240 Color: 1243

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 453611 Color: 9250
Size: 288333 Color: 3464
Size: 258057 Color: 1117

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 453697 Color: 9251
Size: 283533 Color: 3149
Size: 262771 Color: 1572

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 453731 Color: 9252
Size: 277464 Color: 2751
Size: 268806 Color: 2063

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 453797 Color: 9253
Size: 280732 Color: 2972
Size: 265472 Color: 1781

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 453939 Color: 9254
Size: 285284 Color: 3254
Size: 260778 Color: 1399

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 453944 Color: 9255
Size: 292141 Color: 3706
Size: 253916 Color: 604

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 454000 Color: 9256
Size: 280115 Color: 2928
Size: 265886 Color: 1815

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 454063 Color: 9257
Size: 279715 Color: 2897
Size: 266223 Color: 1843

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 454079 Color: 9258
Size: 287808 Color: 3424
Size: 258114 Color: 1123

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 454168 Color: 9259
Size: 284853 Color: 3227
Size: 260980 Color: 1420

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 454175 Color: 9260
Size: 273426 Color: 2441
Size: 272400 Color: 2363

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 454180 Color: 9261
Size: 286436 Color: 3345
Size: 259385 Color: 1256

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 454203 Color: 9262
Size: 275596 Color: 2608
Size: 270202 Color: 2179

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 454205 Color: 9263
Size: 285556 Color: 3278
Size: 260240 Color: 1342

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 454246 Color: 9264
Size: 281295 Color: 3004
Size: 264460 Color: 1702

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 454270 Color: 9265
Size: 284355 Color: 3194
Size: 261376 Color: 1456

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 454309 Color: 9266
Size: 294235 Color: 3824
Size: 251457 Color: 256

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 454404 Color: 9267
Size: 277235 Color: 2735
Size: 268362 Color: 2029

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 454437 Color: 9268
Size: 283900 Color: 3162
Size: 261664 Color: 1482

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 454488 Color: 9269
Size: 290983 Color: 3627
Size: 254530 Color: 690

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 454581 Color: 9270
Size: 273443 Color: 2443
Size: 271977 Color: 2328

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 454708 Color: 9271
Size: 280211 Color: 2937
Size: 265082 Color: 1751

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 454747 Color: 9272
Size: 291776 Color: 3683
Size: 253478 Color: 553

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 454783 Color: 9273
Size: 287572 Color: 3406
Size: 257646 Color: 1068

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 454787 Color: 9274
Size: 288481 Color: 3469
Size: 256733 Color: 962

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 454816 Color: 9275
Size: 275850 Color: 2626
Size: 269335 Color: 2106

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 454845 Color: 9276
Size: 279145 Color: 2868
Size: 266011 Color: 1831

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 454859 Color: 9277
Size: 282931 Color: 3109
Size: 262211 Color: 1525

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 455069 Color: 9278
Size: 284841 Color: 3225
Size: 260091 Color: 1325

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 455098 Color: 9279
Size: 281546 Color: 3019
Size: 263357 Color: 1616

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 455146 Color: 9280
Size: 280012 Color: 2920
Size: 264843 Color: 1724

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 455162 Color: 9281
Size: 283960 Color: 3168
Size: 260879 Color: 1410

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 455261 Color: 9282
Size: 286392 Color: 3338
Size: 258348 Color: 1145

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 455311 Color: 9283
Size: 285947 Color: 3309
Size: 258743 Color: 1186

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 455327 Color: 9284
Size: 278787 Color: 2850
Size: 265887 Color: 1816

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 455468 Color: 9285
Size: 287085 Color: 3379
Size: 257448 Color: 1046

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 455548 Color: 9286
Size: 275129 Color: 2582
Size: 269324 Color: 2104

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 455561 Color: 9287
Size: 278864 Color: 2853
Size: 265576 Color: 1791

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 455737 Color: 9288
Size: 286115 Color: 3321
Size: 258149 Color: 1130

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 455791 Color: 9289
Size: 280721 Color: 2970
Size: 263489 Color: 1632

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 455807 Color: 9290
Size: 285264 Color: 3253
Size: 258930 Color: 1206

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 455821 Color: 9292
Size: 288832 Color: 3486
Size: 255348 Color: 777

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 455821 Color: 9291
Size: 284576 Color: 3207
Size: 259604 Color: 1276

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 455869 Color: 9293
Size: 273928 Color: 2483
Size: 270204 Color: 2180

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 455920 Color: 9294
Size: 292551 Color: 3727
Size: 251530 Color: 272

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 455979 Color: 9295
Size: 280197 Color: 2935
Size: 263825 Color: 1652

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 456080 Color: 9296
Size: 291365 Color: 3648
Size: 252556 Color: 427

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 456086 Color: 9297
Size: 292650 Color: 3732
Size: 251265 Color: 232

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 456106 Color: 9298
Size: 273873 Color: 2474
Size: 270022 Color: 2169

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 456177 Color: 9299
Size: 288092 Color: 3448
Size: 255732 Color: 829

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 456214 Color: 9300
Size: 280282 Color: 2942
Size: 263505 Color: 1633

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 456247 Color: 9301
Size: 279715 Color: 2898
Size: 264039 Color: 1668

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 456306 Color: 9302
Size: 275048 Color: 2572
Size: 268647 Color: 2048

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 456373 Color: 9303
Size: 273695 Color: 2458
Size: 269933 Color: 2161

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 456445 Color: 9304
Size: 282576 Color: 3089
Size: 260980 Color: 1421

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 456457 Color: 9305
Size: 278714 Color: 2844
Size: 264830 Color: 1723

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 456518 Color: 9306
Size: 292961 Color: 3752
Size: 250522 Color: 93

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 456531 Color: 9307
Size: 276686 Color: 2699
Size: 266784 Color: 1887

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 456597 Color: 9308
Size: 290003 Color: 3561
Size: 253401 Color: 540

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 456601 Color: 9309
Size: 286789 Color: 3363
Size: 256611 Color: 946

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 456649 Color: 9310
Size: 293349 Color: 3772
Size: 250003 Color: 1

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 456660 Color: 9311
Size: 282591 Color: 3090
Size: 260750 Color: 1394

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 456670 Color: 9312
Size: 276165 Color: 2652
Size: 267166 Color: 1924

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 456671 Color: 9313
Size: 287732 Color: 3419
Size: 255598 Color: 809

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 456677 Color: 9314
Size: 290323 Color: 3578
Size: 253001 Color: 487

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 456772 Color: 9315
Size: 281026 Color: 2989
Size: 262203 Color: 1524

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 456819 Color: 9316
Size: 283293 Color: 3131
Size: 259889 Color: 1302

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 456843 Color: 9317
Size: 291732 Color: 3679
Size: 251426 Color: 249

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 456861 Color: 9318
Size: 284951 Color: 3232
Size: 258189 Color: 1135

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 456939 Color: 9319
Size: 288292 Color: 3461
Size: 254770 Color: 712

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 456973 Color: 9320
Size: 275071 Color: 2578
Size: 267957 Color: 1988

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 457176 Color: 9321
Size: 273688 Color: 2456
Size: 269137 Color: 2089

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 457211 Color: 9322
Size: 274591 Color: 2531
Size: 268199 Color: 2016

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 457306 Color: 9323
Size: 278697 Color: 2842
Size: 263998 Color: 1665

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 457386 Color: 9324
Size: 277187 Color: 2733
Size: 265428 Color: 1779

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 457539 Color: 9325
Size: 278524 Color: 2829
Size: 263938 Color: 1663

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 457600 Color: 9326
Size: 276609 Color: 2693
Size: 265792 Color: 1808

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 457606 Color: 9327
Size: 287517 Color: 3404
Size: 254878 Color: 727

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 457646 Color: 9328
Size: 282002 Color: 3055
Size: 260353 Color: 1360

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 457661 Color: 9329
Size: 278311 Color: 2808
Size: 264029 Color: 1666

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 457734 Color: 9330
Size: 282217 Color: 3070
Size: 260050 Color: 1317

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 457800 Color: 9331
Size: 290064 Color: 3566
Size: 252137 Color: 371

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 457945 Color: 9332
Size: 283518 Color: 3146
Size: 258538 Color: 1162

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 457996 Color: 9333
Size: 282988 Color: 3114
Size: 259017 Color: 1214

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 458057 Color: 9334
Size: 279864 Color: 2910
Size: 262080 Color: 1511

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 458072 Color: 9335
Size: 279834 Color: 2906
Size: 262095 Color: 1516

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 458178 Color: 9336
Size: 285870 Color: 3299
Size: 255953 Color: 856

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 458190 Color: 9337
Size: 272346 Color: 2359
Size: 269465 Color: 2116

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 458294 Color: 9338
Size: 287247 Color: 3386
Size: 254460 Color: 682

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 458370 Color: 9339
Size: 289988 Color: 3559
Size: 251643 Color: 293

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 458386 Color: 9340
Size: 287684 Color: 3413
Size: 253931 Color: 609

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 458396 Color: 9341
Size: 284688 Color: 3215
Size: 256917 Color: 986

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 458619 Color: 9342
Size: 289337 Color: 3523
Size: 252045 Color: 356

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 458645 Color: 9343
Size: 290633 Color: 3594
Size: 250723 Color: 130

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 458670 Color: 9344
Size: 289866 Color: 3555
Size: 251465 Color: 259

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 458736 Color: 9345
Size: 288235 Color: 3458
Size: 253030 Color: 489

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 458741 Color: 9346
Size: 273353 Color: 2437
Size: 267907 Color: 1982

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 458776 Color: 9347
Size: 284532 Color: 3204
Size: 256693 Color: 955

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 458779 Color: 9348
Size: 289558 Color: 3537
Size: 251664 Color: 295

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 458905 Color: 9349
Size: 280897 Color: 2983
Size: 260199 Color: 1337

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 458950 Color: 9350
Size: 281022 Color: 2988
Size: 260029 Color: 1314

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 458959 Color: 9351
Size: 280743 Color: 2973
Size: 260299 Color: 1352

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 458998 Color: 9352
Size: 283172 Color: 3121
Size: 257831 Color: 1094

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 459046 Color: 9353
Size: 289739 Color: 3548
Size: 251216 Color: 224

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 459200 Color: 9354
Size: 281111 Color: 2993
Size: 259690 Color: 1292

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 459203 Color: 9355
Size: 273754 Color: 2462
Size: 267044 Color: 1915

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 459244 Color: 9356
Size: 272062 Color: 2339
Size: 268695 Color: 2050

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 459266 Color: 9357
Size: 272550 Color: 2377
Size: 268185 Color: 2014

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 459500 Color: 9358
Size: 271947 Color: 2325
Size: 268554 Color: 2040

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 459510 Color: 9359
Size: 284393 Color: 3200
Size: 256098 Color: 873

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 459568 Color: 9360
Size: 281678 Color: 3030
Size: 258755 Color: 1188

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 459615 Color: 9361
Size: 285514 Color: 3274
Size: 254872 Color: 726

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 459624 Color: 9362
Size: 285945 Color: 3308
Size: 254432 Color: 677

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 459638 Color: 9363
Size: 285022 Color: 3236
Size: 255341 Color: 776

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 459649 Color: 9364
Size: 283114 Color: 3118
Size: 257238 Color: 1024

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 459702 Color: 9365
Size: 275034 Color: 2570
Size: 265265 Color: 1767

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 459753 Color: 9366
Size: 286150 Color: 3325
Size: 254098 Color: 631

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 459785 Color: 9367
Size: 274530 Color: 2526
Size: 265686 Color: 1800

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 459852 Color: 9368
Size: 278977 Color: 2856
Size: 261172 Color: 1436

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 459928 Color: 9369
Size: 270910 Color: 2238
Size: 269163 Color: 2090

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 459977 Color: 9370
Size: 278339 Color: 2813
Size: 261685 Color: 1485

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 460103 Color: 9371
Size: 287709 Color: 3416
Size: 252189 Color: 376

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 460153 Color: 9372
Size: 270936 Color: 2241
Size: 268912 Color: 2074

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 460423 Color: 9373
Size: 282976 Color: 3112
Size: 256602 Color: 945

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 460469 Color: 9374
Size: 271602 Color: 2302
Size: 267930 Color: 1984

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 460484 Color: 9375
Size: 280896 Color: 2982
Size: 258621 Color: 1169

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 460518 Color: 9376
Size: 287386 Color: 3398
Size: 252097 Color: 365

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 460573 Color: 9377
Size: 285589 Color: 3280
Size: 253839 Color: 592

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 460610 Color: 9378
Size: 269910 Color: 2158
Size: 269481 Color: 2118

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 460660 Color: 9379
Size: 276864 Color: 2713
Size: 262477 Color: 1551

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 460723 Color: 9380
Size: 285473 Color: 3271
Size: 253805 Color: 590

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 460739 Color: 9381
Size: 286123 Color: 3323
Size: 253139 Color: 512

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 460745 Color: 9382
Size: 287556 Color: 3405
Size: 251700 Color: 301

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 460787 Color: 9383
Size: 285822 Color: 3296
Size: 253392 Color: 539

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 460803 Color: 9384
Size: 282061 Color: 3059
Size: 257137 Color: 1014

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 460830 Color: 9385
Size: 275301 Color: 2594
Size: 263870 Color: 1658

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 460867 Color: 9386
Size: 277409 Color: 2746
Size: 261725 Color: 1487

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 460870 Color: 9387
Size: 276081 Color: 2647
Size: 263050 Color: 1597

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 460905 Color: 9388
Size: 276573 Color: 2690
Size: 262523 Color: 1553

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 460918 Color: 9389
Size: 279520 Color: 2890
Size: 259563 Color: 1273

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 461046 Color: 9390
Size: 283239 Color: 3127
Size: 255716 Color: 827

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 461227 Color: 9391
Size: 279150 Color: 2870
Size: 259624 Color: 1283

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 461238 Color: 9392
Size: 281991 Color: 3054
Size: 256772 Color: 967

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 461272 Color: 9393
Size: 284045 Color: 3176
Size: 254684 Color: 700

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 461329 Color: 9394
Size: 278322 Color: 2810
Size: 260350 Color: 1359

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 461341 Color: 9395
Size: 278459 Color: 2824
Size: 260201 Color: 1338

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 461362 Color: 9396
Size: 282644 Color: 3095
Size: 255995 Color: 863

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 461408 Color: 9397
Size: 272402 Color: 2364
Size: 266191 Color: 1841

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 461423 Color: 9398
Size: 282005 Color: 3057
Size: 256573 Color: 942

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 461450 Color: 9399
Size: 273855 Color: 2469
Size: 264696 Color: 1713

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 461465 Color: 9400
Size: 271453 Color: 2285
Size: 267083 Color: 1920

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 461563 Color: 9401
Size: 275975 Color: 2636
Size: 262463 Color: 1548

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 461584 Color: 9402
Size: 285644 Color: 3287
Size: 252773 Color: 454

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 461605 Color: 9403
Size: 288028 Color: 3442
Size: 250368 Color: 64

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 461657 Color: 9404
Size: 277308 Color: 2738
Size: 261036 Color: 1429

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 461718 Color: 9405
Size: 279147 Color: 2869
Size: 259136 Color: 1227

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 461756 Color: 9406
Size: 288183 Color: 3455
Size: 250062 Color: 12

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 461893 Color: 9407
Size: 276492 Color: 2685
Size: 261616 Color: 1479

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 461915 Color: 9408
Size: 286095 Color: 3319
Size: 251991 Color: 350

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 461927 Color: 9409
Size: 271453 Color: 2286
Size: 266621 Color: 1873

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 461985 Color: 9410
Size: 272690 Color: 2384
Size: 265326 Color: 1772

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 462015 Color: 9411
Size: 283205 Color: 3123
Size: 254781 Color: 714

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 462064 Color: 9412
Size: 275175 Color: 2587
Size: 262762 Color: 1569

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 462131 Color: 9413
Size: 285211 Color: 3249
Size: 252659 Color: 444

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 462167 Color: 9414
Size: 286389 Color: 3336
Size: 251445 Color: 253

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 462263 Color: 9415
Size: 283908 Color: 3163
Size: 253830 Color: 591

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 462381 Color: 9416
Size: 277147 Color: 2729
Size: 260473 Color: 1370

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 462464 Color: 9417
Size: 280865 Color: 2979
Size: 256672 Color: 953

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 462490 Color: 9418
Size: 275943 Color: 2632
Size: 261568 Color: 1473

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 462579 Color: 9419
Size: 276228 Color: 2661
Size: 261194 Color: 1438

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 462622 Color: 9420
Size: 273916 Color: 2482
Size: 263463 Color: 1630

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 462705 Color: 9421
Size: 276937 Color: 2717
Size: 260359 Color: 1361

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 462786 Color: 9422
Size: 278313 Color: 2809
Size: 258902 Color: 1204

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 462982 Color: 9423
Size: 269318 Color: 2102
Size: 267701 Color: 1966

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 463084 Color: 9424
Size: 279239 Color: 2879
Size: 257678 Color: 1070

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 463121 Color: 9425
Size: 279852 Color: 2908
Size: 257028 Color: 1000

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 463403 Color: 9426
Size: 281783 Color: 3041
Size: 254815 Color: 719

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 463513 Color: 9427
Size: 280895 Color: 2981
Size: 255593 Color: 808

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 463581 Color: 9428
Size: 268223 Color: 2019
Size: 268197 Color: 2015

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 463601 Color: 9429
Size: 273822 Color: 2467
Size: 262578 Color: 1558

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 463658 Color: 9430
Size: 279913 Color: 2913
Size: 256430 Color: 928

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 463672 Color: 9431
Size: 275610 Color: 2610
Size: 260719 Color: 1392

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 463834 Color: 9432
Size: 283290 Color: 3130
Size: 252877 Color: 470

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 463845 Color: 9433
Size: 270196 Color: 2178
Size: 265960 Color: 1826

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 463873 Color: 9434
Size: 271890 Color: 2323
Size: 264238 Color: 1686

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 463892 Color: 9435
Size: 277658 Color: 2763
Size: 258451 Color: 1158

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 463903 Color: 9436
Size: 283268 Color: 3128
Size: 252830 Color: 462

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 463906 Color: 9437
Size: 277256 Color: 2736
Size: 258839 Color: 1194

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 464028 Color: 9438
Size: 278295 Color: 2806
Size: 257678 Color: 1071

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 464066 Color: 9439
Size: 282531 Color: 3084
Size: 253404 Color: 541

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 464180 Color: 9440
Size: 279516 Color: 2889
Size: 256305 Color: 907

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 464208 Color: 9441
Size: 272354 Color: 2361
Size: 263439 Color: 1627

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 464243 Color: 9442
Size: 278405 Color: 2819
Size: 257353 Color: 1036

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 464355 Color: 9443
Size: 277871 Color: 2774
Size: 257775 Color: 1089

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 464389 Color: 9444
Size: 284380 Color: 3198
Size: 251232 Color: 226

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 464423 Color: 9445
Size: 273857 Color: 2470
Size: 261721 Color: 1486

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 464546 Color: 9446
Size: 283180 Color: 3122
Size: 252275 Color: 388

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 464708 Color: 9447
Size: 271446 Color: 2284
Size: 263847 Color: 1655

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 464851 Color: 9448
Size: 275886 Color: 2629
Size: 259264 Color: 1245

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 464888 Color: 9449
Size: 284388 Color: 3199
Size: 250725 Color: 131

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 464975 Color: 9450
Size: 281888 Color: 3048
Size: 253138 Color: 511

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 465090 Color: 9451
Size: 278040 Color: 2791
Size: 256871 Color: 982

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 465096 Color: 9452
Size: 281765 Color: 3039
Size: 253140 Color: 513

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 465212 Color: 9453
Size: 274694 Color: 2539
Size: 260095 Color: 1326

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 465372 Color: 9454
Size: 272721 Color: 2387
Size: 261908 Color: 1499

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 465383 Color: 9455
Size: 280371 Color: 2948
Size: 254247 Color: 651

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 465414 Color: 9456
Size: 273329 Color: 2434
Size: 261258 Color: 1444

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 465527 Color: 9457
Size: 277278 Color: 2737
Size: 257196 Color: 1020

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 465537 Color: 9458
Size: 273169 Color: 2420
Size: 261295 Color: 1449

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 465543 Color: 9459
Size: 282106 Color: 3064
Size: 252352 Color: 396

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 465567 Color: 9460
Size: 281357 Color: 3007
Size: 253077 Color: 503

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 465575 Color: 9461
Size: 269306 Color: 2101
Size: 265120 Color: 1754

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 465595 Color: 9462
Size: 276689 Color: 2700
Size: 257717 Color: 1076

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 465677 Color: 9463
Size: 268769 Color: 2059
Size: 265555 Color: 1788

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 465816 Color: 9464
Size: 281922 Color: 3050
Size: 252263 Color: 386

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 465836 Color: 9465
Size: 269852 Color: 2153
Size: 264313 Color: 1695

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 466091 Color: 9466
Size: 277137 Color: 2728
Size: 256773 Color: 968

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 466111 Color: 9467
Size: 270035 Color: 2170
Size: 263855 Color: 1656

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 466190 Color: 9468
Size: 280872 Color: 2980
Size: 252939 Color: 480

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 466207 Color: 9469
Size: 281759 Color: 3037
Size: 252035 Color: 353

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 466274 Color: 9470
Size: 275194 Color: 2589
Size: 258533 Color: 1161

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 466285 Color: 9471
Size: 280809 Color: 2976
Size: 252907 Color: 473

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 466319 Color: 9472
Size: 271597 Color: 2301
Size: 262085 Color: 1512

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 466320 Color: 9473
Size: 283492 Color: 3144
Size: 250189 Color: 37

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 466324 Color: 9474
Size: 275005 Color: 2564
Size: 258672 Color: 1175

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 466328 Color: 9475
Size: 278570 Color: 2832
Size: 255103 Color: 748

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 466360 Color: 9476
Size: 281071 Color: 2992
Size: 252570 Color: 430

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 466397 Color: 9477
Size: 280183 Color: 2932
Size: 253421 Color: 544

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 466437 Color: 9478
Size: 269340 Color: 2107
Size: 264224 Color: 1683

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 466447 Color: 9479
Size: 276221 Color: 2659
Size: 257333 Color: 1035

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 466476 Color: 9480
Size: 274543 Color: 2528
Size: 258982 Color: 1210

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 466504 Color: 9481
Size: 273211 Color: 2425
Size: 260286 Color: 1350

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 466572 Color: 9482
Size: 268544 Color: 2039
Size: 264885 Color: 1727

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 466579 Color: 9483
Size: 278017 Color: 2788
Size: 255405 Color: 784

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 466634 Color: 9484
Size: 274501 Color: 2525
Size: 258866 Color: 1201

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 466781 Color: 9485
Size: 271632 Color: 2304
Size: 261588 Color: 1475

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 466787 Color: 9486
Size: 277364 Color: 2744
Size: 255850 Color: 845

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 466865 Color: 9487
Size: 278373 Color: 2816
Size: 254763 Color: 709

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 466866 Color: 9488
Size: 281316 Color: 3005
Size: 251819 Color: 325

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 466878 Color: 9489
Size: 275107 Color: 2580
Size: 258016 Color: 1112

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 466901 Color: 9490
Size: 281823 Color: 3044
Size: 251277 Color: 236

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 466978 Color: 9491
Size: 282091 Color: 3061
Size: 250932 Color: 175

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 467180 Color: 9492
Size: 275109 Color: 2581
Size: 257712 Color: 1075

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 467211 Color: 9493
Size: 270850 Color: 2231
Size: 261940 Color: 1502

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 467291 Color: 9494
Size: 276197 Color: 2657
Size: 256513 Color: 938

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 467534 Color: 9495
Size: 271169 Color: 2263
Size: 261298 Color: 1450

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 467578 Color: 9496
Size: 278085 Color: 2792
Size: 254338 Color: 660

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 467583 Color: 9497
Size: 271271 Color: 2271
Size: 261147 Color: 1434

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 467673 Color: 9498
Size: 275004 Color: 2563
Size: 257324 Color: 1033

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 467698 Color: 9499
Size: 279173 Color: 2873
Size: 253130 Color: 508

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 467698 Color: 9500
Size: 274755 Color: 2545
Size: 257548 Color: 1057

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 467903 Color: 9501
Size: 267122 Color: 1921
Size: 264976 Color: 1735

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 467947 Color: 9502
Size: 267435 Color: 1945
Size: 264619 Color: 1709

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 468035 Color: 9503
Size: 274223 Color: 2494
Size: 257743 Color: 1080

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 468084 Color: 9504
Size: 278822 Color: 2851
Size: 253095 Color: 506

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 468125 Color: 9505
Size: 280299 Color: 2944
Size: 251577 Color: 279

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 468182 Color: 9506
Size: 267657 Color: 1960
Size: 264162 Color: 1678

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 468193 Color: 9507
Size: 280293 Color: 2943
Size: 251515 Color: 270

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 468267 Color: 9508
Size: 274462 Color: 2523
Size: 257272 Color: 1027

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 468383 Color: 9509
Size: 268259 Color: 2022
Size: 263359 Color: 1617

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 468470 Color: 9510
Size: 266298 Color: 1849
Size: 265233 Color: 1762

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 468501 Color: 9511
Size: 273195 Color: 2423
Size: 258305 Color: 1143

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 468614 Color: 9512
Size: 279818 Color: 2903
Size: 251569 Color: 278

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 468625 Color: 9513
Size: 279863 Color: 2909
Size: 251513 Color: 269

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 468832 Color: 9514
Size: 276672 Color: 2698
Size: 254497 Color: 687

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 468832 Color: 9515
Size: 278337 Color: 2812
Size: 252832 Color: 463

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 468923 Color: 9516
Size: 274392 Color: 2513
Size: 256686 Color: 954

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 468967 Color: 9517
Size: 274420 Color: 2517
Size: 256614 Color: 947

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 468978 Color: 9518
Size: 269177 Color: 2092
Size: 261846 Color: 1494

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 469033 Color: 9519
Size: 278119 Color: 2797
Size: 252849 Color: 466

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 469110 Color: 9520
Size: 271249 Color: 2267
Size: 259642 Color: 1285

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 469138 Color: 9521
Size: 273176 Color: 2421
Size: 257687 Color: 1072

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 469193 Color: 9522
Size: 268530 Color: 2037
Size: 262278 Color: 1533

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 469200 Color: 9523
Size: 269534 Color: 2123
Size: 261267 Color: 1446

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 469224 Color: 9524
Size: 271571 Color: 2300
Size: 259206 Color: 1238

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 469229 Color: 9525
Size: 271797 Color: 2316
Size: 258975 Color: 1209

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 469236 Color: 9526
Size: 279142 Color: 2867
Size: 251623 Color: 289

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 469278 Color: 9527
Size: 270338 Color: 2192
Size: 260385 Color: 1365

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 469359 Color: 9528
Size: 279821 Color: 2904
Size: 250821 Color: 152

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 469370 Color: 9529
Size: 276181 Color: 2656
Size: 254450 Color: 680

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 469558 Color: 9530
Size: 268510 Color: 2036
Size: 261933 Color: 1501

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 469621 Color: 9531
Size: 276609 Color: 2692
Size: 253771 Color: 585

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 469634 Color: 9532
Size: 265644 Color: 1796
Size: 264723 Color: 1716

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 469845 Color: 9533
Size: 270963 Color: 2246
Size: 259193 Color: 1236

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 469867 Color: 9534
Size: 272984 Color: 2405
Size: 257150 Color: 1017

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 469890 Color: 9535
Size: 267481 Color: 1949
Size: 262630 Color: 1563

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 469904 Color: 9536
Size: 279268 Color: 2880
Size: 250829 Color: 157

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 470239 Color: 9537
Size: 268140 Color: 2009
Size: 261622 Color: 1480

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 470317 Color: 9538
Size: 268108 Color: 2002
Size: 261576 Color: 1474

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 470361 Color: 9539
Size: 269354 Color: 2109
Size: 260286 Color: 1349

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 470375 Color: 9540
Size: 272926 Color: 2400
Size: 256700 Color: 958

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 470473 Color: 9541
Size: 271952 Color: 2327
Size: 257576 Color: 1060

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 470489 Color: 9542
Size: 271473 Color: 2287
Size: 258039 Color: 1115

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 470505 Color: 9543
Size: 272523 Color: 2372
Size: 256973 Color: 993

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 470663 Color: 9544
Size: 273873 Color: 2475
Size: 255465 Color: 792

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 470718 Color: 9545
Size: 272167 Color: 2349
Size: 257116 Color: 1011

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 470795 Color: 9546
Size: 271271 Color: 2272
Size: 257935 Color: 1104

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 470856 Color: 9547
Size: 273037 Color: 2409
Size: 256108 Color: 874

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 470889 Color: 9548
Size: 278412 Color: 2820
Size: 250700 Color: 128

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 471131 Color: 9549
Size: 265653 Color: 1799
Size: 263217 Color: 1608

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 471161 Color: 9550
Size: 264881 Color: 1725
Size: 263959 Color: 1664

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 471216 Color: 9551
Size: 272641 Color: 2382
Size: 256144 Color: 879

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 471296 Color: 9552
Size: 271927 Color: 2324
Size: 256778 Color: 969

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 471349 Color: 9553
Size: 268589 Color: 2044
Size: 260063 Color: 1320

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 471390 Color: 9554
Size: 272057 Color: 2338
Size: 256554 Color: 940

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 471396 Color: 9555
Size: 264969 Color: 1734
Size: 263636 Color: 1639

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 471445 Color: 9556
Size: 274390 Color: 2512
Size: 254166 Color: 638

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 471445 Color: 9557
Size: 275487 Color: 2601
Size: 253069 Color: 500

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 471478 Color: 9558
Size: 265046 Color: 1744
Size: 263477 Color: 1631

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 471507 Color: 9559
Size: 272545 Color: 2374
Size: 255949 Color: 855

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 471543 Color: 9560
Size: 269452 Color: 2114
Size: 259006 Color: 1211

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 471601 Color: 9561
Size: 276473 Color: 2681
Size: 251927 Color: 337

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 471649 Color: 9562
Size: 276520 Color: 2686
Size: 251832 Color: 327

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 471655 Color: 9563
Size: 264576 Color: 1708
Size: 263770 Color: 1646

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 471696 Color: 9564
Size: 268791 Color: 2061
Size: 259514 Color: 1266

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 471705 Color: 9565
Size: 272876 Color: 2398
Size: 255420 Color: 787

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 471776 Color: 9566
Size: 277994 Color: 2787
Size: 250231 Color: 47

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 471823 Color: 9567
Size: 272290 Color: 2357
Size: 255888 Color: 849

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 471965 Color: 9568
Size: 273887 Color: 2478
Size: 254149 Color: 633

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 472000 Color: 9569
Size: 270701 Color: 2222
Size: 257300 Color: 1029

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 472056 Color: 9570
Size: 272244 Color: 2354
Size: 255701 Color: 825

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 472064 Color: 9571
Size: 267412 Color: 1943
Size: 260525 Color: 1377

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 472065 Color: 9572
Size: 274296 Color: 2501
Size: 253640 Color: 570

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 472104 Color: 9573
Size: 267828 Color: 1976
Size: 260069 Color: 1323

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 472136 Color: 9574
Size: 267655 Color: 1958
Size: 260210 Color: 1339

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 472209 Color: 9575
Size: 270796 Color: 2230
Size: 256996 Color: 996

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 472307 Color: 9576
Size: 270665 Color: 2219
Size: 257029 Color: 1001

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 472312 Color: 9577
Size: 266807 Color: 1893
Size: 260882 Color: 1411

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 472332 Color: 9578
Size: 268646 Color: 2047
Size: 259023 Color: 1216

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 472385 Color: 9579
Size: 268142 Color: 2010
Size: 259474 Color: 1262

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 472460 Color: 9580
Size: 274406 Color: 2515
Size: 253135 Color: 510

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 472542 Color: 9581
Size: 270867 Color: 2235
Size: 256592 Color: 944

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 472546 Color: 9582
Size: 264779 Color: 1719
Size: 262676 Color: 1565

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 472548 Color: 9583
Size: 276338 Color: 2670
Size: 251115 Color: 205

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 472554 Color: 9584
Size: 277353 Color: 2742
Size: 250094 Color: 18

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 472564 Color: 9585
Size: 270540 Color: 2210
Size: 256897 Color: 983

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 472634 Color: 9586
Size: 265474 Color: 1782
Size: 261893 Color: 1497

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 472635 Color: 9587
Size: 273138 Color: 2417
Size: 254228 Color: 647

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 472718 Color: 9588
Size: 274641 Color: 2534
Size: 252642 Color: 441

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 472722 Color: 9589
Size: 274317 Color: 2502
Size: 252962 Color: 485

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 472759 Color: 9590
Size: 270332 Color: 2190
Size: 256910 Color: 985

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 472760 Color: 9591
Size: 276297 Color: 2665
Size: 250944 Color: 177

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 472905 Color: 9592
Size: 275055 Color: 2576
Size: 252041 Color: 354

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 473025 Color: 9593
Size: 268534 Color: 2038
Size: 258442 Color: 1154

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 473110 Color: 9594
Size: 263935 Color: 1662
Size: 262956 Color: 1588

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 473132 Color: 9595
Size: 271410 Color: 2282
Size: 255459 Color: 791

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 473241 Color: 9596
Size: 275050 Color: 2573
Size: 251710 Color: 306

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 473337 Color: 9597
Size: 271085 Color: 2253
Size: 255579 Color: 806

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 473361 Color: 9598
Size: 271284 Color: 2273
Size: 255356 Color: 778

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 473371 Color: 9599
Size: 270851 Color: 2232
Size: 255779 Color: 836

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 473396 Color: 9600
Size: 267942 Color: 1986
Size: 258663 Color: 1174

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 473407 Color: 9601
Size: 274803 Color: 2550
Size: 251791 Color: 321

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 473416 Color: 9602
Size: 267739 Color: 1971
Size: 258846 Color: 1195

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 473518 Color: 9603
Size: 273868 Color: 2473
Size: 252615 Color: 438

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 473543 Color: 9604
Size: 270421 Color: 2199
Size: 256037 Color: 870

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 473546 Color: 9605
Size: 273501 Color: 2446
Size: 252954 Color: 483

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 473562 Color: 9606
Size: 270446 Color: 2201
Size: 255993 Color: 862

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 473564 Color: 9607
Size: 267418 Color: 1944
Size: 259019 Color: 1215

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 473565 Color: 9608
Size: 264234 Color: 1685
Size: 262202 Color: 1522

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 473576 Color: 9609
Size: 264454 Color: 1699
Size: 261971 Color: 1505

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 473820 Color: 9610
Size: 273805 Color: 2465
Size: 252376 Color: 400

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 473879 Color: 9611
Size: 272975 Color: 2403
Size: 253147 Color: 515

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 473930 Color: 9612
Size: 273814 Color: 2466
Size: 252257 Color: 385

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 473976 Color: 9613
Size: 269514 Color: 2120
Size: 256511 Color: 937

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 474034 Color: 9614
Size: 271977 Color: 2329
Size: 253990 Color: 617

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 474063 Color: 9615
Size: 272014 Color: 2335
Size: 253924 Color: 606

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 474064 Color: 9616
Size: 275295 Color: 2593
Size: 250642 Color: 117

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 474216 Color: 9617
Size: 269698 Color: 2138
Size: 256087 Color: 872

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 474250 Color: 9618
Size: 268901 Color: 2073
Size: 256850 Color: 978

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 474373 Color: 9619
Size: 271253 Color: 2268
Size: 254375 Color: 667

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 474381 Color: 9620
Size: 269802 Color: 2148
Size: 255818 Color: 841

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 474492 Color: 9621
Size: 272592 Color: 2380
Size: 252917 Color: 475

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 474502 Color: 9622
Size: 272421 Color: 2366
Size: 253078 Color: 504

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 474516 Color: 9623
Size: 275238 Color: 2591
Size: 250247 Color: 49

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 474531 Color: 9624
Size: 263708 Color: 1644
Size: 261762 Color: 1489

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 474615 Color: 9625
Size: 264895 Color: 1728
Size: 260491 Color: 1372

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 474647 Color: 9626
Size: 274782 Color: 2547
Size: 250572 Color: 103

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 474650 Color: 9627
Size: 263402 Color: 1622
Size: 261949 Color: 1504

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 474720 Color: 9628
Size: 264575 Color: 1707
Size: 260706 Color: 1390

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 474858 Color: 9629
Size: 273454 Color: 2444
Size: 251689 Color: 297

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 475111 Color: 9630
Size: 265073 Color: 1750
Size: 259817 Color: 1295

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 475121 Color: 9631
Size: 269786 Color: 2145
Size: 255094 Color: 746

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 475144 Color: 9632
Size: 273062 Color: 2411
Size: 251795 Color: 322

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 475162 Color: 9633
Size: 271035 Color: 2250
Size: 253804 Color: 589

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 475291 Color: 9634
Size: 269658 Color: 2135
Size: 255052 Color: 744

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 475394 Color: 9635
Size: 269083 Color: 2083
Size: 255524 Color: 801

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 475488 Color: 9636
Size: 264455 Color: 1700
Size: 260058 Color: 1318

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 475554 Color: 9637
Size: 271128 Color: 2260
Size: 253319 Color: 528

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 475648 Color: 9638
Size: 270001 Color: 2166
Size: 254352 Color: 662

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 475649 Color: 9639
Size: 268743 Color: 2056
Size: 255609 Color: 812

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 475740 Color: 9640
Size: 270674 Color: 2221
Size: 253587 Color: 563

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 475796 Color: 9641
Size: 267367 Color: 1939
Size: 256838 Color: 977

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 475833 Color: 9642
Size: 266759 Color: 1885
Size: 257409 Color: 1043

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 475836 Color: 9643
Size: 267246 Color: 1932
Size: 256919 Color: 987

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 475986 Color: 9644
Size: 267798 Color: 1975
Size: 256217 Color: 890

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 476106 Color: 9645
Size: 271553 Color: 2297
Size: 252342 Color: 395

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 476114 Color: 9646
Size: 268249 Color: 2021
Size: 255638 Color: 814

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 476309 Color: 9647
Size: 267908 Color: 1983
Size: 255784 Color: 837

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 476336 Color: 9648
Size: 266946 Color: 1909
Size: 256719 Color: 960

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 476527 Color: 9649
Size: 269261 Color: 2099
Size: 254213 Color: 645

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 476575 Color: 9650
Size: 263388 Color: 1619
Size: 260038 Color: 1316

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 476598 Color: 9651
Size: 272172 Color: 2350
Size: 251231 Color: 225

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 476622 Color: 9652
Size: 267680 Color: 1961
Size: 255699 Color: 823

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 476696 Color: 9653
Size: 270129 Color: 2174
Size: 253176 Color: 520

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 476701 Color: 9654
Size: 272796 Color: 2391
Size: 250504 Color: 86

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 476833 Color: 9655
Size: 267845 Color: 1977
Size: 255323 Color: 774

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 476857 Color: 9656
Size: 271165 Color: 2262
Size: 251979 Color: 346

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 476910 Color: 9657
Size: 272719 Color: 2386
Size: 250372 Color: 65

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 476931 Color: 9658
Size: 263714 Color: 1645
Size: 259356 Color: 1250

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 476994 Color: 9659
Size: 269104 Color: 2087
Size: 253903 Color: 603

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 477046 Color: 9660
Size: 263861 Color: 1657
Size: 259094 Color: 1220

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 477056 Color: 9661
Size: 267009 Color: 1912
Size: 255936 Color: 854

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 477210 Color: 9662
Size: 268107 Color: 2001
Size: 254684 Color: 699

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 477223 Color: 9663
Size: 271566 Color: 2299
Size: 251212 Color: 223

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 477277 Color: 9664
Size: 270121 Color: 2173
Size: 252603 Color: 437

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 477380 Color: 9665
Size: 265897 Color: 1817
Size: 256724 Color: 961

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 477445 Color: 9666
Size: 263817 Color: 1651
Size: 258739 Color: 1185

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 477518 Color: 9667
Size: 266907 Color: 1903
Size: 255576 Color: 805

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 477699 Color: 9668
Size: 262709 Color: 1566
Size: 259593 Color: 1275

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 477749 Color: 9669
Size: 265026 Color: 1743
Size: 257226 Color: 1023

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 477777 Color: 9670
Size: 269176 Color: 2091
Size: 253048 Color: 495

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 477791 Color: 9671
Size: 272111 Color: 2344
Size: 250099 Color: 19

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 477874 Color: 9672
Size: 264919 Color: 1729
Size: 257208 Color: 1021

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 477889 Color: 9673
Size: 268792 Color: 2062
Size: 253320 Color: 529

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 477935 Color: 9674
Size: 265854 Color: 1810
Size: 256212 Color: 888

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 478027 Color: 9675
Size: 269584 Color: 2128
Size: 252390 Color: 403

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 478269 Color: 9676
Size: 270956 Color: 2245
Size: 250776 Color: 142

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 478331 Color: 9677
Size: 269718 Color: 2140
Size: 251952 Color: 341

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 478334 Color: 9678
Size: 270869 Color: 2236
Size: 250798 Color: 147

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 478433 Color: 9679
Size: 269978 Color: 2165
Size: 251590 Color: 282

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 478508 Color: 9680
Size: 268433 Color: 2032
Size: 253060 Color: 499

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 478566 Color: 9681
Size: 260760 Color: 1396
Size: 260675 Color: 1387

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 478591 Color: 9682
Size: 266839 Color: 1897
Size: 254571 Color: 692

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 478609 Color: 9683
Size: 266352 Color: 1858
Size: 255040 Color: 742

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 478780 Color: 9684
Size: 264943 Color: 1732
Size: 256278 Color: 903

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 478808 Color: 9685
Size: 260963 Color: 1416
Size: 260230 Color: 1341

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 479070 Color: 9686
Size: 268357 Color: 2028
Size: 252574 Color: 431

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 479131 Color: 9687
Size: 262806 Color: 1573
Size: 258064 Color: 1118

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 479201 Color: 9688
Size: 262953 Color: 1587
Size: 257847 Color: 1096

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 479249 Color: 9689
Size: 260389 Color: 1366
Size: 260363 Color: 1363

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 479261 Color: 9690
Size: 265560 Color: 1789
Size: 255180 Color: 758

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 479524 Color: 9691
Size: 262041 Color: 1510
Size: 258436 Color: 1153

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 479582 Color: 9692
Size: 269226 Color: 2097
Size: 251193 Color: 219

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 479613 Color: 9693
Size: 269299 Color: 2100
Size: 251089 Color: 201

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 479654 Color: 9694
Size: 268124 Color: 2005
Size: 252223 Color: 379

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 479680 Color: 9695
Size: 262817 Color: 1575
Size: 257504 Color: 1054

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 479698 Color: 9696
Size: 266853 Color: 1901
Size: 253450 Color: 551

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 479742 Color: 9697
Size: 261250 Color: 1443
Size: 259009 Color: 1212

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 479781 Color: 9698
Size: 261561 Color: 1472
Size: 258659 Color: 1173

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 479958 Color: 9699
Size: 266795 Color: 1892
Size: 253248 Color: 523

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 480012 Color: 9700
Size: 262593 Color: 1559
Size: 257396 Color: 1041

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 480081 Color: 9701
Size: 265939 Color: 1821
Size: 253981 Color: 614

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 480133 Color: 9702
Size: 261293 Color: 1448
Size: 258575 Color: 1166

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 480332 Color: 9703
Size: 263213 Color: 1607
Size: 256456 Color: 933

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 480384 Color: 9704
Size: 267061 Color: 1916
Size: 252556 Color: 428

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 480426 Color: 9705
Size: 265842 Color: 1809
Size: 253733 Color: 578

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 480535 Color: 9706
Size: 268266 Color: 2024
Size: 251200 Color: 220

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 480644 Color: 9707
Size: 264184 Color: 1682
Size: 255173 Color: 756

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 480743 Color: 9708
Size: 265238 Color: 1764
Size: 254020 Color: 622

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 480891 Color: 9709
Size: 261820 Color: 1492
Size: 257290 Color: 1028

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 481083 Color: 9710
Size: 266484 Color: 1864
Size: 252434 Color: 409

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 481344 Color: 9711
Size: 265605 Color: 1794
Size: 253052 Color: 497

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 481373 Color: 9712
Size: 259924 Color: 1306
Size: 258704 Color: 1178

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 481476 Color: 9713
Size: 268409 Color: 2030
Size: 250116 Color: 22

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 481526 Color: 9714
Size: 259322 Color: 1248
Size: 259153 Color: 1229

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 481613 Color: 9715
Size: 266948 Color: 1910
Size: 251440 Color: 252

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 481719 Color: 9716
Size: 260399 Color: 1367
Size: 257883 Color: 1099

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 481723 Color: 9717
Size: 262478 Color: 1552
Size: 255800 Color: 838

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 481747 Color: 9718
Size: 266910 Color: 1904
Size: 251344 Color: 242

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 481781 Color: 9719
Size: 260036 Color: 1315
Size: 258184 Color: 1134

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 481928 Color: 9720
Size: 261002 Color: 1423
Size: 257071 Color: 1004

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 481932 Color: 9721
Size: 262291 Color: 1535
Size: 255778 Color: 834

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 481948 Color: 9722
Size: 268030 Color: 1994
Size: 250023 Color: 5

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 481998 Color: 9723
Size: 260307 Color: 1353
Size: 257696 Color: 1073

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 482063 Color: 9724
Size: 266843 Color: 1899
Size: 251095 Color: 202

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 482198 Color: 9725
Size: 259427 Color: 1259
Size: 258376 Color: 1149

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 482231 Color: 9726
Size: 267076 Color: 1919
Size: 250694 Color: 126

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 482313 Color: 9727
Size: 263697 Color: 1643
Size: 253991 Color: 618

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 482486 Color: 9728
Size: 266254 Color: 1846
Size: 251261 Color: 231

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 482516 Color: 9729
Size: 260624 Color: 1383
Size: 256861 Color: 980

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 482630 Color: 9730
Size: 264250 Color: 1688
Size: 253121 Color: 507

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 482767 Color: 9731
Size: 260295 Color: 1351
Size: 256939 Color: 989

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 482948 Color: 9732
Size: 262970 Color: 1589
Size: 254083 Color: 630

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 483104 Color: 9733
Size: 265951 Color: 1823
Size: 250946 Color: 179

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 483212 Color: 9734
Size: 260342 Color: 1357
Size: 256447 Color: 932

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 483339 Color: 9735
Size: 260490 Color: 1371
Size: 256172 Color: 884

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 483367 Color: 9736
Size: 266349 Color: 1857
Size: 250285 Color: 53

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 483595 Color: 9737
Size: 261601 Color: 1476
Size: 254805 Color: 716

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 483689 Color: 9738
Size: 265423 Color: 1778
Size: 250889 Color: 164

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 483781 Color: 9739
Size: 264066 Color: 1671
Size: 252154 Color: 372

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 483855 Color: 9740
Size: 263639 Color: 1640
Size: 252507 Color: 423

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 484045 Color: 9741
Size: 259736 Color: 1294
Size: 256220 Color: 891

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 484187 Color: 9742
Size: 258805 Color: 1192
Size: 257009 Color: 999

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 484217 Color: 9743
Size: 259372 Color: 1253
Size: 256412 Color: 926

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 484218 Color: 9744
Size: 264278 Color: 1692
Size: 251505 Color: 266

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 484446 Color: 9745
Size: 258097 Color: 1122
Size: 257458 Color: 1047

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 484525 Color: 9746
Size: 258070 Color: 1120
Size: 257406 Color: 1042

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 484563 Color: 9747
Size: 258615 Color: 1168
Size: 256823 Color: 976

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 484647 Color: 9748
Size: 262544 Color: 1556
Size: 252810 Color: 458

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 484654 Color: 9749
Size: 257908 Color: 1101
Size: 257439 Color: 1045

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 484698 Color: 9750
Size: 264533 Color: 1705
Size: 250770 Color: 140

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 484761 Color: 9751
Size: 258135 Color: 1126
Size: 257105 Color: 1009

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 484835 Color: 9752
Size: 260760 Color: 1395
Size: 254406 Color: 671

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 484838 Color: 9753
Size: 259561 Color: 1272
Size: 255602 Color: 811

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 484878 Color: 9754
Size: 258848 Color: 1196
Size: 256275 Color: 901

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 484944 Color: 9755
Size: 259590 Color: 1274
Size: 255467 Color: 793

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 485052 Color: 9756
Size: 263369 Color: 1618
Size: 251580 Color: 280

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 485113 Color: 9757
Size: 258563 Color: 1165
Size: 256325 Color: 914

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 485177 Color: 9758
Size: 259623 Color: 1280
Size: 255201 Color: 764

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 485184 Color: 9759
Size: 257771 Color: 1088
Size: 257046 Color: 1002

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 485451 Color: 9760
Size: 259636 Color: 1284
Size: 254914 Color: 733

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 485508 Color: 9761
Size: 257324 Color: 1034
Size: 257169 Color: 1019

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 485621 Color: 9762
Size: 258267 Color: 1140
Size: 256113 Color: 875

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 485624 Color: 9763
Size: 258510 Color: 1159
Size: 255867 Color: 846

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 485634 Color: 9764
Size: 258213 Color: 1138
Size: 256154 Color: 882

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 485664 Color: 9765
Size: 259327 Color: 1249
Size: 255010 Color: 738

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 485733 Color: 9766
Size: 258008 Color: 1109
Size: 256260 Color: 896

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 485776 Color: 9767
Size: 258809 Color: 1193
Size: 255416 Color: 785

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 485803 Color: 9768
Size: 257464 Color: 1049
Size: 256734 Color: 963

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 485805 Color: 9769
Size: 262741 Color: 1568
Size: 251455 Color: 255

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 485846 Color: 9770
Size: 262876 Color: 1582
Size: 251279 Color: 237

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 485954 Color: 9771
Size: 257770 Color: 1086
Size: 256277 Color: 902

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 485981 Color: 9772
Size: 260266 Color: 1345
Size: 253754 Color: 582

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 486000 Color: 9773
Size: 257530 Color: 1055
Size: 256471 Color: 934

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 486138 Color: 9774
Size: 258018 Color: 1113
Size: 255845 Color: 844

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 486167 Color: 9775
Size: 263045 Color: 1596
Size: 250789 Color: 145

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 486172 Color: 9776
Size: 262088 Color: 1514
Size: 251741 Color: 313

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 486199 Color: 9777
Size: 258419 Color: 1152
Size: 255383 Color: 780

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 486230 Color: 9778
Size: 259410 Color: 1258
Size: 254361 Color: 663

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 486238 Color: 9779
Size: 260949 Color: 1415
Size: 252814 Color: 459

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 486297 Color: 9780
Size: 257992 Color: 1107
Size: 255712 Color: 826

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 486311 Color: 9781
Size: 263294 Color: 1614
Size: 250396 Color: 68

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 486313 Color: 9782
Size: 263127 Color: 1603
Size: 250561 Color: 101

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 486360 Color: 9783
Size: 258166 Color: 1131
Size: 255475 Color: 795

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 486388 Color: 9784
Size: 259104 Color: 1222
Size: 254509 Color: 689

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 486414 Color: 9785
Size: 259606 Color: 1277
Size: 253981 Color: 613

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 486417 Color: 9786
Size: 259274 Color: 1246
Size: 254310 Color: 656

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 486499 Color: 9787
Size: 256936 Color: 988
Size: 256566 Color: 941

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 486524 Color: 9788
Size: 258782 Color: 1189
Size: 254695 Color: 701

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 486567 Color: 9789
Size: 260172 Color: 1333
Size: 253262 Color: 525

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 486702 Color: 9790
Size: 262243 Color: 1528
Size: 251056 Color: 200

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 486715 Color: 9791
Size: 261670 Color: 1483
Size: 251616 Color: 287

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 486742 Color: 9792
Size: 257139 Color: 1015
Size: 256120 Color: 876

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 486809 Color: 9793
Size: 260702 Color: 1389
Size: 252490 Color: 417

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 486942 Color: 9794
Size: 257825 Color: 1092
Size: 255234 Color: 766

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 486957 Color: 9795
Size: 262767 Color: 1570
Size: 250277 Color: 52

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 486972 Color: 9796
Size: 256699 Color: 957
Size: 256330 Color: 917

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 487025 Color: 9797
Size: 258525 Color: 1160
Size: 254451 Color: 681

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 487031 Color: 9798
Size: 260584 Color: 1381
Size: 252386 Color: 401

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 487054 Color: 9799
Size: 259174 Color: 1233
Size: 253773 Color: 586

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 487073 Color: 9800
Size: 262032 Color: 1509
Size: 250896 Color: 166

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 487095 Color: 9801
Size: 259825 Color: 1296
Size: 253081 Color: 505

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 487134 Color: 9802
Size: 262341 Color: 1540
Size: 250526 Color: 96

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 487136 Color: 9803
Size: 261226 Color: 1440
Size: 251639 Color: 292

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 487332 Color: 9804
Size: 260671 Color: 1386
Size: 251998 Color: 351

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 487349 Color: 9805
Size: 258444 Color: 1155
Size: 254208 Color: 643

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 487448 Color: 9806
Size: 261048 Color: 1430
Size: 251505 Color: 265

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 487464 Color: 9807
Size: 260772 Color: 1398
Size: 251765 Color: 317

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 487547 Color: 9808
Size: 261008 Color: 1424
Size: 251446 Color: 254

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 487549 Color: 9809
Size: 256762 Color: 964
Size: 255690 Color: 822

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 487616 Color: 9810
Size: 256964 Color: 991
Size: 255421 Color: 788

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 487705 Color: 9811
Size: 257084 Color: 1005
Size: 255212 Color: 765

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 487706 Color: 9812
Size: 260271 Color: 1347
Size: 252024 Color: 352

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 487774 Color: 9813
Size: 260853 Color: 1407
Size: 251374 Color: 244

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 487780 Color: 9814
Size: 256259 Color: 895
Size: 255962 Color: 858

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 487810 Color: 9815
Size: 258541 Color: 1163
Size: 253650 Color: 572

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 487855 Color: 9816
Size: 257124 Color: 1013
Size: 255022 Color: 739

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 487868 Color: 9817
Size: 256623 Color: 949
Size: 255510 Color: 798

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 487905 Color: 9818
Size: 261521 Color: 1469
Size: 250575 Color: 104

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 487923 Color: 9819
Size: 256964 Color: 992
Size: 255114 Color: 749

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 488066 Color: 9820
Size: 260699 Color: 1388
Size: 251236 Color: 227

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 488071 Color: 9821
Size: 261339 Color: 1451
Size: 250591 Color: 108

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 488219 Color: 9822
Size: 256306 Color: 908
Size: 255476 Color: 796

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 488237 Color: 9823
Size: 259644 Color: 1286
Size: 252120 Color: 367

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 488320 Color: 9824
Size: 259518 Color: 1268
Size: 252163 Color: 374

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 488356 Color: 9825
Size: 260146 Color: 1331
Size: 251499 Color: 264

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 488411 Color: 9826
Size: 259111 Color: 1224
Size: 252479 Color: 415

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 488570 Color: 9827
Size: 260163 Color: 1332
Size: 251268 Color: 233

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 488627 Color: 9828
Size: 257002 Color: 997
Size: 254372 Color: 665

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 488855 Color: 9829
Size: 257148 Color: 1016
Size: 253998 Color: 620

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 488866 Color: 9830
Size: 257576 Color: 1061
Size: 253559 Color: 560

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 488906 Color: 9831
Size: 260661 Color: 1385
Size: 250434 Color: 74

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 488909 Color: 9832
Size: 257709 Color: 1074
Size: 253383 Color: 537

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 489144 Color: 9833
Size: 255976 Color: 860
Size: 254881 Color: 728

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 489210 Color: 9834
Size: 259923 Color: 1305
Size: 250868 Color: 160

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 489364 Color: 9835
Size: 255886 Color: 848
Size: 254751 Color: 706

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 489406 Color: 9836
Size: 255361 Color: 779
Size: 255234 Color: 767

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 489508 Color: 9837
Size: 260002 Color: 1312
Size: 250491 Color: 84

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 489569 Color: 9838
Size: 257830 Color: 1093
Size: 252602 Color: 436

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 489608 Color: 9839
Size: 259510 Color: 1265
Size: 250883 Color: 163

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 489795 Color: 9840
Size: 258220 Color: 1139
Size: 251986 Color: 348

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 489837 Color: 9841
Size: 257664 Color: 1069
Size: 252500 Color: 421

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 489856 Color: 9842
Size: 259528 Color: 1270
Size: 250617 Color: 113

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 489898 Color: 9843
Size: 259617 Color: 1279
Size: 250486 Color: 83

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 490034 Color: 9844
Size: 257008 Color: 998
Size: 252959 Color: 484

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 490417 Color: 9845
Size: 257875 Color: 1098
Size: 251709 Color: 305

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 490448 Color: 9846
Size: 257118 Color: 1012
Size: 252435 Color: 410

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 490507 Color: 9847
Size: 256335 Color: 918
Size: 253159 Color: 518

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 490750 Color: 9848
Size: 256216 Color: 889
Size: 253035 Color: 491

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 490775 Color: 9849
Size: 259155 Color: 1230
Size: 250071 Color: 15

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 490809 Color: 9850
Size: 258291 Color: 1141
Size: 250901 Color: 168

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 490828 Color: 9851
Size: 257916 Color: 1102
Size: 251257 Color: 228

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 490842 Color: 9852
Size: 256580 Color: 943
Size: 252579 Color: 432

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 490991 Color: 9853
Size: 258183 Color: 1133
Size: 250827 Color: 156

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 491001 Color: 9854
Size: 256438 Color: 929
Size: 252562 Color: 429

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 491036 Color: 9855
Size: 257258 Color: 1025
Size: 251707 Color: 304

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 491098 Color: 9856
Size: 257743 Color: 1081
Size: 251160 Color: 212

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 491113 Color: 9857
Size: 258066 Color: 1119
Size: 250822 Color: 153

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 491114 Color: 9858
Size: 254450 Color: 679
Size: 254437 Color: 678

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 491137 Color: 9859
Size: 258646 Color: 1171
Size: 250218 Color: 44

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 491245 Color: 9860
Size: 256771 Color: 966
Size: 251985 Color: 347

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 491319 Color: 9861
Size: 255778 Color: 835
Size: 252904 Color: 472

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 491333 Color: 9862
Size: 254638 Color: 697
Size: 254030 Color: 623

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 491344 Color: 9863
Size: 255599 Color: 810
Size: 253058 Color: 498

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 491452 Color: 9864
Size: 257746 Color: 1082
Size: 250803 Color: 151

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 491504 Color: 9865
Size: 257851 Color: 1097
Size: 250646 Color: 119

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 491564 Color: 9866
Size: 257638 Color: 1067
Size: 250799 Color: 150

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 491579 Color: 9867
Size: 256146 Color: 880
Size: 252276 Color: 389

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 491764 Color: 9868
Size: 256860 Color: 979
Size: 251377 Color: 245

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 491902 Color: 9869
Size: 256143 Color: 878
Size: 251956 Color: 343

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 491927 Color: 9870
Size: 255807 Color: 839
Size: 252267 Color: 387

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 492150 Color: 9871
Size: 254421 Color: 673
Size: 253430 Color: 546

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 492157 Color: 9872
Size: 255975 Color: 859
Size: 251869 Color: 331

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 492169 Color: 9873
Size: 254425 Color: 674
Size: 253407 Color: 542

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 492201 Color: 9874
Size: 254759 Color: 708
Size: 253041 Color: 494

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 492211 Color: 9875
Size: 257271 Color: 1026
Size: 250519 Color: 92

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 492252 Color: 9876
Size: 257626 Color: 1064
Size: 250123 Color: 24

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 492266 Color: 9877
Size: 255758 Color: 833
Size: 251977 Color: 345

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 492295 Color: 9878
Size: 256348 Color: 920
Size: 251358 Color: 243

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 492323 Color: 9879
Size: 254489 Color: 685
Size: 253189 Color: 521

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 492402 Color: 9880
Size: 257088 Color: 1006
Size: 250511 Color: 90

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 492409 Color: 9881
Size: 255817 Color: 840
Size: 251775 Color: 318

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 492461 Color: 9882
Size: 255827 Color: 842
Size: 251713 Color: 307

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 492505 Color: 9883
Size: 256903 Color: 984
Size: 250593 Color: 110

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 492578 Color: 9884
Size: 254506 Color: 688
Size: 252917 Color: 476

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 492611 Color: 9885
Size: 253725 Color: 577
Size: 253665 Color: 574

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 492612 Color: 9886
Size: 254634 Color: 696
Size: 252755 Color: 453

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 492614 Color: 9887
Size: 256177 Color: 885
Size: 251210 Color: 222

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 492655 Color: 9888
Size: 253799 Color: 587
Size: 253547 Color: 558

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 492704 Color: 9889
Size: 255514 Color: 800
Size: 251783 Color: 319

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 492766 Color: 9890
Size: 256698 Color: 956
Size: 250537 Color: 97

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 492865 Color: 9891
Size: 256346 Color: 919
Size: 250790 Color: 146

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 493050 Color: 9892
Size: 254431 Color: 676
Size: 252520 Color: 425

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 493057 Color: 9893
Size: 255307 Color: 772
Size: 251637 Color: 291

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 493062 Color: 9894
Size: 255128 Color: 750
Size: 251811 Color: 324

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 493146 Color: 9895
Size: 255391 Color: 782
Size: 251464 Color: 258

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 493260 Color: 9896
Size: 255282 Color: 770
Size: 251459 Color: 257

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 493265 Color: 9897
Size: 253382 Color: 536
Size: 253354 Color: 534

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 493297 Color: 9898
Size: 255880 Color: 847
Size: 250824 Color: 155

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 493319 Color: 9899
Size: 253742 Color: 580
Size: 252940 Color: 481

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 493448 Color: 9900
Size: 254150 Color: 634
Size: 252403 Color: 405

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 493494 Color: 9901
Size: 255754 Color: 832
Size: 250753 Color: 136

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 493511 Color: 9902
Size: 254208 Color: 644
Size: 252282 Color: 390

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 493517 Color: 9903
Size: 254167 Color: 639
Size: 252317 Color: 393

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 493566 Color: 9904
Size: 254391 Color: 668
Size: 252044 Color: 355

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 493590 Color: 9905
Size: 255732 Color: 830
Size: 250679 Color: 125

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 493619 Color: 9906
Size: 254771 Color: 713
Size: 251611 Color: 286

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 493671 Color: 9907
Size: 253280 Color: 527
Size: 253050 Color: 496

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 493719 Color: 9908
Size: 253894 Color: 599
Size: 252388 Color: 402

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 493728 Color: 9909
Size: 254343 Color: 661
Size: 251930 Color: 338

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 493759 Color: 9910
Size: 253920 Color: 605
Size: 252322 Color: 394

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 493806 Color: 9911
Size: 256052 Color: 871
Size: 250143 Color: 26

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 493842 Color: 9912
Size: 253501 Color: 556
Size: 252658 Color: 443

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 494017 Color: 9913
Size: 255143 Color: 751
Size: 250841 Color: 158

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 494020 Color: 9914
Size: 254926 Color: 734
Size: 251055 Color: 199

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 494075 Color: 9915
Size: 254978 Color: 737
Size: 250948 Color: 181

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 494121 Color: 9916
Size: 255571 Color: 804
Size: 250309 Color: 56

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 494210 Color: 9917
Size: 253947 Color: 611
Size: 251844 Color: 330

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 494226 Color: 9918
Size: 254756 Color: 707
Size: 251019 Color: 195

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 494253 Color: 9919
Size: 253449 Color: 550
Size: 252299 Color: 392

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 494355 Color: 9920
Size: 254698 Color: 702
Size: 250948 Color: 180

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 494409 Color: 9921
Size: 253960 Color: 612
Size: 251632 Color: 290

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 494432 Color: 9922
Size: 253690 Color: 575
Size: 251879 Color: 332

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 494513 Color: 9923
Size: 254944 Color: 735
Size: 250544 Color: 100

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 494769 Color: 9924
Size: 253498 Color: 555
Size: 251734 Color: 310

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 494861 Color: 9925
Size: 254393 Color: 669
Size: 250747 Color: 133

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 495036 Color: 9926
Size: 254019 Color: 621
Size: 250946 Color: 178

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 495050 Color: 9927
Size: 254428 Color: 675
Size: 250523 Color: 94

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 495056 Color: 9928
Size: 254407 Color: 672
Size: 250538 Color: 98

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 495081 Color: 9929
Size: 254150 Color: 635
Size: 250770 Color: 141

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 495300 Color: 9930
Size: 254611 Color: 693
Size: 250090 Color: 17

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 495311 Color: 9931
Size: 253891 Color: 598
Size: 250799 Color: 149

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 495431 Color: 9932
Size: 254064 Color: 628
Size: 250506 Color: 87

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 495436 Color: 9933
Size: 252825 Color: 461
Size: 251740 Color: 312

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 495441 Color: 9934
Size: 253604 Color: 567
Size: 250956 Color: 184

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 495521 Color: 9935
Size: 254161 Color: 637
Size: 250319 Color: 59

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 495685 Color: 9936
Size: 252789 Color: 456
Size: 251527 Color: 271

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 495702 Color: 9937
Size: 253691 Color: 576
Size: 250608 Color: 111

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 495755 Color: 9938
Size: 252293 Color: 391
Size: 251953 Color: 342

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 495826 Color: 9939
Size: 252739 Color: 450
Size: 251436 Color: 250

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 495843 Color: 9940
Size: 252835 Color: 464
Size: 251323 Color: 239

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 495857 Color: 9941
Size: 252886 Color: 471
Size: 251258 Color: 229

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 495895 Color: 9942
Size: 252949 Color: 482
Size: 251157 Color: 211

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 495990 Color: 9943
Size: 253649 Color: 571
Size: 250362 Color: 63

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 496102 Color: 9944
Size: 253143 Color: 514
Size: 250756 Color: 138

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 496279 Color: 9945
Size: 252588 Color: 434
Size: 251134 Color: 209

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 496352 Color: 9946
Size: 253594 Color: 565
Size: 250055 Color: 11

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 496420 Color: 9947
Size: 252584 Color: 433
Size: 250997 Color: 192

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 496517 Color: 9948
Size: 252102 Color: 366
Size: 251382 Color: 246

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 496560 Color: 9949
Size: 253008 Color: 488
Size: 250433 Color: 71

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 496564 Color: 9950
Size: 252588 Color: 435
Size: 250849 Color: 159

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 496590 Color: 9951
Size: 253070 Color: 501
Size: 250341 Color: 60

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 496759 Color: 9952
Size: 251901 Color: 335
Size: 251341 Color: 241

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 496823 Color: 9953
Size: 252544 Color: 426
Size: 250634 Color: 115

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 496855 Color: 9954
Size: 251599 Color: 283
Size: 251547 Color: 275

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 496903 Color: 9955
Size: 252189 Color: 377
Size: 250909 Color: 172

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 496928 Color: 9956
Size: 252172 Color: 375
Size: 250901 Color: 169

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 496975 Color: 9957
Size: 252062 Color: 361
Size: 250964 Color: 186

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 496982 Color: 9958
Size: 252873 Color: 469
Size: 250146 Color: 27

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 496994 Color: 9959
Size: 252430 Color: 408
Size: 250577 Color: 105

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 497014 Color: 9960
Size: 252237 Color: 382
Size: 250750 Color: 135

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 497106 Color: 9961
Size: 251987 Color: 349
Size: 250908 Color: 171

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 497121 Color: 9962
Size: 252496 Color: 420
Size: 250384 Color: 66

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 497212 Color: 9963
Size: 252156 Color: 373
Size: 250633 Color: 114

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 497360 Color: 9964
Size: 252050 Color: 358
Size: 250591 Color: 107

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 497378 Color: 9965
Size: 251438 Color: 251
Size: 251185 Color: 214

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 497393 Color: 9966
Size: 251715 Color: 308
Size: 250893 Color: 165

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 497418 Color: 9967
Size: 251620 Color: 288
Size: 250963 Color: 185

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 497568 Color: 9968
Size: 251666 Color: 296
Size: 250767 Color: 139

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 497601 Color: 9969
Size: 251425 Color: 248
Size: 250975 Color: 188

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 497700 Color: 9970
Size: 251192 Color: 218
Size: 251109 Color: 204

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 497765 Color: 9971
Size: 251326 Color: 240
Size: 250910 Color: 174

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 497977 Color: 9972
Size: 251941 Color: 339
Size: 250083 Color: 16

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 497978 Color: 9973
Size: 251546 Color: 274
Size: 250477 Color: 81

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 498066 Color: 9974
Size: 251136 Color: 210
Size: 250799 Color: 148

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 498079 Color: 9975
Size: 251689 Color: 298
Size: 250233 Color: 48

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 498140 Color: 9976
Size: 251554 Color: 276
Size: 250307 Color: 55

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 498210 Color: 9977
Size: 251121 Color: 208
Size: 250670 Color: 124

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 498268 Color: 9978
Size: 250991 Color: 191
Size: 250742 Color: 132

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 498411 Color: 9979
Size: 251473 Color: 261
Size: 250117 Color: 23

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 498447 Color: 9980
Size: 250909 Color: 173
Size: 250645 Color: 118

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 498448 Color: 9981
Size: 251510 Color: 268
Size: 250043 Color: 9

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 498450 Color: 9982
Size: 251206 Color: 221
Size: 250345 Color: 62

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 498540 Color: 9983
Size: 250951 Color: 183
Size: 250510 Color: 89

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 498629 Color: 9984
Size: 251029 Color: 196
Size: 250343 Color: 61

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 498853 Color: 9985
Size: 250714 Color: 129
Size: 250434 Color: 72

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 498902 Color: 9986
Size: 250669 Color: 123
Size: 250430 Color: 70

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 499054 Color: 9987
Size: 250662 Color: 122
Size: 250285 Color: 54

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 499233 Color: 9988
Size: 250499 Color: 85
Size: 250269 Color: 51

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 499337 Color: 9989
Size: 250564 Color: 102
Size: 250100 Color: 20

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 499338 Color: 9990
Size: 250610 Color: 112
Size: 250053 Color: 10

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 499390 Color: 9991
Size: 250422 Color: 69
Size: 250189 Color: 36

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 499505 Color: 9992
Size: 250472 Color: 80
Size: 250024 Color: 6

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 499516 Color: 9993
Size: 250314 Color: 58
Size: 250171 Color: 31

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 499612 Color: 9994
Size: 250228 Color: 46
Size: 250161 Color: 29

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 499634 Color: 9995
Size: 250194 Color: 38
Size: 250173 Color: 32

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 499638 Color: 9996
Size: 250223 Color: 45
Size: 250140 Color: 25

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 499642 Color: 9997
Size: 250194 Color: 39
Size: 250165 Color: 30

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 499714 Color: 9998
Size: 250173 Color: 33
Size: 250114 Color: 21

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 499833 Color: 9999
Size: 250146 Color: 28
Size: 250022 Color: 4

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 499927 Color: 10000
Size: 250067 Color: 14
Size: 250007 Color: 2

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 499954 Color: 10001
Size: 250027 Color: 7
Size: 250020 Color: 3

Bin 3039: 1 of cap free
Amount of items: 3
Items: 
Size: 367581 Color: 7082
Size: 316953 Color: 5039
Size: 315466 Color: 4961

Bin 3040: 1 of cap free
Amount of items: 3
Items: 
Size: 368859 Color: 7133
Size: 321485 Color: 5250
Size: 309656 Color: 4651

Bin 3041: 1 of cap free
Amount of items: 3
Items: 
Size: 369759 Color: 7156
Size: 320258 Color: 5193
Size: 309983 Color: 4674

Bin 3042: 1 of cap free
Amount of items: 3
Items: 
Size: 370141 Color: 7166
Size: 322094 Color: 5284
Size: 307765 Color: 4552

Bin 3043: 1 of cap free
Amount of items: 3
Items: 
Size: 351104 Color: 6511
Size: 330948 Color: 5663
Size: 317948 Color: 5092

Bin 3044: 1 of cap free
Amount of items: 3
Items: 
Size: 386603 Color: 7654
Size: 340171 Color: 6077
Size: 273226 Color: 2427

Bin 3045: 1 of cap free
Amount of items: 3
Items: 
Size: 385499 Color: 7626
Size: 343985 Color: 6230
Size: 270516 Color: 2208

Bin 3046: 1 of cap free
Amount of items: 3
Items: 
Size: 406948 Color: 8233
Size: 331590 Color: 5693
Size: 261462 Color: 1461

Bin 3047: 1 of cap free
Amount of items: 3
Items: 
Size: 364474 Color: 6991
Size: 351683 Color: 6536
Size: 283843 Color: 3157

Bin 3048: 1 of cap free
Amount of items: 3
Items: 
Size: 359718 Color: 6833
Size: 351783 Color: 6541
Size: 288499 Color: 3471

Bin 3049: 1 of cap free
Amount of items: 3
Items: 
Size: 367243 Color: 7065
Size: 316994 Color: 5041
Size: 315763 Color: 4975

Bin 3050: 1 of cap free
Amount of items: 3
Items: 
Size: 346223 Color: 6322
Size: 331066 Color: 5671
Size: 322711 Color: 5306

Bin 3051: 1 of cap free
Amount of items: 3
Items: 
Size: 355979 Color: 6697
Size: 334737 Color: 5836
Size: 309284 Color: 4626

Bin 3052: 1 of cap free
Amount of items: 3
Items: 
Size: 388251 Color: 7704
Size: 342682 Color: 6175
Size: 269067 Color: 2081

Bin 3053: 1 of cap free
Amount of items: 3
Items: 
Size: 395226 Color: 7921
Size: 333836 Color: 5794
Size: 270938 Color: 2242

Bin 3054: 1 of cap free
Amount of items: 3
Items: 
Size: 384569 Color: 7593
Size: 342611 Color: 6169
Size: 272820 Color: 2395

Bin 3055: 1 of cap free
Amount of items: 3
Items: 
Size: 339009 Color: 6021
Size: 334241 Color: 5814
Size: 326750 Color: 5491

Bin 3056: 1 of cap free
Amount of items: 3
Items: 
Size: 379892 Color: 7460
Size: 347306 Color: 6363
Size: 272802 Color: 2393

Bin 3057: 1 of cap free
Amount of items: 3
Items: 
Size: 367886 Color: 7095
Size: 361725 Color: 6898
Size: 270389 Color: 2197

Bin 3058: 1 of cap free
Amount of items: 3
Items: 
Size: 373887 Color: 7276
Size: 345721 Color: 6298
Size: 280392 Color: 2950

Bin 3059: 1 of cap free
Amount of items: 3
Items: 
Size: 375255 Color: 7315
Size: 344335 Color: 6246
Size: 280410 Color: 2952

Bin 3060: 1 of cap free
Amount of items: 3
Items: 
Size: 379390 Color: 7447
Size: 343157 Color: 6195
Size: 277453 Color: 2748

Bin 3061: 1 of cap free
Amount of items: 3
Items: 
Size: 376474 Color: 7354
Size: 342024 Color: 6152
Size: 281502 Color: 3016

Bin 3062: 1 of cap free
Amount of items: 3
Items: 
Size: 397232 Color: 7980
Size: 323612 Color: 5343
Size: 279156 Color: 2872

Bin 3063: 1 of cap free
Amount of items: 3
Items: 
Size: 378862 Color: 7431
Size: 343272 Color: 6200
Size: 277866 Color: 2773

Bin 3064: 1 of cap free
Amount of items: 3
Items: 
Size: 385165 Color: 7612
Size: 325147 Color: 5410
Size: 289688 Color: 3545

Bin 3065: 1 of cap free
Amount of items: 3
Items: 
Size: 366498 Color: 7045
Size: 328455 Color: 5560
Size: 305047 Color: 4417

Bin 3066: 1 of cap free
Amount of items: 3
Items: 
Size: 365978 Color: 7031
Size: 344922 Color: 6269
Size: 289100 Color: 3507

Bin 3067: 1 of cap free
Amount of items: 3
Items: 
Size: 368766 Color: 7128
Size: 357406 Color: 6745
Size: 273828 Color: 2468

Bin 3068: 1 of cap free
Amount of items: 3
Items: 
Size: 378640 Color: 7423
Size: 328867 Color: 5574
Size: 292493 Color: 3723

Bin 3069: 1 of cap free
Amount of items: 3
Items: 
Size: 361776 Color: 6902
Size: 355246 Color: 6661
Size: 282978 Color: 3113

Bin 3070: 1 of cap free
Amount of items: 3
Items: 
Size: 346951 Color: 6350
Size: 344500 Color: 6253
Size: 308549 Color: 4588

Bin 3071: 1 of cap free
Amount of items: 3
Items: 
Size: 367310 Color: 7071
Size: 329148 Color: 5586
Size: 303542 Color: 4323

Bin 3072: 1 of cap free
Amount of items: 3
Items: 
Size: 383069 Color: 7558
Size: 341596 Color: 6138
Size: 275335 Color: 2595

Bin 3073: 1 of cap free
Amount of items: 3
Items: 
Size: 358798 Color: 6801
Size: 328360 Color: 5555
Size: 312842 Color: 4842

Bin 3074: 1 of cap free
Amount of items: 3
Items: 
Size: 381749 Color: 7520
Size: 341976 Color: 6149
Size: 276275 Color: 2663

Bin 3075: 1 of cap free
Amount of items: 3
Items: 
Size: 409819 Color: 8288
Size: 337023 Color: 5946
Size: 253158 Color: 517

Bin 3076: 1 of cap free
Amount of items: 3
Items: 
Size: 399290 Color: 8040
Size: 337879 Color: 5986
Size: 262831 Color: 1576

Bin 3077: 1 of cap free
Amount of items: 3
Items: 
Size: 401608 Color: 8086
Size: 335971 Color: 5898
Size: 262421 Color: 1544

Bin 3078: 1 of cap free
Amount of items: 3
Items: 
Size: 402681 Color: 8121
Size: 334455 Color: 5824
Size: 262864 Color: 1580

Bin 3079: 1 of cap free
Amount of items: 3
Items: 
Size: 373041 Color: 7242
Size: 335377 Color: 5876
Size: 291582 Color: 3663

Bin 3080: 1 of cap free
Amount of items: 3
Items: 
Size: 401628 Color: 8087
Size: 335381 Color: 5878
Size: 262991 Color: 1593

Bin 3081: 1 of cap free
Amount of items: 3
Items: 
Size: 371746 Color: 7206
Size: 332387 Color: 5729
Size: 295867 Color: 3922

Bin 3082: 1 of cap free
Amount of items: 3
Items: 
Size: 390713 Color: 7777
Size: 332281 Color: 5724
Size: 277006 Color: 2721

Bin 3083: 1 of cap free
Amount of items: 3
Items: 
Size: 399185 Color: 8037
Size: 331300 Color: 5679
Size: 269515 Color: 2121

Bin 3084: 1 of cap free
Amount of items: 3
Items: 
Size: 411471 Color: 8333
Size: 310641 Color: 4719
Size: 277888 Color: 2776

Bin 3085: 1 of cap free
Amount of items: 3
Items: 
Size: 396542 Color: 7966
Size: 336223 Color: 5911
Size: 267235 Color: 1931

Bin 3086: 1 of cap free
Amount of items: 3
Items: 
Size: 395737 Color: 7942
Size: 329574 Color: 5610
Size: 274689 Color: 2536

Bin 3087: 1 of cap free
Amount of items: 3
Items: 
Size: 372764 Color: 7237
Size: 357526 Color: 6750
Size: 269710 Color: 2139

Bin 3088: 1 of cap free
Amount of items: 3
Items: 
Size: 395846 Color: 7948
Size: 310209 Color: 4688
Size: 293945 Color: 3807

Bin 3089: 1 of cap free
Amount of items: 3
Items: 
Size: 394153 Color: 7897
Size: 327957 Color: 5537
Size: 277890 Color: 2777

Bin 3090: 1 of cap free
Amount of items: 3
Items: 
Size: 369516 Color: 7151
Size: 351981 Color: 6549
Size: 278503 Color: 2826

Bin 3091: 1 of cap free
Amount of items: 3
Items: 
Size: 393947 Color: 7884
Size: 327825 Color: 5535
Size: 278228 Color: 2801

Bin 3092: 1 of cap free
Amount of items: 3
Items: 
Size: 363829 Color: 6969
Size: 363321 Color: 6953
Size: 272850 Color: 2396

Bin 3093: 1 of cap free
Amount of items: 3
Items: 
Size: 393955 Color: 7886
Size: 334063 Color: 5803
Size: 271982 Color: 2330

Bin 3094: 1 of cap free
Amount of items: 3
Items: 
Size: 366694 Color: 7053
Size: 321871 Color: 5270
Size: 311435 Color: 4759

Bin 3095: 1 of cap free
Amount of items: 3
Items: 
Size: 353268 Color: 6592
Size: 338538 Color: 6011
Size: 308194 Color: 4573

Bin 3096: 1 of cap free
Amount of items: 3
Items: 
Size: 349203 Color: 6431
Size: 337853 Color: 5984
Size: 312944 Color: 4844

Bin 3097: 1 of cap free
Amount of items: 3
Items: 
Size: 399493 Color: 8046
Size: 308756 Color: 4600
Size: 291751 Color: 3681

Bin 3098: 1 of cap free
Amount of items: 3
Items: 
Size: 379081 Color: 7435
Size: 323107 Color: 5316
Size: 297812 Color: 4027

Bin 3099: 1 of cap free
Amount of items: 3
Items: 
Size: 364582 Color: 6996
Size: 326517 Color: 5479
Size: 308901 Color: 4606

Bin 3100: 1 of cap free
Amount of items: 3
Items: 
Size: 394797 Color: 7907
Size: 331851 Color: 5705
Size: 273352 Color: 2436

Bin 3101: 1 of cap free
Amount of items: 3
Items: 
Size: 374738 Color: 7296
Size: 326348 Color: 5468
Size: 298914 Color: 4084

Bin 3102: 1 of cap free
Amount of items: 3
Items: 
Size: 372559 Color: 7230
Size: 334432 Color: 5823
Size: 293009 Color: 3754

Bin 3103: 1 of cap free
Amount of items: 3
Items: 
Size: 373348 Color: 7253
Size: 332823 Color: 5751
Size: 293829 Color: 3800

Bin 3104: 1 of cap free
Amount of items: 3
Items: 
Size: 349499 Color: 6444
Size: 342316 Color: 6161
Size: 308185 Color: 4571

Bin 3105: 1 of cap free
Amount of items: 3
Items: 
Size: 373124 Color: 7246
Size: 334085 Color: 5804
Size: 292791 Color: 3741

Bin 3106: 1 of cap free
Amount of items: 3
Items: 
Size: 351752 Color: 6539
Size: 346832 Color: 6346
Size: 301416 Color: 4209

Bin 3107: 1 of cap free
Amount of items: 3
Items: 
Size: 364569 Color: 6994
Size: 318686 Color: 5127
Size: 316745 Color: 5030

Bin 3108: 1 of cap free
Amount of items: 3
Items: 
Size: 372110 Color: 7218
Size: 324036 Color: 5360
Size: 303854 Color: 4338

Bin 3109: 1 of cap free
Amount of items: 3
Items: 
Size: 372170 Color: 7219
Size: 340010 Color: 6068
Size: 287820 Color: 3426

Bin 3110: 1 of cap free
Amount of items: 3
Items: 
Size: 353389 Color: 6597
Size: 352838 Color: 6579
Size: 293773 Color: 3792

Bin 3111: 1 of cap free
Amount of items: 3
Items: 
Size: 399549 Color: 8047
Size: 323099 Color: 5313
Size: 277352 Color: 2741

Bin 3112: 1 of cap free
Amount of items: 3
Items: 
Size: 409204 Color: 8275
Size: 321042 Color: 5229
Size: 269754 Color: 2142

Bin 3113: 1 of cap free
Amount of items: 3
Items: 
Size: 362121 Color: 6915
Size: 347190 Color: 6361
Size: 290689 Color: 3598

Bin 3114: 1 of cap free
Amount of items: 3
Items: 
Size: 353891 Color: 6618
Size: 325698 Color: 5446
Size: 320411 Color: 5199

Bin 3115: 1 of cap free
Amount of items: 3
Items: 
Size: 365223 Color: 7013
Size: 330028 Color: 5628
Size: 304749 Color: 4396

Bin 3116: 1 of cap free
Amount of items: 3
Items: 
Size: 357070 Color: 6734
Size: 336597 Color: 5928
Size: 306333 Color: 4477

Bin 3117: 1 of cap free
Amount of items: 3
Items: 
Size: 371881 Color: 7213
Size: 324274 Color: 5371
Size: 303845 Color: 4337

Bin 3118: 1 of cap free
Amount of items: 3
Items: 
Size: 343254 Color: 6199
Size: 334938 Color: 5849
Size: 321808 Color: 5264

Bin 3119: 1 of cap free
Amount of items: 3
Items: 
Size: 404633 Color: 8169
Size: 312131 Color: 4798
Size: 283236 Color: 3126

Bin 3120: 1 of cap free
Amount of items: 3
Items: 
Size: 364139 Color: 6979
Size: 330214 Color: 5638
Size: 305647 Color: 4441

Bin 3121: 1 of cap free
Amount of items: 3
Items: 
Size: 356056 Color: 6705
Size: 338098 Color: 5996
Size: 305846 Color: 4453

Bin 3122: 1 of cap free
Amount of items: 3
Items: 
Size: 363323 Color: 6954
Size: 345853 Color: 6303
Size: 290824 Color: 3611

Bin 3123: 1 of cap free
Amount of items: 3
Items: 
Size: 399173 Color: 8036
Size: 307658 Color: 4548
Size: 293169 Color: 3762

Bin 3124: 1 of cap free
Amount of items: 3
Items: 
Size: 362730 Color: 6935
Size: 333747 Color: 5789
Size: 303523 Color: 4320

Bin 3125: 1 of cap free
Amount of items: 3
Items: 
Size: 367837 Color: 7092
Size: 355828 Color: 6686
Size: 276335 Color: 2669

Bin 3126: 1 of cap free
Amount of items: 3
Items: 
Size: 343340 Color: 6204
Size: 331834 Color: 5703
Size: 324826 Color: 5394

Bin 3127: 1 of cap free
Amount of items: 3
Items: 
Size: 394963 Color: 7911
Size: 337279 Color: 5957
Size: 267758 Color: 1972

Bin 3128: 1 of cap free
Amount of items: 3
Items: 
Size: 361265 Color: 6883
Size: 360756 Color: 6860
Size: 277979 Color: 2785

Bin 3129: 1 of cap free
Amount of items: 3
Items: 
Size: 356078 Color: 6706
Size: 342077 Color: 6154
Size: 301845 Color: 4236

Bin 3130: 1 of cap free
Amount of items: 3
Items: 
Size: 359527 Color: 6825
Size: 345287 Color: 6283
Size: 295186 Color: 3883

Bin 3131: 1 of cap free
Amount of items: 3
Items: 
Size: 359155 Color: 6817
Size: 358914 Color: 6808
Size: 281931 Color: 3051

Bin 3132: 1 of cap free
Amount of items: 3
Items: 
Size: 358899 Color: 6804
Size: 356873 Color: 6726
Size: 284228 Color: 3187

Bin 3133: 1 of cap free
Amount of items: 3
Items: 
Size: 358643 Color: 6791
Size: 358571 Color: 6786
Size: 282786 Color: 3100

Bin 3134: 1 of cap free
Amount of items: 3
Items: 
Size: 366103 Color: 7033
Size: 343403 Color: 6208
Size: 290494 Color: 3589

Bin 3135: 1 of cap free
Amount of items: 3
Items: 
Size: 386862 Color: 7667
Size: 347945 Color: 6387
Size: 265193 Color: 1759

Bin 3136: 1 of cap free
Amount of items: 3
Items: 
Size: 357421 Color: 6747
Size: 357255 Color: 6739
Size: 285324 Color: 3260

Bin 3137: 1 of cap free
Amount of items: 3
Items: 
Size: 355895 Color: 6690
Size: 355870 Color: 6688
Size: 288235 Color: 3457

Bin 3138: 1 of cap free
Amount of items: 3
Items: 
Size: 358099 Color: 6773
Size: 332625 Color: 5740
Size: 309276 Color: 4625

Bin 3139: 1 of cap free
Amount of items: 3
Items: 
Size: 355356 Color: 6671
Size: 354931 Color: 6654
Size: 289713 Color: 3546

Bin 3140: 1 of cap free
Amount of items: 3
Items: 
Size: 354715 Color: 6648
Size: 329079 Color: 5582
Size: 316206 Color: 5008

Bin 3141: 1 of cap free
Amount of items: 3
Items: 
Size: 354066 Color: 6623
Size: 353990 Color: 6621
Size: 291944 Color: 3697

Bin 3142: 1 of cap free
Amount of items: 3
Items: 
Size: 353882 Color: 6617
Size: 353795 Color: 6611
Size: 292323 Color: 3716

Bin 3143: 1 of cap free
Amount of items: 3
Items: 
Size: 354687 Color: 6647
Size: 348519 Color: 6402
Size: 296794 Color: 3971

Bin 3144: 1 of cap free
Amount of items: 3
Items: 
Size: 352413 Color: 6569
Size: 352339 Color: 6564
Size: 295248 Color: 3884

Bin 3145: 1 of cap free
Amount of items: 3
Items: 
Size: 352294 Color: 6561
Size: 352266 Color: 6560
Size: 295440 Color: 3898

Bin 3146: 1 of cap free
Amount of items: 3
Items: 
Size: 346554 Color: 6335
Size: 341480 Color: 6132
Size: 311966 Color: 4787

Bin 3147: 1 of cap free
Amount of items: 3
Items: 
Size: 344946 Color: 6272
Size: 340242 Color: 6080
Size: 314812 Color: 4936

Bin 3148: 1 of cap free
Amount of items: 3
Items: 
Size: 354571 Color: 6639
Size: 349948 Color: 6454
Size: 295481 Color: 3902

Bin 3149: 1 of cap free
Amount of items: 3
Items: 
Size: 359416 Color: 6823
Size: 347433 Color: 6368
Size: 293151 Color: 3759

Bin 3150: 1 of cap free
Amount of items: 3
Items: 
Size: 362819 Color: 6939
Size: 351275 Color: 6517
Size: 285906 Color: 3302

Bin 3151: 1 of cap free
Amount of items: 3
Items: 
Size: 361929 Color: 6905
Size: 351043 Color: 6506
Size: 287028 Color: 3375

Bin 3152: 1 of cap free
Amount of items: 3
Items: 
Size: 351593 Color: 6530
Size: 346388 Color: 6331
Size: 302019 Color: 4242

Bin 3153: 1 of cap free
Amount of items: 3
Items: 
Size: 348833 Color: 6419
Size: 348709 Color: 6418
Size: 302458 Color: 4270

Bin 3154: 1 of cap free
Amount of items: 3
Items: 
Size: 366444 Color: 7043
Size: 351447 Color: 6524
Size: 282109 Color: 3065

Bin 3155: 1 of cap free
Amount of items: 3
Items: 
Size: 348564 Color: 6406
Size: 348563 Color: 6405
Size: 302873 Color: 4292

Bin 3156: 1 of cap free
Amount of items: 3
Items: 
Size: 348313 Color: 6395
Size: 348306 Color: 6394
Size: 303381 Color: 4314

Bin 3157: 1 of cap free
Amount of items: 3
Items: 
Size: 347930 Color: 6386
Size: 331302 Color: 5680
Size: 320768 Color: 5213

Bin 3158: 1 of cap free
Amount of items: 3
Items: 
Size: 347739 Color: 6380
Size: 347702 Color: 6379
Size: 304559 Color: 4383

Bin 3159: 1 of cap free
Amount of items: 3
Items: 
Size: 364567 Color: 6993
Size: 347847 Color: 6383
Size: 287586 Color: 3409

Bin 3160: 2 of cap free
Amount of items: 3
Items: 
Size: 364550 Color: 6992
Size: 331128 Color: 5674
Size: 304321 Color: 4370

Bin 3161: 2 of cap free
Amount of items: 3
Items: 
Size: 389952 Color: 7755
Size: 324279 Color: 5372
Size: 285768 Color: 3293

Bin 3162: 2 of cap free
Amount of items: 3
Items: 
Size: 374850 Color: 7298
Size: 342028 Color: 6153
Size: 283121 Color: 3119

Bin 3163: 2 of cap free
Amount of items: 3
Items: 
Size: 387276 Color: 7676
Size: 340006 Color: 6067
Size: 272717 Color: 2385

Bin 3164: 2 of cap free
Amount of items: 3
Items: 
Size: 371652 Color: 7203
Size: 353012 Color: 6583
Size: 275335 Color: 2596

Bin 3165: 2 of cap free
Amount of items: 3
Items: 
Size: 382130 Color: 7528
Size: 341890 Color: 6147
Size: 275979 Color: 2637

Bin 3166: 2 of cap free
Amount of items: 3
Items: 
Size: 384022 Color: 7581
Size: 336894 Color: 5939
Size: 279083 Color: 2863

Bin 3167: 2 of cap free
Amount of items: 3
Items: 
Size: 408299 Color: 8260
Size: 319903 Color: 5176
Size: 271797 Color: 2317

Bin 3168: 2 of cap free
Amount of items: 3
Items: 
Size: 353336 Color: 6594
Size: 329001 Color: 5580
Size: 317662 Color: 5080

Bin 3169: 2 of cap free
Amount of items: 3
Items: 
Size: 399951 Color: 8058
Size: 340090 Color: 6074
Size: 259958 Color: 1309

Bin 3170: 2 of cap free
Amount of items: 3
Items: 
Size: 399384 Color: 8041
Size: 324690 Color: 5387
Size: 275925 Color: 2631

Bin 3171: 2 of cap free
Amount of items: 3
Items: 
Size: 403705 Color: 8146
Size: 334495 Color: 5825
Size: 261799 Color: 1490

Bin 3172: 2 of cap free
Amount of items: 3
Items: 
Size: 367007 Color: 7061
Size: 361972 Color: 6909
Size: 271020 Color: 2248

Bin 3173: 2 of cap free
Amount of items: 3
Items: 
Size: 401548 Color: 8083
Size: 328601 Color: 5567
Size: 269850 Color: 2152

Bin 3174: 2 of cap free
Amount of items: 3
Items: 
Size: 395246 Color: 7922
Size: 307162 Color: 4516
Size: 297591 Color: 4013

Bin 3175: 2 of cap free
Amount of items: 3
Items: 
Size: 390861 Color: 7780
Size: 324975 Color: 5402
Size: 284163 Color: 3184

Bin 3176: 2 of cap free
Amount of items: 3
Items: 
Size: 345752 Color: 6299
Size: 331698 Color: 5697
Size: 322549 Color: 5297

Bin 3177: 2 of cap free
Amount of items: 3
Items: 
Size: 365101 Color: 7010
Size: 325194 Color: 5414
Size: 309704 Color: 4653

Bin 3178: 2 of cap free
Amount of items: 3
Items: 
Size: 354646 Color: 6645
Size: 335572 Color: 5885
Size: 309781 Color: 4662

Bin 3179: 2 of cap free
Amount of items: 3
Items: 
Size: 367662 Color: 7085
Size: 340995 Color: 6109
Size: 291342 Color: 3646

Bin 3180: 2 of cap free
Amount of items: 3
Items: 
Size: 346355 Color: 6330
Size: 332438 Color: 5730
Size: 321206 Color: 5235

Bin 3181: 2 of cap free
Amount of items: 3
Items: 
Size: 369461 Color: 7149
Size: 339422 Color: 6038
Size: 291116 Color: 3636

Bin 3182: 2 of cap free
Amount of items: 3
Items: 
Size: 352386 Color: 6568
Size: 334392 Color: 5822
Size: 313221 Color: 4860

Bin 3183: 2 of cap free
Amount of items: 3
Items: 
Size: 367813 Color: 7091
Size: 319992 Color: 5178
Size: 312194 Color: 4800

Bin 3184: 2 of cap free
Amount of items: 3
Items: 
Size: 378782 Color: 7428
Size: 326587 Color: 5482
Size: 294630 Color: 3849

Bin 3185: 2 of cap free
Amount of items: 3
Items: 
Size: 343823 Color: 6223
Size: 334850 Color: 5842
Size: 321326 Color: 5242

Bin 3186: 2 of cap free
Amount of items: 3
Items: 
Size: 362315 Color: 6922
Size: 345478 Color: 6288
Size: 292206 Color: 3710

Bin 3187: 2 of cap free
Amount of items: 3
Items: 
Size: 388512 Color: 7711
Size: 325603 Color: 5441
Size: 285884 Color: 3301

Bin 3188: 2 of cap free
Amount of items: 3
Items: 
Size: 359540 Color: 6826
Size: 346407 Color: 6332
Size: 294052 Color: 3813

Bin 3189: 2 of cap free
Amount of items: 3
Items: 
Size: 358334 Color: 6779
Size: 358240 Color: 6776
Size: 283425 Color: 3138

Bin 3190: 2 of cap free
Amount of items: 3
Items: 
Size: 361776 Color: 6901
Size: 345488 Color: 6289
Size: 292735 Color: 3736

Bin 3191: 2 of cap free
Amount of items: 3
Items: 
Size: 356363 Color: 6717
Size: 354946 Color: 6655
Size: 288690 Color: 3478

Bin 3192: 2 of cap free
Amount of items: 3
Items: 
Size: 350631 Color: 6486
Size: 349798 Color: 6451
Size: 299570 Color: 4106

Bin 3193: 2 of cap free
Amount of items: 3
Items: 
Size: 351528 Color: 6528
Size: 330472 Color: 5647
Size: 317999 Color: 5093

Bin 3194: 2 of cap free
Amount of items: 3
Items: 
Size: 358751 Color: 6796
Size: 349755 Color: 6450
Size: 291493 Color: 3656

Bin 3195: 2 of cap free
Amount of items: 3
Items: 
Size: 352339 Color: 6565
Size: 352334 Color: 6563
Size: 295326 Color: 3890

Bin 3196: 2 of cap free
Amount of items: 3
Items: 
Size: 354303 Color: 6633
Size: 351257 Color: 6515
Size: 294439 Color: 3834

Bin 3197: 2 of cap free
Amount of items: 3
Items: 
Size: 351407 Color: 6522
Size: 340145 Color: 6076
Size: 308447 Color: 4584

Bin 3198: 2 of cap free
Amount of items: 3
Items: 
Size: 397414 Color: 7986
Size: 320680 Color: 5209
Size: 281905 Color: 3049

Bin 3199: 3 of cap free
Amount of items: 3
Items: 
Size: 364337 Color: 6988
Size: 345520 Color: 6292
Size: 290141 Color: 3569

Bin 3200: 3 of cap free
Amount of items: 3
Items: 
Size: 351314 Color: 6519
Size: 329544 Color: 5606
Size: 319140 Color: 5143

Bin 3201: 3 of cap free
Amount of items: 3
Items: 
Size: 405019 Color: 8178
Size: 297975 Color: 4034
Size: 297004 Color: 3983

Bin 3202: 3 of cap free
Amount of items: 3
Items: 
Size: 371570 Color: 7202
Size: 344105 Color: 6237
Size: 284323 Color: 3191

Bin 3203: 3 of cap free
Amount of items: 3
Items: 
Size: 403995 Color: 8153
Size: 299200 Color: 4094
Size: 296803 Color: 3973

Bin 3204: 3 of cap free
Amount of items: 3
Items: 
Size: 393694 Color: 7873
Size: 331566 Color: 5691
Size: 274738 Color: 2543

Bin 3205: 3 of cap free
Amount of items: 3
Items: 
Size: 364635 Color: 6999
Size: 358971 Color: 6811
Size: 276392 Color: 2675

Bin 3206: 3 of cap free
Amount of items: 3
Items: 
Size: 351338 Color: 6520
Size: 343525 Color: 6210
Size: 305135 Color: 4422

Bin 3207: 3 of cap free
Amount of items: 3
Items: 
Size: 385615 Color: 7628
Size: 326982 Color: 5499
Size: 287401 Color: 3399

Bin 3208: 3 of cap free
Amount of items: 3
Items: 
Size: 376491 Color: 7357
Size: 326939 Color: 5497
Size: 296568 Color: 3960

Bin 3209: 3 of cap free
Amount of items: 3
Items: 
Size: 373320 Color: 7252
Size: 324443 Color: 5376
Size: 302235 Color: 4258

Bin 3210: 3 of cap free
Amount of items: 3
Items: 
Size: 381767 Color: 7523
Size: 334874 Color: 5845
Size: 283357 Color: 3136

Bin 3211: 3 of cap free
Amount of items: 3
Items: 
Size: 372412 Color: 7227
Size: 344292 Color: 6244
Size: 283294 Color: 3132

Bin 3212: 3 of cap free
Amount of items: 3
Items: 
Size: 343816 Color: 6222
Size: 328161 Color: 5547
Size: 328021 Color: 5539

Bin 3213: 3 of cap free
Amount of items: 3
Items: 
Size: 373007 Color: 7241
Size: 333960 Color: 5797
Size: 293031 Color: 3756

Bin 3214: 3 of cap free
Amount of items: 3
Items: 
Size: 358944 Color: 6810
Size: 357599 Color: 6755
Size: 283455 Color: 3141

Bin 3215: 3 of cap free
Amount of items: 3
Items: 
Size: 359004 Color: 6813
Size: 355822 Color: 6685
Size: 285172 Color: 3245

Bin 3216: 3 of cap free
Amount of items: 3
Items: 
Size: 357467 Color: 6749
Size: 344556 Color: 6256
Size: 297975 Color: 4033

Bin 3217: 3 of cap free
Amount of items: 3
Items: 
Size: 367806 Color: 7090
Size: 343392 Color: 6207
Size: 288800 Color: 3484

Bin 3218: 3 of cap free
Amount of items: 3
Items: 
Size: 351067 Color: 6509
Size: 338358 Color: 6005
Size: 310573 Color: 4711

Bin 3219: 4 of cap free
Amount of items: 3
Items: 
Size: 405640 Color: 8200
Size: 318824 Color: 5130
Size: 275533 Color: 2604

Bin 3220: 4 of cap free
Amount of items: 3
Items: 
Size: 410802 Color: 8313
Size: 298846 Color: 4082
Size: 290349 Color: 3581

Bin 3221: 4 of cap free
Amount of items: 3
Items: 
Size: 367561 Color: 7080
Size: 332158 Color: 5720
Size: 300278 Color: 4140

Bin 3222: 4 of cap free
Amount of items: 3
Items: 
Size: 403918 Color: 8148
Size: 321808 Color: 5265
Size: 274271 Color: 2499

Bin 3223: 4 of cap free
Amount of items: 3
Items: 
Size: 402729 Color: 8122
Size: 325188 Color: 5412
Size: 272080 Color: 2341

Bin 3224: 4 of cap free
Amount of items: 3
Items: 
Size: 355968 Color: 6694
Size: 349305 Color: 6436
Size: 294724 Color: 3854

Bin 3225: 4 of cap free
Amount of items: 3
Items: 
Size: 371381 Color: 7193
Size: 337556 Color: 5964
Size: 291060 Color: 3631

Bin 3226: 4 of cap free
Amount of items: 3
Items: 
Size: 363162 Color: 6945
Size: 351752 Color: 6538
Size: 285083 Color: 3239

Bin 3227: 4 of cap free
Amount of items: 3
Items: 
Size: 394730 Color: 7905
Size: 323119 Color: 5318
Size: 282148 Color: 3067

Bin 3228: 4 of cap free
Amount of items: 3
Items: 
Size: 347598 Color: 6374
Size: 347596 Color: 6373
Size: 304803 Color: 4399

Bin 3229: 4 of cap free
Amount of items: 3
Items: 
Size: 350664 Color: 6488
Size: 326179 Color: 5462
Size: 323154 Color: 5320

Bin 3230: 5 of cap free
Amount of items: 3
Items: 
Size: 411102 Color: 8323
Size: 296480 Color: 3950
Size: 292414 Color: 3719

Bin 3231: 5 of cap free
Amount of items: 3
Items: 
Size: 376293 Color: 7350
Size: 352201 Color: 6558
Size: 271502 Color: 2290

Bin 3232: 5 of cap free
Amount of items: 3
Items: 
Size: 402337 Color: 8106
Size: 318120 Color: 5100
Size: 279539 Color: 2891

Bin 3233: 5 of cap free
Amount of items: 3
Items: 
Size: 340785 Color: 6100
Size: 336171 Color: 5907
Size: 323040 Color: 5311

Bin 3234: 5 of cap free
Amount of items: 3
Items: 
Size: 365050 Color: 7008
Size: 341772 Color: 6142
Size: 293174 Color: 3763

Bin 3235: 5 of cap free
Amount of items: 3
Items: 
Size: 366494 Color: 7044
Size: 320096 Color: 5186
Size: 313406 Color: 4869

Bin 3236: 5 of cap free
Amount of items: 3
Items: 
Size: 341024 Color: 6111
Size: 334147 Color: 5807
Size: 324825 Color: 5393

Bin 3237: 5 of cap free
Amount of items: 3
Items: 
Size: 353107 Color: 6586
Size: 353040 Color: 6584
Size: 293849 Color: 3804

Bin 3238: 6 of cap free
Amount of items: 3
Items: 
Size: 345575 Color: 6295
Size: 342996 Color: 6186
Size: 311424 Color: 4758

Bin 3239: 6 of cap free
Amount of items: 3
Items: 
Size: 383495 Color: 7569
Size: 310609 Color: 4714
Size: 305891 Color: 4456

Bin 3240: 6 of cap free
Amount of items: 3
Items: 
Size: 360909 Color: 6866
Size: 321880 Color: 5271
Size: 317206 Color: 5058

Bin 3241: 6 of cap free
Amount of items: 3
Items: 
Size: 377428 Color: 7383
Size: 325133 Color: 5409
Size: 297434 Color: 4005

Bin 3242: 6 of cap free
Amount of items: 3
Items: 
Size: 362861 Color: 6940
Size: 331170 Color: 5675
Size: 305964 Color: 4460

Bin 3243: 6 of cap free
Amount of items: 3
Items: 
Size: 353682 Color: 6608
Size: 350952 Color: 6502
Size: 295361 Color: 3892

Bin 3244: 6 of cap free
Amount of items: 3
Items: 
Size: 359117 Color: 6816
Size: 357350 Color: 6743
Size: 283528 Color: 3147

Bin 3245: 7 of cap free
Amount of items: 3
Items: 
Size: 395029 Color: 7913
Size: 330044 Color: 5630
Size: 274921 Color: 2558

Bin 3246: 7 of cap free
Amount of items: 3
Items: 
Size: 364152 Color: 6981
Size: 330420 Color: 5644
Size: 305422 Color: 4430

Bin 3247: 7 of cap free
Amount of items: 3
Items: 
Size: 362083 Color: 6912
Size: 326658 Color: 5486
Size: 311253 Color: 4750

Bin 3248: 7 of cap free
Amount of items: 3
Items: 
Size: 376512 Color: 7360
Size: 329324 Color: 5597
Size: 294158 Color: 3818

Bin 3249: 8 of cap free
Amount of items: 3
Items: 
Size: 390951 Color: 7782
Size: 328515 Color: 5564
Size: 280527 Color: 2962

Bin 3250: 8 of cap free
Amount of items: 3
Items: 
Size: 353819 Color: 6614
Size: 351407 Color: 6523
Size: 294767 Color: 3858

Bin 3251: 8 of cap free
Amount of items: 3
Items: 
Size: 349987 Color: 6455
Size: 328491 Color: 5563
Size: 321515 Color: 5251

Bin 3252: 8 of cap free
Amount of items: 3
Items: 
Size: 375754 Color: 7329
Size: 333248 Color: 5768
Size: 290991 Color: 3628

Bin 3253: 8 of cap free
Amount of items: 3
Items: 
Size: 367724 Color: 7087
Size: 344181 Color: 6238
Size: 288088 Color: 3447

Bin 3254: 9 of cap free
Amount of items: 3
Items: 
Size: 405075 Color: 8180
Size: 304803 Color: 4398
Size: 290114 Color: 3568

Bin 3255: 10 of cap free
Amount of items: 3
Items: 
Size: 366501 Color: 7046
Size: 329137 Color: 5585
Size: 304353 Color: 4371

Bin 3256: 10 of cap free
Amount of items: 3
Items: 
Size: 344951 Color: 6273
Size: 336879 Color: 5938
Size: 318161 Color: 5102

Bin 3257: 10 of cap free
Amount of items: 3
Items: 
Size: 350832 Color: 6495
Size: 334336 Color: 5817
Size: 314823 Color: 4937

Bin 3258: 11 of cap free
Amount of items: 3
Items: 
Size: 375411 Color: 7320
Size: 346030 Color: 6311
Size: 278549 Color: 2831

Bin 3259: 11 of cap free
Amount of items: 3
Items: 
Size: 383773 Color: 7575
Size: 339729 Color: 6051
Size: 276488 Color: 2684

Bin 3260: 11 of cap free
Amount of items: 3
Items: 
Size: 386015 Color: 7638
Size: 320788 Color: 5214
Size: 293187 Color: 3764

Bin 3261: 12 of cap free
Amount of items: 3
Items: 
Size: 345291 Color: 6284
Size: 341229 Color: 6122
Size: 313469 Color: 4871

Bin 3262: 13 of cap free
Amount of items: 3
Items: 
Size: 356587 Color: 6722
Size: 328385 Color: 5558
Size: 315016 Color: 4948

Bin 3263: 13 of cap free
Amount of items: 3
Items: 
Size: 399437 Color: 8043
Size: 315856 Color: 4988
Size: 284695 Color: 3216

Bin 3264: 13 of cap free
Amount of items: 3
Items: 
Size: 362815 Color: 6938
Size: 337556 Color: 5965
Size: 299617 Color: 4108

Bin 3265: 13 of cap free
Amount of items: 3
Items: 
Size: 356432 Color: 6718
Size: 347060 Color: 6355
Size: 296496 Color: 3952

Bin 3266: 13 of cap free
Amount of items: 3
Items: 
Size: 386796 Color: 7665
Size: 313492 Color: 4873
Size: 299700 Color: 4113

Bin 3267: 14 of cap free
Amount of items: 3
Items: 
Size: 393758 Color: 7878
Size: 318921 Color: 5134
Size: 287308 Color: 3391

Bin 3268: 14 of cap free
Amount of items: 3
Items: 
Size: 403358 Color: 8137
Size: 308603 Color: 4592
Size: 288026 Color: 3441

Bin 3269: 15 of cap free
Amount of items: 3
Items: 
Size: 399800 Color: 8053
Size: 320451 Color: 5201
Size: 279735 Color: 2900

Bin 3270: 15 of cap free
Amount of items: 3
Items: 
Size: 357382 Color: 6744
Size: 323408 Color: 5332
Size: 319196 Color: 5144

Bin 3271: 15 of cap free
Amount of items: 3
Items: 
Size: 350624 Color: 6485
Size: 350396 Color: 6476
Size: 298966 Color: 4086

Bin 3272: 16 of cap free
Amount of items: 3
Items: 
Size: 355719 Color: 6680
Size: 341226 Color: 6121
Size: 303040 Color: 4303

Bin 3273: 16 of cap free
Amount of items: 3
Items: 
Size: 409162 Color: 8274
Size: 325511 Color: 5432
Size: 265312 Color: 1770

Bin 3274: 17 of cap free
Amount of items: 3
Items: 
Size: 374508 Color: 7291
Size: 339052 Color: 6026
Size: 286424 Color: 3343

Bin 3275: 17 of cap free
Amount of items: 3
Items: 
Size: 402462 Color: 8111
Size: 320617 Color: 5208
Size: 276905 Color: 2716

Bin 3276: 17 of cap free
Amount of items: 3
Items: 
Size: 359044 Color: 6814
Size: 339842 Color: 6056
Size: 301098 Color: 4191

Bin 3277: 18 of cap free
Amount of items: 3
Items: 
Size: 362639 Color: 6932
Size: 348609 Color: 6410
Size: 288735 Color: 3480

Bin 3278: 18 of cap free
Amount of items: 3
Items: 
Size: 347647 Color: 6377
Size: 342732 Color: 6176
Size: 309604 Color: 4647

Bin 3279: 19 of cap free
Amount of items: 3
Items: 
Size: 360711 Color: 6857
Size: 344695 Color: 6262
Size: 294576 Color: 3845

Bin 3280: 21 of cap free
Amount of items: 3
Items: 
Size: 407034 Color: 8234
Size: 316176 Color: 5005
Size: 276770 Color: 2705

Bin 3281: 22 of cap free
Amount of items: 3
Items: 
Size: 357419 Color: 6746
Size: 356038 Color: 6702
Size: 286522 Color: 3352

Bin 3282: 23 of cap free
Amount of items: 3
Items: 
Size: 404358 Color: 8164
Size: 302879 Color: 4293
Size: 292741 Color: 3737

Bin 3283: 23 of cap free
Amount of items: 3
Items: 
Size: 351735 Color: 6537
Size: 340902 Color: 6105
Size: 307341 Color: 4530

Bin 3284: 24 of cap free
Amount of items: 3
Items: 
Size: 373314 Color: 7251
Size: 321359 Color: 5243
Size: 305304 Color: 4427

Bin 3285: 25 of cap free
Amount of items: 3
Items: 
Size: 398263 Color: 8014
Size: 339626 Color: 6047
Size: 262087 Color: 1513

Bin 3286: 25 of cap free
Amount of items: 3
Items: 
Size: 351450 Color: 6525
Size: 349430 Color: 6441
Size: 299096 Color: 4090

Bin 3287: 25 of cap free
Amount of items: 3
Items: 
Size: 349039 Color: 6427
Size: 347187 Color: 6360
Size: 303750 Color: 4333

Bin 3288: 26 of cap free
Amount of items: 3
Items: 
Size: 355782 Color: 6683
Size: 333031 Color: 5763
Size: 311162 Color: 4745

Bin 3289: 28 of cap free
Amount of items: 3
Items: 
Size: 346711 Color: 6340
Size: 340457 Color: 6090
Size: 312805 Color: 4836

Bin 3290: 28 of cap free
Amount of items: 3
Items: 
Size: 357201 Color: 6737
Size: 353449 Color: 6599
Size: 289323 Color: 3522

Bin 3291: 30 of cap free
Amount of items: 3
Items: 
Size: 388540 Color: 7712
Size: 327432 Color: 5517
Size: 283999 Color: 3173

Bin 3292: 31 of cap free
Amount of items: 3
Items: 
Size: 346585 Color: 6337
Size: 330794 Color: 5657
Size: 322591 Color: 5300

Bin 3293: 32 of cap free
Amount of items: 3
Items: 
Size: 404248 Color: 8159
Size: 298552 Color: 4072
Size: 297169 Color: 3992

Bin 3294: 33 of cap free
Amount of items: 3
Items: 
Size: 364291 Color: 6984
Size: 328428 Color: 5559
Size: 307249 Color: 4519

Bin 3295: 37 of cap free
Amount of items: 3
Items: 
Size: 399613 Color: 8049
Size: 341802 Color: 6144
Size: 258549 Color: 1164

Bin 3296: 38 of cap free
Amount of items: 3
Items: 
Size: 366601 Color: 7049
Size: 348040 Color: 6390
Size: 285322 Color: 3259

Bin 3297: 42 of cap free
Amount of items: 3
Items: 
Size: 395006 Color: 7912
Size: 310399 Color: 4699
Size: 294554 Color: 3843

Bin 3298: 42 of cap free
Amount of items: 3
Items: 
Size: 339753 Color: 6052
Size: 334185 Color: 5811
Size: 326021 Color: 5458

Bin 3299: 43 of cap free
Amount of items: 3
Items: 
Size: 383078 Color: 7559
Size: 335126 Color: 5860
Size: 281754 Color: 3036

Bin 3300: 47 of cap free
Amount of items: 3
Items: 
Size: 406285 Color: 8220
Size: 313784 Color: 4893
Size: 279885 Color: 2911

Bin 3301: 47 of cap free
Amount of items: 3
Items: 
Size: 353872 Color: 6616
Size: 328852 Color: 5573
Size: 317230 Color: 5060

Bin 3302: 51 of cap free
Amount of items: 3
Items: 
Size: 360052 Color: 6844
Size: 329127 Color: 5584
Size: 310771 Color: 4724

Bin 3303: 52 of cap free
Amount of items: 3
Items: 
Size: 367909 Color: 7097
Size: 319603 Color: 5163
Size: 312437 Color: 4816

Bin 3304: 52 of cap free
Amount of items: 3
Items: 
Size: 358669 Color: 6793
Size: 344221 Color: 6241
Size: 297059 Color: 3988

Bin 3305: 55 of cap free
Amount of items: 3
Items: 
Size: 393945 Color: 7883
Size: 328120 Color: 5544
Size: 277881 Color: 2775

Bin 3306: 58 of cap free
Amount of items: 3
Items: 
Size: 396035 Color: 7955
Size: 340508 Color: 6092
Size: 263400 Color: 1621

Bin 3307: 59 of cap free
Amount of items: 3
Items: 
Size: 358726 Color: 6795
Size: 331905 Color: 5707
Size: 309311 Color: 4628

Bin 3308: 60 of cap free
Amount of items: 3
Items: 
Size: 390473 Color: 7772
Size: 309604 Color: 4646
Size: 299864 Color: 4124

Bin 3309: 65 of cap free
Amount of items: 3
Items: 
Size: 348103 Color: 6391
Size: 334268 Color: 5815
Size: 317565 Color: 5075

Bin 3310: 74 of cap free
Amount of items: 3
Items: 
Size: 355407 Color: 6674
Size: 350515 Color: 6481
Size: 294005 Color: 3809

Bin 3311: 91 of cap free
Amount of items: 3
Items: 
Size: 396935 Color: 7975
Size: 338649 Color: 6014
Size: 264326 Color: 1696

Bin 3312: 100 of cap free
Amount of items: 3
Items: 
Size: 340310 Color: 6082
Size: 334796 Color: 5839
Size: 324795 Color: 5392

Bin 3313: 105 of cap free
Amount of items: 3
Items: 
Size: 400266 Color: 8067
Size: 331073 Color: 5672
Size: 268557 Color: 2042

Bin 3314: 106 of cap free
Amount of items: 3
Items: 
Size: 389585 Color: 7745
Size: 336300 Color: 5917
Size: 274010 Color: 2488

Bin 3315: 107 of cap free
Amount of items: 3
Items: 
Size: 369807 Color: 7159
Size: 328671 Color: 5569
Size: 301416 Color: 4208

Bin 3316: 120 of cap free
Amount of items: 3
Items: 
Size: 361182 Color: 6878
Size: 324241 Color: 5370
Size: 314458 Color: 4927

Bin 3317: 122 of cap free
Amount of items: 3
Items: 
Size: 344674 Color: 6259
Size: 339048 Color: 6025
Size: 316157 Color: 5004

Bin 3318: 125 of cap free
Amount of items: 3
Items: 
Size: 373804 Color: 7272
Size: 332590 Color: 5737
Size: 293482 Color: 3780

Bin 3319: 128 of cap free
Amount of items: 3
Items: 
Size: 348381 Color: 6398
Size: 343727 Color: 6217
Size: 307765 Color: 4551

Bin 3320: 165 of cap free
Amount of items: 3
Items: 
Size: 367313 Color: 7072
Size: 340433 Color: 6089
Size: 292090 Color: 3702

Bin 3321: 171 of cap free
Amount of items: 3
Items: 
Size: 346222 Color: 6321
Size: 330506 Color: 5649
Size: 323102 Color: 5314

Bin 3322: 189 of cap free
Amount of items: 3
Items: 
Size: 359311 Color: 6818
Size: 335906 Color: 5895
Size: 304595 Color: 4385

Bin 3323: 215 of cap free
Amount of items: 3
Items: 
Size: 358342 Color: 6781
Size: 344920 Color: 6268
Size: 296524 Color: 3956

Bin 3324: 219 of cap free
Amount of items: 3
Items: 
Size: 393941 Color: 7882
Size: 319875 Color: 5172
Size: 285966 Color: 3310

Bin 3325: 272 of cap free
Amount of items: 3
Items: 
Size: 350592 Color: 6482
Size: 335519 Color: 5884
Size: 313618 Color: 4881

Bin 3326: 275 of cap free
Amount of items: 3
Items: 
Size: 363452 Color: 6960
Size: 330461 Color: 5646
Size: 305813 Color: 4449

Bin 3327: 282 of cap free
Amount of items: 3
Items: 
Size: 371044 Color: 7188
Size: 318058 Color: 5096
Size: 310617 Color: 4715

Bin 3328: 364 of cap free
Amount of items: 3
Items: 
Size: 380535 Color: 7477
Size: 311775 Color: 4776
Size: 307327 Color: 4526

Bin 3329: 614 of cap free
Amount of items: 3
Items: 
Size: 399052 Color: 8032
Size: 319543 Color: 5158
Size: 280792 Color: 2975

Bin 3330: 921 of cap free
Amount of items: 3
Items: 
Size: 356871 Color: 6725
Size: 355741 Color: 6682
Size: 286468 Color: 3347

Bin 3331: 1875 of cap free
Amount of items: 3
Items: 
Size: 362547 Color: 6931
Size: 340175 Color: 6078
Size: 295404 Color: 3895

Bin 3332: 3327 of cap free
Amount of items: 3
Items: 
Size: 362352 Color: 6925
Size: 346847 Color: 6347
Size: 287475 Color: 3401

Bin 3333: 4887 of cap free
Amount of items: 3
Items: 
Size: 410668 Color: 8308
Size: 318455 Color: 5114
Size: 265991 Color: 1827

Bin 3334: 244929 of cap free
Amount of items: 2
Items: 
Size: 386300 Color: 7647
Size: 368772 Color: 7129

Bin 3335: 738261 of cap free
Amount of items: 1
Items: 
Size: 261740 Color: 1488

Total size: 3334003334
Total free space: 1000001

