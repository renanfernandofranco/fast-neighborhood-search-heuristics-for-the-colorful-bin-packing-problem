Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 322 Color: 17
Size: 304 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 0
Size: 300 Color: 15
Size: 250 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 15
Size: 344 Color: 16
Size: 276 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 4
Size: 262 Color: 13
Size: 253 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 365 Color: 9
Size: 261 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 317 Color: 14
Size: 424 Color: 19
Size: 259 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 18
Size: 357 Color: 4
Size: 275 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 16
Size: 322 Color: 10
Size: 278 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 9
Size: 355 Color: 10
Size: 276 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 14
Size: 339 Color: 14
Size: 273 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 284 Color: 5
Size: 265 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 19
Size: 321 Color: 19
Size: 274 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 7
Size: 276 Color: 4
Size: 275 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 18
Size: 328 Color: 8
Size: 297 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 337 Color: 5
Size: 269 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 14
Size: 310 Color: 7
Size: 287 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 7
Size: 284 Color: 16
Size: 252 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 7
Size: 292 Color: 10
Size: 281 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 279 Color: 4
Size: 250 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 8
Size: 252 Color: 17
Size: 250 Color: 19

Total size: 20000
Total free space: 0

