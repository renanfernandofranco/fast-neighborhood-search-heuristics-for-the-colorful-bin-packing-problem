Capicity Bin: 16416
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9663 Color: 3
Size: 5765 Color: 2
Size: 988 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9840 Color: 3
Size: 5488 Color: 2
Size: 1088 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10100 Color: 3
Size: 5896 Color: 0
Size: 420 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10164 Color: 3
Size: 5852 Color: 1
Size: 400 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10455 Color: 2
Size: 4969 Color: 4
Size: 992 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10734 Color: 3
Size: 5268 Color: 1
Size: 414 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10916 Color: 3
Size: 4508 Color: 4
Size: 992 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11360 Color: 1
Size: 4328 Color: 2
Size: 728 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11430 Color: 4
Size: 4560 Color: 1
Size: 426 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11688 Color: 3
Size: 4112 Color: 1
Size: 616 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11830 Color: 1
Size: 3424 Color: 2
Size: 1162 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 1
Size: 3572 Color: 1
Size: 944 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11879 Color: 2
Size: 3929 Color: 2
Size: 608 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12260 Color: 0
Size: 3856 Color: 1
Size: 300 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12291 Color: 4
Size: 2584 Color: 4
Size: 1541 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12312 Color: 1
Size: 3432 Color: 2
Size: 672 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12464 Color: 0
Size: 3344 Color: 4
Size: 608 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12612 Color: 4
Size: 3124 Color: 1
Size: 680 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12791 Color: 1
Size: 2040 Color: 0
Size: 1585 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12781 Color: 2
Size: 3031 Color: 4
Size: 604 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12848 Color: 4
Size: 3092 Color: 1
Size: 476 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13042 Color: 4
Size: 1813 Color: 3
Size: 1561 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 0
Size: 3026 Color: 0
Size: 294 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 3
Size: 2988 Color: 1
Size: 278 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13223 Color: 0
Size: 2661 Color: 1
Size: 532 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 1
Size: 2572 Color: 2
Size: 560 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13366 Color: 1
Size: 2184 Color: 2
Size: 866 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13398 Color: 0
Size: 2582 Color: 1
Size: 436 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 2
Size: 1592 Color: 4
Size: 1360 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13488 Color: 0
Size: 2448 Color: 1
Size: 480 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13606 Color: 0
Size: 1482 Color: 2
Size: 1328 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 1
Size: 1380 Color: 3
Size: 1364 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13750 Color: 1
Size: 2360 Color: 2
Size: 306 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13778 Color: 3
Size: 2342 Color: 0
Size: 296 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13936 Color: 1
Size: 2004 Color: 2
Size: 476 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13957 Color: 4
Size: 1771 Color: 2
Size: 688 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 0
Size: 2026 Color: 0
Size: 404 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13953 Color: 2
Size: 1507 Color: 0
Size: 956 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14012 Color: 0
Size: 2056 Color: 3
Size: 348 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 2
Size: 1620 Color: 0
Size: 632 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14176 Color: 2
Size: 1564 Color: 0
Size: 676 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14224 Color: 0
Size: 1488 Color: 2
Size: 704 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14257 Color: 3
Size: 1853 Color: 2
Size: 306 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14265 Color: 4
Size: 1793 Color: 2
Size: 358 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 2
Size: 1452 Color: 0
Size: 672 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14314 Color: 2
Size: 1366 Color: 0
Size: 736 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14352 Color: 2
Size: 1168 Color: 1
Size: 896 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 0
Size: 1364 Color: 2
Size: 612 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14500 Color: 2
Size: 1152 Color: 3
Size: 764 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14520 Color: 1
Size: 1024 Color: 2
Size: 872 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 0
Size: 1480 Color: 4
Size: 414 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14545 Color: 4
Size: 1517 Color: 4
Size: 354 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14548 Color: 3
Size: 1184 Color: 3
Size: 684 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 1
Size: 1772 Color: 3
Size: 32 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14640 Color: 0
Size: 1040 Color: 2
Size: 736 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14676 Color: 0
Size: 1076 Color: 2
Size: 664 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14704 Color: 3
Size: 1336 Color: 4
Size: 376 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14716 Color: 3
Size: 1420 Color: 2
Size: 280 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14764 Color: 3
Size: 1172 Color: 2
Size: 480 Color: 1

Bin 60: 1 of cap free
Amount of items: 5
Items: 
Size: 8218 Color: 2
Size: 4341 Color: 2
Size: 3082 Color: 1
Size: 416 Color: 3
Size: 358 Color: 2

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 11209 Color: 1
Size: 4738 Color: 2
Size: 468 Color: 3

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 11404 Color: 0
Size: 5011 Color: 3

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11572 Color: 3
Size: 4331 Color: 2
Size: 512 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 1
Size: 2051 Color: 0
Size: 1528 Color: 2

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13189 Color: 2
Size: 2598 Color: 3
Size: 628 Color: 1

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13583 Color: 1
Size: 2528 Color: 3
Size: 304 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13857 Color: 1
Size: 2256 Color: 2
Size: 302 Color: 0

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13890 Color: 3
Size: 1581 Color: 4
Size: 944 Color: 1

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13905 Color: 0
Size: 2096 Color: 2
Size: 414 Color: 1

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 14261 Color: 2
Size: 1796 Color: 3
Size: 358 Color: 4

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 14289 Color: 1
Size: 1582 Color: 2
Size: 544 Color: 3

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 14573 Color: 4
Size: 1634 Color: 1
Size: 208 Color: 2

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 14597 Color: 0
Size: 1456 Color: 3
Size: 362 Color: 2

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 9439 Color: 3
Size: 6557 Color: 1
Size: 418 Color: 0

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 9594 Color: 2
Size: 6444 Color: 2
Size: 376 Color: 3

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 9954 Color: 3
Size: 3439 Color: 4
Size: 3021 Color: 0

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 12196 Color: 4
Size: 3770 Color: 1
Size: 448 Color: 4

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 12354 Color: 4
Size: 3468 Color: 2
Size: 592 Color: 1

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 12676 Color: 3
Size: 3386 Color: 2
Size: 352 Color: 2

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 12740 Color: 4
Size: 3522 Color: 1
Size: 152 Color: 0

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 12804 Color: 0
Size: 3002 Color: 4
Size: 608 Color: 1

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 4
Size: 2390 Color: 0

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 14193 Color: 3
Size: 1621 Color: 2
Size: 600 Color: 1

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 14666 Color: 0
Size: 1744 Color: 1
Size: 4 Color: 0

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 10228 Color: 3
Size: 5815 Color: 1
Size: 370 Color: 2

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 10405 Color: 2
Size: 5656 Color: 3
Size: 352 Color: 1

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 11894 Color: 4
Size: 3911 Color: 4
Size: 608 Color: 2

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 13199 Color: 0
Size: 2814 Color: 1
Size: 400 Color: 4

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 13200 Color: 3
Size: 3213 Color: 2

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 13553 Color: 4
Size: 2324 Color: 1
Size: 536 Color: 0

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 1
Size: 2093 Color: 2
Size: 576 Color: 2

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 4
Size: 2081 Color: 3
Size: 64 Color: 0

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 14737 Color: 1
Size: 1644 Color: 0
Size: 32 Color: 0

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 8220 Color: 0
Size: 6834 Color: 1
Size: 1358 Color: 3

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 2
Size: 6832 Color: 4
Size: 272 Color: 1

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 9810 Color: 3
Size: 6602 Color: 4

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 10170 Color: 2
Size: 5866 Color: 4
Size: 376 Color: 4

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 10936 Color: 0
Size: 4976 Color: 1
Size: 500 Color: 3

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 11012 Color: 2
Size: 5048 Color: 4
Size: 352 Color: 3

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 12132 Color: 0
Size: 3764 Color: 1
Size: 516 Color: 1

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 3
Size: 3652 Color: 0

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 12772 Color: 2
Size: 3480 Color: 4
Size: 160 Color: 1

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 0
Size: 2612 Color: 2

Bin 104: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 2
Size: 2584 Color: 4

Bin 105: 4 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 0
Size: 2520 Color: 2

Bin 106: 5 of cap free
Amount of items: 6
Items: 
Size: 8212 Color: 4
Size: 2336 Color: 4
Size: 1801 Color: 3
Size: 1800 Color: 3
Size: 1462 Color: 4
Size: 800 Color: 2

Bin 107: 5 of cap free
Amount of items: 3
Items: 
Size: 9314 Color: 1
Size: 6793 Color: 2
Size: 304 Color: 0

Bin 108: 5 of cap free
Amount of items: 3
Items: 
Size: 9483 Color: 1
Size: 6624 Color: 2
Size: 304 Color: 0

Bin 109: 5 of cap free
Amount of items: 3
Items: 
Size: 13960 Color: 2
Size: 2387 Color: 0
Size: 64 Color: 1

Bin 110: 5 of cap free
Amount of items: 2
Items: 
Size: 14638 Color: 4
Size: 1773 Color: 1

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 8240 Color: 4
Size: 6826 Color: 1
Size: 1344 Color: 0

Bin 112: 6 of cap free
Amount of items: 3
Items: 
Size: 9244 Color: 1
Size: 6646 Color: 0
Size: 520 Color: 2

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 9104 Color: 2
Size: 7306 Color: 3

Bin 114: 6 of cap free
Amount of items: 3
Items: 
Size: 10296 Color: 1
Size: 5714 Color: 3
Size: 400 Color: 1

Bin 115: 6 of cap free
Amount of items: 3
Items: 
Size: 11891 Color: 0
Size: 2982 Color: 3
Size: 1537 Color: 0

Bin 116: 6 of cap free
Amount of items: 2
Items: 
Size: 12036 Color: 4
Size: 4374 Color: 1

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 11221 Color: 2
Size: 4676 Color: 1
Size: 512 Color: 3

Bin 118: 7 of cap free
Amount of items: 3
Items: 
Size: 14473 Color: 4
Size: 1908 Color: 1
Size: 28 Color: 2

Bin 119: 7 of cap free
Amount of items: 2
Items: 
Size: 14569 Color: 3
Size: 1840 Color: 0

Bin 120: 8 of cap free
Amount of items: 3
Items: 
Size: 11802 Color: 2
Size: 3822 Color: 1
Size: 784 Color: 3

Bin 121: 8 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 0
Size: 3472 Color: 3
Size: 688 Color: 3

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 1
Size: 1824 Color: 3

Bin 123: 8 of cap free
Amount of items: 3
Items: 
Size: 14700 Color: 0
Size: 1656 Color: 1
Size: 52 Color: 0

Bin 124: 9 of cap free
Amount of items: 3
Items: 
Size: 9352 Color: 0
Size: 6687 Color: 1
Size: 368 Color: 2

Bin 125: 9 of cap free
Amount of items: 2
Items: 
Size: 12978 Color: 2
Size: 3429 Color: 3

Bin 126: 10 of cap free
Amount of items: 3
Items: 
Size: 11504 Color: 3
Size: 3044 Color: 1
Size: 1858 Color: 2

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 4
Size: 2990 Color: 0

Bin 128: 10 of cap free
Amount of items: 3
Items: 
Size: 13550 Color: 0
Size: 2692 Color: 3
Size: 164 Color: 4

Bin 129: 10 of cap free
Amount of items: 2
Items: 
Size: 13864 Color: 1
Size: 2542 Color: 3

Bin 130: 10 of cap free
Amount of items: 2
Items: 
Size: 14609 Color: 3
Size: 1797 Color: 0

Bin 131: 11 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 1
Size: 5848 Color: 4
Size: 2053 Color: 0

Bin 132: 11 of cap free
Amount of items: 2
Items: 
Size: 14241 Color: 3
Size: 2164 Color: 1

Bin 133: 11 of cap free
Amount of items: 2
Items: 
Size: 14521 Color: 0
Size: 1884 Color: 4

Bin 134: 12 of cap free
Amount of items: 2
Items: 
Size: 13628 Color: 2
Size: 2776 Color: 4

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 1
Size: 4130 Color: 2

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 12626 Color: 1
Size: 3776 Color: 2

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 13302 Color: 4
Size: 3100 Color: 3

Bin 138: 14 of cap free
Amount of items: 2
Items: 
Size: 14394 Color: 3
Size: 2008 Color: 0

Bin 139: 14 of cap free
Amount of items: 2
Items: 
Size: 14648 Color: 0
Size: 1754 Color: 2

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 14293 Color: 0
Size: 2108 Color: 3

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 10304 Color: 0
Size: 6096 Color: 4

Bin 142: 19 of cap free
Amount of items: 2
Items: 
Size: 9236 Color: 4
Size: 7161 Color: 3

Bin 143: 19 of cap free
Amount of items: 3
Items: 
Size: 13921 Color: 2
Size: 2348 Color: 0
Size: 128 Color: 0

Bin 144: 19 of cap free
Amount of items: 2
Items: 
Size: 13925 Color: 2
Size: 2472 Color: 0

Bin 145: 19 of cap free
Amount of items: 3
Items: 
Size: 14132 Color: 4
Size: 2073 Color: 0
Size: 192 Color: 3

Bin 146: 19 of cap free
Amount of items: 2
Items: 
Size: 14264 Color: 3
Size: 2133 Color: 4

Bin 147: 20 of cap free
Amount of items: 2
Items: 
Size: 10960 Color: 2
Size: 5436 Color: 0

Bin 148: 20 of cap free
Amount of items: 4
Items: 
Size: 14142 Color: 0
Size: 2106 Color: 4
Size: 84 Color: 2
Size: 64 Color: 0

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 14194 Color: 4
Size: 2202 Color: 1

Bin 150: 20 of cap free
Amount of items: 2
Items: 
Size: 14476 Color: 1
Size: 1920 Color: 3

Bin 151: 21 of cap free
Amount of items: 3
Items: 
Size: 12100 Color: 4
Size: 2691 Color: 3
Size: 1604 Color: 3

Bin 152: 22 of cap free
Amount of items: 3
Items: 
Size: 8393 Color: 1
Size: 6600 Color: 0
Size: 1401 Color: 0

Bin 153: 24 of cap free
Amount of items: 8
Items: 
Size: 8216 Color: 2
Size: 1436 Color: 3
Size: 1360 Color: 4
Size: 1360 Color: 1
Size: 1108 Color: 1
Size: 1100 Color: 1
Size: 912 Color: 0
Size: 900 Color: 2

Bin 154: 24 of cap free
Amount of items: 2
Items: 
Size: 11824 Color: 2
Size: 4568 Color: 3

Bin 155: 25 of cap free
Amount of items: 12
Items: 
Size: 8209 Color: 4
Size: 864 Color: 3
Size: 864 Color: 1
Size: 832 Color: 2
Size: 828 Color: 1
Size: 824 Color: 3
Size: 800 Color: 2
Size: 768 Color: 4
Size: 696 Color: 4
Size: 696 Color: 0
Size: 686 Color: 0
Size: 324 Color: 0

Bin 156: 25 of cap free
Amount of items: 2
Items: 
Size: 14537 Color: 3
Size: 1854 Color: 4

Bin 157: 26 of cap free
Amount of items: 2
Items: 
Size: 12786 Color: 3
Size: 3604 Color: 2

Bin 158: 27 of cap free
Amount of items: 2
Items: 
Size: 14312 Color: 3
Size: 2077 Color: 1

Bin 159: 28 of cap free
Amount of items: 2
Items: 
Size: 11224 Color: 1
Size: 5164 Color: 4

Bin 160: 30 of cap free
Amount of items: 3
Items: 
Size: 8442 Color: 3
Size: 7688 Color: 2
Size: 256 Color: 0

Bin 161: 30 of cap free
Amount of items: 3
Items: 
Size: 10680 Color: 3
Size: 5386 Color: 0
Size: 320 Color: 2

Bin 162: 30 of cap free
Amount of items: 2
Items: 
Size: 12228 Color: 2
Size: 4158 Color: 3

Bin 163: 36 of cap free
Amount of items: 2
Items: 
Size: 13332 Color: 2
Size: 3048 Color: 4

Bin 164: 40 of cap free
Amount of items: 2
Items: 
Size: 11170 Color: 2
Size: 5206 Color: 0

Bin 165: 42 of cap free
Amount of items: 3
Items: 
Size: 8226 Color: 2
Size: 6836 Color: 2
Size: 1312 Color: 1

Bin 166: 42 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 4
Size: 4792 Color: 2
Size: 3334 Color: 2

Bin 167: 42 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 1
Size: 3846 Color: 2
Size: 616 Color: 2

Bin 168: 42 of cap free
Amount of items: 3
Items: 
Size: 12722 Color: 4
Size: 3492 Color: 2
Size: 160 Color: 4

Bin 169: 44 of cap free
Amount of items: 2
Items: 
Size: 10448 Color: 2
Size: 5924 Color: 4

Bin 170: 46 of cap free
Amount of items: 2
Items: 
Size: 12190 Color: 0
Size: 4180 Color: 2

Bin 171: 48 of cap free
Amount of items: 3
Items: 
Size: 12164 Color: 0
Size: 3012 Color: 3
Size: 1192 Color: 3

Bin 172: 50 of cap free
Amount of items: 2
Items: 
Size: 12842 Color: 2
Size: 3524 Color: 3

Bin 173: 52 of cap free
Amount of items: 2
Items: 
Size: 9455 Color: 4
Size: 6909 Color: 3

Bin 174: 54 of cap free
Amount of items: 2
Items: 
Size: 12418 Color: 4
Size: 3944 Color: 3

Bin 175: 56 of cap free
Amount of items: 2
Items: 
Size: 12592 Color: 0
Size: 3768 Color: 2

Bin 176: 56 of cap free
Amount of items: 2
Items: 
Size: 13188 Color: 0
Size: 3172 Color: 3

Bin 177: 56 of cap free
Amount of items: 2
Items: 
Size: 13632 Color: 4
Size: 2728 Color: 3

Bin 178: 60 of cap free
Amount of items: 2
Items: 
Size: 14458 Color: 3
Size: 1898 Color: 1

Bin 179: 62 of cap free
Amount of items: 2
Items: 
Size: 12818 Color: 3
Size: 3536 Color: 2

Bin 180: 64 of cap free
Amount of items: 10
Items: 
Size: 8210 Color: 1
Size: 1048 Color: 4
Size: 1046 Color: 1
Size: 1008 Color: 3
Size: 1000 Color: 2
Size: 928 Color: 2
Size: 896 Color: 1
Size: 752 Color: 3
Size: 752 Color: 0
Size: 712 Color: 0

Bin 181: 68 of cap free
Amount of items: 2
Items: 
Size: 13252 Color: 0
Size: 3096 Color: 4

Bin 182: 69 of cap free
Amount of items: 2
Items: 
Size: 12303 Color: 0
Size: 4044 Color: 3

Bin 183: 76 of cap free
Amount of items: 2
Items: 
Size: 10360 Color: 0
Size: 5980 Color: 3

Bin 184: 86 of cap free
Amount of items: 2
Items: 
Size: 13608 Color: 2
Size: 2722 Color: 4

Bin 185: 96 of cap free
Amount of items: 34
Items: 
Size: 736 Color: 3
Size: 712 Color: 3
Size: 626 Color: 1
Size: 604 Color: 4
Size: 604 Color: 3
Size: 600 Color: 0
Size: 596 Color: 3
Size: 596 Color: 2
Size: 592 Color: 3
Size: 544 Color: 2
Size: 544 Color: 1
Size: 544 Color: 0
Size: 536 Color: 4
Size: 508 Color: 1
Size: 480 Color: 3
Size: 472 Color: 2
Size: 464 Color: 4
Size: 464 Color: 2
Size: 464 Color: 1
Size: 432 Color: 1
Size: 424 Color: 1
Size: 416 Color: 3
Size: 416 Color: 0
Size: 410 Color: 4
Size: 408 Color: 4
Size: 384 Color: 4
Size: 384 Color: 3
Size: 384 Color: 1
Size: 352 Color: 4
Size: 352 Color: 0
Size: 320 Color: 4
Size: 320 Color: 0
Size: 320 Color: 0
Size: 312 Color: 0

Bin 186: 98 of cap free
Amount of items: 3
Items: 
Size: 8265 Color: 1
Size: 6837 Color: 2
Size: 1216 Color: 0

Bin 187: 98 of cap free
Amount of items: 2
Items: 
Size: 10812 Color: 1
Size: 5506 Color: 2

Bin 188: 101 of cap free
Amount of items: 4
Items: 
Size: 8213 Color: 4
Size: 3286 Color: 2
Size: 2992 Color: 1
Size: 1824 Color: 3

Bin 189: 108 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 3
Size: 3468 Color: 4
Size: 128 Color: 4

Bin 190: 108 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 0
Size: 2704 Color: 4

Bin 191: 110 of cap free
Amount of items: 2
Items: 
Size: 13144 Color: 2
Size: 3162 Color: 0

Bin 192: 126 of cap free
Amount of items: 2
Items: 
Size: 13929 Color: 0
Size: 2361 Color: 4

Bin 193: 128 of cap free
Amount of items: 2
Items: 
Size: 12080 Color: 2
Size: 4208 Color: 4

Bin 194: 136 of cap free
Amount of items: 2
Items: 
Size: 12708 Color: 3
Size: 3572 Color: 1

Bin 195: 156 of cap free
Amount of items: 2
Items: 
Size: 9416 Color: 3
Size: 6844 Color: 2

Bin 196: 164 of cap free
Amount of items: 2
Items: 
Size: 11462 Color: 1
Size: 4790 Color: 3

Bin 197: 198 of cap free
Amount of items: 2
Items: 
Size: 9378 Color: 4
Size: 6840 Color: 3

Bin 198: 210 of cap free
Amount of items: 2
Items: 
Size: 10670 Color: 0
Size: 5536 Color: 1

Bin 199: 12462 of cap free
Amount of items: 13
Items: 
Size: 368 Color: 2
Size: 322 Color: 1
Size: 320 Color: 3
Size: 314 Color: 3
Size: 312 Color: 4
Size: 310 Color: 4
Size: 288 Color: 4
Size: 288 Color: 3
Size: 288 Color: 2
Size: 288 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 280 Color: 1

Total size: 3250368
Total free space: 16416

