Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 87
Size: 330 Color: 66
Size: 295 Color: 44

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 112
Size: 276 Color: 34
Size: 259 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 73
Size: 389 Color: 92
Size: 260 Color: 20

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 81
Size: 340 Color: 71
Size: 295 Color: 45

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 108
Size: 289 Color: 40
Size: 279 Color: 36

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 85
Size: 363 Color: 79
Size: 265 Color: 25

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 80
Size: 324 Color: 61
Size: 311 Color: 54

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 86
Size: 368 Color: 84
Size: 260 Color: 21

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 83
Size: 353 Color: 74
Size: 280 Color: 37

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 107
Size: 303 Color: 49
Size: 268 Color: 30

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 98
Size: 316 Color: 57
Size: 282 Color: 38

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 94
Size: 311 Color: 55
Size: 296 Color: 47

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 91
Size: 313 Color: 56
Size: 298 Color: 48

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 109
Size: 316 Color: 58
Size: 251 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 90
Size: 363 Color: 77
Size: 254 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 119
Size: 252 Color: 8
Size: 250 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 110
Size: 303 Color: 51
Size: 253 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 104
Size: 328 Color: 64
Size: 251 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 89
Size: 357 Color: 76
Size: 262 Color: 23

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 116
Size: 259 Color: 18
Size: 250 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 113
Size: 265 Color: 27
Size: 252 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 103
Size: 322 Color: 60
Size: 257 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 106
Size: 296 Color: 46
Size: 277 Color: 35

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 100
Size: 335 Color: 68
Size: 259 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 97
Size: 336 Color: 69
Size: 265 Color: 26

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 101
Size: 317 Color: 59
Size: 275 Color: 33

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 93
Size: 353 Color: 75
Size: 255 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 117
Size: 256 Color: 14
Size: 251 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 114
Size: 262 Color: 24
Size: 253 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 111
Size: 292 Color: 42
Size: 260 Color: 22

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 102
Size: 303 Color: 50
Size: 283 Color: 39

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 72
Size: 328 Color: 63
Size: 325 Color: 62

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 105
Size: 308 Color: 53
Size: 268 Color: 31

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 99
Size: 329 Color: 65
Size: 268 Color: 29

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 115
Size: 259 Color: 19
Size: 250 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 96
Size: 308 Color: 52
Size: 294 Color: 43

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 82
Size: 363 Color: 78
Size: 271 Color: 32

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 88
Size: 331 Color: 67
Size: 289 Color: 41

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 118
Size: 254 Color: 12
Size: 250 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 95
Size: 338 Color: 70
Size: 266 Color: 28

Total size: 40000
Total free space: 0

