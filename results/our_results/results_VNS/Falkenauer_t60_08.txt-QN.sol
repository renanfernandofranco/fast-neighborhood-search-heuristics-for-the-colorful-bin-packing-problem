Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 51
Size: 293 Color: 27
Size: 256 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 48
Size: 357 Color: 38
Size: 252 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 39
Size: 346 Color: 33
Size: 293 Color: 26

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 56
Size: 282 Color: 21
Size: 250 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 57
Size: 256 Color: 10
Size: 259 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 55
Size: 284 Color: 23
Size: 254 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 40
Size: 356 Color: 37
Size: 283 Color: 22

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 52
Size: 292 Color: 25
Size: 255 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 45
Size: 370 Color: 44
Size: 252 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 59
Size: 252 Color: 5
Size: 250 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 35
Size: 335 Color: 30
Size: 312 Color: 29

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 53
Size: 275 Color: 18
Size: 272 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 43
Size: 363 Color: 41
Size: 269 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 42
Size: 352 Color: 34
Size: 280 Color: 20

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 47
Size: 354 Color: 36
Size: 263 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 58
Size: 258 Color: 11
Size: 251 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 46
Size: 341 Color: 31
Size: 278 Color: 19

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 49
Size: 343 Color: 32
Size: 259 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 54
Size: 295 Color: 28
Size: 251 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 50
Size: 286 Color: 24
Size: 275 Color: 17

Total size: 20000
Total free space: 0

