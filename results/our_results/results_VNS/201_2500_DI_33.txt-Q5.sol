Capicity Bin: 2428
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1263 Color: 0
Size: 975 Color: 1
Size: 190 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1283 Color: 0
Size: 955 Color: 4
Size: 190 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 1
Size: 949 Color: 1
Size: 188 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 4
Size: 631 Color: 0
Size: 154 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 4
Size: 635 Color: 1
Size: 126 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 4
Size: 629 Color: 3
Size: 124 Color: 3

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 1658 Color: 0
Size: 702 Color: 1
Size: 36 Color: 3
Size: 32 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 2
Size: 638 Color: 1
Size: 124 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 4
Size: 670 Color: 1
Size: 60 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 0
Size: 526 Color: 4
Size: 108 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 4
Size: 523 Color: 3
Size: 104 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1838 Color: 3
Size: 530 Color: 4
Size: 60 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 4
Size: 494 Color: 2
Size: 24 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1959 Color: 0
Size: 309 Color: 4
Size: 160 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 4
Size: 306 Color: 2
Size: 156 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 2
Size: 382 Color: 2
Size: 72 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 2
Size: 386 Color: 4
Size: 64 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 2
Size: 371 Color: 3
Size: 74 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1987 Color: 4
Size: 369 Color: 0
Size: 72 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2026 Color: 1
Size: 338 Color: 4
Size: 64 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 2
Size: 307 Color: 0
Size: 60 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2083 Color: 4
Size: 271 Color: 1
Size: 74 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 2
Size: 208 Color: 2
Size: 126 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2101 Color: 1
Size: 273 Color: 1
Size: 54 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2134 Color: 2
Size: 154 Color: 4
Size: 140 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2162 Color: 2
Size: 188 Color: 0
Size: 78 Color: 4

Bin 27: 1 of cap free
Amount of items: 5
Items: 
Size: 1215 Color: 2
Size: 662 Color: 4
Size: 378 Color: 2
Size: 88 Color: 4
Size: 84 Color: 2

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 2
Size: 1011 Color: 0
Size: 126 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1834 Color: 4
Size: 521 Color: 3
Size: 72 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1889 Color: 4
Size: 498 Color: 1
Size: 40 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 0
Size: 342 Color: 0
Size: 130 Color: 4

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 2102 Color: 3
Size: 325 Color: 2

Bin 33: 2 of cap free
Amount of items: 5
Items: 
Size: 1196 Color: 0
Size: 655 Color: 3
Size: 375 Color: 2
Size: 104 Color: 1
Size: 96 Color: 4

Bin 34: 2 of cap free
Amount of items: 4
Items: 
Size: 1481 Color: 0
Size: 769 Color: 4
Size: 132 Color: 2
Size: 44 Color: 2

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 4
Size: 659 Color: 1
Size: 104 Color: 3

Bin 36: 3 of cap free
Amount of items: 15
Items: 
Size: 292 Color: 4
Size: 245 Color: 3
Size: 222 Color: 4
Size: 202 Color: 1
Size: 200 Color: 3
Size: 194 Color: 4
Size: 194 Color: 2
Size: 154 Color: 0
Size: 152 Color: 0
Size: 144 Color: 3
Size: 104 Color: 0
Size: 96 Color: 3
Size: 92 Color: 2
Size: 90 Color: 4
Size: 44 Color: 4

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1454 Color: 1
Size: 971 Color: 4

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 4
Size: 451 Color: 3
Size: 176 Color: 3

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 3
Size: 391 Color: 1
Size: 64 Color: 3

Bin 40: 5 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 1
Size: 1060 Color: 0
Size: 76 Color: 3

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 1507 Color: 0
Size: 916 Color: 1

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 1499 Color: 3
Size: 771 Color: 4
Size: 152 Color: 2

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 1586 Color: 3
Size: 836 Color: 1

Bin 44: 7 of cap free
Amount of items: 4
Items: 
Size: 1259 Color: 0
Size: 1010 Color: 4
Size: 120 Color: 4
Size: 32 Color: 2

Bin 45: 7 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 4
Size: 814 Color: 4
Size: 104 Color: 2

Bin 46: 7 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 4
Size: 775 Color: 2
Size: 104 Color: 1

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 2039 Color: 1
Size: 382 Color: 0

Bin 48: 7 of cap free
Amount of items: 4
Items: 
Size: 2135 Color: 3
Size: 274 Color: 1
Size: 8 Color: 2
Size: 4 Color: 3

Bin 49: 8 of cap free
Amount of items: 3
Items: 
Size: 1219 Color: 1
Size: 1149 Color: 0
Size: 52 Color: 2

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1639 Color: 3
Size: 779 Color: 0

Bin 51: 10 of cap free
Amount of items: 2
Items: 
Size: 1779 Color: 0
Size: 639 Color: 3

Bin 52: 11 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 0
Size: 937 Color: 2
Size: 130 Color: 1

Bin 53: 11 of cap free
Amount of items: 2
Items: 
Size: 1626 Color: 1
Size: 791 Color: 3

Bin 54: 11 of cap free
Amount of items: 2
Items: 
Size: 2022 Color: 3
Size: 395 Color: 0

Bin 55: 13 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 0
Size: 951 Color: 1
Size: 246 Color: 4

Bin 56: 13 of cap free
Amount of items: 2
Items: 
Size: 2105 Color: 4
Size: 310 Color: 0

Bin 57: 15 of cap free
Amount of items: 2
Items: 
Size: 1671 Color: 0
Size: 742 Color: 3

Bin 58: 15 of cap free
Amount of items: 2
Items: 
Size: 1803 Color: 1
Size: 610 Color: 2

Bin 59: 15 of cap free
Amount of items: 2
Items: 
Size: 1979 Color: 2
Size: 434 Color: 1

Bin 60: 17 of cap free
Amount of items: 2
Items: 
Size: 1802 Color: 1
Size: 609 Color: 3

Bin 61: 19 of cap free
Amount of items: 2
Items: 
Size: 1459 Color: 3
Size: 950 Color: 0

Bin 62: 19 of cap free
Amount of items: 2
Items: 
Size: 1956 Color: 1
Size: 453 Color: 3

Bin 63: 21 of cap free
Amount of items: 2
Items: 
Size: 1885 Color: 3
Size: 522 Color: 1

Bin 64: 22 of cap free
Amount of items: 2
Items: 
Size: 2062 Color: 0
Size: 344 Color: 3

Bin 65: 31 of cap free
Amount of items: 2
Items: 
Size: 1495 Color: 1
Size: 902 Color: 3

Bin 66: 2086 of cap free
Amount of items: 6
Items: 
Size: 78 Color: 0
Size: 76 Color: 0
Size: 52 Color: 3
Size: 48 Color: 2
Size: 48 Color: 1
Size: 40 Color: 2

Total size: 157820
Total free space: 2428

