Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 7
Size: 316 Color: 3
Size: 250 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 13
Size: 335 Color: 13
Size: 305 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 11
Size: 322 Color: 10
Size: 255 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 5
Size: 370 Color: 3
Size: 253 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 11
Size: 306 Color: 8
Size: 303 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 15
Size: 371 Color: 0
Size: 258 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 19
Size: 251 Color: 14
Size: 250 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 19
Size: 269 Color: 6
Size: 265 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 16
Size: 271 Color: 12
Size: 255 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 268 Color: 18
Size: 257 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 300 Color: 19
Size: 253 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 19
Size: 266 Color: 2
Size: 254 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 6
Size: 318 Color: 14
Size: 265 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 5
Size: 322 Color: 10
Size: 261 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 352 Color: 7
Size: 256 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 16
Size: 267 Color: 19
Size: 257 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 5
Size: 325 Color: 5
Size: 283 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 13
Size: 356 Color: 18
Size: 273 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7
Size: 305 Color: 16
Size: 299 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 8
Size: 326 Color: 5
Size: 299 Color: 5

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 10
Size: 301 Color: 14
Size: 278 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 11
Size: 305 Color: 8
Size: 260 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 19
Size: 326 Color: 4
Size: 287 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 3
Size: 322 Color: 19
Size: 318 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 2
Size: 312 Color: 12
Size: 253 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 8
Size: 357 Color: 18
Size: 263 Color: 12

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 331 Color: 7
Size: 292 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 3
Size: 311 Color: 6
Size: 251 Color: 15

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9
Size: 271 Color: 11
Size: 256 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 15
Size: 307 Color: 1
Size: 286 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 18
Size: 369 Color: 6
Size: 255 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 267 Color: 12
Size: 254 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 7
Size: 302 Color: 4
Size: 258 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 14
Size: 302 Color: 4
Size: 263 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 324 Color: 10
Size: 318 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 18
Size: 368 Color: 0
Size: 250 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 11
Size: 353 Color: 1
Size: 274 Color: 16

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 14
Size: 304 Color: 16
Size: 288 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 11
Size: 316 Color: 16
Size: 259 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 282 Color: 13
Size: 281 Color: 7

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 10
Size: 325 Color: 7
Size: 291 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 18
Size: 322 Color: 11
Size: 250 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 16
Size: 355 Color: 14
Size: 280 Color: 14

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 13
Size: 333 Color: 1
Size: 307 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 14
Size: 304 Color: 13
Size: 287 Color: 12

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 291 Color: 19
Size: 291 Color: 18

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 2
Size: 287 Color: 14
Size: 259 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 11
Size: 346 Color: 4
Size: 275 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 8
Size: 287 Color: 13
Size: 263 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 14
Size: 308 Color: 11
Size: 251 Color: 19

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 289 Color: 12
Size: 265 Color: 15

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 301 Color: 6
Size: 273 Color: 12

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 295 Color: 0
Size: 262 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 18
Size: 277 Color: 6
Size: 261 Color: 18

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 7
Size: 273 Color: 17
Size: 254 Color: 17

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 0
Size: 348 Color: 17
Size: 293 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 10
Size: 341 Color: 5
Size: 286 Color: 16

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 315 Color: 6
Size: 301 Color: 10

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 11
Size: 334 Color: 18
Size: 267 Color: 12

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 5
Size: 314 Color: 10
Size: 258 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 14
Size: 342 Color: 16
Size: 312 Color: 8

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 8
Size: 336 Color: 15
Size: 282 Color: 9

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 11
Size: 335 Color: 17
Size: 252 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 17
Size: 293 Color: 9
Size: 267 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 4
Size: 287 Color: 16
Size: 259 Color: 19

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9
Size: 293 Color: 6
Size: 255 Color: 19

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 19
Size: 296 Color: 11
Size: 266 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 3
Size: 298 Color: 0
Size: 279 Color: 5

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 13
Size: 288 Color: 3
Size: 259 Color: 11

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 2
Size: 253 Color: 15
Size: 250 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 16
Size: 289 Color: 18
Size: 272 Color: 17

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 15
Size: 326 Color: 18
Size: 313 Color: 7

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 11
Size: 302 Color: 8
Size: 266 Color: 17

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 3
Size: 352 Color: 5
Size: 289 Color: 18

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 18
Size: 277 Color: 18
Size: 258 Color: 15

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 4
Size: 369 Color: 3
Size: 255 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 11
Size: 267 Color: 4
Size: 265 Color: 15

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 16
Size: 328 Color: 12
Size: 275 Color: 6

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 295 Color: 14
Size: 295 Color: 9

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9
Size: 272 Color: 16
Size: 263 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 254 Color: 11
Size: 252 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 356 Color: 18
Size: 277 Color: 9

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 18
Size: 284 Color: 9
Size: 250 Color: 17

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 14
Size: 302 Color: 11
Size: 261 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 16
Size: 325 Color: 3
Size: 281 Color: 18

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6
Size: 351 Color: 19
Size: 285 Color: 14

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 8
Size: 252 Color: 14
Size: 251 Color: 18

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 13
Size: 340 Color: 9
Size: 278 Color: 5

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 17
Size: 290 Color: 18
Size: 281 Color: 13

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 6
Size: 259 Color: 14
Size: 252 Color: 6

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 19
Size: 332 Color: 8
Size: 282 Color: 14

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 17
Size: 345 Color: 3
Size: 302 Color: 16

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 296 Color: 4
Size: 269 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 359 Color: 2
Size: 254 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 16
Size: 322 Color: 13
Size: 286 Color: 16

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 19
Size: 320 Color: 9
Size: 261 Color: 6

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 5
Size: 254 Color: 2
Size: 252 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 8
Size: 353 Color: 8
Size: 287 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9
Size: 293 Color: 15
Size: 262 Color: 13

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 359 Color: 11
Size: 259 Color: 5

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 10
Size: 281 Color: 18
Size: 267 Color: 5

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1
Size: 327 Color: 7
Size: 323 Color: 5

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 14
Size: 276 Color: 15
Size: 254 Color: 13

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 8
Size: 271 Color: 6
Size: 266 Color: 12

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 18
Size: 258 Color: 5
Size: 255 Color: 16

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 8
Size: 270 Color: 0
Size: 264 Color: 14

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 17
Size: 299 Color: 0
Size: 261 Color: 6

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 8
Size: 253 Color: 7
Size: 252 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 5
Size: 335 Color: 2
Size: 255 Color: 4

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 5
Size: 252 Color: 14
Size: 250 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 6
Size: 319 Color: 8
Size: 303 Color: 18

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 10
Size: 340 Color: 17
Size: 258 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 6
Size: 355 Color: 2
Size: 253 Color: 13

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 13
Size: 260 Color: 6
Size: 255 Color: 15

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 10
Size: 271 Color: 9
Size: 266 Color: 15

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 13
Size: 330 Color: 3
Size: 294 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 3
Size: 299 Color: 12
Size: 258 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 11
Size: 277 Color: 10
Size: 263 Color: 11

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9
Size: 303 Color: 6
Size: 254 Color: 17

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 19
Size: 277 Color: 11
Size: 261 Color: 16

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9
Size: 274 Color: 13
Size: 257 Color: 5

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 17
Size: 264 Color: 4
Size: 250 Color: 12

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 16
Size: 330 Color: 17
Size: 262 Color: 16

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 349 Color: 16
Size: 260 Color: 17

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 4
Size: 270 Color: 6
Size: 253 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 19
Size: 284 Color: 16
Size: 261 Color: 15

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 17
Size: 325 Color: 1
Size: 265 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 5
Size: 368 Color: 9
Size: 254 Color: 12

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 14
Size: 297 Color: 11
Size: 295 Color: 6

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 8
Size: 354 Color: 18
Size: 272 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 5
Size: 325 Color: 0
Size: 280 Color: 19

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 16
Size: 257 Color: 13
Size: 254 Color: 17

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 16
Size: 300 Color: 1
Size: 256 Color: 6

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 350 Color: 10
Size: 252 Color: 12

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 18
Size: 293 Color: 4
Size: 291 Color: 13

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 13
Size: 272 Color: 9
Size: 270 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 15
Size: 287 Color: 8
Size: 279 Color: 5

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 2
Size: 357 Color: 7
Size: 274 Color: 6

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 9
Size: 315 Color: 6
Size: 273 Color: 10

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 11
Size: 284 Color: 13
Size: 268 Color: 9

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 15
Size: 301 Color: 14
Size: 299 Color: 11

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 312 Color: 9
Size: 260 Color: 5

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7
Size: 313 Color: 4
Size: 311 Color: 5

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 18
Size: 329 Color: 13
Size: 322 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 13
Size: 269 Color: 3
Size: 252 Color: 15

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 14
Size: 273 Color: 14
Size: 252 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 7
Size: 295 Color: 11
Size: 286 Color: 10

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 6
Size: 319 Color: 15
Size: 306 Color: 5

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 18
Size: 273 Color: 12
Size: 270 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 345 Color: 5
Size: 272 Color: 8

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 4
Size: 305 Color: 14
Size: 257 Color: 6

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 337 Color: 5
Size: 255 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9
Size: 272 Color: 13
Size: 262 Color: 6

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 353 Color: 17
Size: 264 Color: 6

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 6
Size: 297 Color: 14
Size: 277 Color: 17

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 14
Size: 326 Color: 9
Size: 267 Color: 18

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 12
Size: 355 Color: 7
Size: 272 Color: 5

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 4
Size: 257 Color: 5
Size: 251 Color: 4

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 19
Size: 309 Color: 14
Size: 277 Color: 19

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 11
Size: 281 Color: 6
Size: 278 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 297 Color: 6
Size: 287 Color: 4

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 16
Size: 283 Color: 11
Size: 268 Color: 9

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 326 Color: 6
Size: 296 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 11
Size: 338 Color: 16
Size: 293 Color: 14

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 11
Size: 367 Color: 5
Size: 264 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 6
Size: 283 Color: 11
Size: 283 Color: 8

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 11
Size: 268 Color: 6
Size: 257 Color: 9

Total size: 167000
Total free space: 0

