Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 17
Size: 705 Color: 16
Size: 387 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 1
Size: 596 Color: 14
Size: 214 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 9
Size: 436 Color: 11
Size: 272 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 2
Size: 435 Color: 8
Size: 86 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 8
Size: 280 Color: 13
Size: 236 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1986 Color: 12
Size: 394 Color: 10
Size: 76 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2003 Color: 7
Size: 389 Color: 18
Size: 64 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 7
Size: 314 Color: 17
Size: 108 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 5
Size: 384 Color: 17
Size: 36 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 8
Size: 204 Color: 10
Size: 144 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 7
Size: 220 Color: 8
Size: 112 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 3
Size: 174 Color: 7
Size: 96 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 11
Size: 176 Color: 16
Size: 88 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 2
Size: 184 Color: 9
Size: 76 Color: 1

Bin 15: 0 of cap free
Amount of items: 4
Items: 
Size: 2202 Color: 14
Size: 242 Color: 6
Size: 8 Color: 17
Size: 4 Color: 4

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1500 Color: 6
Size: 863 Color: 0
Size: 92 Color: 12

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 1611 Color: 11
Size: 844 Color: 15

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 19
Size: 650 Color: 18
Size: 76 Color: 17

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 11
Size: 574 Color: 0
Size: 60 Color: 4

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 7
Size: 446 Color: 2
Size: 122 Color: 10

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 2
Size: 354 Color: 0
Size: 204 Color: 2

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1924 Color: 17
Size: 531 Color: 10

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1931 Color: 13
Size: 524 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1993 Color: 19
Size: 322 Color: 10
Size: 140 Color: 16

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 2106 Color: 19
Size: 349 Color: 15

Bin 26: 2 of cap free
Amount of items: 5
Items: 
Size: 1405 Color: 2
Size: 833 Color: 1
Size: 128 Color: 12
Size: 48 Color: 14
Size: 40 Color: 14

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 2
Size: 884 Color: 10

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 1
Size: 444 Color: 15
Size: 439 Color: 17

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 5
Size: 668 Color: 1
Size: 32 Color: 14

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1804 Color: 15
Size: 650 Color: 7

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 18
Size: 512 Color: 1
Size: 52 Color: 19

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 17
Size: 292 Color: 1
Size: 16 Color: 12

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 2028 Color: 8
Size: 425 Color: 12

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 1514 Color: 13
Size: 938 Color: 6

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 1536 Color: 9
Size: 916 Color: 1

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1922 Color: 3
Size: 530 Color: 18

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 9
Size: 478 Color: 12

Bin 38: 5 of cap free
Amount of items: 4
Items: 
Size: 1231 Color: 10
Size: 1020 Color: 19
Size: 132 Color: 18
Size: 68 Color: 17

Bin 39: 5 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 4
Size: 356 Color: 3
Size: 56 Color: 1

Bin 40: 6 of cap free
Amount of items: 3
Items: 
Size: 1660 Color: 6
Size: 742 Color: 0
Size: 48 Color: 19

Bin 41: 6 of cap free
Amount of items: 2
Items: 
Size: 2166 Color: 2
Size: 284 Color: 4

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 0
Size: 262 Color: 19
Size: 8 Color: 5

Bin 43: 7 of cap free
Amount of items: 7
Items: 
Size: 1230 Color: 0
Size: 541 Color: 0
Size: 232 Color: 14
Size: 226 Color: 4
Size: 112 Color: 10
Size: 68 Color: 15
Size: 40 Color: 12

Bin 44: 7 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 4
Size: 719 Color: 11
Size: 52 Color: 12

Bin 45: 8 of cap free
Amount of items: 2
Items: 
Size: 1770 Color: 7
Size: 678 Color: 16

Bin 46: 9 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 5
Size: 1021 Color: 16
Size: 92 Color: 9

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 17
Size: 751 Color: 16

Bin 48: 10 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 17
Size: 786 Color: 1
Size: 72 Color: 0

Bin 49: 11 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 0
Size: 873 Color: 9
Size: 104 Color: 18

Bin 50: 12 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 15
Size: 1142 Color: 12
Size: 64 Color: 4

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 7
Size: 1023 Color: 18

Bin 52: 12 of cap free
Amount of items: 2
Items: 
Size: 1947 Color: 18
Size: 497 Color: 10

Bin 53: 13 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 10
Size: 548 Color: 13
Size: 88 Color: 1

Bin 54: 13 of cap free
Amount of items: 2
Items: 
Size: 1822 Color: 11
Size: 621 Color: 7

Bin 55: 13 of cap free
Amount of items: 2
Items: 
Size: 1836 Color: 19
Size: 607 Color: 9

Bin 56: 15 of cap free
Amount of items: 2
Items: 
Size: 1599 Color: 12
Size: 842 Color: 16

Bin 57: 18 of cap free
Amount of items: 4
Items: 
Size: 1229 Color: 8
Size: 568 Color: 18
Size: 467 Color: 11
Size: 174 Color: 4

Bin 58: 18 of cap free
Amount of items: 2
Items: 
Size: 2074 Color: 8
Size: 364 Color: 16

Bin 59: 19 of cap free
Amount of items: 2
Items: 
Size: 1713 Color: 17
Size: 724 Color: 8

Bin 60: 21 of cap free
Amount of items: 2
Items: 
Size: 1595 Color: 14
Size: 840 Color: 13

Bin 61: 24 of cap free
Amount of items: 2
Items: 
Size: 1555 Color: 16
Size: 877 Color: 17

Bin 62: 25 of cap free
Amount of items: 2
Items: 
Size: 1409 Color: 13
Size: 1022 Color: 16

Bin 63: 30 of cap free
Amount of items: 19
Items: 
Size: 204 Color: 3
Size: 200 Color: 2
Size: 172 Color: 9
Size: 164 Color: 7
Size: 156 Color: 19
Size: 150 Color: 1
Size: 144 Color: 15
Size: 142 Color: 5
Size: 142 Color: 0
Size: 128 Color: 11
Size: 120 Color: 11
Size: 112 Color: 1
Size: 104 Color: 12
Size: 104 Color: 6
Size: 104 Color: 6
Size: 88 Color: 14
Size: 80 Color: 10
Size: 64 Color: 18
Size: 48 Color: 4

Bin 64: 30 of cap free
Amount of items: 2
Items: 
Size: 1450 Color: 14
Size: 976 Color: 0

Bin 65: 31 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 19
Size: 715 Color: 13
Size: 474 Color: 6

Bin 66: 2018 of cap free
Amount of items: 7
Items: 
Size: 88 Color: 9
Size: 86 Color: 5
Size: 84 Color: 12
Size: 48 Color: 17
Size: 48 Color: 3
Size: 44 Color: 18
Size: 40 Color: 10

Total size: 159640
Total free space: 2456

