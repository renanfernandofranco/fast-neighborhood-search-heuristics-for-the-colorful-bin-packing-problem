Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 271 Color: 4
Size: 253 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 333 Color: 1
Size: 315 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 309 Color: 2
Size: 296 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 309 Color: 4
Size: 292 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 265 Color: 3
Size: 251 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 3
Size: 267 Color: 1
Size: 254 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 2
Size: 271 Color: 3
Size: 260 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 0
Size: 255 Color: 3
Size: 253 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 319 Color: 3
Size: 301 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 2
Size: 271 Color: 2
Size: 269 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 333 Color: 2
Size: 277 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 3
Size: 269 Color: 3
Size: 259 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 4
Size: 254 Color: 2
Size: 250 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 4
Size: 296 Color: 0
Size: 270 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 4
Size: 251 Color: 1
Size: 250 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 253 Color: 3
Size: 250 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 367 Color: 1
Size: 259 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 2
Size: 276 Color: 4
Size: 257 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 261 Color: 3
Size: 253 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 358 Color: 1
Size: 256 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 330 Color: 4
Size: 291 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 3
Size: 276 Color: 2
Size: 274 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 4
Size: 345 Color: 3
Size: 291 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 2
Size: 258 Color: 2
Size: 251 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 331 Color: 2
Size: 298 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 320 Color: 2
Size: 300 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 3
Size: 309 Color: 2
Size: 275 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 321 Color: 0
Size: 272 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 337 Color: 2
Size: 273 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 309 Color: 2
Size: 265 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 350 Color: 3
Size: 281 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 330 Color: 2
Size: 260 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 305 Color: 1
Size: 253 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 3
Size: 276 Color: 2
Size: 268 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 4
Size: 275 Color: 3
Size: 257 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 341 Color: 0
Size: 254 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 309 Color: 1
Size: 273 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 326 Color: 3
Size: 256 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 340 Color: 2
Size: 279 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 3
Size: 288 Color: 1
Size: 282 Color: 0

Total size: 40000
Total free space: 0

