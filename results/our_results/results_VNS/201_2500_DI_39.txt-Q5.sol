Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 1
Size: 1022 Color: 2
Size: 200 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 0
Size: 881 Color: 2
Size: 176 Color: 3

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1410 Color: 1
Size: 882 Color: 4
Size: 112 Color: 0
Size: 60 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 2
Size: 576 Color: 2
Size: 356 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1671 Color: 2
Size: 661 Color: 4
Size: 132 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1861 Color: 4
Size: 471 Color: 1
Size: 132 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1929 Color: 1
Size: 447 Color: 1
Size: 88 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 4
Size: 516 Color: 0
Size: 40 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2044 Color: 3
Size: 268 Color: 4
Size: 152 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 0
Size: 322 Color: 3
Size: 64 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2099 Color: 2
Size: 277 Color: 0
Size: 88 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 3
Size: 290 Color: 0
Size: 56 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2150 Color: 1
Size: 246 Color: 2
Size: 68 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 2
Size: 212 Color: 4
Size: 54 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 0
Size: 220 Color: 3
Size: 40 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 4
Size: 176 Color: 3
Size: 76 Color: 2

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 1
Size: 1026 Color: 2
Size: 204 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 1
Size: 882 Color: 4
Size: 176 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 1
Size: 846 Color: 3
Size: 68 Color: 4

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 4
Size: 562 Color: 0
Size: 120 Color: 2

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1859 Color: 4
Size: 468 Color: 3
Size: 136 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 3
Size: 428 Color: 3
Size: 52 Color: 2

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 1
Size: 384 Color: 3
Size: 28 Color: 2

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 2093 Color: 1
Size: 222 Color: 2
Size: 148 Color: 0

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 0
Size: 840 Color: 3
Size: 204 Color: 1

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 2
Size: 920 Color: 3
Size: 92 Color: 2

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 2
Size: 546 Color: 1
Size: 176 Color: 0

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 1
Size: 581 Color: 2
Size: 104 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1796 Color: 1
Size: 622 Color: 0
Size: 44 Color: 4

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1850 Color: 3
Size: 564 Color: 2
Size: 48 Color: 1

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1958 Color: 4
Size: 504 Color: 1

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 1
Size: 292 Color: 1
Size: 172 Color: 2

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 2
Size: 204 Color: 4
Size: 88 Color: 0

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1930 Color: 0
Size: 443 Color: 3
Size: 88 Color: 2

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1985 Color: 3
Size: 388 Color: 1
Size: 88 Color: 2

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 2116 Color: 1
Size: 345 Color: 3

Bin 37: 4 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 3
Size: 838 Color: 4
Size: 64 Color: 4

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 2
Size: 758 Color: 1
Size: 56 Color: 4

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 2
Size: 462 Color: 3
Size: 100 Color: 4

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 4
Size: 763 Color: 1

Bin 41: 5 of cap free
Amount of items: 4
Items: 
Size: 2042 Color: 4
Size: 401 Color: 3
Size: 8 Color: 3
Size: 8 Color: 2

Bin 42: 5 of cap free
Amount of items: 3
Items: 
Size: 2084 Color: 0
Size: 367 Color: 1
Size: 8 Color: 1

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 2148 Color: 3
Size: 311 Color: 1

Bin 44: 7 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 1
Size: 505 Color: 2
Size: 354 Color: 4

Bin 45: 7 of cap free
Amount of items: 3
Items: 
Size: 2043 Color: 1
Size: 390 Color: 2
Size: 24 Color: 1

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 2133 Color: 3
Size: 324 Color: 4

Bin 47: 9 of cap free
Amount of items: 3
Items: 
Size: 2025 Color: 1
Size: 262 Color: 4
Size: 168 Color: 4

Bin 48: 11 of cap free
Amount of items: 2
Items: 
Size: 1790 Color: 3
Size: 663 Color: 0

Bin 49: 11 of cap free
Amount of items: 2
Items: 
Size: 2004 Color: 3
Size: 449 Color: 2

Bin 50: 12 of cap free
Amount of items: 4
Items: 
Size: 1234 Color: 1
Size: 874 Color: 1
Size: 272 Color: 3
Size: 72 Color: 4

Bin 51: 13 of cap free
Amount of items: 2
Items: 
Size: 1769 Color: 0
Size: 682 Color: 4

Bin 52: 14 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 0
Size: 628 Color: 4
Size: 100 Color: 2

Bin 53: 21 of cap free
Amount of items: 2
Items: 
Size: 1551 Color: 4
Size: 892 Color: 2

Bin 54: 21 of cap free
Amount of items: 2
Items: 
Size: 1997 Color: 0
Size: 446 Color: 4

Bin 55: 24 of cap free
Amount of items: 15
Items: 
Size: 573 Color: 0
Size: 351 Color: 1
Size: 272 Color: 1
Size: 152 Color: 4
Size: 152 Color: 4
Size: 136 Color: 2
Size: 114 Color: 4
Size: 112 Color: 4
Size: 96 Color: 3
Size: 96 Color: 0
Size: 88 Color: 0
Size: 80 Color: 3
Size: 80 Color: 0
Size: 70 Color: 3
Size: 68 Color: 3

Bin 56: 30 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 3
Size: 1028 Color: 2

Bin 57: 32 of cap free
Amount of items: 3
Items: 
Size: 1235 Color: 2
Size: 1025 Color: 0
Size: 172 Color: 1

Bin 58: 32 of cap free
Amount of items: 3
Items: 
Size: 1927 Color: 3
Size: 305 Color: 1
Size: 200 Color: 2

Bin 59: 33 of cap free
Amount of items: 2
Items: 
Size: 1860 Color: 2
Size: 571 Color: 4

Bin 60: 34 of cap free
Amount of items: 2
Items: 
Size: 1669 Color: 0
Size: 761 Color: 3

Bin 61: 34 of cap free
Amount of items: 2
Items: 
Size: 1956 Color: 4
Size: 474 Color: 0

Bin 62: 37 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 2
Size: 883 Color: 4

Bin 63: 40 of cap free
Amount of items: 2
Items: 
Size: 1644 Color: 0
Size: 780 Color: 2

Bin 64: 41 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 4
Size: 684 Color: 3
Size: 503 Color: 2

Bin 65: 41 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 1
Size: 1027 Color: 2

Bin 66: 1886 of cap free
Amount of items: 9
Items: 
Size: 114 Color: 1
Size: 72 Color: 4
Size: 72 Color: 0
Size: 64 Color: 4
Size: 60 Color: 3
Size: 56 Color: 3
Size: 48 Color: 4
Size: 48 Color: 0
Size: 44 Color: 3

Total size: 160160
Total free space: 2464

