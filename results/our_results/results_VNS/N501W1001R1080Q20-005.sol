Capicity Bin: 1001
Lower Bound: 223

Bins used: 223
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 1
Size: 281 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 322 Color: 2
Size: 284 Color: 3

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 2
Size: 482 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9
Size: 395 Color: 2
Size: 112 Color: 16

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 10
Size: 302 Color: 11

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 11
Size: 203 Color: 1

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 7
Size: 304 Color: 5

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 14
Size: 487 Color: 6

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 1
Size: 474 Color: 2

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 10
Size: 338 Color: 3

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 18
Size: 224 Color: 3

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 219 Color: 19

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 9
Size: 435 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 4
Size: 346 Color: 1
Size: 146 Color: 0

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 15
Size: 348 Color: 6

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 11
Size: 469 Color: 18

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 16
Size: 365 Color: 6

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 8
Size: 372 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 565 Color: 19
Size: 278 Color: 14
Size: 158 Color: 19

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 9
Size: 390 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 662 Color: 9
Size: 189 Color: 6
Size: 150 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 11
Size: 388 Color: 6
Size: 220 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 754 Color: 4
Size: 135 Color: 3
Size: 112 Color: 9

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 19
Size: 374 Color: 11

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 13
Size: 414 Color: 18

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 601 Color: 16
Size: 283 Color: 19
Size: 117 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 661 Color: 0
Size: 180 Color: 1
Size: 160 Color: 16

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 14
Size: 248 Color: 4

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 3
Size: 232 Color: 8

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 18
Size: 467 Color: 17

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 16
Size: 499 Color: 0

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 10

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 12
Size: 245 Color: 17

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 5
Size: 228 Color: 19

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 12
Size: 297 Color: 9

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 3
Size: 478 Color: 13

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 16
Size: 389 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 3
Size: 190 Color: 18
Size: 134 Color: 0

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 7
Size: 451 Color: 6

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 3
Size: 361 Color: 1

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 5
Size: 320 Color: 9

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 12
Size: 278 Color: 4

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 3
Size: 478 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 18
Size: 174 Color: 19
Size: 163 Color: 12

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 14
Size: 298 Color: 5

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 8
Size: 387 Color: 5

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 3
Size: 310 Color: 8

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 12
Size: 476 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 3
Size: 187 Color: 10
Size: 181 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 16
Size: 312 Color: 1
Size: 167 Color: 5

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 4
Size: 249 Color: 15

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 615 Color: 16
Size: 197 Color: 1
Size: 189 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 14
Size: 308 Color: 0
Size: 166 Color: 6

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 15
Size: 478 Color: 18

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 19
Size: 408 Color: 1

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 0
Size: 316 Color: 7

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 2
Size: 173 Color: 17
Size: 171 Color: 13

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 586 Color: 9
Size: 288 Color: 16
Size: 127 Color: 9

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 7
Size: 439 Color: 15

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 10
Size: 481 Color: 4

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 9
Size: 489 Color: 14

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 7
Size: 362 Color: 19

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 16
Size: 306 Color: 8

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 14
Size: 122 Color: 16
Size: 100 Color: 2

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 18
Size: 486 Color: 19

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 741 Color: 18
Size: 148 Color: 19
Size: 112 Color: 14

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 7
Size: 477 Color: 1

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 17
Size: 374 Color: 14

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 14
Size: 239 Color: 1

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 1
Size: 236 Color: 13

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 19
Size: 401 Color: 10
Size: 163 Color: 3

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 13
Size: 344 Color: 5

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 509 Color: 1
Size: 276 Color: 19
Size: 216 Color: 6

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 10
Size: 321 Color: 0
Size: 315 Color: 14

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 6
Size: 316 Color: 15

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 19
Size: 378 Color: 13

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 14
Size: 301 Color: 19

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 6
Size: 403 Color: 13

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 4
Size: 406 Color: 6

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 10
Size: 161 Color: 6
Size: 106 Color: 6

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 11
Size: 380 Color: 17

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 11
Size: 399 Color: 8

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 10
Size: 221 Color: 2

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 8
Size: 233 Color: 7

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 14
Size: 381 Color: 7

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 395 Color: 7

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 10
Size: 355 Color: 5

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 15
Size: 245 Color: 13

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 10
Size: 164 Color: 15
Size: 118 Color: 15

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 0

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 3
Size: 396 Color: 17

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 533 Color: 19
Size: 301 Color: 15
Size: 167 Color: 14

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 12
Size: 313 Color: 5

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 9
Size: 334 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 756 Color: 9
Size: 134 Color: 0
Size: 111 Color: 19

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 583 Color: 2
Size: 228 Color: 1
Size: 190 Color: 17

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 18
Size: 379 Color: 17

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 11
Size: 290 Color: 10

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 510 Color: 12
Size: 271 Color: 14
Size: 220 Color: 5

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 6
Size: 390 Color: 19

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 750 Color: 13
Size: 149 Color: 5
Size: 102 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 660 Color: 5
Size: 183 Color: 17
Size: 158 Color: 11

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 8
Size: 467 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 0
Size: 363 Color: 19
Size: 125 Color: 8

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 2
Size: 215 Color: 1

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 1
Size: 489 Color: 10

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 750 Color: 4
Size: 131 Color: 12
Size: 120 Color: 16

Bin 108: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 10
Size: 490 Color: 2

Bin 109: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 18
Size: 296 Color: 8

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 19
Size: 291 Color: 1
Size: 286 Color: 18

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 0
Size: 162 Color: 5
Size: 105 Color: 3

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 5
Size: 201 Color: 3

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 17
Size: 254 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 3
Size: 170 Color: 7
Size: 111 Color: 2

Bin 115: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 16
Size: 401 Color: 18

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 0
Size: 261 Color: 9
Size: 172 Color: 3

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 18
Size: 392 Color: 15
Size: 217 Color: 3

Bin 118: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 2
Size: 493 Color: 12

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 503 Color: 15
Size: 270 Color: 8
Size: 228 Color: 17

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 15
Size: 231 Color: 6

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 7
Size: 234 Color: 13
Size: 127 Color: 16

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 13
Size: 370 Color: 16

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 530 Color: 3
Size: 284 Color: 3
Size: 187 Color: 11

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 742 Color: 5
Size: 144 Color: 8
Size: 115 Color: 3

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 3
Size: 310 Color: 15

Bin 126: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 0
Size: 345 Color: 2

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 18
Size: 493 Color: 11

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 515 Color: 14
Size: 314 Color: 1
Size: 172 Color: 13

Bin 129: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 15
Size: 461 Color: 16

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 510 Color: 9
Size: 271 Color: 12
Size: 220 Color: 14

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 517 Color: 16
Size: 315 Color: 12
Size: 169 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 7
Size: 255 Color: 1
Size: 123 Color: 8

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 17
Size: 290 Color: 3
Size: 143 Color: 14

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 13
Size: 313 Color: 14

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 1
Size: 166 Color: 4
Size: 131 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 14
Size: 197 Color: 17
Size: 108 Color: 15

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 12
Size: 470 Color: 6

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 16
Size: 206 Color: 15
Size: 101 Color: 17

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 19
Size: 142 Color: 5
Size: 107 Color: 12

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 346 Color: 19
Size: 290 Color: 13

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 13
Size: 314 Color: 17
Size: 119 Color: 13

Bin 142: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 13
Size: 261 Color: 16

Bin 143: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 7
Size: 339 Color: 5

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 11
Size: 363 Color: 7
Size: 176 Color: 2

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 520 Color: 8
Size: 285 Color: 1
Size: 196 Color: 15

Bin 146: 1 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 14
Size: 404 Color: 1

Bin 147: 1 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 8
Size: 302 Color: 1

Bin 148: 1 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 9
Size: 299 Color: 5

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 630 Color: 16
Size: 187 Color: 3
Size: 183 Color: 15

Bin 150: 1 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 19
Size: 437 Color: 16

Bin 151: 1 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 13
Size: 265 Color: 7

Bin 152: 1 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 1
Size: 223 Color: 19

Bin 153: 1 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 1
Size: 401 Color: 6

Bin 154: 1 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 11
Size: 268 Color: 19

Bin 155: 1 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 19
Size: 424 Color: 17

Bin 156: 1 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 10
Size: 428 Color: 5

Bin 157: 1 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 2
Size: 396 Color: 17

Bin 158: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 2
Size: 483 Color: 0

Bin 159: 1 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 287 Color: 5

Bin 160: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 367 Color: 0

Bin 161: 1 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 12
Size: 294 Color: 8

Bin 162: 1 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 16
Size: 342 Color: 8

Bin 163: 1 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 6
Size: 465 Color: 1

Bin 164: 1 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 12
Size: 475 Color: 3

Bin 165: 1 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 8
Size: 322 Color: 9

Bin 166: 1 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 6
Size: 326 Color: 17

Bin 167: 2 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 10
Size: 431 Color: 1

Bin 168: 2 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 18
Size: 242 Color: 5

Bin 169: 2 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 13
Size: 358 Color: 3

Bin 170: 2 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 17
Size: 199 Color: 10

Bin 171: 2 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 14
Size: 398 Color: 3

Bin 172: 2 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 8
Size: 258 Color: 10

Bin 173: 2 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 5
Size: 382 Color: 16

Bin 174: 2 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 11
Size: 408 Color: 16

Bin 175: 2 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 18
Size: 421 Color: 19

Bin 176: 2 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 15
Size: 255 Color: 4

Bin 177: 2 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 17
Size: 317 Color: 11

Bin 178: 2 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 12
Size: 465 Color: 10

Bin 179: 2 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 16
Size: 458 Color: 0

Bin 180: 2 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 12
Size: 489 Color: 18

Bin 181: 2 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 18
Size: 211 Color: 3

Bin 182: 3 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 14
Size: 279 Color: 3

Bin 183: 3 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 19
Size: 242 Color: 18

Bin 184: 3 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 10
Size: 242 Color: 17

Bin 185: 3 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 1
Size: 461 Color: 8

Bin 186: 3 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 11
Size: 447 Color: 8

Bin 187: 3 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 15
Size: 447 Color: 13

Bin 188: 3 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 16
Size: 301 Color: 14

Bin 189: 3 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 272 Color: 6

Bin 190: 3 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 9
Size: 453 Color: 15

Bin 191: 3 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 3
Size: 353 Color: 5

Bin 192: 4 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 10
Size: 278 Color: 13

Bin 193: 4 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 7
Size: 419 Color: 16

Bin 194: 4 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 8
Size: 381 Color: 2

Bin 195: 4 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 253 Color: 7

Bin 196: 4 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 18
Size: 472 Color: 15

Bin 197: 4 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 3
Size: 346 Color: 9

Bin 198: 4 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 13
Size: 373 Color: 16

Bin 199: 5 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 9
Size: 320 Color: 15

Bin 200: 5 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 331 Color: 19

Bin 201: 5 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 3
Size: 206 Color: 8

Bin 202: 6 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 10
Size: 194 Color: 9

Bin 203: 7 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 13
Size: 443 Color: 1

Bin 204: 7 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 9
Size: 443 Color: 18

Bin 205: 7 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 13
Size: 330 Color: 5

Bin 206: 8 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 16
Size: 193 Color: 5

Bin 207: 8 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 10
Size: 206 Color: 7

Bin 208: 8 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 17
Size: 407 Color: 1

Bin 209: 8 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 0
Size: 232 Color: 15

Bin 210: 12 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 17
Size: 252 Color: 1

Bin 211: 12 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 14
Size: 252 Color: 6

Bin 212: 13 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 7
Size: 365 Color: 19

Bin 213: 13 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 2
Size: 202 Color: 10

Bin 214: 14 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 18
Size: 217 Color: 5

Bin 215: 14 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 1
Size: 398 Color: 0

Bin 216: 16 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 7
Size: 452 Color: 10

Bin 217: 18 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 14
Size: 452 Color: 6

Bin 218: 23 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 3
Size: 270 Color: 11

Bin 219: 23 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 15
Size: 226 Color: 0

Bin 220: 24 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 4
Size: 393 Color: 14

Bin 221: 53 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 13
Size: 443 Color: 9

Bin 222: 90 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 15
Size: 389 Color: 19

Bin 223: 114 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 5
Size: 137 Color: 9

Total size: 222601
Total free space: 622

