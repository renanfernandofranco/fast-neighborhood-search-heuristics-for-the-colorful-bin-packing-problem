Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3337
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 452552 Color: 1
Size: 275073 Color: 0
Size: 272376 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 450085 Color: 0
Size: 275927 Color: 1
Size: 273989 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 472232 Color: 1
Size: 275250 Color: 1
Size: 252519 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 454885 Color: 1
Size: 281004 Color: 0
Size: 264112 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 374855 Color: 1
Size: 327652 Color: 1
Size: 297494 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 406818 Color: 0
Size: 338918 Color: 1
Size: 254265 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 382806 Color: 0
Size: 312417 Color: 0
Size: 304778 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 457809 Color: 1
Size: 283035 Color: 1
Size: 259157 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 436031 Color: 1
Size: 310527 Color: 1
Size: 253443 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 479556 Color: 1
Size: 266495 Color: 1
Size: 253950 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 413936 Color: 0
Size: 328880 Color: 0
Size: 257185 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 394085 Color: 1
Size: 303021 Color: 1
Size: 302895 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 498968 Color: 0
Size: 250916 Color: 0
Size: 250117 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 497620 Color: 0
Size: 251947 Color: 1
Size: 250434 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 430434 Color: 1
Size: 312183 Color: 1
Size: 257384 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 402107 Color: 1
Size: 320593 Color: 0
Size: 277301 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 418151 Color: 1
Size: 327092 Color: 1
Size: 254758 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 471420 Color: 1
Size: 274541 Color: 0
Size: 254040 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 425483 Color: 0
Size: 319850 Color: 1
Size: 254668 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 451997 Color: 0
Size: 279967 Color: 1
Size: 268037 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 376568 Color: 0
Size: 361791 Color: 1
Size: 261642 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 495199 Color: 0
Size: 253522 Color: 1
Size: 251280 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 436390 Color: 1
Size: 312750 Color: 1
Size: 250861 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 434905 Color: 1
Size: 289871 Color: 1
Size: 275225 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 487539 Color: 1
Size: 261339 Color: 1
Size: 251123 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 366924 Color: 1
Size: 328864 Color: 1
Size: 304213 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 377733 Color: 1
Size: 345021 Color: 0
Size: 277247 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 488320 Color: 1
Size: 259850 Color: 1
Size: 251831 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 386977 Color: 1
Size: 350240 Color: 1
Size: 262784 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 404724 Color: 0
Size: 322691 Color: 1
Size: 272586 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 406399 Color: 0
Size: 340265 Color: 0
Size: 253337 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 393611 Color: 1
Size: 343980 Color: 0
Size: 262410 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 434046 Color: 0
Size: 294143 Color: 1
Size: 271812 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 431157 Color: 1
Size: 295383 Color: 0
Size: 273461 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 384233 Color: 1
Size: 316395 Color: 0
Size: 299373 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 393029 Color: 0
Size: 351642 Color: 1
Size: 255330 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 463084 Color: 0
Size: 277907 Color: 0
Size: 259010 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 365386 Color: 0
Size: 334565 Color: 0
Size: 300050 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 457636 Color: 1
Size: 271663 Color: 0
Size: 270702 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 395030 Color: 1
Size: 316597 Color: 1
Size: 288374 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 419377 Color: 0
Size: 297327 Color: 1
Size: 283297 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 386837 Color: 0
Size: 348318 Color: 0
Size: 264846 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 362898 Color: 1
Size: 330707 Color: 0
Size: 306396 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 430245 Color: 1
Size: 319170 Color: 1
Size: 250586 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 466783 Color: 0
Size: 274483 Color: 1
Size: 258735 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 369985 Color: 0
Size: 349474 Color: 0
Size: 280542 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 378366 Color: 0
Size: 316741 Color: 0
Size: 304894 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 473203 Color: 1
Size: 274560 Color: 1
Size: 252238 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 428875 Color: 1
Size: 311758 Color: 0
Size: 259368 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 482254 Color: 1
Size: 259424 Color: 0
Size: 258323 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 440287 Color: 1
Size: 305356 Color: 1
Size: 254358 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 455323 Color: 1
Size: 273815 Color: 0
Size: 270863 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 411472 Color: 0
Size: 332964 Color: 1
Size: 255565 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 422505 Color: 1
Size: 298615 Color: 1
Size: 278881 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 353192 Color: 1
Size: 352453 Color: 0
Size: 294356 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 383380 Color: 0
Size: 334608 Color: 0
Size: 282013 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 404611 Color: 0
Size: 323173 Color: 0
Size: 272217 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 399978 Color: 1
Size: 304384 Color: 0
Size: 295639 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 369878 Color: 0
Size: 335820 Color: 1
Size: 294303 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 358546 Color: 1
Size: 353402 Color: 1
Size: 288053 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 456936 Color: 0
Size: 280996 Color: 1
Size: 262069 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 411832 Color: 0
Size: 294531 Color: 0
Size: 293638 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 384146 Color: 0
Size: 318587 Color: 0
Size: 297268 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 459988 Color: 0
Size: 289024 Color: 0
Size: 250989 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 434312 Color: 0
Size: 297937 Color: 1
Size: 267752 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 447307 Color: 0
Size: 291015 Color: 1
Size: 261679 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 427538 Color: 0
Size: 294701 Color: 0
Size: 277762 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 413850 Color: 0
Size: 329985 Color: 1
Size: 256166 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 458065 Color: 0
Size: 287220 Color: 1
Size: 254716 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 489650 Color: 1
Size: 256230 Color: 1
Size: 254121 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 349087 Color: 1
Size: 346818 Color: 0
Size: 304096 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 416815 Color: 0
Size: 326381 Color: 0
Size: 256805 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 370281 Color: 0
Size: 316408 Color: 1
Size: 313312 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 372256 Color: 0
Size: 369462 Color: 1
Size: 258283 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 488421 Color: 1
Size: 256748 Color: 0
Size: 254832 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 453610 Color: 1
Size: 291347 Color: 0
Size: 255044 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 392376 Color: 1
Size: 342686 Color: 0
Size: 264939 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 394399 Color: 0
Size: 320745 Color: 0
Size: 284857 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 404729 Color: 0
Size: 343824 Color: 1
Size: 251448 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 374884 Color: 0
Size: 356814 Color: 0
Size: 268303 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 421993 Color: 1
Size: 301824 Color: 0
Size: 276184 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 405565 Color: 1
Size: 310605 Color: 0
Size: 283831 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 473326 Color: 0
Size: 268858 Color: 0
Size: 257817 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 363799 Color: 1
Size: 358396 Color: 1
Size: 277806 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 418980 Color: 0
Size: 330054 Color: 1
Size: 250967 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 461085 Color: 1
Size: 277799 Color: 0
Size: 261117 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 492432 Color: 0
Size: 256526 Color: 1
Size: 251043 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 389604 Color: 1
Size: 324130 Color: 0
Size: 286267 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 491170 Color: 0
Size: 257640 Color: 1
Size: 251191 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 399160 Color: 0
Size: 327859 Color: 0
Size: 272982 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 437718 Color: 0
Size: 284316 Color: 1
Size: 277967 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 430853 Color: 1
Size: 295622 Color: 0
Size: 273526 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 381308 Color: 1
Size: 309674 Color: 1
Size: 309019 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 401910 Color: 0
Size: 325650 Color: 1
Size: 272441 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 427981 Color: 1
Size: 292453 Color: 0
Size: 279567 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 496842 Color: 1
Size: 251939 Color: 1
Size: 251220 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 454970 Color: 1
Size: 274390 Color: 1
Size: 270641 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 377460 Color: 0
Size: 358361 Color: 0
Size: 264180 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 454284 Color: 1
Size: 275108 Color: 0
Size: 270609 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 430044 Color: 1
Size: 303110 Color: 0
Size: 266847 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 398727 Color: 0
Size: 329413 Color: 0
Size: 271861 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 387061 Color: 0
Size: 341907 Color: 1
Size: 271033 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 418963 Color: 1
Size: 326352 Color: 1
Size: 254686 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 401734 Color: 0
Size: 301784 Color: 1
Size: 296483 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 482747 Color: 1
Size: 267130 Color: 1
Size: 250124 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 377300 Color: 0
Size: 326445 Color: 0
Size: 296256 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 464549 Color: 1
Size: 280128 Color: 0
Size: 255324 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 418306 Color: 1
Size: 304639 Color: 0
Size: 277056 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 397149 Color: 0
Size: 338983 Color: 1
Size: 263869 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 410382 Color: 1
Size: 332437 Color: 0
Size: 257182 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 397108 Color: 1
Size: 346908 Color: 0
Size: 255985 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 443815 Color: 1
Size: 297734 Color: 1
Size: 258452 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 435029 Color: 0
Size: 302871 Color: 1
Size: 262101 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 448159 Color: 1
Size: 299308 Color: 1
Size: 252534 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 468285 Color: 0
Size: 281211 Color: 1
Size: 250505 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 434596 Color: 1
Size: 285059 Color: 1
Size: 280346 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 463200 Color: 1
Size: 275128 Color: 0
Size: 261673 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 385416 Color: 1
Size: 357707 Color: 0
Size: 256878 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 489805 Color: 1
Size: 259666 Color: 0
Size: 250530 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 390741 Color: 0
Size: 334976 Color: 1
Size: 274284 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 416255 Color: 0
Size: 322397 Color: 1
Size: 261349 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 384633 Color: 0
Size: 361644 Color: 0
Size: 253724 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 409004 Color: 1
Size: 314516 Color: 0
Size: 276481 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 410903 Color: 1
Size: 328071 Color: 0
Size: 261027 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 395769 Color: 1
Size: 350710 Color: 0
Size: 253522 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 427127 Color: 0
Size: 302952 Color: 1
Size: 269922 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 495154 Color: 1
Size: 254276 Color: 1
Size: 250571 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 404470 Color: 0
Size: 345160 Color: 0
Size: 250371 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 388230 Color: 0
Size: 308737 Color: 0
Size: 303034 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 419837 Color: 1
Size: 298017 Color: 0
Size: 282147 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 456792 Color: 0
Size: 289757 Color: 1
Size: 253452 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 353012 Color: 1
Size: 325383 Color: 0
Size: 321606 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 492590 Color: 0
Size: 254291 Color: 0
Size: 253120 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 490831 Color: 1
Size: 255575 Color: 0
Size: 253595 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 385446 Color: 0
Size: 338545 Color: 1
Size: 276010 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 404757 Color: 1
Size: 297748 Color: 0
Size: 297496 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 484821 Color: 1
Size: 263936 Color: 1
Size: 251244 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 403281 Color: 1
Size: 337639 Color: 1
Size: 259081 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 474543 Color: 0
Size: 271198 Color: 1
Size: 254260 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 376338 Color: 0
Size: 345873 Color: 1
Size: 277790 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 464581 Color: 0
Size: 270342 Color: 1
Size: 265078 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 413726 Color: 1
Size: 314210 Color: 1
Size: 272065 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 393035 Color: 1
Size: 352141 Color: 0
Size: 254825 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 377164 Color: 0
Size: 372755 Color: 1
Size: 250082 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 385979 Color: 1
Size: 361470 Color: 0
Size: 252552 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 450684 Color: 0
Size: 281252 Color: 1
Size: 268065 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 394436 Color: 0
Size: 346583 Color: 1
Size: 258982 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 393611 Color: 0
Size: 309329 Color: 1
Size: 297061 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 376323 Color: 1
Size: 365742 Color: 0
Size: 257936 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 420008 Color: 0
Size: 320813 Color: 1
Size: 259180 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 397161 Color: 0
Size: 304462 Color: 1
Size: 298378 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 417113 Color: 1
Size: 318810 Color: 0
Size: 264078 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 440350 Color: 0
Size: 305766 Color: 1
Size: 253885 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 389464 Color: 0
Size: 332088 Color: 0
Size: 278449 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 364143 Color: 1
Size: 353442 Color: 0
Size: 282416 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 402843 Color: 0
Size: 301308 Color: 1
Size: 295850 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 438007 Color: 0
Size: 306191 Color: 1
Size: 255803 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 412979 Color: 1
Size: 326933 Color: 0
Size: 260089 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 379331 Color: 0
Size: 337272 Color: 0
Size: 283398 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 482120 Color: 0
Size: 261506 Color: 1
Size: 256375 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 364361 Color: 1
Size: 343895 Color: 0
Size: 291745 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 472480 Color: 0
Size: 277320 Color: 0
Size: 250201 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 350356 Color: 1
Size: 343361 Color: 0
Size: 306284 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 351669 Color: 0
Size: 350750 Color: 0
Size: 297582 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 490668 Color: 1
Size: 255610 Color: 0
Size: 253723 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 426533 Color: 0
Size: 288339 Color: 0
Size: 285129 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 439783 Color: 0
Size: 280605 Color: 0
Size: 279613 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 401519 Color: 1
Size: 324311 Color: 0
Size: 274171 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 381332 Color: 1
Size: 319067 Color: 1
Size: 299602 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 446431 Color: 0
Size: 290446 Color: 0
Size: 263124 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 430335 Color: 0
Size: 317946 Color: 1
Size: 251720 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 436179 Color: 1
Size: 305162 Color: 0
Size: 258660 Color: 1

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 412797 Color: 1
Size: 306478 Color: 0
Size: 280726 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 496796 Color: 1
Size: 253127 Color: 0
Size: 250078 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 418227 Color: 1
Size: 330918 Color: 0
Size: 250856 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 367160 Color: 0
Size: 348296 Color: 0
Size: 284545 Color: 1

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 372835 Color: 1
Size: 346454 Color: 0
Size: 280712 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 410978 Color: 1
Size: 324846 Color: 0
Size: 264177 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 378246 Color: 0
Size: 326979 Color: 1
Size: 294776 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 374872 Color: 0
Size: 312620 Color: 0
Size: 312509 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 479598 Color: 0
Size: 261468 Color: 0
Size: 258935 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 457842 Color: 1
Size: 285545 Color: 1
Size: 256614 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 404405 Color: 1
Size: 342120 Color: 0
Size: 253476 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 497076 Color: 0
Size: 251634 Color: 1
Size: 251291 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 467985 Color: 0
Size: 274397 Color: 1
Size: 257619 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 418069 Color: 0
Size: 328372 Color: 1
Size: 253560 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 373422 Color: 1
Size: 368224 Color: 0
Size: 258355 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 361521 Color: 0
Size: 329448 Color: 0
Size: 309032 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 436593 Color: 1
Size: 282575 Color: 1
Size: 280833 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 451209 Color: 0
Size: 281592 Color: 0
Size: 267200 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 380276 Color: 0
Size: 310610 Color: 0
Size: 309115 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 388732 Color: 1
Size: 322930 Color: 0
Size: 288339 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 400040 Color: 1
Size: 312273 Color: 1
Size: 287688 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 346864 Color: 0
Size: 335524 Color: 1
Size: 317613 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 373188 Color: 1
Size: 345556 Color: 1
Size: 281257 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 385247 Color: 0
Size: 337445 Color: 1
Size: 277309 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 414056 Color: 0
Size: 318239 Color: 1
Size: 267706 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 488995 Color: 1
Size: 258700 Color: 0
Size: 252306 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 452784 Color: 0
Size: 274972 Color: 0
Size: 272245 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 368857 Color: 0
Size: 326441 Color: 0
Size: 304703 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 359197 Color: 1
Size: 323508 Color: 0
Size: 317296 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 462594 Color: 1
Size: 284950 Color: 1
Size: 252457 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 344889 Color: 1
Size: 332385 Color: 1
Size: 322727 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 340054 Color: 1
Size: 330847 Color: 0
Size: 329100 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 493660 Color: 1
Size: 254472 Color: 0
Size: 251869 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 487936 Color: 1
Size: 260037 Color: 0
Size: 252028 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 396935 Color: 0
Size: 324082 Color: 1
Size: 278984 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 402428 Color: 0
Size: 326222 Color: 1
Size: 271351 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 453722 Color: 1
Size: 274574 Color: 1
Size: 271705 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 493786 Color: 1
Size: 253229 Color: 1
Size: 252986 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 441775 Color: 0
Size: 299920 Color: 1
Size: 258306 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 398828 Color: 1
Size: 333890 Color: 1
Size: 267283 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 412995 Color: 1
Size: 298568 Color: 0
Size: 288438 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 430850 Color: 0
Size: 316987 Color: 1
Size: 252164 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 394550 Color: 1
Size: 333897 Color: 0
Size: 271554 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 376180 Color: 0
Size: 319239 Color: 1
Size: 304582 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 386997 Color: 0
Size: 352012 Color: 1
Size: 260992 Color: 1

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 384058 Color: 1
Size: 353807 Color: 0
Size: 262136 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 459480 Color: 1
Size: 289795 Color: 1
Size: 250726 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 368505 Color: 0
Size: 354466 Color: 1
Size: 277030 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 423423 Color: 0
Size: 307047 Color: 1
Size: 269531 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 415798 Color: 0
Size: 297645 Color: 1
Size: 286558 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 448374 Color: 1
Size: 289216 Color: 0
Size: 262411 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 393000 Color: 1
Size: 339865 Color: 0
Size: 267136 Color: 1

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 396464 Color: 1
Size: 342255 Color: 0
Size: 261282 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 403724 Color: 0
Size: 341682 Color: 1
Size: 254595 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 446523 Color: 1
Size: 277648 Color: 0
Size: 275830 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 495054 Color: 0
Size: 253227 Color: 1
Size: 251720 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 452618 Color: 1
Size: 280079 Color: 0
Size: 267304 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 414771 Color: 1
Size: 314581 Color: 0
Size: 270649 Color: 1

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 423055 Color: 0
Size: 315723 Color: 1
Size: 261223 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 413228 Color: 0
Size: 311776 Color: 0
Size: 274997 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 417986 Color: 0
Size: 330718 Color: 1
Size: 251297 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 385700 Color: 0
Size: 316422 Color: 0
Size: 297879 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 383984 Color: 1
Size: 352499 Color: 1
Size: 263518 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 390795 Color: 1
Size: 321803 Color: 0
Size: 287403 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 423860 Color: 1
Size: 319225 Color: 1
Size: 256916 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 396553 Color: 0
Size: 351894 Color: 1
Size: 251554 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 486222 Color: 1
Size: 256945 Color: 0
Size: 256834 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 459108 Color: 0
Size: 276500 Color: 1
Size: 264393 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 439887 Color: 0
Size: 286231 Color: 1
Size: 273883 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 386718 Color: 0
Size: 344755 Color: 1
Size: 268528 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 469383 Color: 1
Size: 268730 Color: 0
Size: 261888 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 407639 Color: 1
Size: 299384 Color: 0
Size: 292978 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 455948 Color: 0
Size: 289486 Color: 1
Size: 254567 Color: 1

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 363067 Color: 1
Size: 320602 Color: 0
Size: 316332 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 451955 Color: 1
Size: 279610 Color: 0
Size: 268436 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 363443 Color: 0
Size: 318865 Color: 1
Size: 317693 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 404984 Color: 0
Size: 321294 Color: 1
Size: 273723 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 492947 Color: 0
Size: 257041 Color: 1
Size: 250013 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 493867 Color: 1
Size: 255136 Color: 0
Size: 250998 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 349060 Color: 1
Size: 341198 Color: 0
Size: 309743 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 416644 Color: 1
Size: 295009 Color: 0
Size: 288348 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 417470 Color: 0
Size: 292056 Color: 0
Size: 290475 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 409074 Color: 0
Size: 300862 Color: 0
Size: 290065 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 387712 Color: 1
Size: 322347 Color: 1
Size: 289942 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 444184 Color: 1
Size: 280399 Color: 0
Size: 275418 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 377332 Color: 0
Size: 372027 Color: 1
Size: 250642 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 459421 Color: 0
Size: 282512 Color: 1
Size: 258068 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 373936 Color: 0
Size: 372650 Color: 1
Size: 253415 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 439300 Color: 1
Size: 305789 Color: 1
Size: 254912 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 403623 Color: 0
Size: 312034 Color: 0
Size: 284344 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 452951 Color: 1
Size: 279340 Color: 0
Size: 267710 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 383135 Color: 0
Size: 341563 Color: 1
Size: 275303 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 382644 Color: 1
Size: 331986 Color: 1
Size: 285371 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 396990 Color: 1
Size: 311572 Color: 1
Size: 291439 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 391938 Color: 1
Size: 355644 Color: 0
Size: 252419 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 436487 Color: 1
Size: 309439 Color: 1
Size: 254075 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 395206 Color: 0
Size: 312775 Color: 1
Size: 292020 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 406728 Color: 0
Size: 311874 Color: 1
Size: 281399 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 403836 Color: 1
Size: 300469 Color: 0
Size: 295696 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 372814 Color: 1
Size: 316435 Color: 1
Size: 310752 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 358768 Color: 0
Size: 326800 Color: 1
Size: 314433 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 377435 Color: 0
Size: 330562 Color: 0
Size: 292004 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 336685 Color: 0
Size: 334146 Color: 1
Size: 329170 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 385410 Color: 0
Size: 345993 Color: 1
Size: 268598 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 386163 Color: 1
Size: 346421 Color: 1
Size: 267417 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 478090 Color: 1
Size: 261289 Color: 0
Size: 260622 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 445099 Color: 0
Size: 283285 Color: 1
Size: 271617 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 443782 Color: 1
Size: 278178 Color: 0
Size: 278041 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 414665 Color: 1
Size: 302187 Color: 0
Size: 283149 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 479407 Color: 1
Size: 267151 Color: 1
Size: 253443 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 412075 Color: 1
Size: 308964 Color: 0
Size: 278962 Color: 1

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 431591 Color: 1
Size: 289347 Color: 0
Size: 279063 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 432576 Color: 1
Size: 314160 Color: 1
Size: 253265 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 475751 Color: 1
Size: 273026 Color: 0
Size: 251224 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 401891 Color: 1
Size: 330559 Color: 1
Size: 267551 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 423621 Color: 1
Size: 312110 Color: 1
Size: 264270 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 402136 Color: 1
Size: 323780 Color: 0
Size: 274085 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 416624 Color: 1
Size: 294548 Color: 0
Size: 288829 Color: 1

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 380518 Color: 0
Size: 348291 Color: 1
Size: 271192 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 489999 Color: 0
Size: 258178 Color: 1
Size: 251824 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 364423 Color: 0
Size: 322352 Color: 0
Size: 313226 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 383137 Color: 1
Size: 323823 Color: 0
Size: 293041 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 416526 Color: 1
Size: 296699 Color: 0
Size: 286776 Color: 1

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 376226 Color: 1
Size: 351764 Color: 0
Size: 272011 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 425074 Color: 0
Size: 305800 Color: 0
Size: 269127 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 372020 Color: 1
Size: 358450 Color: 1
Size: 269531 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 372384 Color: 0
Size: 318675 Color: 1
Size: 308942 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 347678 Color: 0
Size: 346385 Color: 0
Size: 305938 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 393160 Color: 0
Size: 346580 Color: 0
Size: 260261 Color: 1

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 493092 Color: 1
Size: 254821 Color: 0
Size: 252088 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 382531 Color: 1
Size: 359615 Color: 1
Size: 257855 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 393027 Color: 0
Size: 303951 Color: 0
Size: 303023 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 380459 Color: 0
Size: 321254 Color: 1
Size: 298288 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 482902 Color: 0
Size: 262539 Color: 1
Size: 254560 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 457514 Color: 1
Size: 285740 Color: 0
Size: 256747 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 364168 Color: 0
Size: 318539 Color: 0
Size: 317294 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 495779 Color: 0
Size: 253688 Color: 1
Size: 250534 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 470919 Color: 1
Size: 276122 Color: 0
Size: 252960 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 411211 Color: 1
Size: 294799 Color: 0
Size: 293991 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 437892 Color: 0
Size: 293594 Color: 1
Size: 268515 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 390743 Color: 0
Size: 339880 Color: 1
Size: 269378 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 431029 Color: 0
Size: 298087 Color: 0
Size: 270885 Color: 1

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 415960 Color: 0
Size: 325053 Color: 1
Size: 258988 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 400065 Color: 0
Size: 346027 Color: 0
Size: 253909 Color: 1

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 431193 Color: 0
Size: 318474 Color: 0
Size: 250334 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 380849 Color: 0
Size: 314690 Color: 1
Size: 304462 Color: 1

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 471050 Color: 1
Size: 275062 Color: 0
Size: 253889 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 386221 Color: 0
Size: 340700 Color: 1
Size: 273080 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 394440 Color: 1
Size: 307150 Color: 1
Size: 298411 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 375082 Color: 0
Size: 356960 Color: 1
Size: 267959 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 428805 Color: 1
Size: 295489 Color: 1
Size: 275707 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 431407 Color: 0
Size: 306090 Color: 1
Size: 262504 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 374001 Color: 1
Size: 372428 Color: 1
Size: 253572 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 434921 Color: 1
Size: 311144 Color: 0
Size: 253936 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 466431 Color: 1
Size: 275159 Color: 0
Size: 258411 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 412796 Color: 0
Size: 325614 Color: 0
Size: 261591 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 462607 Color: 0
Size: 269534 Color: 1
Size: 267860 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 370539 Color: 0
Size: 315081 Color: 1
Size: 314381 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 454959 Color: 0
Size: 276917 Color: 0
Size: 268125 Color: 1

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 391216 Color: 1
Size: 323707 Color: 0
Size: 285078 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 441474 Color: 1
Size: 282499 Color: 0
Size: 276028 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 418359 Color: 1
Size: 313572 Color: 0
Size: 268070 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 372445 Color: 0
Size: 347594 Color: 1
Size: 279962 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 453935 Color: 1
Size: 292699 Color: 1
Size: 253367 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 360750 Color: 0
Size: 330515 Color: 1
Size: 308736 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 387888 Color: 1
Size: 312792 Color: 1
Size: 299321 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 338373 Color: 1
Size: 338207 Color: 1
Size: 323421 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 370626 Color: 1
Size: 328111 Color: 0
Size: 301264 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 436688 Color: 1
Size: 283724 Color: 0
Size: 279589 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 351469 Color: 1
Size: 337293 Color: 1
Size: 311239 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 396371 Color: 1
Size: 340117 Color: 1
Size: 263513 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 340390 Color: 1
Size: 332805 Color: 1
Size: 326806 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 373273 Color: 0
Size: 329421 Color: 0
Size: 297307 Color: 1

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 436207 Color: 0
Size: 303962 Color: 1
Size: 259832 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 431254 Color: 0
Size: 288746 Color: 0
Size: 280001 Color: 1

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 429194 Color: 0
Size: 308728 Color: 0
Size: 262079 Color: 1

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 422719 Color: 1
Size: 291322 Color: 1
Size: 285960 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 492840 Color: 1
Size: 257068 Color: 0
Size: 250093 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 359475 Color: 0
Size: 320951 Color: 1
Size: 319575 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 385217 Color: 0
Size: 347940 Color: 1
Size: 266844 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 378897 Color: 0
Size: 350582 Color: 0
Size: 270522 Color: 1

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 406599 Color: 0
Size: 305724 Color: 1
Size: 287678 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 377212 Color: 0
Size: 356155 Color: 0
Size: 266634 Color: 1

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 456419 Color: 0
Size: 290300 Color: 0
Size: 253282 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 376886 Color: 0
Size: 340004 Color: 1
Size: 283111 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 439072 Color: 0
Size: 282126 Color: 1
Size: 278803 Color: 1

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 391808 Color: 1
Size: 341616 Color: 0
Size: 266577 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 477122 Color: 0
Size: 268249 Color: 1
Size: 254630 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 344861 Color: 0
Size: 338613 Color: 0
Size: 316527 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 483555 Color: 1
Size: 266288 Color: 0
Size: 250158 Color: 1

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 420072 Color: 0
Size: 298578 Color: 0
Size: 281351 Color: 1

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 392864 Color: 1
Size: 329867 Color: 1
Size: 277270 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 418629 Color: 0
Size: 292316 Color: 1
Size: 289056 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 362871 Color: 0
Size: 328280 Color: 1
Size: 308850 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 438033 Color: 0
Size: 310499 Color: 1
Size: 251469 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 469000 Color: 1
Size: 275597 Color: 0
Size: 255404 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 375398 Color: 1
Size: 366437 Color: 0
Size: 258166 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 374737 Color: 1
Size: 344672 Color: 1
Size: 280592 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 486621 Color: 0
Size: 261163 Color: 1
Size: 252217 Color: 1

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 390720 Color: 0
Size: 355120 Color: 1
Size: 254161 Color: 1

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 410730 Color: 0
Size: 313975 Color: 0
Size: 275296 Color: 1

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 435208 Color: 0
Size: 293850 Color: 0
Size: 270943 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 497025 Color: 0
Size: 252485 Color: 0
Size: 250491 Color: 1

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 491363 Color: 0
Size: 254841 Color: 1
Size: 253797 Color: 1

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 482112 Color: 1
Size: 266207 Color: 0
Size: 251682 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 378331 Color: 0
Size: 325558 Color: 0
Size: 296112 Color: 1

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 359004 Color: 1
Size: 344776 Color: 1
Size: 296221 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 380463 Color: 1
Size: 313920 Color: 0
Size: 305618 Color: 1

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 399547 Color: 1
Size: 339102 Color: 0
Size: 261352 Color: 1

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 382176 Color: 1
Size: 354569 Color: 1
Size: 263256 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 492684 Color: 1
Size: 255012 Color: 0
Size: 252305 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 416597 Color: 1
Size: 318015 Color: 0
Size: 265389 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 422735 Color: 0
Size: 323822 Color: 0
Size: 253444 Color: 1

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 375956 Color: 0
Size: 340569 Color: 1
Size: 283476 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 495926 Color: 0
Size: 252887 Color: 1
Size: 251188 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 474573 Color: 1
Size: 268833 Color: 1
Size: 256595 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 393515 Color: 0
Size: 320438 Color: 1
Size: 286048 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 392400 Color: 1
Size: 339282 Color: 1
Size: 268319 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 450038 Color: 0
Size: 276014 Color: 0
Size: 273949 Color: 1

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 453754 Color: 1
Size: 285892 Color: 0
Size: 260355 Color: 1

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 440149 Color: 1
Size: 292008 Color: 0
Size: 267844 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 382641 Color: 1
Size: 336513 Color: 1
Size: 280847 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 433414 Color: 0
Size: 285334 Color: 1
Size: 281253 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 364958 Color: 0
Size: 350503 Color: 1
Size: 284540 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 460479 Color: 0
Size: 287043 Color: 1
Size: 252479 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 420311 Color: 0
Size: 292840 Color: 1
Size: 286850 Color: 0

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 482455 Color: 0
Size: 260625 Color: 0
Size: 256921 Color: 1

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 394701 Color: 0
Size: 314810 Color: 0
Size: 290490 Color: 1

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 448305 Color: 0
Size: 299381 Color: 1
Size: 252315 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 426037 Color: 0
Size: 301305 Color: 1
Size: 272659 Color: 1

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 365474 Color: 1
Size: 345265 Color: 0
Size: 289262 Color: 1

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 412116 Color: 0
Size: 322876 Color: 0
Size: 265009 Color: 1

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 391521 Color: 1
Size: 356752 Color: 1
Size: 251728 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 360140 Color: 1
Size: 329291 Color: 0
Size: 310570 Color: 1

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 448358 Color: 1
Size: 287793 Color: 1
Size: 263850 Color: 0

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 459399 Color: 1
Size: 273129 Color: 1
Size: 267473 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 431703 Color: 0
Size: 290670 Color: 0
Size: 277628 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 390063 Color: 0
Size: 331259 Color: 1
Size: 278679 Color: 0

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 438577 Color: 0
Size: 285625 Color: 0
Size: 275799 Color: 1

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 464094 Color: 0
Size: 279340 Color: 0
Size: 256567 Color: 1

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 462732 Color: 1
Size: 281262 Color: 1
Size: 256007 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 420580 Color: 1
Size: 298086 Color: 1
Size: 281335 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 393126 Color: 0
Size: 315200 Color: 1
Size: 291675 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 462902 Color: 0
Size: 283832 Color: 1
Size: 253267 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 457395 Color: 0
Size: 286086 Color: 1
Size: 256520 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 376486 Color: 1
Size: 324572 Color: 1
Size: 298943 Color: 0

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 389590 Color: 1
Size: 359782 Color: 0
Size: 250629 Color: 1

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 373057 Color: 1
Size: 319277 Color: 0
Size: 307667 Color: 1

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 447692 Color: 0
Size: 281528 Color: 1
Size: 270781 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 411331 Color: 1
Size: 318339 Color: 0
Size: 270331 Color: 0

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 418274 Color: 1
Size: 301024 Color: 1
Size: 280703 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 371358 Color: 1
Size: 317449 Color: 0
Size: 311194 Color: 1

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 430114 Color: 1
Size: 319154 Color: 0
Size: 250733 Color: 1

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 470837 Color: 1
Size: 277554 Color: 1
Size: 251610 Color: 0

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 361373 Color: 1
Size: 324533 Color: 0
Size: 314095 Color: 1

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 380989 Color: 0
Size: 311339 Color: 0
Size: 307673 Color: 1

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 369426 Color: 0
Size: 357940 Color: 0
Size: 272635 Color: 1

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 374459 Color: 1
Size: 314303 Color: 0
Size: 311239 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 409367 Color: 0
Size: 323328 Color: 1
Size: 267306 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 390352 Color: 0
Size: 338888 Color: 0
Size: 270761 Color: 1

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 444657 Color: 0
Size: 291363 Color: 0
Size: 263981 Color: 1

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 411097 Color: 1
Size: 312633 Color: 1
Size: 276271 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 432189 Color: 0
Size: 289192 Color: 1
Size: 278620 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 440417 Color: 1
Size: 303269 Color: 1
Size: 256315 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 377555 Color: 0
Size: 331330 Color: 0
Size: 291116 Color: 1

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 464079 Color: 0
Size: 281078 Color: 1
Size: 254844 Color: 1

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 449208 Color: 1
Size: 289120 Color: 0
Size: 261673 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 376402 Color: 0
Size: 372443 Color: 0
Size: 251156 Color: 1

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 498002 Color: 1
Size: 251267 Color: 1
Size: 250732 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 402405 Color: 1
Size: 305249 Color: 1
Size: 292347 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 465708 Color: 1
Size: 270773 Color: 1
Size: 263520 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 396178 Color: 1
Size: 332902 Color: 1
Size: 270921 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 363984 Color: 0
Size: 357699 Color: 0
Size: 278318 Color: 1

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 414463 Color: 1
Size: 334737 Color: 1
Size: 250801 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 420223 Color: 0
Size: 324649 Color: 0
Size: 255129 Color: 1

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 366520 Color: 1
Size: 354597 Color: 0
Size: 278884 Color: 1

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 436832 Color: 0
Size: 299883 Color: 1
Size: 263286 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 385542 Color: 0
Size: 319330 Color: 1
Size: 295129 Color: 0

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 495485 Color: 1
Size: 254041 Color: 1
Size: 250475 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 382220 Color: 0
Size: 311888 Color: 0
Size: 305893 Color: 1

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 433475 Color: 0
Size: 285847 Color: 0
Size: 280679 Color: 1

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 478006 Color: 1
Size: 269043 Color: 0
Size: 252952 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 423695 Color: 0
Size: 292445 Color: 0
Size: 283861 Color: 1

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 360878 Color: 1
Size: 337546 Color: 1
Size: 301577 Color: 0

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 451091 Color: 0
Size: 294231 Color: 1
Size: 254679 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 411580 Color: 0
Size: 312762 Color: 1
Size: 275659 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 393901 Color: 1
Size: 355800 Color: 0
Size: 250300 Color: 1

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 358845 Color: 1
Size: 327669 Color: 0
Size: 313487 Color: 0

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 359826 Color: 0
Size: 331608 Color: 1
Size: 308567 Color: 1

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 364425 Color: 1
Size: 362899 Color: 1
Size: 272677 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 366454 Color: 0
Size: 362588 Color: 1
Size: 270959 Color: 1

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 353240 Color: 0
Size: 352544 Color: 1
Size: 294217 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 380591 Color: 0
Size: 352369 Color: 1
Size: 267041 Color: 1

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 359500 Color: 0
Size: 355188 Color: 1
Size: 285313 Color: 1

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 399130 Color: 0
Size: 320464 Color: 1
Size: 280407 Color: 1

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 354952 Color: 0
Size: 343889 Color: 1
Size: 301160 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 366037 Color: 1
Size: 347691 Color: 0
Size: 286273 Color: 1

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 398413 Color: 0
Size: 305253 Color: 1
Size: 296335 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 449494 Color: 0
Size: 277893 Color: 0
Size: 272614 Color: 1

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 344990 Color: 1
Size: 337107 Color: 0
Size: 317904 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 364954 Color: 1
Size: 329603 Color: 0
Size: 305444 Color: 1

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 350954 Color: 0
Size: 329797 Color: 0
Size: 319250 Color: 1

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 350556 Color: 0
Size: 330826 Color: 0
Size: 318619 Color: 1

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 350082 Color: 0
Size: 341522 Color: 0
Size: 308397 Color: 1

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 357006 Color: 0
Size: 354593 Color: 1
Size: 288402 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 347822 Color: 1
Size: 337830 Color: 1
Size: 314349 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 478782 Color: 1
Size: 264416 Color: 0
Size: 256803 Color: 0

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 394200 Color: 1
Size: 344602 Color: 0
Size: 261199 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 337264 Color: 1
Size: 333219 Color: 0
Size: 329518 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 345228 Color: 0
Size: 328151 Color: 0
Size: 326622 Color: 1

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 351258 Color: 1
Size: 343373 Color: 0
Size: 305370 Color: 1

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 404989 Color: 0
Size: 311400 Color: 1
Size: 283612 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 362358 Color: 1
Size: 354984 Color: 1
Size: 282659 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 356802 Color: 0
Size: 348668 Color: 1
Size: 294531 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 380762 Color: 0
Size: 346776 Color: 0
Size: 272463 Color: 1

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 350431 Color: 1
Size: 348428 Color: 0
Size: 301142 Color: 0

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 347003 Color: 0
Size: 346526 Color: 0
Size: 306472 Color: 1

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 345936 Color: 0
Size: 345903 Color: 0
Size: 308162 Color: 1

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 362918 Color: 1
Size: 323855 Color: 0
Size: 313228 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 342450 Color: 1
Size: 342255 Color: 1
Size: 315296 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 392890 Color: 0
Size: 340109 Color: 1
Size: 267002 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 344728 Color: 0
Size: 343798 Color: 0
Size: 311475 Color: 1

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 342452 Color: 0
Size: 341886 Color: 1
Size: 315663 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 352208 Color: 0
Size: 344190 Color: 0
Size: 303603 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 393059 Color: 0
Size: 342899 Color: 1
Size: 264043 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 463834 Color: 1
Size: 272265 Color: 0
Size: 263902 Color: 1

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 340890 Color: 1
Size: 340461 Color: 0
Size: 318650 Color: 1

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 391299 Color: 1
Size: 336239 Color: 1
Size: 272463 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 442599 Color: 0
Size: 285703 Color: 1
Size: 271699 Color: 1

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 394771 Color: 0
Size: 327341 Color: 1
Size: 277889 Color: 1

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 440353 Color: 0
Size: 280606 Color: 1
Size: 279042 Color: 0

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 341809 Color: 0
Size: 331786 Color: 1
Size: 326406 Color: 1

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 339407 Color: 0
Size: 338210 Color: 1
Size: 322384 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 351667 Color: 0
Size: 349847 Color: 1
Size: 298487 Color: 0

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 339385 Color: 0
Size: 333979 Color: 1
Size: 326637 Color: 1

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 350995 Color: 0
Size: 328920 Color: 0
Size: 320086 Color: 1

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 352010 Color: 0
Size: 335457 Color: 1
Size: 312534 Color: 1

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 351944 Color: 1
Size: 328535 Color: 1
Size: 319522 Color: 0

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 354442 Color: 0
Size: 344951 Color: 1
Size: 300608 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 354395 Color: 1
Size: 335099 Color: 0
Size: 310507 Color: 1

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 356045 Color: 1
Size: 325749 Color: 0
Size: 318207 Color: 1

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 356725 Color: 0
Size: 355519 Color: 0
Size: 287757 Color: 1

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 356337 Color: 1
Size: 331212 Color: 1
Size: 312452 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 356826 Color: 1
Size: 347542 Color: 1
Size: 295633 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 357037 Color: 0
Size: 334780 Color: 1
Size: 308184 Color: 0

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 357601 Color: 0
Size: 332239 Color: 1
Size: 310161 Color: 0

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 358142 Color: 1
Size: 345011 Color: 0
Size: 296848 Color: 1

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 358078 Color: 0
Size: 341585 Color: 1
Size: 300338 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 358490 Color: 1
Size: 337728 Color: 1
Size: 303783 Color: 0

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 358146 Color: 0
Size: 348219 Color: 0
Size: 293636 Color: 1

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 358607 Color: 0
Size: 327142 Color: 0
Size: 314252 Color: 1

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 358942 Color: 0
Size: 357840 Color: 1
Size: 283219 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 359072 Color: 0
Size: 326455 Color: 1
Size: 314474 Color: 0

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 359086 Color: 0
Size: 334357 Color: 0
Size: 306558 Color: 1

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 359509 Color: 1
Size: 350695 Color: 0
Size: 289797 Color: 1

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 359708 Color: 1
Size: 324330 Color: 0
Size: 315963 Color: 1

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 359554 Color: 0
Size: 325249 Color: 1
Size: 315198 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 359837 Color: 1
Size: 325494 Color: 1
Size: 314670 Color: 0

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 359628 Color: 0
Size: 346538 Color: 1
Size: 293835 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 359887 Color: 0
Size: 350397 Color: 0
Size: 289717 Color: 1

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 360287 Color: 0
Size: 352998 Color: 1
Size: 286716 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 360331 Color: 0
Size: 350807 Color: 1
Size: 288863 Color: 1

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 360334 Color: 0
Size: 341726 Color: 0
Size: 297941 Color: 1

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 360566 Color: 1
Size: 334258 Color: 1
Size: 305177 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 360633 Color: 0
Size: 349403 Color: 1
Size: 289965 Color: 0

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 360839 Color: 0
Size: 331180 Color: 0
Size: 307982 Color: 1

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 361345 Color: 1
Size: 321031 Color: 1
Size: 317625 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 361178 Color: 0
Size: 335678 Color: 1
Size: 303145 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 361483 Color: 1
Size: 351047 Color: 0
Size: 287471 Color: 1

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 361677 Color: 0
Size: 343992 Color: 1
Size: 294332 Color: 0

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 361793 Color: 0
Size: 326715 Color: 1
Size: 311493 Color: 0

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 361712 Color: 1
Size: 322448 Color: 0
Size: 315841 Color: 1

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 361991 Color: 0
Size: 323505 Color: 0
Size: 314505 Color: 1

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 362124 Color: 1
Size: 334076 Color: 0
Size: 303801 Color: 1

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 362237 Color: 0
Size: 339059 Color: 0
Size: 298705 Color: 1

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 362252 Color: 0
Size: 325159 Color: 0
Size: 312590 Color: 1

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 362506 Color: 1
Size: 326517 Color: 0
Size: 310978 Color: 1

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 362436 Color: 0
Size: 345090 Color: 1
Size: 292475 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 362791 Color: 1
Size: 323887 Color: 0
Size: 313323 Color: 1

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 363053 Color: 1
Size: 335747 Color: 1
Size: 301201 Color: 0

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 363030 Color: 0
Size: 325532 Color: 0
Size: 311439 Color: 1

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 363109 Color: 0
Size: 326975 Color: 1
Size: 309917 Color: 0

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 363245 Color: 1
Size: 344990 Color: 1
Size: 291766 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 363442 Color: 1
Size: 340694 Color: 0
Size: 295865 Color: 1

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 363579 Color: 1
Size: 326087 Color: 0
Size: 310335 Color: 0

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 363619 Color: 0
Size: 323897 Color: 1
Size: 312485 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 363748 Color: 1
Size: 356373 Color: 1
Size: 279880 Color: 0

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 363665 Color: 0
Size: 320722 Color: 1
Size: 315614 Color: 0

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 363757 Color: 1
Size: 356355 Color: 0
Size: 279889 Color: 1

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 363970 Color: 0
Size: 357700 Color: 0
Size: 278331 Color: 1

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 364060 Color: 1
Size: 326194 Color: 1
Size: 309747 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 364103 Color: 1
Size: 334738 Color: 1
Size: 301160 Color: 0

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 364181 Color: 0
Size: 324793 Color: 0
Size: 311027 Color: 1

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 364330 Color: 1
Size: 342457 Color: 1
Size: 293214 Color: 0

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 364233 Color: 0
Size: 337986 Color: 0
Size: 297782 Color: 1

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 364372 Color: 1
Size: 350714 Color: 0
Size: 284915 Color: 1

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 364517 Color: 0
Size: 322458 Color: 0
Size: 313026 Color: 1

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 364585 Color: 0
Size: 323874 Color: 0
Size: 311542 Color: 1

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 364834 Color: 1
Size: 354831 Color: 1
Size: 280336 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 364613 Color: 0
Size: 362426 Color: 1
Size: 272962 Color: 0

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 364629 Color: 0
Size: 345273 Color: 0
Size: 290099 Color: 1

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 364974 Color: 1
Size: 325236 Color: 0
Size: 309791 Color: 1

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 364844 Color: 0
Size: 355634 Color: 1
Size: 279523 Color: 0

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 365160 Color: 1
Size: 322328 Color: 1
Size: 312513 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 365184 Color: 0
Size: 345393 Color: 0
Size: 289424 Color: 1

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 365501 Color: 1
Size: 321528 Color: 1
Size: 312972 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 365489 Color: 0
Size: 348185 Color: 0
Size: 286327 Color: 1

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 365643 Color: 1
Size: 337130 Color: 1
Size: 297228 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 366023 Color: 1
Size: 317824 Color: 0
Size: 316154 Color: 0

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 365728 Color: 0
Size: 323314 Color: 0
Size: 310959 Color: 1

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 366063 Color: 0
Size: 339385 Color: 1
Size: 294553 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 366449 Color: 1
Size: 320748 Color: 1
Size: 312804 Color: 0

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 366253 Color: 0
Size: 333718 Color: 1
Size: 300030 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 366468 Color: 1
Size: 361336 Color: 0
Size: 272197 Color: 1

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 366475 Color: 1
Size: 327528 Color: 0
Size: 305998 Color: 1

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 366347 Color: 0
Size: 321151 Color: 1
Size: 312503 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 366578 Color: 1
Size: 347371 Color: 0
Size: 286052 Color: 1

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 366897 Color: 1
Size: 352494 Color: 1
Size: 280610 Color: 0

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 366554 Color: 0
Size: 351035 Color: 0
Size: 282412 Color: 1

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 366960 Color: 1
Size: 361325 Color: 1
Size: 271716 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 366578 Color: 0
Size: 364486 Color: 1
Size: 268937 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 366984 Color: 1
Size: 352797 Color: 1
Size: 280220 Color: 0

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 366692 Color: 0
Size: 326750 Color: 0
Size: 306559 Color: 1

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 367001 Color: 1
Size: 321956 Color: 0
Size: 311044 Color: 1

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 366747 Color: 0
Size: 340407 Color: 0
Size: 292847 Color: 1

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 367063 Color: 1
Size: 348052 Color: 0
Size: 284886 Color: 1

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 366967 Color: 0
Size: 354683 Color: 1
Size: 278351 Color: 0

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 367068 Color: 1
Size: 341907 Color: 1
Size: 291026 Color: 0

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 367091 Color: 1
Size: 347111 Color: 0
Size: 285799 Color: 1

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 367125 Color: 0
Size: 346895 Color: 1
Size: 285981 Color: 0

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 367206 Color: 1
Size: 351710 Color: 1
Size: 281085 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 367148 Color: 0
Size: 327912 Color: 1
Size: 304941 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 367261 Color: 1
Size: 349551 Color: 0
Size: 283189 Color: 1

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 367291 Color: 1
Size: 317541 Color: 0
Size: 315169 Color: 1

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 367312 Color: 0
Size: 339749 Color: 0
Size: 292940 Color: 1

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 367537 Color: 1
Size: 334382 Color: 1
Size: 298082 Color: 0

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 367327 Color: 0
Size: 337589 Color: 0
Size: 295085 Color: 1

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 367623 Color: 1
Size: 317101 Color: 0
Size: 315277 Color: 1

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 367883 Color: 0
Size: 327854 Color: 0
Size: 304264 Color: 1

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 367965 Color: 0
Size: 339008 Color: 1
Size: 293028 Color: 1

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 367989 Color: 0
Size: 356852 Color: 0
Size: 275160 Color: 1

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 367671 Color: 1
Size: 367654 Color: 0
Size: 264676 Color: 1

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 367990 Color: 0
Size: 323804 Color: 0
Size: 308207 Color: 1

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 368065 Color: 0
Size: 335051 Color: 0
Size: 296885 Color: 1

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 367704 Color: 1
Size: 331520 Color: 0
Size: 300777 Color: 1

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 368195 Color: 0
Size: 350402 Color: 1
Size: 281404 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 367751 Color: 1
Size: 340348 Color: 1
Size: 291902 Color: 0

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 368208 Color: 0
Size: 321264 Color: 0
Size: 310529 Color: 1

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 367778 Color: 1
Size: 365270 Color: 1
Size: 266953 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 368244 Color: 0
Size: 340226 Color: 1
Size: 291531 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 367785 Color: 1
Size: 331619 Color: 1
Size: 300597 Color: 0

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 368287 Color: 0
Size: 357489 Color: 1
Size: 274225 Color: 0

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 368306 Color: 0
Size: 342334 Color: 1
Size: 289361 Color: 0

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 368043 Color: 1
Size: 364323 Color: 0
Size: 267635 Color: 1

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 368250 Color: 1
Size: 361325 Color: 0
Size: 270426 Color: 1

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 368340 Color: 0
Size: 323261 Color: 1
Size: 308400 Color: 0

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 368254 Color: 1
Size: 337525 Color: 0
Size: 294222 Color: 1

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 368384 Color: 0
Size: 358415 Color: 1
Size: 273202 Color: 0

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 368308 Color: 1
Size: 332564 Color: 0
Size: 299129 Color: 1

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 368462 Color: 0
Size: 358182 Color: 1
Size: 273357 Color: 0

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 368381 Color: 1
Size: 359831 Color: 1
Size: 271789 Color: 0

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 368499 Color: 1
Size: 318249 Color: 0
Size: 313253 Color: 1

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 368689 Color: 1
Size: 316754 Color: 1
Size: 314558 Color: 0

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 368603 Color: 0
Size: 365965 Color: 0
Size: 265433 Color: 1

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 368701 Color: 1
Size: 340029 Color: 0
Size: 291271 Color: 1

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 368716 Color: 1
Size: 328591 Color: 0
Size: 302694 Color: 1

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 368919 Color: 0
Size: 334686 Color: 0
Size: 296396 Color: 1

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 368727 Color: 1
Size: 339111 Color: 1
Size: 292163 Color: 0

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 368760 Color: 1
Size: 366216 Color: 0
Size: 265025 Color: 1

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 369097 Color: 0
Size: 343790 Color: 0
Size: 287114 Color: 1

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 369098 Color: 0
Size: 346886 Color: 0
Size: 284017 Color: 1

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 369040 Color: 1
Size: 322341 Color: 1
Size: 308620 Color: 0

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 369295 Color: 1
Size: 328208 Color: 0
Size: 302498 Color: 0

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 369155 Color: 0
Size: 339755 Color: 1
Size: 291091 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 369333 Color: 1
Size: 364873 Color: 1
Size: 265795 Color: 0

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 369420 Color: 1
Size: 334914 Color: 0
Size: 295667 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 369451 Color: 1
Size: 335061 Color: 1
Size: 295489 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 369290 Color: 0
Size: 333977 Color: 0
Size: 296734 Color: 1

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 369545 Color: 1
Size: 336908 Color: 0
Size: 293548 Color: 0

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 369584 Color: 1
Size: 359538 Color: 0
Size: 270879 Color: 1

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 369608 Color: 1
Size: 315761 Color: 0
Size: 314632 Color: 1

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 369437 Color: 0
Size: 327187 Color: 1
Size: 303377 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 369610 Color: 1
Size: 315445 Color: 1
Size: 314946 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 369501 Color: 0
Size: 321983 Color: 1
Size: 308517 Color: 0

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 369619 Color: 1
Size: 360773 Color: 1
Size: 269609 Color: 0

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 369508 Color: 0
Size: 360989 Color: 0
Size: 269504 Color: 1

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 369660 Color: 1
Size: 332720 Color: 0
Size: 297621 Color: 1

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 369659 Color: 0
Size: 349731 Color: 0
Size: 280611 Color: 1

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 369750 Color: 1
Size: 355833 Color: 0
Size: 274418 Color: 1

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 369805 Color: 0
Size: 346237 Color: 0
Size: 283959 Color: 1

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 369937 Color: 1
Size: 336234 Color: 1
Size: 293830 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 369950 Color: 1
Size: 353631 Color: 0
Size: 276420 Color: 1

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 369957 Color: 0
Size: 353373 Color: 0
Size: 276671 Color: 1

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 369961 Color: 1
Size: 338069 Color: 0
Size: 291971 Color: 1

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 369962 Color: 0
Size: 322439 Color: 0
Size: 307600 Color: 1

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 369978 Color: 1
Size: 336533 Color: 1
Size: 293490 Color: 0

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 369985 Color: 0
Size: 340937 Color: 1
Size: 289079 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 370000 Color: 1
Size: 356140 Color: 0
Size: 273861 Color: 1

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 370017 Color: 0
Size: 315562 Color: 1
Size: 314422 Color: 0

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 370066 Color: 1
Size: 325291 Color: 1
Size: 304644 Color: 0

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 370122 Color: 1
Size: 327482 Color: 1
Size: 302397 Color: 0

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 370287 Color: 0
Size: 321741 Color: 0
Size: 307973 Color: 1

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 370340 Color: 1
Size: 351539 Color: 1
Size: 278122 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 370317 Color: 0
Size: 339679 Color: 1
Size: 290005 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 370417 Color: 1
Size: 336749 Color: 1
Size: 292835 Color: 0

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 370371 Color: 0
Size: 361590 Color: 0
Size: 268040 Color: 1

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 370426 Color: 1
Size: 335171 Color: 0
Size: 294404 Color: 1

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 370433 Color: 0
Size: 315486 Color: 1
Size: 314082 Color: 0

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 370469 Color: 1
Size: 369511 Color: 0
Size: 260021 Color: 1

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 370585 Color: 1
Size: 367915 Color: 1
Size: 261501 Color: 0

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 370574 Color: 0
Size: 328982 Color: 0
Size: 300445 Color: 1

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 370630 Color: 0
Size: 316665 Color: 1
Size: 312706 Color: 0

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 370694 Color: 0
Size: 362530 Color: 1
Size: 266777 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 370632 Color: 0
Size: 367728 Color: 1
Size: 261641 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 370696 Color: 1
Size: 355306 Color: 1
Size: 273999 Color: 0

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 370694 Color: 1
Size: 332842 Color: 0
Size: 296465 Color: 1

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 370791 Color: 1
Size: 339881 Color: 1
Size: 289329 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 370756 Color: 0
Size: 315233 Color: 0
Size: 314012 Color: 1

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 370823 Color: 1
Size: 320004 Color: 1
Size: 309174 Color: 0

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 370979 Color: 1
Size: 331585 Color: 1
Size: 297437 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 371178 Color: 1
Size: 343021 Color: 1
Size: 285802 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 370950 Color: 0
Size: 365873 Color: 1
Size: 263178 Color: 0

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 371202 Color: 1
Size: 368444 Color: 0
Size: 260355 Color: 1

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 371262 Color: 0
Size: 327416 Color: 1
Size: 301323 Color: 0

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 371246 Color: 1
Size: 325772 Color: 0
Size: 302983 Color: 1

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 371378 Color: 0
Size: 334590 Color: 1
Size: 294033 Color: 0

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 371271 Color: 1
Size: 347301 Color: 0
Size: 281429 Color: 1

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 371439 Color: 0
Size: 332400 Color: 0
Size: 296162 Color: 1

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 371458 Color: 0
Size: 326912 Color: 1
Size: 301631 Color: 0

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 371560 Color: 1
Size: 317034 Color: 0
Size: 311407 Color: 1

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 371633 Color: 0
Size: 323506 Color: 1
Size: 304862 Color: 0

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 371641 Color: 1
Size: 357986 Color: 1
Size: 270374 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 371823 Color: 0
Size: 338571 Color: 1
Size: 289607 Color: 0

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 371921 Color: 0
Size: 316989 Color: 1
Size: 311091 Color: 1

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 371944 Color: 0
Size: 348417 Color: 0
Size: 279640 Color: 1

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 371966 Color: 0
Size: 370567 Color: 0
Size: 257468 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 372091 Color: 1
Size: 333360 Color: 0
Size: 294550 Color: 1

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 372007 Color: 0
Size: 337498 Color: 0
Size: 290496 Color: 1

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 372123 Color: 1
Size: 352675 Color: 1
Size: 275203 Color: 0

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 372175 Color: 0
Size: 346649 Color: 0
Size: 281177 Color: 1

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 372228 Color: 0
Size: 364051 Color: 1
Size: 263722 Color: 1

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 372344 Color: 0
Size: 317292 Color: 1
Size: 310365 Color: 0

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 372356 Color: 0
Size: 319726 Color: 1
Size: 307919 Color: 1

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 372370 Color: 0
Size: 341163 Color: 0
Size: 286468 Color: 1

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 372383 Color: 1
Size: 322432 Color: 0
Size: 305186 Color: 1

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 372415 Color: 1
Size: 332624 Color: 1
Size: 294962 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 372472 Color: 0
Size: 323002 Color: 1
Size: 304527 Color: 0

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 372562 Color: 0
Size: 318541 Color: 0
Size: 308898 Color: 1

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 372601 Color: 0
Size: 358639 Color: 1
Size: 268761 Color: 1

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 372638 Color: 0
Size: 346301 Color: 1
Size: 281062 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 372673 Color: 0
Size: 369997 Color: 1
Size: 257331 Color: 1

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 372418 Color: 1
Size: 334823 Color: 0
Size: 292760 Color: 1

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 372708 Color: 0
Size: 350689 Color: 1
Size: 276604 Color: 0

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 372475 Color: 1
Size: 361621 Color: 0
Size: 265905 Color: 1

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 372744 Color: 0
Size: 357297 Color: 0
Size: 269960 Color: 1

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 372589 Color: 1
Size: 337026 Color: 0
Size: 290386 Color: 1

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 372874 Color: 0
Size: 371807 Color: 0
Size: 255320 Color: 1

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 372600 Color: 1
Size: 329216 Color: 1
Size: 298185 Color: 0

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 372900 Color: 0
Size: 326131 Color: 1
Size: 300970 Color: 0

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 372648 Color: 1
Size: 372273 Color: 1
Size: 255080 Color: 0

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 372918 Color: 0
Size: 339468 Color: 0
Size: 287615 Color: 1

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 372733 Color: 1
Size: 318961 Color: 0
Size: 308307 Color: 1

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 372970 Color: 0
Size: 357809 Color: 0
Size: 269222 Color: 1

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 372738 Color: 1
Size: 317062 Color: 1
Size: 310201 Color: 0

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 373031 Color: 0
Size: 341878 Color: 0
Size: 285092 Color: 1

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 373035 Color: 0
Size: 347650 Color: 1
Size: 279316 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 373117 Color: 0
Size: 352967 Color: 1
Size: 273917 Color: 0

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 373219 Color: 0
Size: 341030 Color: 1
Size: 285752 Color: 0

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 373151 Color: 1
Size: 341818 Color: 1
Size: 285032 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 373259 Color: 0
Size: 329528 Color: 1
Size: 297214 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 373200 Color: 1
Size: 363607 Color: 0
Size: 263194 Color: 1

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 373332 Color: 0
Size: 351514 Color: 0
Size: 275155 Color: 1

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 373304 Color: 1
Size: 317071 Color: 0
Size: 309626 Color: 1

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 373357 Color: 0
Size: 317112 Color: 0
Size: 309532 Color: 1

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 373455 Color: 0
Size: 372896 Color: 1
Size: 253650 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 373431 Color: 1
Size: 315076 Color: 1
Size: 311494 Color: 0

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 373484 Color: 0
Size: 319546 Color: 0
Size: 306971 Color: 1

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 373450 Color: 1
Size: 341813 Color: 0
Size: 284738 Color: 1

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 373489 Color: 0
Size: 368178 Color: 1
Size: 258334 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 373462 Color: 1
Size: 316554 Color: 0
Size: 309985 Color: 1

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 373554 Color: 0
Size: 335305 Color: 0
Size: 291142 Color: 1

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 373464 Color: 1
Size: 343028 Color: 1
Size: 283509 Color: 0

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 373717 Color: 0
Size: 328576 Color: 1
Size: 297708 Color: 0

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 373510 Color: 1
Size: 321508 Color: 0
Size: 304983 Color: 1

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 373787 Color: 0
Size: 353668 Color: 0
Size: 272546 Color: 1

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 373855 Color: 0
Size: 371625 Color: 1
Size: 254521 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 373618 Color: 1
Size: 327601 Color: 1
Size: 298782 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 373903 Color: 0
Size: 322092 Color: 0
Size: 304006 Color: 1

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 373674 Color: 1
Size: 329579 Color: 1
Size: 296748 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 373710 Color: 1
Size: 317744 Color: 1
Size: 308547 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 373976 Color: 0
Size: 314395 Color: 0
Size: 311630 Color: 1

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 373762 Color: 1
Size: 314563 Color: 0
Size: 311676 Color: 1

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 374065 Color: 0
Size: 320872 Color: 1
Size: 305064 Color: 0

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 374099 Color: 1
Size: 368771 Color: 1
Size: 257131 Color: 0

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 374185 Color: 0
Size: 346672 Color: 0
Size: 279144 Color: 1

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 374183 Color: 1
Size: 316283 Color: 1
Size: 309535 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 374186 Color: 0
Size: 319517 Color: 0
Size: 306298 Color: 1

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 374298 Color: 1
Size: 320543 Color: 1
Size: 305160 Color: 0

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 374217 Color: 0
Size: 363529 Color: 1
Size: 262255 Color: 0

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 374305 Color: 1
Size: 328322 Color: 1
Size: 297374 Color: 0

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 374257 Color: 0
Size: 328531 Color: 1
Size: 297213 Color: 0

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 374331 Color: 1
Size: 360790 Color: 1
Size: 264880 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 374467 Color: 1
Size: 363870 Color: 0
Size: 261664 Color: 1

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 374340 Color: 0
Size: 330749 Color: 0
Size: 294912 Color: 1

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 374513 Color: 1
Size: 350172 Color: 1
Size: 275316 Color: 0

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 374351 Color: 0
Size: 330615 Color: 1
Size: 295035 Color: 0

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 374540 Color: 1
Size: 323959 Color: 0
Size: 301502 Color: 1

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 374478 Color: 0
Size: 328297 Color: 0
Size: 297226 Color: 1

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 374552 Color: 1
Size: 347282 Color: 1
Size: 278167 Color: 0

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 374479 Color: 0
Size: 333441 Color: 0
Size: 292081 Color: 1

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 374646 Color: 1
Size: 318688 Color: 1
Size: 306667 Color: 0

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 374534 Color: 0
Size: 323114 Color: 1
Size: 302353 Color: 0

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 374578 Color: 0
Size: 360251 Color: 0
Size: 265172 Color: 1

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 374783 Color: 1
Size: 347114 Color: 0
Size: 278104 Color: 1

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 374832 Color: 1
Size: 365188 Color: 0
Size: 259981 Color: 0

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 374624 Color: 0
Size: 341164 Color: 0
Size: 284213 Color: 1

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 374874 Color: 1
Size: 326386 Color: 1
Size: 298741 Color: 0

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 374681 Color: 0
Size: 356339 Color: 1
Size: 268981 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 374914 Color: 1
Size: 349164 Color: 1
Size: 275923 Color: 0

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 374731 Color: 0
Size: 317180 Color: 0
Size: 308090 Color: 1

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 374926 Color: 1
Size: 324940 Color: 1
Size: 300135 Color: 0

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 374815 Color: 0
Size: 349503 Color: 0
Size: 275683 Color: 1

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 374953 Color: 1
Size: 345534 Color: 1
Size: 279514 Color: 0

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 374961 Color: 1
Size: 351026 Color: 0
Size: 274014 Color: 1

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 375048 Color: 1
Size: 353375 Color: 0
Size: 271578 Color: 1

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 375076 Color: 1
Size: 356038 Color: 0
Size: 268887 Color: 1

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 375151 Color: 0
Size: 352602 Color: 1
Size: 272248 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 375101 Color: 1
Size: 340735 Color: 1
Size: 284165 Color: 0

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 375174 Color: 0
Size: 348544 Color: 0
Size: 276283 Color: 1

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 375134 Color: 1
Size: 361914 Color: 0
Size: 262953 Color: 1

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 375227 Color: 0
Size: 358592 Color: 0
Size: 266182 Color: 1

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 375251 Color: 1
Size: 352032 Color: 1
Size: 272718 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 375281 Color: 0
Size: 334001 Color: 0
Size: 290719 Color: 1

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 375270 Color: 1
Size: 358311 Color: 1
Size: 266420 Color: 0

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 375342 Color: 0
Size: 347177 Color: 0
Size: 277482 Color: 1

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 375401 Color: 0
Size: 334583 Color: 1
Size: 290017 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 375517 Color: 1
Size: 337747 Color: 1
Size: 286737 Color: 0

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 375479 Color: 0
Size: 336211 Color: 0
Size: 288311 Color: 1

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 375625 Color: 0
Size: 361293 Color: 1
Size: 263083 Color: 1

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 375655 Color: 0
Size: 323798 Color: 0
Size: 300548 Color: 1

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 375726 Color: 0
Size: 334937 Color: 1
Size: 289338 Color: 1

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 375787 Color: 0
Size: 356458 Color: 1
Size: 267756 Color: 0

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 375724 Color: 1
Size: 372140 Color: 1
Size: 252137 Color: 0

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 375795 Color: 0
Size: 367260 Color: 0
Size: 256946 Color: 1

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 375785 Color: 1
Size: 350069 Color: 0
Size: 274147 Color: 1

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 375822 Color: 0
Size: 336861 Color: 1
Size: 287318 Color: 0

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 375864 Color: 0
Size: 364704 Color: 1
Size: 259433 Color: 1

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 375895 Color: 1
Size: 324612 Color: 0
Size: 299494 Color: 1

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 375971 Color: 0
Size: 358942 Color: 0
Size: 265088 Color: 1

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 375933 Color: 1
Size: 320504 Color: 1
Size: 303564 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 375991 Color: 0
Size: 354447 Color: 1
Size: 269563 Color: 0

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 376000 Color: 1
Size: 313890 Color: 0
Size: 310111 Color: 1

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 376090 Color: 0
Size: 365577 Color: 1
Size: 258334 Color: 0

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 376109 Color: 1
Size: 354871 Color: 0
Size: 269021 Color: 1

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 376179 Color: 0
Size: 342893 Color: 1
Size: 280929 Color: 0

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 376224 Color: 0
Size: 319251 Color: 1
Size: 304526 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 376205 Color: 1
Size: 353701 Color: 0
Size: 270095 Color: 1

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 376225 Color: 0
Size: 369471 Color: 1
Size: 254305 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 376342 Color: 0
Size: 369540 Color: 1
Size: 254119 Color: 0

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 376395 Color: 1
Size: 342260 Color: 0
Size: 281346 Color: 1

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 376437 Color: 0
Size: 355790 Color: 1
Size: 267774 Color: 0

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 376495 Color: 0
Size: 340275 Color: 1
Size: 283231 Color: 1

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 376524 Color: 0
Size: 339236 Color: 0
Size: 284241 Color: 1

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 376628 Color: 0
Size: 362700 Color: 0
Size: 260673 Color: 1

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 376470 Color: 1
Size: 344691 Color: 1
Size: 278840 Color: 0

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 376699 Color: 0
Size: 365474 Color: 1
Size: 257828 Color: 0

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 376959 Color: 0
Size: 345457 Color: 0
Size: 277585 Color: 1

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 376609 Color: 1
Size: 332247 Color: 0
Size: 291145 Color: 1

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 376975 Color: 0
Size: 344762 Color: 1
Size: 278264 Color: 0

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 376690 Color: 1
Size: 357654 Color: 0
Size: 265657 Color: 1

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 377086 Color: 0
Size: 365001 Color: 1
Size: 257914 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 377108 Color: 0
Size: 317669 Color: 1
Size: 305224 Color: 1

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 377121 Color: 0
Size: 368323 Color: 0
Size: 254557 Color: 1

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 376790 Color: 1
Size: 341682 Color: 0
Size: 281529 Color: 0

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 377156 Color: 0
Size: 314276 Color: 1
Size: 308569 Color: 0

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 377166 Color: 0
Size: 365562 Color: 0
Size: 257273 Color: 1

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 377175 Color: 0
Size: 363199 Color: 1
Size: 259627 Color: 1

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 377199 Color: 0
Size: 364418 Color: 0
Size: 258384 Color: 1

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 376891 Color: 1
Size: 321957 Color: 0
Size: 301153 Color: 1

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 377230 Color: 0
Size: 372057 Color: 1
Size: 250714 Color: 1

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 377284 Color: 0
Size: 366508 Color: 1
Size: 256209 Color: 0

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 376989 Color: 1
Size: 336937 Color: 1
Size: 286075 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 377361 Color: 0
Size: 318309 Color: 1
Size: 304331 Color: 0

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 377065 Color: 1
Size: 336526 Color: 0
Size: 286410 Color: 1

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 377376 Color: 0
Size: 368907 Color: 1
Size: 253718 Color: 0

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 377403 Color: 0
Size: 356810 Color: 0
Size: 265788 Color: 1

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 377189 Color: 1
Size: 322092 Color: 1
Size: 300720 Color: 0

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 377295 Color: 1
Size: 353423 Color: 0
Size: 269283 Color: 1

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 377471 Color: 1
Size: 357455 Color: 0
Size: 265075 Color: 1

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 377516 Color: 0
Size: 370238 Color: 0
Size: 252247 Color: 1

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 377579 Color: 1
Size: 328276 Color: 0
Size: 294146 Color: 1

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 377609 Color: 1
Size: 356994 Color: 0
Size: 265398 Color: 1

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 377604 Color: 0
Size: 333019 Color: 0
Size: 289378 Color: 1

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 377723 Color: 1
Size: 334862 Color: 0
Size: 287416 Color: 1

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 377880 Color: 1
Size: 312790 Color: 1
Size: 309331 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 377837 Color: 0
Size: 326245 Color: 1
Size: 295919 Color: 0

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 377994 Color: 1
Size: 327857 Color: 0
Size: 294150 Color: 1

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 378012 Color: 1
Size: 341588 Color: 0
Size: 280401 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 378115 Color: 1
Size: 361079 Color: 0
Size: 260807 Color: 1

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 378135 Color: 1
Size: 314984 Color: 0
Size: 306882 Color: 0

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 378236 Color: 1
Size: 323876 Color: 0
Size: 297889 Color: 1

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 378277 Color: 1
Size: 323462 Color: 1
Size: 298262 Color: 0

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 378313 Color: 1
Size: 354053 Color: 0
Size: 267635 Color: 0

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 378419 Color: 0
Size: 317628 Color: 0
Size: 303954 Color: 1

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 378420 Color: 1
Size: 321100 Color: 0
Size: 300481 Color: 1

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 378519 Color: 0
Size: 353398 Color: 1
Size: 268084 Color: 0

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 378434 Color: 1
Size: 369127 Color: 0
Size: 252440 Color: 1

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 378534 Color: 0
Size: 367397 Color: 1
Size: 254070 Color: 0

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 378441 Color: 1
Size: 357411 Color: 1
Size: 264149 Color: 0

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 378638 Color: 0
Size: 339901 Color: 1
Size: 281462 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 378640 Color: 0
Size: 357683 Color: 1
Size: 263678 Color: 1

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 378691 Color: 0
Size: 344016 Color: 1
Size: 277294 Color: 1

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 378754 Color: 0
Size: 315194 Color: 0
Size: 306053 Color: 1

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 378579 Color: 1
Size: 352133 Color: 0
Size: 269289 Color: 1

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 378796 Color: 0
Size: 365102 Color: 1
Size: 256103 Color: 0

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 378642 Color: 1
Size: 330542 Color: 1
Size: 290817 Color: 0

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 378864 Color: 0
Size: 365400 Color: 1
Size: 255737 Color: 0

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 378882 Color: 0
Size: 333461 Color: 1
Size: 287658 Color: 1

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 378797 Color: 1
Size: 353856 Color: 1
Size: 267348 Color: 0

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 378922 Color: 0
Size: 314660 Color: 1
Size: 306419 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 378848 Color: 1
Size: 336643 Color: 0
Size: 284510 Color: 1

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 379007 Color: 0
Size: 315298 Color: 1
Size: 305696 Color: 0

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 378945 Color: 1
Size: 365437 Color: 0
Size: 255619 Color: 1

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 379035 Color: 0
Size: 368629 Color: 1
Size: 252337 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 378984 Color: 1
Size: 324939 Color: 1
Size: 296078 Color: 0

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 379039 Color: 0
Size: 356981 Color: 0
Size: 263981 Color: 1

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 379084 Color: 1
Size: 331445 Color: 0
Size: 289472 Color: 1

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 379110 Color: 0
Size: 327757 Color: 0
Size: 293134 Color: 1

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 379087 Color: 1
Size: 354700 Color: 0
Size: 266214 Color: 1

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 379137 Color: 0
Size: 339131 Color: 1
Size: 281733 Color: 0

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 379089 Color: 1
Size: 332814 Color: 0
Size: 288098 Color: 1

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 379155 Color: 0
Size: 353879 Color: 1
Size: 266967 Color: 0

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 379095 Color: 1
Size: 345692 Color: 0
Size: 275214 Color: 1

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 379416 Color: 1
Size: 353076 Color: 1
Size: 267509 Color: 0

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 379208 Color: 0
Size: 363473 Color: 1
Size: 257320 Color: 0

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 379453 Color: 1
Size: 353673 Color: 0
Size: 266875 Color: 1

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 379465 Color: 1
Size: 324296 Color: 0
Size: 296240 Color: 1

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 379656 Color: 1
Size: 312634 Color: 0
Size: 307711 Color: 0

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 379697 Color: 1
Size: 340518 Color: 1
Size: 279786 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 379542 Color: 0
Size: 341880 Color: 1
Size: 278579 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 379721 Color: 1
Size: 342614 Color: 0
Size: 277666 Color: 1

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 379646 Color: 0
Size: 315511 Color: 0
Size: 304844 Color: 1

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 379734 Color: 1
Size: 365256 Color: 1
Size: 255011 Color: 0

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 379650 Color: 0
Size: 360574 Color: 0
Size: 259777 Color: 1

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 379741 Color: 1
Size: 329230 Color: 0
Size: 291030 Color: 1

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 379965 Color: 1
Size: 317131 Color: 0
Size: 302905 Color: 0

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 380266 Color: 1
Size: 360656 Color: 0
Size: 259079 Color: 1

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 380026 Color: 0
Size: 355126 Color: 0
Size: 264849 Color: 1

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 380276 Color: 1
Size: 343110 Color: 1
Size: 276615 Color: 0

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 380072 Color: 0
Size: 336353 Color: 0
Size: 283576 Color: 1

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 380116 Color: 0
Size: 311748 Color: 1
Size: 308137 Color: 0

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 380462 Color: 1
Size: 338918 Color: 0
Size: 280621 Color: 1

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 380268 Color: 0
Size: 316949 Color: 0
Size: 302784 Color: 1

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 380477 Color: 1
Size: 353907 Color: 0
Size: 265617 Color: 1

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 380359 Color: 0
Size: 320944 Color: 1
Size: 298698 Color: 0

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 380496 Color: 1
Size: 356235 Color: 1
Size: 263270 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 380377 Color: 0
Size: 328170 Color: 1
Size: 291454 Color: 0

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 380534 Color: 1
Size: 341934 Color: 0
Size: 277533 Color: 1

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 380484 Color: 0
Size: 314719 Color: 1
Size: 304798 Color: 0

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 380712 Color: 1
Size: 343662 Color: 0
Size: 275627 Color: 1

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 380738 Color: 1
Size: 355327 Color: 0
Size: 263936 Color: 1

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 380746 Color: 1
Size: 310556 Color: 0
Size: 308699 Color: 1

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 380720 Color: 0
Size: 369037 Color: 1
Size: 250244 Color: 0

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 380761 Color: 1
Size: 351353 Color: 1
Size: 267887 Color: 0

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 380802 Color: 1
Size: 331757 Color: 0
Size: 287442 Color: 1

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 380785 Color: 0
Size: 318323 Color: 1
Size: 300893 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 380979 Color: 1
Size: 341867 Color: 1
Size: 277155 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 380996 Color: 0
Size: 364839 Color: 0
Size: 254166 Color: 1

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 381193 Color: 0
Size: 315840 Color: 1
Size: 302968 Color: 1

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 381231 Color: 0
Size: 325638 Color: 0
Size: 293132 Color: 1

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 381339 Color: 0
Size: 318834 Color: 1
Size: 299828 Color: 1

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 381046 Color: 1
Size: 325658 Color: 1
Size: 293297 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 381371 Color: 0
Size: 335116 Color: 0
Size: 283514 Color: 1

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 381204 Color: 1
Size: 351691 Color: 1
Size: 267106 Color: 0

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 381506 Color: 0
Size: 314930 Color: 0
Size: 303565 Color: 1

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 381219 Color: 1
Size: 356313 Color: 0
Size: 262469 Color: 1

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 381530 Color: 0
Size: 350179 Color: 0
Size: 268292 Color: 1

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 381578 Color: 0
Size: 340198 Color: 0
Size: 278225 Color: 1

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 381595 Color: 0
Size: 366491 Color: 0
Size: 251915 Color: 1

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 381413 Color: 1
Size: 323441 Color: 0
Size: 295147 Color: 1

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 381616 Color: 0
Size: 332175 Color: 1
Size: 286210 Color: 0

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 381449 Color: 1
Size: 353239 Color: 0
Size: 265313 Color: 1

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 381675 Color: 0
Size: 330740 Color: 0
Size: 287586 Color: 1

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 381786 Color: 1
Size: 340574 Color: 0
Size: 277641 Color: 1

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 381815 Color: 1
Size: 318423 Color: 0
Size: 299763 Color: 0

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 381819 Color: 1
Size: 359164 Color: 0
Size: 259018 Color: 1

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 381829 Color: 0
Size: 312127 Color: 0
Size: 306045 Color: 1

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 381949 Color: 1
Size: 349297 Color: 1
Size: 268755 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 382093 Color: 0
Size: 347728 Color: 1
Size: 270180 Color: 0

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 382102 Color: 0
Size: 355861 Color: 0
Size: 262038 Color: 1

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 381999 Color: 1
Size: 356211 Color: 0
Size: 261791 Color: 1

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 382173 Color: 0
Size: 338807 Color: 1
Size: 279021 Color: 0

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 382055 Color: 1
Size: 359092 Color: 1
Size: 258854 Color: 0

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 382184 Color: 0
Size: 362999 Color: 1
Size: 254818 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 382133 Color: 1
Size: 313037 Color: 0
Size: 304831 Color: 1

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 382218 Color: 0
Size: 361291 Color: 0
Size: 256492 Color: 1

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 382133 Color: 1
Size: 345331 Color: 1
Size: 272537 Color: 0

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 382235 Color: 0
Size: 354749 Color: 0
Size: 263017 Color: 1

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 382212 Color: 1
Size: 321417 Color: 0
Size: 296372 Color: 1

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 382262 Color: 0
Size: 355198 Color: 0
Size: 262541 Color: 1

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 382365 Color: 1
Size: 324126 Color: 0
Size: 293510 Color: 1

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 382370 Color: 0
Size: 351241 Color: 1
Size: 266390 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 382370 Color: 1
Size: 310166 Color: 1
Size: 307465 Color: 0

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 382376 Color: 0
Size: 360165 Color: 1
Size: 257460 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 382382 Color: 1
Size: 346342 Color: 1
Size: 271277 Color: 0

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 382404 Color: 0
Size: 311324 Color: 0
Size: 306273 Color: 1

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 382529 Color: 1
Size: 330257 Color: 1
Size: 287215 Color: 0

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 382453 Color: 0
Size: 325547 Color: 0
Size: 292001 Color: 1

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 382514 Color: 0
Size: 352917 Color: 0
Size: 264570 Color: 1

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 382543 Color: 1
Size: 321811 Color: 0
Size: 295647 Color: 1

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 382520 Color: 0
Size: 314971 Color: 1
Size: 302510 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 382591 Color: 1
Size: 332088 Color: 0
Size: 285322 Color: 1

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 382614 Color: 0
Size: 318395 Color: 0
Size: 298992 Color: 1

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 382634 Color: 1
Size: 326168 Color: 1
Size: 291199 Color: 0

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 382972 Color: 0
Size: 319252 Color: 0
Size: 297777 Color: 1

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 382887 Color: 1
Size: 332850 Color: 0
Size: 284264 Color: 1

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 383043 Color: 0
Size: 328716 Color: 0
Size: 288242 Color: 1

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 383119 Color: 0
Size: 324784 Color: 1
Size: 292098 Color: 1

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 383128 Color: 0
Size: 352116 Color: 0
Size: 264757 Color: 1

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 383029 Color: 1
Size: 317842 Color: 0
Size: 299130 Color: 1

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 383075 Color: 1
Size: 336682 Color: 1
Size: 280244 Color: 0

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 383157 Color: 0
Size: 318031 Color: 0
Size: 298813 Color: 1

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 383137 Color: 1
Size: 335659 Color: 0
Size: 281205 Color: 1

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 383205 Color: 0
Size: 334120 Color: 1
Size: 282676 Color: 0

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 383147 Color: 1
Size: 329607 Color: 0
Size: 287247 Color: 1

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 383232 Color: 0
Size: 360299 Color: 1
Size: 256470 Color: 0

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 383199 Color: 1
Size: 340845 Color: 0
Size: 275957 Color: 1

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 383266 Color: 0
Size: 319781 Color: 1
Size: 296954 Color: 0

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 383310 Color: 1
Size: 330108 Color: 1
Size: 286583 Color: 0

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 383315 Color: 0
Size: 308572 Color: 0
Size: 308114 Color: 1

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 383340 Color: 1
Size: 341820 Color: 1
Size: 274841 Color: 0

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 383324 Color: 0
Size: 329931 Color: 1
Size: 286746 Color: 0

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 383428 Color: 1
Size: 358942 Color: 1
Size: 257631 Color: 0

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 383587 Color: 1
Size: 313604 Color: 1
Size: 302810 Color: 0

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 383431 Color: 0
Size: 315991 Color: 1
Size: 300579 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 383627 Color: 1
Size: 309440 Color: 0
Size: 306934 Color: 1

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 383664 Color: 1
Size: 360388 Color: 0
Size: 255949 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 383673 Color: 1
Size: 313952 Color: 1
Size: 302376 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 383535 Color: 0
Size: 327470 Color: 1
Size: 288996 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 383673 Color: 1
Size: 353031 Color: 0
Size: 263297 Color: 1

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 383539 Color: 0
Size: 359736 Color: 1
Size: 256726 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 383708 Color: 1
Size: 311770 Color: 0
Size: 304523 Color: 1

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 383763 Color: 0
Size: 308738 Color: 1
Size: 307500 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 383733 Color: 1
Size: 362785 Color: 1
Size: 253483 Color: 0

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 383771 Color: 0
Size: 336882 Color: 1
Size: 279348 Color: 0

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 383767 Color: 1
Size: 325854 Color: 1
Size: 290380 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 383775 Color: 0
Size: 350408 Color: 0
Size: 265818 Color: 1

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 383932 Color: 1
Size: 365281 Color: 1
Size: 250788 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 383815 Color: 0
Size: 318436 Color: 1
Size: 297750 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 383817 Color: 0
Size: 349984 Color: 0
Size: 266200 Color: 1

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 383883 Color: 0
Size: 354035 Color: 0
Size: 262083 Color: 1

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 384058 Color: 1
Size: 359042 Color: 0
Size: 256901 Color: 1

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 384138 Color: 1
Size: 360637 Color: 0
Size: 255226 Color: 0

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 384220 Color: 1
Size: 353727 Color: 0
Size: 262054 Color: 1

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 384260 Color: 1
Size: 328570 Color: 1
Size: 287171 Color: 0

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 384093 Color: 0
Size: 348552 Color: 1
Size: 267356 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 384282 Color: 1
Size: 338525 Color: 0
Size: 277194 Color: 1

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 384367 Color: 1
Size: 355372 Color: 1
Size: 260262 Color: 0

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 384232 Color: 0
Size: 316619 Color: 1
Size: 299150 Color: 0

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 384390 Color: 1
Size: 346562 Color: 0
Size: 269049 Color: 1

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 384457 Color: 1
Size: 340348 Color: 0
Size: 275196 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 384473 Color: 1
Size: 347356 Color: 0
Size: 268172 Color: 1

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 384402 Color: 0
Size: 339292 Color: 1
Size: 276307 Color: 0

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 384630 Color: 1
Size: 314741 Color: 0
Size: 300630 Color: 1

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 384403 Color: 0
Size: 315892 Color: 1
Size: 299706 Color: 0

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 384634 Color: 1
Size: 342162 Color: 1
Size: 273205 Color: 0

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 384566 Color: 0
Size: 312719 Color: 0
Size: 302716 Color: 1

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 384697 Color: 1
Size: 349213 Color: 0
Size: 266091 Color: 1

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 384620 Color: 0
Size: 355857 Color: 0
Size: 259524 Color: 1

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 384764 Color: 1
Size: 349789 Color: 1
Size: 265448 Color: 0

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 384662 Color: 0
Size: 349396 Color: 1
Size: 265943 Color: 0

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 384769 Color: 1
Size: 345715 Color: 0
Size: 269517 Color: 1

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 384671 Color: 0
Size: 318019 Color: 1
Size: 297311 Color: 0

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 384823 Color: 0
Size: 316270 Color: 1
Size: 298908 Color: 1

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 384954 Color: 0
Size: 356729 Color: 0
Size: 258318 Color: 1

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 385009 Color: 1
Size: 341201 Color: 0
Size: 273791 Color: 1

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 346813 Color: 1
Size: 343365 Color: 0
Size: 309823 Color: 1

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 385064 Color: 1
Size: 323536 Color: 1
Size: 291401 Color: 0

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 385116 Color: 0
Size: 312008 Color: 1
Size: 302877 Color: 0

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 385118 Color: 0
Size: 352436 Color: 1
Size: 262447 Color: 1

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 385174 Color: 0
Size: 349056 Color: 1
Size: 265771 Color: 1

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 385272 Color: 0
Size: 360230 Color: 1
Size: 254499 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 385395 Color: 1
Size: 322524 Color: 1
Size: 292082 Color: 0

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 385329 Color: 0
Size: 364359 Color: 0
Size: 250313 Color: 1

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 385503 Color: 0
Size: 312984 Color: 1
Size: 301514 Color: 1

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 385429 Color: 1
Size: 354411 Color: 0
Size: 260161 Color: 1

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 385588 Color: 1
Size: 325642 Color: 0
Size: 288771 Color: 1

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 385713 Color: 0
Size: 308040 Color: 0
Size: 306248 Color: 1

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 385655 Color: 1
Size: 335149 Color: 0
Size: 279197 Color: 1

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 385777 Color: 1
Size: 314630 Color: 0
Size: 299594 Color: 0

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 385814 Color: 1
Size: 355079 Color: 0
Size: 259108 Color: 1

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 385980 Color: 0
Size: 361666 Color: 1
Size: 252355 Color: 0

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 385823 Color: 1
Size: 339404 Color: 1
Size: 274774 Color: 0

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 385987 Color: 0
Size: 332238 Color: 0
Size: 281776 Color: 1

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 385843 Color: 1
Size: 318073 Color: 0
Size: 296085 Color: 1

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 386019 Color: 0
Size: 350197 Color: 1
Size: 263785 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 385937 Color: 1
Size: 357729 Color: 1
Size: 256335 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 386103 Color: 0
Size: 308612 Color: 1
Size: 305286 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 386116 Color: 0
Size: 334882 Color: 1
Size: 279003 Color: 0

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 386055 Color: 1
Size: 325253 Color: 1
Size: 288693 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 386144 Color: 0
Size: 338645 Color: 0
Size: 275212 Color: 1

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 386404 Color: 0
Size: 354361 Color: 1
Size: 259236 Color: 1

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 386406 Color: 0
Size: 314619 Color: 1
Size: 298976 Color: 0

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 386215 Color: 1
Size: 325373 Color: 1
Size: 288413 Color: 0

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 386480 Color: 0
Size: 335533 Color: 1
Size: 277988 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 386315 Color: 1
Size: 322817 Color: 0
Size: 290869 Color: 1

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 386498 Color: 0
Size: 325806 Color: 0
Size: 287697 Color: 1

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 386350 Color: 1
Size: 307835 Color: 1
Size: 305816 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 386528 Color: 0
Size: 345875 Color: 0
Size: 267598 Color: 1

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 386384 Color: 1
Size: 356099 Color: 0
Size: 257518 Color: 1

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 386602 Color: 0
Size: 332928 Color: 1
Size: 280471 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 386465 Color: 1
Size: 315568 Color: 1
Size: 297968 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 386631 Color: 0
Size: 309276 Color: 1
Size: 304094 Color: 1

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 386545 Color: 1
Size: 351685 Color: 1
Size: 261771 Color: 0

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 386711 Color: 1
Size: 356570 Color: 0
Size: 256720 Color: 1

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 386766 Color: 0
Size: 315415 Color: 0
Size: 297820 Color: 1

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 386748 Color: 1
Size: 310788 Color: 1
Size: 302465 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 386830 Color: 0
Size: 330147 Color: 0
Size: 283024 Color: 1

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 386777 Color: 1
Size: 313858 Color: 1
Size: 299366 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 387014 Color: 0
Size: 346593 Color: 1
Size: 266394 Color: 0

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 386901 Color: 1
Size: 335337 Color: 0
Size: 277763 Color: 1

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 387043 Color: 0
Size: 340176 Color: 0
Size: 272782 Color: 1

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 387006 Color: 1
Size: 344027 Color: 1
Size: 268968 Color: 0

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 387270 Color: 0
Size: 347040 Color: 0
Size: 265691 Color: 1

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 387111 Color: 1
Size: 351457 Color: 0
Size: 261433 Color: 1

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 387417 Color: 0
Size: 311694 Color: 0
Size: 300890 Color: 1

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 387331 Color: 1
Size: 330930 Color: 0
Size: 281740 Color: 1

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 387459 Color: 0
Size: 362320 Color: 0
Size: 250222 Color: 1

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 387540 Color: 1
Size: 315752 Color: 0
Size: 296709 Color: 1

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 387610 Color: 0
Size: 337882 Color: 0
Size: 274509 Color: 1

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 387665 Color: 1
Size: 353441 Color: 0
Size: 258895 Color: 1

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 387638 Color: 0
Size: 358958 Color: 1
Size: 253405 Color: 0

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 387677 Color: 1
Size: 321790 Color: 0
Size: 290534 Color: 1

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 387741 Color: 0
Size: 315361 Color: 1
Size: 296899 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 387819 Color: 0
Size: 352968 Color: 0
Size: 259214 Color: 1

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 387831 Color: 0
Size: 319958 Color: 1
Size: 292212 Color: 0

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 387942 Color: 1
Size: 323708 Color: 0
Size: 288351 Color: 1

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 387963 Color: 1
Size: 321843 Color: 0
Size: 290195 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 388044 Color: 1
Size: 310799 Color: 0
Size: 301158 Color: 1

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 388023 Color: 0
Size: 311412 Color: 0
Size: 300566 Color: 1

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 388173 Color: 1
Size: 336677 Color: 0
Size: 275151 Color: 1

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 388220 Color: 0
Size: 358437 Color: 1
Size: 253344 Color: 0

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 388196 Color: 1
Size: 333074 Color: 0
Size: 278731 Color: 1

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 388264 Color: 1
Size: 309236 Color: 0
Size: 302501 Color: 1

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 388385 Color: 1
Size: 335354 Color: 0
Size: 276262 Color: 0

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 388416 Color: 1
Size: 327199 Color: 0
Size: 284386 Color: 1

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 388437 Color: 1
Size: 343266 Color: 1
Size: 268298 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 388304 Color: 0
Size: 352187 Color: 1
Size: 259510 Color: 0

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 388463 Color: 1
Size: 350666 Color: 0
Size: 260872 Color: 1

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 388344 Color: 0
Size: 345005 Color: 0
Size: 266652 Color: 1

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 388487 Color: 1
Size: 342009 Color: 1
Size: 269505 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 388505 Color: 0
Size: 338969 Color: 1
Size: 272527 Color: 0

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 388514 Color: 1
Size: 306208 Color: 0
Size: 305279 Color: 1

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 388508 Color: 0
Size: 310573 Color: 1
Size: 300920 Color: 0

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 388538 Color: 0
Size: 313062 Color: 1
Size: 298401 Color: 0

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 388584 Color: 1
Size: 360590 Color: 0
Size: 250827 Color: 1

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 388556 Color: 0
Size: 333986 Color: 0
Size: 277459 Color: 1

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 388604 Color: 1
Size: 349462 Color: 0
Size: 261935 Color: 1

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 388565 Color: 0
Size: 331925 Color: 0
Size: 279511 Color: 1

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 388762 Color: 1
Size: 349477 Color: 0
Size: 261762 Color: 0

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 388830 Color: 1
Size: 316804 Color: 0
Size: 294367 Color: 1

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 388666 Color: 0
Size: 312966 Color: 0
Size: 298369 Color: 1

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 388835 Color: 0
Size: 339733 Color: 0
Size: 271433 Color: 1

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 388902 Color: 1
Size: 351539 Color: 0
Size: 259560 Color: 1

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 388858 Color: 0
Size: 314898 Color: 0
Size: 296245 Color: 1

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 389054 Color: 1
Size: 313410 Color: 0
Size: 297537 Color: 0

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 389135 Color: 1
Size: 320969 Color: 0
Size: 289897 Color: 1

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 388956 Color: 0
Size: 306265 Color: 1
Size: 304780 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 389147 Color: 1
Size: 345144 Color: 1
Size: 265710 Color: 0

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 389158 Color: 1
Size: 311071 Color: 1
Size: 299772 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 389225 Color: 1
Size: 360658 Color: 0
Size: 250118 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 389232 Color: 1
Size: 310630 Color: 1
Size: 300139 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 389266 Color: 1
Size: 318248 Color: 0
Size: 292487 Color: 0

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 389270 Color: 1
Size: 355917 Color: 0
Size: 254814 Color: 1

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 389273 Color: 1
Size: 329592 Color: 0
Size: 281136 Color: 0

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 389286 Color: 1
Size: 336251 Color: 1
Size: 274464 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 389246 Color: 0
Size: 311887 Color: 0
Size: 298868 Color: 1

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 389418 Color: 1
Size: 332442 Color: 0
Size: 278141 Color: 1

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 389315 Color: 0
Size: 334056 Color: 0
Size: 276630 Color: 1

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 389420 Color: 1
Size: 342358 Color: 0
Size: 268223 Color: 1

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 389319 Color: 0
Size: 316472 Color: 0
Size: 294210 Color: 1

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 389559 Color: 1
Size: 344609 Color: 1
Size: 265833 Color: 0

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 389497 Color: 0
Size: 334131 Color: 1
Size: 276373 Color: 0

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 389588 Color: 0
Size: 345237 Color: 0
Size: 265176 Color: 1

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 389905 Color: 0
Size: 342626 Color: 1
Size: 267470 Color: 0

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 389901 Color: 1
Size: 354780 Color: 0
Size: 255320 Color: 1

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 390056 Color: 1
Size: 306746 Color: 1
Size: 303199 Color: 0

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 390095 Color: 0
Size: 322638 Color: 0
Size: 287268 Color: 1

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 390177 Color: 1
Size: 329220 Color: 0
Size: 280604 Color: 1

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 390122 Color: 0
Size: 316707 Color: 0
Size: 293172 Color: 1

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 390178 Color: 1
Size: 357413 Color: 0
Size: 252410 Color: 1

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 390144 Color: 0
Size: 309636 Color: 0
Size: 300221 Color: 1

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 390195 Color: 1
Size: 344219 Color: 0
Size: 265587 Color: 1

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 390268 Color: 0
Size: 352187 Color: 1
Size: 257546 Color: 0

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 390347 Color: 0
Size: 316824 Color: 1
Size: 292830 Color: 1

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 390401 Color: 0
Size: 308106 Color: 1
Size: 301494 Color: 1

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 390489 Color: 0
Size: 348090 Color: 1
Size: 261422 Color: 0

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 390497 Color: 0
Size: 347269 Color: 1
Size: 262235 Color: 1

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 390409 Color: 1
Size: 308673 Color: 0
Size: 300919 Color: 0

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 390528 Color: 1
Size: 348351 Color: 0
Size: 261122 Color: 1

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 390580 Color: 1
Size: 317938 Color: 0
Size: 291483 Color: 1

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 390612 Color: 1
Size: 321245 Color: 1
Size: 288144 Color: 0

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 390855 Color: 0
Size: 350323 Color: 1
Size: 258823 Color: 1

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 390888 Color: 0
Size: 347027 Color: 0
Size: 262086 Color: 1

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 390910 Color: 0
Size: 342261 Color: 0
Size: 266830 Color: 1

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 390817 Color: 1
Size: 312601 Color: 0
Size: 296583 Color: 1

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 390915 Color: 0
Size: 317168 Color: 1
Size: 291918 Color: 0

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 390846 Color: 1
Size: 349133 Color: 1
Size: 260022 Color: 0

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 390969 Color: 1
Size: 305333 Color: 0
Size: 303699 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 391005 Color: 1
Size: 318560 Color: 0
Size: 290436 Color: 1

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 391037 Color: 1
Size: 328674 Color: 0
Size: 280290 Color: 0

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 391210 Color: 0
Size: 357384 Color: 1
Size: 251407 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 391143 Color: 1
Size: 306258 Color: 0
Size: 302600 Color: 1

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 391229 Color: 0
Size: 346729 Color: 1
Size: 262043 Color: 0

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 391163 Color: 1
Size: 346612 Color: 0
Size: 262226 Color: 1

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 391282 Color: 0
Size: 325624 Color: 0
Size: 283095 Color: 1

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 391312 Color: 0
Size: 327895 Color: 0
Size: 280794 Color: 1

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 391427 Color: 1
Size: 351883 Color: 0
Size: 256691 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 391446 Color: 1
Size: 305823 Color: 1
Size: 302732 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 391493 Color: 1
Size: 309285 Color: 0
Size: 299223 Color: 0

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 391468 Color: 0
Size: 313611 Color: 1
Size: 294922 Color: 0

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 391629 Color: 1
Size: 326229 Color: 1
Size: 282143 Color: 0

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 391673 Color: 1
Size: 321601 Color: 0
Size: 286727 Color: 0

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 391674 Color: 1
Size: 347563 Color: 1
Size: 260764 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 391724 Color: 0
Size: 306725 Color: 1
Size: 301552 Color: 0

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 391723 Color: 1
Size: 323359 Color: 0
Size: 284919 Color: 1

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 391749 Color: 0
Size: 309936 Color: 1
Size: 298316 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 391739 Color: 1
Size: 307556 Color: 0
Size: 300706 Color: 1

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 391847 Color: 0
Size: 356993 Color: 1
Size: 251161 Color: 0

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 391942 Color: 1
Size: 312355 Color: 1
Size: 295704 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 391934 Color: 0
Size: 340431 Color: 1
Size: 267636 Color: 0

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 391980 Color: 1
Size: 354588 Color: 1
Size: 253433 Color: 0

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 391991 Color: 0
Size: 354819 Color: 1
Size: 253191 Color: 0

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 392006 Color: 1
Size: 343704 Color: 0
Size: 264291 Color: 1

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 392065 Color: 0
Size: 345877 Color: 1
Size: 262059 Color: 0

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 392225 Color: 0
Size: 312014 Color: 1
Size: 295762 Color: 1

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 392243 Color: 0
Size: 343049 Color: 1
Size: 264709 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 392257 Color: 0
Size: 355922 Color: 1
Size: 251822 Color: 1

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 392295 Color: 0
Size: 339891 Color: 0
Size: 267815 Color: 1

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 392286 Color: 1
Size: 335649 Color: 0
Size: 272066 Color: 1

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 392327 Color: 0
Size: 321969 Color: 0
Size: 285705 Color: 1

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 392402 Color: 0
Size: 341529 Color: 0
Size: 266070 Color: 1

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 392406 Color: 0
Size: 318452 Color: 0
Size: 289143 Color: 1

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 392552 Color: 1
Size: 339865 Color: 1
Size: 267584 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 392606 Color: 0
Size: 328292 Color: 1
Size: 279103 Color: 0

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 392749 Color: 1
Size: 334650 Color: 1
Size: 272602 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 392721 Color: 0
Size: 312959 Color: 0
Size: 294321 Color: 1

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 392784 Color: 1
Size: 307056 Color: 1
Size: 300161 Color: 0

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 392861 Color: 1
Size: 317746 Color: 0
Size: 289394 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 392850 Color: 0
Size: 311932 Color: 0
Size: 295219 Color: 1

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 392895 Color: 1
Size: 330834 Color: 1
Size: 276272 Color: 0

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 392899 Color: 1
Size: 333055 Color: 1
Size: 274047 Color: 0

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 392995 Color: 1
Size: 315684 Color: 1
Size: 291322 Color: 0

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 393107 Color: 0
Size: 344034 Color: 1
Size: 262860 Color: 0

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 393065 Color: 1
Size: 346323 Color: 0
Size: 260613 Color: 1

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 393171 Color: 1
Size: 337483 Color: 0
Size: 269347 Color: 1

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 393306 Color: 1
Size: 322583 Color: 0
Size: 284112 Color: 1

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 393230 Color: 0
Size: 321563 Color: 1
Size: 285208 Color: 0

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 393345 Color: 1
Size: 309966 Color: 0
Size: 296690 Color: 1

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 393397 Color: 1
Size: 342101 Color: 0
Size: 264503 Color: 0

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 393428 Color: 1
Size: 306426 Color: 0
Size: 300147 Color: 1

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 393392 Color: 0
Size: 352856 Color: 1
Size: 253753 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 393439 Color: 1
Size: 323650 Color: 0
Size: 282912 Color: 1

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 393449 Color: 0
Size: 314455 Color: 0
Size: 292097 Color: 1

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 393544 Color: 1
Size: 331829 Color: 0
Size: 274628 Color: 1

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 393469 Color: 0
Size: 355238 Color: 0
Size: 251294 Color: 1

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 393572 Color: 1
Size: 332822 Color: 0
Size: 273607 Color: 1

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 393606 Color: 1
Size: 338833 Color: 1
Size: 267562 Color: 0

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 393579 Color: 0
Size: 324152 Color: 1
Size: 282270 Color: 0

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 393755 Color: 1
Size: 353091 Color: 1
Size: 253155 Color: 0

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 393623 Color: 0
Size: 316476 Color: 0
Size: 289902 Color: 1

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 393782 Color: 1
Size: 342642 Color: 1
Size: 263577 Color: 0

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 393695 Color: 0
Size: 346718 Color: 1
Size: 259588 Color: 0

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 393810 Color: 1
Size: 340102 Color: 0
Size: 266089 Color: 1

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 393730 Color: 0
Size: 322798 Color: 0
Size: 283473 Color: 1

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 393956 Color: 1
Size: 349719 Color: 0
Size: 256326 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 393982 Color: 1
Size: 315922 Color: 1
Size: 290097 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 394006 Color: 1
Size: 345272 Color: 0
Size: 260723 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 394048 Color: 0
Size: 343870 Color: 0
Size: 262083 Color: 1

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 394050 Color: 0
Size: 334297 Color: 0
Size: 271654 Color: 1

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 394213 Color: 1
Size: 305283 Color: 0
Size: 300505 Color: 1

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 394079 Color: 0
Size: 326897 Color: 1
Size: 279025 Color: 0

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 394232 Color: 1
Size: 339782 Color: 1
Size: 265987 Color: 0

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 394203 Color: 0
Size: 352330 Color: 0
Size: 253468 Color: 1

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 394357 Color: 1
Size: 336757 Color: 1
Size: 268887 Color: 0

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 394351 Color: 0
Size: 321976 Color: 0
Size: 283674 Color: 1

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 394385 Color: 1
Size: 331463 Color: 0
Size: 274153 Color: 1

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 394421 Color: 0
Size: 339887 Color: 1
Size: 265693 Color: 1

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 394450 Color: 0
Size: 312867 Color: 1
Size: 292684 Color: 0

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 394477 Color: 1
Size: 333330 Color: 1
Size: 272194 Color: 0

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 394450 Color: 0
Size: 304722 Color: 1
Size: 300829 Color: 0

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 394478 Color: 0
Size: 312367 Color: 0
Size: 293156 Color: 1

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 394558 Color: 1
Size: 332739 Color: 0
Size: 272704 Color: 1

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 394508 Color: 0
Size: 327616 Color: 0
Size: 277877 Color: 1

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 394664 Color: 0
Size: 333879 Color: 1
Size: 271458 Color: 1

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 394729 Color: 1
Size: 310302 Color: 0
Size: 294970 Color: 1

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 394755 Color: 0
Size: 342285 Color: 1
Size: 262961 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 394767 Color: 0
Size: 315093 Color: 1
Size: 290141 Color: 1

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 394877 Color: 0
Size: 310001 Color: 1
Size: 295123 Color: 1

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 394882 Color: 0
Size: 305838 Color: 1
Size: 299281 Color: 0

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 394934 Color: 0
Size: 332345 Color: 1
Size: 272722 Color: 1

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 394888 Color: 1
Size: 327851 Color: 1
Size: 277262 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 394978 Color: 1
Size: 321506 Color: 0
Size: 283517 Color: 0

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 395431 Color: 0
Size: 321218 Color: 1
Size: 283352 Color: 0

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 395348 Color: 1
Size: 352533 Color: 1
Size: 252120 Color: 0

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 395459 Color: 0
Size: 341376 Color: 1
Size: 263166 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 395453 Color: 1
Size: 323915 Color: 1
Size: 280633 Color: 0

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 395533 Color: 0
Size: 314746 Color: 1
Size: 289722 Color: 0

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 395496 Color: 1
Size: 339008 Color: 1
Size: 265497 Color: 0

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 395625 Color: 0
Size: 353643 Color: 0
Size: 250733 Color: 1

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 395535 Color: 1
Size: 340224 Color: 1
Size: 264242 Color: 0

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 395731 Color: 0
Size: 311425 Color: 0
Size: 292845 Color: 1

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 395667 Color: 1
Size: 323811 Color: 1
Size: 280523 Color: 0

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 395804 Color: 0
Size: 319579 Color: 1
Size: 284618 Color: 0

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 395820 Color: 0
Size: 306608 Color: 0
Size: 297573 Color: 1

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 395837 Color: 1
Size: 310028 Color: 0
Size: 294136 Color: 1

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 395918 Color: 1
Size: 334306 Color: 0
Size: 269777 Color: 1

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 395958 Color: 1
Size: 310623 Color: 0
Size: 293420 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 395994 Color: 1
Size: 302647 Color: 0
Size: 301360 Color: 1

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 396015 Color: 0
Size: 302133 Color: 1
Size: 301853 Color: 0

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 396001 Color: 1
Size: 304130 Color: 1
Size: 299870 Color: 0

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 396023 Color: 0
Size: 350434 Color: 0
Size: 253544 Color: 1

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 396014 Color: 1
Size: 326393 Color: 1
Size: 277594 Color: 0

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 396137 Color: 0
Size: 305964 Color: 1
Size: 297900 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 396190 Color: 0
Size: 309972 Color: 1
Size: 293839 Color: 0

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 396182 Color: 1
Size: 352693 Color: 0
Size: 251126 Color: 1

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 396310 Color: 1
Size: 352507 Color: 0
Size: 251184 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 396514 Color: 1
Size: 344019 Color: 0
Size: 259468 Color: 1

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 396577 Color: 0
Size: 347756 Color: 1
Size: 255668 Color: 0

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 396633 Color: 0
Size: 309219 Color: 0
Size: 294149 Color: 1

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 396602 Color: 1
Size: 319585 Color: 1
Size: 283814 Color: 0

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 396652 Color: 0
Size: 345375 Color: 0
Size: 257974 Color: 1

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 396631 Color: 1
Size: 320993 Color: 0
Size: 282377 Color: 1

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 396728 Color: 1
Size: 339678 Color: 0
Size: 263595 Color: 0

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 396887 Color: 0
Size: 323909 Color: 1
Size: 279205 Color: 1

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 396977 Color: 1
Size: 308605 Color: 0
Size: 294419 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 397022 Color: 1
Size: 321450 Color: 1
Size: 281529 Color: 0

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 396969 Color: 0
Size: 339661 Color: 0
Size: 263371 Color: 1

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 397041 Color: 1
Size: 341778 Color: 1
Size: 261182 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 397010 Color: 0
Size: 328127 Color: 0
Size: 274864 Color: 1

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 397068 Color: 1
Size: 351256 Color: 1
Size: 251677 Color: 0

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 397092 Color: 0
Size: 331630 Color: 1
Size: 271279 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 397168 Color: 1
Size: 338892 Color: 1
Size: 263941 Color: 0

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 397186 Color: 1
Size: 312105 Color: 1
Size: 290710 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 397170 Color: 0
Size: 323422 Color: 1
Size: 279409 Color: 0

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 397303 Color: 1
Size: 302420 Color: 1
Size: 300278 Color: 0

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 397331 Color: 1
Size: 325578 Color: 0
Size: 277092 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 397391 Color: 1
Size: 306686 Color: 0
Size: 295924 Color: 1

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 397418 Color: 1
Size: 313466 Color: 0
Size: 289117 Color: 0

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 397490 Color: 1
Size: 303573 Color: 0
Size: 298938 Color: 1

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 397413 Color: 0
Size: 314850 Color: 1
Size: 287738 Color: 0

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 397533 Color: 1
Size: 332333 Color: 1
Size: 270135 Color: 0

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 397509 Color: 0
Size: 313367 Color: 0
Size: 289125 Color: 1

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 397587 Color: 1
Size: 320655 Color: 1
Size: 281759 Color: 0

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 397527 Color: 0
Size: 343173 Color: 1
Size: 259301 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 397598 Color: 1
Size: 310568 Color: 0
Size: 291835 Color: 1

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 397573 Color: 0
Size: 319648 Color: 0
Size: 282780 Color: 1

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 397736 Color: 0
Size: 309536 Color: 1
Size: 292729 Color: 1

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 397772 Color: 0
Size: 351367 Color: 1
Size: 250862 Color: 0

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 397700 Color: 1
Size: 343213 Color: 1
Size: 259088 Color: 0

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 397797 Color: 0
Size: 351710 Color: 1
Size: 250494 Color: 0

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 397914 Color: 1
Size: 312447 Color: 1
Size: 289640 Color: 0

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 397813 Color: 0
Size: 333381 Color: 1
Size: 268807 Color: 0

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 397847 Color: 0
Size: 318428 Color: 1
Size: 283726 Color: 0

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 398001 Color: 1
Size: 316425 Color: 1
Size: 285575 Color: 0

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 397948 Color: 0
Size: 322144 Color: 0
Size: 279909 Color: 1

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 398282 Color: 1
Size: 302166 Color: 0
Size: 299553 Color: 0

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 398363 Color: 1
Size: 343993 Color: 1
Size: 257645 Color: 0

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 398450 Color: 1
Size: 347152 Color: 0
Size: 254399 Color: 0

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 398452 Color: 1
Size: 303250 Color: 0
Size: 298299 Color: 1

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 398539 Color: 1
Size: 332259 Color: 0
Size: 269203 Color: 1

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 398419 Color: 0
Size: 338515 Color: 0
Size: 263067 Color: 1

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 398591 Color: 1
Size: 322505 Color: 1
Size: 278905 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 398489 Color: 0
Size: 334194 Color: 0
Size: 267318 Color: 1

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 398652 Color: 1
Size: 323873 Color: 1
Size: 277476 Color: 0

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 398598 Color: 0
Size: 316828 Color: 0
Size: 284575 Color: 1

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 398682 Color: 1
Size: 339501 Color: 0
Size: 261818 Color: 1

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 398612 Color: 0
Size: 343681 Color: 0
Size: 257708 Color: 1

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 398766 Color: 1
Size: 330488 Color: 0
Size: 270747 Color: 1

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 398811 Color: 1
Size: 314317 Color: 0
Size: 286873 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 398879 Color: 1
Size: 338047 Color: 1
Size: 263075 Color: 0

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 398885 Color: 0
Size: 328903 Color: 0
Size: 272213 Color: 1

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 398908 Color: 1
Size: 348787 Color: 1
Size: 252306 Color: 0

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 399038 Color: 0
Size: 313682 Color: 0
Size: 287281 Color: 1

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 399020 Color: 1
Size: 315867 Color: 0
Size: 285114 Color: 1

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 399048 Color: 0
Size: 337286 Color: 1
Size: 263667 Color: 0

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 399253 Color: 0
Size: 309911 Color: 1
Size: 290837 Color: 1

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 399281 Color: 0
Size: 327376 Color: 1
Size: 273344 Color: 0

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 399348 Color: 1
Size: 321686 Color: 0
Size: 278967 Color: 1

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 399413 Color: 0
Size: 327335 Color: 0
Size: 273253 Color: 1

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 399409 Color: 1
Size: 349333 Color: 0
Size: 251259 Color: 1

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 399423 Color: 0
Size: 304779 Color: 0
Size: 295799 Color: 1

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 399450 Color: 0
Size: 312209 Color: 0
Size: 288342 Color: 1

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 399461 Color: 0
Size: 313291 Color: 1
Size: 287249 Color: 0

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 399574 Color: 1
Size: 307447 Color: 1
Size: 292980 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 399669 Color: 1
Size: 302520 Color: 0
Size: 297812 Color: 0

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 399711 Color: 1
Size: 305814 Color: 1
Size: 294476 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 399854 Color: 1
Size: 313888 Color: 0
Size: 286259 Color: 0

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 399933 Color: 1
Size: 313433 Color: 0
Size: 286635 Color: 1

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 400008 Color: 0
Size: 347761 Color: 1
Size: 252232 Color: 1

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 400026 Color: 0
Size: 322253 Color: 0
Size: 277722 Color: 1

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 400040 Color: 0
Size: 320893 Color: 0
Size: 279068 Color: 1

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 400056 Color: 1
Size: 316815 Color: 0
Size: 283130 Color: 1

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 400069 Color: 0
Size: 327184 Color: 1
Size: 272748 Color: 1

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 400079 Color: 0
Size: 345712 Color: 0
Size: 254210 Color: 1

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 400141 Color: 0
Size: 329201 Color: 1
Size: 270659 Color: 1

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 400231 Color: 0
Size: 345262 Color: 0
Size: 254508 Color: 1

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 400262 Color: 0
Size: 299898 Color: 1
Size: 299841 Color: 1

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 400275 Color: 0
Size: 323030 Color: 1
Size: 276696 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 400303 Color: 0
Size: 316210 Color: 1
Size: 283488 Color: 1

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 400320 Color: 0
Size: 347453 Color: 1
Size: 252228 Color: 0

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 400533 Color: 0
Size: 315915 Color: 1
Size: 283553 Color: 1

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 400534 Color: 0
Size: 308960 Color: 1
Size: 290507 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 400387 Color: 1
Size: 329702 Color: 0
Size: 269912 Color: 1

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 400610 Color: 0
Size: 316418 Color: 0
Size: 282973 Color: 1

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 400644 Color: 1
Size: 326569 Color: 1
Size: 272788 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 400675 Color: 1
Size: 338705 Color: 0
Size: 260621 Color: 0

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 400696 Color: 1
Size: 310262 Color: 1
Size: 289043 Color: 0

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 400705 Color: 0
Size: 327393 Color: 1
Size: 271903 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 400750 Color: 1
Size: 338111 Color: 0
Size: 261140 Color: 1

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 400808 Color: 0
Size: 326804 Color: 0
Size: 272389 Color: 1

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 400789 Color: 1
Size: 312309 Color: 0
Size: 286903 Color: 1

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 400827 Color: 1
Size: 319116 Color: 0
Size: 280058 Color: 0

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 400831 Color: 1
Size: 336789 Color: 0
Size: 262381 Color: 1

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 400861 Color: 1
Size: 332364 Color: 0
Size: 266776 Color: 0

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 400869 Color: 1
Size: 305989 Color: 0
Size: 293143 Color: 1

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 400882 Color: 0
Size: 320067 Color: 0
Size: 279052 Color: 1

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 400877 Color: 1
Size: 341436 Color: 0
Size: 257688 Color: 1

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 400912 Color: 0
Size: 317276 Color: 0
Size: 281813 Color: 1

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 400954 Color: 1
Size: 328904 Color: 1
Size: 270143 Color: 0

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 401108 Color: 0
Size: 312951 Color: 0
Size: 285942 Color: 1

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 401036 Color: 1
Size: 308379 Color: 1
Size: 290586 Color: 0

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 401145 Color: 0
Size: 306459 Color: 0
Size: 292397 Color: 1

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 401093 Color: 1
Size: 319651 Color: 1
Size: 279257 Color: 0

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 401259 Color: 0
Size: 325420 Color: 1
Size: 273322 Color: 1

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 401270 Color: 0
Size: 308276 Color: 1
Size: 290455 Color: 1

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 401353 Color: 0
Size: 329819 Color: 1
Size: 268829 Color: 0

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 401460 Color: 0
Size: 312473 Color: 1
Size: 286068 Color: 1

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 401463 Color: 0
Size: 331341 Color: 0
Size: 267197 Color: 1

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 401335 Color: 1
Size: 310134 Color: 0
Size: 288532 Color: 1

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 401488 Color: 0
Size: 336000 Color: 1
Size: 262513 Color: 0

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 401417 Color: 1
Size: 302944 Color: 0
Size: 295640 Color: 1

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 401495 Color: 0
Size: 333677 Color: 0
Size: 264829 Color: 1

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 401516 Color: 0
Size: 320760 Color: 1
Size: 277725 Color: 0

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 401530 Color: 1
Size: 316925 Color: 1
Size: 281546 Color: 0

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 401546 Color: 0
Size: 302967 Color: 0
Size: 295488 Color: 1

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 401582 Color: 1
Size: 340734 Color: 0
Size: 257685 Color: 1

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 401623 Color: 0
Size: 300941 Color: 0
Size: 297437 Color: 1

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 401597 Color: 1
Size: 307262 Color: 0
Size: 291142 Color: 1

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 401718 Color: 1
Size: 311611 Color: 0
Size: 286672 Color: 1

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 401747 Color: 0
Size: 340718 Color: 1
Size: 257536 Color: 0

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 401719 Color: 1
Size: 342262 Color: 1
Size: 256020 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 401799 Color: 0
Size: 301705 Color: 0
Size: 296497 Color: 1

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 401853 Color: 1
Size: 311026 Color: 0
Size: 287122 Color: 1

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 402186 Color: 0
Size: 324577 Color: 1
Size: 273238 Color: 0

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 402150 Color: 1
Size: 301643 Color: 0
Size: 296208 Color: 1

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 402186 Color: 0
Size: 320148 Color: 1
Size: 277667 Color: 0

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 402284 Color: 1
Size: 312144 Color: 1
Size: 285573 Color: 0

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 402238 Color: 0
Size: 303454 Color: 1
Size: 294309 Color: 0

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 402291 Color: 1
Size: 343575 Color: 1
Size: 254135 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 402419 Color: 0
Size: 347576 Color: 1
Size: 250006 Color: 0

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 402445 Color: 1
Size: 344717 Color: 1
Size: 252839 Color: 0

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 402478 Color: 1
Size: 326776 Color: 0
Size: 270747 Color: 1

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 402550 Color: 0
Size: 332936 Color: 0
Size: 264515 Color: 1

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 402605 Color: 0
Size: 302008 Color: 1
Size: 295388 Color: 1

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 402657 Color: 0
Size: 337907 Color: 1
Size: 259437 Color: 0

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 402648 Color: 1
Size: 332963 Color: 1
Size: 264390 Color: 0

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 402757 Color: 0
Size: 319402 Color: 0
Size: 277842 Color: 1

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 402744 Color: 1
Size: 317284 Color: 0
Size: 279973 Color: 1

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 402779 Color: 0
Size: 337452 Color: 0
Size: 259770 Color: 1

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 402770 Color: 1
Size: 308789 Color: 0
Size: 288442 Color: 1

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 402832 Color: 0
Size: 311925 Color: 1
Size: 285244 Color: 0

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 402774 Color: 1
Size: 322227 Color: 0
Size: 275000 Color: 1

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 402784 Color: 1
Size: 330732 Color: 0
Size: 266485 Color: 1

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 402902 Color: 0
Size: 314925 Color: 0
Size: 282174 Color: 1

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 403004 Color: 0
Size: 323585 Color: 1
Size: 273412 Color: 1

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 403031 Color: 0
Size: 313999 Color: 1
Size: 282971 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 403173 Color: 1
Size: 304308 Color: 1
Size: 292520 Color: 0

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 403217 Color: 0
Size: 319650 Color: 1
Size: 277134 Color: 0

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 403196 Color: 1
Size: 324257 Color: 0
Size: 272548 Color: 1

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 403264 Color: 0
Size: 346091 Color: 1
Size: 250646 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 403351 Color: 0
Size: 315037 Color: 0
Size: 281613 Color: 1

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 403314 Color: 1
Size: 316903 Color: 0
Size: 279784 Color: 1

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 403412 Color: 0
Size: 342445 Color: 1
Size: 254144 Color: 0

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 403438 Color: 0
Size: 298336 Color: 1
Size: 298227 Color: 1

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 403489 Color: 0
Size: 332338 Color: 0
Size: 264174 Color: 1

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 403451 Color: 1
Size: 305473 Color: 1
Size: 291077 Color: 0

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 403601 Color: 0
Size: 323421 Color: 0
Size: 272979 Color: 1

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 403550 Color: 1
Size: 309124 Color: 0
Size: 287327 Color: 1

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 403680 Color: 0
Size: 304916 Color: 1
Size: 291405 Color: 1

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 403718 Color: 1
Size: 299032 Color: 1
Size: 297251 Color: 0

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 403964 Color: 0
Size: 331317 Color: 0
Size: 264720 Color: 1

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 403917 Color: 1
Size: 326356 Color: 1
Size: 269728 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 403981 Color: 0
Size: 344394 Color: 1
Size: 251626 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 404124 Color: 1
Size: 342730 Color: 1
Size: 253147 Color: 0

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 404166 Color: 0
Size: 308981 Color: 0
Size: 286854 Color: 1

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 404285 Color: 1
Size: 315595 Color: 0
Size: 280121 Color: 1

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 404184 Color: 0
Size: 301017 Color: 0
Size: 294800 Color: 1

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 404355 Color: 1
Size: 310247 Color: 1
Size: 285399 Color: 0

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 404382 Color: 1
Size: 322967 Color: 0
Size: 272652 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 404405 Color: 1
Size: 308956 Color: 0
Size: 286640 Color: 0

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 404513 Color: 1
Size: 320128 Color: 1
Size: 275360 Color: 0

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 404675 Color: 1
Size: 308084 Color: 0
Size: 287242 Color: 1

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 404564 Color: 0
Size: 318614 Color: 1
Size: 276823 Color: 0

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 404685 Color: 1
Size: 344649 Color: 1
Size: 250667 Color: 0

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 404749 Color: 1
Size: 319479 Color: 0
Size: 275773 Color: 0

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 404784 Color: 1
Size: 325732 Color: 1
Size: 269485 Color: 0

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 404695 Color: 0
Size: 343416 Color: 0
Size: 251890 Color: 1

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 404810 Color: 1
Size: 329844 Color: 0
Size: 265347 Color: 1

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 404826 Color: 1
Size: 319962 Color: 0
Size: 275213 Color: 1

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 404886 Color: 1
Size: 300082 Color: 0
Size: 295033 Color: 1

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 404815 Color: 0
Size: 341788 Color: 0
Size: 253398 Color: 1

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 405024 Color: 0
Size: 307008 Color: 1
Size: 287969 Color: 1

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 405074 Color: 0
Size: 328837 Color: 0
Size: 266090 Color: 1

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 405077 Color: 0
Size: 326895 Color: 1
Size: 268029 Color: 1

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 405247 Color: 1
Size: 297907 Color: 0
Size: 296847 Color: 0

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 405255 Color: 1
Size: 324160 Color: 0
Size: 270586 Color: 1

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 405268 Color: 1
Size: 303714 Color: 0
Size: 291019 Color: 0

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 405280 Color: 1
Size: 299729 Color: 0
Size: 294992 Color: 1

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 405297 Color: 1
Size: 308319 Color: 0
Size: 286385 Color: 0

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 405345 Color: 1
Size: 338378 Color: 0
Size: 256278 Color: 1

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 405184 Color: 0
Size: 343646 Color: 1
Size: 251171 Color: 0

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 405388 Color: 1
Size: 334646 Color: 0
Size: 259967 Color: 1

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 405592 Color: 1
Size: 329565 Color: 0
Size: 264844 Color: 1

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 405678 Color: 0
Size: 309292 Color: 1
Size: 285031 Color: 0

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 405760 Color: 0
Size: 329688 Color: 1
Size: 264553 Color: 1

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 405817 Color: 0
Size: 301309 Color: 1
Size: 292875 Color: 0

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 405938 Color: 0
Size: 299970 Color: 1
Size: 294093 Color: 1

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 405972 Color: 0
Size: 328069 Color: 0
Size: 265960 Color: 1

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 405811 Color: 1
Size: 332000 Color: 1
Size: 262190 Color: 0

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 405988 Color: 0
Size: 339475 Color: 0
Size: 254538 Color: 1

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 405992 Color: 0
Size: 301306 Color: 1
Size: 292703 Color: 1

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 406038 Color: 0
Size: 326626 Color: 0
Size: 267337 Color: 1

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 405909 Color: 1
Size: 326496 Color: 1
Size: 267596 Color: 0

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 406097 Color: 0
Size: 311907 Color: 1
Size: 281997 Color: 0

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 406172 Color: 0
Size: 320749 Color: 1
Size: 273080 Color: 1

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 406308 Color: 1
Size: 299721 Color: 0
Size: 293972 Color: 0

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 406312 Color: 1
Size: 332946 Color: 0
Size: 260743 Color: 1

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 406399 Color: 1
Size: 341320 Color: 0
Size: 252282 Color: 1

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 406380 Color: 1
Size: 321892 Color: 0
Size: 271729 Color: 1

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 406465 Color: 0
Size: 310912 Color: 1
Size: 282624 Color: 0

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 406520 Color: 0
Size: 329413 Color: 1
Size: 264068 Color: 0

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 406442 Color: 1
Size: 329030 Color: 0
Size: 264529 Color: 1

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 406588 Color: 0
Size: 304343 Color: 1
Size: 289070 Color: 0

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 406508 Color: 1
Size: 312054 Color: 1
Size: 281439 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 406635 Color: 1
Size: 340125 Color: 1
Size: 253241 Color: 0

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 406653 Color: 1
Size: 325234 Color: 0
Size: 268114 Color: 0

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 406654 Color: 1
Size: 317973 Color: 1
Size: 275374 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 406756 Color: 1
Size: 307003 Color: 1
Size: 286242 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 406911 Color: 1
Size: 328833 Color: 0
Size: 264257 Color: 1

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 406901 Color: 0
Size: 332264 Color: 1
Size: 260836 Color: 0

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 406933 Color: 1
Size: 311082 Color: 1
Size: 281986 Color: 0

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 406980 Color: 0
Size: 323365 Color: 1
Size: 269656 Color: 0

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 407009 Color: 1
Size: 319909 Color: 0
Size: 273083 Color: 1

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 407075 Color: 0
Size: 335070 Color: 0
Size: 257856 Color: 1

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 407070 Color: 1
Size: 332352 Color: 0
Size: 260579 Color: 1

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 407113 Color: 0
Size: 325797 Color: 1
Size: 267091 Color: 0

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 407171 Color: 1
Size: 305519 Color: 0
Size: 287311 Color: 1

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 407155 Color: 0
Size: 300286 Color: 1
Size: 292560 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 407479 Color: 1
Size: 302943 Color: 1
Size: 289579 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 407595 Color: 0
Size: 331805 Color: 1
Size: 260601 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 407488 Color: 1
Size: 310084 Color: 1
Size: 282429 Color: 0

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 407700 Color: 0
Size: 318391 Color: 1
Size: 273910 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 407725 Color: 1
Size: 302222 Color: 0
Size: 290054 Color: 1

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 407736 Color: 0
Size: 303907 Color: 1
Size: 288358 Color: 0

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 407897 Color: 1
Size: 313391 Color: 1
Size: 278713 Color: 0

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 407841 Color: 0
Size: 298952 Color: 1
Size: 293208 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 407952 Color: 1
Size: 336357 Color: 1
Size: 255692 Color: 0

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 407931 Color: 0
Size: 299834 Color: 0
Size: 292236 Color: 1

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 407958 Color: 1
Size: 326308 Color: 0
Size: 265735 Color: 1

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 408054 Color: 1
Size: 315322 Color: 1
Size: 276625 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 408098 Color: 0
Size: 335760 Color: 1
Size: 256143 Color: 0

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 408129 Color: 1
Size: 339418 Color: 0
Size: 252454 Color: 1

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 408150 Color: 0
Size: 300011 Color: 1
Size: 291840 Color: 0

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 408183 Color: 1
Size: 324666 Color: 1
Size: 267152 Color: 0

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 408198 Color: 0
Size: 330007 Color: 0
Size: 261796 Color: 1

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 408236 Color: 0
Size: 336350 Color: 0
Size: 255415 Color: 1

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 408279 Color: 1
Size: 341066 Color: 1
Size: 250656 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 408350 Color: 0
Size: 322528 Color: 0
Size: 269123 Color: 1

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 408468 Color: 0
Size: 317301 Color: 1
Size: 274232 Color: 1

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 408488 Color: 0
Size: 338816 Color: 0
Size: 252697 Color: 1

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 408514 Color: 1
Size: 301470 Color: 0
Size: 290017 Color: 1

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 408537 Color: 0
Size: 331830 Color: 0
Size: 259634 Color: 1

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 408514 Color: 1
Size: 307631 Color: 0
Size: 283856 Color: 1

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 408560 Color: 0
Size: 297939 Color: 1
Size: 293502 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 408680 Color: 0
Size: 338461 Color: 1
Size: 252860 Color: 1

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 408688 Color: 0
Size: 337843 Color: 0
Size: 253470 Color: 1

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 408746 Color: 0
Size: 318023 Color: 1
Size: 273232 Color: 1

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 408747 Color: 0
Size: 340376 Color: 1
Size: 250878 Color: 0

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 408678 Color: 1
Size: 304853 Color: 1
Size: 286470 Color: 0

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 408832 Color: 0
Size: 297054 Color: 1
Size: 294115 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 408806 Color: 1
Size: 319286 Color: 1
Size: 271909 Color: 0

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 408884 Color: 0
Size: 339181 Color: 0
Size: 251936 Color: 1

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 409035 Color: 0
Size: 325292 Color: 1
Size: 265674 Color: 0

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 409040 Color: 1
Size: 298265 Color: 0
Size: 292696 Color: 1

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 409095 Color: 1
Size: 317895 Color: 0
Size: 273011 Color: 1

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 409143 Color: 1
Size: 300417 Color: 0
Size: 290441 Color: 1

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 409076 Color: 0
Size: 332721 Color: 0
Size: 258204 Color: 1

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 409204 Color: 1
Size: 295884 Color: 0
Size: 294913 Color: 1

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 409121 Color: 0
Size: 326121 Color: 1
Size: 264759 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 409252 Color: 1
Size: 324840 Color: 0
Size: 265909 Color: 1

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 409238 Color: 0
Size: 336227 Color: 0
Size: 254536 Color: 1

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 409347 Color: 0
Size: 319107 Color: 1
Size: 271547 Color: 1

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 409366 Color: 0
Size: 332229 Color: 0
Size: 258406 Color: 1

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 409365 Color: 1
Size: 336344 Color: 1
Size: 254292 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 409366 Color: 1
Size: 322695 Color: 0
Size: 267940 Color: 1

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 409560 Color: 1
Size: 321445 Color: 0
Size: 268996 Color: 0

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 409567 Color: 1
Size: 317010 Color: 1
Size: 273424 Color: 0

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 409571 Color: 1
Size: 309888 Color: 0
Size: 280542 Color: 0

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 409613 Color: 1
Size: 322892 Color: 0
Size: 267496 Color: 1

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 409693 Color: 1
Size: 314298 Color: 0
Size: 276010 Color: 0

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 409724 Color: 1
Size: 323248 Color: 0
Size: 267029 Color: 1

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 409649 Color: 0
Size: 325024 Color: 0
Size: 265328 Color: 1

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 409755 Color: 1
Size: 321584 Color: 0
Size: 268662 Color: 1

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 409827 Color: 1
Size: 323653 Color: 0
Size: 266521 Color: 0

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 409887 Color: 1
Size: 336662 Color: 0
Size: 253452 Color: 0

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 409944 Color: 1
Size: 315456 Color: 0
Size: 274601 Color: 0

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 409944 Color: 1
Size: 307433 Color: 0
Size: 282624 Color: 1

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 409859 Color: 0
Size: 308851 Color: 1
Size: 281291 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 410027 Color: 1
Size: 339015 Color: 1
Size: 250959 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 410076 Color: 0
Size: 305548 Color: 0
Size: 284377 Color: 1

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 410078 Color: 1
Size: 298686 Color: 1
Size: 291237 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 410091 Color: 0
Size: 322533 Color: 0
Size: 267377 Color: 1

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 410262 Color: 1
Size: 306749 Color: 1
Size: 282990 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 410346 Color: 1
Size: 318340 Color: 0
Size: 271315 Color: 0

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 410434 Color: 1
Size: 309961 Color: 1
Size: 279606 Color: 0

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 410293 Color: 0
Size: 298445 Color: 0
Size: 291263 Color: 1

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 410454 Color: 1
Size: 302266 Color: 0
Size: 287281 Color: 1

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 410362 Color: 0
Size: 299639 Color: 1
Size: 290000 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 410457 Color: 1
Size: 319012 Color: 1
Size: 270532 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 410496 Color: 0
Size: 327405 Color: 0
Size: 262100 Color: 1

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 410523 Color: 1
Size: 296141 Color: 0
Size: 293337 Color: 1

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 410680 Color: 1
Size: 317294 Color: 0
Size: 272027 Color: 0

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 410709 Color: 1
Size: 336524 Color: 1
Size: 252768 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 410603 Color: 0
Size: 328708 Color: 1
Size: 260690 Color: 1

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 410763 Color: 1
Size: 336749 Color: 1
Size: 252489 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 410780 Color: 1
Size: 297064 Color: 0
Size: 292157 Color: 0

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 410788 Color: 1
Size: 313628 Color: 0
Size: 275585 Color: 1

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 410848 Color: 1
Size: 332749 Color: 0
Size: 256404 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 410851 Color: 1
Size: 330408 Color: 1
Size: 258742 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 410990 Color: 1
Size: 314579 Color: 1
Size: 274432 Color: 0

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 410994 Color: 1
Size: 320579 Color: 0
Size: 268428 Color: 0

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 411126 Color: 1
Size: 333394 Color: 0
Size: 255481 Color: 0

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 411147 Color: 0
Size: 309812 Color: 1
Size: 279042 Color: 1

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 411181 Color: 0
Size: 334907 Color: 1
Size: 253913 Color: 0

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 411202 Color: 0
Size: 303609 Color: 1
Size: 285190 Color: 1

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 411217 Color: 1
Size: 322876 Color: 0
Size: 265908 Color: 0

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 411329 Color: 1
Size: 319053 Color: 1
Size: 269619 Color: 0

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 411364 Color: 1
Size: 314935 Color: 1
Size: 273702 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 411383 Color: 1
Size: 327833 Color: 0
Size: 260785 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 411453 Color: 1
Size: 323362 Color: 1
Size: 265186 Color: 0

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 411489 Color: 1
Size: 323142 Color: 0
Size: 265370 Color: 0

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 411498 Color: 1
Size: 312018 Color: 1
Size: 276485 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 411738 Color: 1
Size: 321814 Color: 0
Size: 266449 Color: 0

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 411791 Color: 1
Size: 310272 Color: 0
Size: 277938 Color: 1

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 411793 Color: 1
Size: 333528 Color: 0
Size: 254680 Color: 1

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 411851 Color: 0
Size: 316939 Color: 0
Size: 271211 Color: 1

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 411865 Color: 1
Size: 332030 Color: 1
Size: 256106 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 411921 Color: 1
Size: 294256 Color: 0
Size: 293824 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 411951 Color: 1
Size: 310596 Color: 1
Size: 277454 Color: 0

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 411990 Color: 0
Size: 337359 Color: 1
Size: 250652 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 411997 Color: 1
Size: 332533 Color: 0
Size: 255471 Color: 1

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 412113 Color: 0
Size: 328979 Color: 1
Size: 258909 Color: 0

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 412108 Color: 1
Size: 336838 Color: 0
Size: 251055 Color: 1

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 412136 Color: 1
Size: 323439 Color: 0
Size: 264426 Color: 1

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 412247 Color: 1
Size: 333619 Color: 0
Size: 254135 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 412362 Color: 1
Size: 330140 Color: 1
Size: 257499 Color: 0

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 412377 Color: 0
Size: 336835 Color: 0
Size: 250789 Color: 1

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 412467 Color: 1
Size: 328858 Color: 0
Size: 258676 Color: 1

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 412475 Color: 1
Size: 334167 Color: 0
Size: 253359 Color: 0

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 412502 Color: 1
Size: 333171 Color: 0
Size: 254328 Color: 1

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 412554 Color: 0
Size: 319214 Color: 1
Size: 268233 Color: 0

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 412677 Color: 0
Size: 326677 Color: 1
Size: 260647 Color: 1

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 412710 Color: 0
Size: 306805 Color: 0
Size: 280486 Color: 1

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 412610 Color: 1
Size: 331635 Color: 1
Size: 255756 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 412699 Color: 1
Size: 333237 Color: 0
Size: 254065 Color: 1

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 412803 Color: 0
Size: 324890 Color: 0
Size: 262308 Color: 1

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 412871 Color: 1
Size: 327468 Color: 1
Size: 259662 Color: 0

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 412907 Color: 0
Size: 301982 Color: 1
Size: 285112 Color: 0

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 412887 Color: 1
Size: 301749 Color: 1
Size: 285365 Color: 0

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 412919 Color: 0
Size: 296827 Color: 1
Size: 290255 Color: 0

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 412980 Color: 1
Size: 321282 Color: 0
Size: 265739 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 413313 Color: 1
Size: 312158 Color: 1
Size: 274530 Color: 0

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 413308 Color: 0
Size: 310367 Color: 0
Size: 276326 Color: 1

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 413418 Color: 1
Size: 333475 Color: 1
Size: 253108 Color: 0

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 413395 Color: 0
Size: 308691 Color: 1
Size: 277915 Color: 0

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 413484 Color: 1
Size: 319208 Color: 1
Size: 267309 Color: 0

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 413485 Color: 1
Size: 316565 Color: 0
Size: 269951 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 413498 Color: 1
Size: 320728 Color: 0
Size: 265775 Color: 1

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 413596 Color: 0
Size: 324898 Color: 1
Size: 261507 Color: 0

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 413589 Color: 1
Size: 310705 Color: 1
Size: 275707 Color: 0

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 413600 Color: 0
Size: 300942 Color: 0
Size: 285459 Color: 1

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 413689 Color: 1
Size: 295111 Color: 0
Size: 291201 Color: 1

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 413694 Color: 1
Size: 296445 Color: 0
Size: 289862 Color: 0

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 413719 Color: 1
Size: 321795 Color: 1
Size: 264487 Color: 0

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 413716 Color: 0
Size: 308085 Color: 0
Size: 278200 Color: 1

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 413765 Color: 0
Size: 326145 Color: 1
Size: 260091 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 413899 Color: 1
Size: 300725 Color: 1
Size: 285377 Color: 0

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 414031 Color: 0
Size: 317383 Color: 1
Size: 268587 Color: 0

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 414036 Color: 0
Size: 329388 Color: 1
Size: 256577 Color: 1

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 414057 Color: 1
Size: 313404 Color: 1
Size: 272540 Color: 0

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 414103 Color: 0
Size: 293613 Color: 1
Size: 292285 Color: 0

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 414082 Color: 1
Size: 293188 Color: 0
Size: 292731 Color: 1

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 414273 Color: 1
Size: 313171 Color: 0
Size: 272557 Color: 0

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 414274 Color: 1
Size: 333068 Color: 1
Size: 252659 Color: 0

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 414275 Color: 1
Size: 309276 Color: 0
Size: 276450 Color: 0

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 414338 Color: 0
Size: 312701 Color: 0
Size: 272962 Color: 1

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 414344 Color: 1
Size: 320238 Color: 0
Size: 265419 Color: 1

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 414383 Color: 0
Size: 326774 Color: 0
Size: 258844 Color: 1

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 414361 Color: 1
Size: 300699 Color: 0
Size: 284941 Color: 1

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 414454 Color: 0
Size: 299178 Color: 1
Size: 286369 Color: 0

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 414491 Color: 0
Size: 294306 Color: 1
Size: 291204 Color: 0

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 414472 Color: 1
Size: 297761 Color: 0
Size: 287768 Color: 1

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 414517 Color: 0
Size: 300638 Color: 0
Size: 284846 Color: 1

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 414595 Color: 1
Size: 304925 Color: 0
Size: 280481 Color: 1

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 414565 Color: 0
Size: 298066 Color: 0
Size: 287370 Color: 1

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 414601 Color: 1
Size: 324057 Color: 0
Size: 261343 Color: 1

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 414582 Color: 0
Size: 293669 Color: 0
Size: 291750 Color: 1

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 414712 Color: 1
Size: 329969 Color: 0
Size: 255320 Color: 0

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 414733 Color: 1
Size: 321334 Color: 0
Size: 263934 Color: 1

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 414787 Color: 0
Size: 322702 Color: 0
Size: 262512 Color: 1

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 414827 Color: 0
Size: 308818 Color: 1
Size: 276356 Color: 0

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 414790 Color: 1
Size: 302046 Color: 0
Size: 283165 Color: 1

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 414857 Color: 0
Size: 327110 Color: 0
Size: 258034 Color: 1

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 414879 Color: 1
Size: 293243 Color: 0
Size: 291879 Color: 1

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 414985 Color: 1
Size: 312224 Color: 0
Size: 272792 Color: 0

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 415143 Color: 1
Size: 305218 Color: 0
Size: 279640 Color: 0

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 415154 Color: 1
Size: 327660 Color: 0
Size: 257187 Color: 1

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 415144 Color: 0
Size: 333349 Color: 1
Size: 251508 Color: 0

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 415320 Color: 1
Size: 333708 Color: 0
Size: 250973 Color: 1

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 415233 Color: 0
Size: 334086 Color: 0
Size: 250682 Color: 1

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 415338 Color: 1
Size: 332649 Color: 0
Size: 252014 Color: 1

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 415245 Color: 0
Size: 322740 Color: 1
Size: 262016 Color: 0

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 415456 Color: 1
Size: 303808 Color: 1
Size: 280737 Color: 0

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 415601 Color: 1
Size: 318384 Color: 0
Size: 266016 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 415658 Color: 1
Size: 318307 Color: 1
Size: 266036 Color: 0

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 415562 Color: 0
Size: 331714 Color: 1
Size: 252725 Color: 0

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 415665 Color: 1
Size: 292221 Color: 1
Size: 292115 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 415703 Color: 1
Size: 310624 Color: 0
Size: 273674 Color: 0

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 415715 Color: 1
Size: 332845 Color: 0
Size: 251441 Color: 1

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 415739 Color: 0
Size: 309704 Color: 1
Size: 274558 Color: 0

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 415794 Color: 0
Size: 307695 Color: 1
Size: 276512 Color: 1

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 415803 Color: 1
Size: 304166 Color: 1
Size: 280032 Color: 0

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 415875 Color: 0
Size: 308400 Color: 0
Size: 275726 Color: 1

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 415881 Color: 0
Size: 325662 Color: 1
Size: 258458 Color: 1

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 415882 Color: 0
Size: 294608 Color: 1
Size: 289511 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 415892 Color: 0
Size: 323946 Color: 1
Size: 260163 Color: 1

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 416029 Color: 1
Size: 312923 Color: 1
Size: 271049 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 416051 Color: 0
Size: 297806 Color: 1
Size: 286144 Color: 0

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 416198 Color: 1
Size: 311245 Color: 1
Size: 272558 Color: 0

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 416227 Color: 0
Size: 326019 Color: 0
Size: 257755 Color: 1

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 416234 Color: 0
Size: 302244 Color: 1
Size: 281523 Color: 1

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 416292 Color: 1
Size: 327584 Color: 0
Size: 256125 Color: 1

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 416276 Color: 0
Size: 315608 Color: 1
Size: 268117 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 416308 Color: 0
Size: 317574 Color: 1
Size: 266119 Color: 1

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 416455 Color: 0
Size: 294380 Color: 1
Size: 289166 Color: 0

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 416512 Color: 1
Size: 309002 Color: 0
Size: 274487 Color: 1

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 416577 Color: 0
Size: 317663 Color: 1
Size: 265761 Color: 0

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 416540 Color: 1
Size: 328567 Color: 1
Size: 254894 Color: 0

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 416609 Color: 1
Size: 292275 Color: 1
Size: 291117 Color: 0

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 416603 Color: 0
Size: 299284 Color: 0
Size: 284114 Color: 1

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 416674 Color: 0
Size: 321837 Color: 1
Size: 261490 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 416707 Color: 0
Size: 292090 Color: 0
Size: 291204 Color: 1

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 416798 Color: 0
Size: 332780 Color: 1
Size: 250423 Color: 1

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 416716 Color: 1
Size: 310219 Color: 0
Size: 273066 Color: 1

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 416877 Color: 0
Size: 300760 Color: 0
Size: 282364 Color: 1

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 416877 Color: 0
Size: 319576 Color: 1
Size: 263548 Color: 1

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 416923 Color: 1
Size: 312183 Color: 0
Size: 270895 Color: 0

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 416999 Color: 1
Size: 329034 Color: 0
Size: 253968 Color: 1

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 416997 Color: 0
Size: 313670 Color: 1
Size: 269334 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 417001 Color: 1
Size: 307793 Color: 1
Size: 275207 Color: 0

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 417101 Color: 0
Size: 332079 Color: 0
Size: 250821 Color: 1

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 417319 Color: 0
Size: 319656 Color: 0
Size: 263026 Color: 1

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 417279 Color: 1
Size: 302469 Color: 1
Size: 280253 Color: 0

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 417382 Color: 0
Size: 322114 Color: 1
Size: 260505 Color: 0

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 417385 Color: 1
Size: 331266 Color: 0
Size: 251350 Color: 1

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 417405 Color: 1
Size: 302649 Color: 0
Size: 279947 Color: 1

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 417474 Color: 0
Size: 327154 Color: 0
Size: 255373 Color: 1

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 417422 Color: 1
Size: 310452 Color: 1
Size: 272127 Color: 0

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 417522 Color: 0
Size: 328667 Color: 1
Size: 253812 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 417478 Color: 1
Size: 311654 Color: 0
Size: 270869 Color: 1

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 417552 Color: 0
Size: 304572 Color: 0
Size: 277877 Color: 1

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 417524 Color: 1
Size: 303401 Color: 0
Size: 279076 Color: 1

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 417567 Color: 0
Size: 304527 Color: 1
Size: 277907 Color: 1

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 417649 Color: 0
Size: 301718 Color: 1
Size: 280634 Color: 1

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 417666 Color: 0
Size: 329466 Color: 1
Size: 252869 Color: 0

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 417583 Color: 1
Size: 312502 Color: 1
Size: 269916 Color: 0

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 417713 Color: 0
Size: 296982 Color: 1
Size: 285306 Color: 0

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 417641 Color: 1
Size: 308894 Color: 0
Size: 273466 Color: 1

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 417715 Color: 0
Size: 303802 Color: 1
Size: 278484 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 417825 Color: 0
Size: 293137 Color: 1
Size: 289039 Color: 1

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 417841 Color: 0
Size: 307739 Color: 1
Size: 274421 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 417838 Color: 1
Size: 330253 Color: 0
Size: 251910 Color: 1

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 417875 Color: 0
Size: 312622 Color: 0
Size: 269504 Color: 1

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 418006 Color: 1
Size: 309376 Color: 0
Size: 272619 Color: 1

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 418043 Color: 1
Size: 323824 Color: 0
Size: 258134 Color: 1

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 418083 Color: 1
Size: 308207 Color: 0
Size: 273711 Color: 0

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 418093 Color: 1
Size: 309111 Color: 0
Size: 272797 Color: 1

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 418063 Color: 0
Size: 325600 Color: 1
Size: 256338 Color: 0

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 418124 Color: 1
Size: 298990 Color: 1
Size: 282887 Color: 0

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 418156 Color: 0
Size: 308414 Color: 0
Size: 273431 Color: 1

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 418167 Color: 1
Size: 310595 Color: 1
Size: 271239 Color: 0

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 418320 Color: 1
Size: 318161 Color: 0
Size: 263520 Color: 1

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 418454 Color: 1
Size: 295091 Color: 1
Size: 286456 Color: 0

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 418420 Color: 0
Size: 313246 Color: 0
Size: 268335 Color: 1

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 418472 Color: 1
Size: 320718 Color: 0
Size: 260811 Color: 0

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 418553 Color: 0
Size: 319097 Color: 0
Size: 262351 Color: 1

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 418581 Color: 1
Size: 301344 Color: 1
Size: 280076 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 418607 Color: 1
Size: 326884 Color: 0
Size: 254510 Color: 1

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 418690 Color: 0
Size: 311564 Color: 0
Size: 269747 Color: 1

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 418656 Color: 1
Size: 295864 Color: 0
Size: 285481 Color: 1

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 418769 Color: 0
Size: 293688 Color: 1
Size: 287544 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 418760 Color: 1
Size: 316152 Color: 0
Size: 265089 Color: 1

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 418883 Color: 0
Size: 304274 Color: 1
Size: 276844 Color: 1

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 418884 Color: 0
Size: 292614 Color: 1
Size: 288503 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 418873 Color: 1
Size: 304767 Color: 1
Size: 276361 Color: 0

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 418907 Color: 0
Size: 292528 Color: 1
Size: 288566 Color: 0

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 418958 Color: 1
Size: 323139 Color: 0
Size: 257904 Color: 1

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 418960 Color: 0
Size: 312332 Color: 1
Size: 268709 Color: 0

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 418979 Color: 1
Size: 320584 Color: 1
Size: 260438 Color: 0

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 419102 Color: 1
Size: 325288 Color: 1
Size: 255611 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 419105 Color: 0
Size: 321671 Color: 0
Size: 259225 Color: 1

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 419306 Color: 1
Size: 321429 Color: 1
Size: 259266 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 419294 Color: 0
Size: 292486 Color: 0
Size: 288221 Color: 1

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 419534 Color: 1
Size: 329835 Color: 0
Size: 250632 Color: 0

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 419698 Color: 1
Size: 303443 Color: 0
Size: 276860 Color: 1

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 419676 Color: 0
Size: 296262 Color: 1
Size: 284063 Color: 0

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 419735 Color: 1
Size: 292111 Color: 1
Size: 288155 Color: 0

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 419715 Color: 0
Size: 303644 Color: 1
Size: 276642 Color: 0

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 419797 Color: 1
Size: 314215 Color: 0
Size: 265989 Color: 1

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 419869 Color: 0
Size: 299971 Color: 1
Size: 280161 Color: 0

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 419887 Color: 0
Size: 328288 Color: 0
Size: 251826 Color: 1

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 420026 Color: 0
Size: 300177 Color: 1
Size: 279798 Color: 1

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 420236 Color: 1
Size: 296533 Color: 1
Size: 283232 Color: 0

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 420248 Color: 1
Size: 291669 Color: 1
Size: 288084 Color: 0

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 420304 Color: 1
Size: 320257 Color: 0
Size: 259440 Color: 1

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 420366 Color: 0
Size: 298116 Color: 1
Size: 281519 Color: 0

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 420429 Color: 1
Size: 324535 Color: 0
Size: 255037 Color: 1

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 420468 Color: 0
Size: 322921 Color: 0
Size: 256612 Color: 1

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 420493 Color: 0
Size: 306279 Color: 1
Size: 273229 Color: 1

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 420519 Color: 0
Size: 318958 Color: 0
Size: 260524 Color: 1

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 420747 Color: 1
Size: 316106 Color: 0
Size: 263148 Color: 0

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 420896 Color: 0
Size: 312453 Color: 1
Size: 266652 Color: 1

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 420951 Color: 0
Size: 313677 Color: 0
Size: 265373 Color: 1

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 420999 Color: 0
Size: 327776 Color: 1
Size: 251226 Color: 1

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 421100 Color: 1
Size: 328845 Color: 1
Size: 250056 Color: 0

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 421084 Color: 0
Size: 291259 Color: 1
Size: 287658 Color: 0

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 421293 Color: 0
Size: 295160 Color: 1
Size: 283548 Color: 1

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 421368 Color: 0
Size: 303789 Color: 1
Size: 274844 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 421410 Color: 0
Size: 323586 Color: 1
Size: 255005 Color: 1

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 421417 Color: 0
Size: 310889 Color: 0
Size: 267695 Color: 1

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 421414 Color: 1
Size: 302592 Color: 1
Size: 275995 Color: 0

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 421464 Color: 0
Size: 317563 Color: 1
Size: 260974 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 421482 Color: 1
Size: 292142 Color: 1
Size: 286377 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 421518 Color: 0
Size: 328147 Color: 0
Size: 250336 Color: 1

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 421637 Color: 1
Size: 312935 Color: 0
Size: 265429 Color: 1

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 421669 Color: 0
Size: 315739 Color: 1
Size: 262593 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 421796 Color: 1
Size: 326550 Color: 1
Size: 251655 Color: 0

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 421743 Color: 0
Size: 309616 Color: 1
Size: 268642 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 421882 Color: 0
Size: 321133 Color: 1
Size: 256986 Color: 1

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 421886 Color: 0
Size: 313180 Color: 1
Size: 264935 Color: 0

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 421972 Color: 1
Size: 313711 Color: 1
Size: 264318 Color: 0

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 421901 Color: 0
Size: 313365 Color: 0
Size: 264735 Color: 1

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 421986 Color: 0
Size: 305832 Color: 1
Size: 272183 Color: 1

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 422013 Color: 0
Size: 302240 Color: 1
Size: 275748 Color: 0

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 422057 Color: 0
Size: 308615 Color: 1
Size: 269329 Color: 1

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 422095 Color: 1
Size: 294808 Color: 0
Size: 283098 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 422105 Color: 1
Size: 291406 Color: 0
Size: 286490 Color: 1

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 422133 Color: 0
Size: 314802 Color: 0
Size: 263066 Color: 1

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 422297 Color: 1
Size: 320207 Color: 1
Size: 257497 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 422430 Color: 0
Size: 294723 Color: 0
Size: 282848 Color: 1

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 422387 Color: 1
Size: 319568 Color: 1
Size: 258046 Color: 0

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 422456 Color: 1
Size: 319777 Color: 0
Size: 257768 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 422502 Color: 0
Size: 323754 Color: 1
Size: 253745 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 422521 Color: 1
Size: 323991 Color: 1
Size: 253489 Color: 0

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 422633 Color: 0
Size: 309749 Color: 1
Size: 267619 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 422695 Color: 1
Size: 299178 Color: 1
Size: 278128 Color: 0

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 422764 Color: 0
Size: 316918 Color: 1
Size: 260319 Color: 0

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 422713 Color: 1
Size: 325690 Color: 0
Size: 251598 Color: 1

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 422815 Color: 0
Size: 320391 Color: 0
Size: 256795 Color: 1

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 422832 Color: 0
Size: 303939 Color: 1
Size: 273230 Color: 0

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 422863 Color: 1
Size: 292790 Color: 0
Size: 284348 Color: 1

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 422928 Color: 0
Size: 306360 Color: 1
Size: 270713 Color: 0

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 423059 Color: 0
Size: 294653 Color: 0
Size: 282289 Color: 1

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 422939 Color: 1
Size: 313775 Color: 1
Size: 263287 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 423060 Color: 0
Size: 290195 Color: 1
Size: 286746 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 423137 Color: 0
Size: 295161 Color: 1
Size: 281703 Color: 1

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 423143 Color: 0
Size: 300158 Color: 0
Size: 276700 Color: 1

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 423186 Color: 0
Size: 303381 Color: 1
Size: 273434 Color: 1

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 423205 Color: 0
Size: 310932 Color: 1
Size: 265864 Color: 0

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 423206 Color: 0
Size: 322696 Color: 1
Size: 254099 Color: 1

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 423209 Color: 0
Size: 296303 Color: 0
Size: 280489 Color: 1

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 423105 Color: 1
Size: 321492 Color: 0
Size: 255404 Color: 1

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 423312 Color: 0
Size: 310252 Color: 0
Size: 266437 Color: 1

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 423670 Color: 0
Size: 314063 Color: 0
Size: 262268 Color: 1

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 423740 Color: 0
Size: 321350 Color: 1
Size: 254911 Color: 1

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 423748 Color: 0
Size: 307808 Color: 1
Size: 268445 Color: 1

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 423756 Color: 0
Size: 288194 Color: 1
Size: 288051 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 423828 Color: 1
Size: 305984 Color: 1
Size: 270189 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 423846 Color: 1
Size: 292311 Color: 0
Size: 283844 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 423991 Color: 0
Size: 322294 Color: 0
Size: 253716 Color: 1

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 424122 Color: 1
Size: 295919 Color: 1
Size: 279960 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 424181 Color: 1
Size: 324520 Color: 0
Size: 251300 Color: 0

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 424197 Color: 1
Size: 307980 Color: 1
Size: 267824 Color: 0

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 424209 Color: 0
Size: 325379 Color: 0
Size: 250413 Color: 1

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 424231 Color: 1
Size: 315863 Color: 0
Size: 259907 Color: 1

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 424258 Color: 0
Size: 305989 Color: 0
Size: 269754 Color: 1

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 424273 Color: 1
Size: 323561 Color: 1
Size: 252167 Color: 0

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 424334 Color: 0
Size: 315511 Color: 0
Size: 260156 Color: 1

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 424371 Color: 0
Size: 325007 Color: 1
Size: 250623 Color: 1

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 424431 Color: 1
Size: 313910 Color: 0
Size: 261660 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 424514 Color: 1
Size: 322510 Color: 0
Size: 252977 Color: 0

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 424578 Color: 1
Size: 296170 Color: 1
Size: 279253 Color: 0

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 424750 Color: 1
Size: 289576 Color: 0
Size: 285675 Color: 0

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 424758 Color: 1
Size: 325019 Color: 1
Size: 250224 Color: 0

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 424844 Color: 0
Size: 316692 Color: 0
Size: 258465 Color: 1

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 424851 Color: 0
Size: 295413 Color: 1
Size: 279737 Color: 1

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 424873 Color: 0
Size: 299721 Color: 0
Size: 275407 Color: 1

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 424894 Color: 1
Size: 296911 Color: 0
Size: 278196 Color: 1

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 424909 Color: 0
Size: 295830 Color: 0
Size: 279262 Color: 1

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 424931 Color: 1
Size: 322002 Color: 1
Size: 253068 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 425188 Color: 0
Size: 311197 Color: 1
Size: 263616 Color: 1

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 425203 Color: 0
Size: 288378 Color: 0
Size: 286420 Color: 1

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 425225 Color: 0
Size: 304960 Color: 1
Size: 269816 Color: 1

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 425306 Color: 0
Size: 323856 Color: 1
Size: 250839 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 425365 Color: 0
Size: 297276 Color: 1
Size: 277360 Color: 0

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 425809 Color: 0
Size: 321956 Color: 0
Size: 252236 Color: 1

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 425833 Color: 0
Size: 296480 Color: 1
Size: 277688 Color: 1

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 425847 Color: 0
Size: 289387 Color: 1
Size: 284767 Color: 0

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 425845 Color: 1
Size: 306618 Color: 1
Size: 267538 Color: 0

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 425874 Color: 0
Size: 289437 Color: 1
Size: 284690 Color: 0

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 425930 Color: 0
Size: 287062 Color: 1
Size: 287009 Color: 1

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 425961 Color: 0
Size: 313118 Color: 0
Size: 260922 Color: 1

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 426079 Color: 0
Size: 317152 Color: 0
Size: 256770 Color: 1

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 426149 Color: 1
Size: 318296 Color: 1
Size: 255556 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 426091 Color: 0
Size: 287508 Color: 0
Size: 286402 Color: 1

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 426379 Color: 1
Size: 308053 Color: 1
Size: 265569 Color: 0

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 426456 Color: 0
Size: 288105 Color: 0
Size: 285440 Color: 1

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 426478 Color: 1
Size: 322599 Color: 1
Size: 250924 Color: 0

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 426574 Color: 0
Size: 288241 Color: 1
Size: 285186 Color: 1

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 426725 Color: 0
Size: 308238 Color: 0
Size: 265038 Color: 1

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 426791 Color: 1
Size: 318633 Color: 0
Size: 254577 Color: 1

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 426832 Color: 0
Size: 294721 Color: 0
Size: 278448 Color: 1

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 426805 Color: 1
Size: 286945 Color: 1
Size: 286251 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 426848 Color: 0
Size: 314102 Color: 1
Size: 259051 Color: 0

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 426885 Color: 1
Size: 300655 Color: 0
Size: 272461 Color: 1

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 427005 Color: 1
Size: 306672 Color: 0
Size: 266324 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 427079 Color: 1
Size: 322011 Color: 1
Size: 250911 Color: 0

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 427066 Color: 0
Size: 288255 Color: 1
Size: 284680 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 427122 Color: 0
Size: 305619 Color: 1
Size: 267260 Color: 1

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 427107 Color: 1
Size: 306635 Color: 0
Size: 266259 Color: 1

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 427239 Color: 1
Size: 316627 Color: 0
Size: 256135 Color: 0

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 427309 Color: 1
Size: 317992 Color: 0
Size: 254700 Color: 1

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 427389 Color: 0
Size: 301765 Color: 1
Size: 270847 Color: 0

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 427407 Color: 0
Size: 306552 Color: 1
Size: 266042 Color: 1

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 427528 Color: 0
Size: 307510 Color: 1
Size: 264963 Color: 0

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 427546 Color: 1
Size: 294271 Color: 1
Size: 278184 Color: 0

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 427557 Color: 1
Size: 317451 Color: 1
Size: 254993 Color: 0

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 427710 Color: 1
Size: 290069 Color: 1
Size: 282222 Color: 0

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 427713 Color: 1
Size: 291926 Color: 0
Size: 280362 Color: 0

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 427715 Color: 1
Size: 292068 Color: 1
Size: 280218 Color: 0

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 427732 Color: 1
Size: 318202 Color: 0
Size: 254067 Color: 0

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 427819 Color: 1
Size: 316304 Color: 0
Size: 255878 Color: 1

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 427709 Color: 0
Size: 295845 Color: 0
Size: 276447 Color: 1

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 427860 Color: 1
Size: 313071 Color: 1
Size: 259070 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 427811 Color: 0
Size: 303694 Color: 0
Size: 268496 Color: 1

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 428158 Color: 1
Size: 301310 Color: 1
Size: 270533 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 428185 Color: 1
Size: 311672 Color: 0
Size: 260144 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 428272 Color: 1
Size: 305087 Color: 0
Size: 266642 Color: 1

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 428172 Color: 0
Size: 293404 Color: 1
Size: 278425 Color: 0

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 428311 Color: 1
Size: 292282 Color: 0
Size: 279408 Color: 1

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 428308 Color: 0
Size: 308505 Color: 0
Size: 263188 Color: 1

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 428329 Color: 1
Size: 311912 Color: 0
Size: 259760 Color: 1

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 428350 Color: 0
Size: 298802 Color: 0
Size: 272849 Color: 1

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 428373 Color: 0
Size: 321417 Color: 1
Size: 250211 Color: 0

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 428408 Color: 0
Size: 319470 Color: 0
Size: 252123 Color: 1

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 428409 Color: 0
Size: 314321 Color: 1
Size: 257271 Color: 1

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 428414 Color: 0
Size: 293967 Color: 0
Size: 277620 Color: 1

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 428430 Color: 0
Size: 296064 Color: 1
Size: 275507 Color: 1

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 428570 Color: 1
Size: 289701 Color: 0
Size: 281730 Color: 0

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 428633 Color: 1
Size: 291822 Color: 0
Size: 279546 Color: 1

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 428636 Color: 1
Size: 296067 Color: 0
Size: 275298 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 428649 Color: 0
Size: 288809 Color: 1
Size: 282543 Color: 1

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 428641 Color: 1
Size: 312339 Color: 1
Size: 259021 Color: 0

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 428668 Color: 1
Size: 293168 Color: 0
Size: 278165 Color: 0

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 428683 Color: 1
Size: 319472 Color: 0
Size: 251846 Color: 1

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 428804 Color: 0
Size: 319114 Color: 1
Size: 252083 Color: 0

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 428726 Color: 1
Size: 315134 Color: 0
Size: 256141 Color: 1

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 428823 Color: 0
Size: 311043 Color: 0
Size: 260135 Color: 1

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 428863 Color: 1
Size: 288865 Color: 0
Size: 282273 Color: 0

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 428880 Color: 0
Size: 287803 Color: 0
Size: 283318 Color: 1

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 428943 Color: 1
Size: 300013 Color: 0
Size: 271045 Color: 1

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 428978 Color: 0
Size: 300419 Color: 0
Size: 270604 Color: 1

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 428983 Color: 1
Size: 306140 Color: 1
Size: 264878 Color: 0

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 429091 Color: 1
Size: 313728 Color: 0
Size: 257182 Color: 1

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 429301 Color: 0
Size: 307282 Color: 0
Size: 263418 Color: 1

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 429214 Color: 1
Size: 296710 Color: 0
Size: 274077 Color: 1

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 429452 Color: 0
Size: 314243 Color: 0
Size: 256306 Color: 1

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 429509 Color: 1
Size: 303876 Color: 0
Size: 266616 Color: 1

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 429569 Color: 1
Size: 316948 Color: 0
Size: 253484 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 429683 Color: 0
Size: 310145 Color: 0
Size: 260173 Color: 1

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 429718 Color: 1
Size: 292481 Color: 1
Size: 277802 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 429879 Color: 0
Size: 285597 Color: 0
Size: 284525 Color: 1

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 429946 Color: 0
Size: 287372 Color: 1
Size: 282683 Color: 1

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 429947 Color: 0
Size: 316237 Color: 0
Size: 253817 Color: 1

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 429910 Color: 1
Size: 308316 Color: 0
Size: 261775 Color: 1

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 430014 Color: 0
Size: 305619 Color: 0
Size: 264368 Color: 1

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 430162 Color: 0
Size: 294202 Color: 1
Size: 275637 Color: 0

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 430169 Color: 0
Size: 304970 Color: 1
Size: 264862 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 430151 Color: 1
Size: 318039 Color: 1
Size: 251811 Color: 0

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 430315 Color: 0
Size: 319225 Color: 0
Size: 250461 Color: 1

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 430286 Color: 1
Size: 317722 Color: 1
Size: 251993 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 430378 Color: 0
Size: 312913 Color: 1
Size: 256710 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 430327 Color: 1
Size: 302801 Color: 0
Size: 266873 Color: 1

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 430380 Color: 0
Size: 304163 Color: 0
Size: 265458 Color: 1

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 430374 Color: 1
Size: 285712 Color: 1
Size: 283915 Color: 0

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 430385 Color: 0
Size: 306920 Color: 0
Size: 262696 Color: 1

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 430399 Color: 1
Size: 319231 Color: 1
Size: 250371 Color: 0

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 430440 Color: 0
Size: 305284 Color: 1
Size: 264277 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 430547 Color: 1
Size: 300194 Color: 0
Size: 269260 Color: 0

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 430583 Color: 1
Size: 296358 Color: 1
Size: 273060 Color: 0

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 430603 Color: 1
Size: 300299 Color: 0
Size: 269099 Color: 0

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 430844 Color: 0
Size: 299791 Color: 1
Size: 269366 Color: 1

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 430860 Color: 0
Size: 291199 Color: 1
Size: 277942 Color: 1

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 430879 Color: 0
Size: 306687 Color: 0
Size: 262435 Color: 1

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 430887 Color: 0
Size: 304348 Color: 1
Size: 264766 Color: 1

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 430916 Color: 0
Size: 294200 Color: 1
Size: 274885 Color: 0

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 430932 Color: 0
Size: 291891 Color: 1
Size: 277178 Color: 0

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 430872 Color: 1
Size: 299947 Color: 1
Size: 269182 Color: 0

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 430939 Color: 0
Size: 291607 Color: 0
Size: 277455 Color: 1

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 430967 Color: 0
Size: 303481 Color: 1
Size: 265553 Color: 1

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 431054 Color: 0
Size: 303673 Color: 1
Size: 265274 Color: 1

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 431268 Color: 1
Size: 302147 Color: 1
Size: 266586 Color: 0

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 431369 Color: 0
Size: 286990 Color: 1
Size: 281642 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 431386 Color: 1
Size: 287167 Color: 1
Size: 281448 Color: 0

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 431392 Color: 0
Size: 300684 Color: 1
Size: 267925 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 431404 Color: 1
Size: 289900 Color: 0
Size: 278697 Color: 1

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 431411 Color: 1
Size: 307366 Color: 0
Size: 261224 Color: 0

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 431414 Color: 0
Size: 290375 Color: 0
Size: 278212 Color: 1

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 431419 Color: 1
Size: 299999 Color: 0
Size: 268583 Color: 1

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 431468 Color: 0
Size: 296036 Color: 1
Size: 272497 Color: 0

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 431482 Color: 0
Size: 292388 Color: 1
Size: 276131 Color: 1

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 431577 Color: 1
Size: 289459 Color: 0
Size: 278965 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 431535 Color: 0
Size: 308520 Color: 0
Size: 259946 Color: 1

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 431667 Color: 1
Size: 311365 Color: 1
Size: 256969 Color: 0

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 431714 Color: 1
Size: 296456 Color: 1
Size: 271831 Color: 0

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 431953 Color: 1
Size: 303277 Color: 0
Size: 264771 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 432213 Color: 1
Size: 317488 Color: 0
Size: 250300 Color: 1

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 432242 Color: 1
Size: 299117 Color: 0
Size: 268642 Color: 1

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 432219 Color: 0
Size: 298981 Color: 1
Size: 268801 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 432447 Color: 0
Size: 314796 Color: 1
Size: 252758 Color: 1

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 432458 Color: 0
Size: 300016 Color: 1
Size: 267527 Color: 0

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 432506 Color: 0
Size: 305089 Color: 1
Size: 262406 Color: 1

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 432527 Color: 0
Size: 305096 Color: 0
Size: 262378 Color: 1

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 432487 Color: 1
Size: 307007 Color: 0
Size: 260507 Color: 1

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 432585 Color: 0
Size: 314082 Color: 0
Size: 253334 Color: 1

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 432611 Color: 0
Size: 302058 Color: 1
Size: 265332 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 432581 Color: 1
Size: 309191 Color: 0
Size: 258229 Color: 1

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 432720 Color: 0
Size: 291493 Color: 1
Size: 275788 Color: 0

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 432977 Color: 1
Size: 313101 Color: 0
Size: 253923 Color: 1

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 433007 Color: 0
Size: 293201 Color: 1
Size: 273793 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 433046 Color: 1
Size: 297197 Color: 1
Size: 269758 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 433054 Color: 0
Size: 304760 Color: 0
Size: 262187 Color: 1

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 433084 Color: 1
Size: 303300 Color: 0
Size: 263617 Color: 1

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 433204 Color: 0
Size: 286417 Color: 1
Size: 280380 Color: 0

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 433196 Color: 1
Size: 294497 Color: 1
Size: 272308 Color: 0

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 433327 Color: 1
Size: 290313 Color: 0
Size: 276361 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 433332 Color: 1
Size: 283834 Color: 0
Size: 282835 Color: 1

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 433238 Color: 0
Size: 300622 Color: 0
Size: 266141 Color: 1

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 433347 Color: 1
Size: 296003 Color: 1
Size: 270651 Color: 0

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 433487 Color: 1
Size: 312466 Color: 0
Size: 254048 Color: 1

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 433499 Color: 1
Size: 307561 Color: 0
Size: 258941 Color: 1

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 433476 Color: 0
Size: 311584 Color: 0
Size: 254941 Color: 1

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 433639 Color: 1
Size: 308900 Color: 0
Size: 257462 Color: 0

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 433694 Color: 1
Size: 310000 Color: 0
Size: 256307 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 433784 Color: 1
Size: 289523 Color: 1
Size: 276694 Color: 0

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 433796 Color: 1
Size: 284865 Color: 0
Size: 281340 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 433910 Color: 1
Size: 310420 Color: 1
Size: 255671 Color: 0

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 433813 Color: 0
Size: 303047 Color: 1
Size: 263141 Color: 0

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 433959 Color: 0
Size: 292303 Color: 1
Size: 273739 Color: 1

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 434002 Color: 0
Size: 305531 Color: 1
Size: 260468 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 434253 Color: 1
Size: 286037 Color: 0
Size: 279711 Color: 0

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 434395 Color: 0
Size: 289019 Color: 1
Size: 276587 Color: 0

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 434403 Color: 1
Size: 295114 Color: 0
Size: 270484 Color: 1

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 434409 Color: 0
Size: 295391 Color: 1
Size: 270201 Color: 0

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 434517 Color: 1
Size: 302257 Color: 1
Size: 263227 Color: 0

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 434587 Color: 1
Size: 291333 Color: 0
Size: 274081 Color: 0

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 434546 Color: 0
Size: 299779 Color: 0
Size: 265676 Color: 1

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 434626 Color: 1
Size: 313642 Color: 0
Size: 251733 Color: 1

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 434681 Color: 1
Size: 290031 Color: 0
Size: 275289 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 434693 Color: 1
Size: 287269 Color: 0
Size: 278039 Color: 1

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 434748 Color: 0
Size: 283561 Color: 0
Size: 281692 Color: 1

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 434770 Color: 0
Size: 314705 Color: 1
Size: 250526 Color: 1

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 434725 Color: 1
Size: 306149 Color: 1
Size: 259127 Color: 0

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 434810 Color: 1
Size: 285857 Color: 0
Size: 279334 Color: 0

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 434874 Color: 1
Size: 292715 Color: 1
Size: 272412 Color: 0

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 434911 Color: 0
Size: 306737 Color: 1
Size: 258353 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 435136 Color: 1
Size: 314233 Color: 1
Size: 250632 Color: 0

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 435152 Color: 0
Size: 304477 Color: 1
Size: 260372 Color: 1

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 435174 Color: 0
Size: 312353 Color: 1
Size: 252474 Color: 0

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 435136 Color: 0
Size: 300046 Color: 0
Size: 264819 Color: 1

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 435197 Color: 0
Size: 285207 Color: 0
Size: 279597 Color: 1

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 435155 Color: 1
Size: 296274 Color: 0
Size: 268572 Color: 1

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 435185 Color: 1
Size: 288551 Color: 0
Size: 276265 Color: 1

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 435233 Color: 0
Size: 286964 Color: 0
Size: 277804 Color: 1

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 435213 Color: 1
Size: 300986 Color: 0
Size: 263802 Color: 1

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 435319 Color: 0
Size: 297721 Color: 0
Size: 266961 Color: 1

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 435260 Color: 1
Size: 311914 Color: 0
Size: 252827 Color: 1

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 435389 Color: 0
Size: 284096 Color: 0
Size: 280516 Color: 1

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 435290 Color: 1
Size: 312979 Color: 1
Size: 251732 Color: 0

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 435466 Color: 0
Size: 287497 Color: 1
Size: 277038 Color: 0

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 435489 Color: 0
Size: 307078 Color: 1
Size: 257434 Color: 1

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 435542 Color: 0
Size: 299158 Color: 0
Size: 265301 Color: 1

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 435529 Color: 1
Size: 311444 Color: 0
Size: 253028 Color: 1

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 435565 Color: 0
Size: 285367 Color: 0
Size: 279069 Color: 1

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 435535 Color: 1
Size: 289653 Color: 1
Size: 274813 Color: 0

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 435605 Color: 0
Size: 283377 Color: 0
Size: 281019 Color: 1

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 435651 Color: 0
Size: 309796 Color: 1
Size: 254554 Color: 1

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 435826 Color: 0
Size: 299052 Color: 1
Size: 265123 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 435828 Color: 1
Size: 290115 Color: 0
Size: 274058 Color: 1

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 435893 Color: 0
Size: 291073 Color: 0
Size: 273035 Color: 1

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 435897 Color: 1
Size: 312162 Color: 0
Size: 251942 Color: 1

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 436024 Color: 0
Size: 285881 Color: 1
Size: 278096 Color: 0

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 435967 Color: 1
Size: 309521 Color: 0
Size: 254513 Color: 1

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 436044 Color: 0
Size: 302733 Color: 1
Size: 261224 Color: 1

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 435976 Color: 1
Size: 287376 Color: 1
Size: 276649 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 436049 Color: 0
Size: 284289 Color: 0
Size: 279663 Color: 1

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 436071 Color: 0
Size: 287143 Color: 1
Size: 276787 Color: 0

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 436253 Color: 0
Size: 300816 Color: 0
Size: 262932 Color: 1

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 436256 Color: 0
Size: 289396 Color: 0
Size: 274349 Color: 1

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 436270 Color: 0
Size: 301320 Color: 1
Size: 262411 Color: 1

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 436274 Color: 0
Size: 301651 Color: 1
Size: 262076 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 436423 Color: 1
Size: 300262 Color: 0
Size: 263316 Color: 0

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 436514 Color: 0
Size: 295193 Color: 1
Size: 268294 Color: 1

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 436554 Color: 0
Size: 303152 Color: 1
Size: 260295 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 436597 Color: 0
Size: 296874 Color: 1
Size: 266530 Color: 0

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 436548 Color: 1
Size: 302663 Color: 1
Size: 260790 Color: 0

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 436616 Color: 0
Size: 310088 Color: 0
Size: 253297 Color: 1

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 436778 Color: 0
Size: 306171 Color: 1
Size: 257052 Color: 1

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 436802 Color: 0
Size: 312596 Color: 1
Size: 250603 Color: 0

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 436796 Color: 1
Size: 299617 Color: 1
Size: 263588 Color: 0

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 436811 Color: 1
Size: 296474 Color: 1
Size: 266716 Color: 0

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 437194 Color: 0
Size: 306676 Color: 1
Size: 256131 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 437290 Color: 1
Size: 289031 Color: 0
Size: 273680 Color: 1

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 437309 Color: 0
Size: 300105 Color: 1
Size: 262587 Color: 0

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 437312 Color: 1
Size: 303537 Color: 0
Size: 259152 Color: 1

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 437386 Color: 0
Size: 310345 Color: 0
Size: 252270 Color: 1

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 437408 Color: 1
Size: 305265 Color: 1
Size: 257328 Color: 0

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 437445 Color: 0
Size: 285143 Color: 1
Size: 277413 Color: 0

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 437442 Color: 1
Size: 297107 Color: 0
Size: 265452 Color: 1

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 437653 Color: 0
Size: 310352 Color: 1
Size: 251996 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 437595 Color: 1
Size: 282687 Color: 1
Size: 279719 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 437702 Color: 0
Size: 283734 Color: 1
Size: 278565 Color: 0

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 437687 Color: 1
Size: 288243 Color: 0
Size: 274071 Color: 1

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 437852 Color: 0
Size: 302066 Color: 1
Size: 260083 Color: 1

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 437883 Color: 0
Size: 302083 Color: 0
Size: 260035 Color: 1

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 437849 Color: 1
Size: 304949 Color: 1
Size: 257203 Color: 0

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 437963 Color: 0
Size: 308508 Color: 1
Size: 253530 Color: 1

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 438030 Color: 1
Size: 287611 Color: 0
Size: 274360 Color: 1

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 438067 Color: 1
Size: 286545 Color: 0
Size: 275389 Color: 1

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 438103 Color: 0
Size: 297679 Color: 0
Size: 264219 Color: 1

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 438129 Color: 0
Size: 308453 Color: 1
Size: 253419 Color: 1

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 438223 Color: 1
Size: 284102 Color: 0
Size: 277676 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 438229 Color: 1
Size: 288002 Color: 0
Size: 273770 Color: 1

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 438254 Color: 1
Size: 297254 Color: 0
Size: 264493 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 438359 Color: 0
Size: 282610 Color: 1
Size: 279032 Color: 1

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 438359 Color: 0
Size: 292661 Color: 1
Size: 268981 Color: 0

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 438359 Color: 1
Size: 287070 Color: 1
Size: 274572 Color: 0

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 438383 Color: 0
Size: 300600 Color: 1
Size: 261018 Color: 0

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 438368 Color: 1
Size: 290614 Color: 1
Size: 271019 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 438429 Color: 0
Size: 291375 Color: 0
Size: 270197 Color: 1

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 438452 Color: 1
Size: 298727 Color: 0
Size: 262822 Color: 1

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 438432 Color: 0
Size: 288032 Color: 0
Size: 273537 Color: 1

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 438593 Color: 1
Size: 311048 Color: 0
Size: 250360 Color: 1

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 438727 Color: 1
Size: 286464 Color: 0
Size: 274810 Color: 1

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 438727 Color: 0
Size: 310506 Color: 1
Size: 250768 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 438731 Color: 1
Size: 307036 Color: 1
Size: 254234 Color: 0

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 438762 Color: 0
Size: 285661 Color: 0
Size: 275578 Color: 1

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 438922 Color: 0
Size: 306394 Color: 1
Size: 254685 Color: 1

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 439000 Color: 0
Size: 296285 Color: 1
Size: 264716 Color: 0

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 439030 Color: 1
Size: 297392 Color: 0
Size: 263579 Color: 1

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 439027 Color: 0
Size: 309844 Color: 0
Size: 251130 Color: 1

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 439114 Color: 0
Size: 296654 Color: 0
Size: 264233 Color: 1

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 439095 Color: 1
Size: 280913 Color: 1
Size: 279993 Color: 0

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 439169 Color: 0
Size: 303449 Color: 0
Size: 257383 Color: 1

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 439202 Color: 1
Size: 285509 Color: 1
Size: 275290 Color: 0

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 439286 Color: 1
Size: 297001 Color: 0
Size: 263714 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 439237 Color: 0
Size: 300823 Color: 1
Size: 259941 Color: 0

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 439385 Color: 0
Size: 294579 Color: 1
Size: 266037 Color: 1

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 439671 Color: 1
Size: 305280 Color: 0
Size: 255050 Color: 1

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 439634 Color: 0
Size: 298390 Color: 0
Size: 261977 Color: 1

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 439745 Color: 1
Size: 306168 Color: 0
Size: 254088 Color: 1

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 439794 Color: 1
Size: 303900 Color: 0
Size: 256307 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 439818 Color: 1
Size: 300162 Color: 1
Size: 260021 Color: 0

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 439869 Color: 1
Size: 285031 Color: 1
Size: 275101 Color: 0

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 439831 Color: 0
Size: 286508 Color: 1
Size: 273662 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 439884 Color: 0
Size: 307578 Color: 1
Size: 252539 Color: 1

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 439931 Color: 0
Size: 294752 Color: 1
Size: 265318 Color: 1

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 439965 Color: 0
Size: 306990 Color: 1
Size: 253046 Color: 0

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 439983 Color: 0
Size: 306868 Color: 1
Size: 253150 Color: 1

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 440270 Color: 1
Size: 287083 Color: 0
Size: 272648 Color: 1

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 440164 Color: 0
Size: 284895 Color: 0
Size: 274942 Color: 1

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 440245 Color: 0
Size: 281287 Color: 1
Size: 278469 Color: 0

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 440291 Color: 1
Size: 292348 Color: 1
Size: 267362 Color: 0

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 440250 Color: 0
Size: 308247 Color: 0
Size: 251504 Color: 1

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 440446 Color: 0
Size: 288754 Color: 1
Size: 270801 Color: 1

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 440503 Color: 0
Size: 298564 Color: 1
Size: 260934 Color: 0

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 440543 Color: 1
Size: 283313 Color: 0
Size: 276145 Color: 1

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 440559 Color: 1
Size: 298472 Color: 0
Size: 260970 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 440667 Color: 1
Size: 307590 Color: 0
Size: 251744 Color: 1

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 440823 Color: 1
Size: 305260 Color: 0
Size: 253918 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 440867 Color: 1
Size: 296466 Color: 0
Size: 262668 Color: 1

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 440905 Color: 0
Size: 292477 Color: 1
Size: 266619 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 440927 Color: 1
Size: 282806 Color: 0
Size: 276268 Color: 1

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 440949 Color: 0
Size: 300238 Color: 0
Size: 258814 Color: 1

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 440984 Color: 1
Size: 307896 Color: 0
Size: 251121 Color: 1

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 441156 Color: 0
Size: 305469 Color: 1
Size: 253376 Color: 0

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 441231 Color: 1
Size: 294348 Color: 1
Size: 264422 Color: 0

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 441247 Color: 0
Size: 284218 Color: 1
Size: 274536 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 441244 Color: 1
Size: 307964 Color: 0
Size: 250793 Color: 0

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 441333 Color: 0
Size: 282390 Color: 0
Size: 276278 Color: 1

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 441277 Color: 1
Size: 295538 Color: 1
Size: 263186 Color: 0

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 441385 Color: 0
Size: 307852 Color: 1
Size: 250764 Color: 0

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 441407 Color: 1
Size: 287508 Color: 1
Size: 271086 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 441411 Color: 0
Size: 279778 Color: 0
Size: 278812 Color: 1

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 441416 Color: 1
Size: 286760 Color: 1
Size: 271825 Color: 0

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 441452 Color: 0
Size: 280954 Color: 0
Size: 277595 Color: 1

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 441426 Color: 1
Size: 297011 Color: 0
Size: 261564 Color: 1

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 441500 Color: 0
Size: 306777 Color: 1
Size: 251724 Color: 0

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 441669 Color: 0
Size: 288937 Color: 1
Size: 269395 Color: 1

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 441734 Color: 0
Size: 280447 Color: 0
Size: 277820 Color: 1

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 441762 Color: 0
Size: 283063 Color: 1
Size: 275176 Color: 1

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 441830 Color: 0
Size: 294717 Color: 1
Size: 263454 Color: 1

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 441970 Color: 0
Size: 307283 Color: 0
Size: 250748 Color: 1

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 441907 Color: 1
Size: 307213 Color: 0
Size: 250881 Color: 1

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 442047 Color: 1
Size: 280108 Color: 1
Size: 277846 Color: 0

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 442226 Color: 0
Size: 292658 Color: 1
Size: 265117 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 442382 Color: 0
Size: 294368 Color: 1
Size: 263251 Color: 1

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 442387 Color: 0
Size: 306282 Color: 0
Size: 251332 Color: 1

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 442645 Color: 0
Size: 303864 Color: 1
Size: 253492 Color: 0

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 442759 Color: 1
Size: 298400 Color: 0
Size: 258842 Color: 1

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 442763 Color: 1
Size: 279491 Color: 0
Size: 277747 Color: 0

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 443045 Color: 1
Size: 286926 Color: 0
Size: 270030 Color: 1

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 443128 Color: 0
Size: 294324 Color: 0
Size: 262549 Color: 1

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 443049 Color: 1
Size: 287121 Color: 1
Size: 269831 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 443192 Color: 0
Size: 297245 Color: 1
Size: 259564 Color: 0

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 443211 Color: 1
Size: 286732 Color: 1
Size: 270058 Color: 0

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 443252 Color: 0
Size: 282435 Color: 0
Size: 274314 Color: 1

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 443286 Color: 1
Size: 280616 Color: 1
Size: 276099 Color: 0

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 443254 Color: 0
Size: 290615 Color: 1
Size: 266132 Color: 0

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 443331 Color: 1
Size: 287382 Color: 1
Size: 269288 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 443370 Color: 1
Size: 280840 Color: 0
Size: 275791 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 443404 Color: 1
Size: 287480 Color: 1
Size: 269117 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 443414 Color: 1
Size: 296164 Color: 0
Size: 260423 Color: 0

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 443447 Color: 1
Size: 304327 Color: 1
Size: 252227 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 443540 Color: 1
Size: 299610 Color: 0
Size: 256851 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 443556 Color: 1
Size: 303406 Color: 1
Size: 253039 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 443635 Color: 0
Size: 303340 Color: 1
Size: 253026 Color: 0

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 443700 Color: 0
Size: 305749 Color: 1
Size: 250552 Color: 0

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 443721 Color: 0
Size: 284452 Color: 1
Size: 271828 Color: 1

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 443790 Color: 0
Size: 296204 Color: 0
Size: 260007 Color: 1

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 443843 Color: 0
Size: 292025 Color: 1
Size: 264133 Color: 0

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 443940 Color: 1
Size: 305294 Color: 0
Size: 250767 Color: 0

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 443960 Color: 1
Size: 281748 Color: 0
Size: 274293 Color: 1

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 443965 Color: 0
Size: 291757 Color: 0
Size: 264279 Color: 1

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 444051 Color: 0
Size: 281833 Color: 1
Size: 274117 Color: 1

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 444179 Color: 1
Size: 292427 Color: 0
Size: 263395 Color: 1

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 444295 Color: 0
Size: 283167 Color: 1
Size: 272539 Color: 1

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 444309 Color: 0
Size: 289026 Color: 0
Size: 266666 Color: 1

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 444394 Color: 1
Size: 281212 Color: 0
Size: 274395 Color: 1

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 444406 Color: 1
Size: 295009 Color: 0
Size: 260586 Color: 0

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 444419 Color: 1
Size: 281036 Color: 1
Size: 274546 Color: 0

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 444408 Color: 0
Size: 285804 Color: 1
Size: 269789 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 444426 Color: 1
Size: 283344 Color: 0
Size: 272231 Color: 1

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 444462 Color: 0
Size: 277916 Color: 1
Size: 277623 Color: 0

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 444478 Color: 0
Size: 300709 Color: 1
Size: 254814 Color: 1

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 444515 Color: 0
Size: 299807 Color: 1
Size: 255679 Color: 0

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 444498 Color: 1
Size: 290667 Color: 1
Size: 264836 Color: 0

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 444750 Color: 1
Size: 304456 Color: 0
Size: 250795 Color: 1

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 444849 Color: 0
Size: 288270 Color: 1
Size: 266882 Color: 0

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 444827 Color: 1
Size: 303083 Color: 1
Size: 252091 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 444942 Color: 0
Size: 289348 Color: 1
Size: 265711 Color: 0

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 445038 Color: 1
Size: 304281 Color: 0
Size: 250682 Color: 1

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 445127 Color: 0
Size: 290942 Color: 1
Size: 263932 Color: 1

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 445127 Color: 0
Size: 281676 Color: 0
Size: 273198 Color: 1

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 445199 Color: 0
Size: 288496 Color: 1
Size: 266306 Color: 1

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 445429 Color: 0
Size: 301660 Color: 0
Size: 252912 Color: 1

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 445493 Color: 1
Size: 279296 Color: 1
Size: 275212 Color: 0

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 445766 Color: 0
Size: 300621 Color: 0
Size: 253614 Color: 1

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 445866 Color: 1
Size: 300987 Color: 0
Size: 253148 Color: 1

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 445930 Color: 0
Size: 280569 Color: 0
Size: 273502 Color: 1

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 446020 Color: 0
Size: 278577 Color: 1
Size: 275404 Color: 1

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 446042 Color: 0
Size: 286145 Color: 1
Size: 267814 Color: 0

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 446056 Color: 0
Size: 285150 Color: 1
Size: 268795 Color: 1

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 446065 Color: 0
Size: 290730 Color: 1
Size: 263206 Color: 0

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 446144 Color: 1
Size: 292772 Color: 1
Size: 261085 Color: 0

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 446134 Color: 0
Size: 281641 Color: 0
Size: 272226 Color: 1

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 446240 Color: 1
Size: 292543 Color: 0
Size: 261218 Color: 1

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 446369 Color: 0
Size: 279472 Color: 0
Size: 274160 Color: 1

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 446407 Color: 0
Size: 295182 Color: 1
Size: 258412 Color: 1

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 446504 Color: 0
Size: 297881 Color: 0
Size: 255616 Color: 1

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 446555 Color: 1
Size: 302897 Color: 0
Size: 250549 Color: 1

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 446572 Color: 0
Size: 289917 Color: 0
Size: 263512 Color: 1

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 446575 Color: 1
Size: 285976 Color: 1
Size: 267450 Color: 0

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 446588 Color: 0
Size: 290137 Color: 0
Size: 263276 Color: 1

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 446578 Color: 1
Size: 281393 Color: 0
Size: 272030 Color: 1

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 446633 Color: 0
Size: 286960 Color: 0
Size: 266408 Color: 1

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 446811 Color: 1
Size: 286719 Color: 1
Size: 266471 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 446830 Color: 1
Size: 301869 Color: 0
Size: 251302 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 446858 Color: 1
Size: 282522 Color: 0
Size: 270621 Color: 1

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 446771 Color: 0
Size: 292123 Color: 1
Size: 261107 Color: 0

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 446873 Color: 1
Size: 300540 Color: 1
Size: 252588 Color: 0

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 446910 Color: 1
Size: 281094 Color: 0
Size: 271997 Color: 0

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 447047 Color: 1
Size: 281320 Color: 1
Size: 271634 Color: 0

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 447224 Color: 1
Size: 284517 Color: 0
Size: 268260 Color: 0

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 447269 Color: 0
Size: 296368 Color: 1
Size: 256364 Color: 1

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 447311 Color: 1
Size: 281796 Color: 1
Size: 270894 Color: 0

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 447325 Color: 0
Size: 297692 Color: 1
Size: 254984 Color: 0

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 447340 Color: 1
Size: 298645 Color: 1
Size: 254016 Color: 0

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 447505 Color: 0
Size: 286377 Color: 1
Size: 266119 Color: 0

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 447549 Color: 0
Size: 295918 Color: 1
Size: 256534 Color: 1

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 447579 Color: 0
Size: 298550 Color: 0
Size: 253872 Color: 1

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 447609 Color: 1
Size: 291570 Color: 1
Size: 260822 Color: 0

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 447881 Color: 0
Size: 297750 Color: 0
Size: 254370 Color: 1

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 447895 Color: 0
Size: 276070 Color: 1
Size: 276036 Color: 1

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 447972 Color: 0
Size: 300844 Color: 1
Size: 251185 Color: 1

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 448242 Color: 0
Size: 294123 Color: 0
Size: 257636 Color: 1

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 448276 Color: 0
Size: 301580 Color: 0
Size: 250145 Color: 1

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 448347 Color: 1
Size: 283429 Color: 0
Size: 268225 Color: 0

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 448413 Color: 0
Size: 279252 Color: 0
Size: 272336 Color: 1

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 448447 Color: 0
Size: 290931 Color: 0
Size: 260623 Color: 1

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 448385 Color: 1
Size: 282129 Color: 1
Size: 269487 Color: 0

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 448454 Color: 0
Size: 296259 Color: 0
Size: 255288 Color: 1

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 448396 Color: 1
Size: 289205 Color: 1
Size: 262400 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 448484 Color: 1
Size: 284353 Color: 0
Size: 267164 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 448565 Color: 1
Size: 281625 Color: 0
Size: 269811 Color: 1

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 448555 Color: 0
Size: 292831 Color: 0
Size: 258615 Color: 1

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 448616 Color: 1
Size: 277831 Color: 1
Size: 273554 Color: 0

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 448661 Color: 1
Size: 281050 Color: 1
Size: 270290 Color: 0

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 448748 Color: 1
Size: 293882 Color: 0
Size: 257371 Color: 1

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 448785 Color: 0
Size: 290573 Color: 0
Size: 260643 Color: 1

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 448856 Color: 0
Size: 281584 Color: 1
Size: 269561 Color: 1

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 448916 Color: 1
Size: 292222 Color: 0
Size: 258863 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 448967 Color: 1
Size: 277973 Color: 0
Size: 273061 Color: 1

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 449066 Color: 1
Size: 291084 Color: 0
Size: 259851 Color: 0

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 449113 Color: 1
Size: 296979 Color: 0
Size: 253909 Color: 1

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 449172 Color: 1
Size: 281968 Color: 0
Size: 268861 Color: 0

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 449186 Color: 0
Size: 289649 Color: 0
Size: 261166 Color: 1

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 449224 Color: 1
Size: 295103 Color: 1
Size: 255674 Color: 0

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 449232 Color: 0
Size: 298484 Color: 1
Size: 252285 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 449288 Color: 1
Size: 295534 Color: 1
Size: 255179 Color: 0

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 449382 Color: 1
Size: 289528 Color: 0
Size: 261091 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 449396 Color: 1
Size: 277100 Color: 1
Size: 273505 Color: 0

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 449398 Color: 1
Size: 282968 Color: 0
Size: 267635 Color: 1

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 449418 Color: 1
Size: 290882 Color: 0
Size: 259701 Color: 1

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 449404 Color: 0
Size: 298166 Color: 0
Size: 252431 Color: 1

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 449487 Color: 1
Size: 299517 Color: 1
Size: 250997 Color: 0

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 449510 Color: 1
Size: 286338 Color: 1
Size: 264153 Color: 0

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 449610 Color: 0
Size: 293949 Color: 1
Size: 256442 Color: 0

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 449627 Color: 0
Size: 300112 Color: 1
Size: 250262 Color: 1

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 449639 Color: 0
Size: 287302 Color: 0
Size: 263060 Color: 1

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 449640 Color: 1
Size: 279603 Color: 0
Size: 270758 Color: 1

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 449651 Color: 0
Size: 295626 Color: 1
Size: 254724 Color: 0

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 449827 Color: 0
Size: 281420 Color: 1
Size: 268754 Color: 1

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 449916 Color: 0
Size: 294661 Color: 1
Size: 255424 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 449942 Color: 0
Size: 284863 Color: 1
Size: 265196 Color: 1

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 450000 Color: 0
Size: 287619 Color: 1
Size: 262382 Color: 0

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 450006 Color: 0
Size: 294805 Color: 1
Size: 255190 Color: 1

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 449996 Color: 1
Size: 276678 Color: 0
Size: 273327 Color: 1

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 450057 Color: 0
Size: 296952 Color: 1
Size: 252992 Color: 0

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 450146 Color: 0
Size: 287342 Color: 0
Size: 262513 Color: 1

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 450105 Color: 1
Size: 282069 Color: 0
Size: 267827 Color: 1

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 450248 Color: 0
Size: 299063 Color: 1
Size: 250690 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 450425 Color: 0
Size: 297330 Color: 0
Size: 252246 Color: 1

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 450377 Color: 1
Size: 276732 Color: 1
Size: 272892 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 450435 Color: 0
Size: 285940 Color: 0
Size: 263626 Color: 1

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 450576 Color: 0
Size: 297710 Color: 1
Size: 251715 Color: 1

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 450588 Color: 0
Size: 274964 Color: 1
Size: 274449 Color: 0

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 450658 Color: 0
Size: 275802 Color: 1
Size: 273541 Color: 1

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 450697 Color: 0
Size: 294616 Color: 1
Size: 254688 Color: 1

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 450809 Color: 0
Size: 277190 Color: 1
Size: 272002 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 450819 Color: 0
Size: 282795 Color: 1
Size: 266387 Color: 1

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 450942 Color: 0
Size: 294538 Color: 0
Size: 254521 Color: 1

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 450889 Color: 1
Size: 288658 Color: 0
Size: 260454 Color: 1

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 451162 Color: 1
Size: 291771 Color: 0
Size: 257068 Color: 1

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 451124 Color: 0
Size: 279315 Color: 1
Size: 269562 Color: 0

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 451239 Color: 1
Size: 291604 Color: 1
Size: 257158 Color: 0

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 451329 Color: 1
Size: 284698 Color: 0
Size: 263974 Color: 1

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 451297 Color: 0
Size: 288425 Color: 0
Size: 260279 Color: 1

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 451477 Color: 0
Size: 289451 Color: 1
Size: 259073 Color: 1

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 451583 Color: 1
Size: 297352 Color: 0
Size: 251066 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 451670 Color: 0
Size: 281399 Color: 1
Size: 266932 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 451734 Color: 0
Size: 296050 Color: 1
Size: 252217 Color: 1

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 451755 Color: 0
Size: 276945 Color: 0
Size: 271301 Color: 1

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 451785 Color: 0
Size: 289484 Color: 1
Size: 258732 Color: 1

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 451865 Color: 1
Size: 289924 Color: 0
Size: 258212 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 451931 Color: 1
Size: 289759 Color: 0
Size: 258311 Color: 1

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 451953 Color: 0
Size: 275800 Color: 1
Size: 272248 Color: 0

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 452076 Color: 1
Size: 287445 Color: 1
Size: 260480 Color: 0

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 452030 Color: 0
Size: 294366 Color: 1
Size: 253605 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 452197 Color: 1
Size: 280621 Color: 1
Size: 267183 Color: 0

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 452239 Color: 0
Size: 281211 Color: 0
Size: 266551 Color: 1

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 452244 Color: 1
Size: 291185 Color: 1
Size: 256572 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 452309 Color: 0
Size: 281874 Color: 0
Size: 265818 Color: 1

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 452392 Color: 0
Size: 274292 Color: 1
Size: 273317 Color: 1

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 452542 Color: 1
Size: 290763 Color: 0
Size: 256696 Color: 0

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 452582 Color: 0
Size: 288292 Color: 1
Size: 259127 Color: 0

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 452728 Color: 0
Size: 288957 Color: 0
Size: 258316 Color: 1

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 452759 Color: 1
Size: 296788 Color: 1
Size: 250454 Color: 0

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 452815 Color: 1
Size: 287822 Color: 1
Size: 259364 Color: 0

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 452914 Color: 0
Size: 294872 Color: 0
Size: 252215 Color: 1

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 453023 Color: 1
Size: 277163 Color: 0
Size: 269815 Color: 0

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 453025 Color: 1
Size: 293337 Color: 0
Size: 253639 Color: 1

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 452986 Color: 0
Size: 292431 Color: 0
Size: 254584 Color: 1

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 453055 Color: 0
Size: 289760 Color: 1
Size: 257186 Color: 1

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 453057 Color: 0
Size: 279337 Color: 0
Size: 267607 Color: 1

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 453117 Color: 0
Size: 281659 Color: 1
Size: 265225 Color: 1

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 453232 Color: 1
Size: 288368 Color: 0
Size: 258401 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 453266 Color: 1
Size: 292069 Color: 1
Size: 254666 Color: 0

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 453285 Color: 0
Size: 283794 Color: 1
Size: 262922 Color: 0

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 453375 Color: 1
Size: 288659 Color: 1
Size: 257967 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 453442 Color: 0
Size: 282818 Color: 1
Size: 263741 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 453493 Color: 1
Size: 288225 Color: 0
Size: 258283 Color: 1

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 453584 Color: 1
Size: 273422 Color: 0
Size: 272995 Color: 1

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 453622 Color: 0
Size: 276411 Color: 1
Size: 269968 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 453799 Color: 1
Size: 284741 Color: 0
Size: 261461 Color: 0

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 453832 Color: 1
Size: 289430 Color: 0
Size: 256739 Color: 1

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 453864 Color: 0
Size: 291481 Color: 0
Size: 254656 Color: 1

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 453933 Color: 1
Size: 285382 Color: 0
Size: 260686 Color: 1

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 453870 Color: 0
Size: 282131 Color: 0
Size: 264000 Color: 1

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 453913 Color: 0
Size: 279734 Color: 0
Size: 266354 Color: 1

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 453983 Color: 0
Size: 291880 Color: 1
Size: 254138 Color: 1

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 454044 Color: 0
Size: 294238 Color: 0
Size: 251719 Color: 1

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 454041 Color: 1
Size: 278182 Color: 0
Size: 267778 Color: 1

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 454048 Color: 0
Size: 292450 Color: 0
Size: 253503 Color: 1

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 454060 Color: 1
Size: 284043 Color: 0
Size: 261898 Color: 1

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 454241 Color: 1
Size: 280766 Color: 0
Size: 264994 Color: 0

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 454408 Color: 0
Size: 273915 Color: 1
Size: 271678 Color: 1

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 454712 Color: 0
Size: 281073 Color: 1
Size: 264216 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 454717 Color: 0
Size: 285330 Color: 1
Size: 259954 Color: 1

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 454728 Color: 0
Size: 294903 Color: 1
Size: 250370 Color: 0

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 454730 Color: 0
Size: 279992 Color: 1
Size: 265279 Color: 1

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 454732 Color: 0
Size: 293260 Color: 1
Size: 252009 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 454667 Color: 1
Size: 288373 Color: 0
Size: 256961 Color: 0

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 454872 Color: 1
Size: 292250 Color: 0
Size: 252879 Color: 0

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 454969 Color: 1
Size: 282286 Color: 0
Size: 262746 Color: 1

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 454988 Color: 0
Size: 291639 Color: 0
Size: 253374 Color: 1

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 455065 Color: 1
Size: 287975 Color: 0
Size: 256961 Color: 1

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 455072 Color: 1
Size: 291793 Color: 1
Size: 253136 Color: 0

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 455104 Color: 1
Size: 290555 Color: 1
Size: 254342 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 455200 Color: 0
Size: 281604 Color: 0
Size: 263197 Color: 1

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 455170 Color: 1
Size: 280138 Color: 0
Size: 264693 Color: 1

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 455356 Color: 1
Size: 278361 Color: 0
Size: 266284 Color: 1

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 455556 Color: 0
Size: 279710 Color: 1
Size: 264735 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 455590 Color: 1
Size: 279479 Color: 0
Size: 264932 Color: 1

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 456027 Color: 0
Size: 283328 Color: 0
Size: 260646 Color: 1

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 456018 Color: 1
Size: 281748 Color: 1
Size: 262235 Color: 0

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 456074 Color: 0
Size: 277948 Color: 1
Size: 265979 Color: 0

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 456082 Color: 1
Size: 292824 Color: 1
Size: 251095 Color: 0

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 456113 Color: 0
Size: 283077 Color: 1
Size: 260811 Color: 1

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 456090 Color: 1
Size: 288451 Color: 0
Size: 255460 Color: 1

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 456193 Color: 0
Size: 281315 Color: 1
Size: 262493 Color: 0

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 456143 Color: 1
Size: 280674 Color: 1
Size: 263184 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 456234 Color: 0
Size: 283061 Color: 0
Size: 260706 Color: 1

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 456282 Color: 0
Size: 290326 Color: 1
Size: 253393 Color: 0

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 456344 Color: 1
Size: 293365 Color: 0
Size: 250292 Color: 1

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 456484 Color: 0
Size: 285389 Color: 1
Size: 258128 Color: 1

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 456559 Color: 0
Size: 287856 Color: 1
Size: 255586 Color: 0

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 456568 Color: 0
Size: 292873 Color: 1
Size: 250560 Color: 1

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 456637 Color: 0
Size: 292499 Color: 1
Size: 250865 Color: 1

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 456677 Color: 0
Size: 278867 Color: 1
Size: 264457 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 456682 Color: 0
Size: 285533 Color: 1
Size: 257786 Color: 1

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 456692 Color: 0
Size: 292516 Color: 1
Size: 250793 Color: 1

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 456740 Color: 0
Size: 281239 Color: 1
Size: 262022 Color: 1

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 456787 Color: 1
Size: 286450 Color: 1
Size: 256764 Color: 0

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 456860 Color: 0
Size: 285394 Color: 0
Size: 257747 Color: 1

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 456843 Color: 1
Size: 292461 Color: 1
Size: 250697 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 457054 Color: 1
Size: 276243 Color: 1
Size: 266704 Color: 0

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 457011 Color: 0
Size: 272107 Color: 0
Size: 270883 Color: 1

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 457057 Color: 1
Size: 272014 Color: 1
Size: 270930 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 457074 Color: 0
Size: 274321 Color: 1
Size: 268606 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 457093 Color: 1
Size: 278995 Color: 0
Size: 263913 Color: 1

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 457107 Color: 0
Size: 286834 Color: 0
Size: 256060 Color: 1

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 457160 Color: 0
Size: 291235 Color: 1
Size: 251606 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 457170 Color: 0
Size: 286954 Color: 1
Size: 255877 Color: 0

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 457145 Color: 1
Size: 287734 Color: 0
Size: 255122 Color: 1

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 457232 Color: 0
Size: 284846 Color: 0
Size: 257923 Color: 1

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 457214 Color: 1
Size: 282367 Color: 0
Size: 260420 Color: 1

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 457274 Color: 0
Size: 287338 Color: 1
Size: 255389 Color: 0

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 457414 Color: 1
Size: 273470 Color: 0
Size: 269117 Color: 1

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 457525 Color: 1
Size: 289466 Color: 1
Size: 253010 Color: 0

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 457528 Color: 1
Size: 271600 Color: 0
Size: 270873 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 457573 Color: 1
Size: 286039 Color: 1
Size: 256389 Color: 0

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 457776 Color: 0
Size: 291470 Color: 1
Size: 250755 Color: 0

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 457993 Color: 1
Size: 272114 Color: 0
Size: 269894 Color: 0

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 458001 Color: 1
Size: 281692 Color: 1
Size: 260308 Color: 0

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 458204 Color: 1
Size: 284634 Color: 0
Size: 257163 Color: 1

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 458264 Color: 0
Size: 291457 Color: 1
Size: 250280 Color: 0

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 458255 Color: 1
Size: 287425 Color: 1
Size: 254321 Color: 0

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 458342 Color: 0
Size: 287325 Color: 1
Size: 254334 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 458496 Color: 1
Size: 283277 Color: 0
Size: 258228 Color: 1

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 458545 Color: 1
Size: 281365 Color: 0
Size: 260091 Color: 0

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 458548 Color: 1
Size: 271632 Color: 1
Size: 269821 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 458590 Color: 1
Size: 284636 Color: 1
Size: 256775 Color: 0

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 458626 Color: 1
Size: 283099 Color: 0
Size: 258276 Color: 0

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 458633 Color: 1
Size: 283630 Color: 1
Size: 257738 Color: 0

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 458709 Color: 1
Size: 284929 Color: 0
Size: 256363 Color: 0

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 458802 Color: 0
Size: 285987 Color: 1
Size: 255212 Color: 1

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 458819 Color: 0
Size: 278497 Color: 1
Size: 262685 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 458922 Color: 1
Size: 281193 Color: 1
Size: 259886 Color: 0

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 459045 Color: 1
Size: 286049 Color: 0
Size: 254907 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 459144 Color: 0
Size: 287414 Color: 1
Size: 253443 Color: 0

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 459381 Color: 0
Size: 272470 Color: 1
Size: 268150 Color: 0

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 459453 Color: 1
Size: 280023 Color: 1
Size: 260525 Color: 0

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 459793 Color: 0
Size: 278594 Color: 0
Size: 261614 Color: 1

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 459969 Color: 1
Size: 284682 Color: 1
Size: 255350 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 460188 Color: 1
Size: 278576 Color: 0
Size: 261237 Color: 1

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 460213 Color: 0
Size: 288138 Color: 0
Size: 251650 Color: 1

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 460218 Color: 1
Size: 271516 Color: 0
Size: 268267 Color: 1

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 460214 Color: 0
Size: 270364 Color: 0
Size: 269423 Color: 1

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 460265 Color: 0
Size: 283989 Color: 1
Size: 255747 Color: 1

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 460357 Color: 0
Size: 269829 Color: 1
Size: 269815 Color: 0

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 460372 Color: 0
Size: 288932 Color: 1
Size: 250697 Color: 1

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 460447 Color: 0
Size: 288495 Color: 0
Size: 251059 Color: 1

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 460479 Color: 0
Size: 281765 Color: 1
Size: 257757 Color: 0

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 460479 Color: 1
Size: 284879 Color: 0
Size: 254643 Color: 1

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 460492 Color: 0
Size: 275820 Color: 1
Size: 263689 Color: 0

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 460523 Color: 1
Size: 276672 Color: 1
Size: 262806 Color: 0

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 460562 Color: 0
Size: 276476 Color: 1
Size: 262963 Color: 0

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 460628 Color: 1
Size: 273574 Color: 1
Size: 265799 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 460667 Color: 0
Size: 284777 Color: 0
Size: 254557 Color: 1

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 460658 Color: 1
Size: 272723 Color: 1
Size: 266620 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 460684 Color: 0
Size: 280499 Color: 0
Size: 258818 Color: 1

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 460777 Color: 0
Size: 284242 Color: 1
Size: 254982 Color: 1

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 460780 Color: 0
Size: 282300 Color: 1
Size: 256921 Color: 0

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 460792 Color: 0
Size: 281604 Color: 1
Size: 257605 Color: 1

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 460982 Color: 1
Size: 270258 Color: 0
Size: 268761 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 461049 Color: 1
Size: 279256 Color: 0
Size: 259696 Color: 1

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 460981 Color: 0
Size: 281170 Color: 0
Size: 257850 Color: 1

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 461080 Color: 1
Size: 283995 Color: 1
Size: 254926 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 460999 Color: 0
Size: 286291 Color: 0
Size: 252711 Color: 1

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 461115 Color: 1
Size: 276067 Color: 0
Size: 262819 Color: 0

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 461116 Color: 1
Size: 283574 Color: 1
Size: 255311 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 461125 Color: 1
Size: 285642 Color: 0
Size: 253234 Color: 0

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 461127 Color: 1
Size: 282409 Color: 0
Size: 256465 Color: 1

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 461147 Color: 0
Size: 280352 Color: 0
Size: 258502 Color: 1

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 461342 Color: 1
Size: 283947 Color: 1
Size: 254712 Color: 0

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 461278 Color: 0
Size: 279959 Color: 1
Size: 258764 Color: 0

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 461423 Color: 0
Size: 276237 Color: 1
Size: 262341 Color: 1

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 461368 Color: 1
Size: 281480 Color: 0
Size: 257153 Color: 1

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 461449 Color: 0
Size: 282037 Color: 1
Size: 256515 Color: 0

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 461495 Color: 1
Size: 285980 Color: 1
Size: 252526 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 461521 Color: 1
Size: 277391 Color: 0
Size: 261089 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 461556 Color: 1
Size: 286286 Color: 0
Size: 252159 Color: 1

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 461559 Color: 0
Size: 278137 Color: 1
Size: 260305 Color: 0

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 461619 Color: 0
Size: 288379 Color: 1
Size: 250003 Color: 0

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 461619 Color: 1
Size: 285263 Color: 1
Size: 253119 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 461686 Color: 0
Size: 280774 Color: 1
Size: 257541 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 461679 Color: 1
Size: 287538 Color: 1
Size: 250784 Color: 0

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 461754 Color: 0
Size: 272328 Color: 0
Size: 265919 Color: 1

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 461842 Color: 0
Size: 273371 Color: 1
Size: 264788 Color: 1

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 462025 Color: 0
Size: 284754 Color: 1
Size: 253222 Color: 0

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 462070 Color: 0
Size: 276181 Color: 1
Size: 261750 Color: 1

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 462158 Color: 0
Size: 283750 Color: 1
Size: 254093 Color: 0

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 462187 Color: 0
Size: 275630 Color: 1
Size: 262184 Color: 1

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 462260 Color: 1
Size: 286324 Color: 0
Size: 251417 Color: 0

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 462365 Color: 0
Size: 276179 Color: 0
Size: 261457 Color: 1

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 462412 Color: 1
Size: 279346 Color: 0
Size: 258243 Color: 0

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 462468 Color: 1
Size: 273353 Color: 0
Size: 264180 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 462746 Color: 1
Size: 273968 Color: 0
Size: 263287 Color: 0

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 462855 Color: 0
Size: 286296 Color: 1
Size: 250850 Color: 1

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 462857 Color: 0
Size: 285647 Color: 0
Size: 251497 Color: 1

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 462794 Color: 1
Size: 284751 Color: 0
Size: 252456 Color: 1

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 462848 Color: 1
Size: 276119 Color: 1
Size: 261034 Color: 0

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 462907 Color: 0
Size: 287078 Color: 0
Size: 250016 Color: 1

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 462970 Color: 0
Size: 276099 Color: 1
Size: 260932 Color: 1

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 463012 Color: 0
Size: 282700 Color: 0
Size: 254289 Color: 1

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 463042 Color: 0
Size: 284841 Color: 1
Size: 252118 Color: 1

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 463078 Color: 0
Size: 273023 Color: 1
Size: 263900 Color: 1

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 463095 Color: 0
Size: 274214 Color: 1
Size: 262692 Color: 1

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 463396 Color: 0
Size: 270673 Color: 1
Size: 265932 Color: 1

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 463407 Color: 0
Size: 280974 Color: 0
Size: 255620 Color: 1

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 463397 Color: 1
Size: 278445 Color: 1
Size: 258159 Color: 0

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 463585 Color: 0
Size: 268706 Color: 0
Size: 267710 Color: 1

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 463575 Color: 1
Size: 275844 Color: 0
Size: 260582 Color: 1

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 463616 Color: 0
Size: 283955 Color: 1
Size: 252430 Color: 0

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 463692 Color: 0
Size: 282591 Color: 1
Size: 253718 Color: 1

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 464055 Color: 0
Size: 285939 Color: 0
Size: 250007 Color: 1

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 463996 Color: 1
Size: 280098 Color: 0
Size: 255907 Color: 1

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 464060 Color: 0
Size: 277378 Color: 1
Size: 258563 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 464098 Color: 0
Size: 281983 Color: 1
Size: 253920 Color: 1

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 464236 Color: 1
Size: 274710 Color: 0
Size: 261055 Color: 0

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 464426 Color: 1
Size: 273337 Color: 1
Size: 262238 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 464455 Color: 0
Size: 283963 Color: 0
Size: 251583 Color: 1

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 464582 Color: 1
Size: 269107 Color: 0
Size: 266312 Color: 1

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 464663 Color: 0
Size: 277383 Color: 1
Size: 257955 Color: 0

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 464643 Color: 1
Size: 283744 Color: 1
Size: 251614 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 464705 Color: 0
Size: 283938 Color: 1
Size: 251358 Color: 0

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 464665 Color: 1
Size: 268571 Color: 1
Size: 266765 Color: 0

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 464787 Color: 0
Size: 283348 Color: 1
Size: 251866 Color: 0

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 464987 Color: 0
Size: 279879 Color: 1
Size: 255135 Color: 1

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 464997 Color: 0
Size: 269935 Color: 0
Size: 265069 Color: 1

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 465003 Color: 1
Size: 269580 Color: 0
Size: 265418 Color: 1

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 465031 Color: 0
Size: 277577 Color: 0
Size: 257393 Color: 1

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 465134 Color: 1
Size: 282646 Color: 0
Size: 252221 Color: 1

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 465135 Color: 1
Size: 283860 Color: 0
Size: 251006 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 465184 Color: 1
Size: 277662 Color: 1
Size: 257155 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 465154 Color: 0
Size: 282154 Color: 1
Size: 252693 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 465324 Color: 1
Size: 272229 Color: 1
Size: 262448 Color: 0

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 465292 Color: 0
Size: 277987 Color: 1
Size: 256722 Color: 0

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 465387 Color: 1
Size: 271856 Color: 1
Size: 262758 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 465428 Color: 1
Size: 280683 Color: 0
Size: 253890 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 465581 Color: 0
Size: 271717 Color: 1
Size: 262703 Color: 1

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 465653 Color: 1
Size: 279584 Color: 0
Size: 254764 Color: 0

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 465959 Color: 0
Size: 267731 Color: 0
Size: 266311 Color: 1

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 465959 Color: 1
Size: 278642 Color: 1
Size: 255400 Color: 0

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 465999 Color: 0
Size: 276982 Color: 0
Size: 257020 Color: 1

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 466029 Color: 1
Size: 282659 Color: 1
Size: 251313 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 466149 Color: 0
Size: 271790 Color: 1
Size: 262062 Color: 0

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 466159 Color: 0
Size: 280889 Color: 1
Size: 252953 Color: 1

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 466160 Color: 0
Size: 278509 Color: 0
Size: 255332 Color: 1

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 466242 Color: 1
Size: 268749 Color: 1
Size: 265010 Color: 0

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 466358 Color: 0
Size: 282377 Color: 0
Size: 251266 Color: 1

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 466399 Color: 1
Size: 267476 Color: 1
Size: 266126 Color: 0

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 466436 Color: 1
Size: 276208 Color: 0
Size: 257357 Color: 1

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 466494 Color: 1
Size: 271800 Color: 0
Size: 261707 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 466613 Color: 0
Size: 282138 Color: 1
Size: 251250 Color: 1

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 466695 Color: 0
Size: 269059 Color: 1
Size: 264247 Color: 0

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 466713 Color: 1
Size: 271877 Color: 1
Size: 261411 Color: 0

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 466849 Color: 1
Size: 273943 Color: 0
Size: 259209 Color: 1

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 467015 Color: 1
Size: 272386 Color: 0
Size: 260600 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 467172 Color: 1
Size: 272358 Color: 1
Size: 260471 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 467116 Color: 0
Size: 275503 Color: 0
Size: 257382 Color: 1

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 467234 Color: 1
Size: 277160 Color: 0
Size: 255607 Color: 1

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 467277 Color: 0
Size: 276961 Color: 1
Size: 255763 Color: 0

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 467648 Color: 1
Size: 273710 Color: 0
Size: 258643 Color: 1

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 467665 Color: 0
Size: 279204 Color: 1
Size: 253132 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 467665 Color: 1
Size: 281742 Color: 0
Size: 250594 Color: 1

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 467726 Color: 0
Size: 270951 Color: 0
Size: 261324 Color: 1

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 467763 Color: 1
Size: 271314 Color: 0
Size: 260924 Color: 1

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 467909 Color: 1
Size: 276135 Color: 0
Size: 255957 Color: 0

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 467916 Color: 1
Size: 277701 Color: 0
Size: 254384 Color: 1

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 467936 Color: 1
Size: 267882 Color: 0
Size: 264183 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 468056 Color: 0
Size: 270131 Color: 0
Size: 261814 Color: 1

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 468058 Color: 0
Size: 279803 Color: 1
Size: 252140 Color: 1

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 468070 Color: 0
Size: 279320 Color: 1
Size: 252611 Color: 0

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 468141 Color: 1
Size: 267182 Color: 1
Size: 264678 Color: 0

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 468252 Color: 0
Size: 274182 Color: 1
Size: 257567 Color: 0

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 468251 Color: 1
Size: 271060 Color: 0
Size: 260690 Color: 0

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 468411 Color: 0
Size: 277317 Color: 1
Size: 254273 Color: 1

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 468601 Color: 0
Size: 267302 Color: 0
Size: 264098 Color: 1

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 468609 Color: 0
Size: 269384 Color: 1
Size: 262008 Color: 1

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 468808 Color: 1
Size: 278963 Color: 0
Size: 252230 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 468818 Color: 1
Size: 281091 Color: 0
Size: 250092 Color: 1

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 468777 Color: 0
Size: 268826 Color: 1
Size: 262398 Color: 0

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 468981 Color: 1
Size: 276925 Color: 0
Size: 254095 Color: 1

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 468962 Color: 0
Size: 275683 Color: 0
Size: 255356 Color: 1

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 469120 Color: 0
Size: 276203 Color: 1
Size: 254678 Color: 1

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 469205 Color: 0
Size: 268346 Color: 1
Size: 262450 Color: 0

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 469209 Color: 0
Size: 269251 Color: 1
Size: 261541 Color: 1

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 469317 Color: 0
Size: 269317 Color: 1
Size: 261367 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 469376 Color: 1
Size: 270844 Color: 1
Size: 259781 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 469322 Color: 0
Size: 272238 Color: 1
Size: 258441 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 469343 Color: 0
Size: 267018 Color: 1
Size: 263640 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 469498 Color: 0
Size: 279431 Color: 1
Size: 251072 Color: 1

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 469503 Color: 1
Size: 270217 Color: 0
Size: 260281 Color: 1

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 469576 Color: 1
Size: 266063 Color: 0
Size: 264362 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 469680 Color: 1
Size: 279575 Color: 0
Size: 250746 Color: 1

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 469851 Color: 0
Size: 279694 Color: 1
Size: 250456 Color: 0

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 469772 Color: 1
Size: 274333 Color: 0
Size: 255896 Color: 1

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 469981 Color: 0
Size: 265449 Color: 0
Size: 264571 Color: 1

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 469896 Color: 1
Size: 277243 Color: 1
Size: 252862 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 470000 Color: 0
Size: 278916 Color: 0
Size: 251085 Color: 1

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 469937 Color: 1
Size: 279593 Color: 1
Size: 250471 Color: 0

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 470032 Color: 0
Size: 270847 Color: 0
Size: 259122 Color: 1

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 498531 Color: 1
Size: 250991 Color: 0
Size: 250479 Color: 1

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 470145 Color: 1
Size: 279091 Color: 0
Size: 250765 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 470250 Color: 1
Size: 271312 Color: 0
Size: 258439 Color: 1

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 470256 Color: 0
Size: 265543 Color: 1
Size: 264202 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 470440 Color: 0
Size: 272663 Color: 1
Size: 256898 Color: 1

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 470441 Color: 0
Size: 272339 Color: 0
Size: 257221 Color: 1

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 470375 Color: 1
Size: 273597 Color: 1
Size: 256029 Color: 0

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 470463 Color: 0
Size: 275177 Color: 0
Size: 254361 Color: 1

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 470863 Color: 0
Size: 272411 Color: 1
Size: 256727 Color: 1

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 470868 Color: 0
Size: 270767 Color: 0
Size: 258366 Color: 1

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 470799 Color: 1
Size: 278218 Color: 1
Size: 250984 Color: 0

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 470886 Color: 0
Size: 266683 Color: 1
Size: 262432 Color: 0

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 470935 Color: 0
Size: 269366 Color: 1
Size: 259700 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 470937 Color: 0
Size: 268852 Color: 0
Size: 260212 Color: 1

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 470929 Color: 1
Size: 269009 Color: 0
Size: 260063 Color: 1

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 471014 Color: 0
Size: 268275 Color: 0
Size: 260712 Color: 1

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 471011 Color: 1
Size: 265717 Color: 1
Size: 263273 Color: 0

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 471177 Color: 0
Size: 273272 Color: 1
Size: 255552 Color: 1

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 471241 Color: 0
Size: 276595 Color: 1
Size: 252165 Color: 0

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 471377 Color: 1
Size: 268316 Color: 1
Size: 260308 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 471423 Color: 1
Size: 269386 Color: 1
Size: 259192 Color: 0

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 471466 Color: 0
Size: 278292 Color: 1
Size: 250243 Color: 0

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 471517 Color: 1
Size: 267387 Color: 1
Size: 261097 Color: 0

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 471612 Color: 0
Size: 269637 Color: 1
Size: 258752 Color: 1

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 471685 Color: 0
Size: 274224 Color: 0
Size: 254092 Color: 1

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 471808 Color: 1
Size: 277154 Color: 1
Size: 251039 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 471857 Color: 0
Size: 276591 Color: 1
Size: 251553 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 471961 Color: 1
Size: 276678 Color: 1
Size: 251362 Color: 0

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 472023 Color: 0
Size: 265460 Color: 1
Size: 262518 Color: 0

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 472023 Color: 1
Size: 265509 Color: 0
Size: 262469 Color: 1

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 472043 Color: 0
Size: 277837 Color: 0
Size: 250121 Color: 1

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 472132 Color: 1
Size: 264491 Color: 0
Size: 263378 Color: 1

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 472127 Color: 0
Size: 269413 Color: 0
Size: 258461 Color: 1

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 472234 Color: 1
Size: 264507 Color: 0
Size: 263260 Color: 0

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 472401 Color: 0
Size: 273416 Color: 1
Size: 254184 Color: 1

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 472526 Color: 0
Size: 273876 Color: 0
Size: 253599 Color: 1

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 472514 Color: 1
Size: 270399 Color: 0
Size: 257088 Color: 1

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 472606 Color: 1
Size: 266922 Color: 0
Size: 260473 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 472699 Color: 0
Size: 273359 Color: 1
Size: 253943 Color: 1

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 472788 Color: 0
Size: 273078 Color: 1
Size: 254135 Color: 0

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 472932 Color: 1
Size: 275480 Color: 0
Size: 251589 Color: 0

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 473011 Color: 0
Size: 269700 Color: 1
Size: 257290 Color: 1

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 473097 Color: 0
Size: 265099 Color: 0
Size: 261805 Color: 1

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 473066 Color: 1
Size: 269729 Color: 1
Size: 257206 Color: 0

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 473201 Color: 1
Size: 266600 Color: 0
Size: 260200 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 473373 Color: 1
Size: 270422 Color: 1
Size: 256206 Color: 0

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 473592 Color: 1
Size: 271090 Color: 0
Size: 255319 Color: 0

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 473812 Color: 0
Size: 263603 Color: 1
Size: 262586 Color: 0

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 473872 Color: 1
Size: 275412 Color: 0
Size: 250717 Color: 1

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 473868 Color: 0
Size: 266171 Color: 0
Size: 259962 Color: 1

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 473910 Color: 1
Size: 273467 Color: 0
Size: 252624 Color: 1

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 473981 Color: 0
Size: 275790 Color: 1
Size: 250230 Color: 0

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 473987 Color: 0
Size: 270388 Color: 1
Size: 255626 Color: 1

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 474089 Color: 0
Size: 273267 Color: 1
Size: 252645 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 474066 Color: 1
Size: 275439 Color: 0
Size: 250496 Color: 1

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 474221 Color: 0
Size: 269117 Color: 0
Size: 256663 Color: 1

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 474320 Color: 1
Size: 268801 Color: 0
Size: 256880 Color: 1

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 474396 Color: 1
Size: 265459 Color: 0
Size: 260146 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 474514 Color: 0
Size: 267575 Color: 1
Size: 257912 Color: 1

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 474501 Color: 1
Size: 275175 Color: 1
Size: 250325 Color: 0

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 474569 Color: 0
Size: 268358 Color: 0
Size: 257074 Color: 1

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 474674 Color: 0
Size: 269367 Color: 0
Size: 255960 Color: 1

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 475170 Color: 0
Size: 269312 Color: 1
Size: 255519 Color: 1

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 475354 Color: 1
Size: 266309 Color: 0
Size: 258338 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 475394 Color: 1
Size: 266522 Color: 1
Size: 258085 Color: 0

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 475375 Color: 0
Size: 271680 Color: 0
Size: 252946 Color: 1

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 475478 Color: 0
Size: 273817 Color: 1
Size: 250706 Color: 1

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 475482 Color: 0
Size: 267829 Color: 1
Size: 256690 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 475571 Color: 1
Size: 272021 Color: 0
Size: 252409 Color: 1

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 475620 Color: 1
Size: 269577 Color: 0
Size: 254804 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 475623 Color: 1
Size: 272155 Color: 0
Size: 252223 Color: 1

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 475587 Color: 0
Size: 273765 Color: 0
Size: 250649 Color: 1

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 475642 Color: 1
Size: 273810 Color: 0
Size: 250549 Color: 1

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 475693 Color: 1
Size: 273872 Color: 0
Size: 250436 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 475972 Color: 1
Size: 263822 Color: 0
Size: 260207 Color: 1

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 475937 Color: 0
Size: 269360 Color: 0
Size: 254704 Color: 1

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 476015 Color: 0
Size: 270515 Color: 1
Size: 253471 Color: 1

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 476077 Color: 0
Size: 264422 Color: 1
Size: 259502 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 476171 Color: 1
Size: 262423 Color: 0
Size: 261407 Color: 1

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 476191 Color: 0
Size: 269213 Color: 1
Size: 254597 Color: 0

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 476296 Color: 0
Size: 265172 Color: 1
Size: 258533 Color: 1

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 476387 Color: 0
Size: 263753 Color: 0
Size: 259861 Color: 1

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 476410 Color: 1
Size: 267154 Color: 0
Size: 256437 Color: 1

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 476461 Color: 0
Size: 269470 Color: 1
Size: 254070 Color: 0

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 476573 Color: 0
Size: 265457 Color: 1
Size: 257971 Color: 1

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 476761 Color: 1
Size: 271256 Color: 0
Size: 251984 Color: 0

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 476837 Color: 1
Size: 272983 Color: 1
Size: 250181 Color: 0

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 476959 Color: 1
Size: 263878 Color: 0
Size: 259164 Color: 1

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 477024 Color: 0
Size: 270419 Color: 1
Size: 252558 Color: 0

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 477053 Color: 0
Size: 263426 Color: 1
Size: 259522 Color: 1

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 477079 Color: 0
Size: 267783 Color: 1
Size: 255139 Color: 0

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 477206 Color: 0
Size: 267568 Color: 1
Size: 255227 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 477204 Color: 1
Size: 272414 Color: 0
Size: 250383 Color: 1

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 477212 Color: 0
Size: 262140 Color: 1
Size: 260649 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 477229 Color: 0
Size: 268041 Color: 1
Size: 254731 Color: 1

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 477377 Color: 1
Size: 269232 Color: 0
Size: 253392 Color: 0

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 477387 Color: 1
Size: 269528 Color: 0
Size: 253086 Color: 1

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 477308 Color: 0
Size: 272013 Color: 1
Size: 250680 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 477421 Color: 0
Size: 266018 Color: 1
Size: 256562 Color: 1

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 477652 Color: 1
Size: 270057 Color: 0
Size: 252292 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 477942 Color: 1
Size: 262645 Color: 0
Size: 259414 Color: 1

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 477953 Color: 0
Size: 269111 Color: 1
Size: 252937 Color: 0

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 477979 Color: 0
Size: 262352 Color: 0
Size: 259670 Color: 1

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 478056 Color: 1
Size: 271598 Color: 1
Size: 250347 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 478100 Color: 1
Size: 271897 Color: 0
Size: 250004 Color: 1

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 478162 Color: 0
Size: 261323 Color: 0
Size: 260516 Color: 1

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 478332 Color: 0
Size: 271510 Color: 1
Size: 250159 Color: 0

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 478513 Color: 0
Size: 261840 Color: 1
Size: 259648 Color: 1

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 478432 Color: 1
Size: 261071 Color: 0
Size: 260498 Color: 1

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 478519 Color: 0
Size: 268177 Color: 1
Size: 253305 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 478570 Color: 0
Size: 271157 Color: 1
Size: 250274 Color: 1

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 478607 Color: 0
Size: 261337 Color: 0
Size: 260057 Color: 1

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 478711 Color: 0
Size: 271236 Color: 1
Size: 250054 Color: 1

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 478967 Color: 0
Size: 270849 Color: 1
Size: 250185 Color: 1

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 478970 Color: 0
Size: 266773 Color: 1
Size: 254258 Color: 0

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 478960 Color: 1
Size: 269321 Color: 0
Size: 251720 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 479042 Color: 0
Size: 260509 Color: 0
Size: 260450 Color: 1

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 479133 Color: 0
Size: 266210 Color: 1
Size: 254658 Color: 1

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 479223 Color: 1
Size: 262209 Color: 0
Size: 258569 Color: 0

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 479577 Color: 0
Size: 269817 Color: 0
Size: 250607 Color: 1

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 479519 Color: 1
Size: 261280 Color: 0
Size: 259202 Color: 1

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 479600 Color: 0
Size: 264342 Color: 1
Size: 256059 Color: 0

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 479747 Color: 0
Size: 269511 Color: 1
Size: 250743 Color: 1

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 479787 Color: 0
Size: 267529 Color: 0
Size: 252685 Color: 1

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 479740 Color: 1
Size: 267461 Color: 1
Size: 252800 Color: 0

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 479833 Color: 1
Size: 265492 Color: 0
Size: 254676 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 480018 Color: 0
Size: 261510 Color: 1
Size: 258473 Color: 1

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 480063 Color: 1
Size: 260919 Color: 0
Size: 259019 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 480195 Color: 1
Size: 269196 Color: 1
Size: 250610 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 480196 Color: 0
Size: 262274 Color: 0
Size: 257531 Color: 1

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 480314 Color: 1
Size: 261699 Color: 0
Size: 257988 Color: 1

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 480322 Color: 0
Size: 259997 Color: 1
Size: 259682 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 480395 Color: 1
Size: 266888 Color: 1
Size: 252718 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 480407 Color: 0
Size: 260918 Color: 0
Size: 258676 Color: 1

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 480509 Color: 1
Size: 268250 Color: 1
Size: 251242 Color: 0

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 480595 Color: 0
Size: 259993 Color: 0
Size: 259413 Color: 1

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 480674 Color: 0
Size: 263646 Color: 0
Size: 255681 Color: 1

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 480711 Color: 1
Size: 263754 Color: 1
Size: 255536 Color: 0

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 480684 Color: 0
Size: 261982 Color: 1
Size: 257335 Color: 0

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 481075 Color: 1
Size: 267582 Color: 0
Size: 251344 Color: 1

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 481130 Color: 1
Size: 261748 Color: 0
Size: 257123 Color: 0

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 481254 Color: 0
Size: 261682 Color: 1
Size: 257065 Color: 1

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 481273 Color: 0
Size: 260030 Color: 1
Size: 258698 Color: 0

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 481338 Color: 1
Size: 262073 Color: 1
Size: 256590 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 481518 Color: 1
Size: 259344 Color: 0
Size: 259139 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 481526 Color: 1
Size: 259301 Color: 0
Size: 259174 Color: 1

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 481683 Color: 1
Size: 264268 Color: 0
Size: 254050 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 481689 Color: 1
Size: 262744 Color: 1
Size: 255568 Color: 0

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 481841 Color: 1
Size: 260274 Color: 1
Size: 257886 Color: 0

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 481845 Color: 0
Size: 265274 Color: 0
Size: 252882 Color: 1

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 482076 Color: 1
Size: 262495 Color: 1
Size: 255430 Color: 0

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 482151 Color: 1
Size: 266320 Color: 0
Size: 251530 Color: 1

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 482250 Color: 1
Size: 263073 Color: 0
Size: 254678 Color: 1

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 482273 Color: 1
Size: 260817 Color: 0
Size: 256911 Color: 1

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 482432 Color: 0
Size: 267522 Color: 1
Size: 250047 Color: 0

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 482432 Color: 0
Size: 264908 Color: 1
Size: 252661 Color: 1

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 482454 Color: 1
Size: 263310 Color: 0
Size: 254237 Color: 1

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 482527 Color: 0
Size: 264634 Color: 1
Size: 252840 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 482551 Color: 1
Size: 261841 Color: 0
Size: 255609 Color: 1

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 482620 Color: 1
Size: 266911 Color: 0
Size: 250470 Color: 0

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 482771 Color: 1
Size: 262535 Color: 0
Size: 254695 Color: 0

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 482822 Color: 1
Size: 261212 Color: 1
Size: 255967 Color: 0

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 482989 Color: 1
Size: 260559 Color: 0
Size: 256453 Color: 1

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 482991 Color: 1
Size: 258736 Color: 0
Size: 258274 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 483079 Color: 0
Size: 263355 Color: 1
Size: 253567 Color: 1

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 483127 Color: 0
Size: 265210 Color: 1
Size: 251664 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 483147 Color: 0
Size: 265641 Color: 1
Size: 251213 Color: 1

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 483231 Color: 1
Size: 264387 Color: 0
Size: 252383 Color: 1

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 483301 Color: 0
Size: 260049 Color: 1
Size: 256651 Color: 1

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 483315 Color: 0
Size: 263119 Color: 0
Size: 253567 Color: 1

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 483332 Color: 1
Size: 262299 Color: 1
Size: 254370 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 483453 Color: 1
Size: 258359 Color: 1
Size: 258189 Color: 0

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 483522 Color: 0
Size: 263574 Color: 1
Size: 252905 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 483528 Color: 0
Size: 264384 Color: 1
Size: 252089 Color: 1

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 483549 Color: 0
Size: 261183 Color: 0
Size: 255269 Color: 1

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 483661 Color: 0
Size: 264749 Color: 1
Size: 251591 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 483633 Color: 1
Size: 266166 Color: 0
Size: 250202 Color: 1

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 483936 Color: 1
Size: 264418 Color: 1
Size: 251647 Color: 0

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 483983 Color: 0
Size: 258281 Color: 0
Size: 257737 Color: 1

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 484111 Color: 1
Size: 264053 Color: 0
Size: 251837 Color: 1

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 484408 Color: 0
Size: 264877 Color: 1
Size: 250716 Color: 0

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 484714 Color: 1
Size: 258758 Color: 0
Size: 256529 Color: 0

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 484804 Color: 1
Size: 257755 Color: 0
Size: 257442 Color: 1

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 484818 Color: 1
Size: 259143 Color: 0
Size: 256040 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 484780 Color: 0
Size: 264310 Color: 1
Size: 250911 Color: 0

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 484827 Color: 1
Size: 259359 Color: 1
Size: 255815 Color: 0

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 484789 Color: 0
Size: 259991 Color: 1
Size: 255221 Color: 0

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 484940 Color: 1
Size: 258659 Color: 1
Size: 256402 Color: 0

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 484969 Color: 1
Size: 257993 Color: 0
Size: 257039 Color: 0

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 485030 Color: 1
Size: 264732 Color: 0
Size: 250239 Color: 1

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 485105 Color: 1
Size: 258792 Color: 0
Size: 256104 Color: 0

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 485326 Color: 0
Size: 262632 Color: 1
Size: 252043 Color: 1

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 485380 Color: 0
Size: 264620 Color: 0
Size: 250001 Color: 1

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 485365 Color: 1
Size: 258885 Color: 0
Size: 255751 Color: 1

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 485614 Color: 1
Size: 263430 Color: 1
Size: 250957 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 485554 Color: 0
Size: 262888 Color: 1
Size: 251559 Color: 0

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 485677 Color: 1
Size: 260734 Color: 0
Size: 253590 Color: 1

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 485622 Color: 0
Size: 263243 Color: 1
Size: 251136 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 485707 Color: 1
Size: 260369 Color: 1
Size: 253925 Color: 0

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 485727 Color: 0
Size: 260779 Color: 0
Size: 253495 Color: 1

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 485980 Color: 1
Size: 257727 Color: 0
Size: 256294 Color: 1

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 486106 Color: 0
Size: 263805 Color: 1
Size: 250090 Color: 0

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 486123 Color: 1
Size: 260397 Color: 0
Size: 253481 Color: 1

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 486231 Color: 1
Size: 262604 Color: 0
Size: 251166 Color: 1

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 486153 Color: 0
Size: 258970 Color: 1
Size: 254878 Color: 0

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 486284 Color: 1
Size: 257255 Color: 1
Size: 256462 Color: 0

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 486329 Color: 0
Size: 262475 Color: 0
Size: 251197 Color: 1

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 486391 Color: 0
Size: 260813 Color: 1
Size: 252797 Color: 1

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 486443 Color: 0
Size: 261005 Color: 0
Size: 252553 Color: 1

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 486650 Color: 0
Size: 261198 Color: 1
Size: 252153 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 486665 Color: 0
Size: 262795 Color: 0
Size: 250541 Color: 1

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 487134 Color: 0
Size: 259915 Color: 1
Size: 252952 Color: 1

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 487136 Color: 0
Size: 261146 Color: 1
Size: 251719 Color: 0

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 487214 Color: 1
Size: 259231 Color: 0
Size: 253556 Color: 1

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 487140 Color: 0
Size: 257642 Color: 1
Size: 255219 Color: 0

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 487302 Color: 1
Size: 259642 Color: 1
Size: 253057 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 487439 Color: 1
Size: 261095 Color: 0
Size: 251467 Color: 1

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 487564 Color: 0
Size: 261070 Color: 1
Size: 251367 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 487622 Color: 1
Size: 258644 Color: 0
Size: 253735 Color: 1

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 487687 Color: 0
Size: 262103 Color: 1
Size: 250211 Color: 1

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 487849 Color: 1
Size: 258643 Color: 0
Size: 253509 Color: 1

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 488000 Color: 1
Size: 261191 Color: 0
Size: 250810 Color: 1

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 488041 Color: 0
Size: 261497 Color: 0
Size: 250463 Color: 1

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 488197 Color: 0
Size: 257434 Color: 1
Size: 254370 Color: 1

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 488151 Color: 1
Size: 261333 Color: 0
Size: 250517 Color: 1

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 488348 Color: 0
Size: 259550 Color: 0
Size: 252103 Color: 1

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 488721 Color: 0
Size: 260666 Color: 1
Size: 250614 Color: 0

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 488749 Color: 1
Size: 260630 Color: 1
Size: 250622 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 488775 Color: 0
Size: 258340 Color: 0
Size: 252886 Color: 1

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 488864 Color: 0
Size: 259104 Color: 1
Size: 252033 Color: 1

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 488945 Color: 0
Size: 260628 Color: 0
Size: 250428 Color: 1

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 488972 Color: 1
Size: 259686 Color: 1
Size: 251343 Color: 0

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 489010 Color: 1
Size: 257177 Color: 1
Size: 253814 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 489022 Color: 1
Size: 258133 Color: 0
Size: 252846 Color: 0

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 489115 Color: 1
Size: 255892 Color: 0
Size: 254994 Color: 1

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 489160 Color: 0
Size: 257219 Color: 1
Size: 253622 Color: 0

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 489252 Color: 1
Size: 260412 Color: 0
Size: 250337 Color: 1

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 489276 Color: 1
Size: 259146 Color: 1
Size: 251579 Color: 0

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 489365 Color: 1
Size: 257781 Color: 0
Size: 252855 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 489427 Color: 1
Size: 255893 Color: 0
Size: 254681 Color: 1

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 489498 Color: 0
Size: 259414 Color: 0
Size: 251089 Color: 1

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 489657 Color: 1
Size: 257191 Color: 0
Size: 253153 Color: 0

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 489909 Color: 0
Size: 256924 Color: 1
Size: 253168 Color: 0

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 489910 Color: 0
Size: 255818 Color: 1
Size: 254273 Color: 1

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 489997 Color: 0
Size: 256476 Color: 0
Size: 253528 Color: 1

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 490060 Color: 1
Size: 258346 Color: 0
Size: 251595 Color: 0

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 490082 Color: 1
Size: 258845 Color: 1
Size: 251074 Color: 0

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 490160 Color: 1
Size: 259516 Color: 0
Size: 250325 Color: 0

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 490189 Color: 1
Size: 258765 Color: 0
Size: 251047 Color: 1

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 490216 Color: 0
Size: 257259 Color: 0
Size: 252526 Color: 1

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 490266 Color: 1
Size: 258680 Color: 0
Size: 251055 Color: 1

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 490472 Color: 0
Size: 257671 Color: 0
Size: 251858 Color: 1

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 490427 Color: 1
Size: 257287 Color: 0
Size: 252287 Color: 1

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 490599 Color: 0
Size: 257036 Color: 1
Size: 252366 Color: 0

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 490588 Color: 1
Size: 255552 Color: 1
Size: 253861 Color: 0

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 490691 Color: 0
Size: 258830 Color: 1
Size: 250480 Color: 0

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 490772 Color: 0
Size: 254800 Color: 0
Size: 254429 Color: 1

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 490829 Color: 0
Size: 258549 Color: 1
Size: 250623 Color: 1

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 490867 Color: 0
Size: 257670 Color: 0
Size: 251464 Color: 1

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 490988 Color: 0
Size: 256178 Color: 0
Size: 252835 Color: 1

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 491352 Color: 0
Size: 257081 Color: 1
Size: 251568 Color: 0

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 491393 Color: 0
Size: 256206 Color: 1
Size: 252402 Color: 0

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 491653 Color: 0
Size: 255965 Color: 1
Size: 252383 Color: 0

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 491691 Color: 1
Size: 255932 Color: 0
Size: 252378 Color: 0

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 491990 Color: 1
Size: 257567 Color: 1
Size: 250444 Color: 0

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 492102 Color: 1
Size: 256118 Color: 0
Size: 251781 Color: 0

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 492290 Color: 0
Size: 257462 Color: 1
Size: 250249 Color: 1

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 492341 Color: 0
Size: 255359 Color: 0
Size: 252301 Color: 1

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 492464 Color: 1
Size: 254307 Color: 1
Size: 253230 Color: 0

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 492552 Color: 0
Size: 256650 Color: 1
Size: 250799 Color: 1

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 492580 Color: 1
Size: 256516 Color: 0
Size: 250905 Color: 1

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 492817 Color: 0
Size: 255709 Color: 1
Size: 251475 Color: 0

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 492971 Color: 0
Size: 256040 Color: 1
Size: 250990 Color: 0

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 493000 Color: 1
Size: 256162 Color: 1
Size: 250839 Color: 0

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 493038 Color: 0
Size: 256452 Color: 0
Size: 250511 Color: 1

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 493065 Color: 0
Size: 256710 Color: 1
Size: 250226 Color: 0

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 493153 Color: 1
Size: 254274 Color: 1
Size: 252574 Color: 0

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 493123 Color: 0
Size: 254630 Color: 0
Size: 252248 Color: 1

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 493300 Color: 1
Size: 254375 Color: 0
Size: 252326 Color: 0

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 493351 Color: 0
Size: 256440 Color: 1
Size: 250210 Color: 1

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 493558 Color: 1
Size: 253289 Color: 0
Size: 253154 Color: 0

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 493645 Color: 0
Size: 255212 Color: 0
Size: 251144 Color: 1

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 495120 Color: 0
Size: 252820 Color: 0
Size: 252061 Color: 1

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 493826 Color: 0
Size: 254604 Color: 0
Size: 251571 Color: 1

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 493843 Color: 0
Size: 254097 Color: 1
Size: 252061 Color: 0

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 493987 Color: 0
Size: 254828 Color: 1
Size: 251186 Color: 1

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 494107 Color: 0
Size: 255813 Color: 1
Size: 250081 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 494136 Color: 0
Size: 254931 Color: 1
Size: 250934 Color: 1

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 494600 Color: 0
Size: 253506 Color: 0
Size: 251895 Color: 1

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 494615 Color: 0
Size: 254222 Color: 0
Size: 251164 Color: 1

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 494919 Color: 0
Size: 254694 Color: 1
Size: 250388 Color: 1

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 495273 Color: 1
Size: 252901 Color: 0
Size: 251827 Color: 0

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 495457 Color: 0
Size: 254425 Color: 1
Size: 250119 Color: 0

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 495531 Color: 1
Size: 254014 Color: 1
Size: 250456 Color: 0

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 495683 Color: 0
Size: 254015 Color: 1
Size: 250303 Color: 0

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 495889 Color: 0
Size: 253938 Color: 1
Size: 250174 Color: 0

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 495920 Color: 0
Size: 253514 Color: 1
Size: 250567 Color: 1

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 495943 Color: 1
Size: 252393 Color: 1
Size: 251665 Color: 0

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 496281 Color: 1
Size: 252596 Color: 0
Size: 251124 Color: 0

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 496458 Color: 0
Size: 253055 Color: 1
Size: 250488 Color: 1

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 496638 Color: 1
Size: 252466 Color: 0
Size: 250897 Color: 0

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 496645 Color: 1
Size: 251889 Color: 0
Size: 251467 Color: 0

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 496623 Color: 0
Size: 251862 Color: 1
Size: 251516 Color: 0

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 496724 Color: 1
Size: 252038 Color: 1
Size: 251239 Color: 0

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 496772 Color: 0
Size: 252224 Color: 0
Size: 251005 Color: 1

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 497019 Color: 0
Size: 252522 Color: 1
Size: 250460 Color: 1

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 497239 Color: 0
Size: 251976 Color: 0
Size: 250786 Color: 1

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 497261 Color: 1
Size: 252490 Color: 1
Size: 250250 Color: 0

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 497244 Color: 0
Size: 252166 Color: 1
Size: 250591 Color: 0

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 497358 Color: 1
Size: 252595 Color: 0
Size: 250048 Color: 1

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 497368 Color: 1
Size: 252163 Color: 0
Size: 250470 Color: 0

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 497369 Color: 1
Size: 251628 Color: 0
Size: 251004 Color: 1

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 497376 Color: 0
Size: 252452 Color: 1
Size: 250173 Color: 0

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 497549 Color: 1
Size: 252170 Color: 0
Size: 250282 Color: 1

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 497615 Color: 0
Size: 251322 Color: 0
Size: 251064 Color: 1

Bin 2963: 1 of cap free
Amount of items: 3
Items: 
Size: 341321 Color: 0
Size: 340718 Color: 1
Size: 317961 Color: 1

Bin 2964: 1 of cap free
Amount of items: 3
Items: 
Size: 360018 Color: 1
Size: 324887 Color: 1
Size: 315095 Color: 0

Bin 2965: 1 of cap free
Amount of items: 3
Items: 
Size: 353241 Color: 0
Size: 329455 Color: 0
Size: 317304 Color: 1

Bin 2966: 1 of cap free
Amount of items: 3
Items: 
Size: 359074 Color: 1
Size: 326688 Color: 0
Size: 314238 Color: 1

Bin 2967: 1 of cap free
Amount of items: 3
Items: 
Size: 395878 Color: 1
Size: 350291 Color: 0
Size: 253831 Color: 0

Bin 2968: 1 of cap free
Amount of items: 3
Items: 
Size: 368015 Color: 1
Size: 353858 Color: 0
Size: 278127 Color: 1

Bin 2969: 1 of cap free
Amount of items: 3
Items: 
Size: 381951 Color: 1
Size: 367459 Color: 0
Size: 250590 Color: 0

Bin 2970: 1 of cap free
Amount of items: 3
Items: 
Size: 358596 Color: 0
Size: 338488 Color: 0
Size: 302916 Color: 1

Bin 2971: 1 of cap free
Amount of items: 3
Items: 
Size: 407645 Color: 0
Size: 336755 Color: 1
Size: 255600 Color: 1

Bin 2972: 1 of cap free
Amount of items: 3
Items: 
Size: 363440 Color: 1
Size: 344115 Color: 0
Size: 292445 Color: 0

Bin 2973: 1 of cap free
Amount of items: 3
Items: 
Size: 397962 Color: 1
Size: 338891 Color: 0
Size: 263147 Color: 1

Bin 2974: 1 of cap free
Amount of items: 3
Items: 
Size: 363079 Color: 1
Size: 336177 Color: 0
Size: 300744 Color: 1

Bin 2975: 1 of cap free
Amount of items: 3
Items: 
Size: 358607 Color: 0
Size: 322767 Color: 1
Size: 318626 Color: 0

Bin 2976: 1 of cap free
Amount of items: 3
Items: 
Size: 362650 Color: 1
Size: 337627 Color: 0
Size: 299723 Color: 1

Bin 2977: 1 of cap free
Amount of items: 3
Items: 
Size: 339173 Color: 1
Size: 331919 Color: 0
Size: 328908 Color: 1

Bin 2978: 1 of cap free
Amount of items: 3
Items: 
Size: 368663 Color: 1
Size: 353838 Color: 0
Size: 277499 Color: 0

Bin 2979: 1 of cap free
Amount of items: 3
Items: 
Size: 336787 Color: 0
Size: 336175 Color: 0
Size: 327038 Color: 1

Bin 2980: 1 of cap free
Amount of items: 3
Items: 
Size: 361994 Color: 1
Size: 356295 Color: 0
Size: 281711 Color: 0

Bin 2981: 1 of cap free
Amount of items: 3
Items: 
Size: 352813 Color: 0
Size: 326129 Color: 1
Size: 321058 Color: 0

Bin 2982: 1 of cap free
Amount of items: 3
Items: 
Size: 353438 Color: 1
Size: 333212 Color: 0
Size: 313350 Color: 1

Bin 2983: 1 of cap free
Amount of items: 3
Items: 
Size: 354083 Color: 0
Size: 333386 Color: 0
Size: 312531 Color: 1

Bin 2984: 1 of cap free
Amount of items: 3
Items: 
Size: 353738 Color: 1
Size: 342740 Color: 0
Size: 303522 Color: 1

Bin 2985: 1 of cap free
Amount of items: 3
Items: 
Size: 355484 Color: 1
Size: 337107 Color: 0
Size: 307409 Color: 1

Bin 2986: 1 of cap free
Amount of items: 3
Items: 
Size: 356182 Color: 0
Size: 326080 Color: 1
Size: 317738 Color: 0

Bin 2987: 1 of cap free
Amount of items: 3
Items: 
Size: 355826 Color: 1
Size: 328286 Color: 0
Size: 315888 Color: 1

Bin 2988: 1 of cap free
Amount of items: 3
Items: 
Size: 356850 Color: 0
Size: 339187 Color: 1
Size: 303963 Color: 0

Bin 2989: 1 of cap free
Amount of items: 3
Items: 
Size: 357820 Color: 0
Size: 336576 Color: 0
Size: 305604 Color: 1

Bin 2990: 1 of cap free
Amount of items: 3
Items: 
Size: 358122 Color: 0
Size: 322115 Color: 0
Size: 319763 Color: 1

Bin 2991: 1 of cap free
Amount of items: 3
Items: 
Size: 358783 Color: 1
Size: 348545 Color: 1
Size: 292672 Color: 0

Bin 2992: 1 of cap free
Amount of items: 3
Items: 
Size: 359344 Color: 1
Size: 358059 Color: 1
Size: 282597 Color: 0

Bin 2993: 1 of cap free
Amount of items: 3
Items: 
Size: 359377 Color: 1
Size: 340068 Color: 0
Size: 300555 Color: 1

Bin 2994: 1 of cap free
Amount of items: 3
Items: 
Size: 359191 Color: 0
Size: 327518 Color: 0
Size: 313291 Color: 1

Bin 2995: 1 of cap free
Amount of items: 3
Items: 
Size: 359562 Color: 1
Size: 325994 Color: 1
Size: 314444 Color: 0

Bin 2996: 1 of cap free
Amount of items: 3
Items: 
Size: 359605 Color: 1
Size: 334655 Color: 0
Size: 305740 Color: 1

Bin 2997: 1 of cap free
Amount of items: 3
Items: 
Size: 360069 Color: 1
Size: 329214 Color: 0
Size: 310717 Color: 1

Bin 2998: 1 of cap free
Amount of items: 3
Items: 
Size: 360164 Color: 0
Size: 344106 Color: 0
Size: 295730 Color: 1

Bin 2999: 1 of cap free
Amount of items: 3
Items: 
Size: 361217 Color: 1
Size: 325034 Color: 1
Size: 313749 Color: 0

Bin 3000: 1 of cap free
Amount of items: 3
Items: 
Size: 361398 Color: 1
Size: 344538 Color: 0
Size: 294064 Color: 1

Bin 3001: 1 of cap free
Amount of items: 3
Items: 
Size: 362645 Color: 1
Size: 324307 Color: 0
Size: 313048 Color: 0

Bin 3002: 1 of cap free
Amount of items: 3
Items: 
Size: 363032 Color: 1
Size: 331673 Color: 0
Size: 305295 Color: 1

Bin 3003: 1 of cap free
Amount of items: 3
Items: 
Size: 362939 Color: 0
Size: 344354 Color: 0
Size: 292707 Color: 1

Bin 3004: 1 of cap free
Amount of items: 3
Items: 
Size: 362942 Color: 0
Size: 355273 Color: 0
Size: 281785 Color: 1

Bin 3005: 1 of cap free
Amount of items: 3
Items: 
Size: 363610 Color: 1
Size: 361715 Color: 1
Size: 274675 Color: 0

Bin 3006: 1 of cap free
Amount of items: 3
Items: 
Size: 364182 Color: 0
Size: 345875 Color: 1
Size: 289943 Color: 0

Bin 3007: 1 of cap free
Amount of items: 3
Items: 
Size: 364962 Color: 1
Size: 364509 Color: 0
Size: 270529 Color: 1

Bin 3008: 1 of cap free
Amount of items: 3
Items: 
Size: 364783 Color: 0
Size: 354746 Color: 0
Size: 280471 Color: 1

Bin 3009: 1 of cap free
Amount of items: 3
Items: 
Size: 365030 Color: 0
Size: 355288 Color: 1
Size: 279682 Color: 0

Bin 3010: 1 of cap free
Amount of items: 3
Items: 
Size: 365233 Color: 1
Size: 318926 Color: 0
Size: 315841 Color: 1

Bin 3011: 1 of cap free
Amount of items: 3
Items: 
Size: 365254 Color: 1
Size: 320351 Color: 1
Size: 314395 Color: 0

Bin 3012: 1 of cap free
Amount of items: 3
Items: 
Size: 365233 Color: 0
Size: 327003 Color: 1
Size: 307764 Color: 0

Bin 3013: 1 of cap free
Amount of items: 3
Items: 
Size: 366429 Color: 1
Size: 337047 Color: 0
Size: 296524 Color: 1

Bin 3014: 1 of cap free
Amount of items: 3
Items: 
Size: 366327 Color: 0
Size: 326990 Color: 1
Size: 306683 Color: 0

Bin 3015: 1 of cap free
Amount of items: 3
Items: 
Size: 366379 Color: 0
Size: 365422 Color: 0
Size: 268199 Color: 1

Bin 3016: 1 of cap free
Amount of items: 3
Items: 
Size: 367631 Color: 1
Size: 334192 Color: 1
Size: 298177 Color: 0

Bin 3017: 1 of cap free
Amount of items: 3
Items: 
Size: 367687 Color: 1
Size: 340657 Color: 1
Size: 291656 Color: 0

Bin 3018: 1 of cap free
Amount of items: 3
Items: 
Size: 368338 Color: 0
Size: 362137 Color: 0
Size: 269525 Color: 1

Bin 3019: 1 of cap free
Amount of items: 3
Items: 
Size: 369027 Color: 0
Size: 349613 Color: 0
Size: 281360 Color: 1

Bin 3020: 1 of cap free
Amount of items: 3
Items: 
Size: 369327 Color: 1
Size: 346904 Color: 1
Size: 283769 Color: 0

Bin 3021: 1 of cap free
Amount of items: 3
Items: 
Size: 369524 Color: 1
Size: 321581 Color: 0
Size: 308895 Color: 1

Bin 3022: 1 of cap free
Amount of items: 3
Items: 
Size: 370878 Color: 0
Size: 347584 Color: 1
Size: 281538 Color: 0

Bin 3023: 1 of cap free
Amount of items: 3
Items: 
Size: 370935 Color: 0
Size: 367333 Color: 0
Size: 261732 Color: 1

Bin 3024: 1 of cap free
Amount of items: 3
Items: 
Size: 371686 Color: 0
Size: 338977 Color: 1
Size: 289337 Color: 1

Bin 3025: 1 of cap free
Amount of items: 3
Items: 
Size: 373855 Color: 0
Size: 371756 Color: 1
Size: 254389 Color: 1

Bin 3026: 1 of cap free
Amount of items: 3
Items: 
Size: 380678 Color: 1
Size: 329316 Color: 1
Size: 290006 Color: 0

Bin 3027: 1 of cap free
Amount of items: 3
Items: 
Size: 341893 Color: 1
Size: 339881 Color: 1
Size: 318226 Color: 0

Bin 3028: 1 of cap free
Amount of items: 3
Items: 
Size: 362387 Color: 1
Size: 351637 Color: 0
Size: 285976 Color: 0

Bin 3029: 1 of cap free
Amount of items: 3
Items: 
Size: 365175 Color: 1
Size: 337199 Color: 1
Size: 297626 Color: 0

Bin 3030: 1 of cap free
Amount of items: 3
Items: 
Size: 352517 Color: 0
Size: 327059 Color: 1
Size: 320424 Color: 0

Bin 3031: 1 of cap free
Amount of items: 3
Items: 
Size: 337998 Color: 0
Size: 332361 Color: 0
Size: 329641 Color: 1

Bin 3032: 1 of cap free
Amount of items: 3
Items: 
Size: 349054 Color: 0
Size: 338844 Color: 1
Size: 312102 Color: 1

Bin 3033: 1 of cap free
Amount of items: 3
Items: 
Size: 338623 Color: 0
Size: 332358 Color: 1
Size: 329019 Color: 1

Bin 3034: 1 of cap free
Amount of items: 3
Items: 
Size: 394019 Color: 1
Size: 354572 Color: 0
Size: 251409 Color: 1

Bin 3035: 1 of cap free
Amount of items: 3
Items: 
Size: 398019 Color: 1
Size: 335768 Color: 0
Size: 266213 Color: 1

Bin 3036: 1 of cap free
Amount of items: 3
Items: 
Size: 360460 Color: 0
Size: 351497 Color: 1
Size: 288043 Color: 1

Bin 3037: 1 of cap free
Amount of items: 3
Items: 
Size: 356603 Color: 1
Size: 345785 Color: 0
Size: 297612 Color: 0

Bin 3038: 1 of cap free
Amount of items: 3
Items: 
Size: 420999 Color: 0
Size: 318297 Color: 1
Size: 260704 Color: 0

Bin 3039: 1 of cap free
Amount of items: 3
Items: 
Size: 372238 Color: 1
Size: 344989 Color: 1
Size: 282773 Color: 0

Bin 3040: 1 of cap free
Amount of items: 3
Items: 
Size: 472467 Color: 1
Size: 272676 Color: 0
Size: 254857 Color: 1

Bin 3041: 1 of cap free
Amount of items: 3
Items: 
Size: 411425 Color: 1
Size: 337144 Color: 0
Size: 251431 Color: 1

Bin 3042: 1 of cap free
Amount of items: 3
Items: 
Size: 396471 Color: 0
Size: 350101 Color: 1
Size: 253428 Color: 1

Bin 3043: 2 of cap free
Amount of items: 3
Items: 
Size: 492325 Color: 0
Size: 254460 Color: 1
Size: 253214 Color: 0

Bin 3044: 2 of cap free
Amount of items: 3
Items: 
Size: 358223 Color: 1
Size: 337818 Color: 1
Size: 303958 Color: 0

Bin 3045: 2 of cap free
Amount of items: 3
Items: 
Size: 351859 Color: 0
Size: 328742 Color: 1
Size: 319398 Color: 1

Bin 3046: 2 of cap free
Amount of items: 3
Items: 
Size: 390583 Color: 0
Size: 350520 Color: 1
Size: 258896 Color: 1

Bin 3047: 2 of cap free
Amount of items: 3
Items: 
Size: 366249 Color: 0
Size: 325712 Color: 1
Size: 308038 Color: 0

Bin 3048: 2 of cap free
Amount of items: 3
Items: 
Size: 338674 Color: 0
Size: 337841 Color: 1
Size: 323484 Color: 0

Bin 3049: 2 of cap free
Amount of items: 3
Items: 
Size: 354163 Color: 1
Size: 325420 Color: 0
Size: 320416 Color: 1

Bin 3050: 2 of cap free
Amount of items: 3
Items: 
Size: 389766 Color: 1
Size: 347819 Color: 1
Size: 262414 Color: 0

Bin 3051: 2 of cap free
Amount of items: 3
Items: 
Size: 356480 Color: 0
Size: 348379 Color: 0
Size: 295140 Color: 1

Bin 3052: 2 of cap free
Amount of items: 3
Items: 
Size: 352579 Color: 0
Size: 326579 Color: 0
Size: 320841 Color: 1

Bin 3053: 2 of cap free
Amount of items: 3
Items: 
Size: 354710 Color: 0
Size: 333247 Color: 1
Size: 312042 Color: 0

Bin 3054: 2 of cap free
Amount of items: 3
Items: 
Size: 355894 Color: 1
Size: 352730 Color: 1
Size: 291375 Color: 0

Bin 3055: 2 of cap free
Amount of items: 3
Items: 
Size: 356870 Color: 0
Size: 327844 Color: 0
Size: 315285 Color: 1

Bin 3056: 2 of cap free
Amount of items: 3
Items: 
Size: 356710 Color: 1
Size: 330021 Color: 0
Size: 313268 Color: 1

Bin 3057: 2 of cap free
Amount of items: 3
Items: 
Size: 358051 Color: 0
Size: 344834 Color: 0
Size: 297114 Color: 1

Bin 3058: 2 of cap free
Amount of items: 3
Items: 
Size: 359886 Color: 1
Size: 323892 Color: 0
Size: 316221 Color: 1

Bin 3059: 2 of cap free
Amount of items: 3
Items: 
Size: 359771 Color: 0
Size: 325891 Color: 0
Size: 314337 Color: 1

Bin 3060: 2 of cap free
Amount of items: 3
Items: 
Size: 361155 Color: 0
Size: 339680 Color: 0
Size: 299164 Color: 1

Bin 3061: 2 of cap free
Amount of items: 3
Items: 
Size: 361356 Color: 0
Size: 339088 Color: 1
Size: 299555 Color: 1

Bin 3062: 2 of cap free
Amount of items: 3
Items: 
Size: 362032 Color: 1
Size: 328955 Color: 0
Size: 309012 Color: 1

Bin 3063: 2 of cap free
Amount of items: 3
Items: 
Size: 362384 Color: 1
Size: 323304 Color: 0
Size: 314311 Color: 1

Bin 3064: 2 of cap free
Amount of items: 3
Items: 
Size: 363627 Color: 1
Size: 341393 Color: 0
Size: 294979 Color: 1

Bin 3065: 2 of cap free
Amount of items: 3
Items: 
Size: 352279 Color: 1
Size: 349504 Color: 1
Size: 298216 Color: 0

Bin 3066: 2 of cap free
Amount of items: 3
Items: 
Size: 395247 Color: 1
Size: 330643 Color: 0
Size: 274109 Color: 1

Bin 3067: 2 of cap free
Amount of items: 3
Items: 
Size: 350447 Color: 1
Size: 341662 Color: 0
Size: 307890 Color: 0

Bin 3068: 2 of cap free
Amount of items: 3
Items: 
Size: 348681 Color: 0
Size: 344040 Color: 0
Size: 307278 Color: 1

Bin 3069: 2 of cap free
Amount of items: 3
Items: 
Size: 340130 Color: 0
Size: 339054 Color: 1
Size: 320815 Color: 0

Bin 3070: 2 of cap free
Amount of items: 3
Items: 
Size: 342707 Color: 1
Size: 332641 Color: 1
Size: 324651 Color: 0

Bin 3071: 2 of cap free
Amount of items: 3
Items: 
Size: 334272 Color: 1
Size: 333959 Color: 0
Size: 331768 Color: 0

Bin 3072: 2 of cap free
Amount of items: 3
Items: 
Size: 499436 Color: 1
Size: 250526 Color: 1
Size: 250037 Color: 0

Bin 3073: 2 of cap free
Amount of items: 3
Items: 
Size: 346410 Color: 0
Size: 340829 Color: 0
Size: 312760 Color: 1

Bin 3074: 2 of cap free
Amount of items: 3
Items: 
Size: 342593 Color: 0
Size: 341673 Color: 1
Size: 315733 Color: 1

Bin 3075: 2 of cap free
Amount of items: 3
Items: 
Size: 341494 Color: 1
Size: 340211 Color: 1
Size: 318294 Color: 0

Bin 3076: 2 of cap free
Amount of items: 3
Items: 
Size: 342306 Color: 1
Size: 342290 Color: 0
Size: 315403 Color: 1

Bin 3077: 2 of cap free
Amount of items: 3
Items: 
Size: 334634 Color: 0
Size: 334394 Color: 0
Size: 330971 Color: 1

Bin 3078: 2 of cap free
Amount of items: 3
Items: 
Size: 379375 Color: 1
Size: 355146 Color: 0
Size: 265478 Color: 0

Bin 3079: 3 of cap free
Amount of items: 3
Items: 
Size: 425274 Color: 1
Size: 322479 Color: 0
Size: 252245 Color: 1

Bin 3080: 3 of cap free
Amount of items: 3
Items: 
Size: 492162 Color: 1
Size: 253936 Color: 1
Size: 253900 Color: 0

Bin 3081: 3 of cap free
Amount of items: 3
Items: 
Size: 432361 Color: 1
Size: 314365 Color: 0
Size: 253272 Color: 0

Bin 3082: 3 of cap free
Amount of items: 3
Items: 
Size: 345125 Color: 1
Size: 333293 Color: 1
Size: 321580 Color: 0

Bin 3083: 3 of cap free
Amount of items: 3
Items: 
Size: 341248 Color: 0
Size: 337973 Color: 0
Size: 320777 Color: 1

Bin 3084: 3 of cap free
Amount of items: 3
Items: 
Size: 344483 Color: 0
Size: 344336 Color: 1
Size: 311179 Color: 0

Bin 3085: 3 of cap free
Amount of items: 3
Items: 
Size: 349388 Color: 0
Size: 345803 Color: 1
Size: 304807 Color: 1

Bin 3086: 3 of cap free
Amount of items: 3
Items: 
Size: 336899 Color: 0
Size: 335670 Color: 1
Size: 327429 Color: 0

Bin 3087: 3 of cap free
Amount of items: 3
Items: 
Size: 346353 Color: 0
Size: 337249 Color: 1
Size: 316396 Color: 1

Bin 3088: 3 of cap free
Amount of items: 3
Items: 
Size: 357042 Color: 1
Size: 348823 Color: 1
Size: 294133 Color: 0

Bin 3089: 3 of cap free
Amount of items: 3
Items: 
Size: 353202 Color: 1
Size: 344058 Color: 1
Size: 302738 Color: 0

Bin 3090: 3 of cap free
Amount of items: 3
Items: 
Size: 356333 Color: 1
Size: 322815 Color: 0
Size: 320850 Color: 1

Bin 3091: 3 of cap free
Amount of items: 3
Items: 
Size: 356698 Color: 1
Size: 333464 Color: 0
Size: 309836 Color: 1

Bin 3092: 3 of cap free
Amount of items: 3
Items: 
Size: 358578 Color: 0
Size: 327343 Color: 1
Size: 314077 Color: 0

Bin 3093: 3 of cap free
Amount of items: 3
Items: 
Size: 359395 Color: 0
Size: 339919 Color: 0
Size: 300684 Color: 1

Bin 3094: 3 of cap free
Amount of items: 3
Items: 
Size: 361405 Color: 0
Size: 321780 Color: 1
Size: 316813 Color: 0

Bin 3095: 3 of cap free
Amount of items: 3
Items: 
Size: 400434 Color: 0
Size: 321962 Color: 1
Size: 277602 Color: 1

Bin 3096: 3 of cap free
Amount of items: 3
Items: 
Size: 365334 Color: 1
Size: 354050 Color: 0
Size: 280614 Color: 0

Bin 3097: 3 of cap free
Amount of items: 3
Items: 
Size: 349260 Color: 1
Size: 348966 Color: 0
Size: 301772 Color: 1

Bin 3098: 3 of cap free
Amount of items: 3
Items: 
Size: 351479 Color: 1
Size: 351134 Color: 1
Size: 297385 Color: 0

Bin 3099: 3 of cap free
Amount of items: 3
Items: 
Size: 350916 Color: 0
Size: 341520 Color: 1
Size: 307562 Color: 0

Bin 3100: 3 of cap free
Amount of items: 3
Items: 
Size: 374025 Color: 0
Size: 317051 Color: 1
Size: 308922 Color: 0

Bin 3101: 3 of cap free
Amount of items: 3
Items: 
Size: 431729 Color: 1
Size: 291847 Color: 0
Size: 276422 Color: 0

Bin 3102: 3 of cap free
Amount of items: 3
Items: 
Size: 339833 Color: 0
Size: 338970 Color: 1
Size: 321195 Color: 1

Bin 3103: 4 of cap free
Amount of items: 3
Items: 
Size: 346043 Color: 1
Size: 343773 Color: 0
Size: 310181 Color: 0

Bin 3104: 4 of cap free
Amount of items: 3
Items: 
Size: 371694 Color: 0
Size: 344530 Color: 0
Size: 283773 Color: 1

Bin 3105: 4 of cap free
Amount of items: 3
Items: 
Size: 442067 Color: 0
Size: 285975 Color: 0
Size: 271955 Color: 1

Bin 3106: 4 of cap free
Amount of items: 3
Items: 
Size: 431614 Color: 0
Size: 317312 Color: 0
Size: 251071 Color: 1

Bin 3107: 4 of cap free
Amount of items: 3
Items: 
Size: 348234 Color: 1
Size: 345731 Color: 0
Size: 306032 Color: 1

Bin 3108: 4 of cap free
Amount of items: 3
Items: 
Size: 463071 Color: 0
Size: 269371 Color: 0
Size: 267555 Color: 1

Bin 3109: 4 of cap free
Amount of items: 3
Items: 
Size: 356030 Color: 0
Size: 326079 Color: 1
Size: 317888 Color: 0

Bin 3110: 4 of cap free
Amount of items: 3
Items: 
Size: 348298 Color: 1
Size: 331224 Color: 1
Size: 320475 Color: 0

Bin 3111: 4 of cap free
Amount of items: 3
Items: 
Size: 350775 Color: 0
Size: 344050 Color: 1
Size: 305172 Color: 1

Bin 3112: 4 of cap free
Amount of items: 3
Items: 
Size: 353039 Color: 1
Size: 336829 Color: 1
Size: 310129 Color: 0

Bin 3113: 4 of cap free
Amount of items: 3
Items: 
Size: 356365 Color: 0
Size: 343347 Color: 1
Size: 300285 Color: 0

Bin 3114: 4 of cap free
Amount of items: 3
Items: 
Size: 344510 Color: 1
Size: 343832 Color: 0
Size: 311655 Color: 0

Bin 3115: 4 of cap free
Amount of items: 3
Items: 
Size: 359557 Color: 1
Size: 355952 Color: 0
Size: 284488 Color: 0

Bin 3116: 4 of cap free
Amount of items: 3
Items: 
Size: 385067 Color: 0
Size: 337071 Color: 1
Size: 277859 Color: 0

Bin 3117: 4 of cap free
Amount of items: 3
Items: 
Size: 357921 Color: 0
Size: 356089 Color: 0
Size: 285987 Color: 1

Bin 3118: 4 of cap free
Amount of items: 3
Items: 
Size: 353861 Color: 0
Size: 351701 Color: 0
Size: 294435 Color: 1

Bin 3119: 4 of cap free
Amount of items: 3
Items: 
Size: 348263 Color: 0
Size: 346373 Color: 1
Size: 305361 Color: 0

Bin 3120: 4 of cap free
Amount of items: 3
Items: 
Size: 336893 Color: 0
Size: 333719 Color: 1
Size: 329385 Color: 1

Bin 3121: 4 of cap free
Amount of items: 3
Items: 
Size: 370062 Color: 1
Size: 352081 Color: 0
Size: 277854 Color: 1

Bin 3122: 5 of cap free
Amount of items: 3
Items: 
Size: 357314 Color: 1
Size: 337668 Color: 1
Size: 305014 Color: 0

Bin 3123: 5 of cap free
Amount of items: 3
Items: 
Size: 489260 Color: 1
Size: 256413 Color: 1
Size: 254323 Color: 0

Bin 3124: 5 of cap free
Amount of items: 3
Items: 
Size: 350624 Color: 0
Size: 342525 Color: 0
Size: 306847 Color: 1

Bin 3125: 5 of cap free
Amount of items: 3
Items: 
Size: 337222 Color: 0
Size: 333154 Color: 0
Size: 329620 Color: 1

Bin 3126: 5 of cap free
Amount of items: 3
Items: 
Size: 418832 Color: 0
Size: 325322 Color: 1
Size: 255842 Color: 1

Bin 3127: 5 of cap free
Amount of items: 3
Items: 
Size: 360472 Color: 0
Size: 348977 Color: 1
Size: 290547 Color: 0

Bin 3128: 5 of cap free
Amount of items: 3
Items: 
Size: 377096 Color: 1
Size: 345507 Color: 1
Size: 277393 Color: 0

Bin 3129: 5 of cap free
Amount of items: 3
Items: 
Size: 345428 Color: 0
Size: 340175 Color: 0
Size: 314393 Color: 1

Bin 3130: 5 of cap free
Amount of items: 3
Items: 
Size: 355137 Color: 1
Size: 350613 Color: 1
Size: 294246 Color: 0

Bin 3131: 6 of cap free
Amount of items: 3
Items: 
Size: 402492 Color: 0
Size: 343858 Color: 1
Size: 253645 Color: 1

Bin 3132: 6 of cap free
Amount of items: 3
Items: 
Size: 491833 Color: 0
Size: 255200 Color: 1
Size: 252962 Color: 1

Bin 3133: 6 of cap free
Amount of items: 3
Items: 
Size: 366395 Color: 0
Size: 354947 Color: 0
Size: 278653 Color: 1

Bin 3134: 6 of cap free
Amount of items: 3
Items: 
Size: 345235 Color: 1
Size: 331607 Color: 0
Size: 323153 Color: 1

Bin 3135: 6 of cap free
Amount of items: 3
Items: 
Size: 445061 Color: 1
Size: 279417 Color: 1
Size: 275517 Color: 0

Bin 3136: 6 of cap free
Amount of items: 3
Items: 
Size: 334489 Color: 1
Size: 334166 Color: 0
Size: 331340 Color: 1

Bin 3137: 6 of cap free
Amount of items: 3
Items: 
Size: 340872 Color: 1
Size: 339015 Color: 1
Size: 320108 Color: 0

Bin 3138: 6 of cap free
Amount of items: 3
Items: 
Size: 351016 Color: 1
Size: 327787 Color: 0
Size: 321192 Color: 1

Bin 3139: 6 of cap free
Amount of items: 3
Items: 
Size: 355214 Color: 1
Size: 331309 Color: 1
Size: 313472 Color: 0

Bin 3140: 6 of cap free
Amount of items: 3
Items: 
Size: 367689 Color: 0
Size: 354115 Color: 1
Size: 278191 Color: 1

Bin 3141: 6 of cap free
Amount of items: 3
Items: 
Size: 411630 Color: 1
Size: 330897 Color: 1
Size: 257468 Color: 0

Bin 3142: 6 of cap free
Amount of items: 3
Items: 
Size: 350048 Color: 0
Size: 348633 Color: 0
Size: 301314 Color: 1

Bin 3143: 6 of cap free
Amount of items: 3
Items: 
Size: 351753 Color: 1
Size: 351235 Color: 0
Size: 297007 Color: 1

Bin 3144: 6 of cap free
Amount of items: 3
Items: 
Size: 350243 Color: 0
Size: 349569 Color: 1
Size: 300183 Color: 1

Bin 3145: 6 of cap free
Amount of items: 3
Items: 
Size: 349937 Color: 0
Size: 349139 Color: 1
Size: 300919 Color: 1

Bin 3146: 6 of cap free
Amount of items: 3
Items: 
Size: 344413 Color: 1
Size: 344049 Color: 0
Size: 311533 Color: 0

Bin 3147: 6 of cap free
Amount of items: 3
Items: 
Size: 340333 Color: 0
Size: 336241 Color: 1
Size: 323421 Color: 1

Bin 3148: 6 of cap free
Amount of items: 3
Items: 
Size: 344829 Color: 1
Size: 339055 Color: 0
Size: 316111 Color: 0

Bin 3149: 7 of cap free
Amount of items: 3
Items: 
Size: 494580 Color: 1
Size: 253383 Color: 1
Size: 252031 Color: 0

Bin 3150: 7 of cap free
Amount of items: 3
Items: 
Size: 342541 Color: 1
Size: 341108 Color: 0
Size: 316345 Color: 0

Bin 3151: 7 of cap free
Amount of items: 3
Items: 
Size: 396589 Color: 1
Size: 350761 Color: 0
Size: 252644 Color: 0

Bin 3152: 7 of cap free
Amount of items: 3
Items: 
Size: 419092 Color: 0
Size: 327668 Color: 1
Size: 253234 Color: 0

Bin 3153: 7 of cap free
Amount of items: 3
Items: 
Size: 355914 Color: 0
Size: 332551 Color: 0
Size: 311529 Color: 1

Bin 3154: 7 of cap free
Amount of items: 3
Items: 
Size: 357698 Color: 0
Size: 336926 Color: 1
Size: 305370 Color: 1

Bin 3155: 7 of cap free
Amount of items: 3
Items: 
Size: 352861 Color: 1
Size: 324571 Color: 0
Size: 322562 Color: 0

Bin 3156: 7 of cap free
Amount of items: 3
Items: 
Size: 345176 Color: 1
Size: 331788 Color: 0
Size: 323030 Color: 1

Bin 3157: 7 of cap free
Amount of items: 3
Items: 
Size: 350921 Color: 1
Size: 347362 Color: 1
Size: 301711 Color: 0

Bin 3158: 7 of cap free
Amount of items: 3
Items: 
Size: 347721 Color: 0
Size: 347136 Color: 0
Size: 305137 Color: 1

Bin 3159: 7 of cap free
Amount of items: 3
Items: 
Size: 344745 Color: 0
Size: 332184 Color: 1
Size: 323065 Color: 1

Bin 3160: 7 of cap free
Amount of items: 3
Items: 
Size: 380314 Color: 1
Size: 340613 Color: 1
Size: 279067 Color: 0

Bin 3161: 8 of cap free
Amount of items: 3
Items: 
Size: 480596 Color: 0
Size: 261154 Color: 1
Size: 258243 Color: 1

Bin 3162: 8 of cap free
Amount of items: 3
Items: 
Size: 462376 Color: 1
Size: 271658 Color: 0
Size: 265959 Color: 0

Bin 3163: 8 of cap free
Amount of items: 3
Items: 
Size: 451621 Color: 1
Size: 274573 Color: 0
Size: 273799 Color: 0

Bin 3164: 8 of cap free
Amount of items: 3
Items: 
Size: 348416 Color: 1
Size: 348081 Color: 1
Size: 303496 Color: 0

Bin 3165: 8 of cap free
Amount of items: 3
Items: 
Size: 352531 Color: 1
Size: 337522 Color: 0
Size: 309940 Color: 0

Bin 3166: 8 of cap free
Amount of items: 3
Items: 
Size: 340202 Color: 0
Size: 331723 Color: 0
Size: 328068 Color: 1

Bin 3167: 8 of cap free
Amount of items: 3
Items: 
Size: 348143 Color: 1
Size: 347750 Color: 0
Size: 304100 Color: 0

Bin 3168: 9 of cap free
Amount of items: 3
Items: 
Size: 376402 Color: 1
Size: 349518 Color: 1
Size: 274072 Color: 0

Bin 3169: 9 of cap free
Amount of items: 3
Items: 
Size: 486689 Color: 1
Size: 256857 Color: 1
Size: 256446 Color: 0

Bin 3170: 9 of cap free
Amount of items: 3
Items: 
Size: 388566 Color: 1
Size: 357649 Color: 1
Size: 253777 Color: 0

Bin 3171: 9 of cap free
Amount of items: 3
Items: 
Size: 389025 Color: 0
Size: 348481 Color: 1
Size: 262486 Color: 1

Bin 3172: 9 of cap free
Amount of items: 3
Items: 
Size: 472760 Color: 1
Size: 264978 Color: 0
Size: 262254 Color: 1

Bin 3173: 9 of cap free
Amount of items: 3
Items: 
Size: 481735 Color: 1
Size: 263348 Color: 0
Size: 254909 Color: 1

Bin 3174: 9 of cap free
Amount of items: 3
Items: 
Size: 355185 Color: 0
Size: 352961 Color: 1
Size: 291846 Color: 0

Bin 3175: 9 of cap free
Amount of items: 3
Items: 
Size: 366470 Color: 1
Size: 352770 Color: 0
Size: 280752 Color: 1

Bin 3176: 9 of cap free
Amount of items: 3
Items: 
Size: 340394 Color: 1
Size: 339736 Color: 0
Size: 319862 Color: 0

Bin 3177: 9 of cap free
Amount of items: 3
Items: 
Size: 356306 Color: 0
Size: 350138 Color: 1
Size: 293548 Color: 0

Bin 3178: 10 of cap free
Amount of items: 3
Items: 
Size: 338388 Color: 0
Size: 336470 Color: 1
Size: 325133 Color: 1

Bin 3179: 10 of cap free
Amount of items: 3
Items: 
Size: 497874 Color: 0
Size: 251174 Color: 1
Size: 250943 Color: 0

Bin 3180: 10 of cap free
Amount of items: 3
Items: 
Size: 458493 Color: 0
Size: 270808 Color: 1
Size: 270690 Color: 0

Bin 3181: 10 of cap free
Amount of items: 3
Items: 
Size: 367556 Color: 1
Size: 345882 Color: 1
Size: 286553 Color: 0

Bin 3182: 11 of cap free
Amount of items: 3
Items: 
Size: 491940 Color: 1
Size: 254037 Color: 0
Size: 254013 Color: 1

Bin 3183: 11 of cap free
Amount of items: 3
Items: 
Size: 475905 Color: 1
Size: 271847 Color: 0
Size: 252238 Color: 0

Bin 3184: 11 of cap free
Amount of items: 3
Items: 
Size: 422660 Color: 0
Size: 298569 Color: 1
Size: 278761 Color: 1

Bin 3185: 11 of cap free
Amount of items: 3
Items: 
Size: 335822 Color: 1
Size: 333921 Color: 0
Size: 330247 Color: 1

Bin 3186: 11 of cap free
Amount of items: 3
Items: 
Size: 412790 Color: 0
Size: 312937 Color: 1
Size: 274263 Color: 0

Bin 3187: 11 of cap free
Amount of items: 3
Items: 
Size: 350950 Color: 0
Size: 349971 Color: 1
Size: 299069 Color: 1

Bin 3188: 11 of cap free
Amount of items: 3
Items: 
Size: 351301 Color: 0
Size: 324952 Color: 1
Size: 323737 Color: 1

Bin 3189: 11 of cap free
Amount of items: 3
Items: 
Size: 498747 Color: 1
Size: 251070 Color: 1
Size: 250173 Color: 0

Bin 3190: 12 of cap free
Amount of items: 3
Items: 
Size: 455091 Color: 1
Size: 279787 Color: 0
Size: 265111 Color: 1

Bin 3191: 12 of cap free
Amount of items: 3
Items: 
Size: 343500 Color: 0
Size: 329273 Color: 1
Size: 327216 Color: 0

Bin 3192: 12 of cap free
Amount of items: 3
Items: 
Size: 352881 Color: 1
Size: 345173 Color: 0
Size: 301935 Color: 0

Bin 3193: 12 of cap free
Amount of items: 3
Items: 
Size: 352878 Color: 0
Size: 345681 Color: 0
Size: 301430 Color: 1

Bin 3194: 12 of cap free
Amount of items: 3
Items: 
Size: 364001 Color: 0
Size: 350351 Color: 0
Size: 285637 Color: 1

Bin 3195: 13 of cap free
Amount of items: 3
Items: 
Size: 447784 Color: 1
Size: 280321 Color: 0
Size: 271883 Color: 0

Bin 3196: 13 of cap free
Amount of items: 3
Items: 
Size: 485427 Color: 0
Size: 257966 Color: 0
Size: 256595 Color: 1

Bin 3197: 13 of cap free
Amount of items: 3
Items: 
Size: 497756 Color: 1
Size: 252096 Color: 0
Size: 250136 Color: 0

Bin 3198: 14 of cap free
Amount of items: 3
Items: 
Size: 339210 Color: 0
Size: 338634 Color: 1
Size: 322143 Color: 1

Bin 3199: 14 of cap free
Amount of items: 3
Items: 
Size: 483944 Color: 0
Size: 258078 Color: 0
Size: 257965 Color: 1

Bin 3200: 14 of cap free
Amount of items: 3
Items: 
Size: 382770 Color: 0
Size: 338833 Color: 1
Size: 278384 Color: 1

Bin 3201: 14 of cap free
Amount of items: 3
Items: 
Size: 378256 Color: 1
Size: 347121 Color: 0
Size: 274610 Color: 0

Bin 3202: 14 of cap free
Amount of items: 3
Items: 
Size: 376878 Color: 0
Size: 349540 Color: 0
Size: 273569 Color: 1

Bin 3203: 15 of cap free
Amount of items: 3
Items: 
Size: 351154 Color: 0
Size: 347894 Color: 1
Size: 300938 Color: 0

Bin 3204: 15 of cap free
Amount of items: 3
Items: 
Size: 391098 Color: 1
Size: 348320 Color: 0
Size: 260568 Color: 0

Bin 3205: 16 of cap free
Amount of items: 3
Items: 
Size: 478329 Color: 1
Size: 260898 Color: 1
Size: 260758 Color: 0

Bin 3206: 16 of cap free
Amount of items: 3
Items: 
Size: 476944 Color: 0
Size: 261892 Color: 1
Size: 261149 Color: 1

Bin 3207: 16 of cap free
Amount of items: 3
Items: 
Size: 444089 Color: 0
Size: 304039 Color: 1
Size: 251857 Color: 0

Bin 3208: 16 of cap free
Amount of items: 3
Items: 
Size: 497899 Color: 0
Size: 251971 Color: 1
Size: 250115 Color: 1

Bin 3209: 16 of cap free
Amount of items: 3
Items: 
Size: 347407 Color: 1
Size: 345731 Color: 0
Size: 306847 Color: 0

Bin 3210: 17 of cap free
Amount of items: 3
Items: 
Size: 420022 Color: 0
Size: 301105 Color: 1
Size: 278857 Color: 0

Bin 3211: 17 of cap free
Amount of items: 3
Items: 
Size: 344453 Color: 1
Size: 333713 Color: 1
Size: 321818 Color: 0

Bin 3212: 17 of cap free
Amount of items: 3
Items: 
Size: 336867 Color: 1
Size: 334451 Color: 0
Size: 328666 Color: 1

Bin 3213: 17 of cap free
Amount of items: 3
Items: 
Size: 350903 Color: 1
Size: 344923 Color: 0
Size: 304158 Color: 1

Bin 3214: 18 of cap free
Amount of items: 3
Items: 
Size: 423744 Color: 0
Size: 319252 Color: 1
Size: 256987 Color: 0

Bin 3215: 18 of cap free
Amount of items: 3
Items: 
Size: 346329 Color: 1
Size: 345118 Color: 0
Size: 308536 Color: 0

Bin 3216: 18 of cap free
Amount of items: 3
Items: 
Size: 388426 Color: 1
Size: 324613 Color: 0
Size: 286944 Color: 0

Bin 3217: 18 of cap free
Amount of items: 3
Items: 
Size: 335397 Color: 0
Size: 334270 Color: 1
Size: 330316 Color: 1

Bin 3218: 19 of cap free
Amount of items: 3
Items: 
Size: 498767 Color: 0
Size: 251100 Color: 1
Size: 250115 Color: 1

Bin 3219: 20 of cap free
Amount of items: 3
Items: 
Size: 388882 Color: 1
Size: 327744 Color: 1
Size: 283355 Color: 0

Bin 3220: 20 of cap free
Amount of items: 3
Items: 
Size: 346101 Color: 1
Size: 333066 Color: 1
Size: 320814 Color: 0

Bin 3221: 20 of cap free
Amount of items: 3
Items: 
Size: 353834 Color: 0
Size: 336839 Color: 0
Size: 309308 Color: 1

Bin 3222: 20 of cap free
Amount of items: 3
Items: 
Size: 366063 Color: 1
Size: 350689 Color: 1
Size: 283229 Color: 0

Bin 3223: 20 of cap free
Amount of items: 3
Items: 
Size: 356924 Color: 0
Size: 326386 Color: 1
Size: 316671 Color: 0

Bin 3224: 21 of cap free
Amount of items: 3
Items: 
Size: 483452 Color: 1
Size: 264648 Color: 0
Size: 251880 Color: 0

Bin 3225: 21 of cap free
Amount of items: 3
Items: 
Size: 350758 Color: 1
Size: 350264 Color: 0
Size: 298958 Color: 1

Bin 3226: 21 of cap free
Amount of items: 3
Items: 
Size: 346369 Color: 1
Size: 346256 Color: 0
Size: 307355 Color: 0

Bin 3227: 22 of cap free
Amount of items: 3
Items: 
Size: 347853 Color: 0
Size: 340093 Color: 0
Size: 312033 Color: 1

Bin 3228: 22 of cap free
Amount of items: 3
Items: 
Size: 355823 Color: 1
Size: 325400 Color: 0
Size: 318756 Color: 0

Bin 3229: 22 of cap free
Amount of items: 3
Items: 
Size: 441491 Color: 1
Size: 291878 Color: 0
Size: 266610 Color: 0

Bin 3230: 23 of cap free
Amount of items: 3
Items: 
Size: 403773 Color: 0
Size: 324639 Color: 0
Size: 271566 Color: 1

Bin 3231: 23 of cap free
Amount of items: 3
Items: 
Size: 448768 Color: 0
Size: 286603 Color: 0
Size: 264607 Color: 1

Bin 3232: 23 of cap free
Amount of items: 3
Items: 
Size: 456255 Color: 0
Size: 273064 Color: 0
Size: 270659 Color: 1

Bin 3233: 23 of cap free
Amount of items: 3
Items: 
Size: 344023 Color: 1
Size: 337009 Color: 0
Size: 318946 Color: 1

Bin 3234: 24 of cap free
Amount of items: 3
Items: 
Size: 388968 Color: 1
Size: 340950 Color: 0
Size: 270059 Color: 0

Bin 3235: 24 of cap free
Amount of items: 3
Items: 
Size: 461572 Color: 1
Size: 269889 Color: 0
Size: 268516 Color: 1

Bin 3236: 25 of cap free
Amount of items: 3
Items: 
Size: 351346 Color: 1
Size: 342467 Color: 0
Size: 306163 Color: 0

Bin 3237: 25 of cap free
Amount of items: 3
Items: 
Size: 335598 Color: 1
Size: 333028 Color: 0
Size: 331350 Color: 1

Bin 3238: 25 of cap free
Amount of items: 3
Items: 
Size: 337196 Color: 1
Size: 336685 Color: 1
Size: 326095 Color: 0

Bin 3239: 26 of cap free
Amount of items: 3
Items: 
Size: 484533 Color: 1
Size: 258688 Color: 0
Size: 256754 Color: 1

Bin 3240: 27 of cap free
Amount of items: 3
Items: 
Size: 468963 Color: 0
Size: 267672 Color: 1
Size: 263339 Color: 0

Bin 3241: 27 of cap free
Amount of items: 3
Items: 
Size: 409053 Color: 0
Size: 318155 Color: 1
Size: 272766 Color: 0

Bin 3242: 29 of cap free
Amount of items: 3
Items: 
Size: 453477 Color: 0
Size: 273585 Color: 1
Size: 272910 Color: 0

Bin 3243: 31 of cap free
Amount of items: 3
Items: 
Size: 488684 Color: 0
Size: 255810 Color: 1
Size: 255476 Color: 1

Bin 3244: 32 of cap free
Amount of items: 3
Items: 
Size: 456583 Color: 0
Size: 273991 Color: 0
Size: 269395 Color: 1

Bin 3245: 32 of cap free
Amount of items: 3
Items: 
Size: 342832 Color: 0
Size: 329840 Color: 1
Size: 327297 Color: 1

Bin 3246: 32 of cap free
Amount of items: 3
Items: 
Size: 457393 Color: 1
Size: 279930 Color: 0
Size: 262646 Color: 0

Bin 3247: 33 of cap free
Amount of items: 3
Items: 
Size: 355412 Color: 1
Size: 344787 Color: 0
Size: 299769 Color: 1

Bin 3248: 33 of cap free
Amount of items: 3
Items: 
Size: 362714 Color: 1
Size: 349351 Color: 1
Size: 287903 Color: 0

Bin 3249: 33 of cap free
Amount of items: 3
Items: 
Size: 368852 Color: 1
Size: 351328 Color: 0
Size: 279788 Color: 1

Bin 3250: 35 of cap free
Amount of items: 3
Items: 
Size: 473698 Color: 1
Size: 264027 Color: 1
Size: 262241 Color: 0

Bin 3251: 37 of cap free
Amount of items: 3
Items: 
Size: 410264 Color: 0
Size: 336961 Color: 1
Size: 252739 Color: 1

Bin 3252: 39 of cap free
Amount of items: 3
Items: 
Size: 359984 Color: 1
Size: 350816 Color: 1
Size: 289162 Color: 0

Bin 3253: 42 of cap free
Amount of items: 3
Items: 
Size: 427592 Color: 0
Size: 288809 Color: 1
Size: 283558 Color: 0

Bin 3254: 42 of cap free
Amount of items: 3
Items: 
Size: 352050 Color: 0
Size: 326737 Color: 0
Size: 321172 Color: 1

Bin 3255: 44 of cap free
Amount of items: 3
Items: 
Size: 351147 Color: 1
Size: 337657 Color: 0
Size: 311153 Color: 1

Bin 3256: 44 of cap free
Amount of items: 3
Items: 
Size: 347922 Color: 0
Size: 340626 Color: 1
Size: 311409 Color: 0

Bin 3257: 45 of cap free
Amount of items: 3
Items: 
Size: 455891 Color: 0
Size: 275210 Color: 1
Size: 268855 Color: 1

Bin 3258: 46 of cap free
Amount of items: 3
Items: 
Size: 498062 Color: 0
Size: 251532 Color: 1
Size: 250361 Color: 1

Bin 3259: 55 of cap free
Amount of items: 3
Items: 
Size: 424498 Color: 1
Size: 323239 Color: 1
Size: 252209 Color: 0

Bin 3260: 55 of cap free
Amount of items: 3
Items: 
Size: 419798 Color: 0
Size: 295838 Color: 1
Size: 284310 Color: 1

Bin 3261: 56 of cap free
Amount of items: 3
Items: 
Size: 383218 Color: 0
Size: 349340 Color: 1
Size: 267387 Color: 1

Bin 3262: 56 of cap free
Amount of items: 3
Items: 
Size: 429653 Color: 1
Size: 290001 Color: 0
Size: 280291 Color: 1

Bin 3263: 56 of cap free
Amount of items: 3
Items: 
Size: 358046 Color: 0
Size: 349608 Color: 1
Size: 292291 Color: 0

Bin 3264: 57 of cap free
Amount of items: 3
Items: 
Size: 463767 Color: 0
Size: 269195 Color: 1
Size: 266982 Color: 0

Bin 3265: 57 of cap free
Amount of items: 3
Items: 
Size: 366989 Color: 0
Size: 348379 Color: 1
Size: 284576 Color: 0

Bin 3266: 58 of cap free
Amount of items: 3
Items: 
Size: 340081 Color: 0
Size: 331913 Color: 1
Size: 327949 Color: 0

Bin 3267: 60 of cap free
Amount of items: 3
Items: 
Size: 399557 Color: 1
Size: 330054 Color: 0
Size: 270330 Color: 0

Bin 3268: 60 of cap free
Amount of items: 3
Items: 
Size: 405622 Color: 1
Size: 338235 Color: 0
Size: 256084 Color: 1

Bin 3269: 62 of cap free
Amount of items: 3
Items: 
Size: 407939 Color: 0
Size: 339345 Color: 0
Size: 252655 Color: 1

Bin 3270: 63 of cap free
Amount of items: 3
Items: 
Size: 346162 Color: 1
Size: 344903 Color: 1
Size: 308873 Color: 0

Bin 3271: 63 of cap free
Amount of items: 3
Items: 
Size: 349902 Color: 1
Size: 347680 Color: 1
Size: 302356 Color: 0

Bin 3272: 67 of cap free
Amount of items: 3
Items: 
Size: 361107 Color: 0
Size: 351526 Color: 1
Size: 287301 Color: 1

Bin 3273: 68 of cap free
Amount of items: 3
Items: 
Size: 344018 Color: 0
Size: 330613 Color: 1
Size: 325302 Color: 0

Bin 3274: 70 of cap free
Amount of items: 3
Items: 
Size: 439506 Color: 1
Size: 294071 Color: 0
Size: 266354 Color: 0

Bin 3275: 70 of cap free
Amount of items: 3
Items: 
Size: 337914 Color: 0
Size: 337469 Color: 0
Size: 324548 Color: 1

Bin 3276: 73 of cap free
Amount of items: 3
Items: 
Size: 339951 Color: 1
Size: 330405 Color: 0
Size: 329572 Color: 0

Bin 3277: 77 of cap free
Amount of items: 3
Items: 
Size: 470105 Color: 0
Size: 265588 Color: 0
Size: 264231 Color: 1

Bin 3278: 86 of cap free
Amount of items: 3
Items: 
Size: 376268 Color: 0
Size: 332705 Color: 1
Size: 290942 Color: 1

Bin 3279: 88 of cap free
Amount of items: 3
Items: 
Size: 345843 Color: 1
Size: 339412 Color: 1
Size: 314658 Color: 0

Bin 3280: 94 of cap free
Amount of items: 3
Items: 
Size: 334898 Color: 1
Size: 334713 Color: 0
Size: 330296 Color: 0

Bin 3281: 97 of cap free
Amount of items: 3
Items: 
Size: 341698 Color: 0
Size: 341306 Color: 0
Size: 316900 Color: 1

Bin 3282: 100 of cap free
Amount of items: 3
Items: 
Size: 361488 Color: 1
Size: 345445 Color: 0
Size: 292968 Color: 0

Bin 3283: 102 of cap free
Amount of items: 3
Items: 
Size: 367382 Color: 0
Size: 343618 Color: 1
Size: 288899 Color: 0

Bin 3284: 104 of cap free
Amount of items: 3
Items: 
Size: 356692 Color: 0
Size: 355810 Color: 1
Size: 287395 Color: 0

Bin 3285: 109 of cap free
Amount of items: 3
Items: 
Size: 394051 Color: 0
Size: 336920 Color: 1
Size: 268921 Color: 1

Bin 3286: 115 of cap free
Amount of items: 3
Items: 
Size: 428097 Color: 1
Size: 289561 Color: 0
Size: 282228 Color: 0

Bin 3287: 118 of cap free
Amount of items: 3
Items: 
Size: 336225 Color: 1
Size: 335557 Color: 1
Size: 328101 Color: 0

Bin 3288: 121 of cap free
Amount of items: 3
Items: 
Size: 498801 Color: 1
Size: 251039 Color: 0
Size: 250040 Color: 1

Bin 3289: 125 of cap free
Amount of items: 3
Items: 
Size: 346161 Color: 1
Size: 337587 Color: 0
Size: 316128 Color: 0

Bin 3290: 129 of cap free
Amount of items: 3
Items: 
Size: 384751 Color: 1
Size: 328718 Color: 0
Size: 286403 Color: 0

Bin 3291: 142 of cap free
Amount of items: 3
Items: 
Size: 493207 Color: 0
Size: 253439 Color: 1
Size: 253213 Color: 0

Bin 3292: 158 of cap free
Amount of items: 3
Items: 
Size: 414260 Color: 0
Size: 309392 Color: 0
Size: 276191 Color: 1

Bin 3293: 161 of cap free
Amount of items: 3
Items: 
Size: 349865 Color: 0
Size: 349317 Color: 0
Size: 300658 Color: 1

Bin 3294: 166 of cap free
Amount of items: 3
Items: 
Size: 345444 Color: 0
Size: 332161 Color: 1
Size: 322230 Color: 1

Bin 3295: 207 of cap free
Amount of items: 3
Items: 
Size: 361410 Color: 1
Size: 352537 Color: 0
Size: 285847 Color: 1

Bin 3296: 208 of cap free
Amount of items: 2
Items: 
Size: 499987 Color: 0
Size: 499806 Color: 1

Bin 3297: 210 of cap free
Amount of items: 3
Items: 
Size: 412048 Color: 0
Size: 312780 Color: 0
Size: 274963 Color: 1

Bin 3298: 226 of cap free
Amount of items: 3
Items: 
Size: 351508 Color: 0
Size: 325132 Color: 1
Size: 323135 Color: 0

Bin 3299: 231 of cap free
Amount of items: 3
Items: 
Size: 415080 Color: 1
Size: 301797 Color: 0
Size: 282893 Color: 0

Bin 3300: 249 of cap free
Amount of items: 3
Items: 
Size: 365138 Color: 0
Size: 329760 Color: 1
Size: 304854 Color: 0

Bin 3301: 275 of cap free
Amount of items: 3
Items: 
Size: 419952 Color: 1
Size: 309376 Color: 0
Size: 270398 Color: 0

Bin 3302: 297 of cap free
Amount of items: 3
Items: 
Size: 348562 Color: 1
Size: 336009 Color: 0
Size: 315133 Color: 1

Bin 3303: 299 of cap free
Amount of items: 3
Items: 
Size: 378670 Color: 0
Size: 340607 Color: 1
Size: 280425 Color: 0

Bin 3304: 351 of cap free
Amount of items: 3
Items: 
Size: 432394 Color: 1
Size: 296408 Color: 0
Size: 270848 Color: 1

Bin 3305: 359 of cap free
Amount of items: 3
Items: 
Size: 408247 Color: 1
Size: 317437 Color: 1
Size: 273958 Color: 0

Bin 3306: 367 of cap free
Amount of items: 3
Items: 
Size: 353320 Color: 0
Size: 334531 Color: 0
Size: 311783 Color: 1

Bin 3307: 399 of cap free
Amount of items: 3
Items: 
Size: 445104 Color: 0
Size: 282026 Color: 1
Size: 272472 Color: 1

Bin 3308: 431 of cap free
Amount of items: 3
Items: 
Size: 448713 Color: 0
Size: 300654 Color: 0
Size: 250203 Color: 1

Bin 3309: 481 of cap free
Amount of items: 3
Items: 
Size: 366313 Color: 0
Size: 342509 Color: 1
Size: 290698 Color: 1

Bin 3310: 500 of cap free
Amount of items: 3
Items: 
Size: 443637 Color: 0
Size: 279025 Color: 0
Size: 276839 Color: 1

Bin 3311: 554 of cap free
Amount of items: 3
Items: 
Size: 471557 Color: 0
Size: 265124 Color: 1
Size: 262766 Color: 0

Bin 3312: 614 of cap free
Amount of items: 3
Items: 
Size: 385145 Color: 0
Size: 345807 Color: 1
Size: 268435 Color: 1

Bin 3313: 690 of cap free
Amount of items: 3
Items: 
Size: 355075 Color: 1
Size: 352555 Color: 1
Size: 291681 Color: 0

Bin 3314: 780 of cap free
Amount of items: 2
Items: 
Size: 499910 Color: 0
Size: 499311 Color: 1

Bin 3315: 1130 of cap free
Amount of items: 2
Items: 
Size: 499697 Color: 0
Size: 499174 Color: 1

Bin 3316: 1515 of cap free
Amount of items: 3
Items: 
Size: 450298 Color: 0
Size: 278149 Color: 1
Size: 270039 Color: 1

Bin 3317: 1686 of cap free
Amount of items: 3
Items: 
Size: 344089 Color: 0
Size: 327621 Color: 1
Size: 326605 Color: 1

Bin 3318: 1834 of cap free
Amount of items: 2
Items: 
Size: 499531 Color: 0
Size: 498636 Color: 1

Bin 3319: 2399 of cap free
Amount of items: 2
Items: 
Size: 499439 Color: 0
Size: 498163 Color: 1

Bin 3320: 2616 of cap free
Amount of items: 2
Items: 
Size: 499234 Color: 0
Size: 498151 Color: 1

Bin 3321: 3037 of cap free
Amount of items: 2
Items: 
Size: 498834 Color: 0
Size: 498130 Color: 1

Bin 3322: 3391 of cap free
Amount of items: 2
Items: 
Size: 498503 Color: 0
Size: 498107 Color: 1

Bin 3323: 4427 of cap free
Amount of items: 3
Items: 
Size: 356019 Color: 1
Size: 350048 Color: 0
Size: 289507 Color: 0

Bin 3324: 26006 of cap free
Amount of items: 3
Items: 
Size: 339583 Color: 0
Size: 326354 Color: 1
Size: 308058 Color: 0

Bin 3325: 189209 of cap free
Amount of items: 3
Items: 
Size: 273451 Color: 0
Size: 272284 Color: 1
Size: 265057 Color: 0

Bin 3326: 210822 of cap free
Amount of items: 3
Items: 
Size: 263441 Color: 0
Size: 263428 Color: 0
Size: 262310 Color: 1

Bin 3327: 215456 of cap free
Amount of items: 3
Items: 
Size: 262126 Color: 1
Size: 261217 Color: 1
Size: 261202 Color: 0

Bin 3328: 219178 of cap free
Amount of items: 3
Items: 
Size: 261091 Color: 1
Size: 259891 Color: 0
Size: 259841 Color: 0

Bin 3329: 222221 of cap free
Amount of items: 3
Items: 
Size: 259589 Color: 0
Size: 259186 Color: 1
Size: 259005 Color: 1

Bin 3330: 224651 of cap free
Amount of items: 3
Items: 
Size: 258732 Color: 1
Size: 258387 Color: 1
Size: 258231 Color: 0

Bin 3331: 229203 of cap free
Amount of items: 3
Items: 
Size: 257704 Color: 1
Size: 256686 Color: 0
Size: 256408 Color: 0

Bin 3332: 231556 of cap free
Amount of items: 3
Items: 
Size: 256576 Color: 1
Size: 256017 Color: 0
Size: 255852 Color: 0

Bin 3333: 233851 of cap free
Amount of items: 3
Items: 
Size: 255466 Color: 1
Size: 255378 Color: 1
Size: 255306 Color: 0

Bin 3334: 235408 of cap free
Amount of items: 3
Items: 
Size: 255120 Color: 1
Size: 254827 Color: 0
Size: 254646 Color: 1

Bin 3335: 237374 of cap free
Amount of items: 3
Items: 
Size: 254591 Color: 1
Size: 254485 Color: 1
Size: 253551 Color: 0

Bin 3336: 239915 of cap free
Amount of items: 3
Items: 
Size: 254259 Color: 1
Size: 252935 Color: 0
Size: 252892 Color: 1

Bin 3337: 249790 of cap free
Amount of items: 2
Items: 
Size: 498363 Color: 0
Size: 251848 Color: 1

Total size: 3334003334
Total free space: 3000003

