Capicity Bin: 2048
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 160
Size: 554 Color: 119
Size: 36 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 161
Size: 491 Color: 113
Size: 96 Color: 49

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 163
Size: 406 Color: 106
Size: 120 Color: 56

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 167
Size: 388 Color: 101
Size: 86 Color: 47

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 170
Size: 362 Color: 100
Size: 104 Color: 52

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 171
Size: 389 Color: 102
Size: 76 Color: 41

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 172
Size: 258 Color: 82
Size: 202 Color: 73

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 174
Size: 351 Color: 98
Size: 72 Color: 38

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 179
Size: 317 Color: 94
Size: 62 Color: 32

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 180
Size: 313 Color: 91
Size: 62 Color: 31

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 181
Size: 314 Color: 92
Size: 60 Color: 29

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1703 Color: 184
Size: 289 Color: 88
Size: 56 Color: 26

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 191
Size: 261 Color: 83
Size: 20 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 192
Size: 274 Color: 86
Size: 4 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 195
Size: 215 Color: 76
Size: 42 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 196
Size: 211 Color: 75
Size: 42 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 200
Size: 168 Color: 67
Size: 62 Color: 30

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 201
Size: 146 Color: 63
Size: 80 Color: 45

Bin 19: 1 of cap free
Amount of items: 4
Items: 
Size: 1321 Color: 151
Size: 630 Color: 124
Size: 48 Color: 19
Size: 48 Color: 18

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 165
Size: 442 Color: 110
Size: 78 Color: 43

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 166
Size: 477 Color: 112
Size: 8 Color: 1

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1575 Color: 168
Size: 472 Color: 111

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 169
Size: 398 Color: 105
Size: 70 Color: 36

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 177
Size: 194 Color: 70
Size: 190 Color: 69

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 183
Size: 220 Color: 77
Size: 126 Color: 58

Bin 26: 2 of cap free
Amount of items: 4
Items: 
Size: 1199 Color: 145
Size: 731 Color: 130
Size: 60 Color: 28
Size: 56 Color: 27

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1285 Color: 149
Size: 709 Color: 129
Size: 52 Color: 21

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 150
Size: 696 Color: 127
Size: 52 Color: 20

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 173
Size: 424 Color: 108
Size: 8 Color: 2

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1725 Color: 187
Size: 321 Color: 95

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1775 Color: 194
Size: 271 Color: 84

Bin 32: 3 of cap free
Amount of items: 5
Items: 
Size: 1415 Color: 156
Size: 494 Color: 115
Size: 52 Color: 23
Size: 44 Color: 13
Size: 40 Color: 10

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1523 Color: 164
Size: 522 Color: 117

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1654 Color: 176
Size: 391 Color: 103

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 178
Size: 316 Color: 93
Size: 64 Color: 34

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1754 Color: 189
Size: 291 Color: 89

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1763 Color: 190
Size: 282 Color: 87

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1771 Color: 193
Size: 274 Color: 85

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1806 Color: 197
Size: 239 Color: 80

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1810 Color: 198
Size: 235 Color: 79

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 1814 Color: 199
Size: 231 Color: 78

Bin 42: 4 of cap free
Amount of items: 5
Items: 
Size: 1379 Color: 153
Size: 493 Color: 114
Size: 78 Color: 44
Size: 48 Color: 17
Size: 46 Color: 16

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1714 Color: 185
Size: 330 Color: 97

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1742 Color: 188
Size: 302 Color: 90

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 162
Size: 529 Color: 118
Size: 12 Color: 3

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 1629 Color: 175
Size: 414 Color: 107

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 1690 Color: 182
Size: 353 Color: 99

Bin 48: 5 of cap free
Amount of items: 2
Items: 
Size: 1722 Color: 186
Size: 321 Color: 96

Bin 49: 9 of cap free
Amount of items: 3
Items: 
Size: 1386 Color: 155
Size: 607 Color: 123
Size: 46 Color: 14

Bin 50: 10 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 148
Size: 706 Color: 128
Size: 52 Color: 22

Bin 51: 11 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 144
Size: 802 Color: 133
Size: 64 Color: 33

Bin 52: 14 of cap free
Amount of items: 4
Items: 
Size: 1026 Color: 139
Size: 828 Color: 134
Size: 96 Color: 48
Size: 84 Color: 46

Bin 53: 14 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 154
Size: 605 Color: 122
Size: 46 Color: 15

Bin 54: 16 of cap free
Amount of items: 4
Items: 
Size: 1029 Color: 140
Size: 851 Color: 135
Size: 76 Color: 42
Size: 76 Color: 40

Bin 55: 19 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 147
Size: 739 Color: 132
Size: 56 Color: 24

Bin 56: 21 of cap free
Amount of items: 4
Items: 
Size: 1457 Color: 159
Size: 498 Color: 116
Size: 36 Color: 7
Size: 36 Color: 6

Bin 57: 24 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 158
Size: 562 Color: 121
Size: 36 Color: 8

Bin 58: 31 of cap free
Amount of items: 2
Items: 
Size: 1163 Color: 142
Size: 854 Color: 137

Bin 59: 32 of cap free
Amount of items: 6
Items: 
Size: 1025 Color: 138
Size: 439 Color: 109
Size: 246 Color: 81
Size: 108 Color: 53
Size: 100 Color: 51
Size: 98 Color: 50

Bin 60: 33 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 141
Size: 853 Color: 136
Size: 76 Color: 39

Bin 61: 33 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 157
Size: 559 Color: 120
Size: 40 Color: 9

Bin 62: 37 of cap free
Amount of items: 2
Items: 
Size: 1374 Color: 152
Size: 637 Color: 125

Bin 63: 55 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 146
Size: 735 Color: 131
Size: 56 Color: 25

Bin 64: 59 of cap free
Amount of items: 4
Items: 
Size: 1167 Color: 143
Size: 682 Color: 126
Size: 72 Color: 37
Size: 68 Color: 35

Bin 65: 93 of cap free
Amount of items: 11
Items: 
Size: 395 Color: 104
Size: 208 Color: 74
Size: 202 Color: 72
Size: 198 Color: 71
Size: 170 Color: 68
Size: 168 Color: 66
Size: 140 Color: 60
Size: 132 Color: 59
Size: 120 Color: 57
Size: 112 Color: 55
Size: 110 Color: 54

Bin 66: 1456 of cap free
Amount of items: 4
Items: 
Size: 160 Color: 65
Size: 146 Color: 64
Size: 146 Color: 62
Size: 140 Color: 61

Total size: 133120
Total free space: 2048

