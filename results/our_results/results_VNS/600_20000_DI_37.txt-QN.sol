Capicity Bin: 16000
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11444 Color: 462
Size: 4364 Color: 360
Size: 192 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11971 Color: 469
Size: 3949 Color: 349
Size: 80 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12452 Color: 482
Size: 3240 Color: 331
Size: 308 Color: 36

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12818 Color: 494
Size: 2886 Color: 318
Size: 296 Color: 31

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12892 Color: 497
Size: 2786 Color: 314
Size: 322 Color: 47

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 499
Size: 2636 Color: 309
Size: 444 Color: 96

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 500
Size: 2542 Color: 305
Size: 504 Color: 111

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 504
Size: 2628 Color: 308
Size: 312 Color: 39

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13063 Color: 505
Size: 1961 Color: 269
Size: 976 Color: 175

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13173 Color: 508
Size: 1449 Color: 218
Size: 1378 Color: 208

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 511
Size: 2438 Color: 297
Size: 340 Color: 54

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 512
Size: 1444 Color: 216
Size: 1332 Color: 202

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13305 Color: 515
Size: 2247 Color: 289
Size: 448 Color: 99

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13459 Color: 521
Size: 1517 Color: 226
Size: 1024 Color: 180

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13460 Color: 522
Size: 2120 Color: 282
Size: 420 Color: 90

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13484 Color: 526
Size: 1984 Color: 272
Size: 532 Color: 120

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 530
Size: 1848 Color: 260
Size: 556 Color: 122

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13692 Color: 536
Size: 1748 Color: 249
Size: 560 Color: 124

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13716 Color: 539
Size: 1984 Color: 271
Size: 300 Color: 32

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 540
Size: 1656 Color: 239
Size: 624 Color: 132

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13796 Color: 543
Size: 1844 Color: 259
Size: 360 Color: 67

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 544
Size: 1512 Color: 225
Size: 688 Color: 144

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13809 Color: 545
Size: 1633 Color: 238
Size: 558 Color: 123

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13894 Color: 550
Size: 1822 Color: 256
Size: 284 Color: 24

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13913 Color: 554
Size: 1511 Color: 224
Size: 576 Color: 125

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13942 Color: 556
Size: 1562 Color: 228
Size: 496 Color: 109

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 557
Size: 1768 Color: 252
Size: 288 Color: 26

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13958 Color: 558
Size: 1454 Color: 220
Size: 588 Color: 128

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13974 Color: 559
Size: 1586 Color: 232
Size: 440 Color: 94

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 564
Size: 1156 Color: 190
Size: 792 Color: 158

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14098 Color: 569
Size: 1120 Color: 185
Size: 782 Color: 155

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14109 Color: 570
Size: 1471 Color: 221
Size: 420 Color: 91

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 571
Size: 1618 Color: 237
Size: 268 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14130 Color: 573
Size: 1358 Color: 205
Size: 512 Color: 113

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 578
Size: 1506 Color: 223
Size: 300 Color: 33

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 579
Size: 1040 Color: 184
Size: 760 Color: 152

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14204 Color: 580
Size: 1494 Color: 222
Size: 302 Color: 34

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 585
Size: 1148 Color: 189
Size: 584 Color: 127

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 588
Size: 1368 Color: 207
Size: 340 Color: 55

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14294 Color: 589
Size: 1438 Color: 215
Size: 268 Color: 13

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14334 Color: 593
Size: 1390 Color: 209
Size: 276 Color: 20

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14350 Color: 594
Size: 1024 Color: 181
Size: 626 Color: 133

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 595
Size: 1328 Color: 200
Size: 316 Color: 42

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 596
Size: 1176 Color: 193
Size: 464 Color: 103

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14374 Color: 597
Size: 1342 Color: 203
Size: 284 Color: 25

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14376 Color: 598
Size: 1176 Color: 194
Size: 448 Color: 98

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 599
Size: 1144 Color: 187
Size: 468 Color: 104

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 600
Size: 1034 Color: 182
Size: 576 Color: 126

Bin 49: 1 of cap free
Amount of items: 9
Items: 
Size: 8003 Color: 407
Size: 1348 Color: 204
Size: 1332 Color: 201
Size: 1328 Color: 199
Size: 1328 Color: 198
Size: 1264 Color: 196
Size: 664 Color: 141
Size: 368 Color: 69
Size: 364 Color: 68

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 10818 Color: 446
Size: 5181 Color: 378

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 12463 Color: 484
Size: 3124 Color: 327
Size: 412 Color: 84

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 12649 Color: 488
Size: 3000 Color: 323
Size: 350 Color: 61

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 12658 Color: 489
Size: 2949 Color: 320
Size: 392 Color: 80

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 12805 Color: 493
Size: 1617 Color: 236
Size: 1577 Color: 231

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 12959 Color: 501
Size: 1588 Color: 234
Size: 1452 Color: 219

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 513
Size: 2449 Color: 298
Size: 312 Color: 38

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 13351 Color: 518
Size: 2648 Color: 310

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 13464 Color: 524
Size: 2535 Color: 304

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 562
Size: 1975 Color: 270

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 14172 Color: 575
Size: 1827 Color: 257

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 14189 Color: 577
Size: 1810 Color: 254

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 14258 Color: 583
Size: 1741 Color: 248

Bin 63: 2 of cap free
Amount of items: 5
Items: 
Size: 9140 Color: 423
Size: 5732 Color: 382
Size: 456 Color: 101
Size: 336 Color: 51
Size: 334 Color: 50

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 10644 Color: 440
Size: 5066 Color: 375
Size: 288 Color: 28

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 10850 Color: 448
Size: 5148 Color: 377

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 11064 Color: 452
Size: 4934 Color: 369

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 459
Size: 4442 Color: 362
Size: 224 Color: 8

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 11428 Color: 461
Size: 4362 Color: 359
Size: 208 Color: 6

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 12070 Color: 473
Size: 3928 Color: 348

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 12214 Color: 477
Size: 3436 Color: 337
Size: 348 Color: 60

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 12642 Color: 487
Size: 3356 Color: 335

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 12840 Color: 495
Size: 3158 Color: 329

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 13478 Color: 525
Size: 2520 Color: 302

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13830 Color: 548
Size: 2168 Color: 285

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 552
Size: 2102 Color: 279

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13908 Color: 553
Size: 2090 Color: 277

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 14124 Color: 572
Size: 1874 Color: 262

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14210 Color: 581
Size: 1788 Color: 253

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14278 Color: 587
Size: 1720 Color: 246

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14296 Color: 590
Size: 1702 Color: 244

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14308 Color: 591
Size: 1690 Color: 243

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 13204 Color: 510
Size: 2793 Color: 315

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 14085 Color: 567
Size: 1912 Color: 264

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 14089 Color: 568
Size: 1908 Color: 263

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 9922 Color: 430
Size: 5762 Color: 384
Size: 312 Color: 37

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 10082 Color: 435
Size: 5608 Color: 380
Size: 306 Color: 35

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 465
Size: 2964 Color: 321
Size: 1312 Color: 197

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 11858 Color: 467
Size: 4042 Color: 351
Size: 96 Color: 2

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 12984 Color: 503
Size: 3012 Color: 324

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 520
Size: 2580 Color: 307

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13624 Color: 531
Size: 2372 Color: 296

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 537
Size: 2302 Color: 291

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14004 Color: 561
Size: 1992 Color: 273

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14263 Color: 584
Size: 1733 Color: 247

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 10104 Color: 436
Size: 5891 Color: 390

Bin 96: 5 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 470
Size: 3627 Color: 341
Size: 388 Color: 79

Bin 97: 5 of cap free
Amount of items: 3
Items: 
Size: 12239 Color: 478
Size: 2968 Color: 322
Size: 788 Color: 157

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 13895 Color: 551
Size: 2100 Color: 278

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 14237 Color: 582
Size: 1758 Color: 251

Bin 100: 6 of cap free
Amount of items: 5
Items: 
Size: 9124 Color: 422
Size: 5686 Color: 381
Size: 512 Color: 116
Size: 336 Color: 53
Size: 336 Color: 52

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 10728 Color: 443
Size: 4986 Color: 372
Size: 280 Color: 22

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 445
Size: 4946 Color: 370
Size: 276 Color: 19

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 12456 Color: 483
Size: 3538 Color: 339

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13662 Color: 535
Size: 2332 Color: 293

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13818 Color: 546
Size: 2176 Color: 286

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 14276 Color: 586
Size: 1718 Color: 245

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 14326 Color: 592
Size: 1668 Color: 240

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 11649 Color: 464
Size: 4168 Color: 354
Size: 176 Color: 4

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 13921 Color: 555
Size: 2072 Color: 276

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 14181 Color: 576
Size: 1812 Color: 255

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 8088 Color: 412
Size: 7904 Color: 403

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 13462 Color: 523
Size: 2530 Color: 303

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 541
Size: 2238 Color: 288

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 14157 Color: 574
Size: 1835 Color: 258

Bin 115: 9 of cap free
Amount of items: 2
Items: 
Size: 13701 Color: 538
Size: 2290 Color: 290

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 14041 Color: 563
Size: 1950 Color: 268

Bin 117: 10 of cap free
Amount of items: 7
Items: 
Size: 8010 Color: 410
Size: 1755 Color: 250
Size: 1681 Color: 242
Size: 1676 Color: 241
Size: 1588 Color: 233
Size: 928 Color: 174
Size: 352 Color: 63

Bin 118: 10 of cap free
Amount of items: 3
Items: 
Size: 10674 Color: 442
Size: 5036 Color: 374
Size: 280 Color: 23

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 12908 Color: 498
Size: 3082 Color: 326

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 12966 Color: 502
Size: 3024 Color: 325

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 13528 Color: 529
Size: 2462 Color: 300

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 13632 Color: 533
Size: 2358 Color: 295

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 13985 Color: 560
Size: 2004 Color: 274

Bin 124: 12 of cap free
Amount of items: 2
Items: 
Size: 12252 Color: 479
Size: 3736 Color: 343

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 491
Size: 3244 Color: 332

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 13631 Color: 532
Size: 2357 Color: 294

Bin 127: 14 of cap free
Amount of items: 3
Items: 
Size: 9042 Color: 418
Size: 6600 Color: 395
Size: 344 Color: 56

Bin 128: 14 of cap free
Amount of items: 5
Items: 
Size: 10417 Color: 437
Size: 2119 Color: 281
Size: 2118 Color: 280
Size: 1040 Color: 183
Size: 292 Color: 30

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 13777 Color: 542
Size: 2209 Color: 287

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 547
Size: 2158 Color: 284

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 14062 Color: 566
Size: 1924 Color: 267

Bin 132: 16 of cap free
Amount of items: 2
Items: 
Size: 12056 Color: 472
Size: 3928 Color: 347

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 12408 Color: 481
Size: 3576 Color: 340

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 13156 Color: 507
Size: 2828 Color: 317

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 13860 Color: 549
Size: 2124 Color: 283

Bin 136: 17 of cap free
Amount of items: 2
Items: 
Size: 13320 Color: 517
Size: 2663 Color: 312

Bin 137: 17 of cap free
Amount of items: 2
Items: 
Size: 14061 Color: 565
Size: 1922 Color: 266

Bin 138: 18 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 492
Size: 3198 Color: 330

Bin 139: 18 of cap free
Amount of items: 2
Items: 
Size: 13254 Color: 514
Size: 2728 Color: 313

Bin 140: 18 of cap free
Amount of items: 2
Items: 
Size: 13414 Color: 519
Size: 2568 Color: 306

Bin 141: 19 of cap free
Amount of items: 2
Items: 
Size: 13525 Color: 528
Size: 2456 Color: 299

Bin 142: 20 of cap free
Amount of items: 2
Items: 
Size: 9028 Color: 417
Size: 6952 Color: 401

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 11528 Color: 463
Size: 4452 Color: 363

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 12526 Color: 485
Size: 3454 Color: 338

Bin 145: 21 of cap free
Amount of items: 7
Items: 
Size: 8856 Color: 413
Size: 2063 Color: 275
Size: 1917 Color: 265
Size: 1853 Color: 261
Size: 592 Color: 129
Size: 352 Color: 62
Size: 346 Color: 59

Bin 146: 21 of cap free
Amount of items: 3
Items: 
Size: 9707 Color: 426
Size: 5952 Color: 392
Size: 320 Color: 44

Bin 147: 21 of cap free
Amount of items: 2
Items: 
Size: 12844 Color: 496
Size: 3135 Color: 328

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 10018 Color: 431
Size: 5960 Color: 393

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 12166 Color: 476
Size: 3812 Color: 345

Bin 150: 23 of cap free
Amount of items: 3
Items: 
Size: 11106 Color: 453
Size: 3278 Color: 333
Size: 1593 Color: 235

Bin 151: 24 of cap free
Amount of items: 2
Items: 
Size: 13078 Color: 506
Size: 2898 Color: 319

Bin 152: 24 of cap free
Amount of items: 2
Items: 
Size: 13174 Color: 509
Size: 2802 Color: 316

Bin 153: 25 of cap free
Amount of items: 2
Items: 
Size: 13647 Color: 534
Size: 2328 Color: 292

Bin 154: 26 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 429
Size: 5812 Color: 386
Size: 314 Color: 40

Bin 155: 26 of cap free
Amount of items: 3
Items: 
Size: 11758 Color: 466
Size: 4120 Color: 353
Size: 96 Color: 3

Bin 156: 26 of cap free
Amount of items: 2
Items: 
Size: 13494 Color: 527
Size: 2480 Color: 301

Bin 157: 28 of cap free
Amount of items: 2
Items: 
Size: 13318 Color: 516
Size: 2654 Color: 311

Bin 158: 29 of cap free
Amount of items: 2
Items: 
Size: 12612 Color: 486
Size: 3359 Color: 336

Bin 159: 30 of cap free
Amount of items: 3
Items: 
Size: 10770 Color: 444
Size: 4920 Color: 368
Size: 280 Color: 21

Bin 160: 30 of cap free
Amount of items: 3
Items: 
Size: 12120 Color: 475
Size: 3804 Color: 344
Size: 46 Color: 0

Bin 161: 32 of cap free
Amount of items: 3
Items: 
Size: 11016 Color: 451
Size: 4680 Color: 365
Size: 272 Color: 16

Bin 162: 34 of cap free
Amount of items: 23
Items: 
Size: 868 Color: 169
Size: 864 Color: 168
Size: 864 Color: 167
Size: 860 Color: 166
Size: 860 Color: 165
Size: 860 Color: 164
Size: 816 Color: 163
Size: 816 Color: 162
Size: 816 Color: 161
Size: 812 Color: 160
Size: 804 Color: 159
Size: 784 Color: 156
Size: 768 Color: 154
Size: 768 Color: 153
Size: 752 Color: 151
Size: 752 Color: 150
Size: 556 Color: 121
Size: 400 Color: 83
Size: 400 Color: 82
Size: 394 Color: 81
Size: 384 Color: 78
Size: 384 Color: 77
Size: 384 Color: 76

Bin 163: 34 of cap free
Amount of items: 2
Items: 
Size: 11884 Color: 468
Size: 4082 Color: 352

Bin 164: 35 of cap free
Amount of items: 2
Items: 
Size: 8012 Color: 411
Size: 7953 Color: 404

Bin 165: 38 of cap free
Amount of items: 2
Items: 
Size: 10834 Color: 447
Size: 5128 Color: 376

Bin 166: 38 of cap free
Amount of items: 2
Items: 
Size: 12302 Color: 480
Size: 3660 Color: 342

Bin 167: 38 of cap free
Amount of items: 2
Items: 
Size: 12674 Color: 490
Size: 3288 Color: 334

Bin 168: 42 of cap free
Amount of items: 2
Items: 
Size: 10076 Color: 434
Size: 5882 Color: 388

Bin 169: 43 of cap free
Amount of items: 2
Items: 
Size: 8933 Color: 414
Size: 7024 Color: 402

Bin 170: 46 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 454
Size: 4528 Color: 364
Size: 272 Color: 15

Bin 171: 46 of cap free
Amount of items: 2
Items: 
Size: 12108 Color: 474
Size: 3846 Color: 346

Bin 172: 48 of cap free
Amount of items: 3
Items: 
Size: 8946 Color: 416
Size: 6662 Color: 397
Size: 344 Color: 57

Bin 173: 48 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 458
Size: 4408 Color: 361
Size: 240 Color: 9

Bin 174: 48 of cap free
Amount of items: 2
Items: 
Size: 11996 Color: 471
Size: 3956 Color: 350

Bin 175: 52 of cap free
Amount of items: 3
Items: 
Size: 8942 Color: 415
Size: 6660 Color: 396
Size: 346 Color: 58

Bin 176: 52 of cap free
Amount of items: 2
Items: 
Size: 10066 Color: 433
Size: 5882 Color: 389

Bin 177: 53 of cap free
Amount of items: 3
Items: 
Size: 10960 Color: 450
Size: 4715 Color: 366
Size: 272 Color: 17

Bin 178: 55 of cap free
Amount of items: 3
Items: 
Size: 11386 Color: 460
Size: 4335 Color: 358
Size: 224 Color: 7

Bin 179: 56 of cap free
Amount of items: 3
Items: 
Size: 10660 Color: 441
Size: 4996 Color: 373
Size: 288 Color: 27

Bin 180: 67 of cap free
Amount of items: 3
Items: 
Size: 10883 Color: 449
Size: 4778 Color: 367
Size: 272 Color: 18

Bin 181: 70 of cap free
Amount of items: 4
Items: 
Size: 9272 Color: 425
Size: 6018 Color: 394
Size: 320 Color: 46
Size: 320 Color: 45

Bin 182: 72 of cap free
Amount of items: 10
Items: 
Size: 8002 Color: 406
Size: 1184 Color: 195
Size: 1172 Color: 192
Size: 1160 Color: 191
Size: 1148 Color: 188
Size: 1136 Color: 186
Size: 1012 Color: 179
Size: 372 Color: 72
Size: 372 Color: 71
Size: 370 Color: 70

Bin 183: 81 of cap free
Amount of items: 7
Items: 
Size: 8008 Color: 409
Size: 1574 Color: 230
Size: 1564 Color: 229
Size: 1537 Color: 227
Size: 1444 Color: 217
Size: 1432 Color: 214
Size: 360 Color: 64

Bin 184: 97 of cap free
Amount of items: 3
Items: 
Size: 9785 Color: 428
Size: 5802 Color: 385
Size: 316 Color: 41

Bin 185: 103 of cap free
Amount of items: 2
Items: 
Size: 10060 Color: 432
Size: 5837 Color: 387

Bin 186: 114 of cap free
Amount of items: 3
Items: 
Size: 10639 Color: 439
Size: 4959 Color: 371
Size: 288 Color: 29

Bin 187: 150 of cap free
Amount of items: 3
Items: 
Size: 11288 Color: 457
Size: 4322 Color: 357
Size: 240 Color: 10

Bin 188: 173 of cap free
Amount of items: 3
Items: 
Size: 11263 Color: 456
Size: 4308 Color: 356
Size: 256 Color: 11

Bin 189: 186 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 455
Size: 4306 Color: 355
Size: 264 Color: 12

Bin 190: 188 of cap free
Amount of items: 3
Items: 
Size: 9748 Color: 427
Size: 5746 Color: 383
Size: 318 Color: 43

Bin 191: 210 of cap free
Amount of items: 2
Items: 
Size: 9122 Color: 421
Size: 6668 Color: 400

Bin 192: 220 of cap free
Amount of items: 4
Items: 
Size: 9178 Color: 424
Size: 5948 Color: 391
Size: 328 Color: 49
Size: 326 Color: 48

Bin 193: 228 of cap free
Amount of items: 2
Items: 
Size: 9106 Color: 420
Size: 6666 Color: 399

Bin 194: 245 of cap free
Amount of items: 2
Items: 
Size: 9090 Color: 419
Size: 6665 Color: 398

Bin 195: 248 of cap free
Amount of items: 8
Items: 
Size: 8004 Color: 408
Size: 1428 Color: 213
Size: 1422 Color: 212
Size: 1412 Color: 211
Size: 1398 Color: 210
Size: 1368 Color: 206
Size: 360 Color: 66
Size: 360 Color: 65

Bin 196: 264 of cap free
Amount of items: 28
Items: 
Size: 736 Color: 149
Size: 724 Color: 148
Size: 712 Color: 147
Size: 704 Color: 146
Size: 704 Color: 145
Size: 680 Color: 143
Size: 670 Color: 142
Size: 656 Color: 140
Size: 652 Color: 139
Size: 648 Color: 138
Size: 646 Color: 137
Size: 640 Color: 136
Size: 636 Color: 135
Size: 628 Color: 134
Size: 624 Color: 131
Size: 472 Color: 106
Size: 470 Color: 105
Size: 460 Color: 102
Size: 456 Color: 100
Size: 448 Color: 97
Size: 440 Color: 95
Size: 428 Color: 93
Size: 422 Color: 92
Size: 416 Color: 89
Size: 416 Color: 88
Size: 416 Color: 87
Size: 416 Color: 86
Size: 416 Color: 85

Bin 197: 284 of cap free
Amount of items: 2
Items: 
Size: 10504 Color: 438
Size: 5212 Color: 379

Bin 198: 309 of cap free
Amount of items: 11
Items: 
Size: 8001 Color: 405
Size: 996 Color: 178
Size: 988 Color: 177
Size: 984 Color: 176
Size: 912 Color: 173
Size: 896 Color: 172
Size: 888 Color: 171
Size: 884 Color: 170
Size: 384 Color: 75
Size: 382 Color: 74
Size: 376 Color: 73

Bin 199: 10802 of cap free
Amount of items: 10
Items: 
Size: 616 Color: 130
Size: 528 Color: 119
Size: 528 Color: 118
Size: 520 Color: 117
Size: 512 Color: 115
Size: 512 Color: 114
Size: 506 Color: 112
Size: 504 Color: 110
Size: 488 Color: 108
Size: 484 Color: 107

Total size: 3168000
Total free space: 16000

