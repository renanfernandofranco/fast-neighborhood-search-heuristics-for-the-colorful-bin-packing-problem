Capicity Bin: 7360
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4308 Color: 288
Size: 2872 Color: 263
Size: 180 Color: 37

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 4386 Color: 291
Size: 2482 Color: 249
Size: 168 Color: 34
Size: 164 Color: 33
Size: 160 Color: 32

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5032 Color: 307
Size: 1850 Color: 229
Size: 478 Color: 117

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5292 Color: 318
Size: 1544 Color: 214
Size: 524 Color: 124

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5305 Color: 319
Size: 1713 Color: 223
Size: 342 Color: 95

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5308 Color: 320
Size: 1724 Color: 225
Size: 328 Color: 90

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5321 Color: 322
Size: 1897 Color: 232
Size: 142 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5337 Color: 323
Size: 1687 Color: 219
Size: 336 Color: 92

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5418 Color: 324
Size: 1832 Color: 227
Size: 110 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 329
Size: 1516 Color: 212
Size: 296 Color: 83

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5576 Color: 330
Size: 968 Color: 174
Size: 816 Color: 158

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5584 Color: 331
Size: 1536 Color: 213
Size: 240 Color: 62

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 336
Size: 1386 Color: 205
Size: 276 Color: 76

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5767 Color: 340
Size: 1329 Color: 200
Size: 264 Color: 72

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 344
Size: 1244 Color: 196
Size: 240 Color: 65

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5944 Color: 349
Size: 942 Color: 170
Size: 474 Color: 116

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5950 Color: 350
Size: 1252 Color: 197
Size: 158 Color: 30

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 355
Size: 866 Color: 162
Size: 422 Color: 111

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 357
Size: 1031 Color: 180
Size: 204 Color: 51

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6130 Color: 358
Size: 758 Color: 149
Size: 472 Color: 115

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 360
Size: 830 Color: 159
Size: 384 Color: 106

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 364
Size: 1028 Color: 179
Size: 148 Color: 25

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6197 Color: 365
Size: 731 Color: 147
Size: 432 Color: 112

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6200 Color: 366
Size: 760 Color: 150
Size: 400 Color: 107

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 373
Size: 917 Color: 166
Size: 160 Color: 31

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 375
Size: 642 Color: 137
Size: 378 Color: 104

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6349 Color: 376
Size: 799 Color: 155
Size: 212 Color: 54

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 384
Size: 612 Color: 134
Size: 320 Color: 89

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6456 Color: 387
Size: 536 Color: 126
Size: 368 Color: 101

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6459 Color: 388
Size: 705 Color: 144
Size: 196 Color: 48

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6483 Color: 390
Size: 719 Color: 145
Size: 158 Color: 29

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6486 Color: 391
Size: 730 Color: 146
Size: 144 Color: 20

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6523 Color: 395
Size: 699 Color: 143
Size: 138 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6531 Color: 396
Size: 691 Color: 141
Size: 138 Color: 17

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6563 Color: 399
Size: 665 Color: 139
Size: 132 Color: 14

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 401
Size: 538 Color: 127
Size: 232 Color: 60

Bin 37: 1 of cap free
Amount of items: 6
Items: 
Size: 3690 Color: 277
Size: 1103 Color: 186
Size: 1102 Color: 185
Size: 1080 Color: 183
Size: 192 Color: 44
Size: 192 Color: 43

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5320 Color: 321
Size: 1701 Color: 221
Size: 338 Color: 94

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5765 Color: 339
Size: 1374 Color: 204
Size: 220 Color: 56

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 371
Size: 608 Color: 130
Size: 492 Color: 119

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6296 Color: 374
Size: 1063 Color: 182

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6419 Color: 382
Size: 940 Color: 169

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6424 Color: 383
Size: 935 Color: 168

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6454 Color: 386
Size: 905 Color: 165

Bin 45: 2 of cap free
Amount of items: 9
Items: 
Size: 3682 Color: 273
Size: 608 Color: 131
Size: 564 Color: 129
Size: 560 Color: 128
Size: 528 Color: 125
Size: 520 Color: 123
Size: 480 Color: 118
Size: 208 Color: 53
Size: 208 Color: 52

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 4837 Color: 304
Size: 2393 Color: 247
Size: 128 Color: 12

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 5897 Color: 346
Size: 1461 Color: 209

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 6234 Color: 369
Size: 1124 Color: 188

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 6470 Color: 389
Size: 888 Color: 164

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6515 Color: 394
Size: 843 Color: 160

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6578 Color: 400
Size: 780 Color: 152

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6616 Color: 402
Size: 742 Color: 148

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 5142 Color: 314
Size: 2103 Color: 238
Size: 112 Color: 7

Bin 54: 3 of cap free
Amount of items: 2
Items: 
Size: 5448 Color: 325
Size: 1909 Color: 234

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 5653 Color: 334
Size: 1704 Color: 222

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 5934 Color: 348
Size: 1423 Color: 206

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 6021 Color: 353
Size: 1336 Color: 202

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 6037 Color: 354
Size: 1320 Color: 199

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 6085 Color: 356
Size: 1272 Color: 198

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 6228 Color: 368
Size: 1129 Color: 189

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 6366 Color: 377
Size: 991 Color: 177

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6403 Color: 381
Size: 954 Color: 172

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 6488 Color: 392
Size: 869 Color: 163

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 4505 Color: 294
Size: 2699 Color: 258
Size: 152 Color: 26

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 5660 Color: 335
Size: 1696 Color: 220

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 5748 Color: 338
Size: 1608 Color: 217

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 5860 Color: 343
Size: 1496 Color: 211

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 5880 Color: 345
Size: 1460 Color: 208
Size: 16 Color: 0

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 6239 Color: 370
Size: 1117 Color: 187

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6275 Color: 372
Size: 1081 Color: 184

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 6372 Color: 378
Size: 984 Color: 176

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 6540 Color: 397
Size: 816 Color: 157

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 6555 Color: 398
Size: 801 Color: 156

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 4196 Color: 286
Size: 2975 Color: 264
Size: 184 Color: 40

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 4489 Color: 293
Size: 2712 Color: 259
Size: 154 Color: 27

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 6007 Color: 351
Size: 1348 Color: 203

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 361
Size: 1207 Color: 194

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 6499 Color: 393
Size: 856 Color: 161

Bin 79: 6 of cap free
Amount of items: 7
Items: 
Size: 3688 Color: 276
Size: 1036 Color: 181
Size: 1026 Color: 178
Size: 964 Color: 173
Size: 256 Color: 68
Size: 192 Color: 46
Size: 192 Color: 45

Bin 80: 6 of cap free
Amount of items: 5
Items: 
Size: 4280 Color: 287
Size: 2444 Color: 248
Size: 264 Color: 71
Size: 184 Color: 39
Size: 182 Color: 38

Bin 81: 6 of cap free
Amount of items: 3
Items: 
Size: 4521 Color: 295
Size: 2685 Color: 257
Size: 148 Color: 24

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6218 Color: 367
Size: 1136 Color: 190

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 6435 Color: 385
Size: 919 Color: 167

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 4844 Color: 305
Size: 2381 Color: 246
Size: 128 Color: 11

Bin 85: 7 of cap free
Amount of items: 3
Items: 
Size: 5071 Color: 308
Size: 1902 Color: 233
Size: 380 Color: 105

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 5470 Color: 326
Size: 1883 Color: 231

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 5501 Color: 327
Size: 1852 Color: 230

Bin 88: 7 of cap free
Amount of items: 2
Items: 
Size: 5637 Color: 333
Size: 1716 Color: 224

Bin 89: 7 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 359
Size: 1221 Color: 195

Bin 90: 7 of cap free
Amount of items: 2
Items: 
Size: 6382 Color: 379
Size: 971 Color: 175

Bin 91: 8 of cap free
Amount of items: 7
Items: 
Size: 3684 Color: 275
Size: 792 Color: 154
Size: 785 Color: 153
Size: 771 Color: 151
Size: 684 Color: 140
Size: 444 Color: 113
Size: 192 Color: 47

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 4820 Color: 302
Size: 2532 Color: 252

Bin 93: 8 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 313
Size: 2100 Color: 237
Size: 112 Color: 8

Bin 94: 9 of cap free
Amount of items: 11
Items: 
Size: 3681 Color: 272
Size: 512 Color: 122
Size: 504 Color: 121
Size: 504 Color: 120
Size: 468 Color: 114
Size: 420 Color: 110
Size: 376 Color: 103
Size: 224 Color: 59
Size: 224 Color: 58
Size: 222 Color: 57
Size: 216 Color: 55

Bin 95: 9 of cap free
Amount of items: 2
Items: 
Size: 6020 Color: 352
Size: 1331 Color: 201

Bin 96: 9 of cap free
Amount of items: 2
Items: 
Size: 6173 Color: 363
Size: 1178 Color: 191

Bin 97: 10 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 290
Size: 2834 Color: 262
Size: 176 Color: 35

Bin 98: 10 of cap free
Amount of items: 2
Items: 
Size: 5913 Color: 347
Size: 1437 Color: 207

Bin 99: 11 of cap free
Amount of items: 2
Items: 
Size: 6157 Color: 362
Size: 1192 Color: 193

Bin 100: 11 of cap free
Amount of items: 2
Items: 
Size: 6401 Color: 380
Size: 948 Color: 171

Bin 101: 12 of cap free
Amount of items: 2
Items: 
Size: 5512 Color: 328
Size: 1836 Color: 228

Bin 102: 13 of cap free
Amount of items: 2
Items: 
Size: 5607 Color: 332
Size: 1740 Color: 226

Bin 103: 14 of cap free
Amount of items: 2
Items: 
Size: 5768 Color: 341
Size: 1578 Color: 216

Bin 104: 16 of cap free
Amount of items: 3
Items: 
Size: 4436 Color: 292
Size: 2752 Color: 260
Size: 156 Color: 28

Bin 105: 18 of cap free
Amount of items: 2
Items: 
Size: 4794 Color: 301
Size: 2548 Color: 253

Bin 106: 20 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 297
Size: 2644 Color: 256
Size: 144 Color: 22

Bin 107: 20 of cap free
Amount of items: 3
Items: 
Size: 4872 Color: 306
Size: 2346 Color: 244
Size: 122 Color: 10

Bin 108: 20 of cap free
Amount of items: 5
Items: 
Size: 5276 Color: 317
Size: 1944 Color: 235
Size: 56 Color: 5
Size: 32 Color: 2
Size: 32 Color: 1

Bin 109: 24 of cap free
Amount of items: 2
Items: 
Size: 5088 Color: 311
Size: 2248 Color: 243

Bin 110: 24 of cap free
Amount of items: 2
Items: 
Size: 5714 Color: 337
Size: 1622 Color: 218

Bin 111: 24 of cap free
Amount of items: 2
Items: 
Size: 5848 Color: 342
Size: 1488 Color: 210

Bin 112: 26 of cap free
Amount of items: 3
Items: 
Size: 4546 Color: 296
Size: 2642 Color: 255
Size: 146 Color: 23

Bin 113: 28 of cap free
Amount of items: 3
Items: 
Size: 5176 Color: 316
Size: 2124 Color: 240
Size: 32 Color: 3

Bin 114: 31 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 315
Size: 2117 Color: 239
Size: 48 Color: 4

Bin 115: 32 of cap free
Amount of items: 3
Items: 
Size: 4324 Color: 289
Size: 2828 Color: 261
Size: 176 Color: 36

Bin 116: 37 of cap free
Amount of items: 2
Items: 
Size: 5085 Color: 310
Size: 2238 Color: 242

Bin 117: 38 of cap free
Amount of items: 3
Items: 
Size: 4682 Color: 300
Size: 2504 Color: 251
Size: 136 Color: 15

Bin 118: 40 of cap free
Amount of items: 3
Items: 
Size: 4608 Color: 298
Size: 2568 Color: 254
Size: 144 Color: 21

Bin 119: 44 of cap free
Amount of items: 5
Items: 
Size: 3692 Color: 278
Size: 1551 Color: 215
Size: 1190 Color: 192
Size: 695 Color: 142
Size: 188 Color: 42

Bin 120: 44 of cap free
Amount of items: 3
Items: 
Size: 4680 Color: 299
Size: 2496 Color: 250
Size: 140 Color: 18

Bin 121: 44 of cap free
Amount of items: 3
Items: 
Size: 4821 Color: 303
Size: 2367 Color: 245
Size: 128 Color: 13

Bin 122: 44 of cap free
Amount of items: 2
Items: 
Size: 5082 Color: 309
Size: 2234 Color: 241

Bin 123: 54 of cap free
Amount of items: 2
Items: 
Size: 3928 Color: 279
Size: 3378 Color: 271

Bin 124: 59 of cap free
Amount of items: 3
Items: 
Size: 5101 Color: 312
Size: 2088 Color: 236
Size: 112 Color: 9

Bin 125: 98 of cap free
Amount of items: 2
Items: 
Size: 4194 Color: 285
Size: 3068 Color: 270

Bin 126: 152 of cap free
Amount of items: 3
Items: 
Size: 3962 Color: 280
Size: 3060 Color: 265
Size: 186 Color: 41

Bin 127: 154 of cap free
Amount of items: 2
Items: 
Size: 4139 Color: 284
Size: 3067 Color: 269

Bin 128: 155 of cap free
Amount of items: 8
Items: 
Size: 3683 Color: 274
Size: 654 Color: 138
Size: 632 Color: 136
Size: 612 Color: 135
Size: 612 Color: 133
Size: 608 Color: 132
Size: 204 Color: 50
Size: 200 Color: 49

Bin 129: 171 of cap free
Amount of items: 2
Items: 
Size: 4123 Color: 283
Size: 3066 Color: 268

Bin 130: 174 of cap free
Amount of items: 2
Items: 
Size: 4121 Color: 282
Size: 3065 Color: 267

Bin 131: 176 of cap free
Amount of items: 2
Items: 
Size: 4120 Color: 281
Size: 3064 Color: 266

Bin 132: 258 of cap free
Amount of items: 23
Items: 
Size: 416 Color: 109
Size: 416 Color: 108
Size: 376 Color: 102
Size: 368 Color: 100
Size: 360 Color: 99
Size: 352 Color: 98
Size: 344 Color: 97
Size: 344 Color: 96
Size: 336 Color: 93
Size: 336 Color: 91
Size: 312 Color: 88
Size: 308 Color: 87
Size: 284 Color: 77
Size: 272 Color: 75
Size: 272 Color: 74
Size: 272 Color: 73
Size: 264 Color: 70
Size: 264 Color: 69
Size: 248 Color: 67
Size: 242 Color: 66
Size: 240 Color: 64
Size: 240 Color: 63
Size: 236 Color: 61

Bin 133: 5002 of cap free
Amount of items: 8
Items: 
Size: 304 Color: 86
Size: 304 Color: 85
Size: 304 Color: 84
Size: 292 Color: 82
Size: 292 Color: 81
Size: 288 Color: 80
Size: 288 Color: 79
Size: 286 Color: 78

Total size: 971520
Total free space: 7360

