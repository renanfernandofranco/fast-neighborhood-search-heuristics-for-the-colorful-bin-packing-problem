Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 357 Color: 0
Size: 273 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 295 Color: 1
Size: 259 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 329 Color: 4
Size: 314 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 366 Color: 3
Size: 268 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 366 Color: 2
Size: 262 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 350 Color: 4
Size: 255 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 276 Color: 2
Size: 258 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 3
Size: 262 Color: 4
Size: 253 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 2
Size: 252 Color: 4
Size: 251 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 3
Size: 274 Color: 2
Size: 252 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 3
Size: 254 Color: 3
Size: 251 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 336 Color: 1
Size: 254 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 315 Color: 1
Size: 271 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 0
Size: 347 Color: 3
Size: 303 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 4
Size: 307 Color: 1
Size: 263 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 3
Size: 302 Color: 2
Size: 259 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 362 Color: 3
Size: 272 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 298 Color: 3
Size: 282 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 266 Color: 0
Size: 254 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 275 Color: 0
Size: 252 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 3
Size: 287 Color: 1
Size: 269 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 4
Size: 253 Color: 1
Size: 250 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 350 Color: 3
Size: 253 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 272 Color: 4
Size: 250 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 4
Size: 288 Color: 2
Size: 267 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 333 Color: 0
Size: 260 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 269 Color: 0
Size: 259 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 325 Color: 0
Size: 256 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 320 Color: 4
Size: 275 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 363 Color: 3
Size: 271 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 356 Color: 4
Size: 283 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 2
Size: 363 Color: 4
Size: 261 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 313 Color: 4
Size: 253 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 3
Size: 298 Color: 3
Size: 290 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 267 Color: 2
Size: 263 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 4
Size: 282 Color: 4
Size: 273 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 4
Size: 298 Color: 1
Size: 252 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 301 Color: 1
Size: 299 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 352 Color: 1
Size: 292 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 0
Size: 351 Color: 1
Size: 294 Color: 3

Total size: 40000
Total free space: 0

