Capicity Bin: 1864
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 6
Size: 602 Color: 2
Size: 116 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1168 Color: 12
Size: 666 Color: 5
Size: 30 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1292 Color: 6
Size: 428 Color: 9
Size: 144 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 4
Size: 380 Color: 7
Size: 68 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 2
Size: 246 Color: 19
Size: 196 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 4
Size: 274 Color: 17
Size: 120 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 19
Size: 330 Color: 0
Size: 44 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1519 Color: 15
Size: 191 Color: 8
Size: 154 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 5
Size: 251 Color: 16
Size: 34 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 11
Size: 182 Color: 15
Size: 88 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1619 Color: 6
Size: 157 Color: 14
Size: 88 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 12
Size: 202 Color: 13
Size: 36 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 15
Size: 193 Color: 12
Size: 38 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 2
Size: 163 Color: 16
Size: 52 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 9
Size: 150 Color: 8
Size: 64 Color: 7

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 13
Size: 753 Color: 7
Size: 76 Color: 16

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 11
Size: 737 Color: 2
Size: 60 Color: 13

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1231 Color: 1
Size: 632 Color: 8

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1257 Color: 16
Size: 554 Color: 6
Size: 52 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1435 Color: 4
Size: 370 Color: 14
Size: 58 Color: 6

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1443 Color: 18
Size: 388 Color: 13
Size: 32 Color: 9

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1511 Color: 8
Size: 250 Color: 9
Size: 102 Color: 8

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1637 Color: 8
Size: 226 Color: 14

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1642 Color: 17
Size: 221 Color: 3

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 2
Size: 205 Color: 12

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1669 Color: 15
Size: 194 Color: 7

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1677 Color: 7
Size: 186 Color: 12

Bin 28: 2 of cap free
Amount of items: 5
Items: 
Size: 933 Color: 16
Size: 422 Color: 19
Size: 375 Color: 11
Size: 76 Color: 9
Size: 56 Color: 10

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1333 Color: 14
Size: 529 Color: 15

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1551 Color: 13
Size: 311 Color: 17

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 10
Size: 208 Color: 2
Size: 20 Color: 6

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1131 Color: 3
Size: 694 Color: 8
Size: 36 Color: 5

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1123 Color: 6
Size: 738 Color: 4

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 16
Size: 382 Color: 18
Size: 124 Color: 9

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 0
Size: 431 Color: 0
Size: 40 Color: 3

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1415 Color: 10
Size: 446 Color: 5

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1565 Color: 2
Size: 288 Color: 13
Size: 8 Color: 12

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1566 Color: 14
Size: 295 Color: 1

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1636 Color: 8
Size: 213 Color: 14
Size: 12 Color: 10

Bin 40: 4 of cap free
Amount of items: 4
Items: 
Size: 1135 Color: 4
Size: 619 Color: 14
Size: 70 Color: 13
Size: 36 Color: 2

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1249 Color: 9
Size: 611 Color: 19

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1538 Color: 10
Size: 314 Color: 0
Size: 8 Color: 6

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1599 Color: 9
Size: 261 Color: 10

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1621 Color: 5
Size: 239 Color: 18

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 982 Color: 17
Size: 731 Color: 3
Size: 146 Color: 17

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 16
Size: 306 Color: 6
Size: 62 Color: 5

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 1
Size: 289 Color: 14

Bin 48: 6 of cap free
Amount of items: 4
Items: 
Size: 941 Color: 13
Size: 777 Color: 12
Size: 104 Color: 19
Size: 36 Color: 4

Bin 49: 7 of cap free
Amount of items: 3
Items: 
Size: 934 Color: 7
Size: 879 Color: 18
Size: 44 Color: 1

Bin 50: 7 of cap free
Amount of items: 3
Items: 
Size: 1447 Color: 4
Size: 398 Color: 13
Size: 12 Color: 3

Bin 51: 7 of cap free
Amount of items: 2
Items: 
Size: 1498 Color: 7
Size: 359 Color: 9

Bin 52: 7 of cap free
Amount of items: 2
Items: 
Size: 1506 Color: 11
Size: 351 Color: 16

Bin 53: 9 of cap free
Amount of items: 19
Items: 
Size: 204 Color: 14
Size: 181 Color: 8
Size: 152 Color: 5
Size: 144 Color: 13
Size: 136 Color: 1
Size: 108 Color: 4
Size: 104 Color: 19
Size: 100 Color: 6
Size: 96 Color: 9
Size: 86 Color: 0
Size: 74 Color: 11
Size: 72 Color: 3
Size: 70 Color: 10
Size: 68 Color: 17
Size: 68 Color: 10
Size: 68 Color: 3
Size: 60 Color: 6
Size: 32 Color: 5
Size: 32 Color: 2

Bin 54: 9 of cap free
Amount of items: 3
Items: 
Size: 981 Color: 9
Size: 752 Color: 16
Size: 122 Color: 11

Bin 55: 10 of cap free
Amount of items: 2
Items: 
Size: 1347 Color: 12
Size: 507 Color: 17

Bin 56: 10 of cap free
Amount of items: 3
Items: 
Size: 1358 Color: 6
Size: 302 Color: 17
Size: 194 Color: 8

Bin 57: 12 of cap free
Amount of items: 3
Items: 
Size: 961 Color: 8
Size: 542 Color: 16
Size: 349 Color: 10

Bin 58: 13 of cap free
Amount of items: 3
Items: 
Size: 989 Color: 17
Size: 778 Color: 16
Size: 84 Color: 19

Bin 59: 15 of cap free
Amount of items: 2
Items: 
Size: 1218 Color: 9
Size: 631 Color: 18

Bin 60: 15 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 2
Size: 443 Color: 5

Bin 61: 21 of cap free
Amount of items: 3
Items: 
Size: 1286 Color: 11
Size: 425 Color: 6
Size: 132 Color: 8

Bin 62: 21 of cap free
Amount of items: 2
Items: 
Size: 1330 Color: 15
Size: 513 Color: 12

Bin 63: 22 of cap free
Amount of items: 4
Items: 
Size: 937 Color: 1
Size: 773 Color: 15
Size: 84 Color: 3
Size: 48 Color: 8

Bin 64: 24 of cap free
Amount of items: 3
Items: 
Size: 1109 Color: 18
Size: 609 Color: 12
Size: 122 Color: 10

Bin 65: 26 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 7
Size: 482 Color: 9
Size: 154 Color: 3

Bin 66: 1544 of cap free
Amount of items: 7
Items: 
Size: 56 Color: 11
Size: 48 Color: 19
Size: 48 Color: 14
Size: 48 Color: 13
Size: 46 Color: 8
Size: 44 Color: 0
Size: 30 Color: 2

Total size: 121160
Total free space: 1864

