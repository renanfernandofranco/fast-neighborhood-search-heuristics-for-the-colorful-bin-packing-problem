Capicity Bin: 2000
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1069 Color: 142
Size: 773 Color: 131
Size: 58 Color: 30
Size: 52 Color: 23
Size: 48 Color: 22

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 159
Size: 562 Color: 120
Size: 32 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 162
Size: 498 Color: 117
Size: 32 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1479 Color: 163
Size: 501 Color: 118
Size: 20 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 172
Size: 262 Color: 90
Size: 132 Color: 62

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1642 Color: 175
Size: 302 Color: 97
Size: 56 Color: 29

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 176
Size: 295 Color: 96
Size: 58 Color: 31

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 180
Size: 265 Color: 92
Size: 52 Color: 26

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1724 Color: 187
Size: 236 Color: 85
Size: 40 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 188
Size: 237 Color: 86
Size: 38 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 189
Size: 210 Color: 76
Size: 64 Color: 34

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 190
Size: 225 Color: 82
Size: 34 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 191
Size: 202 Color: 75
Size: 56 Color: 28

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 192
Size: 211 Color: 77
Size: 44 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 193
Size: 187 Color: 73
Size: 64 Color: 33

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 199
Size: 181 Color: 70
Size: 36 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 201
Size: 164 Color: 67
Size: 42 Color: 16

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1313 Color: 152
Size: 642 Color: 125
Size: 44 Color: 18

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 174
Size: 362 Color: 105
Size: 8 Color: 1

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1655 Color: 178
Size: 344 Color: 103

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 185
Size: 186 Color: 72
Size: 100 Color: 53

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1717 Color: 186
Size: 282 Color: 93

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1758 Color: 195
Size: 241 Color: 87

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1786 Color: 200
Size: 213 Color: 78

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 160
Size: 573 Color: 122
Size: 16 Color: 2

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1417 Color: 161
Size: 581 Color: 124

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1541 Color: 167
Size: 457 Color: 112

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1567 Color: 168
Size: 431 Color: 110

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1707 Color: 184
Size: 291 Color: 95

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1778 Color: 198
Size: 220 Color: 81

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1230 Color: 148
Size: 767 Color: 130

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1515 Color: 165
Size: 442 Color: 111
Size: 40 Color: 12

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1592 Color: 171
Size: 405 Color: 108

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1686 Color: 182
Size: 311 Color: 99

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1777 Color: 197
Size: 220 Color: 80

Bin 36: 4 of cap free
Amount of items: 7
Items: 
Size: 1002 Color: 139
Size: 245 Color: 88
Size: 230 Color: 83
Size: 218 Color: 79
Size: 197 Color: 74
Size: 52 Color: 27
Size: 52 Color: 25

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 1666 Color: 179
Size: 330 Color: 101

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1750 Color: 194
Size: 246 Color: 89

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 196
Size: 231 Color: 84

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 1326 Color: 154
Size: 669 Color: 127

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1384 Color: 155
Size: 571 Color: 121
Size: 40 Color: 15

Bin 42: 5 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 170
Size: 398 Color: 107
Size: 8 Color: 0

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1706 Color: 183
Size: 289 Color: 94

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 1256 Color: 149
Size: 738 Color: 129

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1611 Color: 173
Size: 383 Color: 106

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 1651 Color: 177
Size: 343 Color: 102

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1317 Color: 153
Size: 676 Color: 128

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 181
Size: 308 Color: 98

Bin 49: 9 of cap free
Amount of items: 9
Items: 
Size: 1001 Color: 138
Size: 174 Color: 69
Size: 166 Color: 68
Size: 154 Color: 66
Size: 154 Color: 65
Size: 154 Color: 64
Size: 64 Color: 36
Size: 64 Color: 35
Size: 60 Color: 32

Bin 50: 11 of cap free
Amount of items: 5
Items: 
Size: 1005 Color: 140
Size: 487 Color: 114
Size: 263 Color: 91
Size: 182 Color: 71
Size: 52 Color: 24

Bin 51: 11 of cap free
Amount of items: 3
Items: 
Size: 1303 Color: 150
Size: 361 Color: 104
Size: 325 Color: 100

Bin 52: 12 of cap free
Amount of items: 3
Items: 
Size: 1134 Color: 145
Size: 808 Color: 134
Size: 46 Color: 19

Bin 53: 14 of cap free
Amount of items: 2
Items: 
Size: 1205 Color: 147
Size: 781 Color: 133

Bin 54: 14 of cap free
Amount of items: 2
Items: 
Size: 1485 Color: 164
Size: 501 Color: 119

Bin 55: 14 of cap free
Amount of items: 2
Items: 
Size: 1526 Color: 166
Size: 460 Color: 113

Bin 56: 14 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 169
Size: 416 Color: 109

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 158
Size: 577 Color: 123

Bin 58: 24 of cap free
Amount of items: 20
Items: 
Size: 152 Color: 63
Size: 132 Color: 61
Size: 128 Color: 60
Size: 128 Color: 59
Size: 116 Color: 58
Size: 114 Color: 57
Size: 114 Color: 56
Size: 112 Color: 55
Size: 112 Color: 54
Size: 98 Color: 52
Size: 98 Color: 51
Size: 84 Color: 45
Size: 80 Color: 44
Size: 76 Color: 43
Size: 76 Color: 42
Size: 76 Color: 41
Size: 72 Color: 40
Size: 72 Color: 39
Size: 68 Color: 38
Size: 68 Color: 37

Bin 59: 24 of cap free
Amount of items: 2
Items: 
Size: 1199 Color: 146
Size: 777 Color: 132

Bin 60: 28 of cap free
Amount of items: 2
Items: 
Size: 1065 Color: 141
Size: 907 Color: 137

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1309 Color: 151
Size: 663 Color: 126

Bin 62: 28 of cap free
Amount of items: 4
Items: 
Size: 1399 Color: 156
Size: 493 Color: 115
Size: 40 Color: 13
Size: 40 Color: 11

Bin 63: 30 of cap free
Amount of items: 4
Items: 
Size: 1401 Color: 157
Size: 497 Color: 116
Size: 36 Color: 9
Size: 36 Color: 8

Bin 64: 39 of cap free
Amount of items: 3
Items: 
Size: 1081 Color: 144
Size: 834 Color: 136
Size: 46 Color: 20

Bin 65: 46 of cap free
Amount of items: 3
Items: 
Size: 1073 Color: 143
Size: 833 Color: 135
Size: 48 Color: 21

Bin 66: 1534 of cap free
Amount of items: 5
Items: 
Size: 98 Color: 50
Size: 96 Color: 49
Size: 96 Color: 48
Size: 88 Color: 47
Size: 88 Color: 46

Total size: 130000
Total free space: 2000

