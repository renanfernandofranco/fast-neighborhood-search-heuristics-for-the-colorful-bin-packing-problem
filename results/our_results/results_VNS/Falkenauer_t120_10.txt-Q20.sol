Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 7
Size: 301 Color: 16
Size: 263 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 6
Size: 342 Color: 1
Size: 251 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 8
Size: 272 Color: 2
Size: 257 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 4
Size: 276 Color: 6
Size: 272 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 13
Size: 257 Color: 6
Size: 255 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 3
Size: 339 Color: 13
Size: 306 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 292 Color: 12
Size: 266 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 8
Size: 320 Color: 14
Size: 303 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 19
Size: 298 Color: 4
Size: 264 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 14
Size: 312 Color: 11
Size: 281 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 2
Size: 330 Color: 4
Size: 285 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 4
Size: 258 Color: 1
Size: 250 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 18
Size: 287 Color: 18
Size: 287 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 18
Size: 258 Color: 10
Size: 251 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 5
Size: 255 Color: 1
Size: 250 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8
Size: 306 Color: 12
Size: 269 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 3
Size: 346 Color: 11
Size: 292 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 3
Size: 287 Color: 6
Size: 251 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 5
Size: 339 Color: 10
Size: 253 Color: 14

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 13
Size: 297 Color: 6
Size: 282 Color: 6

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 7
Size: 297 Color: 0
Size: 282 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 12
Size: 308 Color: 6
Size: 277 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 14
Size: 316 Color: 9
Size: 294 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 16
Size: 300 Color: 6
Size: 279 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 14
Size: 368 Color: 17
Size: 254 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 12
Size: 355 Color: 7
Size: 284 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 9
Size: 346 Color: 8
Size: 298 Color: 8

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 14
Size: 329 Color: 14
Size: 319 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 13
Size: 352 Color: 1
Size: 265 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 15
Size: 265 Color: 0
Size: 257 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7
Size: 355 Color: 13
Size: 269 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 9
Size: 305 Color: 19
Size: 305 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 297 Color: 4
Size: 262 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 19
Size: 269 Color: 18
Size: 257 Color: 7

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 8
Size: 345 Color: 17
Size: 270 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 5
Size: 266 Color: 6
Size: 255 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 8
Size: 315 Color: 6
Size: 258 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 15
Size: 254 Color: 12
Size: 251 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 16
Size: 273 Color: 14
Size: 268 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 10
Size: 324 Color: 1
Size: 274 Color: 15

Total size: 40000
Total free space: 0

