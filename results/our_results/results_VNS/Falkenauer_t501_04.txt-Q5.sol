Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 2
Size: 254 Color: 3
Size: 253 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 288 Color: 0
Size: 263 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 0
Size: 269 Color: 4
Size: 268 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 252 Color: 3
Size: 250 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 315 Color: 4
Size: 296 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 326 Color: 3
Size: 280 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 3
Size: 278 Color: 0
Size: 261 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 269 Color: 1
Size: 266 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 3
Size: 276 Color: 0
Size: 272 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 331 Color: 2
Size: 266 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 338 Color: 3
Size: 282 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 357 Color: 2
Size: 261 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 278 Color: 2
Size: 276 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 2
Size: 251 Color: 3
Size: 250 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 365 Color: 1
Size: 253 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 341 Color: 0
Size: 258 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 320 Color: 1
Size: 304 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 0
Size: 330 Color: 2
Size: 255 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 285 Color: 3
Size: 252 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 261 Color: 1
Size: 253 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 330 Color: 1
Size: 250 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 3
Size: 280 Color: 4
Size: 270 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 352 Color: 1
Size: 252 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 341 Color: 2
Size: 298 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 2
Size: 346 Color: 0
Size: 257 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 3
Size: 353 Color: 0
Size: 287 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 307 Color: 4
Size: 255 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 343 Color: 2
Size: 283 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 2
Size: 254 Color: 1
Size: 253 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 4
Size: 318 Color: 3
Size: 265 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 4
Size: 286 Color: 0
Size: 267 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 360 Color: 0
Size: 255 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 4
Size: 264 Color: 4
Size: 259 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 4
Size: 293 Color: 3
Size: 282 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 338 Color: 1
Size: 282 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 2
Size: 274 Color: 4
Size: 267 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 3
Size: 360 Color: 3
Size: 253 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 346 Color: 2
Size: 279 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 344 Color: 0
Size: 284 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 4
Size: 264 Color: 4
Size: 252 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 368 Color: 3
Size: 260 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 4
Size: 352 Color: 3
Size: 269 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 319 Color: 1
Size: 260 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 4
Size: 274 Color: 0
Size: 266 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 256 Color: 3
Size: 256 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 4
Size: 294 Color: 3
Size: 284 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 275 Color: 2
Size: 255 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 3
Size: 315 Color: 1
Size: 263 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 329 Color: 2
Size: 273 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 3
Size: 351 Color: 2
Size: 280 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 4
Size: 292 Color: 3
Size: 269 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 335 Color: 3
Size: 296 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 314 Color: 4
Size: 288 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 356 Color: 2
Size: 269 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 0
Size: 263 Color: 4
Size: 250 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 3
Size: 282 Color: 1
Size: 256 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 259 Color: 0
Size: 256 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 4
Size: 277 Color: 3
Size: 253 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 311 Color: 4
Size: 280 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 340 Color: 3
Size: 257 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 3
Size: 316 Color: 0
Size: 281 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 291 Color: 3
Size: 283 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 281 Color: 1
Size: 258 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 320 Color: 1
Size: 272 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 325 Color: 0
Size: 265 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 3
Size: 264 Color: 2
Size: 251 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 326 Color: 2
Size: 252 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 288 Color: 3
Size: 259 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 312 Color: 3
Size: 260 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 3
Size: 330 Color: 1
Size: 307 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 319 Color: 0
Size: 251 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 297 Color: 1
Size: 313 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 3
Size: 340 Color: 2
Size: 261 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 258 Color: 1
Size: 256 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 294 Color: 3
Size: 283 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 0
Size: 342 Color: 0
Size: 290 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 4
Size: 280 Color: 3
Size: 264 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 258 Color: 3
Size: 257 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 2
Size: 316 Color: 3
Size: 280 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 310 Color: 2
Size: 280 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 4
Size: 268 Color: 3
Size: 258 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 2
Size: 316 Color: 3
Size: 267 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 0
Size: 259 Color: 4
Size: 254 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 3
Size: 312 Color: 0
Size: 289 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 319 Color: 3
Size: 259 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 2
Size: 359 Color: 0
Size: 282 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 3
Size: 342 Color: 3
Size: 298 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 4
Size: 370 Color: 3
Size: 260 Color: 3

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 0
Size: 306 Color: 2
Size: 300 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 281 Color: 2
Size: 264 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 335 Color: 1
Size: 274 Color: 4

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 2
Size: 256 Color: 4
Size: 254 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 306 Color: 4
Size: 285 Color: 3

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 4
Size: 285 Color: 4
Size: 280 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 264 Color: 2
Size: 257 Color: 2

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 2
Size: 295 Color: 1
Size: 258 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 354 Color: 0
Size: 266 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 298 Color: 1
Size: 294 Color: 2

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 4
Size: 261 Color: 4
Size: 253 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 323 Color: 2
Size: 306 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 350 Color: 4
Size: 253 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 341 Color: 0
Size: 284 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 274 Color: 0
Size: 261 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 323 Color: 3
Size: 251 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 326 Color: 2
Size: 268 Color: 4

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 363 Color: 2
Size: 256 Color: 2

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 277 Color: 3
Size: 257 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 308 Color: 2
Size: 298 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 266 Color: 1
Size: 263 Color: 4

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 285 Color: 4
Size: 267 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 4
Size: 265 Color: 1
Size: 264 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 2
Size: 253 Color: 0
Size: 252 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 4
Size: 252 Color: 4
Size: 250 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 351 Color: 4
Size: 257 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 3
Size: 274 Color: 3
Size: 260 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 351 Color: 0
Size: 283 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 335 Color: 1
Size: 294 Color: 3

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 3
Size: 296 Color: 3
Size: 292 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 2
Size: 356 Color: 3
Size: 251 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 328 Color: 0
Size: 289 Color: 3

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 364 Color: 0
Size: 261 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 4
Size: 285 Color: 2
Size: 255 Color: 3

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 3
Size: 312 Color: 4
Size: 264 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 340 Color: 4
Size: 263 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 3
Size: 288 Color: 0
Size: 256 Color: 4

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 318 Color: 0
Size: 316 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 2
Size: 309 Color: 2
Size: 264 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 312 Color: 1
Size: 263 Color: 2

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 318 Color: 0
Size: 257 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 4
Size: 283 Color: 0
Size: 252 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 0
Size: 251 Color: 2
Size: 250 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 2
Size: 289 Color: 0
Size: 283 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 2
Size: 276 Color: 1
Size: 270 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 317 Color: 4
Size: 255 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 4
Size: 283 Color: 4
Size: 277 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 3
Size: 281 Color: 3
Size: 265 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 330 Color: 2
Size: 288 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 302 Color: 3
Size: 262 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 318 Color: 4
Size: 254 Color: 3

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 331 Color: 3
Size: 274 Color: 3

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 268 Color: 0
Size: 253 Color: 4

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 282 Color: 0
Size: 274 Color: 4

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 320 Color: 3
Size: 299 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 333 Color: 2
Size: 290 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 370 Color: 0
Size: 253 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 3
Size: 313 Color: 4
Size: 275 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 353 Color: 4
Size: 267 Color: 4

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 278 Color: 3
Size: 267 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 0
Size: 273 Color: 3
Size: 254 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 4
Size: 342 Color: 2
Size: 293 Color: 2

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 295 Color: 3
Size: 263 Color: 2

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 332 Color: 3
Size: 250 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 2
Size: 302 Color: 1
Size: 266 Color: 2

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 343 Color: 1
Size: 286 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 325 Color: 1
Size: 291 Color: 4

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 2
Size: 258 Color: 1
Size: 256 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 4
Size: 326 Color: 2
Size: 273 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 2
Size: 301 Color: 1
Size: 270 Color: 2

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 1
Size: 331 Color: 0
Size: 326 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 330 Color: 2
Size: 275 Color: 4

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 350 Color: 4
Size: 279 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 255 Color: 2
Size: 254 Color: 4

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 4
Size: 341 Color: 1
Size: 258 Color: 4

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 324 Color: 4
Size: 291 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 265 Color: 3
Size: 252 Color: 3

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 337 Color: 3
Size: 268 Color: 2

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 264 Color: 1
Size: 255 Color: 3

Total size: 167000
Total free space: 0

