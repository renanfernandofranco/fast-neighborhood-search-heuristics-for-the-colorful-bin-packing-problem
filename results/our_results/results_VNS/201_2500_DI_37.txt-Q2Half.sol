Capicity Bin: 2068
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1738 Color: 1
Size: 212 Color: 1
Size: 78 Color: 0
Size: 40 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1612 Color: 1
Size: 380 Color: 1
Size: 76 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 770 Color: 1
Size: 60 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1759 Color: 1
Size: 259 Color: 1
Size: 50 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1662 Color: 1
Size: 296 Color: 1
Size: 68 Color: 0
Size: 42 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 1
Size: 522 Color: 1
Size: 100 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1789 Color: 1
Size: 203 Color: 1
Size: 76 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 1
Size: 401 Color: 1
Size: 78 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 1
Size: 428 Color: 1
Size: 50 Color: 0

Bin 10: 0 of cap free
Amount of items: 5
Items: 
Size: 857 Color: 1
Size: 805 Color: 1
Size: 310 Color: 1
Size: 56 Color: 0
Size: 40 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 1
Size: 146 Color: 0
Size: 64 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 1
Size: 302 Color: 1
Size: 76 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 1
Size: 227 Color: 1
Size: 110 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 1
Size: 387 Color: 1
Size: 76 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 1
Size: 242 Color: 1
Size: 120 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 1
Size: 861 Color: 1
Size: 170 Color: 0

Bin 17: 0 of cap free
Amount of items: 5
Items: 
Size: 1189 Color: 1
Size: 480 Color: 1
Size: 245 Color: 1
Size: 106 Color: 0
Size: 48 Color: 0

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 1486 Color: 1
Size: 486 Color: 1
Size: 56 Color: 0
Size: 40 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1597 Color: 1
Size: 345 Color: 1
Size: 126 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 1
Size: 232 Color: 0
Size: 189 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1049 Color: 1
Size: 851 Color: 1
Size: 168 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1816 Color: 1
Size: 190 Color: 1
Size: 62 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 1
Size: 281 Color: 1
Size: 232 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 1
Size: 694 Color: 1
Size: 60 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 1
Size: 213 Color: 1
Size: 88 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 1
Size: 853 Color: 1
Size: 170 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 1
Size: 182 Color: 1
Size: 170 Color: 0

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 1806 Color: 1
Size: 152 Color: 1
Size: 66 Color: 0
Size: 44 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 1
Size: 278 Color: 1
Size: 168 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 1
Size: 233 Color: 1
Size: 60 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1035 Color: 1
Size: 861 Color: 1
Size: 172 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 1
Size: 386 Color: 1
Size: 76 Color: 0

Bin 33: 0 of cap free
Amount of items: 7
Items: 
Size: 635 Color: 1
Size: 578 Color: 1
Size: 429 Color: 1
Size: 318 Color: 1
Size: 48 Color: 0
Size: 44 Color: 0
Size: 16 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 1
Size: 283 Color: 1
Size: 56 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1300 Color: 1
Size: 696 Color: 1
Size: 72 Color: 0

Bin 36: 0 of cap free
Amount of items: 5
Items: 
Size: 1041 Color: 1
Size: 521 Color: 1
Size: 434 Color: 1
Size: 36 Color: 0
Size: 36 Color: 0

Bin 37: 0 of cap free
Amount of items: 5
Items: 
Size: 1421 Color: 1
Size: 311 Color: 1
Size: 222 Color: 1
Size: 68 Color: 0
Size: 46 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 1
Size: 691 Color: 1
Size: 136 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 1
Size: 298 Color: 1
Size: 56 Color: 0

Bin 40: 0 of cap free
Amount of items: 5
Items: 
Size: 1146 Color: 1
Size: 451 Color: 1
Size: 351 Color: 1
Size: 84 Color: 0
Size: 36 Color: 0

Bin 41: 0 of cap free
Amount of items: 5
Items: 
Size: 1529 Color: 1
Size: 207 Color: 1
Size: 198 Color: 1
Size: 70 Color: 0
Size: 64 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 1
Size: 511 Color: 1
Size: 100 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 1
Size: 317 Color: 1
Size: 52 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 1
Size: 342 Color: 1
Size: 36 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 1
Size: 393 Color: 1
Size: 124 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 1
Size: 862 Color: 1
Size: 8 Color: 0

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 1
Size: 374 Color: 1
Size: 36 Color: 0

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 1
Size: 630 Color: 1
Size: 96 Color: 0

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 1428 Color: 1
Size: 553 Color: 1
Size: 84 Color: 0

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 1
Size: 320 Color: 1
Size: 48 Color: 0

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 1307 Color: 1
Size: 733 Color: 1
Size: 24 Color: 0

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 1
Size: 183 Color: 1
Size: 60 Color: 0

Bin 53: 7 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 1
Size: 251 Color: 1
Size: 32 Color: 0

Bin 54: 7 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 1
Size: 402 Color: 1
Size: 136 Color: 0

Bin 55: 10 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 1
Size: 541 Color: 1
Size: 112 Color: 0

Bin 56: 25 of cap free
Amount of items: 3
Items: 
Size: 1851 Color: 1
Size: 178 Color: 1
Size: 14 Color: 0

Bin 57: 32 of cap free
Amount of items: 3
Items: 
Size: 1038 Color: 1
Size: 768 Color: 0
Size: 230 Color: 1

Bin 58: 41 of cap free
Amount of items: 3
Items: 
Size: 1378 Color: 1
Size: 609 Color: 1
Size: 40 Color: 0

Bin 59: 218 of cap free
Amount of items: 1
Items: 
Size: 1850 Color: 1

Bin 60: 219 of cap free
Amount of items: 1
Items: 
Size: 1849 Color: 1

Bin 61: 225 of cap free
Amount of items: 1
Items: 
Size: 1843 Color: 1

Bin 62: 226 of cap free
Amount of items: 1
Items: 
Size: 1842 Color: 1

Bin 63: 234 of cap free
Amount of items: 1
Items: 
Size: 1834 Color: 1

Bin 64: 255 of cap free
Amount of items: 1
Items: 
Size: 1813 Color: 1

Bin 65: 271 of cap free
Amount of items: 1
Items: 
Size: 1797 Color: 1

Bin 66: 274 of cap free
Amount of items: 1
Items: 
Size: 1794 Color: 1

Total size: 134420
Total free space: 2068

