Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 237
Size: 264 Color: 51
Size: 250 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 245
Size: 254 Color: 18
Size: 253 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 169
Size: 321 Color: 137
Size: 313 Color: 133

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 210
Size: 299 Color: 118
Size: 270 Color: 67

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 233
Size: 269 Color: 58
Size: 254 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 158
Size: 348 Color: 157
Size: 298 Color: 116

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 194
Size: 347 Color: 156
Size: 253 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 240
Size: 260 Color: 41
Size: 251 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 241
Size: 258 Color: 37
Size: 253 Color: 16

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 248
Size: 251 Color: 7
Size: 250 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 234
Size: 265 Color: 54
Size: 253 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 204
Size: 298 Color: 117
Size: 282 Color: 92

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 160
Size: 355 Color: 159
Size: 289 Color: 107

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 178
Size: 359 Color: 162
Size: 263 Color: 47

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 185
Size: 327 Color: 143
Size: 282 Color: 91

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 187
Size: 357 Color: 161
Size: 250 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 239
Size: 257 Color: 32
Size: 255 Color: 23

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 183
Size: 334 Color: 147
Size: 279 Color: 87

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 173
Size: 367 Color: 171
Size: 264 Color: 48

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 209
Size: 311 Color: 131
Size: 259 Color: 38

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 179
Size: 337 Color: 150
Size: 283 Color: 97

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 213
Size: 289 Color: 105
Size: 270 Color: 65

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 220
Size: 285 Color: 99
Size: 264 Color: 50

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 201
Size: 317 Color: 135
Size: 265 Color: 53

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 216
Size: 281 Color: 90
Size: 276 Color: 78

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 193
Size: 341 Color: 154
Size: 261 Color: 44

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 226
Size: 289 Color: 106
Size: 250 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 221
Size: 277 Color: 82
Size: 272 Color: 69

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 212
Size: 282 Color: 94
Size: 280 Color: 88

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 224
Size: 285 Color: 101
Size: 257 Color: 30

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 198
Size: 312 Color: 132
Size: 276 Color: 80

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 195
Size: 329 Color: 144
Size: 266 Color: 56

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 227
Size: 270 Color: 60
Size: 267 Color: 57

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 192
Size: 330 Color: 145
Size: 273 Color: 74

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 243
Size: 258 Color: 35
Size: 251 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 175
Size: 368 Color: 172
Size: 259 Color: 40

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 155
Size: 335 Color: 148
Size: 318 Color: 136

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 164
Size: 361 Color: 163
Size: 277 Color: 83

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 181
Size: 333 Color: 146
Size: 282 Color: 93

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 190
Size: 309 Color: 129
Size: 295 Color: 113

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 184
Size: 339 Color: 152
Size: 270 Color: 61

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 180
Size: 336 Color: 149
Size: 283 Color: 96

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 218
Size: 278 Color: 86
Size: 278 Color: 84

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 200
Size: 308 Color: 127
Size: 278 Color: 85

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 203
Size: 324 Color: 141
Size: 256 Color: 28

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 182
Size: 311 Color: 130
Size: 304 Color: 124

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 207
Size: 290 Color: 108
Size: 285 Color: 100

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 202
Size: 324 Color: 140
Size: 257 Color: 31

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 223
Size: 272 Color: 71
Size: 270 Color: 66

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 206
Size: 304 Color: 123
Size: 272 Color: 70

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 235
Size: 260 Color: 42
Size: 258 Color: 36

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 176
Size: 371 Color: 174
Size: 256 Color: 27

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 228
Size: 282 Color: 95
Size: 252 Color: 12

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 170
Size: 339 Color: 151
Size: 294 Color: 111

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 197
Size: 321 Color: 138
Size: 272 Color: 68

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 208
Size: 303 Color: 122
Size: 270 Color: 63

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 191
Size: 309 Color: 128
Size: 295 Color: 112

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 231
Size: 272 Color: 73
Size: 254 Color: 20

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 214
Size: 286 Color: 103
Size: 272 Color: 72

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 219
Size: 276 Color: 81
Size: 276 Color: 79

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 211
Size: 296 Color: 115
Size: 269 Color: 59

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 186
Size: 305 Color: 125
Size: 303 Color: 120

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 225
Size: 284 Color: 98
Size: 257 Color: 34

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 232
Size: 264 Color: 49
Size: 260 Color: 43

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 189
Size: 313 Color: 134
Size: 292 Color: 109

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 199
Size: 323 Color: 139
Size: 263 Color: 45

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 222
Size: 295 Color: 114
Size: 251 Color: 11

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 217
Size: 281 Color: 89
Size: 275 Color: 76

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 215
Size: 288 Color: 104
Size: 270 Color: 64

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 230
Size: 270 Color: 62
Size: 257 Color: 33

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 238
Size: 259 Color: 39
Size: 254 Color: 21

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 236
Size: 265 Color: 52
Size: 251 Color: 9

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 242
Size: 256 Color: 29
Size: 255 Color: 24

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 188
Size: 305 Color: 126
Size: 302 Color: 119

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 196
Size: 303 Color: 121
Size: 292 Color: 110

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 205
Size: 325 Color: 142
Size: 251 Color: 10

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 168
Size: 273 Color: 75
Size: 363 Color: 166

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 229
Size: 265 Color: 55
Size: 263 Color: 46

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 247
Size: 255 Color: 25
Size: 250 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 177
Size: 340 Color: 153
Size: 286 Color: 102

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 246
Size: 255 Color: 22
Size: 251 Color: 6

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 167
Size: 362 Color: 165
Size: 275 Color: 77

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 244
Size: 256 Color: 26
Size: 252 Color: 13

Total size: 83000
Total free space: 0

