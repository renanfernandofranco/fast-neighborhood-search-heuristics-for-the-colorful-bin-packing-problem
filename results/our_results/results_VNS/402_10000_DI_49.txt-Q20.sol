Capicity Bin: 7904
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4484 Color: 4
Size: 1528 Color: 3
Size: 1324 Color: 16
Size: 424 Color: 10
Size: 144 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5308 Color: 8
Size: 2444 Color: 17
Size: 152 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5412 Color: 16
Size: 2084 Color: 12
Size: 408 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5472 Color: 0
Size: 2132 Color: 4
Size: 300 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 10
Size: 2044 Color: 5
Size: 384 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5746 Color: 15
Size: 1824 Color: 3
Size: 334 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5922 Color: 17
Size: 1802 Color: 6
Size: 180 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5964 Color: 2
Size: 1612 Color: 5
Size: 328 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6088 Color: 3
Size: 1632 Color: 19
Size: 184 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 19
Size: 1322 Color: 2
Size: 260 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6332 Color: 6
Size: 1316 Color: 7
Size: 256 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6468 Color: 6
Size: 1216 Color: 3
Size: 220 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6525 Color: 16
Size: 827 Color: 14
Size: 552 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6616 Color: 2
Size: 880 Color: 1
Size: 408 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 9
Size: 684 Color: 14
Size: 600 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 14
Size: 1098 Color: 3
Size: 164 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 13
Size: 780 Color: 6
Size: 416 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6712 Color: 11
Size: 852 Color: 1
Size: 340 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 16
Size: 1026 Color: 18
Size: 160 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 3
Size: 714 Color: 12
Size: 418 Color: 10

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 6
Size: 880 Color: 0
Size: 298 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 8
Size: 1004 Color: 19
Size: 152 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 12
Size: 1000 Color: 7
Size: 84 Color: 8

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6886 Color: 10
Size: 834 Color: 17
Size: 184 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 13
Size: 778 Color: 19
Size: 228 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6936 Color: 16
Size: 776 Color: 4
Size: 192 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 7
Size: 804 Color: 19
Size: 128 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 3
Size: 767 Color: 15
Size: 152 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 12
Size: 482 Color: 13
Size: 404 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7050 Color: 8
Size: 668 Color: 13
Size: 186 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 3
Size: 564 Color: 0
Size: 232 Color: 14

Bin 32: 1 of cap free
Amount of items: 8
Items: 
Size: 3954 Color: 15
Size: 765 Color: 14
Size: 656 Color: 16
Size: 656 Color: 12
Size: 656 Color: 2
Size: 496 Color: 10
Size: 488 Color: 13
Size: 232 Color: 6

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 4479 Color: 2
Size: 3284 Color: 13
Size: 140 Color: 15

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 14
Size: 2395 Color: 6
Size: 176 Color: 3

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 5889 Color: 0
Size: 1654 Color: 2
Size: 360 Color: 17

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6094 Color: 4
Size: 1151 Color: 19
Size: 658 Color: 4

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 15
Size: 1043 Color: 0
Size: 288 Color: 14

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6583 Color: 5
Size: 1080 Color: 3
Size: 240 Color: 12

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6723 Color: 17
Size: 1180 Color: 12

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 6875 Color: 7
Size: 1028 Color: 14

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6913 Color: 16
Size: 990 Color: 8

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6939 Color: 10
Size: 964 Color: 8

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 3958 Color: 16
Size: 3288 Color: 17
Size: 656 Color: 3

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 5000 Color: 17
Size: 2742 Color: 8
Size: 160 Color: 5

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 5011 Color: 6
Size: 2411 Color: 14
Size: 480 Color: 3

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 5290 Color: 4
Size: 2132 Color: 10
Size: 480 Color: 4

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 5387 Color: 6
Size: 2099 Color: 8
Size: 416 Color: 13

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 5444 Color: 8
Size: 2458 Color: 9

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 1
Size: 2136 Color: 5
Size: 160 Color: 11

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 6248 Color: 19
Size: 1510 Color: 3
Size: 144 Color: 1

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 1
Size: 1294 Color: 4
Size: 148 Color: 16

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 4
Size: 1176 Color: 9
Size: 264 Color: 5

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6720 Color: 11
Size: 1182 Color: 1

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 6834 Color: 11
Size: 828 Color: 2
Size: 240 Color: 3

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 6938 Color: 18
Size: 762 Color: 19
Size: 202 Color: 3

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6994 Color: 11
Size: 908 Color: 18

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 4980 Color: 3
Size: 2761 Color: 16
Size: 160 Color: 0

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 5380 Color: 14
Size: 2385 Color: 5
Size: 136 Color: 6

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 5407 Color: 8
Size: 2494 Color: 18

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 6907 Color: 5
Size: 994 Color: 17

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 10
Size: 985 Color: 11

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6987 Color: 6
Size: 914 Color: 15

Bin 63: 4 of cap free
Amount of items: 4
Items: 
Size: 3957 Color: 9
Size: 2164 Color: 3
Size: 1501 Color: 3
Size: 278 Color: 19

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 4500 Color: 17
Size: 3200 Color: 12
Size: 200 Color: 12

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 4924 Color: 2
Size: 2656 Color: 11
Size: 320 Color: 9

Bin 66: 4 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 0
Size: 2424 Color: 15
Size: 544 Color: 17

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 6166 Color: 19
Size: 1734 Color: 15

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 9
Size: 842 Color: 5
Size: 742 Color: 16

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 6948 Color: 16
Size: 952 Color: 17

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 5043 Color: 14
Size: 2856 Color: 10

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 6341 Color: 17
Size: 1450 Color: 10
Size: 108 Color: 2

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 6053 Color: 3
Size: 1845 Color: 18

Bin 73: 7 of cap free
Amount of items: 3
Items: 
Size: 5640 Color: 1
Size: 2193 Color: 6
Size: 64 Color: 2

Bin 74: 7 of cap free
Amount of items: 3
Items: 
Size: 6233 Color: 15
Size: 859 Color: 17
Size: 805 Color: 15

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 6504 Color: 6
Size: 1393 Color: 13

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 6781 Color: 16
Size: 1116 Color: 6

Bin 77: 7 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 11
Size: 891 Color: 18
Size: 38 Color: 5

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 5748 Color: 12
Size: 2148 Color: 14

Bin 79: 9 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 17
Size: 2038 Color: 3
Size: 937 Color: 16

Bin 80: 9 of cap free
Amount of items: 3
Items: 
Size: 5691 Color: 15
Size: 1812 Color: 3
Size: 392 Color: 6

Bin 81: 9 of cap free
Amount of items: 2
Items: 
Size: 6691 Color: 18
Size: 1204 Color: 0

Bin 82: 9 of cap free
Amount of items: 2
Items: 
Size: 6884 Color: 9
Size: 1011 Color: 5

Bin 83: 9 of cap free
Amount of items: 2
Items: 
Size: 6986 Color: 17
Size: 909 Color: 11

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 5842 Color: 17
Size: 2052 Color: 10

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 11
Size: 1620 Color: 9

Bin 86: 10 of cap free
Amount of items: 3
Items: 
Size: 6984 Color: 14
Size: 894 Color: 9
Size: 16 Color: 11

Bin 87: 10 of cap free
Amount of items: 2
Items: 
Size: 7086 Color: 7
Size: 808 Color: 10

Bin 88: 11 of cap free
Amount of items: 2
Items: 
Size: 6590 Color: 8
Size: 1303 Color: 17

Bin 89: 12 of cap free
Amount of items: 2
Items: 
Size: 5462 Color: 13
Size: 2430 Color: 4

Bin 90: 12 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 3
Size: 1202 Color: 12
Size: 336 Color: 5

Bin 91: 13 of cap free
Amount of items: 10
Items: 
Size: 3953 Color: 16
Size: 488 Color: 7
Size: 488 Color: 4
Size: 480 Color: 10
Size: 478 Color: 0
Size: 476 Color: 18
Size: 476 Color: 0
Size: 468 Color: 6
Size: 328 Color: 5
Size: 256 Color: 1

Bin 92: 13 of cap free
Amount of items: 2
Items: 
Size: 6431 Color: 8
Size: 1460 Color: 16

Bin 93: 13 of cap free
Amount of items: 2
Items: 
Size: 6790 Color: 2
Size: 1101 Color: 11

Bin 94: 13 of cap free
Amount of items: 2
Items: 
Size: 6837 Color: 10
Size: 1054 Color: 14

Bin 95: 14 of cap free
Amount of items: 7
Items: 
Size: 3956 Color: 19
Size: 880 Color: 7
Size: 850 Color: 4
Size: 792 Color: 10
Size: 656 Color: 0
Size: 560 Color: 5
Size: 196 Color: 1

Bin 96: 14 of cap free
Amount of items: 3
Items: 
Size: 5334 Color: 7
Size: 2468 Color: 19
Size: 88 Color: 17

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 6618 Color: 1
Size: 1272 Color: 18

Bin 98: 14 of cap free
Amount of items: 3
Items: 
Size: 6776 Color: 4
Size: 1074 Color: 5
Size: 40 Color: 3

Bin 99: 14 of cap free
Amount of items: 2
Items: 
Size: 7084 Color: 0
Size: 806 Color: 7

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 6880 Color: 6
Size: 1008 Color: 7

Bin 101: 18 of cap free
Amount of items: 3
Items: 
Size: 5732 Color: 4
Size: 1722 Color: 3
Size: 432 Color: 5

Bin 102: 18 of cap free
Amount of items: 2
Items: 
Size: 6810 Color: 1
Size: 1076 Color: 12

Bin 103: 19 of cap free
Amount of items: 3
Items: 
Size: 4488 Color: 8
Size: 3181 Color: 11
Size: 216 Color: 1

Bin 104: 20 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 1
Size: 1384 Color: 18

Bin 105: 21 of cap free
Amount of items: 2
Items: 
Size: 5031 Color: 1
Size: 2852 Color: 17

Bin 106: 22 of cap free
Amount of items: 3
Items: 
Size: 4591 Color: 13
Size: 3003 Color: 3
Size: 288 Color: 11

Bin 107: 22 of cap free
Amount of items: 2
Items: 
Size: 6653 Color: 12
Size: 1229 Color: 19

Bin 108: 24 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 3
Size: 2792 Color: 15
Size: 140 Color: 5

Bin 109: 24 of cap free
Amount of items: 2
Items: 
Size: 6676 Color: 19
Size: 1204 Color: 10

Bin 110: 30 of cap free
Amount of items: 3
Items: 
Size: 6105 Color: 4
Size: 1681 Color: 15
Size: 88 Color: 1

Bin 111: 31 of cap free
Amount of items: 2
Items: 
Size: 6164 Color: 5
Size: 1709 Color: 4

Bin 112: 34 of cap free
Amount of items: 3
Items: 
Size: 6906 Color: 18
Size: 948 Color: 2
Size: 16 Color: 17

Bin 113: 36 of cap free
Amount of items: 2
Items: 
Size: 5972 Color: 19
Size: 1896 Color: 7

Bin 114: 38 of cap free
Amount of items: 2
Items: 
Size: 6062 Color: 19
Size: 1804 Color: 7

Bin 115: 47 of cap free
Amount of items: 3
Items: 
Size: 4942 Color: 10
Size: 2081 Color: 4
Size: 834 Color: 16

Bin 116: 49 of cap free
Amount of items: 2
Items: 
Size: 5673 Color: 6
Size: 2182 Color: 9

Bin 117: 52 of cap free
Amount of items: 2
Items: 
Size: 6490 Color: 12
Size: 1362 Color: 14

Bin 118: 53 of cap free
Amount of items: 2
Items: 
Size: 4769 Color: 5
Size: 3082 Color: 7

Bin 119: 54 of cap free
Amount of items: 2
Items: 
Size: 5880 Color: 0
Size: 1970 Color: 17

Bin 120: 60 of cap free
Amount of items: 2
Items: 
Size: 6156 Color: 14
Size: 1688 Color: 6

Bin 121: 60 of cap free
Amount of items: 2
Items: 
Size: 6392 Color: 13
Size: 1452 Color: 9

Bin 122: 64 of cap free
Amount of items: 30
Items: 
Size: 432 Color: 4
Size: 424 Color: 2
Size: 368 Color: 15
Size: 356 Color: 11
Size: 352 Color: 18
Size: 332 Color: 17
Size: 320 Color: 7
Size: 288 Color: 9
Size: 288 Color: 7
Size: 288 Color: 2
Size: 272 Color: 0
Size: 268 Color: 5
Size: 260 Color: 18
Size: 256 Color: 1
Size: 244 Color: 15
Size: 244 Color: 8
Size: 240 Color: 10
Size: 224 Color: 12
Size: 224 Color: 11
Size: 216 Color: 4
Size: 212 Color: 1
Size: 208 Color: 18
Size: 208 Color: 7
Size: 208 Color: 6
Size: 208 Color: 0
Size: 192 Color: 14
Size: 192 Color: 13
Size: 176 Color: 8
Size: 176 Color: 5
Size: 164 Color: 13

Bin 123: 68 of cap free
Amount of items: 2
Items: 
Size: 5352 Color: 16
Size: 2484 Color: 1

Bin 124: 71 of cap free
Amount of items: 3
Items: 
Size: 4618 Color: 15
Size: 2655 Color: 2
Size: 560 Color: 13

Bin 125: 72 of cap free
Amount of items: 2
Items: 
Size: 5348 Color: 14
Size: 2484 Color: 0

Bin 126: 82 of cap free
Amount of items: 3
Items: 
Size: 3964 Color: 6
Size: 3290 Color: 0
Size: 568 Color: 15

Bin 127: 84 of cap free
Amount of items: 2
Items: 
Size: 4976 Color: 19
Size: 2844 Color: 10

Bin 128: 104 of cap free
Amount of items: 2
Items: 
Size: 4958 Color: 3
Size: 2842 Color: 8

Bin 129: 108 of cap free
Amount of items: 4
Items: 
Size: 3960 Color: 13
Size: 3292 Color: 8
Size: 368 Color: 1
Size: 176 Color: 16

Bin 130: 112 of cap free
Amount of items: 2
Items: 
Size: 4498 Color: 5
Size: 3294 Color: 9

Bin 131: 121 of cap free
Amount of items: 2
Items: 
Size: 4490 Color: 19
Size: 3293 Color: 4

Bin 132: 132 of cap free
Amount of items: 2
Items: 
Size: 4301 Color: 5
Size: 3471 Color: 18

Bin 133: 5752 of cap free
Amount of items: 13
Items: 
Size: 196 Color: 10
Size: 192 Color: 19
Size: 176 Color: 14
Size: 176 Color: 6
Size: 170 Color: 5
Size: 168 Color: 17
Size: 168 Color: 16
Size: 164 Color: 14
Size: 160 Color: 8
Size: 152 Color: 2
Size: 144 Color: 13
Size: 144 Color: 1
Size: 142 Color: 16

Total size: 1043328
Total free space: 7904

