Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 372978 Color: 1
Size: 339284 Color: 0
Size: 287739 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 374478 Color: 0
Size: 322692 Color: 4
Size: 302831 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 396900 Color: 2
Size: 345081 Color: 1
Size: 258020 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 413651 Color: 0
Size: 294773 Color: 4
Size: 291577 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 436447 Color: 2
Size: 283847 Color: 3
Size: 279707 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 350974 Color: 2
Size: 336545 Color: 3
Size: 312482 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 384277 Color: 4
Size: 364285 Color: 4
Size: 251439 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 470032 Color: 1
Size: 265305 Color: 2
Size: 264664 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 365040 Color: 3
Size: 337647 Color: 3
Size: 297314 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 371548 Color: 3
Size: 345435 Color: 0
Size: 283018 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 358812 Color: 4
Size: 358729 Color: 1
Size: 282460 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 377410 Color: 3
Size: 364210 Color: 0
Size: 258381 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 348054 Color: 4
Size: 341293 Color: 2
Size: 310654 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 352979 Color: 1
Size: 349709 Color: 3
Size: 297313 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 354995 Color: 0
Size: 338516 Color: 0
Size: 306490 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 338568 Color: 2
Size: 336835 Color: 1
Size: 324598 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 393513 Color: 2
Size: 344595 Color: 4
Size: 261893 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 354644 Color: 3
Size: 347682 Color: 4
Size: 297675 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 370067 Color: 0
Size: 336236 Color: 1
Size: 293698 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 370813 Color: 4
Size: 363322 Color: 0
Size: 265866 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 371950 Color: 1
Size: 323643 Color: 0
Size: 304408 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 372546 Color: 3
Size: 333696 Color: 1
Size: 293759 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 372840 Color: 3
Size: 330535 Color: 0
Size: 296626 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 375209 Color: 1
Size: 331315 Color: 3
Size: 293477 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 376010 Color: 0
Size: 321324 Color: 2
Size: 302667 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 376367 Color: 1
Size: 363999 Color: 1
Size: 259635 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 377204 Color: 3
Size: 336146 Color: 3
Size: 286651 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 377815 Color: 4
Size: 311590 Color: 1
Size: 310596 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 378146 Color: 2
Size: 363555 Color: 3
Size: 258300 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 379221 Color: 2
Size: 358645 Color: 4
Size: 262135 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 380063 Color: 0
Size: 319433 Color: 3
Size: 300505 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 380238 Color: 0
Size: 338551 Color: 3
Size: 281212 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 379657 Color: 1
Size: 352579 Color: 2
Size: 267765 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 380671 Color: 3
Size: 330833 Color: 1
Size: 288497 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 382130 Color: 0
Size: 348415 Color: 2
Size: 269456 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 381014 Color: 1
Size: 333530 Color: 1
Size: 285457 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 381955 Color: 4
Size: 349126 Color: 3
Size: 268920 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 382305 Color: 4
Size: 316611 Color: 3
Size: 301085 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 383298 Color: 0
Size: 346515 Color: 2
Size: 270188 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 382487 Color: 4
Size: 364542 Color: 3
Size: 252972 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 383000 Color: 1
Size: 324605 Color: 2
Size: 292396 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 383071 Color: 2
Size: 361028 Color: 1
Size: 255902 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 383603 Color: 4
Size: 352851 Color: 1
Size: 263547 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 384292 Color: 1
Size: 338127 Color: 0
Size: 277582 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 384300 Color: 1
Size: 361955 Color: 2
Size: 253746 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 384325 Color: 1
Size: 359248 Color: 4
Size: 256428 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 385402 Color: 0
Size: 322065 Color: 1
Size: 292534 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 385506 Color: 2
Size: 329633 Color: 1
Size: 284862 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 385869 Color: 1
Size: 333594 Color: 2
Size: 280538 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 386637 Color: 4
Size: 341438 Color: 1
Size: 271926 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 387572 Color: 2
Size: 323937 Color: 2
Size: 288492 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 388286 Color: 0
Size: 329176 Color: 2
Size: 282539 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 389403 Color: 0
Size: 331679 Color: 4
Size: 278919 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 389380 Color: 1
Size: 317922 Color: 3
Size: 292699 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 389476 Color: 0
Size: 349723 Color: 3
Size: 260802 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 389566 Color: 4
Size: 318409 Color: 1
Size: 292026 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 390027 Color: 0
Size: 325112 Color: 2
Size: 284862 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 390454 Color: 0
Size: 327670 Color: 4
Size: 281877 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 390128 Color: 4
Size: 328899 Color: 1
Size: 280974 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 390211 Color: 2
Size: 315550 Color: 4
Size: 294240 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 391269 Color: 0
Size: 307424 Color: 1
Size: 301308 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 391340 Color: 0
Size: 342298 Color: 2
Size: 266363 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 391241 Color: 4
Size: 316841 Color: 2
Size: 291919 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 391363 Color: 4
Size: 348864 Color: 0
Size: 259774 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 392632 Color: 3
Size: 321230 Color: 4
Size: 286139 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 393190 Color: 1
Size: 322810 Color: 0
Size: 284001 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 393479 Color: 2
Size: 304819 Color: 4
Size: 301703 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 394401 Color: 3
Size: 329594 Color: 0
Size: 276006 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 394689 Color: 2
Size: 341628 Color: 4
Size: 263684 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 395558 Color: 0
Size: 334887 Color: 3
Size: 269556 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 395217 Color: 3
Size: 327368 Color: 1
Size: 277416 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 395466 Color: 3
Size: 325912 Color: 4
Size: 278623 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 395624 Color: 4
Size: 321500 Color: 4
Size: 282877 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 395676 Color: 3
Size: 331879 Color: 4
Size: 272446 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 396012 Color: 4
Size: 308037 Color: 3
Size: 295952 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 396318 Color: 0
Size: 318345 Color: 4
Size: 285338 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 396409 Color: 4
Size: 343375 Color: 2
Size: 260217 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 396946 Color: 0
Size: 325721 Color: 3
Size: 277334 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 397297 Color: 0
Size: 330641 Color: 3
Size: 272063 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 398944 Color: 1
Size: 339531 Color: 4
Size: 261526 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 399355 Color: 1
Size: 329648 Color: 2
Size: 270998 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 400369 Color: 2
Size: 338579 Color: 2
Size: 261053 Color: 3

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 400434 Color: 3
Size: 339572 Color: 0
Size: 259995 Color: 2

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 400708 Color: 2
Size: 342553 Color: 4
Size: 256740 Color: 3

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 401194 Color: 0
Size: 320668 Color: 1
Size: 278139 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 401499 Color: 0
Size: 321163 Color: 2
Size: 277339 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 401818 Color: 1
Size: 330077 Color: 1
Size: 268106 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 402672 Color: 0
Size: 337725 Color: 4
Size: 259604 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 403092 Color: 4
Size: 303961 Color: 0
Size: 292948 Color: 4

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 403838 Color: 4
Size: 298997 Color: 3
Size: 297166 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 403852 Color: 1
Size: 305367 Color: 0
Size: 290782 Color: 4

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 404455 Color: 2
Size: 324277 Color: 1
Size: 271269 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 404672 Color: 3
Size: 334630 Color: 0
Size: 260699 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 405342 Color: 0
Size: 310190 Color: 4
Size: 284469 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 404791 Color: 4
Size: 344674 Color: 3
Size: 250536 Color: 2

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 405393 Color: 0
Size: 343244 Color: 3
Size: 251364 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 405532 Color: 4
Size: 332929 Color: 0
Size: 261540 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 405596 Color: 4
Size: 315629 Color: 3
Size: 278776 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 406531 Color: 0
Size: 343281 Color: 2
Size: 250189 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 406069 Color: 4
Size: 316876 Color: 4
Size: 277056 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 406828 Color: 4
Size: 299830 Color: 0
Size: 293343 Color: 2

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 409715 Color: 1
Size: 326906 Color: 4
Size: 263380 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 409727 Color: 2
Size: 320983 Color: 0
Size: 269291 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 410008 Color: 3
Size: 317776 Color: 0
Size: 272217 Color: 3

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 410134 Color: 4
Size: 336548 Color: 2
Size: 253319 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 410347 Color: 1
Size: 300288 Color: 0
Size: 289366 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 410654 Color: 3
Size: 309366 Color: 1
Size: 279981 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 411034 Color: 1
Size: 304259 Color: 0
Size: 284708 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 411036 Color: 4
Size: 302172 Color: 0
Size: 286793 Color: 3

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 411676 Color: 4
Size: 310606 Color: 2
Size: 277719 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 411900 Color: 4
Size: 331303 Color: 2
Size: 256798 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 412872 Color: 0
Size: 313555 Color: 2
Size: 273574 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 413103 Color: 3
Size: 328130 Color: 3
Size: 258768 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 413419 Color: 0
Size: 330431 Color: 1
Size: 256151 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 413578 Color: 2
Size: 306736 Color: 4
Size: 279687 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 413693 Color: 1
Size: 297389 Color: 0
Size: 288919 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 413848 Color: 1
Size: 331101 Color: 2
Size: 255052 Color: 3

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 414274 Color: 0
Size: 305748 Color: 1
Size: 279979 Color: 4

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 415057 Color: 4
Size: 298768 Color: 3
Size: 286176 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 415503 Color: 1
Size: 321625 Color: 0
Size: 262873 Color: 2

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 415630 Color: 4
Size: 313110 Color: 3
Size: 271261 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 415640 Color: 1
Size: 309214 Color: 3
Size: 275147 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 415816 Color: 2
Size: 333240 Color: 0
Size: 250945 Color: 4

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 415906 Color: 3
Size: 330283 Color: 0
Size: 253812 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 416030 Color: 3
Size: 300687 Color: 2
Size: 283284 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 416173 Color: 2
Size: 318067 Color: 0
Size: 265761 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 416534 Color: 0
Size: 305624 Color: 1
Size: 277843 Color: 3

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 416192 Color: 2
Size: 322197 Color: 2
Size: 261612 Color: 4

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 416252 Color: 2
Size: 327657 Color: 1
Size: 256092 Color: 4

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 417520 Color: 0
Size: 329316 Color: 2
Size: 253165 Color: 2

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 416871 Color: 2
Size: 304194 Color: 4
Size: 278936 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 417674 Color: 0
Size: 300086 Color: 1
Size: 282241 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 417340 Color: 4
Size: 299105 Color: 3
Size: 283556 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 418168 Color: 0
Size: 292934 Color: 3
Size: 288899 Color: 4

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 417940 Color: 3
Size: 330757 Color: 0
Size: 251304 Color: 4

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 419140 Color: 0
Size: 323938 Color: 4
Size: 256923 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 419188 Color: 3
Size: 304549 Color: 0
Size: 276264 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 419673 Color: 2
Size: 306193 Color: 1
Size: 274135 Color: 4

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 419725 Color: 1
Size: 292232 Color: 2
Size: 288044 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 420972 Color: 3
Size: 300570 Color: 3
Size: 278459 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 421713 Color: 3
Size: 298340 Color: 0
Size: 279948 Color: 4

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 422348 Color: 4
Size: 291163 Color: 0
Size: 286490 Color: 2

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 422953 Color: 2
Size: 322444 Color: 4
Size: 254604 Color: 4

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 423455 Color: 3
Size: 313990 Color: 0
Size: 262556 Color: 3

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 423508 Color: 3
Size: 302757 Color: 2
Size: 273736 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 425144 Color: 1
Size: 324627 Color: 3
Size: 250230 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 425413 Color: 4
Size: 322682 Color: 2
Size: 251906 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 426027 Color: 3
Size: 321331 Color: 2
Size: 252643 Color: 3

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 426185 Color: 4
Size: 307753 Color: 0
Size: 266063 Color: 3

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 426906 Color: 0
Size: 296547 Color: 2
Size: 276548 Color: 4

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 426488 Color: 2
Size: 298407 Color: 4
Size: 275106 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 427429 Color: 2
Size: 287501 Color: 0
Size: 285071 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 428972 Color: 0
Size: 310709 Color: 1
Size: 260320 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 428757 Color: 1
Size: 288204 Color: 3
Size: 283040 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 430273 Color: 1
Size: 301889 Color: 1
Size: 267839 Color: 4

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 430434 Color: 4
Size: 306485 Color: 0
Size: 263082 Color: 2

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 430707 Color: 2
Size: 315070 Color: 2
Size: 254224 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 431597 Color: 0
Size: 290328 Color: 4
Size: 278076 Color: 4

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 431637 Color: 4
Size: 284447 Color: 0
Size: 283917 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 431881 Color: 1
Size: 295514 Color: 0
Size: 272606 Color: 3

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 431895 Color: 3
Size: 291833 Color: 1
Size: 276273 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 432803 Color: 0
Size: 286734 Color: 4
Size: 280464 Color: 4

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 432760 Color: 2
Size: 288771 Color: 1
Size: 278470 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 433483 Color: 3
Size: 294308 Color: 1
Size: 272210 Color: 2

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 433552 Color: 1
Size: 284034 Color: 2
Size: 282415 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 433702 Color: 1
Size: 307954 Color: 0
Size: 258345 Color: 3

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 433908 Color: 1
Size: 313784 Color: 2
Size: 252309 Color: 3

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 433958 Color: 4
Size: 309087 Color: 1
Size: 256956 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 434101 Color: 3
Size: 302080 Color: 4
Size: 263820 Color: 2

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 434397 Color: 3
Size: 287851 Color: 1
Size: 277753 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 434512 Color: 0
Size: 286290 Color: 3
Size: 279199 Color: 3

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 434961 Color: 4
Size: 313792 Color: 1
Size: 251248 Color: 1

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 435108 Color: 2
Size: 300548 Color: 3
Size: 264345 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 435244 Color: 1
Size: 288584 Color: 4
Size: 276173 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 435249 Color: 4
Size: 295165 Color: 3
Size: 269587 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 435926 Color: 4
Size: 285482 Color: 3
Size: 278593 Color: 2

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 436230 Color: 3
Size: 287898 Color: 0
Size: 275873 Color: 3

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 436330 Color: 1
Size: 302176 Color: 0
Size: 261495 Color: 3

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 437021 Color: 0
Size: 311291 Color: 2
Size: 251689 Color: 4

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 437558 Color: 1
Size: 286730 Color: 1
Size: 275713 Color: 2

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 437943 Color: 3
Size: 282739 Color: 3
Size: 279319 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 438007 Color: 2
Size: 290127 Color: 4
Size: 271867 Color: 2

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 438017 Color: 3
Size: 286688 Color: 1
Size: 275296 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 438478 Color: 3
Size: 306390 Color: 2
Size: 255133 Color: 4

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 439914 Color: 2
Size: 282798 Color: 4
Size: 277289 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 440218 Color: 1
Size: 309323 Color: 2
Size: 250460 Color: 4

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 440700 Color: 3
Size: 309124 Color: 1
Size: 250177 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 441227 Color: 0
Size: 286554 Color: 4
Size: 272220 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 441137 Color: 2
Size: 299126 Color: 1
Size: 259738 Color: 2

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 441561 Color: 2
Size: 284918 Color: 2
Size: 273522 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 441658 Color: 4
Size: 292844 Color: 0
Size: 265499 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 442298 Color: 1
Size: 287418 Color: 0
Size: 270285 Color: 3

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 442886 Color: 3
Size: 302775 Color: 0
Size: 254340 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 443006 Color: 2
Size: 303087 Color: 2
Size: 253908 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 443210 Color: 0
Size: 296223 Color: 1
Size: 260568 Color: 3

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 443554 Color: 4
Size: 283066 Color: 2
Size: 273381 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 443648 Color: 2
Size: 288542 Color: 1
Size: 267811 Color: 2

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 443883 Color: 2
Size: 291783 Color: 1
Size: 264335 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 443919 Color: 4
Size: 296397 Color: 2
Size: 259685 Color: 4

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 444580 Color: 0
Size: 289879 Color: 4
Size: 265542 Color: 2

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 444894 Color: 0
Size: 300779 Color: 4
Size: 254328 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 444330 Color: 3
Size: 291144 Color: 4
Size: 264527 Color: 3

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 445230 Color: 0
Size: 300937 Color: 4
Size: 253834 Color: 2

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 444861 Color: 2
Size: 281103 Color: 4
Size: 274037 Color: 3

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 445772 Color: 0
Size: 299956 Color: 4
Size: 254273 Color: 2

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 446686 Color: 4
Size: 299740 Color: 2
Size: 253575 Color: 3

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 446698 Color: 1
Size: 286549 Color: 0
Size: 266754 Color: 3

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 447468 Color: 1
Size: 280777 Color: 0
Size: 271756 Color: 2

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 448067 Color: 3
Size: 299038 Color: 1
Size: 252896 Color: 3

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 448282 Color: 4
Size: 279031 Color: 0
Size: 272688 Color: 3

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 448928 Color: 0
Size: 286471 Color: 2
Size: 264602 Color: 2

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 449490 Color: 2
Size: 293805 Color: 3
Size: 256706 Color: 2

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 450024 Color: 0
Size: 298809 Color: 1
Size: 251168 Color: 4

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 450039 Color: 0
Size: 281377 Color: 2
Size: 268585 Color: 4

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 449741 Color: 3
Size: 283533 Color: 4
Size: 266727 Color: 3

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 450357 Color: 0
Size: 276908 Color: 2
Size: 272736 Color: 2

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 450045 Color: 2
Size: 294193 Color: 3
Size: 255763 Color: 2

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 450058 Color: 2
Size: 290869 Color: 3
Size: 259074 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 450571 Color: 0
Size: 299087 Color: 4
Size: 250343 Color: 3

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 450654 Color: 4
Size: 287016 Color: 3
Size: 262331 Color: 2

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 451071 Color: 1
Size: 291415 Color: 2
Size: 257515 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 451432 Color: 3
Size: 280812 Color: 0
Size: 267757 Color: 1

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 451707 Color: 0
Size: 277370 Color: 4
Size: 270924 Color: 2

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 451550 Color: 4
Size: 285941 Color: 2
Size: 262510 Color: 3

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 452034 Color: 1
Size: 275745 Color: 0
Size: 272222 Color: 2

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 452416 Color: 3
Size: 285735 Color: 0
Size: 261850 Color: 3

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 453175 Color: 4
Size: 289083 Color: 2
Size: 257743 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 454180 Color: 0
Size: 276749 Color: 2
Size: 269072 Color: 3

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 454125 Color: 3
Size: 287976 Color: 3
Size: 257900 Color: 4

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 455147 Color: 4
Size: 278355 Color: 2
Size: 266499 Color: 3

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 455545 Color: 3
Size: 281703 Color: 0
Size: 262753 Color: 3

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 456042 Color: 2
Size: 291924 Color: 2
Size: 252035 Color: 3

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 456196 Color: 3
Size: 274231 Color: 0
Size: 269574 Color: 3

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 456927 Color: 4
Size: 285150 Color: 4
Size: 257924 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 457774 Color: 1
Size: 288851 Color: 3
Size: 253376 Color: 2

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 459555 Color: 0
Size: 277305 Color: 2
Size: 263141 Color: 2

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 459506 Color: 4
Size: 272107 Color: 0
Size: 268388 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 459675 Color: 0
Size: 282258 Color: 1
Size: 258068 Color: 2

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 459849 Color: 2
Size: 272404 Color: 1
Size: 267748 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 460024 Color: 1
Size: 281539 Color: 0
Size: 258438 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 460411 Color: 2
Size: 287273 Color: 1
Size: 252317 Color: 3

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 461361 Color: 0
Size: 272204 Color: 4
Size: 266436 Color: 2

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 461369 Color: 0
Size: 279211 Color: 2
Size: 259421 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 460650 Color: 1
Size: 289330 Color: 3
Size: 250021 Color: 3

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 461533 Color: 2
Size: 277589 Color: 1
Size: 260879 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 462191 Color: 4
Size: 285621 Color: 4
Size: 252189 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 462348 Color: 1
Size: 278073 Color: 3
Size: 259580 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 463587 Color: 3
Size: 285612 Color: 4
Size: 250802 Color: 3

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 464030 Color: 2
Size: 282654 Color: 1
Size: 253317 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 464190 Color: 2
Size: 269078 Color: 1
Size: 266733 Color: 2

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 464195 Color: 4
Size: 282668 Color: 0
Size: 253138 Color: 2

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 464268 Color: 3
Size: 279747 Color: 0
Size: 255986 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 464745 Color: 3
Size: 271743 Color: 4
Size: 263513 Color: 4

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 465636 Color: 4
Size: 272593 Color: 2
Size: 261772 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 467192 Color: 3
Size: 281681 Color: 2
Size: 251128 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 467291 Color: 4
Size: 269653 Color: 1
Size: 263057 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 468327 Color: 1
Size: 273557 Color: 4
Size: 258117 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 468550 Color: 4
Size: 269947 Color: 3
Size: 261504 Color: 2

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 468603 Color: 3
Size: 275581 Color: 0
Size: 255817 Color: 3

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 468701 Color: 2
Size: 273899 Color: 0
Size: 257401 Color: 2

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 468801 Color: 4
Size: 268572 Color: 4
Size: 262628 Color: 2

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 469088 Color: 0
Size: 269048 Color: 4
Size: 261865 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 469832 Color: 0
Size: 278938 Color: 4
Size: 251231 Color: 2

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 469547 Color: 2
Size: 279344 Color: 4
Size: 251110 Color: 3

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 469984 Color: 0
Size: 267096 Color: 3
Size: 262921 Color: 4

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 470473 Color: 1
Size: 277684 Color: 3
Size: 251844 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 471135 Color: 3
Size: 273409 Color: 4
Size: 255457 Color: 3

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 472500 Color: 3
Size: 271859 Color: 0
Size: 255642 Color: 4

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 472597 Color: 4
Size: 269454 Color: 2
Size: 257950 Color: 3

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 472721 Color: 2
Size: 271479 Color: 0
Size: 255801 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 472727 Color: 4
Size: 264382 Color: 0
Size: 262892 Color: 2

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 472827 Color: 2
Size: 269958 Color: 0
Size: 257216 Color: 2

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 473798 Color: 0
Size: 269647 Color: 2
Size: 256556 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 473190 Color: 3
Size: 273015 Color: 4
Size: 253796 Color: 3

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 473829 Color: 4
Size: 266947 Color: 3
Size: 259225 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 474396 Color: 0
Size: 273056 Color: 4
Size: 252549 Color: 2

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 475791 Color: 2
Size: 272640 Color: 4
Size: 251570 Color: 3

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 475984 Color: 3
Size: 273302 Color: 0
Size: 250715 Color: 2

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 476005 Color: 4
Size: 271069 Color: 1
Size: 252927 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 476408 Color: 0
Size: 269705 Color: 1
Size: 253888 Color: 4

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 476243 Color: 1
Size: 269055 Color: 4
Size: 254703 Color: 3

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 476626 Color: 0
Size: 272903 Color: 4
Size: 250472 Color: 2

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 476474 Color: 4
Size: 264191 Color: 2
Size: 259336 Color: 3

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 476541 Color: 2
Size: 269559 Color: 1
Size: 253901 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 476994 Color: 0
Size: 263749 Color: 1
Size: 259258 Color: 4

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 476817 Color: 2
Size: 264752 Color: 3
Size: 258432 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 476935 Color: 1
Size: 272083 Color: 0
Size: 250983 Color: 4

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 477528 Color: 4
Size: 270236 Color: 4
Size: 252237 Color: 2

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 478274 Color: 3
Size: 265389 Color: 0
Size: 256338 Color: 2

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 479032 Color: 0
Size: 261888 Color: 3
Size: 259081 Color: 3

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 479146 Color: 4
Size: 262077 Color: 2
Size: 258778 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 479360 Color: 3
Size: 269833 Color: 1
Size: 250808 Color: 2

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 481015 Color: 0
Size: 262789 Color: 2
Size: 256197 Color: 2

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 481229 Color: 3
Size: 265306 Color: 0
Size: 253466 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 482006 Color: 3
Size: 266670 Color: 2
Size: 251325 Color: 4

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 482610 Color: 1
Size: 263501 Color: 0
Size: 253890 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 482997 Color: 3
Size: 267003 Color: 0
Size: 250001 Color: 4

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 483199 Color: 3
Size: 265891 Color: 4
Size: 250911 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 484107 Color: 1
Size: 263480 Color: 3
Size: 252414 Color: 2

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 484269 Color: 3
Size: 263034 Color: 0
Size: 252698 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 484468 Color: 2
Size: 262330 Color: 0
Size: 253203 Color: 4

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 484802 Color: 1
Size: 263092 Color: 3
Size: 252107 Color: 2

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 485946 Color: 4
Size: 257518 Color: 4
Size: 256537 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 486169 Color: 3
Size: 262320 Color: 4
Size: 251512 Color: 3

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 486409 Color: 3
Size: 262903 Color: 2
Size: 250689 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 487018 Color: 2
Size: 258389 Color: 2
Size: 254594 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 487348 Color: 2
Size: 257627 Color: 2
Size: 255026 Color: 1

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 488987 Color: 0
Size: 259031 Color: 1
Size: 251983 Color: 3

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 489041 Color: 0
Size: 257436 Color: 1
Size: 253524 Color: 4

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 488795 Color: 3
Size: 255728 Color: 4
Size: 255478 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 489141 Color: 4
Size: 259578 Color: 3
Size: 251282 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 489324 Color: 4
Size: 256472 Color: 3
Size: 254205 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 489475 Color: 2
Size: 256940 Color: 4
Size: 253586 Color: 2

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 490056 Color: 3
Size: 258843 Color: 3
Size: 251102 Color: 1

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 490903 Color: 0
Size: 256379 Color: 2
Size: 252719 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 490989 Color: 0
Size: 256017 Color: 1
Size: 252995 Color: 3

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 491017 Color: 0
Size: 257438 Color: 4
Size: 251546 Color: 4

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 490795 Color: 2
Size: 256534 Color: 3
Size: 252672 Color: 2

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 491098 Color: 1
Size: 258375 Color: 4
Size: 250528 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 491580 Color: 2
Size: 256131 Color: 1
Size: 252290 Color: 3

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 492430 Color: 0
Size: 254275 Color: 1
Size: 253296 Color: 3

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 493640 Color: 2
Size: 254973 Color: 3
Size: 251388 Color: 4

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 494267 Color: 0
Size: 253207 Color: 4
Size: 252527 Color: 4

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 494533 Color: 1
Size: 252900 Color: 0
Size: 252568 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 494973 Color: 1
Size: 252938 Color: 3
Size: 252090 Color: 3

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 495719 Color: 1
Size: 254280 Color: 1
Size: 250002 Color: 3

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 495978 Color: 1
Size: 252182 Color: 0
Size: 251841 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 496684 Color: 1
Size: 251940 Color: 0
Size: 251377 Color: 2

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 496808 Color: 2
Size: 253133 Color: 0
Size: 250060 Color: 4

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 497410 Color: 2
Size: 252246 Color: 2
Size: 250345 Color: 3

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 497428 Color: 1
Size: 251782 Color: 0
Size: 250791 Color: 3

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 499839 Color: 1
Size: 250103 Color: 0
Size: 250059 Color: 4

Bin 333: 1 of cap free
Amount of items: 3
Items: 
Size: 364881 Color: 3
Size: 344088 Color: 4
Size: 291031 Color: 3

Bin 334: 1 of cap free
Amount of items: 3
Items: 
Size: 365600 Color: 1
Size: 345055 Color: 1
Size: 289345 Color: 4

Bin 335: 1 of cap free
Amount of items: 3
Items: 
Size: 376486 Color: 0
Size: 342089 Color: 4
Size: 281425 Color: 3

Bin 336: 1 of cap free
Amount of items: 3
Items: 
Size: 378017 Color: 3
Size: 317473 Color: 0
Size: 304510 Color: 4

Bin 337: 1 of cap free
Amount of items: 3
Items: 
Size: 379087 Color: 3
Size: 355214 Color: 3
Size: 265699 Color: 0

Bin 338: 1 of cap free
Amount of items: 3
Items: 
Size: 379338 Color: 1
Size: 338491 Color: 4
Size: 282171 Color: 1

Bin 339: 1 of cap free
Amount of items: 3
Items: 
Size: 381159 Color: 0
Size: 351727 Color: 3
Size: 267114 Color: 4

Bin 340: 1 of cap free
Amount of items: 3
Items: 
Size: 380714 Color: 1
Size: 346545 Color: 0
Size: 272741 Color: 2

Bin 341: 1 of cap free
Amount of items: 3
Items: 
Size: 381558 Color: 0
Size: 311387 Color: 1
Size: 307055 Color: 3

Bin 342: 1 of cap free
Amount of items: 3
Items: 
Size: 382393 Color: 4
Size: 313519 Color: 3
Size: 304088 Color: 0

Bin 343: 1 of cap free
Amount of items: 3
Items: 
Size: 383802 Color: 0
Size: 319300 Color: 4
Size: 296898 Color: 1

Bin 344: 1 of cap free
Amount of items: 3
Items: 
Size: 382715 Color: 1
Size: 312462 Color: 2
Size: 304823 Color: 4

Bin 345: 1 of cap free
Amount of items: 3
Items: 
Size: 383072 Color: 3
Size: 315291 Color: 3
Size: 301637 Color: 0

Bin 346: 1 of cap free
Amount of items: 3
Items: 
Size: 384121 Color: 0
Size: 327450 Color: 3
Size: 288429 Color: 3

Bin 347: 1 of cap free
Amount of items: 3
Items: 
Size: 384202 Color: 0
Size: 308487 Color: 3
Size: 307311 Color: 1

Bin 348: 1 of cap free
Amount of items: 3
Items: 
Size: 384735 Color: 2
Size: 334943 Color: 2
Size: 280322 Color: 0

Bin 349: 1 of cap free
Amount of items: 3
Items: 
Size: 385433 Color: 1
Size: 338635 Color: 0
Size: 275932 Color: 1

Bin 350: 1 of cap free
Amount of items: 3
Items: 
Size: 385965 Color: 4
Size: 347245 Color: 3
Size: 266790 Color: 4

Bin 351: 1 of cap free
Amount of items: 3
Items: 
Size: 386461 Color: 0
Size: 349185 Color: 4
Size: 264354 Color: 4

Bin 352: 1 of cap free
Amount of items: 3
Items: 
Size: 387277 Color: 2
Size: 333226 Color: 4
Size: 279497 Color: 4

Bin 353: 1 of cap free
Amount of items: 3
Items: 
Size: 387415 Color: 4
Size: 348058 Color: 4
Size: 264527 Color: 0

Bin 354: 1 of cap free
Amount of items: 3
Items: 
Size: 387778 Color: 0
Size: 308471 Color: 1
Size: 303751 Color: 4

Bin 355: 1 of cap free
Amount of items: 3
Items: 
Size: 388801 Color: 2
Size: 311608 Color: 0
Size: 299591 Color: 1

Bin 356: 1 of cap free
Amount of items: 3
Items: 
Size: 388909 Color: 1
Size: 321757 Color: 1
Size: 289334 Color: 2

Bin 357: 1 of cap free
Amount of items: 3
Items: 
Size: 389655 Color: 4
Size: 312191 Color: 4
Size: 298154 Color: 0

Bin 358: 1 of cap free
Amount of items: 3
Items: 
Size: 391818 Color: 3
Size: 333454 Color: 4
Size: 274728 Color: 2

Bin 359: 1 of cap free
Amount of items: 3
Items: 
Size: 392722 Color: 2
Size: 318803 Color: 0
Size: 288475 Color: 1

Bin 360: 1 of cap free
Amount of items: 3
Items: 
Size: 392945 Color: 2
Size: 317535 Color: 0
Size: 289520 Color: 4

Bin 361: 1 of cap free
Amount of items: 3
Items: 
Size: 392958 Color: 2
Size: 337742 Color: 3
Size: 269300 Color: 3

Bin 362: 1 of cap free
Amount of items: 3
Items: 
Size: 393812 Color: 0
Size: 354712 Color: 1
Size: 251476 Color: 4

Bin 363: 1 of cap free
Amount of items: 3
Items: 
Size: 393624 Color: 2
Size: 308081 Color: 1
Size: 298295 Color: 2

Bin 364: 1 of cap free
Amount of items: 3
Items: 
Size: 394884 Color: 1
Size: 316797 Color: 3
Size: 288319 Color: 2

Bin 365: 1 of cap free
Amount of items: 3
Items: 
Size: 396910 Color: 2
Size: 334488 Color: 1
Size: 268602 Color: 1

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 398195 Color: 2
Size: 311423 Color: 1
Size: 290382 Color: 3

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 398532 Color: 0
Size: 312455 Color: 4
Size: 289013 Color: 2

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 399183 Color: 3
Size: 328537 Color: 4
Size: 272280 Color: 0

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 399748 Color: 4
Size: 330549 Color: 2
Size: 269703 Color: 1

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 400487 Color: 1
Size: 308974 Color: 1
Size: 290539 Color: 0

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 403179 Color: 0
Size: 327470 Color: 1
Size: 269351 Color: 3

Bin 372: 1 of cap free
Amount of items: 3
Items: 
Size: 403874 Color: 1
Size: 318921 Color: 3
Size: 277205 Color: 0

Bin 373: 1 of cap free
Amount of items: 3
Items: 
Size: 405407 Color: 0
Size: 328688 Color: 2
Size: 265905 Color: 3

Bin 374: 1 of cap free
Amount of items: 3
Items: 
Size: 404890 Color: 2
Size: 309459 Color: 1
Size: 285651 Color: 3

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 405309 Color: 2
Size: 321683 Color: 0
Size: 273008 Color: 1

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 406269 Color: 3
Size: 313163 Color: 0
Size: 280568 Color: 3

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 407280 Color: 0
Size: 320818 Color: 1
Size: 271902 Color: 2

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 408409 Color: 3
Size: 301453 Color: 2
Size: 290138 Color: 0

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 410522 Color: 1
Size: 307870 Color: 0
Size: 281608 Color: 4

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 414033 Color: 2
Size: 311515 Color: 1
Size: 274452 Color: 0

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 415870 Color: 4
Size: 327466 Color: 1
Size: 256664 Color: 2

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 415775 Color: 0
Size: 321812 Color: 4
Size: 262413 Color: 1

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 416660 Color: 0
Size: 309312 Color: 2
Size: 274028 Color: 4

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 417589 Color: 4
Size: 314122 Color: 2
Size: 268289 Color: 0

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 422515 Color: 0
Size: 313833 Color: 1
Size: 263652 Color: 4

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 424728 Color: 0
Size: 323708 Color: 2
Size: 251564 Color: 4

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 424839 Color: 3
Size: 291575 Color: 1
Size: 283586 Color: 0

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 424872 Color: 4
Size: 311658 Color: 0
Size: 263470 Color: 2

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 426637 Color: 0
Size: 312232 Color: 3
Size: 261131 Color: 3

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 427242 Color: 1
Size: 297138 Color: 0
Size: 275620 Color: 4

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 427472 Color: 1
Size: 306544 Color: 3
Size: 265984 Color: 3

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 428806 Color: 2
Size: 288092 Color: 0
Size: 283102 Color: 3

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 430078 Color: 4
Size: 291984 Color: 0
Size: 277938 Color: 3

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 431413 Color: 3
Size: 284983 Color: 3
Size: 283604 Color: 0

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 431567 Color: 1
Size: 309106 Color: 1
Size: 259327 Color: 3

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 432326 Color: 2
Size: 284335 Color: 0
Size: 283339 Color: 4

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 434490 Color: 0
Size: 299907 Color: 4
Size: 265603 Color: 2

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 435175 Color: 4
Size: 284874 Color: 2
Size: 279951 Color: 0

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 435671 Color: 0
Size: 296786 Color: 4
Size: 267543 Color: 1

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 435266 Color: 1
Size: 290379 Color: 3
Size: 274355 Color: 2

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 436240 Color: 3
Size: 311358 Color: 1
Size: 252402 Color: 1

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 436969 Color: 1
Size: 307491 Color: 0
Size: 255540 Color: 2

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 437194 Color: 2
Size: 308998 Color: 3
Size: 253808 Color: 0

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 437793 Color: 2
Size: 281861 Color: 0
Size: 280346 Color: 2

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 438469 Color: 1
Size: 295648 Color: 2
Size: 265883 Color: 0

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 438896 Color: 1
Size: 287148 Color: 4
Size: 273956 Color: 0

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 442159 Color: 2
Size: 284618 Color: 1
Size: 273223 Color: 4

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 444745 Color: 4
Size: 291097 Color: 4
Size: 264158 Color: 0

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 445163 Color: 3
Size: 288396 Color: 2
Size: 266441 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 450918 Color: 3
Size: 283924 Color: 0
Size: 265158 Color: 4

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 450933 Color: 3
Size: 276369 Color: 0
Size: 272698 Color: 2

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 454696 Color: 2
Size: 292739 Color: 0
Size: 252565 Color: 1

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 455944 Color: 0
Size: 286907 Color: 4
Size: 257149 Color: 2

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 456240 Color: 0
Size: 285052 Color: 3
Size: 258708 Color: 3

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 456934 Color: 4
Size: 291935 Color: 4
Size: 251131 Color: 0

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 461273 Color: 0
Size: 276873 Color: 3
Size: 261854 Color: 2

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 460918 Color: 4
Size: 269875 Color: 0
Size: 269207 Color: 1

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 462134 Color: 4
Size: 273391 Color: 2
Size: 264475 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 468071 Color: 3
Size: 272580 Color: 0
Size: 259349 Color: 4

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 470050 Color: 4
Size: 277073 Color: 1
Size: 252877 Color: 2

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 474007 Color: 4
Size: 274651 Color: 3
Size: 251342 Color: 4

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 474644 Color: 3
Size: 268733 Color: 0
Size: 256623 Color: 2

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 475239 Color: 1
Size: 269816 Color: 0
Size: 254945 Color: 3

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 475955 Color: 3
Size: 265004 Color: 0
Size: 259041 Color: 1

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 476286 Color: 3
Size: 269996 Color: 1
Size: 253718 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 478360 Color: 4
Size: 268852 Color: 2
Size: 252788 Color: 2

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 479538 Color: 0
Size: 265051 Color: 4
Size: 255411 Color: 1

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 481148 Color: 0
Size: 267932 Color: 3
Size: 250920 Color: 3

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 481014 Color: 4
Size: 264939 Color: 4
Size: 254047 Color: 1

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 481393 Color: 0
Size: 263616 Color: 1
Size: 254991 Color: 1

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 482977 Color: 2
Size: 264541 Color: 4
Size: 252482 Color: 3

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 485451 Color: 4
Size: 264035 Color: 0
Size: 250514 Color: 3

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 495215 Color: 4
Size: 253049 Color: 1
Size: 251736 Color: 0

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 495532 Color: 4
Size: 252704 Color: 0
Size: 251764 Color: 3

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 497958 Color: 3
Size: 251093 Color: 0
Size: 250949 Color: 1

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 373481 Color: 4
Size: 327069 Color: 2
Size: 299450 Color: 3

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 351290 Color: 3
Size: 350366 Color: 1
Size: 298344 Color: 1

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 347573 Color: 3
Size: 346912 Color: 0
Size: 305515 Color: 2

Bin 439: 2 of cap free
Amount of items: 3
Items: 
Size: 418741 Color: 3
Size: 322691 Color: 2
Size: 258567 Color: 1

Bin 440: 2 of cap free
Amount of items: 3
Items: 
Size: 369412 Color: 0
Size: 329265 Color: 1
Size: 301322 Color: 1

Bin 441: 2 of cap free
Amount of items: 3
Items: 
Size: 373226 Color: 1
Size: 338932 Color: 4
Size: 287841 Color: 3

Bin 442: 2 of cap free
Amount of items: 3
Items: 
Size: 375679 Color: 3
Size: 349527 Color: 0
Size: 274793 Color: 2

Bin 443: 2 of cap free
Amount of items: 3
Items: 
Size: 376336 Color: 4
Size: 356623 Color: 3
Size: 267040 Color: 2

Bin 444: 2 of cap free
Amount of items: 3
Items: 
Size: 377015 Color: 0
Size: 368022 Color: 4
Size: 254962 Color: 1

Bin 445: 2 of cap free
Amount of items: 3
Items: 
Size: 378909 Color: 1
Size: 331823 Color: 4
Size: 289267 Color: 3

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 380074 Color: 3
Size: 317371 Color: 0
Size: 302554 Color: 3

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 380734 Color: 2
Size: 309748 Color: 1
Size: 309517 Color: 1

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 380855 Color: 1
Size: 361069 Color: 0
Size: 258075 Color: 1

Bin 449: 2 of cap free
Amount of items: 3
Items: 
Size: 382344 Color: 0
Size: 309509 Color: 1
Size: 308146 Color: 1

Bin 450: 2 of cap free
Amount of items: 3
Items: 
Size: 383956 Color: 0
Size: 351827 Color: 2
Size: 264216 Color: 2

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 383416 Color: 2
Size: 355548 Color: 1
Size: 261035 Color: 4

Bin 452: 2 of cap free
Amount of items: 3
Items: 
Size: 383635 Color: 1
Size: 352176 Color: 1
Size: 264188 Color: 3

Bin 453: 2 of cap free
Amount of items: 3
Items: 
Size: 384388 Color: 2
Size: 345661 Color: 0
Size: 269950 Color: 3

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 386458 Color: 2
Size: 349846 Color: 1
Size: 263695 Color: 1

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 387066 Color: 0
Size: 328517 Color: 3
Size: 284416 Color: 1

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 387782 Color: 1
Size: 313073 Color: 4
Size: 299144 Color: 0

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 389535 Color: 2
Size: 351834 Color: 0
Size: 258630 Color: 3

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 389729 Color: 1
Size: 324721 Color: 1
Size: 285549 Color: 2

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 396069 Color: 1
Size: 344068 Color: 3
Size: 259862 Color: 3

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 396186 Color: 1
Size: 349677 Color: 2
Size: 254136 Color: 0

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 396315 Color: 3
Size: 317484 Color: 2
Size: 286200 Color: 0

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 398561 Color: 3
Size: 309255 Color: 0
Size: 292183 Color: 1

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 406629 Color: 2
Size: 336335 Color: 2
Size: 257035 Color: 1

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 406953 Color: 3
Size: 336572 Color: 3
Size: 256474 Color: 2

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 408779 Color: 2
Size: 331055 Color: 2
Size: 260165 Color: 4

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 409303 Color: 4
Size: 340511 Color: 4
Size: 250185 Color: 0

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 412948 Color: 3
Size: 329768 Color: 1
Size: 257283 Color: 2

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 416249 Color: 4
Size: 324942 Color: 0
Size: 258808 Color: 4

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 417788 Color: 4
Size: 330701 Color: 4
Size: 251510 Color: 1

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 422766 Color: 3
Size: 308904 Color: 0
Size: 268329 Color: 2

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 427197 Color: 2
Size: 310833 Color: 0
Size: 261969 Color: 2

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 430942 Color: 1
Size: 285744 Color: 1
Size: 283313 Color: 3

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 432951 Color: 4
Size: 295134 Color: 2
Size: 271914 Color: 0

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 433064 Color: 3
Size: 284851 Color: 3
Size: 282084 Color: 0

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 435651 Color: 3
Size: 283553 Color: 1
Size: 280795 Color: 0

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 436035 Color: 1
Size: 283317 Color: 0
Size: 280647 Color: 2

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 443031 Color: 2
Size: 305956 Color: 4
Size: 251012 Color: 4

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 443648 Color: 1
Size: 297738 Color: 1
Size: 258613 Color: 0

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 449808 Color: 3
Size: 296868 Color: 0
Size: 253323 Color: 3

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 470426 Color: 3
Size: 268199 Color: 0
Size: 261374 Color: 2

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 472732 Color: 3
Size: 268885 Color: 2
Size: 258382 Color: 2

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 476002 Color: 3
Size: 273267 Color: 2
Size: 250730 Color: 1

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 477359 Color: 4
Size: 262103 Color: 0
Size: 260537 Color: 1

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 479625 Color: 3
Size: 265501 Color: 3
Size: 254873 Color: 0

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 491169 Color: 0
Size: 257585 Color: 3
Size: 251245 Color: 2

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 491647 Color: 1
Size: 254279 Color: 2
Size: 254073 Color: 0

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 495937 Color: 1
Size: 252942 Color: 0
Size: 251120 Color: 4

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 496370 Color: 3
Size: 253184 Color: 4
Size: 250445 Color: 2

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 498987 Color: 4
Size: 250944 Color: 0
Size: 250068 Color: 3

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 364580 Color: 1
Size: 328968 Color: 2
Size: 306451 Color: 0

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 359627 Color: 2
Size: 344895 Color: 3
Size: 295477 Color: 4

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 360025 Color: 0
Size: 349356 Color: 3
Size: 290618 Color: 0

Bin 493: 3 of cap free
Amount of items: 3
Items: 
Size: 463908 Color: 3
Size: 279706 Color: 3
Size: 256384 Color: 0

Bin 494: 3 of cap free
Amount of items: 3
Items: 
Size: 366609 Color: 0
Size: 316977 Color: 1
Size: 316412 Color: 4

Bin 495: 3 of cap free
Amount of items: 3
Items: 
Size: 367377 Color: 3
Size: 338028 Color: 2
Size: 294593 Color: 4

Bin 496: 3 of cap free
Amount of items: 3
Items: 
Size: 370838 Color: 3
Size: 345393 Color: 2
Size: 283767 Color: 1

Bin 497: 3 of cap free
Amount of items: 3
Items: 
Size: 371166 Color: 1
Size: 333995 Color: 3
Size: 294837 Color: 0

Bin 498: 3 of cap free
Amount of items: 3
Items: 
Size: 371238 Color: 1
Size: 360540 Color: 3
Size: 268220 Color: 1

Bin 499: 3 of cap free
Amount of items: 3
Items: 
Size: 374694 Color: 1
Size: 367175 Color: 4
Size: 258129 Color: 3

Bin 500: 3 of cap free
Amount of items: 3
Items: 
Size: 377361 Color: 3
Size: 315040 Color: 0
Size: 307597 Color: 2

Bin 501: 3 of cap free
Amount of items: 3
Items: 
Size: 378634 Color: 4
Size: 366416 Color: 0
Size: 254948 Color: 2

Bin 502: 3 of cap free
Amount of items: 3
Items: 
Size: 384893 Color: 2
Size: 347543 Color: 2
Size: 267562 Color: 1

Bin 503: 3 of cap free
Amount of items: 3
Items: 
Size: 385751 Color: 2
Size: 336042 Color: 2
Size: 278205 Color: 4

Bin 504: 3 of cap free
Amount of items: 3
Items: 
Size: 385878 Color: 3
Size: 308742 Color: 0
Size: 305378 Color: 2

Bin 505: 3 of cap free
Amount of items: 3
Items: 
Size: 390388 Color: 3
Size: 356379 Color: 4
Size: 253231 Color: 3

Bin 506: 3 of cap free
Amount of items: 3
Items: 
Size: 393019 Color: 4
Size: 317934 Color: 2
Size: 289045 Color: 0

Bin 507: 3 of cap free
Amount of items: 3
Items: 
Size: 394511 Color: 3
Size: 335315 Color: 4
Size: 270172 Color: 4

Bin 508: 3 of cap free
Amount of items: 3
Items: 
Size: 394594 Color: 1
Size: 303909 Color: 4
Size: 301495 Color: 0

Bin 509: 3 of cap free
Amount of items: 3
Items: 
Size: 396965 Color: 2
Size: 303216 Color: 0
Size: 299817 Color: 3

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 402686 Color: 3
Size: 310954 Color: 2
Size: 286358 Color: 1

Bin 511: 3 of cap free
Amount of items: 3
Items: 
Size: 405945 Color: 3
Size: 340344 Color: 3
Size: 253709 Color: 0

Bin 512: 3 of cap free
Amount of items: 3
Items: 
Size: 409334 Color: 2
Size: 301893 Color: 0
Size: 288771 Color: 1

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 416338 Color: 3
Size: 304559 Color: 0
Size: 279101 Color: 2

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 419779 Color: 2
Size: 315840 Color: 2
Size: 264379 Color: 0

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 428298 Color: 3
Size: 306124 Color: 4
Size: 265576 Color: 0

Bin 516: 3 of cap free
Amount of items: 3
Items: 
Size: 435859 Color: 0
Size: 309583 Color: 4
Size: 254556 Color: 2

Bin 517: 3 of cap free
Amount of items: 3
Items: 
Size: 454103 Color: 4
Size: 287817 Color: 4
Size: 258078 Color: 0

Bin 518: 3 of cap free
Amount of items: 3
Items: 
Size: 463520 Color: 3
Size: 276074 Color: 0
Size: 260404 Color: 3

Bin 519: 3 of cap free
Amount of items: 3
Items: 
Size: 471325 Color: 2
Size: 269478 Color: 0
Size: 259195 Color: 3

Bin 520: 3 of cap free
Amount of items: 3
Items: 
Size: 387180 Color: 1
Size: 333778 Color: 4
Size: 279040 Color: 3

Bin 521: 3 of cap free
Amount of items: 3
Items: 
Size: 354446 Color: 3
Size: 333587 Color: 4
Size: 311965 Color: 3

Bin 522: 4 of cap free
Amount of items: 3
Items: 
Size: 364731 Color: 0
Size: 349637 Color: 3
Size: 285629 Color: 2

Bin 523: 4 of cap free
Amount of items: 3
Items: 
Size: 366350 Color: 0
Size: 332344 Color: 4
Size: 301303 Color: 1

Bin 524: 4 of cap free
Amount of items: 3
Items: 
Size: 371633 Color: 4
Size: 370775 Color: 1
Size: 257589 Color: 4

Bin 525: 4 of cap free
Amount of items: 3
Items: 
Size: 372005 Color: 2
Size: 325637 Color: 0
Size: 302355 Color: 3

Bin 526: 4 of cap free
Amount of items: 3
Items: 
Size: 372545 Color: 1
Size: 337479 Color: 2
Size: 289973 Color: 0

Bin 527: 4 of cap free
Amount of items: 3
Items: 
Size: 375562 Color: 4
Size: 368026 Color: 2
Size: 256409 Color: 1

Bin 528: 4 of cap free
Amount of items: 3
Items: 
Size: 382776 Color: 0
Size: 326428 Color: 4
Size: 290793 Color: 3

Bin 529: 4 of cap free
Amount of items: 3
Items: 
Size: 385188 Color: 4
Size: 325878 Color: 4
Size: 288931 Color: 0

Bin 530: 4 of cap free
Amount of items: 3
Items: 
Size: 386418 Color: 4
Size: 353810 Color: 0
Size: 259769 Color: 4

Bin 531: 4 of cap free
Amount of items: 3
Items: 
Size: 394455 Color: 2
Size: 319954 Color: 0
Size: 285588 Color: 3

Bin 532: 4 of cap free
Amount of items: 3
Items: 
Size: 395172 Color: 2
Size: 320769 Color: 1
Size: 284056 Color: 0

Bin 533: 4 of cap free
Amount of items: 3
Items: 
Size: 402047 Color: 2
Size: 333447 Color: 2
Size: 264503 Color: 0

Bin 534: 4 of cap free
Amount of items: 3
Items: 
Size: 407252 Color: 0
Size: 327541 Color: 2
Size: 265204 Color: 2

Bin 535: 4 of cap free
Amount of items: 3
Items: 
Size: 410737 Color: 3
Size: 323120 Color: 0
Size: 266140 Color: 1

Bin 536: 4 of cap free
Amount of items: 3
Items: 
Size: 419343 Color: 2
Size: 291335 Color: 3
Size: 289319 Color: 0

Bin 537: 4 of cap free
Amount of items: 3
Items: 
Size: 454387 Color: 4
Size: 285145 Color: 2
Size: 260465 Color: 0

Bin 538: 4 of cap free
Amount of items: 3
Items: 
Size: 482670 Color: 3
Size: 261653 Color: 4
Size: 255674 Color: 0

Bin 539: 4 of cap free
Amount of items: 3
Items: 
Size: 352432 Color: 2
Size: 341913 Color: 2
Size: 305652 Color: 4

Bin 540: 5 of cap free
Amount of items: 3
Items: 
Size: 385230 Color: 3
Size: 327028 Color: 2
Size: 287738 Color: 1

Bin 541: 5 of cap free
Amount of items: 3
Items: 
Size: 360082 Color: 1
Size: 321078 Color: 4
Size: 318836 Color: 0

Bin 542: 5 of cap free
Amount of items: 3
Items: 
Size: 372717 Color: 4
Size: 361827 Color: 2
Size: 265452 Color: 4

Bin 543: 5 of cap free
Amount of items: 3
Items: 
Size: 374933 Color: 2
Size: 317418 Color: 0
Size: 307645 Color: 1

Bin 544: 5 of cap free
Amount of items: 3
Items: 
Size: 376202 Color: 1
Size: 322314 Color: 3
Size: 301480 Color: 0

Bin 545: 5 of cap free
Amount of items: 3
Items: 
Size: 376457 Color: 1
Size: 343954 Color: 3
Size: 279585 Color: 3

Bin 546: 5 of cap free
Amount of items: 3
Items: 
Size: 376489 Color: 4
Size: 324122 Color: 0
Size: 299385 Color: 4

Bin 547: 5 of cap free
Amount of items: 3
Items: 
Size: 386062 Color: 1
Size: 323948 Color: 4
Size: 289986 Color: 0

Bin 548: 5 of cap free
Amount of items: 3
Items: 
Size: 386266 Color: 3
Size: 345729 Color: 3
Size: 268001 Color: 0

Bin 549: 5 of cap free
Amount of items: 3
Items: 
Size: 388243 Color: 4
Size: 338633 Color: 4
Size: 273120 Color: 2

Bin 550: 5 of cap free
Amount of items: 3
Items: 
Size: 389136 Color: 2
Size: 325718 Color: 2
Size: 285142 Color: 0

Bin 551: 5 of cap free
Amount of items: 3
Items: 
Size: 392064 Color: 4
Size: 313745 Color: 0
Size: 294187 Color: 2

Bin 552: 5 of cap free
Amount of items: 3
Items: 
Size: 392131 Color: 1
Size: 353043 Color: 4
Size: 254822 Color: 0

Bin 553: 5 of cap free
Amount of items: 3
Items: 
Size: 395487 Color: 4
Size: 317311 Color: 0
Size: 287198 Color: 2

Bin 554: 5 of cap free
Amount of items: 3
Items: 
Size: 417165 Color: 2
Size: 315638 Color: 0
Size: 267193 Color: 4

Bin 555: 5 of cap free
Amount of items: 3
Items: 
Size: 357560 Color: 2
Size: 334914 Color: 4
Size: 307522 Color: 0

Bin 556: 6 of cap free
Amount of items: 3
Items: 
Size: 386478 Color: 4
Size: 354992 Color: 0
Size: 258525 Color: 1

Bin 557: 6 of cap free
Amount of items: 3
Items: 
Size: 356553 Color: 3
Size: 353410 Color: 4
Size: 290032 Color: 1

Bin 558: 6 of cap free
Amount of items: 3
Items: 
Size: 371307 Color: 4
Size: 326208 Color: 0
Size: 302480 Color: 2

Bin 559: 6 of cap free
Amount of items: 3
Items: 
Size: 377874 Color: 1
Size: 320679 Color: 2
Size: 301442 Color: 2

Bin 560: 6 of cap free
Amount of items: 3
Items: 
Size: 377791 Color: 0
Size: 370590 Color: 3
Size: 251614 Color: 2

Bin 561: 6 of cap free
Amount of items: 3
Items: 
Size: 381942 Color: 2
Size: 331572 Color: 0
Size: 286481 Color: 4

Bin 562: 6 of cap free
Amount of items: 3
Items: 
Size: 400085 Color: 1
Size: 303280 Color: 0
Size: 296630 Color: 4

Bin 563: 7 of cap free
Amount of items: 3
Items: 
Size: 373771 Color: 1
Size: 326849 Color: 2
Size: 299374 Color: 1

Bin 564: 7 of cap free
Amount of items: 3
Items: 
Size: 374135 Color: 2
Size: 316150 Color: 4
Size: 309709 Color: 4

Bin 565: 7 of cap free
Amount of items: 3
Items: 
Size: 379279 Color: 4
Size: 325543 Color: 1
Size: 295172 Color: 3

Bin 566: 7 of cap free
Amount of items: 3
Items: 
Size: 388760 Color: 1
Size: 330844 Color: 2
Size: 280390 Color: 0

Bin 567: 7 of cap free
Amount of items: 3
Items: 
Size: 390099 Color: 2
Size: 357779 Color: 0
Size: 252116 Color: 2

Bin 568: 7 of cap free
Amount of items: 3
Items: 
Size: 407990 Color: 1
Size: 296229 Color: 3
Size: 295775 Color: 0

Bin 569: 7 of cap free
Amount of items: 3
Items: 
Size: 371192 Color: 3
Size: 319571 Color: 0
Size: 309231 Color: 3

Bin 570: 8 of cap free
Amount of items: 3
Items: 
Size: 368709 Color: 4
Size: 364575 Color: 1
Size: 266709 Color: 2

Bin 571: 8 of cap free
Amount of items: 3
Items: 
Size: 371555 Color: 1
Size: 346890 Color: 4
Size: 281548 Color: 0

Bin 572: 8 of cap free
Amount of items: 3
Items: 
Size: 372024 Color: 1
Size: 360338 Color: 2
Size: 267631 Color: 1

Bin 573: 8 of cap free
Amount of items: 3
Items: 
Size: 372177 Color: 3
Size: 359305 Color: 0
Size: 268511 Color: 3

Bin 574: 8 of cap free
Amount of items: 3
Items: 
Size: 400255 Color: 2
Size: 346007 Color: 1
Size: 253731 Color: 0

Bin 575: 9 of cap free
Amount of items: 3
Items: 
Size: 373478 Color: 1
Size: 372663 Color: 0
Size: 253851 Color: 4

Bin 576: 9 of cap free
Amount of items: 3
Items: 
Size: 360703 Color: 1
Size: 327964 Color: 1
Size: 311325 Color: 0

Bin 577: 9 of cap free
Amount of items: 3
Items: 
Size: 369827 Color: 1
Size: 336207 Color: 4
Size: 293958 Color: 4

Bin 578: 9 of cap free
Amount of items: 3
Items: 
Size: 370653 Color: 4
Size: 335888 Color: 2
Size: 293451 Color: 4

Bin 579: 9 of cap free
Amount of items: 3
Items: 
Size: 372579 Color: 3
Size: 316317 Color: 3
Size: 311096 Color: 0

Bin 580: 9 of cap free
Amount of items: 3
Items: 
Size: 374215 Color: 2
Size: 330153 Color: 0
Size: 295624 Color: 4

Bin 581: 9 of cap free
Amount of items: 3
Items: 
Size: 378895 Color: 2
Size: 346419 Color: 1
Size: 274678 Color: 0

Bin 582: 9 of cap free
Amount of items: 3
Items: 
Size: 380646 Color: 0
Size: 315838 Color: 2
Size: 303508 Color: 2

Bin 583: 9 of cap free
Amount of items: 3
Items: 
Size: 382490 Color: 3
Size: 351316 Color: 0
Size: 266186 Color: 4

Bin 584: 9 of cap free
Amount of items: 3
Items: 
Size: 353529 Color: 0
Size: 343654 Color: 4
Size: 302809 Color: 1

Bin 585: 10 of cap free
Amount of items: 3
Items: 
Size: 368683 Color: 0
Size: 322616 Color: 1
Size: 308692 Color: 1

Bin 586: 10 of cap free
Amount of items: 3
Items: 
Size: 382027 Color: 2
Size: 335961 Color: 0
Size: 282003 Color: 3

Bin 587: 10 of cap free
Amount of items: 3
Items: 
Size: 357617 Color: 3
Size: 341224 Color: 2
Size: 301150 Color: 4

Bin 588: 11 of cap free
Amount of items: 3
Items: 
Size: 369997 Color: 3
Size: 326739 Color: 4
Size: 303254 Color: 0

Bin 589: 11 of cap free
Amount of items: 3
Items: 
Size: 373880 Color: 2
Size: 347723 Color: 3
Size: 278387 Color: 0

Bin 590: 11 of cap free
Amount of items: 3
Items: 
Size: 376995 Color: 1
Size: 325180 Color: 0
Size: 297815 Color: 2

Bin 591: 12 of cap free
Amount of items: 3
Items: 
Size: 373315 Color: 2
Size: 318577 Color: 0
Size: 308097 Color: 4

Bin 592: 12 of cap free
Amount of items: 3
Items: 
Size: 373770 Color: 3
Size: 322101 Color: 0
Size: 304118 Color: 2

Bin 593: 12 of cap free
Amount of items: 3
Items: 
Size: 384225 Color: 3
Size: 365450 Color: 0
Size: 250314 Color: 2

Bin 594: 12 of cap free
Amount of items: 3
Items: 
Size: 360059 Color: 1
Size: 330225 Color: 4
Size: 309705 Color: 1

Bin 595: 13 of cap free
Amount of items: 3
Items: 
Size: 356860 Color: 0
Size: 344044 Color: 2
Size: 299084 Color: 4

Bin 596: 13 of cap free
Amount of items: 3
Items: 
Size: 363001 Color: 2
Size: 342230 Color: 3
Size: 294757 Color: 0

Bin 597: 13 of cap free
Amount of items: 3
Items: 
Size: 366247 Color: 1
Size: 339642 Color: 2
Size: 294099 Color: 0

Bin 598: 13 of cap free
Amount of items: 3
Items: 
Size: 375360 Color: 0
Size: 369572 Color: 4
Size: 255056 Color: 1

Bin 599: 13 of cap free
Amount of items: 3
Items: 
Size: 359698 Color: 2
Size: 333568 Color: 0
Size: 306722 Color: 1

Bin 600: 14 of cap free
Amount of items: 3
Items: 
Size: 359623 Color: 3
Size: 325656 Color: 0
Size: 314708 Color: 1

Bin 601: 14 of cap free
Amount of items: 3
Items: 
Size: 366346 Color: 2
Size: 335062 Color: 0
Size: 298579 Color: 4

Bin 602: 14 of cap free
Amount of items: 3
Items: 
Size: 369509 Color: 0
Size: 348190 Color: 4
Size: 282288 Color: 2

Bin 603: 14 of cap free
Amount of items: 3
Items: 
Size: 370705 Color: 1
Size: 323362 Color: 0
Size: 305920 Color: 4

Bin 604: 15 of cap free
Amount of items: 3
Items: 
Size: 356766 Color: 0
Size: 329982 Color: 4
Size: 313238 Color: 3

Bin 605: 15 of cap free
Amount of items: 3
Items: 
Size: 363068 Color: 0
Size: 321501 Color: 4
Size: 315417 Color: 1

Bin 606: 15 of cap free
Amount of items: 3
Items: 
Size: 362509 Color: 3
Size: 359645 Color: 2
Size: 277832 Color: 1

Bin 607: 15 of cap free
Amount of items: 3
Items: 
Size: 390978 Color: 3
Size: 307375 Color: 0
Size: 301633 Color: 2

Bin 608: 16 of cap free
Amount of items: 3
Items: 
Size: 372389 Color: 0
Size: 361726 Color: 1
Size: 265870 Color: 3

Bin 609: 16 of cap free
Amount of items: 3
Items: 
Size: 360792 Color: 3
Size: 351967 Color: 3
Size: 287226 Color: 4

Bin 610: 17 of cap free
Amount of items: 3
Items: 
Size: 361605 Color: 2
Size: 341705 Color: 1
Size: 296674 Color: 0

Bin 611: 18 of cap free
Amount of items: 3
Items: 
Size: 366981 Color: 1
Size: 329694 Color: 4
Size: 303308 Color: 1

Bin 612: 18 of cap free
Amount of items: 3
Items: 
Size: 391689 Color: 2
Size: 311287 Color: 0
Size: 297007 Color: 2

Bin 613: 18 of cap free
Amount of items: 3
Items: 
Size: 351061 Color: 2
Size: 333759 Color: 0
Size: 315163 Color: 2

Bin 614: 19 of cap free
Amount of items: 3
Items: 
Size: 373308 Color: 2
Size: 355338 Color: 3
Size: 271336 Color: 1

Bin 615: 19 of cap free
Amount of items: 3
Items: 
Size: 363433 Color: 1
Size: 325232 Color: 4
Size: 311317 Color: 1

Bin 616: 19 of cap free
Amount of items: 3
Items: 
Size: 346210 Color: 3
Size: 338552 Color: 2
Size: 315220 Color: 2

Bin 617: 19 of cap free
Amount of items: 3
Items: 
Size: 352629 Color: 2
Size: 350129 Color: 4
Size: 297224 Color: 0

Bin 618: 20 of cap free
Amount of items: 3
Items: 
Size: 366501 Color: 4
Size: 325918 Color: 2
Size: 307562 Color: 3

Bin 619: 20 of cap free
Amount of items: 3
Items: 
Size: 369422 Color: 4
Size: 323674 Color: 0
Size: 306885 Color: 3

Bin 620: 20 of cap free
Amount of items: 3
Items: 
Size: 351558 Color: 4
Size: 324695 Color: 3
Size: 323728 Color: 0

Bin 621: 22 of cap free
Amount of items: 3
Items: 
Size: 365871 Color: 4
Size: 319848 Color: 1
Size: 314260 Color: 0

Bin 622: 23 of cap free
Amount of items: 3
Items: 
Size: 352426 Color: 4
Size: 330902 Color: 1
Size: 316650 Color: 3

Bin 623: 23 of cap free
Amount of items: 3
Items: 
Size: 350203 Color: 3
Size: 349868 Color: 3
Size: 299907 Color: 0

Bin 624: 28 of cap free
Amount of items: 3
Items: 
Size: 363887 Color: 4
Size: 331502 Color: 0
Size: 304584 Color: 3

Bin 625: 28 of cap free
Amount of items: 3
Items: 
Size: 353800 Color: 1
Size: 344321 Color: 0
Size: 301852 Color: 0

Bin 626: 29 of cap free
Amount of items: 3
Items: 
Size: 366791 Color: 1
Size: 359611 Color: 3
Size: 273570 Color: 0

Bin 627: 29 of cap free
Amount of items: 3
Items: 
Size: 356679 Color: 2
Size: 337698 Color: 1
Size: 305595 Color: 0

Bin 628: 35 of cap free
Amount of items: 3
Items: 
Size: 341108 Color: 0
Size: 337642 Color: 1
Size: 321216 Color: 1

Bin 629: 41 of cap free
Amount of items: 3
Items: 
Size: 352010 Color: 3
Size: 326967 Color: 2
Size: 320983 Color: 1

Bin 630: 42 of cap free
Amount of items: 3
Items: 
Size: 347516 Color: 2
Size: 341644 Color: 3
Size: 310799 Color: 3

Bin 631: 44 of cap free
Amount of items: 3
Items: 
Size: 358719 Color: 3
Size: 337353 Color: 2
Size: 303885 Color: 2

Bin 632: 44 of cap free
Amount of items: 3
Items: 
Size: 367114 Color: 3
Size: 335364 Color: 2
Size: 297479 Color: 0

Bin 633: 47 of cap free
Amount of items: 3
Items: 
Size: 338950 Color: 2
Size: 338835 Color: 1
Size: 322169 Color: 4

Bin 634: 62 of cap free
Amount of items: 3
Items: 
Size: 353818 Color: 0
Size: 353552 Color: 0
Size: 292569 Color: 3

Bin 635: 70 of cap free
Amount of items: 3
Items: 
Size: 359324 Color: 0
Size: 340266 Color: 2
Size: 300341 Color: 1

Bin 636: 83 of cap free
Amount of items: 3
Items: 
Size: 359318 Color: 3
Size: 345828 Color: 3
Size: 294772 Color: 0

Bin 637: 84 of cap free
Amount of items: 3
Items: 
Size: 346046 Color: 2
Size: 333867 Color: 0
Size: 320004 Color: 1

Bin 638: 93 of cap free
Amount of items: 3
Items: 
Size: 415079 Color: 1
Size: 298253 Color: 0
Size: 286576 Color: 0

Bin 639: 98 of cap free
Amount of items: 3
Items: 
Size: 334081 Color: 0
Size: 333533 Color: 3
Size: 332289 Color: 2

Bin 640: 138 of cap free
Amount of items: 3
Items: 
Size: 361354 Color: 3
Size: 357636 Color: 3
Size: 280873 Color: 4

Bin 641: 165 of cap free
Amount of items: 3
Items: 
Size: 352151 Color: 3
Size: 326639 Color: 1
Size: 321046 Color: 1

Bin 642: 177 of cap free
Amount of items: 3
Items: 
Size: 383709 Color: 1
Size: 336674 Color: 0
Size: 279441 Color: 0

Bin 643: 191 of cap free
Amount of items: 3
Items: 
Size: 375980 Color: 1
Size: 312051 Color: 3
Size: 311779 Color: 3

Bin 644: 200 of cap free
Amount of items: 3
Items: 
Size: 355035 Color: 4
Size: 327828 Color: 0
Size: 316938 Color: 2

Bin 645: 204 of cap free
Amount of items: 3
Items: 
Size: 365235 Color: 4
Size: 348395 Color: 1
Size: 286167 Color: 1

Bin 646: 205 of cap free
Amount of items: 3
Items: 
Size: 380435 Color: 0
Size: 359784 Color: 2
Size: 259577 Color: 1

Bin 647: 235 of cap free
Amount of items: 3
Items: 
Size: 350373 Color: 0
Size: 327797 Color: 1
Size: 321596 Color: 3

Bin 648: 290 of cap free
Amount of items: 3
Items: 
Size: 341273 Color: 0
Size: 340706 Color: 1
Size: 317732 Color: 4

Bin 649: 462 of cap free
Amount of items: 3
Items: 
Size: 348362 Color: 2
Size: 340069 Color: 1
Size: 311108 Color: 3

Bin 650: 478 of cap free
Amount of items: 3
Items: 
Size: 497585 Color: 4
Size: 251354 Color: 0
Size: 250584 Color: 4

Bin 651: 573 of cap free
Amount of items: 3
Items: 
Size: 339320 Color: 3
Size: 337749 Color: 2
Size: 322359 Color: 3

Bin 652: 688 of cap free
Amount of items: 3
Items: 
Size: 364827 Color: 3
Size: 356761 Color: 4
Size: 277725 Color: 2

Bin 653: 920 of cap free
Amount of items: 3
Items: 
Size: 362704 Color: 0
Size: 360682 Color: 0
Size: 275695 Color: 4

Bin 654: 1066 of cap free
Amount of items: 3
Items: 
Size: 334893 Color: 2
Size: 333663 Color: 4
Size: 330379 Color: 2

Bin 655: 1192 of cap free
Amount of items: 3
Items: 
Size: 489478 Color: 4
Size: 256071 Color: 1
Size: 253260 Color: 4

Bin 656: 1963 of cap free
Amount of items: 2
Items: 
Size: 499025 Color: 3
Size: 499013 Color: 2

Bin 657: 2099 of cap free
Amount of items: 3
Items: 
Size: 422255 Color: 2
Size: 322989 Color: 0
Size: 252658 Color: 4

Bin 658: 2549 of cap free
Amount of items: 3
Items: 
Size: 361749 Color: 4
Size: 356442 Color: 4
Size: 279261 Color: 3

Bin 659: 24218 of cap free
Amount of items: 3
Items: 
Size: 356207 Color: 4
Size: 354916 Color: 4
Size: 264660 Color: 0

Bin 660: 29123 of cap free
Amount of items: 3
Items: 
Size: 354525 Color: 0
Size: 352326 Color: 2
Size: 264027 Color: 3

Bin 661: 35324 of cap free
Amount of items: 3
Items: 
Size: 352300 Color: 1
Size: 351257 Color: 4
Size: 261120 Color: 4

Bin 662: 40196 of cap free
Amount of items: 3
Items: 
Size: 350153 Color: 4
Size: 349217 Color: 0
Size: 260435 Color: 0

Bin 663: 42989 of cap free
Amount of items: 3
Items: 
Size: 349055 Color: 0
Size: 348173 Color: 2
Size: 259784 Color: 0

Bin 664: 45611 of cap free
Amount of items: 3
Items: 
Size: 347669 Color: 0
Size: 347639 Color: 3
Size: 259082 Color: 1

Bin 665: 48698 of cap free
Amount of items: 3
Items: 
Size: 346537 Color: 3
Size: 345809 Color: 2
Size: 258957 Color: 0

Bin 666: 57504 of cap free
Amount of items: 3
Items: 
Size: 345789 Color: 2
Size: 341566 Color: 0
Size: 255142 Color: 0

Bin 667: 159104 of cap free
Amount of items: 3
Items: 
Size: 336346 Color: 1
Size: 252548 Color: 0
Size: 252003 Color: 3

Bin 668: 501309 of cap free
Amount of items: 1
Items: 
Size: 498692 Color: 3

Total size: 667000667
Total free space: 1000001

