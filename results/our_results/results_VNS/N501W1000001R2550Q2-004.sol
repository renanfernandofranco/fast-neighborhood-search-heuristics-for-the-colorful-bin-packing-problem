Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 468427 Color: 1
Size: 279555 Color: 1
Size: 252019 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 395590 Color: 0
Size: 348190 Color: 1
Size: 256221 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 422030 Color: 1
Size: 309109 Color: 1
Size: 268862 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 420794 Color: 0
Size: 308496 Color: 1
Size: 270711 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 400425 Color: 0
Size: 328027 Color: 1
Size: 271549 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 405270 Color: 0
Size: 320279 Color: 0
Size: 274452 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 454777 Color: 0
Size: 291198 Color: 1
Size: 254026 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 359436 Color: 0
Size: 352750 Color: 1
Size: 287815 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 489521 Color: 1
Size: 255405 Color: 0
Size: 255075 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 451194 Color: 0
Size: 283034 Color: 1
Size: 265773 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 400640 Color: 0
Size: 338310 Color: 0
Size: 261051 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 446500 Color: 0
Size: 282977 Color: 1
Size: 270524 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 490162 Color: 0
Size: 258036 Color: 1
Size: 251803 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 459994 Color: 1
Size: 281368 Color: 0
Size: 258639 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 402302 Color: 1
Size: 327275 Color: 0
Size: 270424 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 442911 Color: 1
Size: 300454 Color: 0
Size: 256636 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 382703 Color: 0
Size: 360224 Color: 1
Size: 257074 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 404294 Color: 0
Size: 315915 Color: 1
Size: 279792 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 381375 Color: 0
Size: 344004 Color: 0
Size: 274622 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 361231 Color: 0
Size: 331249 Color: 1
Size: 307521 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 407485 Color: 1
Size: 307438 Color: 0
Size: 285078 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 450159 Color: 1
Size: 299690 Color: 0
Size: 250152 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 381302 Color: 0
Size: 356460 Color: 1
Size: 262239 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 420944 Color: 1
Size: 297926 Color: 0
Size: 281131 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 358667 Color: 1
Size: 326062 Color: 0
Size: 315272 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 441490 Color: 0
Size: 292895 Color: 1
Size: 265616 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 471003 Color: 0
Size: 272583 Color: 1
Size: 256415 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 383395 Color: 1
Size: 314889 Color: 0
Size: 301717 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 444604 Color: 0
Size: 300638 Color: 1
Size: 254759 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 421475 Color: 1
Size: 309543 Color: 0
Size: 268983 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 473723 Color: 0
Size: 275395 Color: 0
Size: 250883 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 384851 Color: 0
Size: 312557 Color: 1
Size: 302593 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 368699 Color: 1
Size: 318095 Color: 0
Size: 313207 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 492185 Color: 1
Size: 255973 Color: 1
Size: 251843 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 479364 Color: 0
Size: 266756 Color: 1
Size: 253881 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 413634 Color: 0
Size: 324328 Color: 1
Size: 262039 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 414536 Color: 0
Size: 304140 Color: 1
Size: 281325 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 445656 Color: 1
Size: 280061 Color: 1
Size: 274284 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 371700 Color: 1
Size: 330864 Color: 0
Size: 297437 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 481022 Color: 0
Size: 267627 Color: 1
Size: 251352 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 375949 Color: 1
Size: 352200 Color: 0
Size: 271852 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 448220 Color: 0
Size: 291682 Color: 1
Size: 260099 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 443711 Color: 0
Size: 299286 Color: 1
Size: 257004 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 386201 Color: 1
Size: 359880 Color: 0
Size: 253920 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 396819 Color: 0
Size: 321806 Color: 1
Size: 281376 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 483272 Color: 0
Size: 266142 Color: 1
Size: 250587 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 392494 Color: 0
Size: 306138 Color: 0
Size: 301369 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 355258 Color: 0
Size: 350868 Color: 1
Size: 293875 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 383661 Color: 1
Size: 348798 Color: 0
Size: 267542 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 378812 Color: 1
Size: 352824 Color: 0
Size: 268365 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 430437 Color: 1
Size: 301837 Color: 0
Size: 267727 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 482448 Color: 0
Size: 259053 Color: 1
Size: 258500 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 383469 Color: 1
Size: 323292 Color: 1
Size: 293240 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 402228 Color: 1
Size: 334530 Color: 0
Size: 263243 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 373701 Color: 1
Size: 351646 Color: 0
Size: 274654 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 437856 Color: 1
Size: 284707 Color: 0
Size: 277438 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 365616 Color: 0
Size: 330114 Color: 1
Size: 304271 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 443127 Color: 1
Size: 305895 Color: 0
Size: 250979 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 360348 Color: 0
Size: 346912 Color: 1
Size: 292741 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 409186 Color: 1
Size: 335608 Color: 1
Size: 255207 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 357000 Color: 0
Size: 335973 Color: 1
Size: 307028 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 477212 Color: 0
Size: 268956 Color: 1
Size: 253833 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 421842 Color: 0
Size: 297559 Color: 0
Size: 280600 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 385088 Color: 1
Size: 318936 Color: 0
Size: 295977 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 478771 Color: 0
Size: 265280 Color: 1
Size: 255950 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 410077 Color: 1
Size: 339203 Color: 0
Size: 250721 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 429257 Color: 1
Size: 296275 Color: 1
Size: 274469 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 446412 Color: 1
Size: 286175 Color: 0
Size: 267414 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 392235 Color: 0
Size: 343465 Color: 0
Size: 264301 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 373734 Color: 0
Size: 348020 Color: 1
Size: 278247 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 397598 Color: 1
Size: 302357 Color: 1
Size: 300046 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 401426 Color: 0
Size: 300074 Color: 1
Size: 298501 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 445283 Color: 1
Size: 290241 Color: 0
Size: 264477 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 347992 Color: 1
Size: 331792 Color: 1
Size: 320217 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 462455 Color: 1
Size: 274846 Color: 0
Size: 262700 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 482258 Color: 0
Size: 259920 Color: 1
Size: 257823 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 448769 Color: 0
Size: 288828 Color: 1
Size: 262404 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 459070 Color: 1
Size: 277333 Color: 1
Size: 263598 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 408755 Color: 0
Size: 328744 Color: 0
Size: 262502 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 483929 Color: 1
Size: 261575 Color: 0
Size: 254497 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 377026 Color: 1
Size: 331628 Color: 0
Size: 291347 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 403135 Color: 1
Size: 312693 Color: 0
Size: 284173 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 465780 Color: 0
Size: 277050 Color: 0
Size: 257171 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 415994 Color: 1
Size: 328080 Color: 0
Size: 255927 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 435715 Color: 1
Size: 301018 Color: 0
Size: 263268 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 407257 Color: 0
Size: 319855 Color: 1
Size: 272889 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 420563 Color: 0
Size: 300826 Color: 1
Size: 278612 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 406503 Color: 1
Size: 327015 Color: 0
Size: 266483 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 432129 Color: 0
Size: 317346 Color: 1
Size: 250526 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 380332 Color: 0
Size: 337238 Color: 0
Size: 282431 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 396573 Color: 1
Size: 302705 Color: 1
Size: 300723 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 390728 Color: 1
Size: 325932 Color: 0
Size: 283341 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 476260 Color: 0
Size: 270968 Color: 1
Size: 252773 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 452514 Color: 0
Size: 286110 Color: 1
Size: 261377 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 420315 Color: 0
Size: 299335 Color: 1
Size: 280351 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 408711 Color: 0
Size: 337796 Color: 1
Size: 253494 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 399406 Color: 0
Size: 339891 Color: 1
Size: 260704 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 413683 Color: 1
Size: 306238 Color: 0
Size: 280080 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 391803 Color: 0
Size: 334849 Color: 1
Size: 273349 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 368187 Color: 0
Size: 321313 Color: 1
Size: 310501 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 417505 Color: 1
Size: 324686 Color: 0
Size: 257810 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 495713 Color: 0
Size: 253508 Color: 1
Size: 250780 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 431160 Color: 1
Size: 309722 Color: 1
Size: 259119 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 368652 Color: 0
Size: 351312 Color: 1
Size: 280037 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 469958 Color: 1
Size: 271893 Color: 1
Size: 258150 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 396516 Color: 1
Size: 317613 Color: 1
Size: 285872 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 408282 Color: 1
Size: 310422 Color: 0
Size: 281297 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 353045 Color: 1
Size: 342375 Color: 0
Size: 304581 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 428597 Color: 0
Size: 287927 Color: 1
Size: 283477 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 401323 Color: 0
Size: 321682 Color: 1
Size: 276996 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 384523 Color: 0
Size: 322864 Color: 1
Size: 292614 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 448037 Color: 0
Size: 291978 Color: 1
Size: 259986 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 373511 Color: 0
Size: 359786 Color: 1
Size: 266704 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 455221 Color: 0
Size: 284070 Color: 0
Size: 260710 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 441110 Color: 0
Size: 303276 Color: 1
Size: 255615 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 400619 Color: 0
Size: 311419 Color: 1
Size: 287963 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 388527 Color: 1
Size: 311419 Color: 1
Size: 300055 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 386015 Color: 0
Size: 318443 Color: 1
Size: 295543 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 417157 Color: 0
Size: 330616 Color: 1
Size: 252228 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 410310 Color: 0
Size: 295734 Color: 1
Size: 293957 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 424856 Color: 0
Size: 296242 Color: 0
Size: 278903 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 474490 Color: 0
Size: 273565 Color: 1
Size: 251946 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 390251 Color: 1
Size: 311633 Color: 0
Size: 298117 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 467969 Color: 0
Size: 267822 Color: 1
Size: 264210 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 421499 Color: 0
Size: 311306 Color: 0
Size: 267196 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 391187 Color: 0
Size: 335327 Color: 1
Size: 273487 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 391953 Color: 1
Size: 349559 Color: 0
Size: 258489 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 492607 Color: 0
Size: 255221 Color: 1
Size: 252173 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 469301 Color: 0
Size: 277667 Color: 1
Size: 253033 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 375914 Color: 1
Size: 335559 Color: 1
Size: 288528 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 460990 Color: 0
Size: 271401 Color: 0
Size: 267610 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 364393 Color: 1
Size: 356228 Color: 0
Size: 279380 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 378535 Color: 0
Size: 330803 Color: 1
Size: 290663 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 451477 Color: 0
Size: 291614 Color: 1
Size: 256910 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 488653 Color: 1
Size: 256131 Color: 1
Size: 255217 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 407010 Color: 0
Size: 309509 Color: 1
Size: 283482 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 360430 Color: 0
Size: 320397 Color: 1
Size: 319174 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 369071 Color: 1
Size: 357904 Color: 0
Size: 273026 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 348927 Color: 1
Size: 340710 Color: 0
Size: 310364 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 372050 Color: 1
Size: 337414 Color: 0
Size: 290537 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 397263 Color: 1
Size: 331333 Color: 1
Size: 271405 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 422000 Color: 1
Size: 289746 Color: 0
Size: 288255 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 410018 Color: 0
Size: 320475 Color: 1
Size: 269508 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 430523 Color: 1
Size: 287653 Color: 0
Size: 281825 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 369053 Color: 0
Size: 317138 Color: 1
Size: 313810 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 415004 Color: 1
Size: 314956 Color: 1
Size: 270041 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 402239 Color: 0
Size: 308224 Color: 1
Size: 289538 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 417356 Color: 0
Size: 302652 Color: 0
Size: 279993 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 452510 Color: 1
Size: 276796 Color: 0
Size: 270695 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 449811 Color: 1
Size: 292585 Color: 0
Size: 257605 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 410412 Color: 1
Size: 313258 Color: 1
Size: 276331 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 442955 Color: 1
Size: 305054 Color: 0
Size: 251992 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 381978 Color: 1
Size: 324790 Color: 1
Size: 293233 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 340695 Color: 1
Size: 331629 Color: 0
Size: 327677 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 353705 Color: 1
Size: 331842 Color: 1
Size: 314454 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 499807 Color: 1
Size: 250123 Color: 0
Size: 250071 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 382840 Color: 0
Size: 315700 Color: 1
Size: 301461 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 414401 Color: 0
Size: 308143 Color: 1
Size: 277457 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 481540 Color: 1
Size: 266004 Color: 0
Size: 252457 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 370657 Color: 1
Size: 361910 Color: 0
Size: 267434 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 391527 Color: 1
Size: 317046 Color: 1
Size: 291428 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 443975 Color: 1
Size: 284302 Color: 0
Size: 271724 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 339301 Color: 0
Size: 316869 Color: 1
Size: 343831 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 458250 Color: 0
Size: 282443 Color: 1
Size: 259308 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 453059 Color: 0
Size: 293822 Color: 1
Size: 253120 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 394611 Color: 1
Size: 317653 Color: 0
Size: 287737 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 353845 Color: 1
Size: 346118 Color: 0
Size: 300038 Color: 0

Total size: 167000167
Total free space: 0

