Capicity Bin: 8184
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 292
Size: 3004 Color: 260
Size: 184 Color: 28

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6115 Color: 324
Size: 1231 Color: 186
Size: 838 Color: 146

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 325
Size: 1716 Color: 215
Size: 336 Color: 84

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6223 Color: 331
Size: 1631 Color: 209
Size: 330 Color: 83

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6330 Color: 336
Size: 1534 Color: 203
Size: 320 Color: 78

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 338
Size: 1479 Color: 202
Size: 294 Color: 73

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6494 Color: 341
Size: 1234 Color: 187
Size: 456 Color: 109

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 342
Size: 1402 Color: 198
Size: 276 Color: 69

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 343
Size: 1222 Color: 185
Size: 440 Color: 103

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6577 Color: 348
Size: 1341 Color: 194
Size: 266 Color: 64

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6581 Color: 349
Size: 1337 Color: 193
Size: 266 Color: 65

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6585 Color: 350
Size: 1333 Color: 192
Size: 266 Color: 63

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 352
Size: 1386 Color: 196
Size: 188 Color: 30

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 357
Size: 782 Color: 142
Size: 680 Color: 137

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 358
Size: 1084 Color: 172
Size: 368 Color: 90

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 359
Size: 1204 Color: 183
Size: 232 Color: 51

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 361
Size: 1095 Color: 174
Size: 280 Color: 70

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6818 Color: 362
Size: 1018 Color: 167
Size: 348 Color: 89

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 368
Size: 902 Color: 154
Size: 390 Color: 94

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 369
Size: 994 Color: 164
Size: 288 Color: 71

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6919 Color: 370
Size: 1055 Color: 170
Size: 210 Color: 42

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6926 Color: 371
Size: 950 Color: 158
Size: 308 Color: 75

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6927 Color: 372
Size: 1049 Color: 168
Size: 208 Color: 41

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7015 Color: 376
Size: 975 Color: 163
Size: 194 Color: 35

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7019 Color: 377
Size: 895 Color: 153
Size: 270 Color: 66

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 378
Size: 592 Color: 128
Size: 572 Color: 125

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7043 Color: 381
Size: 951 Color: 159
Size: 190 Color: 31

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 382
Size: 966 Color: 161
Size: 172 Color: 23

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7047 Color: 383
Size: 949 Color: 157
Size: 188 Color: 29

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 386
Size: 842 Color: 147
Size: 240 Color: 56

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7115 Color: 388
Size: 891 Color: 152
Size: 178 Color: 24

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 390
Size: 830 Color: 145
Size: 212 Color: 43

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 391
Size: 862 Color: 149
Size: 172 Color: 22

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 392
Size: 780 Color: 141
Size: 232 Color: 52

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 393
Size: 870 Color: 150
Size: 140 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 394
Size: 762 Color: 140
Size: 228 Color: 50

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 396
Size: 716 Color: 139
Size: 218 Color: 46

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 397
Size: 556 Color: 124
Size: 376 Color: 91

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 399
Size: 714 Color: 138
Size: 196 Color: 36

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7294 Color: 400
Size: 544 Color: 123
Size: 346 Color: 88

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7330 Color: 401
Size: 528 Color: 119
Size: 326 Color: 80

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7332 Color: 402
Size: 592 Color: 127
Size: 260 Color: 61

Bin 43: 1 of cap free
Amount of items: 7
Items: 
Size: 4093 Color: 273
Size: 884 Color: 151
Size: 844 Color: 148
Size: 826 Color: 144
Size: 812 Color: 143
Size: 480 Color: 111
Size: 244 Color: 58

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5449 Color: 302
Size: 2582 Color: 244
Size: 152 Color: 14

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5766 Color: 309
Size: 2281 Color: 239
Size: 136 Color: 11

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5773 Color: 311
Size: 2274 Color: 238
Size: 136 Color: 10

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5855 Color: 317
Size: 2228 Color: 235
Size: 100 Color: 5

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6203 Color: 329
Size: 1876 Color: 220
Size: 104 Color: 6

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6219 Color: 330
Size: 1964 Color: 225

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 6346 Color: 337
Size: 1837 Color: 218

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 6544 Color: 346
Size: 1639 Color: 211

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 6852 Color: 364
Size: 1331 Color: 191

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 6875 Color: 367
Size: 1308 Color: 189

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 6994 Color: 375
Size: 1189 Color: 182

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 7212 Color: 395
Size: 971 Color: 162

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 285
Size: 3324 Color: 265
Size: 208 Color: 40

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 5458 Color: 303
Size: 2724 Color: 252

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5802 Color: 313
Size: 1860 Color: 219
Size: 520 Color: 118

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 5884 Color: 318
Size: 2298 Color: 240

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 6227 Color: 332
Size: 1955 Color: 224

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 6706 Color: 355
Size: 1476 Color: 201

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 6970 Color: 373
Size: 1212 Color: 184

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 7132 Color: 389
Size: 1050 Color: 169

Bin 64: 3 of cap free
Amount of items: 5
Items: 
Size: 4098 Color: 276
Size: 1698 Color: 213
Size: 1629 Color: 208
Size: 532 Color: 120
Size: 224 Color: 49

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6530 Color: 345
Size: 1651 Color: 212

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6561 Color: 347
Size: 1620 Color: 207

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 6867 Color: 365
Size: 1314 Color: 190

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 7111 Color: 387
Size: 1070 Color: 171

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 7270 Color: 398
Size: 911 Color: 155

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 5465 Color: 304
Size: 2714 Color: 251

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 5786 Color: 312
Size: 2267 Color: 237
Size: 126 Color: 9

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 6427 Color: 340
Size: 1751 Color: 217

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 6632 Color: 354
Size: 1546 Color: 204

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 7028 Color: 380
Size: 1150 Color: 180

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 6589 Color: 351
Size: 1588 Color: 206

Bin 76: 8 of cap free
Amount of items: 3
Items: 
Size: 5772 Color: 310
Size: 1388 Color: 197
Size: 1016 Color: 166

Bin 77: 8 of cap free
Amount of items: 3
Items: 
Size: 5810 Color: 314
Size: 2254 Color: 236
Size: 112 Color: 8

Bin 78: 9 of cap free
Amount of items: 2
Items: 
Size: 6822 Color: 363
Size: 1353 Color: 195

Bin 79: 9 of cap free
Amount of items: 2
Items: 
Size: 7076 Color: 384
Size: 1099 Color: 175

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 7091 Color: 385
Size: 1084 Color: 173

Bin 81: 10 of cap free
Amount of items: 3
Items: 
Size: 5430 Color: 301
Size: 2592 Color: 247
Size: 152 Color: 15

Bin 82: 10 of cap free
Amount of items: 2
Items: 
Size: 6188 Color: 328
Size: 1986 Color: 227

Bin 83: 10 of cap free
Amount of items: 2
Items: 
Size: 6620 Color: 353
Size: 1554 Color: 205

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 6709 Color: 356
Size: 1465 Color: 200

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 6991 Color: 374
Size: 1183 Color: 181

Bin 86: 11 of cap free
Amount of items: 2
Items: 
Size: 7026 Color: 379
Size: 1147 Color: 179

Bin 87: 12 of cap free
Amount of items: 2
Items: 
Size: 4580 Color: 280
Size: 3592 Color: 271

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 6231 Color: 333
Size: 1941 Color: 223

Bin 89: 14 of cap free
Amount of items: 2
Items: 
Size: 6284 Color: 335
Size: 1886 Color: 221

Bin 90: 15 of cap free
Amount of items: 2
Items: 
Size: 6759 Color: 360
Size: 1410 Color: 199

Bin 91: 16 of cap free
Amount of items: 2
Items: 
Size: 6150 Color: 327
Size: 2018 Color: 232

Bin 92: 16 of cap free
Amount of items: 2
Items: 
Size: 6244 Color: 334
Size: 1924 Color: 222

Bin 93: 17 of cap free
Amount of items: 2
Items: 
Size: 6871 Color: 366
Size: 1296 Color: 188

Bin 94: 21 of cap free
Amount of items: 3
Items: 
Size: 6099 Color: 323
Size: 2012 Color: 230
Size: 52 Color: 0

Bin 95: 25 of cap free
Amount of items: 4
Items: 
Size: 5236 Color: 298
Size: 2591 Color: 246
Size: 168 Color: 19
Size: 164 Color: 18

Bin 96: 25 of cap free
Amount of items: 2
Items: 
Size: 6420 Color: 339
Size: 1739 Color: 216

Bin 97: 25 of cap free
Amount of items: 2
Items: 
Size: 6524 Color: 344
Size: 1635 Color: 210

Bin 98: 26 of cap free
Amount of items: 3
Items: 
Size: 6076 Color: 321
Size: 2002 Color: 228
Size: 80 Color: 2

Bin 99: 28 of cap free
Amount of items: 2
Items: 
Size: 4100 Color: 277
Size: 4056 Color: 272

Bin 100: 30 of cap free
Amount of items: 3
Items: 
Size: 4930 Color: 290
Size: 3032 Color: 261
Size: 192 Color: 33

Bin 101: 32 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 300
Size: 2604 Color: 248
Size: 152 Color: 16

Bin 102: 32 of cap free
Amount of items: 2
Items: 
Size: 5828 Color: 315
Size: 2324 Color: 241

Bin 103: 32 of cap free
Amount of items: 3
Items: 
Size: 5839 Color: 316
Size: 2201 Color: 234
Size: 112 Color: 7

Bin 104: 32 of cap free
Amount of items: 3
Items: 
Size: 6085 Color: 322
Size: 2011 Color: 229
Size: 56 Color: 1

Bin 105: 35 of cap free
Amount of items: 3
Items: 
Size: 5759 Color: 308
Size: 1710 Color: 214
Size: 680 Color: 135

Bin 106: 36 of cap free
Amount of items: 2
Items: 
Size: 5482 Color: 305
Size: 2666 Color: 250

Bin 107: 38 of cap free
Amount of items: 2
Items: 
Size: 6134 Color: 326
Size: 2012 Color: 231

Bin 108: 43 of cap free
Amount of items: 7
Items: 
Size: 4094 Color: 274
Size: 995 Color: 165
Size: 964 Color: 160
Size: 924 Color: 156
Size: 680 Color: 136
Size: 244 Color: 57
Size: 240 Color: 55

Bin 109: 43 of cap free
Amount of items: 3
Items: 
Size: 4986 Color: 291
Size: 2963 Color: 259
Size: 192 Color: 32

Bin 110: 44 of cap free
Amount of items: 3
Items: 
Size: 4738 Color: 286
Size: 3194 Color: 264
Size: 208 Color: 39

Bin 111: 48 of cap free
Amount of items: 2
Items: 
Size: 5676 Color: 307
Size: 2460 Color: 243

Bin 112: 52 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 297
Size: 2874 Color: 255
Size: 168 Color: 20

Bin 113: 56 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 319
Size: 2092 Color: 233
Size: 96 Color: 4

Bin 114: 64 of cap free
Amount of items: 3
Items: 
Size: 6058 Color: 320
Size: 1982 Color: 226
Size: 80 Color: 3

Bin 115: 80 of cap free
Amount of items: 16
Items: 
Size: 680 Color: 134
Size: 680 Color: 133
Size: 664 Color: 132
Size: 632 Color: 131
Size: 600 Color: 130
Size: 592 Color: 129
Size: 588 Color: 126
Size: 540 Color: 122
Size: 536 Color: 121
Size: 516 Color: 117
Size: 516 Color: 116
Size: 512 Color: 115
Size: 272 Color: 67
Size: 264 Color: 62
Size: 256 Color: 60
Size: 256 Color: 59

Bin 116: 88 of cap free
Amount of items: 2
Items: 
Size: 5060 Color: 293
Size: 3036 Color: 262

Bin 117: 88 of cap free
Amount of items: 3
Items: 
Size: 5516 Color: 306
Size: 2444 Color: 242
Size: 136 Color: 12

Bin 118: 104 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 299
Size: 2660 Color: 249
Size: 160 Color: 17

Bin 119: 112 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 289
Size: 2962 Color: 258
Size: 194 Color: 34

Bin 120: 141 of cap free
Amount of items: 3
Items: 
Size: 5079 Color: 295
Size: 2786 Color: 253
Size: 178 Color: 25

Bin 121: 142 of cap free
Amount of items: 4
Items: 
Size: 4196 Color: 278
Size: 3404 Color: 266
Size: 224 Color: 48
Size: 218 Color: 47

Bin 122: 142 of cap free
Amount of items: 2
Items: 
Size: 4631 Color: 283
Size: 3411 Color: 270

Bin 123: 144 of cap free
Amount of items: 2
Items: 
Size: 4630 Color: 282
Size: 3410 Color: 269

Bin 124: 144 of cap free
Amount of items: 3
Items: 
Size: 5080 Color: 296
Size: 2792 Color: 254
Size: 168 Color: 21

Bin 125: 146 of cap free
Amount of items: 2
Items: 
Size: 4629 Color: 281
Size: 3409 Color: 268

Bin 126: 156 of cap free
Amount of items: 4
Items: 
Size: 5077 Color: 294
Size: 2589 Color: 245
Size: 182 Color: 27
Size: 180 Color: 26

Bin 127: 166 of cap free
Amount of items: 3
Items: 
Size: 4634 Color: 284
Size: 3172 Color: 263
Size: 212 Color: 44

Bin 128: 182 of cap free
Amount of items: 3
Items: 
Size: 4380 Color: 279
Size: 3406 Color: 267
Size: 216 Color: 45

Bin 129: 184 of cap free
Amount of items: 3
Items: 
Size: 4842 Color: 288
Size: 2962 Color: 257
Size: 196 Color: 37

Bin 130: 189 of cap free
Amount of items: 3
Items: 
Size: 4836 Color: 287
Size: 2961 Color: 256
Size: 198 Color: 38

Bin 131: 235 of cap free
Amount of items: 6
Items: 
Size: 4095 Color: 275
Size: 1142 Color: 178
Size: 1120 Color: 177
Size: 1116 Color: 176
Size: 240 Color: 54
Size: 236 Color: 53

Bin 132: 330 of cap free
Amount of items: 20
Items: 
Size: 512 Color: 114
Size: 504 Color: 113
Size: 488 Color: 112
Size: 464 Color: 110
Size: 456 Color: 108
Size: 454 Color: 107
Size: 452 Color: 106
Size: 452 Color: 105
Size: 448 Color: 104
Size: 416 Color: 102
Size: 400 Color: 101
Size: 336 Color: 85
Size: 326 Color: 82
Size: 326 Color: 81
Size: 324 Color: 79
Size: 312 Color: 77
Size: 312 Color: 76
Size: 304 Color: 74
Size: 292 Color: 72
Size: 276 Color: 68

Bin 133: 4356 of cap free
Amount of items: 10
Items: 
Size: 400 Color: 100
Size: 400 Color: 99
Size: 396 Color: 98
Size: 396 Color: 97
Size: 392 Color: 96
Size: 392 Color: 95
Size: 388 Color: 93
Size: 384 Color: 92
Size: 340 Color: 87
Size: 340 Color: 86

Total size: 1080288
Total free space: 8184

