Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8148 Color: 1
Size: 6788 Color: 1
Size: 1352 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 11442 Color: 1
Size: 2924 Color: 1
Size: 1200 Color: 0
Size: 722 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9253 Color: 1
Size: 5863 Color: 1
Size: 1172 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12078 Color: 1
Size: 3510 Color: 1
Size: 700 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10378 Color: 1
Size: 4926 Color: 1
Size: 984 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10520 Color: 1
Size: 5448 Color: 1
Size: 320 Color: 0

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 8376 Color: 1
Size: 5000 Color: 1
Size: 2000 Color: 0
Size: 912 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8146 Color: 1
Size: 6786 Color: 1
Size: 1356 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8147 Color: 1
Size: 6785 Color: 1
Size: 1356 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 1
Size: 6764 Color: 1
Size: 1344 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 9284 Color: 1
Size: 5844 Color: 1
Size: 1160 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 9300 Color: 1
Size: 5844 Color: 1
Size: 1144 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 9304 Color: 1
Size: 6000 Color: 1
Size: 984 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9380 Color: 1
Size: 5764 Color: 1
Size: 1144 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 9396 Color: 1
Size: 5748 Color: 1
Size: 1144 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 9444 Color: 1
Size: 5812 Color: 1
Size: 1032 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 9766 Color: 1
Size: 5828 Color: 1
Size: 694 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 9798 Color: 1
Size: 5410 Color: 1
Size: 1080 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 10133 Color: 1
Size: 5131 Color: 1
Size: 1024 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 10356 Color: 1
Size: 5292 Color: 1
Size: 640 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 10806 Color: 1
Size: 4570 Color: 1
Size: 912 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 10865 Color: 1
Size: 4521 Color: 1
Size: 902 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11069 Color: 1
Size: 4351 Color: 1
Size: 868 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11246 Color: 1
Size: 4202 Color: 1
Size: 840 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11256 Color: 1
Size: 4328 Color: 1
Size: 704 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11314 Color: 1
Size: 4146 Color: 1
Size: 828 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11452 Color: 1
Size: 4036 Color: 1
Size: 800 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11458 Color: 1
Size: 4166 Color: 1
Size: 664 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 1
Size: 3880 Color: 1
Size: 768 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 1
Size: 4046 Color: 1
Size: 592 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11682 Color: 1
Size: 3646 Color: 1
Size: 960 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11723 Color: 1
Size: 3805 Color: 1
Size: 760 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 1
Size: 3572 Color: 1
Size: 804 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11914 Color: 1
Size: 3214 Color: 1
Size: 1160 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12004 Color: 1
Size: 3564 Color: 1
Size: 720 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12020 Color: 1
Size: 3180 Color: 1
Size: 1088 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 1
Size: 3467 Color: 1
Size: 704 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12161 Color: 1
Size: 3441 Color: 1
Size: 686 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12169 Color: 1
Size: 3433 Color: 1
Size: 686 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 1
Size: 3432 Color: 1
Size: 672 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12334 Color: 1
Size: 3298 Color: 1
Size: 656 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12402 Color: 1
Size: 3242 Color: 1
Size: 644 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12434 Color: 1
Size: 3482 Color: 1
Size: 372 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12492 Color: 1
Size: 2888 Color: 1
Size: 908 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12520 Color: 1
Size: 3144 Color: 1
Size: 624 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12607 Color: 1
Size: 3069 Color: 1
Size: 612 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12634 Color: 1
Size: 3138 Color: 1
Size: 516 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12698 Color: 1
Size: 2994 Color: 1
Size: 596 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12764 Color: 1
Size: 2316 Color: 1
Size: 1208 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 1
Size: 2644 Color: 1
Size: 864 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 2564 Color: 1
Size: 820 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 1
Size: 2448 Color: 1
Size: 920 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 1
Size: 2770 Color: 1
Size: 552 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12994 Color: 1
Size: 2734 Color: 1
Size: 560 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13010 Color: 1
Size: 2746 Color: 1
Size: 532 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 1
Size: 2728 Color: 1
Size: 544 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13040 Color: 1
Size: 2856 Color: 1
Size: 392 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13082 Color: 1
Size: 2646 Color: 1
Size: 560 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13114 Color: 1
Size: 2402 Color: 1
Size: 772 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13140 Color: 1
Size: 2628 Color: 1
Size: 520 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 1
Size: 2605 Color: 1
Size: 520 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 1
Size: 2568 Color: 1
Size: 544 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13236 Color: 1
Size: 2548 Color: 1
Size: 504 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 1
Size: 2508 Color: 1
Size: 496 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 1
Size: 2490 Color: 1
Size: 496 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13314 Color: 1
Size: 2200 Color: 1
Size: 774 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13346 Color: 1
Size: 2454 Color: 1
Size: 488 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13360 Color: 1
Size: 2424 Color: 1
Size: 504 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 1
Size: 2022 Color: 1
Size: 882 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13397 Color: 1
Size: 2411 Color: 1
Size: 480 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13410 Color: 1
Size: 2414 Color: 1
Size: 464 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13480 Color: 1
Size: 2416 Color: 1
Size: 392 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13487 Color: 1
Size: 2497 Color: 1
Size: 304 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13490 Color: 1
Size: 2334 Color: 1
Size: 464 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13558 Color: 1
Size: 2278 Color: 1
Size: 452 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13654 Color: 1
Size: 2086 Color: 1
Size: 548 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 1
Size: 2224 Color: 1
Size: 408 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13686 Color: 1
Size: 2170 Color: 1
Size: 432 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13732 Color: 1
Size: 2396 Color: 1
Size: 160 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13742 Color: 1
Size: 1714 Color: 1
Size: 832 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13804 Color: 1
Size: 2044 Color: 1
Size: 440 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13810 Color: 1
Size: 1986 Color: 1
Size: 492 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13829 Color: 1
Size: 2035 Color: 1
Size: 424 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 1
Size: 1996 Color: 1
Size: 456 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13866 Color: 1
Size: 1842 Color: 1
Size: 580 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 1
Size: 2312 Color: 1
Size: 96 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 1
Size: 1980 Color: 1
Size: 408 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 1
Size: 2244 Color: 1
Size: 128 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13922 Color: 1
Size: 2002 Color: 1
Size: 364 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13932 Color: 1
Size: 1964 Color: 1
Size: 392 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 13960 Color: 1
Size: 1922 Color: 1
Size: 406 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 13964 Color: 1
Size: 2060 Color: 1
Size: 264 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 1
Size: 1940 Color: 1
Size: 324 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 14082 Color: 1
Size: 1882 Color: 1
Size: 324 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14083 Color: 1
Size: 1845 Color: 1
Size: 360 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 14104 Color: 1
Size: 1896 Color: 1
Size: 288 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 1
Size: 1820 Color: 1
Size: 360 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 1
Size: 1814 Color: 1
Size: 360 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 14124 Color: 1
Size: 1832 Color: 1
Size: 332 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 1
Size: 1796 Color: 1
Size: 352 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 14165 Color: 1
Size: 1771 Color: 1
Size: 352 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 1
Size: 1974 Color: 1
Size: 120 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 14234 Color: 1
Size: 1642 Color: 1
Size: 412 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 1
Size: 1608 Color: 1
Size: 432 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 14255 Color: 1
Size: 1695 Color: 1
Size: 338 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 1
Size: 1692 Color: 1
Size: 336 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 1
Size: 1684 Color: 1
Size: 336 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14279 Color: 1
Size: 1675 Color: 1
Size: 334 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14322 Color: 1
Size: 1826 Color: 1
Size: 140 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 1
Size: 1604 Color: 1
Size: 340 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 1
Size: 1612 Color: 1
Size: 320 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14362 Color: 1
Size: 1606 Color: 1
Size: 320 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14364 Color: 1
Size: 1704 Color: 1
Size: 220 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14376 Color: 1
Size: 1532 Color: 1
Size: 380 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14430 Color: 1
Size: 1624 Color: 1
Size: 234 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14431 Color: 1
Size: 1549 Color: 1
Size: 308 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 1
Size: 1464 Color: 1
Size: 384 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14449 Color: 1
Size: 1533 Color: 1
Size: 306 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 1
Size: 1804 Color: 1
Size: 32 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14460 Color: 1
Size: 1524 Color: 1
Size: 304 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14469 Color: 1
Size: 1517 Color: 1
Size: 302 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14494 Color: 1
Size: 1498 Color: 1
Size: 296 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14501 Color: 1
Size: 1491 Color: 1
Size: 296 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 1
Size: 1396 Color: 1
Size: 360 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14536 Color: 1
Size: 1416 Color: 1
Size: 336 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14557 Color: 1
Size: 1443 Color: 1
Size: 288 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14592 Color: 1
Size: 1440 Color: 1
Size: 256 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14594 Color: 1
Size: 1414 Color: 1
Size: 280 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 1
Size: 880 Color: 1
Size: 808 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 1
Size: 1404 Color: 1
Size: 272 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14620 Color: 1
Size: 1364 Color: 1
Size: 304 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 14626 Color: 1
Size: 1386 Color: 1
Size: 276 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 1
Size: 1372 Color: 1
Size: 272 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 1
Size: 1628 Color: 1
Size: 8 Color: 0

Bin 135: 1 of cap free
Amount of items: 3
Items: 
Size: 9034 Color: 1
Size: 5901 Color: 1
Size: 1352 Color: 0

Bin 136: 1 of cap free
Amount of items: 3
Items: 
Size: 10406 Color: 1
Size: 4529 Color: 1
Size: 1352 Color: 0

Bin 137: 1 of cap free
Amount of items: 3
Items: 
Size: 10756 Color: 1
Size: 5055 Color: 1
Size: 476 Color: 0

Bin 138: 1 of cap free
Amount of items: 3
Items: 
Size: 10835 Color: 1
Size: 4612 Color: 1
Size: 840 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 1
Size: 4413 Color: 1
Size: 984 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 11037 Color: 1
Size: 4042 Color: 1
Size: 1208 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 1
Size: 3879 Color: 1
Size: 1312 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 1
Size: 3411 Color: 1
Size: 400 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 1
Size: 2911 Color: 1
Size: 728 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 12745 Color: 1
Size: 2966 Color: 1
Size: 576 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 12797 Color: 1
Size: 2940 Color: 1
Size: 550 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 12979 Color: 1
Size: 2916 Color: 1
Size: 392 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 13055 Color: 1
Size: 2824 Color: 1
Size: 408 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 1
Size: 2759 Color: 1
Size: 304 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 1
Size: 2695 Color: 1
Size: 76 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 13551 Color: 1
Size: 2152 Color: 1
Size: 584 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 13590 Color: 1
Size: 2201 Color: 1
Size: 496 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 13627 Color: 1
Size: 2122 Color: 1
Size: 538 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 1
Size: 2120 Color: 1
Size: 520 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 13748 Color: 1
Size: 2219 Color: 1
Size: 320 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 1
Size: 2051 Color: 1
Size: 416 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 13847 Color: 1
Size: 2344 Color: 1
Size: 96 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 13931 Color: 1
Size: 1544 Color: 1
Size: 812 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 14034 Color: 1
Size: 1669 Color: 1
Size: 584 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 14162 Color: 1
Size: 1965 Color: 1
Size: 160 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 8992 Color: 1
Size: 6782 Color: 1
Size: 512 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 9030 Color: 1
Size: 6096 Color: 1
Size: 1160 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 10504 Color: 1
Size: 4502 Color: 1
Size: 1280 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 11346 Color: 1
Size: 4044 Color: 1
Size: 896 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 11410 Color: 1
Size: 4604 Color: 1
Size: 272 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 11436 Color: 1
Size: 4066 Color: 1
Size: 784 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 1
Size: 3866 Color: 1
Size: 396 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 12585 Color: 1
Size: 2617 Color: 1
Size: 1084 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 12730 Color: 1
Size: 3284 Color: 1
Size: 272 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 12796 Color: 1
Size: 3048 Color: 1
Size: 442 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 13124 Color: 1
Size: 2482 Color: 1
Size: 680 Color: 0

Bin 171: 2 of cap free
Amount of items: 3
Items: 
Size: 13220 Color: 1
Size: 2434 Color: 1
Size: 632 Color: 0

Bin 172: 2 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 1
Size: 2066 Color: 1
Size: 624 Color: 0

Bin 173: 2 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 1
Size: 1356 Color: 0
Size: 1178 Color: 0

Bin 174: 2 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 1
Size: 2124 Color: 1
Size: 176 Color: 0

Bin 175: 3 of cap free
Amount of items: 3
Items: 
Size: 8154 Color: 1
Size: 6787 Color: 1
Size: 1344 Color: 0

Bin 176: 3 of cap free
Amount of items: 3
Items: 
Size: 9209 Color: 1
Size: 6772 Color: 1
Size: 304 Color: 0

Bin 177: 3 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 1
Size: 5161 Color: 1
Size: 352 Color: 0

Bin 178: 3 of cap free
Amount of items: 3
Items: 
Size: 12197 Color: 1
Size: 3560 Color: 1
Size: 528 Color: 0

Bin 179: 3 of cap free
Amount of items: 5
Items: 
Size: 6792 Color: 1
Size: 4808 Color: 1
Size: 3469 Color: 1
Size: 800 Color: 0
Size: 416 Color: 0

Bin 180: 4 of cap free
Amount of items: 3
Items: 
Size: 9412 Color: 1
Size: 6600 Color: 1
Size: 272 Color: 0

Bin 181: 4 of cap free
Amount of items: 3
Items: 
Size: 10072 Color: 1
Size: 5732 Color: 1
Size: 480 Color: 0

Bin 182: 4 of cap free
Amount of items: 3
Items: 
Size: 10372 Color: 1
Size: 5640 Color: 1
Size: 272 Color: 0

Bin 183: 4 of cap free
Amount of items: 3
Items: 
Size: 10438 Color: 1
Size: 5438 Color: 1
Size: 408 Color: 0

Bin 184: 5 of cap free
Amount of items: 3
Items: 
Size: 12110 Color: 1
Size: 3477 Color: 1
Size: 696 Color: 0

Bin 185: 5 of cap free
Amount of items: 3
Items: 
Size: 10095 Color: 1
Size: 5708 Color: 1
Size: 480 Color: 0

Bin 186: 5 of cap free
Amount of items: 3
Items: 
Size: 11635 Color: 1
Size: 4200 Color: 1
Size: 448 Color: 0

Bin 187: 6 of cap free
Amount of items: 3
Items: 
Size: 9752 Color: 1
Size: 6050 Color: 1
Size: 480 Color: 0

Bin 188: 6 of cap free
Amount of items: 3
Items: 
Size: 8152 Color: 1
Size: 5010 Color: 1
Size: 3120 Color: 0

Bin 189: 7 of cap free
Amount of items: 3
Items: 
Size: 10909 Color: 1
Size: 4964 Color: 1
Size: 408 Color: 0

Bin 190: 12 of cap free
Amount of items: 3
Items: 
Size: 9276 Color: 1
Size: 5832 Color: 1
Size: 1168 Color: 0

Bin 191: 16 of cap free
Amount of items: 3
Items: 
Size: 8096 Color: 1
Size: 7168 Color: 1
Size: 1008 Color: 0

Bin 192: 18 of cap free
Amount of items: 3
Items: 
Size: 13890 Color: 1
Size: 2076 Color: 1
Size: 304 Color: 0

Bin 193: 38 of cap free
Amount of items: 3
Items: 
Size: 10993 Color: 1
Size: 4545 Color: 1
Size: 712 Color: 0

Bin 194: 82 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 1
Size: 5474 Color: 1
Size: 392 Color: 0

Bin 195: 334 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 1
Size: 6046 Color: 1
Size: 592 Color: 0

Bin 196: 593 of cap free
Amount of items: 3
Items: 
Size: 14287 Color: 1
Size: 1152 Color: 0
Size: 256 Color: 1

Bin 197: 2040 of cap free
Amount of items: 3
Items: 
Size: 8164 Color: 1
Size: 4948 Color: 1
Size: 1136 Color: 0

Bin 198: 3445 of cap free
Amount of items: 3
Items: 
Size: 8145 Color: 1
Size: 4122 Color: 1
Size: 576 Color: 0

Bin 199: 9590 of cap free
Amount of items: 3
Items: 
Size: 3656 Color: 1
Size: 2674 Color: 1
Size: 368 Color: 0

Total size: 3225024
Total free space: 16288

