Capicity Bin: 15328
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 7670 Color: 12
Size: 1496 Color: 7
Size: 1492 Color: 2
Size: 1364 Color: 11
Size: 1330 Color: 3
Size: 1272 Color: 14
Size: 704 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8732 Color: 8
Size: 6328 Color: 1
Size: 268 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9262 Color: 13
Size: 5662 Color: 3
Size: 404 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10026 Color: 10
Size: 4422 Color: 10
Size: 880 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10148 Color: 9
Size: 4164 Color: 3
Size: 1016 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 1
Size: 4680 Color: 12
Size: 308 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10500 Color: 9
Size: 4446 Color: 15
Size: 382 Color: 8

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10504 Color: 16
Size: 4440 Color: 3
Size: 384 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10760 Color: 3
Size: 4184 Color: 15
Size: 384 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10924 Color: 5
Size: 4028 Color: 3
Size: 376 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11338 Color: 2
Size: 3582 Color: 5
Size: 408 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11412 Color: 3
Size: 3620 Color: 8
Size: 296 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11476 Color: 16
Size: 2844 Color: 4
Size: 1008 Color: 17

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11560 Color: 9
Size: 2860 Color: 7
Size: 908 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11658 Color: 6
Size: 2394 Color: 17
Size: 1276 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11820 Color: 9
Size: 3268 Color: 11
Size: 240 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11848 Color: 15
Size: 2836 Color: 6
Size: 644 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11870 Color: 13
Size: 2482 Color: 19
Size: 976 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 13
Size: 2152 Color: 1
Size: 1276 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12274 Color: 9
Size: 2486 Color: 15
Size: 568 Color: 5

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 5
Size: 2466 Color: 7
Size: 492 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12386 Color: 13
Size: 2434 Color: 15
Size: 508 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12426 Color: 5
Size: 2442 Color: 1
Size: 460 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12545 Color: 3
Size: 2163 Color: 2
Size: 620 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 13
Size: 2096 Color: 14
Size: 664 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12580 Color: 13
Size: 2488 Color: 6
Size: 260 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 3
Size: 1964 Color: 15
Size: 720 Color: 8

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12658 Color: 4
Size: 2226 Color: 2
Size: 444 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12664 Color: 2
Size: 1388 Color: 11
Size: 1276 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 7
Size: 2028 Color: 15
Size: 592 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12760 Color: 15
Size: 2312 Color: 13
Size: 256 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12865 Color: 1
Size: 1903 Color: 17
Size: 560 Color: 19

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12949 Color: 4
Size: 1983 Color: 16
Size: 396 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 16
Size: 1982 Color: 4
Size: 392 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 16
Size: 1480 Color: 5
Size: 892 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13004 Color: 13
Size: 1412 Color: 7
Size: 912 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13118 Color: 19
Size: 1520 Color: 15
Size: 690 Color: 17

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13148 Color: 1
Size: 1604 Color: 17
Size: 576 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 1
Size: 1264 Color: 2
Size: 776 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13298 Color: 8
Size: 1390 Color: 9
Size: 640 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13366 Color: 3
Size: 1482 Color: 0
Size: 480 Color: 18

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13398 Color: 10
Size: 1610 Color: 16
Size: 320 Color: 15

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 8
Size: 1540 Color: 3
Size: 384 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13490 Color: 19
Size: 1486 Color: 13
Size: 352 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13504 Color: 19
Size: 1428 Color: 8
Size: 396 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 16
Size: 1444 Color: 8
Size: 324 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13598 Color: 11
Size: 1314 Color: 12
Size: 416 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13642 Color: 9
Size: 1302 Color: 1
Size: 384 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 15
Size: 1406 Color: 18
Size: 254 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 3
Size: 928 Color: 18
Size: 680 Color: 6

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 5
Size: 1308 Color: 10
Size: 320 Color: 16

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13734 Color: 17
Size: 842 Color: 12
Size: 752 Color: 11

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 4
Size: 944 Color: 3
Size: 648 Color: 17

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 8531 Color: 6
Size: 6356 Color: 0
Size: 440 Color: 0

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 9251 Color: 4
Size: 5692 Color: 12
Size: 384 Color: 8

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 10301 Color: 18
Size: 4482 Color: 16
Size: 544 Color: 14

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 10312 Color: 12
Size: 4647 Color: 1
Size: 368 Color: 10

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 10757 Color: 10
Size: 3530 Color: 3
Size: 1040 Color: 14

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 11030 Color: 6
Size: 3833 Color: 19
Size: 464 Color: 4

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11352 Color: 2
Size: 3811 Color: 14
Size: 164 Color: 7

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 11511 Color: 13
Size: 2214 Color: 14
Size: 1602 Color: 11

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 12038 Color: 6
Size: 2845 Color: 15
Size: 444 Color: 5

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 3
Size: 3007 Color: 14
Size: 264 Color: 9

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12069 Color: 7
Size: 2422 Color: 6
Size: 836 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12389 Color: 15
Size: 2282 Color: 0
Size: 656 Color: 6

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12663 Color: 2
Size: 2212 Color: 13
Size: 452 Color: 8

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12898 Color: 1
Size: 2221 Color: 3
Size: 208 Color: 10

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 13406 Color: 17
Size: 1921 Color: 6

Bin 69: 2 of cap free
Amount of items: 4
Items: 
Size: 7678 Color: 16
Size: 6378 Color: 18
Size: 1056 Color: 2
Size: 214 Color: 1

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 7752 Color: 15
Size: 6862 Color: 5
Size: 712 Color: 13

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 8538 Color: 12
Size: 6308 Color: 15
Size: 480 Color: 4

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 12
Size: 5062 Color: 13
Size: 1248 Color: 16

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 9720 Color: 2
Size: 5058 Color: 9
Size: 548 Color: 18

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 9851 Color: 11
Size: 5065 Color: 2
Size: 410 Color: 3

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 9922 Color: 12
Size: 5404 Color: 13

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 10662 Color: 16
Size: 3816 Color: 13
Size: 848 Color: 0

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 19
Size: 4024 Color: 12
Size: 268 Color: 16

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11032 Color: 3
Size: 3282 Color: 9
Size: 1012 Color: 16

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11689 Color: 5
Size: 2469 Color: 2
Size: 1168 Color: 4

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 12181 Color: 19
Size: 2661 Color: 3
Size: 484 Color: 14

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 12230 Color: 6
Size: 3096 Color: 16

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12293 Color: 16
Size: 2713 Color: 6
Size: 320 Color: 14

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 8
Size: 2546 Color: 3
Size: 456 Color: 19

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 12479 Color: 3
Size: 2847 Color: 15

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12770 Color: 10
Size: 1920 Color: 3
Size: 636 Color: 19

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 12900 Color: 0
Size: 2426 Color: 4

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 13023 Color: 3
Size: 2303 Color: 10

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 13186 Color: 6
Size: 1132 Color: 3
Size: 1008 Color: 18

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 13484 Color: 0
Size: 1842 Color: 19

Bin 90: 3 of cap free
Amount of items: 11
Items: 
Size: 7665 Color: 13
Size: 1032 Color: 9
Size: 1000 Color: 0
Size: 960 Color: 1
Size: 928 Color: 5
Size: 928 Color: 2
Size: 884 Color: 3
Size: 776 Color: 8
Size: 600 Color: 14
Size: 280 Color: 7
Size: 272 Color: 17

Bin 91: 3 of cap free
Amount of items: 5
Items: 
Size: 7668 Color: 10
Size: 3886 Color: 6
Size: 2577 Color: 3
Size: 706 Color: 1
Size: 488 Color: 11

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 8529 Color: 8
Size: 6364 Color: 2
Size: 432 Color: 12

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 9660 Color: 0
Size: 5665 Color: 18

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 12873 Color: 7
Size: 2188 Color: 13
Size: 264 Color: 11

Bin 95: 3 of cap free
Amount of items: 2
Items: 
Size: 13002 Color: 1
Size: 2323 Color: 19

Bin 96: 3 of cap free
Amount of items: 2
Items: 
Size: 13033 Color: 16
Size: 2292 Color: 10

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 13045 Color: 11
Size: 1272 Color: 3
Size: 1008 Color: 19

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 10018 Color: 9
Size: 4856 Color: 12
Size: 450 Color: 19

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 11179 Color: 8
Size: 4145 Color: 17

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 12418 Color: 5
Size: 2906 Color: 17

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 18
Size: 2026 Color: 4
Size: 704 Color: 13

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 12676 Color: 17
Size: 2648 Color: 16

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 12972 Color: 10
Size: 2352 Color: 2

Bin 104: 4 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 15
Size: 1942 Color: 1

Bin 105: 4 of cap free
Amount of items: 2
Items: 
Size: 13540 Color: 15
Size: 1784 Color: 11

Bin 106: 4 of cap free
Amount of items: 2
Items: 
Size: 13702 Color: 3
Size: 1622 Color: 14

Bin 107: 4 of cap free
Amount of items: 2
Items: 
Size: 13620 Color: 12
Size: 1704 Color: 2

Bin 108: 5 of cap free
Amount of items: 3
Items: 
Size: 11913 Color: 17
Size: 3114 Color: 18
Size: 296 Color: 9

Bin 109: 5 of cap free
Amount of items: 2
Items: 
Size: 13270 Color: 1
Size: 2053 Color: 12

Bin 110: 6 of cap free
Amount of items: 23
Items: 
Size: 880 Color: 11
Size: 856 Color: 7
Size: 842 Color: 3
Size: 832 Color: 10
Size: 824 Color: 1
Size: 800 Color: 19
Size: 800 Color: 18
Size: 800 Color: 13
Size: 766 Color: 2
Size: 760 Color: 13
Size: 720 Color: 17
Size: 704 Color: 10
Size: 608 Color: 0
Size: 606 Color: 19
Size: 584 Color: 3
Size: 576 Color: 15
Size: 576 Color: 10
Size: 568 Color: 2
Size: 568 Color: 1
Size: 542 Color: 14
Size: 542 Color: 8
Size: 288 Color: 15
Size: 280 Color: 8

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 11
Size: 2882 Color: 6
Size: 288 Color: 9

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 12791 Color: 12
Size: 2531 Color: 17

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 16
Size: 1718 Color: 7

Bin 114: 7 of cap free
Amount of items: 7
Items: 
Size: 7667 Color: 5
Size: 1464 Color: 10
Size: 1442 Color: 16
Size: 1358 Color: 9
Size: 1352 Color: 19
Size: 1272 Color: 4
Size: 766 Color: 11

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 9258 Color: 11
Size: 5163 Color: 9
Size: 900 Color: 9

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 11916 Color: 2
Size: 3405 Color: 13

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 12775 Color: 9
Size: 2546 Color: 5

Bin 118: 7 of cap free
Amount of items: 2
Items: 
Size: 13192 Color: 9
Size: 2129 Color: 12

Bin 119: 8 of cap free
Amount of items: 3
Items: 
Size: 8788 Color: 2
Size: 6152 Color: 12
Size: 380 Color: 18

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 13474 Color: 19
Size: 1846 Color: 5

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 9212 Color: 6
Size: 5658 Color: 3
Size: 448 Color: 7

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 12137 Color: 12
Size: 3181 Color: 17

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 12256 Color: 4
Size: 3062 Color: 8

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 18
Size: 2106 Color: 6

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 11858 Color: 12
Size: 3459 Color: 0

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 12627 Color: 1
Size: 2690 Color: 0

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 13
Size: 1993 Color: 8

Bin 128: 12 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 2
Size: 5664 Color: 10
Size: 328 Color: 5

Bin 129: 12 of cap free
Amount of items: 3
Items: 
Size: 10666 Color: 8
Size: 4426 Color: 13
Size: 224 Color: 9

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 13064 Color: 15
Size: 2251 Color: 16

Bin 131: 14 of cap free
Amount of items: 37
Items: 
Size: 536 Color: 9
Size: 530 Color: 10
Size: 528 Color: 5
Size: 524 Color: 0
Size: 512 Color: 19
Size: 504 Color: 4
Size: 496 Color: 3
Size: 496 Color: 1
Size: 492 Color: 14
Size: 492 Color: 14
Size: 484 Color: 12
Size: 484 Color: 10
Size: 462 Color: 5
Size: 440 Color: 17
Size: 440 Color: 17
Size: 432 Color: 3
Size: 432 Color: 2
Size: 424 Color: 18
Size: 424 Color: 7
Size: 420 Color: 6
Size: 400 Color: 9
Size: 398 Color: 11
Size: 392 Color: 16
Size: 392 Color: 8
Size: 368 Color: 8
Size: 360 Color: 5
Size: 352 Color: 2
Size: 348 Color: 0
Size: 340 Color: 17
Size: 336 Color: 8
Size: 336 Color: 4
Size: 324 Color: 1
Size: 320 Color: 11
Size: 304 Color: 7
Size: 304 Color: 6
Size: 296 Color: 11
Size: 192 Color: 0

Bin 132: 14 of cap free
Amount of items: 9
Items: 
Size: 7666 Color: 9
Size: 1264 Color: 5
Size: 1128 Color: 11
Size: 1128 Color: 7
Size: 1088 Color: 10
Size: 1080 Color: 9
Size: 728 Color: 17
Size: 624 Color: 4
Size: 608 Color: 14

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 12102 Color: 15
Size: 3212 Color: 10

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 12410 Color: 18
Size: 2904 Color: 13

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 13550 Color: 1
Size: 1764 Color: 8

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 0
Size: 1548 Color: 6

Bin 137: 15 of cap free
Amount of items: 2
Items: 
Size: 11721 Color: 3
Size: 3592 Color: 8

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 8920 Color: 13
Size: 6392 Color: 8

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 10988 Color: 14
Size: 4324 Color: 11

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 6
Size: 1676 Color: 16

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 19
Size: 1640 Color: 11

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 11094 Color: 15
Size: 4217 Color: 2

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 8542 Color: 12
Size: 6768 Color: 5

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 9858 Color: 4
Size: 5452 Color: 18

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 12802 Color: 1
Size: 2508 Color: 2

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 7
Size: 1546 Color: 14

Bin 147: 20 of cap free
Amount of items: 2
Items: 
Size: 13670 Color: 18
Size: 1638 Color: 6

Bin 148: 21 of cap free
Amount of items: 2
Items: 
Size: 12565 Color: 2
Size: 2742 Color: 8

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 9954 Color: 17
Size: 5352 Color: 0

Bin 150: 24 of cap free
Amount of items: 3
Items: 
Size: 7692 Color: 3
Size: 6956 Color: 0
Size: 656 Color: 13

Bin 151: 26 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 4
Size: 1694 Color: 14
Size: 96 Color: 10

Bin 152: 27 of cap free
Amount of items: 2
Items: 
Size: 11243 Color: 16
Size: 4058 Color: 12

Bin 153: 28 of cap free
Amount of items: 3
Items: 
Size: 9753 Color: 9
Size: 3539 Color: 19
Size: 2008 Color: 5

Bin 154: 28 of cap free
Amount of items: 2
Items: 
Size: 11624 Color: 0
Size: 3676 Color: 7

Bin 155: 28 of cap free
Amount of items: 2
Items: 
Size: 12073 Color: 5
Size: 3227 Color: 15

Bin 156: 30 of cap free
Amount of items: 2
Items: 
Size: 12402 Color: 15
Size: 2896 Color: 13

Bin 157: 30 of cap free
Amount of items: 2
Items: 
Size: 13054 Color: 1
Size: 2244 Color: 17

Bin 158: 31 of cap free
Amount of items: 2
Items: 
Size: 12674 Color: 18
Size: 2623 Color: 15

Bin 159: 31 of cap free
Amount of items: 2
Items: 
Size: 12733 Color: 12
Size: 2564 Color: 8

Bin 160: 32 of cap free
Amount of items: 2
Items: 
Size: 11083 Color: 10
Size: 4213 Color: 5

Bin 161: 32 of cap free
Amount of items: 2
Items: 
Size: 13476 Color: 5
Size: 1820 Color: 13

Bin 162: 33 of cap free
Amount of items: 2
Items: 
Size: 9512 Color: 18
Size: 5783 Color: 8

Bin 163: 34 of cap free
Amount of items: 2
Items: 
Size: 10729 Color: 4
Size: 4565 Color: 5

Bin 164: 34 of cap free
Amount of items: 2
Items: 
Size: 11457 Color: 15
Size: 3837 Color: 16

Bin 165: 35 of cap free
Amount of items: 2
Items: 
Size: 12149 Color: 17
Size: 3144 Color: 13

Bin 166: 36 of cap free
Amount of items: 2
Items: 
Size: 7708 Color: 18
Size: 7584 Color: 5

Bin 167: 36 of cap free
Amount of items: 2
Items: 
Size: 9416 Color: 7
Size: 5876 Color: 4

Bin 168: 36 of cap free
Amount of items: 2
Items: 
Size: 12367 Color: 8
Size: 2925 Color: 6

Bin 169: 36 of cap free
Amount of items: 2
Items: 
Size: 13546 Color: 9
Size: 1746 Color: 15

Bin 170: 38 of cap free
Amount of items: 2
Items: 
Size: 12562 Color: 13
Size: 2728 Color: 5

Bin 171: 40 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 7
Size: 1534 Color: 0

Bin 172: 41 of cap free
Amount of items: 2
Items: 
Size: 10725 Color: 5
Size: 4562 Color: 16

Bin 173: 43 of cap free
Amount of items: 2
Items: 
Size: 12252 Color: 0
Size: 3033 Color: 19

Bin 174: 47 of cap free
Amount of items: 2
Items: 
Size: 13234 Color: 10
Size: 2047 Color: 13

Bin 175: 47 of cap free
Amount of items: 2
Items: 
Size: 13368 Color: 8
Size: 1913 Color: 18

Bin 176: 48 of cap free
Amount of items: 2
Items: 
Size: 10008 Color: 9
Size: 5272 Color: 4

Bin 177: 48 of cap free
Amount of items: 2
Items: 
Size: 11390 Color: 8
Size: 3890 Color: 9

Bin 178: 48 of cap free
Amount of items: 2
Items: 
Size: 13048 Color: 16
Size: 2232 Color: 19

Bin 179: 50 of cap free
Amount of items: 2
Items: 
Size: 12354 Color: 18
Size: 2924 Color: 6

Bin 180: 51 of cap free
Amount of items: 2
Items: 
Size: 10273 Color: 6
Size: 5004 Color: 3

Bin 181: 51 of cap free
Amount of items: 2
Items: 
Size: 12937 Color: 19
Size: 2340 Color: 2

Bin 182: 59 of cap free
Amount of items: 3
Items: 
Size: 9133 Color: 7
Size: 5432 Color: 12
Size: 704 Color: 5

Bin 183: 70 of cap free
Amount of items: 2
Items: 
Size: 11932 Color: 8
Size: 3326 Color: 7

Bin 184: 71 of cap free
Amount of items: 2
Items: 
Size: 12936 Color: 8
Size: 2321 Color: 4

Bin 185: 73 of cap free
Amount of items: 3
Items: 
Size: 10968 Color: 0
Size: 4191 Color: 11
Size: 96 Color: 6

Bin 186: 81 of cap free
Amount of items: 2
Items: 
Size: 8860 Color: 6
Size: 6387 Color: 1

Bin 187: 86 of cap free
Amount of items: 6
Items: 
Size: 7676 Color: 9
Size: 1940 Color: 3
Size: 1898 Color: 8
Size: 1896 Color: 2
Size: 1528 Color: 17
Size: 304 Color: 10

Bin 188: 87 of cap free
Amount of items: 2
Items: 
Size: 12524 Color: 4
Size: 2717 Color: 16

Bin 189: 90 of cap free
Amount of items: 2
Items: 
Size: 12344 Color: 0
Size: 2894 Color: 16

Bin 190: 93 of cap free
Amount of items: 2
Items: 
Size: 11915 Color: 0
Size: 3320 Color: 19

Bin 191: 94 of cap free
Amount of items: 2
Items: 
Size: 11594 Color: 8
Size: 3640 Color: 7

Bin 192: 99 of cap free
Amount of items: 2
Items: 
Size: 8844 Color: 16
Size: 6385 Color: 6

Bin 193: 106 of cap free
Amount of items: 2
Items: 
Size: 10498 Color: 4
Size: 4724 Color: 11

Bin 194: 119 of cap free
Amount of items: 2
Items: 
Size: 10703 Color: 2
Size: 4506 Color: 6

Bin 195: 122 of cap free
Amount of items: 2
Items: 
Size: 8824 Color: 5
Size: 6382 Color: 13

Bin 196: 123 of cap free
Amount of items: 2
Items: 
Size: 10269 Color: 6
Size: 4936 Color: 16

Bin 197: 132 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 13
Size: 6380 Color: 5
Size: 568 Color: 17

Bin 198: 145 of cap free
Amount of items: 5
Items: 
Size: 7672 Color: 14
Size: 2451 Color: 19
Size: 2008 Color: 4
Size: 1980 Color: 15
Size: 1072 Color: 17

Bin 199: 11868 of cap free
Amount of items: 13
Items: 
Size: 304 Color: 13
Size: 296 Color: 17
Size: 288 Color: 14
Size: 288 Color: 6
Size: 280 Color: 19
Size: 280 Color: 18
Size: 280 Color: 16
Size: 272 Color: 13
Size: 272 Color: 7
Size: 260 Color: 4
Size: 256 Color: 14
Size: 192 Color: 11
Size: 192 Color: 2

Total size: 3034944
Total free space: 15328

