Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 351
Size: 371 Color: 350
Size: 258 Color: 81

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 497
Size: 255 Color: 51
Size: 251 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 490
Size: 257 Color: 65
Size: 254 Color: 44

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 377
Size: 339 Color: 291
Size: 273 Color: 163

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 483
Size: 264 Color: 123
Size: 252 Color: 29

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 479
Size: 261 Color: 99
Size: 259 Color: 93

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 391
Size: 302 Color: 243
Size: 292 Color: 226

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 378
Size: 353 Color: 315
Size: 255 Color: 54

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 444
Size: 289 Color: 221
Size: 250 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 454
Size: 281 Color: 193
Size: 253 Color: 39

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 389
Size: 313 Color: 260
Size: 283 Color: 199

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 316
Size: 345 Color: 306
Size: 302 Color: 244

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 476
Size: 269 Color: 145
Size: 251 Color: 21

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 434
Size: 282 Color: 198
Size: 266 Color: 129

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 387
Size: 312 Color: 259
Size: 291 Color: 224

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 450
Size: 283 Color: 201
Size: 253 Color: 37

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 419
Size: 303 Color: 247
Size: 262 Color: 112

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 346
Size: 361 Color: 331
Size: 269 Color: 143

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 420
Size: 315 Color: 264
Size: 250 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 456
Size: 272 Color: 156
Size: 261 Color: 102

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 370
Size: 342 Color: 298
Size: 276 Color: 174

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 430
Size: 294 Color: 230
Size: 258 Color: 76

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 343
Size: 275 Color: 167
Size: 356 Color: 325

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 467
Size: 266 Color: 130
Size: 262 Color: 104

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 368
Size: 344 Color: 303
Size: 277 Color: 178

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 393
Size: 318 Color: 270
Size: 276 Color: 172

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 358
Size: 355 Color: 322
Size: 271 Color: 150

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 482
Size: 265 Color: 128
Size: 253 Color: 40

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 335
Size: 344 Color: 300
Size: 293 Color: 227

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 435
Size: 284 Color: 203
Size: 263 Color: 114

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 403
Size: 325 Color: 281
Size: 259 Color: 91

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 400
Size: 297 Color: 237
Size: 289 Color: 220

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 410
Size: 320 Color: 277
Size: 252 Color: 32

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 339
Size: 328 Color: 284
Size: 306 Color: 252

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 404
Size: 326 Color: 283
Size: 258 Color: 82

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 353
Size: 370 Color: 345
Size: 259 Color: 87

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 375
Size: 341 Color: 297
Size: 273 Color: 159

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 439
Size: 273 Color: 160
Size: 269 Color: 141

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 352
Size: 371 Color: 349
Size: 258 Color: 80

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 330
Size: 331 Color: 289
Size: 308 Color: 253

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 443
Size: 283 Color: 202
Size: 256 Color: 56

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 369
Size: 369 Color: 342
Size: 250 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 471
Size: 272 Color: 154
Size: 252 Color: 33

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 457
Size: 270 Color: 148
Size: 263 Color: 118

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 417
Size: 289 Color: 219
Size: 276 Color: 171

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 372
Size: 316 Color: 266
Size: 300 Color: 242

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 367
Size: 354 Color: 317
Size: 267 Color: 134

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 356
Size: 341 Color: 296
Size: 285 Color: 208

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 360
Size: 323 Color: 280
Size: 302 Color: 246

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 451
Size: 276 Color: 177
Size: 259 Color: 85

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 398
Size: 329 Color: 286
Size: 259 Color: 84

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 442
Size: 278 Color: 184
Size: 262 Color: 109

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 425
Size: 302 Color: 245
Size: 256 Color: 55

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 480
Size: 265 Color: 126
Size: 254 Color: 47

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 461
Size: 276 Color: 173
Size: 256 Color: 61

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 321
Size: 348 Color: 309
Size: 298 Color: 241

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 364
Size: 362 Color: 333
Size: 261 Color: 100

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 431
Size: 293 Color: 229
Size: 258 Color: 83

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 362
Size: 315 Color: 263
Size: 309 Color: 256

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 441
Size: 290 Color: 222
Size: 250 Color: 9

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 460
Size: 276 Color: 176
Size: 256 Color: 58

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 495
Size: 255 Color: 52
Size: 254 Color: 45

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 416
Size: 296 Color: 234
Size: 271 Color: 149

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 481
Size: 262 Color: 111
Size: 256 Color: 60

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 394
Size: 330 Color: 287
Size: 263 Color: 116

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 423
Size: 309 Color: 255
Size: 250 Color: 11

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 446
Size: 278 Color: 182
Size: 260 Color: 97

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 395
Size: 326 Color: 282
Size: 264 Color: 121

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 376
Size: 340 Color: 294
Size: 273 Color: 162

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 415
Size: 319 Color: 271
Size: 250 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 379
Size: 341 Color: 295
Size: 267 Color: 135

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 371
Size: 354 Color: 318
Size: 263 Color: 113

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 449
Size: 274 Color: 164
Size: 262 Color: 106

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 455
Size: 282 Color: 196
Size: 251 Color: 12

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 304
Size: 344 Color: 301
Size: 312 Color: 258

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 484
Size: 266 Color: 132
Size: 250 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 341
Size: 357 Color: 327
Size: 275 Color: 169

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 324
Size: 354 Color: 319
Size: 291 Color: 225

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 366
Size: 360 Color: 328
Size: 261 Color: 101

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 390
Size: 314 Color: 262
Size: 281 Color: 194

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 462
Size: 277 Color: 180
Size: 254 Color: 43

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 445
Size: 285 Color: 205
Size: 254 Color: 46

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 489
Size: 262 Color: 107
Size: 250 Color: 8

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 422
Size: 303 Color: 249
Size: 257 Color: 68

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 496
Size: 257 Color: 67
Size: 251 Color: 22

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 405
Size: 304 Color: 250
Size: 278 Color: 185

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 433
Size: 297 Color: 238
Size: 251 Color: 20

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 380
Size: 355 Color: 323
Size: 252 Color: 27

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 469
Size: 265 Color: 125
Size: 262 Color: 103

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 336
Size: 360 Color: 329
Size: 277 Color: 179

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 337
Size: 362 Color: 332
Size: 274 Color: 166

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 382
Size: 348 Color: 310
Size: 258 Color: 75

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 385
Size: 335 Color: 290
Size: 269 Color: 146

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 411
Size: 320 Color: 276
Size: 252 Color: 34

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 409
Size: 321 Color: 278
Size: 252 Color: 36

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 408
Size: 288 Color: 216
Size: 287 Color: 210

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 438
Size: 274 Color: 165
Size: 272 Color: 158

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 464
Size: 279 Color: 189
Size: 251 Color: 19

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 407
Size: 319 Color: 273
Size: 257 Color: 62

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 427
Size: 289 Color: 218
Size: 266 Color: 133

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 355
Size: 370 Color: 348
Size: 257 Color: 72

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 383
Size: 318 Color: 268
Size: 287 Color: 211

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 357
Size: 357 Color: 326
Size: 269 Color: 144

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 436
Size: 295 Color: 233
Size: 252 Color: 35

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 414
Size: 319 Color: 274
Size: 250 Color: 6

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 499
Size: 251 Color: 23
Size: 251 Color: 14

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 424
Size: 287 Color: 215
Size: 272 Color: 157

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 472
Size: 267 Color: 137
Size: 256 Color: 57

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 448
Size: 276 Color: 170
Size: 262 Color: 105

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 488
Size: 262 Color: 108
Size: 252 Color: 30

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 459
Size: 279 Color: 187
Size: 253 Color: 38

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 465
Size: 271 Color: 153
Size: 258 Color: 79

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 384
Size: 311 Color: 257
Size: 294 Color: 232

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 470
Size: 265 Color: 127
Size: 262 Color: 110

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 397
Size: 296 Color: 236
Size: 293 Color: 228

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 406
Size: 319 Color: 275
Size: 257 Color: 69

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 498
Size: 254 Color: 41
Size: 251 Color: 16

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 426
Size: 305 Color: 251
Size: 252 Color: 28

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 392
Size: 303 Color: 248
Size: 291 Color: 223

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 452
Size: 271 Color: 151
Size: 263 Color: 117

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 492
Size: 255 Color: 49
Size: 255 Color: 48

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 363
Size: 373 Color: 354
Size: 250 Color: 5

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 359
Size: 339 Color: 293
Size: 287 Color: 213

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 453
Size: 277 Color: 181
Size: 257 Color: 66

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 478
Size: 268 Color: 139
Size: 252 Color: 24

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 320
Size: 352 Color: 314
Size: 294 Color: 231

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 466
Size: 271 Color: 152
Size: 258 Color: 78

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 428
Size: 296 Color: 235
Size: 258 Color: 74

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 418
Size: 283 Color: 200
Size: 282 Color: 197

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 491
Size: 261 Color: 98
Size: 250 Color: 4

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 440
Size: 278 Color: 183
Size: 264 Color: 120

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 477
Size: 266 Color: 131
Size: 254 Color: 42

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 388
Size: 314 Color: 261
Size: 282 Color: 195

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 463
Size: 273 Color: 161
Size: 257 Color: 64

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 429
Size: 287 Color: 212
Size: 267 Color: 138

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 401
Size: 318 Color: 269
Size: 267 Color: 136

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 487
Size: 259 Color: 92
Size: 255 Color: 53

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 399
Size: 329 Color: 285
Size: 259 Color: 88

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 447
Size: 287 Color: 214
Size: 251 Color: 17

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 413
Size: 285 Color: 207
Size: 285 Color: 206

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 402
Size: 315 Color: 265
Size: 270 Color: 147

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 381
Size: 348 Color: 311
Size: 258 Color: 73

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 386
Size: 322 Color: 279
Size: 281 Color: 192

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 373
Size: 318 Color: 267
Size: 297 Color: 239

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 347
Size: 370 Color: 344
Size: 260 Color: 94

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 338
Size: 363 Color: 334
Size: 272 Color: 155

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 361
Size: 344 Color: 302
Size: 281 Color: 191

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 313
Size: 298 Color: 240
Size: 351 Color: 312

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 308
Size: 346 Color: 307
Size: 308 Color: 254

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 340
Size: 345 Color: 305
Size: 288 Color: 217

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 365
Size: 343 Color: 299
Size: 279 Color: 186

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 374
Size: 339 Color: 292
Size: 275 Color: 168

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 396
Size: 330 Color: 288
Size: 259 Color: 86

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 421
Size: 284 Color: 204
Size: 279 Color: 188

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 412
Size: 319 Color: 272
Size: 252 Color: 26

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 432
Size: 281 Color: 190
Size: 268 Color: 140

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 437
Size: 286 Color: 209
Size: 260 Color: 95

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 458
Size: 276 Color: 175
Size: 256 Color: 59

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 468
Size: 264 Color: 124
Size: 264 Color: 122

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 473
Size: 263 Color: 119
Size: 258 Color: 77

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 474
Size: 269 Color: 142
Size: 252 Color: 31

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 485
Size: 259 Color: 90
Size: 257 Color: 70

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 475
Size: 263 Color: 115
Size: 257 Color: 63

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 493
Size: 259 Color: 89
Size: 251 Color: 15

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 486
Size: 260 Color: 96
Size: 255 Color: 50

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 494
Size: 257 Color: 71
Size: 252 Color: 25

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 500
Size: 251 Color: 13
Size: 250 Color: 7

Total size: 167000
Total free space: 0

