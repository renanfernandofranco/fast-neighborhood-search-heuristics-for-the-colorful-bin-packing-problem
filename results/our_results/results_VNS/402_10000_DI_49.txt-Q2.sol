Capicity Bin: 7904
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5308 Color: 0
Size: 2444 Color: 1
Size: 152 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5444 Color: 1
Size: 2132 Color: 0
Size: 328 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5972 Color: 0
Size: 1452 Color: 1
Size: 480 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5964 Color: 1
Size: 1620 Color: 0
Size: 320 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6248 Color: 1
Size: 1528 Color: 0
Size: 128 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 0
Size: 1362 Color: 1
Size: 220 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 0
Size: 1322 Color: 1
Size: 228 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 1
Size: 1028 Color: 0
Size: 416 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 0
Size: 742 Color: 0
Size: 658 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 1
Size: 1076 Color: 0
Size: 256 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6583 Color: 1
Size: 1043 Color: 0
Size: 278 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 1
Size: 762 Color: 0
Size: 552 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6691 Color: 1
Size: 805 Color: 1
Size: 408 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 0
Size: 714 Color: 0
Size: 482 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6712 Color: 1
Size: 852 Color: 0
Size: 340 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 0
Size: 842 Color: 0
Size: 336 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 0
Size: 804 Color: 1
Size: 328 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 0
Size: 828 Color: 1
Size: 256 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6906 Color: 1
Size: 850 Color: 0
Size: 148 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 0
Size: 668 Color: 1
Size: 268 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 0
Size: 894 Color: 1
Size: 38 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6936 Color: 1
Size: 560 Color: 0
Size: 408 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6994 Color: 0
Size: 478 Color: 0
Size: 432 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6984 Color: 1
Size: 880 Color: 0
Size: 40 Color: 1

Bin 25: 1 of cap free
Amount of items: 7
Items: 
Size: 3957 Color: 1
Size: 778 Color: 0
Size: 776 Color: 1
Size: 767 Color: 1
Size: 765 Color: 0
Size: 600 Color: 0
Size: 260 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 4479 Color: 1
Size: 3200 Color: 1
Size: 224 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 6233 Color: 0
Size: 1654 Color: 0
Size: 16 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 1
Size: 1303 Color: 1
Size: 208 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 6431 Color: 1
Size: 1080 Color: 0
Size: 392 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 1
Size: 985 Color: 0
Size: 418 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6525 Color: 1
Size: 834 Color: 1
Size: 544 Color: 0

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 6939 Color: 0
Size: 964 Color: 1

Bin 33: 2 of cap free
Amount of items: 5
Items: 
Size: 3958 Color: 0
Size: 1324 Color: 1
Size: 1316 Color: 1
Size: 1004 Color: 0
Size: 300 Color: 1

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 4942 Color: 1
Size: 1510 Color: 0
Size: 1450 Color: 1

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 5000 Color: 1
Size: 2742 Color: 0
Size: 160 Color: 1

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 5334 Color: 1
Size: 2424 Color: 0
Size: 144 Color: 1

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 6720 Color: 1
Size: 1182 Color: 0

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 6834 Color: 0
Size: 684 Color: 1
Size: 384 Color: 1

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 0
Size: 2385 Color: 0
Size: 184 Color: 1

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 5880 Color: 0
Size: 1845 Color: 1
Size: 176 Color: 1

Bin 41: 3 of cap free
Amount of items: 4
Items: 
Size: 6053 Color: 1
Size: 1632 Color: 0
Size: 152 Color: 0
Size: 64 Color: 1

Bin 42: 3 of cap free
Amount of items: 2
Items: 
Size: 6875 Color: 0
Size: 1026 Color: 1

Bin 43: 3 of cap free
Amount of items: 2
Items: 
Size: 6907 Color: 0
Size: 994 Color: 1

Bin 44: 3 of cap free
Amount of items: 2
Items: 
Size: 6987 Color: 1
Size: 914 Color: 0

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 3960 Color: 1
Size: 3284 Color: 1
Size: 656 Color: 0

Bin 46: 4 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 0
Size: 3082 Color: 1
Size: 334 Color: 0

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 5748 Color: 0
Size: 2044 Color: 1
Size: 108 Color: 1

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 6166 Color: 1
Size: 1734 Color: 0

Bin 49: 4 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 1
Size: 1294 Color: 1
Size: 144 Color: 0

Bin 50: 4 of cap free
Amount of items: 2
Items: 
Size: 6948 Color: 1
Size: 952 Color: 0

Bin 51: 4 of cap free
Amount of items: 2
Items: 
Size: 7108 Color: 1
Size: 792 Color: 0

Bin 52: 5 of cap free
Amount of items: 4
Items: 
Size: 3964 Color: 0
Size: 2052 Color: 0
Size: 1681 Color: 1
Size: 202 Color: 1

Bin 53: 5 of cap free
Amount of items: 3
Items: 
Size: 4924 Color: 0
Size: 2655 Color: 0
Size: 320 Color: 1

Bin 54: 5 of cap free
Amount of items: 3
Items: 
Size: 5011 Color: 0
Size: 2656 Color: 0
Size: 232 Color: 1

Bin 55: 5 of cap free
Amount of items: 3
Items: 
Size: 6341 Color: 1
Size: 990 Color: 0
Size: 568 Color: 1

Bin 56: 5 of cap free
Amount of items: 2
Items: 
Size: 6723 Color: 1
Size: 1176 Color: 0

Bin 57: 5 of cap free
Amount of items: 2
Items: 
Size: 6748 Color: 1
Size: 1151 Color: 0

Bin 58: 6 of cap free
Amount of items: 3
Items: 
Size: 5462 Color: 0
Size: 2084 Color: 0
Size: 352 Color: 1

Bin 59: 6 of cap free
Amount of items: 2
Items: 
Size: 6718 Color: 0
Size: 1180 Color: 1

Bin 60: 6 of cap free
Amount of items: 2
Items: 
Size: 6898 Color: 0
Size: 1000 Color: 1

Bin 61: 6 of cap free
Amount of items: 2
Items: 
Size: 7018 Color: 0
Size: 880 Color: 1

Bin 62: 7 of cap free
Amount of items: 2
Items: 
Size: 6781 Color: 1
Size: 1116 Color: 0

Bin 63: 8 of cap free
Amount of items: 2
Items: 
Size: 5412 Color: 0
Size: 2484 Color: 1

Bin 64: 8 of cap free
Amount of items: 2
Items: 
Size: 5732 Color: 1
Size: 2164 Color: 0

Bin 65: 8 of cap free
Amount of items: 3
Items: 
Size: 5842 Color: 0
Size: 2038 Color: 0
Size: 16 Color: 1

Bin 66: 8 of cap free
Amount of items: 2
Items: 
Size: 6094 Color: 0
Size: 1802 Color: 1

Bin 67: 9 of cap free
Amount of items: 2
Items: 
Size: 6986 Color: 0
Size: 909 Color: 1

Bin 68: 10 of cap free
Amount of items: 2
Items: 
Size: 5746 Color: 1
Size: 2148 Color: 0

Bin 69: 10 of cap free
Amount of items: 2
Items: 
Size: 6886 Color: 0
Size: 1008 Color: 1

Bin 70: 11 of cap free
Amount of items: 2
Items: 
Size: 6985 Color: 0
Size: 908 Color: 1

Bin 71: 12 of cap free
Amount of items: 8
Items: 
Size: 3956 Color: 1
Size: 656 Color: 1
Size: 656 Color: 1
Size: 656 Color: 1
Size: 656 Color: 0
Size: 560 Color: 0
Size: 488 Color: 0
Size: 264 Color: 0

Bin 72: 12 of cap free
Amount of items: 2
Items: 
Size: 6088 Color: 1
Size: 1804 Color: 0

Bin 73: 12 of cap free
Amount of items: 2
Items: 
Size: 6620 Color: 1
Size: 1272 Color: 0

Bin 74: 12 of cap free
Amount of items: 3
Items: 
Size: 6913 Color: 0
Size: 891 Color: 1
Size: 88 Color: 0

Bin 75: 12 of cap free
Amount of items: 2
Items: 
Size: 7086 Color: 0
Size: 806 Color: 1

Bin 76: 12 of cap free
Amount of items: 2
Items: 
Size: 7084 Color: 1
Size: 808 Color: 0

Bin 77: 13 of cap free
Amount of items: 2
Items: 
Size: 5407 Color: 1
Size: 2484 Color: 0

Bin 78: 13 of cap free
Amount of items: 2
Items: 
Size: 6790 Color: 1
Size: 1101 Color: 0

Bin 79: 13 of cap free
Amount of items: 2
Items: 
Size: 6837 Color: 0
Size: 1054 Color: 1

Bin 80: 13 of cap free
Amount of items: 2
Items: 
Size: 6880 Color: 1
Size: 1011 Color: 0

Bin 81: 14 of cap free
Amount of items: 9
Items: 
Size: 3954 Color: 1
Size: 564 Color: 1
Size: 496 Color: 1
Size: 488 Color: 1
Size: 488 Color: 0
Size: 480 Color: 1
Size: 476 Color: 0
Size: 476 Color: 0
Size: 468 Color: 0

Bin 82: 17 of cap free
Amount of items: 2
Items: 
Size: 5043 Color: 1
Size: 2844 Color: 0

Bin 83: 17 of cap free
Amount of items: 2
Items: 
Size: 5476 Color: 0
Size: 2411 Color: 1

Bin 84: 18 of cap free
Amount of items: 2
Items: 
Size: 6062 Color: 1
Size: 1824 Color: 0

Bin 85: 18 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 0
Size: 1612 Color: 1

Bin 86: 18 of cap free
Amount of items: 2
Items: 
Size: 6938 Color: 1
Size: 948 Color: 0

Bin 87: 19 of cap free
Amount of items: 2
Items: 
Size: 4591 Color: 1
Size: 3294 Color: 0

Bin 88: 20 of cap free
Amount of items: 2
Items: 
Size: 6810 Color: 0
Size: 1074 Color: 1

Bin 89: 20 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 0
Size: 834 Color: 1

Bin 90: 21 of cap free
Amount of items: 2
Items: 
Size: 6490 Color: 1
Size: 1393 Color: 0

Bin 91: 22 of cap free
Amount of items: 3
Items: 
Size: 5290 Color: 0
Size: 1812 Color: 1
Size: 780 Color: 0

Bin 92: 22 of cap free
Amount of items: 2
Items: 
Size: 6653 Color: 1
Size: 1229 Color: 0

Bin 93: 24 of cap free
Amount of items: 2
Items: 
Size: 6676 Color: 0
Size: 1204 Color: 1

Bin 94: 25 of cap free
Amount of items: 11
Items: 
Size: 3953 Color: 0
Size: 480 Color: 1
Size: 432 Color: 1
Size: 424 Color: 1
Size: 424 Color: 0
Size: 416 Color: 1
Size: 368 Color: 1
Size: 368 Color: 0
Size: 360 Color: 0
Size: 356 Color: 0
Size: 298 Color: 0

Bin 95: 26 of cap free
Amount of items: 2
Items: 
Size: 6156 Color: 1
Size: 1722 Color: 0

Bin 96: 30 of cap free
Amount of items: 2
Items: 
Size: 5380 Color: 0
Size: 2494 Color: 1

Bin 97: 30 of cap free
Amount of items: 2
Items: 
Size: 6776 Color: 0
Size: 1098 Color: 1

Bin 98: 32 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 1
Size: 2081 Color: 0
Size: 859 Color: 0

Bin 99: 37 of cap free
Amount of items: 2
Items: 
Size: 5472 Color: 0
Size: 2395 Color: 1

Bin 100: 38 of cap free
Amount of items: 2
Items: 
Size: 5673 Color: 1
Size: 2193 Color: 0

Bin 101: 44 of cap free
Amount of items: 3
Items: 
Size: 4980 Color: 0
Size: 2792 Color: 1
Size: 88 Color: 1

Bin 102: 45 of cap free
Amount of items: 2
Items: 
Size: 5889 Color: 1
Size: 1970 Color: 0

Bin 103: 50 of cap free
Amount of items: 34
Items: 
Size: 404 Color: 1
Size: 332 Color: 1
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 272 Color: 1
Size: 260 Color: 0
Size: 256 Color: 1
Size: 244 Color: 1
Size: 244 Color: 1
Size: 240 Color: 0
Size: 240 Color: 0
Size: 240 Color: 0
Size: 232 Color: 1
Size: 224 Color: 1
Size: 216 Color: 0
Size: 216 Color: 0
Size: 212 Color: 0
Size: 208 Color: 1
Size: 208 Color: 1
Size: 208 Color: 1
Size: 200 Color: 1
Size: 196 Color: 1
Size: 196 Color: 1
Size: 192 Color: 0
Size: 192 Color: 0
Size: 186 Color: 0
Size: 184 Color: 0
Size: 176 Color: 0
Size: 176 Color: 0
Size: 176 Color: 0
Size: 84 Color: 1

Bin 104: 51 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 0
Size: 937 Color: 1

Bin 105: 52 of cap free
Amount of items: 2
Items: 
Size: 6164 Color: 0
Size: 1688 Color: 1

Bin 106: 52 of cap free
Amount of items: 2
Items: 
Size: 6468 Color: 1
Size: 1384 Color: 0

Bin 107: 58 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 1
Size: 2099 Color: 1
Size: 827 Color: 0

Bin 108: 59 of cap free
Amount of items: 2
Items: 
Size: 5387 Color: 1
Size: 2458 Color: 0

Bin 109: 60 of cap free
Amount of items: 2
Items: 
Size: 6642 Color: 0
Size: 1202 Color: 1

Bin 110: 70 of cap free
Amount of items: 2
Items: 
Size: 6618 Color: 1
Size: 1216 Color: 0

Bin 111: 71 of cap free
Amount of items: 2
Items: 
Size: 6332 Color: 0
Size: 1501 Color: 1

Bin 112: 72 of cap free
Amount of items: 2
Items: 
Size: 4976 Color: 0
Size: 2856 Color: 1

Bin 113: 77 of cap free
Amount of items: 2
Items: 
Size: 5691 Color: 0
Size: 2136 Color: 1

Bin 114: 84 of cap free
Amount of items: 2
Items: 
Size: 5352 Color: 0
Size: 2468 Color: 1

Bin 115: 84 of cap free
Amount of items: 2
Items: 
Size: 6616 Color: 1
Size: 1204 Color: 0

Bin 116: 86 of cap free
Amount of items: 2
Items: 
Size: 5922 Color: 0
Size: 1896 Color: 1

Bin 117: 90 of cap free
Amount of items: 2
Items: 
Size: 6105 Color: 1
Size: 1709 Color: 0

Bin 118: 94 of cap free
Amount of items: 2
Items: 
Size: 4958 Color: 0
Size: 2852 Color: 1

Bin 119: 105 of cap free
Amount of items: 2
Items: 
Size: 4618 Color: 1
Size: 3181 Color: 0

Bin 120: 112 of cap free
Amount of items: 2
Items: 
Size: 4500 Color: 1
Size: 3292 Color: 0

Bin 121: 112 of cap free
Amount of items: 2
Items: 
Size: 5031 Color: 1
Size: 2761 Color: 0

Bin 122: 113 of cap free
Amount of items: 2
Items: 
Size: 4498 Color: 0
Size: 3293 Color: 1

Bin 123: 114 of cap free
Amount of items: 2
Items: 
Size: 4948 Color: 0
Size: 2842 Color: 1

Bin 124: 116 of cap free
Amount of items: 2
Items: 
Size: 5606 Color: 1
Size: 2182 Color: 0

Bin 125: 126 of cap free
Amount of items: 2
Items: 
Size: 4488 Color: 1
Size: 3290 Color: 0

Bin 126: 126 of cap free
Amount of items: 2
Items: 
Size: 4490 Color: 0
Size: 3288 Color: 1

Bin 127: 126 of cap free
Amount of items: 2
Items: 
Size: 5348 Color: 1
Size: 2430 Color: 0

Bin 128: 128 of cap free
Amount of items: 2
Items: 
Size: 6316 Color: 0
Size: 1460 Color: 1

Bin 129: 132 of cap free
Amount of items: 2
Items: 
Size: 4301 Color: 1
Size: 3471 Color: 0

Bin 130: 132 of cap free
Amount of items: 2
Items: 
Size: 4769 Color: 1
Size: 3003 Color: 0

Bin 131: 132 of cap free
Amount of items: 2
Items: 
Size: 5640 Color: 0
Size: 2132 Color: 1

Bin 132: 140 of cap free
Amount of items: 2
Items: 
Size: 6884 Color: 0
Size: 880 Color: 1

Bin 133: 4200 of cap free
Amount of items: 23
Items: 
Size: 192 Color: 1
Size: 192 Color: 1
Size: 180 Color: 1
Size: 176 Color: 1
Size: 176 Color: 0
Size: 170 Color: 1
Size: 168 Color: 1
Size: 168 Color: 0
Size: 164 Color: 1
Size: 164 Color: 1
Size: 164 Color: 0
Size: 160 Color: 0
Size: 160 Color: 0
Size: 160 Color: 0
Size: 160 Color: 0
Size: 152 Color: 0
Size: 152 Color: 0
Size: 144 Color: 0
Size: 144 Color: 0
Size: 142 Color: 0
Size: 140 Color: 1
Size: 140 Color: 1
Size: 136 Color: 1

Total size: 1043328
Total free space: 7904

