Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 4
Size: 1022 Color: 1
Size: 196 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 0
Size: 822 Color: 3
Size: 164 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 0
Size: 818 Color: 1
Size: 102 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 1
Size: 446 Color: 0
Size: 364 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 3
Size: 658 Color: 1
Size: 136 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 2
Size: 684 Color: 3
Size: 40 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 4
Size: 542 Color: 1
Size: 38 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 4
Size: 275 Color: 1
Size: 259 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1943 Color: 0
Size: 441 Color: 2
Size: 88 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1985 Color: 3
Size: 407 Color: 1
Size: 80 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 2
Size: 334 Color: 4
Size: 104 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 4
Size: 361 Color: 1
Size: 72 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2101 Color: 0
Size: 283 Color: 1
Size: 88 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 4
Size: 268 Color: 2
Size: 88 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 4
Size: 204 Color: 1
Size: 150 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2179 Color: 1
Size: 245 Color: 0
Size: 48 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2201 Color: 1
Size: 233 Color: 2
Size: 38 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 0
Size: 200 Color: 1
Size: 60 Color: 3

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 0
Size: 1022 Color: 1
Size: 42 Color: 4

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 4
Size: 889 Color: 1
Size: 88 Color: 3

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1529 Color: 0
Size: 838 Color: 2
Size: 104 Color: 4

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1540 Color: 1
Size: 931 Color: 4

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 4
Size: 678 Color: 0
Size: 64 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 3
Size: 662 Color: 0
Size: 72 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 1
Size: 546 Color: 4
Size: 128 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1855 Color: 3
Size: 556 Color: 1
Size: 60 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 2
Size: 475 Color: 1
Size: 62 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 0
Size: 515 Color: 4
Size: 16 Color: 3

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 2030 Color: 0
Size: 387 Color: 1
Size: 54 Color: 4

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 2163 Color: 3
Size: 176 Color: 4
Size: 132 Color: 1

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 3
Size: 892 Color: 1
Size: 108 Color: 3

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1828 Color: 0
Size: 576 Color: 4
Size: 66 Color: 3

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 0
Size: 384 Color: 4
Size: 50 Color: 2

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 3
Size: 204 Color: 1
Size: 156 Color: 4

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 2198 Color: 0
Size: 272 Color: 4

Bin 36: 2 of cap free
Amount of items: 4
Items: 
Size: 2158 Color: 4
Size: 272 Color: 0
Size: 32 Color: 1
Size: 8 Color: 4

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 1
Size: 670 Color: 0
Size: 200 Color: 3

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 0
Size: 739 Color: 3
Size: 72 Color: 1

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 2019 Color: 4
Size: 450 Color: 3

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 0
Size: 787 Color: 1
Size: 94 Color: 2

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 2143 Color: 0
Size: 317 Color: 4
Size: 8 Color: 3

Bin 42: 5 of cap free
Amount of items: 4
Items: 
Size: 1237 Color: 2
Size: 846 Color: 0
Size: 262 Color: 1
Size: 122 Color: 2

Bin 43: 5 of cap free
Amount of items: 4
Items: 
Size: 1387 Color: 0
Size: 540 Color: 2
Size: 444 Color: 2
Size: 96 Color: 1

Bin 44: 5 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 4
Size: 729 Color: 2
Size: 52 Color: 1

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 4
Size: 333 Color: 4
Size: 316 Color: 1

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 0
Size: 621 Color: 1
Size: 20 Color: 3

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 1652 Color: 4
Size: 758 Color: 0
Size: 56 Color: 2

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 1903 Color: 4
Size: 563 Color: 2

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 2164 Color: 2
Size: 302 Color: 4

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 2093 Color: 1
Size: 370 Color: 4

Bin 51: 9 of cap free
Amount of items: 3
Items: 
Size: 2073 Color: 4
Size: 366 Color: 0
Size: 24 Color: 0

Bin 52: 10 of cap free
Amount of items: 2
Items: 
Size: 2151 Color: 1
Size: 311 Color: 4

Bin 53: 11 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 2
Size: 1000 Color: 3
Size: 220 Color: 3

Bin 54: 11 of cap free
Amount of items: 4
Items: 
Size: 1246 Color: 2
Size: 1103 Color: 4
Size: 72 Color: 1
Size: 40 Color: 4

Bin 55: 14 of cap free
Amount of items: 4
Items: 
Size: 1238 Color: 1
Size: 604 Color: 0
Size: 504 Color: 3
Size: 112 Color: 2

Bin 56: 14 of cap free
Amount of items: 4
Items: 
Size: 1244 Color: 3
Size: 1028 Color: 1
Size: 146 Color: 0
Size: 40 Color: 4

Bin 57: 16 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 4
Size: 1010 Color: 3
Size: 184 Color: 1

Bin 58: 16 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 0
Size: 830 Color: 1
Size: 168 Color: 4

Bin 59: 17 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 0
Size: 840 Color: 2
Size: 44 Color: 4

Bin 60: 17 of cap free
Amount of items: 2
Items: 
Size: 1704 Color: 0
Size: 751 Color: 3

Bin 61: 22 of cap free
Amount of items: 2
Items: 
Size: 1357 Color: 4
Size: 1093 Color: 2

Bin 62: 22 of cap free
Amount of items: 2
Items: 
Size: 1670 Color: 2
Size: 780 Color: 4

Bin 63: 38 of cap free
Amount of items: 2
Items: 
Size: 1404 Color: 2
Size: 1030 Color: 3

Bin 64: 39 of cap free
Amount of items: 2
Items: 
Size: 1478 Color: 4
Size: 955 Color: 3

Bin 65: 57 of cap free
Amount of items: 12
Items: 
Size: 693 Color: 2
Size: 230 Color: 0
Size: 176 Color: 1
Size: 164 Color: 3
Size: 164 Color: 1
Size: 160 Color: 2
Size: 152 Color: 2
Size: 144 Color: 4
Size: 142 Color: 4
Size: 132 Color: 0
Size: 130 Color: 0
Size: 128 Color: 1

Bin 66: 2066 of cap free
Amount of items: 4
Items: 
Size: 132 Color: 4
Size: 120 Color: 0
Size: 88 Color: 2
Size: 66 Color: 1

Total size: 160680
Total free space: 2472

