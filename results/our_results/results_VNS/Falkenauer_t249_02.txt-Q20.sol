Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 5
Size: 282 Color: 13
Size: 257 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7
Size: 333 Color: 9
Size: 290 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 16
Size: 346 Color: 4
Size: 287 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 4
Size: 262 Color: 9
Size: 254 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 16
Size: 319 Color: 4
Size: 267 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 9
Size: 343 Color: 15
Size: 250 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 14
Size: 353 Color: 3
Size: 290 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 17
Size: 373 Color: 5
Size: 253 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 12
Size: 332 Color: 4
Size: 254 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 14
Size: 299 Color: 4
Size: 271 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 12
Size: 259 Color: 14
Size: 253 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 290 Color: 10
Size: 279 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 18
Size: 333 Color: 16
Size: 270 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 339 Color: 4
Size: 286 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 2
Size: 272 Color: 10
Size: 269 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7
Size: 350 Color: 17
Size: 259 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 0
Size: 356 Color: 12
Size: 271 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 10
Size: 281 Color: 4
Size: 260 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 18
Size: 301 Color: 16
Size: 326 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 9
Size: 372 Color: 0
Size: 255 Color: 16

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 19
Size: 339 Color: 7
Size: 254 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 11
Size: 288 Color: 12
Size: 255 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 16
Size: 296 Color: 0
Size: 271 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 281 Color: 2
Size: 256 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 5
Size: 324 Color: 18
Size: 311 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 4
Size: 302 Color: 18
Size: 257 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 16
Size: 345 Color: 11
Size: 250 Color: 8

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 2
Size: 287 Color: 3
Size: 280 Color: 7

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 15
Size: 283 Color: 14
Size: 264 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 366 Color: 5
Size: 257 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 19
Size: 340 Color: 0
Size: 273 Color: 8

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 17
Size: 345 Color: 15
Size: 258 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 4
Size: 255 Color: 11
Size: 251 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 17
Size: 368 Color: 4
Size: 258 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 10
Size: 290 Color: 15
Size: 286 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 0
Size: 263 Color: 18
Size: 256 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 10
Size: 294 Color: 7
Size: 263 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 8
Size: 322 Color: 10
Size: 304 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 15
Size: 319 Color: 2
Size: 271 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 19
Size: 363 Color: 18
Size: 269 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 329 Color: 15
Size: 252 Color: 5

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 13
Size: 256 Color: 0
Size: 254 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 17
Size: 288 Color: 19
Size: 264 Color: 8

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 11
Size: 303 Color: 16
Size: 263 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 15
Size: 252 Color: 19
Size: 252 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 15
Size: 356 Color: 14
Size: 250 Color: 7

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 13
Size: 264 Color: 18
Size: 259 Color: 17

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 315 Color: 4
Size: 311 Color: 10

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 15
Size: 307 Color: 6
Size: 272 Color: 5

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 5
Size: 263 Color: 13
Size: 253 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 5
Size: 285 Color: 15
Size: 257 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 6
Size: 357 Color: 6
Size: 253 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 4
Size: 335 Color: 2
Size: 278 Color: 12

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 14
Size: 300 Color: 4
Size: 250 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 329 Color: 8
Size: 270 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 6
Size: 269 Color: 6
Size: 265 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 11
Size: 324 Color: 13
Size: 314 Color: 7

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 6
Size: 335 Color: 6
Size: 274 Color: 17

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 323 Color: 5
Size: 285 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 317 Color: 3
Size: 300 Color: 14

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 11
Size: 318 Color: 15
Size: 281 Color: 15

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 11
Size: 264 Color: 6
Size: 260 Color: 17

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 14
Size: 329 Color: 15
Size: 281 Color: 18

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 13
Size: 311 Color: 7
Size: 308 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 8
Size: 286 Color: 12
Size: 269 Color: 13

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 5
Size: 281 Color: 1
Size: 250 Color: 18

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 5
Size: 350 Color: 13
Size: 260 Color: 18

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 16
Size: 310 Color: 2
Size: 287 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 3
Size: 278 Color: 8
Size: 255 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 297 Color: 13
Size: 260 Color: 9

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 19
Size: 351 Color: 7
Size: 265 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 9
Size: 256 Color: 4
Size: 250 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 274 Color: 1
Size: 270 Color: 9

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 8
Size: 280 Color: 11
Size: 271 Color: 16

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 5
Size: 349 Color: 11
Size: 259 Color: 13

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 324 Color: 0
Size: 280 Color: 16

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 11
Size: 298 Color: 17
Size: 281 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 3
Size: 346 Color: 16
Size: 285 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 9
Size: 311 Color: 10
Size: 276 Color: 19

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 362 Color: 0
Size: 256 Color: 15

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 292 Color: 13
Size: 266 Color: 6

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 2
Size: 360 Color: 11
Size: 273 Color: 16

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 8
Size: 261 Color: 3
Size: 252 Color: 0

Total size: 83000
Total free space: 0

