Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 255 Color: 0
Size: 312 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 279 Color: 1
Size: 271 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 266 Color: 0
Size: 299 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 339 Color: 1
Size: 272 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 314 Color: 1
Size: 250 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 254 Color: 1
Size: 251 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 341 Color: 1
Size: 268 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 372 Color: 1
Size: 255 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 329 Color: 1
Size: 266 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 302 Color: 1
Size: 256 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 270 Color: 1
Size: 260 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 304 Color: 1
Size: 256 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 274 Color: 1
Size: 254 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 337 Color: 1
Size: 268 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 309 Color: 1
Size: 267 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 321 Color: 1
Size: 286 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 301 Color: 1
Size: 261 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 286 Color: 1
Size: 252 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 304 Color: 1
Size: 276 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 352 Color: 1
Size: 281 Color: 0

Total size: 20000
Total free space: 0

