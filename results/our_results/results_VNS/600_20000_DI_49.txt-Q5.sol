Capicity Bin: 14912
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 7462 Color: 1
Size: 1456 Color: 2
Size: 1380 Color: 4
Size: 1322 Color: 2
Size: 1308 Color: 0
Size: 1008 Color: 3
Size: 976 Color: 3

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 7458 Color: 0
Size: 1744 Color: 2
Size: 1682 Color: 3
Size: 1470 Color: 0
Size: 992 Color: 1
Size: 792 Color: 1
Size: 774 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9236 Color: 4
Size: 5372 Color: 2
Size: 304 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9808 Color: 4
Size: 4680 Color: 3
Size: 424 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9944 Color: 0
Size: 4040 Color: 0
Size: 928 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 1
Size: 4088 Color: 3
Size: 816 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10072 Color: 0
Size: 4524 Color: 3
Size: 316 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10224 Color: 3
Size: 4272 Color: 4
Size: 416 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10257 Color: 1
Size: 3141 Color: 0
Size: 1514 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10558 Color: 3
Size: 4098 Color: 0
Size: 256 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10733 Color: 3
Size: 2337 Color: 4
Size: 1842 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10693 Color: 1
Size: 3847 Color: 2
Size: 372 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 10960 Color: 2
Size: 3608 Color: 3
Size: 344 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11330 Color: 3
Size: 2986 Color: 1
Size: 596 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11416 Color: 3
Size: 3208 Color: 4
Size: 288 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11746 Color: 0
Size: 2940 Color: 4
Size: 226 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11784 Color: 3
Size: 2358 Color: 4
Size: 770 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11874 Color: 4
Size: 2850 Color: 1
Size: 188 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11970 Color: 3
Size: 2586 Color: 4
Size: 356 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12086 Color: 4
Size: 1586 Color: 2
Size: 1240 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 2
Size: 2304 Color: 4
Size: 456 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12158 Color: 4
Size: 2122 Color: 1
Size: 632 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12203 Color: 4
Size: 2225 Color: 0
Size: 484 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 3
Size: 2424 Color: 3
Size: 288 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12304 Color: 4
Size: 1368 Color: 2
Size: 1240 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 2
Size: 2264 Color: 3
Size: 324 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12352 Color: 4
Size: 2160 Color: 2
Size: 400 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12368 Color: 2
Size: 2240 Color: 0
Size: 304 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12388 Color: 0
Size: 1894 Color: 4
Size: 630 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12457 Color: 4
Size: 1383 Color: 0
Size: 1072 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12492 Color: 4
Size: 1492 Color: 2
Size: 928 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12562 Color: 4
Size: 2066 Color: 1
Size: 284 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12642 Color: 4
Size: 1734 Color: 0
Size: 536 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12660 Color: 4
Size: 1604 Color: 2
Size: 648 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12680 Color: 4
Size: 1448 Color: 1
Size: 784 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12716 Color: 3
Size: 1888 Color: 4
Size: 308 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12770 Color: 4
Size: 1768 Color: 2
Size: 374 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12803 Color: 4
Size: 1661 Color: 3
Size: 448 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 2
Size: 1776 Color: 4
Size: 328 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 2
Size: 1416 Color: 4
Size: 624 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12946 Color: 4
Size: 1324 Color: 2
Size: 642 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12985 Color: 4
Size: 1597 Color: 3
Size: 330 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12977 Color: 3
Size: 1757 Color: 2
Size: 178 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 4
Size: 1664 Color: 1
Size: 260 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13010 Color: 0
Size: 1656 Color: 3
Size: 246 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 2
Size: 976 Color: 0
Size: 876 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13086 Color: 3
Size: 1522 Color: 2
Size: 304 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13121 Color: 2
Size: 1493 Color: 3
Size: 298 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13133 Color: 3
Size: 1427 Color: 0
Size: 352 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 0
Size: 976 Color: 4
Size: 786 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 1
Size: 1424 Color: 3
Size: 328 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13034 Color: 4
Size: 1374 Color: 3
Size: 504 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 0
Size: 1464 Color: 3
Size: 272 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 3
Size: 896 Color: 2
Size: 816 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 0
Size: 1240 Color: 3
Size: 448 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13234 Color: 0
Size: 1402 Color: 1
Size: 276 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13260 Color: 0
Size: 1152 Color: 4
Size: 500 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13266 Color: 0
Size: 1302 Color: 4
Size: 344 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 1
Size: 1072 Color: 4
Size: 516 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13354 Color: 3
Size: 1294 Color: 0
Size: 264 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13370 Color: 2
Size: 1286 Color: 2
Size: 256 Color: 4

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 8327 Color: 3
Size: 6216 Color: 1
Size: 368 Color: 4

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10194 Color: 4
Size: 4381 Color: 0
Size: 336 Color: 2

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 10233 Color: 3
Size: 4084 Color: 2
Size: 594 Color: 0

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 10297 Color: 4
Size: 4342 Color: 0
Size: 272 Color: 4

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 10738 Color: 3
Size: 3901 Color: 1
Size: 272 Color: 1

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11057 Color: 3
Size: 2436 Color: 0
Size: 1418 Color: 2

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 11428 Color: 4
Size: 3483 Color: 0

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 11440 Color: 0
Size: 2977 Color: 4
Size: 494 Color: 3

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 11692 Color: 0
Size: 2123 Color: 4
Size: 1096 Color: 0

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12003 Color: 2
Size: 2212 Color: 2
Size: 696 Color: 4

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12314 Color: 4
Size: 2153 Color: 3
Size: 444 Color: 1

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 12599 Color: 0
Size: 1948 Color: 4
Size: 364 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 12663 Color: 0
Size: 1960 Color: 4
Size: 288 Color: 3

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 12751 Color: 0
Size: 1216 Color: 4
Size: 944 Color: 3

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 12834 Color: 3
Size: 1759 Color: 0
Size: 318 Color: 4

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 12867 Color: 3
Size: 1444 Color: 3
Size: 600 Color: 4

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 13201 Color: 0
Size: 1642 Color: 3
Size: 68 Color: 2

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 7996 Color: 1
Size: 6210 Color: 4
Size: 704 Color: 1

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 10516 Color: 0
Size: 3162 Color: 3
Size: 1232 Color: 4

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 10602 Color: 0
Size: 3920 Color: 3
Size: 388 Color: 2

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 11118 Color: 1
Size: 3536 Color: 3
Size: 256 Color: 4

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 1
Size: 3342 Color: 2
Size: 404 Color: 2

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 11466 Color: 4
Size: 2348 Color: 2
Size: 1096 Color: 3

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12553 Color: 0
Size: 1525 Color: 2
Size: 832 Color: 4

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 3
Size: 2166 Color: 2

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 3
Size: 1786 Color: 0

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 9642 Color: 3
Size: 4883 Color: 0
Size: 384 Color: 2

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 9657 Color: 0
Size: 5060 Color: 4
Size: 192 Color: 2

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 4
Size: 3525 Color: 3
Size: 320 Color: 1

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 11161 Color: 0
Size: 3372 Color: 3
Size: 376 Color: 4

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 11425 Color: 2
Size: 2684 Color: 4
Size: 800 Color: 3

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 11721 Color: 4
Size: 2908 Color: 1
Size: 280 Color: 1

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 11909 Color: 4
Size: 2616 Color: 1
Size: 384 Color: 0

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 11943 Color: 1
Size: 2454 Color: 4
Size: 512 Color: 2

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 12080 Color: 4
Size: 2299 Color: 1
Size: 530 Color: 1

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 12434 Color: 2
Size: 2475 Color: 0

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 13330 Color: 3
Size: 1547 Color: 4
Size: 32 Color: 2

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 7528 Color: 2
Size: 6980 Color: 2
Size: 400 Color: 3

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 9053 Color: 3
Size: 5487 Color: 1
Size: 368 Color: 0

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 10193 Color: 0
Size: 4347 Color: 4
Size: 368 Color: 3

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 10868 Color: 4
Size: 3312 Color: 2
Size: 728 Color: 3

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 11628 Color: 1
Size: 2896 Color: 4
Size: 384 Color: 3

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 11297 Color: 2
Size: 3482 Color: 1
Size: 128 Color: 1

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 12243 Color: 3
Size: 2664 Color: 1

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 13253 Color: 0
Size: 1654 Color: 4

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 10012 Color: 2
Size: 4894 Color: 4

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 12481 Color: 0
Size: 2425 Color: 1

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 13348 Color: 1
Size: 1548 Color: 2
Size: 10 Color: 0

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 13362 Color: 2
Size: 1512 Color: 0
Size: 32 Color: 3

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 11121 Color: 3
Size: 2968 Color: 4
Size: 816 Color: 2

Bin 112: 7 of cap free
Amount of items: 2
Items: 
Size: 11664 Color: 0
Size: 3241 Color: 1

Bin 113: 8 of cap free
Amount of items: 11
Items: 
Size: 7460 Color: 4
Size: 1096 Color: 2
Size: 868 Color: 3
Size: 864 Color: 2
Size: 778 Color: 1
Size: 768 Color: 3
Size: 768 Color: 2
Size: 702 Color: 1
Size: 640 Color: 0
Size: 576 Color: 0
Size: 384 Color: 2

Bin 114: 8 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 4
Size: 6214 Color: 3
Size: 368 Color: 0

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 8456 Color: 0
Size: 6168 Color: 2
Size: 280 Color: 3

Bin 116: 8 of cap free
Amount of items: 3
Items: 
Size: 8468 Color: 2
Size: 6016 Color: 1
Size: 420 Color: 2

Bin 117: 8 of cap free
Amount of items: 3
Items: 
Size: 9376 Color: 4
Size: 5136 Color: 3
Size: 392 Color: 4

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 12627 Color: 1
Size: 2276 Color: 3

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 12936 Color: 1
Size: 1967 Color: 0

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 7476 Color: 4
Size: 6184 Color: 4
Size: 1242 Color: 0

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 12260 Color: 2
Size: 2642 Color: 0

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 12894 Color: 4
Size: 2008 Color: 0

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 11720 Color: 0
Size: 3181 Color: 3

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 12155 Color: 3
Size: 2746 Color: 1

Bin 125: 11 of cap free
Amount of items: 3
Items: 
Size: 12305 Color: 2
Size: 2444 Color: 1
Size: 152 Color: 4

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 9360 Color: 4
Size: 5540 Color: 3

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 11980 Color: 2
Size: 2920 Color: 1

Bin 128: 13 of cap free
Amount of items: 3
Items: 
Size: 8272 Color: 4
Size: 6211 Color: 0
Size: 416 Color: 1

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 11145 Color: 0
Size: 3754 Color: 4

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 12832 Color: 2
Size: 2067 Color: 3

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 13098 Color: 4
Size: 1801 Color: 1

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 12706 Color: 1
Size: 2192 Color: 2

Bin 133: 15 of cap free
Amount of items: 3
Items: 
Size: 9051 Color: 3
Size: 5494 Color: 0
Size: 352 Color: 1

Bin 134: 15 of cap free
Amount of items: 2
Items: 
Size: 11016 Color: 4
Size: 3881 Color: 1

Bin 135: 16 of cap free
Amount of items: 3
Items: 
Size: 9304 Color: 1
Size: 5336 Color: 3
Size: 256 Color: 2

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 13232 Color: 1
Size: 1664 Color: 2

Bin 137: 17 of cap free
Amount of items: 5
Items: 
Size: 7464 Color: 4
Size: 2345 Color: 0
Size: 1816 Color: 4
Size: 1704 Color: 2
Size: 1566 Color: 1

Bin 138: 17 of cap free
Amount of items: 3
Items: 
Size: 12921 Color: 2
Size: 1864 Color: 0
Size: 110 Color: 0

Bin 139: 17 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 2
Size: 1607 Color: 1

Bin 140: 19 of cap free
Amount of items: 3
Items: 
Size: 8329 Color: 0
Size: 6204 Color: 1
Size: 360 Color: 3

Bin 141: 19 of cap free
Amount of items: 2
Items: 
Size: 13057 Color: 1
Size: 1836 Color: 3

Bin 142: 19 of cap free
Amount of items: 2
Items: 
Size: 13188 Color: 2
Size: 1705 Color: 1

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 2
Size: 2312 Color: 0

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 1
Size: 2108 Color: 3

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 12930 Color: 3
Size: 1962 Color: 2

Bin 146: 20 of cap free
Amount of items: 2
Items: 
Size: 13112 Color: 0
Size: 1780 Color: 1

Bin 147: 22 of cap free
Amount of items: 3
Items: 
Size: 8784 Color: 4
Size: 3933 Color: 3
Size: 2173 Color: 1

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 13214 Color: 0
Size: 1676 Color: 1

Bin 149: 24 of cap free
Amount of items: 3
Items: 
Size: 10584 Color: 4
Size: 4092 Color: 1
Size: 212 Color: 4

Bin 150: 24 of cap free
Amount of items: 2
Items: 
Size: 10704 Color: 4
Size: 4184 Color: 0

Bin 151: 24 of cap free
Amount of items: 2
Items: 
Size: 12504 Color: 2
Size: 2384 Color: 1

Bin 152: 27 of cap free
Amount of items: 2
Items: 
Size: 12329 Color: 3
Size: 2556 Color: 0

Bin 153: 28 of cap free
Amount of items: 2
Items: 
Size: 10906 Color: 0
Size: 3978 Color: 1

Bin 154: 28 of cap free
Amount of items: 2
Items: 
Size: 11023 Color: 1
Size: 3861 Color: 4

Bin 155: 30 of cap free
Amount of items: 2
Items: 
Size: 11810 Color: 1
Size: 3072 Color: 3

Bin 156: 31 of cap free
Amount of items: 2
Items: 
Size: 11757 Color: 0
Size: 3124 Color: 2

Bin 157: 31 of cap free
Amount of items: 2
Items: 
Size: 12997 Color: 3
Size: 1884 Color: 1

Bin 158: 34 of cap free
Amount of items: 2
Items: 
Size: 11960 Color: 0
Size: 2918 Color: 1

Bin 159: 35 of cap free
Amount of items: 3
Items: 
Size: 12008 Color: 1
Size: 2733 Color: 0
Size: 136 Color: 1

Bin 160: 36 of cap free
Amount of items: 3
Items: 
Size: 8844 Color: 0
Size: 5712 Color: 2
Size: 320 Color: 1

Bin 161: 36 of cap free
Amount of items: 2
Items: 
Size: 9492 Color: 2
Size: 5384 Color: 4

Bin 162: 37 of cap free
Amount of items: 3
Items: 
Size: 9046 Color: 0
Size: 5489 Color: 2
Size: 340 Color: 1

Bin 163: 37 of cap free
Amount of items: 2
Items: 
Size: 12848 Color: 3
Size: 2027 Color: 0

Bin 164: 38 of cap free
Amount of items: 2
Items: 
Size: 9706 Color: 4
Size: 5168 Color: 1

Bin 165: 38 of cap free
Amount of items: 2
Items: 
Size: 11618 Color: 4
Size: 3256 Color: 0

Bin 166: 39 of cap free
Amount of items: 2
Items: 
Size: 12370 Color: 0
Size: 2503 Color: 3

Bin 167: 40 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 3
Size: 2256 Color: 1

Bin 168: 41 of cap free
Amount of items: 2
Items: 
Size: 12240 Color: 0
Size: 2631 Color: 3

Bin 169: 42 of cap free
Amount of items: 2
Items: 
Size: 10142 Color: 0
Size: 4728 Color: 2

Bin 170: 46 of cap free
Amount of items: 2
Items: 
Size: 12568 Color: 1
Size: 2298 Color: 2

Bin 171: 47 of cap free
Amount of items: 2
Items: 
Size: 11852 Color: 0
Size: 3013 Color: 1

Bin 172: 49 of cap free
Amount of items: 3
Items: 
Size: 9697 Color: 0
Size: 3934 Color: 2
Size: 1232 Color: 3

Bin 173: 53 of cap free
Amount of items: 3
Items: 
Size: 8326 Color: 0
Size: 6213 Color: 3
Size: 320 Color: 1

Bin 174: 54 of cap free
Amount of items: 2
Items: 
Size: 11341 Color: 1
Size: 3517 Color: 0

Bin 175: 60 of cap free
Amount of items: 2
Items: 
Size: 13083 Color: 2
Size: 1769 Color: 0

Bin 176: 63 of cap free
Amount of items: 2
Items: 
Size: 12188 Color: 2
Size: 2661 Color: 0

Bin 177: 66 of cap free
Amount of items: 2
Items: 
Size: 10452 Color: 1
Size: 4394 Color: 2

Bin 178: 66 of cap free
Amount of items: 2
Items: 
Size: 11633 Color: 0
Size: 3213 Color: 1

Bin 179: 68 of cap free
Amount of items: 2
Items: 
Size: 12108 Color: 0
Size: 2736 Color: 3

Bin 180: 74 of cap free
Amount of items: 2
Items: 
Size: 9948 Color: 4
Size: 4890 Color: 0

Bin 181: 75 of cap free
Amount of items: 2
Items: 
Size: 12365 Color: 2
Size: 2472 Color: 0

Bin 182: 76 of cap free
Amount of items: 2
Items: 
Size: 12908 Color: 3
Size: 1928 Color: 2

Bin 183: 84 of cap free
Amount of items: 2
Items: 
Size: 10304 Color: 4
Size: 4524 Color: 3

Bin 184: 88 of cap free
Amount of items: 2
Items: 
Size: 11368 Color: 3
Size: 3456 Color: 2

Bin 185: 97 of cap free
Amount of items: 4
Items: 
Size: 7472 Color: 3
Size: 3560 Color: 3
Size: 2907 Color: 4
Size: 876 Color: 4

Bin 186: 106 of cap free
Amount of items: 2
Items: 
Size: 9042 Color: 0
Size: 5764 Color: 4

Bin 187: 112 of cap free
Amount of items: 2
Items: 
Size: 10648 Color: 4
Size: 4152 Color: 2

Bin 188: 112 of cap free
Amount of items: 2
Items: 
Size: 12780 Color: 1
Size: 2020 Color: 2

Bin 189: 131 of cap free
Amount of items: 2
Items: 
Size: 9896 Color: 4
Size: 4885 Color: 1

Bin 190: 137 of cap free
Amount of items: 13
Items: 
Size: 7457 Color: 3
Size: 720 Color: 4
Size: 692 Color: 1
Size: 672 Color: 3
Size: 672 Color: 1
Size: 664 Color: 3
Size: 640 Color: 1
Size: 640 Color: 1
Size: 634 Color: 3
Size: 602 Color: 2
Size: 480 Color: 0
Size: 472 Color: 0
Size: 430 Color: 0

Bin 191: 147 of cap free
Amount of items: 2
Items: 
Size: 11097 Color: 4
Size: 3668 Color: 1

Bin 192: 163 of cap free
Amount of items: 4
Items: 
Size: 7496 Color: 0
Size: 3161 Color: 3
Size: 2217 Color: 1
Size: 1875 Color: 2

Bin 193: 168 of cap free
Amount of items: 2
Items: 
Size: 8520 Color: 3
Size: 6224 Color: 4

Bin 194: 178 of cap free
Amount of items: 2
Items: 
Size: 10281 Color: 3
Size: 4453 Color: 1

Bin 195: 182 of cap free
Amount of items: 2
Items: 
Size: 9240 Color: 3
Size: 5490 Color: 2

Bin 196: 233 of cap free
Amount of items: 2
Items: 
Size: 9655 Color: 3
Size: 5024 Color: 2

Bin 197: 236 of cap free
Amount of items: 31
Items: 
Size: 626 Color: 4
Size: 580 Color: 4
Size: 576 Color: 2
Size: 576 Color: 2
Size: 576 Color: 1
Size: 548 Color: 1
Size: 546 Color: 1
Size: 544 Color: 3
Size: 528 Color: 3
Size: 528 Color: 2
Size: 524 Color: 4
Size: 524 Color: 4
Size: 512 Color: 1
Size: 488 Color: 4
Size: 488 Color: 3
Size: 480 Color: 4
Size: 480 Color: 2
Size: 468 Color: 2
Size: 458 Color: 1
Size: 456 Color: 2
Size: 448 Color: 2
Size: 448 Color: 1
Size: 440 Color: 3
Size: 440 Color: 3
Size: 416 Color: 0
Size: 352 Color: 0
Size: 350 Color: 0
Size: 336 Color: 0
Size: 320 Color: 0
Size: 320 Color: 0
Size: 300 Color: 0

Bin 198: 241 of cap free
Amount of items: 8
Items: 
Size: 7461 Color: 4
Size: 1232 Color: 4
Size: 1216 Color: 3
Size: 1096 Color: 2
Size: 1056 Color: 0
Size: 976 Color: 1
Size: 832 Color: 0
Size: 802 Color: 1

Bin 199: 10354 of cap free
Amount of items: 14
Items: 
Size: 434 Color: 4
Size: 432 Color: 1
Size: 412 Color: 3
Size: 360 Color: 2
Size: 336 Color: 2
Size: 320 Color: 4
Size: 320 Color: 3
Size: 312 Color: 1
Size: 296 Color: 1
Size: 292 Color: 3
Size: 276 Color: 0
Size: 256 Color: 2
Size: 256 Color: 0
Size: 256 Color: 0

Total size: 2952576
Total free space: 14912

