Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 42
Size: 341 Color: 37
Size: 270 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 51
Size: 304 Color: 29
Size: 260 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 47
Size: 312 Color: 31
Size: 268 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 49
Size: 299 Color: 25
Size: 268 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 55
Size: 279 Color: 21
Size: 271 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 43
Size: 337 Color: 35
Size: 272 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 321 Color: 33
Size: 286 Color: 24
Size: 393 Color: 44

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 59
Size: 255 Color: 6
Size: 250 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 48
Size: 309 Color: 30
Size: 267 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 50
Size: 314 Color: 32
Size: 251 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 41
Size: 372 Color: 40
Size: 255 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 53
Size: 304 Color: 28
Size: 256 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 52
Size: 301 Color: 26
Size: 261 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 54
Size: 302 Color: 27
Size: 256 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 56
Size: 286 Color: 23
Size: 252 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 46
Size: 329 Color: 34
Size: 266 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 58
Size: 274 Color: 19
Size: 254 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 45
Size: 339 Color: 36
Size: 266 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 57
Size: 276 Color: 20
Size: 254 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 39
Size: 352 Color: 38
Size: 281 Color: 22

Total size: 20000
Total free space: 0

