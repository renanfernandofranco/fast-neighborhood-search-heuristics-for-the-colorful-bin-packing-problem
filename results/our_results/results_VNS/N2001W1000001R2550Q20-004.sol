Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 365232 Color: 1
Size: 321399 Color: 19
Size: 313370 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 365621 Color: 8
Size: 344902 Color: 9
Size: 289478 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 368993 Color: 9
Size: 329633 Color: 10
Size: 301375 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 368291 Color: 16
Size: 343185 Color: 12
Size: 288525 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370038 Color: 4
Size: 323017 Color: 17
Size: 306946 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 370140 Color: 0
Size: 354271 Color: 16
Size: 275590 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 370686 Color: 17
Size: 360264 Color: 18
Size: 269051 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 370973 Color: 6
Size: 363954 Color: 2
Size: 265074 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 372744 Color: 0
Size: 352875 Color: 4
Size: 274382 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 373343 Color: 0
Size: 360477 Color: 5
Size: 266181 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 375208 Color: 19
Size: 363173 Color: 3
Size: 261620 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 375459 Color: 15
Size: 339648 Color: 6
Size: 284894 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 375481 Color: 10
Size: 344870 Color: 5
Size: 279650 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 376632 Color: 19
Size: 366941 Color: 13
Size: 256428 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 376990 Color: 10
Size: 371143 Color: 1
Size: 251868 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 377180 Color: 15
Size: 326976 Color: 14
Size: 295845 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 377615 Color: 8
Size: 334635 Color: 5
Size: 287751 Color: 12

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 377927 Color: 5
Size: 323260 Color: 17
Size: 298814 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 378683 Color: 5
Size: 323579 Color: 15
Size: 297739 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 378980 Color: 10
Size: 360004 Color: 5
Size: 261017 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 379045 Color: 10
Size: 346482 Color: 17
Size: 274474 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 379268 Color: 5
Size: 361467 Color: 0
Size: 259266 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 381547 Color: 0
Size: 347833 Color: 5
Size: 270621 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 381984 Color: 0
Size: 355549 Color: 10
Size: 262468 Color: 7

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 381043 Color: 14
Size: 321612 Color: 2
Size: 297346 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 381189 Color: 16
Size: 318636 Color: 5
Size: 300176 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 381471 Color: 3
Size: 315379 Color: 18
Size: 303151 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 381896 Color: 16
Size: 342369 Color: 5
Size: 275736 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 382223 Color: 1
Size: 334090 Color: 10
Size: 283688 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 383939 Color: 4
Size: 339928 Color: 6
Size: 276134 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 384016 Color: 4
Size: 364847 Color: 8
Size: 251138 Color: 19

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 384860 Color: 17
Size: 340499 Color: 19
Size: 274642 Color: 17

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 384919 Color: 1
Size: 315600 Color: 11
Size: 299482 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 385732 Color: 14
Size: 363867 Color: 7
Size: 250402 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 386060 Color: 6
Size: 343582 Color: 9
Size: 270359 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 386122 Color: 6
Size: 339467 Color: 8
Size: 274412 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 386234 Color: 8
Size: 313470 Color: 5
Size: 300297 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 386719 Color: 9
Size: 318798 Color: 4
Size: 294484 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 387903 Color: 0
Size: 342191 Color: 12
Size: 269907 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 386849 Color: 18
Size: 331994 Color: 10
Size: 281158 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 387004 Color: 4
Size: 339445 Color: 19
Size: 273552 Color: 9

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 387186 Color: 17
Size: 328077 Color: 12
Size: 284738 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 387201 Color: 7
Size: 311088 Color: 16
Size: 301712 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 388537 Color: 0
Size: 361317 Color: 18
Size: 250147 Color: 19

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 387374 Color: 17
Size: 318926 Color: 15
Size: 293701 Color: 15

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 387548 Color: 7
Size: 359182 Color: 9
Size: 253271 Color: 16

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 389209 Color: 0
Size: 316395 Color: 15
Size: 294397 Color: 9

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 389050 Color: 8
Size: 325599 Color: 19
Size: 285352 Color: 19

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 389099 Color: 3
Size: 341921 Color: 11
Size: 268981 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 389344 Color: 5
Size: 359235 Color: 8
Size: 251422 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 389680 Color: 14
Size: 320164 Color: 3
Size: 290157 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 390365 Color: 2
Size: 329799 Color: 6
Size: 279837 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 390596 Color: 1
Size: 346860 Color: 2
Size: 262545 Color: 5

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 390602 Color: 16
Size: 308936 Color: 8
Size: 300463 Color: 15

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 391521 Color: 0
Size: 356650 Color: 10
Size: 251830 Color: 15

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 391631 Color: 0
Size: 345362 Color: 10
Size: 263008 Color: 19

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 391694 Color: 14
Size: 323833 Color: 9
Size: 284474 Color: 18

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 391803 Color: 7
Size: 323346 Color: 7
Size: 284852 Color: 18

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 391995 Color: 1
Size: 322798 Color: 18
Size: 285208 Color: 16

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 392368 Color: 7
Size: 354656 Color: 10
Size: 252977 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 393463 Color: 18
Size: 317555 Color: 0
Size: 288983 Color: 7

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 393632 Color: 15
Size: 342454 Color: 16
Size: 263915 Color: 18

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 393694 Color: 1
Size: 330572 Color: 15
Size: 275735 Color: 11

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 393746 Color: 10
Size: 320391 Color: 13
Size: 285864 Color: 6

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 394755 Color: 9
Size: 334669 Color: 4
Size: 270577 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 395433 Color: 15
Size: 305820 Color: 6
Size: 298748 Color: 18

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 396028 Color: 15
Size: 352787 Color: 9
Size: 251186 Color: 10

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 396135 Color: 16
Size: 350362 Color: 8
Size: 253504 Color: 18

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 396807 Color: 1
Size: 326332 Color: 0
Size: 276862 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 396835 Color: 7
Size: 326888 Color: 12
Size: 276278 Color: 17

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 397098 Color: 10
Size: 339596 Color: 13
Size: 263307 Color: 18

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 397381 Color: 8
Size: 349495 Color: 17
Size: 253125 Color: 14

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 397485 Color: 1
Size: 312228 Color: 18
Size: 290288 Color: 14

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 397911 Color: 16
Size: 324955 Color: 2
Size: 277135 Color: 10

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 398105 Color: 10
Size: 310943 Color: 4
Size: 290953 Color: 15

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 398123 Color: 8
Size: 334117 Color: 13
Size: 267761 Color: 10

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 398839 Color: 9
Size: 315693 Color: 10
Size: 285469 Color: 17

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 398665 Color: 17
Size: 337782 Color: 19
Size: 263554 Color: 12

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 398821 Color: 17
Size: 325096 Color: 10
Size: 276084 Color: 8

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 399518 Color: 1
Size: 347009 Color: 11
Size: 253474 Color: 9

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 399720 Color: 2
Size: 314746 Color: 1
Size: 285535 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 399778 Color: 18
Size: 344238 Color: 18
Size: 255985 Color: 19

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 400401 Color: 15
Size: 302922 Color: 2
Size: 296678 Color: 18

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 400434 Color: 1
Size: 326389 Color: 5
Size: 273178 Color: 16

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 400780 Color: 15
Size: 321056 Color: 7
Size: 278165 Color: 18

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 400814 Color: 7
Size: 320760 Color: 16
Size: 278427 Color: 13

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 401264 Color: 11
Size: 329078 Color: 16
Size: 269659 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 401944 Color: 1
Size: 339398 Color: 19
Size: 258659 Color: 6

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 402320 Color: 18
Size: 305555 Color: 1
Size: 292126 Color: 17

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 403110 Color: 7
Size: 342246 Color: 3
Size: 254645 Color: 14

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 403801 Color: 18
Size: 305480 Color: 3
Size: 290720 Color: 7

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 404283 Color: 3
Size: 307981 Color: 6
Size: 287737 Color: 12

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 404641 Color: 3
Size: 331988 Color: 18
Size: 263372 Color: 17

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 405023 Color: 18
Size: 313742 Color: 18
Size: 281236 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 405629 Color: 11
Size: 329246 Color: 7
Size: 265126 Color: 10

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 405754 Color: 3
Size: 309120 Color: 4
Size: 285127 Color: 19

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 406484 Color: 7
Size: 330390 Color: 4
Size: 263127 Color: 12

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 406577 Color: 11
Size: 327826 Color: 0
Size: 265598 Color: 15

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 406421 Color: 10
Size: 331110 Color: 8
Size: 262470 Color: 11

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 408081 Color: 11
Size: 315887 Color: 7
Size: 276033 Color: 12

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 408426 Color: 10
Size: 327570 Color: 4
Size: 264005 Color: 19

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 408796 Color: 18
Size: 325943 Color: 12
Size: 265262 Color: 13

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 409910 Color: 7
Size: 301713 Color: 3
Size: 288378 Color: 19

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 410139 Color: 12
Size: 326274 Color: 6
Size: 263588 Color: 16

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 410870 Color: 19
Size: 300848 Color: 5
Size: 288283 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 411043 Color: 1
Size: 309909 Color: 0
Size: 279049 Color: 12

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 411221 Color: 15
Size: 331815 Color: 10
Size: 256965 Color: 12

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 411267 Color: 1
Size: 322183 Color: 4
Size: 266551 Color: 14

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 411428 Color: 7
Size: 317459 Color: 18
Size: 271114 Color: 3

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 411507 Color: 16
Size: 326756 Color: 8
Size: 261738 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 411785 Color: 12
Size: 320289 Color: 7
Size: 267927 Color: 6

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 411839 Color: 7
Size: 296300 Color: 15
Size: 291862 Color: 5

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 412546 Color: 5
Size: 327710 Color: 17
Size: 259745 Color: 17

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 412628 Color: 1
Size: 297458 Color: 13
Size: 289915 Color: 8

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 412866 Color: 4
Size: 312812 Color: 5
Size: 274323 Color: 7

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 412977 Color: 2
Size: 333161 Color: 2
Size: 253863 Color: 19

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 413410 Color: 18
Size: 315533 Color: 3
Size: 271058 Color: 17

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 413619 Color: 1
Size: 319346 Color: 14
Size: 267036 Color: 12

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 413673 Color: 1
Size: 293736 Color: 9
Size: 292592 Color: 16

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 413863 Color: 19
Size: 335104 Color: 7
Size: 251034 Color: 5

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 413950 Color: 0
Size: 321512 Color: 19
Size: 264539 Color: 5

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 414084 Color: 14
Size: 314163 Color: 18
Size: 271754 Color: 19

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 414235 Color: 12
Size: 318264 Color: 10
Size: 267502 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 414240 Color: 6
Size: 319757 Color: 3
Size: 266004 Color: 19

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 414309 Color: 12
Size: 326764 Color: 7
Size: 258928 Color: 7

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 415408 Color: 17
Size: 314778 Color: 13
Size: 269815 Color: 8

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 416100 Color: 3
Size: 315769 Color: 13
Size: 268132 Color: 6

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 416532 Color: 14
Size: 305083 Color: 15
Size: 278386 Color: 11

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 416696 Color: 3
Size: 320619 Color: 14
Size: 262686 Color: 12

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 416743 Color: 11
Size: 316209 Color: 15
Size: 267049 Color: 18

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 416807 Color: 11
Size: 320501 Color: 6
Size: 262693 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 417113 Color: 0
Size: 326544 Color: 3
Size: 256344 Color: 15

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 418924 Color: 9
Size: 316523 Color: 10
Size: 264554 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 417455 Color: 8
Size: 316437 Color: 0
Size: 266109 Color: 17

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 417574 Color: 7
Size: 319347 Color: 18
Size: 263080 Color: 17

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 418945 Color: 9
Size: 301211 Color: 7
Size: 279845 Color: 8

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 418363 Color: 7
Size: 300088 Color: 0
Size: 281550 Color: 12

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 418425 Color: 10
Size: 321171 Color: 4
Size: 260405 Color: 12

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 418684 Color: 3
Size: 314607 Color: 1
Size: 266710 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 420120 Color: 9
Size: 301198 Color: 13
Size: 278683 Color: 10

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 419141 Color: 13
Size: 295252 Color: 14
Size: 285608 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 419623 Color: 16
Size: 325432 Color: 18
Size: 254946 Color: 10

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 420263 Color: 8
Size: 300245 Color: 5
Size: 279493 Color: 16

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 420334 Color: 11
Size: 308700 Color: 4
Size: 270967 Color: 6

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 420802 Color: 1
Size: 313873 Color: 6
Size: 265326 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 420871 Color: 19
Size: 318979 Color: 11
Size: 260151 Color: 17

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 422222 Color: 9
Size: 308349 Color: 19
Size: 269430 Color: 14

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 421757 Color: 10
Size: 323272 Color: 8
Size: 254972 Color: 7

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 421842 Color: 18
Size: 293005 Color: 14
Size: 285154 Color: 18

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 421969 Color: 16
Size: 303751 Color: 5
Size: 274281 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 422223 Color: 2
Size: 290361 Color: 15
Size: 287417 Color: 11

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 422317 Color: 18
Size: 295993 Color: 17
Size: 281691 Color: 2

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 422377 Color: 8
Size: 313841 Color: 5
Size: 263783 Color: 6

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 422505 Color: 10
Size: 323399 Color: 18
Size: 254097 Color: 10

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 422673 Color: 14
Size: 301563 Color: 16
Size: 275765 Color: 4

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 422881 Color: 12
Size: 317846 Color: 13
Size: 259274 Color: 19

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 423015 Color: 15
Size: 304118 Color: 17
Size: 272868 Color: 19

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 423189 Color: 16
Size: 317582 Color: 18
Size: 259230 Color: 19

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 424203 Color: 16
Size: 291060 Color: 1
Size: 284738 Color: 2

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 424383 Color: 8
Size: 305036 Color: 19
Size: 270582 Color: 18

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 424570 Color: 3
Size: 310993 Color: 17
Size: 264438 Color: 11

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 424670 Color: 10
Size: 303188 Color: 13
Size: 272143 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 424797 Color: 7
Size: 310736 Color: 6
Size: 264468 Color: 9

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 424875 Color: 0
Size: 320050 Color: 4
Size: 255076 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 424914 Color: 11
Size: 288495 Color: 13
Size: 286592 Color: 14

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 425004 Color: 5
Size: 314396 Color: 1
Size: 260601 Color: 7

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 425454 Color: 7
Size: 322249 Color: 2
Size: 252298 Color: 19

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 425980 Color: 5
Size: 312297 Color: 19
Size: 261724 Color: 17

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 426035 Color: 7
Size: 322196 Color: 17
Size: 251770 Color: 13

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 426100 Color: 18
Size: 312241 Color: 14
Size: 261660 Color: 17

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 426672 Color: 11
Size: 294817 Color: 1
Size: 278512 Color: 19

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 426695 Color: 12
Size: 320758 Color: 1
Size: 252548 Color: 10

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 427345 Color: 2
Size: 314221 Color: 13
Size: 258435 Color: 10

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 427881 Color: 16
Size: 316009 Color: 17
Size: 256111 Color: 3

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 428643 Color: 1
Size: 300638 Color: 3
Size: 270720 Color: 5

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 429807 Color: 2
Size: 285325 Color: 14
Size: 284869 Color: 13

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 430651 Color: 7
Size: 311409 Color: 3
Size: 257941 Color: 10

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 430782 Color: 12
Size: 300017 Color: 11
Size: 269202 Color: 10

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 430792 Color: 16
Size: 311043 Color: 14
Size: 258166 Color: 14

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 431044 Color: 8
Size: 300622 Color: 2
Size: 268335 Color: 7

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 431140 Color: 19
Size: 300967 Color: 13
Size: 267894 Color: 4

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 431212 Color: 13
Size: 285542 Color: 11
Size: 283247 Color: 6

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 431508 Color: 9
Size: 311387 Color: 17
Size: 257106 Color: 17

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 431418 Color: 13
Size: 290410 Color: 17
Size: 278173 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 431586 Color: 12
Size: 287728 Color: 11
Size: 280687 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 431622 Color: 19
Size: 295975 Color: 19
Size: 272404 Color: 13

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 431740 Color: 5
Size: 313103 Color: 12
Size: 255158 Color: 18

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 432196 Color: 8
Size: 301807 Color: 19
Size: 265998 Color: 11

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 432264 Color: 4
Size: 284739 Color: 17
Size: 282998 Color: 17

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 432312 Color: 7
Size: 288698 Color: 5
Size: 278991 Color: 16

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 432455 Color: 8
Size: 296610 Color: 17
Size: 270936 Color: 4

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 432681 Color: 3
Size: 286106 Color: 13
Size: 281214 Color: 5

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 432960 Color: 18
Size: 306424 Color: 9
Size: 260617 Color: 7

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 433053 Color: 10
Size: 311544 Color: 12
Size: 255404 Color: 11

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 433136 Color: 10
Size: 292991 Color: 6
Size: 273874 Color: 19

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 433432 Color: 2
Size: 294732 Color: 12
Size: 271837 Color: 5

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 433742 Color: 0
Size: 289016 Color: 1
Size: 277243 Color: 18

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 434102 Color: 9
Size: 308512 Color: 7
Size: 257387 Color: 12

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 435276 Color: 14
Size: 308951 Color: 8
Size: 255774 Color: 5

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 435339 Color: 11
Size: 300748 Color: 7
Size: 263914 Color: 17

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 435666 Color: 19
Size: 304160 Color: 6
Size: 260175 Color: 6

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 435919 Color: 19
Size: 312160 Color: 2
Size: 251922 Color: 7

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 436440 Color: 1
Size: 300312 Color: 15
Size: 263249 Color: 8

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 436839 Color: 16
Size: 295915 Color: 6
Size: 267247 Color: 13

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 436943 Color: 13
Size: 306684 Color: 11
Size: 256374 Color: 12

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 437127 Color: 8
Size: 290519 Color: 18
Size: 272355 Color: 12

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 437680 Color: 7
Size: 286790 Color: 14
Size: 275531 Color: 11

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 438044 Color: 1
Size: 304560 Color: 0
Size: 257397 Color: 2

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 439362 Color: 6
Size: 309443 Color: 0
Size: 251196 Color: 11

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 439379 Color: 15
Size: 287801 Color: 13
Size: 272821 Color: 5

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 439640 Color: 10
Size: 298444 Color: 0
Size: 261917 Color: 8

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 439645 Color: 14
Size: 282251 Color: 6
Size: 278105 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 439847 Color: 14
Size: 289634 Color: 18
Size: 270520 Color: 18

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 440044 Color: 10
Size: 290598 Color: 2
Size: 269359 Color: 3

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 440311 Color: 16
Size: 307340 Color: 11
Size: 252350 Color: 5

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 440825 Color: 3
Size: 304802 Color: 7
Size: 254374 Color: 5

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 441235 Color: 17
Size: 299854 Color: 3
Size: 258912 Color: 6

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 441330 Color: 10
Size: 307210 Color: 19
Size: 251461 Color: 15

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 441490 Color: 4
Size: 306633 Color: 2
Size: 251878 Color: 11

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 441550 Color: 10
Size: 288031 Color: 6
Size: 270420 Color: 11

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 441749 Color: 18
Size: 294229 Color: 3
Size: 264023 Color: 12

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 442246 Color: 4
Size: 306768 Color: 13
Size: 250987 Color: 5

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 442571 Color: 17
Size: 283567 Color: 8
Size: 273863 Color: 15

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 442695 Color: 4
Size: 282508 Color: 0
Size: 274798 Color: 11

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 442851 Color: 13
Size: 284268 Color: 10
Size: 272882 Color: 16

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 443191 Color: 6
Size: 301961 Color: 13
Size: 254849 Color: 3

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 443229 Color: 10
Size: 284055 Color: 10
Size: 272717 Color: 18

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 443433 Color: 5
Size: 290733 Color: 0
Size: 265835 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 443585 Color: 5
Size: 300319 Color: 9
Size: 256097 Color: 3

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 443651 Color: 14
Size: 284307 Color: 16
Size: 272043 Color: 15

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 444047 Color: 13
Size: 296660 Color: 10
Size: 259294 Color: 10

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 444330 Color: 18
Size: 288667 Color: 19
Size: 267004 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 444441 Color: 7
Size: 301313 Color: 10
Size: 254247 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 445234 Color: 3
Size: 279542 Color: 2
Size: 275225 Color: 13

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 445455 Color: 6
Size: 304137 Color: 7
Size: 250409 Color: 13

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 445521 Color: 16
Size: 290797 Color: 2
Size: 263683 Color: 18

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 445614 Color: 14
Size: 301112 Color: 3
Size: 253275 Color: 18

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 445792 Color: 15
Size: 301080 Color: 14
Size: 253129 Color: 11

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 446433 Color: 2
Size: 288054 Color: 7
Size: 265514 Color: 3

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 446506 Color: 12
Size: 279425 Color: 10
Size: 274070 Color: 14

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 446842 Color: 16
Size: 285296 Color: 3
Size: 267863 Color: 18

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 447499 Color: 14
Size: 280673 Color: 15
Size: 271829 Color: 17

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 447662 Color: 14
Size: 288418 Color: 13
Size: 263921 Color: 11

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 447824 Color: 16
Size: 301296 Color: 8
Size: 250881 Color: 3

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 448028 Color: 19
Size: 278542 Color: 11
Size: 273431 Color: 4

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 448093 Color: 18
Size: 290990 Color: 5
Size: 260918 Color: 15

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 448376 Color: 1
Size: 297298 Color: 10
Size: 254327 Color: 13

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 448637 Color: 7
Size: 301243 Color: 11
Size: 250121 Color: 17

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 449077 Color: 3
Size: 295099 Color: 4
Size: 255825 Color: 17

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 449336 Color: 13
Size: 277402 Color: 10
Size: 273263 Color: 4

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 449562 Color: 2
Size: 298460 Color: 17
Size: 251979 Color: 5

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 449919 Color: 19
Size: 277596 Color: 11
Size: 272486 Color: 14

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 450248 Color: 8
Size: 292121 Color: 18
Size: 257632 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 450284 Color: 14
Size: 292487 Color: 11
Size: 257230 Color: 13

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 450313 Color: 0
Size: 286762 Color: 12
Size: 262926 Color: 2

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 450688 Color: 9
Size: 280666 Color: 6
Size: 268647 Color: 15

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 450645 Color: 7
Size: 297732 Color: 12
Size: 251624 Color: 8

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 451260 Color: 18
Size: 292830 Color: 15
Size: 255911 Color: 8

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 451850 Color: 16
Size: 295940 Color: 8
Size: 252211 Color: 11

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 452535 Color: 11
Size: 273882 Color: 6
Size: 273584 Color: 14

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 452554 Color: 10
Size: 281213 Color: 4
Size: 266234 Color: 11

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 452567 Color: 12
Size: 295038 Color: 2
Size: 252396 Color: 10

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 453362 Color: 14
Size: 278039 Color: 5
Size: 268600 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 453473 Color: 2
Size: 290417 Color: 15
Size: 256111 Color: 2

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 453757 Color: 7
Size: 287832 Color: 16
Size: 258412 Color: 5

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 453864 Color: 13
Size: 275510 Color: 10
Size: 270627 Color: 12

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 454114 Color: 19
Size: 278404 Color: 19
Size: 267483 Color: 3

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 454363 Color: 15
Size: 295332 Color: 10
Size: 250306 Color: 14

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 455105 Color: 9
Size: 294369 Color: 10
Size: 250527 Color: 8

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 454779 Color: 18
Size: 288953 Color: 1
Size: 256269 Color: 12

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 455558 Color: 8
Size: 284139 Color: 10
Size: 260304 Color: 16

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 455844 Color: 11
Size: 287049 Color: 11
Size: 257108 Color: 2

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 456009 Color: 7
Size: 290180 Color: 16
Size: 253812 Color: 14

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 456147 Color: 2
Size: 274050 Color: 1
Size: 269804 Color: 14

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 456311 Color: 2
Size: 274958 Color: 0
Size: 268732 Color: 9

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 456519 Color: 15
Size: 280323 Color: 5
Size: 263159 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 456739 Color: 1
Size: 275496 Color: 14
Size: 267766 Color: 18

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 456960 Color: 14
Size: 292090 Color: 8
Size: 250951 Color: 17

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 457765 Color: 10
Size: 280067 Color: 2
Size: 262169 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 458074 Color: 1
Size: 276235 Color: 19
Size: 265692 Color: 2

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 459114 Color: 9
Size: 277044 Color: 1
Size: 263843 Color: 13

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 458607 Color: 7
Size: 272936 Color: 5
Size: 268458 Color: 16

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 459270 Color: 18
Size: 276073 Color: 13
Size: 264658 Color: 14

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 460102 Color: 16
Size: 283181 Color: 15
Size: 256718 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 460441 Color: 4
Size: 270499 Color: 11
Size: 269061 Color: 18

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 460476 Color: 6
Size: 274351 Color: 11
Size: 265174 Color: 4

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 460845 Color: 8
Size: 270544 Color: 3
Size: 268612 Color: 17

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 461105 Color: 11
Size: 282095 Color: 12
Size: 256801 Color: 5

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 461686 Color: 9
Size: 281315 Color: 6
Size: 257000 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 461801 Color: 18
Size: 281541 Color: 14
Size: 256659 Color: 14

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 461984 Color: 18
Size: 274264 Color: 0
Size: 263753 Color: 16

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 461989 Color: 19
Size: 284343 Color: 2
Size: 253669 Color: 13

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 462166 Color: 16
Size: 270848 Color: 9
Size: 266987 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 462395 Color: 0
Size: 270616 Color: 17
Size: 266990 Color: 11

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 462514 Color: 4
Size: 274337 Color: 12
Size: 263150 Color: 11

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 462912 Color: 15
Size: 276251 Color: 12
Size: 260838 Color: 12

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 463442 Color: 10
Size: 279300 Color: 14
Size: 257259 Color: 14

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 463706 Color: 9
Size: 270112 Color: 12
Size: 266183 Color: 7

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 463604 Color: 4
Size: 269599 Color: 15
Size: 266798 Color: 14

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 464119 Color: 1
Size: 277715 Color: 7
Size: 258167 Color: 12

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 464575 Color: 14
Size: 277452 Color: 12
Size: 257974 Color: 8

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 464578 Color: 16
Size: 274020 Color: 1
Size: 261403 Color: 16

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 465037 Color: 7
Size: 281979 Color: 19
Size: 252985 Color: 11

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 465291 Color: 4
Size: 282128 Color: 0
Size: 252582 Color: 14

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 465383 Color: 1
Size: 277887 Color: 17
Size: 256731 Color: 14

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 465460 Color: 5
Size: 272235 Color: 3
Size: 262306 Color: 3

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 465662 Color: 17
Size: 271685 Color: 15
Size: 262654 Color: 6

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 465941 Color: 11
Size: 275158 Color: 3
Size: 258902 Color: 2

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 466950 Color: 9
Size: 268606 Color: 17
Size: 264445 Color: 12

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 467543 Color: 8
Size: 272709 Color: 7
Size: 259749 Color: 6

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 467647 Color: 10
Size: 279144 Color: 18
Size: 253210 Color: 9

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 468538 Color: 5
Size: 269743 Color: 0
Size: 261720 Color: 1

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 468631 Color: 3
Size: 274687 Color: 11
Size: 256683 Color: 17

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 468968 Color: 16
Size: 274555 Color: 10
Size: 256478 Color: 7

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 469344 Color: 1
Size: 278720 Color: 17
Size: 251937 Color: 18

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 469753 Color: 12
Size: 267566 Color: 2
Size: 262682 Color: 4

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 469761 Color: 16
Size: 275661 Color: 19
Size: 254579 Color: 10

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 470319 Color: 4
Size: 268069 Color: 0
Size: 261613 Color: 5

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 470485 Color: 14
Size: 278625 Color: 3
Size: 250891 Color: 15

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 470631 Color: 1
Size: 269359 Color: 7
Size: 260011 Color: 3

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 471129 Color: 19
Size: 274682 Color: 12
Size: 254190 Color: 1

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 471971 Color: 7
Size: 276068 Color: 17
Size: 251962 Color: 18

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 472347 Color: 9
Size: 277320 Color: 3
Size: 250334 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 472755 Color: 19
Size: 267006 Color: 15
Size: 260240 Color: 15

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 473386 Color: 18
Size: 266960 Color: 2
Size: 259655 Color: 2

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 473395 Color: 19
Size: 266186 Color: 10
Size: 260420 Color: 17

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 473425 Color: 13
Size: 275572 Color: 4
Size: 251004 Color: 16

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 474159 Color: 2
Size: 271285 Color: 11
Size: 254557 Color: 6

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 474672 Color: 18
Size: 264787 Color: 15
Size: 260542 Color: 15

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 474817 Color: 11
Size: 262717 Color: 11
Size: 262467 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 475654 Color: 13
Size: 266826 Color: 17
Size: 257521 Color: 2

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 476006 Color: 11
Size: 273238 Color: 10
Size: 250757 Color: 4

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 476448 Color: 14
Size: 272137 Color: 1
Size: 251416 Color: 18

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 476473 Color: 0
Size: 272211 Color: 19
Size: 251317 Color: 14

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 476626 Color: 2
Size: 268306 Color: 18
Size: 255069 Color: 12

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 476839 Color: 1
Size: 265064 Color: 15
Size: 258098 Color: 15

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 477461 Color: 16
Size: 262281 Color: 4
Size: 260259 Color: 3

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 478206 Color: 2
Size: 268358 Color: 3
Size: 253437 Color: 14

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 478266 Color: 17
Size: 268074 Color: 3
Size: 253661 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 478644 Color: 3
Size: 270867 Color: 11
Size: 250490 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 478694 Color: 18
Size: 262985 Color: 1
Size: 258322 Color: 16

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 478892 Color: 2
Size: 269230 Color: 13
Size: 251879 Color: 4

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 478940 Color: 6
Size: 261030 Color: 1
Size: 260031 Color: 13

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 479409 Color: 17
Size: 268800 Color: 18
Size: 251792 Color: 19

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 480571 Color: 12
Size: 262916 Color: 11
Size: 256514 Color: 8

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 481395 Color: 5
Size: 262029 Color: 18
Size: 256577 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 483310 Color: 9
Size: 263495 Color: 17
Size: 253196 Color: 18

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 482035 Color: 19
Size: 267583 Color: 13
Size: 250383 Color: 1

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 483755 Color: 9
Size: 266039 Color: 4
Size: 250207 Color: 13

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 482849 Color: 5
Size: 265408 Color: 7
Size: 251744 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 483192 Color: 4
Size: 265674 Color: 16
Size: 251135 Color: 3

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 483242 Color: 12
Size: 265391 Color: 4
Size: 251368 Color: 6

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 483603 Color: 8
Size: 264742 Color: 15
Size: 251656 Color: 19

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 484553 Color: 19
Size: 261532 Color: 15
Size: 253916 Color: 6

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 484803 Color: 15
Size: 262919 Color: 16
Size: 252279 Color: 17

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 486401 Color: 9
Size: 261692 Color: 15
Size: 251908 Color: 15

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 485279 Color: 3
Size: 259476 Color: 3
Size: 255246 Color: 13

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 485586 Color: 15
Size: 262333 Color: 13
Size: 252082 Color: 17

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 487213 Color: 9
Size: 256826 Color: 17
Size: 255962 Color: 4

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 486330 Color: 15
Size: 259643 Color: 12
Size: 254028 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 489728 Color: 10
Size: 259482 Color: 15
Size: 250791 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 490794 Color: 9
Size: 256551 Color: 6
Size: 252656 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 490727 Color: 1
Size: 258276 Color: 5
Size: 250998 Color: 17

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 491143 Color: 17
Size: 258517 Color: 14
Size: 250341 Color: 2

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 491490 Color: 19
Size: 254857 Color: 10
Size: 253654 Color: 16

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 492222 Color: 7
Size: 254126 Color: 1
Size: 253653 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 492631 Color: 14
Size: 256087 Color: 15
Size: 251283 Color: 2

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 492803 Color: 6
Size: 255421 Color: 10
Size: 251777 Color: 9

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 492834 Color: 3
Size: 256119 Color: 4
Size: 251048 Color: 16

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 493122 Color: 10
Size: 256837 Color: 14
Size: 250042 Color: 5

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 493189 Color: 8
Size: 255034 Color: 17
Size: 251778 Color: 7

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 493563 Color: 10
Size: 255526 Color: 18
Size: 250912 Color: 18

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 494252 Color: 12
Size: 254240 Color: 0
Size: 251509 Color: 4

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 494267 Color: 14
Size: 254062 Color: 4
Size: 251672 Color: 11

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 495387 Color: 14
Size: 253955 Color: 4
Size: 250659 Color: 3

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 495989 Color: 14
Size: 253706 Color: 14
Size: 250306 Color: 15

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 496603 Color: 10
Size: 252935 Color: 18
Size: 250463 Color: 3

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 496895 Color: 9
Size: 252142 Color: 14
Size: 250964 Color: 4

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 360271 Color: 17
Size: 341142 Color: 8
Size: 298587 Color: 12

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 363770 Color: 1
Size: 327687 Color: 9
Size: 308543 Color: 13

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 367092 Color: 19
Size: 330219 Color: 2
Size: 302689 Color: 7

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 367318 Color: 19
Size: 323850 Color: 4
Size: 308832 Color: 10

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 368612 Color: 7
Size: 366753 Color: 17
Size: 264635 Color: 7

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 371324 Color: 8
Size: 344126 Color: 5
Size: 284550 Color: 15

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 372518 Color: 11
Size: 360001 Color: 17
Size: 267481 Color: 14

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 373230 Color: 7
Size: 372132 Color: 4
Size: 254638 Color: 11

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 375362 Color: 17
Size: 349198 Color: 6
Size: 275440 Color: 2

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 377758 Color: 6
Size: 348547 Color: 14
Size: 273695 Color: 0

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 378340 Color: 1
Size: 325908 Color: 19
Size: 295752 Color: 2

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 379635 Color: 9
Size: 310971 Color: 3
Size: 309394 Color: 18

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 380356 Color: 0
Size: 312822 Color: 0
Size: 306822 Color: 1

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 380972 Color: 19
Size: 354236 Color: 13
Size: 264792 Color: 8

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 382319 Color: 7
Size: 331894 Color: 19
Size: 285787 Color: 8

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 385030 Color: 18
Size: 326587 Color: 1
Size: 288383 Color: 6

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 386357 Color: 2
Size: 331619 Color: 3
Size: 282024 Color: 6

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 387388 Color: 18
Size: 357908 Color: 16
Size: 254704 Color: 1

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 389106 Color: 18
Size: 355923 Color: 9
Size: 254971 Color: 1

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 389296 Color: 12
Size: 334416 Color: 6
Size: 276288 Color: 2

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 389394 Color: 13
Size: 317544 Color: 2
Size: 293062 Color: 4

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 391595 Color: 15
Size: 312148 Color: 7
Size: 296257 Color: 10

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 391823 Color: 18
Size: 322706 Color: 8
Size: 285471 Color: 6

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 395542 Color: 14
Size: 318637 Color: 16
Size: 285821 Color: 18

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 395785 Color: 11
Size: 352887 Color: 15
Size: 251328 Color: 19

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 396434 Color: 7
Size: 337055 Color: 2
Size: 266511 Color: 4

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 398491 Color: 7
Size: 318907 Color: 6
Size: 282602 Color: 2

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 399658 Color: 3
Size: 309126 Color: 19
Size: 291216 Color: 1

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 399742 Color: 11
Size: 349512 Color: 3
Size: 250746 Color: 5

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 401244 Color: 1
Size: 312528 Color: 8
Size: 286228 Color: 6

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 401628 Color: 18
Size: 333493 Color: 17
Size: 264879 Color: 5

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 401667 Color: 9
Size: 334267 Color: 10
Size: 264066 Color: 17

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 401915 Color: 18
Size: 313283 Color: 11
Size: 284802 Color: 7

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 403234 Color: 10
Size: 310283 Color: 0
Size: 286483 Color: 6

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 403255 Color: 18
Size: 340313 Color: 9
Size: 256432 Color: 19

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 405265 Color: 19
Size: 320538 Color: 0
Size: 274197 Color: 2

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 405381 Color: 18
Size: 328754 Color: 17
Size: 265865 Color: 12

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 407032 Color: 0
Size: 328841 Color: 2
Size: 264127 Color: 19

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 408277 Color: 7
Size: 306487 Color: 11
Size: 285236 Color: 11

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 408335 Color: 4
Size: 327025 Color: 7
Size: 264640 Color: 13

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 411220 Color: 3
Size: 331917 Color: 15
Size: 256863 Color: 4

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 411292 Color: 16
Size: 314462 Color: 16
Size: 274246 Color: 10

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 411942 Color: 16
Size: 304043 Color: 9
Size: 284015 Color: 1

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 412648 Color: 8
Size: 315546 Color: 16
Size: 271806 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 413206 Color: 12
Size: 335683 Color: 16
Size: 251111 Color: 1

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 413648 Color: 8
Size: 298327 Color: 10
Size: 288025 Color: 3

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 416654 Color: 18
Size: 309426 Color: 19
Size: 273920 Color: 12

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 417105 Color: 3
Size: 322480 Color: 12
Size: 260415 Color: 6

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 418096 Color: 11
Size: 313991 Color: 15
Size: 267913 Color: 12

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 420359 Color: 11
Size: 318135 Color: 0
Size: 261506 Color: 5

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 422450 Color: 18
Size: 292958 Color: 13
Size: 284592 Color: 4

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 428600 Color: 16
Size: 312581 Color: 10
Size: 258819 Color: 18

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 435435 Color: 14
Size: 292173 Color: 17
Size: 272392 Color: 15

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 435859 Color: 19
Size: 282698 Color: 16
Size: 281443 Color: 14

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 436874 Color: 0
Size: 303970 Color: 11
Size: 259156 Color: 9

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 441214 Color: 4
Size: 288083 Color: 10
Size: 270703 Color: 9

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 441849 Color: 9
Size: 287620 Color: 17
Size: 270531 Color: 6

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 445059 Color: 1
Size: 303396 Color: 2
Size: 251545 Color: 9

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 447228 Color: 18
Size: 300323 Color: 9
Size: 252449 Color: 1

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 448055 Color: 2
Size: 300815 Color: 12
Size: 251130 Color: 9

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 451074 Color: 15
Size: 280616 Color: 7
Size: 268310 Color: 15

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 454822 Color: 12
Size: 290429 Color: 6
Size: 254749 Color: 10

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 456294 Color: 16
Size: 276505 Color: 5
Size: 267201 Color: 10

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 457323 Color: 8
Size: 290846 Color: 13
Size: 251831 Color: 15

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 461009 Color: 7
Size: 275421 Color: 10
Size: 263570 Color: 14

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 465347 Color: 8
Size: 271671 Color: 3
Size: 262982 Color: 3

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 467632 Color: 15
Size: 271503 Color: 2
Size: 260865 Color: 13

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 468400 Color: 14
Size: 273062 Color: 12
Size: 258538 Color: 14

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 473842 Color: 11
Size: 263635 Color: 9
Size: 262523 Color: 7

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 475820 Color: 16
Size: 265245 Color: 2
Size: 258935 Color: 5

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 475848 Color: 11
Size: 269120 Color: 13
Size: 255032 Color: 9

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 476836 Color: 10
Size: 269216 Color: 9
Size: 253948 Color: 16

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 484648 Color: 0
Size: 259920 Color: 13
Size: 255432 Color: 4

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 491994 Color: 19
Size: 254586 Color: 12
Size: 253420 Color: 15

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 493702 Color: 8
Size: 254780 Color: 8
Size: 251518 Color: 9

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 493838 Color: 3
Size: 253978 Color: 19
Size: 252184 Color: 15

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 495375 Color: 1
Size: 253123 Color: 1
Size: 251502 Color: 17

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 495838 Color: 19
Size: 253910 Color: 2
Size: 250252 Color: 9

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 496413 Color: 11
Size: 252255 Color: 6
Size: 251332 Color: 17

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 371245 Color: 10
Size: 336331 Color: 11
Size: 292423 Color: 9

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 371426 Color: 5
Size: 328505 Color: 3
Size: 300068 Color: 7

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 373821 Color: 0
Size: 325760 Color: 17
Size: 300418 Color: 8

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 373876 Color: 1
Size: 336311 Color: 7
Size: 289812 Color: 16

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 380994 Color: 1
Size: 319806 Color: 13
Size: 299199 Color: 6

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 382825 Color: 14
Size: 359409 Color: 5
Size: 257765 Color: 0

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 385321 Color: 16
Size: 362734 Color: 7
Size: 251944 Color: 7

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 385728 Color: 14
Size: 340481 Color: 16
Size: 273790 Color: 8

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 389966 Color: 0
Size: 324270 Color: 8
Size: 285763 Color: 14

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 389562 Color: 16
Size: 343537 Color: 3
Size: 266900 Color: 3

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 390507 Color: 10
Size: 344741 Color: 14
Size: 264751 Color: 17

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 391107 Color: 14
Size: 307591 Color: 6
Size: 301301 Color: 3

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 396026 Color: 13
Size: 322424 Color: 0
Size: 281549 Color: 11

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 397609 Color: 0
Size: 324620 Color: 12
Size: 277770 Color: 14

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 397571 Color: 8
Size: 317009 Color: 8
Size: 285419 Color: 9

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 398409 Color: 18
Size: 319143 Color: 7
Size: 282447 Color: 16

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 404069 Color: 6
Size: 330445 Color: 4
Size: 265485 Color: 18

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 407207 Color: 11
Size: 340917 Color: 4
Size: 251875 Color: 1

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 408043 Color: 12
Size: 309701 Color: 4
Size: 282255 Color: 17

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 413968 Color: 14
Size: 300274 Color: 13
Size: 285757 Color: 10

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 414024 Color: 4
Size: 325796 Color: 19
Size: 260179 Color: 18

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 422627 Color: 3
Size: 324557 Color: 9
Size: 252815 Color: 3

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 425532 Color: 11
Size: 292998 Color: 12
Size: 281469 Color: 9

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 431023 Color: 19
Size: 290674 Color: 11
Size: 278302 Color: 6

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 438915 Color: 11
Size: 308277 Color: 9
Size: 252807 Color: 17

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 439302 Color: 6
Size: 299289 Color: 18
Size: 261408 Color: 4

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 439804 Color: 3
Size: 289161 Color: 9
Size: 271034 Color: 7

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 441738 Color: 14
Size: 286948 Color: 4
Size: 271313 Color: 3

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 459365 Color: 9
Size: 280395 Color: 10
Size: 260239 Color: 18

Bin 487: 3 of cap free
Amount of items: 3
Items: 
Size: 354289 Color: 19
Size: 340186 Color: 13
Size: 305523 Color: 16

Bin 488: 3 of cap free
Amount of items: 3
Items: 
Size: 360975 Color: 9
Size: 342774 Color: 17
Size: 296249 Color: 5

Bin 489: 3 of cap free
Amount of items: 3
Items: 
Size: 366393 Color: 15
Size: 339714 Color: 19
Size: 293891 Color: 9

Bin 490: 3 of cap free
Amount of items: 3
Items: 
Size: 366651 Color: 3
Size: 351758 Color: 1
Size: 281589 Color: 9

Bin 491: 3 of cap free
Amount of items: 3
Items: 
Size: 367639 Color: 3
Size: 349165 Color: 11
Size: 283194 Color: 4

Bin 492: 3 of cap free
Amount of items: 3
Items: 
Size: 368531 Color: 12
Size: 352434 Color: 8
Size: 279033 Color: 15

Bin 493: 3 of cap free
Amount of items: 3
Items: 
Size: 370462 Color: 2
Size: 332143 Color: 0
Size: 297393 Color: 13

Bin 494: 3 of cap free
Amount of items: 3
Items: 
Size: 371918 Color: 5
Size: 344781 Color: 17
Size: 283299 Color: 18

Bin 495: 3 of cap free
Amount of items: 3
Items: 
Size: 372499 Color: 2
Size: 343456 Color: 9
Size: 284043 Color: 3

Bin 496: 3 of cap free
Amount of items: 3
Items: 
Size: 374786 Color: 13
Size: 332826 Color: 12
Size: 292386 Color: 6

Bin 497: 3 of cap free
Amount of items: 3
Items: 
Size: 377965 Color: 1
Size: 341857 Color: 2
Size: 280176 Color: 0

Bin 498: 3 of cap free
Amount of items: 3
Items: 
Size: 382230 Color: 0
Size: 318711 Color: 19
Size: 299057 Color: 13

Bin 499: 3 of cap free
Amount of items: 3
Items: 
Size: 382133 Color: 18
Size: 338051 Color: 4
Size: 279814 Color: 14

Bin 500: 3 of cap free
Amount of items: 3
Items: 
Size: 382939 Color: 17
Size: 325430 Color: 3
Size: 291629 Color: 7

Bin 501: 3 of cap free
Amount of items: 3
Items: 
Size: 388491 Color: 7
Size: 314587 Color: 8
Size: 296920 Color: 5

Bin 502: 3 of cap free
Amount of items: 3
Items: 
Size: 389113 Color: 10
Size: 318724 Color: 6
Size: 292161 Color: 2

Bin 503: 3 of cap free
Amount of items: 3
Items: 
Size: 390036 Color: 0
Size: 327995 Color: 4
Size: 281967 Color: 9

Bin 504: 3 of cap free
Amount of items: 3
Items: 
Size: 390667 Color: 14
Size: 317433 Color: 6
Size: 291898 Color: 2

Bin 505: 3 of cap free
Amount of items: 3
Items: 
Size: 390854 Color: 18
Size: 309418 Color: 6
Size: 299726 Color: 4

Bin 506: 3 of cap free
Amount of items: 3
Items: 
Size: 391292 Color: 17
Size: 337378 Color: 12
Size: 271328 Color: 18

Bin 507: 3 of cap free
Amount of items: 3
Items: 
Size: 393736 Color: 16
Size: 318413 Color: 14
Size: 287849 Color: 6

Bin 508: 3 of cap free
Amount of items: 3
Items: 
Size: 394567 Color: 19
Size: 311945 Color: 0
Size: 293486 Color: 8

Bin 509: 3 of cap free
Amount of items: 3
Items: 
Size: 408955 Color: 2
Size: 329780 Color: 9
Size: 261263 Color: 16

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 427294 Color: 15
Size: 314293 Color: 9
Size: 258411 Color: 12

Bin 511: 3 of cap free
Amount of items: 3
Items: 
Size: 430941 Color: 6
Size: 316132 Color: 12
Size: 252925 Color: 9

Bin 512: 3 of cap free
Amount of items: 3
Items: 
Size: 456161 Color: 9
Size: 281551 Color: 19
Size: 262286 Color: 10

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 496642 Color: 11
Size: 252178 Color: 4
Size: 251178 Color: 13

Bin 514: 4 of cap free
Amount of items: 3
Items: 
Size: 359688 Color: 15
Size: 359608 Color: 12
Size: 280701 Color: 3

Bin 515: 4 of cap free
Amount of items: 3
Items: 
Size: 368383 Color: 9
Size: 346556 Color: 14
Size: 285058 Color: 14

Bin 516: 4 of cap free
Amount of items: 3
Items: 
Size: 372766 Color: 2
Size: 368916 Color: 6
Size: 258315 Color: 5

Bin 517: 4 of cap free
Amount of items: 3
Items: 
Size: 378174 Color: 16
Size: 333247 Color: 8
Size: 288576 Color: 9

Bin 518: 4 of cap free
Amount of items: 3
Items: 
Size: 379621 Color: 15
Size: 360659 Color: 3
Size: 259717 Color: 13

Bin 519: 4 of cap free
Amount of items: 3
Items: 
Size: 384716 Color: 2
Size: 348500 Color: 14
Size: 266781 Color: 0

Bin 520: 4 of cap free
Amount of items: 3
Items: 
Size: 384934 Color: 5
Size: 343723 Color: 16
Size: 271340 Color: 13

Bin 521: 4 of cap free
Amount of items: 3
Items: 
Size: 385386 Color: 10
Size: 327342 Color: 10
Size: 287269 Color: 6

Bin 522: 4 of cap free
Amount of items: 3
Items: 
Size: 386170 Color: 2
Size: 333105 Color: 16
Size: 280722 Color: 1

Bin 523: 4 of cap free
Amount of items: 3
Items: 
Size: 388460 Color: 9
Size: 350402 Color: 13
Size: 261135 Color: 3

Bin 524: 4 of cap free
Amount of items: 3
Items: 
Size: 391717 Color: 4
Size: 352625 Color: 9
Size: 255655 Color: 12

Bin 525: 4 of cap free
Amount of items: 3
Items: 
Size: 401793 Color: 2
Size: 337468 Color: 9
Size: 260736 Color: 6

Bin 526: 4 of cap free
Amount of items: 3
Items: 
Size: 404849 Color: 7
Size: 300334 Color: 9
Size: 294814 Color: 5

Bin 527: 4 of cap free
Amount of items: 3
Items: 
Size: 411100 Color: 19
Size: 307974 Color: 14
Size: 280923 Color: 9

Bin 528: 4 of cap free
Amount of items: 3
Items: 
Size: 411411 Color: 13
Size: 333810 Color: 9
Size: 254776 Color: 5

Bin 529: 4 of cap free
Amount of items: 3
Items: 
Size: 412999 Color: 5
Size: 308022 Color: 9
Size: 278976 Color: 11

Bin 530: 4 of cap free
Amount of items: 3
Items: 
Size: 420205 Color: 19
Size: 311006 Color: 9
Size: 268786 Color: 11

Bin 531: 4 of cap free
Amount of items: 3
Items: 
Size: 431869 Color: 17
Size: 296565 Color: 15
Size: 271563 Color: 9

Bin 532: 4 of cap free
Amount of items: 3
Items: 
Size: 445622 Color: 17
Size: 284360 Color: 8
Size: 270015 Color: 9

Bin 533: 4 of cap free
Amount of items: 3
Items: 
Size: 449435 Color: 12
Size: 279183 Color: 9
Size: 271379 Color: 17

Bin 534: 4 of cap free
Amount of items: 3
Items: 
Size: 451583 Color: 3
Size: 293693 Color: 1
Size: 254721 Color: 9

Bin 535: 4 of cap free
Amount of items: 3
Items: 
Size: 464879 Color: 8
Size: 282008 Color: 16
Size: 253110 Color: 9

Bin 536: 4 of cap free
Amount of items: 3
Items: 
Size: 470363 Color: 5
Size: 270064 Color: 9
Size: 259570 Color: 16

Bin 537: 5 of cap free
Amount of items: 3
Items: 
Size: 361044 Color: 10
Size: 335178 Color: 14
Size: 303774 Color: 14

Bin 538: 5 of cap free
Amount of items: 3
Items: 
Size: 362279 Color: 4
Size: 346208 Color: 16
Size: 291509 Color: 6

Bin 539: 5 of cap free
Amount of items: 3
Items: 
Size: 363902 Color: 6
Size: 337753 Color: 9
Size: 298341 Color: 18

Bin 540: 5 of cap free
Amount of items: 3
Items: 
Size: 368748 Color: 0
Size: 350727 Color: 12
Size: 280521 Color: 18

Bin 541: 5 of cap free
Amount of items: 3
Items: 
Size: 375956 Color: 12
Size: 353127 Color: 5
Size: 270913 Color: 18

Bin 542: 5 of cap free
Amount of items: 3
Items: 
Size: 379446 Color: 15
Size: 313305 Color: 19
Size: 307245 Color: 5

Bin 543: 5 of cap free
Amount of items: 3
Items: 
Size: 387165 Color: 9
Size: 322475 Color: 12
Size: 290356 Color: 1

Bin 544: 5 of cap free
Amount of items: 3
Items: 
Size: 389487 Color: 3
Size: 328108 Color: 13
Size: 282401 Color: 16

Bin 545: 5 of cap free
Amount of items: 3
Items: 
Size: 491146 Color: 0
Size: 255608 Color: 1
Size: 253242 Color: 9

Bin 546: 6 of cap free
Amount of items: 3
Items: 
Size: 367386 Color: 13
Size: 364063 Color: 0
Size: 268546 Color: 12

Bin 547: 6 of cap free
Amount of items: 3
Items: 
Size: 368050 Color: 10
Size: 359599 Color: 17
Size: 272346 Color: 6

Bin 548: 6 of cap free
Amount of items: 3
Items: 
Size: 368472 Color: 19
Size: 344442 Color: 14
Size: 287081 Color: 16

Bin 549: 6 of cap free
Amount of items: 3
Items: 
Size: 373420 Color: 2
Size: 334346 Color: 10
Size: 292229 Color: 8

Bin 550: 6 of cap free
Amount of items: 3
Items: 
Size: 376742 Color: 18
Size: 368227 Color: 5
Size: 255026 Color: 4

Bin 551: 6 of cap free
Amount of items: 3
Items: 
Size: 381329 Color: 9
Size: 364238 Color: 10
Size: 254428 Color: 12

Bin 552: 6 of cap free
Amount of items: 3
Items: 
Size: 414848 Color: 1
Size: 306877 Color: 9
Size: 278270 Color: 19

Bin 553: 6 of cap free
Amount of items: 3
Items: 
Size: 422297 Color: 3
Size: 312538 Color: 9
Size: 265160 Color: 11

Bin 554: 6 of cap free
Amount of items: 3
Items: 
Size: 423316 Color: 2
Size: 291516 Color: 2
Size: 285163 Color: 9

Bin 555: 6 of cap free
Amount of items: 3
Items: 
Size: 442606 Color: 17
Size: 288037 Color: 9
Size: 269352 Color: 15

Bin 556: 6 of cap free
Amount of items: 3
Items: 
Size: 469268 Color: 2
Size: 279789 Color: 16
Size: 250938 Color: 9

Bin 557: 7 of cap free
Amount of items: 3
Items: 
Size: 365243 Color: 1
Size: 357961 Color: 9
Size: 276790 Color: 1

Bin 558: 7 of cap free
Amount of items: 3
Items: 
Size: 373264 Color: 5
Size: 324494 Color: 10
Size: 302236 Color: 19

Bin 559: 7 of cap free
Amount of items: 3
Items: 
Size: 377618 Color: 7
Size: 328990 Color: 9
Size: 293386 Color: 7

Bin 560: 7 of cap free
Amount of items: 3
Items: 
Size: 390632 Color: 3
Size: 320663 Color: 9
Size: 288699 Color: 17

Bin 561: 7 of cap free
Amount of items: 3
Items: 
Size: 408309 Color: 16
Size: 328889 Color: 9
Size: 262796 Color: 16

Bin 562: 7 of cap free
Amount of items: 3
Items: 
Size: 408906 Color: 7
Size: 302867 Color: 10
Size: 288221 Color: 5

Bin 563: 8 of cap free
Amount of items: 3
Items: 
Size: 360439 Color: 6
Size: 355408 Color: 14
Size: 284146 Color: 12

Bin 564: 8 of cap free
Amount of items: 3
Items: 
Size: 361542 Color: 7
Size: 319777 Color: 6
Size: 318674 Color: 12

Bin 565: 8 of cap free
Amount of items: 3
Items: 
Size: 369109 Color: 10
Size: 326887 Color: 11
Size: 303997 Color: 18

Bin 566: 8 of cap free
Amount of items: 3
Items: 
Size: 389853 Color: 9
Size: 312411 Color: 0
Size: 297729 Color: 16

Bin 567: 8 of cap free
Amount of items: 3
Items: 
Size: 428866 Color: 8
Size: 308920 Color: 9
Size: 262207 Color: 17

Bin 568: 9 of cap free
Amount of items: 3
Items: 
Size: 351746 Color: 4
Size: 331931 Color: 1
Size: 316315 Color: 13

Bin 569: 9 of cap free
Amount of items: 3
Items: 
Size: 358862 Color: 16
Size: 335531 Color: 9
Size: 305599 Color: 18

Bin 570: 9 of cap free
Amount of items: 3
Items: 
Size: 366382 Color: 17
Size: 361119 Color: 15
Size: 272491 Color: 18

Bin 571: 9 of cap free
Amount of items: 3
Items: 
Size: 369125 Color: 13
Size: 335397 Color: 15
Size: 295470 Color: 12

Bin 572: 9 of cap free
Amount of items: 3
Items: 
Size: 405994 Color: 9
Size: 318484 Color: 10
Size: 275514 Color: 8

Bin 573: 9 of cap free
Amount of items: 3
Items: 
Size: 406618 Color: 16
Size: 302410 Color: 8
Size: 290964 Color: 9

Bin 574: 10 of cap free
Amount of items: 3
Items: 
Size: 360984 Color: 10
Size: 335168 Color: 4
Size: 303839 Color: 0

Bin 575: 10 of cap free
Amount of items: 3
Items: 
Size: 365859 Color: 15
Size: 326532 Color: 0
Size: 307600 Color: 8

Bin 576: 10 of cap free
Amount of items: 3
Items: 
Size: 367203 Color: 6
Size: 362487 Color: 10
Size: 270301 Color: 2

Bin 577: 10 of cap free
Amount of items: 3
Items: 
Size: 372089 Color: 18
Size: 323114 Color: 12
Size: 304788 Color: 2

Bin 578: 10 of cap free
Amount of items: 3
Items: 
Size: 384460 Color: 8
Size: 311227 Color: 3
Size: 304304 Color: 10

Bin 579: 10 of cap free
Amount of items: 3
Items: 
Size: 385201 Color: 8
Size: 327482 Color: 17
Size: 287308 Color: 0

Bin 580: 10 of cap free
Amount of items: 3
Items: 
Size: 453227 Color: 6
Size: 277331 Color: 0
Size: 269433 Color: 9

Bin 581: 12 of cap free
Amount of items: 3
Items: 
Size: 396539 Color: 4
Size: 306867 Color: 12
Size: 296583 Color: 9

Bin 582: 13 of cap free
Amount of items: 3
Items: 
Size: 363046 Color: 17
Size: 344471 Color: 17
Size: 292471 Color: 16

Bin 583: 13 of cap free
Amount of items: 3
Items: 
Size: 372159 Color: 8
Size: 364691 Color: 10
Size: 263138 Color: 17

Bin 584: 13 of cap free
Amount of items: 3
Items: 
Size: 382803 Color: 7
Size: 351795 Color: 10
Size: 265390 Color: 18

Bin 585: 13 of cap free
Amount of items: 3
Items: 
Size: 401230 Color: 11
Size: 315817 Color: 5
Size: 282941 Color: 10

Bin 586: 13 of cap free
Amount of items: 3
Items: 
Size: 414054 Color: 18
Size: 335164 Color: 9
Size: 250770 Color: 3

Bin 587: 14 of cap free
Amount of items: 3
Items: 
Size: 357979 Color: 0
Size: 343672 Color: 14
Size: 298336 Color: 11

Bin 588: 14 of cap free
Amount of items: 3
Items: 
Size: 363171 Color: 4
Size: 345485 Color: 11
Size: 291331 Color: 14

Bin 589: 15 of cap free
Amount of items: 3
Items: 
Size: 362007 Color: 14
Size: 319367 Color: 16
Size: 318612 Color: 13

Bin 590: 15 of cap free
Amount of items: 3
Items: 
Size: 372453 Color: 12
Size: 356799 Color: 14
Size: 270734 Color: 0

Bin 591: 15 of cap free
Amount of items: 3
Items: 
Size: 381965 Color: 10
Size: 320561 Color: 19
Size: 297460 Color: 0

Bin 592: 15 of cap free
Amount of items: 3
Items: 
Size: 400174 Color: 10
Size: 348715 Color: 18
Size: 251097 Color: 9

Bin 593: 16 of cap free
Amount of items: 3
Items: 
Size: 435698 Color: 7
Size: 311388 Color: 9
Size: 252899 Color: 16

Bin 594: 17 of cap free
Amount of items: 3
Items: 
Size: 362428 Color: 8
Size: 359617 Color: 18
Size: 277939 Color: 4

Bin 595: 17 of cap free
Amount of items: 3
Items: 
Size: 376156 Color: 0
Size: 335036 Color: 7
Size: 288792 Color: 9

Bin 596: 19 of cap free
Amount of items: 3
Items: 
Size: 359022 Color: 9
Size: 355547 Color: 18
Size: 285413 Color: 18

Bin 597: 19 of cap free
Amount of items: 3
Items: 
Size: 361983 Color: 9
Size: 342397 Color: 15
Size: 295602 Color: 5

Bin 598: 19 of cap free
Amount of items: 3
Items: 
Size: 385843 Color: 3
Size: 346527 Color: 0
Size: 267612 Color: 14

Bin 599: 20 of cap free
Amount of items: 3
Items: 
Size: 391778 Color: 15
Size: 336176 Color: 13
Size: 272027 Color: 0

Bin 600: 22 of cap free
Amount of items: 3
Items: 
Size: 371058 Color: 6
Size: 335381 Color: 12
Size: 293540 Color: 13

Bin 601: 25 of cap free
Amount of items: 3
Items: 
Size: 353112 Color: 13
Size: 344729 Color: 19
Size: 302135 Color: 5

Bin 602: 25 of cap free
Amount of items: 3
Items: 
Size: 366815 Color: 10
Size: 330431 Color: 0
Size: 302730 Color: 13

Bin 603: 25 of cap free
Amount of items: 3
Items: 
Size: 380430 Color: 3
Size: 343848 Color: 9
Size: 275698 Color: 3

Bin 604: 26 of cap free
Amount of items: 3
Items: 
Size: 375318 Color: 19
Size: 333647 Color: 9
Size: 291010 Color: 15

Bin 605: 26 of cap free
Amount of items: 3
Items: 
Size: 399486 Color: 4
Size: 317562 Color: 0
Size: 282927 Color: 10

Bin 606: 28 of cap free
Amount of items: 3
Items: 
Size: 393106 Color: 13
Size: 345501 Color: 9
Size: 261366 Color: 15

Bin 607: 31 of cap free
Amount of items: 3
Items: 
Size: 362356 Color: 13
Size: 350494 Color: 5
Size: 287120 Color: 11

Bin 608: 31 of cap free
Amount of items: 3
Items: 
Size: 369934 Color: 9
Size: 317537 Color: 7
Size: 312499 Color: 10

Bin 609: 40 of cap free
Amount of items: 3
Items: 
Size: 358286 Color: 6
Size: 334288 Color: 2
Size: 307387 Color: 11

Bin 610: 41 of cap free
Amount of items: 3
Items: 
Size: 370150 Color: 16
Size: 353542 Color: 10
Size: 276268 Color: 16

Bin 611: 42 of cap free
Amount of items: 3
Items: 
Size: 359909 Color: 15
Size: 339933 Color: 19
Size: 300117 Color: 1

Bin 612: 42 of cap free
Amount of items: 3
Items: 
Size: 378024 Color: 11
Size: 329427 Color: 12
Size: 292508 Color: 19

Bin 613: 43 of cap free
Amount of items: 3
Items: 
Size: 354285 Color: 12
Size: 340860 Color: 4
Size: 304813 Color: 19

Bin 614: 45 of cap free
Amount of items: 3
Items: 
Size: 364821 Color: 12
Size: 332172 Color: 7
Size: 302963 Color: 1

Bin 615: 48 of cap free
Amount of items: 3
Items: 
Size: 373055 Color: 11
Size: 338254 Color: 16
Size: 288644 Color: 9

Bin 616: 49 of cap free
Amount of items: 3
Items: 
Size: 366964 Color: 1
Size: 352689 Color: 11
Size: 280299 Color: 8

Bin 617: 57 of cap free
Amount of items: 3
Items: 
Size: 356099 Color: 12
Size: 332994 Color: 18
Size: 310851 Color: 15

Bin 618: 58 of cap free
Amount of items: 3
Items: 
Size: 347968 Color: 11
Size: 341187 Color: 18
Size: 310788 Color: 15

Bin 619: 59 of cap free
Amount of items: 3
Items: 
Size: 361781 Color: 7
Size: 337175 Color: 12
Size: 300986 Color: 17

Bin 620: 66 of cap free
Amount of items: 3
Items: 
Size: 373427 Color: 1
Size: 328236 Color: 18
Size: 298272 Color: 9

Bin 621: 70 of cap free
Amount of items: 3
Items: 
Size: 343570 Color: 10
Size: 342659 Color: 14
Size: 313702 Color: 0

Bin 622: 78 of cap free
Amount of items: 3
Items: 
Size: 361854 Color: 0
Size: 322806 Color: 19
Size: 315263 Color: 8

Bin 623: 84 of cap free
Amount of items: 3
Items: 
Size: 357554 Color: 9
Size: 355204 Color: 8
Size: 287159 Color: 2

Bin 624: 88 of cap free
Amount of items: 3
Items: 
Size: 362806 Color: 3
Size: 332360 Color: 12
Size: 304747 Color: 7

Bin 625: 98 of cap free
Amount of items: 3
Items: 
Size: 369052 Color: 9
Size: 359313 Color: 10
Size: 271538 Color: 19

Bin 626: 102 of cap free
Amount of items: 3
Items: 
Size: 366211 Color: 14
Size: 337232 Color: 12
Size: 296456 Color: 8

Bin 627: 102 of cap free
Amount of items: 3
Items: 
Size: 370318 Color: 7
Size: 353679 Color: 9
Size: 275902 Color: 17

Bin 628: 108 of cap free
Amount of items: 3
Items: 
Size: 356794 Color: 2
Size: 354510 Color: 12
Size: 288589 Color: 10

Bin 629: 129 of cap free
Amount of items: 3
Items: 
Size: 359060 Color: 11
Size: 336153 Color: 16
Size: 304659 Color: 14

Bin 630: 138 of cap free
Amount of items: 3
Items: 
Size: 360654 Color: 15
Size: 326679 Color: 6
Size: 312530 Color: 19

Bin 631: 223 of cap free
Amount of items: 3
Items: 
Size: 353305 Color: 13
Size: 329380 Color: 19
Size: 317093 Color: 16

Bin 632: 224 of cap free
Amount of items: 3
Items: 
Size: 357866 Color: 1
Size: 348778 Color: 4
Size: 293133 Color: 2

Bin 633: 311 of cap free
Amount of items: 3
Items: 
Size: 357701 Color: 8
Size: 349929 Color: 10
Size: 292060 Color: 14

Bin 634: 317 of cap free
Amount of items: 3
Items: 
Size: 351932 Color: 19
Size: 331863 Color: 0
Size: 315889 Color: 5

Bin 635: 322 of cap free
Amount of items: 3
Items: 
Size: 356734 Color: 19
Size: 330268 Color: 4
Size: 312677 Color: 11

Bin 636: 338 of cap free
Amount of items: 3
Items: 
Size: 358589 Color: 7
Size: 335531 Color: 10
Size: 305543 Color: 16

Bin 637: 495 of cap free
Amount of items: 3
Items: 
Size: 359690 Color: 7
Size: 323314 Color: 8
Size: 316502 Color: 3

Bin 638: 501 of cap free
Amount of items: 3
Items: 
Size: 357743 Color: 6
Size: 339332 Color: 8
Size: 302425 Color: 5

Bin 639: 564 of cap free
Amount of items: 3
Items: 
Size: 359846 Color: 2
Size: 339265 Color: 7
Size: 300326 Color: 12

Bin 640: 584 of cap free
Amount of items: 3
Items: 
Size: 348920 Color: 7
Size: 341933 Color: 10
Size: 308564 Color: 16

Bin 641: 985 of cap free
Amount of items: 3
Items: 
Size: 347609 Color: 11
Size: 334941 Color: 9
Size: 316466 Color: 2

Bin 642: 1608 of cap free
Amount of items: 3
Items: 
Size: 347515 Color: 8
Size: 336944 Color: 14
Size: 313934 Color: 13

Bin 643: 1715 of cap free
Amount of items: 3
Items: 
Size: 351354 Color: 17
Size: 332884 Color: 11
Size: 314048 Color: 15

Bin 644: 1758 of cap free
Amount of items: 3
Items: 
Size: 353171 Color: 19
Size: 323002 Color: 4
Size: 322070 Color: 5

Bin 645: 1766 of cap free
Amount of items: 3
Items: 
Size: 343193 Color: 16
Size: 342948 Color: 15
Size: 312094 Color: 1

Bin 646: 1820 of cap free
Amount of items: 3
Items: 
Size: 356611 Color: 16
Size: 322808 Color: 13
Size: 318762 Color: 0

Bin 647: 2272 of cap free
Amount of items: 3
Items: 
Size: 350998 Color: 3
Size: 338936 Color: 16
Size: 307795 Color: 12

Bin 648: 2823 of cap free
Amount of items: 2
Items: 
Size: 498709 Color: 14
Size: 498469 Color: 17

Bin 649: 3819 of cap free
Amount of items: 2
Items: 
Size: 498469 Color: 13
Size: 497713 Color: 2

Bin 650: 4471 of cap free
Amount of items: 3
Items: 
Size: 351218 Color: 10
Size: 322537 Color: 2
Size: 321775 Color: 1

Bin 651: 4966 of cap free
Amount of items: 2
Items: 
Size: 497633 Color: 15
Size: 497402 Color: 13

Bin 652: 6715 of cap free
Amount of items: 3
Items: 
Size: 348736 Color: 8
Size: 322599 Color: 0
Size: 321951 Color: 10

Bin 653: 7789 of cap free
Amount of items: 3
Items: 
Size: 356653 Color: 15
Size: 323038 Color: 12
Size: 312521 Color: 7

Bin 654: 18896 of cap free
Amount of items: 3
Items: 
Size: 350962 Color: 17
Size: 350761 Color: 19
Size: 279382 Color: 2

Bin 655: 20998 of cap free
Amount of items: 3
Items: 
Size: 351368 Color: 17
Size: 347080 Color: 7
Size: 280555 Color: 12

Bin 656: 21274 of cap free
Amount of items: 3
Items: 
Size: 351240 Color: 1
Size: 349892 Color: 15
Size: 277595 Color: 13

Bin 657: 22821 of cap free
Amount of items: 3
Items: 
Size: 348620 Color: 19
Size: 316396 Color: 0
Size: 312164 Color: 5

Bin 658: 23479 of cap free
Amount of items: 3
Items: 
Size: 348964 Color: 5
Size: 347038 Color: 7
Size: 280520 Color: 9

Bin 659: 35801 of cap free
Amount of items: 3
Items: 
Size: 347474 Color: 6
Size: 346870 Color: 9
Size: 269856 Color: 13

Bin 660: 37274 of cap free
Amount of items: 3
Items: 
Size: 349521 Color: 9
Size: 343238 Color: 4
Size: 269968 Color: 11

Bin 661: 37966 of cap free
Amount of items: 3
Items: 
Size: 347719 Color: 18
Size: 346292 Color: 7
Size: 268024 Color: 8

Bin 662: 43213 of cap free
Amount of items: 3
Items: 
Size: 346771 Color: 1
Size: 346615 Color: 11
Size: 263402 Color: 4

Bin 663: 44458 of cap free
Amount of items: 3
Items: 
Size: 345908 Color: 6
Size: 345731 Color: 17
Size: 263904 Color: 5

Bin 664: 46530 of cap free
Amount of items: 3
Items: 
Size: 345585 Color: 7
Size: 345229 Color: 18
Size: 262657 Color: 8

Bin 665: 52473 of cap free
Amount of items: 3
Items: 
Size: 343597 Color: 14
Size: 343457 Color: 2
Size: 260474 Color: 10

Bin 666: 64091 of cap free
Amount of items: 3
Items: 
Size: 342145 Color: 15
Size: 341897 Color: 16
Size: 251868 Color: 4

Bin 667: 235047 of cap free
Amount of items: 3
Items: 
Size: 256332 Color: 7
Size: 255247 Color: 5
Size: 253375 Color: 11

Bin 668: 246549 of cap free
Amount of items: 3
Items: 
Size: 252391 Color: 8
Size: 250786 Color: 10
Size: 250275 Color: 13

Total size: 667000667
Total free space: 1000001

