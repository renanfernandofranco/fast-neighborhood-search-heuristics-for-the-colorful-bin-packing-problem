Capicity Bin: 1860
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 17
Items: 
Size: 426 Color: 0
Size: 190 Color: 0
Size: 126 Color: 0
Size: 120 Color: 0
Size: 106 Color: 1
Size: 106 Color: 1
Size: 106 Color: 1
Size: 96 Color: 0
Size: 88 Color: 0
Size: 76 Color: 0
Size: 76 Color: 0
Size: 64 Color: 1
Size: 64 Color: 1
Size: 60 Color: 1
Size: 56 Color: 1
Size: 56 Color: 1
Size: 44 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 1
Size: 566 Color: 0
Size: 32 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 0
Size: 473 Color: 1
Size: 154 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1371 Color: 1
Size: 337 Color: 0
Size: 152 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1398 Color: 0
Size: 386 Color: 1
Size: 76 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 1
Size: 265 Color: 1
Size: 157 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 1
Size: 333 Color: 1
Size: 66 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 0
Size: 208 Color: 1
Size: 174 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1511 Color: 1
Size: 213 Color: 1
Size: 136 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 1
Size: 271 Color: 0
Size: 58 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 1
Size: 213 Color: 0
Size: 108 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 0
Size: 208 Color: 0
Size: 66 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 0
Size: 207 Color: 1
Size: 40 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 0
Size: 152 Color: 0
Size: 54 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1637 Color: 1
Size: 171 Color: 0
Size: 52 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 0
Size: 189 Color: 0
Size: 16 Color: 1

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 1674 Color: 0
Size: 178 Color: 1
Size: 4 Color: 1
Size: 4 Color: 0

Bin 18: 1 of cap free
Amount of items: 5
Items: 
Size: 931 Color: 0
Size: 451 Color: 0
Size: 391 Color: 1
Size: 46 Color: 1
Size: 40 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 1
Size: 771 Color: 1
Size: 54 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1105 Color: 0
Size: 628 Color: 0
Size: 126 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1284 Color: 0
Size: 537 Color: 1
Size: 38 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1393 Color: 0
Size: 432 Color: 1
Size: 34 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1435 Color: 0
Size: 388 Color: 1
Size: 36 Color: 0

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 0
Size: 322 Color: 1
Size: 80 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 1
Size: 294 Color: 0
Size: 30 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 0
Size: 218 Color: 1
Size: 36 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1635 Color: 1
Size: 158 Color: 0
Size: 66 Color: 1

Bin 28: 1 of cap free
Amount of items: 4
Items: 
Size: 1673 Color: 0
Size: 130 Color: 1
Size: 28 Color: 1
Size: 28 Color: 0

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 935 Color: 1
Size: 769 Color: 1
Size: 154 Color: 0

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1097 Color: 0
Size: 637 Color: 0
Size: 124 Color: 1

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 1
Size: 690 Color: 0
Size: 50 Color: 1

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1217 Color: 0
Size: 641 Color: 1

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1567 Color: 0
Size: 291 Color: 1

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 0
Size: 288 Color: 1

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1577 Color: 0
Size: 281 Color: 1

Bin 36: 3 of cap free
Amount of items: 4
Items: 
Size: 1321 Color: 1
Size: 474 Color: 0
Size: 40 Color: 1
Size: 22 Color: 0

Bin 37: 4 of cap free
Amount of items: 4
Items: 
Size: 1168 Color: 0
Size: 622 Color: 1
Size: 40 Color: 0
Size: 26 Color: 1

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 1
Size: 562 Color: 0
Size: 48 Color: 0

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 0
Size: 271 Color: 1
Size: 42 Color: 0

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 1322 Color: 1
Size: 533 Color: 0

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1453 Color: 1
Size: 354 Color: 1
Size: 48 Color: 0

Bin 42: 6 of cap free
Amount of items: 2
Items: 
Size: 1634 Color: 0
Size: 220 Color: 1

Bin 43: 7 of cap free
Amount of items: 2
Items: 
Size: 1530 Color: 0
Size: 323 Color: 1

Bin 44: 7 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 0
Size: 195 Color: 1

Bin 45: 8 of cap free
Amount of items: 2
Items: 
Size: 1311 Color: 1
Size: 541 Color: 0

Bin 46: 8 of cap free
Amount of items: 2
Items: 
Size: 1350 Color: 1
Size: 502 Color: 0

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1510 Color: 0
Size: 341 Color: 1

Bin 48: 11 of cap free
Amount of items: 2
Items: 
Size: 1101 Color: 1
Size: 748 Color: 0

Bin 49: 11 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 0
Size: 275 Color: 1
Size: 24 Color: 0

Bin 50: 11 of cap free
Amount of items: 2
Items: 
Size: 1604 Color: 1
Size: 245 Color: 0

Bin 51: 12 of cap free
Amount of items: 4
Items: 
Size: 1093 Color: 0
Size: 409 Color: 1
Size: 262 Color: 0
Size: 84 Color: 1

Bin 52: 12 of cap free
Amount of items: 3
Items: 
Size: 1221 Color: 0
Size: 523 Color: 1
Size: 104 Color: 0

Bin 53: 14 of cap free
Amount of items: 2
Items: 
Size: 1213 Color: 0
Size: 633 Color: 1

Bin 54: 16 of cap free
Amount of items: 3
Items: 
Size: 939 Color: 0
Size: 775 Color: 1
Size: 130 Color: 0

Bin 55: 16 of cap free
Amount of items: 2
Items: 
Size: 1573 Color: 1
Size: 271 Color: 0

Bin 56: 16 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 1
Size: 242 Color: 0

Bin 57: 17 of cap free
Amount of items: 2
Items: 
Size: 1186 Color: 0
Size: 657 Color: 1

Bin 58: 18 of cap free
Amount of items: 3
Items: 
Size: 1077 Color: 1
Size: 653 Color: 0
Size: 112 Color: 1

Bin 59: 18 of cap free
Amount of items: 2
Items: 
Size: 1211 Color: 0
Size: 631 Color: 1

Bin 60: 18 of cap free
Amount of items: 2
Items: 
Size: 1473 Color: 1
Size: 369 Color: 0

Bin 61: 19 of cap free
Amount of items: 2
Items: 
Size: 1563 Color: 0
Size: 278 Color: 1

Bin 62: 21 of cap free
Amount of items: 2
Items: 
Size: 1609 Color: 0
Size: 230 Color: 1

Bin 63: 23 of cap free
Amount of items: 2
Items: 
Size: 1600 Color: 1
Size: 237 Color: 0

Bin 64: 26 of cap free
Amount of items: 3
Items: 
Size: 934 Color: 0
Size: 774 Color: 1
Size: 126 Color: 0

Bin 65: 30 of cap free
Amount of items: 3
Items: 
Size: 1073 Color: 1
Size: 541 Color: 0
Size: 216 Color: 0

Bin 66: 1456 of cap free
Amount of items: 8
Items: 
Size: 68 Color: 0
Size: 68 Color: 0
Size: 56 Color: 1
Size: 48 Color: 1
Size: 48 Color: 1
Size: 40 Color: 0
Size: 40 Color: 0
Size: 36 Color: 1

Total size: 120900
Total free space: 1860

