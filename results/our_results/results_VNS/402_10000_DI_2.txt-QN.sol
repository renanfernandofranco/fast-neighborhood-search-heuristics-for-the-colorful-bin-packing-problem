Capicity Bin: 7472
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 14
Items: 
Size: 736 Color: 147
Size: 730 Color: 146
Size: 724 Color: 145
Size: 686 Color: 142
Size: 674 Color: 140
Size: 620 Color: 135
Size: 620 Color: 134
Size: 538 Color: 123
Size: 528 Color: 120
Size: 528 Color: 119
Size: 272 Color: 75
Size: 272 Color: 74
Size: 272 Color: 73
Size: 272 Color: 72

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 313
Size: 1226 Color: 197
Size: 986 Color: 177

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5508 Color: 322
Size: 1876 Color: 227
Size: 88 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5612 Color: 327
Size: 1676 Color: 220
Size: 184 Color: 39

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5716 Color: 331
Size: 1214 Color: 195
Size: 542 Color: 125

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5727 Color: 332
Size: 1241 Color: 199
Size: 504 Color: 118

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 335
Size: 1102 Color: 187
Size: 550 Color: 129

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5892 Color: 340
Size: 1556 Color: 214
Size: 24 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 342
Size: 1324 Color: 201
Size: 168 Color: 33

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5985 Color: 343
Size: 1221 Color: 196
Size: 266 Color: 71

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6018 Color: 347
Size: 1374 Color: 205
Size: 80 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 351
Size: 986 Color: 176
Size: 370 Color: 97

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6149 Color: 353
Size: 755 Color: 149
Size: 568 Color: 131

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6150 Color: 354
Size: 678 Color: 141
Size: 644 Color: 138

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6166 Color: 356
Size: 1090 Color: 185
Size: 216 Color: 55

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6195 Color: 357
Size: 1065 Color: 183
Size: 212 Color: 53

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 360
Size: 804 Color: 154
Size: 400 Color: 100

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6273 Color: 361
Size: 803 Color: 153
Size: 396 Color: 99

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 362
Size: 870 Color: 162
Size: 328 Color: 88

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6307 Color: 365
Size: 1001 Color: 179
Size: 164 Color: 30

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 366
Size: 708 Color: 143
Size: 456 Color: 109

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6314 Color: 367
Size: 622 Color: 137
Size: 536 Color: 122

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 368
Size: 708 Color: 144
Size: 448 Color: 108

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6323 Color: 369
Size: 931 Color: 168
Size: 218 Color: 56

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6355 Color: 372
Size: 925 Color: 166
Size: 192 Color: 46

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 375
Size: 620 Color: 136
Size: 424 Color: 107

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 377
Size: 861 Color: 161
Size: 172 Color: 36

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6446 Color: 378
Size: 538 Color: 124
Size: 488 Color: 117

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6447 Color: 379
Size: 855 Color: 160
Size: 170 Color: 34

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 389
Size: 765 Color: 150
Size: 152 Color: 23

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 391
Size: 742 Color: 148
Size: 144 Color: 19

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6592 Color: 392
Size: 544 Color: 127
Size: 336 Color: 89

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6628 Color: 396
Size: 478 Color: 116
Size: 366 Color: 93

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6686 Color: 399
Size: 542 Color: 126
Size: 244 Color: 65

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 401
Size: 532 Color: 121
Size: 232 Color: 61

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 402
Size: 560 Color: 130
Size: 194 Color: 47

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 4171 Color: 281
Size: 3084 Color: 264
Size: 216 Color: 54

Bin 38: 1 of cap free
Amount of items: 5
Items: 
Size: 4219 Color: 285
Size: 2628 Color: 250
Size: 224 Color: 60
Size: 202 Color: 52
Size: 198 Color: 51

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 5630 Color: 329
Size: 1841 Color: 224

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5743 Color: 333
Size: 1538 Color: 212
Size: 190 Color: 42

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 352
Size: 1339 Color: 203

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6155 Color: 355
Size: 972 Color: 175
Size: 344 Color: 90

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6331 Color: 370
Size: 1140 Color: 191

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6347 Color: 371
Size: 1124 Color: 189

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 383
Size: 616 Color: 133
Size: 368 Color: 95

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6650 Color: 397
Size: 821 Color: 156

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6662 Color: 398
Size: 809 Color: 155

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 4599 Color: 295
Size: 2711 Color: 257
Size: 160 Color: 27

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 4623 Color: 297
Size: 2691 Color: 255
Size: 156 Color: 25

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 5090 Color: 309
Size: 2380 Color: 245

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5241 Color: 312
Size: 2125 Color: 238
Size: 104 Color: 9

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 5468 Color: 320
Size: 2002 Color: 231

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 5555 Color: 325
Size: 1783 Color: 223
Size: 132 Color: 13

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 334
Size: 1362 Color: 204
Size: 304 Color: 81

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5976 Color: 341
Size: 836 Color: 157
Size: 658 Color: 139

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6002 Color: 345
Size: 1468 Color: 210

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6430 Color: 376
Size: 1040 Color: 182

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 6542 Color: 388
Size: 928 Color: 167

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 6692 Color: 400
Size: 778 Color: 151

Bin 60: 3 of cap free
Amount of items: 5
Items: 
Size: 3740 Color: 275
Size: 1550 Color: 213
Size: 1325 Color: 202
Size: 608 Color: 132
Size: 246 Color: 67

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 5535 Color: 324
Size: 1934 Color: 230

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 5826 Color: 336
Size: 1643 Color: 219

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 5838 Color: 337
Size: 1631 Color: 218

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 6503 Color: 384
Size: 966 Color: 173

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6567 Color: 390
Size: 902 Color: 165

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6593 Color: 393
Size: 876 Color: 164

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 6598 Color: 394
Size: 871 Color: 163

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 293
Size: 2776 Color: 262
Size: 160 Color: 29

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 4908 Color: 302
Size: 2424 Color: 247
Size: 136 Color: 16

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 5993 Color: 344
Size: 1475 Color: 211

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 6509 Color: 385
Size: 959 Color: 171

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 4576 Color: 294
Size: 2731 Color: 259
Size: 160 Color: 28

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6476 Color: 382
Size: 991 Color: 178

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 6516 Color: 386
Size: 951 Color: 170

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 395
Size: 855 Color: 159

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 5333 Color: 316
Size: 2061 Color: 236
Size: 72 Color: 4

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 5867 Color: 338
Size: 1599 Color: 216

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 6025 Color: 348
Size: 1441 Color: 208

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6259 Color: 359
Size: 1207 Color: 194

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6363 Color: 373
Size: 1103 Color: 188

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 4620 Color: 296
Size: 2685 Color: 254
Size: 160 Color: 26

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 5442 Color: 319
Size: 2023 Color: 233

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 5703 Color: 330
Size: 1762 Color: 222

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 6290 Color: 364
Size: 1175 Color: 193

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 6454 Color: 380
Size: 1011 Color: 181

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 6463 Color: 381
Size: 1002 Color: 180

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 387
Size: 939 Color: 169

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 5883 Color: 339
Size: 1581 Color: 215

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 6009 Color: 346
Size: 1455 Color: 209

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 6068 Color: 349
Size: 1396 Color: 207

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 6219 Color: 358
Size: 1244 Color: 200

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 6390 Color: 374
Size: 1073 Color: 184

Bin 93: 10 of cap free
Amount of items: 3
Items: 
Size: 4764 Color: 301
Size: 2560 Color: 249
Size: 138 Color: 17

Bin 94: 10 of cap free
Amount of items: 3
Items: 
Size: 5074 Color: 308
Size: 2260 Color: 240
Size: 128 Color: 11

Bin 95: 12 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 311
Size: 1694 Color: 221
Size: 546 Color: 128

Bin 96: 13 of cap free
Amount of items: 2
Items: 
Size: 4195 Color: 283
Size: 3264 Color: 271

Bin 97: 13 of cap free
Amount of items: 2
Items: 
Size: 5575 Color: 326
Size: 1884 Color: 228

Bin 98: 14 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 307
Size: 2302 Color: 242
Size: 128 Color: 12

Bin 99: 14 of cap free
Amount of items: 2
Items: 
Size: 5614 Color: 328
Size: 1844 Color: 225

Bin 100: 15 of cap free
Amount of items: 3
Items: 
Size: 4639 Color: 298
Size: 2666 Color: 253
Size: 152 Color: 24

Bin 101: 15 of cap free
Amount of items: 2
Items: 
Size: 6077 Color: 350
Size: 1380 Color: 206

Bin 102: 16 of cap free
Amount of items: 7
Items: 
Size: 3737 Color: 273
Size: 971 Color: 174
Size: 964 Color: 172
Size: 790 Color: 152
Size: 474 Color: 115
Size: 264 Color: 70
Size: 256 Color: 69

Bin 103: 16 of cap free
Amount of items: 3
Items: 
Size: 5378 Color: 318
Size: 2014 Color: 232
Size: 64 Color: 3

Bin 104: 17 of cap free
Amount of items: 2
Items: 
Size: 6283 Color: 363
Size: 1172 Color: 192

Bin 105: 18 of cap free
Amount of items: 3
Items: 
Size: 4923 Color: 303
Size: 2395 Color: 246
Size: 136 Color: 15

Bin 106: 19 of cap free
Amount of items: 2
Items: 
Size: 5001 Color: 305
Size: 2452 Color: 248

Bin 107: 22 of cap free
Amount of items: 5
Items: 
Size: 3738 Color: 274
Size: 1233 Color: 198
Size: 1132 Color: 190
Size: 1099 Color: 186
Size: 248 Color: 68

Bin 108: 22 of cap free
Amount of items: 4
Items: 
Size: 5501 Color: 321
Size: 1861 Color: 226
Size: 48 Color: 2
Size: 40 Color: 1

Bin 109: 25 of cap free
Amount of items: 3
Items: 
Size: 5021 Color: 306
Size: 2290 Color: 241
Size: 136 Color: 14

Bin 110: 26 of cap free
Amount of items: 3
Items: 
Size: 4981 Color: 304
Size: 1615 Color: 217
Size: 850 Color: 158

Bin 111: 33 of cap free
Amount of items: 2
Items: 
Size: 5362 Color: 317
Size: 2077 Color: 237

Bin 112: 33 of cap free
Amount of items: 2
Items: 
Size: 5515 Color: 323
Size: 1924 Color: 229

Bin 113: 40 of cap free
Amount of items: 3
Items: 
Size: 5172 Color: 310
Size: 2140 Color: 239
Size: 120 Color: 10

Bin 114: 51 of cap free
Amount of items: 2
Items: 
Size: 3741 Color: 276
Size: 3680 Color: 272

Bin 115: 55 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 280
Size: 3113 Color: 268
Size: 220 Color: 57

Bin 116: 56 of cap free
Amount of items: 3
Items: 
Size: 4420 Color: 292
Size: 2828 Color: 263
Size: 168 Color: 31

Bin 117: 59 of cap free
Amount of items: 3
Items: 
Size: 5281 Color: 315
Size: 2044 Color: 235
Size: 88 Color: 6

Bin 118: 76 of cap free
Amount of items: 3
Items: 
Size: 5265 Color: 314
Size: 2043 Color: 234
Size: 88 Color: 8

Bin 119: 82 of cap free
Amount of items: 4
Items: 
Size: 4290 Color: 290
Size: 2740 Color: 260
Size: 180 Color: 38
Size: 180 Color: 37

Bin 120: 83 of cap free
Amount of items: 4
Items: 
Size: 4726 Color: 300
Size: 2375 Color: 244
Size: 144 Color: 20
Size: 144 Color: 18

Bin 121: 89 of cap free
Amount of items: 4
Items: 
Size: 4292 Color: 291
Size: 2751 Color: 261
Size: 172 Color: 35
Size: 168 Color: 32

Bin 122: 97 of cap free
Amount of items: 2
Items: 
Size: 4211 Color: 284
Size: 3164 Color: 270

Bin 123: 99 of cap free
Amount of items: 4
Items: 
Size: 4710 Color: 299
Size: 2361 Color: 243
Size: 152 Color: 22
Size: 150 Color: 21

Bin 124: 107 of cap free
Amount of items: 4
Items: 
Size: 4274 Color: 289
Size: 2719 Color: 258
Size: 186 Color: 41
Size: 186 Color: 40

Bin 125: 132 of cap free
Amount of items: 4
Items: 
Size: 3754 Color: 278
Size: 3106 Color: 266
Size: 240 Color: 63
Size: 240 Color: 62

Bin 126: 136 of cap free
Amount of items: 4
Items: 
Size: 3746 Color: 277
Size: 3102 Color: 265
Size: 246 Color: 66
Size: 242 Color: 64

Bin 127: 140 of cap free
Amount of items: 4
Items: 
Size: 4251 Color: 288
Size: 2699 Color: 256
Size: 192 Color: 44
Size: 190 Color: 43

Bin 128: 141 of cap free
Amount of items: 4
Items: 
Size: 3780 Color: 279
Size: 3111 Color: 267
Size: 220 Color: 59
Size: 220 Color: 58

Bin 129: 170 of cap free
Amount of items: 2
Items: 
Size: 4188 Color: 282
Size: 3114 Color: 269

Bin 130: 187 of cap free
Amount of items: 4
Items: 
Size: 4243 Color: 287
Size: 2654 Color: 252
Size: 196 Color: 48
Size: 192 Color: 45

Bin 131: 191 of cap free
Amount of items: 4
Items: 
Size: 4235 Color: 286
Size: 2652 Color: 251
Size: 198 Color: 50
Size: 196 Color: 49

Bin 132: 222 of cap free
Amount of items: 19
Items: 
Size: 472 Color: 114
Size: 472 Color: 113
Size: 472 Color: 112
Size: 472 Color: 111
Size: 460 Color: 110
Size: 424 Color: 106
Size: 424 Color: 105
Size: 416 Color: 104
Size: 414 Color: 103
Size: 410 Color: 102
Size: 408 Color: 101
Size: 318 Color: 84
Size: 316 Color: 83
Size: 308 Color: 82
Size: 304 Color: 80
Size: 294 Color: 79
Size: 290 Color: 78
Size: 288 Color: 77
Size: 288 Color: 76

Bin 133: 4680 of cap free
Amount of items: 8
Items: 
Size: 376 Color: 98
Size: 368 Color: 96
Size: 368 Color: 94
Size: 356 Color: 92
Size: 348 Color: 91
Size: 328 Color: 87
Size: 326 Color: 86
Size: 322 Color: 85

Total size: 986304
Total free space: 7472

