Capicity Bin: 2020
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 1011 Color: 0
Size: 506 Color: 1
Size: 267 Color: 0
Size: 142 Color: 1
Size: 58 Color: 1
Size: 36 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1013 Color: 0
Size: 713 Color: 0
Size: 198 Color: 1
Size: 52 Color: 1
Size: 44 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1014 Color: 0
Size: 515 Color: 1
Size: 491 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 1
Size: 666 Color: 0
Size: 64 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1362 Color: 1
Size: 398 Color: 0
Size: 168 Color: 0
Size: 92 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1386 Color: 1
Size: 574 Color: 1
Size: 60 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 0
Size: 322 Color: 0
Size: 100 Color: 1

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 1623 Color: 0
Size: 365 Color: 1
Size: 16 Color: 1
Size: 16 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 0
Size: 329 Color: 0
Size: 64 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 1
Size: 297 Color: 0
Size: 64 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 1
Size: 286 Color: 0
Size: 56 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 0
Size: 190 Color: 0
Size: 144 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 0
Size: 202 Color: 1
Size: 72 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 1
Size: 214 Color: 0
Size: 48 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1766 Color: 0
Size: 134 Color: 1
Size: 120 Color: 0

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1078 Color: 0
Size: 671 Color: 1
Size: 270 Color: 0

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 1186 Color: 0
Size: 833 Color: 1

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1222 Color: 0
Size: 721 Color: 0
Size: 76 Color: 1

Bin 19: 1 of cap free
Amount of items: 4
Items: 
Size: 1378 Color: 1
Size: 294 Color: 0
Size: 287 Color: 0
Size: 60 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 1
Size: 282 Color: 0
Size: 220 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 0
Size: 164 Color: 1
Size: 156 Color: 0

Bin 22: 2 of cap free
Amount of items: 4
Items: 
Size: 1017 Color: 1
Size: 721 Color: 0
Size: 212 Color: 1
Size: 68 Color: 0

Bin 23: 2 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 1
Size: 831 Color: 0
Size: 166 Color: 1

Bin 24: 2 of cap free
Amount of items: 4
Items: 
Size: 1155 Color: 0
Size: 775 Color: 1
Size: 44 Color: 1
Size: 44 Color: 0

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1213 Color: 1
Size: 673 Color: 0
Size: 132 Color: 1

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 1
Size: 405 Color: 0
Size: 132 Color: 0

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1706 Color: 1
Size: 312 Color: 0

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 1
Size: 224 Color: 0

Bin 29: 3 of cap free
Amount of items: 2
Items: 
Size: 1403 Color: 1
Size: 614 Color: 0

Bin 30: 3 of cap free
Amount of items: 2
Items: 
Size: 1407 Color: 1
Size: 610 Color: 0

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 1
Size: 451 Color: 1
Size: 56 Color: 0

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 0
Size: 363 Color: 1
Size: 36 Color: 1

Bin 33: 4 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 1
Size: 331 Color: 0
Size: 20 Color: 0

Bin 34: 4 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 0
Size: 263 Color: 1
Size: 22 Color: 1

Bin 35: 4 of cap free
Amount of items: 4
Items: 
Size: 1778 Color: 1
Size: 222 Color: 0
Size: 8 Color: 1
Size: 8 Color: 0

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1786 Color: 1
Size: 230 Color: 0

Bin 37: 5 of cap free
Amount of items: 2
Items: 
Size: 1317 Color: 1
Size: 698 Color: 0

Bin 38: 5 of cap free
Amount of items: 2
Items: 
Size: 1661 Color: 0
Size: 354 Color: 1

Bin 39: 5 of cap free
Amount of items: 2
Items: 
Size: 1677 Color: 0
Size: 338 Color: 1

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 1772 Color: 0
Size: 241 Color: 1

Bin 41: 9 of cap free
Amount of items: 2
Items: 
Size: 1585 Color: 1
Size: 426 Color: 0

Bin 42: 10 of cap free
Amount of items: 2
Items: 
Size: 1169 Color: 0
Size: 841 Color: 1

Bin 43: 10 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 1
Size: 509 Color: 0
Size: 102 Color: 0

Bin 44: 12 of cap free
Amount of items: 2
Items: 
Size: 1546 Color: 1
Size: 462 Color: 0

Bin 45: 12 of cap free
Amount of items: 2
Items: 
Size: 1634 Color: 1
Size: 374 Color: 0

Bin 46: 13 of cap free
Amount of items: 2
Items: 
Size: 1670 Color: 0
Size: 337 Color: 1

Bin 47: 14 of cap free
Amount of items: 2
Items: 
Size: 1165 Color: 0
Size: 841 Color: 1

Bin 48: 14 of cap free
Amount of items: 2
Items: 
Size: 1705 Color: 1
Size: 301 Color: 0

Bin 49: 14 of cap free
Amount of items: 2
Items: 
Size: 1737 Color: 0
Size: 269 Color: 1

Bin 50: 15 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 0
Size: 816 Color: 0
Size: 164 Color: 1

Bin 51: 17 of cap free
Amount of items: 2
Items: 
Size: 1217 Color: 0
Size: 786 Color: 1

Bin 52: 17 of cap free
Amount of items: 2
Items: 
Size: 1414 Color: 0
Size: 589 Color: 1

Bin 53: 19 of cap free
Amount of items: 2
Items: 
Size: 1537 Color: 0
Size: 464 Color: 1

Bin 54: 21 of cap free
Amount of items: 2
Items: 
Size: 1157 Color: 1
Size: 842 Color: 0

Bin 55: 21 of cap free
Amount of items: 2
Items: 
Size: 1315 Color: 0
Size: 684 Color: 1

Bin 56: 22 of cap free
Amount of items: 2
Items: 
Size: 1161 Color: 0
Size: 837 Color: 1

Bin 57: 23 of cap free
Amount of items: 2
Items: 
Size: 1411 Color: 0
Size: 586 Color: 1

Bin 58: 26 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 1
Size: 420 Color: 0

Bin 59: 30 of cap free
Amount of items: 19
Items: 
Size: 166 Color: 0
Size: 166 Color: 0
Size: 142 Color: 1
Size: 142 Color: 1
Size: 136 Color: 1
Size: 116 Color: 1
Size: 116 Color: 0
Size: 102 Color: 1
Size: 102 Color: 0
Size: 100 Color: 0
Size: 90 Color: 0
Size: 84 Color: 0
Size: 80 Color: 1
Size: 80 Color: 0
Size: 76 Color: 1
Size: 76 Color: 1
Size: 72 Color: 1
Size: 72 Color: 0
Size: 72 Color: 0

Bin 60: 32 of cap free
Amount of items: 2
Items: 
Size: 1477 Color: 1
Size: 511 Color: 0

Bin 61: 32 of cap free
Amount of items: 2
Items: 
Size: 1535 Color: 0
Size: 453 Color: 1

Bin 62: 33 of cap free
Amount of items: 2
Items: 
Size: 1400 Color: 1
Size: 587 Color: 0

Bin 63: 34 of cap free
Amount of items: 2
Items: 
Size: 1583 Color: 0
Size: 403 Color: 1

Bin 64: 35 of cap free
Amount of items: 2
Items: 
Size: 1268 Color: 0
Size: 717 Color: 1

Bin 65: 35 of cap free
Amount of items: 2
Items: 
Size: 1466 Color: 0
Size: 519 Color: 1

Bin 66: 1430 of cap free
Amount of items: 12
Items: 
Size: 66 Color: 1
Size: 56 Color: 1
Size: 56 Color: 1
Size: 52 Color: 0
Size: 52 Color: 0
Size: 52 Color: 0
Size: 52 Color: 0
Size: 48 Color: 0
Size: 40 Color: 1
Size: 40 Color: 1
Size: 40 Color: 1
Size: 36 Color: 0

Total size: 131300
Total free space: 2020

