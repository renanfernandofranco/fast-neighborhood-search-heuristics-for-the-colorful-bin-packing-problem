Capicity Bin: 1001
Lower Bound: 224

Bins used: 225
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 393 Color: 9
Size: 272 Color: 13
Size: 182 Color: 16
Size: 154 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 634 Color: 17
Size: 244 Color: 15
Size: 123 Color: 5

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 9
Size: 265 Color: 8

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 16
Size: 331 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 571 Color: 10
Size: 244 Color: 17
Size: 186 Color: 5

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 429 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 16
Size: 272 Color: 16
Size: 264 Color: 1

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 14
Size: 326 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 8
Size: 397 Color: 6
Size: 131 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 10
Size: 375 Color: 7
Size: 131 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 515 Color: 3
Size: 364 Color: 15
Size: 122 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 2
Size: 357 Color: 12
Size: 122 Color: 18

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 19
Size: 419 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 7
Size: 287 Color: 17
Size: 141 Color: 19

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 12
Size: 480 Color: 1

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 5
Size: 281 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 587 Color: 18
Size: 282 Color: 4
Size: 132 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 9
Size: 342 Color: 9
Size: 280 Color: 0

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 7
Size: 418 Color: 14

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 3
Size: 494 Color: 14

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 12
Size: 486 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 17
Size: 275 Color: 3
Size: 210 Color: 15

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 18
Size: 500 Color: 8

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 8
Size: 461 Color: 13

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 13
Size: 392 Color: 9

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 2
Size: 218 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 19
Size: 332 Color: 4
Size: 281 Color: 19

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 19
Size: 434 Color: 16
Size: 114 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 514 Color: 2
Size: 373 Color: 8
Size: 114 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 9
Size: 393 Color: 14
Size: 197 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 11
Size: 373 Color: 8
Size: 255 Color: 19

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 12
Size: 487 Color: 10

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 15
Size: 477 Color: 7

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 12
Size: 479 Color: 3

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 13
Size: 466 Color: 8

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 17
Size: 462 Color: 1

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 14
Size: 458 Color: 19

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 5
Size: 455 Color: 16

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 2
Size: 452 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 19
Size: 279 Color: 9
Size: 170 Color: 4

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 14
Size: 444 Color: 3

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 2
Size: 438 Color: 6

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 5
Size: 440 Color: 6

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 10

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 569 Color: 0
Size: 271 Color: 5
Size: 161 Color: 14

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 12
Size: 427 Color: 16

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 2
Size: 424 Color: 5

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 0
Size: 422 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 593 Color: 13
Size: 265 Color: 0
Size: 143 Color: 12

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 7
Size: 270 Color: 5
Size: 136 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 601 Color: 3
Size: 267 Color: 0
Size: 133 Color: 2

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 6
Size: 403 Color: 12

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 8
Size: 398 Color: 1

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 6
Size: 397 Color: 4

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 5
Size: 390 Color: 6

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 3
Size: 278 Color: 6
Size: 109 Color: 19

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 13
Size: 386 Color: 15

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 17
Size: 382 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 620 Color: 14
Size: 265 Color: 8
Size: 116 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 4
Size: 238 Color: 14
Size: 131 Color: 6

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 636 Color: 3
Size: 234 Color: 9
Size: 131 Color: 12

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 10
Size: 363 Color: 6

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 642 Color: 11
Size: 186 Color: 15
Size: 173 Color: 13

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 4
Size: 359 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 0
Size: 183 Color: 14
Size: 168 Color: 15

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 12
Size: 210 Color: 18
Size: 140 Color: 15

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 652 Color: 3
Size: 237 Color: 16
Size: 112 Color: 15

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 15
Size: 349 Color: 5

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 13
Size: 180 Color: 4
Size: 168 Color: 6

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 3
Size: 179 Color: 16
Size: 166 Color: 18

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 17
Size: 342 Color: 11

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 662 Color: 1
Size: 217 Color: 0
Size: 122 Color: 14

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 3
Size: 186 Color: 13
Size: 151 Color: 0

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 0
Size: 336 Color: 6

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 6
Size: 177 Color: 9
Size: 158 Color: 6

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 13
Size: 178 Color: 13
Size: 157 Color: 16

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 0
Size: 209 Color: 18
Size: 116 Color: 6

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 14
Size: 324 Color: 3

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 17
Size: 323 Color: 1

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 7
Size: 322 Color: 10

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 321 Color: 16

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 3
Size: 318 Color: 16

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 18
Size: 310 Color: 8

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 14
Size: 309 Color: 5

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 10
Size: 305 Color: 15

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 12
Size: 305 Color: 3

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 17
Size: 298 Color: 5

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 19
Size: 149 Color: 0
Size: 144 Color: 18

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 3
Size: 164 Color: 18
Size: 129 Color: 17

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 9
Size: 291 Color: 14

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 11
Size: 284 Color: 6

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 4
Size: 155 Color: 13
Size: 127 Color: 9

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 12
Size: 279 Color: 13

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 12
Size: 276 Color: 9

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 16
Size: 274 Color: 15

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 728 Color: 12
Size: 149 Color: 14
Size: 124 Color: 16

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 4
Size: 267 Color: 3

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 15
Size: 262 Color: 2

Bin 99: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 8
Size: 253 Color: 3

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 7
Size: 249 Color: 17

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 9
Size: 244 Color: 1

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 5
Size: 242 Color: 18

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 6
Size: 239 Color: 5

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 764 Color: 7
Size: 125 Color: 13
Size: 112 Color: 12

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 3
Size: 122 Color: 14
Size: 111 Color: 4

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 13
Size: 226 Color: 12

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 19
Size: 221 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 17
Size: 120 Color: 2
Size: 101 Color: 13

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 782 Color: 6
Size: 112 Color: 19
Size: 107 Color: 8

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 782 Color: 10
Size: 119 Color: 3
Size: 100 Color: 0

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 5
Size: 217 Color: 8

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 8
Size: 214 Color: 0

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 18
Size: 211 Color: 5

Bin 114: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 9
Size: 209 Color: 16

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 793 Color: 12
Size: 107 Color: 4
Size: 101 Color: 19

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 793 Color: 1
Size: 108 Color: 18
Size: 100 Color: 15

Bin 117: 1 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 8
Size: 500 Color: 7

Bin 118: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 10
Size: 474 Color: 19

Bin 119: 1 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 19
Size: 478 Color: 9

Bin 120: 1 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 1
Size: 467 Color: 18

Bin 121: 1 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 18
Size: 455 Color: 4

Bin 122: 1 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 15
Size: 448 Color: 6

Bin 123: 1 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 17
Size: 439 Color: 0

Bin 124: 1 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 421 Color: 16

Bin 125: 1 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 8
Size: 419 Color: 5

Bin 126: 1 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 407 Color: 4

Bin 127: 1 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 16
Size: 389 Color: 7

Bin 128: 1 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 10
Size: 384 Color: 19

Bin 129: 1 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 9
Size: 376 Color: 1

Bin 130: 1 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 9
Size: 376 Color: 15

Bin 131: 1 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 11
Size: 368 Color: 8

Bin 132: 1 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 16
Size: 365 Color: 9

Bin 133: 1 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 2
Size: 359 Color: 0

Bin 134: 1 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 16
Size: 348 Color: 11

Bin 135: 1 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 0
Size: 348 Color: 17

Bin 136: 1 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 7
Size: 337 Color: 11

Bin 137: 1 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 13
Size: 314 Color: 16

Bin 138: 1 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 8
Size: 310 Color: 7

Bin 139: 1 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 5
Size: 307 Color: 0

Bin 140: 1 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 5
Size: 295 Color: 9

Bin 141: 1 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 11
Size: 285 Color: 14

Bin 142: 1 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 0
Size: 283 Color: 17

Bin 143: 1 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 0
Size: 272 Color: 11

Bin 144: 1 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 15
Size: 244 Color: 10

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 12
Size: 128 Color: 3
Size: 107 Color: 6

Bin 146: 1 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 7
Size: 228 Color: 18

Bin 147: 1 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 8
Size: 203 Color: 10

Bin 148: 1 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 19
Size: 423 Color: 14

Bin 149: 1 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 4
Size: 337 Color: 3

Bin 150: 1 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 12
Size: 482 Color: 7

Bin 151: 1 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 329 Color: 3

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 2
Size: 320 Color: 18
Size: 320 Color: 4

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 556 Color: 11
Size: 283 Color: 14
Size: 161 Color: 3

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 416 Color: 16
Size: 145 Color: 11

Bin 155: 2 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 426 Color: 3

Bin 156: 2 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 14
Size: 264 Color: 3

Bin 157: 2 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 2
Size: 454 Color: 6

Bin 158: 2 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 14
Size: 458 Color: 18

Bin 159: 2 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 404 Color: 5

Bin 160: 2 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 15
Size: 384 Color: 5

Bin 161: 2 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 9
Size: 379 Color: 0

Bin 162: 2 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 18
Size: 314 Color: 15

Bin 163: 2 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 17
Size: 299 Color: 18

Bin 164: 2 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 19
Size: 228 Color: 17

Bin 165: 2 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 13
Size: 202 Color: 4

Bin 166: 2 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 14
Size: 469 Color: 2

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 563 Color: 3
Size: 264 Color: 1
Size: 172 Color: 6

Bin 168: 2 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 8
Size: 351 Color: 3

Bin 169: 3 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 19
Size: 490 Color: 17

Bin 170: 3 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 8
Size: 388 Color: 1

Bin 171: 3 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 8
Size: 383 Color: 19

Bin 172: 3 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 8
Size: 299 Color: 1

Bin 173: 3 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 9
Size: 288 Color: 18

Bin 174: 3 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 2
Size: 227 Color: 0

Bin 175: 3 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 13
Size: 421 Color: 14

Bin 176: 4 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 3
Size: 325 Color: 0

Bin 177: 4 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 2
Size: 467 Color: 16

Bin 178: 4 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 7
Size: 336 Color: 12

Bin 179: 4 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 10
Size: 314 Color: 17

Bin 180: 4 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 15
Size: 252 Color: 5

Bin 181: 5 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 6
Size: 251 Color: 2

Bin 182: 5 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 3
Size: 474 Color: 13

Bin 183: 5 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 13
Size: 466 Color: 3

Bin 184: 5 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 9
Size: 306 Color: 14

Bin 185: 5 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 0
Size: 201 Color: 18

Bin 186: 5 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 17
Size: 195 Color: 7

Bin 187: 5 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 0
Size: 195 Color: 9

Bin 188: 5 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 394 Color: 11
Size: 144 Color: 8

Bin 189: 6 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 6
Size: 365 Color: 0

Bin 190: 6 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 11
Size: 347 Color: 10

Bin 191: 6 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 0
Size: 324 Color: 18

Bin 192: 6 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 11
Size: 305 Color: 13

Bin 193: 6 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 6
Size: 251 Color: 8

Bin 194: 6 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 251 Color: 18

Bin 195: 6 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 4
Size: 219 Color: 9

Bin 196: 6 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 9
Size: 490 Color: 11

Bin 197: 7 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 18
Size: 218 Color: 12

Bin 198: 7 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 17
Size: 457 Color: 13

Bin 199: 8 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 8
Size: 347 Color: 11

Bin 200: 8 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 12
Size: 304 Color: 5

Bin 201: 8 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 9
Size: 336 Color: 12

Bin 202: 9 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 472 Color: 7

Bin 203: 9 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 5
Size: 248 Color: 7

Bin 204: 9 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 3
Size: 441 Color: 16

Bin 205: 9 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 17
Size: 416 Color: 7

Bin 206: 11 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 8
Size: 472 Color: 4

Bin 207: 12 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 12
Size: 218 Color: 18

Bin 208: 13 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 17
Size: 304 Color: 14

Bin 209: 13 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 2
Size: 374 Color: 12

Bin 210: 15 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 4
Size: 303 Color: 10

Bin 211: 17 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 9
Size: 279 Color: 5

Bin 212: 17 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 11
Size: 243 Color: 17

Bin 213: 18 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 13
Size: 412 Color: 16

Bin 214: 18 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 12
Size: 359 Color: 14

Bin 215: 19 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 15
Size: 241 Color: 9

Bin 216: 25 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 7
Size: 320 Color: 6

Bin 217: 28 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 11
Size: 410 Color: 7

Bin 218: 31 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 15
Size: 457 Color: 16

Bin 219: 40 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 6
Size: 160 Color: 3

Bin 220: 40 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 18
Size: 347 Color: 19

Bin 221: 47 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 19
Size: 393 Color: 3

Bin 222: 207 of cap free
Amount of items: 1
Items: 
Size: 794 Color: 1

Bin 223: 216 of cap free
Amount of items: 1
Items: 
Size: 785 Color: 2

Bin 224: 231 of cap free
Amount of items: 1
Items: 
Size: 770 Color: 12

Bin 225: 263 of cap free
Amount of items: 1
Items: 
Size: 738 Color: 15

Total size: 223675
Total free space: 1550

