Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 10002

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 390916 Color: 7853
Size: 321211 Color: 5159
Size: 287874 Color: 3351

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 357526 Color: 6708
Size: 335258 Color: 5823
Size: 307217 Color: 4480

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 368983 Color: 7155
Size: 326524 Color: 5405
Size: 304494 Color: 4345

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 357844 Color: 6718
Size: 334629 Color: 5784
Size: 307528 Color: 4495

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 391516 Color: 7877
Size: 334664 Color: 5789
Size: 273821 Color: 2417

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 349557 Color: 6413
Size: 335129 Color: 5814
Size: 315315 Color: 4887

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 352945 Color: 6536
Size: 332349 Color: 5679
Size: 314707 Color: 4851

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 350376 Color: 6448
Size: 332383 Color: 5681
Size: 317242 Color: 4979

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 371361 Color: 7235
Size: 345978 Color: 6269
Size: 282662 Color: 3019

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 398384 Color: 8072
Size: 325944 Color: 5379
Size: 275673 Color: 2560

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 375456 Color: 7383
Size: 355606 Color: 6635
Size: 268939 Color: 2030

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 381116 Color: 7555
Size: 349557 Color: 6412
Size: 269328 Color: 2054

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 365095 Color: 6989
Size: 361410 Color: 6869
Size: 273496 Color: 2386

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 377382 Color: 7456
Size: 345827 Color: 6258
Size: 276792 Color: 2624

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 363354 Color: 6933
Size: 361167 Color: 6854
Size: 275480 Color: 2547

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 368846 Color: 7151
Size: 347812 Color: 6347
Size: 283343 Color: 3059

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 348113 Color: 6362
Size: 339062 Color: 5965
Size: 312826 Color: 4746

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 391231 Color: 7865
Size: 313574 Color: 4781
Size: 295196 Color: 3807

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 401287 Color: 8150
Size: 335291 Color: 5825
Size: 263423 Color: 1565

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 367325 Color: 7086
Size: 327581 Color: 5455
Size: 305095 Color: 4381

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 383181 Color: 7630
Size: 348070 Color: 6361
Size: 268750 Color: 2015

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 397125 Color: 8029
Size: 324625 Color: 5321
Size: 278251 Color: 2728

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 382745 Color: 7609
Size: 327865 Color: 5471
Size: 289391 Color: 3433

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 368865 Color: 7152
Size: 344878 Color: 6217
Size: 286258 Color: 3235

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 370294 Color: 7199
Size: 346578 Color: 6298
Size: 283129 Color: 3049

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 376373 Color: 7426
Size: 352414 Color: 6525
Size: 271214 Color: 2227

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 380331 Color: 7542
Size: 344748 Color: 6209
Size: 274922 Color: 2501

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 357281 Color: 6697
Size: 329795 Color: 5557
Size: 312925 Color: 4756

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 368826 Color: 7148
Size: 345804 Color: 6255
Size: 285371 Color: 3184

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 365394 Color: 7005
Size: 355086 Color: 6615
Size: 279521 Color: 2808

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 337345 Color: 5896
Size: 331659 Color: 5645
Size: 330997 Color: 5602

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 398203 Color: 8064
Size: 322612 Color: 5226
Size: 279186 Color: 2786

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 363746 Color: 6950
Size: 358507 Color: 6747
Size: 277748 Color: 2699

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 380067 Color: 7535
Size: 342773 Color: 6125
Size: 277161 Color: 2661

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 392478 Color: 7907
Size: 327104 Color: 5431
Size: 280419 Color: 2865

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 389749 Color: 7814
Size: 333101 Color: 5720
Size: 277151 Color: 2657

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 402194 Color: 8180
Size: 322527 Color: 5223
Size: 275280 Color: 2525

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 397299 Color: 8034
Size: 305007 Color: 4374
Size: 297695 Color: 3958

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 368922 Color: 7153
Size: 350099 Color: 6432
Size: 280980 Color: 2908

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 369476 Color: 7171
Size: 363069 Color: 6923
Size: 267456 Color: 1914

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 368259 Color: 7130
Size: 367219 Color: 7080
Size: 264523 Color: 1661

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 379318 Color: 7506
Size: 342965 Color: 6132
Size: 277718 Color: 2696

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 408259 Color: 8317
Size: 321755 Color: 5192
Size: 269987 Color: 2108

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 365802 Color: 7018
Size: 350401 Color: 6450
Size: 283798 Color: 3087

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 375164 Color: 7373
Size: 359469 Color: 6785
Size: 265368 Color: 1736

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 374255 Color: 7341
Size: 339165 Color: 5973
Size: 286581 Color: 3261

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 367892 Color: 7114
Size: 319264 Color: 5062
Size: 312845 Color: 4748

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 372178 Color: 7273
Size: 370394 Color: 7205
Size: 257429 Color: 954

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 373862 Color: 7332
Size: 373026 Color: 7298
Size: 253113 Color: 481

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 359869 Color: 6803
Size: 353813 Color: 6574
Size: 286319 Color: 3239

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 376652 Color: 7433
Size: 346028 Color: 6274
Size: 277321 Color: 2672

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 379401 Color: 7509
Size: 338862 Color: 5956
Size: 281738 Color: 2964

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 381876 Color: 7580
Size: 353549 Color: 6560
Size: 264576 Color: 1668

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 376192 Color: 7420
Size: 364723 Color: 6974
Size: 259086 Color: 1152

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 367577 Color: 7099
Size: 366646 Color: 7057
Size: 265778 Color: 1770

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 363367 Color: 6935
Size: 357368 Color: 6700
Size: 279266 Color: 2791

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 378670 Color: 7486
Size: 353216 Color: 6547
Size: 268115 Color: 1963

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 379890 Color: 7527
Size: 358846 Color: 6761
Size: 261265 Color: 1372

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 380658 Color: 7545
Size: 352257 Color: 6518
Size: 267086 Color: 1893

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 340932 Color: 6045
Size: 340253 Color: 6014
Size: 318816 Color: 5041

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 406683 Color: 8286
Size: 319878 Color: 5096
Size: 273440 Color: 2382

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 366987 Color: 7071
Size: 350756 Color: 6456
Size: 282258 Color: 2986

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 397626 Color: 8049
Size: 345370 Color: 6232
Size: 257005 Color: 911

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 391384 Color: 7870
Size: 332774 Color: 5706
Size: 275843 Color: 2571

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 374907 Color: 7364
Size: 345755 Color: 6251
Size: 279339 Color: 2795

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 369477 Color: 7172
Size: 346088 Color: 6277
Size: 284436 Color: 3128

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 366781 Color: 7063
Size: 333957 Color: 5760
Size: 299263 Color: 4051

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 373675 Color: 7325
Size: 344936 Color: 6220
Size: 281390 Color: 2940

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 381001 Color: 7553
Size: 335128 Color: 5813
Size: 283872 Color: 3090

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 398805 Color: 8087
Size: 341034 Color: 6049
Size: 260162 Color: 1256

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 365540 Color: 7009
Size: 358760 Color: 6758
Size: 275701 Color: 2562

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 407203 Color: 8299
Size: 341553 Color: 6064
Size: 251245 Color: 231

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 382044 Color: 7586
Size: 333991 Color: 5761
Size: 283966 Color: 3096

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 368605 Color: 7142
Size: 352740 Color: 6532
Size: 278656 Color: 2757

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 397741 Color: 8052
Size: 349470 Color: 6409
Size: 252790 Color: 430

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 366514 Color: 7049
Size: 346433 Color: 6292
Size: 287054 Color: 3301

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 381486 Color: 7565
Size: 367997 Color: 7117
Size: 250518 Color: 112

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 378515 Color: 7482
Size: 340153 Color: 6010
Size: 281333 Color: 2935

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 356551 Color: 6672
Size: 341813 Color: 6080
Size: 301637 Color: 4181

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 366372 Color: 7040
Size: 346224 Color: 6283
Size: 287405 Color: 3326

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 365272 Color: 7003
Size: 363379 Color: 6936
Size: 271350 Color: 2241

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 366197 Color: 7032
Size: 354740 Color: 6606
Size: 279064 Color: 2779

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 382015 Color: 7585
Size: 342062 Color: 6091
Size: 275924 Color: 2575

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 373435 Color: 7314
Size: 346587 Color: 6299
Size: 279979 Color: 2839

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 358126 Color: 6734
Size: 325828 Color: 5376
Size: 316047 Color: 4924

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 396136 Color: 7999
Size: 329034 Color: 5523
Size: 274831 Color: 2494

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 336987 Color: 5879
Size: 335270 Color: 5824
Size: 327744 Color: 5465

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 372032 Color: 7263
Size: 338203 Color: 5933
Size: 289766 Color: 3456

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 379522 Color: 7512
Size: 342894 Color: 6129
Size: 277585 Color: 2689

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 365317 Color: 7004
Size: 346590 Color: 6300
Size: 288094 Color: 3363

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 367848 Color: 7110
Size: 329760 Color: 5555
Size: 302393 Color: 4233

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 379036 Color: 7497
Size: 342875 Color: 6128
Size: 278090 Color: 2716

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 400893 Color: 8140
Size: 332433 Color: 5687
Size: 266675 Color: 1856

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 363727 Color: 6948
Size: 349624 Color: 6417
Size: 286650 Color: 3269

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 377913 Color: 7465
Size: 346634 Color: 6302
Size: 275454 Color: 2541

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 380679 Color: 7546
Size: 350810 Color: 6458
Size: 268512 Color: 1995

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 392900 Color: 7917
Size: 341303 Color: 6059
Size: 265798 Color: 1773

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 380077 Color: 7536
Size: 340767 Color: 6041
Size: 279157 Color: 2785

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 405541 Color: 8263
Size: 336521 Color: 5864
Size: 257939 Color: 1000

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 367849 Color: 7111
Size: 345805 Color: 6256
Size: 286347 Color: 3242

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 370579 Color: 7210
Size: 345658 Color: 6246
Size: 283764 Color: 3082

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 359965 Color: 6804
Size: 357109 Color: 6689
Size: 282927 Color: 3035

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 389123 Color: 7794
Size: 358945 Color: 6763
Size: 251933 Color: 324

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 378240 Color: 7472
Size: 352229 Color: 6515
Size: 269532 Color: 2073

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 408238 Color: 8316
Size: 339278 Color: 5979
Size: 252485 Color: 395

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 379973 Color: 7533
Size: 368111 Color: 7126
Size: 251917 Color: 319

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 370305 Color: 7200
Size: 351487 Color: 6483
Size: 278209 Color: 2725

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 394605 Color: 7961
Size: 332211 Color: 5674
Size: 273185 Color: 2368

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 366521 Color: 7051
Size: 326197 Color: 5393
Size: 307283 Color: 4485

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 374230 Color: 7340
Size: 357087 Color: 6688
Size: 268684 Color: 2013

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 407859 Color: 8308
Size: 328460 Color: 5503
Size: 263682 Color: 1587

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 372023 Color: 7262
Size: 351172 Color: 6476
Size: 276806 Color: 2626

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 376413 Color: 7427
Size: 360640 Color: 6831
Size: 262948 Color: 1518

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 401063 Color: 8146
Size: 340498 Color: 6027
Size: 258440 Color: 1070

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 404117 Color: 8221
Size: 323432 Color: 5267
Size: 272452 Color: 2314

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 373334 Color: 7310
Size: 348355 Color: 6372
Size: 278312 Color: 2735

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 364178 Color: 6960
Size: 346854 Color: 6311
Size: 288969 Color: 3410

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 361716 Color: 6876
Size: 359527 Color: 6790
Size: 278758 Color: 2761

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 361512 Color: 6872
Size: 357811 Color: 6716
Size: 280678 Color: 2890

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 375565 Color: 7388
Size: 344422 Color: 6195
Size: 280014 Color: 2841

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 382335 Color: 7593
Size: 342352 Color: 6104
Size: 275314 Color: 2529

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 372272 Color: 7277
Size: 348371 Color: 6373
Size: 279358 Color: 2796

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 390592 Color: 7846
Size: 346533 Color: 6296
Size: 262876 Color: 1511

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 377017 Color: 7444
Size: 372984 Color: 7293
Size: 250000 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 365807 Color: 7019
Size: 365227 Color: 7001
Size: 268967 Color: 2032

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 377566 Color: 7462
Size: 345419 Color: 6234
Size: 277016 Color: 2647

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 359823 Color: 6802
Size: 347369 Color: 6329
Size: 292809 Color: 3649

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 391406 Color: 7872
Size: 346200 Color: 6281
Size: 262395 Color: 1458

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 376595 Color: 7430
Size: 362080 Color: 6890
Size: 261326 Color: 1377

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 373701 Color: 7327
Size: 339685 Color: 5992
Size: 286615 Color: 3265

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 380467 Color: 7544
Size: 342597 Color: 6118
Size: 276937 Color: 2639

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 382367 Color: 7595
Size: 331483 Color: 5631
Size: 286151 Color: 3225

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 366450 Color: 7043
Size: 338648 Color: 5949
Size: 294903 Color: 3789

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 351870 Color: 6498
Size: 349212 Color: 6400
Size: 298919 Color: 4023

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 378520 Color: 7483
Size: 343283 Color: 6142
Size: 278198 Color: 2724

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 363549 Color: 6940
Size: 361637 Color: 6874
Size: 274815 Color: 2493

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 369676 Color: 7178
Size: 343768 Color: 6162
Size: 286557 Color: 3259

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 357723 Color: 6714
Size: 349902 Color: 6426
Size: 292376 Color: 3626

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 404115 Color: 8220
Size: 340597 Color: 6032
Size: 255289 Color: 735

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 375139 Color: 7372
Size: 352203 Color: 6511
Size: 272659 Color: 2328

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 389021 Color: 7787
Size: 348298 Color: 6371
Size: 262682 Color: 1493

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 388496 Color: 7779
Size: 333190 Color: 5723
Size: 278315 Color: 2737

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 376423 Color: 7428
Size: 333096 Color: 5719
Size: 290482 Color: 3501

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 338405 Color: 5941
Size: 331580 Color: 5640
Size: 330016 Color: 5561

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 378395 Color: 7477
Size: 370091 Color: 7189
Size: 251515 Color: 267

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 407217 Color: 8300
Size: 341798 Color: 6077
Size: 250986 Color: 187

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 393532 Color: 7936
Size: 344233 Color: 6184
Size: 262236 Color: 1447

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 397584 Color: 8047
Size: 320535 Color: 5124
Size: 281882 Color: 2969

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 367375 Color: 7089
Size: 346791 Color: 6309
Size: 285835 Color: 3208

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 409145 Color: 8334
Size: 340824 Color: 6043
Size: 250032 Color: 7

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 383712 Color: 7647
Size: 339132 Color: 5972
Size: 277157 Color: 2658

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 371910 Color: 7255
Size: 350367 Color: 6447
Size: 277724 Color: 2697

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 366998 Color: 7072
Size: 335238 Color: 5822
Size: 297765 Color: 3961

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 375011 Color: 7368
Size: 366775 Color: 7061
Size: 258215 Color: 1040

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 382735 Color: 7608
Size: 336292 Color: 5854
Size: 280974 Color: 2907

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 352074 Color: 6506
Size: 330621 Color: 5586
Size: 317306 Color: 4981

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 381240 Color: 7558
Size: 342156 Color: 6098
Size: 276605 Color: 2615

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 401462 Color: 8159
Size: 334679 Color: 5790
Size: 263860 Color: 1603

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 408842 Color: 8324
Size: 309890 Color: 4623
Size: 281269 Color: 2932

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 374385 Color: 7345
Size: 331153 Color: 5614
Size: 294463 Color: 3760

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 376985 Color: 7441
Size: 342593 Color: 6117
Size: 280423 Color: 2867

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 382835 Color: 7616
Size: 318005 Color: 5016
Size: 299161 Color: 4044

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 394743 Color: 7964
Size: 351296 Color: 6481
Size: 253962 Color: 593

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 381187 Color: 7556
Size: 337607 Color: 5908
Size: 281207 Color: 2926

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 379938 Color: 7529
Size: 334156 Color: 5768
Size: 285907 Color: 3212

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 377549 Color: 7461
Size: 326501 Color: 5403
Size: 295951 Color: 3854

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 379492 Color: 7511
Size: 361279 Color: 6860
Size: 259230 Color: 1173

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 374939 Color: 7366
Size: 347273 Color: 6327
Size: 277789 Color: 2702

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 363737 Color: 6949
Size: 347126 Color: 6323
Size: 289138 Color: 3422

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 403620 Color: 8207
Size: 309659 Color: 4607
Size: 286722 Color: 3273

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 337879 Color: 5919
Size: 332088 Color: 5665
Size: 330034 Color: 5562

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 397180 Color: 8032
Size: 320542 Color: 5125
Size: 282279 Color: 2988

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 374064 Color: 7337
Size: 362897 Color: 6918
Size: 263040 Color: 1529

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 391063 Color: 7858
Size: 341215 Color: 6055
Size: 267723 Color: 1934

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 378240 Color: 7473
Size: 338922 Color: 5959
Size: 282839 Color: 3028

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 376628 Color: 7431
Size: 350943 Color: 6466
Size: 272430 Color: 2311

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 361178 Color: 6855
Size: 346721 Color: 6305
Size: 292102 Color: 3601

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 368030 Color: 7121
Size: 361331 Color: 6863
Size: 270640 Color: 2173

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 371758 Color: 7246
Size: 366803 Color: 7064
Size: 261440 Color: 1385

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 376334 Color: 7424
Size: 339050 Color: 5964
Size: 284617 Color: 3137

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 401978 Color: 8170
Size: 325019 Color: 5342
Size: 273004 Color: 2355

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 364054 Color: 6956
Size: 347082 Color: 6321
Size: 288865 Color: 3407

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 373942 Color: 7334
Size: 360760 Color: 6836
Size: 265299 Color: 1731

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 389052 Color: 7790
Size: 329239 Color: 5534
Size: 281710 Color: 2962

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 389527 Color: 7807
Size: 357628 Color: 6711
Size: 252846 Color: 437

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 388086 Color: 7769
Size: 361832 Color: 6881
Size: 250083 Color: 17

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 367302 Color: 7085
Size: 353102 Color: 6542
Size: 279597 Color: 2810

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 373044 Color: 7299
Size: 362696 Color: 6909
Size: 264261 Color: 1638

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 396229 Color: 8002
Size: 341115 Color: 6052
Size: 262657 Color: 1488

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 377130 Color: 7450
Size: 343762 Color: 6161
Size: 279109 Color: 2782

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 365174 Color: 6996
Size: 352337 Color: 6522
Size: 282490 Color: 3004

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 402982 Color: 8195
Size: 334563 Color: 5783
Size: 262456 Color: 1465

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 408417 Color: 8319
Size: 325204 Color: 5350
Size: 266380 Color: 1832

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 378905 Color: 7492
Size: 361497 Color: 6871
Size: 259599 Color: 1204

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 380297 Color: 7540
Size: 357683 Color: 6713
Size: 262021 Color: 1427

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 376344 Color: 7425
Size: 343975 Color: 6173
Size: 279682 Color: 2813

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 381201 Color: 7557
Size: 343871 Color: 6168
Size: 274929 Color: 2502

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 405233 Color: 8255
Size: 327368 Color: 5443
Size: 267400 Color: 1908

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 408713 Color: 8323
Size: 309387 Color: 4592
Size: 281901 Color: 2970

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 372266 Color: 7276
Size: 362771 Color: 6911
Size: 264964 Color: 1699

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 373716 Color: 7328
Size: 344907 Color: 6218
Size: 281378 Color: 2937

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 390045 Color: 7824
Size: 359491 Color: 6787
Size: 250465 Color: 100

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 408662 Color: 8320
Size: 335879 Color: 5839
Size: 255460 Color: 751

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 408943 Color: 8327
Size: 340729 Color: 6039
Size: 250329 Color: 75

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 368620 Color: 7143
Size: 346156 Color: 6278
Size: 285225 Color: 3169

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 367692 Color: 7102
Size: 366597 Color: 7052
Size: 265712 Color: 1764

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 379187 Color: 7504
Size: 369675 Color: 7177
Size: 251139 Color: 214

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 389748 Color: 7813
Size: 346007 Color: 6271
Size: 264246 Color: 1636

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 389736 Color: 7811
Size: 331043 Color: 5608
Size: 279222 Color: 2789

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 381031 Color: 7554
Size: 365178 Color: 6998
Size: 253792 Color: 567

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 380109 Color: 7539
Size: 342570 Color: 6114
Size: 277322 Color: 2673

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 409112 Color: 8332
Size: 340282 Color: 6016
Size: 250607 Color: 125

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 379960 Color: 7530
Size: 362536 Color: 6898
Size: 257505 Color: 957

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 382243 Color: 7592
Size: 354555 Color: 6595
Size: 263203 Color: 1547

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 379967 Color: 7532
Size: 361409 Color: 6868
Size: 258625 Color: 1092

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 347860 Color: 6348
Size: 332270 Color: 5675
Size: 319871 Color: 5095

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 401383 Color: 8155
Size: 343770 Color: 6163
Size: 254848 Color: 686

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 379929 Color: 7528
Size: 351558 Color: 6485
Size: 268514 Color: 1996

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 407636 Color: 8304
Size: 340690 Color: 6038
Size: 251675 Color: 291

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 366235 Color: 7035
Size: 333398 Color: 5730
Size: 300368 Color: 4113

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 389719 Color: 7810
Size: 341484 Color: 6062
Size: 268798 Color: 2019

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 364450 Color: 6965
Size: 320683 Color: 5134
Size: 314868 Color: 4866

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 379332 Color: 7507
Size: 345113 Color: 6224
Size: 275556 Color: 2551

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 359610 Color: 6794
Size: 344246 Color: 6185
Size: 296145 Color: 3870

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 407823 Color: 8306
Size: 341944 Color: 6084
Size: 250234 Color: 59

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 360389 Color: 6822
Size: 360221 Color: 6813
Size: 279391 Color: 2800

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 358294 Color: 6740
Size: 348439 Color: 6378
Size: 293268 Color: 3681

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 358803 Color: 6759
Size: 357161 Color: 6694
Size: 284037 Color: 3101

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 407099 Color: 8297
Size: 341003 Color: 6048
Size: 251899 Color: 316

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 407065 Color: 8295
Size: 330960 Color: 5599
Size: 261976 Color: 1424

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 406404 Color: 8281
Size: 327334 Color: 5440
Size: 266263 Color: 1822

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 406208 Color: 8275
Size: 323120 Color: 5252
Size: 270673 Color: 2178

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 361073 Color: 6848
Size: 340538 Color: 6028
Size: 298390 Color: 3998

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 407041 Color: 8294
Size: 342406 Color: 6109
Size: 250554 Color: 121

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 406146 Color: 8272
Size: 343151 Color: 6139
Size: 250704 Color: 138

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 406323 Color: 8279
Size: 341618 Color: 6069
Size: 252060 Color: 341

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 392193 Color: 7898
Size: 325222 Color: 5351
Size: 282586 Color: 3014

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 363558 Color: 6941
Size: 329394 Color: 5538
Size: 307049 Color: 4470

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 405718 Color: 8267
Size: 342087 Color: 6093
Size: 252196 Color: 362

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 406905 Color: 8292
Size: 342019 Color: 6088
Size: 251077 Color: 202

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 406260 Color: 8278
Size: 338098 Color: 5928
Size: 255643 Color: 773

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 388811 Color: 7784
Size: 329739 Color: 5553
Size: 281451 Color: 2946

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 406211 Color: 8277
Size: 340880 Color: 6044
Size: 252910 Color: 443

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 405853 Color: 8270
Size: 343595 Color: 6153
Size: 250553 Color: 120

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 405907 Color: 8271
Size: 333447 Color: 5732
Size: 260647 Color: 1309

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 371290 Color: 7234
Size: 366848 Color: 7066
Size: 261863 Color: 1412

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 360767 Color: 6837
Size: 341096 Color: 6050
Size: 298138 Color: 3982

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 378436 Color: 7479
Size: 343906 Color: 6170
Size: 277659 Color: 2692

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 392108 Color: 7893
Size: 334160 Color: 5769
Size: 273733 Color: 2406

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 405715 Color: 8266
Size: 343041 Color: 6136
Size: 251245 Color: 230

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 405265 Color: 8257
Size: 342153 Color: 6097
Size: 252583 Color: 406

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 405249 Color: 8256
Size: 342128 Color: 6096
Size: 252624 Color: 412

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 405216 Color: 8254
Size: 339922 Color: 5999
Size: 254863 Color: 689

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 407539 Color: 8303
Size: 342364 Color: 6105
Size: 250098 Color: 21

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 404970 Color: 8243
Size: 344264 Color: 6187
Size: 250767 Color: 148

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 362053 Color: 6889
Size: 353858 Color: 6576
Size: 284090 Color: 3108

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 404513 Color: 8235
Size: 338364 Color: 5940
Size: 257124 Color: 922

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 400478 Color: 8128
Size: 328400 Color: 5501
Size: 271123 Color: 2220

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 406770 Color: 8289
Size: 342480 Color: 6112
Size: 250751 Color: 145

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 383290 Color: 7635
Size: 362502 Color: 6897
Size: 254209 Color: 618

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 398378 Color: 8071
Size: 350300 Color: 6443
Size: 251323 Color: 244

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 383918 Color: 7652
Size: 355958 Color: 6646
Size: 260125 Color: 1251

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 392769 Color: 7913
Size: 353203 Color: 6546
Size: 254029 Color: 600

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 406439 Color: 8282
Size: 339925 Color: 6001
Size: 253637 Color: 545

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 369089 Color: 7158
Size: 339858 Color: 5997
Size: 291054 Color: 3533

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 402148 Color: 8177
Size: 340551 Color: 6030
Size: 257302 Color: 935

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 409038 Color: 8329
Size: 336806 Color: 5872
Size: 254157 Color: 611

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 405122 Color: 8246
Size: 338295 Color: 5937
Size: 256584 Color: 871

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 403782 Color: 8212
Size: 341112 Color: 6051
Size: 255107 Color: 718

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 397421 Color: 8039
Size: 347654 Color: 6341
Size: 254926 Color: 694

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 404221 Color: 8225
Size: 341911 Color: 6083
Size: 253869 Color: 584

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 404211 Color: 8224
Size: 340074 Color: 6005
Size: 255716 Color: 776

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 374765 Color: 7358
Size: 339065 Color: 5966
Size: 286171 Color: 3227

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 389297 Color: 7800
Size: 340602 Color: 6035
Size: 270102 Color: 2127

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 401768 Color: 8162
Size: 347526 Color: 6337
Size: 250707 Color: 139

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 404523 Color: 8236
Size: 344041 Color: 6174
Size: 251437 Color: 260

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 404537 Color: 8237
Size: 340481 Color: 6025
Size: 254983 Color: 704

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 405205 Color: 8252
Size: 327710 Color: 5463
Size: 267086 Color: 1894

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 401455 Color: 8158
Size: 343852 Color: 6167
Size: 254694 Color: 664

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 406909 Color: 8293
Size: 339412 Color: 5982
Size: 253680 Color: 551

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 409092 Color: 8331
Size: 328376 Color: 5499
Size: 262533 Color: 1477

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 393693 Color: 7940
Size: 342013 Color: 6087
Size: 264295 Color: 1641

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 404543 Color: 8238
Size: 340096 Color: 6008
Size: 255362 Color: 744

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 406720 Color: 8288
Size: 341555 Color: 6065
Size: 251726 Color: 295

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 405078 Color: 8245
Size: 340365 Color: 6020
Size: 254558 Color: 649

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 393423 Color: 7932
Size: 331838 Color: 5653
Size: 274740 Color: 2490

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 404463 Color: 8233
Size: 335840 Color: 5837
Size: 259698 Color: 1213

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 403708 Color: 8209
Size: 341234 Color: 6056
Size: 255059 Color: 714

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 360219 Color: 6812
Size: 356814 Color: 6682
Size: 282968 Color: 3038

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 357025 Color: 6687
Size: 330681 Color: 5590
Size: 312295 Color: 4726

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 401646 Color: 8160
Size: 338863 Color: 5957
Size: 259492 Color: 1192

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 395710 Color: 7991
Size: 319202 Color: 5057
Size: 285089 Color: 3163

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 391227 Color: 7864
Size: 348212 Color: 6367
Size: 260562 Color: 1301

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 402865 Color: 8194
Size: 314104 Color: 4818
Size: 283032 Color: 3043

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 398874 Color: 8090
Size: 345807 Color: 6257
Size: 255320 Color: 739

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 404376 Color: 8231
Size: 329401 Color: 5539
Size: 266224 Color: 1818

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 396582 Color: 8015
Size: 344686 Color: 6206
Size: 258733 Color: 1102

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 403541 Color: 8206
Size: 333784 Color: 5749
Size: 262676 Color: 1491

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 373222 Color: 7303
Size: 363442 Color: 6937
Size: 263337 Color: 1557

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 403996 Color: 8216
Size: 299228 Color: 4050
Size: 296777 Color: 3908

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 360239 Color: 6814
Size: 344573 Color: 6200
Size: 295189 Color: 3806

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 404908 Color: 8242
Size: 337664 Color: 5912
Size: 257429 Color: 953

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 405418 Color: 8261
Size: 339501 Color: 5985
Size: 255082 Color: 717

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 398876 Color: 8091
Size: 341558 Color: 6066
Size: 259567 Color: 1200

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 340789 Color: 6042
Size: 333892 Color: 5756
Size: 325320 Color: 5354

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 399843 Color: 8112
Size: 340554 Color: 6031
Size: 259604 Color: 1205

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 366637 Color: 7056
Size: 328248 Color: 5493
Size: 305116 Color: 4382

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 397970 Color: 8061
Size: 339241 Color: 5977
Size: 262790 Color: 1502

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 404488 Color: 8234
Size: 342240 Color: 6099
Size: 253273 Color: 506

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 365849 Color: 7020
Size: 354469 Color: 6593
Size: 279683 Color: 2814

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 401916 Color: 8167
Size: 339046 Color: 5963
Size: 259039 Color: 1143

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 401872 Color: 8166
Size: 338992 Color: 5962
Size: 259137 Color: 1161

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 373145 Color: 7300
Size: 366478 Color: 7047
Size: 260378 Color: 1279

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 401941 Color: 8169
Size: 339099 Color: 5970
Size: 258961 Color: 1133

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 400466 Color: 8127
Size: 343657 Color: 6156
Size: 255878 Color: 791

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 398358 Color: 8070
Size: 343506 Color: 6149
Size: 258137 Color: 1027

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 382781 Color: 7615
Size: 363189 Color: 6927
Size: 254031 Color: 601

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 403251 Color: 8199
Size: 341687 Color: 6070
Size: 255063 Color: 716

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 395778 Color: 7992
Size: 347943 Color: 6354
Size: 256280 Color: 838

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 359602 Color: 6793
Size: 355838 Color: 6642
Size: 284561 Color: 3133

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 401813 Color: 8165
Size: 339080 Color: 5968
Size: 259108 Color: 1156

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 399581 Color: 8107
Size: 339576 Color: 5988
Size: 260844 Color: 1327

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 404138 Color: 8222
Size: 339221 Color: 5976
Size: 256642 Color: 881

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 341794 Color: 6076
Size: 334998 Color: 5801
Size: 323209 Color: 5259

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 392784 Color: 7914
Size: 352077 Color: 6507
Size: 255140 Color: 721

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 392982 Color: 7919
Size: 342095 Color: 6094
Size: 264924 Color: 1691

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 398526 Color: 8078
Size: 340008 Color: 6003
Size: 261467 Color: 1386

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 407094 Color: 8296
Size: 327964 Color: 5475
Size: 264943 Color: 1693

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 395386 Color: 7980
Size: 347893 Color: 6349
Size: 256722 Color: 891

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 403710 Color: 8210
Size: 322480 Color: 5218
Size: 273811 Color: 2414

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 408865 Color: 8325
Size: 333563 Color: 5737
Size: 257573 Color: 965

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 401331 Color: 8153
Size: 339528 Color: 5986
Size: 259142 Color: 1162

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 406580 Color: 8284
Size: 328801 Color: 5514
Size: 264620 Color: 1670

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 402667 Color: 8191
Size: 342326 Color: 6102
Size: 255008 Color: 710

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 390068 Color: 7825
Size: 352216 Color: 6514
Size: 257717 Color: 982

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 402168 Color: 8179
Size: 339532 Color: 5987
Size: 258301 Color: 1051

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 407267 Color: 8301
Size: 338888 Color: 5958
Size: 253846 Color: 580

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 374395 Color: 7346
Size: 367412 Color: 7092
Size: 258194 Color: 1035

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 402028 Color: 8175
Size: 344570 Color: 6198
Size: 253403 Color: 518

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 404289 Color: 8227
Size: 337427 Color: 5898
Size: 258285 Color: 1048

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 405213 Color: 8253
Size: 337564 Color: 5904
Size: 257224 Color: 930

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 403291 Color: 8203
Size: 345390 Color: 6233
Size: 251320 Color: 243

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 403796 Color: 8214
Size: 343318 Color: 6145
Size: 252887 Color: 440

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 409144 Color: 8333
Size: 337073 Color: 5885
Size: 253784 Color: 565

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 402126 Color: 8176
Size: 345842 Color: 6260
Size: 252033 Color: 337

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 397368 Color: 8037
Size: 328061 Color: 5480
Size: 274572 Color: 2478

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 402011 Color: 8173
Size: 344572 Color: 6199
Size: 253418 Color: 519

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 394456 Color: 7959
Size: 340476 Color: 6024
Size: 265069 Color: 1709

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 403765 Color: 8211
Size: 336921 Color: 5878
Size: 259315 Color: 1178

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 339095 Color: 5969
Size: 331403 Color: 5626
Size: 329503 Color: 5545

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 397866 Color: 8057
Size: 340089 Color: 6007
Size: 262046 Color: 1429

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 395128 Color: 7971
Size: 339217 Color: 5975
Size: 265656 Color: 1758

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 404817 Color: 8241
Size: 336914 Color: 5877
Size: 258270 Color: 1046

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 401271 Color: 8149
Size: 344161 Color: 6181
Size: 254569 Color: 651

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 398920 Color: 8094
Size: 342899 Color: 6130
Size: 258182 Color: 1032

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 401417 Color: 8156
Size: 339791 Color: 5994
Size: 258793 Color: 1108

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 401008 Color: 8143
Size: 336516 Color: 5863
Size: 262477 Color: 1468

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 403267 Color: 8201
Size: 336699 Color: 5867
Size: 260035 Color: 1243

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 404590 Color: 8240
Size: 339286 Color: 5980
Size: 256125 Color: 820

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 355903 Color: 6645
Size: 345534 Color: 6242
Size: 298564 Color: 4005

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 408076 Color: 8313
Size: 339497 Color: 5984
Size: 252428 Color: 386

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 401016 Color: 8144
Size: 345787 Color: 6253
Size: 253198 Color: 493

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 401428 Color: 8157
Size: 338975 Color: 5961
Size: 259598 Color: 1203

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 400662 Color: 8135
Size: 336633 Color: 5866
Size: 262706 Color: 1496

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 400828 Color: 8139
Size: 336344 Color: 5859
Size: 262829 Color: 1508

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 371769 Color: 7247
Size: 368524 Color: 7140
Size: 259708 Color: 1215

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 394008 Color: 7948
Size: 328836 Color: 5515
Size: 277157 Color: 2659

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 379963 Color: 7531
Size: 354983 Color: 6610
Size: 265055 Color: 1708

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 393901 Color: 7945
Size: 340352 Color: 6019
Size: 265748 Color: 1766

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 393533 Color: 7937
Size: 345911 Color: 6265
Size: 260557 Color: 1298

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 393566 Color: 7938
Size: 353641 Color: 6562
Size: 252794 Color: 431

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 400082 Color: 8117
Size: 338596 Color: 5946
Size: 261323 Color: 1376

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 400516 Color: 8129
Size: 343365 Color: 6147
Size: 256120 Color: 819

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 397915 Color: 8059
Size: 345857 Color: 6262
Size: 256229 Color: 831

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 400570 Color: 8131
Size: 341709 Color: 6072
Size: 257722 Color: 983

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 397787 Color: 8053
Size: 340293 Color: 6017
Size: 261921 Color: 1417

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 399548 Color: 8106
Size: 339877 Color: 5998
Size: 260576 Color: 1303

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 404160 Color: 8223
Size: 336009 Color: 5846
Size: 259832 Color: 1229

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 363493 Color: 6939
Size: 355003 Color: 6611
Size: 281505 Color: 2950

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 372215 Color: 7275
Size: 354739 Color: 6605
Size: 273047 Color: 2358

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 391885 Color: 7889
Size: 338254 Color: 5935
Size: 269862 Color: 2096

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 399954 Color: 8115
Size: 335690 Color: 5835
Size: 264357 Color: 1648

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 400238 Color: 8121
Size: 335949 Color: 5842
Size: 263814 Color: 1597

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 400364 Color: 8125
Size: 342971 Color: 6133
Size: 256666 Color: 884

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 392668 Color: 7911
Size: 335614 Color: 5834
Size: 271719 Color: 2269

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 400193 Color: 8120
Size: 335907 Color: 5841
Size: 263901 Color: 1609

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 399760 Color: 8111
Size: 334823 Color: 5793
Size: 265418 Color: 1738

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 394698 Color: 7963
Size: 351975 Color: 6503
Size: 253328 Color: 510

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 372157 Color: 7271
Size: 352978 Color: 6538
Size: 274866 Color: 2497

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 391426 Color: 7873
Size: 348279 Color: 6369
Size: 260296 Color: 1271

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 394053 Color: 7949
Size: 339217 Color: 5974
Size: 266731 Color: 1861

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 396954 Color: 8025
Size: 344819 Color: 6212
Size: 258228 Color: 1042

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 401301 Color: 8151
Size: 337975 Color: 5924
Size: 260725 Color: 1319

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 391514 Color: 7876
Size: 335232 Color: 5821
Size: 273255 Color: 2374

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 395369 Color: 7978
Size: 331990 Color: 5659
Size: 272642 Color: 2326

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 402415 Color: 8185
Size: 335096 Color: 5807
Size: 262490 Color: 1470

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 405393 Color: 8260
Size: 341124 Color: 6053
Size: 253484 Color: 525

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 398892 Color: 8093
Size: 339415 Color: 5983
Size: 261694 Color: 1400

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 402531 Color: 8189
Size: 335064 Color: 5802
Size: 262406 Color: 1459

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 396636 Color: 8018
Size: 337933 Color: 5922
Size: 265432 Color: 1739

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 398819 Color: 8088
Size: 342797 Color: 6127
Size: 258385 Color: 1062

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 399201 Color: 8100
Size: 334895 Color: 5798
Size: 265905 Color: 1779

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 398826 Color: 8089
Size: 335097 Color: 5808
Size: 266078 Color: 1800

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 391563 Color: 7879
Size: 350991 Color: 6468
Size: 257447 Color: 955

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 398764 Color: 8085
Size: 321379 Color: 5170
Size: 279858 Color: 2825

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 390236 Color: 7830
Size: 347593 Color: 6340
Size: 262172 Color: 1441

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 398786 Color: 8086
Size: 335363 Color: 5828
Size: 265852 Color: 1775

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 394179 Color: 7951
Size: 333603 Color: 5740
Size: 272219 Color: 2301

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 390520 Color: 7841
Size: 343126 Color: 6138
Size: 266355 Color: 1829

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 402403 Color: 8184
Size: 337144 Color: 5887
Size: 260454 Color: 1289

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 398990 Color: 8096
Size: 330328 Color: 5573
Size: 270683 Color: 2180

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 399755 Color: 8110
Size: 331200 Color: 5617
Size: 269046 Color: 2034

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 398442 Color: 8074
Size: 335206 Color: 5820
Size: 266353 Color: 1828

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 390448 Color: 7839
Size: 346757 Color: 6306
Size: 262796 Color: 1503

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 398579 Color: 8081
Size: 342393 Color: 6107
Size: 259029 Color: 1142

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 395223 Color: 7974
Size: 334646 Color: 5787
Size: 270132 Color: 2132

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 405822 Color: 8269
Size: 322847 Color: 5241
Size: 271332 Color: 2237

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 401333 Color: 8154
Size: 334839 Color: 5794
Size: 263829 Color: 1600

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 396510 Color: 8012
Size: 321443 Color: 5176
Size: 282048 Color: 2977

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 364688 Color: 6973
Size: 364575 Color: 6968
Size: 270738 Color: 2183

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 398632 Color: 8082
Size: 334659 Color: 5788
Size: 266710 Color: 1858

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 405143 Color: 8249
Size: 334711 Color: 5792
Size: 260147 Color: 1254

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 398444 Color: 8075
Size: 334701 Color: 5791
Size: 266856 Color: 1875

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 370404 Color: 7206
Size: 370048 Color: 7188
Size: 259549 Color: 1196

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 369638 Color: 7175
Size: 366475 Color: 7046
Size: 263888 Color: 1606

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 380090 Color: 7537
Size: 331530 Color: 5635
Size: 288381 Color: 3379

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 369084 Color: 7157
Size: 365712 Color: 7013
Size: 265205 Color: 1722

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 396260 Color: 8003
Size: 331623 Color: 5642
Size: 272118 Color: 2294

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 396652 Color: 8020
Size: 319737 Color: 5086
Size: 283612 Color: 3075

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 397957 Color: 8060
Size: 335298 Color: 5826
Size: 266746 Color: 1863

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 403257 Color: 8200
Size: 343290 Color: 6143
Size: 253454 Color: 522

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 403035 Color: 8197
Size: 334350 Color: 5776
Size: 262616 Color: 1482

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 371133 Color: 7229
Size: 358271 Color: 6739
Size: 270597 Color: 2166

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 398746 Color: 8084
Size: 339642 Color: 5990
Size: 261613 Color: 1396

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 402237 Color: 8182
Size: 327527 Color: 5450
Size: 270237 Color: 2138

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 336099 Color: 5848
Size: 333784 Color: 5750
Size: 330118 Color: 5564

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 349279 Color: 6405
Size: 333143 Color: 5722
Size: 317579 Color: 4994

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 397442 Color: 8041
Size: 333884 Color: 5755
Size: 268675 Color: 2010

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 397616 Color: 8048
Size: 331468 Color: 5630
Size: 270917 Color: 2197

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 402008 Color: 8172
Size: 333810 Color: 5752
Size: 264183 Color: 1627

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 395641 Color: 7988
Size: 346398 Color: 6289
Size: 257962 Color: 1003

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 397087 Color: 8027
Size: 334072 Color: 5766
Size: 268842 Color: 2024

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 397346 Color: 8035
Size: 333753 Color: 5748
Size: 268902 Color: 2026

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 391610 Color: 7881
Size: 328630 Color: 5510
Size: 279761 Color: 2819

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 401791 Color: 8164
Size: 331323 Color: 5622
Size: 266887 Color: 1880

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 397284 Color: 8033
Size: 335101 Color: 5809
Size: 267616 Color: 1928

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 388787 Color: 7783
Size: 321530 Color: 5181
Size: 289684 Color: 3448

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 402207 Color: 8181
Size: 338665 Color: 5950
Size: 259129 Color: 1159

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 358115 Color: 6733
Size: 357147 Color: 6693
Size: 284739 Color: 3144

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 397664 Color: 8051
Size: 334025 Color: 5763
Size: 268312 Color: 1981

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 404318 Color: 8228
Size: 327084 Color: 5430
Size: 268599 Color: 2007

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 399926 Color: 8113
Size: 334032 Color: 5764
Size: 266043 Color: 1794

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 370610 Color: 7211
Size: 337516 Color: 5903
Size: 291875 Color: 3587

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 397507 Color: 8045
Size: 340060 Color: 6004
Size: 262434 Color: 1463

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 397434 Color: 8040
Size: 337450 Color: 5901
Size: 265117 Color: 1716

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 401924 Color: 8168
Size: 333714 Color: 5746
Size: 264363 Color: 1649

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 404369 Color: 8230
Size: 329261 Color: 5536
Size: 266371 Color: 1831

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 368031 Color: 7122
Size: 341801 Color: 6078
Size: 290169 Color: 3486

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 402149 Color: 8178
Size: 333447 Color: 5731
Size: 264405 Color: 1654

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 353392 Color: 6555
Size: 330095 Color: 5563
Size: 316514 Color: 4948

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 404324 Color: 8229
Size: 332399 Color: 5683
Size: 263278 Color: 1554

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 372956 Color: 7292
Size: 362906 Color: 6919
Size: 264139 Color: 1620

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 397099 Color: 8028
Size: 315138 Color: 4878
Size: 287764 Color: 3345

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 365891 Color: 7021
Size: 319355 Color: 5065
Size: 314755 Color: 4855

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 396742 Color: 8021
Size: 333637 Color: 5741
Size: 269622 Color: 2078

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 402432 Color: 8186
Size: 336049 Color: 5847
Size: 261520 Color: 1392

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 401756 Color: 8161
Size: 333385 Color: 5729
Size: 264860 Color: 1685

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 396859 Color: 8024
Size: 346641 Color: 6303
Size: 256501 Color: 862

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 400573 Color: 8132
Size: 332955 Color: 5714
Size: 266473 Color: 1839

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 399329 Color: 8104
Size: 330736 Color: 5592
Size: 269936 Color: 2099

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 407121 Color: 8298
Size: 329232 Color: 5533
Size: 263648 Color: 1585

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 403638 Color: 8208
Size: 328952 Color: 5522
Size: 267411 Color: 1910

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 394758 Color: 7965
Size: 335113 Color: 5810
Size: 270130 Color: 2130

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 395819 Color: 7993
Size: 334547 Color: 5782
Size: 269635 Color: 2079

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 400778 Color: 8138
Size: 333599 Color: 5739
Size: 265624 Color: 1751

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 405188 Color: 8251
Size: 324430 Color: 5316
Size: 270383 Color: 2147

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 396439 Color: 8009
Size: 342588 Color: 6116
Size: 260974 Color: 1344

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 396423 Color: 8008
Size: 332971 Color: 5715
Size: 270607 Color: 2167

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 375936 Color: 7403
Size: 360615 Color: 6830
Size: 263450 Color: 1569

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 396363 Color: 8006
Size: 338142 Color: 5931
Size: 265496 Color: 1741

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 397362 Color: 8036
Size: 313518 Color: 4777
Size: 289121 Color: 3420

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 396369 Color: 8007
Size: 342600 Color: 6119
Size: 261032 Color: 1350

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 396545 Color: 8013
Size: 309852 Color: 4618
Size: 293604 Color: 3710

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 382967 Color: 7623
Size: 345193 Color: 6226
Size: 271841 Color: 2279

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 400159 Color: 8119
Size: 336144 Color: 5850
Size: 263698 Color: 1588

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 396222 Color: 8001
Size: 338103 Color: 5929
Size: 265676 Color: 1760

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 395697 Color: 7989
Size: 330762 Color: 5593
Size: 273542 Color: 2390

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 396022 Color: 7997
Size: 340487 Color: 6026
Size: 263492 Color: 1572

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 395550 Color: 7983
Size: 331075 Color: 5611
Size: 273376 Color: 2376

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 395930 Color: 7995
Size: 332721 Color: 5704
Size: 271350 Color: 2240

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 395706 Color: 7990
Size: 333020 Color: 5717
Size: 271275 Color: 2232

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 403315 Color: 8204
Size: 333254 Color: 5725
Size: 263432 Color: 1566

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 395820 Color: 7994
Size: 332674 Color: 5699
Size: 271507 Color: 2248

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 391398 Color: 7871
Size: 335151 Color: 5818
Size: 273452 Color: 2383

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 396626 Color: 8016
Size: 348524 Color: 6385
Size: 254851 Color: 687

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 396554 Color: 8014
Size: 352357 Color: 6523
Size: 251090 Color: 207

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 395991 Color: 7996
Size: 338224 Color: 5934
Size: 265786 Color: 1771

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 395629 Color: 7987
Size: 341830 Color: 6082
Size: 262542 Color: 1478

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 405143 Color: 8248
Size: 327862 Color: 5470
Size: 266996 Color: 1885

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 399643 Color: 8109
Size: 335973 Color: 5844
Size: 264385 Color: 1653

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 395615 Color: 7986
Size: 345208 Color: 6227
Size: 259178 Color: 1166

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 402480 Color: 8187
Size: 331007 Color: 5603
Size: 266514 Color: 1841

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 400240 Color: 8122
Size: 306570 Color: 4444
Size: 293191 Color: 3675

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 391882 Color: 7888
Size: 336853 Color: 5875
Size: 271266 Color: 2231

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 398511 Color: 8076
Size: 347346 Color: 6328
Size: 254144 Color: 609

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 395299 Color: 7976
Size: 341573 Color: 6067
Size: 263129 Color: 1541

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 408025 Color: 8311
Size: 329482 Color: 5544
Size: 262494 Color: 1471

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 405051 Color: 8244
Size: 332166 Color: 5671
Size: 262784 Color: 1501

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 396221 Color: 8000
Size: 332193 Color: 5673
Size: 271587 Color: 2256

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 377114 Color: 7448
Size: 344712 Color: 6207
Size: 278175 Color: 2721

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 394912 Color: 7969
Size: 338525 Color: 5945
Size: 266564 Color: 1848

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 394810 Color: 7967
Size: 342052 Color: 6090
Size: 263139 Color: 1544

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 389153 Color: 7796
Size: 337449 Color: 5900
Size: 273399 Color: 2378

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 394762 Color: 7966
Size: 346293 Color: 6284
Size: 258946 Color: 1130

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 396643 Color: 8019
Size: 337797 Color: 5916
Size: 265561 Color: 1747

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 394337 Color: 7956
Size: 334120 Color: 5767
Size: 271544 Color: 2252

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 392784 Color: 7915
Size: 337201 Color: 5891
Size: 270016 Color: 2116

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 397571 Color: 8046
Size: 336293 Color: 5855
Size: 266137 Color: 1807

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 394198 Color: 7952
Size: 340082 Color: 6006
Size: 265721 Color: 1765

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 394137 Color: 7950
Size: 338425 Color: 5942
Size: 267439 Color: 1912

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 394582 Color: 7960
Size: 338602 Color: 5947
Size: 266817 Color: 1873

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 408187 Color: 8314
Size: 331822 Color: 5650
Size: 259992 Color: 1238

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 394325 Color: 7955
Size: 332075 Color: 5663
Size: 273601 Color: 2395

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 394372 Color: 7958
Size: 318318 Color: 5028
Size: 287311 Color: 3319

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 350849 Color: 6462
Size: 350161 Color: 6434
Size: 298991 Color: 4033

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 398547 Color: 8080
Size: 328764 Color: 5512
Size: 272690 Color: 2330

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 398336 Color: 8067
Size: 324998 Color: 5339
Size: 276667 Color: 2618

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 393511 Color: 7934
Size: 345055 Color: 6223
Size: 261435 Color: 1384

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 400732 Color: 8137
Size: 331635 Color: 5643
Size: 267634 Color: 1929

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 390660 Color: 7847
Size: 347766 Color: 6344
Size: 261575 Color: 1394

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 393856 Color: 7943
Size: 331588 Color: 5641
Size: 274557 Color: 2474

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 397855 Color: 8056
Size: 344802 Color: 6211
Size: 257344 Color: 947

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 393743 Color: 7942
Size: 342722 Color: 6124
Size: 263536 Color: 1576

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 409035 Color: 8328
Size: 334629 Color: 5785
Size: 256337 Color: 844

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 400643 Color: 8134
Size: 331567 Color: 5638
Size: 267791 Color: 1938

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 391291 Color: 7867
Size: 345587 Color: 6244
Size: 263123 Color: 1540

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 406161 Color: 8273
Size: 328294 Color: 5495
Size: 265546 Color: 1745

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 400677 Color: 8136
Size: 338718 Color: 5952
Size: 260606 Color: 1305

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 400404 Color: 8126
Size: 331416 Color: 5628
Size: 268181 Color: 1968

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 393232 Color: 7928
Size: 332769 Color: 5705
Size: 274000 Color: 2435

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 402016 Color: 8174
Size: 322646 Color: 5229
Size: 275339 Color: 2531

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 396125 Color: 7998
Size: 335997 Color: 5845
Size: 267879 Color: 1944

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 393937 Color: 7946
Size: 327573 Color: 5454
Size: 278491 Color: 2745

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 400547 Color: 8130
Size: 331493 Color: 5633
Size: 267961 Color: 1952

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 400631 Color: 8133
Size: 334022 Color: 5762
Size: 265348 Color: 1734

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 393250 Color: 7929
Size: 331361 Color: 5624
Size: 275390 Color: 2536

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 393254 Color: 7930
Size: 338429 Color: 5943
Size: 268318 Color: 1983

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 393179 Color: 7924
Size: 336703 Color: 5868
Size: 270119 Color: 2128

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 398403 Color: 8073
Size: 327558 Color: 5453
Size: 274040 Color: 2436

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 391279 Color: 7866
Size: 337715 Color: 5914
Size: 271007 Color: 2207

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 393188 Color: 7925
Size: 336796 Color: 5871
Size: 270017 Color: 2117

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 392427 Color: 7906
Size: 330684 Color: 5591
Size: 276890 Color: 2636

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 393136 Color: 7923
Size: 331169 Color: 5616
Size: 275696 Color: 2561

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 393087 Color: 7922
Size: 342781 Color: 6126
Size: 264133 Color: 1619

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 370029 Color: 7187
Size: 356277 Color: 6658
Size: 273695 Color: 2404

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 397892 Color: 8058
Size: 331087 Color: 5612
Size: 271022 Color: 2208

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 393050 Color: 7921
Size: 329163 Color: 5529
Size: 277788 Color: 2701

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 392188 Color: 7897
Size: 337852 Color: 5918
Size: 269961 Color: 2106

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 392506 Color: 7908
Size: 347077 Color: 6320
Size: 260418 Color: 1285

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 394923 Color: 7970
Size: 338322 Color: 5938
Size: 266756 Color: 1865

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 408075 Color: 8312
Size: 337577 Color: 5905
Size: 254349 Color: 626

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 399985 Color: 8116
Size: 333698 Color: 5744
Size: 266318 Color: 1827

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 397455 Color: 8043
Size: 331122 Color: 5613
Size: 271424 Color: 2245

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 392828 Color: 7916
Size: 350587 Color: 6451
Size: 256586 Color: 873

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 392154 Color: 7895
Size: 330635 Color: 5587
Size: 277212 Color: 2663

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 392167 Color: 7896
Size: 341746 Color: 6075
Size: 266088 Color: 1804

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 395143 Color: 7973
Size: 330987 Color: 5600
Size: 273871 Color: 2424

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 394299 Color: 7953
Size: 343837 Color: 6165
Size: 261865 Color: 1413

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 404084 Color: 8218
Size: 327617 Color: 5457
Size: 268300 Color: 1977

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 392361 Color: 7903
Size: 340378 Color: 6022
Size: 267262 Color: 1902

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 392140 Color: 7894
Size: 311619 Color: 4700
Size: 296242 Color: 3879

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 392349 Color: 7901
Size: 330675 Color: 5589
Size: 276977 Color: 2642

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 392682 Color: 7912
Size: 333453 Color: 5733
Size: 273866 Color: 2423

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 367688 Color: 7101
Size: 350308 Color: 6444
Size: 282005 Color: 2976

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 392360 Color: 7902
Size: 351607 Color: 6489
Size: 256034 Color: 810

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 391995 Color: 7891
Size: 330448 Color: 5580
Size: 277558 Color: 2686

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 367234 Color: 7081
Size: 333951 Color: 5759
Size: 298816 Color: 4018

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 351958 Color: 6502
Size: 328241 Color: 5491
Size: 319802 Color: 5090

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 400277 Color: 8123
Size: 343042 Color: 6137
Size: 256682 Color: 889

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 391804 Color: 7886
Size: 355807 Color: 6638
Size: 252390 Color: 379

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 381963 Color: 7582
Size: 344367 Color: 6191
Size: 273671 Color: 2400

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 379540 Color: 7513
Size: 346699 Color: 6304
Size: 273762 Color: 2407

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 393211 Color: 7927
Size: 338801 Color: 5954
Size: 267989 Color: 1953

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 397799 Color: 8054
Size: 351475 Color: 6482
Size: 250727 Color: 143

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 399240 Color: 8101
Size: 334038 Color: 5765
Size: 266723 Color: 1860

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 404381 Color: 8232
Size: 344223 Color: 6183
Size: 251397 Color: 254

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 406700 Color: 8287
Size: 328076 Color: 5482
Size: 265225 Color: 1724

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 399314 Color: 8103
Size: 330391 Color: 5575
Size: 270296 Color: 2142

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 391497 Color: 7875
Size: 353856 Color: 6575
Size: 254648 Color: 657

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 391346 Color: 7869
Size: 340334 Color: 6018
Size: 268321 Color: 1984

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 356204 Color: 6655
Size: 340965 Color: 6046
Size: 302832 Color: 4263

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 408690 Color: 8322
Size: 337816 Color: 5917
Size: 253495 Color: 528

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 391056 Color: 7857
Size: 349181 Color: 6398
Size: 259764 Color: 1223

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 388164 Color: 7771
Size: 332571 Color: 5693
Size: 279266 Color: 2792

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 399612 Color: 8108
Size: 344372 Color: 6192
Size: 256017 Color: 807

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 391798 Color: 7885
Size: 332687 Color: 5701
Size: 275516 Color: 2548

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 391187 Color: 7863
Size: 329534 Color: 5546
Size: 279280 Color: 2793

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 398664 Color: 8083
Size: 329814 Color: 5558
Size: 271523 Color: 2249

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 396488 Color: 8011
Size: 346895 Color: 6315
Size: 256618 Color: 878

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 389323 Color: 7802
Size: 331643 Color: 5644
Size: 279035 Color: 2778

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 391099 Color: 7860
Size: 345513 Color: 6239
Size: 263389 Color: 1561

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 364684 Color: 6972
Size: 331921 Color: 5655
Size: 303396 Color: 4288

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 390995 Color: 7856
Size: 331346 Color: 5623
Size: 277660 Color: 2693

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 405732 Color: 8268
Size: 336168 Color: 5852
Size: 258101 Color: 1018

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 395567 Color: 7984
Size: 340109 Color: 6009
Size: 264325 Color: 1645

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 371155 Color: 7230
Size: 337780 Color: 5915
Size: 291066 Color: 3534

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 406460 Color: 8283
Size: 329127 Color: 5526
Size: 264414 Color: 1656

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 388943 Color: 7785
Size: 328616 Color: 5509
Size: 282442 Color: 3000

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 405146 Color: 8250
Size: 330204 Color: 5569
Size: 264651 Color: 1672

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 402716 Color: 8193
Size: 328460 Color: 5502
Size: 268825 Color: 2022

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 390107 Color: 7827
Size: 337270 Color: 5894
Size: 272624 Color: 2324

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 389984 Color: 7819
Size: 357854 Color: 6720
Size: 252163 Color: 356

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 389878 Color: 7816
Size: 334872 Color: 5796
Size: 275251 Color: 2523

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 390582 Color: 7844
Size: 337883 Color: 5920
Size: 271536 Color: 2251

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 390699 Color: 7849
Size: 338018 Color: 5926
Size: 271284 Color: 2233

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 390202 Color: 7829
Size: 343381 Color: 6148
Size: 266418 Color: 1834

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 368941 Color: 7154
Size: 358436 Color: 6744
Size: 272624 Color: 2325

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 404246 Color: 8226
Size: 308170 Color: 4527
Size: 287585 Color: 3337

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 398964 Color: 8095
Size: 339923 Color: 6000
Size: 261114 Color: 1358

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 393964 Color: 7947
Size: 324327 Color: 5310
Size: 281710 Color: 2963

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 390284 Color: 7833
Size: 328561 Color: 5507
Size: 281156 Color: 2923

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 392983 Color: 7920
Size: 329188 Color: 5531
Size: 277830 Color: 2706

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 399358 Color: 8105
Size: 324042 Color: 5298
Size: 276601 Color: 2614

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 399028 Color: 8097
Size: 330919 Color: 5598
Size: 270054 Color: 2122

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 390014 Color: 7822
Size: 337236 Color: 5892
Size: 272751 Color: 2335

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 390115 Color: 7828
Size: 337365 Color: 5897
Size: 272521 Color: 2318

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 397141 Color: 8030
Size: 328247 Color: 5492
Size: 274613 Color: 2482

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 390284 Color: 7832
Size: 340190 Color: 6011
Size: 269527 Color: 2072

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 390307 Color: 7834
Size: 328565 Color: 5508
Size: 281129 Color: 2920

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 390532 Color: 7842
Size: 332476 Color: 5690
Size: 276993 Color: 2643

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 403012 Color: 8196
Size: 330842 Color: 5594
Size: 266147 Color: 1809

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 390494 Color: 7840
Size: 349263 Color: 6403
Size: 260244 Color: 1265

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 390006 Color: 7821
Size: 337161 Color: 5889
Size: 272834 Color: 2344

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 401063 Color: 8147
Size: 337177 Color: 5890
Size: 261761 Color: 1406

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 390434 Color: 7838
Size: 344382 Color: 6193
Size: 265185 Color: 1720

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 398516 Color: 8077
Size: 334176 Color: 5770
Size: 267309 Color: 1904

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 389149 Color: 7795
Size: 360055 Color: 6809
Size: 250797 Color: 151

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 389987 Color: 7820
Size: 328069 Color: 5481
Size: 281945 Color: 2971

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 405467 Color: 8262
Size: 317706 Color: 4999
Size: 276828 Color: 2632

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 341743 Color: 6074
Size: 339073 Color: 5967
Size: 319185 Color: 5056

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 364791 Color: 6977
Size: 361382 Color: 6865
Size: 273828 Color: 2418

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 389360 Color: 7804
Size: 330167 Color: 5567
Size: 280474 Color: 2873

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 389299 Color: 7801
Size: 334309 Color: 5775
Size: 276393 Color: 2602

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 356496 Color: 6668
Size: 345427 Color: 6236
Size: 298078 Color: 3978

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 388328 Color: 7776
Size: 328740 Color: 5511
Size: 282933 Color: 3036

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 395602 Color: 7985
Size: 330161 Color: 5566
Size: 274238 Color: 2452

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 360325 Color: 6818
Size: 359504 Color: 6789
Size: 280172 Color: 2851

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 389110 Color: 7792
Size: 337059 Color: 5884
Size: 273832 Color: 2419

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 352689 Color: 6531
Size: 327669 Color: 5460
Size: 319643 Color: 5082

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 389066 Color: 7791
Size: 347419 Color: 6330
Size: 263516 Color: 1574

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 391745 Color: 7884
Size: 327779 Color: 5466
Size: 280477 Color: 2874

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 395350 Color: 7977
Size: 327494 Color: 5449
Size: 277157 Color: 2660

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 365750 Color: 7015
Size: 360692 Color: 6832
Size: 273559 Color: 2391

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 389267 Color: 7799
Size: 343952 Color: 6172
Size: 266782 Color: 1870

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 389114 Color: 7793
Size: 327322 Color: 5439
Size: 283565 Color: 3073

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 393888 Color: 7944
Size: 349298 Color: 6406
Size: 256815 Color: 896

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 360049 Color: 6808
Size: 343335 Color: 6146
Size: 296617 Color: 3900

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 402614 Color: 8190
Size: 327383 Color: 5445
Size: 270004 Color: 2113

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 388591 Color: 7780
Size: 337598 Color: 5907
Size: 273812 Color: 2415

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 376647 Color: 7432
Size: 352796 Color: 6535
Size: 270558 Color: 2163

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 388212 Color: 7772
Size: 327167 Color: 5432
Size: 284622 Color: 3138

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 389416 Color: 7805
Size: 353990 Color: 6577
Size: 256595 Color: 874

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 397835 Color: 8055
Size: 330447 Color: 5579
Size: 271719 Color: 2268

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 366210 Color: 7034
Size: 319969 Color: 5099
Size: 313822 Color: 4798

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 359197 Color: 6777
Size: 328153 Color: 5488
Size: 312651 Color: 4742

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 366048 Color: 7026
Size: 325364 Color: 5356
Size: 308589 Color: 4553

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 363594 Color: 6942
Size: 332492 Color: 5691
Size: 303915 Color: 4311

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 369651 Color: 7176
Size: 346008 Color: 6272
Size: 284342 Color: 3124

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 372992 Color: 7294
Size: 332319 Color: 5678
Size: 294690 Color: 3771

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 389338 Color: 7803
Size: 326867 Color: 5421
Size: 283796 Color: 3086

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 367263 Color: 7082
Size: 353703 Color: 6565
Size: 279035 Color: 2777

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 372933 Color: 7291
Size: 333569 Color: 5738
Size: 293499 Color: 3701

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 391532 Color: 7878
Size: 336360 Color: 5861
Size: 272109 Color: 2293

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 391846 Color: 7887
Size: 357648 Color: 6712
Size: 250507 Color: 110

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 392901 Color: 7918
Size: 334977 Color: 5799
Size: 272123 Color: 2295

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 406818 Color: 8291
Size: 308117 Color: 4522
Size: 285066 Color: 3160

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 395385 Color: 7979
Size: 334539 Color: 5781
Size: 270077 Color: 2124

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 371941 Color: 7258
Size: 343849 Color: 6166
Size: 284211 Color: 3114

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 371971 Color: 7260
Size: 343754 Color: 6160
Size: 284276 Color: 3117

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 364808 Color: 6978
Size: 347732 Color: 6343
Size: 287461 Color: 3329

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 404073 Color: 8217
Size: 304649 Color: 4353
Size: 291279 Color: 3548

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 378602 Color: 7484
Size: 350367 Color: 6446
Size: 271032 Color: 2209

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 371915 Color: 7256
Size: 325971 Color: 5383
Size: 302115 Color: 4211

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 354860 Color: 6609
Size: 346759 Color: 6307
Size: 298382 Color: 3997

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 382892 Color: 7618
Size: 325323 Color: 5355
Size: 291786 Color: 3578

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 389215 Color: 7797
Size: 323949 Color: 5292
Size: 286837 Color: 3287

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 349048 Color: 6395
Size: 348065 Color: 6360
Size: 302888 Color: 4268

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 359556 Color: 6792
Size: 348423 Color: 6377
Size: 292022 Color: 3596

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 373006 Color: 7295
Size: 371117 Color: 7228
Size: 255878 Color: 792

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 359980 Color: 6805
Size: 332949 Color: 5713
Size: 307072 Color: 4475

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 365783 Color: 7017
Size: 327362 Color: 5442
Size: 306856 Color: 4460

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 405374 Color: 8259
Size: 326819 Color: 5419
Size: 267808 Color: 1941

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 349077 Color: 6396
Size: 346312 Color: 6286
Size: 304612 Color: 4350

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 408899 Color: 8326
Size: 327668 Color: 5459
Size: 263434 Color: 1568

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 357119 Color: 6690
Size: 336139 Color: 5849
Size: 306743 Color: 4455

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 407997 Color: 8310
Size: 320684 Color: 5135
Size: 271320 Color: 2236

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 396443 Color: 8010
Size: 322646 Color: 5230
Size: 280912 Color: 2899

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 398260 Color: 8066
Size: 345849 Color: 6261
Size: 255892 Color: 793

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 369721 Color: 7180
Size: 330184 Color: 5568
Size: 300096 Color: 4094

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 369394 Color: 7168
Size: 343514 Color: 6150
Size: 287093 Color: 3303

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 348617 Color: 6388
Size: 344941 Color: 6221
Size: 306443 Color: 4435

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 367219 Color: 7079
Size: 324424 Color: 5315
Size: 308358 Color: 4538

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 371883 Color: 7253
Size: 326968 Color: 5424
Size: 301150 Color: 4151

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 362828 Color: 6915
Size: 319372 Color: 5066
Size: 317801 Color: 5003

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 359137 Color: 6773
Size: 346903 Color: 6316
Size: 293961 Color: 3731

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 403270 Color: 8202
Size: 333899 Color: 5757
Size: 262832 Color: 1510

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 340367 Color: 6021
Size: 337029 Color: 5881
Size: 322605 Color: 5225

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 367036 Color: 7074
Size: 326323 Color: 5396
Size: 306642 Color: 4450

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 366597 Color: 7053
Size: 325511 Color: 5362
Size: 307893 Color: 4513

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 368394 Color: 7134
Size: 322475 Color: 5216
Size: 309132 Color: 4582

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 366519 Color: 7050
Size: 337691 Color: 5913
Size: 295791 Color: 3843

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 359656 Color: 6798
Size: 337645 Color: 5910
Size: 302700 Color: 4251

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 353386 Color: 6554
Size: 332814 Color: 5708
Size: 313801 Color: 4795

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 374606 Color: 7351
Size: 371830 Color: 7250
Size: 253565 Color: 535

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 407806 Color: 8305
Size: 337045 Color: 5883
Size: 255150 Color: 723

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 346630 Color: 6301
Size: 330620 Color: 5585
Size: 322751 Color: 5236

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 365185 Color: 6999
Size: 329153 Color: 5528
Size: 305663 Color: 4402

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 368536 Color: 7141
Size: 367899 Color: 7115
Size: 263566 Color: 1580

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 367549 Color: 7095
Size: 342994 Color: 6134
Size: 289458 Color: 3437

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 347510 Color: 6336
Size: 342603 Color: 6120
Size: 309888 Color: 4622

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 366633 Color: 7055
Size: 340195 Color: 6012
Size: 293173 Color: 3672

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 372016 Color: 7261
Size: 369567 Color: 7173
Size: 258418 Color: 1067

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 360015 Color: 6807
Size: 344969 Color: 6222
Size: 295017 Color: 3796

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 359630 Color: 6796
Size: 330503 Color: 5582
Size: 309868 Color: 4621

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 394359 Color: 7957
Size: 334987 Color: 5800
Size: 270655 Color: 2175

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 361080 Color: 6849
Size: 334889 Color: 5797
Size: 304032 Color: 4321

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 349625 Color: 6418
Size: 345356 Color: 6231
Size: 305020 Color: 4376

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 367146 Color: 7077
Size: 349664 Color: 6419
Size: 283191 Color: 3052

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 367159 Color: 7078
Size: 359156 Color: 6774
Size: 273686 Color: 2403

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 345666 Color: 6247
Size: 328328 Color: 5497
Size: 326007 Color: 5385

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 366810 Color: 7065
Size: 343901 Color: 6169
Size: 289290 Color: 3429

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 361637 Color: 6873
Size: 338356 Color: 5939
Size: 300008 Color: 4088

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 380826 Color: 7549
Size: 346375 Color: 6288
Size: 272800 Color: 2340

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 371787 Color: 7248
Size: 360742 Color: 6835
Size: 267472 Color: 1915

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 357235 Color: 6696
Size: 338123 Color: 5930
Size: 304643 Color: 4352

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 363109 Color: 6924
Size: 341411 Color: 6061
Size: 295481 Color: 3824

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 407846 Color: 8307
Size: 332596 Color: 5694
Size: 259559 Color: 1198

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 365031 Color: 6984
Size: 332156 Color: 5670
Size: 302814 Color: 4261

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 356114 Color: 6651
Size: 350632 Color: 6452
Size: 293255 Color: 3678

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 364757 Color: 6976
Size: 332666 Color: 5698
Size: 302578 Color: 4245

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 356090 Color: 6650
Size: 350848 Color: 6461
Size: 293063 Color: 3665

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 337267 Color: 5893
Size: 333018 Color: 5716
Size: 329716 Color: 5551

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 374914 Color: 7365
Size: 364615 Color: 6970
Size: 260472 Color: 1291

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 359995 Color: 6806
Size: 337910 Color: 5921
Size: 302096 Color: 4209

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 364876 Color: 6979
Size: 333215 Color: 5724
Size: 301910 Color: 4196

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 354487 Color: 6594
Size: 346057 Color: 6275
Size: 299457 Color: 4058

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 364366 Color: 6963
Size: 345330 Color: 6230
Size: 290305 Color: 3493

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 363940 Color: 6954
Size: 334435 Color: 5780
Size: 301626 Color: 4180

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 365178 Color: 6997
Size: 345152 Color: 6225
Size: 289671 Color: 3446

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 371788 Color: 7249
Size: 361824 Color: 6879
Size: 266389 Color: 1833

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 363651 Color: 6947
Size: 335069 Color: 5803
Size: 301281 Color: 4156

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 370782 Color: 7216
Size: 354357 Color: 6588
Size: 274862 Color: 2496

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 367693 Color: 7103
Size: 331203 Color: 5618
Size: 301105 Color: 4149

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 362204 Color: 6893
Size: 344840 Color: 6213
Size: 292957 Color: 3661

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 370390 Color: 7204
Size: 363622 Color: 6944
Size: 265989 Color: 1787

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 374116 Color: 7339
Size: 363361 Color: 6934
Size: 262524 Color: 1475

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 365131 Color: 6994
Size: 359058 Color: 6765
Size: 275812 Color: 2570

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 372101 Color: 7268
Size: 368622 Color: 7144
Size: 259278 Color: 1176

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 403877 Color: 8215
Size: 318840 Color: 5042
Size: 277284 Color: 2669

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 363599 Color: 6943
Size: 346295 Color: 6285
Size: 290107 Color: 3482

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 393732 Color: 7941
Size: 328895 Color: 5518
Size: 277374 Color: 2676

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 372114 Color: 7270
Size: 363302 Color: 6930
Size: 264585 Color: 1669

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 372091 Color: 7267
Size: 338841 Color: 5955
Size: 289069 Color: 3415

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 388297 Color: 7774
Size: 347138 Color: 6324
Size: 264566 Color: 1666

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 363123 Color: 6925
Size: 346004 Color: 6270
Size: 290874 Color: 3521

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 389049 Color: 7788
Size: 342676 Color: 6123
Size: 268276 Color: 1975

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 362688 Color: 6907
Size: 362631 Color: 6905
Size: 274682 Color: 2486

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 369997 Color: 7185
Size: 333304 Color: 5727
Size: 296700 Color: 3904

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 366777 Color: 7062
Size: 361020 Color: 6846
Size: 272204 Color: 2298

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 362992 Color: 6922
Size: 347093 Color: 6322
Size: 289916 Color: 3468

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 367750 Color: 7106
Size: 345424 Color: 6235
Size: 286827 Color: 3286

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 362853 Color: 6916
Size: 345593 Color: 6245
Size: 291555 Color: 3562

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 371969 Color: 7259
Size: 324512 Color: 5319
Size: 303520 Color: 4292

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 372038 Color: 7264
Size: 341538 Color: 6063
Size: 286425 Color: 3250

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 365115 Color: 6992
Size: 360511 Color: 6826
Size: 274375 Color: 2464

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 366936 Color: 7070
Size: 338931 Color: 5960
Size: 294134 Color: 3741

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 362869 Color: 6917
Size: 354817 Color: 6608
Size: 282315 Color: 2993

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 366034 Color: 7025
Size: 359479 Color: 6786
Size: 274488 Color: 2471

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 408238 Color: 8315
Size: 315874 Color: 4912
Size: 275889 Color: 2573

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 407870 Color: 8309
Size: 299073 Color: 4041
Size: 293058 Color: 3664

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 398354 Color: 8068
Size: 347264 Color: 6326
Size: 254383 Color: 631

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 367550 Color: 7096
Size: 363126 Color: 6926
Size: 269325 Color: 2053

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 362350 Color: 6895
Size: 345235 Color: 6228
Size: 292416 Color: 3631

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 362798 Color: 6913
Size: 362558 Color: 6901
Size: 274645 Color: 2484

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 362542 Color: 6899
Size: 358747 Color: 6757
Size: 278712 Color: 2758

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 383453 Color: 7639
Size: 362012 Color: 6887
Size: 254536 Color: 646

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 360166 Color: 6811
Size: 351622 Color: 6490
Size: 288213 Color: 3369

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 362959 Color: 6920
Size: 344350 Color: 6189
Size: 292692 Color: 3646

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 361669 Color: 6875
Size: 353385 Color: 6553
Size: 284947 Color: 3156

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 361901 Color: 6884
Size: 361852 Color: 6882
Size: 276248 Color: 2593

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 361900 Color: 6883
Size: 330424 Color: 5577
Size: 307677 Color: 4499

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 360253 Color: 6815
Size: 357193 Color: 6695
Size: 282555 Color: 3011

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 341144 Color: 6054
Size: 330551 Color: 5584
Size: 328306 Color: 5496

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 402296 Color: 8183
Size: 337593 Color: 5906
Size: 260112 Color: 1249

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 361910 Color: 6885
Size: 344685 Color: 6205
Size: 293406 Color: 3692

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 364199 Color: 6961
Size: 342431 Color: 6111
Size: 293371 Color: 3689

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 373672 Color: 7324
Size: 364906 Color: 6980
Size: 261423 Color: 1383

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 380794 Color: 7548
Size: 356998 Color: 6686
Size: 262209 Color: 1446

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 353292 Color: 6549
Size: 341995 Color: 6085
Size: 304714 Color: 4361

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 361137 Color: 6852
Size: 344843 Color: 6215
Size: 294021 Color: 3734

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 367713 Color: 7104
Size: 362800 Color: 6914
Size: 269488 Color: 2069

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 365430 Color: 7006
Size: 340541 Color: 6029
Size: 294030 Color: 3735

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 364598 Color: 6969
Size: 357283 Color: 6698
Size: 278120 Color: 2718

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 392002 Color: 7892
Size: 312865 Color: 4750
Size: 295134 Color: 3804

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 361469 Color: 6870
Size: 344660 Color: 6204
Size: 293872 Color: 3724

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 377494 Color: 7460
Size: 345794 Color: 6254
Size: 276713 Color: 2621

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 378104 Color: 7468
Size: 329793 Color: 5556
Size: 292104 Color: 3603

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 361297 Color: 6862
Size: 354619 Color: 6600
Size: 284085 Color: 3106

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 365462 Color: 7007
Size: 359109 Color: 6769
Size: 275430 Color: 2539

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 388487 Color: 7778
Size: 340659 Color: 6037
Size: 270855 Color: 2190

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 388297 Color: 7775
Size: 332684 Color: 5700
Size: 279020 Color: 2774

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 382004 Color: 7583
Size: 342279 Color: 6100
Size: 275718 Color: 2563

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 360841 Color: 6842
Size: 344401 Color: 6194
Size: 294759 Color: 3775

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 377391 Color: 7457
Size: 327615 Color: 5456
Size: 294995 Color: 3795

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 369414 Color: 7169
Size: 362691 Color: 6908
Size: 267896 Color: 1947

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 356430 Color: 6665
Size: 346771 Color: 6308
Size: 296800 Color: 3910

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 365110 Color: 6991
Size: 361126 Color: 6850
Size: 273765 Color: 2409

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 378642 Color: 7485
Size: 334257 Color: 5773
Size: 287102 Color: 3304

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 368494 Color: 7138
Size: 367758 Color: 7107
Size: 263749 Color: 1592

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 366263 Color: 7036
Size: 358340 Color: 6741
Size: 275398 Color: 2537

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 360607 Color: 6829
Size: 360576 Color: 6828
Size: 278818 Color: 2763

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 366265 Color: 7037
Size: 366051 Color: 7027
Size: 267685 Color: 1932

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 366117 Color: 7028
Size: 364756 Color: 6975
Size: 269128 Color: 2040

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 406778 Color: 8290
Size: 330441 Color: 5578
Size: 262782 Color: 1500

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 366207 Color: 7033
Size: 360376 Color: 6821
Size: 273418 Color: 2380

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 366293 Color: 7039
Size: 365141 Color: 6995
Size: 268567 Color: 2002

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 361383 Color: 6866
Size: 327981 Color: 5476
Size: 310637 Color: 4659

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 368495 Color: 7139
Size: 360276 Color: 6817
Size: 271230 Color: 2228

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 383126 Color: 7629
Size: 350294 Color: 6442
Size: 266581 Color: 1850

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 364575 Color: 6967
Size: 335086 Color: 5805
Size: 300340 Color: 4110

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 371629 Color: 7245
Size: 362600 Color: 6903
Size: 265772 Color: 1769

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 390309 Color: 7835
Size: 359649 Color: 6797
Size: 250043 Color: 11

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 381734 Color: 7576
Size: 357410 Color: 6702
Size: 260857 Color: 1330

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 396764 Color: 8022
Size: 302224 Color: 4223
Size: 301013 Color: 4144

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 360351 Color: 6820
Size: 355414 Color: 6627
Size: 284236 Color: 3115

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 379094 Color: 7502
Size: 364406 Color: 6964
Size: 256501 Color: 863

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 354084 Color: 6579
Size: 352796 Color: 6534
Size: 293121 Color: 3669

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 398117 Color: 8062
Size: 348026 Color: 6358
Size: 253858 Color: 582

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 359247 Color: 6780
Size: 359197 Color: 6778
Size: 281557 Color: 2953

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 370820 Color: 7217
Size: 354806 Color: 6607
Size: 274375 Color: 2465

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 359615 Color: 6795
Size: 359136 Color: 6772
Size: 281250 Color: 2931

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 361382 Color: 6864
Size: 359797 Color: 6801
Size: 278822 Color: 2764

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 350265 Color: 6441
Size: 335125 Color: 5812
Size: 314611 Color: 4844

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 377022 Color: 7445
Size: 331054 Color: 5610
Size: 291925 Color: 3589

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 392421 Color: 7905
Size: 356520 Color: 6670
Size: 251060 Color: 199

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 389868 Color: 7815
Size: 332107 Color: 5666
Size: 278026 Color: 2711

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 401988 Color: 8171
Size: 331464 Color: 5629
Size: 266549 Color: 1847

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 391135 Color: 7862
Size: 358680 Color: 6753
Size: 250186 Color: 41

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 390100 Color: 7826
Size: 357990 Color: 6728
Size: 251911 Color: 318

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 357581 Color: 6710
Size: 357133 Color: 6692
Size: 285287 Color: 3176

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 392567 Color: 7910
Size: 355732 Color: 6637
Size: 251702 Color: 294

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 359131 Color: 6771
Size: 358359 Color: 6742
Size: 282511 Color: 3008

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 389891 Color: 7817
Size: 352603 Color: 6530
Size: 257507 Color: 958

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 358236 Color: 6737
Size: 358206 Color: 6736
Size: 283559 Color: 3072

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 358422 Color: 6743
Size: 357730 Color: 6715
Size: 283849 Color: 3089

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 363633 Color: 6946
Size: 363328 Color: 6931
Size: 273040 Color: 2357

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 363868 Color: 6953
Size: 363852 Color: 6952
Size: 272281 Color: 2305

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 367579 Color: 7100
Size: 351569 Color: 6486
Size: 280853 Color: 2895

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 357943 Color: 6726
Size: 357941 Color: 6724
Size: 284117 Color: 3112

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 406207 Color: 8274
Size: 334426 Color: 5779
Size: 259368 Color: 1183

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 394306 Color: 7954
Size: 349116 Color: 6397
Size: 256579 Color: 870

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 376858 Color: 7434
Size: 350257 Color: 6440
Size: 272886 Color: 2348

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 362429 Color: 6896
Size: 360697 Color: 6833
Size: 276875 Color: 2633

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 390590 Color: 7845
Size: 357438 Color: 6704
Size: 251973 Color: 330

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 379780 Color: 7525
Size: 348458 Color: 6381
Size: 271763 Color: 2272

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 365108 Color: 6990
Size: 354456 Color: 6592
Size: 280437 Color: 2868

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 405321 Color: 8258
Size: 300631 Color: 4126
Size: 294049 Color: 3737

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 388727 Color: 7781
Size: 357417 Color: 6703
Size: 253857 Color: 581

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 356524 Color: 6671
Size: 346967 Color: 6317
Size: 296510 Color: 3893

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 392338 Color: 7900
Size: 344746 Color: 6208
Size: 262917 Color: 1516

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 390948 Color: 7854
Size: 357913 Color: 6722
Size: 251140 Color: 215

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 350843 Color: 6460
Size: 342995 Color: 6135
Size: 306163 Color: 4420

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 268184 Color: 1969
Size: 343562 Color: 6151
Size: 388255 Color: 7773

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 369599 Color: 7174
Size: 353016 Color: 6540
Size: 277386 Color: 2677

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 380107 Color: 7538
Size: 355523 Color: 6631
Size: 264371 Color: 1650

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 356666 Color: 6678
Size: 349969 Color: 6428
Size: 293366 Color: 3688

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 383501 Color: 7640
Size: 345966 Color: 6267
Size: 270534 Color: 2159

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 363768 Color: 6951
Size: 356575 Color: 6674
Size: 279658 Color: 2812

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 360720 Color: 6834
Size: 353526 Color: 6559
Size: 285755 Color: 3204

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 374957 Color: 7367
Size: 354092 Color: 6580
Size: 270952 Color: 2199

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 362567 Color: 6902
Size: 351757 Color: 6493
Size: 285677 Color: 3198

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 356307 Color: 6661
Size: 356300 Color: 6660
Size: 287394 Color: 3325

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 357521 Color: 6707
Size: 356173 Color: 6654
Size: 286307 Color: 3238

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 368471 Color: 7137
Size: 353509 Color: 6558
Size: 278021 Color: 2709

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 405639 Color: 8265
Size: 335142 Color: 5816
Size: 259220 Color: 1172

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 355960 Color: 6647
Size: 355887 Color: 6644
Size: 288154 Color: 3365

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 396318 Color: 8005
Size: 316944 Color: 4965
Size: 286739 Color: 3275

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 357470 Color: 6705
Size: 353726 Color: 6568
Size: 288805 Color: 3404

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 354254 Color: 6584
Size: 336345 Color: 5860
Size: 309402 Color: 4594

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 365935 Color: 7024
Size: 361282 Color: 6861
Size: 272784 Color: 2337

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 368457 Color: 7136
Size: 356337 Color: 6663
Size: 275207 Color: 2518

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 379120 Color: 7503
Size: 332859 Color: 5710
Size: 288022 Color: 3358

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 353321 Color: 6550
Size: 353121 Color: 6544
Size: 293559 Color: 3709

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 357847 Color: 6719
Size: 355881 Color: 6643
Size: 286273 Color: 3236

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 379248 Color: 7505
Size: 354671 Color: 6604
Size: 266082 Color: 1801

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 336821 Color: 5873
Size: 332142 Color: 5669
Size: 331038 Color: 5607

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 355350 Color: 6623
Size: 336310 Color: 5857
Size: 308341 Color: 4535

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 362611 Color: 6904
Size: 346412 Color: 6291
Size: 290978 Color: 3528

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 367758 Color: 7108
Size: 354670 Color: 6603
Size: 277573 Color: 2687

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 357831 Color: 6717
Size: 351634 Color: 6491
Size: 290536 Color: 3507

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 354669 Color: 6602
Size: 348455 Color: 6380
Size: 296877 Color: 3917

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 361393 Color: 6867
Size: 347932 Color: 6353
Size: 290676 Color: 3512

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 354645 Color: 6601
Size: 354618 Color: 6599
Size: 290738 Color: 3514

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 391595 Color: 7880
Size: 355125 Color: 6616
Size: 253281 Color: 507

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 354574 Color: 6596
Size: 352283 Color: 6519
Size: 293144 Color: 3670

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 362555 Color: 6900
Size: 348415 Color: 6376
Size: 289031 Color: 3414

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 392330 Color: 7899
Size: 355007 Color: 6612
Size: 252664 Color: 415

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 359103 Color: 6768
Size: 345449 Color: 6238
Size: 295449 Color: 3820

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 354278 Color: 6585
Size: 354151 Color: 6583
Size: 291572 Color: 3563

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 366468 Color: 7045
Size: 365028 Color: 6983
Size: 268505 Color: 1994

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 348410 Color: 6375
Size: 345750 Color: 6250
Size: 305841 Color: 4410

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 353798 Color: 6573
Size: 353789 Color: 6572
Size: 292414 Color: 3630

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 353774 Color: 6571
Size: 353650 Color: 6564
Size: 292577 Color: 3639

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 368843 Color: 7149
Size: 350841 Color: 6459
Size: 280317 Color: 2860

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 370504 Color: 7208
Size: 362986 Color: 6921
Size: 266511 Color: 1840

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 343949 Color: 6171
Size: 329750 Color: 5554
Size: 326302 Color: 5395

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 375317 Color: 7379
Size: 332426 Color: 5684
Size: 292258 Color: 3612

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 352252 Color: 6517
Size: 352210 Color: 6513
Size: 295539 Color: 3830

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 366279 Color: 7038
Size: 331679 Color: 5647
Size: 302043 Color: 4206

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 352149 Color: 6509
Size: 352110 Color: 6508
Size: 295742 Color: 3838

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 361824 Color: 6880
Size: 361149 Color: 6853
Size: 277028 Color: 2648

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 373935 Color: 7333
Size: 331919 Color: 5654
Size: 294147 Color: 3742

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 359448 Color: 6783
Size: 347792 Color: 6346
Size: 292761 Color: 3648

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 351920 Color: 6501
Size: 351903 Color: 6499
Size: 296178 Color: 3877

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 396630 Color: 8017
Size: 352567 Color: 6528
Size: 250804 Color: 153

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 351704 Color: 6492
Size: 343702 Color: 6158
Size: 304595 Color: 4349

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 375692 Color: 7393
Size: 350873 Color: 6464
Size: 273436 Color: 2381

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 370940 Color: 7222
Size: 345971 Color: 6268
Size: 283090 Color: 3046

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 357962 Color: 6727
Size: 351551 Color: 6484
Size: 290488 Color: 3503

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 355411 Color: 6626
Size: 347546 Color: 6338
Size: 297044 Color: 3923

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 337016 Color: 5880
Size: 335300 Color: 5827
Size: 327685 Color: 5461

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 406649 Color: 8285
Size: 306748 Color: 4457
Size: 286604 Color: 3263

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 351099 Color: 6472
Size: 351011 Color: 6469
Size: 297891 Color: 3969

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 349852 Color: 6423
Size: 348293 Color: 6370
Size: 301856 Color: 4192

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 364930 Color: 6981
Size: 336836 Color: 5874
Size: 298235 Color: 3987

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 375916 Color: 7402
Size: 350686 Color: 6454
Size: 273399 Color: 2379

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 360834 Color: 6841
Size: 350876 Color: 6465
Size: 288291 Color: 3375

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 368227 Color: 7129
Size: 351210 Color: 6478
Size: 280564 Color: 2881

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 356276 Color: 6657
Size: 350697 Color: 6455
Size: 293028 Color: 3663

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 401314 Color: 8152
Size: 347427 Color: 6331
Size: 251260 Color: 233

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 371537 Color: 7242
Size: 332632 Color: 5696
Size: 295832 Color: 3846

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 400998 Color: 8142
Size: 342296 Color: 6101
Size: 256707 Color: 890

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 361135 Color: 6851
Size: 347958 Color: 6355
Size: 290908 Color: 3525

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 401784 Color: 8163
Size: 311363 Color: 4691
Size: 286854 Color: 3292

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 367843 Color: 7109
Size: 348693 Color: 6389
Size: 283465 Color: 3066

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 353648 Color: 6563
Size: 348233 Color: 6368
Size: 298120 Color: 3980

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 357298 Color: 6699
Size: 341998 Color: 6086
Size: 300705 Color: 4131

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 369899 Color: 7184
Size: 352979 Color: 6539
Size: 277123 Color: 2655

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 390408 Color: 7836
Size: 355832 Color: 6641
Size: 253761 Color: 561

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 355012 Color: 6613
Size: 351584 Color: 6487
Size: 293405 Color: 3691

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 354341 Color: 6586
Size: 330485 Color: 5581
Size: 315175 Color: 4879

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 355460 Color: 6628
Size: 347672 Color: 6342
Size: 296869 Color: 3916

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 363346 Color: 6932
Size: 361201 Color: 6857
Size: 275454 Color: 2540

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 366898 Color: 7067
Size: 340611 Color: 6036
Size: 292492 Color: 3634

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 353734 Color: 6569
Size: 349563 Color: 6414
Size: 296704 Color: 3905

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 389237 Color: 7798
Size: 339794 Color: 5995
Size: 270970 Color: 2202

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 369677 Color: 7179
Size: 348020 Color: 6357
Size: 282304 Color: 2992

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 353441 Color: 6556
Size: 331043 Color: 5609
Size: 315517 Color: 4897

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 356499 Color: 6669
Size: 347577 Color: 6339
Size: 295925 Color: 3852

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 362778 Color: 6912
Size: 328877 Color: 5516
Size: 308346 Color: 4537

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 356310 Color: 6662
Size: 347928 Color: 6352
Size: 295763 Color: 3840

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 353022 Color: 6541
Size: 351225 Color: 6479
Size: 295754 Color: 3839

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 349431 Color: 6407
Size: 335890 Color: 5840
Size: 314680 Color: 4849

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 358452 Color: 6745
Size: 354349 Color: 6587
Size: 287200 Color: 3308

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 348059 Color: 6359
Size: 331570 Color: 5639
Size: 320372 Color: 5116

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 389051 Color: 7789
Size: 332037 Color: 5662
Size: 278913 Color: 2768

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 388751 Color: 7782
Size: 331488 Color: 5632
Size: 279762 Color: 2820

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 353765 Color: 6570
Size: 352582 Color: 6529
Size: 293654 Color: 3714

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 358250 Color: 6738
Size: 348116 Color: 6363
Size: 293635 Color: 3713

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 340598 Color: 6033
Size: 340195 Color: 6013
Size: 319208 Color: 5058

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 368845 Color: 7150
Size: 332706 Color: 5703
Size: 298450 Color: 4001

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 382646 Color: 7606
Size: 323130 Color: 5254
Size: 294225 Color: 3749

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 376311 Color: 7423
Size: 324287 Color: 5305
Size: 299403 Color: 4056

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 365696 Color: 7012
Size: 331539 Color: 5636
Size: 302766 Color: 4258

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 350199 Color: 6437
Size: 325493 Color: 5361
Size: 324309 Color: 5308

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 353135 Color: 6545
Size: 350213 Color: 6438
Size: 296653 Color: 3903

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 350957 Color: 6467
Size: 350059 Color: 6430
Size: 298985 Color: 4032

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 349897 Color: 6425
Size: 349753 Color: 6420
Size: 300351 Color: 4112

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 365573 Color: 7010
Size: 365041 Color: 6986
Size: 269387 Color: 2063

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 349783 Color: 6422
Size: 349515 Color: 6411
Size: 300703 Color: 4130

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 367564 Color: 7098
Size: 340414 Color: 6023
Size: 292023 Color: 3598

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 366165 Color: 7031
Size: 332786 Color: 5707
Size: 301050 Color: 4146

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 393209 Color: 7926
Size: 343653 Color: 6155
Size: 263139 Color: 1543

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 354145 Color: 6582
Size: 344843 Color: 6214
Size: 301013 Color: 4143

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 390554 Color: 7843
Size: 350318 Color: 6445
Size: 259129 Color: 1160

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 368000 Color: 7119
Size: 349457 Color: 6408
Size: 282544 Color: 3010

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 357501 Color: 6706
Size: 349245 Color: 6402
Size: 293255 Color: 3679

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 389471 Color: 7806
Size: 335076 Color: 5804
Size: 275454 Color: 2542

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 357863 Color: 6721
Size: 346177 Color: 6279
Size: 295961 Color: 3855

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 350062 Color: 6431
Size: 347233 Color: 6325
Size: 302706 Color: 4253

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 348974 Color: 6394
Size: 348799 Color: 6392
Size: 302228 Color: 4225

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 358630 Color: 6751
Size: 348787 Color: 6391
Size: 292584 Color: 3640

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 365624 Color: 7011
Size: 332441 Color: 5688
Size: 301936 Color: 4200

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 382621 Color: 7605
Size: 334358 Color: 5777
Size: 283022 Color: 3042

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 404107 Color: 8219
Size: 299917 Color: 4083
Size: 295977 Color: 3857

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 365728 Color: 7014
Size: 348157 Color: 6366
Size: 286116 Color: 3222

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 365780 Color: 7016
Size: 348123 Color: 6364
Size: 286098 Color: 3221

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 364225 Color: 6962
Size: 346980 Color: 6318
Size: 288796 Color: 3403

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 364477 Color: 6966
Size: 346866 Color: 6313
Size: 288658 Color: 3394

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 365927 Color: 7023
Size: 346406 Color: 6290
Size: 287668 Color: 3340

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 366136 Color: 7029
Size: 346322 Color: 6287
Size: 287543 Color: 3334

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 366460 Color: 7044
Size: 346182 Color: 6280
Size: 287359 Color: 3322

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 367357 Color: 7087
Size: 319183 Color: 5055
Size: 313461 Color: 4775

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 367359 Color: 7088
Size: 320850 Color: 5141
Size: 311792 Color: 4708

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 367429 Color: 7093
Size: 316496 Color: 4947
Size: 316076 Color: 4928

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 367852 Color: 7112
Size: 317914 Color: 5011
Size: 314235 Color: 4822

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 368039 Color: 7123
Size: 317324 Color: 4982
Size: 314638 Color: 4846

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 368048 Color: 7124
Size: 319681 Color: 5083
Size: 312272 Color: 4725

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 368197 Color: 7128
Size: 321872 Color: 5197
Size: 309932 Color: 4628

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 368333 Color: 7131
Size: 322827 Color: 5239
Size: 308841 Color: 4568

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 368379 Color: 7132
Size: 318063 Color: 5020
Size: 313559 Color: 4779

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 368392 Color: 7133
Size: 317923 Color: 5012
Size: 313686 Color: 4786

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 368664 Color: 7145
Size: 345833 Color: 6259
Size: 285504 Color: 3194

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 369039 Color: 7156
Size: 315640 Color: 4901
Size: 315322 Color: 4888

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 369114 Color: 7160
Size: 316784 Color: 4957
Size: 314103 Color: 4817

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 369120 Color: 7161
Size: 319882 Color: 5097
Size: 310999 Color: 4674

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 369154 Color: 7163
Size: 321867 Color: 5196
Size: 308980 Color: 4575

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 369166 Color: 7164
Size: 316036 Color: 4923
Size: 314799 Color: 4859

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 369214 Color: 7165
Size: 323653 Color: 5275
Size: 307134 Color: 4477

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 369221 Color: 7166
Size: 319579 Color: 5076
Size: 311201 Color: 4683

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 369353 Color: 7167
Size: 317570 Color: 4992
Size: 313078 Color: 4763

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 369748 Color: 7181
Size: 345776 Color: 6252
Size: 284477 Color: 3131

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 370003 Color: 7186
Size: 322405 Color: 5212
Size: 307593 Color: 4496

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 370180 Color: 7191
Size: 318533 Color: 5034
Size: 311288 Color: 4688

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 370184 Color: 7192
Size: 323936 Color: 5290
Size: 305881 Color: 4413

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 370207 Color: 7193
Size: 317283 Color: 4980
Size: 312511 Color: 4736

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 370210 Color: 7194
Size: 317418 Color: 4985
Size: 312373 Color: 4733

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 370213 Color: 7195
Size: 320811 Color: 5140
Size: 308977 Color: 4574

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 370218 Color: 7196
Size: 317623 Color: 4995
Size: 312160 Color: 4722

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 370318 Color: 7201
Size: 321166 Color: 5155
Size: 308517 Color: 4548

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 370352 Color: 7202
Size: 317946 Color: 5013
Size: 311703 Color: 4703

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 370361 Color: 7203
Size: 320256 Color: 5107
Size: 309384 Color: 4590

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 370461 Color: 7207
Size: 315196 Color: 4880
Size: 314344 Color: 4828

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 370573 Color: 7209
Size: 320313 Color: 5113
Size: 309115 Color: 4580

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 370668 Color: 7212
Size: 316053 Color: 4926
Size: 313280 Color: 4770

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 370701 Color: 7213
Size: 324947 Color: 5337
Size: 304353 Color: 4337

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 370708 Color: 7214
Size: 315282 Color: 4885
Size: 314011 Color: 4813

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 370748 Color: 7215
Size: 314812 Color: 4860
Size: 314441 Color: 4833

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 370853 Color: 7218
Size: 320884 Color: 5144
Size: 308264 Color: 4530

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 370898 Color: 7219
Size: 322502 Color: 5222
Size: 306601 Color: 4447

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 370916 Color: 7220
Size: 321331 Color: 5168
Size: 307754 Color: 4505

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 370923 Color: 7221
Size: 323025 Color: 5250
Size: 306053 Color: 4418

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 371000 Color: 7223
Size: 325019 Color: 5341
Size: 303982 Color: 4316

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 371048 Color: 7224
Size: 315253 Color: 4883
Size: 313700 Color: 4788

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 371079 Color: 7225
Size: 323516 Color: 5271
Size: 305406 Color: 4389

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 371079 Color: 7226
Size: 315007 Color: 4874
Size: 313915 Color: 4807

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 371093 Color: 7227
Size: 319611 Color: 5078
Size: 309297 Color: 4584

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 371167 Color: 7231
Size: 319013 Color: 5048
Size: 309821 Color: 4613

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 371369 Color: 7236
Size: 314769 Color: 4857
Size: 313863 Color: 4800

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 371385 Color: 7237
Size: 323169 Color: 5256
Size: 305447 Color: 4390

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 371419 Color: 7238
Size: 320366 Color: 5115
Size: 308216 Color: 4528

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 371455 Color: 7239
Size: 314760 Color: 4856
Size: 313786 Color: 4794

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 371468 Color: 7240
Size: 316232 Color: 4936
Size: 312301 Color: 4727

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 371512 Color: 7241
Size: 323236 Color: 5260
Size: 305253 Color: 4385

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 371540 Color: 7243
Size: 320982 Color: 5148
Size: 307479 Color: 4491

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 371582 Color: 7244
Size: 324094 Color: 5300
Size: 304325 Color: 4335

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 372628 Color: 7280
Size: 325625 Color: 5369
Size: 301748 Color: 4186

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 372635 Color: 7281
Size: 317517 Color: 4989
Size: 309849 Color: 4616

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 372637 Color: 7282
Size: 324292 Color: 5307
Size: 303072 Color: 4274

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 372670 Color: 7283
Size: 324937 Color: 5336
Size: 302394 Color: 4234

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 372676 Color: 7284
Size: 324763 Color: 5328
Size: 302562 Color: 4244

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 372755 Color: 7286
Size: 316398 Color: 4942
Size: 310848 Color: 4671

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 372761 Color: 7287
Size: 314275 Color: 4825
Size: 312965 Color: 4757

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 372762 Color: 7288
Size: 324145 Color: 5302
Size: 303094 Color: 4275

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 372787 Color: 7289
Size: 314381 Color: 4829
Size: 312833 Color: 4747

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 373025 Color: 7297
Size: 323001 Color: 5248
Size: 303975 Color: 4314

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 373158 Color: 7301
Size: 316169 Color: 4933
Size: 310674 Color: 4661

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 373202 Color: 7302
Size: 314553 Color: 4842
Size: 312246 Color: 4724

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 373276 Color: 7304
Size: 317374 Color: 4983
Size: 309351 Color: 4587

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 373278 Color: 7305
Size: 319245 Color: 5059
Size: 307478 Color: 4490

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 373285 Color: 7306
Size: 318577 Color: 5035
Size: 308139 Color: 4526

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 373294 Color: 7307
Size: 326508 Color: 5404
Size: 300199 Color: 4100

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 373295 Color: 7308
Size: 317054 Color: 4971
Size: 309652 Color: 4606

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 373318 Color: 7309
Size: 316674 Color: 4955
Size: 310009 Color: 4631

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 373386 Color: 7311
Size: 321618 Color: 5186
Size: 304997 Color: 4373

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 373395 Color: 7312
Size: 314242 Color: 4823
Size: 312364 Color: 4731

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 373410 Color: 7313
Size: 324452 Color: 5317
Size: 302139 Color: 4215

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 373461 Color: 7315
Size: 326572 Color: 5409
Size: 299968 Color: 4084

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 373533 Color: 7317
Size: 315935 Color: 4919
Size: 310533 Color: 4654

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 373536 Color: 7318
Size: 322009 Color: 5201
Size: 304456 Color: 4343

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 373591 Color: 7319
Size: 319470 Color: 5071
Size: 306940 Color: 4464

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 373593 Color: 7320
Size: 326633 Color: 5411
Size: 299775 Color: 4076

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 373619 Color: 7321
Size: 317951 Color: 5014
Size: 308431 Color: 4543

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 373623 Color: 7322
Size: 324023 Color: 5295
Size: 302355 Color: 4232

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 373643 Color: 7323
Size: 322652 Color: 5232
Size: 303706 Color: 4301

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 373693 Color: 7326
Size: 322908 Color: 5244
Size: 303400 Color: 4289

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 373816 Color: 7329
Size: 321532 Color: 5182
Size: 304653 Color: 4356

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 373822 Color: 7330
Size: 315865 Color: 4911
Size: 310314 Color: 4644

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 374013 Color: 7335
Size: 344844 Color: 6216
Size: 281144 Color: 2922

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 374062 Color: 7336
Size: 322502 Color: 5221
Size: 303437 Color: 4290

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 374308 Color: 7342
Size: 323186 Color: 5257
Size: 302507 Color: 4241

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 374320 Color: 7343
Size: 313928 Color: 4808
Size: 311753 Color: 4705

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 374337 Color: 7344
Size: 321784 Color: 5193
Size: 303880 Color: 4307

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 374507 Color: 7347
Size: 323607 Color: 5272
Size: 301887 Color: 4194

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 374549 Color: 7348
Size: 325604 Color: 5368
Size: 299848 Color: 4080

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 374555 Color: 7349
Size: 323512 Color: 5270
Size: 301934 Color: 4199

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 374556 Color: 7350
Size: 318607 Color: 5037
Size: 306838 Color: 4459

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 374676 Color: 7353
Size: 320667 Color: 5131
Size: 304658 Color: 4357

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 374681 Color: 7354
Size: 321440 Color: 5175
Size: 303880 Color: 4308

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 374728 Color: 7355
Size: 326642 Color: 5412
Size: 298631 Color: 4008

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 374763 Color: 7356
Size: 322198 Color: 5208
Size: 303040 Color: 4273

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 374764 Color: 7357
Size: 321161 Color: 5154
Size: 304076 Color: 4324

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 374783 Color: 7359
Size: 320566 Color: 5126
Size: 304652 Color: 4355

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 374850 Color: 7360
Size: 323629 Color: 5274
Size: 301522 Color: 4170

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 374875 Color: 7361
Size: 344639 Color: 6203
Size: 280487 Color: 2876

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 374893 Color: 7362
Size: 320247 Color: 5106
Size: 304861 Color: 4368

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 374898 Color: 7363
Size: 319445 Color: 5069
Size: 305658 Color: 4400

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 375033 Color: 7369
Size: 322617 Color: 5227
Size: 302351 Color: 4231

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 375101 Color: 7370
Size: 322848 Color: 5242
Size: 302052 Color: 4207

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 375106 Color: 7371
Size: 321289 Color: 5165
Size: 303606 Color: 4298

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 375197 Color: 7374
Size: 323491 Color: 5269
Size: 301313 Color: 4157

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 375226 Color: 7375
Size: 314843 Color: 4865
Size: 309932 Color: 4627

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 375250 Color: 7376
Size: 324958 Color: 5338
Size: 299793 Color: 4078

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 375262 Color: 7377
Size: 317506 Color: 4988
Size: 307233 Color: 4482

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 375286 Color: 7378
Size: 320462 Color: 5122
Size: 304253 Color: 4333

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 375356 Color: 7380
Size: 316068 Color: 4927
Size: 308577 Color: 4551

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 375392 Color: 7381
Size: 321404 Color: 5174
Size: 303205 Color: 4279

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 375430 Color: 7382
Size: 320470 Color: 5123
Size: 304101 Color: 4329

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 375468 Color: 7384
Size: 323761 Color: 5282
Size: 300772 Color: 4135

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 375475 Color: 7385
Size: 321243 Color: 5161
Size: 303283 Color: 4282

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 375526 Color: 7386
Size: 320874 Color: 5143
Size: 303601 Color: 4296

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 375534 Color: 7387
Size: 324803 Color: 5331
Size: 299664 Color: 4071

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 375666 Color: 7391
Size: 316846 Color: 4960
Size: 307489 Color: 4492

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 375689 Color: 7392
Size: 344337 Color: 6188
Size: 279975 Color: 2838

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 375701 Color: 7394
Size: 344355 Color: 6190
Size: 279945 Color: 2834

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 375772 Color: 7395
Size: 318889 Color: 5044
Size: 305340 Color: 4387

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 375804 Color: 7396
Size: 317038 Color: 4970
Size: 307159 Color: 4478

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 375820 Color: 7397
Size: 322477 Color: 5217
Size: 301704 Color: 4183

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 375836 Color: 7399
Size: 321155 Color: 5153
Size: 303010 Color: 4272

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 375895 Color: 7401
Size: 314735 Color: 4853
Size: 309371 Color: 4589

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 376035 Color: 7405
Size: 312885 Color: 4752
Size: 311081 Color: 4679

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 376040 Color: 7406
Size: 312009 Color: 4719
Size: 311952 Color: 4715

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 376047 Color: 7407
Size: 313650 Color: 4784
Size: 310304 Color: 4643

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 376065 Color: 7408
Size: 318047 Color: 5019
Size: 305889 Color: 4414

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 376066 Color: 7409
Size: 320387 Color: 5117
Size: 303548 Color: 4293

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 376069 Color: 7410
Size: 313659 Color: 4785
Size: 310273 Color: 4641

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 376096 Color: 7411
Size: 319512 Color: 5073
Size: 304393 Color: 4339

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 376115 Color: 7412
Size: 314497 Color: 4837
Size: 309389 Color: 4593

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 376116 Color: 7413
Size: 326669 Color: 5413
Size: 297216 Color: 3932

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 376131 Color: 7414
Size: 313348 Color: 4771
Size: 310522 Color: 4652

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 376131 Color: 7415
Size: 313612 Color: 4783
Size: 310258 Color: 4640

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 376136 Color: 7416
Size: 320279 Color: 5111
Size: 303586 Color: 4295

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 376145 Color: 7417
Size: 325477 Color: 5360
Size: 298379 Color: 3996

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 376153 Color: 7418
Size: 316795 Color: 4958
Size: 307053 Color: 4471

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 376179 Color: 7419
Size: 324869 Color: 5332
Size: 298953 Color: 4026

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 376289 Color: 7421
Size: 324729 Color: 5326
Size: 298983 Color: 4031

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 376292 Color: 7422
Size: 312435 Color: 4734
Size: 311274 Color: 4687

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 376884 Color: 7435
Size: 323928 Color: 5289
Size: 299189 Color: 4045

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 376946 Color: 7437
Size: 314461 Color: 4835
Size: 308594 Color: 4555

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 376950 Color: 7438
Size: 326731 Color: 5417
Size: 296320 Color: 3884

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 376954 Color: 7439
Size: 314433 Color: 4832
Size: 308614 Color: 4557

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 376976 Color: 7440
Size: 316569 Color: 4951
Size: 306456 Color: 4436

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 377003 Color: 7442
Size: 322482 Color: 5219
Size: 300516 Color: 4122

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 377016 Color: 7443
Size: 316301 Color: 4938
Size: 306684 Color: 4451

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 377063 Color: 7446
Size: 316465 Color: 4945
Size: 306473 Color: 4438

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 377064 Color: 7447
Size: 321110 Color: 5151
Size: 301827 Color: 4190

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 377237 Color: 7451
Size: 317844 Color: 5006
Size: 304920 Color: 4370

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 377263 Color: 7452
Size: 314916 Color: 4870
Size: 307822 Color: 4510

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 377263 Color: 7453
Size: 316911 Color: 4963
Size: 305827 Color: 4409

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 377371 Color: 7454
Size: 325580 Color: 5367
Size: 297050 Color: 3924

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 377374 Color: 7455
Size: 323349 Color: 5263
Size: 299278 Color: 4053

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 378009 Color: 7466
Size: 313879 Color: 4802
Size: 308113 Color: 4521

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 378147 Color: 7469
Size: 319834 Color: 5093
Size: 302020 Color: 4203

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 378152 Color: 7470
Size: 319630 Color: 5080
Size: 302219 Color: 4222

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 378218 Color: 7471
Size: 325531 Color: 5363
Size: 296252 Color: 3880

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 378301 Color: 7474
Size: 320277 Color: 5110
Size: 301423 Color: 4164

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 378331 Color: 7475
Size: 320629 Color: 5127
Size: 301041 Color: 4145

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 378365 Color: 7476
Size: 313060 Color: 4761
Size: 308576 Color: 4550

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 378425 Color: 7478
Size: 311027 Color: 4676
Size: 310549 Color: 4655

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 378456 Color: 7480
Size: 320898 Color: 5145
Size: 300647 Color: 4128

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 378473 Color: 7481
Size: 326398 Color: 5399
Size: 295130 Color: 3803

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 378789 Color: 7487
Size: 317209 Color: 4976
Size: 304003 Color: 4319

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 378825 Color: 7488
Size: 313050 Color: 4759
Size: 308126 Color: 4524

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 378871 Color: 7489
Size: 320948 Color: 5146
Size: 300182 Color: 4099

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 378889 Color: 7490
Size: 317119 Color: 4972
Size: 303993 Color: 4317

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 378903 Color: 7491
Size: 316050 Color: 4925
Size: 305048 Color: 4379

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 378946 Color: 7493
Size: 343225 Color: 6141
Size: 277830 Color: 2705

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 379012 Color: 7494
Size: 326831 Color: 5420
Size: 294158 Color: 3744

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 379022 Color: 7495
Size: 313770 Color: 4793
Size: 307209 Color: 4479

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 379024 Color: 7496
Size: 315829 Color: 4905
Size: 305148 Color: 4383

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 379052 Color: 7498
Size: 319009 Color: 5047
Size: 301940 Color: 4201

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 379061 Color: 7499
Size: 310532 Color: 4653
Size: 310408 Color: 4647

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 379075 Color: 7500
Size: 325075 Color: 5344
Size: 295851 Color: 3848

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 379381 Color: 7508
Size: 322185 Color: 5207
Size: 298435 Color: 3999

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 379469 Color: 7510
Size: 319625 Color: 5079
Size: 300907 Color: 4139

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 379585 Color: 7514
Size: 317864 Color: 5009
Size: 302552 Color: 4243

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 379645 Color: 7515
Size: 321386 Color: 5171
Size: 298970 Color: 4027

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 379671 Color: 7516
Size: 310497 Color: 4650
Size: 309833 Color: 4614

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 379686 Color: 7517
Size: 317733 Color: 5001
Size: 302582 Color: 4246

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 379702 Color: 7518
Size: 320449 Color: 5120
Size: 299850 Color: 4081

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 379711 Color: 7519
Size: 325469 Color: 5359
Size: 294821 Color: 3782

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 379727 Color: 7520
Size: 324765 Color: 5329
Size: 295509 Color: 3826

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 379733 Color: 7521
Size: 324400 Color: 5312
Size: 295868 Color: 3849

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 379747 Color: 7522
Size: 315477 Color: 4896
Size: 304777 Color: 4363

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 379761 Color: 7523
Size: 325969 Color: 5382
Size: 294271 Color: 3752

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 379770 Color: 7524
Size: 326726 Color: 5416
Size: 293505 Color: 3702

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 380939 Color: 7551
Size: 319790 Color: 5089
Size: 299272 Color: 4052

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 380999 Color: 7552
Size: 310617 Color: 4657
Size: 308385 Color: 4539

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 381336 Color: 7559
Size: 323193 Color: 5258
Size: 295472 Color: 3822

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 381355 Color: 7560
Size: 315831 Color: 4906
Size: 302815 Color: 4262

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 381363 Color: 7561
Size: 326141 Color: 5389
Size: 292497 Color: 3635

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 381393 Color: 7562
Size: 322814 Color: 5238
Size: 295794 Color: 3844

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 381426 Color: 7563
Size: 323972 Color: 5293
Size: 294603 Color: 3768

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 381455 Color: 7564
Size: 323034 Color: 5251
Size: 295512 Color: 3827

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 381546 Color: 7566
Size: 311951 Color: 4714
Size: 306504 Color: 4441

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 381559 Color: 7567
Size: 309850 Color: 4617
Size: 308592 Color: 4554

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 381571 Color: 7568
Size: 316954 Color: 4967
Size: 301476 Color: 4168

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 381583 Color: 7569
Size: 318856 Color: 5043
Size: 299562 Color: 4062

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 381629 Color: 7570
Size: 323834 Color: 5285
Size: 294538 Color: 3765

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 381631 Color: 7571
Size: 312348 Color: 4728
Size: 306022 Color: 4416

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 381680 Color: 7572
Size: 309384 Color: 4591
Size: 308937 Color: 4571

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 381686 Color: 7573
Size: 312056 Color: 4720
Size: 306259 Color: 4424

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 381694 Color: 7574
Size: 318274 Color: 5026
Size: 300033 Color: 4091

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 381720 Color: 7575
Size: 323152 Color: 5255
Size: 295129 Color: 3802

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 381787 Color: 7577
Size: 313565 Color: 4780
Size: 304649 Color: 4354

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 381803 Color: 7578
Size: 323426 Color: 5266
Size: 294772 Color: 3777

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 381811 Color: 7579
Size: 326441 Color: 5400
Size: 291749 Color: 3575

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 382008 Color: 7584
Size: 342082 Color: 6092
Size: 275911 Color: 2574

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 382165 Color: 7587
Size: 309500 Color: 4601
Size: 308336 Color: 4534

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 382232 Color: 7589
Size: 319029 Color: 5049
Size: 298740 Color: 4013

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 382234 Color: 7590
Size: 314519 Color: 4841
Size: 303248 Color: 4280

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 382237 Color: 7591
Size: 326953 Color: 5423
Size: 290811 Color: 3517

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 382361 Color: 7594
Size: 326488 Color: 5402
Size: 291152 Color: 3537

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 382441 Color: 7596
Size: 316339 Color: 4940
Size: 301221 Color: 4153

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 382467 Color: 7597
Size: 316458 Color: 4944
Size: 301076 Color: 4147

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 382508 Color: 7598
Size: 325796 Color: 5373
Size: 291697 Color: 3568

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 382532 Color: 7600
Size: 309128 Color: 4581
Size: 308341 Color: 4536

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 382569 Color: 7602
Size: 326542 Color: 5406
Size: 290890 Color: 3524

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 382610 Color: 7603
Size: 317822 Color: 5004
Size: 299569 Color: 4063

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 382616 Color: 7604
Size: 316727 Color: 4956
Size: 300658 Color: 4129

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 382754 Color: 7610
Size: 319099 Color: 5054
Size: 298148 Color: 3983

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 382762 Color: 7611
Size: 322470 Color: 5215
Size: 294769 Color: 3776

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 382768 Color: 7612
Size: 310318 Color: 4645
Size: 306915 Color: 4463

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 382779 Color: 7613
Size: 325555 Color: 5365
Size: 291667 Color: 3567

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 382917 Color: 7619
Size: 317573 Color: 4993
Size: 299511 Color: 4061

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 382930 Color: 7620
Size: 319698 Color: 5084
Size: 297373 Color: 3942

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 382932 Color: 7621
Size: 319559 Color: 5075
Size: 297510 Color: 3946

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 382936 Color: 7622
Size: 309103 Color: 4578
Size: 307962 Color: 4516

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 383031 Color: 7624
Size: 326683 Color: 5414
Size: 290287 Color: 3492

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 383033 Color: 7625
Size: 323667 Color: 5276
Size: 293301 Color: 3683

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 383078 Color: 7626
Size: 320661 Color: 5130
Size: 296262 Color: 3881

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 383091 Color: 7627
Size: 325972 Color: 5384
Size: 290938 Color: 3527

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 383124 Color: 7628
Size: 311150 Color: 4681
Size: 305727 Color: 4405

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 383186 Color: 7631
Size: 319582 Color: 5077
Size: 297233 Color: 3937

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 383278 Color: 7634
Size: 309768 Color: 4610
Size: 306955 Color: 4465

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 383361 Color: 7636
Size: 320669 Color: 5132
Size: 295971 Color: 3856

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 383376 Color: 7637
Size: 321806 Color: 5194
Size: 294819 Color: 3780

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 383431 Color: 7638
Size: 341580 Color: 6068
Size: 274990 Color: 2508

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 383617 Color: 7641
Size: 312628 Color: 4741
Size: 303756 Color: 4304

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 383650 Color: 7642
Size: 309863 Color: 4620
Size: 306488 Color: 4439

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 383678 Color: 7643
Size: 325816 Color: 5374
Size: 290507 Color: 3504

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 383696 Color: 7644
Size: 318220 Color: 5023
Size: 298085 Color: 3979

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 383706 Color: 7645
Size: 317239 Color: 4978
Size: 299056 Color: 4039

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 383707 Color: 7646
Size: 319074 Color: 5052
Size: 297220 Color: 3933

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 383781 Color: 7649
Size: 324391 Color: 5311
Size: 291829 Color: 3584

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 383907 Color: 7651
Size: 316343 Color: 4941
Size: 299751 Color: 4074

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 383990 Color: 7653
Size: 313991 Color: 4811
Size: 302020 Color: 4204

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 383991 Color: 7654
Size: 318386 Color: 5029
Size: 297624 Color: 3955

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 384004 Color: 7655
Size: 318427 Color: 5031
Size: 297570 Color: 3951

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 384008 Color: 7656
Size: 315037 Color: 4876
Size: 300956 Color: 4140

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 384063 Color: 7657
Size: 326135 Color: 5388
Size: 289803 Color: 3460

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 384099 Color: 7658
Size: 313091 Color: 4764
Size: 302811 Color: 4260

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 384121 Color: 7659
Size: 311999 Color: 4718
Size: 303881 Color: 4309

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 384139 Color: 7660
Size: 318233 Color: 5024
Size: 297629 Color: 3956

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 384164 Color: 7661
Size: 311529 Color: 4695
Size: 304308 Color: 4334

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 384229 Color: 7662
Size: 312766 Color: 4744
Size: 303006 Color: 4271

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 384263 Color: 7663
Size: 308397 Color: 4541
Size: 307341 Color: 4487

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 384287 Color: 7664
Size: 314840 Color: 4864
Size: 300874 Color: 4137

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 384294 Color: 7665
Size: 325868 Color: 5377
Size: 289839 Color: 3463

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 384450 Color: 7666
Size: 316888 Color: 4962
Size: 298663 Color: 4009

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 384460 Color: 7667
Size: 322599 Color: 5224
Size: 292942 Color: 3658

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 384513 Color: 7668
Size: 308949 Color: 4572
Size: 306539 Color: 4443

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 384529 Color: 7669
Size: 325443 Color: 5358
Size: 290029 Color: 3474

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 384595 Color: 7670
Size: 320324 Color: 5114
Size: 295082 Color: 3798

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 384640 Color: 7671
Size: 323751 Color: 5280
Size: 291610 Color: 3564

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 384655 Color: 7672
Size: 308768 Color: 4563
Size: 306578 Color: 4445

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 384671 Color: 7673
Size: 309546 Color: 4603
Size: 305784 Color: 4407

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 384688 Color: 7674
Size: 320648 Color: 5129
Size: 294665 Color: 3769

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 384698 Color: 7675
Size: 316947 Color: 4966
Size: 298356 Color: 3994

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 384708 Color: 7676
Size: 313879 Color: 4801
Size: 301414 Color: 4163

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 384735 Color: 7677
Size: 326803 Color: 5418
Size: 288463 Color: 3383

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 384743 Color: 7678
Size: 324026 Color: 5296
Size: 291232 Color: 3543

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 384771 Color: 7679
Size: 317846 Color: 5008
Size: 297384 Color: 3943

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 384813 Color: 7680
Size: 316812 Color: 4959
Size: 298376 Color: 3995

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 384834 Color: 7681
Size: 324680 Color: 5324
Size: 290487 Color: 3502

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 384867 Color: 7682
Size: 325431 Color: 5357
Size: 289703 Color: 3450

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 384923 Color: 7683
Size: 325274 Color: 5353
Size: 289804 Color: 3461

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 385033 Color: 7684
Size: 316533 Color: 4950
Size: 298435 Color: 4000

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 385044 Color: 7685
Size: 318921 Color: 5046
Size: 296036 Color: 3861

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 385050 Color: 7686
Size: 311272 Color: 4686
Size: 303679 Color: 4300

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 385075 Color: 7687
Size: 316077 Color: 4929
Size: 298849 Color: 4019

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 385076 Color: 7688
Size: 322765 Color: 5237
Size: 292160 Color: 3607

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 385095 Color: 7689
Size: 308831 Color: 4567
Size: 306075 Color: 4419

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 385207 Color: 7690
Size: 316020 Color: 4922
Size: 298774 Color: 4014

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 385271 Color: 7691
Size: 322717 Color: 5234
Size: 292013 Color: 3595

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 385289 Color: 7692
Size: 321848 Color: 5195
Size: 292864 Color: 3653

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 385336 Color: 7693
Size: 311961 Color: 4716
Size: 302704 Color: 4252

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 385344 Color: 7694
Size: 307641 Color: 4497
Size: 307016 Color: 4468

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 385347 Color: 7695
Size: 318620 Color: 5038
Size: 296034 Color: 3860

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 385387 Color: 7696
Size: 322178 Color: 5206
Size: 292436 Color: 3632

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 385387 Color: 7697
Size: 315011 Color: 4875
Size: 299603 Color: 4064

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 385491 Color: 7698
Size: 308117 Color: 4523
Size: 306393 Color: 4433

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 385545 Color: 7699
Size: 310681 Color: 4662
Size: 303775 Color: 4305

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 385562 Color: 7700
Size: 311553 Color: 4697
Size: 302886 Color: 4267

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 385589 Color: 7701
Size: 313055 Color: 4760
Size: 301357 Color: 4158

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 385597 Color: 7702
Size: 313250 Color: 4768
Size: 301154 Color: 4152

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 385613 Color: 7703
Size: 311016 Color: 4675
Size: 303372 Color: 4285

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 385643 Color: 7704
Size: 316490 Color: 4946
Size: 297868 Color: 3968

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 385760 Color: 7705
Size: 320021 Color: 5102
Size: 294220 Color: 3748

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 385766 Color: 7706
Size: 320785 Color: 5139
Size: 293450 Color: 3694

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 385773 Color: 7707
Size: 313738 Color: 4792
Size: 300490 Color: 4121

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 385799 Color: 7708
Size: 307743 Color: 4504
Size: 306459 Color: 4437

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 385810 Color: 7709
Size: 313451 Color: 4774
Size: 300740 Color: 4132

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 385841 Color: 7710
Size: 312592 Color: 4739
Size: 301568 Color: 4173

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 385900 Color: 7711
Size: 320301 Color: 5112
Size: 293800 Color: 3722

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 385901 Color: 7712
Size: 323352 Color: 5264
Size: 290748 Color: 3515

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 385906 Color: 7713
Size: 314902 Color: 4868
Size: 299193 Color: 4046

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 385939 Color: 7714
Size: 314838 Color: 4863
Size: 299224 Color: 4049

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 385943 Color: 7715
Size: 323024 Color: 5249
Size: 291034 Color: 3532

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 385976 Color: 7716
Size: 326552 Color: 5407
Size: 287473 Color: 3330

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 386056 Color: 7717
Size: 308970 Color: 4573
Size: 304975 Color: 4372

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 386086 Color: 7718
Size: 315877 Color: 4913
Size: 298038 Color: 3976

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 386129 Color: 7719
Size: 311424 Color: 4694
Size: 302448 Color: 4239

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 386154 Color: 7720
Size: 310241 Color: 4638
Size: 303606 Color: 4297

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 386174 Color: 7721
Size: 323893 Color: 5288
Size: 289934 Color: 3469

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 386200 Color: 7722
Size: 311051 Color: 4677
Size: 302750 Color: 4256

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 386222 Color: 7723
Size: 324035 Color: 5297
Size: 289744 Color: 3453

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 386280 Color: 7724
Size: 318067 Color: 5021
Size: 295654 Color: 3834

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 386284 Color: 7725
Size: 306981 Color: 4466
Size: 306736 Color: 4454

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 386297 Color: 7726
Size: 323348 Color: 5262
Size: 290356 Color: 3495

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 386305 Color: 7727
Size: 311366 Color: 4692
Size: 302330 Color: 4229

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 386307 Color: 7728
Size: 308104 Color: 4518
Size: 305590 Color: 4395

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 386345 Color: 7729
Size: 322377 Color: 5210
Size: 291279 Color: 3547

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 386357 Color: 7730
Size: 320735 Color: 5137
Size: 292909 Color: 3656

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 386364 Color: 7731
Size: 322129 Color: 5205
Size: 291508 Color: 3559

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 386373 Color: 7732
Size: 314898 Color: 4867
Size: 298730 Color: 4012

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 386380 Color: 7733
Size: 309112 Color: 4579
Size: 304509 Color: 4346

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 386419 Color: 7734
Size: 307807 Color: 4508
Size: 305775 Color: 4406

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 386430 Color: 7735
Size: 326128 Color: 5387
Size: 287443 Color: 3327

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 386486 Color: 7736
Size: 309780 Color: 4611
Size: 303735 Color: 4302

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 386509 Color: 7737
Size: 321393 Color: 5172
Size: 292099 Color: 3600

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 386546 Color: 7738
Size: 310606 Color: 4656
Size: 302849 Color: 4264

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 386559 Color: 7739
Size: 314508 Color: 4839
Size: 298934 Color: 4025

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 386639 Color: 7740
Size: 323247 Color: 5261
Size: 290115 Color: 3484

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 386697 Color: 7741
Size: 310720 Color: 4664
Size: 302584 Color: 4247

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 386721 Color: 7742
Size: 308790 Color: 4565
Size: 304490 Color: 4344

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 386733 Color: 7743
Size: 322398 Color: 5211
Size: 290870 Color: 3520

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 386746 Color: 7744
Size: 314726 Color: 4852
Size: 298529 Color: 4003

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 386751 Color: 7745
Size: 321351 Color: 5169
Size: 291899 Color: 3588

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 386773 Color: 7746
Size: 310474 Color: 4649
Size: 302754 Color: 4257

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 386884 Color: 7747
Size: 324409 Color: 5314
Size: 288708 Color: 3397

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 386890 Color: 7748
Size: 322680 Color: 5233
Size: 290431 Color: 3498

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 386914 Color: 7749
Size: 317879 Color: 5010
Size: 295208 Color: 3808

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 386933 Color: 7750
Size: 307703 Color: 4501
Size: 305365 Color: 4388

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 387084 Color: 7751
Size: 319854 Color: 5094
Size: 293063 Color: 3666

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 387110 Color: 7752
Size: 312881 Color: 4751
Size: 300010 Color: 4089

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 387195 Color: 7753
Size: 324657 Color: 5322
Size: 288149 Color: 3364

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 387245 Color: 7754
Size: 313274 Color: 4769
Size: 299482 Color: 4059

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 387402 Color: 7755
Size: 308506 Color: 4547
Size: 304093 Color: 4328

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 387404 Color: 7756
Size: 313810 Color: 4797
Size: 298787 Color: 4015

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 387549 Color: 7757
Size: 310843 Color: 4670
Size: 301609 Color: 4178

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 387550 Color: 7758
Size: 321520 Color: 5179
Size: 290931 Color: 3526

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 387681 Color: 7759
Size: 323747 Color: 5279
Size: 288573 Color: 3390

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 387744 Color: 7760
Size: 318431 Color: 5032
Size: 293826 Color: 3723

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 387769 Color: 7761
Size: 314486 Color: 4836
Size: 297746 Color: 3960

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 387770 Color: 7762
Size: 315864 Color: 4910
Size: 296367 Color: 3887

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 387784 Color: 7763
Size: 316335 Color: 4939
Size: 295882 Color: 3850

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 387819 Color: 7764
Size: 309332 Color: 4585
Size: 302850 Color: 4265

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 387860 Color: 7765
Size: 322744 Color: 5235
Size: 289397 Color: 3435

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 387908 Color: 7766
Size: 315918 Color: 4916
Size: 296175 Color: 3876

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 387912 Color: 7767
Size: 321721 Color: 5189
Size: 290368 Color: 3496

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 409173 Color: 8335
Size: 305030 Color: 4377
Size: 285798 Color: 3206

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 409179 Color: 8336
Size: 305845 Color: 4411
Size: 284977 Color: 3157

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 409187 Color: 8337
Size: 334210 Color: 5771
Size: 256604 Color: 875

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 409250 Color: 8338
Size: 324699 Color: 5325
Size: 266052 Color: 1797

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 409252 Color: 8339
Size: 323753 Color: 5281
Size: 266996 Color: 1886

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 409278 Color: 8340
Size: 309339 Color: 4586
Size: 281384 Color: 2939

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 409287 Color: 8341
Size: 314672 Color: 4848
Size: 276042 Color: 2584

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 409409 Color: 8342
Size: 329416 Color: 5540
Size: 261176 Color: 1364

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 409453 Color: 8343
Size: 313732 Color: 4791
Size: 276816 Color: 2628

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 409483 Color: 8344
Size: 295770 Color: 3841
Size: 294748 Color: 3774

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 409489 Color: 8345
Size: 296569 Color: 3896
Size: 293943 Color: 3730

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 409523 Color: 8346
Size: 329424 Color: 5541
Size: 261054 Color: 1354

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 409547 Color: 8347
Size: 318014 Color: 5018
Size: 272440 Color: 2313

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 409597 Color: 8348
Size: 327939 Color: 5472
Size: 262465 Color: 1466

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 409651 Color: 8349
Size: 295830 Color: 3845
Size: 294520 Color: 3764

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 409661 Color: 8350
Size: 314211 Color: 4820
Size: 276129 Color: 2590

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 409710 Color: 8351
Size: 337944 Color: 5923
Size: 252347 Color: 374

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 409778 Color: 8352
Size: 321543 Color: 5183
Size: 268680 Color: 2012

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 409821 Color: 8353
Size: 309471 Color: 4599
Size: 280709 Color: 2892

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 409826 Color: 8354
Size: 309731 Color: 4609
Size: 280444 Color: 2869

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 409829 Color: 8355
Size: 333812 Color: 5753
Size: 256360 Color: 847

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 409849 Color: 8356
Size: 327529 Color: 5451
Size: 262623 Color: 1483

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 409867 Color: 8357
Size: 321457 Color: 5177
Size: 268677 Color: 2011

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 409896 Color: 8358
Size: 332429 Color: 5685
Size: 257676 Color: 979

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 409955 Color: 8359
Size: 331229 Color: 5619
Size: 258817 Color: 1111

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 409992 Color: 8360
Size: 332394 Color: 5682
Size: 257615 Color: 972

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 410000 Color: 8361
Size: 333545 Color: 5736
Size: 256456 Color: 854

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 410017 Color: 8362
Size: 331014 Color: 5604
Size: 258970 Color: 1135

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 410028 Color: 8363
Size: 322413 Color: 5214
Size: 267560 Color: 1924

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 410032 Color: 8364
Size: 331781 Color: 5649
Size: 258188 Color: 1033

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 410095 Color: 8365
Size: 322894 Color: 5243
Size: 267012 Color: 1889

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 410100 Color: 8366
Size: 331544 Color: 5637
Size: 258357 Color: 1059

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 410133 Color: 8367
Size: 321699 Color: 5188
Size: 268169 Color: 1967

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 410233 Color: 8368
Size: 331028 Color: 5606
Size: 258740 Color: 1104

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 410249 Color: 8369
Size: 298992 Color: 4034
Size: 290760 Color: 3516

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 410295 Color: 8370
Size: 329995 Color: 5560
Size: 259711 Color: 1217

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 410339 Color: 8371
Size: 326563 Color: 5408
Size: 263099 Color: 1535

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 410341 Color: 8372
Size: 309550 Color: 4604
Size: 280110 Color: 2848

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 410370 Color: 8373
Size: 333115 Color: 5721
Size: 256516 Color: 865

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 410373 Color: 8374
Size: 324752 Color: 5327
Size: 264876 Color: 1688

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 410403 Color: 8375
Size: 315349 Color: 4890
Size: 274249 Color: 2453

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 410407 Color: 8376
Size: 295092 Color: 3799
Size: 294502 Color: 3763

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 410435 Color: 8377
Size: 297851 Color: 3966
Size: 291715 Color: 3572

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 410464 Color: 8378
Size: 295434 Color: 3818
Size: 294103 Color: 3739

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 410483 Color: 8379
Size: 322411 Color: 5213
Size: 267107 Color: 1895

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 410487 Color: 8380
Size: 309036 Color: 4577
Size: 280478 Color: 2875

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 410488 Color: 8381
Size: 296175 Color: 3875
Size: 293338 Color: 3686

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 410503 Color: 8382
Size: 299510 Color: 4060
Size: 289988 Color: 3471

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 410561 Color: 8383
Size: 311400 Color: 4693
Size: 278040 Color: 2712

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 410562 Color: 8384
Size: 337433 Color: 5899
Size: 252006 Color: 335

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 410630 Color: 8385
Size: 304687 Color: 4358
Size: 284684 Color: 3143

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 410666 Color: 8386
Size: 318503 Color: 5033
Size: 270832 Color: 2189

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 410706 Color: 8387
Size: 318415 Color: 5030
Size: 270880 Color: 2194

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 410757 Color: 8388
Size: 323618 Color: 5273
Size: 265626 Color: 1752

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 410815 Color: 8389
Size: 326166 Color: 5391
Size: 263020 Color: 1525

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 410872 Color: 8390
Size: 314787 Color: 4858
Size: 274342 Color: 2460

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 410910 Color: 8391
Size: 296736 Color: 3907
Size: 292355 Color: 3622

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 410930 Color: 8392
Size: 300314 Color: 4109
Size: 288757 Color: 3402

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 410988 Color: 8393
Size: 301746 Color: 4185
Size: 287267 Color: 3313

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 410991 Color: 8394
Size: 323802 Color: 5284
Size: 265208 Color: 1723

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 411016 Color: 8395
Size: 321402 Color: 5173
Size: 267583 Color: 1925

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 411046 Color: 8396
Size: 319752 Color: 5087
Size: 269203 Color: 2049

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 411125 Color: 8397
Size: 324216 Color: 5304
Size: 264660 Color: 1673

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 411138 Color: 8398
Size: 335494 Color: 5831
Size: 253369 Color: 514

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 411286 Color: 8399
Size: 338289 Color: 5936
Size: 250426 Color: 96

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 411316 Color: 8400
Size: 302291 Color: 4228
Size: 286394 Color: 3246

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 411317 Color: 8401
Size: 315900 Color: 4915
Size: 272784 Color: 2338

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 411322 Color: 8402
Size: 335167 Color: 5819
Size: 253512 Color: 529

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 411343 Color: 8403
Size: 308783 Color: 4564
Size: 279875 Color: 2828

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 411545 Color: 8404
Size: 334289 Color: 5774
Size: 254167 Color: 613

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 411615 Color: 8405
Size: 297230 Color: 3936
Size: 291156 Color: 3538

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 411734 Color: 8406
Size: 297597 Color: 3954
Size: 290670 Color: 3511

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 411757 Color: 8407
Size: 320172 Color: 5104
Size: 268072 Color: 1958

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 411769 Color: 8408
Size: 328907 Color: 5520
Size: 259325 Color: 1180

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 411779 Color: 8409
Size: 314276 Color: 4826
Size: 273946 Color: 2430

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 411783 Color: 8410
Size: 311577 Color: 4698
Size: 276641 Color: 2616

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 411820 Color: 8411
Size: 309922 Color: 4626
Size: 278259 Color: 2729

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 411845 Color: 8412
Size: 321930 Color: 5199
Size: 266226 Color: 1819

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 411887 Color: 8413
Size: 314051 Color: 4814
Size: 274063 Color: 2438

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 411964 Color: 8414
Size: 305594 Color: 4397
Size: 282443 Color: 3001

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 411978 Color: 8415
Size: 298123 Color: 3981
Size: 289900 Color: 3467

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 412041 Color: 8416
Size: 331660 Color: 5646
Size: 256300 Color: 840

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 412044 Color: 8417
Size: 304416 Color: 4341
Size: 283541 Color: 3070

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 412140 Color: 8418
Size: 295496 Color: 3825
Size: 292365 Color: 3624

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 412151 Color: 8419
Size: 301618 Color: 4179
Size: 286232 Color: 3230

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 412174 Color: 8420
Size: 317664 Color: 4997
Size: 270163 Color: 2134

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 412192 Color: 8421
Size: 296482 Color: 3892
Size: 291327 Color: 3550

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 412242 Color: 8422
Size: 322082 Color: 5204
Size: 265677 Color: 1761

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 412315 Color: 8423
Size: 300381 Color: 4115
Size: 287305 Color: 3318

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 412321 Color: 8424
Size: 295289 Color: 3813
Size: 292391 Color: 3628

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 412416 Color: 8425
Size: 329053 Color: 5524
Size: 258532 Color: 1083

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 412478 Color: 8426
Size: 317778 Color: 5002
Size: 269745 Color: 2084

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 412537 Color: 8427
Size: 330913 Color: 5597
Size: 256551 Color: 866

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 412550 Color: 8428
Size: 295923 Color: 3851
Size: 291528 Color: 3560

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 412571 Color: 8429
Size: 314506 Color: 4838
Size: 272924 Color: 2350

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 412584 Color: 8430
Size: 311339 Color: 4690
Size: 276078 Color: 2586

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 412632 Color: 8431
Size: 335504 Color: 5832
Size: 251865 Color: 311

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 412720 Color: 8432
Size: 295172 Color: 3805
Size: 292109 Color: 3604

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 412729 Color: 8433
Size: 304049 Color: 4322
Size: 283223 Color: 3055

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 412731 Color: 8434
Size: 330128 Color: 5565
Size: 257142 Color: 923

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 412745 Color: 8435
Size: 311110 Color: 4680
Size: 276146 Color: 2591

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 412766 Color: 8436
Size: 317474 Color: 4987
Size: 269761 Color: 2086

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 412902 Color: 8437
Size: 333285 Color: 5726
Size: 253814 Color: 570

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 412952 Color: 8438
Size: 334632 Color: 5786
Size: 252417 Color: 382

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 412959 Color: 8439
Size: 332614 Color: 5695
Size: 254428 Color: 636

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 412976 Color: 8440
Size: 330895 Color: 5595
Size: 256130 Color: 821

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 413003 Color: 8441
Size: 313216 Color: 4767
Size: 273782 Color: 2411

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 413145 Color: 8442
Size: 297191 Color: 3930
Size: 289665 Color: 3445

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 413146 Color: 8443
Size: 314657 Color: 4847
Size: 272198 Color: 2297

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 413232 Color: 8444
Size: 315854 Color: 4907
Size: 270915 Color: 2196

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 413331 Color: 8445
Size: 331733 Color: 5648
Size: 254937 Color: 696

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 413414 Color: 8446
Size: 294030 Color: 3736
Size: 292557 Color: 3637

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 413441 Color: 8447
Size: 296838 Color: 3912
Size: 289722 Color: 3451

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 413504 Color: 8448
Size: 331160 Color: 5615
Size: 255337 Color: 740

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 413511 Color: 8449
Size: 313977 Color: 4810
Size: 272513 Color: 2317

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 413516 Color: 8450
Size: 312161 Color: 4723
Size: 274324 Color: 2458

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 413560 Color: 8451
Size: 303323 Color: 4283
Size: 283118 Color: 3048

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 413602 Color: 8452
Size: 315862 Color: 4908
Size: 270537 Color: 2160

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 413652 Color: 8453
Size: 309990 Color: 4630
Size: 276359 Color: 2600

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 413660 Color: 8454
Size: 321610 Color: 5185
Size: 264731 Color: 1677

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 413848 Color: 8455
Size: 294813 Color: 3779
Size: 291340 Color: 3553

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 413878 Color: 8456
Size: 331405 Color: 5627
Size: 254718 Color: 669

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 413936 Color: 8457
Size: 328389 Color: 5500
Size: 257676 Color: 980

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 413942 Color: 8458
Size: 319054 Color: 5050
Size: 267005 Color: 1888

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 413956 Color: 8459
Size: 301930 Color: 4198
Size: 284115 Color: 3111

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 413998 Color: 8460
Size: 310770 Color: 4667
Size: 275233 Color: 2520

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 414073 Color: 8461
Size: 332839 Color: 5709
Size: 253089 Color: 476

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 414100 Color: 8462
Size: 296522 Color: 3894
Size: 289379 Color: 3432

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 414135 Color: 8463
Size: 328883 Color: 5517
Size: 256983 Color: 908

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 414142 Color: 8464
Size: 329640 Color: 5548
Size: 256219 Color: 828

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 414227 Color: 8465
Size: 314588 Color: 4843
Size: 271186 Color: 2224

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 414239 Color: 8466
Size: 307272 Color: 4484
Size: 278490 Color: 2744

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 414279 Color: 8467
Size: 300562 Color: 4124
Size: 285160 Color: 3168

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 414325 Color: 8468
Size: 299043 Color: 4037
Size: 286633 Color: 3266

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 414345 Color: 8470
Size: 302335 Color: 4230
Size: 283321 Color: 3057

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 414345 Color: 8469
Size: 323943 Color: 5291
Size: 261713 Color: 1403

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 414374 Color: 8471
Size: 319541 Color: 5074
Size: 266086 Color: 1803

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 414544 Color: 8472
Size: 315441 Color: 4894
Size: 270016 Color: 2115

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 414592 Color: 8473
Size: 294206 Color: 3746
Size: 291203 Color: 3541

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 414604 Color: 8474
Size: 297897 Color: 3970
Size: 287500 Color: 3332

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 414682 Color: 8475
Size: 298184 Color: 3985
Size: 287135 Color: 3306

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 414726 Color: 8476
Size: 322845 Color: 5240
Size: 262430 Color: 1462

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 414785 Color: 8477
Size: 298886 Color: 4021
Size: 286330 Color: 3241

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 414809 Color: 8478
Size: 307946 Color: 4515
Size: 277246 Color: 2667

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 414810 Color: 8479
Size: 333091 Color: 5718
Size: 252100 Color: 345

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 414836 Color: 8480
Size: 314200 Color: 4819
Size: 270965 Color: 2200

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 414986 Color: 8481
Size: 294820 Color: 3781
Size: 290195 Color: 3488

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 415029 Color: 8482
Size: 296573 Color: 3897
Size: 288399 Color: 3380

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 415037 Color: 8483
Size: 310187 Color: 4637
Size: 274777 Color: 2492

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 415093 Color: 8484
Size: 314906 Color: 4869
Size: 270002 Color: 2112

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 415217 Color: 8485
Size: 325957 Color: 5380
Size: 258827 Color: 1112

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 415227 Color: 8486
Size: 302631 Color: 4249
Size: 282143 Color: 2982

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 415228 Color: 8487
Size: 298190 Color: 3986
Size: 286583 Color: 3262

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 415255 Color: 8488
Size: 333486 Color: 5734
Size: 251260 Color: 234

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 415255 Color: 8489
Size: 310179 Color: 4635
Size: 274567 Color: 2475

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 415260 Color: 8490
Size: 322489 Color: 5220
Size: 262252 Color: 1450

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 415264 Color: 8491
Size: 313992 Color: 4812
Size: 270745 Color: 2184

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 415298 Color: 8492
Size: 329720 Color: 5552
Size: 254983 Color: 705

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 415308 Color: 8493
Size: 333707 Color: 5745
Size: 250986 Color: 188

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 415324 Color: 8494
Size: 304084 Color: 4325
Size: 280593 Color: 2882

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 415333 Color: 8495
Size: 306372 Color: 4431
Size: 278296 Color: 2734

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 415351 Color: 8496
Size: 324924 Color: 5335
Size: 259726 Color: 1220

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 415385 Color: 8497
Size: 315451 Color: 4895
Size: 269165 Color: 2044

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 415430 Color: 8498
Size: 331498 Color: 5634
Size: 253073 Color: 472

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 415473 Color: 8499
Size: 311691 Color: 4702
Size: 272837 Color: 2345

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 415551 Color: 8500
Size: 301545 Color: 4172
Size: 282905 Color: 3033

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 415578 Color: 8501
Size: 306591 Color: 4446
Size: 277832 Color: 2707

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 415616 Color: 8502
Size: 303371 Color: 4284
Size: 281014 Color: 2913

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 415637 Color: 8503
Size: 314987 Color: 4873
Size: 269377 Color: 2062

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 415716 Color: 8504
Size: 328195 Color: 5489
Size: 256090 Color: 814

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 415718 Color: 8505
Size: 317706 Color: 5000
Size: 266577 Color: 1849

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 415849 Color: 8506
Size: 294338 Color: 3754
Size: 289814 Color: 3462

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 415857 Color: 8507
Size: 314954 Color: 4871
Size: 269190 Color: 2046

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 415894 Color: 8508
Size: 296421 Color: 3889
Size: 287686 Color: 3341

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 415930 Color: 8509
Size: 292843 Color: 3651
Size: 291228 Color: 3542

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 415979 Color: 8510
Size: 318706 Color: 5039
Size: 265316 Color: 1733

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 415999 Color: 8511
Size: 332078 Color: 5664
Size: 251924 Color: 321

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 416113 Color: 8512
Size: 306312 Color: 4427
Size: 277576 Color: 2688

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 416257 Color: 8513
Size: 296421 Color: 3890
Size: 287323 Color: 3321

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 416283 Color: 8514
Size: 325158 Color: 5348
Size: 258560 Color: 1086

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 416324 Color: 8515
Size: 328150 Color: 5487
Size: 255527 Color: 757

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 416353 Color: 8516
Size: 312966 Color: 4758
Size: 270682 Color: 2179

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 416398 Color: 8517
Size: 302178 Color: 4218
Size: 281425 Color: 2945

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 416457 Color: 8518
Size: 327997 Color: 5477
Size: 255547 Color: 758

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 416469 Color: 8519
Size: 301409 Color: 4162
Size: 282123 Color: 2981

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 416505 Color: 8520
Size: 326468 Color: 5401
Size: 257028 Color: 915

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 416589 Color: 8522
Size: 320750 Color: 5138
Size: 262662 Color: 1489

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 416589 Color: 8521
Size: 300744 Color: 4133
Size: 282668 Color: 3022

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 416618 Color: 8523
Size: 306345 Color: 4430
Size: 277038 Color: 2651

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 416665 Color: 8524
Size: 332431 Color: 5686
Size: 250905 Color: 173

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 416702 Color: 8525
Size: 308728 Color: 4560
Size: 274571 Color: 2477

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 416718 Color: 8526
Size: 305035 Color: 4378
Size: 278248 Color: 2727

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 416721 Color: 8527
Size: 317234 Color: 4977
Size: 266046 Color: 1795

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 416796 Color: 8528
Size: 326118 Color: 5386
Size: 257087 Color: 918

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 416811 Color: 8529
Size: 309836 Color: 4615
Size: 273354 Color: 2375

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 416814 Color: 8530
Size: 317545 Color: 4991
Size: 265642 Color: 1755

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 416819 Color: 8531
Size: 312545 Color: 4738
Size: 270637 Color: 2172

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 416830 Color: 8532
Size: 313829 Color: 4799
Size: 269342 Color: 2056

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 416839 Color: 8533
Size: 291728 Color: 3574
Size: 291434 Color: 3556

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 416930 Color: 8534
Size: 295782 Color: 3842
Size: 287289 Color: 3315

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 416947 Color: 8535
Size: 329926 Color: 5559
Size: 253128 Color: 483

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 416998 Color: 8536
Size: 309804 Color: 4612
Size: 273199 Color: 2370

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 417020 Color: 8537
Size: 301573 Color: 4175
Size: 281408 Color: 2943

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 417021 Color: 8538
Size: 302432 Color: 4236
Size: 280548 Color: 2880

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 417197 Color: 8539
Size: 314515 Color: 4840
Size: 268289 Color: 1976

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 417201 Color: 8540
Size: 332527 Color: 5692
Size: 250273 Color: 65

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 417204 Color: 8541
Size: 301456 Color: 4167
Size: 281341 Color: 2936

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 417342 Color: 8542
Size: 296081 Color: 3863
Size: 286578 Color: 3260

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 417376 Color: 8543
Size: 314829 Color: 4861
Size: 267796 Color: 1939

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 417396 Color: 8544
Size: 291937 Color: 3591
Size: 290668 Color: 3510

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 417457 Color: 8545
Size: 328899 Color: 5519
Size: 253645 Color: 546

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 417482 Color: 8546
Size: 304750 Color: 4362
Size: 277769 Color: 2700

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 417511 Color: 8547
Size: 317822 Color: 5005
Size: 264668 Color: 1674

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 417543 Color: 8548
Size: 329463 Color: 5543
Size: 252995 Color: 457

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 417595 Color: 8549
Size: 323995 Color: 5294
Size: 258411 Color: 1066

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 417622 Color: 8550
Size: 306634 Color: 4449
Size: 275745 Color: 2564

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 417644 Color: 8551
Size: 320452 Color: 5121
Size: 261905 Color: 1415

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 417660 Color: 8552
Size: 321179 Color: 5157
Size: 261162 Color: 1363

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 417820 Color: 8553
Size: 319377 Color: 5067
Size: 262804 Color: 1504

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 417867 Color: 8554
Size: 292236 Color: 3610
Size: 289898 Color: 3466

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 417899 Color: 8555
Size: 294267 Color: 3751
Size: 287835 Color: 3348

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 417909 Color: 8556
Size: 329248 Color: 5535
Size: 252844 Color: 436

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 417972 Color: 8557
Size: 303851 Color: 4306
Size: 278178 Color: 2722

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 417976 Color: 8558
Size: 302444 Color: 4238
Size: 279581 Color: 2809

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 418074 Color: 8559
Size: 309018 Color: 4576
Size: 272909 Color: 2349

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 418101 Color: 8560
Size: 294368 Color: 3756
Size: 287532 Color: 3333

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 418106 Color: 8561
Size: 311897 Color: 4711
Size: 269998 Color: 2111

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 418221 Color: 8562
Size: 320991 Color: 5149
Size: 260789 Color: 1322

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 418283 Color: 8563
Size: 294450 Color: 3759
Size: 287268 Color: 3314

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 418302 Color: 8564
Size: 292305 Color: 3616
Size: 289394 Color: 3434

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 418346 Color: 8565
Size: 313604 Color: 4782
Size: 268051 Color: 1957

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 418485 Color: 8566
Size: 328772 Color: 5513
Size: 252744 Color: 422

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 418496 Color: 8567
Size: 326349 Color: 5397
Size: 255156 Color: 724

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 418504 Color: 8568
Size: 319987 Color: 5100
Size: 261510 Color: 1390

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 418524 Color: 8569
Size: 312890 Color: 4753
Size: 268587 Color: 2004

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 418547 Color: 8570
Size: 312350 Color: 4729
Size: 269104 Color: 2039

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 418593 Color: 8571
Size: 310335 Color: 4646
Size: 271073 Color: 2215

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 418625 Color: 8572
Size: 314832 Color: 4862
Size: 266544 Color: 1845

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 418728 Color: 8573
Size: 296160 Color: 3872
Size: 285113 Color: 3164

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 418735 Color: 8574
Size: 321888 Color: 5198
Size: 259378 Color: 1185

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 418737 Color: 8575
Size: 310742 Color: 4665
Size: 270522 Color: 2158

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 418831 Color: 8576
Size: 292881 Color: 3655
Size: 288289 Color: 3373

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 418851 Color: 8577
Size: 324129 Color: 5301
Size: 257021 Color: 913

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 418883 Color: 8578
Size: 320273 Color: 5109
Size: 260845 Color: 1328

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 418897 Color: 8579
Size: 310097 Color: 4634
Size: 271007 Color: 2206

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 418970 Color: 8580
Size: 322970 Color: 5246
Size: 258061 Color: 1013

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 418999 Color: 8581
Size: 300240 Color: 4104
Size: 280762 Color: 2894

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 419063 Color: 8582
Size: 319463 Color: 5070
Size: 261475 Color: 1388

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 419140 Color: 8583
Size: 305533 Color: 4392
Size: 275328 Color: 2530

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 419250 Color: 8584
Size: 316862 Color: 4961
Size: 263889 Color: 1607

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 419307 Color: 8585
Size: 330320 Color: 5572
Size: 250374 Color: 87

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 419334 Color: 8586
Size: 310636 Color: 4658
Size: 270031 Color: 2119

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 419445 Color: 8587
Size: 325821 Color: 5375
Size: 254735 Color: 673

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 419462 Color: 8588
Size: 292603 Color: 3642
Size: 287936 Color: 3355

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 419478 Color: 8589
Size: 293873 Color: 3725
Size: 286650 Color: 3270

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 419502 Color: 8590
Size: 301585 Color: 4176
Size: 278914 Color: 2769

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 419510 Color: 8591
Size: 325136 Color: 5347
Size: 255355 Color: 743

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 419517 Color: 8592
Size: 313098 Color: 4765
Size: 267386 Color: 1906

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 419528 Color: 8593
Size: 308738 Color: 4561
Size: 271735 Color: 2270

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 419544 Color: 8594
Size: 312355 Color: 4730
Size: 268102 Color: 1960

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 419598 Color: 8595
Size: 308620 Color: 4558
Size: 271783 Color: 2275

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 419669 Color: 8596
Size: 329214 Color: 5532
Size: 251118 Color: 210

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 419672 Color: 8597
Size: 319329 Color: 5064
Size: 261000 Color: 1346

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 419700 Color: 8598
Size: 294829 Color: 3783
Size: 285472 Color: 3192

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 419714 Color: 8599
Size: 327247 Color: 5435
Size: 253040 Color: 466

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 419740 Color: 8600
Size: 307061 Color: 4473
Size: 273200 Color: 2371

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 419765 Color: 8601
Size: 315998 Color: 4921
Size: 264238 Color: 1633

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 419798 Color: 8602
Size: 301489 Color: 4169
Size: 278714 Color: 2759

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 419878 Color: 8603
Size: 311927 Color: 4713
Size: 268196 Color: 1971

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 419880 Color: 8604
Size: 304550 Color: 4347
Size: 275571 Color: 2553

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 419888 Color: 8605
Size: 303628 Color: 4299
Size: 276485 Color: 2606

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 419912 Color: 8606
Size: 295438 Color: 3819
Size: 284651 Color: 3140

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 419964 Color: 8607
Size: 307060 Color: 4472
Size: 272977 Color: 2352

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 419968 Color: 8608
Size: 311262 Color: 4685
Size: 268771 Color: 2018

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 419998 Color: 8609
Size: 294570 Color: 3766
Size: 285433 Color: 3190

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 420059 Color: 8610
Size: 300000 Color: 4087
Size: 279942 Color: 2833

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 420116 Color: 8611
Size: 314307 Color: 4827
Size: 265578 Color: 1749

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 420123 Color: 8612
Size: 328091 Color: 5485
Size: 251787 Color: 304

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 420253 Color: 8613
Size: 302734 Color: 4254
Size: 277014 Color: 2646

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 420263 Color: 8614
Size: 320639 Color: 5128
Size: 259099 Color: 1155

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 420362 Color: 8615
Size: 319479 Color: 5072
Size: 260160 Color: 1255

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 420398 Color: 8616
Size: 297227 Color: 3934
Size: 282376 Color: 2998

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 420488 Color: 8617
Size: 324662 Color: 5323
Size: 254851 Color: 688

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 420560 Color: 8618
Size: 324491 Color: 5318
Size: 254950 Color: 698

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 420580 Color: 8619
Size: 317638 Color: 4996
Size: 261783 Color: 1407

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 420588 Color: 8620
Size: 323839 Color: 5287
Size: 255574 Color: 761

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 420600 Color: 8621
Size: 320971 Color: 5147
Size: 258430 Color: 1069

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 420647 Color: 8622
Size: 304244 Color: 4332
Size: 275110 Color: 2511

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 420679 Color: 8623
Size: 313408 Color: 4773
Size: 265914 Color: 1782

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 420684 Color: 8624
Size: 327386 Color: 5446
Size: 251931 Color: 322

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 420693 Color: 8625
Size: 328117 Color: 5486
Size: 251191 Color: 219

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 420820 Color: 8626
Size: 319631 Color: 5081
Size: 259550 Color: 1197

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 420826 Color: 8627
Size: 314072 Color: 4815
Size: 265103 Color: 1712

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 420877 Color: 8628
Size: 291860 Color: 3586
Size: 287264 Color: 3312

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 420951 Color: 8629
Size: 294149 Color: 3743
Size: 284901 Color: 3154

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 420964 Color: 8630
Size: 320858 Color: 5142
Size: 258179 Color: 1030

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 421028 Color: 8631
Size: 298034 Color: 3975
Size: 280939 Color: 2903

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 421047 Color: 8632
Size: 317386 Color: 4984
Size: 261568 Color: 1393

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 421069 Color: 8633
Size: 293619 Color: 3712
Size: 285313 Color: 3177

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 421088 Color: 8634
Size: 296616 Color: 3899
Size: 282297 Color: 2991

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 421097 Color: 8635
Size: 292297 Color: 3615
Size: 286607 Color: 3264

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 421106 Color: 8636
Size: 307227 Color: 4481
Size: 271668 Color: 2260

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 421205 Color: 8637
Size: 310028 Color: 4632
Size: 268768 Color: 2017

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 421260 Color: 8638
Size: 322650 Color: 5231
Size: 256091 Color: 815

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 421321 Color: 8639
Size: 298975 Color: 4029
Size: 279705 Color: 2816

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 421357 Color: 8640
Size: 324406 Color: 5313
Size: 254238 Color: 621

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 421391 Color: 8641
Size: 309461 Color: 4597
Size: 269149 Color: 2041

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 421425 Color: 8642
Size: 299660 Color: 4069
Size: 278916 Color: 2770

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 421439 Color: 8643
Size: 308110 Color: 4520
Size: 270452 Color: 2152

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 421441 Color: 8644
Size: 313404 Color: 4772
Size: 265156 Color: 1717

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 421545 Color: 8645
Size: 292272 Color: 3614
Size: 286184 Color: 3229

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 421578 Color: 8646
Size: 322063 Color: 5203
Size: 256360 Color: 846

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 421613 Color: 8647
Size: 314226 Color: 4821
Size: 264162 Color: 1623

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 421641 Color: 8648
Size: 311335 Color: 4689
Size: 267025 Color: 1890

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 421654 Color: 8649
Size: 290634 Color: 3508
Size: 287713 Color: 3342

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 421771 Color: 8650
Size: 308409 Color: 4542
Size: 269821 Color: 2092

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 421783 Color: 8651
Size: 297770 Color: 3962
Size: 280448 Color: 2870

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 421927 Color: 8652
Size: 324874 Color: 5333
Size: 253200 Color: 494

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 421957 Color: 8653
Size: 327955 Color: 5474
Size: 250089 Color: 20

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 421987 Color: 8654
Size: 306246 Color: 4423
Size: 271768 Color: 2273

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 422041 Color: 8655
Size: 322623 Color: 5228
Size: 255337 Color: 741

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 422053 Color: 8656
Size: 307295 Color: 4486
Size: 270653 Color: 2174

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 422092 Color: 8657
Size: 325124 Color: 5345
Size: 252785 Color: 429

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 422100 Color: 8658
Size: 314637 Color: 4845
Size: 263264 Color: 1553

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 422117 Color: 8659
Size: 311886 Color: 4709
Size: 265998 Color: 1788

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 422171 Color: 8660
Size: 325569 Color: 5366
Size: 252261 Color: 367

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 422183 Color: 8661
Size: 296546 Color: 3895
Size: 281272 Color: 2933

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 422197 Color: 8662
Size: 327407 Color: 5448
Size: 250397 Color: 90

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 422268 Color: 8663
Size: 321222 Color: 5160
Size: 256511 Color: 864

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 422356 Color: 8664
Size: 324535 Color: 5320
Size: 253110 Color: 480

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 422394 Color: 8665
Size: 325125 Color: 5346
Size: 252482 Color: 393

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 422404 Color: 8666
Size: 291071 Color: 3535
Size: 286526 Color: 3255

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 422414 Color: 8667
Size: 293173 Color: 3673
Size: 284414 Color: 3126

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 422427 Color: 8668
Size: 320241 Color: 5105
Size: 257333 Color: 943

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 422507 Color: 8669
Size: 314447 Color: 4834
Size: 263047 Color: 1530

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 422520 Color: 8670
Size: 301444 Color: 4166
Size: 276037 Color: 2582

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 422542 Color: 8671
Size: 292640 Color: 3644
Size: 284819 Color: 3148

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 422676 Color: 8672
Size: 297001 Color: 3920
Size: 280324 Color: 2862

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 422837 Color: 8673
Size: 289114 Color: 3419
Size: 288050 Color: 3360

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 422848 Color: 8674
Size: 314741 Color: 4854
Size: 262412 Color: 1461

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 422884 Color: 8675
Size: 301525 Color: 4171
Size: 275592 Color: 2555

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 422887 Color: 8676
Size: 312777 Color: 4745
Size: 264337 Color: 1646

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 422900 Color: 8677
Size: 295049 Color: 3797
Size: 282052 Color: 2978

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 422982 Color: 8678
Size: 303161 Color: 4277
Size: 273858 Color: 2422

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 423013 Color: 8679
Size: 325001 Color: 5340
Size: 251987 Color: 332

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 423019 Color: 8680
Size: 313549 Color: 4778
Size: 263433 Color: 1567

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 423043 Color: 8681
Size: 308217 Color: 4529
Size: 268741 Color: 2014

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 423212 Color: 8682
Size: 323708 Color: 5277
Size: 253081 Color: 474

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 423216 Color: 8683
Size: 320677 Color: 5133
Size: 256108 Color: 817

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 423218 Color: 8684
Size: 296138 Color: 3869
Size: 280645 Color: 2887

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 423276 Color: 8685
Size: 289503 Color: 3438
Size: 287222 Color: 3311

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 423297 Color: 8686
Size: 299159 Color: 4043
Size: 277545 Color: 2685

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 423300 Color: 8687
Size: 294910 Color: 3790
Size: 281791 Color: 2966

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 423364 Color: 8688
Size: 293756 Color: 3721
Size: 282881 Color: 3031

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 423370 Color: 8689
Size: 316106 Color: 4931
Size: 260525 Color: 1295

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 423467 Color: 8690
Size: 303753 Color: 4303
Size: 272781 Color: 2336

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 423493 Color: 8691
Size: 321301 Color: 5166
Size: 255207 Color: 727

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 423518 Color: 8692
Size: 316143 Color: 4932
Size: 260340 Color: 1276

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 423534 Color: 8693
Size: 294683 Color: 3770
Size: 281784 Color: 2965

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 423582 Color: 8694
Size: 300206 Color: 4101
Size: 276213 Color: 2592

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 423609 Color: 8695
Size: 288464 Color: 3384
Size: 287928 Color: 3354

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 423671 Color: 8696
Size: 324080 Color: 5299
Size: 252250 Color: 366

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 423678 Color: 8697
Size: 289674 Color: 3447
Size: 286649 Color: 3268

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 423724 Color: 8698
Size: 292943 Color: 3659
Size: 283334 Color: 3058

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 423852 Color: 8699
Size: 308132 Color: 4525
Size: 268017 Color: 1954

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 423872 Color: 8700
Size: 324780 Color: 5330
Size: 251349 Color: 246

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 423900 Color: 8701
Size: 291807 Color: 3580
Size: 284294 Color: 3119

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 423920 Color: 8702
Size: 289660 Color: 3444
Size: 286421 Color: 3249

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 423949 Color: 8703
Size: 310694 Color: 4663
Size: 265358 Color: 1735

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 423951 Color: 8704
Size: 297568 Color: 3950
Size: 278482 Color: 2743

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 423990 Color: 8705
Size: 322258 Color: 5209
Size: 253753 Color: 559

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 424012 Color: 8706
Size: 323800 Color: 5283
Size: 252189 Color: 361

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 424016 Color: 8707
Size: 296844 Color: 3913
Size: 279141 Color: 2784

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 424101 Color: 8708
Size: 309246 Color: 4583
Size: 266654 Color: 1855

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 424124 Color: 8709
Size: 299196 Color: 4048
Size: 276681 Color: 2620

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 424192 Color: 8710
Size: 296091 Color: 3865
Size: 279718 Color: 2817

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 424310 Color: 8711
Size: 312142 Color: 4721
Size: 263549 Color: 1578

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 424317 Color: 8712
Size: 323418 Color: 5265
Size: 252266 Color: 369

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 424375 Color: 8713
Size: 308856 Color: 4569
Size: 266770 Color: 1867

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 424388 Color: 8714
Size: 308561 Color: 4549
Size: 267052 Color: 1892

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 424393 Color: 8715
Size: 313687 Color: 4787
Size: 261921 Color: 1416

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 424402 Color: 8716
Size: 293908 Color: 3728
Size: 281691 Color: 2961

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 424420 Color: 8717
Size: 301230 Color: 4154
Size: 274351 Color: 2461

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 424440 Color: 8718
Size: 316664 Color: 4954
Size: 258897 Color: 1122

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 424559 Color: 8719
Size: 307064 Color: 4474
Size: 268378 Color: 1990

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 424592 Color: 8720
Size: 290147 Color: 3485
Size: 285262 Color: 3174

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 424673 Color: 8721
Size: 290887 Color: 3522
Size: 284441 Color: 3129

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 424772 Color: 8722
Size: 289416 Color: 3436
Size: 285813 Color: 3207

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 424780 Color: 8723
Size: 315056 Color: 4877
Size: 260165 Color: 1258

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 424818 Color: 8724
Size: 310762 Color: 4666
Size: 264421 Color: 1657

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 424844 Color: 8725
Size: 305806 Color: 4408
Size: 269351 Color: 2059

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 424869 Color: 8726
Size: 307524 Color: 4494
Size: 267608 Color: 1927

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 424874 Color: 8727
Size: 299987 Color: 4085
Size: 275140 Color: 2516

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 424904 Color: 8728
Size: 289611 Color: 3442
Size: 285486 Color: 3193

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 425001 Color: 8729
Size: 309914 Color: 4624
Size: 265086 Color: 1710

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 425082 Color: 8730
Size: 305725 Color: 4404
Size: 269194 Color: 2047

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 425096 Color: 8731
Size: 315920 Color: 4918
Size: 258985 Color: 1138

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 425105 Color: 8732
Size: 305471 Color: 4391
Size: 269425 Color: 2067

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 425134 Color: 8733
Size: 296075 Color: 3862
Size: 278792 Color: 2762

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 425137 Color: 8734
Size: 288078 Color: 3361
Size: 286786 Color: 3280

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 425140 Color: 8735
Size: 303190 Color: 4278
Size: 271671 Color: 2262

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 425159 Color: 8736
Size: 313890 Color: 4803
Size: 260952 Color: 1339

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 425189 Color: 8737
Size: 295119 Color: 3801
Size: 279693 Color: 2815

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 425247 Color: 8738
Size: 322970 Color: 5247
Size: 251784 Color: 303

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 425290 Color: 8739
Size: 322940 Color: 5245
Size: 251771 Color: 301

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 425343 Color: 8740
Size: 324165 Color: 5303
Size: 250493 Color: 106

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 425390 Color: 8741
Size: 301132 Color: 4150
Size: 273479 Color: 2385

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 425432 Color: 8742
Size: 300872 Color: 4136
Size: 273697 Color: 2405

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 425532 Color: 8743
Size: 293458 Color: 3695
Size: 281011 Color: 2912

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 425534 Color: 8744
Size: 289351 Color: 3431
Size: 285116 Color: 3165

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 425572 Color: 8745
Size: 311982 Color: 4717
Size: 262447 Color: 1464

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 425580 Color: 8746
Size: 295838 Color: 3847
Size: 278583 Color: 2752

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 425604 Color: 8747
Size: 316292 Color: 4937
Size: 258105 Color: 1019

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 425616 Color: 8748
Size: 287455 Color: 3328
Size: 286930 Color: 3295

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 425630 Color: 8749
Size: 323453 Color: 5268
Size: 250918 Color: 175

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 425683 Color: 8750
Size: 320404 Color: 5119
Size: 253914 Color: 586

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 425781 Color: 8751
Size: 312923 Color: 4755
Size: 261297 Color: 1375

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 425798 Color: 8752
Size: 315241 Color: 4882
Size: 258962 Color: 1134

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 425811 Color: 8753
Size: 292812 Color: 3650
Size: 281378 Color: 2938

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 425830 Color: 8754
Size: 287322 Color: 3320
Size: 286849 Color: 3291

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 425889 Color: 8755
Size: 321750 Color: 5191
Size: 252362 Color: 376

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 426096 Color: 8756
Size: 294894 Color: 3788
Size: 279011 Color: 2773

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 426136 Color: 8757
Size: 299990 Color: 4086
Size: 273875 Color: 2425

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 426201 Color: 8758
Size: 297546 Color: 3947
Size: 276254 Color: 2596

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 426231 Color: 8759
Size: 306264 Color: 4426
Size: 267506 Color: 1919

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 426244 Color: 8760
Size: 302194 Color: 4219
Size: 271563 Color: 2254

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 426293 Color: 8761
Size: 318312 Color: 5027
Size: 255396 Color: 748

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 426398 Color: 8762
Size: 303966 Color: 4312
Size: 269637 Color: 2080

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 426440 Color: 8763
Size: 300343 Color: 4111
Size: 273218 Color: 2372

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 426654 Color: 8764
Size: 313069 Color: 4762
Size: 260278 Color: 1267

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 426666 Color: 8765
Size: 304085 Color: 4326
Size: 269250 Color: 2051

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 426764 Color: 8766
Size: 320394 Color: 5118
Size: 252843 Color: 435

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 426781 Color: 8767
Size: 308107 Color: 4519
Size: 265113 Color: 1715

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 426870 Color: 8768
Size: 298913 Color: 4022
Size: 274218 Color: 2451

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 426935 Color: 8769
Size: 304696 Color: 4360
Size: 268370 Color: 1989

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 427002 Color: 8770
Size: 288988 Color: 3411
Size: 284011 Color: 3100

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 427026 Color: 8771
Size: 291984 Color: 3593
Size: 280991 Color: 2909

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 427149 Color: 8772
Size: 317150 Color: 4973
Size: 255702 Color: 775

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 427172 Color: 8773
Size: 309462 Color: 4598
Size: 263367 Color: 1560

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 427188 Color: 8774
Size: 307703 Color: 4500
Size: 265110 Color: 1714

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 427249 Color: 8775
Size: 311893 Color: 4710
Size: 260859 Color: 1331

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 427299 Color: 8776
Size: 287024 Color: 3299
Size: 285678 Color: 3199

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 427322 Color: 8777
Size: 319071 Color: 5051
Size: 253608 Color: 543

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 427323 Color: 8778
Size: 306516 Color: 4442
Size: 266162 Color: 1813

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 427362 Color: 8779
Size: 321127 Color: 5152
Size: 251512 Color: 266

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 427373 Color: 8781
Size: 320014 Color: 5101
Size: 252614 Color: 409

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 427373 Color: 8780
Size: 319391 Color: 5068
Size: 253237 Color: 499

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 427396 Color: 8782
Size: 316185 Color: 4934
Size: 256420 Color: 851

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 427433 Color: 8783
Size: 315593 Color: 4899
Size: 256975 Color: 905

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 427477 Color: 8784
Size: 309585 Color: 4605
Size: 262939 Color: 1517

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 427495 Color: 8785
Size: 307945 Color: 4514
Size: 264561 Color: 1665

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 427502 Color: 8786
Size: 314242 Color: 4824
Size: 258257 Color: 1044

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 427537 Color: 8787
Size: 293099 Color: 3667
Size: 279365 Color: 2797

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 427576 Color: 8788
Size: 305551 Color: 4394
Size: 266874 Color: 1879

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 427588 Color: 8789
Size: 321598 Color: 5184
Size: 250815 Color: 158

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 427638 Color: 8790
Size: 308392 Color: 4540
Size: 263971 Color: 1612

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 427756 Color: 8791
Size: 314396 Color: 4830
Size: 257849 Color: 993

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 427807 Color: 8792
Size: 306886 Color: 4462
Size: 265308 Color: 1732

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 427850 Color: 8793
Size: 300129 Color: 4097
Size: 272022 Color: 2286

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 427955 Color: 8794
Size: 289560 Color: 3441
Size: 282486 Color: 3003

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 428043 Color: 8795
Size: 297107 Color: 3926
Size: 274851 Color: 2495

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 428064 Color: 8796
Size: 318249 Color: 5025
Size: 253688 Color: 552

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 428137 Color: 8797
Size: 294829 Color: 3784
Size: 277035 Color: 2650

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 428155 Color: 8798
Size: 300758 Color: 4134
Size: 271088 Color: 2217

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 428162 Color: 8799
Size: 306886 Color: 4461
Size: 264953 Color: 1696

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 428240 Color: 8800
Size: 296852 Color: 3915
Size: 274909 Color: 2499

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 428243 Color: 8801
Size: 286913 Color: 3294
Size: 284845 Color: 3151

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 428250 Color: 8802
Size: 291933 Color: 3590
Size: 279818 Color: 2822

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 428265 Color: 8803
Size: 305549 Color: 4393
Size: 266187 Color: 1814

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 428281 Color: 8804
Size: 289618 Color: 3443
Size: 282102 Color: 2979

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 428303 Color: 8805
Size: 310981 Color: 4673
Size: 260717 Color: 1317

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 428327 Color: 8806
Size: 297583 Color: 3952
Size: 274091 Color: 2440

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 428330 Color: 8807
Size: 301905 Color: 4195
Size: 269766 Color: 2087

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 428399 Color: 8808
Size: 321272 Color: 5163
Size: 250330 Color: 78

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 428412 Color: 8809
Size: 297108 Color: 3927
Size: 274481 Color: 2470

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 428414 Color: 8810
Size: 299009 Color: 4036
Size: 272578 Color: 2322

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 428436 Color: 8811
Size: 297362 Color: 3941
Size: 274203 Color: 2449

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 428537 Color: 8812
Size: 295476 Color: 3823
Size: 275988 Color: 2578

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 428837 Color: 8813
Size: 290197 Color: 3489
Size: 280967 Color: 2906

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 428863 Color: 8814
Size: 300234 Color: 4103
Size: 270904 Color: 2195

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 428875 Color: 8815
Size: 285733 Color: 3203
Size: 285393 Color: 3185

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 428884 Color: 8816
Size: 297127 Color: 3929
Size: 273990 Color: 2434

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 428945 Color: 8817
Size: 298261 Color: 3990
Size: 272795 Color: 2339

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 428959 Color: 8818
Size: 289534 Color: 3440
Size: 281508 Color: 2951

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 429001 Color: 8819
Size: 307653 Color: 4498
Size: 263347 Color: 1559

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 429008 Color: 8820
Size: 290678 Color: 3513
Size: 280315 Color: 2859

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 429009 Color: 8821
Size: 288621 Color: 3392
Size: 282371 Color: 2997

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 429088 Color: 8822
Size: 290045 Color: 3477
Size: 280868 Color: 2897

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 429235 Color: 8823
Size: 286440 Color: 3251
Size: 284326 Color: 3123

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 429246 Color: 8824
Size: 304812 Color: 4365
Size: 265943 Color: 1785

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 429387 Color: 8825
Size: 304181 Color: 4330
Size: 266433 Color: 1835

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 429438 Color: 8826
Size: 308093 Color: 4517
Size: 262470 Color: 1467

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 429536 Color: 8827
Size: 290045 Color: 3476
Size: 280420 Color: 2866

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 429576 Color: 8828
Size: 286784 Color: 3279
Size: 283641 Color: 3077

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 429595 Color: 8829
Size: 304587 Color: 4348
Size: 265819 Color: 1774

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 429627 Color: 8830
Size: 302439 Color: 4237
Size: 267935 Color: 1949

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 429649 Color: 8831
Size: 319776 Color: 5088
Size: 250576 Color: 123

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 429743 Color: 8832
Size: 285583 Color: 3196
Size: 284675 Color: 3142

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 429746 Color: 8833
Size: 300297 Color: 4108
Size: 269958 Color: 2102

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 429836 Color: 8834
Size: 297565 Color: 3949
Size: 272600 Color: 2323

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 429852 Color: 8835
Size: 288334 Color: 3377
Size: 281815 Color: 2967

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 429871 Color: 8836
Size: 305662 Color: 4401
Size: 264468 Color: 1658

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 429876 Color: 8837
Size: 313892 Color: 4804
Size: 256233 Color: 834

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 429882 Color: 8838
Size: 302480 Color: 4240
Size: 267639 Color: 1931

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 429979 Color: 8839
Size: 305272 Color: 4386
Size: 264750 Color: 1680

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 429980 Color: 8840
Size: 300176 Color: 4098
Size: 269845 Color: 2095

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 430009 Color: 8841
Size: 302266 Color: 4226
Size: 267726 Color: 1935

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 430066 Color: 8842
Size: 311637 Color: 4701
Size: 258298 Color: 1050

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 430094 Color: 8843
Size: 317964 Color: 5015
Size: 251943 Color: 325

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 430132 Color: 8844
Size: 301720 Color: 4184
Size: 268149 Color: 1966

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 430153 Color: 8845
Size: 316924 Color: 4964
Size: 252924 Color: 445

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 430162 Color: 8846
Size: 289893 Color: 3465
Size: 279946 Color: 2835

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 430240 Color: 8847
Size: 311898 Color: 4712
Size: 257863 Color: 994

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 430256 Color: 8848
Size: 295281 Color: 3812
Size: 274464 Color: 2469

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 430269 Color: 8849
Size: 300384 Color: 4116
Size: 269348 Color: 2058

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 430360 Color: 8850
Size: 299085 Color: 4042
Size: 270556 Color: 2162

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 430366 Color: 8851
Size: 303568 Color: 4294
Size: 266067 Color: 1799

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 430432 Color: 8852
Size: 286945 Color: 3296
Size: 282624 Color: 3016

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 430436 Color: 8853
Size: 292687 Color: 3645
Size: 276878 Color: 2634

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 430458 Color: 8854
Size: 289084 Color: 3416
Size: 280459 Color: 2872

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 430465 Color: 8855
Size: 286155 Color: 3226
Size: 283381 Color: 3060

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 430526 Color: 8856
Size: 293664 Color: 3715
Size: 275811 Color: 2569

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 430576 Color: 8857
Size: 292189 Color: 3608
Size: 277236 Color: 2665

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 430581 Color: 8858
Size: 292355 Color: 3623
Size: 277065 Color: 2652

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 430618 Color: 8859
Size: 319248 Color: 5061
Size: 250135 Color: 31

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 430701 Color: 8860
Size: 287061 Color: 3302
Size: 282239 Color: 2984

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 430730 Color: 8861
Size: 313489 Color: 4776
Size: 255782 Color: 783

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 430736 Color: 8862
Size: 310501 Color: 4651
Size: 258764 Color: 1105

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 430926 Color: 8863
Size: 305012 Color: 4375
Size: 264063 Color: 1616

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 430969 Color: 8864
Size: 308594 Color: 4556
Size: 260438 Color: 1288

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 430985 Color: 8865
Size: 290055 Color: 3479
Size: 278961 Color: 2772

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 431015 Color: 8866
Size: 285033 Color: 3159
Size: 283953 Color: 3095

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 431052 Color: 8867
Size: 305621 Color: 4398
Size: 263328 Color: 1556

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 431153 Color: 8868
Size: 303902 Color: 4310
Size: 264946 Color: 1694

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 431272 Color: 8869
Size: 293178 Color: 3674
Size: 275551 Color: 2550

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 431290 Color: 8870
Size: 308478 Color: 4546
Size: 260233 Color: 1263

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 431309 Color: 8871
Size: 292122 Color: 3605
Size: 276570 Color: 2612

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 431434 Color: 8872
Size: 285264 Color: 3175
Size: 283303 Color: 3056

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 431459 Color: 8873
Size: 298546 Color: 4004
Size: 269996 Color: 2109

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 431512 Color: 8874
Size: 287889 Color: 3352
Size: 280600 Color: 2884

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 431559 Color: 8875
Size: 291698 Color: 3569
Size: 276744 Color: 2622

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 431642 Color: 8876
Size: 300216 Color: 4102
Size: 268143 Color: 1965

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 431656 Color: 8877
Size: 307857 Color: 4511
Size: 260488 Color: 1292

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 431753 Color: 8878
Size: 292249 Color: 3611
Size: 275999 Color: 2579

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 431757 Color: 8879
Size: 285247 Color: 3172
Size: 282997 Color: 3040

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 431761 Color: 8880
Size: 300053 Color: 4092
Size: 268187 Color: 1970

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 431794 Color: 8881
Size: 315199 Color: 4881
Size: 253008 Color: 461

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 431864 Color: 8882
Size: 296723 Color: 3906
Size: 271414 Color: 2243

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 431988 Color: 8883
Size: 290023 Color: 3473
Size: 277990 Color: 2708

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 432007 Color: 8884
Size: 292709 Color: 3647
Size: 275285 Color: 2527

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 432018 Color: 8885
Size: 313900 Color: 4806
Size: 254083 Color: 607

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 432089 Color: 8886
Size: 310186 Color: 4636
Size: 257726 Color: 984

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 432097 Color: 8887
Size: 286504 Color: 3253
Size: 281400 Color: 2942

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 432100 Color: 8888
Size: 299636 Color: 4066
Size: 268265 Color: 1974

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 432106 Color: 8889
Size: 298241 Color: 3988
Size: 269654 Color: 2082

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 432118 Color: 8890
Size: 288450 Color: 3382
Size: 279433 Color: 2802

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 432159 Color: 8891
Size: 302809 Color: 4259
Size: 265033 Color: 1705

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 432257 Color: 8892
Size: 315256 Color: 4884
Size: 252488 Color: 397

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 432270 Color: 8893
Size: 311590 Color: 4699
Size: 256141 Color: 824

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 432397 Color: 8894
Size: 311053 Color: 4678
Size: 256551 Color: 867

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 432425 Color: 8895
Size: 313804 Color: 4796
Size: 253772 Color: 562

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 432551 Color: 8896
Size: 299342 Color: 4055
Size: 268108 Color: 1962

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 432616 Color: 8897
Size: 286839 Color: 3288
Size: 280546 Color: 2879

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 432784 Color: 8898
Size: 297641 Color: 3957
Size: 269576 Color: 2076

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 432794 Color: 8899
Size: 296342 Color: 3885
Size: 270865 Color: 2192

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 432878 Color: 8900
Size: 291760 Color: 3576
Size: 275363 Color: 2534

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 432957 Color: 8901
Size: 284175 Color: 3113
Size: 282869 Color: 3029

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 432970 Color: 8902
Size: 296636 Color: 3902
Size: 270395 Color: 2149

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 433002 Color: 8903
Size: 302130 Color: 4214
Size: 264869 Color: 1686

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 433032 Color: 8904
Size: 308928 Color: 4570
Size: 258041 Color: 1010

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 433103 Color: 8905
Size: 291313 Color: 3549
Size: 275585 Color: 2554

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 433120 Color: 8906
Size: 312441 Color: 4735
Size: 254440 Color: 637

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 433257 Color: 8908
Size: 289219 Color: 3426
Size: 277525 Color: 2683

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 433257 Color: 8907
Size: 297808 Color: 3964
Size: 268936 Color: 2029

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 433335 Color: 8909
Size: 308456 Color: 4545
Size: 258210 Color: 1038

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 433340 Color: 8910
Size: 310431 Color: 4648
Size: 256230 Color: 832

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 433403 Color: 8911
Size: 289775 Color: 3457
Size: 276823 Color: 2629

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 433463 Color: 8912
Size: 293971 Color: 3732
Size: 272567 Color: 2320

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 433533 Color: 8913
Size: 283838 Color: 3088
Size: 282630 Color: 3017

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 433599 Color: 8914
Size: 306991 Color: 4467
Size: 259411 Color: 1186

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 433828 Color: 8915
Size: 301874 Color: 4193
Size: 264299 Color: 1643

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 433838 Color: 8916
Size: 304051 Color: 4323
Size: 262112 Color: 1435

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 434011 Color: 8917
Size: 292875 Color: 3654
Size: 273115 Color: 2360

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 434022 Color: 8918
Size: 315649 Color: 4902
Size: 250330 Color: 77

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 434039 Color: 8919
Size: 286239 Color: 3233
Size: 279723 Color: 2818

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 434114 Color: 8920
Size: 315804 Color: 4903
Size: 250083 Color: 16

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 434301 Color: 8921
Size: 300447 Color: 4118
Size: 265253 Color: 1727

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 434413 Color: 8922
Size: 289934 Color: 3470
Size: 275654 Color: 2559

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 434472 Color: 8923
Size: 283940 Color: 3093
Size: 281589 Color: 2954

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 434477 Color: 8924
Size: 304347 Color: 4336
Size: 261177 Color: 1365

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 434533 Color: 8925
Size: 285076 Color: 3161
Size: 280392 Color: 2864

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 434659 Color: 8926
Size: 304927 Color: 4371
Size: 260415 Color: 1284

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 434844 Color: 8927
Size: 296620 Color: 3901
Size: 268537 Color: 1998

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 434923 Color: 8928
Size: 298930 Color: 4024
Size: 266148 Color: 1811

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 434944 Color: 8929
Size: 293992 Color: 3733
Size: 271065 Color: 2213

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 434996 Color: 8930
Size: 310274 Color: 4642
Size: 254731 Color: 671

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 435046 Color: 8931
Size: 302225 Color: 4224
Size: 262730 Color: 1498

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 435108 Color: 8932
Size: 310778 Color: 4668
Size: 254115 Color: 608

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 435141 Color: 8933
Size: 294835 Color: 3785
Size: 270025 Color: 2118

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 435164 Color: 8934
Size: 294710 Color: 3772
Size: 270127 Color: 2129

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 435180 Color: 8935
Size: 299734 Color: 4073
Size: 265087 Color: 1711

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 435197 Color: 8936
Size: 304622 Color: 4351
Size: 260182 Color: 1260

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 435282 Color: 8937
Size: 295331 Color: 3816
Size: 269388 Color: 2064

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 435440 Color: 8938
Size: 309953 Color: 4629
Size: 254608 Color: 654

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 435481 Color: 8939
Size: 309404 Color: 4595
Size: 255116 Color: 719

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 435665 Color: 8940
Size: 293267 Color: 3680
Size: 271069 Color: 2214

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 435684 Color: 8941
Size: 284817 Color: 3146
Size: 279500 Color: 2805

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 435759 Color: 8942
Size: 300248 Color: 4106
Size: 263994 Color: 1614

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 435761 Color: 8943
Size: 312607 Color: 4740
Size: 251633 Color: 283

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 435847 Color: 8944
Size: 300013 Color: 4090
Size: 264141 Color: 1621

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 435901 Color: 8945
Size: 301810 Color: 4188
Size: 262290 Color: 1452

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 435954 Color: 8946
Size: 288878 Color: 3408
Size: 275169 Color: 2517

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 436001 Color: 8947
Size: 305848 Color: 4412
Size: 258152 Color: 1028

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 436021 Color: 8948
Size: 292349 Color: 3621
Size: 271631 Color: 2257

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 436032 Color: 8949
Size: 300961 Color: 4141
Size: 263008 Color: 1524

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 436057 Color: 8950
Size: 304689 Color: 4359
Size: 259255 Color: 1174

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 436067 Color: 8951
Size: 312368 Color: 4732
Size: 251566 Color: 276

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 436311 Color: 8952
Size: 307266 Color: 4483
Size: 256424 Color: 852

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 436322 Color: 8953
Size: 291345 Color: 3554
Size: 272334 Color: 2308

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 436360 Color: 8954
Size: 293167 Color: 3671
Size: 270474 Color: 2155

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 436415 Color: 8955
Size: 312694 Color: 4743
Size: 250892 Color: 168

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 436417 Color: 8956
Size: 291826 Color: 3583
Size: 271758 Color: 2271

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 436420 Color: 8957
Size: 286450 Color: 3252
Size: 277131 Color: 2656

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 436429 Color: 8958
Size: 288692 Color: 3396
Size: 274880 Color: 2498

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 436440 Color: 8959
Size: 291484 Color: 3558
Size: 272077 Color: 2289

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 436522 Color: 8960
Size: 308294 Color: 4531
Size: 255185 Color: 726

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 436651 Color: 8961
Size: 308726 Color: 4559
Size: 254624 Color: 655

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 436739 Color: 8962
Size: 304805 Color: 4364
Size: 258457 Color: 1071

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 436816 Color: 8963
Size: 300543 Color: 4123
Size: 262642 Color: 1484

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 436835 Color: 8964
Size: 306387 Color: 4432
Size: 256779 Color: 893

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 436851 Color: 8965
Size: 304087 Color: 4327
Size: 259063 Color: 1148

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 436863 Color: 8966
Size: 286847 Color: 3290
Size: 276291 Color: 2599

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 436883 Color: 8967
Size: 306263 Color: 4425
Size: 256855 Color: 897

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 436913 Color: 8968
Size: 281608 Color: 2955
Size: 281480 Color: 2947

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 436996 Color: 8969
Size: 297979 Color: 3972
Size: 265026 Color: 1702

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 437000 Color: 8970
Size: 300883 Color: 4138
Size: 262118 Color: 1436

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 437057 Color: 8971
Size: 281946 Color: 2972
Size: 280998 Color: 2910

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 437076 Color: 8972
Size: 292599 Color: 3641
Size: 270326 Color: 2143

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 437098 Color: 8973
Size: 296134 Color: 3867
Size: 266769 Color: 1866

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 437118 Color: 8974
Size: 283973 Color: 3097
Size: 278910 Color: 2767

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 437192 Color: 8976
Size: 294716 Color: 3773
Size: 268093 Color: 1959

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 437192 Color: 8975
Size: 291727 Color: 3573
Size: 271082 Color: 2216

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 437205 Color: 8977
Size: 301003 Color: 4142
Size: 261793 Color: 1409

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 437350 Color: 8978
Size: 294920 Color: 3791
Size: 267731 Color: 1936

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 437427 Color: 8979
Size: 285470 Color: 3191
Size: 277104 Color: 2654

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 437452 Color: 8980
Size: 290475 Color: 3500
Size: 272074 Color: 2288

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 437569 Color: 8981
Size: 282342 Color: 2995
Size: 280090 Color: 2846

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 437768 Color: 8982
Size: 311250 Color: 4684
Size: 250983 Color: 185

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 437865 Color: 8983
Size: 283991 Color: 3098
Size: 278145 Color: 2720

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 437873 Color: 8984
Size: 285326 Color: 3178
Size: 276802 Color: 2625

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 437877 Color: 8985
Size: 311780 Color: 4706
Size: 250344 Color: 81

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 437945 Color: 8986
Size: 303387 Color: 4286
Size: 258669 Color: 1095

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 438038 Color: 8987
Size: 288432 Color: 3381
Size: 273531 Color: 2388

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 438058 Color: 8988
Size: 306819 Color: 4458
Size: 255124 Color: 720

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 438102 Color: 8989
Size: 287634 Color: 3339
Size: 274265 Color: 2454

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 438191 Color: 8990
Size: 295671 Color: 3835
Size: 266139 Color: 1808

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 438220 Color: 8991
Size: 307807 Color: 4507
Size: 253974 Color: 595

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 438264 Color: 8992
Size: 286278 Color: 3237
Size: 275459 Color: 2543

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 438288 Color: 8993
Size: 298184 Color: 3984
Size: 263529 Color: 1575

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 438305 Color: 8994
Size: 281516 Color: 2952
Size: 280180 Color: 2852

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 438325 Color: 8995
Size: 295306 Color: 3814
Size: 266370 Color: 1830

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 438407 Color: 8996
Size: 290090 Color: 3481
Size: 271504 Color: 2247

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 438501 Color: 8997
Size: 295530 Color: 3829
Size: 265970 Color: 1786

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 438599 Color: 8998
Size: 306743 Color: 4456
Size: 254659 Color: 660

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 438607 Color: 8999
Size: 303994 Color: 4318
Size: 257400 Color: 951

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 438693 Color: 9000
Size: 293493 Color: 3700
Size: 267815 Color: 1942

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 438725 Color: 9001
Size: 299759 Color: 4075
Size: 261517 Color: 1391

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 438875 Color: 9002
Size: 296601 Color: 3898
Size: 264525 Color: 1662

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 438913 Color: 9003
Size: 301380 Color: 4159
Size: 259708 Color: 1216

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 438918 Color: 9004
Size: 308578 Color: 4552
Size: 252505 Color: 399

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 439043 Color: 9005
Size: 304865 Color: 4369
Size: 256093 Color: 816

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 439124 Color: 9006
Size: 293330 Color: 3685
Size: 267547 Color: 1923

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 439247 Color: 9007
Size: 280919 Color: 2900
Size: 279835 Color: 2823

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 439259 Color: 9008
Size: 286792 Color: 3281
Size: 273950 Color: 2431

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 439269 Color: 9009
Size: 295568 Color: 3831
Size: 265164 Color: 1719

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 439301 Color: 9010
Size: 302735 Color: 4255
Size: 257965 Color: 1004

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 439342 Color: 9011
Size: 307378 Color: 4488
Size: 253281 Color: 508

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 439400 Color: 9012
Size: 299682 Color: 4072
Size: 260919 Color: 1336

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 439430 Color: 9013
Size: 300255 Color: 4107
Size: 260316 Color: 1273

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 439443 Color: 9014
Size: 302153 Color: 4217
Size: 258405 Color: 1063

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 439452 Color: 9015
Size: 294987 Color: 3794
Size: 265562 Color: 1748

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 439514 Color: 9016
Size: 293711 Color: 3718
Size: 266776 Color: 1868

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 439555 Color: 9017
Size: 290434 Color: 3499
Size: 270012 Color: 2114

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 439585 Color: 9018
Size: 297311 Color: 3938
Size: 263105 Color: 1536

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 439589 Color: 9019
Size: 284461 Color: 3130
Size: 275951 Color: 2576

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 439593 Color: 9020
Size: 294357 Color: 3755
Size: 266051 Color: 1796

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 439650 Color: 9021
Size: 307886 Color: 4512
Size: 252465 Color: 392

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 439718 Color: 9022
Size: 309354 Color: 4588
Size: 250929 Color: 178

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 439802 Color: 9023
Size: 285250 Color: 3173
Size: 274949 Color: 2504

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 439822 Color: 9024
Size: 288519 Color: 3387
Size: 271660 Color: 2258

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 439929 Color: 9025
Size: 301388 Color: 4160
Size: 258684 Color: 1097

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 439989 Color: 9026
Size: 306221 Color: 4421
Size: 253791 Color: 566

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 440003 Color: 9028
Size: 285414 Color: 3187
Size: 274584 Color: 2479

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 440003 Color: 9027
Size: 294386 Color: 3757
Size: 265612 Color: 1750

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 440060 Color: 9029
Size: 304025 Color: 4320
Size: 255916 Color: 797

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 440086 Color: 9030
Size: 307708 Color: 4502
Size: 252207 Color: 363

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 440113 Color: 9031
Size: 289223 Color: 3427
Size: 270665 Color: 2176

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 440120 Color: 9032
Size: 296822 Color: 3911
Size: 263059 Color: 1532

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 440134 Color: 9034
Size: 283759 Color: 3080
Size: 276108 Color: 2589

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 440134 Color: 9033
Size: 302955 Color: 4269
Size: 256912 Color: 901

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 440161 Color: 9035
Size: 308431 Color: 4544
Size: 251409 Color: 256

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 440178 Color: 9036
Size: 305656 Color: 4399
Size: 254167 Color: 614

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 440193 Color: 9037
Size: 293514 Color: 3704
Size: 266294 Color: 1826

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 440284 Color: 9038
Size: 293462 Color: 3696
Size: 266255 Color: 1821

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 440372 Color: 9039
Size: 309450 Color: 4596
Size: 250179 Color: 39

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 440411 Color: 9040
Size: 283795 Color: 3085
Size: 275795 Color: 2568

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 440461 Color: 9041
Size: 296147 Color: 3871
Size: 263393 Color: 1562

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 440500 Color: 9042
Size: 308302 Color: 4532
Size: 251199 Color: 221

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 440557 Color: 9043
Size: 295936 Color: 3853
Size: 263508 Color: 1573

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 440644 Color: 9044
Size: 280165 Color: 2850
Size: 279192 Color: 2787

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 440670 Color: 9045
Size: 291810 Color: 3581
Size: 267521 Color: 1922

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 440691 Color: 9046
Size: 285707 Color: 3202
Size: 273603 Color: 2396

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 440713 Color: 9047
Size: 301390 Color: 4161
Size: 257898 Color: 996

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 440742 Color: 9048
Size: 287010 Color: 3298
Size: 272249 Color: 2303

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 440760 Color: 9049
Size: 294890 Color: 3787
Size: 264351 Color: 1647

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 440799 Color: 9050
Size: 305690 Color: 4403
Size: 253512 Color: 530

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 440917 Color: 9051
Size: 286376 Color: 3245
Size: 272708 Color: 2332

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 440985 Color: 9052
Size: 285430 Color: 3189
Size: 273586 Color: 2393

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 441017 Color: 9053
Size: 282732 Color: 3024
Size: 276252 Color: 2595

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 441038 Color: 9054
Size: 296137 Color: 3868
Size: 262826 Color: 1507

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 441054 Color: 9055
Size: 293018 Color: 3662
Size: 265929 Color: 1784

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 441073 Color: 9056
Size: 308800 Color: 4566
Size: 250128 Color: 28

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 441092 Color: 9057
Size: 305092 Color: 4380
Size: 253817 Color: 573

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 441111 Color: 9058
Size: 285891 Color: 3210
Size: 272999 Color: 2354

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 441162 Color: 9059
Size: 281653 Color: 2959
Size: 277186 Color: 2662

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 441470 Color: 9060
Size: 288026 Color: 3359
Size: 270505 Color: 2157

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 441518 Color: 9061
Size: 292022 Color: 3597
Size: 266461 Color: 1838

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 441541 Color: 9062
Size: 304841 Color: 4367
Size: 253619 Color: 544

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 441638 Color: 9063
Size: 289801 Color: 3459
Size: 268562 Color: 2001

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 441646 Color: 9064
Size: 307736 Color: 4503
Size: 250619 Color: 129

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 441699 Color: 9065
Size: 298302 Color: 3992
Size: 260000 Color: 1240

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 441770 Color: 9066
Size: 281227 Color: 2929
Size: 277004 Color: 2645

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 441865 Color: 9067
Size: 279511 Color: 2806
Size: 278625 Color: 2755

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 441875 Color: 9068
Size: 296187 Color: 3878
Size: 261939 Color: 1420

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 441891 Color: 9069
Size: 283152 Color: 3050
Size: 274958 Color: 2505

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 441897 Color: 9070
Size: 287371 Color: 3323
Size: 270733 Color: 2182

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 441926 Color: 9071
Size: 288257 Color: 3371
Size: 269818 Color: 2091

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 442064 Color: 9072
Size: 279858 Color: 2826
Size: 278079 Color: 2715

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 442101 Color: 9073
Size: 279607 Color: 2811
Size: 278293 Color: 2733

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 442201 Color: 9074
Size: 303272 Color: 4281
Size: 254528 Color: 645

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 442211 Color: 9075
Size: 280093 Color: 2847
Size: 277697 Color: 2694

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 442272 Color: 9076
Size: 283059 Color: 3044
Size: 274670 Color: 2485

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 442365 Color: 9077
Size: 295226 Color: 3809
Size: 262410 Color: 1460

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 442400 Color: 9078
Size: 296264 Color: 3882
Size: 261337 Color: 1379

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 442432 Color: 9079
Size: 302275 Color: 4227
Size: 255294 Color: 736

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 442436 Color: 9080
Size: 300244 Color: 4105
Size: 257321 Color: 939

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 442497 Color: 9081
Size: 297202 Color: 3931
Size: 260302 Color: 1272

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 442520 Color: 9082
Size: 301753 Color: 4187
Size: 255728 Color: 779

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 442546 Color: 9083
Size: 297106 Color: 3925
Size: 260349 Color: 1277

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 442577 Color: 9084
Size: 306610 Color: 4448
Size: 250814 Color: 157

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 442616 Color: 9085
Size: 287863 Color: 3350
Size: 269522 Color: 2071

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 442683 Color: 9086
Size: 296481 Color: 3891
Size: 260837 Color: 1326

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 442746 Color: 9087
Size: 282291 Color: 2990
Size: 274964 Color: 2506

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 442761 Color: 9088
Size: 294104 Color: 3740
Size: 263136 Color: 1542

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 442827 Color: 9089
Size: 299662 Color: 4070
Size: 257512 Color: 960

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 442849 Color: 9090
Size: 307019 Color: 4469
Size: 250133 Color: 29

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 442857 Color: 9091
Size: 280045 Color: 2842
Size: 277099 Color: 2653

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 442890 Color: 9092
Size: 288806 Color: 3405
Size: 268305 Color: 1978

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 442906 Color: 9093
Size: 282526 Color: 3009
Size: 274569 Color: 2476

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 442910 Color: 9094
Size: 287925 Color: 3353
Size: 269166 Color: 2045

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 442944 Color: 9095
Size: 297336 Color: 3940
Size: 259721 Color: 1219

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 442993 Color: 9096
Size: 280067 Color: 2844
Size: 276941 Color: 2640

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 443078 Color: 9097
Size: 283072 Color: 3045
Size: 273851 Color: 2421

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 443106 Color: 9098
Size: 296032 Color: 3859
Size: 260863 Color: 1332

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 443200 Color: 9099
Size: 280933 Color: 2902
Size: 275868 Color: 2572

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 443236 Color: 9100
Size: 302055 Color: 4208
Size: 254710 Color: 667

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 443281 Color: 9101
Size: 284433 Color: 3127
Size: 272287 Color: 2307

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 443355 Color: 9102
Size: 291706 Color: 3571
Size: 264940 Color: 1692

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 443358 Color: 9103
Size: 289204 Color: 3425
Size: 267439 Color: 1913

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 443363 Color: 9104
Size: 280082 Color: 2845
Size: 276556 Color: 2610

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 443426 Color: 9105
Size: 300117 Color: 4096
Size: 256458 Color: 855

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 443564 Color: 9106
Size: 297025 Color: 3921
Size: 259412 Color: 1187

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 443582 Color: 9107
Size: 296365 Color: 3886
Size: 260054 Color: 1244

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 443655 Color: 9108
Size: 288207 Color: 3368
Size: 268139 Color: 1964

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 443704 Color: 9109
Size: 306043 Color: 4417
Size: 250254 Color: 61

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 443907 Color: 9110
Size: 286810 Color: 3284
Size: 269284 Color: 2052

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 443951 Color: 9111
Size: 300411 Color: 4117
Size: 255639 Color: 772

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 443970 Color: 9112
Size: 293706 Color: 3717
Size: 262325 Color: 1453

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 443986 Color: 9113
Size: 305208 Color: 4384
Size: 250807 Color: 154

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 443990 Color: 9114
Size: 293941 Color: 3729
Size: 262070 Color: 1431

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 443997 Color: 9115
Size: 302536 Color: 4242
Size: 253468 Color: 523

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 444026 Color: 9116
Size: 283719 Color: 3079
Size: 272256 Color: 2304

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 444170 Color: 9117
Size: 287387 Color: 3324
Size: 268444 Color: 1992

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 444193 Color: 9118
Size: 278312 Color: 2736
Size: 277496 Color: 2681

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 444217 Color: 9119
Size: 294424 Color: 3758
Size: 261360 Color: 1381

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 444437 Color: 9120
Size: 302612 Color: 4248
Size: 252952 Color: 449

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 444475 Color: 9121
Size: 285394 Color: 3186
Size: 270132 Color: 2133

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 444512 Color: 9122
Size: 297720 Color: 3959
Size: 257769 Color: 987

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 444544 Color: 9123
Size: 301921 Color: 4197
Size: 253536 Color: 532

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 444560 Color: 9124
Size: 283456 Color: 3065
Size: 271985 Color: 2283

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 444565 Color: 9125
Size: 280496 Color: 2877
Size: 274940 Color: 2503

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 444565 Color: 9126
Size: 284321 Color: 3122
Size: 271115 Color: 2219

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 444694 Color: 9127
Size: 283534 Color: 3069
Size: 271773 Color: 2274

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 444698 Color: 9128
Size: 283588 Color: 3074
Size: 271715 Color: 2267

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 444824 Color: 9129
Size: 293478 Color: 3698
Size: 261699 Color: 1401

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 444839 Color: 9130
Size: 282354 Color: 2996
Size: 272808 Color: 2342

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 444854 Color: 9131
Size: 291329 Color: 3551
Size: 263818 Color: 1599

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 444859 Color: 9132
Size: 278892 Color: 2766
Size: 276250 Color: 2594

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 444873 Color: 9133
Size: 290821 Color: 3519
Size: 264307 Color: 1644

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 444922 Color: 9134
Size: 284082 Color: 3105
Size: 270997 Color: 2204

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 444979 Color: 9135
Size: 278141 Color: 2719
Size: 276881 Color: 2635

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 445024 Color: 9136
Size: 293903 Color: 3727
Size: 261074 Color: 1356

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 445069 Color: 9137
Size: 302968 Color: 4270
Size: 251964 Color: 327

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 445138 Color: 9138
Size: 292919 Color: 3657
Size: 261944 Color: 1421

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 445174 Color: 9139
Size: 284562 Color: 3134
Size: 270265 Color: 2141

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 445180 Color: 9140
Size: 292001 Color: 3594
Size: 262820 Color: 1506

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 445282 Color: 9141
Size: 279260 Color: 2790
Size: 275459 Color: 2544

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 445308 Color: 9142
Size: 292340 Color: 3620
Size: 262353 Color: 1455

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 445423 Color: 9143
Size: 292329 Color: 3619
Size: 262249 Color: 1449

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 445442 Color: 9144
Size: 302412 Color: 4235
Size: 252147 Color: 352

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 445551 Color: 9145
Size: 287211 Color: 3309
Size: 267239 Color: 1901

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 445554 Color: 9146
Size: 294486 Color: 3761
Size: 259961 Color: 1233

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 445609 Color: 9147
Size: 301234 Color: 4155
Size: 253158 Color: 487

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 445662 Color: 9148
Size: 284377 Color: 3125
Size: 269962 Color: 2107

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 445666 Color: 9149
Size: 287790 Color: 3346
Size: 266545 Color: 1846

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 445743 Color: 9150
Size: 289509 Color: 3439
Size: 264749 Color: 1679

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 445752 Color: 9151
Size: 297820 Color: 3965
Size: 256429 Color: 853

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 445773 Color: 9152
Size: 294243 Color: 3750
Size: 259985 Color: 1237

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 445828 Color: 9153
Size: 280207 Color: 2854
Size: 273966 Color: 2432

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 445916 Color: 9154
Size: 293275 Color: 3682
Size: 260810 Color: 1323

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 445928 Color: 9155
Size: 296098 Color: 3866
Size: 257975 Color: 1006

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 445987 Color: 9156
Size: 291011 Color: 3529
Size: 263003 Color: 1523

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 445995 Color: 9157
Size: 295401 Color: 3817
Size: 258605 Color: 1089

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 446009 Color: 9158
Size: 297326 Color: 3939
Size: 256666 Color: 885

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 446016 Color: 9159
Size: 287127 Color: 3305
Size: 266858 Color: 1876

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 446106 Color: 9160
Size: 286707 Color: 3272
Size: 267188 Color: 1898

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 446127 Color: 9161
Size: 299647 Color: 4068
Size: 254227 Color: 619

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 446191 Color: 9162
Size: 297585 Color: 3953
Size: 256225 Color: 830

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 446217 Color: 9163
Size: 279388 Color: 2799
Size: 274396 Color: 2467

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 446242 Color: 9164
Size: 301851 Color: 4191
Size: 251908 Color: 317

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 446284 Color: 9165
Size: 297227 Color: 3935
Size: 256490 Color: 859

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 446300 Color: 9166
Size: 299840 Color: 4079
Size: 253861 Color: 583

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 446557 Color: 9167
Size: 300464 Color: 4120
Size: 252980 Color: 453

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 446667 Color: 9168
Size: 298853 Color: 4020
Size: 254481 Color: 642

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 446804 Color: 9169
Size: 285988 Color: 3217
Size: 267209 Color: 1899

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 446864 Color: 9170
Size: 286552 Color: 3257
Size: 266585 Color: 1851

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 446908 Color: 9171
Size: 280863 Color: 2896
Size: 272230 Color: 2302

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 446983 Color: 9172
Size: 285966 Color: 3215
Size: 267052 Color: 1891

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 446986 Color: 9173
Size: 286796 Color: 3282
Size: 266219 Color: 1816

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 446995 Color: 9174
Size: 281309 Color: 2934
Size: 271697 Color: 2264

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 447017 Color: 9175
Size: 293251 Color: 3677
Size: 259733 Color: 1221

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 447078 Color: 9176
Size: 295314 Color: 3815
Size: 257609 Color: 971

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 447180 Color: 9177
Size: 288923 Color: 3409
Size: 263898 Color: 1608

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 447271 Color: 9178
Size: 278581 Color: 2751
Size: 274149 Color: 2446

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 447281 Color: 9179
Size: 295522 Color: 3828
Size: 257198 Color: 925

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 447301 Color: 9180
Size: 282460 Color: 3002
Size: 270240 Color: 2139

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 447355 Color: 9181
Size: 289747 Color: 3455
Size: 262899 Color: 1513

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 447408 Color: 9182
Size: 282648 Color: 3018
Size: 269945 Color: 2101

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 447469 Color: 9183
Size: 282590 Color: 3015
Size: 269942 Color: 2100

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 447520 Color: 9184
Size: 288523 Color: 3388
Size: 263958 Color: 1611

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 447580 Color: 9185
Size: 289740 Color: 3452
Size: 262681 Color: 1492

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 447586 Color: 9186
Size: 284067 Color: 3103
Size: 268348 Color: 1986

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 447591 Color: 9187
Size: 293233 Color: 3676
Size: 259177 Color: 1165

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 447654 Color: 9188
Size: 278361 Color: 2738
Size: 273986 Color: 2433

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 447685 Color: 9189
Size: 292455 Color: 3633
Size: 259861 Color: 1230

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 448026 Color: 9190
Size: 297558 Color: 3948
Size: 254417 Color: 634

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 448065 Color: 9191
Size: 285156 Color: 3167
Size: 266780 Color: 1869

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 448199 Color: 9192
Size: 286753 Color: 3277
Size: 265049 Color: 1707

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 448225 Color: 9193
Size: 286233 Color: 3231
Size: 265543 Color: 1744

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 448277 Color: 9194
Size: 291194 Color: 3540
Size: 260530 Color: 1296

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 448293 Color: 9195
Size: 296965 Color: 3919
Size: 254743 Color: 675

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 448318 Color: 9196
Size: 298692 Color: 4010
Size: 252991 Color: 455

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 448445 Color: 9197
Size: 291149 Color: 3536
Size: 260407 Color: 1282

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 448521 Color: 9198
Size: 281985 Color: 2974
Size: 269495 Color: 2070

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 448650 Color: 9199
Size: 280685 Color: 2891
Size: 270666 Color: 2177

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 448673 Color: 9200
Size: 293390 Color: 3690
Size: 257938 Color: 999

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 448694 Color: 9201
Size: 299195 Color: 4047
Size: 252112 Color: 346

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 448755 Color: 9202
Size: 298799 Color: 4016
Size: 252447 Color: 388

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 448760 Color: 9203
Size: 278110 Color: 2717
Size: 273131 Color: 2362

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 448795 Color: 9204
Size: 284670 Color: 3141
Size: 266536 Color: 1843

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 448883 Color: 9205
Size: 295098 Color: 3800
Size: 256020 Color: 808

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 449045 Color: 9206
Size: 299867 Color: 4082
Size: 251089 Color: 206

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 449201 Color: 9207
Size: 290112 Color: 3483
Size: 260688 Color: 1313

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 449223 Color: 9208
Size: 277793 Color: 2703
Size: 272985 Color: 2353

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 449275 Color: 9209
Size: 294805 Color: 3778
Size: 255921 Color: 798

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 449288 Color: 9210
Size: 279457 Color: 2803
Size: 271256 Color: 2230

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 449345 Color: 9211
Size: 295267 Color: 3811
Size: 255389 Color: 747

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 449416 Color: 9212
Size: 288741 Color: 3400
Size: 261844 Color: 1410

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 449462 Color: 9213
Size: 290517 Color: 3506
Size: 260022 Color: 1242

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 449538 Color: 9214
Size: 296088 Color: 3864
Size: 254375 Color: 630

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 449600 Color: 9215
Size: 276505 Color: 2607
Size: 273896 Color: 2427

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 449614 Color: 9216
Size: 280306 Color: 2857
Size: 270081 Color: 2125

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 449650 Color: 9217
Size: 297865 Color: 3967
Size: 252486 Color: 396

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 449799 Color: 9218
Size: 276545 Color: 2609
Size: 273657 Color: 2399

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 449812 Color: 9219
Size: 291703 Color: 3570
Size: 258486 Color: 1076

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 449836 Color: 9220
Size: 282274 Color: 2987
Size: 267891 Color: 1946

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 449856 Color: 9221
Size: 298023 Color: 3974
Size: 252122 Color: 349

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 449940 Color: 9222
Size: 293447 Color: 3693
Size: 256614 Color: 877

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 450162 Color: 9223
Size: 294866 Color: 3786
Size: 254973 Color: 702

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 450225 Color: 9224
Size: 281218 Color: 2928
Size: 268558 Color: 2000

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 450316 Color: 9225
Size: 279095 Color: 2780
Size: 270590 Color: 2165

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 450360 Color: 9226
Size: 297400 Color: 3944
Size: 252241 Color: 365

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 450397 Color: 9227
Size: 276474 Color: 2605
Size: 273130 Color: 2361

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 450405 Color: 9228
Size: 275238 Color: 2521
Size: 274358 Color: 2463

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 450458 Color: 9229
Size: 278290 Color: 2732
Size: 271253 Color: 2229

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 450469 Color: 9230
Size: 297487 Color: 3945
Size: 252045 Color: 339

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 450513 Color: 9231
Size: 277541 Color: 2684
Size: 271947 Color: 2282

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 450541 Color: 9232
Size: 283167 Color: 3051
Size: 266293 Color: 1825

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 450563 Color: 9233
Size: 292548 Color: 3636
Size: 256890 Color: 899

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 450613 Color: 9234
Size: 286736 Color: 3274
Size: 262652 Color: 1486

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 450806 Color: 9235
Size: 288603 Color: 3391
Size: 260592 Color: 1304

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 450841 Color: 9236
Size: 290046 Color: 3478
Size: 259114 Color: 1157

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 450895 Color: 9237
Size: 276412 Color: 2603
Size: 272694 Color: 2331

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 450898 Color: 9238
Size: 294590 Color: 3767
Size: 254513 Color: 644

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 450928 Color: 9239
Size: 285338 Color: 3180
Size: 263735 Color: 1591

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 450937 Color: 9240
Size: 283543 Color: 3071
Size: 265521 Color: 1742

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 450973 Color: 9241
Size: 285542 Color: 3195
Size: 263486 Color: 1570

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 451002 Color: 9242
Size: 290818 Color: 3518
Size: 258181 Color: 1031

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 451157 Color: 9243
Size: 295242 Color: 3810
Size: 253602 Color: 541

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 451184 Color: 9244
Size: 287752 Color: 3344
Size: 261065 Color: 1355

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 451233 Color: 9245
Size: 291257 Color: 3545
Size: 257511 Color: 959

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 451245 Color: 9246
Size: 295689 Color: 3836
Size: 253067 Color: 471

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 451326 Color: 9247
Size: 277511 Color: 2682
Size: 271164 Color: 2222

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 451418 Color: 9248
Size: 291179 Color: 3539
Size: 257404 Color: 952

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 451430 Color: 9249
Size: 292102 Color: 3602
Size: 256469 Color: 857

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 451481 Color: 9250
Size: 290344 Color: 3494
Size: 258176 Color: 1029

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 451574 Color: 9251
Size: 282667 Color: 3020
Size: 265760 Color: 1767

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 451598 Color: 9252
Size: 280066 Color: 2843
Size: 268337 Color: 1985

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 451682 Color: 9253
Size: 280922 Color: 2901
Size: 267397 Color: 1907

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 451688 Color: 9254
Size: 288708 Color: 3398
Size: 259605 Color: 1206

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 451714 Color: 9255
Size: 296171 Color: 3873
Size: 252116 Color: 347

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 451733 Color: 9256
Size: 279419 Color: 2801
Size: 268849 Color: 2025

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 451797 Color: 9257
Size: 283098 Color: 3047
Size: 265106 Color: 1713

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 451893 Color: 9258
Size: 289187 Color: 3423
Size: 258921 Color: 1126

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 451910 Color: 9259
Size: 285417 Color: 3188
Size: 262674 Color: 1490

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 451914 Color: 9260
Size: 274267 Color: 2455
Size: 273820 Color: 2416

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 452031 Color: 9261
Size: 289188 Color: 3424
Size: 258782 Color: 1107

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 452243 Color: 9262
Size: 275357 Color: 2533
Size: 272401 Color: 2310

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 452318 Color: 9263
Size: 286646 Color: 3267
Size: 261037 Color: 1351

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 452322 Color: 9264
Size: 281399 Color: 2941
Size: 266280 Color: 1824

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 452335 Color: 9265
Size: 289991 Color: 3472
Size: 257675 Color: 978

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 452359 Color: 9266
Size: 295468 Color: 3821
Size: 252174 Color: 358

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 452407 Color: 9267
Size: 276966 Color: 2641
Size: 270628 Color: 2170

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 452529 Color: 9268
Size: 285998 Color: 3219
Size: 261474 Color: 1387

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 452561 Color: 9269
Size: 292571 Color: 3638
Size: 254869 Color: 692

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 452642 Color: 9270
Size: 273780 Color: 2410
Size: 273579 Color: 2392

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 452647 Color: 9271
Size: 282667 Color: 3021
Size: 264687 Color: 1675

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 452666 Color: 9272
Size: 292615 Color: 3643
Size: 254720 Color: 670

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 452723 Color: 9273
Size: 291243 Color: 3544
Size: 256035 Color: 811

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 452864 Color: 9274
Size: 286843 Color: 3289
Size: 260294 Color: 1269

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 452897 Color: 9275
Size: 280156 Color: 2849
Size: 266948 Color: 1882

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 452933 Color: 9276
Size: 280945 Color: 2904
Size: 266123 Color: 1806

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 452934 Color: 9277
Size: 284040 Color: 3102
Size: 263027 Color: 1527

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 452980 Color: 9278
Size: 287039 Color: 3300
Size: 259982 Color: 1236

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 452984 Color: 9279
Size: 282498 Color: 3007
Size: 264519 Color: 1660

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 453160 Color: 9280
Size: 281137 Color: 2921
Size: 265704 Color: 1762

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 453280 Color: 9281
Size: 287220 Color: 3310
Size: 259501 Color: 1193

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 453316 Color: 9282
Size: 290190 Color: 3487
Size: 256495 Color: 860

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 453371 Color: 9283
Size: 291787 Color: 3579
Size: 254843 Color: 684

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 453386 Color: 9284
Size: 280458 Color: 2871
Size: 266157 Color: 1812

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 453413 Color: 9285
Size: 286972 Color: 3297
Size: 259616 Color: 1207

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 453417 Color: 9286
Size: 276081 Color: 2587
Size: 270503 Color: 2156

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 453597 Color: 9287
Size: 281166 Color: 2925
Size: 265238 Color: 1726

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 453627 Color: 9288
Size: 287176 Color: 3307
Size: 259198 Color: 1168

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 454022 Color: 9289
Size: 285339 Color: 3181
Size: 260640 Color: 1308

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 454155 Color: 9290
Size: 285127 Color: 3166
Size: 260719 Color: 1318

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 454189 Color: 9291
Size: 291333 Color: 3552
Size: 254479 Color: 641

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 454191 Color: 9292
Size: 282703 Color: 3023
Size: 263107 Color: 1537

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 454246 Color: 9293
Size: 273672 Color: 2401
Size: 272083 Color: 2290

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 454281 Color: 9294
Size: 293533 Color: 3705
Size: 252187 Color: 359

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 454282 Color: 9295
Size: 284582 Color: 3136
Size: 261137 Color: 1359

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 454283 Color: 9296
Size: 293557 Color: 3708
Size: 252161 Color: 355

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 454307 Color: 9297
Size: 294209 Color: 3747
Size: 251485 Color: 265

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 454328 Color: 9298
Size: 275602 Color: 2556
Size: 270071 Color: 2123

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 454358 Color: 9299
Size: 292379 Color: 3627
Size: 253264 Color: 504

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 454459 Color: 9300
Size: 278043 Color: 2713
Size: 267499 Color: 1918

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 454478 Color: 9301
Size: 284827 Color: 3149
Size: 260696 Color: 1314

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 454514 Color: 9302
Size: 275283 Color: 2526
Size: 270204 Color: 2136

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 454583 Color: 9303
Size: 275247 Color: 2522
Size: 270171 Color: 2135

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 454585 Color: 9304
Size: 279512 Color: 2807
Size: 265904 Color: 1778

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 454698 Color: 9305
Size: 281063 Color: 2915
Size: 264240 Color: 1634

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 454721 Color: 9306
Size: 294160 Color: 3745
Size: 251120 Color: 211

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 454754 Color: 9307
Size: 276650 Color: 2617
Size: 268597 Color: 2006

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 454773 Color: 9308
Size: 291629 Color: 3566
Size: 253599 Color: 540

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 454836 Color: 9309
Size: 290512 Color: 3505
Size: 254653 Color: 659

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 454875 Color: 9310
Size: 294957 Color: 3792
Size: 250169 Color: 38

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 454983 Color: 9311
Size: 286504 Color: 3254
Size: 258514 Color: 1082

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 455195 Color: 9312
Size: 278534 Color: 2747
Size: 266272 Color: 1823

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 455219 Color: 9313
Size: 282739 Color: 3025
Size: 262043 Color: 1428

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 455232 Color: 9314
Size: 291395 Color: 3555
Size: 253374 Color: 515

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 455254 Color: 9315
Size: 279197 Color: 2788
Size: 265550 Color: 1746

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 455333 Color: 9316
Size: 287994 Color: 3356
Size: 256674 Color: 887

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 455334 Color: 9317
Size: 291612 Color: 3565
Size: 253055 Color: 469

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 455398 Color: 9318
Size: 286176 Color: 3228
Size: 258427 Color: 1068

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 455407 Color: 9319
Size: 289862 Color: 3464
Size: 254732 Color: 672

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 455434 Color: 9320
Size: 275414 Color: 2538
Size: 269153 Color: 2043

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 455446 Color: 9321
Size: 274597 Color: 2481
Size: 269958 Color: 2104

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 455469 Color: 9322
Size: 274277 Color: 2456
Size: 270255 Color: 2140

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 455601 Color: 9323
Size: 278387 Color: 2740
Size: 266013 Color: 1791

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 455654 Color: 9324
Size: 279809 Color: 2821
Size: 264538 Color: 1663

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 455697 Color: 9325
Size: 278274 Color: 2730
Size: 266030 Color: 1792

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 455743 Color: 9326
Size: 278547 Color: 2750
Size: 265711 Color: 1763

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 455856 Color: 9327
Size: 283945 Color: 3094
Size: 260200 Color: 1261

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 455926 Color: 9328
Size: 284818 Color: 3147
Size: 259257 Color: 1175

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 456045 Color: 9329
Size: 278542 Color: 2749
Size: 265414 Color: 1737

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 456152 Color: 9330
Size: 286536 Color: 3256
Size: 257313 Color: 938

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 456155 Color: 9331
Size: 290254 Color: 3490
Size: 253592 Color: 538

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 456166 Color: 9332
Size: 281481 Color: 2948
Size: 262354 Color: 1456

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 456192 Color: 9333
Size: 283400 Color: 3061
Size: 260409 Color: 1283

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 456261 Color: 9334
Size: 281002 Color: 2911
Size: 262738 Color: 1499

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 456275 Color: 9335
Size: 284748 Color: 3145
Size: 258978 Color: 1137

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 456330 Color: 9336
Size: 289796 Color: 3458
Size: 253875 Color: 585

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 456363 Color: 9337
Size: 274710 Color: 2488
Size: 268928 Color: 2028

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 456386 Color: 9338
Size: 290664 Color: 3509
Size: 252951 Color: 448

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 456403 Color: 9339
Size: 292232 Color: 3609
Size: 251366 Color: 250

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 456434 Color: 9340
Size: 289326 Color: 3430
Size: 254241 Color: 622

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 456462 Color: 9341
Size: 284850 Color: 3152
Size: 258689 Color: 1098

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 456469 Color: 9342
Size: 291473 Color: 3557
Size: 252059 Color: 340

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 456622 Color: 9343
Size: 291948 Color: 3592
Size: 251431 Color: 258

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 456697 Color: 9344
Size: 291778 Color: 3577
Size: 251526 Color: 269

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 456728 Color: 9345
Size: 291837 Color: 3585
Size: 251436 Color: 259

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 456742 Color: 9346
Size: 273383 Color: 2377
Size: 269876 Color: 2098

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 456774 Color: 9347
Size: 282909 Color: 3034
Size: 260318 Color: 1274

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 456832 Color: 9348
Size: 292265 Color: 3613
Size: 250904 Color: 171

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 456866 Color: 9349
Size: 284086 Color: 3107
Size: 259049 Color: 1145

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 456972 Color: 9350
Size: 282111 Color: 2980
Size: 260918 Color: 1335

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 456983 Color: 9351
Size: 281230 Color: 2930
Size: 261788 Color: 1408

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 457029 Color: 9352
Size: 285931 Color: 3214
Size: 257041 Color: 916

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 457037 Color: 9353
Size: 292851 Color: 3652
Size: 250113 Color: 26

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 457083 Color: 9354
Size: 283873 Color: 3091
Size: 259045 Color: 1144

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 457124 Color: 9355
Size: 276261 Color: 2597
Size: 266616 Color: 1853

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 457175 Color: 9356
Size: 272742 Color: 2334
Size: 270084 Color: 2126

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 457191 Color: 9357
Size: 273086 Color: 2359
Size: 269724 Color: 2083

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 457281 Color: 9358
Size: 273652 Color: 2398
Size: 269068 Color: 2036

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 457387 Color: 9359
Size: 280612 Color: 2885
Size: 262002 Color: 1425

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 457401 Color: 9360
Size: 283712 Color: 3078
Size: 258888 Color: 1121

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 457459 Color: 9361
Size: 288350 Color: 3378
Size: 254192 Color: 616

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 457544 Color: 9362
Size: 288739 Color: 3399
Size: 253718 Color: 554

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 457616 Color: 9363
Size: 286146 Color: 3224
Size: 256239 Color: 835

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 457680 Color: 9364
Size: 286688 Color: 3271
Size: 255633 Color: 771

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 457708 Color: 9365
Size: 277416 Color: 2678
Size: 264877 Color: 1689

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 457758 Color: 9366
Size: 287578 Color: 3335
Size: 254665 Color: 662

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 457804 Color: 9367
Size: 278656 Color: 2756
Size: 263541 Color: 1577

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 457827 Color: 9368
Size: 281616 Color: 2956
Size: 260558 Color: 1299

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 457875 Color: 9369
Size: 271702 Color: 2265
Size: 270424 Color: 2150

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 457971 Color: 9370
Size: 279903 Color: 2830
Size: 262127 Color: 1438

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 458111 Color: 9371
Size: 288321 Color: 3376
Size: 253569 Color: 537

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 458208 Color: 9372
Size: 272020 Color: 2285
Size: 269773 Color: 2088

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 458373 Color: 9373
Size: 284297 Color: 3120
Size: 257331 Color: 942

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 458641 Color: 9374
Size: 271309 Color: 2234
Size: 270051 Color: 2121

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 458727 Color: 9375
Size: 282802 Color: 3026
Size: 258472 Color: 1074

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 458769 Color: 9376
Size: 288241 Color: 3370
Size: 252991 Color: 456

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 458809 Color: 9377
Size: 286239 Color: 3232
Size: 254953 Color: 699

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 458927 Color: 9378
Size: 271711 Color: 2266
Size: 269363 Color: 2061

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 459022 Color: 9379
Size: 276810 Color: 2627
Size: 264169 Color: 1625

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 459463 Color: 9380
Size: 285703 Color: 3201
Size: 254835 Color: 682

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 459519 Color: 9381
Size: 286127 Color: 3223
Size: 254355 Color: 627

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 459887 Color: 9382
Size: 288844 Color: 3406
Size: 251270 Color: 237

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 459912 Color: 9383
Size: 288170 Color: 3367
Size: 251919 Color: 320

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 460101 Color: 9384
Size: 283618 Color: 3076
Size: 256282 Color: 839

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 460128 Color: 9385
Size: 275130 Color: 2513
Size: 264743 Color: 1678

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 460174 Color: 9386
Size: 275639 Color: 2557
Size: 264188 Color: 1629

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 460232 Color: 9387
Size: 276536 Color: 2608
Size: 263233 Color: 1550

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 460277 Color: 9388
Size: 279022 Color: 2775
Size: 260702 Color: 1315

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 460295 Color: 9389
Size: 282491 Color: 3006
Size: 257215 Color: 928

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 460333 Color: 9390
Size: 283886 Color: 3092
Size: 255782 Color: 784

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 460411 Color: 9391
Size: 281505 Color: 2949
Size: 258085 Color: 1016

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 460450 Color: 9392
Size: 284623 Color: 3139
Size: 254928 Color: 695

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 460481 Color: 9393
Size: 285698 Color: 3200
Size: 253822 Color: 574

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 460597 Color: 9394
Size: 280318 Color: 2861
Size: 259086 Color: 1151

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 460615 Color: 9395
Size: 278497 Color: 2746
Size: 260889 Color: 1333

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 460624 Color: 9396
Size: 282326 Color: 2994
Size: 257051 Color: 917

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 460658 Color: 9397
Size: 274340 Color: 2459
Size: 265003 Color: 1700

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 460698 Color: 9398
Size: 282413 Color: 2999
Size: 256890 Color: 900

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 460721 Color: 9399
Size: 273635 Color: 2397
Size: 265645 Color: 1757

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 460850 Color: 9400
Size: 272343 Color: 2309
Size: 266808 Color: 1872

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 460857 Color: 9401
Size: 276096 Color: 2588
Size: 263048 Color: 1531

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 460864 Color: 9402
Size: 286360 Color: 3244
Size: 252777 Color: 426

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 460881 Color: 9403
Size: 288502 Color: 3386
Size: 250618 Color: 128

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 460986 Color: 9404
Size: 275460 Color: 2545
Size: 263555 Color: 1579

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 461065 Color: 9405
Size: 281026 Color: 2914
Size: 257910 Color: 997

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 461079 Color: 9406
Size: 288548 Color: 3389
Size: 250374 Color: 88

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 461155 Color: 9407
Size: 273900 Color: 2428
Size: 264946 Color: 1695

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 461202 Color: 9408
Size: 284984 Color: 3158
Size: 253815 Color: 572

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 461265 Color: 9409
Size: 272651 Color: 2327
Size: 266085 Color: 1802

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 461293 Color: 9410
Size: 274298 Color: 2457
Size: 264410 Color: 1655

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 461391 Color: 9411
Size: 283766 Color: 3083
Size: 254844 Color: 685

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 461418 Color: 9412
Size: 274766 Color: 2491
Size: 263817 Color: 1598

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 461475 Color: 9413
Size: 286554 Color: 3258
Size: 251972 Color: 329

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 461544 Color: 9414
Size: 285793 Color: 3205
Size: 252664 Color: 416

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 461601 Color: 9415
Size: 284006 Color: 3099
Size: 254394 Color: 632

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 461741 Color: 9416
Size: 279853 Color: 2824
Size: 258407 Color: 1064

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 461745 Color: 9417
Size: 277302 Color: 2671
Size: 260954 Color: 1341

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 461769 Color: 9418
Size: 277328 Color: 2674
Size: 260904 Color: 1334

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 461912 Color: 9419
Size: 275518 Color: 2549
Size: 262571 Color: 1479

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 461950 Color: 9420
Size: 275138 Color: 2515
Size: 262913 Color: 1514

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 461958 Color: 9421
Size: 275341 Color: 2532
Size: 262702 Color: 1495

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 462036 Color: 9422
Size: 280730 Color: 2893
Size: 257235 Color: 933

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 462145 Color: 9423
Size: 269752 Color: 2085
Size: 268104 Color: 1961

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 462147 Color: 9424
Size: 278537 Color: 2748
Size: 259317 Color: 1179

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 462194 Color: 9425
Size: 281839 Color: 2968
Size: 255968 Color: 804

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 462251 Color: 9426
Size: 281078 Color: 2916
Size: 256672 Color: 886

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 462262 Color: 9427
Size: 283779 Color: 3084
Size: 253960 Color: 592

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 462355 Color: 9428
Size: 269075 Color: 2037
Size: 268571 Color: 2003

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 462383 Color: 9429
Size: 272850 Color: 2346
Size: 264768 Color: 1682

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 462438 Color: 9430
Size: 279867 Color: 2827
Size: 257696 Color: 981

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 462580 Color: 9431
Size: 278435 Color: 2741
Size: 258986 Color: 1139

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 462590 Color: 9432
Size: 284927 Color: 3155
Size: 252484 Color: 394

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 462676 Color: 9433
Size: 272162 Color: 2296
Size: 265163 Color: 1718

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 462719 Color: 9434
Size: 273886 Color: 2426
Size: 263396 Color: 1563

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 462721 Color: 9435
Size: 276904 Color: 2637
Size: 260376 Color: 1278

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 462817 Color: 9436
Size: 282810 Color: 3027
Size: 254374 Color: 629

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 462848 Color: 9437
Size: 278277 Color: 2731
Size: 258876 Color: 1119

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 462950 Color: 9438
Size: 276383 Color: 2601
Size: 260668 Color: 1312

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 462977 Color: 9439
Size: 284278 Color: 3118
Size: 252746 Color: 423

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 463110 Color: 9440
Size: 278076 Color: 2714
Size: 258815 Color: 1110

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 463165 Color: 9441
Size: 274144 Color: 2445
Size: 262692 Color: 1494

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 463196 Color: 9442
Size: 276675 Color: 2619
Size: 260130 Color: 1252

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 463219 Color: 9443
Size: 277731 Color: 2698
Size: 259051 Color: 1146

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 463236 Color: 9444
Size: 284108 Color: 3110
Size: 252657 Color: 414

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 463309 Color: 9445
Size: 272493 Color: 2315
Size: 264199 Color: 1631

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 463358 Color: 9446
Size: 283437 Color: 3064
Size: 253206 Color: 495

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 463426 Color: 9447
Size: 271343 Color: 2239
Size: 265232 Color: 1725

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 463536 Color: 9448
Size: 278378 Color: 2739
Size: 258087 Color: 1017

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 463555 Color: 9449
Size: 286024 Color: 3220
Size: 250422 Color: 93

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 463579 Color: 9450
Size: 282491 Color: 3005
Size: 253931 Color: 589

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 463629 Color: 9451
Size: 281669 Color: 2960
Size: 254703 Color: 665

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 463694 Color: 9452
Size: 283216 Color: 3053
Size: 253091 Color: 478

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 463914 Color: 9453
Size: 274203 Color: 2450
Size: 261884 Color: 1414

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 463973 Color: 9454
Size: 272537 Color: 2319
Size: 263491 Color: 1571

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 463990 Color: 9455
Size: 278922 Color: 2771
Size: 257089 Color: 919

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 464083 Color: 9456
Size: 271828 Color: 2278
Size: 264090 Color: 1618

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 464477 Color: 9457
Size: 279024 Color: 2776
Size: 256500 Color: 861

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 464533 Color: 9458
Size: 274910 Color: 2500
Size: 260558 Color: 1300

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 464632 Color: 9459
Size: 283016 Color: 3041
Size: 252353 Color: 375

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 464666 Color: 9460
Size: 280594 Color: 2883
Size: 254741 Color: 674

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 464708 Color: 9461
Size: 268588 Color: 2005
Size: 266705 Color: 1857

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 464728 Color: 9462
Size: 276928 Color: 2638
Size: 258345 Color: 1056

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 464757 Color: 9463
Size: 269335 Color: 2055
Size: 265909 Color: 1780

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 464768 Color: 9464
Size: 282557 Color: 3012
Size: 252676 Color: 417

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 464900 Color: 9465
Size: 270614 Color: 2168
Size: 264487 Color: 1659

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 464935 Color: 9466
Size: 277033 Color: 2649
Size: 258033 Color: 1009

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 464942 Color: 9467
Size: 271193 Color: 2225
Size: 263866 Color: 1604

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 464952 Color: 9468
Size: 283491 Color: 3067
Size: 251558 Color: 274

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 465107 Color: 9469
Size: 284068 Color: 3104
Size: 250826 Color: 160

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 465231 Color: 9470
Size: 278186 Color: 2723
Size: 256584 Color: 872

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 465320 Color: 9471
Size: 280646 Color: 2888
Size: 254035 Color: 602

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 465404 Color: 9472
Size: 275135 Color: 2514
Size: 259462 Color: 1189

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 465429 Color: 9473
Size: 284269 Color: 3116
Size: 250303 Color: 71

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 465440 Color: 9474
Size: 274099 Color: 2443
Size: 260462 Color: 1290

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 465639 Color: 9475
Size: 280622 Color: 2886
Size: 253740 Color: 556

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 465873 Color: 9476
Size: 282561 Color: 3013
Size: 251567 Color: 277

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 465895 Color: 9477
Size: 280539 Color: 2878
Size: 253567 Color: 536

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 465903 Color: 9478
Size: 268835 Color: 2023
Size: 265263 Color: 1729

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 466018 Color: 9479
Size: 272953 Color: 2351
Size: 261030 Color: 1349

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 466107 Color: 9480
Size: 274092 Color: 2441
Size: 259802 Color: 1226

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 466151 Color: 9481
Size: 273191 Color: 2369
Size: 260659 Color: 1310

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 466155 Color: 9482
Size: 268811 Color: 2020
Size: 265035 Color: 1706

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 466203 Color: 9483
Size: 281157 Color: 2924
Size: 252641 Color: 413

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 466208 Color: 9484
Size: 274079 Color: 2439
Size: 259714 Color: 1218

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 466334 Color: 9485
Size: 273901 Color: 2429
Size: 259766 Color: 1224

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 466468 Color: 9486
Size: 275023 Color: 2509
Size: 258510 Color: 1081

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 466507 Color: 9487
Size: 278889 Color: 2765
Size: 254605 Color: 653

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 466661 Color: 9488
Size: 281118 Color: 2919
Size: 252222 Color: 364

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 466679 Color: 9489
Size: 275271 Color: 2524
Size: 258051 Color: 1011

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 466690 Color: 9490
Size: 281647 Color: 2957
Size: 251664 Color: 289

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 466702 Color: 9491
Size: 282970 Color: 3039
Size: 250329 Color: 76

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 466845 Color: 9492
Size: 277242 Color: 2666
Size: 255914 Color: 796

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 466861 Color: 9493
Size: 272285 Color: 2306
Size: 260855 Color: 1329

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 467093 Color: 9494
Size: 277249 Color: 2668
Size: 255659 Color: 774

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 467323 Color: 9495
Size: 271419 Color: 2244
Size: 261259 Color: 1371

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 467337 Color: 9496
Size: 279912 Color: 2831
Size: 252752 Color: 424

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 467378 Color: 9497
Size: 271669 Color: 2261
Size: 260954 Color: 1340

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 467539 Color: 9498
Size: 274357 Color: 2462
Size: 258105 Color: 1020

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 467596 Color: 9499
Size: 279968 Color: 2837
Size: 252437 Color: 387

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 467669 Color: 9500
Size: 273764 Color: 2408
Size: 258568 Color: 1087

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 467710 Color: 9501
Size: 267332 Color: 1905
Size: 264959 Color: 1697

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 467744 Color: 9502
Size: 268649 Color: 2008
Size: 263608 Color: 1581

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 467767 Color: 9503
Size: 274156 Color: 2447
Size: 258078 Color: 1014

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 467799 Color: 9504
Size: 280207 Color: 2855
Size: 251995 Color: 334

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 467803 Color: 9505
Size: 280897 Color: 2898
Size: 251301 Color: 241

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 467843 Color: 9506
Size: 267125 Color: 1896
Size: 265033 Color: 1704

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 467949 Color: 9507
Size: 279131 Color: 2783
Size: 252921 Color: 444

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 467964 Color: 9508
Size: 276416 Color: 2604
Size: 255621 Color: 768

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 468051 Color: 9509
Size: 269452 Color: 2068
Size: 262498 Color: 1473

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 468067 Color: 9510
Size: 266039 Color: 1793
Size: 265895 Color: 1777

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 468090 Color: 9511
Size: 271684 Color: 2263
Size: 260227 Color: 1262

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 468107 Color: 9512
Size: 280340 Color: 2863
Size: 251554 Color: 272

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 468111 Color: 9513
Size: 281118 Color: 2918
Size: 250772 Color: 149

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 468137 Color: 9514
Size: 277001 Color: 2644
Size: 254863 Color: 690

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 468152 Color: 9515
Size: 279387 Color: 2798
Size: 252462 Color: 390

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 468271 Color: 9516
Size: 275755 Color: 2566
Size: 255975 Color: 805

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 468359 Color: 9517
Size: 273164 Color: 2365
Size: 258478 Color: 1075

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 468383 Color: 9518
Size: 268309 Color: 1980
Size: 263309 Color: 1555

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 468434 Color: 9519
Size: 278022 Color: 2710
Size: 253545 Color: 534

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 468570 Color: 9520
Size: 270462 Color: 2153
Size: 260969 Color: 1343

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 468574 Color: 9521
Size: 273844 Color: 2420
Size: 257583 Color: 968

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 468663 Color: 9522
Size: 267920 Color: 1948
Size: 263418 Color: 1564

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 468735 Color: 9523
Size: 266974 Color: 1883
Size: 264292 Color: 1640

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 468863 Color: 9524
Size: 272440 Color: 2312
Size: 258698 Color: 1099

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 468918 Color: 9525
Size: 271550 Color: 2253
Size: 259533 Color: 1195

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 469011 Color: 9526
Size: 277623 Color: 2690
Size: 253367 Color: 513

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 469083 Color: 9527
Size: 272084 Color: 2291
Size: 258834 Color: 1113

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 469165 Color: 9528
Size: 279302 Color: 2794
Size: 251534 Color: 270

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 469267 Color: 9529
Size: 277445 Color: 2679
Size: 253289 Color: 509

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 469280 Color: 9530
Size: 269571 Color: 2075
Size: 261150 Color: 1360

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 469290 Color: 9531
Size: 277454 Color: 2680
Size: 253257 Color: 502

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 469362 Color: 9532
Size: 265876 Color: 1776
Size: 264763 Color: 1681

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 469402 Color: 9533
Size: 270634 Color: 2171
Size: 259965 Color: 1234

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 469424 Color: 9534
Size: 274096 Color: 2442
Size: 256481 Color: 858

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 469427 Color: 9535
Size: 267406 Color: 1909
Size: 263168 Color: 1545

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 469452 Color: 9536
Size: 280204 Color: 2853
Size: 250345 Color: 82

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 469464 Color: 9537
Size: 269201 Color: 2048
Size: 261336 Color: 1378

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 469646 Color: 9538
Size: 270222 Color: 2137
Size: 260133 Color: 1253

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 469669 Color: 9539
Size: 268383 Color: 1991
Size: 261949 Color: 1422

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 469701 Color: 9540
Size: 272044 Color: 2287
Size: 258256 Color: 1043

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 469792 Color: 9541
Size: 272575 Color: 2321
Size: 257634 Color: 973

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 469794 Color: 9542
Size: 271212 Color: 2226
Size: 258995 Color: 1140

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 469798 Color: 9543
Size: 271990 Color: 2284
Size: 258213 Color: 1039

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 469811 Color: 9544
Size: 274586 Color: 2480
Size: 255604 Color: 766

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 469964 Color: 9545
Size: 270330 Color: 2145
Size: 259707 Color: 1214

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 469971 Color: 9546
Size: 272207 Color: 2299
Size: 257823 Color: 991

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 470131 Color: 9547
Size: 273502 Color: 2387
Size: 256368 Color: 848

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 470141 Color: 9548
Size: 278620 Color: 2754
Size: 251240 Color: 227

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 470204 Color: 9549
Size: 266098 Color: 1805
Size: 263699 Color: 1589

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 470310 Color: 9550
Size: 266053 Color: 1798
Size: 263638 Color: 1584

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 470374 Color: 9551
Size: 271136 Color: 2221
Size: 258491 Color: 1079

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 470407 Color: 9552
Size: 272673 Color: 2329
Size: 256921 Color: 903

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 470444 Color: 9553
Size: 267272 Color: 1903
Size: 262285 Color: 1451

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 470494 Color: 9554
Size: 270792 Color: 2185
Size: 258715 Color: 1100

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 470533 Color: 9555
Size: 265633 Color: 1754
Size: 263835 Color: 1601

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 470545 Color: 9556
Size: 276017 Color: 2580
Size: 253439 Color: 521

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 470573 Color: 9557
Size: 275765 Color: 2567
Size: 253663 Color: 549

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 470606 Color: 9558
Size: 265524 Color: 1743
Size: 263871 Color: 1605

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 470673 Color: 9559
Size: 270969 Color: 2201
Size: 258359 Color: 1061

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 470678 Color: 9560
Size: 273466 Color: 2384
Size: 255857 Color: 789

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 470710 Color: 9561
Size: 277658 Color: 2691
Size: 251633 Color: 284

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 471056 Color: 9562
Size: 275469 Color: 2546
Size: 253476 Color: 524

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 471177 Color: 9563
Size: 264633 Color: 1671
Size: 264191 Color: 1630

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 471177 Color: 9564
Size: 267601 Color: 1926
Size: 261223 Color: 1368

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 471182 Color: 9565
Size: 274041 Color: 2437
Size: 254778 Color: 678

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 471247 Color: 9566
Size: 278749 Color: 2760
Size: 250005 Color: 1

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 471270 Color: 9567
Size: 269875 Color: 2097
Size: 258856 Color: 1116

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 471322 Color: 9568
Size: 274625 Color: 2483
Size: 254054 Color: 603

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 471337 Color: 9569
Size: 270131 Color: 2131
Size: 258533 Color: 1084

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 471478 Color: 9570
Size: 271874 Color: 2280
Size: 256649 Color: 882

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 471642 Color: 9571
Size: 268039 Color: 1956
Size: 260320 Color: 1275

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 471662 Color: 9572
Size: 274400 Color: 2468
Size: 253939 Color: 590

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 471886 Color: 9573
Size: 267696 Color: 1933
Size: 260419 Color: 1286

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 471900 Color: 9574
Size: 266598 Color: 1852
Size: 261503 Color: 1389

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 471950 Color: 9575
Size: 270819 Color: 2187
Size: 257232 Color: 932

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 471957 Color: 9576
Size: 269785 Color: 2089
Size: 258259 Color: 1045

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 472042 Color: 9577
Size: 266802 Color: 1871
Size: 261157 Color: 1361

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 472063 Color: 9578
Size: 268314 Color: 1982
Size: 259624 Color: 1208

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 472087 Color: 9579
Size: 267423 Color: 1911
Size: 260491 Color: 1293

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 472091 Color: 9580
Size: 274553 Color: 2473
Size: 253357 Color: 512

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 472136 Color: 9581
Size: 270540 Color: 2161
Size: 257325 Color: 940

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 472165 Color: 9582
Size: 264083 Color: 1617
Size: 263753 Color: 1594

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 472189 Color: 9583
Size: 277232 Color: 2664
Size: 250580 Color: 124

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 472229 Color: 9584
Size: 276824 Color: 2630
Size: 250948 Color: 181

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 472244 Color: 9585
Size: 270876 Color: 2193
Size: 256881 Color: 898

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 472325 Color: 9586
Size: 265627 Color: 1753
Size: 262049 Color: 1430

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 472404 Color: 9587
Size: 273679 Color: 2402
Size: 253918 Color: 587

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 472482 Color: 9588
Size: 275755 Color: 2565
Size: 251764 Color: 299

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 472569 Color: 9589
Size: 275036 Color: 2510
Size: 252396 Color: 380

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 472620 Color: 9590
Size: 269086 Color: 2038
Size: 258295 Color: 1049

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 472697 Color: 9591
Size: 276037 Color: 2583
Size: 251267 Color: 235

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 472714 Color: 9592
Size: 276264 Color: 2598
Size: 251023 Color: 195

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 472717 Color: 9593
Size: 268308 Color: 1979
Size: 258976 Color: 1136

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 472749 Color: 9594
Size: 264173 Color: 1626
Size: 263079 Color: 1533

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 472751 Color: 9595
Size: 273184 Color: 2367
Size: 254066 Color: 605

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 472885 Color: 9596
Size: 274696 Color: 2487
Size: 252420 Color: 384

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 472886 Color: 9597
Size: 271529 Color: 2250
Size: 255586 Color: 764

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 472982 Color: 9598
Size: 271667 Color: 2259
Size: 255352 Color: 742

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 473053 Color: 9599
Size: 271043 Color: 2211
Size: 255905 Color: 794

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 473065 Color: 9600
Size: 266874 Color: 1878
Size: 260062 Color: 1245

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 473153 Color: 9601
Size: 274971 Color: 2507
Size: 251877 Color: 313

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 473154 Color: 9602
Size: 268359 Color: 1988
Size: 258488 Color: 1078

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 473160 Color: 9603
Size: 275561 Color: 2552
Size: 251280 Color: 240

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 473247 Color: 9604
Size: 269959 Color: 2105
Size: 256795 Color: 894

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 473289 Color: 9605
Size: 272879 Color: 2347
Size: 253833 Color: 577

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 473396 Color: 9606
Size: 271037 Color: 2210
Size: 255568 Color: 760

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 473421 Color: 9607
Size: 265644 Color: 1756
Size: 260936 Color: 1337

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 473663 Color: 9608
Size: 263231 Color: 1549
Size: 263107 Color: 1538

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 473725 Color: 9609
Size: 263628 Color: 1583
Size: 262648 Color: 1485

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 473824 Color: 9610
Size: 273146 Color: 2363
Size: 253031 Color: 463

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 473828 Color: 9611
Size: 273169 Color: 2366
Size: 253004 Color: 460

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 474020 Color: 9612
Size: 273597 Color: 2394
Size: 252384 Color: 377

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 474023 Color: 9613
Size: 269346 Color: 2057
Size: 256632 Color: 880

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 474027 Color: 9614
Size: 271107 Color: 2218
Size: 254867 Color: 691

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 474224 Color: 9615
Size: 269998 Color: 2110
Size: 255779 Color: 782

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 474238 Color: 9616
Size: 275310 Color: 2528
Size: 250453 Color: 99

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 474293 Color: 9617
Size: 266710 Color: 1859
Size: 258998 Color: 1141

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 474445 Color: 9618
Size: 268223 Color: 1973
Size: 257333 Color: 944

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 474519 Color: 9619
Size: 267952 Color: 1951
Size: 257530 Color: 962

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 474761 Color: 9620
Size: 269010 Color: 2033
Size: 256230 Color: 833

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 474770 Color: 9621
Size: 272717 Color: 2333
Size: 252514 Color: 400

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 474793 Color: 9622
Size: 272503 Color: 2316
Size: 252705 Color: 419

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 474845 Color: 9623
Size: 275115 Color: 2512
Size: 250041 Color: 10

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 474938 Color: 9624
Size: 262915 Color: 1515
Size: 262148 Color: 1439

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 475120 Color: 9625
Size: 265663 Color: 1759
Size: 259218 Color: 1171

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 475274 Color: 9626
Size: 273253 Color: 2373
Size: 251474 Color: 264

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 475322 Color: 9627
Size: 262968 Color: 1521
Size: 261711 Color: 1402

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 475373 Color: 9628
Size: 262503 Color: 1474
Size: 262125 Color: 1437

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 475408 Color: 9629
Size: 273784 Color: 2412
Size: 250809 Color: 156

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 475438 Color: 9630
Size: 264560 Color: 1664
Size: 260003 Color: 1241

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 475599 Color: 9631
Size: 269422 Color: 2066
Size: 254980 Color: 703

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 475646 Color: 9632
Size: 271179 Color: 2223
Size: 253176 Color: 491

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 475812 Color: 9633
Size: 270439 Color: 2151
Size: 253750 Color: 558

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 475940 Color: 9634
Size: 270394 Color: 2148
Size: 253667 Color: 550

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 476016 Color: 9635
Size: 267769 Color: 1937
Size: 256216 Color: 827

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 476025 Color: 9636
Size: 264184 Color: 1628
Size: 259792 Color: 1225

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 476035 Color: 9637
Size: 270041 Color: 2120
Size: 253925 Color: 588

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 476039 Color: 9638
Size: 269566 Color: 2074
Size: 254396 Color: 633

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 476046 Color: 9639
Size: 270466 Color: 2154
Size: 253489 Color: 526

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 476087 Color: 9640
Size: 268925 Color: 2027
Size: 254989 Color: 708

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 476105 Color: 9641
Size: 266982 Color: 1884
Size: 256914 Color: 902

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 476125 Color: 9642
Size: 267495 Color: 1917
Size: 256381 Color: 849

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 476235 Color: 9643
Size: 265193 Color: 1721
Size: 258573 Color: 1088

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 476243 Color: 9644
Size: 264822 Color: 1683
Size: 258936 Color: 1128

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 476336 Color: 9645
Size: 271807 Color: 2276
Size: 251858 Color: 310

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 476466 Color: 9646
Size: 267213 Color: 1900
Size: 256322 Color: 841

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 476479 Color: 9647
Size: 266001 Color: 1790
Size: 257521 Color: 961

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 476561 Color: 9648
Size: 266434 Color: 1836
Size: 257006 Color: 912

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 476623 Color: 9649
Size: 270823 Color: 2188
Size: 252555 Color: 404

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 476646 Color: 9650
Size: 263191 Color: 1546
Size: 260164 Color: 1257

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 476653 Color: 9651
Size: 271902 Color: 2281
Size: 251446 Color: 261

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 476668 Color: 9652
Size: 264375 Color: 1652
Size: 258958 Color: 1132

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 476725 Color: 9653
Size: 268350 Color: 1987
Size: 254926 Color: 693

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 476740 Color: 9654
Size: 273039 Color: 2356
Size: 250222 Color: 51

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 476763 Color: 9655
Size: 267635 Color: 1930
Size: 255603 Color: 765

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 476835 Color: 9656
Size: 271353 Color: 2242
Size: 251813 Color: 305

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 476988 Color: 9657
Size: 272818 Color: 2343
Size: 250195 Color: 45

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 477005 Color: 9658
Size: 262817 Color: 1505
Size: 260179 Color: 1259

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 477085 Color: 9659
Size: 270616 Color: 2169
Size: 252300 Color: 372

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 477121 Color: 9660
Size: 262600 Color: 1481
Size: 260280 Color: 1268

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 477157 Color: 9661
Size: 264717 Color: 1676
Size: 258127 Color: 1023

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 477182 Color: 9662
Size: 266905 Color: 1881
Size: 255914 Color: 795

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 477493 Color: 9663
Size: 272101 Color: 2292
Size: 250407 Color: 92

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 477525 Color: 9664
Size: 268767 Color: 2016
Size: 253709 Color: 553

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 477583 Color: 9665
Size: 263623 Color: 1582
Size: 258795 Color: 1109

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 477584 Color: 9666
Size: 263800 Color: 1596
Size: 258617 Color: 1090

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 477585 Color: 9667
Size: 266449 Color: 1837
Size: 255967 Color: 803

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 477598 Color: 9668
Size: 263259 Color: 1551
Size: 259144 Color: 1163

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 477688 Color: 9669
Size: 264373 Color: 1651
Size: 257940 Color: 1001

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 477753 Color: 9670
Size: 268814 Color: 2021
Size: 253434 Color: 520

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 477811 Color: 9671
Size: 271827 Color: 2277
Size: 250363 Color: 84

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 477837 Color: 9672
Size: 264298 Color: 1642
Size: 257866 Color: 995

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 477861 Color: 9673
Size: 269359 Color: 2060
Size: 252781 Color: 427

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 477883 Color: 9674
Size: 266641 Color: 1854
Size: 255477 Color: 752

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 477959 Color: 9675
Size: 269054 Color: 2035
Size: 252988 Color: 454

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 477980 Color: 9676
Size: 270938 Color: 2198
Size: 251083 Color: 205

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 478054 Color: 9677
Size: 269828 Color: 2093
Size: 252119 Color: 348

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 478134 Color: 9678
Size: 271333 Color: 2238
Size: 250534 Color: 115

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 478158 Color: 9679
Size: 270326 Color: 2144
Size: 251517 Color: 268

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 478280 Color: 9680
Size: 266735 Color: 1862
Size: 254986 Color: 707

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 478305 Color: 9681
Size: 261294 Color: 1374
Size: 260402 Color: 1281

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 478322 Color: 9682
Size: 268505 Color: 1993
Size: 253174 Color: 490

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 478331 Color: 9683
Size: 266192 Color: 1815
Size: 255478 Color: 753

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 478402 Color: 9684
Size: 263751 Color: 1593
Size: 257848 Color: 992

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 478467 Color: 9685
Size: 262072 Color: 1432
Size: 259462 Color: 1190

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 478615 Color: 9686
Size: 268656 Color: 2009
Size: 252730 Color: 420

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 478648 Color: 9687
Size: 264143 Color: 1622
Size: 257210 Color: 926

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 478653 Color: 9688
Size: 263786 Color: 1595
Size: 257562 Color: 964

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 478679 Color: 9689
Size: 261028 Color: 1348
Size: 260294 Color: 1270

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 478705 Color: 9690
Size: 266830 Color: 1874
Size: 254466 Color: 639

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 478726 Color: 9691
Size: 262954 Color: 1519
Size: 258321 Color: 1054

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 478750 Color: 9692
Size: 269799 Color: 2090
Size: 251452 Color: 262

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 478775 Color: 9693
Size: 270707 Color: 2181
Size: 250519 Color: 113

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 478807 Color: 9694
Size: 269149 Color: 2042
Size: 252045 Color: 338

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 478922 Color: 9695
Size: 263001 Color: 1522
Size: 258078 Color: 1015

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 478977 Color: 9696
Size: 267002 Color: 1887
Size: 254022 Color: 599

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 478996 Color: 9697
Size: 261929 Color: 1419
Size: 259076 Color: 1150

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 479011 Color: 9698
Size: 263649 Color: 1586
Size: 257341 Color: 946

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 479044 Color: 9699
Size: 266534 Color: 1842
Size: 254423 Color: 635

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 479049 Color: 9700
Size: 265021 Color: 1701
Size: 255931 Color: 800

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 479060 Color: 9701
Size: 267801 Color: 1940
Size: 253140 Color: 486

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 479129 Color: 9702
Size: 261725 Color: 1404
Size: 259147 Color: 1164

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 479251 Color: 9703
Size: 263722 Color: 1590
Size: 257028 Color: 914

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 479326 Color: 9704
Size: 267891 Color: 1945
Size: 252784 Color: 428

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 479411 Color: 9705
Size: 266869 Color: 1877
Size: 253721 Color: 555

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 479457 Color: 9706
Size: 269837 Color: 2094
Size: 250707 Color: 140

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 479491 Color: 9707
Size: 266222 Color: 1817
Size: 254288 Color: 624

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 479598 Color: 9708
Size: 265255 Color: 1728
Size: 255148 Color: 722

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 479612 Color: 9709
Size: 262829 Color: 1509
Size: 257560 Color: 963

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 479622 Color: 9710
Size: 267126 Color: 1897
Size: 253253 Color: 501

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 479780 Color: 9711
Size: 265768 Color: 1768
Size: 254453 Color: 638

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 479881 Color: 9712
Size: 261221 Color: 1367
Size: 258899 Color: 1123

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 480092 Color: 9713
Size: 269651 Color: 2081
Size: 250258 Color: 63

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 480117 Color: 9714
Size: 259948 Color: 1232
Size: 259936 Color: 1231

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 480178 Color: 9715
Size: 269620 Color: 2077
Size: 250203 Color: 46

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 480186 Color: 9716
Size: 261347 Color: 1380
Size: 258468 Color: 1073

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 480221 Color: 9717
Size: 262183 Color: 1442
Size: 257597 Color: 970

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 480306 Color: 9718
Size: 267877 Color: 1943
Size: 251818 Color: 306

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 480452 Color: 9719
Size: 260823 Color: 1324
Size: 258726 Color: 1101

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 480488 Color: 9720
Size: 262209 Color: 1445
Size: 257304 Color: 937

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 480520 Color: 9721
Size: 263906 Color: 1610
Size: 255575 Color: 762

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 480554 Color: 9722
Size: 269214 Color: 2050
Size: 250233 Color: 57

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 480609 Color: 9723
Size: 261050 Color: 1353
Size: 258342 Color: 1055

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 480655 Color: 9724
Size: 267938 Color: 1950
Size: 251408 Color: 255

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 480909 Color: 9725
Size: 260736 Color: 1320
Size: 258356 Color: 1058

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 481036 Color: 9726
Size: 268537 Color: 1999
Size: 250428 Color: 97

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 481086 Color: 9727
Size: 264263 Color: 1639
Size: 254652 Color: 658

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 481094 Color: 9728
Size: 266752 Color: 1864
Size: 252155 Color: 353

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 481095 Color: 9729
Size: 260071 Color: 1246
Size: 258835 Color: 1114

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 481160 Color: 9730
Size: 265032 Color: 1703
Size: 253809 Color: 569

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 481193 Color: 9731
Size: 260824 Color: 1325
Size: 257984 Color: 1007

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 481195 Color: 9732
Size: 265797 Color: 1772
Size: 253009 Color: 462

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 481348 Color: 9733
Size: 267477 Color: 1916
Size: 251176 Color: 218

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 481359 Color: 9734
Size: 262497 Color: 1472
Size: 256145 Color: 825

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 481368 Color: 9735
Size: 260979 Color: 1345
Size: 257654 Color: 976

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 481487 Color: 9736
Size: 268034 Color: 1955
Size: 250480 Color: 104

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 481488 Color: 9737
Size: 263263 Color: 1552
Size: 255250 Color: 732

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 481509 Color: 9738
Size: 268199 Color: 1972
Size: 250293 Color: 67

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 481584 Color: 9739
Size: 265926 Color: 1783
Size: 252491 Color: 398

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 481627 Color: 9740
Size: 265911 Color: 1781
Size: 252463 Color: 391

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 481757 Color: 9741
Size: 261860 Color: 1411
Size: 256384 Color: 850

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 481779 Color: 9742
Size: 259734 Color: 1222
Size: 258488 Color: 1077

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 481794 Color: 9743
Size: 260241 Color: 1264
Size: 257966 Color: 1005

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 482076 Color: 9744
Size: 266251 Color: 1820
Size: 251674 Color: 290

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 482140 Color: 9745
Size: 259327 Color: 1181
Size: 258534 Color: 1085

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 482222 Color: 9746
Size: 258906 Color: 1124
Size: 258873 Color: 1118

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 482256 Color: 9747
Size: 259971 Color: 1235
Size: 257774 Color: 989

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 482263 Color: 9748
Size: 264245 Color: 1635
Size: 253493 Color: 527

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 482299 Color: 9749
Size: 258928 Color: 1127
Size: 258774 Color: 1106

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 482439 Color: 9750
Size: 266147 Color: 1810
Size: 251415 Color: 257

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 482609 Color: 9751
Size: 259198 Color: 1169
Size: 258194 Color: 1036

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 482768 Color: 9752
Size: 262195 Color: 1443
Size: 255038 Color: 713

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 482855 Color: 9753
Size: 260571 Color: 1302
Size: 256575 Color: 869

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 482861 Color: 9754
Size: 259187 Color: 1167
Size: 257953 Color: 1002

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 482889 Color: 9755
Size: 262101 Color: 1433
Size: 255011 Color: 711

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 482950 Color: 9756
Size: 265495 Color: 1740
Size: 251556 Color: 273

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 482964 Color: 9757
Size: 259822 Color: 1228
Size: 257215 Color: 929

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 482983 Color: 9758
Size: 261610 Color: 1395
Size: 255408 Color: 749

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 483095 Color: 9759
Size: 259518 Color: 1194
Size: 257388 Color: 950

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 483118 Color: 9760
Size: 260744 Color: 1321
Size: 256139 Color: 822

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 483129 Color: 9761
Size: 258736 Color: 1103
Size: 258136 Color: 1026

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 483239 Color: 9762
Size: 259664 Color: 1210
Size: 257098 Color: 920

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 483320 Color: 9763
Size: 260611 Color: 1306
Size: 256070 Color: 813

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 483388 Color: 9764
Size: 258880 Color: 1120
Size: 257733 Color: 985

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 483511 Color: 9765
Size: 261257 Color: 1369
Size: 255233 Color: 730

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 483540 Color: 9766
Size: 258407 Color: 1065
Size: 258054 Color: 1012

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 483647 Color: 9767
Size: 262206 Color: 1444
Size: 254148 Color: 610

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 483671 Color: 9768
Size: 259117 Color: 1158
Size: 257213 Color: 927

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 483899 Color: 9769
Size: 264855 Color: 1684
Size: 251247 Color: 232

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 483921 Color: 9770
Size: 264962 Color: 1698
Size: 251118 Color: 209

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 484005 Color: 9771
Size: 260271 Color: 1266
Size: 255725 Color: 777

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 484037 Color: 9772
Size: 261258 Color: 1370
Size: 254706 Color: 666

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 484039 Color: 9773
Size: 258461 Color: 1072
Size: 257501 Color: 956

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 484079 Color: 9774
Size: 259454 Color: 1188
Size: 256468 Color: 856

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 484112 Color: 9775
Size: 264878 Color: 1690
Size: 251011 Color: 194

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 484149 Color: 9776
Size: 264167 Color: 1624
Size: 251685 Color: 292

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 484204 Color: 9777
Size: 260536 Color: 1297
Size: 255261 Color: 733

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 484251 Color: 9778
Size: 262526 Color: 1476
Size: 253224 Color: 496

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 484295 Color: 9779
Size: 263089 Color: 1534
Size: 252617 Color: 410

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 484333 Color: 9780
Size: 260082 Color: 1247
Size: 255586 Color: 763

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 484461 Color: 9781
Size: 265294 Color: 1730
Size: 250246 Color: 60

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 484513 Color: 9782
Size: 264873 Color: 1687
Size: 250615 Color: 127

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 484594 Color: 9783
Size: 259680 Color: 1211
Size: 255727 Color: 778

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 484611 Color: 9784
Size: 261161 Color: 1362
Size: 254229 Color: 620

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 484659 Color: 9785
Size: 262488 Color: 1469
Size: 252854 Color: 438

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 484680 Color: 9786
Size: 262346 Color: 1454
Size: 252975 Color: 452

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 484718 Color: 9787
Size: 258110 Color: 1021
Size: 257173 Color: 924

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 484756 Color: 9788
Size: 260668 Color: 1311
Size: 254577 Color: 652

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 484759 Color: 9789
Size: 261287 Color: 1373
Size: 253955 Color: 591

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 484810 Color: 9790
Size: 262895 Color: 1512
Size: 252296 Color: 371

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 484858 Color: 9791
Size: 264255 Color: 1637
Size: 250888 Color: 167

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 484886 Color: 9792
Size: 258859 Color: 1117
Size: 256256 Color: 837

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 484952 Color: 9793
Size: 261387 Color: 1382
Size: 253662 Color: 548

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 485042 Color: 9794
Size: 259092 Color: 1154
Size: 255867 Color: 790

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 485063 Color: 9795
Size: 264575 Color: 1667
Size: 250363 Color: 85

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 485309 Color: 9796
Size: 257356 Color: 948
Size: 257336 Color: 945

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 485462 Color: 9797
Size: 259582 Color: 1201
Size: 254957 Color: 700

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 485534 Color: 9798
Size: 262719 Color: 1497
Size: 251748 Color: 297

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 485588 Color: 9799
Size: 261039 Color: 1352
Size: 253374 Color: 516

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 485699 Color: 9800
Size: 263835 Color: 1602
Size: 250467 Color: 101

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 485754 Color: 9801
Size: 260705 Color: 1316
Size: 253542 Color: 533

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 485775 Color: 9802
Size: 264003 Color: 1615
Size: 250223 Color: 52

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 485776 Color: 9803
Size: 262957 Color: 1520
Size: 251268 Color: 236

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 485799 Color: 9804
Size: 261923 Color: 1418
Size: 252279 Color: 370

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 485799 Color: 9805
Size: 259367 Color: 1182
Size: 254835 Color: 683

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 485826 Color: 9806
Size: 263343 Color: 1558
Size: 250832 Color: 162

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 485881 Color: 9807
Size: 262374 Color: 1457
Size: 251746 Color: 296

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 485955 Color: 9808
Size: 261645 Color: 1397
Size: 252401 Color: 381

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 485991 Color: 9809
Size: 257671 Color: 977
Size: 256339 Color: 845

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 486048 Color: 9810
Size: 257326 Color: 941
Size: 256627 Color: 879

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 486075 Color: 9811
Size: 258920 Color: 1125
Size: 255006 Color: 709

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 486185 Color: 9812
Size: 261963 Color: 1423
Size: 251853 Color: 308

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 486352 Color: 9813
Size: 262652 Color: 1487
Size: 250997 Color: 193

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 486390 Color: 9814
Size: 256955 Color: 904
Size: 256656 Color: 883

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 486501 Color: 9815
Size: 260499 Color: 1294
Size: 253001 Color: 459

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 486610 Color: 9816
Size: 258641 Color: 1093
Size: 254750 Color: 677

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 486618 Color: 9817
Size: 257579 Color: 966
Size: 255804 Color: 785

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 486618 Color: 9818
Size: 263037 Color: 1528
Size: 250346 Color: 83

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 486623 Color: 9819
Size: 257754 Color: 986
Size: 255624 Color: 769

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 486635 Color: 9820
Size: 262158 Color: 1440
Size: 251208 Color: 225

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 486967 Color: 9821
Size: 263025 Color: 1526
Size: 250009 Color: 2

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 487011 Color: 9822
Size: 257772 Color: 988
Size: 255218 Color: 729

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 487030 Color: 9823
Size: 259999 Color: 1239
Size: 252972 Color: 451

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 487051 Color: 9824
Size: 260117 Color: 1250
Size: 252833 Color: 434

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 487139 Color: 9825
Size: 261656 Color: 1398
Size: 251206 Color: 224

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 487178 Color: 9826
Size: 259562 Color: 1199
Size: 253261 Color: 503

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 487404 Color: 9827
Size: 261758 Color: 1405
Size: 250839 Color: 163

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 487471 Color: 9828
Size: 258358 Color: 1060
Size: 254172 Color: 615

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 487513 Color: 9829
Size: 258122 Color: 1022
Size: 254366 Color: 628

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 487524 Color: 9830
Size: 259591 Color: 1202
Size: 252886 Color: 439

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 487588 Color: 9831
Size: 262248 Color: 1448
Size: 250165 Color: 37

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 487617 Color: 9832
Size: 258624 Color: 1091
Size: 253760 Color: 560

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 487636 Color: 9833
Size: 256247 Color: 836
Size: 256118 Color: 818

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 487736 Color: 9834
Size: 261195 Color: 1366
Size: 251070 Color: 200

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 487802 Color: 9835
Size: 256568 Color: 868
Size: 255631 Color: 770

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 487818 Color: 9836
Size: 256219 Color: 829
Size: 255964 Color: 801

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 487846 Color: 9837
Size: 262021 Color: 1426
Size: 250134 Color: 30

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 487905 Color: 9838
Size: 258937 Color: 1129
Size: 253159 Color: 488

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 487916 Color: 9839
Size: 261013 Color: 1347
Size: 251072 Color: 201

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 487999 Color: 9840
Size: 259688 Color: 1212
Size: 252314 Color: 373

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 488026 Color: 9841
Size: 258838 Color: 1115
Size: 253137 Color: 485

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 488177 Color: 9842
Size: 261111 Color: 1357
Size: 250713 Color: 141

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 488251 Color: 9843
Size: 260950 Color: 1338
Size: 250800 Color: 152

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 488279 Color: 9844
Size: 258671 Color: 1096
Size: 253051 Color: 468

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 488414 Color: 9845
Size: 259655 Color: 1209
Size: 251932 Color: 323

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 488500 Color: 9846
Size: 258273 Color: 1047
Size: 253228 Color: 498

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 488657 Color: 9847
Size: 256776 Color: 892
Size: 254568 Color: 650

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 488759 Color: 9848
Size: 257639 Color: 974
Size: 253603 Color: 542

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 488905 Color: 9849
Size: 260955 Color: 1342
Size: 250141 Color: 33

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 488928 Color: 9850
Size: 260082 Color: 1248
Size: 250991 Color: 190

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 488934 Color: 9851
Size: 259297 Color: 1177
Size: 251770 Color: 300

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 489017 Color: 9852
Size: 257581 Color: 967
Size: 253403 Color: 517

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 489069 Color: 9853
Size: 260435 Color: 1287
Size: 250497 Color: 108

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 489261 Color: 9854
Size: 258200 Color: 1037
Size: 252540 Color: 402

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 489360 Color: 9855
Size: 258495 Color: 1080
Size: 252146 Color: 351

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 489481 Color: 9857
Size: 258955 Color: 1131
Size: 251565 Color: 275

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 489481 Color: 9856
Size: 259058 Color: 1147
Size: 251462 Color: 263

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 489596 Color: 9858
Size: 255373 Color: 746
Size: 255032 Color: 712

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 489692 Color: 9859
Size: 259810 Color: 1227
Size: 250499 Color: 109

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 489803 Color: 9860
Size: 258345 Color: 1057
Size: 251853 Color: 309

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 489806 Color: 9861
Size: 257777 Color: 990
Size: 252418 Color: 383

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 489813 Color: 9862
Size: 256023 Color: 809
Size: 254165 Color: 612

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 489939 Color: 9863
Size: 257230 Color: 931
Size: 252832 Color: 433

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 490099 Color: 9864
Size: 259477 Color: 1191
Size: 250425 Color: 95

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 490100 Color: 9865
Size: 259369 Color: 1184
Size: 250532 Color: 114

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 490101 Color: 9866
Size: 259075 Color: 1149
Size: 250825 Color: 159

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 490110 Color: 9867
Size: 257370 Color: 949
Size: 252521 Color: 401

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 490110 Color: 9868
Size: 258321 Color: 1053
Size: 251570 Color: 278

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 490289 Color: 9869
Size: 258319 Color: 1052
Size: 251393 Color: 253

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 490295 Color: 9870
Size: 257245 Color: 934
Size: 252461 Color: 389

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 490311 Color: 9871
Size: 254943 Color: 697
Size: 254747 Color: 676

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 490319 Color: 9872
Size: 257595 Color: 969
Size: 252087 Color: 344

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 490344 Color: 9873
Size: 255820 Color: 787
Size: 253837 Color: 578

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 490363 Color: 9874
Size: 255830 Color: 788
Size: 253808 Color: 568

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 490379 Color: 9875
Size: 259086 Color: 1153
Size: 250536 Color: 116

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 490409 Color: 9876
Size: 259211 Color: 1170
Size: 250381 Color: 89

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 490420 Color: 9877
Size: 258002 Color: 1008
Size: 251579 Color: 279

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 490491 Color: 9878
Size: 257640 Color: 975
Size: 251870 Color: 312

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 490530 Color: 9879
Size: 255411 Color: 750
Size: 254060 Color: 604

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 490708 Color: 9880
Size: 258641 Color: 1094
Size: 250652 Color: 134

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 490810 Color: 9881
Size: 257304 Color: 936
Size: 251887 Color: 315

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 490818 Color: 9882
Size: 258224 Color: 1041
Size: 250959 Color: 182

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 490866 Color: 9883
Size: 258193 Color: 1034
Size: 250942 Color: 180

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 490930 Color: 9884
Size: 255977 Color: 806
Size: 253094 Color: 479

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 490931 Color: 9885
Size: 254790 Color: 679
Size: 254280 Color: 623

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 491003 Color: 9886
Size: 255967 Color: 802
Size: 253031 Color: 464

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 491064 Color: 9887
Size: 258130 Color: 1024
Size: 250807 Color: 155

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 491086 Color: 9888
Size: 254717 Color: 668
Size: 254198 Color: 617

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 491250 Color: 9889
Size: 256679 Color: 888
Size: 252072 Color: 342

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 491324 Color: 9890
Size: 258135 Color: 1025
Size: 250542 Color: 118

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 491349 Color: 9891
Size: 257929 Color: 998
Size: 250723 Color: 142

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 491402 Color: 9892
Size: 255510 Color: 756
Size: 253089 Color: 477

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 491432 Color: 9893
Size: 256979 Color: 906
Size: 251590 Color: 280

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 491438 Color: 9894
Size: 256140 Color: 823
Size: 252423 Color: 385

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 491693 Color: 9895
Size: 257112 Color: 921
Size: 251196 Color: 220

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 491864 Color: 9896
Size: 256982 Color: 907
Size: 251155 Color: 216

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 491929 Color: 9897
Size: 254067 Color: 606
Size: 254005 Color: 597

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 491938 Color: 9898
Size: 256985 Color: 909
Size: 251078 Color: 203

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 491958 Color: 9899
Size: 254692 Color: 663
Size: 253351 Color: 511

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 491969 Color: 9900
Size: 254967 Color: 701
Size: 253065 Color: 470

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 492042 Color: 9901
Size: 256324 Color: 842
Size: 251635 Color: 285

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 492088 Color: 9902
Size: 255753 Color: 781
Size: 252160 Color: 354

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 492175 Color: 9903
Size: 255247 Color: 731
Size: 252579 Color: 405

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 492188 Color: 9904
Size: 255208 Color: 728
Size: 252605 Color: 407

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 492216 Color: 9905
Size: 256801 Color: 895
Size: 250984 Color: 186

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 492261 Color: 9906
Size: 255478 Color: 754
Size: 252262 Color: 368

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 492336 Color: 9907
Size: 254007 Color: 598
Size: 253658 Color: 547

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 492429 Color: 9908
Size: 254664 Color: 661
Size: 252908 Color: 442

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 492460 Color: 9909
Size: 254497 Color: 643
Size: 253044 Color: 467

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 492706 Color: 9910
Size: 255275 Color: 734
Size: 252020 Color: 336

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 492710 Color: 9911
Size: 256998 Color: 910
Size: 250293 Color: 68

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 492847 Color: 9912
Size: 254545 Color: 647
Size: 252609 Color: 408

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 492907 Color: 9913
Size: 256331 Color: 843
Size: 250763 Color: 147

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 492968 Color: 9914
Size: 255927 Color: 799
Size: 251106 Color: 208

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 493057 Color: 9915
Size: 255818 Color: 786
Size: 251126 Color: 212

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 493163 Color: 9916
Size: 256612 Color: 876
Size: 250226 Color: 53

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 493201 Color: 9917
Size: 254831 Color: 681
Size: 251969 Color: 328

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 493232 Color: 9918
Size: 255493 Color: 755
Size: 251276 Color: 238

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 493254 Color: 9919
Size: 253747 Color: 557
Size: 253000 Color: 458

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 493322 Color: 9920
Size: 255316 Color: 738
Size: 251363 Color: 249

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 493372 Color: 9921
Size: 255549 Color: 759
Size: 251080 Color: 204

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 493480 Color: 9922
Size: 253828 Color: 576
Size: 252693 Color: 418

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 493507 Color: 9923
Size: 256163 Color: 826
Size: 250331 Color: 79

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 493555 Color: 9924
Size: 254307 Color: 625
Size: 252139 Color: 350

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 493714 Color: 9925
Size: 256053 Color: 812
Size: 250234 Color: 58

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 493852 Color: 9926
Size: 255605 Color: 767
Size: 250544 Color: 119

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 494024 Color: 9927
Size: 255746 Color: 780
Size: 250231 Color: 56

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 494151 Color: 9928
Size: 255174 Color: 725
Size: 250676 Color: 136

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 494180 Color: 9929
Size: 254828 Color: 680
Size: 250993 Color: 191

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 494617 Color: 9930
Size: 255303 Color: 737
Size: 250081 Color: 14

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 494636 Color: 9931
Size: 253823 Color: 575
Size: 251542 Color: 271

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 494641 Color: 9932
Size: 255060 Color: 715
Size: 250300 Color: 70

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 494798 Color: 9933
Size: 253598 Color: 539
Size: 251605 Color: 282

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 494800 Color: 9934
Size: 253814 Color: 571
Size: 251387 Color: 252

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 494833 Color: 9935
Size: 254985 Color: 706
Size: 250183 Color: 40

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 494846 Color: 9936
Size: 253513 Color: 531
Size: 251642 Color: 286

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 494901 Color: 9937
Size: 254630 Color: 656
Size: 250470 Color: 103

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 494919 Color: 9938
Size: 252894 Color: 441
Size: 252188 Color: 360

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 494937 Color: 9939
Size: 253084 Color: 475
Size: 251980 Color: 331

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 495048 Color: 9940
Size: 253177 Color: 492
Size: 251776 Color: 302

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 495077 Color: 9941
Size: 253039 Color: 465
Size: 251885 Color: 314

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 495171 Color: 9942
Size: 253784 Color: 564
Size: 251046 Color: 198

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 495334 Color: 9943
Size: 254556 Color: 648
Size: 250111 Color: 24

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 495384 Color: 9944
Size: 253837 Color: 579
Size: 250780 Color: 150

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 495400 Color: 9945
Size: 253966 Color: 594
Size: 250635 Color: 131

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 495426 Color: 9946
Size: 254474 Color: 640
Size: 250101 Color: 23

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 495598 Color: 9947
Size: 253162 Color: 489
Size: 251241 Color: 228

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 495661 Color: 9948
Size: 253137 Color: 484
Size: 251203 Color: 223

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 495755 Color: 9949
Size: 253991 Color: 596
Size: 250255 Color: 62

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 495788 Color: 9950
Size: 253226 Color: 497
Size: 250987 Color: 189

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 495794 Color: 9951
Size: 253783 Color: 563
Size: 250424 Color: 94

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 495823 Color: 9952
Size: 252807 Color: 432
Size: 251371 Color: 251

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 495898 Color: 9953
Size: 253241 Color: 500
Size: 250862 Color: 165

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 495991 Color: 9954
Size: 252166 Color: 357
Size: 251844 Color: 307

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 496122 Color: 9955
Size: 252742 Color: 421
Size: 251137 Color: 213

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 496157 Color: 9956
Size: 252942 Color: 447
Size: 250902 Color: 170

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 496208 Color: 9957
Size: 252621 Color: 411
Size: 251172 Color: 217

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 496241 Color: 9958
Size: 253265 Color: 505
Size: 250495 Color: 107

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 496289 Color: 9959
Size: 253080 Color: 473
Size: 250632 Color: 130

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 496320 Color: 9960
Size: 253123 Color: 482
Size: 250558 Color: 122

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 496569 Color: 9961
Size: 252550 Color: 403
Size: 250882 Color: 166

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 496715 Color: 9962
Size: 252957 Color: 450
Size: 250329 Color: 74

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 496750 Color: 9963
Size: 252770 Color: 425
Size: 250481 Color: 105

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 496753 Color: 9964
Size: 252387 Color: 378
Size: 250861 Color: 164

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 497078 Color: 9965
Size: 251646 Color: 288
Size: 251277 Color: 239

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 497081 Color: 9966
Size: 251995 Color: 333
Size: 250925 Color: 176

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 497156 Color: 9967
Size: 251644 Color: 287
Size: 251201 Color: 222

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 497181 Color: 9968
Size: 252081 Color: 343
Size: 250739 Color: 144

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 497261 Color: 9969
Size: 251699 Color: 293
Size: 251041 Color: 197

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 497446 Color: 9970
Size: 251313 Color: 242
Size: 251242 Color: 229

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 497749 Color: 9971
Size: 251348 Color: 245
Size: 250904 Color: 172

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 497903 Color: 9972
Size: 251961 Color: 326
Size: 250137 Color: 32

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 497942 Color: 9973
Size: 251590 Color: 281
Size: 250469 Color: 102

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 497989 Color: 9974
Size: 251041 Color: 196
Size: 250971 Color: 183

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 498082 Color: 9975
Size: 251757 Color: 298
Size: 250162 Color: 36

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 498108 Color: 9976
Size: 251354 Color: 247
Size: 250539 Color: 117

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 498311 Color: 9977
Size: 250933 Color: 179
Size: 250757 Color: 146

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 498570 Color: 9978
Size: 250917 Color: 174
Size: 250514 Color: 111

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 498585 Color: 9979
Size: 251359 Color: 248
Size: 250057 Color: 13

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 498678 Color: 9980
Size: 250925 Color: 177
Size: 250398 Color: 91

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 498700 Color: 9982
Size: 251214 Color: 226
Size: 250087 Color: 19

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 498700 Color: 9981
Size: 250978 Color: 184
Size: 250323 Color: 73

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 498770 Color: 9983
Size: 250898 Color: 169
Size: 250333 Color: 80

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 498853 Color: 9984
Size: 250996 Color: 192
Size: 250152 Color: 35

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 498986 Color: 9985
Size: 250829 Color: 161
Size: 250186 Color: 42

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 499034 Color: 9986
Size: 250673 Color: 135
Size: 250294 Color: 69

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 499059 Color: 9987
Size: 250683 Color: 137
Size: 250259 Color: 64

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 499076 Color: 9988
Size: 250612 Color: 126
Size: 250313 Color: 72

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 499149 Color: 9989
Size: 250636 Color: 132
Size: 250216 Color: 49

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 499275 Color: 9990
Size: 250644 Color: 133
Size: 250082 Color: 15

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 499494 Color: 9991
Size: 250279 Color: 66
Size: 250228 Color: 54

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 499535 Color: 9992
Size: 250444 Color: 98
Size: 250022 Color: 5

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 499589 Color: 9993
Size: 250221 Color: 50
Size: 250191 Color: 43

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 499617 Color: 9994
Size: 250364 Color: 86
Size: 250020 Color: 3

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 499648 Color: 9995
Size: 250210 Color: 47
Size: 250143 Color: 34

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 499724 Color: 9996
Size: 250191 Color: 44
Size: 250086 Color: 18

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 499726 Color: 9997
Size: 250229 Color: 55
Size: 250046 Color: 12

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 499748 Color: 9998
Size: 250215 Color: 48
Size: 250038 Color: 9

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 499844 Color: 9999
Size: 250125 Color: 27
Size: 250032 Color: 6

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 499867 Color: 10000
Size: 250100 Color: 22
Size: 250034 Color: 8

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 499868 Color: 10001
Size: 250112 Color: 25
Size: 250021 Color: 4

Bin 3039: 1 of cap free
Amount of items: 3
Items: 
Size: 367060 Color: 7075
Size: 316980 Color: 4968
Size: 315960 Color: 4920

Bin 3040: 1 of cap free
Amount of items: 3
Items: 
Size: 355462 Color: 6629
Size: 345734 Color: 6249
Size: 298804 Color: 4017

Bin 3041: 1 of cap free
Amount of items: 3
Items: 
Size: 367875 Color: 7113
Size: 316515 Color: 4949
Size: 315610 Color: 4900

Bin 3042: 1 of cap free
Amount of items: 3
Items: 
Size: 368058 Color: 7125
Size: 316634 Color: 4953
Size: 315308 Color: 4886

Bin 3043: 1 of cap free
Amount of items: 3
Items: 
Size: 369110 Color: 7159
Size: 315920 Color: 4917
Size: 314970 Color: 4872

Bin 3044: 1 of cap free
Amount of items: 3
Items: 
Size: 369140 Color: 7162
Size: 321192 Color: 5158
Size: 309668 Color: 4608

Bin 3045: 1 of cap free
Amount of items: 3
Items: 
Size: 370109 Color: 7190
Size: 320037 Color: 5103
Size: 309854 Color: 4619

Bin 3046: 1 of cap free
Amount of items: 3
Items: 
Size: 370244 Color: 7197
Size: 315862 Color: 4909
Size: 313894 Color: 4805

Bin 3047: 1 of cap free
Amount of items: 3
Items: 
Size: 371173 Color: 7232
Size: 318751 Color: 5040
Size: 310076 Color: 4633

Bin 3048: 1 of cap free
Amount of items: 3
Items: 
Size: 372185 Color: 7274
Size: 345535 Color: 6243
Size: 282280 Color: 2989

Bin 3049: 1 of cap free
Amount of items: 3
Items: 
Size: 372332 Color: 7278
Size: 345435 Color: 6237
Size: 282233 Color: 2983

Bin 3050: 1 of cap free
Amount of items: 3
Items: 
Size: 372714 Color: 7285
Size: 314101 Color: 4816
Size: 313185 Color: 4766

Bin 3051: 1 of cap free
Amount of items: 3
Items: 
Size: 380835 Color: 7550
Size: 342402 Color: 6108
Size: 276763 Color: 2623

Bin 3052: 1 of cap free
Amount of items: 3
Items: 
Size: 350141 Color: 6433
Size: 332014 Color: 5661
Size: 317845 Color: 5007

Bin 3053: 1 of cap free
Amount of items: 3
Items: 
Size: 400938 Color: 8141
Size: 324902 Color: 5334
Size: 274160 Color: 2448

Bin 3054: 1 of cap free
Amount of items: 3
Items: 
Size: 343303 Color: 6144
Size: 338613 Color: 5948
Size: 318084 Color: 5022

Bin 3055: 1 of cap free
Amount of items: 3
Items: 
Size: 366696 Color: 7058
Size: 345725 Color: 6248
Size: 287579 Color: 3336

Bin 3056: 1 of cap free
Amount of items: 3
Items: 
Size: 393332 Color: 7931
Size: 319075 Color: 5053
Size: 287593 Color: 3338

Bin 3057: 1 of cap free
Amount of items: 3
Items: 
Size: 366929 Color: 7069
Size: 321280 Color: 5164
Size: 311791 Color: 4707

Bin 3058: 1 of cap free
Amount of items: 3
Items: 
Size: 358645 Color: 6752
Size: 321647 Color: 5187
Size: 319708 Color: 5085

Bin 3059: 1 of cap free
Amount of items: 3
Items: 
Size: 351800 Color: 6495
Size: 332108 Color: 5667
Size: 316092 Color: 4930

Bin 3060: 1 of cap free
Amount of items: 3
Items: 
Size: 375974 Color: 7404
Size: 343716 Color: 6159
Size: 280310 Color: 2858

Bin 3061: 1 of cap free
Amount of items: 3
Items: 
Size: 408678 Color: 8321
Size: 301638 Color: 4182
Size: 289684 Color: 3449

Bin 3062: 1 of cap free
Amount of items: 3
Items: 
Size: 359018 Color: 6764
Size: 338786 Color: 5953
Size: 302196 Color: 4220

Bin 3063: 1 of cap free
Amount of items: 3
Items: 
Size: 393519 Color: 7935
Size: 307764 Color: 4506
Size: 298717 Color: 4011

Bin 3064: 1 of cap free
Amount of items: 3
Items: 
Size: 360820 Color: 6840
Size: 351183 Color: 6477
Size: 287997 Color: 3357

Bin 3065: 1 of cap free
Amount of items: 3
Items: 
Size: 394903 Color: 7968
Size: 331949 Color: 5657
Size: 273148 Color: 2364

Bin 3066: 1 of cap free
Amount of items: 3
Items: 
Size: 399175 Color: 8099
Size: 333310 Color: 5728
Size: 267515 Color: 1921

Bin 3067: 1 of cap free
Amount of items: 3
Items: 
Size: 382890 Color: 7617
Size: 341731 Color: 6073
Size: 275379 Color: 2535

Bin 3068: 1 of cap free
Amount of items: 3
Items: 
Size: 351994 Color: 6504
Size: 327741 Color: 5464
Size: 320265 Color: 5108

Bin 3069: 1 of cap free
Amount of items: 3
Items: 
Size: 367490 Color: 7094
Size: 346528 Color: 6295
Size: 285982 Color: 3216

Bin 3070: 1 of cap free
Amount of items: 3
Items: 
Size: 376898 Color: 7436
Size: 352538 Color: 6527
Size: 270564 Color: 2164

Bin 3071: 1 of cap free
Amount of items: 3
Items: 
Size: 373823 Color: 7331
Size: 321939 Color: 5200
Size: 304238 Color: 4331

Bin 3072: 1 of cap free
Amount of items: 3
Items: 
Size: 376439 Color: 7429
Size: 336757 Color: 5870
Size: 286804 Color: 3283

Bin 3073: 1 of cap free
Amount of items: 3
Items: 
Size: 365225 Color: 7000
Size: 345532 Color: 6241
Size: 289243 Color: 3428

Bin 3074: 1 of cap free
Amount of items: 3
Items: 
Size: 361214 Color: 6858
Size: 348396 Color: 6374
Size: 290390 Color: 3497

Bin 3075: 1 of cap free
Amount of items: 3
Items: 
Size: 380013 Color: 7534
Size: 330902 Color: 5596
Size: 289085 Color: 3417

Bin 3076: 1 of cap free
Amount of items: 3
Items: 
Size: 409089 Color: 8330
Size: 337983 Color: 5925
Size: 252928 Color: 446

Bin 3077: 1 of cap free
Amount of items: 3
Items: 
Size: 390412 Color: 7837
Size: 347926 Color: 6351
Size: 261662 Color: 1399

Bin 3078: 1 of cap free
Amount of items: 3
Items: 
Size: 382780 Color: 7614
Size: 328089 Color: 5484
Size: 289131 Color: 3421

Bin 3079: 1 of cap free
Amount of items: 3
Items: 
Size: 398354 Color: 8069
Size: 327251 Color: 5436
Size: 274395 Color: 2466

Bin 3080: 1 of cap free
Amount of items: 3
Items: 
Size: 362012 Color: 6888
Size: 339732 Color: 5993
Size: 298256 Color: 3989

Bin 3081: 1 of cap free
Amount of items: 3
Items: 
Size: 401026 Color: 8145
Size: 336394 Color: 5862
Size: 262580 Color: 1480

Bin 3082: 1 of cap free
Amount of items: 3
Items: 
Size: 383230 Color: 7633
Size: 333865 Color: 5754
Size: 282905 Color: 3032

Bin 3083: 1 of cap free
Amount of items: 3
Items: 
Size: 377401 Color: 7458
Size: 325748 Color: 5371
Size: 296851 Color: 3914

Bin 3084: 1 of cap free
Amount of items: 3
Items: 
Size: 393434 Color: 7933
Size: 345942 Color: 6266
Size: 260624 Color: 1307

Bin 3085: 1 of cap free
Amount of items: 3
Items: 
Size: 398888 Color: 8092
Size: 335113 Color: 5811
Size: 265999 Color: 1789

Bin 3086: 1 of cap free
Amount of items: 3
Items: 
Size: 398542 Color: 8079
Size: 316217 Color: 4935
Size: 285241 Color: 3171

Bin 3087: 1 of cap free
Amount of items: 3
Items: 
Size: 398204 Color: 8065
Size: 338684 Color: 5951
Size: 263112 Color: 1539

Bin 3088: 1 of cap free
Amount of items: 3
Items: 
Size: 399952 Color: 8114
Size: 306314 Color: 4428
Size: 293734 Color: 3719

Bin 3089: 1 of cap free
Amount of items: 3
Items: 
Size: 396848 Color: 8023
Size: 331836 Color: 5652
Size: 271316 Color: 2235

Bin 3090: 1 of cap free
Amount of items: 3
Items: 
Size: 361955 Color: 6886
Size: 347775 Color: 6345
Size: 290270 Color: 3491

Bin 3091: 1 of cap free
Amount of items: 3
Items: 
Size: 403082 Color: 8198
Size: 323121 Color: 5253
Size: 273797 Color: 2413

Bin 3092: 1 of cap free
Amount of items: 3
Items: 
Size: 395456 Color: 7982
Size: 333748 Color: 5747
Size: 270796 Color: 2186

Bin 3093: 1 of cap free
Amount of items: 3
Items: 
Size: 400358 Color: 8124
Size: 332127 Color: 5668
Size: 267515 Color: 1920

Bin 3094: 1 of cap free
Amount of items: 3
Items: 
Size: 391942 Color: 7890
Size: 331998 Color: 5660
Size: 276060 Color: 2585

Bin 3095: 1 of cap free
Amount of items: 3
Items: 
Size: 380333 Color: 7543
Size: 329610 Color: 5547
Size: 290057 Color: 3480

Bin 3096: 1 of cap free
Amount of items: 3
Items: 
Size: 403782 Color: 8213
Size: 300639 Color: 4127
Size: 295579 Color: 3832

Bin 3097: 1 of cap free
Amount of items: 3
Items: 
Size: 393689 Color: 7939
Size: 309915 Color: 4625
Size: 296396 Color: 3888

Bin 3098: 1 of cap free
Amount of items: 3
Items: 
Size: 391103 Color: 7861
Size: 329438 Color: 5542
Size: 279459 Color: 2804

Bin 3099: 1 of cap free
Amount of items: 3
Items: 
Size: 390914 Color: 7852
Size: 329138 Color: 5527
Size: 279948 Color: 2836

Bin 3100: 1 of cap free
Amount of items: 3
Items: 
Size: 366509 Color: 7048
Size: 337316 Color: 5895
Size: 296175 Color: 3874

Bin 3101: 1 of cap free
Amount of items: 3
Items: 
Size: 399279 Color: 8102
Size: 328508 Color: 5505
Size: 272213 Color: 2300

Bin 3102: 1 of cap free
Amount of items: 3
Items: 
Size: 389975 Color: 7818
Size: 307822 Color: 4509
Size: 302203 Color: 4221

Bin 3103: 1 of cap free
Amount of items: 3
Items: 
Size: 389744 Color: 7812
Size: 328262 Color: 5494
Size: 281994 Color: 2975

Bin 3104: 1 of cap free
Amount of items: 3
Items: 
Size: 373483 Color: 7316
Size: 343645 Color: 6154
Size: 282872 Color: 3030

Bin 3105: 1 of cap free
Amount of items: 3
Items: 
Size: 390730 Color: 7850
Size: 327022 Color: 5426
Size: 282248 Color: 2985

Bin 3106: 1 of cap free
Amount of items: 3
Items: 
Size: 366156 Color: 7030
Size: 327356 Color: 5441
Size: 306488 Color: 4440

Bin 3107: 1 of cap free
Amount of items: 3
Items: 
Size: 372888 Color: 7290
Size: 333648 Color: 5743
Size: 293464 Color: 3697

Bin 3108: 1 of cap free
Amount of items: 3
Items: 
Size: 365912 Color: 7022
Size: 340986 Color: 6047
Size: 293102 Color: 3668

Bin 3109: 1 of cap free
Amount of items: 3
Items: 
Size: 371891 Color: 7254
Size: 325958 Color: 5381
Size: 302151 Color: 4216

Bin 3110: 1 of cap free
Amount of items: 3
Items: 
Size: 377813 Color: 7464
Size: 351142 Color: 6474
Size: 271045 Color: 2212

Bin 3111: 1 of cap free
Amount of items: 3
Items: 
Size: 362192 Color: 6892
Size: 354404 Color: 6591
Size: 283404 Color: 3062

Bin 3112: 1 of cap free
Amount of items: 3
Items: 
Size: 369473 Color: 7170
Size: 324291 Color: 5306
Size: 306236 Color: 4422

Bin 3113: 1 of cap free
Amount of items: 3
Items: 
Size: 357916 Color: 6723
Size: 355270 Color: 6621
Size: 286814 Color: 3285

Bin 3114: 1 of cap free
Amount of items: 3
Items: 
Size: 367734 Color: 7105
Size: 322015 Color: 5202
Size: 310251 Color: 4639

Bin 3115: 1 of cap free
Amount of items: 3
Items: 
Size: 365118 Color: 6993
Size: 333799 Color: 5751
Size: 301083 Color: 4148

Bin 3116: 1 of cap free
Amount of items: 3
Items: 
Size: 366750 Color: 7060
Size: 321526 Color: 5180
Size: 311724 Color: 4704

Bin 3117: 1 of cap free
Amount of items: 3
Items: 
Size: 365045 Color: 6987
Size: 331824 Color: 5651
Size: 303131 Color: 4276

Bin 3118: 1 of cap free
Amount of items: 3
Items: 
Size: 367030 Color: 7073
Size: 330994 Color: 5601
Size: 301976 Color: 4202

Bin 3119: 1 of cap free
Amount of items: 3
Items: 
Size: 359496 Color: 6788
Size: 349615 Color: 6416
Size: 290889 Color: 3523

Bin 3120: 1 of cap free
Amount of items: 3
Items: 
Size: 335512 Color: 5833
Size: 334840 Color: 5795
Size: 329648 Color: 5549

Bin 3121: 1 of cap free
Amount of items: 3
Items: 
Size: 356447 Color: 6667
Size: 343184 Color: 6140
Size: 300369 Color: 4114

Bin 3122: 1 of cap free
Amount of items: 3
Items: 
Size: 363468 Color: 6938
Size: 336746 Color: 5869
Size: 299786 Color: 4077

Bin 3123: 1 of cap free
Amount of items: 3
Items: 
Size: 375571 Color: 7389
Size: 324315 Color: 5309
Size: 300114 Color: 4095

Bin 3124: 1 of cap free
Amount of items: 3
Items: 
Size: 362678 Color: 6906
Size: 345259 Color: 6229
Size: 292063 Color: 3599

Bin 3125: 1 of cap free
Amount of items: 3
Items: 
Size: 361278 Color: 6859
Size: 361009 Color: 6845
Size: 277713 Color: 2695

Bin 3126: 1 of cap free
Amount of items: 3
Items: 
Size: 359232 Color: 6779
Size: 344782 Color: 6210
Size: 295986 Color: 3858

Bin 3127: 1 of cap free
Amount of items: 3
Items: 
Size: 360783 Color: 6839
Size: 360773 Color: 6838
Size: 278444 Color: 2742

Bin 3128: 1 of cap free
Amount of items: 3
Items: 
Size: 368701 Color: 7146
Size: 360436 Color: 6824
Size: 270863 Color: 2191

Bin 3129: 1 of cap free
Amount of items: 3
Items: 
Size: 378031 Color: 7467
Size: 366598 Color: 7054
Size: 255371 Color: 745

Bin 3130: 1 of cap free
Amount of items: 3
Items: 
Size: 358061 Color: 6732
Size: 350398 Color: 6449
Size: 291541 Color: 3561

Bin 3131: 1 of cap free
Amount of items: 3
Items: 
Size: 369823 Color: 7182
Size: 359177 Color: 6775
Size: 271000 Color: 2205

Bin 3132: 1 of cap free
Amount of items: 3
Items: 
Size: 367291 Color: 7084
Size: 358173 Color: 6735
Size: 274536 Color: 2472

Bin 3133: 1 of cap free
Amount of items: 3
Items: 
Size: 358530 Color: 6749
Size: 358528 Color: 6748
Size: 282942 Color: 3037

Bin 3134: 1 of cap free
Amount of items: 3
Items: 
Size: 358566 Color: 6750
Size: 358013 Color: 6729
Size: 283421 Color: 3063

Bin 3135: 1 of cap free
Amount of items: 3
Items: 
Size: 355483 Color: 6630
Size: 345903 Color: 6264
Size: 298614 Color: 4007

Bin 3136: 1 of cap free
Amount of items: 3
Items: 
Size: 372104 Color: 7269
Size: 317000 Color: 4969
Size: 310896 Color: 4672

Bin 3137: 1 of cap free
Amount of items: 3
Items: 
Size: 363628 Color: 6945
Size: 344054 Color: 6175
Size: 292318 Color: 3617

Bin 3138: 1 of cap free
Amount of items: 3
Items: 
Size: 356805 Color: 6681
Size: 356782 Color: 6680
Size: 286413 Color: 3248

Bin 3139: 1 of cap free
Amount of items: 3
Items: 
Size: 356864 Color: 6684
Size: 327796 Color: 5467
Size: 315340 Color: 4889

Bin 3140: 1 of cap free
Amount of items: 3
Items: 
Size: 361182 Color: 6856
Size: 350656 Color: 6453
Size: 288162 Color: 3366

Bin 3141: 1 of cap free
Amount of items: 3
Items: 
Size: 356000 Color: 6648
Size: 355245 Color: 6620
Size: 288755 Color: 3401

Bin 3142: 1 of cap free
Amount of items: 3
Items: 
Size: 367407 Color: 7090
Size: 344112 Color: 6180
Size: 288481 Color: 3385

Bin 3143: 1 of cap free
Amount of items: 3
Items: 
Size: 358039 Color: 6731
Size: 355216 Color: 6618
Size: 286745 Color: 3276

Bin 3144: 1 of cap free
Amount of items: 3
Items: 
Size: 354580 Color: 6597
Size: 354400 Color: 6590
Size: 291020 Color: 3531

Bin 3145: 1 of cap free
Amount of items: 3
Items: 
Size: 353720 Color: 6566
Size: 344172 Color: 6182
Size: 302108 Color: 4210

Bin 3146: 1 of cap free
Amount of items: 3
Items: 
Size: 397491 Color: 8044
Size: 327796 Color: 5468
Size: 274713 Color: 2489

Bin 3147: 1 of cap free
Amount of items: 3
Items: 
Size: 372053 Color: 7266
Size: 334396 Color: 5778
Size: 293551 Color: 3707

Bin 3148: 1 of cap free
Amount of items: 3
Items: 
Size: 352741 Color: 6533
Size: 352296 Color: 6520
Size: 294963 Color: 3793

Bin 3149: 1 of cap free
Amount of items: 3
Items: 
Size: 352409 Color: 6524
Size: 344104 Color: 6179
Size: 303487 Color: 4291

Bin 3150: 1 of cap free
Amount of items: 3
Items: 
Size: 352208 Color: 6512
Size: 352151 Color: 6510
Size: 295641 Color: 3833

Bin 3151: 1 of cap free
Amount of items: 3
Items: 
Size: 352018 Color: 6505
Size: 335129 Color: 5815
Size: 312853 Color: 4749

Bin 3152: 1 of cap free
Amount of items: 3
Items: 
Size: 371836 Color: 7252
Size: 331378 Color: 5625
Size: 296786 Color: 3909

Bin 3153: 1 of cap free
Amount of items: 3
Items: 
Size: 360447 Color: 6825
Size: 342614 Color: 6121
Size: 296939 Color: 3918

Bin 3154: 1 of cap free
Amount of items: 3
Items: 
Size: 351757 Color: 6494
Size: 351116 Color: 6473
Size: 297127 Color: 3928

Bin 3155: 1 of cap free
Amount of items: 3
Items: 
Size: 353371 Color: 6552
Size: 344501 Color: 6196
Size: 302128 Color: 4213

Bin 3156: 1 of cap free
Amount of items: 3
Items: 
Size: 351030 Color: 6471
Size: 351023 Color: 6470
Size: 297947 Color: 3971

Bin 3157: 1 of cap free
Amount of items: 3
Items: 
Size: 353110 Color: 6543
Size: 347916 Color: 6350
Size: 298974 Color: 4028

Bin 3158: 1 of cap free
Amount of items: 3
Items: 
Size: 337158 Color: 5888
Size: 335145 Color: 5817
Size: 327697 Color: 5462

Bin 3159: 1 of cap free
Amount of items: 3
Items: 
Size: 356227 Color: 6656
Size: 347457 Color: 6334
Size: 296316 Color: 3883

Bin 3160: 1 of cap free
Amount of items: 3
Items: 
Size: 356356 Color: 6664
Size: 344575 Color: 6201
Size: 299069 Color: 4040

Bin 3161: 1 of cap free
Amount of items: 3
Items: 
Size: 350191 Color: 6436
Size: 350169 Color: 6435
Size: 299640 Color: 4067

Bin 3162: 1 of cap free
Amount of items: 3
Items: 
Size: 356573 Color: 6673
Size: 349915 Color: 6427
Size: 293512 Color: 3703

Bin 3163: 1 of cap free
Amount of items: 3
Items: 
Size: 356650 Color: 6677
Size: 349869 Color: 6424
Size: 293481 Color: 3699

Bin 3164: 1 of cap free
Amount of items: 3
Items: 
Size: 349224 Color: 6401
Size: 349205 Color: 6399
Size: 301571 Color: 4174

Bin 3165: 1 of cap free
Amount of items: 3
Items: 
Size: 358494 Color: 6746
Size: 348562 Color: 6386
Size: 292944 Color: 3660

Bin 3166: 1 of cap free
Amount of items: 3
Items: 
Size: 360513 Color: 6827
Size: 348475 Color: 6383
Size: 291012 Color: 3530

Bin 3167: 1 of cap free
Amount of items: 3
Items: 
Size: 371267 Color: 7233
Size: 348461 Color: 6382
Size: 280272 Color: 2856

Bin 3168: 1 of cap free
Amount of items: 3
Items: 
Size: 359180 Color: 6776
Size: 348450 Color: 6379
Size: 292370 Color: 3625

Bin 3169: 1 of cap free
Amount of items: 3
Items: 
Size: 359072 Color: 6766
Size: 348604 Color: 6387
Size: 292324 Color: 3618

Bin 3170: 2 of cap free
Amount of items: 3
Items: 
Size: 365051 Color: 6988
Size: 347465 Color: 6335
Size: 287483 Color: 3331

Bin 3171: 2 of cap free
Amount of items: 3
Items: 
Size: 344931 Color: 6219
Size: 342531 Color: 6113
Size: 312537 Color: 4737

Bin 3172: 2 of cap free
Amount of items: 3
Items: 
Size: 368020 Color: 7120
Size: 321169 Color: 5156
Size: 310810 Color: 4669

Bin 3173: 2 of cap free
Amount of items: 3
Items: 
Size: 381908 Color: 7581
Size: 340746 Color: 6040
Size: 277345 Color: 2675

Bin 3174: 2 of cap free
Amount of items: 3
Items: 
Size: 357941 Color: 6725
Size: 326180 Color: 5392
Size: 315878 Color: 4914

Bin 3175: 2 of cap free
Amount of items: 3
Items: 
Size: 345863 Color: 6263
Size: 341242 Color: 6057
Size: 312894 Color: 4754

Bin 3176: 2 of cap free
Amount of items: 3
Items: 
Size: 366421 Color: 7041
Size: 349274 Color: 6404
Size: 284304 Color: 3121

Bin 3177: 2 of cap free
Amount of items: 3
Items: 
Size: 356443 Color: 6666
Size: 342935 Color: 6131
Size: 300621 Color: 4125

Bin 3178: 2 of cap free
Amount of items: 3
Items: 
Size: 375861 Color: 7400
Size: 344259 Color: 6186
Size: 279879 Color: 2829

Bin 3179: 2 of cap free
Amount of items: 3
Items: 
Size: 355233 Color: 6619
Size: 350870 Color: 6463
Size: 293896 Color: 3726

Bin 3180: 2 of cap free
Amount of items: 3
Items: 
Size: 377438 Color: 7459
Size: 336312 Color: 5858
Size: 286249 Color: 3234

Bin 3181: 2 of cap free
Amount of items: 3
Items: 
Size: 375651 Color: 7390
Size: 355822 Color: 6639
Size: 268526 Color: 1997

Bin 3182: 2 of cap free
Amount of items: 3
Items: 
Size: 389563 Color: 7808
Size: 325069 Color: 5343
Size: 285367 Color: 3183

Bin 3183: 2 of cap free
Amount of items: 3
Items: 
Size: 391484 Color: 7874
Size: 337033 Color: 5882
Size: 271482 Color: 2246

Bin 3184: 2 of cap free
Amount of items: 3
Items: 
Size: 400153 Color: 8118
Size: 335868 Color: 5838
Size: 263978 Color: 1613

Bin 3185: 2 of cap free
Amount of items: 3
Items: 
Size: 390665 Color: 7848
Size: 335801 Color: 5836
Size: 273533 Color: 2389

Bin 3186: 2 of cap free
Amount of items: 3
Items: 
Size: 397001 Color: 8026
Size: 332663 Color: 5697
Size: 270335 Color: 2146

Bin 3187: 2 of cap free
Amount of items: 3
Items: 
Size: 394676 Color: 7962
Size: 328025 Color: 5479
Size: 277298 Color: 2670

Bin 3188: 2 of cap free
Amount of items: 3
Items: 
Size: 348915 Color: 6393
Size: 343580 Color: 6152
Size: 307504 Color: 4493

Bin 3189: 2 of cap free
Amount of items: 3
Items: 
Size: 380311 Color: 7541
Size: 330672 Color: 5588
Size: 289016 Color: 3413

Bin 3190: 2 of cap free
Amount of items: 3
Items: 
Size: 391654 Color: 7883
Size: 332363 Color: 5680
Size: 275982 Color: 2577

Bin 3191: 2 of cap free
Amount of items: 3
Items: 
Size: 364157 Color: 6959
Size: 330248 Color: 5571
Size: 305594 Color: 4396

Bin 3192: 2 of cap free
Amount of items: 3
Items: 
Size: 363192 Color: 6928
Size: 333926 Color: 5758
Size: 302881 Color: 4266

Bin 3193: 2 of cap free
Amount of items: 3
Items: 
Size: 379092 Color: 7501
Size: 327223 Color: 5434
Size: 293684 Color: 3716

Bin 3194: 2 of cap free
Amount of items: 3
Items: 
Size: 388111 Color: 7770
Size: 327307 Color: 5438
Size: 284581 Color: 3135

Bin 3195: 2 of cap free
Amount of items: 3
Items: 
Size: 388037 Color: 7768
Size: 327078 Color: 5428
Size: 284884 Color: 3153

Bin 3196: 2 of cap free
Amount of items: 3
Items: 
Size: 395138 Color: 7972
Size: 326631 Color: 5410
Size: 278230 Color: 2726

Bin 3197: 2 of cap free
Amount of items: 3
Items: 
Size: 368138 Color: 7127
Size: 327405 Color: 5447
Size: 304456 Color: 4342

Bin 3198: 2 of cap free
Amount of items: 3
Items: 
Size: 364025 Color: 6955
Size: 342617 Color: 6122
Size: 293357 Color: 3687

Bin 3199: 2 of cap free
Amount of items: 3
Items: 
Size: 383853 Color: 7650
Size: 331305 Color: 5620
Size: 284841 Color: 3150

Bin 3200: 2 of cap free
Amount of items: 3
Items: 
Size: 360122 Color: 6810
Size: 341808 Color: 6079
Size: 298069 Color: 3977

Bin 3201: 2 of cap free
Amount of items: 3
Items: 
Size: 359096 Color: 6767
Size: 358933 Color: 6762
Size: 281970 Color: 2973

Bin 3202: 2 of cap free
Amount of items: 3
Items: 
Size: 356158 Color: 6653
Size: 356121 Color: 6652
Size: 287720 Color: 3343

Bin 3203: 2 of cap free
Amount of items: 3
Items: 
Size: 351844 Color: 6496
Size: 328235 Color: 5490
Size: 319920 Color: 5098

Bin 3204: 3 of cap free
Amount of items: 3
Items: 
Size: 342419 Color: 6110
Size: 329065 Color: 5525
Size: 328514 Color: 5506

Bin 3205: 3 of cap free
Amount of items: 3
Items: 
Size: 382532 Color: 7599
Size: 348513 Color: 6384
Size: 268953 Color: 2031

Bin 3206: 3 of cap free
Amount of items: 3
Items: 
Size: 365486 Color: 7008
Size: 346221 Color: 6282
Size: 288291 Color: 3374

Bin 3207: 3 of cap free
Amount of items: 3
Items: 
Size: 374647 Color: 7352
Size: 340271 Color: 6015
Size: 285080 Color: 3162

Bin 3208: 3 of cap free
Amount of items: 3
Items: 
Size: 391627 Color: 7882
Size: 328375 Color: 5498
Size: 279996 Color: 2840

Bin 3209: 3 of cap free
Amount of items: 3
Items: 
Size: 364655 Color: 6971
Size: 344077 Color: 6177
Size: 291266 Color: 3546

Bin 3210: 3 of cap free
Amount of items: 3
Items: 
Size: 359324 Color: 6782
Size: 347056 Color: 6319
Size: 293618 Color: 3711

Bin 3211: 3 of cap free
Amount of items: 3
Items: 
Size: 337659 Color: 5911
Size: 331312 Color: 5621
Size: 331027 Color: 5605

Bin 3212: 3 of cap free
Amount of items: 3
Items: 
Size: 359118 Color: 6770
Size: 353578 Color: 6561
Size: 287302 Color: 3317

Bin 3213: 3 of cap free
Amount of items: 3
Items: 
Size: 357132 Color: 6691
Size: 325666 Color: 5370
Size: 317200 Color: 4975

Bin 3214: 3 of cap free
Amount of items: 3
Items: 
Size: 356582 Color: 6676
Size: 329690 Color: 5550
Size: 313726 Color: 4789

Bin 3215: 3 of cap free
Amount of items: 3
Items: 
Size: 373012 Color: 7296
Size: 326905 Color: 5422
Size: 300081 Color: 4093

Bin 3216: 3 of cap free
Amount of items: 3
Items: 
Size: 372045 Color: 7265
Size: 325258 Color: 5352
Size: 302695 Color: 4250

Bin 3217: 3 of cap free
Amount of items: 3
Items: 
Size: 367989 Color: 7116
Size: 333529 Color: 5735
Size: 298480 Color: 4002

Bin 3218: 3 of cap free
Amount of items: 3
Items: 
Size: 397419 Color: 8038
Size: 327369 Color: 5444
Size: 275210 Color: 2519

Bin 3219: 3 of cap free
Amount of items: 3
Items: 
Size: 360912 Color: 6844
Size: 344590 Color: 6202
Size: 294496 Color: 3762

Bin 3220: 3 of cap free
Amount of items: 3
Items: 
Size: 408300 Color: 8318
Size: 306331 Color: 4429
Size: 285367 Color: 3182

Bin 3221: 3 of cap free
Amount of items: 3
Items: 
Size: 339990 Color: 6002
Size: 332925 Color: 5712
Size: 327083 Color: 5429

Bin 3222: 3 of cap free
Amount of items: 3
Items: 
Size: 354383 Color: 6589
Size: 346011 Color: 6273
Size: 299604 Color: 4065

Bin 3223: 3 of cap free
Amount of items: 3
Items: 
Size: 360879 Color: 6843
Size: 355355 Color: 6624
Size: 283764 Color: 3081

Bin 3224: 3 of cap free
Amount of items: 3
Items: 
Size: 352955 Color: 6537
Size: 347996 Color: 6356
Size: 299047 Color: 4038

Bin 3225: 4 of cap free
Amount of items: 3
Items: 
Size: 382707 Color: 7607
Size: 336197 Color: 5853
Size: 281093 Color: 2917

Bin 3226: 4 of cap free
Amount of items: 3
Items: 
Size: 368413 Color: 7135
Size: 317181 Color: 4974
Size: 314403 Color: 4831

Bin 3227: 4 of cap free
Amount of items: 3
Items: 
Size: 382541 Color: 7601
Size: 341815 Color: 6081
Size: 275641 Color: 2558

Bin 3228: 4 of cap free
Amount of items: 3
Items: 
Size: 352242 Color: 6516
Size: 327952 Color: 5473
Size: 319803 Color: 5091

Bin 3229: 4 of cap free
Amount of items: 3
Items: 
Size: 362096 Color: 6891
Size: 358803 Color: 6760
Size: 279098 Color: 2781

Bin 3230: 4 of cap free
Amount of items: 3
Items: 
Size: 379855 Color: 7526
Size: 356919 Color: 6685
Size: 263223 Color: 1548

Bin 3231: 4 of cap free
Amount of items: 3
Items: 
Size: 392416 Color: 7904
Size: 337623 Color: 5909
Size: 269958 Color: 2103

Bin 3232: 4 of cap free
Amount of items: 3
Items: 
Size: 391322 Color: 7868
Size: 346571 Color: 6297
Size: 262104 Color: 1434

Bin 3233: 4 of cap free
Amount of items: 3
Items: 
Size: 349600 Color: 6415
Size: 343694 Color: 6157
Size: 306703 Color: 4452

Bin 3234: 4 of cap free
Amount of items: 3
Items: 
Size: 359735 Color: 6799
Size: 336296 Color: 5856
Size: 303966 Color: 4313

Bin 3235: 4 of cap free
Amount of items: 3
Items: 
Size: 367410 Color: 7091
Size: 351162 Color: 6475
Size: 281425 Color: 2944

Bin 3236: 4 of cap free
Amount of items: 3
Items: 
Size: 349500 Color: 6410
Size: 344101 Color: 6178
Size: 306396 Color: 4434

Bin 3237: 5 of cap free
Amount of items: 3
Items: 
Size: 360263 Color: 6816
Size: 354133 Color: 6581
Size: 285600 Color: 3197

Bin 3238: 5 of cap free
Amount of items: 3
Items: 
Size: 404549 Color: 8239
Size: 321309 Color: 5167
Size: 274138 Color: 2444

Bin 3239: 5 of cap free
Amount of items: 3
Items: 
Size: 371926 Color: 7257
Size: 346858 Color: 6312
Size: 281212 Color: 2927

Bin 3240: 5 of cap free
Amount of items: 3
Items: 
Size: 372451 Color: 7279
Size: 346886 Color: 6314
Size: 280659 Color: 2889

Bin 3241: 5 of cap free
Amount of items: 3
Items: 
Size: 370292 Color: 7198
Size: 343799 Color: 6164
Size: 285905 Color: 3211

Bin 3242: 5 of cap free
Amount of items: 3
Items: 
Size: 352447 Color: 6526
Size: 348124 Color: 6365
Size: 299425 Color: 4057

Bin 3243: 6 of cap free
Amount of items: 3
Items: 
Size: 377579 Color: 7463
Size: 336526 Color: 5865
Size: 285890 Color: 3209

Bin 3244: 6 of cap free
Amount of items: 3
Items: 
Size: 364140 Color: 6958
Size: 346849 Color: 6310
Size: 289006 Color: 3412

Bin 3245: 6 of cap free
Amount of items: 3
Items: 
Size: 358688 Color: 6754
Size: 355389 Color: 6625
Size: 285918 Color: 3213

Bin 3246: 6 of cap free
Amount of items: 3
Items: 
Size: 363281 Color: 6929
Size: 332298 Color: 5676
Size: 304416 Color: 4340

Bin 3247: 7 of cap free
Amount of items: 3
Items: 
Size: 355677 Color: 6636
Size: 328938 Color: 5521
Size: 315379 Color: 4893

Bin 3248: 7 of cap free
Amount of items: 3
Items: 
Size: 397147 Color: 8031
Size: 308757 Color: 4562
Size: 294090 Color: 3738

Bin 3249: 7 of cap free
Amount of items: 3
Items: 
Size: 390961 Color: 7855
Size: 338057 Color: 5927
Size: 270976 Color: 2203

Bin 3250: 7 of cap free
Amount of items: 3
Items: 
Size: 347448 Color: 6333
Size: 341362 Color: 6060
Size: 311184 Color: 4682

Bin 3251: 8 of cap free
Amount of items: 3
Items: 
Size: 341708 Color: 6071
Size: 340599 Color: 6034
Size: 317686 Color: 4998

Bin 3252: 8 of cap free
Amount of items: 3
Items: 
Size: 356579 Color: 6675
Size: 351598 Color: 6488
Size: 291816 Color: 3582

Bin 3253: 8 of cap free
Amount of items: 3
Items: 
Size: 399105 Color: 8098
Size: 328087 Color: 5483
Size: 272801 Color: 2341

Bin 3254: 8 of cap free
Amount of items: 3
Items: 
Size: 402694 Color: 8192
Size: 298998 Color: 4035
Size: 298301 Color: 3991

Bin 3255: 9 of cap free
Amount of items: 3
Items: 
Size: 388401 Color: 7777
Size: 323736 Color: 5278
Size: 287855 Color: 3349

Bin 3256: 9 of cap free
Amount of items: 3
Items: 
Size: 388962 Color: 7786
Size: 329379 Color: 5537
Size: 281651 Color: 2958

Bin 3257: 9 of cap free
Amount of items: 3
Items: 
Size: 339268 Color: 5978
Size: 330394 Color: 5576
Size: 330330 Color: 5574

Bin 3258: 9 of cap free
Amount of items: 3
Items: 
Size: 359466 Color: 6784
Size: 350781 Color: 6457
Size: 289745 Color: 3454

Bin 3259: 9 of cap free
Amount of items: 3
Items: 
Size: 355548 Color: 6632
Size: 355344 Color: 6622
Size: 289100 Color: 3418

Bin 3260: 10 of cap free
Amount of items: 3
Items: 
Size: 354610 Color: 6598
Size: 325551 Color: 5364
Size: 319830 Color: 5092

Bin 3261: 10 of cap free
Amount of items: 3
Items: 
Size: 390857 Color: 7851
Size: 332307 Color: 5677
Size: 276827 Color: 2631

Bin 3262: 10 of cap free
Amount of items: 3
Items: 
Size: 344529 Color: 6197
Size: 336871 Color: 5876
Size: 318591 Color: 5036

Bin 3263: 11 of cap free
Amount of items: 3
Items: 
Size: 337515 Color: 5902
Size: 335422 Color: 5829
Size: 327053 Color: 5427

Bin 3264: 11 of cap free
Amount of items: 3
Items: 
Size: 361062 Color: 6847
Size: 360344 Color: 6819
Size: 278584 Color: 2753

Bin 3265: 11 of cap free
Amount of items: 3
Items: 
Size: 365231 Color: 7002
Size: 335444 Color: 5830
Size: 299315 Color: 4054

Bin 3266: 11 of cap free
Amount of items: 3
Items: 
Size: 364122 Color: 6957
Size: 342326 Color: 6103
Size: 293542 Color: 3706

Bin 3267: 12 of cap free
Amount of items: 3
Items: 
Size: 390242 Color: 7831
Size: 345522 Color: 6240
Size: 264225 Color: 1632

Bin 3268: 13 of cap free
Amount of items: 3
Items: 
Size: 355160 Color: 6617
Size: 325913 Color: 5378
Size: 318915 Color: 5045

Bin 3269: 14 of cap free
Amount of items: 3
Items: 
Size: 352320 Color: 6521
Size: 346070 Color: 6276
Size: 301597 Color: 4177

Bin 3270: 16 of cap free
Amount of items: 3
Items: 
Size: 405143 Color: 8247
Size: 297798 Color: 3963
Size: 297044 Color: 3922

Bin 3271: 17 of cap free
Amount of items: 3
Items: 
Size: 358038 Color: 6730
Size: 356714 Color: 6679
Size: 285232 Color: 3170

Bin 3272: 17 of cap free
Amount of items: 3
Items: 
Size: 395250 Color: 7975
Size: 338193 Color: 5932
Size: 266541 Color: 1844

Bin 3273: 18 of cap free
Amount of items: 3
Items: 
Size: 406210 Color: 8276
Size: 307422 Color: 4489
Size: 286351 Color: 3243

Bin 3274: 18 of cap free
Amount of items: 3
Items: 
Size: 366727 Color: 7059
Size: 353323 Color: 6551
Size: 279933 Color: 2832

Bin 3275: 21 of cap free
Amount of items: 3
Items: 
Size: 374085 Color: 7338
Size: 339122 Color: 5971
Size: 286773 Color: 3278

Bin 3276: 22 of cap free
Amount of items: 3
Items: 
Size: 362701 Color: 6910
Size: 321462 Color: 5178
Size: 315816 Color: 4904

Bin 3277: 23 of cap free
Amount of items: 3
Items: 
Size: 369857 Color: 7183
Size: 326145 Color: 5390
Size: 303976 Color: 4315

Bin 3278: 23 of cap free
Amount of items: 3
Items: 
Size: 383767 Color: 7648
Size: 327541 Color: 5452
Size: 288670 Color: 3395

Bin 3279: 23 of cap free
Amount of items: 3
Items: 
Size: 392513 Color: 7909
Size: 313726 Color: 4790
Size: 293739 Color: 3720

Bin 3280: 24 of cap free
Amount of items: 3
Items: 
Size: 406388 Color: 8280
Size: 309487 Color: 4600
Size: 284102 Color: 3109

Bin 3281: 24 of cap free
Amount of items: 3
Items: 
Size: 405554 Color: 8264
Size: 302023 Color: 4205
Size: 292400 Color: 3629

Bin 3282: 25 of cap free
Amount of items: 3
Items: 
Size: 407361 Color: 8302
Size: 321048 Color: 5150
Size: 271567 Color: 2255

Bin 3283: 26 of cap free
Amount of items: 3
Items: 
Size: 360429 Color: 6823
Size: 336152 Color: 5851
Size: 303394 Color: 4287

Bin 3284: 29 of cap free
Amount of items: 3
Items: 
Size: 356296 Color: 6659
Size: 355595 Color: 6634
Size: 288081 Color: 3362

Bin 3285: 30 of cap free
Amount of items: 3
Items: 
Size: 358723 Color: 6756
Size: 353454 Color: 6557
Size: 287794 Color: 3347

Bin 3286: 30 of cap free
Amount of items: 3
Items: 
Size: 396266 Color: 8004
Size: 316416 Color: 4943
Size: 287289 Color: 3316

Bin 3287: 31 of cap free
Amount of items: 3
Items: 
Size: 349765 Color: 6421
Size: 326368 Color: 5398
Size: 323837 Color: 5286

Bin 3288: 31 of cap free
Amount of items: 3
Items: 
Size: 367136 Color: 7076
Size: 338515 Color: 5944
Size: 294319 Color: 3753

Bin 3289: 32 of cap free
Amount of items: 3
Items: 
Size: 350055 Color: 6429
Size: 335961 Color: 5843
Size: 313953 Color: 4809

Bin 3290: 33 of cap free
Amount of items: 3
Items: 
Size: 356822 Color: 6683
Size: 347429 Color: 6332
Size: 295717 Color: 3837

Bin 3291: 33 of cap free
Amount of items: 3
Items: 
Size: 359541 Color: 6791
Size: 354029 Color: 6578
Size: 286398 Color: 3247

Bin 3292: 40 of cap free
Amount of items: 3
Items: 
Size: 355826 Color: 6640
Size: 339322 Color: 5981
Size: 304813 Color: 4366

Bin 3293: 40 of cap free
Amount of items: 3
Items: 
Size: 397632 Color: 8050
Size: 325751 Color: 5372
Size: 276578 Color: 2613

Bin 3294: 40 of cap free
Amount of items: 3
Items: 
Size: 335095 Color: 5806
Size: 332913 Color: 5711
Size: 331953 Color: 5658

Bin 3295: 43 of cap free
Amount of items: 3
Items: 
Size: 395455 Color: 7981
Size: 328476 Color: 5504
Size: 276027 Color: 2581

Bin 3296: 45 of cap free
Amount of items: 3
Items: 
Size: 355548 Color: 6633
Size: 342583 Color: 6115
Size: 301825 Color: 4189

Bin 3297: 46 of cap free
Amount of items: 3
Items: 
Size: 402522 Color: 8188
Size: 328024 Color: 5478
Size: 269409 Color: 2065

Bin 3298: 48 of cap free
Amount of items: 3
Items: 
Size: 357577 Color: 6709
Size: 327004 Color: 5425
Size: 315372 Color: 4892

Bin 3299: 51 of cap free
Amount of items: 3
Items: 
Size: 368766 Color: 7147
Size: 350217 Color: 6439
Size: 280967 Color: 2905

Bin 3300: 52 of cap free
Amount of items: 3
Items: 
Size: 367550 Color: 7097
Size: 321745 Color: 5190
Size: 310654 Color: 4660

Bin 3301: 54 of cap free
Amount of items: 3
Items: 
Size: 353245 Color: 6548
Size: 339602 Color: 5989
Size: 307100 Color: 4476

Bin 3302: 57 of cap free
Amount of items: 3
Items: 
Size: 397448 Color: 8042
Size: 342115 Color: 6095
Size: 260381 Color: 1280

Bin 3303: 58 of cap free
Amount of items: 3
Items: 
Size: 344062 Color: 6176
Size: 329184 Color: 5530
Size: 326697 Color: 5415

Bin 3304: 58 of cap free
Amount of items: 3
Items: 
Size: 357403 Color: 6701
Size: 327854 Color: 5469
Size: 314686 Color: 4850

Bin 3305: 60 of cap free
Amount of items: 3
Items: 
Size: 390030 Color: 7823
Size: 321254 Color: 5162
Size: 288657 Color: 3393

Bin 3306: 69 of cap free
Amount of items: 3
Items: 
Size: 365038 Color: 6985
Size: 330530 Color: 5583
Size: 304364 Color: 4338

Bin 3307: 78 of cap free
Amount of items: 3
Items: 
Size: 367997 Color: 7118
Size: 348710 Color: 6390
Size: 283216 Color: 3054

Bin 3308: 89 of cap free
Amount of items: 3
Items: 
Size: 364937 Color: 6982
Size: 317521 Color: 4990
Size: 317454 Color: 4986

Bin 3309: 93 of cap free
Amount of items: 3
Items: 
Size: 366919 Color: 7068
Size: 326267 Color: 5394
Size: 306722 Color: 4453

Bin 3310: 94 of cap free
Amount of items: 3
Items: 
Size: 372171 Color: 7272
Size: 327285 Color: 5437
Size: 300451 Color: 4119

Bin 3311: 98 of cap free
Amount of items: 3
Items: 
Size: 351904 Color: 6500
Size: 339676 Color: 5991
Size: 308323 Color: 4533

Bin 3312: 99 of cap free
Amount of items: 3
Items: 
Size: 339806 Color: 5996
Size: 332475 Color: 5689
Size: 327621 Color: 5458

Bin 3313: 106 of cap free
Amount of items: 3
Items: 
Size: 361789 Color: 6878
Size: 351241 Color: 6480
Size: 286865 Color: 3293

Bin 3314: 125 of cap free
Amount of items: 3
Items: 
Size: 367291 Color: 7083
Size: 334246 Color: 5772
Size: 298339 Color: 3993

Bin 3315: 134 of cap free
Amount of items: 3
Items: 
Size: 371832 Color: 7251
Size: 342042 Color: 6089
Size: 285993 Color: 3218

Bin 3316: 147 of cap free
Amount of items: 3
Items: 
Size: 377126 Color: 7449
Size: 332688 Color: 5702
Size: 290040 Color: 3475

Bin 3317: 151 of cap free
Amount of items: 3
Items: 
Size: 356029 Color: 6649
Size: 342384 Color: 6106
Size: 301437 Color: 4165

Bin 3318: 158 of cap free
Amount of items: 3
Items: 
Size: 380788 Color: 7547
Size: 341254 Color: 6058
Size: 277801 Color: 2704

Bin 3319: 168 of cap free
Amount of items: 3
Items: 
Size: 389676 Color: 7809
Size: 318009 Color: 5017
Size: 292148 Color: 3606

Bin 3320: 176 of cap free
Amount of items: 3
Items: 
Size: 361728 Color: 6877
Size: 332172 Color: 5672
Size: 305925 Color: 4415

Bin 3321: 184 of cap free
Amount of items: 3
Items: 
Size: 391084 Color: 7859
Size: 325201 Color: 5349
Size: 283532 Color: 3068

Bin 3322: 188 of cap free
Amount of items: 3
Items: 
Size: 398127 Color: 8063
Size: 315357 Color: 4891
Size: 286329 Color: 3240

Bin 3323: 365 of cap free
Amount of items: 3
Items: 
Size: 359285 Color: 6781
Size: 355023 Color: 6614
Size: 285328 Color: 3179

Bin 3324: 643 of cap free
Amount of items: 3
Items: 
Size: 375821 Color: 7398
Size: 330219 Color: 5570
Size: 293318 Color: 3684

Bin 3325: 783 of cap free
Amount of items: 3
Items: 
Size: 353723 Color: 6567
Size: 346516 Color: 6294
Size: 298979 Color: 4030

Bin 3326: 852 of cap free
Amount of items: 3
Items: 
Size: 403340 Color: 8205
Size: 319246 Color: 5060
Size: 276563 Color: 2611

Bin 3327: 1093 of cap free
Amount of items: 3
Items: 
Size: 401106 Color: 8148
Size: 309544 Color: 4602
Size: 288258 Color: 3372

Bin 3328: 1319 of cap free
Amount of items: 3
Items: 
Size: 366440 Color: 7042
Size: 320692 Color: 5136
Size: 311550 Color: 4696

Bin 3329: 1962 of cap free
Amount of items: 3
Items: 
Size: 362275 Color: 6894
Size: 333642 Color: 5742
Size: 302122 Color: 4212

Bin 3330: 3133 of cap free
Amount of items: 3
Items: 
Size: 351849 Color: 6497
Size: 346434 Color: 6293
Size: 298585 Color: 4006

Bin 3331: 8069 of cap free
Amount of items: 3
Items: 
Size: 359775 Color: 6800
Size: 316601 Color: 4952
Size: 315556 Color: 4898

Bin 3332: 11346 of cap free
Amount of items: 3
Items: 
Size: 358712 Color: 6755
Size: 331921 Color: 5656
Size: 298022 Color: 3973

Bin 3333: 234625 of cap free
Amount of items: 2
Items: 
Size: 383189 Color: 7632
Size: 382187 Color: 7588

Bin 3334: 343578 of cap free
Amount of items: 2
Items: 
Size: 319301 Color: 5063
Size: 337122 Color: 5886

Bin 3335: 388256 of cap free
Amount of items: 2
Items: 
Size: 284524 Color: 3132
Size: 327221 Color: 5433

Total size: 3334003334
Total free space: 1000001

