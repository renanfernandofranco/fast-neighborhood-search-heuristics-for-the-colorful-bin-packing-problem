Capicity Bin: 8264
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 10
Items: 
Size: 1289 Color: 1
Size: 1151 Color: 1
Size: 1111 Color: 1
Size: 1098 Color: 4
Size: 841 Color: 0
Size: 802 Color: 0
Size: 756 Color: 2
Size: 468 Color: 3
Size: 448 Color: 3
Size: 300 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 1
Size: 3764 Color: 1
Size: 184 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4516 Color: 0
Size: 3584 Color: 1
Size: 164 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 4
Size: 2492 Color: 1
Size: 496 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5425 Color: 0
Size: 2367 Color: 1
Size: 472 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5574 Color: 1
Size: 2518 Color: 4
Size: 172 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 1
Size: 2284 Color: 2
Size: 304 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5749 Color: 4
Size: 2097 Color: 1
Size: 418 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6236 Color: 1
Size: 1862 Color: 2
Size: 166 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6245 Color: 0
Size: 1233 Color: 1
Size: 786 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 2
Size: 1518 Color: 0
Size: 334 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6446 Color: 3
Size: 1114 Color: 1
Size: 704 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6580 Color: 0
Size: 1548 Color: 1
Size: 136 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 1
Size: 1284 Color: 3
Size: 300 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6719 Color: 0
Size: 977 Color: 3
Size: 568 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6734 Color: 2
Size: 1018 Color: 4
Size: 512 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6793 Color: 1
Size: 1227 Color: 3
Size: 244 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 1
Size: 826 Color: 2
Size: 536 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6963 Color: 1
Size: 813 Color: 0
Size: 488 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 1
Size: 878 Color: 2
Size: 416 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 4
Size: 1054 Color: 4
Size: 232 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7002 Color: 2
Size: 664 Color: 1
Size: 598 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 1
Size: 680 Color: 0
Size: 476 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7141 Color: 1
Size: 923 Color: 2
Size: 200 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7164 Color: 1
Size: 684 Color: 2
Size: 416 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 1
Size: 726 Color: 0
Size: 356 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7154 Color: 3
Size: 924 Color: 1
Size: 186 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 3
Size: 908 Color: 2
Size: 176 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7236 Color: 4
Size: 724 Color: 4
Size: 304 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7257 Color: 4
Size: 821 Color: 1
Size: 186 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7281 Color: 1
Size: 751 Color: 3
Size: 232 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 2
Size: 854 Color: 1
Size: 136 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7309 Color: 0
Size: 783 Color: 1
Size: 172 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7322 Color: 1
Size: 782 Color: 3
Size: 160 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7354 Color: 3
Size: 684 Color: 2
Size: 226 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7364 Color: 0
Size: 844 Color: 1
Size: 56 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7380 Color: 3
Size: 512 Color: 2
Size: 372 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7388 Color: 0
Size: 732 Color: 1
Size: 144 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7404 Color: 4
Size: 716 Color: 0
Size: 144 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7412 Color: 3
Size: 684 Color: 0
Size: 168 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7428 Color: 0
Size: 688 Color: 1
Size: 148 Color: 3

Bin 42: 1 of cap free
Amount of items: 6
Items: 
Size: 4133 Color: 2
Size: 1334 Color: 1
Size: 1320 Color: 1
Size: 1124 Color: 0
Size: 196 Color: 0
Size: 156 Color: 4

Bin 43: 1 of cap free
Amount of items: 4
Items: 
Size: 4134 Color: 1
Size: 3443 Color: 0
Size: 542 Color: 1
Size: 144 Color: 3

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 4681 Color: 3
Size: 3438 Color: 1
Size: 144 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5362 Color: 4
Size: 2721 Color: 2
Size: 180 Color: 4

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5409 Color: 4
Size: 2716 Color: 1
Size: 138 Color: 3

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 3
Size: 2381 Color: 1
Size: 152 Color: 4

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6414 Color: 2
Size: 1665 Color: 1
Size: 184 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6459 Color: 4
Size: 1606 Color: 1
Size: 198 Color: 3

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6619 Color: 1
Size: 1372 Color: 4
Size: 272 Color: 0

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 2
Size: 1379 Color: 1
Size: 264 Color: 4

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6686 Color: 2
Size: 1397 Color: 1
Size: 180 Color: 2

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 1
Size: 1242 Color: 2
Size: 260 Color: 2

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6852 Color: 1
Size: 1131 Color: 2
Size: 280 Color: 3

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6907 Color: 1
Size: 982 Color: 0
Size: 374 Color: 2

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 4
Size: 1116 Color: 1
Size: 264 Color: 0

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 7069 Color: 2
Size: 964 Color: 1
Size: 230 Color: 2

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 2
Size: 937 Color: 1
Size: 236 Color: 4

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 7201 Color: 0
Size: 986 Color: 2
Size: 76 Color: 1

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 7359 Color: 1
Size: 700 Color: 2
Size: 204 Color: 1

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5012 Color: 2
Size: 2962 Color: 2
Size: 288 Color: 1

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 0
Size: 2108 Color: 3
Size: 158 Color: 1

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6027 Color: 1
Size: 2091 Color: 0
Size: 144 Color: 3

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 6605 Color: 1
Size: 1505 Color: 4
Size: 152 Color: 4

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 6648 Color: 1
Size: 1278 Color: 4
Size: 336 Color: 3

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 7082 Color: 4
Size: 1180 Color: 2

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 7330 Color: 2
Size: 868 Color: 3
Size: 64 Color: 0

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 5001 Color: 3
Size: 3124 Color: 1
Size: 136 Color: 2

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5850 Color: 1
Size: 1671 Color: 0
Size: 740 Color: 0

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 6019 Color: 4
Size: 2242 Color: 3

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 1
Size: 1361 Color: 2
Size: 790 Color: 4

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 7260 Color: 2
Size: 1000 Color: 3

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 7093 Color: 2
Size: 1166 Color: 3

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 7238 Color: 0
Size: 1021 Color: 4

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 7418 Color: 1
Size: 841 Color: 2

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 1
Size: 1511 Color: 3
Size: 480 Color: 3

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 3
Size: 1679 Color: 1
Size: 320 Color: 2

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 3
Size: 902 Color: 2
Size: 336 Color: 1

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 7214 Color: 0
Size: 1044 Color: 4

Bin 80: 7 of cap free
Amount of items: 3
Items: 
Size: 5009 Color: 3
Size: 3104 Color: 4
Size: 144 Color: 2

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 5174 Color: 3
Size: 2707 Color: 1
Size: 376 Color: 4

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 0
Size: 1730 Color: 2

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 6732 Color: 2
Size: 1524 Color: 4

Bin 84: 8 of cap free
Amount of items: 3
Items: 
Size: 7039 Color: 0
Size: 797 Color: 1
Size: 420 Color: 0

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 7137 Color: 0
Size: 1119 Color: 4

Bin 86: 9 of cap free
Amount of items: 3
Items: 
Size: 4262 Color: 0
Size: 3441 Color: 1
Size: 552 Color: 4

Bin 87: 9 of cap free
Amount of items: 2
Items: 
Size: 6785 Color: 0
Size: 1470 Color: 4

Bin 88: 9 of cap free
Amount of items: 2
Items: 
Size: 7117 Color: 4
Size: 1138 Color: 0

Bin 89: 9 of cap free
Amount of items: 3
Items: 
Size: 7289 Color: 2
Size: 854 Color: 4
Size: 112 Color: 3

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 7298 Color: 2
Size: 957 Color: 4

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 7004 Color: 4
Size: 1248 Color: 3

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 7394 Color: 4
Size: 858 Color: 2

Bin 93: 12 of cap free
Amount of items: 2
Items: 
Size: 7365 Color: 2
Size: 887 Color: 0

Bin 94: 13 of cap free
Amount of items: 3
Items: 
Size: 6190 Color: 3
Size: 1865 Color: 0
Size: 196 Color: 1

Bin 95: 13 of cap free
Amount of items: 2
Items: 
Size: 7325 Color: 4
Size: 926 Color: 2

Bin 96: 14 of cap free
Amount of items: 3
Items: 
Size: 5417 Color: 1
Size: 2385 Color: 3
Size: 448 Color: 0

Bin 97: 15 of cap free
Amount of items: 2
Items: 
Size: 5876 Color: 3
Size: 2373 Color: 2

Bin 98: 15 of cap free
Amount of items: 2
Items: 
Size: 6451 Color: 4
Size: 1798 Color: 0

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 3
Size: 1383 Color: 2

Bin 100: 15 of cap free
Amount of items: 2
Items: 
Size: 6931 Color: 3
Size: 1318 Color: 0

Bin 101: 17 of cap free
Amount of items: 2
Items: 
Size: 6251 Color: 2
Size: 1996 Color: 0

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 7306 Color: 3
Size: 941 Color: 2

Bin 103: 18 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 2
Size: 1176 Color: 3
Size: 24 Color: 2

Bin 104: 19 of cap free
Amount of items: 3
Items: 
Size: 4135 Color: 1
Size: 3422 Color: 3
Size: 688 Color: 2

Bin 105: 19 of cap free
Amount of items: 2
Items: 
Size: 5532 Color: 2
Size: 2713 Color: 0

Bin 106: 22 of cap free
Amount of items: 2
Items: 
Size: 6666 Color: 2
Size: 1576 Color: 0

Bin 107: 22 of cap free
Amount of items: 2
Items: 
Size: 7157 Color: 4
Size: 1085 Color: 2

Bin 108: 35 of cap free
Amount of items: 2
Items: 
Size: 6924 Color: 4
Size: 1305 Color: 0

Bin 109: 38 of cap free
Amount of items: 2
Items: 
Size: 6212 Color: 2
Size: 2014 Color: 0

Bin 110: 39 of cap free
Amount of items: 3
Items: 
Size: 6765 Color: 2
Size: 1404 Color: 0
Size: 56 Color: 3

Bin 111: 40 of cap free
Amount of items: 2
Items: 
Size: 6436 Color: 2
Size: 1788 Color: 4

Bin 112: 40 of cap free
Amount of items: 2
Items: 
Size: 6774 Color: 0
Size: 1450 Color: 2

Bin 113: 42 of cap free
Amount of items: 3
Items: 
Size: 4730 Color: 0
Size: 3298 Color: 3
Size: 194 Color: 1

Bin 114: 44 of cap free
Amount of items: 27
Items: 
Size: 542 Color: 1
Size: 540 Color: 0
Size: 448 Color: 3
Size: 400 Color: 2
Size: 392 Color: 3
Size: 376 Color: 3
Size: 372 Color: 4
Size: 344 Color: 4
Size: 334 Color: 3
Size: 332 Color: 4
Size: 308 Color: 2
Size: 302 Color: 1
Size: 292 Color: 4
Size: 276 Color: 2
Size: 274 Color: 1
Size: 256 Color: 0
Size: 252 Color: 1
Size: 248 Color: 4
Size: 248 Color: 3
Size: 248 Color: 0
Size: 224 Color: 2
Size: 222 Color: 2
Size: 216 Color: 2
Size: 216 Color: 0
Size: 192 Color: 0
Size: 190 Color: 0
Size: 176 Color: 3

Bin 115: 49 of cap free
Amount of items: 2
Items: 
Size: 4673 Color: 1
Size: 3542 Color: 2

Bin 116: 50 of cap free
Amount of items: 2
Items: 
Size: 4142 Color: 3
Size: 4072 Color: 1

Bin 117: 52 of cap free
Amount of items: 3
Items: 
Size: 4158 Color: 0
Size: 3430 Color: 1
Size: 624 Color: 4

Bin 118: 55 of cap free
Amount of items: 2
Items: 
Size: 6338 Color: 0
Size: 1871 Color: 4

Bin 119: 56 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 4
Size: 2578 Color: 1
Size: 474 Color: 2

Bin 120: 63 of cap free
Amount of items: 3
Items: 
Size: 6611 Color: 2
Size: 1542 Color: 3
Size: 48 Color: 4

Bin 121: 65 of cap free
Amount of items: 4
Items: 
Size: 4150 Color: 0
Size: 1892 Color: 1
Size: 1683 Color: 4
Size: 474 Color: 2

Bin 122: 69 of cap free
Amount of items: 3
Items: 
Size: 4938 Color: 4
Size: 2993 Color: 2
Size: 264 Color: 0

Bin 123: 70 of cap free
Amount of items: 2
Items: 
Size: 6502 Color: 0
Size: 1692 Color: 3

Bin 124: 85 of cap free
Amount of items: 2
Items: 
Size: 5757 Color: 2
Size: 2422 Color: 4

Bin 125: 87 of cap free
Amount of items: 2
Items: 
Size: 5403 Color: 4
Size: 2774 Color: 3

Bin 126: 90 of cap free
Amount of items: 2
Items: 
Size: 4836 Color: 3
Size: 3338 Color: 1

Bin 127: 92 of cap free
Amount of items: 3
Items: 
Size: 4140 Color: 4
Size: 3444 Color: 0
Size: 588 Color: 3

Bin 128: 102 of cap free
Amount of items: 3
Items: 
Size: 4714 Color: 2
Size: 2860 Color: 4
Size: 588 Color: 0

Bin 129: 102 of cap free
Amount of items: 2
Items: 
Size: 5566 Color: 3
Size: 2596 Color: 0

Bin 130: 112 of cap free
Amount of items: 2
Items: 
Size: 5740 Color: 4
Size: 2412 Color: 2

Bin 131: 120 of cap free
Amount of items: 2
Items: 
Size: 6030 Color: 0
Size: 2114 Color: 3

Bin 132: 132 of cap free
Amount of items: 2
Items: 
Size: 5017 Color: 4
Size: 3115 Color: 0

Bin 133: 6086 of cap free
Amount of items: 12
Items: 
Size: 246 Color: 4
Size: 208 Color: 4
Size: 200 Color: 2
Size: 192 Color: 2
Size: 184 Color: 0
Size: 176 Color: 4
Size: 176 Color: 2
Size: 162 Color: 0
Size: 162 Color: 0
Size: 160 Color: 3
Size: 156 Color: 3
Size: 156 Color: 3

Total size: 1090848
Total free space: 8264

