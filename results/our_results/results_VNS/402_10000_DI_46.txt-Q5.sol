Capicity Bin: 7360
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4280 Color: 4
Size: 2872 Color: 2
Size: 208 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 1
Size: 2568 Color: 1
Size: 240 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4662 Color: 4
Size: 2494 Color: 0
Size: 204 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5086 Color: 1
Size: 1898 Color: 0
Size: 376 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5108 Color: 4
Size: 2132 Color: 1
Size: 120 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5165 Color: 2
Size: 1831 Color: 3
Size: 364 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5592 Color: 0
Size: 1564 Color: 3
Size: 204 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5724 Color: 3
Size: 1218 Color: 2
Size: 418 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 2
Size: 1474 Color: 3
Size: 172 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5864 Color: 2
Size: 1244 Color: 3
Size: 252 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 2
Size: 744 Color: 3
Size: 714 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5896 Color: 3
Size: 1320 Color: 2
Size: 144 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5987 Color: 0
Size: 1137 Color: 0
Size: 236 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 0
Size: 1208 Color: 3
Size: 148 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6102 Color: 3
Size: 770 Color: 2
Size: 488 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6131 Color: 3
Size: 945 Color: 4
Size: 284 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6221 Color: 3
Size: 723 Color: 2
Size: 416 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6280 Color: 3
Size: 984 Color: 2
Size: 96 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 3
Size: 985 Color: 0
Size: 74 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6323 Color: 2
Size: 741 Color: 3
Size: 296 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6388 Color: 3
Size: 812 Color: 1
Size: 160 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6387 Color: 1
Size: 781 Color: 4
Size: 192 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6434 Color: 3
Size: 786 Color: 4
Size: 140 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6418 Color: 1
Size: 808 Color: 3
Size: 134 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6466 Color: 4
Size: 550 Color: 3
Size: 344 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6472 Color: 3
Size: 684 Color: 1
Size: 204 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 3
Size: 728 Color: 2
Size: 128 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6508 Color: 2
Size: 716 Color: 3
Size: 136 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6540 Color: 3
Size: 484 Color: 0
Size: 336 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 1
Size: 512 Color: 2
Size: 276 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 2
Size: 630 Color: 3
Size: 144 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6622 Color: 1
Size: 452 Color: 2
Size: 286 Color: 3

Bin 33: 1 of cap free
Amount of items: 4
Items: 
Size: 3692 Color: 0
Size: 2752 Color: 4
Size: 675 Color: 2
Size: 240 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 4190 Color: 4
Size: 2833 Color: 0
Size: 336 Color: 3

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 4776 Color: 3
Size: 2427 Color: 4
Size: 156 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 4857 Color: 0
Size: 1928 Color: 2
Size: 574 Color: 3

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5088 Color: 4
Size: 2087 Color: 1
Size: 184 Color: 3

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5175 Color: 4
Size: 1884 Color: 2
Size: 300 Color: 3

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5437 Color: 0
Size: 1668 Color: 0
Size: 254 Color: 4

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 2
Size: 958 Color: 0
Size: 941 Color: 3

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5528 Color: 2
Size: 1603 Color: 1
Size: 228 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5635 Color: 0
Size: 1268 Color: 3
Size: 456 Color: 4

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5784 Color: 4
Size: 1127 Color: 3
Size: 448 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5827 Color: 1
Size: 1364 Color: 3
Size: 168 Color: 4

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 2
Size: 1291 Color: 4
Size: 88 Color: 3

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 3
Size: 1062 Color: 2
Size: 288 Color: 2

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6070 Color: 3
Size: 1145 Color: 4
Size: 144 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 3
Size: 811 Color: 0
Size: 416 Color: 2

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 2
Size: 1031 Color: 0
Size: 144 Color: 3

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6307 Color: 0
Size: 660 Color: 4
Size: 392 Color: 3

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6403 Color: 0
Size: 560 Color: 3
Size: 396 Color: 1

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 6408 Color: 1
Size: 951 Color: 2

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6471 Color: 3
Size: 560 Color: 4
Size: 328 Color: 0

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6551 Color: 0
Size: 608 Color: 3
Size: 200 Color: 4

Bin 55: 2 of cap free
Amount of items: 24
Items: 
Size: 528 Color: 4
Size: 528 Color: 3
Size: 422 Color: 1
Size: 420 Color: 4
Size: 416 Color: 3
Size: 400 Color: 0
Size: 384 Color: 0
Size: 368 Color: 3
Size: 364 Color: 3
Size: 304 Color: 1
Size: 272 Color: 0
Size: 272 Color: 0
Size: 264 Color: 1
Size: 256 Color: 1
Size: 248 Color: 2
Size: 240 Color: 2
Size: 240 Color: 2
Size: 224 Color: 3
Size: 224 Color: 0
Size: 212 Color: 3
Size: 208 Color: 2
Size: 192 Color: 2
Size: 188 Color: 1
Size: 184 Color: 2

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 4610 Color: 4
Size: 2284 Color: 2
Size: 464 Color: 1

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 4964 Color: 4
Size: 2168 Color: 3
Size: 226 Color: 4

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5048 Color: 2
Size: 2118 Color: 3
Size: 192 Color: 1

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5290 Color: 0
Size: 1136 Color: 0
Size: 932 Color: 4

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5390 Color: 1
Size: 1488 Color: 3
Size: 480 Color: 2

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5844 Color: 2
Size: 1386 Color: 1
Size: 128 Color: 3

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 6493 Color: 4
Size: 865 Color: 2

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 4
Size: 2678 Color: 4
Size: 208 Color: 3

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 5651 Color: 1
Size: 1586 Color: 3
Size: 120 Color: 0

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 0
Size: 1374 Color: 2
Size: 140 Color: 4

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6423 Color: 2
Size: 934 Color: 1

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 5492 Color: 3
Size: 1592 Color: 1
Size: 272 Color: 4

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 5876 Color: 1
Size: 1480 Color: 4

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 6168 Color: 2
Size: 1156 Color: 4
Size: 32 Color: 4

Bin 70: 4 of cap free
Amount of items: 4
Items: 
Size: 6226 Color: 2
Size: 1050 Color: 0
Size: 48 Color: 4
Size: 32 Color: 2

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 6506 Color: 2
Size: 850 Color: 4

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 4455 Color: 1
Size: 2712 Color: 4
Size: 188 Color: 3

Bin 73: 6 of cap free
Amount of items: 3
Items: 
Size: 4120 Color: 1
Size: 3066 Color: 3
Size: 168 Color: 0

Bin 74: 6 of cap free
Amount of items: 3
Items: 
Size: 4578 Color: 0
Size: 2652 Color: 1
Size: 124 Color: 4

Bin 75: 6 of cap free
Amount of items: 3
Items: 
Size: 4940 Color: 2
Size: 2020 Color: 0
Size: 394 Color: 3

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 5684 Color: 2
Size: 1670 Color: 0

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 3
Size: 1206 Color: 0

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 6342 Color: 4
Size: 1012 Color: 0

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6354 Color: 4
Size: 1000 Color: 1

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6450 Color: 1
Size: 904 Color: 4

Bin 81: 7 of cap free
Amount of items: 8
Items: 
Size: 3682 Color: 4
Size: 776 Color: 4
Size: 737 Color: 4
Size: 728 Color: 0
Size: 646 Color: 3
Size: 288 Color: 2
Size: 272 Color: 2
Size: 224 Color: 0

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 3941 Color: 0
Size: 3060 Color: 3
Size: 352 Color: 1

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 4180 Color: 1
Size: 2869 Color: 0
Size: 304 Color: 4

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 5914 Color: 4
Size: 1439 Color: 1

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 6244 Color: 4
Size: 1108 Color: 2

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 6536 Color: 4
Size: 816 Color: 0

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 6606 Color: 0
Size: 746 Color: 4

Bin 88: 9 of cap free
Amount of items: 4
Items: 
Size: 6440 Color: 4
Size: 879 Color: 1
Size: 16 Color: 3
Size: 16 Color: 1

Bin 89: 11 of cap free
Amount of items: 3
Items: 
Size: 5421 Color: 3
Size: 1688 Color: 2
Size: 240 Color: 4

Bin 90: 11 of cap free
Amount of items: 2
Items: 
Size: 5813 Color: 4
Size: 1536 Color: 1

Bin 91: 11 of cap free
Amount of items: 2
Items: 
Size: 6125 Color: 0
Size: 1224 Color: 1

Bin 92: 11 of cap free
Amount of items: 2
Items: 
Size: 6324 Color: 0
Size: 1025 Color: 1

Bin 93: 12 of cap free
Amount of items: 3
Items: 
Size: 4822 Color: 3
Size: 1998 Color: 2
Size: 528 Color: 4

Bin 94: 12 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 2
Size: 1528 Color: 0
Size: 456 Color: 4

Bin 95: 12 of cap free
Amount of items: 3
Items: 
Size: 5464 Color: 2
Size: 1784 Color: 0
Size: 100 Color: 0

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 6522 Color: 0
Size: 826 Color: 4

Bin 97: 13 of cap free
Amount of items: 2
Items: 
Size: 6068 Color: 4
Size: 1279 Color: 2

Bin 98: 14 of cap free
Amount of items: 2
Items: 
Size: 5650 Color: 2
Size: 1696 Color: 1

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 6477 Color: 1
Size: 868 Color: 0

Bin 100: 16 of cap free
Amount of items: 3
Items: 
Size: 3909 Color: 0
Size: 2933 Color: 3
Size: 502 Color: 2

Bin 101: 17 of cap free
Amount of items: 4
Items: 
Size: 3684 Color: 3
Size: 2409 Color: 3
Size: 946 Color: 0
Size: 304 Color: 2

Bin 102: 18 of cap free
Amount of items: 2
Items: 
Size: 6086 Color: 0
Size: 1256 Color: 4

Bin 103: 20 of cap free
Amount of items: 2
Items: 
Size: 4646 Color: 3
Size: 2694 Color: 0

Bin 104: 20 of cap free
Amount of items: 2
Items: 
Size: 4996 Color: 4
Size: 2344 Color: 1

Bin 105: 22 of cap free
Amount of items: 5
Items: 
Size: 3683 Color: 1
Size: 1588 Color: 0
Size: 1425 Color: 3
Size: 322 Color: 2
Size: 320 Color: 4

Bin 106: 22 of cap free
Amount of items: 2
Items: 
Size: 6242 Color: 4
Size: 1096 Color: 0

Bin 107: 23 of cap free
Amount of items: 3
Items: 
Size: 3928 Color: 3
Size: 3261 Color: 2
Size: 148 Color: 2

Bin 108: 23 of cap free
Amount of items: 3
Items: 
Size: 3961 Color: 2
Size: 3064 Color: 3
Size: 312 Color: 0

Bin 109: 23 of cap free
Amount of items: 2
Items: 
Size: 4841 Color: 4
Size: 2496 Color: 0

Bin 110: 23 of cap free
Amount of items: 2
Items: 
Size: 5224 Color: 3
Size: 2113 Color: 4

Bin 111: 23 of cap free
Amount of items: 3
Items: 
Size: 6227 Color: 0
Size: 1078 Color: 1
Size: 32 Color: 3

Bin 112: 24 of cap free
Amount of items: 3
Items: 
Size: 3925 Color: 4
Size: 2803 Color: 4
Size: 608 Color: 0

Bin 113: 24 of cap free
Amount of items: 2
Items: 
Size: 4825 Color: 3
Size: 2511 Color: 4

Bin 114: 28 of cap free
Amount of items: 2
Items: 
Size: 5928 Color: 2
Size: 1404 Color: 0

Bin 115: 28 of cap free
Amount of items: 2
Items: 
Size: 6056 Color: 4
Size: 1276 Color: 1

Bin 116: 33 of cap free
Amount of items: 3
Items: 
Size: 4608 Color: 3
Size: 2101 Color: 3
Size: 618 Color: 4

Bin 117: 34 of cap free
Amount of items: 2
Items: 
Size: 4449 Color: 1
Size: 2877 Color: 2

Bin 118: 39 of cap free
Amount of items: 2
Items: 
Size: 5997 Color: 4
Size: 1324 Color: 0

Bin 119: 45 of cap free
Amount of items: 2
Items: 
Size: 5698 Color: 4
Size: 1617 Color: 0

Bin 120: 50 of cap free
Amount of items: 2
Items: 
Size: 5584 Color: 4
Size: 1726 Color: 2

Bin 121: 52 of cap free
Amount of items: 2
Items: 
Size: 5336 Color: 2
Size: 1972 Color: 3

Bin 122: 55 of cap free
Amount of items: 8
Items: 
Size: 3681 Color: 1
Size: 702 Color: 4
Size: 612 Color: 4
Size: 608 Color: 4
Size: 608 Color: 4
Size: 566 Color: 1
Size: 272 Color: 2
Size: 256 Color: 2

Bin 123: 61 of cap free
Amount of items: 2
Items: 
Size: 5478 Color: 2
Size: 1821 Color: 0

Bin 124: 66 of cap free
Amount of items: 4
Items: 
Size: 3688 Color: 3
Size: 2262 Color: 1
Size: 816 Color: 3
Size: 528 Color: 2

Bin 125: 72 of cap free
Amount of items: 2
Items: 
Size: 3945 Color: 0
Size: 3343 Color: 3

Bin 126: 72 of cap free
Amount of items: 2
Items: 
Size: 3997 Color: 3
Size: 3291 Color: 1

Bin 127: 86 of cap free
Amount of items: 2
Items: 
Size: 4966 Color: 1
Size: 2308 Color: 0

Bin 128: 88 of cap free
Amount of items: 2
Items: 
Size: 4628 Color: 1
Size: 2644 Color: 0

Bin 129: 98 of cap free
Amount of items: 2
Items: 
Size: 4194 Color: 4
Size: 3068 Color: 2

Bin 130: 110 of cap free
Amount of items: 2
Items: 
Size: 4188 Color: 3
Size: 3062 Color: 4

Bin 131: 111 of cap free
Amount of items: 3
Items: 
Size: 3690 Color: 4
Size: 2294 Color: 0
Size: 1265 Color: 0

Bin 132: 122 of cap free
Amount of items: 2
Items: 
Size: 4596 Color: 4
Size: 2642 Color: 1

Bin 133: 5494 of cap free
Amount of items: 12
Items: 
Size: 212 Color: 3
Size: 188 Color: 1
Size: 176 Color: 1
Size: 174 Color: 3
Size: 162 Color: 0
Size: 156 Color: 0
Size: 146 Color: 4
Size: 136 Color: 4
Size: 136 Color: 1
Size: 128 Color: 2
Size: 128 Color: 2
Size: 124 Color: 4

Total size: 971520
Total free space: 7360

