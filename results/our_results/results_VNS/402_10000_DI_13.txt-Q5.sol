Capicity Bin: 8224
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 4115 Color: 0
Size: 2987 Color: 0
Size: 918 Color: 2
Size: 204 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4654 Color: 1
Size: 2639 Color: 0
Size: 931 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4760 Color: 2
Size: 3044 Color: 0
Size: 420 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 1
Size: 2732 Color: 0
Size: 544 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 3
Size: 2568 Color: 0
Size: 144 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 0
Size: 1500 Color: 2
Size: 1034 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6046 Color: 0
Size: 1928 Color: 3
Size: 250 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 0
Size: 1860 Color: 3
Size: 232 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 0
Size: 1712 Color: 4
Size: 170 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 4
Size: 1608 Color: 2
Size: 224 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6489 Color: 4
Size: 1429 Color: 3
Size: 306 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6503 Color: 4
Size: 1257 Color: 2
Size: 464 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6540 Color: 3
Size: 1000 Color: 1
Size: 684 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6632 Color: 2
Size: 912 Color: 1
Size: 680 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 0
Size: 1140 Color: 2
Size: 416 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6776 Color: 4
Size: 1242 Color: 1
Size: 206 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6786 Color: 4
Size: 1174 Color: 0
Size: 264 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 4
Size: 1300 Color: 1
Size: 112 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 4
Size: 915 Color: 2
Size: 492 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6818 Color: 4
Size: 880 Color: 2
Size: 526 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 4
Size: 1125 Color: 1
Size: 232 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 3
Size: 1128 Color: 4
Size: 236 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 1
Size: 776 Color: 1
Size: 576 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6932 Color: 0
Size: 1026 Color: 1
Size: 266 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 4
Size: 1022 Color: 2
Size: 224 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 3
Size: 999 Color: 4
Size: 240 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7002 Color: 4
Size: 936 Color: 2
Size: 286 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6993 Color: 2
Size: 1033 Color: 4
Size: 198 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7027 Color: 4
Size: 925 Color: 1
Size: 272 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7032 Color: 3
Size: 888 Color: 2
Size: 304 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 1
Size: 948 Color: 2
Size: 200 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7107 Color: 0
Size: 909 Color: 4
Size: 208 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7112 Color: 0
Size: 928 Color: 3
Size: 184 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7115 Color: 2
Size: 901 Color: 1
Size: 208 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7134 Color: 0
Size: 910 Color: 4
Size: 180 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 2
Size: 738 Color: 4
Size: 344 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 4
Size: 588 Color: 2
Size: 448 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7176 Color: 1
Size: 868 Color: 2
Size: 180 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7207 Color: 4
Size: 849 Color: 1
Size: 168 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 4
Size: 742 Color: 3
Size: 228 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7300 Color: 4
Size: 772 Color: 3
Size: 152 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7304 Color: 4
Size: 684 Color: 3
Size: 236 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7338 Color: 4
Size: 526 Color: 2
Size: 360 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 7384 Color: 4
Size: 608 Color: 3
Size: 232 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 7400 Color: 1
Size: 580 Color: 1
Size: 244 Color: 4

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5400 Color: 0
Size: 2331 Color: 4
Size: 492 Color: 4

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5967 Color: 2
Size: 2088 Color: 4
Size: 168 Color: 3

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 1
Size: 1818 Color: 0
Size: 224 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6490 Color: 2
Size: 1605 Color: 0
Size: 128 Color: 4

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6582 Color: 4
Size: 1329 Color: 3
Size: 312 Color: 1

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6717 Color: 2
Size: 1280 Color: 4
Size: 226 Color: 1

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 1
Size: 1131 Color: 2

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 0
Size: 696 Color: 4
Size: 400 Color: 3

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 7143 Color: 0
Size: 680 Color: 4
Size: 400 Color: 0

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 4572 Color: 4
Size: 3458 Color: 1
Size: 192 Color: 4

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 4730 Color: 0
Size: 3240 Color: 3
Size: 252 Color: 3

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5210 Color: 0
Size: 2888 Color: 3
Size: 124 Color: 4

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5961 Color: 2
Size: 2085 Color: 0
Size: 176 Color: 4

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 4
Size: 2146 Color: 2
Size: 80 Color: 0

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 6631 Color: 0
Size: 1175 Color: 2
Size: 416 Color: 4

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 1
Size: 652 Color: 0
Size: 598 Color: 4

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 4641 Color: 0
Size: 3420 Color: 3
Size: 160 Color: 4

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 3
Size: 2978 Color: 4
Size: 176 Color: 0

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 5429 Color: 3
Size: 2600 Color: 4
Size: 192 Color: 0

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 3
Size: 1904 Color: 2
Size: 144 Color: 0

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 1
Size: 1753 Color: 4
Size: 176 Color: 0

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 6334 Color: 4
Size: 1887 Color: 2

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 5059 Color: 3
Size: 3001 Color: 4
Size: 160 Color: 3

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 5258 Color: 4
Size: 2474 Color: 0
Size: 488 Color: 4

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 5421 Color: 0
Size: 2091 Color: 3
Size: 708 Color: 1

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 5770 Color: 3
Size: 2264 Color: 2
Size: 186 Color: 0

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 6472 Color: 2
Size: 1748 Color: 3

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 7193 Color: 3
Size: 1027 Color: 1

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 4738 Color: 3
Size: 2993 Color: 0
Size: 488 Color: 4

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 6349 Color: 2
Size: 1662 Color: 4
Size: 208 Color: 0

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 6803 Color: 1
Size: 1416 Color: 0

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 5706 Color: 2
Size: 1800 Color: 4
Size: 712 Color: 0

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 6952 Color: 0
Size: 1266 Color: 2

Bin 79: 6 of cap free
Amount of items: 4
Items: 
Size: 7380 Color: 2
Size: 814 Color: 3
Size: 16 Color: 3
Size: 8 Color: 4

Bin 80: 7 of cap free
Amount of items: 3
Items: 
Size: 5075 Color: 0
Size: 2906 Color: 4
Size: 236 Color: 4

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 5736 Color: 0
Size: 2337 Color: 3
Size: 144 Color: 4

Bin 82: 8 of cap free
Amount of items: 3
Items: 
Size: 4124 Color: 4
Size: 2768 Color: 0
Size: 1324 Color: 1

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 2
Size: 1251 Color: 0
Size: 1185 Color: 2

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 7204 Color: 1
Size: 1012 Color: 0

Bin 85: 9 of cap free
Amount of items: 3
Items: 
Size: 6312 Color: 1
Size: 1563 Color: 3
Size: 340 Color: 0

Bin 86: 10 of cap free
Amount of items: 3
Items: 
Size: 5680 Color: 1
Size: 2406 Color: 4
Size: 128 Color: 4

Bin 87: 10 of cap free
Amount of items: 2
Items: 
Size: 6511 Color: 3
Size: 1703 Color: 1

Bin 88: 10 of cap free
Amount of items: 2
Items: 
Size: 7150 Color: 3
Size: 1064 Color: 2

Bin 89: 10 of cap free
Amount of items: 2
Items: 
Size: 7250 Color: 2
Size: 964 Color: 3

Bin 90: 12 of cap free
Amount of items: 3
Items: 
Size: 5266 Color: 1
Size: 2044 Color: 1
Size: 902 Color: 2

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 6106 Color: 4
Size: 2106 Color: 3

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 7168 Color: 3
Size: 1044 Color: 0

Bin 93: 13 of cap free
Amount of items: 2
Items: 
Size: 6165 Color: 4
Size: 2046 Color: 1

Bin 94: 14 of cap free
Amount of items: 3
Items: 
Size: 4568 Color: 1
Size: 2462 Color: 0
Size: 1180 Color: 1

Bin 95: 14 of cap free
Amount of items: 2
Items: 
Size: 6875 Color: 0
Size: 1335 Color: 2

Bin 96: 14 of cap free
Amount of items: 2
Items: 
Size: 7126 Color: 2
Size: 1084 Color: 3

Bin 97: 16 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 0
Size: 1612 Color: 4
Size: 524 Color: 2

Bin 98: 18 of cap free
Amount of items: 4
Items: 
Size: 4116 Color: 0
Size: 2514 Color: 0
Size: 1370 Color: 3
Size: 206 Color: 4

Bin 99: 18 of cap free
Amount of items: 2
Items: 
Size: 5260 Color: 3
Size: 2946 Color: 4

Bin 100: 18 of cap free
Amount of items: 2
Items: 
Size: 6636 Color: 3
Size: 1570 Color: 1

Bin 101: 18 of cap free
Amount of items: 2
Items: 
Size: 6802 Color: 0
Size: 1404 Color: 3

Bin 102: 19 of cap free
Amount of items: 2
Items: 
Size: 5723 Color: 2
Size: 2482 Color: 4

Bin 103: 21 of cap free
Amount of items: 2
Items: 
Size: 7342 Color: 2
Size: 861 Color: 0

Bin 104: 22 of cap free
Amount of items: 3
Items: 
Size: 5168 Color: 0
Size: 2914 Color: 4
Size: 120 Color: 3

Bin 105: 22 of cap free
Amount of items: 2
Items: 
Size: 6428 Color: 3
Size: 1774 Color: 2

Bin 106: 22 of cap free
Amount of items: 2
Items: 
Size: 6994 Color: 2
Size: 1208 Color: 3

Bin 107: 28 of cap free
Amount of items: 7
Items: 
Size: 4114 Color: 2
Size: 898 Color: 2
Size: 810 Color: 4
Size: 672 Color: 2
Size: 598 Color: 4
Size: 592 Color: 3
Size: 512 Color: 1

Bin 108: 32 of cap free
Amount of items: 3
Items: 
Size: 5650 Color: 4
Size: 2114 Color: 0
Size: 428 Color: 4

Bin 109: 35 of cap free
Amount of items: 9
Items: 
Size: 4113 Color: 4
Size: 596 Color: 1
Size: 580 Color: 3
Size: 576 Color: 0
Size: 500 Color: 2
Size: 492 Color: 0
Size: 466 Color: 1
Size: 448 Color: 0
Size: 418 Color: 3

Bin 110: 35 of cap free
Amount of items: 2
Items: 
Size: 6725 Color: 2
Size: 1464 Color: 1

Bin 111: 36 of cap free
Amount of items: 2
Items: 
Size: 6986 Color: 1
Size: 1202 Color: 0

Bin 112: 38 of cap free
Amount of items: 3
Items: 
Size: 4120 Color: 0
Size: 2631 Color: 0
Size: 1435 Color: 3

Bin 113: 40 of cap free
Amount of items: 2
Items: 
Size: 6738 Color: 1
Size: 1446 Color: 3

Bin 114: 41 of cap free
Amount of items: 3
Items: 
Size: 4625 Color: 1
Size: 3422 Color: 4
Size: 136 Color: 0

Bin 115: 43 of cap free
Amount of items: 2
Items: 
Size: 5715 Color: 0
Size: 2466 Color: 1

Bin 116: 44 of cap free
Amount of items: 2
Items: 
Size: 6299 Color: 2
Size: 1881 Color: 3

Bin 117: 47 of cap free
Amount of items: 2
Items: 
Size: 7135 Color: 3
Size: 1042 Color: 1

Bin 118: 50 of cap free
Amount of items: 2
Items: 
Size: 5698 Color: 4
Size: 2476 Color: 3

Bin 119: 51 of cap free
Amount of items: 2
Items: 
Size: 4746 Color: 1
Size: 3427 Color: 4

Bin 120: 52 of cap free
Amount of items: 2
Items: 
Size: 5912 Color: 2
Size: 2260 Color: 1

Bin 121: 56 of cap free
Amount of items: 2
Items: 
Size: 5112 Color: 3
Size: 3056 Color: 2

Bin 122: 61 of cap free
Amount of items: 2
Items: 
Size: 6977 Color: 1
Size: 1186 Color: 0

Bin 123: 62 of cap free
Amount of items: 27
Items: 
Size: 420 Color: 4
Size: 408 Color: 1
Size: 384 Color: 2
Size: 376 Color: 4
Size: 376 Color: 0
Size: 368 Color: 2
Size: 352 Color: 4
Size: 344 Color: 4
Size: 320 Color: 3
Size: 320 Color: 3
Size: 312 Color: 4
Size: 304 Color: 3
Size: 304 Color: 3
Size: 296 Color: 3
Size: 288 Color: 4
Size: 288 Color: 2
Size: 288 Color: 0
Size: 284 Color: 1
Size: 280 Color: 2
Size: 264 Color: 2
Size: 256 Color: 3
Size: 256 Color: 1
Size: 256 Color: 1
Size: 248 Color: 2
Size: 204 Color: 3
Size: 184 Color: 1
Size: 182 Color: 1

Bin 124: 71 of cap free
Amount of items: 2
Items: 
Size: 6706 Color: 2
Size: 1447 Color: 1

Bin 125: 72 of cap free
Amount of items: 2
Items: 
Size: 5250 Color: 4
Size: 2902 Color: 1

Bin 126: 73 of cap free
Amount of items: 2
Items: 
Size: 6623 Color: 3
Size: 1528 Color: 0

Bin 127: 75 of cap free
Amount of items: 2
Items: 
Size: 7108 Color: 0
Size: 1041 Color: 2

Bin 128: 83 of cap free
Amount of items: 2
Items: 
Size: 5516 Color: 1
Size: 2625 Color: 4

Bin 129: 91 of cap free
Amount of items: 3
Items: 
Size: 5274 Color: 0
Size: 1927 Color: 1
Size: 932 Color: 3

Bin 130: 109 of cap free
Amount of items: 2
Items: 
Size: 4690 Color: 1
Size: 3425 Color: 3

Bin 131: 114 of cap free
Amount of items: 2
Items: 
Size: 4122 Color: 0
Size: 3988 Color: 4

Bin 132: 159 of cap free
Amount of items: 2
Items: 
Size: 4633 Color: 0
Size: 3432 Color: 1

Bin 133: 6156 of cap free
Amount of items: 11
Items: 
Size: 256 Color: 0
Size: 204 Color: 4
Size: 204 Color: 0
Size: 184 Color: 3
Size: 184 Color: 2
Size: 180 Color: 3
Size: 180 Color: 1
Size: 180 Color: 0
Size: 176 Color: 1
Size: 160 Color: 3
Size: 160 Color: 1

Total size: 1085568
Total free space: 8224

