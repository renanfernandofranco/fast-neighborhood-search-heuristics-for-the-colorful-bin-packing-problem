Capicity Bin: 2068
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 1035 Color: 8
Size: 266 Color: 17
Size: 261 Color: 15
Size: 232 Color: 17
Size: 232 Color: 17
Size: 42 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1445 Color: 13
Size: 521 Color: 7
Size: 102 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 17
Size: 438 Color: 5
Size: 40 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 18
Size: 342 Color: 4
Size: 120 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 3
Size: 383 Color: 1
Size: 76 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1641 Color: 0
Size: 357 Color: 14
Size: 70 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 17
Size: 354 Color: 11
Size: 68 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 7
Size: 329 Color: 13
Size: 90 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 0
Size: 351 Color: 15
Size: 42 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 13
Size: 267 Color: 4
Size: 68 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 11
Size: 218 Color: 18
Size: 100 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 15
Size: 273 Color: 15
Size: 38 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 4
Size: 274 Color: 15
Size: 36 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 14
Size: 178 Color: 15
Size: 114 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 3
Size: 221 Color: 6
Size: 38 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 13
Size: 181 Color: 3
Size: 58 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 7
Size: 187 Color: 15
Size: 44 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 12
Size: 853 Color: 13
Size: 68 Color: 5

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1373 Color: 13
Size: 694 Color: 16

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 7
Size: 417 Color: 8
Size: 100 Color: 19

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 2
Size: 434 Color: 13
Size: 64 Color: 10

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 6
Size: 362 Color: 14
Size: 32 Color: 12

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 19
Size: 193 Color: 14
Size: 64 Color: 12

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1845 Color: 12
Size: 170 Color: 10
Size: 52 Color: 14

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1853 Color: 5
Size: 214 Color: 11

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 11
Size: 201 Color: 13
Size: 8 Color: 19

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1053 Color: 13
Size: 861 Color: 11
Size: 152 Color: 18

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1177 Color: 19
Size: 841 Color: 13
Size: 48 Color: 5

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1289 Color: 12
Size: 693 Color: 19
Size: 84 Color: 3

Bin 30: 2 of cap free
Amount of items: 4
Items: 
Size: 1453 Color: 7
Size: 513 Color: 3
Size: 76 Color: 14
Size: 24 Color: 4

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1605 Color: 9
Size: 461 Color: 12

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1612 Color: 8
Size: 386 Color: 10
Size: 68 Color: 13

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1638 Color: 10
Size: 428 Color: 4

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 18
Size: 261 Color: 14
Size: 64 Color: 16

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 8
Size: 244 Color: 4
Size: 8 Color: 4

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1038 Color: 12
Size: 889 Color: 1
Size: 138 Color: 5

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1719 Color: 4
Size: 346 Color: 1

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1734 Color: 3
Size: 331 Color: 16

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 6
Size: 282 Color: 15
Size: 34 Color: 1

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1803 Color: 3
Size: 262 Color: 10

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1486 Color: 2
Size: 578 Color: 10

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1662 Color: 6
Size: 402 Color: 1

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1773 Color: 8
Size: 291 Color: 6

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1817 Color: 5
Size: 247 Color: 18

Bin 45: 5 of cap free
Amount of items: 4
Items: 
Size: 1045 Color: 14
Size: 486 Color: 18
Size: 480 Color: 4
Size: 52 Color: 6

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 16
Size: 281 Color: 4
Size: 40 Color: 13

Bin 47: 5 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 16
Size: 282 Color: 10
Size: 16 Color: 10

Bin 48: 6 of cap free
Amount of items: 17
Items: 
Size: 217 Color: 12
Size: 211 Color: 15
Size: 168 Color: 13
Size: 168 Color: 7
Size: 146 Color: 2
Size: 142 Color: 3
Size: 136 Color: 9
Size: 128 Color: 3
Size: 124 Color: 10
Size: 112 Color: 11
Size: 106 Color: 5
Size: 96 Color: 1
Size: 84 Color: 8
Size: 82 Color: 16
Size: 52 Color: 6
Size: 50 Color: 14
Size: 40 Color: 0

Bin 49: 6 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 9
Size: 768 Color: 5
Size: 56 Color: 1

Bin 50: 6 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 4
Size: 581 Color: 2
Size: 56 Color: 5

Bin 51: 7 of cap free
Amount of items: 5
Items: 
Size: 1378 Color: 2
Size: 507 Color: 4
Size: 68 Color: 16
Size: 54 Color: 14
Size: 54 Color: 12

Bin 52: 7 of cap free
Amount of items: 2
Items: 
Size: 1622 Color: 5
Size: 439 Color: 12

Bin 53: 8 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 2
Size: 421 Color: 11
Size: 76 Color: 14

Bin 54: 9 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 3
Size: 770 Color: 19
Size: 52 Color: 14

Bin 55: 9 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 1
Size: 522 Color: 6
Size: 76 Color: 13

Bin 56: 10 of cap free
Amount of items: 2
Items: 
Size: 1428 Color: 8
Size: 630 Color: 7

Bin 57: 12 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 9
Size: 847 Color: 1
Size: 172 Color: 10

Bin 58: 13 of cap free
Amount of items: 2
Items: 
Size: 1446 Color: 19
Size: 609 Color: 17

Bin 59: 14 of cap free
Amount of items: 2
Items: 
Size: 1517 Color: 4
Size: 537 Color: 0

Bin 60: 14 of cap free
Amount of items: 2
Items: 
Size: 1654 Color: 15
Size: 400 Color: 18

Bin 61: 15 of cap free
Amount of items: 3
Items: 
Size: 1300 Color: 8
Size: 651 Color: 10
Size: 102 Color: 1

Bin 62: 17 of cap free
Amount of items: 2
Items: 
Size: 1314 Color: 8
Size: 737 Color: 14

Bin 63: 18 of cap free
Amount of items: 2
Items: 
Size: 1730 Color: 5
Size: 320 Color: 11

Bin 64: 21 of cap free
Amount of items: 2
Items: 
Size: 1185 Color: 11
Size: 862 Color: 3

Bin 65: 33 of cap free
Amount of items: 2
Items: 
Size: 1339 Color: 12
Size: 696 Color: 6

Bin 66: 1770 of cap free
Amount of items: 6
Items: 
Size: 64 Color: 10
Size: 50 Color: 19
Size: 48 Color: 15
Size: 48 Color: 14
Size: 48 Color: 6
Size: 40 Color: 7

Total size: 134420
Total free space: 2068

