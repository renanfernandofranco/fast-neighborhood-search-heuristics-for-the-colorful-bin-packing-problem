Capicity Bin: 15760
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 13371 Color: 1
Size: 1965 Color: 1
Size: 424 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11260 Color: 1
Size: 3740 Color: 1
Size: 760 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12333 Color: 1
Size: 2857 Color: 1
Size: 570 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 14073 Color: 1
Size: 1383 Color: 1
Size: 304 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 1
Size: 4042 Color: 1
Size: 474 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13058 Color: 1
Size: 2162 Color: 1
Size: 540 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 8980 Color: 1
Size: 5652 Color: 1
Size: 1128 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7884 Color: 1
Size: 6564 Color: 1
Size: 1312 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12140 Color: 1
Size: 3020 Color: 1
Size: 600 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7908 Color: 1
Size: 6548 Color: 1
Size: 1304 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11426 Color: 1
Size: 3614 Color: 1
Size: 720 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 1
Size: 2336 Color: 1
Size: 328 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 1
Size: 1448 Color: 1
Size: 368 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14090 Color: 1
Size: 1386 Color: 1
Size: 284 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12156 Color: 1
Size: 3150 Color: 1
Size: 454 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13410 Color: 1
Size: 2296 Color: 1
Size: 54 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14034 Color: 1
Size: 1414 Color: 1
Size: 312 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 10870 Color: 1
Size: 4078 Color: 1
Size: 812 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 1
Size: 2232 Color: 1
Size: 348 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 10910 Color: 1
Size: 4582 Color: 1
Size: 268 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13885 Color: 1
Size: 1507 Color: 1
Size: 368 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 1
Size: 1654 Color: 1
Size: 370 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 1
Size: 2900 Color: 1
Size: 276 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13576 Color: 1
Size: 1896 Color: 1
Size: 288 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11276 Color: 1
Size: 3180 Color: 1
Size: 1304 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13283 Color: 1
Size: 2123 Color: 1
Size: 354 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 10628 Color: 1
Size: 4284 Color: 1
Size: 848 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 1
Size: 3416 Color: 1
Size: 160 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13788 Color: 1
Size: 1756 Color: 1
Size: 216 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11628 Color: 1
Size: 3324 Color: 1
Size: 808 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 1
Size: 4324 Color: 1
Size: 340 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 1
Size: 1528 Color: 1
Size: 424 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12755 Color: 1
Size: 2377 Color: 1
Size: 628 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7896 Color: 1
Size: 6568 Color: 1
Size: 1296 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11420 Color: 1
Size: 3876 Color: 1
Size: 464 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12814 Color: 1
Size: 2458 Color: 1
Size: 488 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13242 Color: 1
Size: 1750 Color: 1
Size: 768 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14148 Color: 1
Size: 1348 Color: 1
Size: 264 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7883 Color: 1
Size: 6565 Color: 1
Size: 1312 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 1
Size: 1644 Color: 1
Size: 456 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13013 Color: 1
Size: 1867 Color: 1
Size: 880 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12078 Color: 1
Size: 3294 Color: 1
Size: 388 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 9208 Color: 1
Size: 5808 Color: 1
Size: 744 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13403 Color: 1
Size: 2085 Color: 1
Size: 272 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 1
Size: 3028 Color: 1
Size: 184 Color: 0

Bin 46: 0 of cap free
Amount of items: 5
Items: 
Size: 10792 Color: 1
Size: 1964 Color: 1
Size: 1844 Color: 1
Size: 856 Color: 0
Size: 304 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 7892 Color: 1
Size: 6564 Color: 1
Size: 1304 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13906 Color: 1
Size: 1516 Color: 1
Size: 338 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 1
Size: 1580 Color: 1
Size: 300 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13521 Color: 1
Size: 1851 Color: 1
Size: 388 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13382 Color: 1
Size: 1962 Color: 1
Size: 416 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 1
Size: 1648 Color: 1
Size: 296 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 1
Size: 1976 Color: 1
Size: 912 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 8938 Color: 1
Size: 6532 Color: 1
Size: 290 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14170 Color: 1
Size: 1322 Color: 1
Size: 268 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13090 Color: 1
Size: 2408 Color: 1
Size: 262 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13938 Color: 1
Size: 1546 Color: 1
Size: 276 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 1
Size: 2072 Color: 1
Size: 172 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 11470 Color: 1
Size: 3578 Color: 1
Size: 712 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 1
Size: 1412 Color: 1
Size: 324 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 12570 Color: 1
Size: 2662 Color: 1
Size: 528 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 12151 Color: 1
Size: 3009 Color: 1
Size: 600 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13805 Color: 1
Size: 1631 Color: 1
Size: 324 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14101 Color: 1
Size: 1351 Color: 1
Size: 308 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13139 Color: 1
Size: 2185 Color: 1
Size: 436 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 9954 Color: 1
Size: 5486 Color: 1
Size: 320 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 7882 Color: 1
Size: 6566 Color: 1
Size: 1312 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 12624 Color: 1
Size: 2188 Color: 1
Size: 948 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 11672 Color: 1
Size: 3608 Color: 1
Size: 480 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 11441 Color: 1
Size: 3601 Color: 1
Size: 718 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 14023 Color: 1
Size: 1449 Color: 1
Size: 288 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 10983 Color: 1
Size: 3981 Color: 1
Size: 796 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 12700 Color: 1
Size: 2668 Color: 1
Size: 392 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13662 Color: 1
Size: 1738 Color: 1
Size: 360 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 9581 Color: 1
Size: 5151 Color: 1
Size: 1028 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 14141 Color: 1
Size: 1331 Color: 1
Size: 288 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 8996 Color: 1
Size: 5644 Color: 1
Size: 1120 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13948 Color: 1
Size: 1444 Color: 1
Size: 368 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 1
Size: 4916 Color: 1
Size: 976 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 1
Size: 2648 Color: 1
Size: 1200 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13539 Color: 1
Size: 1549 Color: 1
Size: 672 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 12506 Color: 1
Size: 2118 Color: 1
Size: 1136 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13858 Color: 1
Size: 1482 Color: 1
Size: 420 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 12076 Color: 1
Size: 2984 Color: 1
Size: 700 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 11432 Color: 1
Size: 3896 Color: 1
Size: 432 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13542 Color: 1
Size: 1690 Color: 1
Size: 528 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13754 Color: 1
Size: 1674 Color: 1
Size: 332 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 7940 Color: 1
Size: 6524 Color: 1
Size: 1296 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13170 Color: 1
Size: 2102 Color: 1
Size: 488 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 1
Size: 1624 Color: 1
Size: 480 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 1
Size: 5816 Color: 1
Size: 288 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 11933 Color: 1
Size: 3191 Color: 1
Size: 636 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 1
Size: 4728 Color: 1
Size: 928 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 12246 Color: 1
Size: 2930 Color: 1
Size: 584 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 10146 Color: 1
Size: 5338 Color: 1
Size: 276 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 1
Size: 2176 Color: 1
Size: 296 Color: 0

Bin 97: 0 of cap free
Amount of items: 5
Items: 
Size: 10058 Color: 1
Size: 2956 Color: 1
Size: 2116 Color: 1
Size: 326 Color: 0
Size: 304 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 1
Size: 2004 Color: 1
Size: 392 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 8697 Color: 1
Size: 5887 Color: 1
Size: 1176 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 1
Size: 6504 Color: 1
Size: 464 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 9852 Color: 1
Size: 5464 Color: 1
Size: 444 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 12220 Color: 1
Size: 2556 Color: 1
Size: 984 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 1
Size: 1356 Color: 1
Size: 264 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 12812 Color: 1
Size: 2460 Color: 1
Size: 488 Color: 0

Bin 105: 0 of cap free
Amount of items: 5
Items: 
Size: 8871 Color: 1
Size: 5177 Color: 1
Size: 864 Color: 1
Size: 576 Color: 0
Size: 272 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 13503 Color: 1
Size: 1753 Color: 1
Size: 504 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 13982 Color: 1
Size: 1442 Color: 1
Size: 336 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 9664 Color: 1
Size: 5104 Color: 1
Size: 992 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 1
Size: 2696 Color: 1
Size: 500 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 12453 Color: 1
Size: 2757 Color: 1
Size: 550 Color: 0

Bin 111: 0 of cap free
Amount of items: 5
Items: 
Size: 6040 Color: 1
Size: 5896 Color: 1
Size: 2968 Color: 1
Size: 528 Color: 0
Size: 328 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 12585 Color: 1
Size: 2647 Color: 1
Size: 528 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 13412 Color: 1
Size: 2186 Color: 1
Size: 162 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 12881 Color: 1
Size: 2401 Color: 1
Size: 478 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 1
Size: 6928 Color: 1
Size: 312 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 1
Size: 5624 Color: 1
Size: 1120 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 1
Size: 3260 Color: 1
Size: 720 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 7886 Color: 1
Size: 6562 Color: 1
Size: 1312 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 9400 Color: 1
Size: 5686 Color: 1
Size: 674 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 9818 Color: 1
Size: 5682 Color: 1
Size: 260 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 10580 Color: 1
Size: 4028 Color: 1
Size: 1152 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 1
Size: 3530 Color: 1
Size: 1034 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 11898 Color: 1
Size: 2714 Color: 1
Size: 1148 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 12134 Color: 1
Size: 3022 Color: 1
Size: 604 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 1
Size: 2291 Color: 1
Size: 464 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 12790 Color: 1
Size: 2598 Color: 1
Size: 372 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 1
Size: 2364 Color: 1
Size: 472 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 13213 Color: 1
Size: 1631 Color: 1
Size: 916 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 13228 Color: 1
Size: 1688 Color: 1
Size: 844 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 13381 Color: 1
Size: 1983 Color: 1
Size: 396 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 1
Size: 1912 Color: 1
Size: 448 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 13442 Color: 1
Size: 1934 Color: 1
Size: 384 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 13548 Color: 1
Size: 2084 Color: 1
Size: 128 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 13594 Color: 1
Size: 1806 Color: 1
Size: 360 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 1
Size: 1625 Color: 1
Size: 412 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 13708 Color: 1
Size: 1716 Color: 1
Size: 336 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 10596 Color: 1
Size: 4028 Color: 1
Size: 1136 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 1
Size: 1576 Color: 1
Size: 688 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 1
Size: 1522 Color: 1
Size: 342 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 1
Size: 1586 Color: 1
Size: 320 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 14066 Color: 1
Size: 1326 Color: 1
Size: 368 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 13970 Color: 1
Size: 1494 Color: 1
Size: 296 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 1
Size: 1484 Color: 1
Size: 288 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 1
Size: 924 Color: 0
Size: 768 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 7881 Color: 1
Size: 6739 Color: 1
Size: 1140 Color: 0

Bin 146: 1 of cap free
Amount of items: 5
Items: 
Size: 9549 Color: 1
Size: 3444 Color: 1
Size: 2340 Color: 1
Size: 300 Color: 0
Size: 126 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 11982 Color: 1
Size: 3377 Color: 1
Size: 400 Color: 0

Bin 148: 1 of cap free
Amount of items: 5
Items: 
Size: 7924 Color: 1
Size: 4585 Color: 1
Size: 2470 Color: 1
Size: 436 Color: 0
Size: 344 Color: 0

Bin 149: 1 of cap free
Amount of items: 4
Items: 
Size: 11573 Color: 1
Size: 3222 Color: 1
Size: 592 Color: 0
Size: 372 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 13707 Color: 1
Size: 1560 Color: 1
Size: 492 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 13961 Color: 1
Size: 1346 Color: 1
Size: 452 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 12909 Color: 1
Size: 2266 Color: 1
Size: 584 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 10693 Color: 1
Size: 4634 Color: 1
Size: 432 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 10285 Color: 1
Size: 4946 Color: 1
Size: 528 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 7960 Color: 1
Size: 4775 Color: 1
Size: 3024 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 12284 Color: 1
Size: 2227 Color: 1
Size: 1248 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 12060 Color: 1
Size: 1920 Color: 0
Size: 1779 Color: 1

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 10266 Color: 1
Size: 4837 Color: 1
Size: 656 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 1
Size: 2505 Color: 1
Size: 298 Color: 0

Bin 160: 1 of cap free
Amount of items: 3
Items: 
Size: 10259 Color: 1
Size: 4924 Color: 1
Size: 576 Color: 0

Bin 161: 1 of cap free
Amount of items: 3
Items: 
Size: 10661 Color: 1
Size: 4754 Color: 1
Size: 344 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 11709 Color: 1
Size: 3733 Color: 1
Size: 316 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 13042 Color: 1
Size: 2456 Color: 1
Size: 260 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 13778 Color: 1
Size: 1852 Color: 1
Size: 128 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 1
Size: 3084 Color: 1
Size: 458 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 13903 Color: 1
Size: 1711 Color: 1
Size: 144 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 13879 Color: 1
Size: 1455 Color: 1
Size: 424 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 13591 Color: 1
Size: 1563 Color: 1
Size: 604 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 1
Size: 1394 Color: 1
Size: 336 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 11641 Color: 1
Size: 3733 Color: 1
Size: 384 Color: 0

Bin 171: 3 of cap free
Amount of items: 3
Items: 
Size: 14165 Color: 1
Size: 976 Color: 1
Size: 616 Color: 0

Bin 172: 4 of cap free
Amount of items: 3
Items: 
Size: 9826 Color: 1
Size: 4842 Color: 1
Size: 1088 Color: 0

Bin 173: 4 of cap free
Amount of items: 3
Items: 
Size: 13218 Color: 1
Size: 2122 Color: 1
Size: 416 Color: 0

Bin 174: 6 of cap free
Amount of items: 3
Items: 
Size: 13627 Color: 1
Size: 1699 Color: 1
Size: 428 Color: 0

Bin 175: 7 of cap free
Amount of items: 3
Items: 
Size: 14098 Color: 1
Size: 1375 Color: 1
Size: 280 Color: 0

Bin 176: 7 of cap free
Amount of items: 3
Items: 
Size: 13868 Color: 1
Size: 1501 Color: 1
Size: 384 Color: 0

Bin 177: 8 of cap free
Amount of items: 5
Items: 
Size: 7808 Color: 1
Size: 4408 Color: 1
Size: 1768 Color: 1
Size: 1296 Color: 0
Size: 472 Color: 0

Bin 178: 9 of cap free
Amount of items: 3
Items: 
Size: 13678 Color: 1
Size: 1809 Color: 1
Size: 264 Color: 0

Bin 179: 10 of cap free
Amount of items: 4
Items: 
Size: 10202 Color: 1
Size: 4308 Color: 1
Size: 816 Color: 0
Size: 424 Color: 0

Bin 180: 10 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 1
Size: 2226 Color: 1
Size: 988 Color: 0

Bin 181: 10 of cap free
Amount of items: 5
Items: 
Size: 5741 Color: 1
Size: 5096 Color: 1
Size: 3993 Color: 1
Size: 640 Color: 0
Size: 280 Color: 0

Bin 182: 11 of cap free
Amount of items: 3
Items: 
Size: 13540 Color: 1
Size: 2065 Color: 1
Size: 144 Color: 0

Bin 183: 15 of cap free
Amount of items: 3
Items: 
Size: 13140 Color: 1
Size: 2297 Color: 1
Size: 308 Color: 0

Bin 184: 22 of cap free
Amount of items: 3
Items: 
Size: 11890 Color: 1
Size: 3208 Color: 1
Size: 640 Color: 0

Bin 185: 23 of cap free
Amount of items: 3
Items: 
Size: 11281 Color: 1
Size: 4152 Color: 1
Size: 304 Color: 0

Bin 186: 32 of cap free
Amount of items: 3
Items: 
Size: 14015 Color: 1
Size: 1633 Color: 1
Size: 80 Color: 0

Bin 187: 34 of cap free
Amount of items: 3
Items: 
Size: 12824 Color: 1
Size: 2156 Color: 1
Size: 746 Color: 0

Bin 188: 38 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 1
Size: 1850 Color: 1
Size: 856 Color: 0

Bin 189: 38 of cap free
Amount of items: 3
Items: 
Size: 8942 Color: 1
Size: 5816 Color: 1
Size: 964 Color: 0

Bin 190: 57 of cap free
Amount of items: 3
Items: 
Size: 10472 Color: 1
Size: 4223 Color: 1
Size: 1008 Color: 0

Bin 191: 130 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 1
Size: 2558 Color: 1
Size: 274 Color: 0

Bin 192: 1582 of cap free
Amount of items: 1
Items: 
Size: 14178 Color: 1

Bin 193: 1614 of cap free
Amount of items: 1
Items: 
Size: 14146 Color: 1

Bin 194: 1649 of cap free
Amount of items: 1
Items: 
Size: 14111 Color: 1

Bin 195: 1807 of cap free
Amount of items: 1
Items: 
Size: 13953 Color: 1

Bin 196: 1949 of cap free
Amount of items: 1
Items: 
Size: 13811 Color: 1

Bin 197: 1957 of cap free
Amount of items: 1
Items: 
Size: 13803 Color: 1

Bin 198: 2068 of cap free
Amount of items: 3
Items: 
Size: 9624 Color: 1
Size: 3804 Color: 1
Size: 264 Color: 0

Bin 199: 2622 of cap free
Amount of items: 1
Items: 
Size: 13138 Color: 1

Total size: 3120480
Total free space: 15760

