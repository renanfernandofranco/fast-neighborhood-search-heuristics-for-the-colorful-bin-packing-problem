Capicity Bin: 1000001
Lower Bound: 4508

Bins used: 4511
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 621405 Color: 9
Size: 192803 Color: 15
Size: 185793 Color: 19

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 664993 Color: 11
Size: 170684 Color: 7
Size: 164324 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 716799 Color: 19
Size: 146674 Color: 6
Size: 136528 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 724038 Color: 0
Size: 142299 Color: 9
Size: 133664 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 767796 Color: 1
Size: 128135 Color: 4
Size: 104070 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 794050 Color: 14
Size: 104053 Color: 1
Size: 101898 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 763447 Color: 2
Size: 119527 Color: 2
Size: 117027 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 774722 Color: 16
Size: 121955 Color: 9
Size: 103324 Color: 17

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 676973 Color: 1
Size: 323028 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 705796 Color: 17
Size: 148365 Color: 0
Size: 145840 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 762486 Color: 19
Size: 123432 Color: 19
Size: 114083 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 794148 Color: 12
Size: 103451 Color: 12
Size: 102402 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 764253 Color: 3
Size: 123868 Color: 14
Size: 111880 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 759743 Color: 3
Size: 120803 Color: 1
Size: 119455 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 721618 Color: 17
Size: 154952 Color: 15
Size: 123431 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 761961 Color: 19
Size: 135723 Color: 16
Size: 102317 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 784197 Color: 6
Size: 108461 Color: 11
Size: 107343 Color: 6

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 628850 Color: 19
Size: 371151 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 724048 Color: 14
Size: 138834 Color: 1
Size: 137119 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 667128 Color: 12
Size: 169859 Color: 15
Size: 163014 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 773721 Color: 5
Size: 116785 Color: 7
Size: 109495 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 785615 Color: 0
Size: 109190 Color: 3
Size: 105196 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 752764 Color: 2
Size: 124495 Color: 1
Size: 122742 Color: 13

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 609260 Color: 12
Size: 197252 Color: 11
Size: 193489 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 776945 Color: 18
Size: 116935 Color: 6
Size: 106121 Color: 6

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 769973 Color: 5
Size: 116204 Color: 3
Size: 113824 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 741761 Color: 1
Size: 130342 Color: 0
Size: 127898 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 779271 Color: 6
Size: 110381 Color: 7
Size: 110349 Color: 7

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 368244 Color: 4
Size: 321495 Color: 9
Size: 310262 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 762776 Color: 17
Size: 120933 Color: 16
Size: 116292 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 793650 Color: 15
Size: 105535 Color: 13
Size: 100816 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 765913 Color: 9
Size: 117170 Color: 13
Size: 116918 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 783357 Color: 16
Size: 112400 Color: 5
Size: 104244 Color: 13

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 786449 Color: 19
Size: 110469 Color: 3
Size: 103083 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 674723 Color: 16
Size: 162699 Color: 13
Size: 162579 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 795993 Color: 16
Size: 102217 Color: 3
Size: 101791 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 794628 Color: 10
Size: 102975 Color: 0
Size: 102398 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 529518 Color: 15
Size: 240086 Color: 6
Size: 230397 Color: 10

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 651412 Color: 3
Size: 348589 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 674327 Color: 10
Size: 164060 Color: 17
Size: 161614 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 775011 Color: 14
Size: 113600 Color: 12
Size: 111390 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 645717 Color: 8
Size: 177938 Color: 11
Size: 176346 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 689277 Color: 14
Size: 155491 Color: 6
Size: 155233 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 359154 Color: 14
Size: 342657 Color: 17
Size: 298190 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 761304 Color: 14
Size: 119705 Color: 10
Size: 118992 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 769252 Color: 17
Size: 117379 Color: 8
Size: 113370 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 782845 Color: 19
Size: 108662 Color: 15
Size: 108494 Color: 15

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 788166 Color: 17
Size: 106779 Color: 14
Size: 105056 Color: 7

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 796203 Color: 19
Size: 103112 Color: 17
Size: 100686 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 683932 Color: 18
Size: 159010 Color: 1
Size: 157059 Color: 7

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 796326 Color: 8
Size: 102619 Color: 3
Size: 101056 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 795697 Color: 19
Size: 103764 Color: 0
Size: 100540 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 481078 Color: 5
Size: 276727 Color: 17
Size: 242196 Color: 9

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 583313 Color: 19
Size: 416688 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 778902 Color: 19
Size: 116407 Color: 5
Size: 104692 Color: 10

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 767927 Color: 5
Size: 124824 Color: 10
Size: 107250 Color: 13

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 787300 Color: 3
Size: 107941 Color: 10
Size: 104760 Color: 6

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 783301 Color: 6
Size: 109015 Color: 9
Size: 107685 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 745739 Color: 16
Size: 139363 Color: 2
Size: 114899 Color: 17

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 353902 Color: 9
Size: 336219 Color: 10
Size: 309880 Color: 7

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 769938 Color: 3
Size: 117286 Color: 15
Size: 112777 Color: 17

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 369583 Color: 3
Size: 330319 Color: 14
Size: 300099 Color: 8

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 787082 Color: 4
Size: 107342 Color: 8
Size: 105577 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 782189 Color: 15
Size: 113880 Color: 11
Size: 103932 Color: 8

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 793589 Color: 9
Size: 103378 Color: 1
Size: 103034 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 773723 Color: 3
Size: 121859 Color: 9
Size: 104419 Color: 15

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 795498 Color: 0
Size: 104121 Color: 18
Size: 100382 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 790035 Color: 1
Size: 105340 Color: 12
Size: 104626 Color: 6

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 649219 Color: 17
Size: 175566 Color: 4
Size: 175216 Color: 16

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 764796 Color: 2
Size: 122759 Color: 1
Size: 112446 Color: 17

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 538567 Color: 12
Size: 461434 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 764519 Color: 15
Size: 124225 Color: 0
Size: 111257 Color: 10

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 770168 Color: 4
Size: 120685 Color: 19
Size: 109148 Color: 6

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 772006 Color: 4
Size: 117485 Color: 17
Size: 110510 Color: 9

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 585379 Color: 13
Size: 223501 Color: 18
Size: 191121 Color: 12

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 745136 Color: 0
Size: 128282 Color: 14
Size: 126583 Color: 14

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 659258 Color: 5
Size: 183041 Color: 13
Size: 157702 Color: 11

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 795649 Color: 11
Size: 103595 Color: 14
Size: 100757 Color: 10

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 785599 Color: 4
Size: 107318 Color: 1
Size: 107084 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 796734 Color: 0
Size: 101692 Color: 3
Size: 101575 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 648424 Color: 17
Size: 176692 Color: 17
Size: 174885 Color: 10

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 406532 Color: 14
Size: 350951 Color: 14
Size: 242518 Color: 12

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 737597 Color: 18
Size: 131280 Color: 11
Size: 131124 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 599084 Color: 10
Size: 208488 Color: 16
Size: 192429 Color: 17

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 629559 Color: 17
Size: 185618 Color: 6
Size: 184824 Color: 15

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 791850 Color: 3
Size: 104527 Color: 10
Size: 103624 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 794225 Color: 0
Size: 103179 Color: 10
Size: 102597 Color: 6

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 648843 Color: 17
Size: 217725 Color: 4
Size: 133433 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 785651 Color: 13
Size: 107427 Color: 16
Size: 106923 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 756221 Color: 13
Size: 125919 Color: 8
Size: 117861 Color: 12

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 787811 Color: 9
Size: 107221 Color: 10
Size: 104969 Color: 5

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 786952 Color: 3
Size: 213049 Color: 10

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 695269 Color: 8
Size: 153978 Color: 5
Size: 150754 Color: 3

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 774071 Color: 9
Size: 113412 Color: 4
Size: 112518 Color: 2

Bin 95: 0 of cap free
Amount of items: 4
Items: 
Size: 596852 Color: 19
Size: 194734 Color: 12
Size: 106500 Color: 3
Size: 101915 Color: 9

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 759723 Color: 16
Size: 120931 Color: 3
Size: 119347 Color: 18

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 793473 Color: 5
Size: 104406 Color: 10
Size: 102122 Color: 3

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 782793 Color: 14
Size: 113867 Color: 8
Size: 103341 Color: 7

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 549101 Color: 4
Size: 254802 Color: 13
Size: 196098 Color: 18

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 770206 Color: 16
Size: 117021 Color: 8
Size: 112774 Color: 7

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 744739 Color: 4
Size: 139355 Color: 3
Size: 115907 Color: 19

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 779045 Color: 12
Size: 119689 Color: 3
Size: 101267 Color: 19

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 666545 Color: 1
Size: 333456 Color: 6

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 769330 Color: 13
Size: 115398 Color: 1
Size: 115273 Color: 15

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 707088 Color: 7
Size: 292913 Color: 5

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 786218 Color: 3
Size: 106956 Color: 12
Size: 106827 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 665946 Color: 15
Size: 170629 Color: 3
Size: 163426 Color: 4

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 783688 Color: 6
Size: 109477 Color: 18
Size: 106836 Color: 6

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 797574 Color: 16
Size: 101947 Color: 1
Size: 100480 Color: 16

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 790471 Color: 3
Size: 105180 Color: 16
Size: 104350 Color: 8

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 565396 Color: 14
Size: 240166 Color: 6
Size: 194439 Color: 14

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 483881 Color: 5
Size: 262959 Color: 2
Size: 253161 Color: 6

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 795331 Color: 8
Size: 204670 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 760956 Color: 16
Size: 132297 Color: 7
Size: 106748 Color: 6

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 781782 Color: 13
Size: 112497 Color: 16
Size: 105722 Color: 5

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 773995 Color: 18
Size: 117518 Color: 3
Size: 108488 Color: 17

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 793355 Color: 2
Size: 103780 Color: 9
Size: 102866 Color: 16

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 368559 Color: 15
Size: 333876 Color: 1
Size: 297566 Color: 13

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 790074 Color: 9
Size: 107446 Color: 9
Size: 102481 Color: 8

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 600160 Color: 16
Size: 399841 Color: 15

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 778405 Color: 6
Size: 112978 Color: 14
Size: 108618 Color: 18

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 787246 Color: 10
Size: 107815 Color: 7
Size: 104940 Color: 18

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 597152 Color: 8
Size: 208016 Color: 19
Size: 194833 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 756717 Color: 10
Size: 125355 Color: 19
Size: 117929 Color: 16

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 549544 Color: 12
Size: 450457 Color: 17

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 792802 Color: 15
Size: 106032 Color: 13
Size: 101167 Color: 7

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 594679 Color: 11
Size: 296430 Color: 17
Size: 108892 Color: 19

Bin 128: 0 of cap free
Amount of items: 2
Items: 
Size: 793265 Color: 10
Size: 206736 Color: 7

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 741988 Color: 14
Size: 129089 Color: 5
Size: 128924 Color: 3

Bin 130: 0 of cap free
Amount of items: 2
Items: 
Size: 737986 Color: 13
Size: 262015 Color: 12

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 774812 Color: 9
Size: 113699 Color: 2
Size: 111490 Color: 16

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 794325 Color: 16
Size: 104331 Color: 8
Size: 101345 Color: 16

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 773315 Color: 16
Size: 114508 Color: 6
Size: 112178 Color: 8

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 514499 Color: 11
Size: 485502 Color: 3

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 358264 Color: 2
Size: 357130 Color: 2
Size: 284607 Color: 5

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 634111 Color: 12
Size: 185789 Color: 5
Size: 180101 Color: 5

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 351178 Color: 17
Size: 338452 Color: 2
Size: 310371 Color: 17

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 627643 Color: 11
Size: 372358 Color: 7

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 528471 Color: 6
Size: 257580 Color: 5
Size: 213950 Color: 12

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 776582 Color: 4
Size: 112962 Color: 1
Size: 110457 Color: 11

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 795901 Color: 16
Size: 204100 Color: 3

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 677938 Color: 4
Size: 199275 Color: 18
Size: 122788 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 607322 Color: 5
Size: 278494 Color: 19
Size: 114185 Color: 11

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 764586 Color: 12
Size: 131197 Color: 12
Size: 104218 Color: 13

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 685979 Color: 16
Size: 203606 Color: 3
Size: 110416 Color: 1

Bin 146: 0 of cap free
Amount of items: 2
Items: 
Size: 552215 Color: 13
Size: 447786 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 792763 Color: 5
Size: 104934 Color: 17
Size: 102304 Color: 19

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 556365 Color: 17
Size: 232803 Color: 12
Size: 210833 Color: 11

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 343820 Color: 14
Size: 341393 Color: 16
Size: 314788 Color: 18

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 344120 Color: 17
Size: 337398 Color: 9
Size: 318483 Color: 18

Bin 151: 0 of cap free
Amount of items: 4
Items: 
Size: 280936 Color: 9
Size: 280734 Color: 0
Size: 280046 Color: 0
Size: 158285 Color: 15

Bin 152: 0 of cap free
Amount of items: 4
Items: 
Size: 278338 Color: 3
Size: 277364 Color: 16
Size: 272433 Color: 11
Size: 171866 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 361816 Color: 5
Size: 328419 Color: 5
Size: 309766 Color: 15

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 369341 Color: 5
Size: 335824 Color: 12
Size: 294836 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 369722 Color: 16
Size: 324479 Color: 7
Size: 305800 Color: 15

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 370618 Color: 9
Size: 319554 Color: 3
Size: 309829 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 369833 Color: 19
Size: 318547 Color: 3
Size: 311621 Color: 13

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 371592 Color: 5
Size: 332123 Color: 17
Size: 296286 Color: 18

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 370876 Color: 19
Size: 348126 Color: 15
Size: 280999 Color: 8

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 388841 Color: 14
Size: 309848 Color: 13
Size: 301312 Color: 14

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 419410 Color: 8
Size: 299939 Color: 12
Size: 280652 Color: 15

Bin 162: 0 of cap free
Amount of items: 2
Items: 
Size: 508715 Color: 1
Size: 491286 Color: 17

Bin 163: 0 of cap free
Amount of items: 2
Items: 
Size: 513122 Color: 8
Size: 486879 Color: 18

Bin 164: 0 of cap free
Amount of items: 2
Items: 
Size: 525867 Color: 1
Size: 474134 Color: 4

Bin 165: 0 of cap free
Amount of items: 2
Items: 
Size: 526745 Color: 19
Size: 473256 Color: 16

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 540349 Color: 13
Size: 459652 Color: 1

Bin 167: 0 of cap free
Amount of items: 2
Items: 
Size: 540610 Color: 13
Size: 459391 Color: 18

Bin 168: 0 of cap free
Amount of items: 2
Items: 
Size: 543485 Color: 14
Size: 456516 Color: 10

Bin 169: 0 of cap free
Amount of items: 2
Items: 
Size: 545452 Color: 3
Size: 454549 Color: 15

Bin 170: 0 of cap free
Amount of items: 2
Items: 
Size: 551449 Color: 11
Size: 448552 Color: 10

Bin 171: 0 of cap free
Amount of items: 2
Items: 
Size: 554924 Color: 12
Size: 445077 Color: 11

Bin 172: 0 of cap free
Amount of items: 2
Items: 
Size: 557648 Color: 8
Size: 442353 Color: 4

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 558388 Color: 18
Size: 247662 Color: 17
Size: 193951 Color: 19

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 559455 Color: 8
Size: 247035 Color: 12
Size: 193511 Color: 10

Bin 175: 0 of cap free
Amount of items: 2
Items: 
Size: 560708 Color: 18
Size: 439293 Color: 0

Bin 176: 0 of cap free
Amount of items: 2
Items: 
Size: 563821 Color: 10
Size: 436180 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 564609 Color: 1
Size: 242038 Color: 9
Size: 193354 Color: 7

Bin 178: 0 of cap free
Amount of items: 2
Items: 
Size: 567572 Color: 11
Size: 432429 Color: 15

Bin 179: 0 of cap free
Amount of items: 2
Items: 
Size: 567608 Color: 11
Size: 432393 Color: 17

Bin 180: 0 of cap free
Amount of items: 2
Items: 
Size: 567724 Color: 5
Size: 432277 Color: 14

Bin 181: 0 of cap free
Amount of items: 2
Items: 
Size: 575320 Color: 12
Size: 424681 Color: 3

Bin 182: 0 of cap free
Amount of items: 2
Items: 
Size: 575366 Color: 14
Size: 424635 Color: 4

Bin 183: 0 of cap free
Amount of items: 2
Items: 
Size: 575547 Color: 11
Size: 424454 Color: 0

Bin 184: 0 of cap free
Amount of items: 2
Items: 
Size: 578897 Color: 5
Size: 421104 Color: 1

Bin 185: 0 of cap free
Amount of items: 2
Items: 
Size: 593443 Color: 13
Size: 406558 Color: 16

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 591746 Color: 18
Size: 408255 Color: 6

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 595733 Color: 8
Size: 404268 Color: 16

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 598657 Color: 14
Size: 401344 Color: 16

Bin 189: 0 of cap free
Amount of items: 2
Items: 
Size: 599175 Color: 0
Size: 400826 Color: 5

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 599282 Color: 1
Size: 206370 Color: 4
Size: 194349 Color: 15

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 603411 Color: 19
Size: 199710 Color: 9
Size: 196880 Color: 8

Bin 192: 0 of cap free
Amount of items: 2
Items: 
Size: 605752 Color: 1
Size: 394249 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 614266 Color: 8
Size: 196295 Color: 14
Size: 189440 Color: 9

Bin 194: 0 of cap free
Amount of items: 2
Items: 
Size: 614714 Color: 14
Size: 385287 Color: 6

Bin 195: 0 of cap free
Amount of items: 2
Items: 
Size: 614734 Color: 2
Size: 385267 Color: 13

Bin 196: 0 of cap free
Amount of items: 2
Items: 
Size: 616186 Color: 16
Size: 383815 Color: 1

Bin 197: 0 of cap free
Amount of items: 2
Items: 
Size: 619057 Color: 10
Size: 380944 Color: 8

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 619482 Color: 1
Size: 191132 Color: 7
Size: 189387 Color: 15

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 620150 Color: 1
Size: 190981 Color: 9
Size: 188870 Color: 17

Bin 200: 0 of cap free
Amount of items: 2
Items: 
Size: 630754 Color: 14
Size: 369247 Color: 3

Bin 201: 0 of cap free
Amount of items: 2
Items: 
Size: 634815 Color: 0
Size: 365186 Color: 19

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 638179 Color: 5
Size: 361822 Color: 14

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 639895 Color: 10
Size: 180056 Color: 18
Size: 180050 Color: 4

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 640624 Color: 8
Size: 359377 Color: 5

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 641868 Color: 14
Size: 181169 Color: 17
Size: 176964 Color: 3

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 647633 Color: 2
Size: 352368 Color: 4

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 661616 Color: 8
Size: 172860 Color: 1
Size: 165525 Color: 12

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 662732 Color: 8
Size: 171776 Color: 3
Size: 165493 Color: 18

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 666371 Color: 6
Size: 167039 Color: 14
Size: 166591 Color: 11

Bin 210: 0 of cap free
Amount of items: 2
Items: 
Size: 666455 Color: 1
Size: 333546 Color: 19

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 668769 Color: 18
Size: 166150 Color: 14
Size: 165082 Color: 6

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 672832 Color: 2
Size: 163707 Color: 15
Size: 163462 Color: 6

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 675444 Color: 16
Size: 324557 Color: 6

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 682133 Color: 5
Size: 158953 Color: 4
Size: 158915 Color: 5

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 684191 Color: 6
Size: 315810 Color: 9

Bin 216: 0 of cap free
Amount of items: 2
Items: 
Size: 685691 Color: 4
Size: 314310 Color: 19

Bin 217: 0 of cap free
Amount of items: 2
Items: 
Size: 686436 Color: 15
Size: 313565 Color: 18

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 687354 Color: 5
Size: 156375 Color: 19
Size: 156272 Color: 19

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 692225 Color: 19
Size: 153893 Color: 1
Size: 153883 Color: 19

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 699333 Color: 1
Size: 153530 Color: 2
Size: 147138 Color: 17

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 703280 Color: 3
Size: 149131 Color: 14
Size: 147590 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 704682 Color: 12
Size: 148377 Color: 8
Size: 146942 Color: 19

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 705367 Color: 10
Size: 148595 Color: 6
Size: 146039 Color: 9

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 705781 Color: 2
Size: 149327 Color: 0
Size: 144893 Color: 15

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 708412 Color: 12
Size: 291589 Color: 15

Bin 226: 0 of cap free
Amount of items: 2
Items: 
Size: 714101 Color: 12
Size: 285900 Color: 4

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 717502 Color: 7
Size: 141335 Color: 4
Size: 141164 Color: 12

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 718978 Color: 4
Size: 145308 Color: 7
Size: 135715 Color: 6

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 719387 Color: 3
Size: 145083 Color: 13
Size: 135531 Color: 5

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 719565 Color: 4
Size: 145038 Color: 5
Size: 135398 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 720736 Color: 10
Size: 140107 Color: 17
Size: 139158 Color: 14

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 720967 Color: 6
Size: 279034 Color: 14

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 721856 Color: 0
Size: 141320 Color: 5
Size: 136825 Color: 5

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 722039 Color: 3
Size: 140410 Color: 1
Size: 137552 Color: 7

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 723839 Color: 3
Size: 141358 Color: 15
Size: 134804 Color: 4

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 725051 Color: 3
Size: 137575 Color: 5
Size: 137375 Color: 7

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 726020 Color: 8
Size: 140063 Color: 8
Size: 133918 Color: 16

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 726653 Color: 14
Size: 138360 Color: 12
Size: 134988 Color: 14

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 726658 Color: 10
Size: 139368 Color: 3
Size: 133975 Color: 10

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 726928 Color: 17
Size: 138655 Color: 6
Size: 134418 Color: 9

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 728320 Color: 12
Size: 137011 Color: 7
Size: 134670 Color: 7

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 734759 Color: 10
Size: 132676 Color: 10
Size: 132566 Color: 8

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 735514 Color: 15
Size: 132694 Color: 6
Size: 131793 Color: 3

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 739789 Color: 9
Size: 260212 Color: 8

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 745453 Color: 19
Size: 127390 Color: 12
Size: 127158 Color: 7

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 752037 Color: 12
Size: 124080 Color: 4
Size: 123884 Color: 5

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 752329 Color: 10
Size: 124274 Color: 19
Size: 123398 Color: 11

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 752745 Color: 0
Size: 123922 Color: 6
Size: 123334 Color: 16

Bin 249: 0 of cap free
Amount of items: 2
Items: 
Size: 755119 Color: 12
Size: 244882 Color: 9

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 757150 Color: 9
Size: 121482 Color: 0
Size: 121369 Color: 5

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 757651 Color: 19
Size: 242350 Color: 5

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 761360 Color: 3
Size: 119523 Color: 5
Size: 119118 Color: 6

Bin 253: 0 of cap free
Amount of items: 2
Items: 
Size: 774397 Color: 8
Size: 225604 Color: 18

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 779802 Color: 1
Size: 110120 Color: 17
Size: 110079 Color: 16

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 780259 Color: 9
Size: 219742 Color: 14

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 781020 Color: 7
Size: 218981 Color: 16

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 782575 Color: 4
Size: 217426 Color: 8

Bin 258: 0 of cap free
Amount of items: 2
Items: 
Size: 788423 Color: 19
Size: 211578 Color: 0

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 790651 Color: 18
Size: 209350 Color: 5

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 791095 Color: 1
Size: 104589 Color: 17
Size: 104317 Color: 19

Bin 261: 0 of cap free
Amount of items: 2
Items: 
Size: 797690 Color: 14
Size: 202311 Color: 19

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 799088 Color: 3
Size: 100501 Color: 18
Size: 100412 Color: 10

Bin 263: 1 of cap free
Amount of items: 3
Items: 
Size: 359174 Color: 17
Size: 358371 Color: 19
Size: 282455 Color: 19

Bin 264: 1 of cap free
Amount of items: 3
Items: 
Size: 367298 Color: 10
Size: 350176 Color: 8
Size: 282526 Color: 19

Bin 265: 1 of cap free
Amount of items: 3
Items: 
Size: 368331 Color: 17
Size: 331173 Color: 6
Size: 300496 Color: 17

Bin 266: 1 of cap free
Amount of items: 3
Items: 
Size: 368516 Color: 8
Size: 353420 Color: 14
Size: 278064 Color: 10

Bin 267: 1 of cap free
Amount of items: 3
Items: 
Size: 370826 Color: 5
Size: 321403 Color: 12
Size: 307771 Color: 10

Bin 268: 1 of cap free
Amount of items: 3
Items: 
Size: 370669 Color: 19
Size: 348377 Color: 11
Size: 280954 Color: 18

Bin 269: 1 of cap free
Amount of items: 3
Items: 
Size: 374965 Color: 1
Size: 327517 Color: 10
Size: 297518 Color: 12

Bin 270: 1 of cap free
Amount of items: 3
Items: 
Size: 379080 Color: 8
Size: 337242 Color: 13
Size: 283678 Color: 9

Bin 271: 1 of cap free
Amount of items: 3
Items: 
Size: 423425 Color: 4
Size: 297234 Color: 1
Size: 279341 Color: 19

Bin 272: 1 of cap free
Amount of items: 3
Items: 
Size: 457096 Color: 13
Size: 276823 Color: 1
Size: 266081 Color: 14

Bin 273: 1 of cap free
Amount of items: 2
Items: 
Size: 500381 Color: 7
Size: 499619 Color: 12

Bin 274: 1 of cap free
Amount of items: 2
Items: 
Size: 506953 Color: 4
Size: 493047 Color: 19

Bin 275: 1 of cap free
Amount of items: 2
Items: 
Size: 513355 Color: 4
Size: 486645 Color: 9

Bin 276: 1 of cap free
Amount of items: 2
Items: 
Size: 520934 Color: 19
Size: 479066 Color: 13

Bin 277: 1 of cap free
Amount of items: 2
Items: 
Size: 527473 Color: 1
Size: 472527 Color: 15

Bin 278: 1 of cap free
Amount of items: 2
Items: 
Size: 535553 Color: 8
Size: 464447 Color: 16

Bin 279: 1 of cap free
Amount of items: 2
Items: 
Size: 544144 Color: 15
Size: 455856 Color: 2

Bin 280: 1 of cap free
Amount of items: 2
Items: 
Size: 547680 Color: 18
Size: 452320 Color: 19

Bin 281: 1 of cap free
Amount of items: 3
Items: 
Size: 558372 Color: 19
Size: 248044 Color: 7
Size: 193584 Color: 3

Bin 282: 1 of cap free
Amount of items: 3
Items: 
Size: 561750 Color: 18
Size: 245615 Color: 16
Size: 192635 Color: 4

Bin 283: 1 of cap free
Amount of items: 3
Items: 
Size: 564584 Color: 1
Size: 239061 Color: 3
Size: 196355 Color: 8

Bin 284: 1 of cap free
Amount of items: 2
Items: 
Size: 569230 Color: 11
Size: 430770 Color: 9

Bin 285: 1 of cap free
Amount of items: 2
Items: 
Size: 570918 Color: 11
Size: 429082 Color: 2

Bin 286: 1 of cap free
Amount of items: 2
Items: 
Size: 579543 Color: 11
Size: 420457 Color: 2

Bin 287: 1 of cap free
Amount of items: 2
Items: 
Size: 581274 Color: 5
Size: 418726 Color: 7

Bin 288: 1 of cap free
Amount of items: 2
Items: 
Size: 582791 Color: 13
Size: 417209 Color: 11

Bin 289: 1 of cap free
Amount of items: 2
Items: 
Size: 586775 Color: 6
Size: 413225 Color: 9

Bin 290: 1 of cap free
Amount of items: 2
Items: 
Size: 588339 Color: 7
Size: 411661 Color: 17

Bin 291: 1 of cap free
Amount of items: 2
Items: 
Size: 592061 Color: 12
Size: 407939 Color: 4

Bin 292: 1 of cap free
Amount of items: 2
Items: 
Size: 592773 Color: 15
Size: 407227 Color: 6

Bin 293: 1 of cap free
Amount of items: 2
Items: 
Size: 598749 Color: 3
Size: 401251 Color: 11

Bin 294: 1 of cap free
Amount of items: 3
Items: 
Size: 599136 Color: 7
Size: 207973 Color: 7
Size: 192891 Color: 15

Bin 295: 1 of cap free
Amount of items: 3
Items: 
Size: 603607 Color: 0
Size: 199584 Color: 5
Size: 196809 Color: 0

Bin 296: 1 of cap free
Amount of items: 2
Items: 
Size: 605810 Color: 7
Size: 394190 Color: 10

Bin 297: 1 of cap free
Amount of items: 2
Items: 
Size: 608445 Color: 12
Size: 391555 Color: 9

Bin 298: 1 of cap free
Amount of items: 3
Items: 
Size: 613317 Color: 18
Size: 195166 Color: 0
Size: 191517 Color: 7

Bin 299: 1 of cap free
Amount of items: 3
Items: 
Size: 614071 Color: 3
Size: 194683 Color: 1
Size: 191246 Color: 4

Bin 300: 1 of cap free
Amount of items: 2
Items: 
Size: 616746 Color: 1
Size: 383254 Color: 2

Bin 301: 1 of cap free
Amount of items: 2
Items: 
Size: 617416 Color: 6
Size: 382584 Color: 11

Bin 302: 1 of cap free
Amount of items: 2
Items: 
Size: 618999 Color: 13
Size: 381001 Color: 16

Bin 303: 1 of cap free
Amount of items: 2
Items: 
Size: 625632 Color: 3
Size: 374368 Color: 14

Bin 304: 1 of cap free
Amount of items: 2
Items: 
Size: 627713 Color: 12
Size: 372287 Color: 15

Bin 305: 1 of cap free
Amount of items: 2
Items: 
Size: 629482 Color: 19
Size: 370518 Color: 0

Bin 306: 1 of cap free
Amount of items: 2
Items: 
Size: 638117 Color: 12
Size: 361883 Color: 8

Bin 307: 1 of cap free
Amount of items: 3
Items: 
Size: 639353 Color: 8
Size: 180888 Color: 15
Size: 179759 Color: 8

Bin 308: 1 of cap free
Amount of items: 3
Items: 
Size: 642374 Color: 17
Size: 180263 Color: 14
Size: 177363 Color: 9

Bin 309: 1 of cap free
Amount of items: 3
Items: 
Size: 645161 Color: 4
Size: 178272 Color: 12
Size: 176567 Color: 10

Bin 310: 1 of cap free
Amount of items: 2
Items: 
Size: 645875 Color: 9
Size: 354125 Color: 18

Bin 311: 1 of cap free
Amount of items: 3
Items: 
Size: 647396 Color: 0
Size: 176472 Color: 16
Size: 176132 Color: 16

Bin 312: 1 of cap free
Amount of items: 3
Items: 
Size: 661332 Color: 3
Size: 171495 Color: 16
Size: 167173 Color: 16

Bin 313: 1 of cap free
Amount of items: 3
Items: 
Size: 665363 Color: 14
Size: 168951 Color: 8
Size: 165686 Color: 16

Bin 314: 1 of cap free
Amount of items: 2
Items: 
Size: 665543 Color: 7
Size: 334457 Color: 6

Bin 315: 1 of cap free
Amount of items: 3
Items: 
Size: 666275 Color: 10
Size: 170415 Color: 17
Size: 163310 Color: 13

Bin 316: 1 of cap free
Amount of items: 3
Items: 
Size: 669643 Color: 2
Size: 166769 Color: 4
Size: 163588 Color: 18

Bin 317: 1 of cap free
Amount of items: 3
Items: 
Size: 681332 Color: 8
Size: 159667 Color: 5
Size: 159001 Color: 2

Bin 318: 1 of cap free
Amount of items: 2
Items: 
Size: 682789 Color: 3
Size: 317211 Color: 10

Bin 319: 1 of cap free
Amount of items: 2
Items: 
Size: 694371 Color: 10
Size: 305629 Color: 6

Bin 320: 1 of cap free
Amount of items: 2
Items: 
Size: 697154 Color: 3
Size: 302846 Color: 12

Bin 321: 1 of cap free
Amount of items: 3
Items: 
Size: 699514 Color: 5
Size: 151642 Color: 10
Size: 148844 Color: 14

Bin 322: 1 of cap free
Amount of items: 3
Items: 
Size: 704871 Color: 1
Size: 147720 Color: 13
Size: 147409 Color: 10

Bin 323: 1 of cap free
Amount of items: 3
Items: 
Size: 718211 Color: 3
Size: 146222 Color: 9
Size: 135567 Color: 14

Bin 324: 1 of cap free
Amount of items: 3
Items: 
Size: 718522 Color: 7
Size: 143182 Color: 0
Size: 138296 Color: 5

Bin 325: 1 of cap free
Amount of items: 3
Items: 
Size: 719606 Color: 10
Size: 143923 Color: 4
Size: 136471 Color: 10

Bin 326: 1 of cap free
Amount of items: 3
Items: 
Size: 721438 Color: 0
Size: 144890 Color: 13
Size: 133672 Color: 10

Bin 327: 1 of cap free
Amount of items: 3
Items: 
Size: 721892 Color: 11
Size: 140866 Color: 14
Size: 137242 Color: 12

Bin 328: 1 of cap free
Amount of items: 3
Items: 
Size: 723258 Color: 7
Size: 141724 Color: 18
Size: 135018 Color: 0

Bin 329: 1 of cap free
Amount of items: 3
Items: 
Size: 723671 Color: 8
Size: 141033 Color: 18
Size: 135296 Color: 6

Bin 330: 1 of cap free
Amount of items: 3
Items: 
Size: 723718 Color: 10
Size: 139592 Color: 8
Size: 136690 Color: 9

Bin 331: 1 of cap free
Amount of items: 3
Items: 
Size: 723792 Color: 5
Size: 139911 Color: 13
Size: 136297 Color: 14

Bin 332: 1 of cap free
Amount of items: 3
Items: 
Size: 724824 Color: 17
Size: 138886 Color: 18
Size: 136290 Color: 11

Bin 333: 1 of cap free
Amount of items: 3
Items: 
Size: 725381 Color: 15
Size: 139801 Color: 9
Size: 134818 Color: 0

Bin 334: 1 of cap free
Amount of items: 3
Items: 
Size: 725423 Color: 3
Size: 140202 Color: 4
Size: 134375 Color: 8

Bin 335: 1 of cap free
Amount of items: 2
Items: 
Size: 725550 Color: 2
Size: 274450 Color: 5

Bin 336: 1 of cap free
Amount of items: 3
Items: 
Size: 727060 Color: 14
Size: 138326 Color: 10
Size: 134614 Color: 18

Bin 337: 1 of cap free
Amount of items: 3
Items: 
Size: 728292 Color: 4
Size: 135975 Color: 13
Size: 135733 Color: 13

Bin 338: 1 of cap free
Amount of items: 2
Items: 
Size: 729739 Color: 4
Size: 270261 Color: 14

Bin 339: 1 of cap free
Amount of items: 3
Items: 
Size: 734970 Color: 1
Size: 133371 Color: 12
Size: 131659 Color: 9

Bin 340: 1 of cap free
Amount of items: 3
Items: 
Size: 741930 Color: 1
Size: 129111 Color: 15
Size: 128959 Color: 12

Bin 341: 1 of cap free
Amount of items: 3
Items: 
Size: 742116 Color: 5
Size: 129019 Color: 13
Size: 128865 Color: 11

Bin 342: 1 of cap free
Amount of items: 2
Items: 
Size: 748023 Color: 4
Size: 251977 Color: 2

Bin 343: 1 of cap free
Amount of items: 2
Items: 
Size: 754947 Color: 8
Size: 245053 Color: 18

Bin 344: 1 of cap free
Amount of items: 2
Items: 
Size: 757016 Color: 0
Size: 242984 Color: 7

Bin 345: 1 of cap free
Amount of items: 2
Items: 
Size: 757914 Color: 5
Size: 242086 Color: 0

Bin 346: 1 of cap free
Amount of items: 3
Items: 
Size: 759870 Color: 2
Size: 120830 Color: 18
Size: 119300 Color: 14

Bin 347: 1 of cap free
Amount of items: 2
Items: 
Size: 762278 Color: 17
Size: 237722 Color: 4

Bin 348: 1 of cap free
Amount of items: 3
Items: 
Size: 765511 Color: 6
Size: 117561 Color: 12
Size: 116928 Color: 5

Bin 349: 1 of cap free
Amount of items: 3
Items: 
Size: 766536 Color: 0
Size: 116804 Color: 17
Size: 116660 Color: 13

Bin 350: 1 of cap free
Amount of items: 2
Items: 
Size: 773196 Color: 7
Size: 226804 Color: 0

Bin 351: 1 of cap free
Amount of items: 2
Items: 
Size: 779185 Color: 1
Size: 220815 Color: 9

Bin 352: 1 of cap free
Amount of items: 2
Items: 
Size: 780705 Color: 7
Size: 219295 Color: 5

Bin 353: 1 of cap free
Amount of items: 3
Items: 
Size: 598288 Color: 15
Size: 208008 Color: 17
Size: 193704 Color: 4

Bin 354: 1 of cap free
Amount of items: 4
Items: 
Size: 599098 Color: 6
Size: 194156 Color: 0
Size: 104778 Color: 17
Size: 101968 Color: 0

Bin 355: 1 of cap free
Amount of items: 3
Items: 
Size: 668956 Color: 17
Size: 165706 Color: 17
Size: 165338 Color: 6

Bin 356: 1 of cap free
Amount of items: 3
Items: 
Size: 701543 Color: 9
Size: 152225 Color: 7
Size: 146232 Color: 11

Bin 357: 1 of cap free
Amount of items: 3
Items: 
Size: 603685 Color: 11
Size: 199644 Color: 7
Size: 196671 Color: 4

Bin 358: 1 of cap free
Amount of items: 3
Items: 
Size: 770441 Color: 18
Size: 115154 Color: 3
Size: 114405 Color: 8

Bin 359: 1 of cap free
Amount of items: 3
Items: 
Size: 602527 Color: 14
Size: 199234 Color: 17
Size: 198239 Color: 19

Bin 360: 1 of cap free
Amount of items: 3
Items: 
Size: 796289 Color: 16
Size: 102683 Color: 5
Size: 101028 Color: 10

Bin 361: 1 of cap free
Amount of items: 3
Items: 
Size: 627038 Color: 11
Size: 187121 Color: 10
Size: 185841 Color: 16

Bin 362: 1 of cap free
Amount of items: 3
Items: 
Size: 736670 Color: 5
Size: 131812 Color: 4
Size: 131518 Color: 2

Bin 363: 1 of cap free
Amount of items: 3
Items: 
Size: 705804 Color: 17
Size: 147304 Color: 2
Size: 146892 Color: 6

Bin 364: 1 of cap free
Amount of items: 2
Items: 
Size: 741631 Color: 16
Size: 258369 Color: 6

Bin 365: 1 of cap free
Amount of items: 3
Items: 
Size: 755506 Color: 0
Size: 126964 Color: 2
Size: 117530 Color: 3

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 424871 Color: 2
Size: 311869 Color: 19
Size: 263260 Color: 11

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 733442 Color: 16
Size: 133367 Color: 11
Size: 133191 Color: 6

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 741621 Color: 18
Size: 129380 Color: 9
Size: 128999 Color: 17

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 637452 Color: 5
Size: 182085 Color: 6
Size: 180463 Color: 10

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 766766 Color: 10
Size: 124065 Color: 0
Size: 109169 Color: 7

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 779216 Color: 4
Size: 110395 Color: 0
Size: 110389 Color: 8

Bin 372: 1 of cap free
Amount of items: 3
Items: 
Size: 774278 Color: 16
Size: 113284 Color: 11
Size: 112438 Color: 12

Bin 373: 1 of cap free
Amount of items: 3
Items: 
Size: 357058 Color: 2
Size: 321549 Color: 6
Size: 321393 Color: 5

Bin 374: 1 of cap free
Amount of items: 3
Items: 
Size: 379549 Color: 6
Size: 310364 Color: 4
Size: 310087 Color: 19

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 755678 Color: 12
Size: 122717 Color: 0
Size: 121605 Color: 8

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 761632 Color: 0
Size: 119607 Color: 2
Size: 118761 Color: 8

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 629162 Color: 18
Size: 185490 Color: 16
Size: 185348 Color: 0

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 779719 Color: 10
Size: 111097 Color: 13
Size: 109184 Color: 16

Bin 379: 1 of cap free
Amount of items: 2
Items: 
Size: 502733 Color: 1
Size: 497267 Color: 11

Bin 380: 1 of cap free
Amount of items: 2
Items: 
Size: 779238 Color: 8
Size: 220762 Color: 10

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 637564 Color: 19
Size: 183848 Color: 3
Size: 178588 Color: 3

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 771162 Color: 16
Size: 114582 Color: 5
Size: 114256 Color: 18

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 557887 Color: 6
Size: 253324 Color: 5
Size: 188789 Color: 16

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 779955 Color: 16
Size: 110247 Color: 11
Size: 109798 Color: 0

Bin 385: 1 of cap free
Amount of items: 2
Items: 
Size: 588120 Color: 3
Size: 411880 Color: 7

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 712717 Color: 6
Size: 144795 Color: 9
Size: 142488 Color: 5

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 364374 Color: 5
Size: 338261 Color: 5
Size: 297365 Color: 8

Bin 388: 1 of cap free
Amount of items: 2
Items: 
Size: 676708 Color: 3
Size: 323292 Color: 4

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 778288 Color: 17
Size: 111784 Color: 13
Size: 109928 Color: 5

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 359392 Color: 1
Size: 329445 Color: 11
Size: 311163 Color: 8

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 774541 Color: 19
Size: 120470 Color: 13
Size: 104989 Color: 0

Bin 392: 1 of cap free
Amount of items: 2
Items: 
Size: 753610 Color: 14
Size: 246390 Color: 8

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 755925 Color: 5
Size: 122594 Color: 3
Size: 121481 Color: 16

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 680973 Color: 16
Size: 161599 Color: 5
Size: 157428 Color: 1

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 676355 Color: 9
Size: 162282 Color: 1
Size: 161363 Color: 18

Bin 396: 1 of cap free
Amount of items: 2
Items: 
Size: 619110 Color: 5
Size: 380890 Color: 11

Bin 397: 1 of cap free
Amount of items: 2
Items: 
Size: 726039 Color: 14
Size: 273961 Color: 0

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 617148 Color: 7
Size: 192116 Color: 18
Size: 190736 Color: 1

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 654721 Color: 16
Size: 172662 Color: 4
Size: 172617 Color: 10

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 438567 Color: 15
Size: 283334 Color: 15
Size: 278099 Color: 7

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 789950 Color: 11
Size: 105120 Color: 19
Size: 104930 Color: 17

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 565895 Color: 13
Size: 242799 Color: 0
Size: 191306 Color: 3

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 358883 Color: 5
Size: 321058 Color: 4
Size: 320059 Color: 18

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 660553 Color: 2
Size: 172578 Color: 0
Size: 166869 Color: 13

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 778252 Color: 12
Size: 120262 Color: 17
Size: 101486 Color: 16

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 428431 Color: 6
Size: 299627 Color: 7
Size: 271942 Color: 13

Bin 407: 1 of cap free
Amount of items: 2
Items: 
Size: 637727 Color: 11
Size: 362273 Color: 9

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 790933 Color: 2
Size: 105403 Color: 2
Size: 103664 Color: 11

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 798803 Color: 8
Size: 101158 Color: 8
Size: 100039 Color: 9

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 416611 Color: 14
Size: 304027 Color: 5
Size: 279362 Color: 19

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 697869 Color: 5
Size: 151151 Color: 0
Size: 150980 Color: 10

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 368540 Color: 0
Size: 324441 Color: 2
Size: 307019 Color: 8

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 786441 Color: 8
Size: 113249 Color: 18
Size: 100310 Color: 12

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 744338 Color: 5
Size: 129357 Color: 13
Size: 126305 Color: 14

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 339184 Color: 17
Size: 333661 Color: 18
Size: 327155 Color: 9

Bin 416: 1 of cap free
Amount of items: 2
Items: 
Size: 791247 Color: 0
Size: 208753 Color: 14

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 728845 Color: 5
Size: 138017 Color: 7
Size: 133138 Color: 10

Bin 418: 1 of cap free
Amount of items: 2
Items: 
Size: 699746 Color: 9
Size: 300254 Color: 8

Bin 419: 1 of cap free
Amount of items: 2
Items: 
Size: 540126 Color: 12
Size: 459874 Color: 7

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 627191 Color: 12
Size: 186856 Color: 0
Size: 185953 Color: 0

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 438598 Color: 7
Size: 430633 Color: 15
Size: 130769 Color: 7

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 787312 Color: 8
Size: 109798 Color: 0
Size: 102890 Color: 5

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 344160 Color: 8
Size: 328362 Color: 3
Size: 327478 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 512503 Color: 2
Size: 254551 Color: 5
Size: 232946 Color: 18

Bin 425: 1 of cap free
Amount of items: 2
Items: 
Size: 683588 Color: 8
Size: 316412 Color: 3

Bin 426: 1 of cap free
Amount of items: 2
Items: 
Size: 645244 Color: 11
Size: 354756 Color: 2

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 779613 Color: 8
Size: 116131 Color: 2
Size: 104256 Color: 17

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 593867 Color: 8
Size: 206854 Color: 5
Size: 199279 Color: 8

Bin 429: 1 of cap free
Amount of items: 2
Items: 
Size: 500294 Color: 6
Size: 499706 Color: 5

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 378319 Color: 9
Size: 338782 Color: 7
Size: 282899 Color: 16

Bin 431: 1 of cap free
Amount of items: 4
Items: 
Size: 283279 Color: 7
Size: 281135 Color: 7
Size: 278534 Color: 10
Size: 157052 Color: 12

Bin 432: 2 of cap free
Amount of items: 3
Items: 
Size: 367467 Color: 1
Size: 349289 Color: 13
Size: 283243 Color: 8

Bin 433: 2 of cap free
Amount of items: 3
Items: 
Size: 370175 Color: 1
Size: 329045 Color: 12
Size: 300779 Color: 0

Bin 434: 2 of cap free
Amount of items: 3
Items: 
Size: 379101 Color: 5
Size: 312202 Color: 4
Size: 308696 Color: 15

Bin 435: 2 of cap free
Amount of items: 3
Items: 
Size: 388832 Color: 8
Size: 327652 Color: 1
Size: 283515 Color: 7

Bin 436: 2 of cap free
Amount of items: 3
Items: 
Size: 456908 Color: 9
Size: 279186 Color: 11
Size: 263905 Color: 4

Bin 437: 2 of cap free
Amount of items: 2
Items: 
Size: 510786 Color: 6
Size: 489213 Color: 17

Bin 438: 2 of cap free
Amount of items: 2
Items: 
Size: 514014 Color: 8
Size: 485985 Color: 0

Bin 439: 2 of cap free
Amount of items: 2
Items: 
Size: 518967 Color: 9
Size: 481032 Color: 13

Bin 440: 2 of cap free
Amount of items: 2
Items: 
Size: 523537 Color: 10
Size: 476462 Color: 4

Bin 441: 2 of cap free
Amount of items: 2
Items: 
Size: 529488 Color: 7
Size: 470511 Color: 18

Bin 442: 2 of cap free
Amount of items: 2
Items: 
Size: 554737 Color: 6
Size: 445262 Color: 1

Bin 443: 2 of cap free
Amount of items: 3
Items: 
Size: 563983 Color: 8
Size: 242255 Color: 7
Size: 193761 Color: 19

Bin 444: 2 of cap free
Amount of items: 3
Items: 
Size: 564554 Color: 0
Size: 242220 Color: 10
Size: 193225 Color: 16

Bin 445: 2 of cap free
Amount of items: 2
Items: 
Size: 569740 Color: 14
Size: 430259 Color: 10

Bin 446: 2 of cap free
Amount of items: 2
Items: 
Size: 573403 Color: 15
Size: 426596 Color: 9

Bin 447: 2 of cap free
Amount of items: 2
Items: 
Size: 576133 Color: 11
Size: 423866 Color: 15

Bin 448: 2 of cap free
Amount of items: 2
Items: 
Size: 578697 Color: 6
Size: 421302 Color: 10

Bin 449: 2 of cap free
Amount of items: 2
Items: 
Size: 590409 Color: 12
Size: 409590 Color: 16

Bin 450: 2 of cap free
Amount of items: 2
Items: 
Size: 595819 Color: 8
Size: 404180 Color: 19

Bin 451: 2 of cap free
Amount of items: 2
Items: 
Size: 599668 Color: 6
Size: 400331 Color: 18

Bin 452: 2 of cap free
Amount of items: 2
Items: 
Size: 602097 Color: 3
Size: 397902 Color: 5

Bin 453: 2 of cap free
Amount of items: 2
Items: 
Size: 609136 Color: 10
Size: 390863 Color: 19

Bin 454: 2 of cap free
Amount of items: 2
Items: 
Size: 626618 Color: 14
Size: 373381 Color: 19

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 630201 Color: 0
Size: 184958 Color: 17
Size: 184840 Color: 19

Bin 456: 2 of cap free
Amount of items: 2
Items: 
Size: 634197 Color: 11
Size: 365802 Color: 3

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 640957 Color: 16
Size: 179961 Color: 12
Size: 179081 Color: 9

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 642823 Color: 8
Size: 180995 Color: 12
Size: 176181 Color: 5

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 643056 Color: 1
Size: 178879 Color: 17
Size: 178064 Color: 10

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 643308 Color: 14
Size: 180186 Color: 15
Size: 176505 Color: 18

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 645516 Color: 14
Size: 177591 Color: 11
Size: 176892 Color: 7

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 651412 Color: 8
Size: 174406 Color: 12
Size: 174181 Color: 0

Bin 463: 2 of cap free
Amount of items: 2
Items: 
Size: 651898 Color: 3
Size: 348101 Color: 16

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 655734 Color: 2
Size: 172676 Color: 18
Size: 171589 Color: 15

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 655928 Color: 6
Size: 172591 Color: 0
Size: 171480 Color: 5

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 661743 Color: 3
Size: 170586 Color: 13
Size: 167670 Color: 4

Bin 467: 2 of cap free
Amount of items: 2
Items: 
Size: 663865 Color: 0
Size: 336134 Color: 15

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 671550 Color: 6
Size: 164484 Color: 14
Size: 163965 Color: 4

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 671587 Color: 4
Size: 165216 Color: 0
Size: 163196 Color: 16

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 673331 Color: 16
Size: 163436 Color: 9
Size: 163232 Color: 1

Bin 471: 2 of cap free
Amount of items: 2
Items: 
Size: 675441 Color: 4
Size: 324558 Color: 10

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 681097 Color: 5
Size: 159591 Color: 1
Size: 159311 Color: 1

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 688310 Color: 8
Size: 155963 Color: 6
Size: 155726 Color: 17

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 698985 Color: 10
Size: 151738 Color: 7
Size: 149276 Color: 11

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 704790 Color: 16
Size: 149474 Color: 16
Size: 145735 Color: 19

Bin 476: 2 of cap free
Amount of items: 2
Items: 
Size: 705692 Color: 18
Size: 294307 Color: 13

Bin 477: 2 of cap free
Amount of items: 2
Items: 
Size: 707284 Color: 12
Size: 292715 Color: 17

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 724509 Color: 3
Size: 137862 Color: 16
Size: 137628 Color: 16

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 734831 Color: 15
Size: 133679 Color: 5
Size: 131489 Color: 10

Bin 480: 2 of cap free
Amount of items: 2
Items: 
Size: 741253 Color: 13
Size: 258746 Color: 19

Bin 481: 2 of cap free
Amount of items: 2
Items: 
Size: 741266 Color: 4
Size: 258733 Color: 7

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 741293 Color: 12
Size: 129795 Color: 12
Size: 128911 Color: 18

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 745875 Color: 8
Size: 127560 Color: 8
Size: 126564 Color: 15

Bin 484: 2 of cap free
Amount of items: 2
Items: 
Size: 749385 Color: 16
Size: 250614 Color: 5

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 750178 Color: 13
Size: 125069 Color: 4
Size: 124752 Color: 17

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 752332 Color: 5
Size: 124663 Color: 16
Size: 123004 Color: 0

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 758188 Color: 7
Size: 121574 Color: 12
Size: 120237 Color: 3

Bin 488: 2 of cap free
Amount of items: 2
Items: 
Size: 766507 Color: 7
Size: 233492 Color: 0

Bin 489: 2 of cap free
Amount of items: 2
Items: 
Size: 793946 Color: 11
Size: 206053 Color: 12

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 662586 Color: 12
Size: 171919 Color: 18
Size: 165494 Color: 14

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 760806 Color: 12
Size: 119613 Color: 2
Size: 119580 Color: 9

Bin 492: 2 of cap free
Amount of items: 2
Items: 
Size: 744453 Color: 12
Size: 255546 Color: 7

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 745168 Color: 3
Size: 127905 Color: 1
Size: 126926 Color: 19

Bin 494: 2 of cap free
Amount of items: 2
Items: 
Size: 663054 Color: 3
Size: 336945 Color: 17

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 759460 Color: 14
Size: 120874 Color: 16
Size: 119665 Color: 9

Bin 496: 2 of cap free
Amount of items: 2
Items: 
Size: 693829 Color: 16
Size: 306170 Color: 17

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 677411 Color: 3
Size: 161355 Color: 15
Size: 161233 Color: 0

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 699412 Color: 17
Size: 150852 Color: 19
Size: 149735 Color: 2

Bin 499: 2 of cap free
Amount of items: 2
Items: 
Size: 627077 Color: 14
Size: 372922 Color: 7

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 368567 Color: 1
Size: 320200 Color: 7
Size: 311232 Color: 19

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 787763 Color: 2
Size: 106526 Color: 8
Size: 105710 Color: 18

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 359495 Color: 6
Size: 334427 Color: 17
Size: 306077 Color: 14

Bin 503: 2 of cap free
Amount of items: 2
Items: 
Size: 671847 Color: 13
Size: 328152 Color: 11

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 683419 Color: 17
Size: 160229 Color: 10
Size: 156351 Color: 4

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 780904 Color: 15
Size: 111521 Color: 17
Size: 107574 Color: 0

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 429562 Color: 0
Size: 292018 Color: 19
Size: 278419 Color: 1

Bin 507: 2 of cap free
Amount of items: 2
Items: 
Size: 507312 Color: 14
Size: 492687 Color: 13

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 581747 Color: 7
Size: 223525 Color: 18
Size: 194727 Color: 3

Bin 509: 2 of cap free
Amount of items: 3
Items: 
Size: 686127 Color: 8
Size: 157110 Color: 3
Size: 156762 Color: 0

Bin 510: 2 of cap free
Amount of items: 3
Items: 
Size: 358958 Color: 0
Size: 327744 Color: 3
Size: 313297 Color: 2

Bin 511: 2 of cap free
Amount of items: 3
Items: 
Size: 657513 Color: 19
Size: 171409 Color: 10
Size: 171077 Color: 8

Bin 512: 2 of cap free
Amount of items: 2
Items: 
Size: 631383 Color: 3
Size: 368616 Color: 5

Bin 513: 2 of cap free
Amount of items: 3
Items: 
Size: 612291 Color: 6
Size: 196914 Color: 3
Size: 190794 Color: 10

Bin 514: 2 of cap free
Amount of items: 3
Items: 
Size: 636521 Color: 8
Size: 182242 Color: 6
Size: 181236 Color: 17

Bin 515: 2 of cap free
Amount of items: 2
Items: 
Size: 796307 Color: 13
Size: 203692 Color: 19

Bin 516: 2 of cap free
Amount of items: 2
Items: 
Size: 710037 Color: 15
Size: 289962 Color: 11

Bin 517: 2 of cap free
Amount of items: 3
Items: 
Size: 439221 Color: 15
Size: 294613 Color: 16
Size: 266165 Color: 5

Bin 518: 2 of cap free
Amount of items: 3
Items: 
Size: 721179 Color: 15
Size: 144886 Color: 1
Size: 133934 Color: 2

Bin 519: 2 of cap free
Amount of items: 3
Items: 
Size: 777301 Color: 8
Size: 112227 Color: 9
Size: 110471 Color: 13

Bin 520: 2 of cap free
Amount of items: 3
Items: 
Size: 518118 Color: 17
Size: 254924 Color: 10
Size: 226957 Color: 3

Bin 521: 2 of cap free
Amount of items: 2
Items: 
Size: 789187 Color: 13
Size: 210812 Color: 8

Bin 522: 2 of cap free
Amount of items: 2
Items: 
Size: 776170 Color: 13
Size: 223829 Color: 4

Bin 523: 2 of cap free
Amount of items: 3
Items: 
Size: 561192 Color: 14
Size: 247018 Color: 18
Size: 191789 Color: 16

Bin 524: 2 of cap free
Amount of items: 3
Items: 
Size: 386951 Color: 7
Size: 333888 Color: 6
Size: 279160 Color: 12

Bin 525: 2 of cap free
Amount of items: 3
Items: 
Size: 719698 Color: 4
Size: 145505 Color: 3
Size: 134796 Color: 18

Bin 526: 2 of cap free
Amount of items: 3
Items: 
Size: 424646 Color: 10
Size: 292001 Color: 7
Size: 283352 Color: 5

Bin 527: 2 of cap free
Amount of items: 2
Items: 
Size: 696478 Color: 9
Size: 303521 Color: 1

Bin 528: 2 of cap free
Amount of items: 3
Items: 
Size: 510071 Color: 16
Size: 247820 Color: 19
Size: 242108 Color: 7

Bin 529: 2 of cap free
Amount of items: 3
Items: 
Size: 353502 Color: 4
Size: 338225 Color: 6
Size: 308272 Color: 8

Bin 530: 2 of cap free
Amount of items: 2
Items: 
Size: 763914 Color: 0
Size: 236085 Color: 3

Bin 531: 2 of cap free
Amount of items: 2
Items: 
Size: 653448 Color: 18
Size: 346551 Color: 15

Bin 532: 2 of cap free
Amount of items: 3
Items: 
Size: 593848 Color: 5
Size: 207270 Color: 6
Size: 198881 Color: 8

Bin 533: 2 of cap free
Amount of items: 3
Items: 
Size: 353049 Color: 14
Size: 351910 Color: 4
Size: 295040 Color: 7

Bin 534: 2 of cap free
Amount of items: 3
Items: 
Size: 371169 Color: 12
Size: 344189 Color: 6
Size: 284641 Color: 5

Bin 535: 3 of cap free
Amount of items: 3
Items: 
Size: 369604 Color: 0
Size: 324412 Color: 10
Size: 305982 Color: 18

Bin 536: 3 of cap free
Amount of items: 3
Items: 
Size: 370904 Color: 11
Size: 328918 Color: 10
Size: 300176 Color: 3

Bin 537: 3 of cap free
Amount of items: 3
Items: 
Size: 371715 Color: 14
Size: 333051 Color: 11
Size: 295232 Color: 9

Bin 538: 3 of cap free
Amount of items: 3
Items: 
Size: 388797 Color: 4
Size: 329516 Color: 5
Size: 281685 Color: 16

Bin 539: 3 of cap free
Amount of items: 2
Items: 
Size: 500242 Color: 0
Size: 499756 Color: 11

Bin 540: 3 of cap free
Amount of items: 2
Items: 
Size: 508509 Color: 10
Size: 491489 Color: 7

Bin 541: 3 of cap free
Amount of items: 2
Items: 
Size: 514352 Color: 11
Size: 485646 Color: 6

Bin 542: 3 of cap free
Amount of items: 2
Items: 
Size: 518585 Color: 4
Size: 481413 Color: 19

Bin 543: 3 of cap free
Amount of items: 2
Items: 
Size: 521832 Color: 4
Size: 478166 Color: 14

Bin 544: 3 of cap free
Amount of items: 2
Items: 
Size: 533760 Color: 15
Size: 466238 Color: 18

Bin 545: 3 of cap free
Amount of items: 3
Items: 
Size: 543023 Color: 9
Size: 260588 Color: 5
Size: 196387 Color: 19

Bin 546: 3 of cap free
Amount of items: 2
Items: 
Size: 548461 Color: 2
Size: 451537 Color: 16

Bin 547: 3 of cap free
Amount of items: 2
Items: 
Size: 548652 Color: 4
Size: 451346 Color: 19

Bin 548: 3 of cap free
Amount of items: 2
Items: 
Size: 550929 Color: 19
Size: 449069 Color: 7

Bin 549: 3 of cap free
Amount of items: 2
Items: 
Size: 556229 Color: 13
Size: 443769 Color: 10

Bin 550: 3 of cap free
Amount of items: 2
Items: 
Size: 566865 Color: 1
Size: 433133 Color: 4

Bin 551: 3 of cap free
Amount of items: 2
Items: 
Size: 570768 Color: 9
Size: 429230 Color: 4

Bin 552: 3 of cap free
Amount of items: 2
Items: 
Size: 572557 Color: 13
Size: 427441 Color: 7

Bin 553: 3 of cap free
Amount of items: 2
Items: 
Size: 594417 Color: 0
Size: 405581 Color: 4

Bin 554: 3 of cap free
Amount of items: 2
Items: 
Size: 594458 Color: 9
Size: 405540 Color: 12

Bin 555: 3 of cap free
Amount of items: 2
Items: 
Size: 595707 Color: 17
Size: 404291 Color: 11

Bin 556: 3 of cap free
Amount of items: 3
Items: 
Size: 598271 Color: 1
Size: 208196 Color: 5
Size: 193531 Color: 19

Bin 557: 3 of cap free
Amount of items: 3
Items: 
Size: 599661 Color: 9
Size: 205869 Color: 1
Size: 194468 Color: 3

Bin 558: 3 of cap free
Amount of items: 2
Items: 
Size: 606702 Color: 10
Size: 393296 Color: 11

Bin 559: 3 of cap free
Amount of items: 2
Items: 
Size: 610209 Color: 12
Size: 389789 Color: 9

Bin 560: 3 of cap free
Amount of items: 2
Items: 
Size: 611379 Color: 6
Size: 388619 Color: 19

Bin 561: 3 of cap free
Amount of items: 2
Items: 
Size: 614002 Color: 0
Size: 385996 Color: 5

Bin 562: 3 of cap free
Amount of items: 2
Items: 
Size: 618932 Color: 15
Size: 381066 Color: 11

Bin 563: 3 of cap free
Amount of items: 2
Items: 
Size: 623356 Color: 13
Size: 376642 Color: 7

Bin 564: 3 of cap free
Amount of items: 2
Items: 
Size: 628697 Color: 2
Size: 371301 Color: 16

Bin 565: 3 of cap free
Amount of items: 2
Items: 
Size: 629626 Color: 11
Size: 370372 Color: 16

Bin 566: 3 of cap free
Amount of items: 3
Items: 
Size: 630705 Color: 9
Size: 184702 Color: 5
Size: 184591 Color: 3

Bin 567: 3 of cap free
Amount of items: 2
Items: 
Size: 633224 Color: 18
Size: 366774 Color: 17

Bin 568: 3 of cap free
Amount of items: 3
Items: 
Size: 637658 Color: 19
Size: 185318 Color: 9
Size: 177022 Color: 3

Bin 569: 3 of cap free
Amount of items: 2
Items: 
Size: 651638 Color: 11
Size: 348360 Color: 12

Bin 570: 3 of cap free
Amount of items: 3
Items: 
Size: 662689 Color: 8
Size: 169615 Color: 11
Size: 167694 Color: 7

Bin 571: 3 of cap free
Amount of items: 3
Items: 
Size: 667609 Color: 9
Size: 166792 Color: 5
Size: 165597 Color: 14

Bin 572: 3 of cap free
Amount of items: 3
Items: 
Size: 668200 Color: 14
Size: 167855 Color: 0
Size: 163943 Color: 2

Bin 573: 3 of cap free
Amount of items: 3
Items: 
Size: 670013 Color: 1
Size: 166456 Color: 9
Size: 163529 Color: 9

Bin 574: 3 of cap free
Amount of items: 2
Items: 
Size: 671173 Color: 17
Size: 328825 Color: 18

Bin 575: 3 of cap free
Amount of items: 2
Items: 
Size: 671762 Color: 12
Size: 328236 Color: 8

Bin 576: 3 of cap free
Amount of items: 3
Items: 
Size: 673356 Color: 6
Size: 163542 Color: 15
Size: 163100 Color: 5

Bin 577: 3 of cap free
Amount of items: 2
Items: 
Size: 690793 Color: 16
Size: 309205 Color: 19

Bin 578: 3 of cap free
Amount of items: 3
Items: 
Size: 690856 Color: 2
Size: 155171 Color: 3
Size: 153971 Color: 19

Bin 579: 3 of cap free
Amount of items: 3
Items: 
Size: 691001 Color: 2
Size: 155166 Color: 10
Size: 153831 Color: 0

Bin 580: 3 of cap free
Amount of items: 3
Items: 
Size: 698692 Color: 14
Size: 151425 Color: 2
Size: 149881 Color: 10

Bin 581: 3 of cap free
Amount of items: 3
Items: 
Size: 701499 Color: 14
Size: 149599 Color: 9
Size: 148900 Color: 19

Bin 582: 3 of cap free
Amount of items: 2
Items: 
Size: 701558 Color: 7
Size: 298440 Color: 18

Bin 583: 3 of cap free
Amount of items: 2
Items: 
Size: 711185 Color: 15
Size: 288813 Color: 13

Bin 584: 3 of cap free
Amount of items: 2
Items: 
Size: 711527 Color: 19
Size: 288471 Color: 8

Bin 585: 3 of cap free
Amount of items: 2
Items: 
Size: 711750 Color: 14
Size: 288248 Color: 11

Bin 586: 3 of cap free
Amount of items: 3
Items: 
Size: 715681 Color: 17
Size: 142978 Color: 17
Size: 141339 Color: 14

Bin 587: 3 of cap free
Amount of items: 3
Items: 
Size: 717276 Color: 10
Size: 143533 Color: 2
Size: 139189 Color: 12

Bin 588: 3 of cap free
Amount of items: 3
Items: 
Size: 724085 Color: 3
Size: 140458 Color: 14
Size: 135455 Color: 12

Bin 589: 3 of cap free
Amount of items: 3
Items: 
Size: 725005 Color: 2
Size: 140755 Color: 11
Size: 134238 Color: 8

Bin 590: 3 of cap free
Amount of items: 2
Items: 
Size: 727500 Color: 15
Size: 272498 Color: 8

Bin 591: 3 of cap free
Amount of items: 2
Items: 
Size: 732286 Color: 18
Size: 267712 Color: 19

Bin 592: 3 of cap free
Amount of items: 2
Items: 
Size: 733228 Color: 13
Size: 266770 Color: 10

Bin 593: 3 of cap free
Amount of items: 3
Items: 
Size: 734815 Color: 19
Size: 133461 Color: 6
Size: 131722 Color: 16

Bin 594: 3 of cap free
Amount of items: 2
Items: 
Size: 741777 Color: 19
Size: 258221 Color: 14

Bin 595: 3 of cap free
Amount of items: 3
Items: 
Size: 745904 Color: 9
Size: 127697 Color: 0
Size: 126397 Color: 2

Bin 596: 3 of cap free
Amount of items: 2
Items: 
Size: 748055 Color: 17
Size: 251943 Color: 0

Bin 597: 3 of cap free
Amount of items: 3
Items: 
Size: 752359 Color: 1
Size: 124132 Color: 3
Size: 123507 Color: 6

Bin 598: 3 of cap free
Amount of items: 3
Items: 
Size: 752211 Color: 11
Size: 124452 Color: 4
Size: 123335 Color: 13

Bin 599: 3 of cap free
Amount of items: 2
Items: 
Size: 773159 Color: 3
Size: 226839 Color: 11

Bin 600: 3 of cap free
Amount of items: 3
Items: 
Size: 777323 Color: 5
Size: 111543 Color: 11
Size: 111132 Color: 6

Bin 601: 3 of cap free
Amount of items: 2
Items: 
Size: 783033 Color: 17
Size: 216965 Color: 7

Bin 602: 3 of cap free
Amount of items: 2
Items: 
Size: 784127 Color: 5
Size: 215871 Color: 11

Bin 603: 3 of cap free
Amount of items: 2
Items: 
Size: 785723 Color: 10
Size: 214275 Color: 13

Bin 604: 3 of cap free
Amount of items: 2
Items: 
Size: 788441 Color: 12
Size: 211557 Color: 7

Bin 605: 3 of cap free
Amount of items: 2
Items: 
Size: 793421 Color: 9
Size: 206577 Color: 7

Bin 606: 3 of cap free
Amount of items: 3
Items: 
Size: 651415 Color: 4
Size: 174528 Color: 6
Size: 174055 Color: 2

Bin 607: 3 of cap free
Amount of items: 3
Items: 
Size: 690954 Color: 13
Size: 155059 Color: 13
Size: 153985 Color: 9

Bin 608: 3 of cap free
Amount of items: 3
Items: 
Size: 759413 Color: 7
Size: 120794 Color: 10
Size: 119791 Color: 4

Bin 609: 3 of cap free
Amount of items: 2
Items: 
Size: 674128 Color: 16
Size: 325870 Color: 10

Bin 610: 3 of cap free
Amount of items: 3
Items: 
Size: 762137 Color: 1
Size: 119090 Color: 7
Size: 118771 Color: 19

Bin 611: 3 of cap free
Amount of items: 3
Items: 
Size: 564243 Color: 7
Size: 240338 Color: 14
Size: 195417 Color: 8

Bin 612: 3 of cap free
Amount of items: 3
Items: 
Size: 664987 Color: 17
Size: 167881 Color: 6
Size: 167130 Color: 7

Bin 613: 3 of cap free
Amount of items: 3
Items: 
Size: 751610 Color: 0
Size: 125659 Color: 11
Size: 122729 Color: 11

Bin 614: 3 of cap free
Amount of items: 3
Items: 
Size: 734430 Color: 13
Size: 132876 Color: 18
Size: 132692 Color: 5

Bin 615: 3 of cap free
Amount of items: 2
Items: 
Size: 556354 Color: 18
Size: 443644 Color: 13

Bin 616: 3 of cap free
Amount of items: 3
Items: 
Size: 768225 Color: 15
Size: 116676 Color: 4
Size: 115097 Color: 0

Bin 617: 3 of cap free
Amount of items: 3
Items: 
Size: 728077 Color: 7
Size: 138304 Color: 3
Size: 133617 Color: 2

Bin 618: 3 of cap free
Amount of items: 3
Items: 
Size: 411831 Color: 5
Size: 316610 Color: 12
Size: 271557 Color: 7

Bin 619: 3 of cap free
Amount of items: 3
Items: 
Size: 761626 Color: 19
Size: 119391 Color: 3
Size: 118981 Color: 4

Bin 620: 3 of cap free
Amount of items: 2
Items: 
Size: 617649 Color: 12
Size: 382349 Color: 11

Bin 621: 3 of cap free
Amount of items: 2
Items: 
Size: 773762 Color: 2
Size: 226236 Color: 1

Bin 622: 3 of cap free
Amount of items: 2
Items: 
Size: 576959 Color: 14
Size: 423039 Color: 19

Bin 623: 3 of cap free
Amount of items: 2
Items: 
Size: 617198 Color: 17
Size: 382800 Color: 4

Bin 624: 3 of cap free
Amount of items: 3
Items: 
Size: 780048 Color: 6
Size: 112539 Color: 1
Size: 107411 Color: 3

Bin 625: 3 of cap free
Amount of items: 3
Items: 
Size: 360433 Color: 5
Size: 358553 Color: 19
Size: 281012 Color: 11

Bin 626: 3 of cap free
Amount of items: 3
Items: 
Size: 755739 Color: 12
Size: 122850 Color: 19
Size: 121409 Color: 11

Bin 627: 3 of cap free
Amount of items: 3
Items: 
Size: 507285 Color: 7
Size: 254567 Color: 3
Size: 238146 Color: 8

Bin 628: 3 of cap free
Amount of items: 2
Items: 
Size: 603998 Color: 13
Size: 396000 Color: 0

Bin 629: 3 of cap free
Amount of items: 2
Items: 
Size: 554001 Color: 18
Size: 445997 Color: 11

Bin 630: 3 of cap free
Amount of items: 3
Items: 
Size: 770353 Color: 17
Size: 115047 Color: 13
Size: 114598 Color: 15

Bin 631: 3 of cap free
Amount of items: 3
Items: 
Size: 554782 Color: 6
Size: 248519 Color: 0
Size: 196697 Color: 1

Bin 632: 3 of cap free
Amount of items: 3
Items: 
Size: 514307 Color: 2
Size: 248884 Color: 14
Size: 236807 Color: 12

Bin 633: 3 of cap free
Amount of items: 3
Items: 
Size: 783878 Color: 14
Size: 109425 Color: 16
Size: 106695 Color: 4

Bin 634: 3 of cap free
Amount of items: 2
Items: 
Size: 665655 Color: 19
Size: 334343 Color: 2

Bin 635: 3 of cap free
Amount of items: 3
Items: 
Size: 430522 Color: 12
Size: 297286 Color: 5
Size: 272190 Color: 14

Bin 636: 3 of cap free
Amount of items: 3
Items: 
Size: 357366 Color: 14
Size: 322404 Color: 12
Size: 320228 Color: 10

Bin 637: 3 of cap free
Amount of items: 3
Items: 
Size: 349758 Color: 6
Size: 339863 Color: 0
Size: 310377 Color: 15

Bin 638: 3 of cap free
Amount of items: 2
Items: 
Size: 534861 Color: 9
Size: 465137 Color: 11

Bin 639: 3 of cap free
Amount of items: 3
Items: 
Size: 430199 Color: 7
Size: 296369 Color: 14
Size: 273430 Color: 8

Bin 640: 3 of cap free
Amount of items: 3
Items: 
Size: 344163 Color: 4
Size: 333877 Color: 16
Size: 321958 Color: 18

Bin 641: 3 of cap free
Amount of items: 2
Items: 
Size: 638287 Color: 12
Size: 361711 Color: 1

Bin 642: 3 of cap free
Amount of items: 2
Items: 
Size: 789614 Color: 1
Size: 210384 Color: 16

Bin 643: 3 of cap free
Amount of items: 3
Items: 
Size: 776645 Color: 12
Size: 118740 Color: 17
Size: 104613 Color: 17

Bin 644: 3 of cap free
Amount of items: 3
Items: 
Size: 353448 Color: 12
Size: 324416 Color: 7
Size: 322134 Color: 18

Bin 645: 3 of cap free
Amount of items: 3
Items: 
Size: 552213 Color: 14
Size: 248869 Color: 6
Size: 198916 Color: 15

Bin 646: 3 of cap free
Amount of items: 3
Items: 
Size: 593902 Color: 8
Size: 206775 Color: 2
Size: 199321 Color: 1

Bin 647: 3 of cap free
Amount of items: 3
Items: 
Size: 774741 Color: 2
Size: 115797 Color: 0
Size: 109460 Color: 12

Bin 648: 3 of cap free
Amount of items: 3
Items: 
Size: 375745 Color: 11
Size: 341355 Color: 8
Size: 282898 Color: 0

Bin 649: 3 of cap free
Amount of items: 3
Items: 
Size: 440194 Color: 13
Size: 281294 Color: 15
Size: 278510 Color: 5

Bin 650: 4 of cap free
Amount of items: 3
Items: 
Size: 367133 Color: 3
Size: 361629 Color: 0
Size: 271235 Color: 4

Bin 651: 4 of cap free
Amount of items: 3
Items: 
Size: 369158 Color: 1
Size: 354113 Color: 13
Size: 276726 Color: 4

Bin 652: 4 of cap free
Amount of items: 3
Items: 
Size: 372381 Color: 5
Size: 343183 Color: 9
Size: 284433 Color: 0

Bin 653: 4 of cap free
Amount of items: 2
Items: 
Size: 524161 Color: 2
Size: 475836 Color: 12

Bin 654: 4 of cap free
Amount of items: 2
Items: 
Size: 526537 Color: 19
Size: 473460 Color: 4

Bin 655: 4 of cap free
Amount of items: 3
Items: 
Size: 541293 Color: 3
Size: 263009 Color: 12
Size: 195695 Color: 3

Bin 656: 4 of cap free
Amount of items: 2
Items: 
Size: 542535 Color: 4
Size: 457462 Color: 5

Bin 657: 4 of cap free
Amount of items: 2
Items: 
Size: 544751 Color: 18
Size: 455246 Color: 19

Bin 658: 4 of cap free
Amount of items: 2
Items: 
Size: 556989 Color: 4
Size: 443008 Color: 5

Bin 659: 4 of cap free
Amount of items: 3
Items: 
Size: 557758 Color: 7
Size: 248799 Color: 2
Size: 193440 Color: 17

Bin 660: 4 of cap free
Amount of items: 2
Items: 
Size: 567150 Color: 9
Size: 432847 Color: 4

Bin 661: 4 of cap free
Amount of items: 2
Items: 
Size: 570490 Color: 4
Size: 429507 Color: 12

Bin 662: 4 of cap free
Amount of items: 2
Items: 
Size: 572193 Color: 12
Size: 427804 Color: 19

Bin 663: 4 of cap free
Amount of items: 2
Items: 
Size: 576464 Color: 17
Size: 423533 Color: 6

Bin 664: 4 of cap free
Amount of items: 2
Items: 
Size: 583658 Color: 2
Size: 416339 Color: 11

Bin 665: 4 of cap free
Amount of items: 2
Items: 
Size: 586066 Color: 16
Size: 413931 Color: 11

Bin 666: 4 of cap free
Amount of items: 2
Items: 
Size: 591269 Color: 6
Size: 408728 Color: 17

Bin 667: 4 of cap free
Amount of items: 2
Items: 
Size: 599187 Color: 15
Size: 400810 Color: 8

Bin 668: 4 of cap free
Amount of items: 2
Items: 
Size: 605611 Color: 9
Size: 394386 Color: 19

Bin 669: 4 of cap free
Amount of items: 2
Items: 
Size: 608241 Color: 1
Size: 391756 Color: 3

Bin 670: 4 of cap free
Amount of items: 3
Items: 
Size: 613351 Color: 5
Size: 194989 Color: 13
Size: 191657 Color: 15

Bin 671: 4 of cap free
Amount of items: 2
Items: 
Size: 616020 Color: 3
Size: 383977 Color: 13

Bin 672: 4 of cap free
Amount of items: 3
Items: 
Size: 619463 Color: 13
Size: 191674 Color: 19
Size: 188860 Color: 3

Bin 673: 4 of cap free
Amount of items: 3
Items: 
Size: 637436 Color: 10
Size: 182354 Color: 5
Size: 180207 Color: 3

Bin 674: 4 of cap free
Amount of items: 2
Items: 
Size: 641788 Color: 13
Size: 358209 Color: 12

Bin 675: 4 of cap free
Amount of items: 3
Items: 
Size: 643238 Color: 19
Size: 179820 Color: 7
Size: 176939 Color: 3

Bin 676: 4 of cap free
Amount of items: 3
Items: 
Size: 645168 Color: 8
Size: 177564 Color: 0
Size: 177265 Color: 3

Bin 677: 4 of cap free
Amount of items: 2
Items: 
Size: 652339 Color: 6
Size: 347658 Color: 8

Bin 678: 4 of cap free
Amount of items: 2
Items: 
Size: 653664 Color: 0
Size: 346333 Color: 4

Bin 679: 4 of cap free
Amount of items: 2
Items: 
Size: 654114 Color: 16
Size: 345883 Color: 11

Bin 680: 4 of cap free
Amount of items: 3
Items: 
Size: 665769 Color: 11
Size: 167670 Color: 6
Size: 166558 Color: 14

Bin 681: 4 of cap free
Amount of items: 3
Items: 
Size: 669601 Color: 9
Size: 165940 Color: 14
Size: 164456 Color: 1

Bin 682: 4 of cap free
Amount of items: 2
Items: 
Size: 669870 Color: 7
Size: 330127 Color: 17

Bin 683: 4 of cap free
Amount of items: 3
Items: 
Size: 676100 Color: 14
Size: 162097 Color: 12
Size: 161800 Color: 15

Bin 684: 4 of cap free
Amount of items: 3
Items: 
Size: 692147 Color: 18
Size: 154060 Color: 13
Size: 153790 Color: 9

Bin 685: 4 of cap free
Amount of items: 3
Items: 
Size: 699122 Color: 0
Size: 151814 Color: 19
Size: 149061 Color: 18

Bin 686: 4 of cap free
Amount of items: 2
Items: 
Size: 705265 Color: 12
Size: 294732 Color: 2

Bin 687: 4 of cap free
Amount of items: 2
Items: 
Size: 709912 Color: 16
Size: 290085 Color: 18

Bin 688: 4 of cap free
Amount of items: 2
Items: 
Size: 710332 Color: 10
Size: 289665 Color: 11

Bin 689: 4 of cap free
Amount of items: 3
Items: 
Size: 712258 Color: 19
Size: 143888 Color: 17
Size: 143851 Color: 1

Bin 690: 4 of cap free
Amount of items: 3
Items: 
Size: 717129 Color: 7
Size: 143762 Color: 19
Size: 139106 Color: 9

Bin 691: 4 of cap free
Amount of items: 3
Items: 
Size: 717216 Color: 4
Size: 141435 Color: 12
Size: 141346 Color: 13

Bin 692: 4 of cap free
Amount of items: 3
Items: 
Size: 718495 Color: 16
Size: 141011 Color: 7
Size: 140491 Color: 4

Bin 693: 4 of cap free
Amount of items: 3
Items: 
Size: 719042 Color: 3
Size: 144681 Color: 17
Size: 136274 Color: 15

Bin 694: 4 of cap free
Amount of items: 3
Items: 
Size: 718716 Color: 5
Size: 143753 Color: 19
Size: 137528 Color: 13

Bin 695: 4 of cap free
Amount of items: 3
Items: 
Size: 719012 Color: 15
Size: 144021 Color: 8
Size: 136964 Color: 8

Bin 696: 4 of cap free
Amount of items: 3
Items: 
Size: 720830 Color: 19
Size: 143106 Color: 0
Size: 136061 Color: 8

Bin 697: 4 of cap free
Amount of items: 3
Items: 
Size: 724334 Color: 13
Size: 140304 Color: 10
Size: 135359 Color: 4

Bin 698: 4 of cap free
Amount of items: 3
Items: 
Size: 724910 Color: 15
Size: 139860 Color: 19
Size: 135227 Color: 10

Bin 699: 4 of cap free
Amount of items: 3
Items: 
Size: 725867 Color: 17
Size: 139002 Color: 7
Size: 135128 Color: 6

Bin 700: 4 of cap free
Amount of items: 2
Items: 
Size: 730593 Color: 14
Size: 269404 Color: 15

Bin 701: 4 of cap free
Amount of items: 3
Items: 
Size: 739145 Color: 11
Size: 130517 Color: 11
Size: 130335 Color: 19

Bin 702: 4 of cap free
Amount of items: 3
Items: 
Size: 742096 Color: 2
Size: 129317 Color: 13
Size: 128584 Color: 3

Bin 703: 4 of cap free
Amount of items: 2
Items: 
Size: 742543 Color: 4
Size: 257454 Color: 2

Bin 704: 4 of cap free
Amount of items: 3
Items: 
Size: 751523 Color: 2
Size: 125796 Color: 13
Size: 122678 Color: 17

Bin 705: 4 of cap free
Amount of items: 3
Items: 
Size: 753354 Color: 3
Size: 123399 Color: 19
Size: 123244 Color: 7

Bin 706: 4 of cap free
Amount of items: 2
Items: 
Size: 758770 Color: 0
Size: 241227 Color: 5

Bin 707: 4 of cap free
Amount of items: 3
Items: 
Size: 762935 Color: 13
Size: 118697 Color: 17
Size: 118365 Color: 7

Bin 708: 4 of cap free
Amount of items: 2
Items: 
Size: 763756 Color: 4
Size: 236241 Color: 1

Bin 709: 4 of cap free
Amount of items: 2
Items: 
Size: 789284 Color: 14
Size: 210713 Color: 17

Bin 710: 4 of cap free
Amount of items: 2
Items: 
Size: 794521 Color: 10
Size: 205476 Color: 4

Bin 711: 4 of cap free
Amount of items: 3
Items: 
Size: 720120 Color: 19
Size: 143737 Color: 2
Size: 136140 Color: 4

Bin 712: 4 of cap free
Amount of items: 3
Items: 
Size: 598279 Color: 16
Size: 204572 Color: 12
Size: 197146 Color: 0

Bin 713: 4 of cap free
Amount of items: 3
Items: 
Size: 648577 Color: 13
Size: 175760 Color: 0
Size: 175660 Color: 19

Bin 714: 4 of cap free
Amount of items: 3
Items: 
Size: 741498 Color: 0
Size: 129295 Color: 0
Size: 129204 Color: 11

Bin 715: 4 of cap free
Amount of items: 3
Items: 
Size: 646878 Color: 19
Size: 177058 Color: 18
Size: 176061 Color: 10

Bin 716: 4 of cap free
Amount of items: 2
Items: 
Size: 631528 Color: 9
Size: 368469 Color: 0

Bin 717: 4 of cap free
Amount of items: 3
Items: 
Size: 663040 Color: 2
Size: 169393 Color: 9
Size: 167564 Color: 6

Bin 718: 4 of cap free
Amount of items: 3
Items: 
Size: 367225 Color: 1
Size: 366868 Color: 14
Size: 265904 Color: 14

Bin 719: 4 of cap free
Amount of items: 3
Items: 
Size: 370185 Color: 1
Size: 329692 Color: 15
Size: 300120 Color: 16

Bin 720: 4 of cap free
Amount of items: 2
Items: 
Size: 625636 Color: 10
Size: 374361 Color: 15

Bin 721: 4 of cap free
Amount of items: 3
Items: 
Size: 685553 Color: 4
Size: 158003 Color: 12
Size: 156441 Color: 2

Bin 722: 4 of cap free
Amount of items: 2
Items: 
Size: 640235 Color: 8
Size: 359762 Color: 1

Bin 723: 4 of cap free
Amount of items: 3
Items: 
Size: 782034 Color: 2
Size: 109191 Color: 11
Size: 108772 Color: 16

Bin 724: 4 of cap free
Amount of items: 3
Items: 
Size: 714989 Color: 10
Size: 144345 Color: 3
Size: 140663 Color: 11

Bin 725: 4 of cap free
Amount of items: 2
Items: 
Size: 783421 Color: 19
Size: 216576 Color: 17

Bin 726: 4 of cap free
Amount of items: 2
Items: 
Size: 510308 Color: 1
Size: 489689 Color: 18

Bin 727: 4 of cap free
Amount of items: 3
Items: 
Size: 369730 Color: 5
Size: 358606 Color: 3
Size: 271661 Color: 9

Bin 728: 4 of cap free
Amount of items: 3
Items: 
Size: 636726 Color: 11
Size: 183638 Color: 14
Size: 179633 Color: 18

Bin 729: 4 of cap free
Amount of items: 3
Items: 
Size: 677607 Color: 4
Size: 169045 Color: 4
Size: 153345 Color: 5

Bin 730: 4 of cap free
Amount of items: 3
Items: 
Size: 630343 Color: 8
Size: 184928 Color: 5
Size: 184726 Color: 15

Bin 731: 4 of cap free
Amount of items: 2
Items: 
Size: 538269 Color: 14
Size: 461728 Color: 4

Bin 732: 4 of cap free
Amount of items: 3
Items: 
Size: 749895 Color: 18
Size: 125208 Color: 18
Size: 124894 Color: 3

Bin 733: 4 of cap free
Amount of items: 3
Items: 
Size: 761683 Color: 6
Size: 124461 Color: 2
Size: 113853 Color: 7

Bin 734: 4 of cap free
Amount of items: 2
Items: 
Size: 665319 Color: 19
Size: 334678 Color: 11

Bin 735: 4 of cap free
Amount of items: 2
Items: 
Size: 567768 Color: 14
Size: 432229 Color: 12

Bin 736: 4 of cap free
Amount of items: 3
Items: 
Size: 738181 Color: 13
Size: 134892 Color: 3
Size: 126924 Color: 2

Bin 737: 4 of cap free
Amount of items: 3
Items: 
Size: 478149 Color: 0
Size: 260958 Color: 8
Size: 260890 Color: 13

Bin 738: 4 of cap free
Amount of items: 2
Items: 
Size: 766721 Color: 11
Size: 233276 Color: 14

Bin 739: 4 of cap free
Amount of items: 3
Items: 
Size: 513289 Color: 2
Size: 248639 Color: 11
Size: 238069 Color: 0

Bin 740: 4 of cap free
Amount of items: 2
Items: 
Size: 656764 Color: 17
Size: 343233 Color: 12

Bin 741: 4 of cap free
Amount of items: 3
Items: 
Size: 541169 Color: 5
Size: 262878 Color: 14
Size: 195950 Color: 7

Bin 742: 4 of cap free
Amount of items: 3
Items: 
Size: 359477 Color: 9
Size: 328622 Color: 2
Size: 311898 Color: 19

Bin 743: 4 of cap free
Amount of items: 2
Items: 
Size: 512971 Color: 5
Size: 487026 Color: 19

Bin 744: 4 of cap free
Amount of items: 3
Items: 
Size: 486276 Color: 16
Size: 266126 Color: 15
Size: 247595 Color: 11

Bin 745: 4 of cap free
Amount of items: 2
Items: 
Size: 772461 Color: 10
Size: 227536 Color: 7

Bin 746: 4 of cap free
Amount of items: 3
Items: 
Size: 382462 Color: 9
Size: 335689 Color: 2
Size: 281846 Color: 4

Bin 747: 5 of cap free
Amount of items: 3
Items: 
Size: 667240 Color: 15
Size: 168698 Color: 11
Size: 164058 Color: 4

Bin 748: 5 of cap free
Amount of items: 3
Items: 
Size: 358901 Color: 19
Size: 328935 Color: 6
Size: 312160 Color: 15

Bin 749: 5 of cap free
Amount of items: 3
Items: 
Size: 373979 Color: 2
Size: 331069 Color: 4
Size: 294948 Color: 17

Bin 750: 5 of cap free
Amount of items: 2
Items: 
Size: 500513 Color: 17
Size: 499483 Color: 13

Bin 751: 5 of cap free
Amount of items: 2
Items: 
Size: 508260 Color: 16
Size: 491736 Color: 4

Bin 752: 5 of cap free
Amount of items: 2
Items: 
Size: 524310 Color: 10
Size: 475686 Color: 14

Bin 753: 5 of cap free
Amount of items: 2
Items: 
Size: 539882 Color: 3
Size: 460114 Color: 10

Bin 754: 5 of cap free
Amount of items: 2
Items: 
Size: 540061 Color: 19
Size: 459935 Color: 16

Bin 755: 5 of cap free
Amount of items: 2
Items: 
Size: 546522 Color: 6
Size: 453474 Color: 9

Bin 756: 5 of cap free
Amount of items: 2
Items: 
Size: 554731 Color: 14
Size: 445265 Color: 13

Bin 757: 5 of cap free
Amount of items: 2
Items: 
Size: 559533 Color: 11
Size: 440463 Color: 6

Bin 758: 5 of cap free
Amount of items: 2
Items: 
Size: 572385 Color: 10
Size: 427611 Color: 7

Bin 759: 5 of cap free
Amount of items: 2
Items: 
Size: 574660 Color: 18
Size: 425336 Color: 3

Bin 760: 5 of cap free
Amount of items: 2
Items: 
Size: 584437 Color: 14
Size: 415559 Color: 4

Bin 761: 5 of cap free
Amount of items: 2
Items: 
Size: 592456 Color: 9
Size: 407540 Color: 15

Bin 762: 5 of cap free
Amount of items: 2
Items: 
Size: 593217 Color: 8
Size: 406779 Color: 0

Bin 763: 5 of cap free
Amount of items: 2
Items: 
Size: 593265 Color: 16
Size: 406731 Color: 7

Bin 764: 5 of cap free
Amount of items: 2
Items: 
Size: 610076 Color: 13
Size: 389920 Color: 8

Bin 765: 5 of cap free
Amount of items: 2
Items: 
Size: 613113 Color: 8
Size: 386883 Color: 6

Bin 766: 5 of cap free
Amount of items: 2
Items: 
Size: 619385 Color: 6
Size: 380611 Color: 8

Bin 767: 5 of cap free
Amount of items: 3
Items: 
Size: 621273 Color: 10
Size: 189506 Color: 3
Size: 189217 Color: 18

Bin 768: 5 of cap free
Amount of items: 3
Items: 
Size: 642989 Color: 16
Size: 178964 Color: 11
Size: 178043 Color: 13

Bin 769: 5 of cap free
Amount of items: 2
Items: 
Size: 652329 Color: 12
Size: 347667 Color: 5

Bin 770: 5 of cap free
Amount of items: 2
Items: 
Size: 657690 Color: 5
Size: 342306 Color: 10

Bin 771: 5 of cap free
Amount of items: 2
Items: 
Size: 661972 Color: 17
Size: 338024 Color: 12

Bin 772: 5 of cap free
Amount of items: 3
Items: 
Size: 666576 Color: 17
Size: 170122 Color: 7
Size: 163298 Color: 14

Bin 773: 5 of cap free
Amount of items: 3
Items: 
Size: 667228 Color: 9
Size: 166778 Color: 13
Size: 165990 Color: 2

Bin 774: 5 of cap free
Amount of items: 2
Items: 
Size: 685743 Color: 17
Size: 314253 Color: 16

Bin 775: 5 of cap free
Amount of items: 2
Items: 
Size: 687765 Color: 4
Size: 312231 Color: 2

Bin 776: 5 of cap free
Amount of items: 2
Items: 
Size: 690329 Color: 1
Size: 309667 Color: 11

Bin 777: 5 of cap free
Amount of items: 2
Items: 
Size: 690295 Color: 12
Size: 309701 Color: 19

Bin 778: 5 of cap free
Amount of items: 3
Items: 
Size: 692681 Color: 14
Size: 153750 Color: 10
Size: 153565 Color: 3

Bin 779: 5 of cap free
Amount of items: 2
Items: 
Size: 703375 Color: 0
Size: 296621 Color: 6

Bin 780: 5 of cap free
Amount of items: 2
Items: 
Size: 706593 Color: 7
Size: 293403 Color: 8

Bin 781: 5 of cap free
Amount of items: 3
Items: 
Size: 718956 Color: 14
Size: 143463 Color: 1
Size: 137577 Color: 12

Bin 782: 5 of cap free
Amount of items: 3
Items: 
Size: 727794 Color: 6
Size: 136330 Color: 1
Size: 135872 Color: 13

Bin 783: 5 of cap free
Amount of items: 2
Items: 
Size: 729895 Color: 9
Size: 270101 Color: 7

Bin 784: 5 of cap free
Amount of items: 3
Items: 
Size: 735570 Color: 12
Size: 132215 Color: 3
Size: 132211 Color: 0

Bin 785: 5 of cap free
Amount of items: 2
Items: 
Size: 738284 Color: 1
Size: 261712 Color: 7

Bin 786: 5 of cap free
Amount of items: 2
Items: 
Size: 750691 Color: 18
Size: 249305 Color: 6

Bin 787: 5 of cap free
Amount of items: 3
Items: 
Size: 757724 Color: 13
Size: 121164 Color: 16
Size: 121108 Color: 17

Bin 788: 5 of cap free
Amount of items: 2
Items: 
Size: 759259 Color: 0
Size: 240737 Color: 15

Bin 789: 5 of cap free
Amount of items: 2
Items: 
Size: 759417 Color: 7
Size: 240579 Color: 9

Bin 790: 5 of cap free
Amount of items: 2
Items: 
Size: 768988 Color: 15
Size: 231008 Color: 4

Bin 791: 5 of cap free
Amount of items: 2
Items: 
Size: 769873 Color: 0
Size: 230123 Color: 19

Bin 792: 5 of cap free
Amount of items: 2
Items: 
Size: 782432 Color: 8
Size: 217564 Color: 12

Bin 793: 5 of cap free
Amount of items: 2
Items: 
Size: 784198 Color: 16
Size: 215798 Color: 4

Bin 794: 5 of cap free
Amount of items: 2
Items: 
Size: 787948 Color: 0
Size: 212048 Color: 5

Bin 795: 5 of cap free
Amount of items: 2
Items: 
Size: 797507 Color: 0
Size: 202489 Color: 1

Bin 796: 5 of cap free
Amount of items: 3
Items: 
Size: 647582 Color: 10
Size: 176879 Color: 18
Size: 175535 Color: 1

Bin 797: 5 of cap free
Amount of items: 3
Items: 
Size: 637748 Color: 17
Size: 183164 Color: 18
Size: 179084 Color: 14

Bin 798: 5 of cap free
Amount of items: 3
Items: 
Size: 627375 Color: 3
Size: 186996 Color: 10
Size: 185625 Color: 16

Bin 799: 5 of cap free
Amount of items: 3
Items: 
Size: 369698 Color: 15
Size: 364353 Color: 8
Size: 265945 Color: 15

Bin 800: 5 of cap free
Amount of items: 3
Items: 
Size: 406653 Color: 18
Size: 310647 Color: 14
Size: 282696 Color: 12

Bin 801: 5 of cap free
Amount of items: 3
Items: 
Size: 667252 Color: 17
Size: 167118 Color: 16
Size: 165626 Color: 8

Bin 802: 5 of cap free
Amount of items: 2
Items: 
Size: 644636 Color: 14
Size: 355360 Color: 11

Bin 803: 5 of cap free
Amount of items: 3
Items: 
Size: 733488 Color: 5
Size: 133329 Color: 19
Size: 133179 Color: 13

Bin 804: 5 of cap free
Amount of items: 3
Items: 
Size: 750210 Color: 4
Size: 125090 Color: 4
Size: 124696 Color: 13

Bin 805: 5 of cap free
Amount of items: 3
Items: 
Size: 416210 Color: 6
Size: 299608 Color: 1
Size: 284178 Color: 18

Bin 806: 5 of cap free
Amount of items: 2
Items: 
Size: 634296 Color: 9
Size: 365700 Color: 0

Bin 807: 5 of cap free
Amount of items: 3
Items: 
Size: 370762 Color: 18
Size: 320968 Color: 5
Size: 308266 Color: 11

Bin 808: 5 of cap free
Amount of items: 3
Items: 
Size: 738404 Color: 6
Size: 130944 Color: 1
Size: 130648 Color: 5

Bin 809: 5 of cap free
Amount of items: 3
Items: 
Size: 689628 Color: 13
Size: 155256 Color: 11
Size: 155112 Color: 2

Bin 810: 5 of cap free
Amount of items: 2
Items: 
Size: 525547 Color: 15
Size: 474449 Color: 18

Bin 811: 5 of cap free
Amount of items: 3
Items: 
Size: 679111 Color: 13
Size: 160500 Color: 7
Size: 160385 Color: 13

Bin 812: 5 of cap free
Amount of items: 2
Items: 
Size: 521221 Color: 12
Size: 478775 Color: 4

Bin 813: 5 of cap free
Amount of items: 3
Items: 
Size: 710435 Color: 1
Size: 152302 Color: 3
Size: 137259 Color: 10

Bin 814: 5 of cap free
Amount of items: 3
Items: 
Size: 439267 Color: 15
Size: 296256 Color: 5
Size: 264473 Color: 8

Bin 815: 5 of cap free
Amount of items: 3
Items: 
Size: 776741 Color: 0
Size: 111703 Color: 8
Size: 111552 Color: 0

Bin 816: 5 of cap free
Amount of items: 3
Items: 
Size: 676632 Color: 6
Size: 163408 Color: 7
Size: 159956 Color: 2

Bin 817: 5 of cap free
Amount of items: 3
Items: 
Size: 723473 Color: 4
Size: 142094 Color: 3
Size: 134429 Color: 14

Bin 818: 5 of cap free
Amount of items: 3
Items: 
Size: 754489 Color: 15
Size: 143828 Color: 5
Size: 101679 Color: 7

Bin 819: 5 of cap free
Amount of items: 2
Items: 
Size: 549385 Color: 15
Size: 450611 Color: 10

Bin 820: 5 of cap free
Amount of items: 2
Items: 
Size: 619177 Color: 3
Size: 380819 Color: 10

Bin 821: 5 of cap free
Amount of items: 3
Items: 
Size: 484090 Color: 7
Size: 263232 Color: 19
Size: 252674 Color: 18

Bin 822: 5 of cap free
Amount of items: 2
Items: 
Size: 756569 Color: 14
Size: 243427 Color: 4

Bin 823: 5 of cap free
Amount of items: 3
Items: 
Size: 337317 Color: 12
Size: 332003 Color: 2
Size: 330676 Color: 18

Bin 824: 5 of cap free
Amount of items: 2
Items: 
Size: 652238 Color: 18
Size: 347758 Color: 1

Bin 825: 5 of cap free
Amount of items: 3
Items: 
Size: 344115 Color: 13
Size: 334307 Color: 12
Size: 321574 Color: 1

Bin 826: 6 of cap free
Amount of items: 3
Items: 
Size: 669643 Color: 1
Size: 165335 Color: 18
Size: 165017 Color: 8

Bin 827: 6 of cap free
Amount of items: 3
Items: 
Size: 369531 Color: 5
Size: 330668 Color: 14
Size: 299796 Color: 1

Bin 828: 6 of cap free
Amount of items: 3
Items: 
Size: 379649 Color: 14
Size: 329013 Color: 17
Size: 291333 Color: 11

Bin 829: 6 of cap free
Amount of items: 2
Items: 
Size: 500853 Color: 6
Size: 499142 Color: 8

Bin 830: 6 of cap free
Amount of items: 2
Items: 
Size: 505215 Color: 7
Size: 494780 Color: 4

Bin 831: 6 of cap free
Amount of items: 2
Items: 
Size: 520295 Color: 10
Size: 479700 Color: 1

Bin 832: 6 of cap free
Amount of items: 2
Items: 
Size: 551594 Color: 3
Size: 448401 Color: 2

Bin 833: 6 of cap free
Amount of items: 3
Items: 
Size: 564461 Color: 2
Size: 238816 Color: 8
Size: 196718 Color: 12

Bin 834: 6 of cap free
Amount of items: 2
Items: 
Size: 585146 Color: 2
Size: 414849 Color: 9

Bin 835: 6 of cap free
Amount of items: 2
Items: 
Size: 588277 Color: 12
Size: 411718 Color: 7

Bin 836: 6 of cap free
Amount of items: 2
Items: 
Size: 593372 Color: 16
Size: 406623 Color: 17

Bin 837: 6 of cap free
Amount of items: 2
Items: 
Size: 594274 Color: 2
Size: 405721 Color: 4

Bin 838: 6 of cap free
Amount of items: 2
Items: 
Size: 594664 Color: 7
Size: 405331 Color: 2

Bin 839: 6 of cap free
Amount of items: 2
Items: 
Size: 596150 Color: 18
Size: 403845 Color: 19

Bin 840: 6 of cap free
Amount of items: 3
Items: 
Size: 604145 Color: 5
Size: 198337 Color: 11
Size: 197513 Color: 15

Bin 841: 6 of cap free
Amount of items: 2
Items: 
Size: 606539 Color: 6
Size: 393456 Color: 15

Bin 842: 6 of cap free
Amount of items: 2
Items: 
Size: 606870 Color: 16
Size: 393125 Color: 3

Bin 843: 6 of cap free
Amount of items: 3
Items: 
Size: 616349 Color: 14
Size: 193102 Color: 2
Size: 190544 Color: 9

Bin 844: 6 of cap free
Amount of items: 2
Items: 
Size: 617769 Color: 3
Size: 382226 Color: 11

Bin 845: 6 of cap free
Amount of items: 3
Items: 
Size: 627970 Color: 13
Size: 186216 Color: 18
Size: 185809 Color: 10

Bin 846: 6 of cap free
Amount of items: 3
Items: 
Size: 632082 Color: 13
Size: 183989 Color: 7
Size: 183924 Color: 2

Bin 847: 6 of cap free
Amount of items: 2
Items: 
Size: 645433 Color: 17
Size: 354562 Color: 10

Bin 848: 6 of cap free
Amount of items: 2
Items: 
Size: 650461 Color: 17
Size: 349534 Color: 5

Bin 849: 6 of cap free
Amount of items: 2
Items: 
Size: 653201 Color: 7
Size: 346794 Color: 15

Bin 850: 6 of cap free
Amount of items: 2
Items: 
Size: 654902 Color: 4
Size: 345093 Color: 15

Bin 851: 6 of cap free
Amount of items: 2
Items: 
Size: 659367 Color: 19
Size: 340628 Color: 9

Bin 852: 6 of cap free
Amount of items: 3
Items: 
Size: 660897 Color: 9
Size: 169629 Color: 16
Size: 169469 Color: 10

Bin 853: 6 of cap free
Amount of items: 2
Items: 
Size: 693277 Color: 12
Size: 306718 Color: 15

Bin 854: 6 of cap free
Amount of items: 3
Items: 
Size: 717516 Color: 13
Size: 143067 Color: 18
Size: 139412 Color: 8

Bin 855: 6 of cap free
Amount of items: 3
Items: 
Size: 717971 Color: 8
Size: 141241 Color: 17
Size: 140783 Color: 15

Bin 856: 6 of cap free
Amount of items: 3
Items: 
Size: 718693 Color: 4
Size: 143915 Color: 16
Size: 137387 Color: 9

Bin 857: 6 of cap free
Amount of items: 2
Items: 
Size: 722267 Color: 15
Size: 277728 Color: 1

Bin 858: 6 of cap free
Amount of items: 3
Items: 
Size: 722409 Color: 12
Size: 142045 Color: 3
Size: 135541 Color: 1

Bin 859: 6 of cap free
Amount of items: 2
Items: 
Size: 731515 Color: 7
Size: 268480 Color: 8

Bin 860: 6 of cap free
Amount of items: 2
Items: 
Size: 747516 Color: 14
Size: 252479 Color: 7

Bin 861: 6 of cap free
Amount of items: 2
Items: 
Size: 748400 Color: 15
Size: 251595 Color: 8

Bin 862: 6 of cap free
Amount of items: 2
Items: 
Size: 751012 Color: 9
Size: 248983 Color: 12

Bin 863: 6 of cap free
Amount of items: 3
Items: 
Size: 751878 Color: 18
Size: 124092 Color: 2
Size: 124025 Color: 7

Bin 864: 6 of cap free
Amount of items: 2
Items: 
Size: 757357 Color: 13
Size: 242638 Color: 5

Bin 865: 6 of cap free
Amount of items: 2
Items: 
Size: 792047 Color: 16
Size: 207948 Color: 4

Bin 866: 6 of cap free
Amount of items: 2
Items: 
Size: 795831 Color: 13
Size: 204164 Color: 17

Bin 867: 6 of cap free
Amount of items: 2
Items: 
Size: 796118 Color: 0
Size: 203877 Color: 13

Bin 868: 6 of cap free
Amount of items: 2
Items: 
Size: 796500 Color: 14
Size: 203495 Color: 19

Bin 869: 6 of cap free
Amount of items: 2
Items: 
Size: 799190 Color: 5
Size: 200805 Color: 8

Bin 870: 6 of cap free
Amount of items: 3
Items: 
Size: 373774 Color: 1
Size: 320707 Color: 5
Size: 305514 Color: 8

Bin 871: 6 of cap free
Amount of items: 3
Items: 
Size: 600763 Color: 19
Size: 208035 Color: 7
Size: 191197 Color: 6

Bin 872: 6 of cap free
Amount of items: 3
Items: 
Size: 660016 Color: 4
Size: 170833 Color: 4
Size: 169146 Color: 3

Bin 873: 6 of cap free
Amount of items: 3
Items: 
Size: 430651 Color: 5
Size: 297906 Color: 2
Size: 271438 Color: 17

Bin 874: 6 of cap free
Amount of items: 3
Items: 
Size: 358384 Color: 0
Size: 344249 Color: 18
Size: 297362 Color: 12

Bin 875: 6 of cap free
Amount of items: 3
Items: 
Size: 367554 Color: 19
Size: 325891 Color: 5
Size: 306550 Color: 12

Bin 876: 6 of cap free
Amount of items: 3
Items: 
Size: 751335 Color: 6
Size: 124365 Color: 4
Size: 124295 Color: 13

Bin 877: 6 of cap free
Amount of items: 3
Items: 
Size: 557904 Color: 18
Size: 248052 Color: 14
Size: 194039 Color: 3

Bin 878: 6 of cap free
Amount of items: 2
Items: 
Size: 553433 Color: 9
Size: 446562 Color: 16

Bin 879: 6 of cap free
Amount of items: 3
Items: 
Size: 690843 Color: 10
Size: 154884 Color: 0
Size: 154268 Color: 2

Bin 880: 6 of cap free
Amount of items: 2
Items: 
Size: 721783 Color: 12
Size: 278212 Color: 0

Bin 881: 6 of cap free
Amount of items: 2
Items: 
Size: 501261 Color: 1
Size: 498734 Color: 2

Bin 882: 6 of cap free
Amount of items: 2
Items: 
Size: 624320 Color: 10
Size: 375675 Color: 2

Bin 883: 6 of cap free
Amount of items: 3
Items: 
Size: 638641 Color: 1
Size: 182979 Color: 1
Size: 178375 Color: 16

Bin 884: 6 of cap free
Amount of items: 3
Items: 
Size: 481567 Color: 0
Size: 263913 Color: 18
Size: 254515 Color: 9

Bin 885: 6 of cap free
Amount of items: 3
Items: 
Size: 483483 Color: 11
Size: 261752 Color: 4
Size: 254760 Color: 16

Bin 886: 6 of cap free
Amount of items: 3
Items: 
Size: 487487 Color: 4
Size: 264610 Color: 2
Size: 247898 Color: 4

Bin 887: 6 of cap free
Amount of items: 2
Items: 
Size: 687529 Color: 1
Size: 312466 Color: 17

Bin 888: 7 of cap free
Amount of items: 3
Items: 
Size: 748616 Color: 8
Size: 132646 Color: 11
Size: 118732 Color: 0

Bin 889: 7 of cap free
Amount of items: 3
Items: 
Size: 667583 Color: 2
Size: 166918 Color: 16
Size: 165493 Color: 18

Bin 890: 7 of cap free
Amount of items: 3
Items: 
Size: 723241 Color: 5
Size: 140614 Color: 10
Size: 136139 Color: 15

Bin 891: 7 of cap free
Amount of items: 3
Items: 
Size: 361593 Color: 8
Size: 354088 Color: 5
Size: 284313 Color: 15

Bin 892: 7 of cap free
Amount of items: 3
Items: 
Size: 388801 Color: 9
Size: 313320 Color: 2
Size: 297873 Color: 1

Bin 893: 7 of cap free
Amount of items: 2
Items: 
Size: 507600 Color: 17
Size: 492394 Color: 3

Bin 894: 7 of cap free
Amount of items: 2
Items: 
Size: 512877 Color: 18
Size: 487117 Color: 15

Bin 895: 7 of cap free
Amount of items: 2
Items: 
Size: 520999 Color: 4
Size: 478995 Color: 3

Bin 896: 7 of cap free
Amount of items: 2
Items: 
Size: 528024 Color: 14
Size: 471970 Color: 8

Bin 897: 7 of cap free
Amount of items: 2
Items: 
Size: 528077 Color: 0
Size: 471917 Color: 9

Bin 898: 7 of cap free
Amount of items: 2
Items: 
Size: 535987 Color: 10
Size: 464007 Color: 0

Bin 899: 7 of cap free
Amount of items: 2
Items: 
Size: 537517 Color: 10
Size: 462477 Color: 6

Bin 900: 7 of cap free
Amount of items: 2
Items: 
Size: 538921 Color: 0
Size: 461073 Color: 5

Bin 901: 7 of cap free
Amount of items: 2
Items: 
Size: 539770 Color: 0
Size: 460224 Color: 14

Bin 902: 7 of cap free
Amount of items: 2
Items: 
Size: 550357 Color: 3
Size: 449637 Color: 2

Bin 903: 7 of cap free
Amount of items: 2
Items: 
Size: 556048 Color: 1
Size: 443946 Color: 13

Bin 904: 7 of cap free
Amount of items: 2
Items: 
Size: 559659 Color: 6
Size: 440335 Color: 8

Bin 905: 7 of cap free
Amount of items: 2
Items: 
Size: 560336 Color: 14
Size: 439658 Color: 8

Bin 906: 7 of cap free
Amount of items: 2
Items: 
Size: 571948 Color: 6
Size: 428046 Color: 5

Bin 907: 7 of cap free
Amount of items: 2
Items: 
Size: 576179 Color: 17
Size: 423815 Color: 2

Bin 908: 7 of cap free
Amount of items: 2
Items: 
Size: 578855 Color: 7
Size: 421139 Color: 14

Bin 909: 7 of cap free
Amount of items: 2
Items: 
Size: 586679 Color: 5
Size: 413315 Color: 1

Bin 910: 7 of cap free
Amount of items: 2
Items: 
Size: 602757 Color: 7
Size: 397237 Color: 19

Bin 911: 7 of cap free
Amount of items: 2
Items: 
Size: 611502 Color: 9
Size: 388492 Color: 14

Bin 912: 7 of cap free
Amount of items: 2
Items: 
Size: 627363 Color: 6
Size: 372631 Color: 0

Bin 913: 7 of cap free
Amount of items: 3
Items: 
Size: 640321 Color: 6
Size: 183020 Color: 7
Size: 176653 Color: 11

Bin 914: 7 of cap free
Amount of items: 3
Items: 
Size: 644612 Color: 16
Size: 178390 Color: 0
Size: 176992 Color: 9

Bin 915: 7 of cap free
Amount of items: 3
Items: 
Size: 645638 Color: 19
Size: 177451 Color: 19
Size: 176905 Color: 18

Bin 916: 7 of cap free
Amount of items: 2
Items: 
Size: 668652 Color: 17
Size: 331342 Color: 13

Bin 917: 7 of cap free
Amount of items: 2
Items: 
Size: 670253 Color: 0
Size: 329741 Color: 2

Bin 918: 7 of cap free
Amount of items: 2
Items: 
Size: 675368 Color: 13
Size: 324626 Color: 18

Bin 919: 7 of cap free
Amount of items: 3
Items: 
Size: 689823 Color: 13
Size: 155325 Color: 12
Size: 154846 Color: 14

Bin 920: 7 of cap free
Amount of items: 3
Items: 
Size: 699219 Color: 1
Size: 151157 Color: 11
Size: 149618 Color: 5

Bin 921: 7 of cap free
Amount of items: 3
Items: 
Size: 700978 Color: 18
Size: 150468 Color: 8
Size: 148548 Color: 15

Bin 922: 7 of cap free
Amount of items: 2
Items: 
Size: 701637 Color: 15
Size: 298357 Color: 4

Bin 923: 7 of cap free
Amount of items: 3
Items: 
Size: 701643 Color: 4
Size: 149201 Color: 9
Size: 149150 Color: 15

Bin 924: 7 of cap free
Amount of items: 3
Items: 
Size: 702418 Color: 16
Size: 151481 Color: 13
Size: 146095 Color: 19

Bin 925: 7 of cap free
Amount of items: 2
Items: 
Size: 707422 Color: 9
Size: 292572 Color: 18

Bin 926: 7 of cap free
Amount of items: 3
Items: 
Size: 717285 Color: 14
Size: 146598 Color: 5
Size: 136111 Color: 6

Bin 927: 7 of cap free
Amount of items: 3
Items: 
Size: 723990 Color: 4
Size: 138393 Color: 12
Size: 137611 Color: 19

Bin 928: 7 of cap free
Amount of items: 2
Items: 
Size: 731392 Color: 19
Size: 268602 Color: 16

Bin 929: 7 of cap free
Amount of items: 2
Items: 
Size: 742661 Color: 17
Size: 257333 Color: 2

Bin 930: 7 of cap free
Amount of items: 2
Items: 
Size: 746258 Color: 0
Size: 253736 Color: 5

Bin 931: 7 of cap free
Amount of items: 2
Items: 
Size: 747966 Color: 3
Size: 252028 Color: 12

Bin 932: 7 of cap free
Amount of items: 2
Items: 
Size: 766707 Color: 18
Size: 233287 Color: 8

Bin 933: 7 of cap free
Amount of items: 2
Items: 
Size: 769426 Color: 5
Size: 230568 Color: 4

Bin 934: 7 of cap free
Amount of items: 3
Items: 
Size: 770462 Color: 0
Size: 114801 Color: 12
Size: 114731 Color: 18

Bin 935: 7 of cap free
Amount of items: 3
Items: 
Size: 777153 Color: 3
Size: 111606 Color: 12
Size: 111235 Color: 14

Bin 936: 7 of cap free
Amount of items: 2
Items: 
Size: 786828 Color: 11
Size: 213166 Color: 18

Bin 937: 7 of cap free
Amount of items: 3
Items: 
Size: 763982 Color: 10
Size: 118080 Color: 12
Size: 117932 Color: 1

Bin 938: 7 of cap free
Amount of items: 3
Items: 
Size: 667536 Color: 2
Size: 170275 Color: 2
Size: 162183 Color: 3

Bin 939: 7 of cap free
Amount of items: 3
Items: 
Size: 700388 Color: 7
Size: 150388 Color: 1
Size: 149218 Color: 2

Bin 940: 7 of cap free
Amount of items: 3
Items: 
Size: 382347 Color: 3
Size: 357101 Color: 3
Size: 260546 Color: 4

Bin 941: 7 of cap free
Amount of items: 3
Items: 
Size: 757069 Color: 17
Size: 123600 Color: 4
Size: 119325 Color: 16

Bin 942: 7 of cap free
Amount of items: 3
Items: 
Size: 412120 Color: 15
Size: 307792 Color: 7
Size: 280082 Color: 6

Bin 943: 7 of cap free
Amount of items: 3
Items: 
Size: 609960 Color: 0
Size: 199313 Color: 2
Size: 190721 Color: 19

Bin 944: 7 of cap free
Amount of items: 3
Items: 
Size: 634343 Color: 1
Size: 183209 Color: 10
Size: 182442 Color: 15

Bin 945: 7 of cap free
Amount of items: 3
Items: 
Size: 698683 Color: 9
Size: 152633 Color: 0
Size: 148678 Color: 11

Bin 946: 7 of cap free
Amount of items: 3
Items: 
Size: 483115 Color: 6
Size: 263306 Color: 14
Size: 253573 Color: 19

Bin 947: 7 of cap free
Amount of items: 3
Items: 
Size: 685616 Color: 0
Size: 157311 Color: 13
Size: 157067 Color: 6

Bin 948: 7 of cap free
Amount of items: 2
Items: 
Size: 555586 Color: 8
Size: 444408 Color: 17

Bin 949: 7 of cap free
Amount of items: 2
Items: 
Size: 649625 Color: 6
Size: 350369 Color: 4

Bin 950: 7 of cap free
Amount of items: 3
Items: 
Size: 779688 Color: 8
Size: 110724 Color: 1
Size: 109582 Color: 6

Bin 951: 7 of cap free
Amount of items: 2
Items: 
Size: 644710 Color: 5
Size: 355284 Color: 14

Bin 952: 7 of cap free
Amount of items: 2
Items: 
Size: 734458 Color: 13
Size: 265536 Color: 9

Bin 953: 7 of cap free
Amount of items: 3
Items: 
Size: 787963 Color: 5
Size: 106021 Color: 10
Size: 106010 Color: 14

Bin 954: 7 of cap free
Amount of items: 3
Items: 
Size: 428427 Color: 6
Size: 306579 Color: 11
Size: 264988 Color: 1

Bin 955: 7 of cap free
Amount of items: 2
Items: 
Size: 685439 Color: 11
Size: 314555 Color: 1

Bin 956: 7 of cap free
Amount of items: 3
Items: 
Size: 486521 Color: 0
Size: 271400 Color: 15
Size: 242073 Color: 17

Bin 957: 7 of cap free
Amount of items: 2
Items: 
Size: 518354 Color: 1
Size: 481640 Color: 13

Bin 958: 7 of cap free
Amount of items: 2
Items: 
Size: 694438 Color: 19
Size: 305556 Color: 3

Bin 959: 7 of cap free
Amount of items: 2
Items: 
Size: 513767 Color: 7
Size: 486227 Color: 15

Bin 960: 8 of cap free
Amount of items: 3
Items: 
Size: 423520 Color: 3
Size: 311845 Color: 19
Size: 264628 Color: 13

Bin 961: 8 of cap free
Amount of items: 3
Items: 
Size: 709158 Color: 13
Size: 145836 Color: 14
Size: 144999 Color: 8

Bin 962: 8 of cap free
Amount of items: 3
Items: 
Size: 733930 Color: 14
Size: 133163 Color: 4
Size: 132900 Color: 15

Bin 963: 8 of cap free
Amount of items: 3
Items: 
Size: 627178 Color: 2
Size: 187068 Color: 4
Size: 185747 Color: 15

Bin 964: 8 of cap free
Amount of items: 3
Items: 
Size: 369557 Color: 19
Size: 358972 Color: 5
Size: 271464 Color: 12

Bin 965: 8 of cap free
Amount of items: 3
Items: 
Size: 386437 Color: 4
Size: 333416 Color: 5
Size: 280140 Color: 15

Bin 966: 8 of cap free
Amount of items: 3
Items: 
Size: 385091 Color: 15
Size: 333595 Color: 17
Size: 281307 Color: 18

Bin 967: 8 of cap free
Amount of items: 2
Items: 
Size: 503983 Color: 8
Size: 496010 Color: 11

Bin 968: 8 of cap free
Amount of items: 2
Items: 
Size: 504696 Color: 1
Size: 495297 Color: 3

Bin 969: 8 of cap free
Amount of items: 2
Items: 
Size: 505442 Color: 13
Size: 494551 Color: 10

Bin 970: 8 of cap free
Amount of items: 2
Items: 
Size: 523941 Color: 10
Size: 476052 Color: 7

Bin 971: 8 of cap free
Amount of items: 2
Items: 
Size: 527151 Color: 7
Size: 472842 Color: 18

Bin 972: 8 of cap free
Amount of items: 2
Items: 
Size: 532386 Color: 6
Size: 467607 Color: 1

Bin 973: 8 of cap free
Amount of items: 2
Items: 
Size: 544725 Color: 4
Size: 455268 Color: 2

Bin 974: 8 of cap free
Amount of items: 2
Items: 
Size: 546249 Color: 11
Size: 453744 Color: 5

Bin 975: 8 of cap free
Amount of items: 2
Items: 
Size: 552048 Color: 0
Size: 447945 Color: 12

Bin 976: 8 of cap free
Amount of items: 2
Items: 
Size: 553714 Color: 11
Size: 446279 Color: 4

Bin 977: 8 of cap free
Amount of items: 2
Items: 
Size: 568231 Color: 9
Size: 431762 Color: 6

Bin 978: 8 of cap free
Amount of items: 2
Items: 
Size: 574376 Color: 6
Size: 425617 Color: 2

Bin 979: 8 of cap free
Amount of items: 2
Items: 
Size: 580142 Color: 1
Size: 419851 Color: 9

Bin 980: 8 of cap free
Amount of items: 2
Items: 
Size: 593815 Color: 7
Size: 406178 Color: 2

Bin 981: 8 of cap free
Amount of items: 2
Items: 
Size: 596715 Color: 19
Size: 403278 Color: 3

Bin 982: 8 of cap free
Amount of items: 2
Items: 
Size: 604101 Color: 17
Size: 395892 Color: 9

Bin 983: 8 of cap free
Amount of items: 3
Items: 
Size: 612960 Color: 0
Size: 196323 Color: 12
Size: 190710 Color: 16

Bin 984: 8 of cap free
Amount of items: 2
Items: 
Size: 618553 Color: 5
Size: 381440 Color: 3

Bin 985: 8 of cap free
Amount of items: 3
Items: 
Size: 621280 Color: 16
Size: 189939 Color: 18
Size: 188774 Color: 14

Bin 986: 8 of cap free
Amount of items: 2
Items: 
Size: 634153 Color: 8
Size: 365840 Color: 14

Bin 987: 8 of cap free
Amount of items: 3
Items: 
Size: 643935 Color: 7
Size: 178768 Color: 8
Size: 177290 Color: 5

Bin 988: 8 of cap free
Amount of items: 2
Items: 
Size: 649585 Color: 9
Size: 350408 Color: 16

Bin 989: 8 of cap free
Amount of items: 2
Items: 
Size: 652116 Color: 14
Size: 347877 Color: 4

Bin 990: 8 of cap free
Amount of items: 2
Items: 
Size: 656584 Color: 5
Size: 343409 Color: 6

Bin 991: 8 of cap free
Amount of items: 2
Items: 
Size: 657995 Color: 10
Size: 341998 Color: 6

Bin 992: 8 of cap free
Amount of items: 3
Items: 
Size: 661614 Color: 8
Size: 171909 Color: 2
Size: 166470 Color: 5

Bin 993: 8 of cap free
Amount of items: 2
Items: 
Size: 664522 Color: 16
Size: 335471 Color: 6

Bin 994: 8 of cap free
Amount of items: 3
Items: 
Size: 678160 Color: 15
Size: 160944 Color: 16
Size: 160889 Color: 16

Bin 995: 8 of cap free
Amount of items: 3
Items: 
Size: 680454 Color: 3
Size: 159800 Color: 15
Size: 159739 Color: 11

Bin 996: 8 of cap free
Amount of items: 3
Items: 
Size: 704084 Color: 2
Size: 148479 Color: 5
Size: 147430 Color: 2

Bin 997: 8 of cap free
Amount of items: 2
Items: 
Size: 713505 Color: 6
Size: 286488 Color: 7

Bin 998: 8 of cap free
Amount of items: 2
Items: 
Size: 721347 Color: 5
Size: 278646 Color: 2

Bin 999: 8 of cap free
Amount of items: 3
Items: 
Size: 721434 Color: 12
Size: 140020 Color: 14
Size: 138539 Color: 12

Bin 1000: 8 of cap free
Amount of items: 2
Items: 
Size: 721545 Color: 6
Size: 278448 Color: 4

Bin 1001: 8 of cap free
Amount of items: 2
Items: 
Size: 726078 Color: 12
Size: 273915 Color: 10

Bin 1002: 8 of cap free
Amount of items: 2
Items: 
Size: 738514 Color: 7
Size: 261479 Color: 16

Bin 1003: 8 of cap free
Amount of items: 3
Items: 
Size: 745926 Color: 13
Size: 127696 Color: 1
Size: 126371 Color: 3

Bin 1004: 8 of cap free
Amount of items: 2
Items: 
Size: 756321 Color: 3
Size: 243672 Color: 0

Bin 1005: 8 of cap free
Amount of items: 2
Items: 
Size: 772960 Color: 3
Size: 227033 Color: 18

Bin 1006: 8 of cap free
Amount of items: 2
Items: 
Size: 778452 Color: 7
Size: 221541 Color: 4

Bin 1007: 8 of cap free
Amount of items: 2
Items: 
Size: 780584 Color: 16
Size: 219409 Color: 4

Bin 1008: 8 of cap free
Amount of items: 2
Items: 
Size: 783889 Color: 10
Size: 216104 Color: 5

Bin 1009: 8 of cap free
Amount of items: 2
Items: 
Size: 790761 Color: 19
Size: 209232 Color: 1

Bin 1010: 8 of cap free
Amount of items: 2
Items: 
Size: 780727 Color: 6
Size: 219266 Color: 11

Bin 1011: 8 of cap free
Amount of items: 3
Items: 
Size: 631515 Color: 3
Size: 184270 Color: 5
Size: 184208 Color: 0

Bin 1012: 8 of cap free
Amount of items: 3
Items: 
Size: 557266 Color: 13
Size: 249121 Color: 19
Size: 193606 Color: 1

Bin 1013: 8 of cap free
Amount of items: 3
Items: 
Size: 624339 Color: 0
Size: 187920 Color: 6
Size: 187734 Color: 7

Bin 1014: 8 of cap free
Amount of items: 3
Items: 
Size: 629759 Color: 5
Size: 185183 Color: 2
Size: 185051 Color: 6

Bin 1015: 8 of cap free
Amount of items: 3
Items: 
Size: 691048 Color: 15
Size: 155099 Color: 11
Size: 153846 Color: 12

Bin 1016: 8 of cap free
Amount of items: 2
Items: 
Size: 543501 Color: 13
Size: 456492 Color: 3

Bin 1017: 8 of cap free
Amount of items: 3
Items: 
Size: 429315 Color: 5
Size: 306109 Color: 0
Size: 264569 Color: 1

Bin 1018: 8 of cap free
Amount of items: 2
Items: 
Size: 782132 Color: 18
Size: 217861 Color: 1

Bin 1019: 8 of cap free
Amount of items: 3
Items: 
Size: 412155 Color: 19
Size: 305917 Color: 17
Size: 281921 Color: 15

Bin 1020: 8 of cap free
Amount of items: 2
Items: 
Size: 777358 Color: 15
Size: 222635 Color: 19

Bin 1021: 8 of cap free
Amount of items: 2
Items: 
Size: 515993 Color: 14
Size: 484000 Color: 10

Bin 1022: 8 of cap free
Amount of items: 3
Items: 
Size: 358595 Color: 9
Size: 331040 Color: 12
Size: 310358 Color: 15

Bin 1023: 8 of cap free
Amount of items: 3
Items: 
Size: 628612 Color: 18
Size: 185809 Color: 14
Size: 185572 Color: 19

Bin 1024: 8 of cap free
Amount of items: 2
Items: 
Size: 770867 Color: 0
Size: 229126 Color: 3

Bin 1025: 8 of cap free
Amount of items: 2
Items: 
Size: 775214 Color: 3
Size: 224779 Color: 5

Bin 1026: 8 of cap free
Amount of items: 2
Items: 
Size: 533552 Color: 2
Size: 466441 Color: 4

Bin 1027: 8 of cap free
Amount of items: 2
Items: 
Size: 760401 Color: 8
Size: 239592 Color: 3

Bin 1028: 9 of cap free
Amount of items: 3
Items: 
Size: 689593 Color: 17
Size: 155464 Color: 2
Size: 154935 Color: 16

Bin 1029: 9 of cap free
Amount of items: 3
Items: 
Size: 673312 Color: 7
Size: 163937 Color: 12
Size: 162743 Color: 9

Bin 1030: 9 of cap free
Amount of items: 3
Items: 
Size: 375009 Color: 3
Size: 359071 Color: 2
Size: 265912 Color: 10

Bin 1031: 9 of cap free
Amount of items: 3
Items: 
Size: 419725 Color: 19
Size: 301227 Color: 1
Size: 279040 Color: 6

Bin 1032: 9 of cap free
Amount of items: 3
Items: 
Size: 457198 Color: 6
Size: 279369 Color: 5
Size: 263425 Color: 7

Bin 1033: 9 of cap free
Amount of items: 2
Items: 
Size: 503589 Color: 8
Size: 496403 Color: 17

Bin 1034: 9 of cap free
Amount of items: 2
Items: 
Size: 520949 Color: 10
Size: 479043 Color: 3

Bin 1035: 9 of cap free
Amount of items: 2
Items: 
Size: 526549 Color: 14
Size: 473443 Color: 1

Bin 1036: 9 of cap free
Amount of items: 2
Items: 
Size: 527247 Color: 10
Size: 472745 Color: 18

Bin 1037: 9 of cap free
Amount of items: 2
Items: 
Size: 543625 Color: 1
Size: 456367 Color: 17

Bin 1038: 9 of cap free
Amount of items: 3
Items: 
Size: 549473 Color: 17
Size: 254484 Color: 1
Size: 196035 Color: 13

Bin 1039: 9 of cap free
Amount of items: 2
Items: 
Size: 591172 Color: 7
Size: 408820 Color: 0

Bin 1040: 9 of cap free
Amount of items: 2
Items: 
Size: 591904 Color: 2
Size: 408088 Color: 17

Bin 1041: 9 of cap free
Amount of items: 2
Items: 
Size: 593108 Color: 11
Size: 406884 Color: 1

Bin 1042: 9 of cap free
Amount of items: 2
Items: 
Size: 594336 Color: 2
Size: 405656 Color: 8

Bin 1043: 9 of cap free
Amount of items: 2
Items: 
Size: 597774 Color: 16
Size: 402218 Color: 8

Bin 1044: 9 of cap free
Amount of items: 2
Items: 
Size: 605017 Color: 14
Size: 394975 Color: 19

Bin 1045: 9 of cap free
Amount of items: 2
Items: 
Size: 608612 Color: 13
Size: 391380 Color: 19

Bin 1046: 9 of cap free
Amount of items: 2
Items: 
Size: 609810 Color: 2
Size: 390182 Color: 15

Bin 1047: 9 of cap free
Amount of items: 2
Items: 
Size: 611535 Color: 7
Size: 388457 Color: 16

Bin 1048: 9 of cap free
Amount of items: 3
Items: 
Size: 618086 Color: 12
Size: 191718 Color: 18
Size: 190188 Color: 6

Bin 1049: 9 of cap free
Amount of items: 3
Items: 
Size: 624864 Color: 17
Size: 187718 Color: 3
Size: 187410 Color: 10

Bin 1050: 9 of cap free
Amount of items: 2
Items: 
Size: 635104 Color: 8
Size: 364888 Color: 10

Bin 1051: 9 of cap free
Amount of items: 2
Items: 
Size: 646842 Color: 11
Size: 353150 Color: 10

Bin 1052: 9 of cap free
Amount of items: 2
Items: 
Size: 653977 Color: 12
Size: 346015 Color: 18

Bin 1053: 9 of cap free
Amount of items: 2
Items: 
Size: 656058 Color: 10
Size: 343934 Color: 16

Bin 1054: 9 of cap free
Amount of items: 2
Items: 
Size: 656279 Color: 18
Size: 343713 Color: 2

Bin 1055: 9 of cap free
Amount of items: 2
Items: 
Size: 667130 Color: 9
Size: 332862 Color: 6

Bin 1056: 9 of cap free
Amount of items: 2
Items: 
Size: 668099 Color: 19
Size: 331893 Color: 8

Bin 1057: 9 of cap free
Amount of items: 3
Items: 
Size: 669363 Color: 3
Size: 165543 Color: 11
Size: 165086 Color: 7

Bin 1058: 9 of cap free
Amount of items: 2
Items: 
Size: 678346 Color: 10
Size: 321646 Color: 5

Bin 1059: 9 of cap free
Amount of items: 3
Items: 
Size: 688198 Color: 19
Size: 156028 Color: 11
Size: 155766 Color: 9

Bin 1060: 9 of cap free
Amount of items: 2
Items: 
Size: 696021 Color: 0
Size: 303971 Color: 18

Bin 1061: 9 of cap free
Amount of items: 2
Items: 
Size: 699958 Color: 3
Size: 300034 Color: 13

Bin 1062: 9 of cap free
Amount of items: 3
Items: 
Size: 703075 Color: 8
Size: 150678 Color: 16
Size: 146239 Color: 18

Bin 1063: 9 of cap free
Amount of items: 3
Items: 
Size: 705343 Color: 13
Size: 147636 Color: 6
Size: 147013 Color: 13

Bin 1064: 9 of cap free
Amount of items: 2
Items: 
Size: 709348 Color: 11
Size: 290644 Color: 9

Bin 1065: 9 of cap free
Amount of items: 3
Items: 
Size: 735192 Color: 15
Size: 132729 Color: 4
Size: 132071 Color: 11

Bin 1066: 9 of cap free
Amount of items: 2
Items: 
Size: 739191 Color: 4
Size: 260801 Color: 9

Bin 1067: 9 of cap free
Amount of items: 2
Items: 
Size: 748822 Color: 0
Size: 251170 Color: 10

Bin 1068: 9 of cap free
Amount of items: 3
Items: 
Size: 762122 Color: 5
Size: 119048 Color: 14
Size: 118822 Color: 18

Bin 1069: 9 of cap free
Amount of items: 2
Items: 
Size: 771457 Color: 15
Size: 228535 Color: 17

Bin 1070: 9 of cap free
Amount of items: 2
Items: 
Size: 777924 Color: 18
Size: 222068 Color: 13

Bin 1071: 9 of cap free
Amount of items: 2
Items: 
Size: 783523 Color: 15
Size: 216469 Color: 8

Bin 1072: 9 of cap free
Amount of items: 3
Items: 
Size: 783719 Color: 1
Size: 108201 Color: 5
Size: 108072 Color: 10

Bin 1073: 9 of cap free
Amount of items: 2
Items: 
Size: 784319 Color: 9
Size: 215673 Color: 4

Bin 1074: 9 of cap free
Amount of items: 2
Items: 
Size: 647967 Color: 4
Size: 352025 Color: 13

Bin 1075: 9 of cap free
Amount of items: 3
Items: 
Size: 646422 Color: 18
Size: 177119 Color: 11
Size: 176451 Color: 15

Bin 1076: 9 of cap free
Amount of items: 2
Items: 
Size: 622488 Color: 16
Size: 377504 Color: 19

Bin 1077: 9 of cap free
Amount of items: 2
Items: 
Size: 748451 Color: 7
Size: 251541 Color: 5

Bin 1078: 9 of cap free
Amount of items: 3
Items: 
Size: 354018 Color: 5
Size: 336147 Color: 10
Size: 309827 Color: 10

Bin 1079: 9 of cap free
Amount of items: 3
Items: 
Size: 639714 Color: 5
Size: 180158 Color: 14
Size: 180120 Color: 1

Bin 1080: 9 of cap free
Amount of items: 3
Items: 
Size: 637141 Color: 0
Size: 183620 Color: 10
Size: 179231 Color: 11

Bin 1081: 9 of cap free
Amount of items: 2
Items: 
Size: 796762 Color: 12
Size: 203230 Color: 18

Bin 1082: 9 of cap free
Amount of items: 3
Items: 
Size: 756497 Color: 15
Size: 121986 Color: 8
Size: 121509 Color: 7

Bin 1083: 9 of cap free
Amount of items: 3
Items: 
Size: 411358 Color: 5
Size: 306618 Color: 14
Size: 282016 Color: 16

Bin 1084: 9 of cap free
Amount of items: 3
Items: 
Size: 633351 Color: 1
Size: 183330 Color: 14
Size: 183311 Color: 15

Bin 1085: 9 of cap free
Amount of items: 3
Items: 
Size: 641037 Color: 9
Size: 180202 Color: 6
Size: 178753 Color: 10

Bin 1086: 9 of cap free
Amount of items: 3
Items: 
Size: 357475 Color: 5
Size: 331598 Color: 8
Size: 310919 Color: 14

Bin 1087: 9 of cap free
Amount of items: 2
Items: 
Size: 578042 Color: 13
Size: 421950 Color: 12

Bin 1088: 9 of cap free
Amount of items: 3
Items: 
Size: 616115 Color: 8
Size: 194985 Color: 9
Size: 188892 Color: 6

Bin 1089: 9 of cap free
Amount of items: 2
Items: 
Size: 721497 Color: 1
Size: 278495 Color: 13

Bin 1090: 9 of cap free
Amount of items: 2
Items: 
Size: 740487 Color: 1
Size: 259505 Color: 16

Bin 1091: 9 of cap free
Amount of items: 3
Items: 
Size: 354530 Color: 7
Size: 333216 Color: 6
Size: 312246 Color: 9

Bin 1092: 9 of cap free
Amount of items: 2
Items: 
Size: 636380 Color: 1
Size: 363612 Color: 18

Bin 1093: 9 of cap free
Amount of items: 2
Items: 
Size: 577811 Color: 19
Size: 422181 Color: 10

Bin 1094: 10 of cap free
Amount of items: 3
Items: 
Size: 369582 Color: 1
Size: 323807 Color: 14
Size: 306602 Color: 2

Bin 1095: 10 of cap free
Amount of items: 3
Items: 
Size: 369320 Color: 9
Size: 336076 Color: 8
Size: 294595 Color: 10

Bin 1096: 10 of cap free
Amount of items: 2
Items: 
Size: 506427 Color: 1
Size: 493564 Color: 4

Bin 1097: 10 of cap free
Amount of items: 2
Items: 
Size: 526267 Color: 9
Size: 473724 Color: 12

Bin 1098: 10 of cap free
Amount of items: 2
Items: 
Size: 530428 Color: 3
Size: 469563 Color: 8

Bin 1099: 10 of cap free
Amount of items: 2
Items: 
Size: 546747 Color: 7
Size: 453244 Color: 16

Bin 1100: 10 of cap free
Amount of items: 2
Items: 
Size: 550298 Color: 19
Size: 449693 Color: 13

Bin 1101: 10 of cap free
Amount of items: 2
Items: 
Size: 552190 Color: 18
Size: 447801 Color: 19

Bin 1102: 10 of cap free
Amount of items: 2
Items: 
Size: 570871 Color: 12
Size: 429120 Color: 17

Bin 1103: 10 of cap free
Amount of items: 2
Items: 
Size: 584808 Color: 5
Size: 415183 Color: 11

Bin 1104: 10 of cap free
Amount of items: 2
Items: 
Size: 584901 Color: 2
Size: 415090 Color: 7

Bin 1105: 10 of cap free
Amount of items: 2
Items: 
Size: 601155 Color: 5
Size: 398836 Color: 4

Bin 1106: 10 of cap free
Amount of items: 2
Items: 
Size: 602216 Color: 17
Size: 397775 Color: 11

Bin 1107: 10 of cap free
Amount of items: 2
Items: 
Size: 614733 Color: 2
Size: 385258 Color: 1

Bin 1108: 10 of cap free
Amount of items: 2
Items: 
Size: 615509 Color: 17
Size: 384482 Color: 13

Bin 1109: 10 of cap free
Amount of items: 2
Items: 
Size: 615909 Color: 5
Size: 384082 Color: 12

Bin 1110: 10 of cap free
Amount of items: 3
Items: 
Size: 623713 Color: 19
Size: 188360 Color: 5
Size: 187918 Color: 19

Bin 1111: 10 of cap free
Amount of items: 2
Items: 
Size: 632961 Color: 7
Size: 367030 Color: 19

Bin 1112: 10 of cap free
Amount of items: 3
Items: 
Size: 638988 Color: 13
Size: 181152 Color: 13
Size: 179851 Color: 6

Bin 1113: 10 of cap free
Amount of items: 2
Items: 
Size: 640137 Color: 11
Size: 359854 Color: 4

Bin 1114: 10 of cap free
Amount of items: 3
Items: 
Size: 640377 Color: 0
Size: 180080 Color: 12
Size: 179534 Color: 16

Bin 1115: 10 of cap free
Amount of items: 2
Items: 
Size: 641500 Color: 14
Size: 358491 Color: 7

Bin 1116: 10 of cap free
Amount of items: 2
Items: 
Size: 647243 Color: 12
Size: 352748 Color: 16

Bin 1117: 10 of cap free
Amount of items: 2
Items: 
Size: 655667 Color: 19
Size: 344324 Color: 9

Bin 1118: 10 of cap free
Amount of items: 2
Items: 
Size: 657093 Color: 13
Size: 342898 Color: 12

Bin 1119: 10 of cap free
Amount of items: 2
Items: 
Size: 658298 Color: 16
Size: 341693 Color: 15

Bin 1120: 10 of cap free
Amount of items: 3
Items: 
Size: 664708 Color: 0
Size: 167764 Color: 16
Size: 167519 Color: 4

Bin 1121: 10 of cap free
Amount of items: 2
Items: 
Size: 665638 Color: 19
Size: 334353 Color: 4

Bin 1122: 10 of cap free
Amount of items: 2
Items: 
Size: 679971 Color: 3
Size: 320020 Color: 8

Bin 1123: 10 of cap free
Amount of items: 2
Items: 
Size: 694336 Color: 17
Size: 305655 Color: 8

Bin 1124: 10 of cap free
Amount of items: 2
Items: 
Size: 695446 Color: 11
Size: 304545 Color: 6

Bin 1125: 10 of cap free
Amount of items: 2
Items: 
Size: 703251 Color: 19
Size: 296740 Color: 5

Bin 1126: 10 of cap free
Amount of items: 2
Items: 
Size: 706113 Color: 3
Size: 293878 Color: 5

Bin 1127: 10 of cap free
Amount of items: 2
Items: 
Size: 710555 Color: 2
Size: 289436 Color: 14

Bin 1128: 10 of cap free
Amount of items: 2
Items: 
Size: 711749 Color: 9
Size: 288242 Color: 17

Bin 1129: 10 of cap free
Amount of items: 3
Items: 
Size: 717215 Color: 3
Size: 143093 Color: 11
Size: 139683 Color: 16

Bin 1130: 10 of cap free
Amount of items: 2
Items: 
Size: 738062 Color: 13
Size: 261929 Color: 16

Bin 1131: 10 of cap free
Amount of items: 2
Items: 
Size: 742797 Color: 16
Size: 257194 Color: 19

Bin 1132: 10 of cap free
Amount of items: 2
Items: 
Size: 745746 Color: 10
Size: 254245 Color: 19

Bin 1133: 10 of cap free
Amount of items: 2
Items: 
Size: 753422 Color: 5
Size: 246569 Color: 1

Bin 1134: 10 of cap free
Amount of items: 2
Items: 
Size: 756238 Color: 10
Size: 243753 Color: 12

Bin 1135: 10 of cap free
Amount of items: 2
Items: 
Size: 756737 Color: 17
Size: 243254 Color: 9

Bin 1136: 10 of cap free
Amount of items: 3
Items: 
Size: 778172 Color: 7
Size: 110915 Color: 5
Size: 110904 Color: 11

Bin 1137: 10 of cap free
Amount of items: 2
Items: 
Size: 786995 Color: 4
Size: 212996 Color: 12

Bin 1138: 10 of cap free
Amount of items: 2
Items: 
Size: 789363 Color: 11
Size: 210628 Color: 1

Bin 1139: 10 of cap free
Amount of items: 2
Items: 
Size: 798661 Color: 13
Size: 201330 Color: 14

Bin 1140: 10 of cap free
Amount of items: 3
Items: 
Size: 354109 Color: 8
Size: 335998 Color: 2
Size: 309884 Color: 2

Bin 1141: 10 of cap free
Amount of items: 3
Items: 
Size: 766432 Color: 2
Size: 117424 Color: 14
Size: 116135 Color: 3

Bin 1142: 10 of cap free
Amount of items: 3
Items: 
Size: 600163 Color: 2
Size: 208092 Color: 12
Size: 191736 Color: 7

Bin 1143: 10 of cap free
Amount of items: 3
Items: 
Size: 439428 Color: 8
Size: 280526 Color: 10
Size: 280037 Color: 3

Bin 1144: 10 of cap free
Amount of items: 3
Items: 
Size: 684737 Color: 11
Size: 157823 Color: 11
Size: 157431 Color: 13

Bin 1145: 10 of cap free
Amount of items: 3
Items: 
Size: 649545 Color: 5
Size: 175508 Color: 17
Size: 174938 Color: 11

Bin 1146: 10 of cap free
Amount of items: 2
Items: 
Size: 630370 Color: 17
Size: 369621 Color: 18

Bin 1147: 10 of cap free
Amount of items: 3
Items: 
Size: 734646 Color: 15
Size: 133202 Color: 13
Size: 132143 Color: 19

Bin 1148: 10 of cap free
Amount of items: 3
Items: 
Size: 769711 Color: 8
Size: 115203 Color: 13
Size: 115077 Color: 3

Bin 1149: 10 of cap free
Amount of items: 3
Items: 
Size: 737335 Color: 10
Size: 131425 Color: 16
Size: 131231 Color: 14

Bin 1150: 10 of cap free
Amount of items: 2
Items: 
Size: 760001 Color: 7
Size: 239990 Color: 18

Bin 1151: 10 of cap free
Amount of items: 3
Items: 
Size: 766528 Color: 6
Size: 118195 Color: 2
Size: 115268 Color: 15

Bin 1152: 10 of cap free
Amount of items: 3
Items: 
Size: 760165 Color: 13
Size: 120053 Color: 0
Size: 119773 Color: 13

Bin 1153: 10 of cap free
Amount of items: 2
Items: 
Size: 759526 Color: 2
Size: 240465 Color: 0

Bin 1154: 10 of cap free
Amount of items: 3
Items: 
Size: 361685 Color: 14
Size: 331658 Color: 19
Size: 306648 Color: 5

Bin 1155: 10 of cap free
Amount of items: 3
Items: 
Size: 731020 Color: 9
Size: 136137 Color: 3
Size: 132834 Color: 13

Bin 1156: 10 of cap free
Amount of items: 3
Items: 
Size: 675292 Color: 10
Size: 162433 Color: 8
Size: 162266 Color: 18

Bin 1157: 10 of cap free
Amount of items: 3
Items: 
Size: 667624 Color: 8
Size: 167560 Color: 3
Size: 164807 Color: 18

Bin 1158: 10 of cap free
Amount of items: 3
Items: 
Size: 681146 Color: 15
Size: 159628 Color: 3
Size: 159217 Color: 3

Bin 1159: 10 of cap free
Amount of items: 2
Items: 
Size: 649863 Color: 15
Size: 350128 Color: 1

Bin 1160: 10 of cap free
Amount of items: 2
Items: 
Size: 625024 Color: 16
Size: 374967 Color: 14

Bin 1161: 10 of cap free
Amount of items: 3
Items: 
Size: 521580 Color: 11
Size: 239300 Color: 13
Size: 239111 Color: 13

Bin 1162: 10 of cap free
Amount of items: 2
Items: 
Size: 639457 Color: 13
Size: 360534 Color: 16

Bin 1163: 10 of cap free
Amount of items: 2
Items: 
Size: 521262 Color: 16
Size: 478729 Color: 7

Bin 1164: 10 of cap free
Amount of items: 3
Items: 
Size: 352217 Color: 19
Size: 327872 Color: 14
Size: 319902 Color: 7

Bin 1165: 10 of cap free
Amount of items: 2
Items: 
Size: 766177 Color: 6
Size: 233814 Color: 11

Bin 1166: 10 of cap free
Amount of items: 2
Items: 
Size: 788337 Color: 7
Size: 211654 Color: 3

Bin 1167: 10 of cap free
Amount of items: 2
Items: 
Size: 536931 Color: 1
Size: 463060 Color: 0

Bin 1168: 11 of cap free
Amount of items: 2
Items: 
Size: 769324 Color: 3
Size: 230666 Color: 16

Bin 1169: 11 of cap free
Amount of items: 3
Items: 
Size: 741736 Color: 0
Size: 129200 Color: 10
Size: 129054 Color: 11

Bin 1170: 11 of cap free
Amount of items: 2
Items: 
Size: 518437 Color: 19
Size: 481553 Color: 13

Bin 1171: 11 of cap free
Amount of items: 2
Items: 
Size: 543795 Color: 17
Size: 456195 Color: 2

Bin 1172: 11 of cap free
Amount of items: 2
Items: 
Size: 546900 Color: 8
Size: 453090 Color: 5

Bin 1173: 11 of cap free
Amount of items: 2
Items: 
Size: 557901 Color: 11
Size: 442089 Color: 18

Bin 1174: 11 of cap free
Amount of items: 2
Items: 
Size: 573020 Color: 10
Size: 426970 Color: 17

Bin 1175: 11 of cap free
Amount of items: 2
Items: 
Size: 587429 Color: 13
Size: 412561 Color: 19

Bin 1176: 11 of cap free
Amount of items: 3
Items: 
Size: 603247 Color: 15
Size: 198433 Color: 14
Size: 198310 Color: 4

Bin 1177: 11 of cap free
Amount of items: 2
Items: 
Size: 624551 Color: 5
Size: 375439 Color: 4

Bin 1178: 11 of cap free
Amount of items: 3
Items: 
Size: 638371 Color: 9
Size: 184423 Color: 4
Size: 177196 Color: 2

Bin 1179: 11 of cap free
Amount of items: 3
Items: 
Size: 640874 Color: 10
Size: 180558 Color: 4
Size: 178558 Color: 7

Bin 1180: 11 of cap free
Amount of items: 2
Items: 
Size: 641759 Color: 17
Size: 358231 Color: 10

Bin 1181: 11 of cap free
Amount of items: 2
Items: 
Size: 643314 Color: 8
Size: 356676 Color: 15

Bin 1182: 11 of cap free
Amount of items: 2
Items: 
Size: 651566 Color: 15
Size: 348424 Color: 7

Bin 1183: 11 of cap free
Amount of items: 2
Items: 
Size: 658251 Color: 13
Size: 341739 Color: 11

Bin 1184: 11 of cap free
Amount of items: 2
Items: 
Size: 664521 Color: 0
Size: 335469 Color: 17

Bin 1185: 11 of cap free
Amount of items: 2
Items: 
Size: 696975 Color: 4
Size: 303015 Color: 12

Bin 1186: 11 of cap free
Amount of items: 3
Items: 
Size: 702589 Color: 14
Size: 150593 Color: 15
Size: 146808 Color: 16

Bin 1187: 11 of cap free
Amount of items: 3
Items: 
Size: 718425 Color: 15
Size: 142803 Color: 11
Size: 138762 Color: 17

Bin 1188: 11 of cap free
Amount of items: 3
Items: 
Size: 718438 Color: 19
Size: 145948 Color: 17
Size: 135604 Color: 1

Bin 1189: 11 of cap free
Amount of items: 3
Items: 
Size: 722319 Color: 19
Size: 140838 Color: 18
Size: 136833 Color: 0

Bin 1190: 11 of cap free
Amount of items: 2
Items: 
Size: 729070 Color: 3
Size: 270920 Color: 18

Bin 1191: 11 of cap free
Amount of items: 3
Items: 
Size: 742513 Color: 1
Size: 128888 Color: 15
Size: 128589 Color: 19

Bin 1192: 11 of cap free
Amount of items: 2
Items: 
Size: 747090 Color: 19
Size: 252900 Color: 14

Bin 1193: 11 of cap free
Amount of items: 3
Items: 
Size: 755326 Color: 10
Size: 122374 Color: 12
Size: 122290 Color: 13

Bin 1194: 11 of cap free
Amount of items: 2
Items: 
Size: 765860 Color: 16
Size: 234130 Color: 8

Bin 1195: 11 of cap free
Amount of items: 2
Items: 
Size: 766333 Color: 12
Size: 233657 Color: 14

Bin 1196: 11 of cap free
Amount of items: 2
Items: 
Size: 769306 Color: 14
Size: 230684 Color: 4

Bin 1197: 11 of cap free
Amount of items: 2
Items: 
Size: 777191 Color: 5
Size: 222799 Color: 7

Bin 1198: 11 of cap free
Amount of items: 2
Items: 
Size: 778407 Color: 9
Size: 221583 Color: 11

Bin 1199: 11 of cap free
Amount of items: 2
Items: 
Size: 793482 Color: 16
Size: 206508 Color: 10

Bin 1200: 11 of cap free
Amount of items: 3
Items: 
Size: 560237 Color: 16
Size: 245673 Color: 13
Size: 194080 Color: 11

Bin 1201: 11 of cap free
Amount of items: 3
Items: 
Size: 368258 Color: 18
Size: 333661 Color: 0
Size: 298071 Color: 11

Bin 1202: 11 of cap free
Amount of items: 2
Items: 
Size: 569282 Color: 1
Size: 430708 Color: 12

Bin 1203: 11 of cap free
Amount of items: 3
Items: 
Size: 614467 Color: 9
Size: 196118 Color: 0
Size: 189405 Color: 11

Bin 1204: 11 of cap free
Amount of items: 2
Items: 
Size: 776868 Color: 1
Size: 223122 Color: 16

Bin 1205: 11 of cap free
Amount of items: 3
Items: 
Size: 357017 Color: 18
Size: 342784 Color: 1
Size: 300189 Color: 9

Bin 1206: 11 of cap free
Amount of items: 3
Items: 
Size: 758294 Color: 6
Size: 124635 Color: 3
Size: 117061 Color: 0

Bin 1207: 11 of cap free
Amount of items: 3
Items: 
Size: 684592 Color: 8
Size: 158134 Color: 10
Size: 157264 Color: 9

Bin 1208: 11 of cap free
Amount of items: 3
Items: 
Size: 626981 Color: 18
Size: 187576 Color: 8
Size: 185433 Color: 11

Bin 1209: 11 of cap free
Amount of items: 3
Items: 
Size: 692996 Color: 13
Size: 153571 Color: 1
Size: 153423 Color: 17

Bin 1210: 11 of cap free
Amount of items: 3
Items: 
Size: 357179 Color: 13
Size: 322142 Color: 10
Size: 320669 Color: 11

Bin 1211: 11 of cap free
Amount of items: 3
Items: 
Size: 528623 Color: 13
Size: 242288 Color: 2
Size: 229079 Color: 3

Bin 1212: 11 of cap free
Amount of items: 3
Items: 
Size: 419542 Color: 16
Size: 298414 Color: 5
Size: 282034 Color: 7

Bin 1213: 11 of cap free
Amount of items: 2
Items: 
Size: 555904 Color: 14
Size: 444086 Color: 18

Bin 1214: 11 of cap free
Amount of items: 3
Items: 
Size: 515936 Color: 6
Size: 247691 Color: 8
Size: 236363 Color: 1

Bin 1215: 11 of cap free
Amount of items: 3
Items: 
Size: 439274 Color: 0
Size: 281288 Color: 9
Size: 279428 Color: 11

Bin 1216: 11 of cap free
Amount of items: 3
Items: 
Size: 411934 Color: 8
Size: 321269 Color: 17
Size: 266787 Color: 13

Bin 1217: 11 of cap free
Amount of items: 2
Items: 
Size: 518880 Color: 17
Size: 481110 Color: 15

Bin 1218: 11 of cap free
Amount of items: 3
Items: 
Size: 558798 Color: 15
Size: 242960 Color: 7
Size: 198232 Color: 3

Bin 1219: 11 of cap free
Amount of items: 2
Items: 
Size: 560591 Color: 14
Size: 439399 Color: 12

Bin 1220: 11 of cap free
Amount of items: 2
Items: 
Size: 549807 Color: 6
Size: 450183 Color: 1

Bin 1221: 11 of cap free
Amount of items: 3
Items: 
Size: 513997 Color: 13
Size: 256971 Color: 17
Size: 229022 Color: 15

Bin 1222: 11 of cap free
Amount of items: 2
Items: 
Size: 640390 Color: 5
Size: 359600 Color: 2

Bin 1223: 11 of cap free
Amount of items: 3
Items: 
Size: 541024 Color: 4
Size: 263254 Color: 4
Size: 195712 Color: 6

Bin 1224: 11 of cap free
Amount of items: 3
Items: 
Size: 596417 Color: 9
Size: 203916 Color: 14
Size: 199657 Color: 5

Bin 1225: 11 of cap free
Amount of items: 2
Items: 
Size: 650491 Color: 17
Size: 349499 Color: 1

Bin 1226: 11 of cap free
Amount of items: 2
Items: 
Size: 522507 Color: 1
Size: 477483 Color: 18

Bin 1227: 12 of cap free
Amount of items: 3
Items: 
Size: 790787 Color: 15
Size: 104605 Color: 12
Size: 104597 Color: 15

Bin 1228: 12 of cap free
Amount of items: 3
Items: 
Size: 644675 Color: 12
Size: 178587 Color: 3
Size: 176727 Color: 9

Bin 1229: 12 of cap free
Amount of items: 3
Items: 
Size: 367138 Color: 3
Size: 320622 Color: 13
Size: 312229 Color: 17

Bin 1230: 12 of cap free
Amount of items: 2
Items: 
Size: 512083 Color: 7
Size: 487906 Color: 5

Bin 1231: 12 of cap free
Amount of items: 2
Items: 
Size: 512546 Color: 0
Size: 487443 Color: 18

Bin 1232: 12 of cap free
Amount of items: 2
Items: 
Size: 515835 Color: 13
Size: 484154 Color: 2

Bin 1233: 12 of cap free
Amount of items: 2
Items: 
Size: 519318 Color: 7
Size: 480671 Color: 9

Bin 1234: 12 of cap free
Amount of items: 2
Items: 
Size: 521715 Color: 9
Size: 478274 Color: 1

Bin 1235: 12 of cap free
Amount of items: 2
Items: 
Size: 531030 Color: 11
Size: 468959 Color: 10

Bin 1236: 12 of cap free
Amount of items: 2
Items: 
Size: 531150 Color: 5
Size: 468839 Color: 15

Bin 1237: 12 of cap free
Amount of items: 2
Items: 
Size: 538942 Color: 17
Size: 461047 Color: 18

Bin 1238: 12 of cap free
Amount of items: 2
Items: 
Size: 541954 Color: 9
Size: 458035 Color: 16

Bin 1239: 12 of cap free
Amount of items: 2
Items: 
Size: 547187 Color: 7
Size: 452802 Color: 10

Bin 1240: 12 of cap free
Amount of items: 2
Items: 
Size: 556969 Color: 11
Size: 443020 Color: 18

Bin 1241: 12 of cap free
Amount of items: 2
Items: 
Size: 558575 Color: 5
Size: 441414 Color: 17

Bin 1242: 12 of cap free
Amount of items: 2
Items: 
Size: 562763 Color: 3
Size: 437226 Color: 10

Bin 1243: 12 of cap free
Amount of items: 2
Items: 
Size: 566994 Color: 1
Size: 432995 Color: 10

Bin 1244: 12 of cap free
Amount of items: 2
Items: 
Size: 573284 Color: 17
Size: 426705 Color: 11

Bin 1245: 12 of cap free
Amount of items: 2
Items: 
Size: 611248 Color: 10
Size: 388741 Color: 12

Bin 1246: 12 of cap free
Amount of items: 2
Items: 
Size: 615182 Color: 10
Size: 384807 Color: 5

Bin 1247: 12 of cap free
Amount of items: 2
Items: 
Size: 617707 Color: 3
Size: 382282 Color: 0

Bin 1248: 12 of cap free
Amount of items: 2
Items: 
Size: 634038 Color: 6
Size: 365951 Color: 0

Bin 1249: 12 of cap free
Amount of items: 2
Items: 
Size: 637469 Color: 5
Size: 362520 Color: 11

Bin 1250: 12 of cap free
Amount of items: 3
Items: 
Size: 639061 Color: 2
Size: 182677 Color: 8
Size: 178251 Color: 17

Bin 1251: 12 of cap free
Amount of items: 2
Items: 
Size: 647427 Color: 2
Size: 352562 Color: 7

Bin 1252: 12 of cap free
Amount of items: 2
Items: 
Size: 648501 Color: 12
Size: 351488 Color: 10

Bin 1253: 12 of cap free
Amount of items: 3
Items: 
Size: 649681 Color: 19
Size: 175574 Color: 2
Size: 174734 Color: 8

Bin 1254: 12 of cap free
Amount of items: 3
Items: 
Size: 666239 Color: 12
Size: 170359 Color: 7
Size: 163391 Color: 10

Bin 1255: 12 of cap free
Amount of items: 2
Items: 
Size: 698338 Color: 18
Size: 301651 Color: 4

Bin 1256: 12 of cap free
Amount of items: 2
Items: 
Size: 730630 Color: 11
Size: 269359 Color: 4

Bin 1257: 12 of cap free
Amount of items: 3
Items: 
Size: 739048 Color: 5
Size: 130545 Color: 2
Size: 130396 Color: 5

Bin 1258: 12 of cap free
Amount of items: 2
Items: 
Size: 748143 Color: 9
Size: 251846 Color: 11

Bin 1259: 12 of cap free
Amount of items: 3
Items: 
Size: 756848 Color: 18
Size: 121786 Color: 14
Size: 121355 Color: 8

Bin 1260: 12 of cap free
Amount of items: 3
Items: 
Size: 763216 Color: 19
Size: 118508 Color: 10
Size: 118265 Color: 19

Bin 1261: 12 of cap free
Amount of items: 2
Items: 
Size: 765682 Color: 4
Size: 234307 Color: 1

Bin 1262: 12 of cap free
Amount of items: 2
Items: 
Size: 773413 Color: 16
Size: 226576 Color: 2

Bin 1263: 12 of cap free
Amount of items: 2
Items: 
Size: 780209 Color: 17
Size: 219780 Color: 9

Bin 1264: 12 of cap free
Amount of items: 2
Items: 
Size: 783317 Color: 12
Size: 216672 Color: 0

Bin 1265: 12 of cap free
Amount of items: 2
Items: 
Size: 790916 Color: 12
Size: 209073 Color: 18

Bin 1266: 12 of cap free
Amount of items: 2
Items: 
Size: 795883 Color: 15
Size: 204106 Color: 8

Bin 1267: 12 of cap free
Amount of items: 2
Items: 
Size: 798246 Color: 1
Size: 201743 Color: 8

Bin 1268: 12 of cap free
Amount of items: 2
Items: 
Size: 799644 Color: 16
Size: 200345 Color: 1

Bin 1269: 12 of cap free
Amount of items: 3
Items: 
Size: 690660 Color: 4
Size: 155410 Color: 3
Size: 153919 Color: 4

Bin 1270: 12 of cap free
Amount of items: 3
Items: 
Size: 726681 Color: 2
Size: 139299 Color: 1
Size: 134009 Color: 2

Bin 1271: 12 of cap free
Amount of items: 3
Items: 
Size: 696527 Color: 12
Size: 151771 Color: 0
Size: 151691 Color: 14

Bin 1272: 12 of cap free
Amount of items: 2
Items: 
Size: 672821 Color: 6
Size: 327168 Color: 16

Bin 1273: 12 of cap free
Amount of items: 2
Items: 
Size: 677996 Color: 3
Size: 321993 Color: 2

Bin 1274: 12 of cap free
Amount of items: 3
Items: 
Size: 617608 Color: 15
Size: 192428 Color: 15
Size: 189953 Color: 9

Bin 1275: 12 of cap free
Amount of items: 3
Items: 
Size: 710988 Color: 17
Size: 146265 Color: 12
Size: 142736 Color: 18

Bin 1276: 12 of cap free
Amount of items: 3
Items: 
Size: 626822 Color: 1
Size: 187323 Color: 11
Size: 185844 Color: 11

Bin 1277: 12 of cap free
Amount of items: 3
Items: 
Size: 674602 Color: 14
Size: 162853 Color: 6
Size: 162534 Color: 7

Bin 1278: 12 of cap free
Amount of items: 3
Items: 
Size: 424029 Color: 13
Size: 295083 Color: 17
Size: 280877 Color: 2

Bin 1279: 12 of cap free
Amount of items: 2
Items: 
Size: 746381 Color: 2
Size: 253608 Color: 0

Bin 1280: 12 of cap free
Amount of items: 2
Items: 
Size: 637443 Color: 6
Size: 362546 Color: 5

Bin 1281: 12 of cap free
Amount of items: 3
Items: 
Size: 357803 Color: 7
Size: 336303 Color: 4
Size: 305883 Color: 8

Bin 1282: 12 of cap free
Amount of items: 2
Items: 
Size: 657137 Color: 9
Size: 342852 Color: 12

Bin 1283: 12 of cap free
Amount of items: 2
Items: 
Size: 773936 Color: 19
Size: 226053 Color: 14

Bin 1284: 12 of cap free
Amount of items: 3
Items: 
Size: 353523 Color: 2
Size: 345035 Color: 14
Size: 301431 Color: 0

Bin 1285: 12 of cap free
Amount of items: 2
Items: 
Size: 639933 Color: 2
Size: 360056 Color: 3

Bin 1286: 13 of cap free
Amount of items: 3
Items: 
Size: 677396 Color: 17
Size: 161353 Color: 12
Size: 161239 Color: 6

Bin 1287: 13 of cap free
Amount of items: 3
Items: 
Size: 736661 Color: 9
Size: 132857 Color: 6
Size: 130470 Color: 1

Bin 1288: 13 of cap free
Amount of items: 3
Items: 
Size: 638508 Color: 1
Size: 185161 Color: 16
Size: 176319 Color: 3

Bin 1289: 13 of cap free
Amount of items: 2
Items: 
Size: 757918 Color: 4
Size: 242070 Color: 2

Bin 1290: 13 of cap free
Amount of items: 3
Items: 
Size: 359041 Color: 4
Size: 333177 Color: 11
Size: 307770 Color: 15

Bin 1291: 13 of cap free
Amount of items: 2
Items: 
Size: 503244 Color: 12
Size: 496744 Color: 4

Bin 1292: 13 of cap free
Amount of items: 2
Items: 
Size: 511458 Color: 15
Size: 488530 Color: 4

Bin 1293: 13 of cap free
Amount of items: 2
Items: 
Size: 520756 Color: 13
Size: 479232 Color: 4

Bin 1294: 13 of cap free
Amount of items: 2
Items: 
Size: 523781 Color: 6
Size: 476207 Color: 10

Bin 1295: 13 of cap free
Amount of items: 2
Items: 
Size: 526889 Color: 18
Size: 473099 Color: 16

Bin 1296: 13 of cap free
Amount of items: 2
Items: 
Size: 538645 Color: 13
Size: 461343 Color: 12

Bin 1297: 13 of cap free
Amount of items: 2
Items: 
Size: 547224 Color: 6
Size: 452764 Color: 0

Bin 1298: 13 of cap free
Amount of items: 2
Items: 
Size: 555845 Color: 16
Size: 444143 Color: 15

Bin 1299: 13 of cap free
Amount of items: 3
Items: 
Size: 559989 Color: 1
Size: 247755 Color: 18
Size: 192244 Color: 5

Bin 1300: 13 of cap free
Amount of items: 2
Items: 
Size: 596787 Color: 8
Size: 403201 Color: 18

Bin 1301: 13 of cap free
Amount of items: 2
Items: 
Size: 614646 Color: 12
Size: 385342 Color: 11

Bin 1302: 13 of cap free
Amount of items: 2
Items: 
Size: 618977 Color: 13
Size: 381011 Color: 19

Bin 1303: 13 of cap free
Amount of items: 3
Items: 
Size: 619407 Color: 16
Size: 190900 Color: 8
Size: 189681 Color: 1

Bin 1304: 13 of cap free
Amount of items: 2
Items: 
Size: 645406 Color: 4
Size: 354582 Color: 18

Bin 1305: 13 of cap free
Amount of items: 3
Items: 
Size: 649583 Color: 1
Size: 175219 Color: 3
Size: 175186 Color: 5

Bin 1306: 13 of cap free
Amount of items: 3
Items: 
Size: 651012 Color: 7
Size: 174501 Color: 11
Size: 174475 Color: 5

Bin 1307: 13 of cap free
Amount of items: 2
Items: 
Size: 659886 Color: 15
Size: 340102 Color: 1

Bin 1308: 13 of cap free
Amount of items: 2
Items: 
Size: 683257 Color: 1
Size: 316731 Color: 7

Bin 1309: 13 of cap free
Amount of items: 2
Items: 
Size: 685282 Color: 10
Size: 314706 Color: 13

Bin 1310: 13 of cap free
Amount of items: 3
Items: 
Size: 704168 Color: 1
Size: 149962 Color: 3
Size: 145858 Color: 8

Bin 1311: 13 of cap free
Amount of items: 3
Items: 
Size: 724382 Color: 9
Size: 140338 Color: 5
Size: 135268 Color: 16

Bin 1312: 13 of cap free
Amount of items: 2
Items: 
Size: 727900 Color: 5
Size: 272088 Color: 18

Bin 1313: 13 of cap free
Amount of items: 2
Items: 
Size: 751100 Color: 0
Size: 248888 Color: 18

Bin 1314: 13 of cap free
Amount of items: 2
Items: 
Size: 771350 Color: 6
Size: 228638 Color: 9

Bin 1315: 13 of cap free
Amount of items: 2
Items: 
Size: 776041 Color: 5
Size: 223947 Color: 15

Bin 1316: 13 of cap free
Amount of items: 2
Items: 
Size: 776782 Color: 2
Size: 223206 Color: 13

Bin 1317: 13 of cap free
Amount of items: 2
Items: 
Size: 796619 Color: 7
Size: 203369 Color: 2

Bin 1318: 13 of cap free
Amount of items: 2
Items: 
Size: 564988 Color: 17
Size: 435000 Color: 14

Bin 1319: 13 of cap free
Amount of items: 2
Items: 
Size: 535695 Color: 8
Size: 464293 Color: 10

Bin 1320: 13 of cap free
Amount of items: 3
Items: 
Size: 411989 Color: 9
Size: 309910 Color: 10
Size: 278089 Color: 14

Bin 1321: 13 of cap free
Amount of items: 3
Items: 
Size: 751297 Color: 3
Size: 127504 Color: 1
Size: 121187 Color: 1

Bin 1322: 13 of cap free
Amount of items: 2
Items: 
Size: 503266 Color: 15
Size: 496722 Color: 8

Bin 1323: 13 of cap free
Amount of items: 3
Items: 
Size: 697467 Color: 12
Size: 151453 Color: 15
Size: 151068 Color: 5

Bin 1324: 13 of cap free
Amount of items: 2
Items: 
Size: 604644 Color: 7
Size: 395344 Color: 13

Bin 1325: 13 of cap free
Amount of items: 3
Items: 
Size: 367409 Color: 5
Size: 319308 Color: 5
Size: 313271 Color: 15

Bin 1326: 13 of cap free
Amount of items: 2
Items: 
Size: 649844 Color: 17
Size: 350144 Color: 11

Bin 1327: 13 of cap free
Amount of items: 3
Items: 
Size: 667427 Color: 9
Size: 171033 Color: 17
Size: 161528 Color: 12

Bin 1328: 13 of cap free
Amount of items: 2
Items: 
Size: 763642 Color: 3
Size: 236346 Color: 14

Bin 1329: 13 of cap free
Amount of items: 2
Items: 
Size: 685117 Color: 11
Size: 314871 Color: 0

Bin 1330: 13 of cap free
Amount of items: 3
Items: 
Size: 477905 Color: 18
Size: 261224 Color: 0
Size: 260859 Color: 9

Bin 1331: 13 of cap free
Amount of items: 3
Items: 
Size: 700241 Color: 2
Size: 154531 Color: 9
Size: 145216 Color: 17

Bin 1332: 13 of cap free
Amount of items: 3
Items: 
Size: 534953 Color: 3
Size: 241533 Color: 5
Size: 223502 Color: 7

Bin 1333: 13 of cap free
Amount of items: 2
Items: 
Size: 697982 Color: 7
Size: 302006 Color: 4

Bin 1334: 14 of cap free
Amount of items: 2
Items: 
Size: 507070 Color: 13
Size: 492917 Color: 10

Bin 1335: 14 of cap free
Amount of items: 2
Items: 
Size: 508243 Color: 15
Size: 491744 Color: 9

Bin 1336: 14 of cap free
Amount of items: 2
Items: 
Size: 512226 Color: 6
Size: 487761 Color: 7

Bin 1337: 14 of cap free
Amount of items: 2
Items: 
Size: 518122 Color: 11
Size: 481865 Color: 3

Bin 1338: 14 of cap free
Amount of items: 2
Items: 
Size: 525809 Color: 16
Size: 474178 Color: 17

Bin 1339: 14 of cap free
Amount of items: 2
Items: 
Size: 540157 Color: 10
Size: 459830 Color: 4

Bin 1340: 14 of cap free
Amount of items: 2
Items: 
Size: 574338 Color: 11
Size: 425649 Color: 19

Bin 1341: 14 of cap free
Amount of items: 2
Items: 
Size: 576231 Color: 0
Size: 423756 Color: 13

Bin 1342: 14 of cap free
Amount of items: 2
Items: 
Size: 587965 Color: 12
Size: 412022 Color: 3

Bin 1343: 14 of cap free
Amount of items: 2
Items: 
Size: 588291 Color: 2
Size: 411696 Color: 12

Bin 1344: 14 of cap free
Amount of items: 2
Items: 
Size: 596883 Color: 5
Size: 403104 Color: 11

Bin 1345: 14 of cap free
Amount of items: 2
Items: 
Size: 602272 Color: 3
Size: 397715 Color: 2

Bin 1346: 14 of cap free
Amount of items: 2
Items: 
Size: 633149 Color: 17
Size: 366838 Color: 4

Bin 1347: 14 of cap free
Amount of items: 2
Items: 
Size: 633773 Color: 4
Size: 366214 Color: 9

Bin 1348: 14 of cap free
Amount of items: 3
Items: 
Size: 639001 Color: 19
Size: 182650 Color: 13
Size: 178336 Color: 18

Bin 1349: 14 of cap free
Amount of items: 2
Items: 
Size: 646995 Color: 3
Size: 352992 Color: 0

Bin 1350: 14 of cap free
Amount of items: 2
Items: 
Size: 650060 Color: 6
Size: 349927 Color: 17

Bin 1351: 14 of cap free
Amount of items: 2
Items: 
Size: 651942 Color: 14
Size: 348045 Color: 9

Bin 1352: 14 of cap free
Amount of items: 3
Items: 
Size: 680721 Color: 13
Size: 160296 Color: 3
Size: 158970 Color: 9

Bin 1353: 14 of cap free
Amount of items: 2
Items: 
Size: 682247 Color: 15
Size: 317740 Color: 12

Bin 1354: 14 of cap free
Amount of items: 3
Items: 
Size: 700047 Color: 1
Size: 152821 Color: 10
Size: 147119 Color: 16

Bin 1355: 14 of cap free
Amount of items: 2
Items: 
Size: 703944 Color: 16
Size: 296043 Color: 19

Bin 1356: 14 of cap free
Amount of items: 2
Items: 
Size: 708089 Color: 11
Size: 291898 Color: 10

Bin 1357: 14 of cap free
Amount of items: 2
Items: 
Size: 713694 Color: 14
Size: 286293 Color: 0

Bin 1358: 14 of cap free
Amount of items: 2
Items: 
Size: 713816 Color: 2
Size: 286171 Color: 5

Bin 1359: 14 of cap free
Amount of items: 3
Items: 
Size: 720077 Color: 7
Size: 140879 Color: 4
Size: 139031 Color: 9

Bin 1360: 14 of cap free
Amount of items: 2
Items: 
Size: 724081 Color: 18
Size: 275906 Color: 4

Bin 1361: 14 of cap free
Amount of items: 2
Items: 
Size: 731176 Color: 6
Size: 268811 Color: 13

Bin 1362: 14 of cap free
Amount of items: 2
Items: 
Size: 742776 Color: 7
Size: 257211 Color: 16

Bin 1363: 14 of cap free
Amount of items: 2
Items: 
Size: 743057 Color: 3
Size: 256930 Color: 4

Bin 1364: 14 of cap free
Amount of items: 3
Items: 
Size: 751737 Color: 5
Size: 124410 Color: 10
Size: 123840 Color: 9

Bin 1365: 14 of cap free
Amount of items: 2
Items: 
Size: 755462 Color: 18
Size: 244525 Color: 19

Bin 1366: 14 of cap free
Amount of items: 2
Items: 
Size: 761478 Color: 19
Size: 238509 Color: 4

Bin 1367: 14 of cap free
Amount of items: 2
Items: 
Size: 769055 Color: 16
Size: 230932 Color: 0

Bin 1368: 14 of cap free
Amount of items: 2
Items: 
Size: 774426 Color: 4
Size: 225561 Color: 11

Bin 1369: 14 of cap free
Amount of items: 2
Items: 
Size: 778006 Color: 9
Size: 221981 Color: 0

Bin 1370: 14 of cap free
Amount of items: 3
Items: 
Size: 713984 Color: 15
Size: 147216 Color: 17
Size: 138787 Color: 3

Bin 1371: 14 of cap free
Amount of items: 2
Items: 
Size: 554863 Color: 19
Size: 445124 Color: 13

Bin 1372: 14 of cap free
Amount of items: 2
Items: 
Size: 579234 Color: 13
Size: 420753 Color: 0

Bin 1373: 14 of cap free
Amount of items: 2
Items: 
Size: 738253 Color: 16
Size: 261734 Color: 19

Bin 1374: 14 of cap free
Amount of items: 2
Items: 
Size: 795456 Color: 9
Size: 204531 Color: 8

Bin 1375: 14 of cap free
Amount of items: 2
Items: 
Size: 771369 Color: 12
Size: 228618 Color: 10

Bin 1376: 14 of cap free
Amount of items: 2
Items: 
Size: 509335 Color: 7
Size: 490652 Color: 10

Bin 1377: 14 of cap free
Amount of items: 2
Items: 
Size: 715233 Color: 8
Size: 284754 Color: 2

Bin 1378: 14 of cap free
Amount of items: 3
Items: 
Size: 559455 Color: 8
Size: 242325 Color: 16
Size: 198207 Color: 6

Bin 1379: 15 of cap free
Amount of items: 3
Items: 
Size: 647272 Color: 10
Size: 176381 Color: 16
Size: 176333 Color: 14

Bin 1380: 15 of cap free
Amount of items: 2
Items: 
Size: 502545 Color: 0
Size: 497441 Color: 8

Bin 1381: 15 of cap free
Amount of items: 2
Items: 
Size: 504362 Color: 7
Size: 495624 Color: 16

Bin 1382: 15 of cap free
Amount of items: 2
Items: 
Size: 520364 Color: 14
Size: 479622 Color: 17

Bin 1383: 15 of cap free
Amount of items: 2
Items: 
Size: 540173 Color: 4
Size: 459813 Color: 18

Bin 1384: 15 of cap free
Amount of items: 2
Items: 
Size: 552003 Color: 11
Size: 447983 Color: 0

Bin 1385: 15 of cap free
Amount of items: 2
Items: 
Size: 555005 Color: 6
Size: 444981 Color: 5

Bin 1386: 15 of cap free
Amount of items: 3
Items: 
Size: 564531 Color: 7
Size: 241462 Color: 3
Size: 193993 Color: 16

Bin 1387: 15 of cap free
Amount of items: 2
Items: 
Size: 567063 Color: 15
Size: 432923 Color: 17

Bin 1388: 15 of cap free
Amount of items: 2
Items: 
Size: 579937 Color: 5
Size: 420049 Color: 8

Bin 1389: 15 of cap free
Amount of items: 2
Items: 
Size: 580021 Color: 19
Size: 419965 Color: 14

Bin 1390: 15 of cap free
Amount of items: 2
Items: 
Size: 599900 Color: 1
Size: 400086 Color: 6

Bin 1391: 15 of cap free
Amount of items: 2
Items: 
Size: 611061 Color: 17
Size: 388925 Color: 1

Bin 1392: 15 of cap free
Amount of items: 2
Items: 
Size: 632062 Color: 11
Size: 367924 Color: 6

Bin 1393: 15 of cap free
Amount of items: 3
Items: 
Size: 651095 Color: 5
Size: 174561 Color: 10
Size: 174330 Color: 10

Bin 1394: 15 of cap free
Amount of items: 2
Items: 
Size: 655592 Color: 0
Size: 344394 Color: 17

Bin 1395: 15 of cap free
Amount of items: 2
Items: 
Size: 656971 Color: 17
Size: 343015 Color: 7

Bin 1396: 15 of cap free
Amount of items: 2
Items: 
Size: 661270 Color: 18
Size: 338716 Color: 1

Bin 1397: 15 of cap free
Amount of items: 3
Items: 
Size: 661519 Color: 7
Size: 170439 Color: 4
Size: 168028 Color: 0

Bin 1398: 15 of cap free
Amount of items: 2
Items: 
Size: 673159 Color: 16
Size: 326827 Color: 2

Bin 1399: 15 of cap free
Amount of items: 3
Items: 
Size: 678031 Color: 14
Size: 161032 Color: 18
Size: 160923 Color: 4

Bin 1400: 15 of cap free
Amount of items: 2
Items: 
Size: 682914 Color: 9
Size: 317072 Color: 13

Bin 1401: 15 of cap free
Amount of items: 3
Items: 
Size: 698722 Color: 6
Size: 151227 Color: 0
Size: 150037 Color: 13

Bin 1402: 15 of cap free
Amount of items: 3
Items: 
Size: 709103 Color: 8
Size: 148149 Color: 8
Size: 142734 Color: 4

Bin 1403: 15 of cap free
Amount of items: 2
Items: 
Size: 709994 Color: 10
Size: 289992 Color: 12

Bin 1404: 15 of cap free
Amount of items: 2
Items: 
Size: 710703 Color: 9
Size: 289283 Color: 19

Bin 1405: 15 of cap free
Amount of items: 2
Items: 
Size: 712780 Color: 5
Size: 287206 Color: 13

Bin 1406: 15 of cap free
Amount of items: 3
Items: 
Size: 717005 Color: 13
Size: 144270 Color: 17
Size: 138711 Color: 7

Bin 1407: 15 of cap free
Amount of items: 2
Items: 
Size: 755878 Color: 14
Size: 244108 Color: 2

Bin 1408: 15 of cap free
Amount of items: 2
Items: 
Size: 767077 Color: 0
Size: 232909 Color: 5

Bin 1409: 15 of cap free
Amount of items: 2
Items: 
Size: 768885 Color: 19
Size: 231101 Color: 17

Bin 1410: 15 of cap free
Amount of items: 2
Items: 
Size: 783110 Color: 6
Size: 216876 Color: 16

Bin 1411: 15 of cap free
Amount of items: 3
Items: 
Size: 553552 Color: 19
Size: 251152 Color: 19
Size: 195282 Color: 8

Bin 1412: 15 of cap free
Amount of items: 3
Items: 
Size: 631269 Color: 1
Size: 184393 Color: 19
Size: 184324 Color: 2

Bin 1413: 15 of cap free
Amount of items: 2
Items: 
Size: 518224 Color: 11
Size: 481762 Color: 10

Bin 1414: 15 of cap free
Amount of items: 2
Items: 
Size: 635855 Color: 1
Size: 364131 Color: 3

Bin 1415: 15 of cap free
Amount of items: 3
Items: 
Size: 602550 Color: 3
Size: 206331 Color: 6
Size: 191105 Color: 19

Bin 1416: 15 of cap free
Amount of items: 2
Items: 
Size: 682813 Color: 4
Size: 317173 Color: 3

Bin 1417: 15 of cap free
Amount of items: 2
Items: 
Size: 756782 Color: 2
Size: 243204 Color: 4

Bin 1418: 15 of cap free
Amount of items: 2
Items: 
Size: 715476 Color: 15
Size: 284510 Color: 3

Bin 1419: 15 of cap free
Amount of items: 2
Items: 
Size: 549306 Color: 16
Size: 450680 Color: 1

Bin 1420: 15 of cap free
Amount of items: 3
Items: 
Size: 695113 Color: 8
Size: 153021 Color: 4
Size: 151852 Color: 2

Bin 1421: 15 of cap free
Amount of items: 2
Items: 
Size: 557847 Color: 8
Size: 442139 Color: 3

Bin 1422: 15 of cap free
Amount of items: 3
Items: 
Size: 642315 Color: 19
Size: 181507 Color: 12
Size: 176164 Color: 14

Bin 1423: 15 of cap free
Amount of items: 2
Items: 
Size: 534256 Color: 4
Size: 465730 Color: 1

Bin 1424: 15 of cap free
Amount of items: 2
Items: 
Size: 712107 Color: 10
Size: 287879 Color: 11

Bin 1425: 15 of cap free
Amount of items: 2
Items: 
Size: 623053 Color: 15
Size: 376933 Color: 4

Bin 1426: 15 of cap free
Amount of items: 4
Items: 
Size: 281854 Color: 2
Size: 280361 Color: 19
Size: 264537 Color: 15
Size: 173234 Color: 18

Bin 1427: 16 of cap free
Amount of items: 3
Items: 
Size: 782594 Color: 17
Size: 108882 Color: 10
Size: 108509 Color: 3

Bin 1428: 16 of cap free
Amount of items: 2
Items: 
Size: 501642 Color: 1
Size: 498343 Color: 6

Bin 1429: 16 of cap free
Amount of items: 2
Items: 
Size: 506165 Color: 10
Size: 493820 Color: 19

Bin 1430: 16 of cap free
Amount of items: 2
Items: 
Size: 508212 Color: 12
Size: 491773 Color: 6

Bin 1431: 16 of cap free
Amount of items: 2
Items: 
Size: 510964 Color: 6
Size: 489021 Color: 5

Bin 1432: 16 of cap free
Amount of items: 2
Items: 
Size: 517112 Color: 9
Size: 482873 Color: 18

Bin 1433: 16 of cap free
Amount of items: 2
Items: 
Size: 529277 Color: 1
Size: 470708 Color: 11

Bin 1434: 16 of cap free
Amount of items: 2
Items: 
Size: 555734 Color: 12
Size: 444251 Color: 11

Bin 1435: 16 of cap free
Amount of items: 2
Items: 
Size: 559609 Color: 1
Size: 440376 Color: 4

Bin 1436: 16 of cap free
Amount of items: 2
Items: 
Size: 562668 Color: 4
Size: 437317 Color: 12

Bin 1437: 16 of cap free
Amount of items: 2
Items: 
Size: 567957 Color: 10
Size: 432028 Color: 6

Bin 1438: 16 of cap free
Amount of items: 2
Items: 
Size: 579864 Color: 3
Size: 420121 Color: 10

Bin 1439: 16 of cap free
Amount of items: 2
Items: 
Size: 596787 Color: 0
Size: 403198 Color: 11

Bin 1440: 16 of cap free
Amount of items: 2
Items: 
Size: 604793 Color: 1
Size: 395192 Color: 12

Bin 1441: 16 of cap free
Amount of items: 2
Items: 
Size: 624799 Color: 11
Size: 375186 Color: 1

Bin 1442: 16 of cap free
Amount of items: 2
Items: 
Size: 631045 Color: 5
Size: 368940 Color: 4

Bin 1443: 16 of cap free
Amount of items: 2
Items: 
Size: 635807 Color: 13
Size: 364178 Color: 5

Bin 1444: 16 of cap free
Amount of items: 2
Items: 
Size: 638772 Color: 6
Size: 361213 Color: 4

Bin 1445: 16 of cap free
Amount of items: 2
Items: 
Size: 657008 Color: 9
Size: 342977 Color: 7

Bin 1446: 16 of cap free
Amount of items: 2
Items: 
Size: 659340 Color: 10
Size: 340645 Color: 7

Bin 1447: 16 of cap free
Amount of items: 3
Items: 
Size: 685407 Color: 10
Size: 157984 Color: 15
Size: 156594 Color: 2

Bin 1448: 16 of cap free
Amount of items: 3
Items: 
Size: 700924 Color: 10
Size: 149538 Color: 10
Size: 149523 Color: 19

Bin 1449: 16 of cap free
Amount of items: 2
Items: 
Size: 707819 Color: 1
Size: 292166 Color: 15

Bin 1450: 16 of cap free
Amount of items: 2
Items: 
Size: 708299 Color: 7
Size: 291686 Color: 9

Bin 1451: 16 of cap free
Amount of items: 2
Items: 
Size: 712683 Color: 13
Size: 287302 Color: 16

Bin 1452: 16 of cap free
Amount of items: 2
Items: 
Size: 713074 Color: 18
Size: 286911 Color: 5

Bin 1453: 16 of cap free
Amount of items: 2
Items: 
Size: 720045 Color: 9
Size: 279940 Color: 19

Bin 1454: 16 of cap free
Amount of items: 2
Items: 
Size: 729737 Color: 8
Size: 270248 Color: 0

Bin 1455: 16 of cap free
Amount of items: 2
Items: 
Size: 738452 Color: 4
Size: 261533 Color: 9

Bin 1456: 16 of cap free
Amount of items: 2
Items: 
Size: 740056 Color: 4
Size: 259929 Color: 8

Bin 1457: 16 of cap free
Amount of items: 2
Items: 
Size: 746856 Color: 1
Size: 253129 Color: 14

Bin 1458: 16 of cap free
Amount of items: 2
Items: 
Size: 763795 Color: 11
Size: 236190 Color: 16

Bin 1459: 16 of cap free
Amount of items: 3
Items: 
Size: 615066 Color: 13
Size: 223723 Color: 18
Size: 161196 Color: 14

Bin 1460: 16 of cap free
Amount of items: 3
Items: 
Size: 358923 Color: 7
Size: 343494 Color: 5
Size: 297568 Color: 10

Bin 1461: 16 of cap free
Amount of items: 3
Items: 
Size: 738368 Color: 9
Size: 130843 Color: 7
Size: 130774 Color: 8

Bin 1462: 16 of cap free
Amount of items: 3
Items: 
Size: 691430 Color: 14
Size: 154878 Color: 5
Size: 153677 Color: 6

Bin 1463: 16 of cap free
Amount of items: 3
Items: 
Size: 752165 Color: 8
Size: 124375 Color: 3
Size: 123445 Color: 15

Bin 1464: 16 of cap free
Amount of items: 3
Items: 
Size: 682225 Color: 2
Size: 159009 Color: 2
Size: 158751 Color: 7

Bin 1465: 16 of cap free
Amount of items: 3
Items: 
Size: 717099 Color: 3
Size: 144735 Color: 9
Size: 138151 Color: 3

Bin 1466: 16 of cap free
Amount of items: 2
Items: 
Size: 604452 Color: 1
Size: 395533 Color: 19

Bin 1467: 16 of cap free
Amount of items: 3
Items: 
Size: 638934 Color: 6
Size: 182715 Color: 3
Size: 178336 Color: 12

Bin 1468: 16 of cap free
Amount of items: 2
Items: 
Size: 661070 Color: 2
Size: 338915 Color: 12

Bin 1469: 16 of cap free
Amount of items: 2
Items: 
Size: 604408 Color: 14
Size: 395577 Color: 17

Bin 1470: 16 of cap free
Amount of items: 2
Items: 
Size: 513483 Color: 14
Size: 486502 Color: 19

Bin 1471: 16 of cap free
Amount of items: 3
Items: 
Size: 356994 Color: 4
Size: 328996 Color: 1
Size: 313995 Color: 0

Bin 1472: 16 of cap free
Amount of items: 3
Items: 
Size: 516252 Color: 13
Size: 248851 Color: 8
Size: 234882 Color: 2

Bin 1473: 16 of cap free
Amount of items: 2
Items: 
Size: 507462 Color: 9
Size: 492523 Color: 3

Bin 1474: 16 of cap free
Amount of items: 3
Items: 
Size: 389851 Color: 12
Size: 329140 Color: 12
Size: 280994 Color: 2

Bin 1475: 17 of cap free
Amount of items: 2
Items: 
Size: 525834 Color: 4
Size: 474150 Color: 6

Bin 1476: 17 of cap free
Amount of items: 2
Items: 
Size: 566956 Color: 9
Size: 433028 Color: 6

Bin 1477: 17 of cap free
Amount of items: 2
Items: 
Size: 569523 Color: 7
Size: 430461 Color: 15

Bin 1478: 17 of cap free
Amount of items: 2
Items: 
Size: 581666 Color: 1
Size: 418318 Color: 9

Bin 1479: 17 of cap free
Amount of items: 2
Items: 
Size: 582498 Color: 6
Size: 417486 Color: 12

Bin 1480: 17 of cap free
Amount of items: 2
Items: 
Size: 615854 Color: 1
Size: 384130 Color: 11

Bin 1481: 17 of cap free
Amount of items: 2
Items: 
Size: 625600 Color: 0
Size: 374384 Color: 5

Bin 1482: 17 of cap free
Amount of items: 3
Items: 
Size: 632113 Color: 2
Size: 184036 Color: 9
Size: 183835 Color: 17

Bin 1483: 17 of cap free
Amount of items: 2
Items: 
Size: 639140 Color: 5
Size: 360844 Color: 19

Bin 1484: 17 of cap free
Amount of items: 2
Items: 
Size: 658088 Color: 2
Size: 341896 Color: 7

Bin 1485: 17 of cap free
Amount of items: 2
Items: 
Size: 662803 Color: 16
Size: 337181 Color: 1

Bin 1486: 17 of cap free
Amount of items: 2
Items: 
Size: 665825 Color: 17
Size: 334159 Color: 5

Bin 1487: 17 of cap free
Amount of items: 2
Items: 
Size: 669440 Color: 1
Size: 330544 Color: 5

Bin 1488: 17 of cap free
Amount of items: 2
Items: 
Size: 672261 Color: 6
Size: 327723 Color: 10

Bin 1489: 17 of cap free
Amount of items: 2
Items: 
Size: 691633 Color: 9
Size: 308351 Color: 7

Bin 1490: 17 of cap free
Amount of items: 3
Items: 
Size: 717149 Color: 19
Size: 141605 Color: 16
Size: 141230 Color: 19

Bin 1491: 17 of cap free
Amount of items: 2
Items: 
Size: 733527 Color: 15
Size: 266457 Color: 6

Bin 1492: 17 of cap free
Amount of items: 2
Items: 
Size: 737634 Color: 17
Size: 262350 Color: 12

Bin 1493: 17 of cap free
Amount of items: 2
Items: 
Size: 753955 Color: 10
Size: 246029 Color: 13

Bin 1494: 17 of cap free
Amount of items: 2
Items: 
Size: 756136 Color: 1
Size: 243848 Color: 16

Bin 1495: 17 of cap free
Amount of items: 2
Items: 
Size: 763834 Color: 2
Size: 236150 Color: 0

Bin 1496: 17 of cap free
Amount of items: 2
Items: 
Size: 768421 Color: 17
Size: 231563 Color: 6

Bin 1497: 17 of cap free
Amount of items: 3
Items: 
Size: 361683 Color: 2
Size: 343455 Color: 12
Size: 294846 Color: 12

Bin 1498: 17 of cap free
Amount of items: 2
Items: 
Size: 758332 Color: 3
Size: 241652 Color: 12

Bin 1499: 17 of cap free
Amount of items: 2
Items: 
Size: 674746 Color: 15
Size: 325238 Color: 18

Bin 1500: 17 of cap free
Amount of items: 3
Items: 
Size: 683291 Color: 12
Size: 158676 Color: 2
Size: 158017 Color: 9

Bin 1501: 17 of cap free
Amount of items: 3
Items: 
Size: 350265 Color: 3
Size: 328906 Color: 6
Size: 320813 Color: 2

Bin 1502: 17 of cap free
Amount of items: 3
Items: 
Size: 711673 Color: 15
Size: 144486 Color: 9
Size: 143825 Color: 11

Bin 1503: 17 of cap free
Amount of items: 3
Items: 
Size: 535187 Color: 14
Size: 232802 Color: 17
Size: 231995 Color: 15

Bin 1504: 17 of cap free
Amount of items: 3
Items: 
Size: 349566 Color: 9
Size: 340539 Color: 7
Size: 309879 Color: 18

Bin 1505: 18 of cap free
Amount of items: 3
Items: 
Size: 439559 Color: 8
Size: 280291 Color: 0
Size: 280133 Color: 2

Bin 1506: 18 of cap free
Amount of items: 2
Items: 
Size: 518384 Color: 18
Size: 481599 Color: 2

Bin 1507: 18 of cap free
Amount of items: 2
Items: 
Size: 518509 Color: 7
Size: 481474 Color: 1

Bin 1508: 18 of cap free
Amount of items: 2
Items: 
Size: 523066 Color: 14
Size: 476917 Color: 18

Bin 1509: 18 of cap free
Amount of items: 2
Items: 
Size: 551662 Color: 16
Size: 448321 Color: 5

Bin 1510: 18 of cap free
Amount of items: 2
Items: 
Size: 567923 Color: 1
Size: 432060 Color: 12

Bin 1511: 18 of cap free
Amount of items: 2
Items: 
Size: 581421 Color: 19
Size: 418562 Color: 1

Bin 1512: 18 of cap free
Amount of items: 2
Items: 
Size: 581855 Color: 9
Size: 418128 Color: 4

Bin 1513: 18 of cap free
Amount of items: 2
Items: 
Size: 586620 Color: 1
Size: 413363 Color: 10

Bin 1514: 18 of cap free
Amount of items: 2
Items: 
Size: 595975 Color: 12
Size: 404008 Color: 11

Bin 1515: 18 of cap free
Amount of items: 2
Items: 
Size: 598032 Color: 1
Size: 401951 Color: 3

Bin 1516: 18 of cap free
Amount of items: 3
Items: 
Size: 598193 Color: 6
Size: 207957 Color: 0
Size: 193833 Color: 10

Bin 1517: 18 of cap free
Amount of items: 2
Items: 
Size: 613325 Color: 8
Size: 386658 Color: 4

Bin 1518: 18 of cap free
Amount of items: 2
Items: 
Size: 614698 Color: 14
Size: 385285 Color: 10

Bin 1519: 18 of cap free
Amount of items: 2
Items: 
Size: 619846 Color: 4
Size: 380137 Color: 5

Bin 1520: 18 of cap free
Amount of items: 3
Items: 
Size: 622504 Color: 10
Size: 189085 Color: 4
Size: 188394 Color: 5

Bin 1521: 18 of cap free
Amount of items: 2
Items: 
Size: 622799 Color: 17
Size: 377184 Color: 9

Bin 1522: 18 of cap free
Amount of items: 2
Items: 
Size: 626655 Color: 7
Size: 373328 Color: 0

Bin 1523: 18 of cap free
Amount of items: 2
Items: 
Size: 632475 Color: 14
Size: 367508 Color: 13

Bin 1524: 18 of cap free
Amount of items: 2
Items: 
Size: 636398 Color: 0
Size: 363585 Color: 14

Bin 1525: 18 of cap free
Amount of items: 2
Items: 
Size: 644805 Color: 2
Size: 355178 Color: 15

Bin 1526: 18 of cap free
Amount of items: 2
Items: 
Size: 646121 Color: 9
Size: 353862 Color: 5

Bin 1527: 18 of cap free
Amount of items: 2
Items: 
Size: 659674 Color: 13
Size: 340309 Color: 0

Bin 1528: 18 of cap free
Amount of items: 2
Items: 
Size: 678314 Color: 5
Size: 321669 Color: 8

Bin 1529: 18 of cap free
Amount of items: 2
Items: 
Size: 682132 Color: 6
Size: 317851 Color: 9

Bin 1530: 18 of cap free
Amount of items: 2
Items: 
Size: 709592 Color: 7
Size: 290391 Color: 19

Bin 1531: 18 of cap free
Amount of items: 2
Items: 
Size: 721035 Color: 18
Size: 278948 Color: 17

Bin 1532: 18 of cap free
Amount of items: 2
Items: 
Size: 728716 Color: 19
Size: 271267 Color: 5

Bin 1533: 18 of cap free
Amount of items: 2
Items: 
Size: 740234 Color: 8
Size: 259749 Color: 12

Bin 1534: 18 of cap free
Amount of items: 2
Items: 
Size: 745972 Color: 14
Size: 254011 Color: 8

Bin 1535: 18 of cap free
Amount of items: 2
Items: 
Size: 771776 Color: 14
Size: 228207 Color: 1

Bin 1536: 18 of cap free
Amount of items: 2
Items: 
Size: 729120 Color: 14
Size: 270863 Color: 19

Bin 1537: 18 of cap free
Amount of items: 2
Items: 
Size: 751504 Color: 17
Size: 248479 Color: 3

Bin 1538: 18 of cap free
Amount of items: 2
Items: 
Size: 576663 Color: 18
Size: 423320 Color: 19

Bin 1539: 18 of cap free
Amount of items: 2
Items: 
Size: 650602 Color: 4
Size: 349381 Color: 5

Bin 1540: 18 of cap free
Amount of items: 2
Items: 
Size: 784120 Color: 16
Size: 215863 Color: 3

Bin 1541: 18 of cap free
Amount of items: 3
Items: 
Size: 358431 Color: 7
Size: 358281 Color: 6
Size: 283271 Color: 16

Bin 1542: 18 of cap free
Amount of items: 2
Items: 
Size: 616948 Color: 19
Size: 383035 Color: 5

Bin 1543: 18 of cap free
Amount of items: 2
Items: 
Size: 590196 Color: 6
Size: 409787 Color: 9

Bin 1544: 18 of cap free
Amount of items: 3
Items: 
Size: 772643 Color: 15
Size: 113861 Color: 0
Size: 113479 Color: 13

Bin 1545: 18 of cap free
Amount of items: 3
Items: 
Size: 359372 Color: 6
Size: 325719 Color: 1
Size: 314892 Color: 11

Bin 1546: 18 of cap free
Amount of items: 3
Items: 
Size: 416071 Color: 11
Size: 300212 Color: 19
Size: 283700 Color: 10

Bin 1547: 18 of cap free
Amount of items: 2
Items: 
Size: 510823 Color: 18
Size: 489160 Color: 2

Bin 1548: 18 of cap free
Amount of items: 2
Items: 
Size: 530360 Color: 10
Size: 469623 Color: 2

Bin 1549: 18 of cap free
Amount of items: 2
Items: 
Size: 643864 Color: 4
Size: 356119 Color: 1

Bin 1550: 19 of cap free
Amount of items: 2
Items: 
Size: 507223 Color: 18
Size: 492759 Color: 15

Bin 1551: 19 of cap free
Amount of items: 2
Items: 
Size: 508645 Color: 13
Size: 491337 Color: 12

Bin 1552: 19 of cap free
Amount of items: 2
Items: 
Size: 516481 Color: 17
Size: 483501 Color: 2

Bin 1553: 19 of cap free
Amount of items: 2
Items: 
Size: 542855 Color: 9
Size: 457127 Color: 3

Bin 1554: 19 of cap free
Amount of items: 2
Items: 
Size: 544653 Color: 8
Size: 455329 Color: 16

Bin 1555: 19 of cap free
Amount of items: 2
Items: 
Size: 557643 Color: 12
Size: 442339 Color: 9

Bin 1556: 19 of cap free
Amount of items: 2
Items: 
Size: 573642 Color: 14
Size: 426340 Color: 11

Bin 1557: 19 of cap free
Amount of items: 2
Items: 
Size: 581630 Color: 2
Size: 418352 Color: 6

Bin 1558: 19 of cap free
Amount of items: 2
Items: 
Size: 607042 Color: 9
Size: 392940 Color: 13

Bin 1559: 19 of cap free
Amount of items: 2
Items: 
Size: 609702 Color: 11
Size: 390280 Color: 2

Bin 1560: 19 of cap free
Amount of items: 2
Items: 
Size: 610332 Color: 17
Size: 389650 Color: 8

Bin 1561: 19 of cap free
Amount of items: 2
Items: 
Size: 663335 Color: 4
Size: 336647 Color: 11

Bin 1562: 19 of cap free
Amount of items: 2
Items: 
Size: 678431 Color: 16
Size: 321551 Color: 17

Bin 1563: 19 of cap free
Amount of items: 2
Items: 
Size: 693954 Color: 15
Size: 306028 Color: 0

Bin 1564: 19 of cap free
Amount of items: 2
Items: 
Size: 708810 Color: 4
Size: 291172 Color: 15

Bin 1565: 19 of cap free
Amount of items: 2
Items: 
Size: 720511 Color: 11
Size: 279471 Color: 5

Bin 1566: 19 of cap free
Amount of items: 3
Items: 
Size: 722488 Color: 18
Size: 141408 Color: 1
Size: 136086 Color: 8

Bin 1567: 19 of cap free
Amount of items: 2
Items: 
Size: 762038 Color: 7
Size: 237944 Color: 8

Bin 1568: 19 of cap free
Amount of items: 3
Items: 
Size: 419453 Color: 5
Size: 299872 Color: 14
Size: 280657 Color: 13

Bin 1569: 19 of cap free
Amount of items: 2
Items: 
Size: 795613 Color: 16
Size: 204369 Color: 3

Bin 1570: 19 of cap free
Amount of items: 2
Items: 
Size: 514918 Color: 3
Size: 485064 Color: 4

Bin 1571: 19 of cap free
Amount of items: 2
Items: 
Size: 530629 Color: 10
Size: 469353 Color: 13

Bin 1572: 19 of cap free
Amount of items: 2
Items: 
Size: 649384 Color: 9
Size: 350598 Color: 10

Bin 1573: 19 of cap free
Amount of items: 3
Items: 
Size: 549702 Color: 16
Size: 253775 Color: 1
Size: 196505 Color: 6

Bin 1574: 19 of cap free
Amount of items: 3
Items: 
Size: 745528 Color: 12
Size: 127246 Color: 19
Size: 127208 Color: 7

Bin 1575: 20 of cap free
Amount of items: 2
Items: 
Size: 504833 Color: 11
Size: 495148 Color: 10

Bin 1576: 20 of cap free
Amount of items: 2
Items: 
Size: 528073 Color: 9
Size: 471908 Color: 18

Bin 1577: 20 of cap free
Amount of items: 2
Items: 
Size: 529384 Color: 17
Size: 470597 Color: 6

Bin 1578: 20 of cap free
Amount of items: 2
Items: 
Size: 551606 Color: 16
Size: 448375 Color: 13

Bin 1579: 20 of cap free
Amount of items: 2
Items: 
Size: 576861 Color: 12
Size: 423120 Color: 9

Bin 1580: 20 of cap free
Amount of items: 2
Items: 
Size: 587386 Color: 12
Size: 412595 Color: 17

Bin 1581: 20 of cap free
Amount of items: 2
Items: 
Size: 589853 Color: 8
Size: 410128 Color: 3

Bin 1582: 20 of cap free
Amount of items: 2
Items: 
Size: 589901 Color: 19
Size: 410080 Color: 11

Bin 1583: 20 of cap free
Amount of items: 2
Items: 
Size: 597532 Color: 3
Size: 402449 Color: 9

Bin 1584: 20 of cap free
Amount of items: 2
Items: 
Size: 598236 Color: 9
Size: 401745 Color: 8

Bin 1585: 20 of cap free
Amount of items: 2
Items: 
Size: 606041 Color: 18
Size: 393940 Color: 17

Bin 1586: 20 of cap free
Amount of items: 2
Items: 
Size: 632926 Color: 5
Size: 367055 Color: 3

Bin 1587: 20 of cap free
Amount of items: 3
Items: 
Size: 638651 Color: 5
Size: 181330 Color: 5
Size: 180000 Color: 18

Bin 1588: 20 of cap free
Amount of items: 2
Items: 
Size: 644368 Color: 15
Size: 355613 Color: 1

Bin 1589: 20 of cap free
Amount of items: 3
Items: 
Size: 665135 Color: 3
Size: 168914 Color: 4
Size: 165932 Color: 9

Bin 1590: 20 of cap free
Amount of items: 3
Items: 
Size: 613600 Color: 12
Size: 218844 Color: 6
Size: 167537 Color: 18

Bin 1591: 20 of cap free
Amount of items: 2
Items: 
Size: 667897 Color: 11
Size: 332084 Color: 4

Bin 1592: 20 of cap free
Amount of items: 2
Items: 
Size: 668054 Color: 18
Size: 331927 Color: 11

Bin 1593: 20 of cap free
Amount of items: 2
Items: 
Size: 673868 Color: 7
Size: 326113 Color: 1

Bin 1594: 20 of cap free
Amount of items: 3
Items: 
Size: 678292 Color: 0
Size: 160865 Color: 6
Size: 160824 Color: 1

Bin 1595: 20 of cap free
Amount of items: 2
Items: 
Size: 682568 Color: 4
Size: 317413 Color: 11

Bin 1596: 20 of cap free
Amount of items: 2
Items: 
Size: 696592 Color: 1
Size: 303389 Color: 2

Bin 1597: 20 of cap free
Amount of items: 3
Items: 
Size: 701672 Color: 16
Size: 149735 Color: 13
Size: 148574 Color: 7

Bin 1598: 20 of cap free
Amount of items: 2
Items: 
Size: 729471 Color: 14
Size: 270510 Color: 11

Bin 1599: 20 of cap free
Amount of items: 2
Items: 
Size: 774091 Color: 14
Size: 225890 Color: 16

Bin 1600: 20 of cap free
Amount of items: 2
Items: 
Size: 779815 Color: 9
Size: 220166 Color: 15

Bin 1601: 20 of cap free
Amount of items: 2
Items: 
Size: 783940 Color: 7
Size: 216041 Color: 9

Bin 1602: 20 of cap free
Amount of items: 2
Items: 
Size: 793064 Color: 16
Size: 206917 Color: 4

Bin 1603: 20 of cap free
Amount of items: 3
Items: 
Size: 569781 Color: 11
Size: 239139 Color: 18
Size: 191061 Color: 14

Bin 1604: 20 of cap free
Amount of items: 2
Items: 
Size: 656339 Color: 17
Size: 343642 Color: 7

Bin 1605: 20 of cap free
Amount of items: 3
Items: 
Size: 407450 Color: 1
Size: 301227 Color: 8
Size: 291304 Color: 1

Bin 1606: 20 of cap free
Amount of items: 2
Items: 
Size: 581747 Color: 13
Size: 418234 Color: 14

Bin 1607: 20 of cap free
Amount of items: 3
Items: 
Size: 412109 Color: 16
Size: 306168 Color: 2
Size: 281704 Color: 17

Bin 1608: 20 of cap free
Amount of items: 3
Items: 
Size: 655298 Color: 3
Size: 173000 Color: 5
Size: 171683 Color: 0

Bin 1609: 20 of cap free
Amount of items: 2
Items: 
Size: 536081 Color: 0
Size: 463900 Color: 14

Bin 1610: 20 of cap free
Amount of items: 3
Items: 
Size: 385186 Color: 13
Size: 317462 Color: 17
Size: 297333 Color: 19

Bin 1611: 21 of cap free
Amount of items: 3
Items: 
Size: 668962 Color: 15
Size: 165615 Color: 16
Size: 165403 Color: 12

Bin 1612: 21 of cap free
Amount of items: 2
Items: 
Size: 647961 Color: 2
Size: 352019 Color: 19

Bin 1613: 21 of cap free
Amount of items: 3
Items: 
Size: 697868 Color: 9
Size: 151763 Color: 11
Size: 150349 Color: 6

Bin 1614: 21 of cap free
Amount of items: 2
Items: 
Size: 729118 Color: 15
Size: 270862 Color: 3

Bin 1615: 21 of cap free
Amount of items: 2
Items: 
Size: 521685 Color: 13
Size: 478295 Color: 7

Bin 1616: 21 of cap free
Amount of items: 2
Items: 
Size: 528278 Color: 1
Size: 471702 Color: 11

Bin 1617: 21 of cap free
Amount of items: 2
Items: 
Size: 541737 Color: 6
Size: 458243 Color: 17

Bin 1618: 21 of cap free
Amount of items: 2
Items: 
Size: 553747 Color: 0
Size: 446233 Color: 16

Bin 1619: 21 of cap free
Amount of items: 2
Items: 
Size: 568221 Color: 17
Size: 431759 Color: 15

Bin 1620: 21 of cap free
Amount of items: 2
Items: 
Size: 570144 Color: 19
Size: 429836 Color: 16

Bin 1621: 21 of cap free
Amount of items: 2
Items: 
Size: 597871 Color: 5
Size: 402109 Color: 12

Bin 1622: 21 of cap free
Amount of items: 2
Items: 
Size: 598529 Color: 17
Size: 401451 Color: 0

Bin 1623: 21 of cap free
Amount of items: 2
Items: 
Size: 603155 Color: 17
Size: 396825 Color: 19

Bin 1624: 21 of cap free
Amount of items: 2
Items: 
Size: 644957 Color: 5
Size: 355023 Color: 17

Bin 1625: 21 of cap free
Amount of items: 2
Items: 
Size: 660578 Color: 10
Size: 339402 Color: 13

Bin 1626: 21 of cap free
Amount of items: 3
Items: 
Size: 671856 Color: 19
Size: 164574 Color: 7
Size: 163550 Color: 12

Bin 1627: 21 of cap free
Amount of items: 2
Items: 
Size: 685225 Color: 5
Size: 314755 Color: 9

Bin 1628: 21 of cap free
Amount of items: 2
Items: 
Size: 686827 Color: 5
Size: 313153 Color: 18

Bin 1629: 21 of cap free
Amount of items: 2
Items: 
Size: 689401 Color: 12
Size: 310579 Color: 11

Bin 1630: 21 of cap free
Amount of items: 2
Items: 
Size: 702816 Color: 2
Size: 297164 Color: 19

Bin 1631: 21 of cap free
Amount of items: 2
Items: 
Size: 776590 Color: 13
Size: 223390 Color: 4

Bin 1632: 21 of cap free
Amount of items: 2
Items: 
Size: 780420 Color: 18
Size: 219560 Color: 6

Bin 1633: 21 of cap free
Amount of items: 3
Items: 
Size: 684884 Color: 7
Size: 158885 Color: 18
Size: 156211 Color: 9

Bin 1634: 21 of cap free
Amount of items: 2
Items: 
Size: 788386 Color: 19
Size: 211594 Color: 6

Bin 1635: 21 of cap free
Amount of items: 3
Items: 
Size: 554071 Color: 7
Size: 248180 Color: 2
Size: 197729 Color: 16

Bin 1636: 21 of cap free
Amount of items: 3
Items: 
Size: 622832 Color: 15
Size: 188696 Color: 10
Size: 188452 Color: 19

Bin 1637: 21 of cap free
Amount of items: 3
Items: 
Size: 484452 Color: 17
Size: 267898 Color: 5
Size: 247630 Color: 16

Bin 1638: 22 of cap free
Amount of items: 3
Items: 
Size: 677028 Color: 16
Size: 161646 Color: 10
Size: 161305 Color: 17

Bin 1639: 22 of cap free
Amount of items: 3
Items: 
Size: 622477 Color: 16
Size: 189084 Color: 10
Size: 188418 Color: 9

Bin 1640: 22 of cap free
Amount of items: 3
Items: 
Size: 672812 Color: 4
Size: 164095 Color: 16
Size: 163072 Color: 7

Bin 1641: 22 of cap free
Amount of items: 3
Items: 
Size: 361914 Color: 9
Size: 357307 Color: 6
Size: 280758 Color: 10

Bin 1642: 22 of cap free
Amount of items: 2
Items: 
Size: 505013 Color: 8
Size: 494966 Color: 4

Bin 1643: 22 of cap free
Amount of items: 2
Items: 
Size: 506501 Color: 12
Size: 493478 Color: 0

Bin 1644: 22 of cap free
Amount of items: 2
Items: 
Size: 517670 Color: 16
Size: 482309 Color: 6

Bin 1645: 22 of cap free
Amount of items: 2
Items: 
Size: 525227 Color: 14
Size: 474752 Color: 10

Bin 1646: 22 of cap free
Amount of items: 2
Items: 
Size: 558549 Color: 6
Size: 441430 Color: 3

Bin 1647: 22 of cap free
Amount of items: 2
Items: 
Size: 571792 Color: 1
Size: 428187 Color: 16

Bin 1648: 22 of cap free
Amount of items: 2
Items: 
Size: 577149 Color: 6
Size: 422830 Color: 3

Bin 1649: 22 of cap free
Amount of items: 2
Items: 
Size: 629876 Color: 19
Size: 370103 Color: 5

Bin 1650: 22 of cap free
Amount of items: 2
Items: 
Size: 674480 Color: 9
Size: 325499 Color: 0

Bin 1651: 22 of cap free
Amount of items: 2
Items: 
Size: 710822 Color: 17
Size: 289157 Color: 13

Bin 1652: 22 of cap free
Amount of items: 2
Items: 
Size: 727504 Color: 7
Size: 272475 Color: 6

Bin 1653: 22 of cap free
Amount of items: 2
Items: 
Size: 746360 Color: 18
Size: 253619 Color: 2

Bin 1654: 22 of cap free
Amount of items: 2
Items: 
Size: 752608 Color: 18
Size: 247371 Color: 0

Bin 1655: 22 of cap free
Amount of items: 2
Items: 
Size: 797620 Color: 5
Size: 202359 Color: 19

Bin 1656: 22 of cap free
Amount of items: 2
Items: 
Size: 531398 Color: 14
Size: 468581 Color: 8

Bin 1657: 22 of cap free
Amount of items: 2
Items: 
Size: 666194 Color: 11
Size: 333785 Color: 18

Bin 1658: 22 of cap free
Amount of items: 2
Items: 
Size: 782141 Color: 15
Size: 217838 Color: 13

Bin 1659: 22 of cap free
Amount of items: 2
Items: 
Size: 714396 Color: 6
Size: 285583 Color: 17

Bin 1660: 22 of cap free
Amount of items: 2
Items: 
Size: 587584 Color: 13
Size: 412395 Color: 3

Bin 1661: 22 of cap free
Amount of items: 3
Items: 
Size: 687617 Color: 13
Size: 169728 Color: 7
Size: 142634 Color: 9

Bin 1662: 22 of cap free
Amount of items: 3
Items: 
Size: 702108 Color: 5
Size: 150989 Color: 1
Size: 146882 Color: 1

Bin 1663: 22 of cap free
Amount of items: 2
Items: 
Size: 558944 Color: 19
Size: 441035 Color: 6

Bin 1664: 22 of cap free
Amount of items: 2
Items: 
Size: 686555 Color: 2
Size: 313424 Color: 19

Bin 1665: 22 of cap free
Amount of items: 3
Items: 
Size: 385132 Color: 1
Size: 331355 Color: 15
Size: 283492 Color: 2

Bin 1666: 22 of cap free
Amount of items: 2
Items: 
Size: 767856 Color: 2
Size: 232123 Color: 10

Bin 1667: 22 of cap free
Amount of items: 2
Items: 
Size: 688925 Color: 2
Size: 311054 Color: 1

Bin 1668: 23 of cap free
Amount of items: 3
Items: 
Size: 755598 Color: 2
Size: 122811 Color: 3
Size: 121569 Color: 10

Bin 1669: 23 of cap free
Amount of items: 3
Items: 
Size: 761286 Color: 1
Size: 119513 Color: 11
Size: 119179 Color: 5

Bin 1670: 23 of cap free
Amount of items: 3
Items: 
Size: 636661 Color: 16
Size: 185737 Color: 12
Size: 177580 Color: 15

Bin 1671: 23 of cap free
Amount of items: 3
Items: 
Size: 639710 Color: 19
Size: 182566 Color: 14
Size: 177702 Color: 8

Bin 1672: 23 of cap free
Amount of items: 2
Items: 
Size: 501749 Color: 7
Size: 498229 Color: 14

Bin 1673: 23 of cap free
Amount of items: 2
Items: 
Size: 503012 Color: 16
Size: 496966 Color: 18

Bin 1674: 23 of cap free
Amount of items: 2
Items: 
Size: 508140 Color: 4
Size: 491838 Color: 7

Bin 1675: 23 of cap free
Amount of items: 2
Items: 
Size: 510663 Color: 8
Size: 489315 Color: 10

Bin 1676: 23 of cap free
Amount of items: 2
Items: 
Size: 517735 Color: 12
Size: 482243 Color: 19

Bin 1677: 23 of cap free
Amount of items: 2
Items: 
Size: 524407 Color: 5
Size: 475571 Color: 2

Bin 1678: 23 of cap free
Amount of items: 2
Items: 
Size: 532298 Color: 16
Size: 467680 Color: 19

Bin 1679: 23 of cap free
Amount of items: 2
Items: 
Size: 535489 Color: 2
Size: 464489 Color: 17

Bin 1680: 23 of cap free
Amount of items: 2
Items: 
Size: 536746 Color: 5
Size: 463232 Color: 7

Bin 1681: 23 of cap free
Amount of items: 2
Items: 
Size: 545618 Color: 11
Size: 454360 Color: 13

Bin 1682: 23 of cap free
Amount of items: 2
Items: 
Size: 564409 Color: 14
Size: 435569 Color: 7

Bin 1683: 23 of cap free
Amount of items: 3
Items: 
Size: 598165 Color: 1
Size: 207982 Color: 14
Size: 193831 Color: 6

Bin 1684: 23 of cap free
Amount of items: 2
Items: 
Size: 602170 Color: 18
Size: 397808 Color: 6

Bin 1685: 23 of cap free
Amount of items: 2
Items: 
Size: 606524 Color: 9
Size: 393454 Color: 5

Bin 1686: 23 of cap free
Amount of items: 3
Items: 
Size: 651069 Color: 9
Size: 174675 Color: 16
Size: 174234 Color: 10

Bin 1687: 23 of cap free
Amount of items: 2
Items: 
Size: 651704 Color: 14
Size: 348274 Color: 18

Bin 1688: 23 of cap free
Amount of items: 2
Items: 
Size: 655702 Color: 9
Size: 344276 Color: 3

Bin 1689: 23 of cap free
Amount of items: 2
Items: 
Size: 667071 Color: 13
Size: 332907 Color: 5

Bin 1690: 23 of cap free
Amount of items: 2
Items: 
Size: 683534 Color: 7
Size: 316444 Color: 4

Bin 1691: 23 of cap free
Amount of items: 3
Items: 
Size: 700465 Color: 14
Size: 152332 Color: 13
Size: 147181 Color: 14

Bin 1692: 23 of cap free
Amount of items: 2
Items: 
Size: 709122 Color: 13
Size: 290856 Color: 16

Bin 1693: 23 of cap free
Amount of items: 2
Items: 
Size: 733796 Color: 17
Size: 266182 Color: 9

Bin 1694: 23 of cap free
Amount of items: 2
Items: 
Size: 737299 Color: 6
Size: 262679 Color: 3

Bin 1695: 23 of cap free
Amount of items: 2
Items: 
Size: 739889 Color: 18
Size: 260089 Color: 15

Bin 1696: 23 of cap free
Amount of items: 2
Items: 
Size: 740997 Color: 4
Size: 258981 Color: 19

Bin 1697: 23 of cap free
Amount of items: 2
Items: 
Size: 741291 Color: 17
Size: 258687 Color: 9

Bin 1698: 23 of cap free
Amount of items: 2
Items: 
Size: 780910 Color: 5
Size: 219068 Color: 7

Bin 1699: 23 of cap free
Amount of items: 2
Items: 
Size: 786058 Color: 2
Size: 213920 Color: 15

Bin 1700: 23 of cap free
Amount of items: 2
Items: 
Size: 798332 Color: 6
Size: 201646 Color: 2

Bin 1701: 23 of cap free
Amount of items: 3
Items: 
Size: 367196 Color: 16
Size: 348392 Color: 13
Size: 284390 Color: 7

Bin 1702: 23 of cap free
Amount of items: 2
Items: 
Size: 615254 Color: 2
Size: 384724 Color: 0

Bin 1703: 23 of cap free
Amount of items: 3
Items: 
Size: 521512 Color: 11
Size: 239381 Color: 10
Size: 239085 Color: 11

Bin 1704: 23 of cap free
Amount of items: 3
Items: 
Size: 379440 Color: 2
Size: 337733 Color: 5
Size: 282805 Color: 0

Bin 1705: 23 of cap free
Amount of items: 3
Items: 
Size: 444372 Color: 6
Size: 279388 Color: 18
Size: 276218 Color: 4

Bin 1706: 23 of cap free
Amount of items: 2
Items: 
Size: 501998 Color: 9
Size: 497980 Color: 4

Bin 1707: 23 of cap free
Amount of items: 2
Items: 
Size: 674120 Color: 13
Size: 325858 Color: 9

Bin 1708: 23 of cap free
Amount of items: 2
Items: 
Size: 675901 Color: 1
Size: 324077 Color: 3

Bin 1709: 23 of cap free
Amount of items: 3
Items: 
Size: 510327 Color: 16
Size: 248746 Color: 16
Size: 240905 Color: 1

Bin 1710: 24 of cap free
Amount of items: 3
Items: 
Size: 764968 Color: 8
Size: 118112 Color: 10
Size: 116897 Color: 5

Bin 1711: 24 of cap free
Amount of items: 3
Items: 
Size: 755458 Color: 7
Size: 122696 Color: 19
Size: 121823 Color: 7

Bin 1712: 24 of cap free
Amount of items: 3
Items: 
Size: 627555 Color: 11
Size: 186321 Color: 2
Size: 186101 Color: 2

Bin 1713: 24 of cap free
Amount of items: 3
Items: 
Size: 370818 Color: 15
Size: 348359 Color: 17
Size: 280800 Color: 13

Bin 1714: 24 of cap free
Amount of items: 2
Items: 
Size: 506791 Color: 10
Size: 493186 Color: 4

Bin 1715: 24 of cap free
Amount of items: 2
Items: 
Size: 550626 Color: 13
Size: 449351 Color: 17

Bin 1716: 24 of cap free
Amount of items: 2
Items: 
Size: 589627 Color: 16
Size: 410350 Color: 2

Bin 1717: 24 of cap free
Amount of items: 2
Items: 
Size: 608975 Color: 1
Size: 391002 Color: 18

Bin 1718: 24 of cap free
Amount of items: 2
Items: 
Size: 614055 Color: 0
Size: 385922 Color: 14

Bin 1719: 24 of cap free
Amount of items: 3
Items: 
Size: 651995 Color: 8
Size: 174829 Color: 3
Size: 173153 Color: 1

Bin 1720: 24 of cap free
Amount of items: 2
Items: 
Size: 680744 Color: 9
Size: 319233 Color: 8

Bin 1721: 24 of cap free
Amount of items: 2
Items: 
Size: 686393 Color: 9
Size: 313584 Color: 15

Bin 1722: 24 of cap free
Amount of items: 2
Items: 
Size: 691110 Color: 10
Size: 308867 Color: 18

Bin 1723: 24 of cap free
Amount of items: 3
Items: 
Size: 699957 Color: 9
Size: 151257 Color: 19
Size: 148763 Color: 2

Bin 1724: 24 of cap free
Amount of items: 2
Items: 
Size: 753735 Color: 8
Size: 246242 Color: 0

Bin 1725: 24 of cap free
Amount of items: 2
Items: 
Size: 759693 Color: 17
Size: 240284 Color: 15

Bin 1726: 24 of cap free
Amount of items: 2
Items: 
Size: 780838 Color: 18
Size: 219139 Color: 2

Bin 1727: 24 of cap free
Amount of items: 2
Items: 
Size: 781543 Color: 19
Size: 218434 Color: 5

Bin 1728: 24 of cap free
Amount of items: 2
Items: 
Size: 795255 Color: 12
Size: 204722 Color: 15

Bin 1729: 24 of cap free
Amount of items: 2
Items: 
Size: 503319 Color: 3
Size: 496658 Color: 2

Bin 1730: 24 of cap free
Amount of items: 3
Items: 
Size: 621975 Color: 19
Size: 195882 Color: 9
Size: 182120 Color: 4

Bin 1731: 24 of cap free
Amount of items: 2
Items: 
Size: 696964 Color: 10
Size: 303013 Color: 1

Bin 1732: 24 of cap free
Amount of items: 2
Items: 
Size: 624225 Color: 8
Size: 375752 Color: 15

Bin 1733: 24 of cap free
Amount of items: 3
Items: 
Size: 493757 Color: 9
Size: 262894 Color: 5
Size: 243326 Color: 13

Bin 1734: 25 of cap free
Amount of items: 3
Items: 
Size: 759968 Color: 9
Size: 120798 Color: 0
Size: 119210 Color: 4

Bin 1735: 25 of cap free
Amount of items: 2
Items: 
Size: 505072 Color: 14
Size: 494904 Color: 19

Bin 1736: 25 of cap free
Amount of items: 2
Items: 
Size: 535649 Color: 6
Size: 464327 Color: 0

Bin 1737: 25 of cap free
Amount of items: 2
Items: 
Size: 556563 Color: 0
Size: 443413 Color: 10

Bin 1738: 25 of cap free
Amount of items: 2
Items: 
Size: 595073 Color: 2
Size: 404903 Color: 11

Bin 1739: 25 of cap free
Amount of items: 2
Items: 
Size: 632522 Color: 8
Size: 367454 Color: 13

Bin 1740: 25 of cap free
Amount of items: 2
Items: 
Size: 663243 Color: 11
Size: 336733 Color: 0

Bin 1741: 25 of cap free
Amount of items: 2
Items: 
Size: 668699 Color: 9
Size: 331277 Color: 11

Bin 1742: 25 of cap free
Amount of items: 2
Items: 
Size: 669868 Color: 12
Size: 330108 Color: 5

Bin 1743: 25 of cap free
Amount of items: 2
Items: 
Size: 681541 Color: 4
Size: 318435 Color: 11

Bin 1744: 25 of cap free
Amount of items: 2
Items: 
Size: 788782 Color: 0
Size: 211194 Color: 11

Bin 1745: 25 of cap free
Amount of items: 2
Items: 
Size: 627058 Color: 17
Size: 372918 Color: 12

Bin 1746: 25 of cap free
Amount of items: 2
Items: 
Size: 658449 Color: 14
Size: 341527 Color: 3

Bin 1747: 25 of cap free
Amount of items: 3
Items: 
Size: 498953 Color: 15
Size: 262936 Color: 17
Size: 238087 Color: 13

Bin 1748: 25 of cap free
Amount of items: 3
Items: 
Size: 541370 Color: 12
Size: 276417 Color: 13
Size: 182189 Color: 19

Bin 1749: 25 of cap free
Amount of items: 2
Items: 
Size: 554293 Color: 11
Size: 445683 Color: 0

Bin 1750: 25 of cap free
Amount of items: 2
Items: 
Size: 666571 Color: 3
Size: 333405 Color: 19

Bin 1751: 26 of cap free
Amount of items: 2
Items: 
Size: 514298 Color: 11
Size: 485677 Color: 4

Bin 1752: 26 of cap free
Amount of items: 2
Items: 
Size: 515129 Color: 11
Size: 484846 Color: 13

Bin 1753: 26 of cap free
Amount of items: 2
Items: 
Size: 534728 Color: 14
Size: 465247 Color: 18

Bin 1754: 26 of cap free
Amount of items: 2
Items: 
Size: 547955 Color: 17
Size: 452020 Color: 2

Bin 1755: 26 of cap free
Amount of items: 2
Items: 
Size: 550381 Color: 10
Size: 449594 Color: 1

Bin 1756: 26 of cap free
Amount of items: 3
Items: 
Size: 551099 Color: 10
Size: 253255 Color: 12
Size: 195621 Color: 19

Bin 1757: 26 of cap free
Amount of items: 2
Items: 
Size: 584336 Color: 17
Size: 415639 Color: 3

Bin 1758: 26 of cap free
Amount of items: 2
Items: 
Size: 592946 Color: 10
Size: 407029 Color: 0

Bin 1759: 26 of cap free
Amount of items: 2
Items: 
Size: 615798 Color: 0
Size: 384177 Color: 11

Bin 1760: 26 of cap free
Amount of items: 2
Items: 
Size: 619654 Color: 9
Size: 380321 Color: 3

Bin 1761: 26 of cap free
Amount of items: 2
Items: 
Size: 636592 Color: 9
Size: 363383 Color: 8

Bin 1762: 26 of cap free
Amount of items: 2
Items: 
Size: 647176 Color: 5
Size: 352799 Color: 16

Bin 1763: 26 of cap free
Amount of items: 2
Items: 
Size: 660348 Color: 7
Size: 339627 Color: 8

Bin 1764: 26 of cap free
Amount of items: 2
Items: 
Size: 696865 Color: 4
Size: 303110 Color: 12

Bin 1765: 26 of cap free
Amount of items: 2
Items: 
Size: 713614 Color: 19
Size: 286361 Color: 2

Bin 1766: 26 of cap free
Amount of items: 3
Items: 
Size: 763128 Color: 16
Size: 118534 Color: 4
Size: 118313 Color: 12

Bin 1767: 26 of cap free
Amount of items: 2
Items: 
Size: 771066 Color: 4
Size: 228909 Color: 1

Bin 1768: 26 of cap free
Amount of items: 2
Items: 
Size: 774356 Color: 0
Size: 225619 Color: 2

Bin 1769: 26 of cap free
Amount of items: 2
Items: 
Size: 794845 Color: 7
Size: 205130 Color: 17

Bin 1770: 26 of cap free
Amount of items: 2
Items: 
Size: 782804 Color: 16
Size: 217171 Color: 4

Bin 1771: 26 of cap free
Amount of items: 2
Items: 
Size: 789054 Color: 10
Size: 210921 Color: 0

Bin 1772: 26 of cap free
Amount of items: 3
Items: 
Size: 700658 Color: 18
Size: 150422 Color: 1
Size: 148895 Color: 8

Bin 1773: 26 of cap free
Amount of items: 3
Items: 
Size: 516042 Color: 2
Size: 242679 Color: 18
Size: 241254 Color: 8

Bin 1774: 26 of cap free
Amount of items: 2
Items: 
Size: 630489 Color: 1
Size: 369486 Color: 9

Bin 1775: 26 of cap free
Amount of items: 3
Items: 
Size: 568345 Color: 6
Size: 233989 Color: 7
Size: 197641 Color: 1

Bin 1776: 26 of cap free
Amount of items: 2
Items: 
Size: 709875 Color: 1
Size: 290100 Color: 2

Bin 1777: 27 of cap free
Amount of items: 2
Items: 
Size: 679754 Color: 1
Size: 320220 Color: 3

Bin 1778: 27 of cap free
Amount of items: 3
Items: 
Size: 699214 Color: 1
Size: 150380 Color: 17
Size: 150380 Color: 12

Bin 1779: 27 of cap free
Amount of items: 3
Items: 
Size: 677215 Color: 6
Size: 161410 Color: 19
Size: 161349 Color: 7

Bin 1780: 27 of cap free
Amount of items: 2
Items: 
Size: 502968 Color: 6
Size: 497006 Color: 17

Bin 1781: 27 of cap free
Amount of items: 2
Items: 
Size: 511022 Color: 6
Size: 488952 Color: 3

Bin 1782: 27 of cap free
Amount of items: 2
Items: 
Size: 520304 Color: 4
Size: 479670 Color: 15

Bin 1783: 27 of cap free
Amount of items: 2
Items: 
Size: 558164 Color: 2
Size: 441810 Color: 4

Bin 1784: 27 of cap free
Amount of items: 2
Items: 
Size: 571060 Color: 9
Size: 428914 Color: 18

Bin 1785: 27 of cap free
Amount of items: 2
Items: 
Size: 602729 Color: 12
Size: 397245 Color: 14

Bin 1786: 27 of cap free
Amount of items: 2
Items: 
Size: 603264 Color: 18
Size: 396710 Color: 13

Bin 1787: 27 of cap free
Amount of items: 2
Items: 
Size: 640748 Color: 10
Size: 359226 Color: 14

Bin 1788: 27 of cap free
Amount of items: 2
Items: 
Size: 654649 Color: 8
Size: 345325 Color: 0

Bin 1789: 27 of cap free
Amount of items: 2
Items: 
Size: 671641 Color: 10
Size: 328333 Color: 19

Bin 1790: 27 of cap free
Amount of items: 2
Items: 
Size: 684123 Color: 12
Size: 315851 Color: 7

Bin 1791: 27 of cap free
Amount of items: 2
Items: 
Size: 695596 Color: 11
Size: 304378 Color: 18

Bin 1792: 27 of cap free
Amount of items: 2
Items: 
Size: 703507 Color: 2
Size: 296467 Color: 9

Bin 1793: 27 of cap free
Amount of items: 2
Items: 
Size: 713813 Color: 8
Size: 286161 Color: 12

Bin 1794: 27 of cap free
Amount of items: 2
Items: 
Size: 720716 Color: 9
Size: 279258 Color: 10

Bin 1795: 27 of cap free
Amount of items: 2
Items: 
Size: 729293 Color: 15
Size: 270681 Color: 6

Bin 1796: 27 of cap free
Amount of items: 2
Items: 
Size: 739488 Color: 6
Size: 260486 Color: 11

Bin 1797: 27 of cap free
Amount of items: 2
Items: 
Size: 748173 Color: 15
Size: 251801 Color: 14

Bin 1798: 27 of cap free
Amount of items: 2
Items: 
Size: 750902 Color: 6
Size: 249072 Color: 7

Bin 1799: 27 of cap free
Amount of items: 2
Items: 
Size: 755538 Color: 7
Size: 244436 Color: 11

Bin 1800: 27 of cap free
Amount of items: 2
Items: 
Size: 765808 Color: 9
Size: 234166 Color: 6

Bin 1801: 27 of cap free
Amount of items: 2
Items: 
Size: 789299 Color: 12
Size: 210675 Color: 15

Bin 1802: 27 of cap free
Amount of items: 2
Items: 
Size: 795823 Color: 5
Size: 204151 Color: 19

Bin 1803: 27 of cap free
Amount of items: 2
Items: 
Size: 595119 Color: 16
Size: 404855 Color: 8

Bin 1804: 27 of cap free
Amount of items: 3
Items: 
Size: 649948 Color: 13
Size: 175658 Color: 17
Size: 174368 Color: 13

Bin 1805: 27 of cap free
Amount of items: 2
Items: 
Size: 744683 Color: 1
Size: 255291 Color: 9

Bin 1806: 27 of cap free
Amount of items: 2
Items: 
Size: 701717 Color: 10
Size: 298257 Color: 18

Bin 1807: 27 of cap free
Amount of items: 3
Items: 
Size: 503417 Color: 11
Size: 248325 Color: 5
Size: 248232 Color: 15

Bin 1808: 28 of cap free
Amount of items: 3
Items: 
Size: 371812 Color: 13
Size: 330105 Color: 9
Size: 298056 Color: 14

Bin 1809: 28 of cap free
Amount of items: 2
Items: 
Size: 502624 Color: 19
Size: 497349 Color: 10

Bin 1810: 28 of cap free
Amount of items: 2
Items: 
Size: 507618 Color: 10
Size: 492355 Color: 14

Bin 1811: 28 of cap free
Amount of items: 2
Items: 
Size: 516898 Color: 16
Size: 483075 Color: 15

Bin 1812: 28 of cap free
Amount of items: 2
Items: 
Size: 542134 Color: 8
Size: 457839 Color: 19

Bin 1813: 28 of cap free
Amount of items: 2
Items: 
Size: 575317 Color: 19
Size: 424656 Color: 5

Bin 1814: 28 of cap free
Amount of items: 2
Items: 
Size: 612697 Color: 18
Size: 387276 Color: 3

Bin 1815: 28 of cap free
Amount of items: 2
Items: 
Size: 621233 Color: 7
Size: 378740 Color: 10

Bin 1816: 28 of cap free
Amount of items: 2
Items: 
Size: 649906 Color: 14
Size: 350067 Color: 9

Bin 1817: 28 of cap free
Amount of items: 2
Items: 
Size: 681759 Color: 11
Size: 318214 Color: 0

Bin 1818: 28 of cap free
Amount of items: 2
Items: 
Size: 696616 Color: 0
Size: 303357 Color: 3

Bin 1819: 28 of cap free
Amount of items: 2
Items: 
Size: 697291 Color: 10
Size: 302682 Color: 18

Bin 1820: 28 of cap free
Amount of items: 2
Items: 
Size: 705962 Color: 3
Size: 294011 Color: 16

Bin 1821: 28 of cap free
Amount of items: 2
Items: 
Size: 725043 Color: 15
Size: 274930 Color: 12

Bin 1822: 28 of cap free
Amount of items: 2
Items: 
Size: 751376 Color: 18
Size: 248597 Color: 10

Bin 1823: 28 of cap free
Amount of items: 2
Items: 
Size: 769785 Color: 13
Size: 230188 Color: 14

Bin 1824: 28 of cap free
Amount of items: 2
Items: 
Size: 695963 Color: 15
Size: 304010 Color: 17

Bin 1825: 28 of cap free
Amount of items: 3
Items: 
Size: 623681 Color: 19
Size: 193163 Color: 8
Size: 183129 Color: 12

Bin 1826: 28 of cap free
Amount of items: 3
Items: 
Size: 359527 Color: 19
Size: 329621 Color: 1
Size: 310825 Color: 12

Bin 1827: 28 of cap free
Amount of items: 3
Items: 
Size: 683987 Color: 2
Size: 158098 Color: 19
Size: 157888 Color: 2

Bin 1828: 28 of cap free
Amount of items: 2
Items: 
Size: 660819 Color: 3
Size: 339154 Color: 5

Bin 1829: 28 of cap free
Amount of items: 2
Items: 
Size: 564677 Color: 3
Size: 435296 Color: 19

Bin 1830: 28 of cap free
Amount of items: 2
Items: 
Size: 519914 Color: 12
Size: 480059 Color: 11

Bin 1831: 28 of cap free
Amount of items: 2
Items: 
Size: 714578 Color: 7
Size: 285395 Color: 2

Bin 1832: 28 of cap free
Amount of items: 2
Items: 
Size: 635623 Color: 2
Size: 364350 Color: 6

Bin 1833: 28 of cap free
Amount of items: 2
Items: 
Size: 624360 Color: 17
Size: 375613 Color: 3

Bin 1834: 29 of cap free
Amount of items: 3
Items: 
Size: 611676 Color: 1
Size: 197203 Color: 12
Size: 191093 Color: 17

Bin 1835: 29 of cap free
Amount of items: 2
Items: 
Size: 769702 Color: 1
Size: 230270 Color: 2

Bin 1836: 29 of cap free
Amount of items: 2
Items: 
Size: 507858 Color: 7
Size: 492114 Color: 2

Bin 1837: 29 of cap free
Amount of items: 2
Items: 
Size: 517111 Color: 16
Size: 482861 Color: 15

Bin 1838: 29 of cap free
Amount of items: 2
Items: 
Size: 524095 Color: 0
Size: 475877 Color: 17

Bin 1839: 29 of cap free
Amount of items: 2
Items: 
Size: 525669 Color: 6
Size: 474303 Color: 8

Bin 1840: 29 of cap free
Amount of items: 2
Items: 
Size: 529963 Color: 6
Size: 470009 Color: 1

Bin 1841: 29 of cap free
Amount of items: 2
Items: 
Size: 534099 Color: 8
Size: 465873 Color: 3

Bin 1842: 29 of cap free
Amount of items: 2
Items: 
Size: 556731 Color: 0
Size: 443241 Color: 5

Bin 1843: 29 of cap free
Amount of items: 2
Items: 
Size: 565137 Color: 13
Size: 434835 Color: 18

Bin 1844: 29 of cap free
Amount of items: 2
Items: 
Size: 579672 Color: 5
Size: 420300 Color: 7

Bin 1845: 29 of cap free
Amount of items: 2
Items: 
Size: 609731 Color: 15
Size: 390241 Color: 3

Bin 1846: 29 of cap free
Amount of items: 2
Items: 
Size: 653795 Color: 9
Size: 346177 Color: 5

Bin 1847: 29 of cap free
Amount of items: 2
Items: 
Size: 704591 Color: 12
Size: 295381 Color: 16

Bin 1848: 29 of cap free
Amount of items: 2
Items: 
Size: 728868 Color: 18
Size: 271104 Color: 15

Bin 1849: 29 of cap free
Amount of items: 2
Items: 
Size: 754211 Color: 2
Size: 245761 Color: 5

Bin 1850: 29 of cap free
Amount of items: 3
Items: 
Size: 755400 Color: 17
Size: 122409 Color: 15
Size: 122163 Color: 7

Bin 1851: 29 of cap free
Amount of items: 2
Items: 
Size: 797405 Color: 5
Size: 202567 Color: 6

Bin 1852: 29 of cap free
Amount of items: 2
Items: 
Size: 799179 Color: 4
Size: 200793 Color: 9

Bin 1853: 29 of cap free
Amount of items: 2
Items: 
Size: 515768 Color: 2
Size: 484204 Color: 9

Bin 1854: 29 of cap free
Amount of items: 2
Items: 
Size: 680666 Color: 7
Size: 319306 Color: 3

Bin 1855: 29 of cap free
Amount of items: 2
Items: 
Size: 648146 Color: 5
Size: 351826 Color: 1

Bin 1856: 29 of cap free
Amount of items: 3
Items: 
Size: 358700 Color: 15
Size: 320636 Color: 9
Size: 320636 Color: 4

Bin 1857: 29 of cap free
Amount of items: 2
Items: 
Size: 784757 Color: 19
Size: 215215 Color: 18

Bin 1858: 29 of cap free
Amount of items: 2
Items: 
Size: 561092 Color: 12
Size: 438880 Color: 18

Bin 1859: 29 of cap free
Amount of items: 2
Items: 
Size: 784447 Color: 3
Size: 215525 Color: 8

Bin 1860: 29 of cap free
Amount of items: 2
Items: 
Size: 758250 Color: 19
Size: 241722 Color: 3

Bin 1861: 29 of cap free
Amount of items: 2
Items: 
Size: 506699 Color: 2
Size: 493273 Color: 6

Bin 1862: 29 of cap free
Amount of items: 2
Items: 
Size: 549261 Color: 0
Size: 450711 Color: 4

Bin 1863: 29 of cap free
Amount of items: 3
Items: 
Size: 507036 Color: 19
Size: 257368 Color: 1
Size: 235568 Color: 8

Bin 1864: 29 of cap free
Amount of items: 2
Items: 
Size: 513156 Color: 14
Size: 486816 Color: 8

Bin 1865: 30 of cap free
Amount of items: 3
Items: 
Size: 698748 Color: 4
Size: 150878 Color: 15
Size: 150345 Color: 14

Bin 1866: 30 of cap free
Amount of items: 3
Items: 
Size: 343870 Color: 16
Size: 329054 Color: 0
Size: 327047 Color: 17

Bin 1867: 30 of cap free
Amount of items: 3
Items: 
Size: 367157 Color: 6
Size: 350300 Color: 13
Size: 282514 Color: 19

Bin 1868: 30 of cap free
Amount of items: 2
Items: 
Size: 501844 Color: 15
Size: 498127 Color: 10

Bin 1869: 30 of cap free
Amount of items: 2
Items: 
Size: 510925 Color: 1
Size: 489046 Color: 15

Bin 1870: 30 of cap free
Amount of items: 2
Items: 
Size: 514445 Color: 4
Size: 485526 Color: 3

Bin 1871: 30 of cap free
Amount of items: 2
Items: 
Size: 543460 Color: 4
Size: 456511 Color: 7

Bin 1872: 30 of cap free
Amount of items: 2
Items: 
Size: 546258 Color: 4
Size: 453713 Color: 11

Bin 1873: 30 of cap free
Amount of items: 2
Items: 
Size: 555114 Color: 15
Size: 444857 Color: 4

Bin 1874: 30 of cap free
Amount of items: 2
Items: 
Size: 575535 Color: 1
Size: 424436 Color: 3

Bin 1875: 30 of cap free
Amount of items: 2
Items: 
Size: 581017 Color: 8
Size: 418954 Color: 13

Bin 1876: 30 of cap free
Amount of items: 2
Items: 
Size: 594837 Color: 18
Size: 405134 Color: 2

Bin 1877: 30 of cap free
Amount of items: 2
Items: 
Size: 607653 Color: 5
Size: 392318 Color: 3

Bin 1878: 30 of cap free
Amount of items: 3
Items: 
Size: 620258 Color: 17
Size: 190254 Color: 9
Size: 189459 Color: 8

Bin 1879: 30 of cap free
Amount of items: 2
Items: 
Size: 680263 Color: 10
Size: 319708 Color: 19

Bin 1880: 30 of cap free
Amount of items: 2
Items: 
Size: 681207 Color: 1
Size: 318764 Color: 14

Bin 1881: 30 of cap free
Amount of items: 2
Items: 
Size: 686089 Color: 8
Size: 313882 Color: 7

Bin 1882: 30 of cap free
Amount of items: 2
Items: 
Size: 694270 Color: 6
Size: 305701 Color: 4

Bin 1883: 30 of cap free
Amount of items: 3
Items: 
Size: 715436 Color: 7
Size: 143243 Color: 12
Size: 141292 Color: 16

Bin 1884: 30 of cap free
Amount of items: 2
Items: 
Size: 723367 Color: 18
Size: 276604 Color: 13

Bin 1885: 30 of cap free
Amount of items: 3
Items: 
Size: 739089 Color: 8
Size: 130604 Color: 10
Size: 130278 Color: 19

Bin 1886: 30 of cap free
Amount of items: 2
Items: 
Size: 749005 Color: 9
Size: 250966 Color: 13

Bin 1887: 30 of cap free
Amount of items: 2
Items: 
Size: 662345 Color: 16
Size: 337626 Color: 17

Bin 1888: 30 of cap free
Amount of items: 3
Items: 
Size: 385444 Color: 8
Size: 331079 Color: 8
Size: 283448 Color: 3

Bin 1889: 30 of cap free
Amount of items: 2
Items: 
Size: 506449 Color: 11
Size: 493522 Color: 5

Bin 1890: 30 of cap free
Amount of items: 2
Items: 
Size: 548852 Color: 0
Size: 451119 Color: 12

Bin 1891: 30 of cap free
Amount of items: 2
Items: 
Size: 586163 Color: 2
Size: 413808 Color: 0

Bin 1892: 30 of cap free
Amount of items: 2
Items: 
Size: 681452 Color: 3
Size: 318519 Color: 17

Bin 1893: 31 of cap free
Amount of items: 3
Items: 
Size: 704296 Color: 13
Size: 148628 Color: 9
Size: 147046 Color: 1

Bin 1894: 31 of cap free
Amount of items: 2
Items: 
Size: 507397 Color: 1
Size: 492573 Color: 14

Bin 1895: 31 of cap free
Amount of items: 2
Items: 
Size: 509798 Color: 16
Size: 490172 Color: 18

Bin 1896: 31 of cap free
Amount of items: 2
Items: 
Size: 516447 Color: 17
Size: 483523 Color: 12

Bin 1897: 31 of cap free
Amount of items: 2
Items: 
Size: 537594 Color: 17
Size: 462376 Color: 4

Bin 1898: 31 of cap free
Amount of items: 2
Items: 
Size: 552813 Color: 18
Size: 447157 Color: 13

Bin 1899: 31 of cap free
Amount of items: 2
Items: 
Size: 557340 Color: 8
Size: 442630 Color: 18

Bin 1900: 31 of cap free
Amount of items: 2
Items: 
Size: 564295 Color: 7
Size: 435675 Color: 18

Bin 1901: 31 of cap free
Amount of items: 2
Items: 
Size: 568007 Color: 0
Size: 431963 Color: 7

Bin 1902: 31 of cap free
Amount of items: 2
Items: 
Size: 579509 Color: 5
Size: 420461 Color: 1

Bin 1903: 31 of cap free
Amount of items: 2
Items: 
Size: 579990 Color: 18
Size: 419980 Color: 19

Bin 1904: 31 of cap free
Amount of items: 2
Items: 
Size: 603580 Color: 17
Size: 396390 Color: 14

Bin 1905: 31 of cap free
Amount of items: 2
Items: 
Size: 623511 Color: 6
Size: 376459 Color: 18

Bin 1906: 31 of cap free
Amount of items: 2
Items: 
Size: 630824 Color: 13
Size: 369146 Color: 7

Bin 1907: 31 of cap free
Amount of items: 2
Items: 
Size: 658875 Color: 10
Size: 341095 Color: 17

Bin 1908: 31 of cap free
Amount of items: 2
Items: 
Size: 671390 Color: 6
Size: 328580 Color: 9

Bin 1909: 31 of cap free
Amount of items: 2
Items: 
Size: 708206 Color: 2
Size: 291764 Color: 5

Bin 1910: 31 of cap free
Amount of items: 2
Items: 
Size: 722521 Color: 8
Size: 277449 Color: 5

Bin 1911: 31 of cap free
Amount of items: 2
Items: 
Size: 731547 Color: 12
Size: 268423 Color: 1

Bin 1912: 31 of cap free
Amount of items: 2
Items: 
Size: 788853 Color: 10
Size: 211117 Color: 16

Bin 1913: 31 of cap free
Amount of items: 3
Items: 
Size: 728420 Color: 8
Size: 138285 Color: 8
Size: 133265 Color: 1

Bin 1914: 31 of cap free
Amount of items: 3
Items: 
Size: 349886 Color: 18
Size: 339550 Color: 15
Size: 310534 Color: 17

Bin 1915: 31 of cap free
Amount of items: 2
Items: 
Size: 535772 Color: 13
Size: 464198 Color: 12

Bin 1916: 31 of cap free
Amount of items: 2
Items: 
Size: 676559 Color: 15
Size: 323411 Color: 3

Bin 1917: 31 of cap free
Amount of items: 3
Items: 
Size: 745744 Color: 3
Size: 127147 Color: 8
Size: 127079 Color: 9

Bin 1918: 31 of cap free
Amount of items: 3
Items: 
Size: 519263 Color: 17
Size: 248882 Color: 14
Size: 231825 Color: 5

Bin 1919: 32 of cap free
Amount of items: 2
Items: 
Size: 508369 Color: 13
Size: 491600 Color: 3

Bin 1920: 32 of cap free
Amount of items: 2
Items: 
Size: 525026 Color: 11
Size: 474943 Color: 14

Bin 1921: 32 of cap free
Amount of items: 2
Items: 
Size: 531017 Color: 16
Size: 468952 Color: 1

Bin 1922: 32 of cap free
Amount of items: 2
Items: 
Size: 534875 Color: 3
Size: 465094 Color: 10

Bin 1923: 32 of cap free
Amount of items: 2
Items: 
Size: 536445 Color: 15
Size: 463524 Color: 17

Bin 1924: 32 of cap free
Amount of items: 3
Items: 
Size: 559869 Color: 17
Size: 247049 Color: 6
Size: 193051 Color: 2

Bin 1925: 32 of cap free
Amount of items: 2
Items: 
Size: 569411 Color: 3
Size: 430558 Color: 11

Bin 1926: 32 of cap free
Amount of items: 2
Items: 
Size: 576363 Color: 19
Size: 423606 Color: 2

Bin 1927: 32 of cap free
Amount of items: 2
Items: 
Size: 584042 Color: 11
Size: 415927 Color: 7

Bin 1928: 32 of cap free
Amount of items: 2
Items: 
Size: 608696 Color: 8
Size: 391273 Color: 16

Bin 1929: 32 of cap free
Amount of items: 2
Items: 
Size: 616381 Color: 11
Size: 383588 Color: 18

Bin 1930: 32 of cap free
Amount of items: 3
Items: 
Size: 625359 Color: 17
Size: 187452 Color: 4
Size: 187158 Color: 1

Bin 1931: 32 of cap free
Amount of items: 2
Items: 
Size: 629010 Color: 6
Size: 370959 Color: 14

Bin 1932: 32 of cap free
Amount of items: 2
Items: 
Size: 635031 Color: 15
Size: 364938 Color: 10

Bin 1933: 32 of cap free
Amount of items: 2
Items: 
Size: 674807 Color: 10
Size: 325162 Color: 17

Bin 1934: 32 of cap free
Amount of items: 2
Items: 
Size: 699305 Color: 12
Size: 300664 Color: 13

Bin 1935: 32 of cap free
Amount of items: 2
Items: 
Size: 719408 Color: 0
Size: 280561 Color: 18

Bin 1936: 32 of cap free
Amount of items: 3
Items: 
Size: 736625 Color: 7
Size: 131925 Color: 16
Size: 131419 Color: 16

Bin 1937: 32 of cap free
Amount of items: 2
Items: 
Size: 786121 Color: 6
Size: 213848 Color: 17

Bin 1938: 32 of cap free
Amount of items: 2
Items: 
Size: 629914 Color: 6
Size: 370055 Color: 10

Bin 1939: 32 of cap free
Amount of items: 2
Items: 
Size: 655347 Color: 12
Size: 344622 Color: 5

Bin 1940: 32 of cap free
Amount of items: 2
Items: 
Size: 730741 Color: 19
Size: 269228 Color: 7

Bin 1941: 32 of cap free
Amount of items: 2
Items: 
Size: 514399 Color: 19
Size: 485570 Color: 17

Bin 1942: 32 of cap free
Amount of items: 2
Items: 
Size: 735618 Color: 10
Size: 264351 Color: 2

Bin 1943: 32 of cap free
Amount of items: 2
Items: 
Size: 750184 Color: 14
Size: 249785 Color: 18

Bin 1944: 32 of cap free
Amount of items: 2
Items: 
Size: 720143 Color: 1
Size: 279826 Color: 3

Bin 1945: 33 of cap free
Amount of items: 3
Items: 
Size: 647903 Color: 7
Size: 176053 Color: 12
Size: 176012 Color: 16

Bin 1946: 33 of cap free
Amount of items: 3
Items: 
Size: 366899 Color: 15
Size: 335638 Color: 2
Size: 297431 Color: 7

Bin 1947: 33 of cap free
Amount of items: 2
Items: 
Size: 507804 Color: 18
Size: 492164 Color: 11

Bin 1948: 33 of cap free
Amount of items: 2
Items: 
Size: 521012 Color: 15
Size: 478956 Color: 16

Bin 1949: 33 of cap free
Amount of items: 2
Items: 
Size: 530706 Color: 8
Size: 469262 Color: 15

Bin 1950: 33 of cap free
Amount of items: 2
Items: 
Size: 558490 Color: 4
Size: 441478 Color: 16

Bin 1951: 33 of cap free
Amount of items: 2
Items: 
Size: 574670 Color: 8
Size: 425298 Color: 19

Bin 1952: 33 of cap free
Amount of items: 2
Items: 
Size: 579581 Color: 4
Size: 420387 Color: 12

Bin 1953: 33 of cap free
Amount of items: 2
Items: 
Size: 602660 Color: 6
Size: 397308 Color: 1

Bin 1954: 33 of cap free
Amount of items: 2
Items: 
Size: 617768 Color: 5
Size: 382200 Color: 8

Bin 1955: 33 of cap free
Amount of items: 2
Items: 
Size: 633195 Color: 15
Size: 366773 Color: 14

Bin 1956: 33 of cap free
Amount of items: 2
Items: 
Size: 640295 Color: 15
Size: 359673 Color: 10

Bin 1957: 33 of cap free
Amount of items: 2
Items: 
Size: 651278 Color: 5
Size: 348690 Color: 12

Bin 1958: 33 of cap free
Amount of items: 2
Items: 
Size: 671732 Color: 16
Size: 328236 Color: 8

Bin 1959: 33 of cap free
Amount of items: 2
Items: 
Size: 681541 Color: 13
Size: 318427 Color: 10

Bin 1960: 33 of cap free
Amount of items: 2
Items: 
Size: 705069 Color: 14
Size: 294899 Color: 15

Bin 1961: 33 of cap free
Amount of items: 2
Items: 
Size: 712215 Color: 9
Size: 287753 Color: 13

Bin 1962: 33 of cap free
Amount of items: 2
Items: 
Size: 740336 Color: 18
Size: 259632 Color: 6

Bin 1963: 33 of cap free
Amount of items: 2
Items: 
Size: 745702 Color: 9
Size: 254266 Color: 8

Bin 1964: 33 of cap free
Amount of items: 2
Items: 
Size: 754871 Color: 13
Size: 245097 Color: 11

Bin 1965: 33 of cap free
Amount of items: 2
Items: 
Size: 772146 Color: 0
Size: 227822 Color: 12

Bin 1966: 33 of cap free
Amount of items: 2
Items: 
Size: 783514 Color: 12
Size: 216454 Color: 0

Bin 1967: 33 of cap free
Amount of items: 2
Items: 
Size: 796912 Color: 15
Size: 203056 Color: 17

Bin 1968: 33 of cap free
Amount of items: 2
Items: 
Size: 797117 Color: 10
Size: 202851 Color: 13

Bin 1969: 33 of cap free
Amount of items: 2
Items: 
Size: 698942 Color: 8
Size: 301026 Color: 19

Bin 1970: 33 of cap free
Amount of items: 2
Items: 
Size: 667236 Color: 9
Size: 332732 Color: 3

Bin 1971: 33 of cap free
Amount of items: 2
Items: 
Size: 581176 Color: 15
Size: 418792 Color: 16

Bin 1972: 33 of cap free
Amount of items: 3
Items: 
Size: 678765 Color: 15
Size: 160636 Color: 11
Size: 160567 Color: 13

Bin 1973: 33 of cap free
Amount of items: 2
Items: 
Size: 655512 Color: 13
Size: 344456 Color: 1

Bin 1974: 33 of cap free
Amount of items: 2
Items: 
Size: 510075 Color: 7
Size: 489893 Color: 9

Bin 1975: 33 of cap free
Amount of items: 2
Items: 
Size: 767294 Color: 12
Size: 232674 Color: 11

Bin 1976: 34 of cap free
Amount of items: 3
Items: 
Size: 686743 Color: 3
Size: 156878 Color: 7
Size: 156346 Color: 8

Bin 1977: 34 of cap free
Amount of items: 2
Items: 
Size: 548386 Color: 3
Size: 451581 Color: 17

Bin 1978: 34 of cap free
Amount of items: 2
Items: 
Size: 573808 Color: 12
Size: 426159 Color: 16

Bin 1979: 34 of cap free
Amount of items: 2
Items: 
Size: 573925 Color: 3
Size: 426042 Color: 14

Bin 1980: 34 of cap free
Amount of items: 2
Items: 
Size: 584916 Color: 15
Size: 415051 Color: 19

Bin 1981: 34 of cap free
Amount of items: 2
Items: 
Size: 595803 Color: 2
Size: 404164 Color: 14

Bin 1982: 34 of cap free
Amount of items: 2
Items: 
Size: 613344 Color: 17
Size: 386623 Color: 19

Bin 1983: 34 of cap free
Amount of items: 2
Items: 
Size: 616669 Color: 3
Size: 383298 Color: 19

Bin 1984: 34 of cap free
Amount of items: 2
Items: 
Size: 627279 Color: 18
Size: 372688 Color: 17

Bin 1985: 34 of cap free
Amount of items: 2
Items: 
Size: 655335 Color: 8
Size: 344632 Color: 12

Bin 1986: 34 of cap free
Amount of items: 2
Items: 
Size: 691759 Color: 11
Size: 308208 Color: 4

Bin 1987: 34 of cap free
Amount of items: 2
Items: 
Size: 711619 Color: 3
Size: 288348 Color: 6

Bin 1988: 34 of cap free
Amount of items: 2
Items: 
Size: 712914 Color: 10
Size: 287053 Color: 8

Bin 1989: 34 of cap free
Amount of items: 2
Items: 
Size: 721748 Color: 9
Size: 278219 Color: 7

Bin 1990: 34 of cap free
Amount of items: 2
Items: 
Size: 732832 Color: 15
Size: 267135 Color: 9

Bin 1991: 34 of cap free
Amount of items: 2
Items: 
Size: 737437 Color: 14
Size: 262530 Color: 16

Bin 1992: 34 of cap free
Amount of items: 2
Items: 
Size: 739969 Color: 18
Size: 259998 Color: 16

Bin 1993: 34 of cap free
Amount of items: 2
Items: 
Size: 748246 Color: 2
Size: 251721 Color: 19

Bin 1994: 34 of cap free
Amount of items: 3
Items: 
Size: 757570 Color: 8
Size: 121372 Color: 8
Size: 121025 Color: 7

Bin 1995: 34 of cap free
Amount of items: 2
Items: 
Size: 773349 Color: 19
Size: 226618 Color: 1

Bin 1996: 34 of cap free
Amount of items: 3
Items: 
Size: 600520 Color: 13
Size: 208086 Color: 7
Size: 191361 Color: 0

Bin 1997: 34 of cap free
Amount of items: 2
Items: 
Size: 637697 Color: 10
Size: 362270 Color: 14

Bin 1998: 34 of cap free
Amount of items: 2
Items: 
Size: 772852 Color: 11
Size: 227115 Color: 14

Bin 1999: 35 of cap free
Amount of items: 3
Items: 
Size: 359466 Color: 10
Size: 329618 Color: 10
Size: 310882 Color: 19

Bin 2000: 35 of cap free
Amount of items: 2
Items: 
Size: 501499 Color: 4
Size: 498467 Color: 10

Bin 2001: 35 of cap free
Amount of items: 2
Items: 
Size: 521757 Color: 8
Size: 478209 Color: 9

Bin 2002: 35 of cap free
Amount of items: 2
Items: 
Size: 524733 Color: 18
Size: 475233 Color: 9

Bin 2003: 35 of cap free
Amount of items: 2
Items: 
Size: 528189 Color: 15
Size: 471777 Color: 13

Bin 2004: 35 of cap free
Amount of items: 2
Items: 
Size: 537419 Color: 18
Size: 462547 Color: 3

Bin 2005: 35 of cap free
Amount of items: 2
Items: 
Size: 546337 Color: 17
Size: 453629 Color: 3

Bin 2006: 35 of cap free
Amount of items: 2
Items: 
Size: 562242 Color: 11
Size: 437724 Color: 14

Bin 2007: 35 of cap free
Amount of items: 2
Items: 
Size: 584290 Color: 6
Size: 415676 Color: 10

Bin 2008: 35 of cap free
Amount of items: 2
Items: 
Size: 587471 Color: 15
Size: 412495 Color: 4

Bin 2009: 35 of cap free
Amount of items: 2
Items: 
Size: 605108 Color: 1
Size: 394858 Color: 13

Bin 2010: 35 of cap free
Amount of items: 2
Items: 
Size: 617311 Color: 17
Size: 382655 Color: 12

Bin 2011: 35 of cap free
Amount of items: 2
Items: 
Size: 619318 Color: 9
Size: 380648 Color: 7

Bin 2012: 35 of cap free
Amount of items: 2
Items: 
Size: 621828 Color: 2
Size: 378138 Color: 4

Bin 2013: 35 of cap free
Amount of items: 2
Items: 
Size: 631586 Color: 2
Size: 368380 Color: 4

Bin 2014: 35 of cap free
Amount of items: 2
Items: 
Size: 639569 Color: 9
Size: 360397 Color: 16

Bin 2015: 35 of cap free
Amount of items: 2
Items: 
Size: 650629 Color: 14
Size: 349337 Color: 3

Bin 2016: 35 of cap free
Amount of items: 2
Items: 
Size: 651922 Color: 5
Size: 348044 Color: 12

Bin 2017: 35 of cap free
Amount of items: 2
Items: 
Size: 658992 Color: 8
Size: 340974 Color: 19

Bin 2018: 35 of cap free
Amount of items: 2
Items: 
Size: 659117 Color: 13
Size: 340849 Color: 12

Bin 2019: 35 of cap free
Amount of items: 2
Items: 
Size: 665499 Color: 12
Size: 334467 Color: 15

Bin 2020: 35 of cap free
Amount of items: 2
Items: 
Size: 678980 Color: 0
Size: 320986 Color: 7

Bin 2021: 35 of cap free
Amount of items: 2
Items: 
Size: 753242 Color: 2
Size: 246724 Color: 5

Bin 2022: 35 of cap free
Amount of items: 2
Items: 
Size: 763709 Color: 5
Size: 236257 Color: 15

Bin 2023: 35 of cap free
Amount of items: 2
Items: 
Size: 790360 Color: 2
Size: 209606 Color: 8

Bin 2024: 35 of cap free
Amount of items: 3
Items: 
Size: 367282 Color: 15
Size: 332186 Color: 10
Size: 300498 Color: 17

Bin 2025: 35 of cap free
Amount of items: 2
Items: 
Size: 585561 Color: 5
Size: 414405 Color: 2

Bin 2026: 35 of cap free
Amount of items: 2
Items: 
Size: 571317 Color: 18
Size: 428649 Color: 3

Bin 2027: 35 of cap free
Amount of items: 3
Items: 
Size: 353177 Color: 8
Size: 344111 Color: 14
Size: 302678 Color: 14

Bin 2028: 36 of cap free
Amount of items: 3
Items: 
Size: 641820 Color: 16
Size: 179207 Color: 9
Size: 178938 Color: 2

Bin 2029: 36 of cap free
Amount of items: 2
Items: 
Size: 615247 Color: 6
Size: 384718 Color: 18

Bin 2030: 36 of cap free
Amount of items: 3
Items: 
Size: 627482 Color: 1
Size: 186748 Color: 3
Size: 185735 Color: 14

Bin 2031: 36 of cap free
Amount of items: 2
Items: 
Size: 517480 Color: 9
Size: 482485 Color: 12

Bin 2032: 36 of cap free
Amount of items: 2
Items: 
Size: 533000 Color: 12
Size: 466965 Color: 7

Bin 2033: 36 of cap free
Amount of items: 2
Items: 
Size: 607392 Color: 3
Size: 392573 Color: 7

Bin 2034: 36 of cap free
Amount of items: 2
Items: 
Size: 607765 Color: 10
Size: 392200 Color: 4

Bin 2035: 36 of cap free
Amount of items: 2
Items: 
Size: 607842 Color: 19
Size: 392123 Color: 11

Bin 2036: 36 of cap free
Amount of items: 2
Items: 
Size: 612940 Color: 10
Size: 387025 Color: 2

Bin 2037: 36 of cap free
Amount of items: 2
Items: 
Size: 621151 Color: 3
Size: 378814 Color: 11

Bin 2038: 36 of cap free
Amount of items: 2
Items: 
Size: 625166 Color: 11
Size: 374799 Color: 9

Bin 2039: 36 of cap free
Amount of items: 3
Items: 
Size: 639442 Color: 15
Size: 180778 Color: 4
Size: 179745 Color: 3

Bin 2040: 36 of cap free
Amount of items: 2
Items: 
Size: 648989 Color: 8
Size: 350976 Color: 14

Bin 2041: 36 of cap free
Amount of items: 3
Items: 
Size: 652486 Color: 3
Size: 173814 Color: 9
Size: 173665 Color: 18

Bin 2042: 36 of cap free
Amount of items: 2
Items: 
Size: 660073 Color: 7
Size: 339892 Color: 2

Bin 2043: 36 of cap free
Amount of items: 2
Items: 
Size: 662878 Color: 16
Size: 337087 Color: 9

Bin 2044: 36 of cap free
Amount of items: 2
Items: 
Size: 664400 Color: 10
Size: 335565 Color: 1

Bin 2045: 36 of cap free
Amount of items: 2
Items: 
Size: 668404 Color: 10
Size: 331561 Color: 17

Bin 2046: 36 of cap free
Amount of items: 2
Items: 
Size: 797680 Color: 12
Size: 202285 Color: 1

Bin 2047: 36 of cap free
Amount of items: 2
Items: 
Size: 606617 Color: 4
Size: 393348 Color: 6

Bin 2048: 36 of cap free
Amount of items: 2
Items: 
Size: 561284 Color: 4
Size: 438681 Color: 10

Bin 2049: 36 of cap free
Amount of items: 2
Items: 
Size: 796549 Color: 13
Size: 203416 Color: 5

Bin 2050: 36 of cap free
Amount of items: 2
Items: 
Size: 787270 Color: 4
Size: 212695 Color: 19

Bin 2051: 37 of cap free
Amount of items: 3
Items: 
Size: 671818 Color: 4
Size: 164972 Color: 2
Size: 163174 Color: 5

Bin 2052: 37 of cap free
Amount of items: 2
Items: 
Size: 781291 Color: 18
Size: 218673 Color: 12

Bin 2053: 37 of cap free
Amount of items: 2
Items: 
Size: 758111 Color: 9
Size: 241853 Color: 18

Bin 2054: 37 of cap free
Amount of items: 2
Items: 
Size: 513504 Color: 19
Size: 486460 Color: 11

Bin 2055: 37 of cap free
Amount of items: 2
Items: 
Size: 538034 Color: 8
Size: 461930 Color: 11

Bin 2056: 37 of cap free
Amount of items: 2
Items: 
Size: 548194 Color: 19
Size: 451770 Color: 11

Bin 2057: 37 of cap free
Amount of items: 2
Items: 
Size: 589417 Color: 14
Size: 410547 Color: 13

Bin 2058: 37 of cap free
Amount of items: 2
Items: 
Size: 597589 Color: 9
Size: 402375 Color: 16

Bin 2059: 37 of cap free
Amount of items: 2
Items: 
Size: 597719 Color: 12
Size: 402245 Color: 2

Bin 2060: 37 of cap free
Amount of items: 2
Items: 
Size: 642216 Color: 18
Size: 357748 Color: 13

Bin 2061: 37 of cap free
Amount of items: 2
Items: 
Size: 652895 Color: 6
Size: 347069 Color: 4

Bin 2062: 37 of cap free
Amount of items: 2
Items: 
Size: 685144 Color: 18
Size: 314820 Color: 6

Bin 2063: 37 of cap free
Amount of items: 2
Items: 
Size: 688883 Color: 0
Size: 311081 Color: 8

Bin 2064: 37 of cap free
Amount of items: 2
Items: 
Size: 716951 Color: 18
Size: 283013 Color: 7

Bin 2065: 37 of cap free
Amount of items: 2
Items: 
Size: 731340 Color: 17
Size: 268624 Color: 16

Bin 2066: 37 of cap free
Amount of items: 3
Items: 
Size: 741625 Color: 18
Size: 129709 Color: 11
Size: 128630 Color: 6

Bin 2067: 37 of cap free
Amount of items: 2
Items: 
Size: 657797 Color: 4
Size: 342167 Color: 18

Bin 2068: 37 of cap free
Amount of items: 3
Items: 
Size: 744745 Color: 17
Size: 128529 Color: 15
Size: 126690 Color: 14

Bin 2069: 37 of cap free
Amount of items: 2
Items: 
Size: 697390 Color: 13
Size: 302574 Color: 11

Bin 2070: 37 of cap free
Amount of items: 2
Items: 
Size: 653058 Color: 3
Size: 346906 Color: 1

Bin 2071: 37 of cap free
Amount of items: 2
Items: 
Size: 780718 Color: 1
Size: 219246 Color: 18

Bin 2072: 37 of cap free
Amount of items: 2
Items: 
Size: 732315 Color: 14
Size: 267649 Color: 11

Bin 2073: 37 of cap free
Amount of items: 2
Items: 
Size: 543654 Color: 12
Size: 456310 Color: 0

Bin 2074: 37 of cap free
Amount of items: 2
Items: 
Size: 612370 Color: 13
Size: 387594 Color: 17

Bin 2075: 37 of cap free
Amount of items: 2
Items: 
Size: 528698 Color: 15
Size: 471266 Color: 1

Bin 2076: 37 of cap free
Amount of items: 2
Items: 
Size: 569059 Color: 19
Size: 430905 Color: 5

Bin 2077: 37 of cap free
Amount of items: 3
Items: 
Size: 652733 Color: 5
Size: 173621 Color: 19
Size: 173610 Color: 5

Bin 2078: 37 of cap free
Amount of items: 2
Items: 
Size: 558404 Color: 2
Size: 441560 Color: 12

Bin 2079: 37 of cap free
Amount of items: 2
Items: 
Size: 574476 Color: 18
Size: 425488 Color: 0

Bin 2080: 38 of cap free
Amount of items: 2
Items: 
Size: 694094 Color: 13
Size: 305869 Color: 0

Bin 2081: 38 of cap free
Amount of items: 2
Items: 
Size: 533183 Color: 6
Size: 466780 Color: 0

Bin 2082: 38 of cap free
Amount of items: 2
Items: 
Size: 551902 Color: 7
Size: 448061 Color: 10

Bin 2083: 38 of cap free
Amount of items: 2
Items: 
Size: 568134 Color: 11
Size: 431829 Color: 1

Bin 2084: 38 of cap free
Amount of items: 2
Items: 
Size: 588690 Color: 4
Size: 411273 Color: 3

Bin 2085: 38 of cap free
Amount of items: 2
Items: 
Size: 598423 Color: 9
Size: 401540 Color: 14

Bin 2086: 38 of cap free
Amount of items: 2
Items: 
Size: 698264 Color: 5
Size: 301699 Color: 15

Bin 2087: 38 of cap free
Amount of items: 2
Items: 
Size: 715120 Color: 12
Size: 284843 Color: 13

Bin 2088: 38 of cap free
Amount of items: 2
Items: 
Size: 723077 Color: 6
Size: 276886 Color: 14

Bin 2089: 38 of cap free
Amount of items: 2
Items: 
Size: 790649 Color: 13
Size: 209314 Color: 15

Bin 2090: 38 of cap free
Amount of items: 3
Items: 
Size: 478858 Color: 6
Size: 260560 Color: 1
Size: 260545 Color: 9

Bin 2091: 38 of cap free
Amount of items: 3
Items: 
Size: 715433 Color: 2
Size: 143177 Color: 19
Size: 141353 Color: 10

Bin 2092: 38 of cap free
Amount of items: 2
Items: 
Size: 751503 Color: 4
Size: 248460 Color: 10

Bin 2093: 38 of cap free
Amount of items: 2
Items: 
Size: 539333 Color: 18
Size: 460630 Color: 14

Bin 2094: 38 of cap free
Amount of items: 2
Items: 
Size: 651138 Color: 14
Size: 348825 Color: 4

Bin 2095: 38 of cap free
Amount of items: 3
Items: 
Size: 602453 Color: 2
Size: 198864 Color: 12
Size: 198646 Color: 0

Bin 2096: 39 of cap free
Amount of items: 2
Items: 
Size: 579211 Color: 1
Size: 420751 Color: 3

Bin 2097: 39 of cap free
Amount of items: 2
Items: 
Size: 719894 Color: 8
Size: 280068 Color: 1

Bin 2098: 39 of cap free
Amount of items: 2
Items: 
Size: 535684 Color: 18
Size: 464278 Color: 1

Bin 2099: 39 of cap free
Amount of items: 2
Items: 
Size: 678907 Color: 3
Size: 321055 Color: 1

Bin 2100: 39 of cap free
Amount of items: 4
Items: 
Size: 688366 Color: 15
Size: 106813 Color: 7
Size: 103204 Color: 6
Size: 101579 Color: 2

Bin 2101: 39 of cap free
Amount of items: 3
Items: 
Size: 519014 Color: 2
Size: 241025 Color: 16
Size: 239923 Color: 7

Bin 2102: 39 of cap free
Amount of items: 2
Items: 
Size: 548728 Color: 3
Size: 451234 Color: 9

Bin 2103: 39 of cap free
Amount of items: 2
Items: 
Size: 643067 Color: 19
Size: 356895 Color: 3

Bin 2104: 39 of cap free
Amount of items: 2
Items: 
Size: 662998 Color: 19
Size: 336964 Color: 12

Bin 2105: 39 of cap free
Amount of items: 2
Items: 
Size: 671472 Color: 11
Size: 328490 Color: 2

Bin 2106: 39 of cap free
Amount of items: 2
Items: 
Size: 691580 Color: 9
Size: 308382 Color: 13

Bin 2107: 39 of cap free
Amount of items: 2
Items: 
Size: 726591 Color: 1
Size: 273371 Color: 13

Bin 2108: 39 of cap free
Amount of items: 2
Items: 
Size: 775498 Color: 15
Size: 224464 Color: 8

Bin 2109: 39 of cap free
Amount of items: 2
Items: 
Size: 512740 Color: 13
Size: 487222 Color: 10

Bin 2110: 39 of cap free
Amount of items: 3
Items: 
Size: 578382 Color: 6
Size: 227165 Color: 3
Size: 194415 Color: 13

Bin 2111: 39 of cap free
Amount of items: 2
Items: 
Size: 676969 Color: 14
Size: 322993 Color: 10

Bin 2112: 39 of cap free
Amount of items: 2
Items: 
Size: 781191 Color: 0
Size: 218771 Color: 3

Bin 2113: 39 of cap free
Amount of items: 2
Items: 
Size: 550072 Color: 18
Size: 449890 Color: 1

Bin 2114: 40 of cap free
Amount of items: 3
Items: 
Size: 389586 Color: 9
Size: 338849 Color: 19
Size: 271526 Color: 8

Bin 2115: 40 of cap free
Amount of items: 2
Items: 
Size: 502838 Color: 12
Size: 497123 Color: 6

Bin 2116: 40 of cap free
Amount of items: 2
Items: 
Size: 531351 Color: 1
Size: 468610 Color: 8

Bin 2117: 40 of cap free
Amount of items: 2
Items: 
Size: 535349 Color: 5
Size: 464612 Color: 10

Bin 2118: 40 of cap free
Amount of items: 2
Items: 
Size: 547171 Color: 19
Size: 452790 Color: 17

Bin 2119: 40 of cap free
Amount of items: 2
Items: 
Size: 560048 Color: 3
Size: 439913 Color: 8

Bin 2120: 40 of cap free
Amount of items: 2
Items: 
Size: 561666 Color: 13
Size: 438295 Color: 15

Bin 2121: 40 of cap free
Amount of items: 2
Items: 
Size: 585336 Color: 17
Size: 414625 Color: 5

Bin 2122: 40 of cap free
Amount of items: 2
Items: 
Size: 599857 Color: 5
Size: 400104 Color: 19

Bin 2123: 40 of cap free
Amount of items: 2
Items: 
Size: 602167 Color: 4
Size: 397794 Color: 2

Bin 2124: 40 of cap free
Amount of items: 2
Items: 
Size: 613461 Color: 6
Size: 386500 Color: 0

Bin 2125: 40 of cap free
Amount of items: 2
Items: 
Size: 623973 Color: 6
Size: 375988 Color: 7

Bin 2126: 40 of cap free
Amount of items: 2
Items: 
Size: 624794 Color: 10
Size: 375167 Color: 17

Bin 2127: 40 of cap free
Amount of items: 2
Items: 
Size: 664686 Color: 4
Size: 335275 Color: 18

Bin 2128: 40 of cap free
Amount of items: 2
Items: 
Size: 695589 Color: 19
Size: 304372 Color: 16

Bin 2129: 40 of cap free
Amount of items: 2
Items: 
Size: 730124 Color: 6
Size: 269837 Color: 0

Bin 2130: 40 of cap free
Amount of items: 2
Items: 
Size: 780992 Color: 7
Size: 218969 Color: 15

Bin 2131: 40 of cap free
Amount of items: 3
Items: 
Size: 655993 Color: 9
Size: 172162 Color: 8
Size: 171806 Color: 11

Bin 2132: 40 of cap free
Amount of items: 2
Items: 
Size: 766416 Color: 9
Size: 233545 Color: 7

Bin 2133: 40 of cap free
Amount of items: 2
Items: 
Size: 649380 Color: 2
Size: 350581 Color: 6

Bin 2134: 40 of cap free
Amount of items: 2
Items: 
Size: 716522 Color: 3
Size: 283439 Color: 17

Bin 2135: 41 of cap free
Amount of items: 3
Items: 
Size: 638487 Color: 0
Size: 180863 Color: 3
Size: 180610 Color: 9

Bin 2136: 41 of cap free
Amount of items: 2
Items: 
Size: 658587 Color: 4
Size: 341373 Color: 10

Bin 2137: 41 of cap free
Amount of items: 2
Items: 
Size: 794719 Color: 4
Size: 205241 Color: 5

Bin 2138: 41 of cap free
Amount of items: 2
Items: 
Size: 547368 Color: 9
Size: 452592 Color: 19

Bin 2139: 41 of cap free
Amount of items: 2
Items: 
Size: 651200 Color: 14
Size: 348760 Color: 19

Bin 2140: 41 of cap free
Amount of items: 2
Items: 
Size: 708077 Color: 0
Size: 291883 Color: 7

Bin 2141: 41 of cap free
Amount of items: 2
Items: 
Size: 739770 Color: 16
Size: 260190 Color: 10

Bin 2142: 41 of cap free
Amount of items: 2
Items: 
Size: 715331 Color: 12
Size: 284629 Color: 19

Bin 2143: 41 of cap free
Amount of items: 2
Items: 
Size: 659943 Color: 3
Size: 340017 Color: 11

Bin 2144: 41 of cap free
Amount of items: 3
Items: 
Size: 418005 Color: 8
Size: 416109 Color: 2
Size: 165846 Color: 7

Bin 2145: 42 of cap free
Amount of items: 3
Items: 
Size: 634263 Color: 16
Size: 182929 Color: 0
Size: 182767 Color: 13

Bin 2146: 42 of cap free
Amount of items: 3
Items: 
Size: 759139 Color: 13
Size: 125074 Color: 7
Size: 115746 Color: 0

Bin 2147: 42 of cap free
Amount of items: 3
Items: 
Size: 358473 Color: 17
Size: 343434 Color: 7
Size: 298052 Color: 13

Bin 2148: 42 of cap free
Amount of items: 2
Items: 
Size: 503874 Color: 8
Size: 496085 Color: 0

Bin 2149: 42 of cap free
Amount of items: 2
Items: 
Size: 522766 Color: 16
Size: 477193 Color: 18

Bin 2150: 42 of cap free
Amount of items: 2
Items: 
Size: 530904 Color: 2
Size: 469055 Color: 5

Bin 2151: 42 of cap free
Amount of items: 2
Items: 
Size: 532454 Color: 19
Size: 467505 Color: 17

Bin 2152: 42 of cap free
Amount of items: 2
Items: 
Size: 536435 Color: 8
Size: 463524 Color: 1

Bin 2153: 42 of cap free
Amount of items: 2
Items: 
Size: 539193 Color: 16
Size: 460766 Color: 9

Bin 2154: 42 of cap free
Amount of items: 2
Items: 
Size: 540235 Color: 10
Size: 459724 Color: 19

Bin 2155: 42 of cap free
Amount of items: 2
Items: 
Size: 551844 Color: 5
Size: 448115 Color: 16

Bin 2156: 42 of cap free
Amount of items: 2
Items: 
Size: 555063 Color: 14
Size: 444896 Color: 7

Bin 2157: 42 of cap free
Amount of items: 2
Items: 
Size: 574255 Color: 15
Size: 425704 Color: 19

Bin 2158: 42 of cap free
Amount of items: 2
Items: 
Size: 592816 Color: 17
Size: 407143 Color: 0

Bin 2159: 42 of cap free
Amount of items: 2
Items: 
Size: 601906 Color: 11
Size: 398053 Color: 16

Bin 2160: 42 of cap free
Amount of items: 2
Items: 
Size: 627807 Color: 3
Size: 372152 Color: 15

Bin 2161: 42 of cap free
Amount of items: 2
Items: 
Size: 716859 Color: 11
Size: 283100 Color: 12

Bin 2162: 42 of cap free
Amount of items: 2
Items: 
Size: 725187 Color: 15
Size: 274772 Color: 1

Bin 2163: 42 of cap free
Amount of items: 2
Items: 
Size: 760261 Color: 4
Size: 239698 Color: 10

Bin 2164: 42 of cap free
Amount of items: 2
Items: 
Size: 796983 Color: 5
Size: 202976 Color: 19

Bin 2165: 42 of cap free
Amount of items: 2
Items: 
Size: 541471 Color: 9
Size: 458488 Color: 2

Bin 2166: 42 of cap free
Amount of items: 3
Items: 
Size: 539573 Color: 4
Size: 264595 Color: 12
Size: 195791 Color: 13

Bin 2167: 42 of cap free
Amount of items: 2
Items: 
Size: 716798 Color: 9
Size: 283161 Color: 10

Bin 2168: 42 of cap free
Amount of items: 2
Items: 
Size: 534198 Color: 12
Size: 465761 Color: 7

Bin 2169: 42 of cap free
Amount of items: 3
Items: 
Size: 553090 Color: 18
Size: 252583 Color: 11
Size: 194286 Color: 17

Bin 2170: 43 of cap free
Amount of items: 3
Items: 
Size: 668443 Color: 12
Size: 166849 Color: 19
Size: 164666 Color: 1

Bin 2171: 43 of cap free
Amount of items: 3
Items: 
Size: 367971 Color: 15
Size: 335951 Color: 17
Size: 296036 Color: 1

Bin 2172: 43 of cap free
Amount of items: 2
Items: 
Size: 518602 Color: 1
Size: 481356 Color: 6

Bin 2173: 43 of cap free
Amount of items: 2
Items: 
Size: 526771 Color: 8
Size: 473187 Color: 18

Bin 2174: 43 of cap free
Amount of items: 2
Items: 
Size: 545178 Color: 14
Size: 454780 Color: 16

Bin 2175: 43 of cap free
Amount of items: 2
Items: 
Size: 574541 Color: 6
Size: 425417 Color: 7

Bin 2176: 43 of cap free
Amount of items: 2
Items: 
Size: 581318 Color: 6
Size: 418640 Color: 4

Bin 2177: 43 of cap free
Amount of items: 2
Items: 
Size: 591003 Color: 7
Size: 408955 Color: 18

Bin 2178: 43 of cap free
Amount of items: 2
Items: 
Size: 597176 Color: 1
Size: 402782 Color: 0

Bin 2179: 43 of cap free
Amount of items: 2
Items: 
Size: 605307 Color: 9
Size: 394651 Color: 1

Bin 2180: 43 of cap free
Amount of items: 2
Items: 
Size: 621150 Color: 14
Size: 378808 Color: 16

Bin 2181: 43 of cap free
Amount of items: 2
Items: 
Size: 637578 Color: 5
Size: 362380 Color: 16

Bin 2182: 43 of cap free
Amount of items: 2
Items: 
Size: 694386 Color: 11
Size: 305572 Color: 15

Bin 2183: 43 of cap free
Amount of items: 2
Items: 
Size: 731307 Color: 12
Size: 268651 Color: 17

Bin 2184: 43 of cap free
Amount of items: 2
Items: 
Size: 766880 Color: 16
Size: 233078 Color: 17

Bin 2185: 43 of cap free
Amount of items: 2
Items: 
Size: 782227 Color: 0
Size: 217731 Color: 1

Bin 2186: 43 of cap free
Amount of items: 3
Items: 
Size: 438926 Color: 9
Size: 294895 Color: 17
Size: 266137 Color: 1

Bin 2187: 43 of cap free
Amount of items: 2
Items: 
Size: 738152 Color: 15
Size: 261806 Color: 8

Bin 2188: 43 of cap free
Amount of items: 3
Items: 
Size: 632172 Color: 4
Size: 184247 Color: 3
Size: 183539 Color: 8

Bin 2189: 43 of cap free
Amount of items: 2
Items: 
Size: 629908 Color: 3
Size: 370050 Color: 5

Bin 2190: 43 of cap free
Amount of items: 2
Items: 
Size: 620319 Color: 4
Size: 379639 Color: 7

Bin 2191: 43 of cap free
Amount of items: 2
Items: 
Size: 544518 Color: 14
Size: 455440 Color: 3

Bin 2192: 44 of cap free
Amount of items: 2
Items: 
Size: 789424 Color: 1
Size: 210533 Color: 11

Bin 2193: 44 of cap free
Amount of items: 2
Items: 
Size: 581688 Color: 18
Size: 418269 Color: 13

Bin 2194: 44 of cap free
Amount of items: 2
Items: 
Size: 769584 Color: 16
Size: 230373 Color: 3

Bin 2195: 44 of cap free
Amount of items: 3
Items: 
Size: 631169 Color: 12
Size: 184423 Color: 14
Size: 184365 Color: 9

Bin 2196: 44 of cap free
Amount of items: 2
Items: 
Size: 500556 Color: 4
Size: 499401 Color: 10

Bin 2197: 44 of cap free
Amount of items: 2
Items: 
Size: 505709 Color: 13
Size: 494248 Color: 10

Bin 2198: 44 of cap free
Amount of items: 2
Items: 
Size: 564843 Color: 16
Size: 435114 Color: 10

Bin 2199: 44 of cap free
Amount of items: 2
Items: 
Size: 571970 Color: 19
Size: 427987 Color: 18

Bin 2200: 44 of cap free
Amount of items: 2
Items: 
Size: 589357 Color: 19
Size: 410600 Color: 5

Bin 2201: 44 of cap free
Amount of items: 2
Items: 
Size: 601352 Color: 14
Size: 398605 Color: 5

Bin 2202: 44 of cap free
Amount of items: 2
Items: 
Size: 634668 Color: 4
Size: 365289 Color: 14

Bin 2203: 44 of cap free
Amount of items: 3
Items: 
Size: 674213 Color: 19
Size: 162873 Color: 18
Size: 162871 Color: 14

Bin 2204: 44 of cap free
Amount of items: 3
Items: 
Size: 716468 Color: 10
Size: 142407 Color: 18
Size: 141082 Color: 14

Bin 2205: 44 of cap free
Amount of items: 2
Items: 
Size: 741549 Color: 10
Size: 258408 Color: 3

Bin 2206: 44 of cap free
Amount of items: 2
Items: 
Size: 775378 Color: 9
Size: 224579 Color: 15

Bin 2207: 44 of cap free
Amount of items: 3
Items: 
Size: 734043 Color: 4
Size: 134076 Color: 3
Size: 131838 Color: 4

Bin 2208: 44 of cap free
Amount of items: 2
Items: 
Size: 783827 Color: 7
Size: 216130 Color: 13

Bin 2209: 44 of cap free
Amount of items: 2
Items: 
Size: 777371 Color: 6
Size: 222586 Color: 19

Bin 2210: 44 of cap free
Amount of items: 2
Items: 
Size: 756496 Color: 15
Size: 243461 Color: 8

Bin 2211: 44 of cap free
Amount of items: 2
Items: 
Size: 796856 Color: 6
Size: 203101 Color: 10

Bin 2212: 45 of cap free
Amount of items: 2
Items: 
Size: 501241 Color: 9
Size: 498715 Color: 14

Bin 2213: 45 of cap free
Amount of items: 2
Items: 
Size: 545603 Color: 13
Size: 454353 Color: 7

Bin 2214: 45 of cap free
Amount of items: 2
Items: 
Size: 548147 Color: 19
Size: 451809 Color: 14

Bin 2215: 45 of cap free
Amount of items: 2
Items: 
Size: 573099 Color: 11
Size: 426857 Color: 4

Bin 2216: 45 of cap free
Amount of items: 2
Items: 
Size: 576846 Color: 6
Size: 423110 Color: 2

Bin 2217: 45 of cap free
Amount of items: 2
Items: 
Size: 639629 Color: 18
Size: 360327 Color: 17

Bin 2218: 45 of cap free
Amount of items: 2
Items: 
Size: 650507 Color: 9
Size: 349449 Color: 16

Bin 2219: 45 of cap free
Amount of items: 2
Items: 
Size: 664217 Color: 18
Size: 335739 Color: 6

Bin 2220: 45 of cap free
Amount of items: 2
Items: 
Size: 665821 Color: 0
Size: 334135 Color: 5

Bin 2221: 45 of cap free
Amount of items: 3
Items: 
Size: 676391 Color: 0
Size: 161816 Color: 7
Size: 161749 Color: 0

Bin 2222: 45 of cap free
Amount of items: 2
Items: 
Size: 684629 Color: 2
Size: 315327 Color: 14

Bin 2223: 45 of cap free
Amount of items: 2
Items: 
Size: 687545 Color: 11
Size: 312411 Color: 6

Bin 2224: 45 of cap free
Amount of items: 2
Items: 
Size: 692588 Color: 18
Size: 307368 Color: 8

Bin 2225: 45 of cap free
Amount of items: 2
Items: 
Size: 775649 Color: 15
Size: 224307 Color: 19

Bin 2226: 45 of cap free
Amount of items: 3
Items: 
Size: 354159 Color: 17
Size: 337892 Color: 18
Size: 307905 Color: 13

Bin 2227: 45 of cap free
Amount of items: 2
Items: 
Size: 682225 Color: 1
Size: 317731 Color: 5

Bin 2228: 45 of cap free
Amount of items: 2
Items: 
Size: 629203 Color: 0
Size: 370753 Color: 17

Bin 2229: 45 of cap free
Amount of items: 2
Items: 
Size: 768822 Color: 9
Size: 231134 Color: 3

Bin 2230: 45 of cap free
Amount of items: 3
Items: 
Size: 465816 Color: 0
Size: 271206 Color: 17
Size: 262934 Color: 9

Bin 2231: 46 of cap free
Amount of items: 2
Items: 
Size: 543492 Color: 14
Size: 456463 Color: 11

Bin 2232: 46 of cap free
Amount of items: 2
Items: 
Size: 507709 Color: 14
Size: 492246 Color: 7

Bin 2233: 46 of cap free
Amount of items: 2
Items: 
Size: 543337 Color: 7
Size: 456618 Color: 17

Bin 2234: 46 of cap free
Amount of items: 2
Items: 
Size: 625300 Color: 11
Size: 374655 Color: 17

Bin 2235: 46 of cap free
Amount of items: 2
Items: 
Size: 644529 Color: 2
Size: 355426 Color: 3

Bin 2236: 46 of cap free
Amount of items: 2
Items: 
Size: 663721 Color: 7
Size: 336234 Color: 6

Bin 2237: 46 of cap free
Amount of items: 2
Items: 
Size: 694269 Color: 18
Size: 305686 Color: 5

Bin 2238: 46 of cap free
Amount of items: 2
Items: 
Size: 627954 Color: 10
Size: 372001 Color: 13

Bin 2239: 46 of cap free
Amount of items: 2
Items: 
Size: 518754 Color: 12
Size: 481201 Color: 11

Bin 2240: 46 of cap free
Amount of items: 2
Items: 
Size: 671590 Color: 0
Size: 328365 Color: 10

Bin 2241: 46 of cap free
Amount of items: 3
Items: 
Size: 362441 Color: 16
Size: 327501 Color: 17
Size: 310013 Color: 4

Bin 2242: 47 of cap free
Amount of items: 3
Items: 
Size: 682686 Color: 6
Size: 160202 Color: 6
Size: 157066 Color: 1

Bin 2243: 47 of cap free
Amount of items: 3
Items: 
Size: 361733 Color: 5
Size: 356986 Color: 14
Size: 281235 Color: 9

Bin 2244: 47 of cap free
Amount of items: 2
Items: 
Size: 677673 Color: 13
Size: 322281 Color: 0

Bin 2245: 47 of cap free
Amount of items: 2
Items: 
Size: 695298 Color: 18
Size: 304656 Color: 10

Bin 2246: 47 of cap free
Amount of items: 2
Items: 
Size: 740679 Color: 9
Size: 259275 Color: 13

Bin 2247: 47 of cap free
Amount of items: 2
Items: 
Size: 778098 Color: 9
Size: 221856 Color: 5

Bin 2248: 47 of cap free
Amount of items: 3
Items: 
Size: 660690 Color: 7
Size: 170914 Color: 9
Size: 168350 Color: 18

Bin 2249: 47 of cap free
Amount of items: 2
Items: 
Size: 797986 Color: 2
Size: 201968 Color: 3

Bin 2250: 47 of cap free
Amount of items: 2
Items: 
Size: 749586 Color: 1
Size: 250368 Color: 0

Bin 2251: 47 of cap free
Amount of items: 2
Items: 
Size: 500871 Color: 2
Size: 499083 Color: 17

Bin 2252: 47 of cap free
Amount of items: 2
Items: 
Size: 734932 Color: 10
Size: 265022 Color: 1

Bin 2253: 47 of cap free
Amount of items: 2
Items: 
Size: 572917 Color: 4
Size: 427037 Color: 14

Bin 2254: 47 of cap free
Amount of items: 3
Items: 
Size: 540993 Color: 3
Size: 260734 Color: 18
Size: 198227 Color: 1

Bin 2255: 47 of cap free
Amount of items: 2
Items: 
Size: 593898 Color: 5
Size: 406056 Color: 12

Bin 2256: 48 of cap free
Amount of items: 3
Items: 
Size: 703270 Color: 11
Size: 150802 Color: 5
Size: 145881 Color: 11

Bin 2257: 48 of cap free
Amount of items: 2
Items: 
Size: 701571 Color: 19
Size: 298382 Color: 11

Bin 2258: 48 of cap free
Amount of items: 3
Items: 
Size: 665212 Color: 8
Size: 171486 Color: 1
Size: 163255 Color: 13

Bin 2259: 48 of cap free
Amount of items: 2
Items: 
Size: 560228 Color: 3
Size: 439725 Color: 18

Bin 2260: 48 of cap free
Amount of items: 3
Items: 
Size: 755924 Color: 7
Size: 122461 Color: 18
Size: 121568 Color: 5

Bin 2261: 48 of cap free
Amount of items: 3
Items: 
Size: 344099 Color: 18
Size: 333724 Color: 19
Size: 322130 Color: 7

Bin 2262: 48 of cap free
Amount of items: 2
Items: 
Size: 507889 Color: 0
Size: 492064 Color: 11

Bin 2263: 48 of cap free
Amount of items: 2
Items: 
Size: 555194 Color: 3
Size: 444759 Color: 17

Bin 2264: 48 of cap free
Amount of items: 2
Items: 
Size: 565279 Color: 16
Size: 434674 Color: 7

Bin 2265: 48 of cap free
Amount of items: 2
Items: 
Size: 616212 Color: 9
Size: 383741 Color: 10

Bin 2266: 48 of cap free
Amount of items: 2
Items: 
Size: 698215 Color: 2
Size: 301738 Color: 11

Bin 2267: 48 of cap free
Amount of items: 2
Items: 
Size: 709657 Color: 0
Size: 290296 Color: 1

Bin 2268: 48 of cap free
Amount of items: 2
Items: 
Size: 720435 Color: 0
Size: 279518 Color: 15

Bin 2269: 48 of cap free
Amount of items: 2
Items: 
Size: 735928 Color: 0
Size: 264025 Color: 18

Bin 2270: 48 of cap free
Amount of items: 2
Items: 
Size: 567832 Color: 6
Size: 432121 Color: 18

Bin 2271: 48 of cap free
Amount of items: 2
Items: 
Size: 510388 Color: 2
Size: 489565 Color: 13

Bin 2272: 48 of cap free
Amount of items: 2
Items: 
Size: 514443 Color: 12
Size: 485510 Color: 18

Bin 2273: 48 of cap free
Amount of items: 2
Items: 
Size: 751447 Color: 1
Size: 248506 Color: 9

Bin 2274: 48 of cap free
Amount of items: 3
Items: 
Size: 558403 Color: 16
Size: 248221 Color: 5
Size: 193329 Color: 15

Bin 2275: 48 of cap free
Amount of items: 2
Items: 
Size: 789562 Color: 11
Size: 210391 Color: 8

Bin 2276: 49 of cap free
Amount of items: 3
Items: 
Size: 760110 Color: 3
Size: 120442 Color: 0
Size: 119400 Color: 3

Bin 2277: 49 of cap free
Amount of items: 2
Items: 
Size: 502428 Color: 14
Size: 497524 Color: 13

Bin 2278: 49 of cap free
Amount of items: 2
Items: 
Size: 537167 Color: 1
Size: 462785 Color: 16

Bin 2279: 49 of cap free
Amount of items: 2
Items: 
Size: 573433 Color: 7
Size: 426519 Color: 1

Bin 2280: 49 of cap free
Amount of items: 2
Items: 
Size: 611547 Color: 14
Size: 388405 Color: 6

Bin 2281: 49 of cap free
Amount of items: 2
Items: 
Size: 613253 Color: 3
Size: 386699 Color: 11

Bin 2282: 49 of cap free
Amount of items: 2
Items: 
Size: 656601 Color: 15
Size: 343351 Color: 7

Bin 2283: 49 of cap free
Amount of items: 2
Items: 
Size: 656682 Color: 8
Size: 343270 Color: 7

Bin 2284: 49 of cap free
Amount of items: 2
Items: 
Size: 733122 Color: 9
Size: 266830 Color: 12

Bin 2285: 49 of cap free
Amount of items: 2
Items: 
Size: 767544 Color: 11
Size: 232408 Color: 1

Bin 2286: 49 of cap free
Amount of items: 2
Items: 
Size: 780479 Color: 11
Size: 219473 Color: 19

Bin 2287: 49 of cap free
Amount of items: 2
Items: 
Size: 793682 Color: 15
Size: 206270 Color: 1

Bin 2288: 49 of cap free
Amount of items: 2
Items: 
Size: 577501 Color: 3
Size: 422451 Color: 16

Bin 2289: 49 of cap free
Amount of items: 2
Items: 
Size: 706333 Color: 2
Size: 293619 Color: 7

Bin 2290: 50 of cap free
Amount of items: 2
Items: 
Size: 714982 Color: 0
Size: 284969 Color: 13

Bin 2291: 50 of cap free
Amount of items: 2
Items: 
Size: 685525 Color: 16
Size: 314426 Color: 2

Bin 2292: 50 of cap free
Amount of items: 3
Items: 
Size: 406584 Color: 5
Size: 313213 Color: 2
Size: 280154 Color: 18

Bin 2293: 50 of cap free
Amount of items: 2
Items: 
Size: 509933 Color: 19
Size: 490018 Color: 16

Bin 2294: 50 of cap free
Amount of items: 2
Items: 
Size: 524555 Color: 7
Size: 475396 Color: 11

Bin 2295: 50 of cap free
Amount of items: 2
Items: 
Size: 525701 Color: 16
Size: 474250 Color: 4

Bin 2296: 50 of cap free
Amount of items: 2
Items: 
Size: 551578 Color: 16
Size: 448373 Color: 2

Bin 2297: 50 of cap free
Amount of items: 2
Items: 
Size: 553492 Color: 0
Size: 446459 Color: 3

Bin 2298: 50 of cap free
Amount of items: 2
Items: 
Size: 610560 Color: 19
Size: 389391 Color: 8

Bin 2299: 50 of cap free
Amount of items: 2
Items: 
Size: 619840 Color: 0
Size: 380111 Color: 18

Bin 2300: 50 of cap free
Amount of items: 2
Items: 
Size: 658361 Color: 17
Size: 341590 Color: 14

Bin 2301: 50 of cap free
Amount of items: 3
Items: 
Size: 675952 Color: 12
Size: 162030 Color: 18
Size: 161969 Color: 7

Bin 2302: 50 of cap free
Amount of items: 2
Items: 
Size: 686999 Color: 16
Size: 312952 Color: 2

Bin 2303: 50 of cap free
Amount of items: 2
Items: 
Size: 707004 Color: 2
Size: 292947 Color: 7

Bin 2304: 50 of cap free
Amount of items: 2
Items: 
Size: 732473 Color: 7
Size: 267478 Color: 16

Bin 2305: 50 of cap free
Amount of items: 2
Items: 
Size: 775750 Color: 11
Size: 224201 Color: 17

Bin 2306: 50 of cap free
Amount of items: 2
Items: 
Size: 769898 Color: 0
Size: 230053 Color: 16

Bin 2307: 50 of cap free
Amount of items: 3
Items: 
Size: 596656 Color: 10
Size: 203608 Color: 14
Size: 199687 Color: 3

Bin 2308: 50 of cap free
Amount of items: 3
Items: 
Size: 385179 Color: 9
Size: 334336 Color: 6
Size: 280436 Color: 1

Bin 2309: 51 of cap free
Amount of items: 2
Items: 
Size: 728291 Color: 5
Size: 271659 Color: 19

Bin 2310: 51 of cap free
Amount of items: 2
Items: 
Size: 504911 Color: 12
Size: 495039 Color: 13

Bin 2311: 51 of cap free
Amount of items: 2
Items: 
Size: 526928 Color: 2
Size: 473022 Color: 15

Bin 2312: 51 of cap free
Amount of items: 2
Items: 
Size: 550766 Color: 19
Size: 449184 Color: 9

Bin 2313: 51 of cap free
Amount of items: 2
Items: 
Size: 591222 Color: 9
Size: 408728 Color: 19

Bin 2314: 51 of cap free
Amount of items: 2
Items: 
Size: 592811 Color: 1
Size: 407139 Color: 13

Bin 2315: 51 of cap free
Amount of items: 2
Items: 
Size: 672368 Color: 19
Size: 327582 Color: 16

Bin 2316: 51 of cap free
Amount of items: 2
Items: 
Size: 681703 Color: 5
Size: 318247 Color: 2

Bin 2317: 51 of cap free
Amount of items: 2
Items: 
Size: 749741 Color: 8
Size: 250209 Color: 9

Bin 2318: 51 of cap free
Amount of items: 2
Items: 
Size: 750545 Color: 4
Size: 249405 Color: 5

Bin 2319: 51 of cap free
Amount of items: 2
Items: 
Size: 527765 Color: 10
Size: 472185 Color: 2

Bin 2320: 51 of cap free
Amount of items: 2
Items: 
Size: 770484 Color: 0
Size: 229466 Color: 16

Bin 2321: 51 of cap free
Amount of items: 3
Items: 
Size: 701317 Color: 18
Size: 149891 Color: 3
Size: 148742 Color: 2

Bin 2322: 51 of cap free
Amount of items: 2
Items: 
Size: 505474 Color: 16
Size: 494476 Color: 15

Bin 2323: 51 of cap free
Amount of items: 2
Items: 
Size: 789129 Color: 3
Size: 210821 Color: 12

Bin 2324: 51 of cap free
Amount of items: 2
Items: 
Size: 566214 Color: 14
Size: 433736 Color: 8

Bin 2325: 52 of cap free
Amount of items: 2
Items: 
Size: 691398 Color: 16
Size: 308551 Color: 7

Bin 2326: 52 of cap free
Amount of items: 2
Items: 
Size: 554770 Color: 18
Size: 445179 Color: 19

Bin 2327: 52 of cap free
Amount of items: 2
Items: 
Size: 688053 Color: 0
Size: 311896 Color: 4

Bin 2328: 52 of cap free
Amount of items: 2
Items: 
Size: 526563 Color: 16
Size: 473386 Color: 11

Bin 2329: 52 of cap free
Amount of items: 2
Items: 
Size: 549456 Color: 16
Size: 450493 Color: 14

Bin 2330: 52 of cap free
Amount of items: 2
Items: 
Size: 586033 Color: 1
Size: 413916 Color: 10

Bin 2331: 52 of cap free
Amount of items: 2
Items: 
Size: 595947 Color: 17
Size: 404002 Color: 11

Bin 2332: 52 of cap free
Amount of items: 2
Items: 
Size: 665050 Color: 5
Size: 334899 Color: 10

Bin 2333: 52 of cap free
Amount of items: 2
Items: 
Size: 709919 Color: 1
Size: 290030 Color: 10

Bin 2334: 52 of cap free
Amount of items: 2
Items: 
Size: 727660 Color: 8
Size: 272289 Color: 0

Bin 2335: 52 of cap free
Amount of items: 2
Items: 
Size: 744511 Color: 9
Size: 255438 Color: 16

Bin 2336: 52 of cap free
Amount of items: 2
Items: 
Size: 745003 Color: 6
Size: 254946 Color: 18

Bin 2337: 52 of cap free
Amount of items: 2
Items: 
Size: 739343 Color: 1
Size: 260606 Color: 5

Bin 2338: 52 of cap free
Amount of items: 2
Items: 
Size: 541054 Color: 12
Size: 458895 Color: 6

Bin 2339: 52 of cap free
Amount of items: 2
Items: 
Size: 611886 Color: 11
Size: 388063 Color: 17

Bin 2340: 52 of cap free
Amount of items: 2
Items: 
Size: 779037 Color: 8
Size: 220912 Color: 6

Bin 2341: 52 of cap free
Amount of items: 2
Items: 
Size: 762744 Color: 14
Size: 237205 Color: 19

Bin 2342: 53 of cap free
Amount of items: 2
Items: 
Size: 522352 Color: 11
Size: 477596 Color: 15

Bin 2343: 53 of cap free
Amount of items: 2
Items: 
Size: 529251 Color: 18
Size: 470697 Color: 6

Bin 2344: 53 of cap free
Amount of items: 2
Items: 
Size: 565536 Color: 16
Size: 434412 Color: 9

Bin 2345: 53 of cap free
Amount of items: 2
Items: 
Size: 571182 Color: 17
Size: 428766 Color: 18

Bin 2346: 53 of cap free
Amount of items: 2
Items: 
Size: 581103 Color: 6
Size: 418845 Color: 11

Bin 2347: 53 of cap free
Amount of items: 2
Items: 
Size: 605692 Color: 18
Size: 394256 Color: 12

Bin 2348: 53 of cap free
Amount of items: 2
Items: 
Size: 609102 Color: 15
Size: 390846 Color: 9

Bin 2349: 53 of cap free
Amount of items: 2
Items: 
Size: 616436 Color: 5
Size: 383512 Color: 13

Bin 2350: 53 of cap free
Amount of items: 2
Items: 
Size: 637641 Color: 15
Size: 362307 Color: 16

Bin 2351: 53 of cap free
Amount of items: 2
Items: 
Size: 648559 Color: 7
Size: 351389 Color: 9

Bin 2352: 53 of cap free
Amount of items: 2
Items: 
Size: 658292 Color: 6
Size: 341656 Color: 1

Bin 2353: 53 of cap free
Amount of items: 2
Items: 
Size: 680973 Color: 4
Size: 318975 Color: 13

Bin 2354: 53 of cap free
Amount of items: 2
Items: 
Size: 692902 Color: 10
Size: 307046 Color: 11

Bin 2355: 53 of cap free
Amount of items: 2
Items: 
Size: 709019 Color: 8
Size: 290929 Color: 7

Bin 2356: 53 of cap free
Amount of items: 2
Items: 
Size: 720348 Color: 0
Size: 279600 Color: 7

Bin 2357: 53 of cap free
Amount of items: 3
Items: 
Size: 636692 Color: 17
Size: 183706 Color: 7
Size: 179550 Color: 14

Bin 2358: 53 of cap free
Amount of items: 3
Items: 
Size: 649133 Color: 14
Size: 175833 Color: 6
Size: 174982 Color: 2

Bin 2359: 53 of cap free
Amount of items: 3
Items: 
Size: 703134 Color: 16
Size: 152598 Color: 3
Size: 144216 Color: 8

Bin 2360: 53 of cap free
Amount of items: 2
Items: 
Size: 593596 Color: 8
Size: 406352 Color: 12

Bin 2361: 53 of cap free
Amount of items: 3
Items: 
Size: 542496 Color: 12
Size: 262090 Color: 6
Size: 195362 Color: 15

Bin 2362: 53 of cap free
Amount of items: 2
Items: 
Size: 786259 Color: 1
Size: 213689 Color: 13

Bin 2363: 54 of cap free
Amount of items: 2
Items: 
Size: 514912 Color: 19
Size: 485035 Color: 17

Bin 2364: 54 of cap free
Amount of items: 2
Items: 
Size: 556336 Color: 0
Size: 443611 Color: 2

Bin 2365: 54 of cap free
Amount of items: 2
Items: 
Size: 508961 Color: 7
Size: 490986 Color: 6

Bin 2366: 54 of cap free
Amount of items: 2
Items: 
Size: 529055 Color: 1
Size: 470892 Color: 16

Bin 2367: 54 of cap free
Amount of items: 2
Items: 
Size: 567243 Color: 8
Size: 432704 Color: 5

Bin 2368: 54 of cap free
Amount of items: 2
Items: 
Size: 571164 Color: 18
Size: 428783 Color: 10

Bin 2369: 54 of cap free
Amount of items: 2
Items: 
Size: 581474 Color: 6
Size: 418473 Color: 16

Bin 2370: 54 of cap free
Amount of items: 2
Items: 
Size: 608837 Color: 15
Size: 391110 Color: 10

Bin 2371: 54 of cap free
Amount of items: 2
Items: 
Size: 616208 Color: 14
Size: 383739 Color: 9

Bin 2372: 54 of cap free
Amount of items: 2
Items: 
Size: 662801 Color: 1
Size: 337146 Color: 19

Bin 2373: 54 of cap free
Amount of items: 3
Items: 
Size: 689509 Color: 5
Size: 155565 Color: 4
Size: 154873 Color: 10

Bin 2374: 54 of cap free
Amount of items: 2
Items: 
Size: 690512 Color: 19
Size: 309435 Color: 7

Bin 2375: 54 of cap free
Amount of items: 2
Items: 
Size: 711064 Color: 1
Size: 288883 Color: 8

Bin 2376: 54 of cap free
Amount of items: 2
Items: 
Size: 768446 Color: 11
Size: 231501 Color: 16

Bin 2377: 54 of cap free
Amount of items: 2
Items: 
Size: 774004 Color: 6
Size: 225943 Color: 7

Bin 2378: 54 of cap free
Amount of items: 2
Items: 
Size: 601206 Color: 17
Size: 398741 Color: 10

Bin 2379: 54 of cap free
Amount of items: 3
Items: 
Size: 738198 Color: 17
Size: 130875 Color: 6
Size: 130874 Color: 14

Bin 2380: 54 of cap free
Amount of items: 2
Items: 
Size: 565454 Color: 14
Size: 434493 Color: 1

Bin 2381: 55 of cap free
Amount of items: 3
Items: 
Size: 756750 Color: 13
Size: 121747 Color: 18
Size: 121449 Color: 18

Bin 2382: 55 of cap free
Amount of items: 3
Items: 
Size: 416460 Color: 8
Size: 302062 Color: 4
Size: 281424 Color: 15

Bin 2383: 55 of cap free
Amount of items: 2
Items: 
Size: 661176 Color: 1
Size: 338770 Color: 10

Bin 2384: 55 of cap free
Amount of items: 2
Items: 
Size: 504755 Color: 14
Size: 495191 Color: 11

Bin 2385: 55 of cap free
Amount of items: 2
Items: 
Size: 551712 Color: 15
Size: 448234 Color: 13

Bin 2386: 55 of cap free
Amount of items: 2
Items: 
Size: 610861 Color: 3
Size: 389085 Color: 7

Bin 2387: 55 of cap free
Amount of items: 2
Items: 
Size: 629578 Color: 12
Size: 370368 Color: 4

Bin 2388: 55 of cap free
Amount of items: 2
Items: 
Size: 636030 Color: 18
Size: 363916 Color: 3

Bin 2389: 55 of cap free
Amount of items: 2
Items: 
Size: 679583 Color: 18
Size: 320363 Color: 15

Bin 2390: 55 of cap free
Amount of items: 2
Items: 
Size: 682997 Color: 14
Size: 316949 Color: 15

Bin 2391: 55 of cap free
Amount of items: 2
Items: 
Size: 774124 Color: 12
Size: 225822 Color: 19

Bin 2392: 55 of cap free
Amount of items: 2
Items: 
Size: 567710 Color: 0
Size: 432236 Color: 13

Bin 2393: 56 of cap free
Amount of items: 2
Items: 
Size: 670531 Color: 4
Size: 329414 Color: 2

Bin 2394: 56 of cap free
Amount of items: 2
Items: 
Size: 507012 Color: 7
Size: 492933 Color: 17

Bin 2395: 56 of cap free
Amount of items: 2
Items: 
Size: 509846 Color: 10
Size: 490099 Color: 12

Bin 2396: 56 of cap free
Amount of items: 2
Items: 
Size: 557185 Color: 13
Size: 442760 Color: 8

Bin 2397: 56 of cap free
Amount of items: 2
Items: 
Size: 578817 Color: 1
Size: 421128 Color: 15

Bin 2398: 56 of cap free
Amount of items: 2
Items: 
Size: 590496 Color: 5
Size: 409449 Color: 6

Bin 2399: 56 of cap free
Amount of items: 2
Items: 
Size: 590711 Color: 16
Size: 409234 Color: 3

Bin 2400: 56 of cap free
Amount of items: 2
Items: 
Size: 595193 Color: 5
Size: 404752 Color: 18

Bin 2401: 56 of cap free
Amount of items: 2
Items: 
Size: 595357 Color: 3
Size: 404588 Color: 13

Bin 2402: 56 of cap free
Amount of items: 2
Items: 
Size: 603002 Color: 7
Size: 396943 Color: 12

Bin 2403: 56 of cap free
Amount of items: 2
Items: 
Size: 620725 Color: 4
Size: 379220 Color: 16

Bin 2404: 56 of cap free
Amount of items: 2
Items: 
Size: 677330 Color: 7
Size: 322615 Color: 6

Bin 2405: 56 of cap free
Amount of items: 2
Items: 
Size: 675327 Color: 19
Size: 324618 Color: 0

Bin 2406: 56 of cap free
Amount of items: 2
Items: 
Size: 590807 Color: 9
Size: 409138 Color: 6

Bin 2407: 57 of cap free
Amount of items: 2
Items: 
Size: 791536 Color: 5
Size: 208408 Color: 12

Bin 2408: 57 of cap free
Amount of items: 2
Items: 
Size: 630486 Color: 10
Size: 369458 Color: 3

Bin 2409: 57 of cap free
Amount of items: 2
Items: 
Size: 797518 Color: 9
Size: 202426 Color: 16

Bin 2410: 57 of cap free
Amount of items: 3
Items: 
Size: 753841 Color: 7
Size: 126065 Color: 16
Size: 120038 Color: 17

Bin 2411: 57 of cap free
Amount of items: 2
Items: 
Size: 517189 Color: 6
Size: 482755 Color: 1

Bin 2412: 57 of cap free
Amount of items: 2
Items: 
Size: 571425 Color: 18
Size: 428519 Color: 1

Bin 2413: 57 of cap free
Amount of items: 2
Items: 
Size: 574994 Color: 8
Size: 424950 Color: 5

Bin 2414: 57 of cap free
Amount of items: 2
Items: 
Size: 586852 Color: 2
Size: 413092 Color: 13

Bin 2415: 57 of cap free
Amount of items: 2
Items: 
Size: 598420 Color: 9
Size: 401524 Color: 1

Bin 2416: 57 of cap free
Amount of items: 2
Items: 
Size: 610433 Color: 12
Size: 389511 Color: 3

Bin 2417: 57 of cap free
Amount of items: 2
Items: 
Size: 620705 Color: 11
Size: 379239 Color: 4

Bin 2418: 57 of cap free
Amount of items: 2
Items: 
Size: 692305 Color: 7
Size: 307639 Color: 9

Bin 2419: 57 of cap free
Amount of items: 2
Items: 
Size: 702231 Color: 13
Size: 297713 Color: 4

Bin 2420: 57 of cap free
Amount of items: 2
Items: 
Size: 677395 Color: 13
Size: 322549 Color: 4

Bin 2421: 57 of cap free
Amount of items: 2
Items: 
Size: 749883 Color: 17
Size: 250061 Color: 1

Bin 2422: 57 of cap free
Amount of items: 2
Items: 
Size: 564947 Color: 17
Size: 434997 Color: 7

Bin 2423: 57 of cap free
Amount of items: 2
Items: 
Size: 696517 Color: 1
Size: 303427 Color: 6

Bin 2424: 57 of cap free
Amount of items: 2
Items: 
Size: 683982 Color: 13
Size: 315962 Color: 8

Bin 2425: 57 of cap free
Amount of items: 2
Items: 
Size: 790419 Color: 16
Size: 209525 Color: 3

Bin 2426: 58 of cap free
Amount of items: 3
Items: 
Size: 657794 Color: 6
Size: 171092 Color: 18
Size: 171057 Color: 19

Bin 2427: 58 of cap free
Amount of items: 2
Items: 
Size: 523758 Color: 17
Size: 476185 Color: 12

Bin 2428: 58 of cap free
Amount of items: 2
Items: 
Size: 574050 Color: 0
Size: 425893 Color: 15

Bin 2429: 58 of cap free
Amount of items: 2
Items: 
Size: 603067 Color: 1
Size: 396876 Color: 8

Bin 2430: 58 of cap free
Amount of items: 2
Items: 
Size: 636799 Color: 17
Size: 363144 Color: 9

Bin 2431: 58 of cap free
Amount of items: 2
Items: 
Size: 646329 Color: 12
Size: 353614 Color: 14

Bin 2432: 58 of cap free
Amount of items: 2
Items: 
Size: 653951 Color: 2
Size: 345992 Color: 12

Bin 2433: 58 of cap free
Amount of items: 2
Items: 
Size: 758808 Color: 18
Size: 241135 Color: 14

Bin 2434: 58 of cap free
Amount of items: 3
Items: 
Size: 553011 Color: 13
Size: 251144 Color: 2
Size: 195788 Color: 2

Bin 2435: 58 of cap free
Amount of items: 2
Items: 
Size: 635537 Color: 10
Size: 364406 Color: 12

Bin 2436: 59 of cap free
Amount of items: 3
Items: 
Size: 691952 Color: 3
Size: 154182 Color: 19
Size: 153808 Color: 1

Bin 2437: 59 of cap free
Amount of items: 2
Items: 
Size: 660682 Color: 15
Size: 339260 Color: 2

Bin 2438: 59 of cap free
Amount of items: 2
Items: 
Size: 679237 Color: 9
Size: 320705 Color: 12

Bin 2439: 59 of cap free
Amount of items: 2
Items: 
Size: 576359 Color: 5
Size: 423583 Color: 12

Bin 2440: 59 of cap free
Amount of items: 2
Items: 
Size: 614936 Color: 3
Size: 385006 Color: 19

Bin 2441: 59 of cap free
Amount of items: 2
Items: 
Size: 615966 Color: 2
Size: 383976 Color: 9

Bin 2442: 59 of cap free
Amount of items: 2
Items: 
Size: 684055 Color: 12
Size: 315887 Color: 11

Bin 2443: 59 of cap free
Amount of items: 2
Items: 
Size: 695856 Color: 14
Size: 304086 Color: 4

Bin 2444: 59 of cap free
Amount of items: 2
Items: 
Size: 710662 Color: 9
Size: 289280 Color: 11

Bin 2445: 59 of cap free
Amount of items: 2
Items: 
Size: 735094 Color: 14
Size: 264848 Color: 17

Bin 2446: 59 of cap free
Amount of items: 2
Items: 
Size: 799789 Color: 16
Size: 200153 Color: 9

Bin 2447: 59 of cap free
Amount of items: 2
Items: 
Size: 615243 Color: 11
Size: 384699 Color: 3

Bin 2448: 59 of cap free
Amount of items: 3
Items: 
Size: 510303 Color: 10
Size: 258491 Color: 7
Size: 231148 Color: 9

Bin 2449: 60 of cap free
Amount of items: 2
Items: 
Size: 636501 Color: 8
Size: 363440 Color: 15

Bin 2450: 60 of cap free
Amount of items: 2
Items: 
Size: 662320 Color: 3
Size: 337621 Color: 7

Bin 2451: 60 of cap free
Amount of items: 2
Items: 
Size: 550526 Color: 15
Size: 449415 Color: 10

Bin 2452: 60 of cap free
Amount of items: 2
Items: 
Size: 581542 Color: 18
Size: 418399 Color: 7

Bin 2453: 60 of cap free
Amount of items: 2
Items: 
Size: 587754 Color: 1
Size: 412187 Color: 18

Bin 2454: 60 of cap free
Amount of items: 2
Items: 
Size: 609023 Color: 11
Size: 390918 Color: 5

Bin 2455: 60 of cap free
Amount of items: 3
Items: 
Size: 614306 Color: 11
Size: 196179 Color: 18
Size: 189456 Color: 14

Bin 2456: 60 of cap free
Amount of items: 2
Items: 
Size: 675010 Color: 2
Size: 324931 Color: 0

Bin 2457: 60 of cap free
Amount of items: 2
Items: 
Size: 697769 Color: 8
Size: 302172 Color: 17

Bin 2458: 60 of cap free
Amount of items: 2
Items: 
Size: 753160 Color: 8
Size: 246781 Color: 17

Bin 2459: 60 of cap free
Amount of items: 2
Items: 
Size: 756235 Color: 13
Size: 243706 Color: 10

Bin 2460: 60 of cap free
Amount of items: 2
Items: 
Size: 787397 Color: 1
Size: 212544 Color: 2

Bin 2461: 60 of cap free
Amount of items: 2
Items: 
Size: 789037 Color: 0
Size: 210904 Color: 17

Bin 2462: 60 of cap free
Amount of items: 3
Items: 
Size: 714722 Color: 13
Size: 142707 Color: 10
Size: 142512 Color: 3

Bin 2463: 60 of cap free
Amount of items: 2
Items: 
Size: 733446 Color: 0
Size: 266495 Color: 6

Bin 2464: 60 of cap free
Amount of items: 2
Items: 
Size: 714373 Color: 13
Size: 285568 Color: 5

Bin 2465: 60 of cap free
Amount of items: 2
Items: 
Size: 654743 Color: 13
Size: 345198 Color: 14

Bin 2466: 60 of cap free
Amount of items: 2
Items: 
Size: 773272 Color: 3
Size: 226669 Color: 16

Bin 2467: 60 of cap free
Amount of items: 2
Items: 
Size: 638767 Color: 11
Size: 361174 Color: 19

Bin 2468: 61 of cap free
Amount of items: 2
Items: 
Size: 721597 Color: 6
Size: 278343 Color: 16

Bin 2469: 61 of cap free
Amount of items: 2
Items: 
Size: 508958 Color: 16
Size: 490982 Color: 19

Bin 2470: 61 of cap free
Amount of items: 2
Items: 
Size: 528882 Color: 10
Size: 471058 Color: 9

Bin 2471: 61 of cap free
Amount of items: 2
Items: 
Size: 654186 Color: 12
Size: 345754 Color: 3

Bin 2472: 61 of cap free
Amount of items: 2
Items: 
Size: 674475 Color: 16
Size: 325465 Color: 15

Bin 2473: 61 of cap free
Amount of items: 2
Items: 
Size: 729090 Color: 11
Size: 270850 Color: 15

Bin 2474: 61 of cap free
Amount of items: 2
Items: 
Size: 780990 Color: 14
Size: 218950 Color: 0

Bin 2475: 61 of cap free
Amount of items: 2
Items: 
Size: 635834 Color: 2
Size: 364106 Color: 7

Bin 2476: 61 of cap free
Amount of items: 2
Items: 
Size: 744141 Color: 1
Size: 255799 Color: 15

Bin 2477: 62 of cap free
Amount of items: 3
Items: 
Size: 705621 Color: 4
Size: 149383 Color: 0
Size: 144935 Color: 0

Bin 2478: 62 of cap free
Amount of items: 2
Items: 
Size: 501616 Color: 7
Size: 498323 Color: 13

Bin 2479: 62 of cap free
Amount of items: 2
Items: 
Size: 521879 Color: 1
Size: 478060 Color: 18

Bin 2480: 62 of cap free
Amount of items: 2
Items: 
Size: 595504 Color: 16
Size: 404435 Color: 14

Bin 2481: 62 of cap free
Amount of items: 2
Items: 
Size: 602803 Color: 15
Size: 397136 Color: 12

Bin 2482: 62 of cap free
Amount of items: 2
Items: 
Size: 608481 Color: 18
Size: 391458 Color: 7

Bin 2483: 62 of cap free
Amount of items: 2
Items: 
Size: 619889 Color: 18
Size: 380050 Color: 9

Bin 2484: 62 of cap free
Amount of items: 2
Items: 
Size: 631791 Color: 12
Size: 368148 Color: 19

Bin 2485: 62 of cap free
Amount of items: 2
Items: 
Size: 633498 Color: 3
Size: 366441 Color: 6

Bin 2486: 62 of cap free
Amount of items: 2
Items: 
Size: 646607 Color: 12
Size: 353332 Color: 2

Bin 2487: 62 of cap free
Amount of items: 2
Items: 
Size: 655973 Color: 11
Size: 343966 Color: 3

Bin 2488: 62 of cap free
Amount of items: 2
Items: 
Size: 686277 Color: 8
Size: 313662 Color: 7

Bin 2489: 62 of cap free
Amount of items: 2
Items: 
Size: 772358 Color: 7
Size: 227581 Color: 15

Bin 2490: 62 of cap free
Amount of items: 2
Items: 
Size: 777479 Color: 6
Size: 222460 Color: 9

Bin 2491: 62 of cap free
Amount of items: 2
Items: 
Size: 799032 Color: 8
Size: 200907 Color: 0

Bin 2492: 62 of cap free
Amount of items: 2
Items: 
Size: 759510 Color: 8
Size: 240429 Color: 13

Bin 2493: 62 of cap free
Amount of items: 2
Items: 
Size: 619528 Color: 7
Size: 380411 Color: 6

Bin 2494: 62 of cap free
Amount of items: 2
Items: 
Size: 740329 Color: 9
Size: 259610 Color: 1

Bin 2495: 63 of cap free
Amount of items: 3
Items: 
Size: 681302 Color: 13
Size: 159639 Color: 7
Size: 158997 Color: 2

Bin 2496: 63 of cap free
Amount of items: 2
Items: 
Size: 504706 Color: 17
Size: 495232 Color: 14

Bin 2497: 63 of cap free
Amount of items: 2
Items: 
Size: 609795 Color: 2
Size: 390143 Color: 13

Bin 2498: 63 of cap free
Amount of items: 2
Items: 
Size: 633852 Color: 3
Size: 366086 Color: 15

Bin 2499: 63 of cap free
Amount of items: 2
Items: 
Size: 761468 Color: 5
Size: 238470 Color: 17

Bin 2500: 63 of cap free
Amount of items: 2
Items: 
Size: 785980 Color: 15
Size: 213958 Color: 19

Bin 2501: 63 of cap free
Amount of items: 2
Items: 
Size: 792467 Color: 11
Size: 207471 Color: 8

Bin 2502: 63 of cap free
Amount of items: 2
Items: 
Size: 577489 Color: 19
Size: 422449 Color: 13

Bin 2503: 64 of cap free
Amount of items: 2
Items: 
Size: 635616 Color: 15
Size: 364321 Color: 11

Bin 2504: 64 of cap free
Amount of items: 3
Items: 
Size: 370958 Color: 2
Size: 333041 Color: 0
Size: 295938 Color: 8

Bin 2505: 64 of cap free
Amount of items: 2
Items: 
Size: 523272 Color: 0
Size: 476665 Color: 13

Bin 2506: 64 of cap free
Amount of items: 2
Items: 
Size: 529527 Color: 18
Size: 470410 Color: 6

Bin 2507: 64 of cap free
Amount of items: 2
Items: 
Size: 558995 Color: 15
Size: 440942 Color: 8

Bin 2508: 64 of cap free
Amount of items: 2
Items: 
Size: 560019 Color: 1
Size: 439918 Color: 3

Bin 2509: 64 of cap free
Amount of items: 2
Items: 
Size: 603879 Color: 19
Size: 396058 Color: 14

Bin 2510: 64 of cap free
Amount of items: 2
Items: 
Size: 610856 Color: 6
Size: 389081 Color: 19

Bin 2511: 64 of cap free
Amount of items: 2
Items: 
Size: 701372 Color: 5
Size: 298565 Color: 18

Bin 2512: 64 of cap free
Amount of items: 2
Items: 
Size: 750238 Color: 4
Size: 249699 Color: 3

Bin 2513: 64 of cap free
Amount of items: 2
Items: 
Size: 658116 Color: 17
Size: 341821 Color: 8

Bin 2514: 64 of cap free
Amount of items: 2
Items: 
Size: 774624 Color: 1
Size: 225313 Color: 3

Bin 2515: 64 of cap free
Amount of items: 2
Items: 
Size: 667410 Color: 10
Size: 332527 Color: 15

Bin 2516: 65 of cap free
Amount of items: 3
Items: 
Size: 702611 Color: 1
Size: 150453 Color: 3
Size: 146872 Color: 19

Bin 2517: 65 of cap free
Amount of items: 2
Items: 
Size: 504906 Color: 7
Size: 495030 Color: 0

Bin 2518: 65 of cap free
Amount of items: 2
Items: 
Size: 551418 Color: 1
Size: 448518 Color: 5

Bin 2519: 65 of cap free
Amount of items: 2
Items: 
Size: 571634 Color: 13
Size: 428302 Color: 16

Bin 2520: 65 of cap free
Amount of items: 2
Items: 
Size: 621462 Color: 19
Size: 378474 Color: 1

Bin 2521: 65 of cap free
Amount of items: 2
Items: 
Size: 626749 Color: 6
Size: 373187 Color: 9

Bin 2522: 65 of cap free
Amount of items: 2
Items: 
Size: 652290 Color: 15
Size: 347646 Color: 3

Bin 2523: 65 of cap free
Amount of items: 2
Items: 
Size: 654868 Color: 9
Size: 345068 Color: 5

Bin 2524: 65 of cap free
Amount of items: 2
Items: 
Size: 690767 Color: 13
Size: 309169 Color: 11

Bin 2525: 65 of cap free
Amount of items: 2
Items: 
Size: 716992 Color: 8
Size: 282944 Color: 4

Bin 2526: 65 of cap free
Amount of items: 2
Items: 
Size: 736300 Color: 13
Size: 263636 Color: 8

Bin 2527: 65 of cap free
Amount of items: 2
Items: 
Size: 658443 Color: 3
Size: 341493 Color: 10

Bin 2528: 65 of cap free
Amount of items: 2
Items: 
Size: 799297 Color: 1
Size: 200639 Color: 16

Bin 2529: 65 of cap free
Amount of items: 2
Items: 
Size: 627169 Color: 13
Size: 372767 Color: 15

Bin 2530: 65 of cap free
Amount of items: 2
Items: 
Size: 675180 Color: 0
Size: 324756 Color: 3

Bin 2531: 65 of cap free
Amount of items: 3
Items: 
Size: 533932 Color: 2
Size: 242526 Color: 10
Size: 223478 Color: 4

Bin 2532: 66 of cap free
Amount of items: 2
Items: 
Size: 575076 Color: 12
Size: 424859 Color: 6

Bin 2533: 66 of cap free
Amount of items: 2
Items: 
Size: 531241 Color: 5
Size: 468694 Color: 8

Bin 2534: 66 of cap free
Amount of items: 2
Items: 
Size: 586978 Color: 0
Size: 412957 Color: 3

Bin 2535: 66 of cap free
Amount of items: 2
Items: 
Size: 595777 Color: 9
Size: 404158 Color: 6

Bin 2536: 66 of cap free
Amount of items: 2
Items: 
Size: 651181 Color: 4
Size: 348754 Color: 9

Bin 2537: 66 of cap free
Amount of items: 3
Items: 
Size: 720893 Color: 16
Size: 144135 Color: 3
Size: 134907 Color: 11

Bin 2538: 66 of cap free
Amount of items: 2
Items: 
Size: 729720 Color: 12
Size: 270215 Color: 5

Bin 2539: 66 of cap free
Amount of items: 2
Items: 
Size: 731111 Color: 0
Size: 268824 Color: 6

Bin 2540: 66 of cap free
Amount of items: 2
Items: 
Size: 758484 Color: 4
Size: 241451 Color: 12

Bin 2541: 66 of cap free
Amount of items: 2
Items: 
Size: 796173 Color: 12
Size: 203762 Color: 7

Bin 2542: 66 of cap free
Amount of items: 3
Items: 
Size: 661175 Color: 17
Size: 170735 Color: 16
Size: 168025 Color: 14

Bin 2543: 66 of cap free
Amount of items: 2
Items: 
Size: 791315 Color: 4
Size: 208620 Color: 1

Bin 2544: 66 of cap free
Amount of items: 2
Items: 
Size: 693113 Color: 0
Size: 306822 Color: 8

Bin 2545: 67 of cap free
Amount of items: 3
Items: 
Size: 604263 Color: 12
Size: 198481 Color: 11
Size: 197190 Color: 9

Bin 2546: 67 of cap free
Amount of items: 3
Items: 
Size: 771467 Color: 5
Size: 117536 Color: 7
Size: 110931 Color: 2

Bin 2547: 67 of cap free
Amount of items: 2
Items: 
Size: 616929 Color: 1
Size: 383005 Color: 12

Bin 2548: 67 of cap free
Amount of items: 2
Items: 
Size: 799296 Color: 3
Size: 200638 Color: 18

Bin 2549: 67 of cap free
Amount of items: 2
Items: 
Size: 578644 Color: 0
Size: 421290 Color: 12

Bin 2550: 67 of cap free
Amount of items: 2
Items: 
Size: 690572 Color: 8
Size: 309362 Color: 2

Bin 2551: 67 of cap free
Amount of items: 2
Items: 
Size: 706752 Color: 10
Size: 293182 Color: 17

Bin 2552: 67 of cap free
Amount of items: 2
Items: 
Size: 723610 Color: 1
Size: 276324 Color: 6

Bin 2553: 67 of cap free
Amount of items: 2
Items: 
Size: 750440 Color: 2
Size: 249494 Color: 3

Bin 2554: 67 of cap free
Amount of items: 2
Items: 
Size: 755228 Color: 15
Size: 244706 Color: 3

Bin 2555: 67 of cap free
Amount of items: 2
Items: 
Size: 796968 Color: 19
Size: 202966 Color: 0

Bin 2556: 67 of cap free
Amount of items: 2
Items: 
Size: 504629 Color: 2
Size: 495305 Color: 1

Bin 2557: 68 of cap free
Amount of items: 3
Items: 
Size: 748438 Color: 4
Size: 126675 Color: 14
Size: 124820 Color: 12

Bin 2558: 68 of cap free
Amount of items: 2
Items: 
Size: 519893 Color: 18
Size: 480040 Color: 12

Bin 2559: 68 of cap free
Amount of items: 3
Items: 
Size: 693814 Color: 15
Size: 153177 Color: 4
Size: 152942 Color: 3

Bin 2560: 68 of cap free
Amount of items: 2
Items: 
Size: 539067 Color: 9
Size: 460866 Color: 0

Bin 2561: 68 of cap free
Amount of items: 2
Items: 
Size: 572654 Color: 8
Size: 427279 Color: 3

Bin 2562: 68 of cap free
Amount of items: 2
Items: 
Size: 665590 Color: 6
Size: 334343 Color: 15

Bin 2563: 68 of cap free
Amount of items: 2
Items: 
Size: 674601 Color: 10
Size: 325332 Color: 14

Bin 2564: 68 of cap free
Amount of items: 2
Items: 
Size: 772130 Color: 9
Size: 227803 Color: 5

Bin 2565: 68 of cap free
Amount of items: 2
Items: 
Size: 773513 Color: 16
Size: 226420 Color: 14

Bin 2566: 68 of cap free
Amount of items: 3
Items: 
Size: 560409 Color: 3
Size: 247140 Color: 1
Size: 192384 Color: 0

Bin 2567: 69 of cap free
Amount of items: 2
Items: 
Size: 657301 Color: 11
Size: 342631 Color: 4

Bin 2568: 69 of cap free
Amount of items: 2
Items: 
Size: 590195 Color: 15
Size: 409737 Color: 4

Bin 2569: 69 of cap free
Amount of items: 2
Items: 
Size: 517031 Color: 12
Size: 482901 Color: 5

Bin 2570: 69 of cap free
Amount of items: 2
Items: 
Size: 548896 Color: 7
Size: 451036 Color: 6

Bin 2571: 69 of cap free
Amount of items: 2
Items: 
Size: 576074 Color: 10
Size: 423858 Color: 2

Bin 2572: 69 of cap free
Amount of items: 2
Items: 
Size: 598311 Color: 10
Size: 401621 Color: 5

Bin 2573: 69 of cap free
Amount of items: 2
Items: 
Size: 635759 Color: 4
Size: 364173 Color: 9

Bin 2574: 69 of cap free
Amount of items: 2
Items: 
Size: 707704 Color: 1
Size: 292228 Color: 13

Bin 2575: 69 of cap free
Amount of items: 2
Items: 
Size: 711733 Color: 7
Size: 288199 Color: 0

Bin 2576: 69 of cap free
Amount of items: 2
Items: 
Size: 764334 Color: 17
Size: 235598 Color: 10

Bin 2577: 69 of cap free
Amount of items: 2
Items: 
Size: 771996 Color: 16
Size: 227936 Color: 2

Bin 2578: 69 of cap free
Amount of items: 2
Items: 
Size: 781601 Color: 3
Size: 218331 Color: 13

Bin 2579: 69 of cap free
Amount of items: 3
Items: 
Size: 733439 Color: 19
Size: 134457 Color: 18
Size: 132036 Color: 7

Bin 2580: 69 of cap free
Amount of items: 2
Items: 
Size: 685592 Color: 11
Size: 314340 Color: 3

Bin 2581: 69 of cap free
Amount of items: 2
Items: 
Size: 520775 Color: 11
Size: 479157 Color: 14

Bin 2582: 69 of cap free
Amount of items: 2
Items: 
Size: 794939 Color: 8
Size: 204993 Color: 7

Bin 2583: 70 of cap free
Amount of items: 2
Items: 
Size: 503234 Color: 10
Size: 496697 Color: 8

Bin 2584: 70 of cap free
Amount of items: 2
Items: 
Size: 517103 Color: 18
Size: 482828 Color: 4

Bin 2585: 70 of cap free
Amount of items: 2
Items: 
Size: 529176 Color: 15
Size: 470755 Color: 9

Bin 2586: 70 of cap free
Amount of items: 2
Items: 
Size: 540541 Color: 15
Size: 459390 Color: 4

Bin 2587: 70 of cap free
Amount of items: 2
Items: 
Size: 545717 Color: 2
Size: 454214 Color: 15

Bin 2588: 70 of cap free
Amount of items: 2
Items: 
Size: 546845 Color: 1
Size: 453086 Color: 11

Bin 2589: 70 of cap free
Amount of items: 2
Items: 
Size: 559704 Color: 6
Size: 440227 Color: 14

Bin 2590: 70 of cap free
Amount of items: 2
Items: 
Size: 607170 Color: 13
Size: 392761 Color: 17

Bin 2591: 70 of cap free
Amount of items: 2
Items: 
Size: 623550 Color: 10
Size: 376381 Color: 11

Bin 2592: 70 of cap free
Amount of items: 2
Items: 
Size: 627733 Color: 8
Size: 372198 Color: 14

Bin 2593: 70 of cap free
Amount of items: 2
Items: 
Size: 742941 Color: 0
Size: 256990 Color: 12

Bin 2594: 70 of cap free
Amount of items: 2
Items: 
Size: 765671 Color: 10
Size: 234260 Color: 16

Bin 2595: 70 of cap free
Amount of items: 2
Items: 
Size: 796094 Color: 15
Size: 203837 Color: 19

Bin 2596: 70 of cap free
Amount of items: 2
Items: 
Size: 797227 Color: 0
Size: 202704 Color: 13

Bin 2597: 70 of cap free
Amount of items: 2
Items: 
Size: 680042 Color: 7
Size: 319889 Color: 11

Bin 2598: 70 of cap free
Amount of items: 3
Items: 
Size: 610551 Color: 10
Size: 198778 Color: 18
Size: 190602 Color: 1

Bin 2599: 70 of cap free
Amount of items: 2
Items: 
Size: 793515 Color: 13
Size: 206416 Color: 8

Bin 2600: 70 of cap free
Amount of items: 2
Items: 
Size: 737518 Color: 19
Size: 262413 Color: 17

Bin 2601: 70 of cap free
Amount of items: 3
Items: 
Size: 615530 Color: 14
Size: 194678 Color: 12
Size: 189723 Color: 6

Bin 2602: 70 of cap free
Amount of items: 2
Items: 
Size: 675359 Color: 0
Size: 324572 Color: 7

Bin 2603: 70 of cap free
Amount of items: 2
Items: 
Size: 772250 Color: 18
Size: 227681 Color: 17

Bin 2604: 71 of cap free
Amount of items: 2
Items: 
Size: 745072 Color: 1
Size: 254858 Color: 10

Bin 2605: 71 of cap free
Amount of items: 2
Items: 
Size: 502742 Color: 19
Size: 497188 Color: 7

Bin 2606: 71 of cap free
Amount of items: 2
Items: 
Size: 502882 Color: 9
Size: 497048 Color: 3

Bin 2607: 71 of cap free
Amount of items: 2
Items: 
Size: 513609 Color: 3
Size: 486321 Color: 6

Bin 2608: 71 of cap free
Amount of items: 2
Items: 
Size: 531116 Color: 19
Size: 468814 Color: 14

Bin 2609: 71 of cap free
Amount of items: 2
Items: 
Size: 540810 Color: 5
Size: 459120 Color: 6

Bin 2610: 71 of cap free
Amount of items: 2
Items: 
Size: 542043 Color: 15
Size: 457887 Color: 17

Bin 2611: 71 of cap free
Amount of items: 2
Items: 
Size: 548262 Color: 16
Size: 451668 Color: 0

Bin 2612: 71 of cap free
Amount of items: 2
Items: 
Size: 620812 Color: 12
Size: 379118 Color: 1

Bin 2613: 71 of cap free
Amount of items: 2
Items: 
Size: 675004 Color: 4
Size: 324926 Color: 6

Bin 2614: 71 of cap free
Amount of items: 2
Items: 
Size: 707005 Color: 7
Size: 292925 Color: 0

Bin 2615: 71 of cap free
Amount of items: 2
Items: 
Size: 746558 Color: 11
Size: 253372 Color: 3

Bin 2616: 71 of cap free
Amount of items: 2
Items: 
Size: 747627 Color: 16
Size: 252303 Color: 10

Bin 2617: 71 of cap free
Amount of items: 2
Items: 
Size: 679091 Color: 3
Size: 320839 Color: 13

Bin 2618: 71 of cap free
Amount of items: 2
Items: 
Size: 791162 Color: 0
Size: 208768 Color: 14

Bin 2619: 71 of cap free
Amount of items: 2
Items: 
Size: 502335 Color: 3
Size: 497595 Color: 2

Bin 2620: 71 of cap free
Amount of items: 2
Items: 
Size: 697370 Color: 4
Size: 302560 Color: 19

Bin 2621: 72 of cap free
Amount of items: 2
Items: 
Size: 525487 Color: 15
Size: 474442 Color: 17

Bin 2622: 72 of cap free
Amount of items: 2
Items: 
Size: 505312 Color: 1
Size: 494617 Color: 7

Bin 2623: 72 of cap free
Amount of items: 2
Items: 
Size: 538729 Color: 16
Size: 461200 Color: 5

Bin 2624: 72 of cap free
Amount of items: 2
Items: 
Size: 573900 Color: 11
Size: 426029 Color: 17

Bin 2625: 72 of cap free
Amount of items: 2
Items: 
Size: 596163 Color: 19
Size: 403766 Color: 18

Bin 2626: 72 of cap free
Amount of items: 2
Items: 
Size: 658985 Color: 7
Size: 340944 Color: 2

Bin 2627: 72 of cap free
Amount of items: 2
Items: 
Size: 721220 Color: 16
Size: 278709 Color: 4

Bin 2628: 72 of cap free
Amount of items: 2
Items: 
Size: 696430 Color: 14
Size: 303499 Color: 3

Bin 2629: 73 of cap free
Amount of items: 2
Items: 
Size: 688772 Color: 1
Size: 311156 Color: 13

Bin 2630: 73 of cap free
Amount of items: 2
Items: 
Size: 548362 Color: 12
Size: 451566 Color: 9

Bin 2631: 73 of cap free
Amount of items: 2
Items: 
Size: 595589 Color: 7
Size: 404339 Color: 15

Bin 2632: 73 of cap free
Amount of items: 2
Items: 
Size: 632875 Color: 7
Size: 367053 Color: 5

Bin 2633: 73 of cap free
Amount of items: 2
Items: 
Size: 663161 Color: 15
Size: 336767 Color: 7

Bin 2634: 73 of cap free
Amount of items: 2
Items: 
Size: 734316 Color: 2
Size: 265612 Color: 18

Bin 2635: 73 of cap free
Amount of items: 2
Items: 
Size: 763491 Color: 12
Size: 236437 Color: 19

Bin 2636: 73 of cap free
Amount of items: 2
Items: 
Size: 788632 Color: 13
Size: 211296 Color: 2

Bin 2637: 73 of cap free
Amount of items: 2
Items: 
Size: 769101 Color: 3
Size: 230827 Color: 8

Bin 2638: 73 of cap free
Amount of items: 2
Items: 
Size: 737839 Color: 16
Size: 262089 Color: 17

Bin 2639: 73 of cap free
Amount of items: 2
Items: 
Size: 646412 Color: 13
Size: 353516 Color: 9

Bin 2640: 74 of cap free
Amount of items: 3
Items: 
Size: 625618 Color: 10
Size: 187244 Color: 10
Size: 187065 Color: 14

Bin 2641: 74 of cap free
Amount of items: 2
Items: 
Size: 667521 Color: 10
Size: 332406 Color: 14

Bin 2642: 74 of cap free
Amount of items: 2
Items: 
Size: 558458 Color: 10
Size: 441469 Color: 19

Bin 2643: 74 of cap free
Amount of items: 2
Items: 
Size: 580304 Color: 3
Size: 419623 Color: 9

Bin 2644: 74 of cap free
Amount of items: 2
Items: 
Size: 623104 Color: 8
Size: 376823 Color: 7

Bin 2645: 74 of cap free
Amount of items: 2
Items: 
Size: 632993 Color: 10
Size: 366934 Color: 18

Bin 2646: 74 of cap free
Amount of items: 2
Items: 
Size: 637036 Color: 11
Size: 362891 Color: 7

Bin 2647: 74 of cap free
Amount of items: 2
Items: 
Size: 658281 Color: 4
Size: 341646 Color: 17

Bin 2648: 74 of cap free
Amount of items: 2
Items: 
Size: 726222 Color: 4
Size: 273705 Color: 11

Bin 2649: 74 of cap free
Amount of items: 2
Items: 
Size: 730190 Color: 1
Size: 269737 Color: 17

Bin 2650: 74 of cap free
Amount of items: 3
Items: 
Size: 729514 Color: 12
Size: 156235 Color: 19
Size: 114178 Color: 17

Bin 2651: 74 of cap free
Amount of items: 2
Items: 
Size: 545832 Color: 12
Size: 454095 Color: 4

Bin 2652: 74 of cap free
Amount of items: 2
Items: 
Size: 527229 Color: 18
Size: 472698 Color: 2

Bin 2653: 75 of cap free
Amount of items: 2
Items: 
Size: 583278 Color: 19
Size: 416648 Color: 3

Bin 2654: 75 of cap free
Amount of items: 2
Items: 
Size: 624764 Color: 1
Size: 375162 Color: 19

Bin 2655: 75 of cap free
Amount of items: 2
Items: 
Size: 755093 Color: 15
Size: 244833 Color: 19

Bin 2656: 75 of cap free
Amount of items: 2
Items: 
Size: 758015 Color: 17
Size: 241911 Color: 4

Bin 2657: 75 of cap free
Amount of items: 2
Items: 
Size: 780808 Color: 5
Size: 219118 Color: 8

Bin 2658: 75 of cap free
Amount of items: 2
Items: 
Size: 512725 Color: 11
Size: 487201 Color: 18

Bin 2659: 75 of cap free
Amount of items: 2
Items: 
Size: 795940 Color: 11
Size: 203986 Color: 5

Bin 2660: 76 of cap free
Amount of items: 2
Items: 
Size: 659422 Color: 10
Size: 340503 Color: 6

Bin 2661: 76 of cap free
Amount of items: 2
Items: 
Size: 606586 Color: 9
Size: 393339 Color: 18

Bin 2662: 76 of cap free
Amount of items: 2
Items: 
Size: 545056 Color: 2
Size: 454869 Color: 11

Bin 2663: 76 of cap free
Amount of items: 2
Items: 
Size: 566502 Color: 6
Size: 433423 Color: 5

Bin 2664: 76 of cap free
Amount of items: 2
Items: 
Size: 618080 Color: 16
Size: 381845 Color: 7

Bin 2665: 76 of cap free
Amount of items: 2
Items: 
Size: 673322 Color: 8
Size: 326603 Color: 15

Bin 2666: 76 of cap free
Amount of items: 2
Items: 
Size: 684769 Color: 1
Size: 315156 Color: 9

Bin 2667: 76 of cap free
Amount of items: 2
Items: 
Size: 738583 Color: 3
Size: 261342 Color: 12

Bin 2668: 76 of cap free
Amount of items: 2
Items: 
Size: 771670 Color: 19
Size: 228255 Color: 11

Bin 2669: 76 of cap free
Amount of items: 2
Items: 
Size: 545232 Color: 5
Size: 454693 Color: 1

Bin 2670: 77 of cap free
Amount of items: 2
Items: 
Size: 543950 Color: 1
Size: 455974 Color: 2

Bin 2671: 77 of cap free
Amount of items: 2
Items: 
Size: 561140 Color: 8
Size: 438784 Color: 0

Bin 2672: 77 of cap free
Amount of items: 2
Items: 
Size: 584783 Color: 13
Size: 415141 Color: 2

Bin 2673: 77 of cap free
Amount of items: 2
Items: 
Size: 612666 Color: 1
Size: 387258 Color: 9

Bin 2674: 77 of cap free
Amount of items: 2
Items: 
Size: 634129 Color: 15
Size: 365795 Color: 3

Bin 2675: 77 of cap free
Amount of items: 2
Items: 
Size: 665047 Color: 14
Size: 334877 Color: 11

Bin 2676: 77 of cap free
Amount of items: 2
Items: 
Size: 692794 Color: 6
Size: 307130 Color: 16

Bin 2677: 77 of cap free
Amount of items: 2
Items: 
Size: 760975 Color: 14
Size: 238949 Color: 18

Bin 2678: 77 of cap free
Amount of items: 2
Items: 
Size: 772344 Color: 13
Size: 227580 Color: 1

Bin 2679: 77 of cap free
Amount of items: 2
Items: 
Size: 727097 Color: 15
Size: 272827 Color: 19

Bin 2680: 77 of cap free
Amount of items: 2
Items: 
Size: 751185 Color: 17
Size: 248739 Color: 8

Bin 2681: 78 of cap free
Amount of items: 2
Items: 
Size: 515897 Color: 11
Size: 484026 Color: 12

Bin 2682: 78 of cap free
Amount of items: 2
Items: 
Size: 616628 Color: 12
Size: 383295 Color: 3

Bin 2683: 78 of cap free
Amount of items: 2
Items: 
Size: 724065 Color: 7
Size: 275858 Color: 4

Bin 2684: 78 of cap free
Amount of items: 2
Items: 
Size: 749323 Color: 11
Size: 250600 Color: 2

Bin 2685: 78 of cap free
Amount of items: 2
Items: 
Size: 796638 Color: 1
Size: 203285 Color: 13

Bin 2686: 78 of cap free
Amount of items: 2
Items: 
Size: 603681 Color: 8
Size: 396242 Color: 7

Bin 2687: 79 of cap free
Amount of items: 2
Items: 
Size: 536432 Color: 9
Size: 463490 Color: 19

Bin 2688: 79 of cap free
Amount of items: 2
Items: 
Size: 569198 Color: 4
Size: 430724 Color: 6

Bin 2689: 79 of cap free
Amount of items: 2
Items: 
Size: 597489 Color: 6
Size: 402433 Color: 1

Bin 2690: 79 of cap free
Amount of items: 2
Items: 
Size: 617687 Color: 8
Size: 382235 Color: 0

Bin 2691: 79 of cap free
Amount of items: 2
Items: 
Size: 706158 Color: 3
Size: 293764 Color: 13

Bin 2692: 79 of cap free
Amount of items: 2
Items: 
Size: 713529 Color: 6
Size: 286393 Color: 18

Bin 2693: 79 of cap free
Amount of items: 2
Items: 
Size: 753079 Color: 19
Size: 246843 Color: 6

Bin 2694: 79 of cap free
Amount of items: 2
Items: 
Size: 780986 Color: 9
Size: 218936 Color: 6

Bin 2695: 79 of cap free
Amount of items: 3
Items: 
Size: 561458 Color: 5
Size: 245737 Color: 16
Size: 192727 Color: 4

Bin 2696: 79 of cap free
Amount of items: 2
Items: 
Size: 779311 Color: 3
Size: 220611 Color: 12

Bin 2697: 80 of cap free
Amount of items: 2
Items: 
Size: 654552 Color: 0
Size: 345369 Color: 16

Bin 2698: 80 of cap free
Amount of items: 2
Items: 
Size: 679747 Color: 10
Size: 320174 Color: 3

Bin 2699: 80 of cap free
Amount of items: 2
Items: 
Size: 506041 Color: 1
Size: 493880 Color: 6

Bin 2700: 80 of cap free
Amount of items: 2
Items: 
Size: 517559 Color: 5
Size: 482362 Color: 0

Bin 2701: 80 of cap free
Amount of items: 2
Items: 
Size: 565912 Color: 5
Size: 434009 Color: 14

Bin 2702: 80 of cap free
Amount of items: 2
Items: 
Size: 593746 Color: 3
Size: 406175 Color: 8

Bin 2703: 80 of cap free
Amount of items: 2
Items: 
Size: 774857 Color: 5
Size: 225064 Color: 12

Bin 2704: 80 of cap free
Amount of items: 2
Items: 
Size: 746096 Color: 16
Size: 253825 Color: 8

Bin 2705: 81 of cap free
Amount of items: 2
Items: 
Size: 770480 Color: 14
Size: 229440 Color: 13

Bin 2706: 81 of cap free
Amount of items: 2
Items: 
Size: 612096 Color: 2
Size: 387824 Color: 1

Bin 2707: 81 of cap free
Amount of items: 2
Items: 
Size: 653689 Color: 7
Size: 346231 Color: 10

Bin 2708: 81 of cap free
Amount of items: 2
Items: 
Size: 710924 Color: 18
Size: 288996 Color: 17

Bin 2709: 81 of cap free
Amount of items: 2
Items: 
Size: 715082 Color: 19
Size: 284838 Color: 7

Bin 2710: 81 of cap free
Amount of items: 2
Items: 
Size: 756403 Color: 16
Size: 243517 Color: 10

Bin 2711: 81 of cap free
Amount of items: 2
Items: 
Size: 504623 Color: 3
Size: 495297 Color: 15

Bin 2712: 81 of cap free
Amount of items: 3
Items: 
Size: 386799 Color: 4
Size: 334603 Color: 12
Size: 278518 Color: 11

Bin 2713: 81 of cap free
Amount of items: 2
Items: 
Size: 519576 Color: 19
Size: 480344 Color: 4

Bin 2714: 82 of cap free
Amount of items: 2
Items: 
Size: 695960 Color: 2
Size: 303959 Color: 11

Bin 2715: 82 of cap free
Amount of items: 2
Items: 
Size: 696424 Color: 6
Size: 303495 Color: 16

Bin 2716: 82 of cap free
Amount of items: 2
Items: 
Size: 529801 Color: 14
Size: 470118 Color: 17

Bin 2717: 82 of cap free
Amount of items: 2
Items: 
Size: 536530 Color: 8
Size: 463389 Color: 12

Bin 2718: 82 of cap free
Amount of items: 2
Items: 
Size: 588929 Color: 16
Size: 410990 Color: 19

Bin 2719: 82 of cap free
Amount of items: 2
Items: 
Size: 623312 Color: 7
Size: 376607 Color: 3

Bin 2720: 82 of cap free
Amount of items: 2
Items: 
Size: 687207 Color: 15
Size: 312712 Color: 19

Bin 2721: 82 of cap free
Amount of items: 2
Items: 
Size: 693573 Color: 13
Size: 306346 Color: 2

Bin 2722: 82 of cap free
Amount of items: 2
Items: 
Size: 740535 Color: 19
Size: 259384 Color: 13

Bin 2723: 82 of cap free
Amount of items: 3
Items: 
Size: 343492 Color: 5
Size: 329275 Color: 10
Size: 327152 Color: 1

Bin 2724: 83 of cap free
Amount of items: 3
Items: 
Size: 764136 Color: 7
Size: 120910 Color: 8
Size: 114872 Color: 0

Bin 2725: 83 of cap free
Amount of items: 3
Items: 
Size: 685980 Color: 10
Size: 157505 Color: 17
Size: 156433 Color: 0

Bin 2726: 83 of cap free
Amount of items: 2
Items: 
Size: 526183 Color: 9
Size: 473735 Color: 3

Bin 2727: 83 of cap free
Amount of items: 2
Items: 
Size: 591406 Color: 1
Size: 408512 Color: 11

Bin 2728: 83 of cap free
Amount of items: 2
Items: 
Size: 597977 Color: 11
Size: 401941 Color: 6

Bin 2729: 83 of cap free
Amount of items: 2
Items: 
Size: 621575 Color: 14
Size: 378343 Color: 4

Bin 2730: 83 of cap free
Amount of items: 3
Items: 
Size: 705394 Color: 8
Size: 148034 Color: 1
Size: 146490 Color: 14

Bin 2731: 83 of cap free
Amount of items: 2
Items: 
Size: 708495 Color: 1
Size: 291423 Color: 10

Bin 2732: 83 of cap free
Amount of items: 2
Items: 
Size: 759752 Color: 7
Size: 240166 Color: 13

Bin 2733: 83 of cap free
Amount of items: 2
Items: 
Size: 773510 Color: 10
Size: 226408 Color: 11

Bin 2734: 84 of cap free
Amount of items: 2
Items: 
Size: 515988 Color: 7
Size: 483929 Color: 17

Bin 2735: 84 of cap free
Amount of items: 2
Items: 
Size: 593278 Color: 16
Size: 406639 Color: 12

Bin 2736: 84 of cap free
Amount of items: 2
Items: 
Size: 783011 Color: 3
Size: 216906 Color: 4

Bin 2737: 84 of cap free
Amount of items: 2
Items: 
Size: 632723 Color: 1
Size: 367194 Color: 6

Bin 2738: 84 of cap free
Amount of items: 2
Items: 
Size: 545977 Color: 5
Size: 453940 Color: 16

Bin 2739: 84 of cap free
Amount of items: 2
Items: 
Size: 754091 Color: 11
Size: 245826 Color: 17

Bin 2740: 84 of cap free
Amount of items: 2
Items: 
Size: 647640 Color: 2
Size: 352277 Color: 1

Bin 2741: 84 of cap free
Amount of items: 2
Items: 
Size: 519228 Color: 7
Size: 480689 Color: 14

Bin 2742: 85 of cap free
Amount of items: 2
Items: 
Size: 734029 Color: 0
Size: 265887 Color: 12

Bin 2743: 85 of cap free
Amount of items: 2
Items: 
Size: 602415 Color: 1
Size: 397501 Color: 12

Bin 2744: 85 of cap free
Amount of items: 2
Items: 
Size: 525335 Color: 13
Size: 474581 Color: 9

Bin 2745: 85 of cap free
Amount of items: 2
Items: 
Size: 545066 Color: 11
Size: 454850 Color: 13

Bin 2746: 85 of cap free
Amount of items: 2
Items: 
Size: 575839 Color: 10
Size: 424077 Color: 8

Bin 2747: 85 of cap free
Amount of items: 2
Items: 
Size: 582303 Color: 12
Size: 417613 Color: 19

Bin 2748: 85 of cap free
Amount of items: 2
Items: 
Size: 615373 Color: 7
Size: 384543 Color: 1

Bin 2749: 85 of cap free
Amount of items: 2
Items: 
Size: 674007 Color: 15
Size: 325909 Color: 2

Bin 2750: 85 of cap free
Amount of items: 3
Items: 
Size: 739840 Color: 2
Size: 130046 Color: 15
Size: 130030 Color: 7

Bin 2751: 85 of cap free
Amount of items: 3
Items: 
Size: 782768 Color: 17
Size: 108707 Color: 17
Size: 108441 Color: 4

Bin 2752: 85 of cap free
Amount of items: 2
Items: 
Size: 553366 Color: 14
Size: 446550 Color: 9

Bin 2753: 85 of cap free
Amount of items: 2
Items: 
Size: 621217 Color: 3
Size: 378699 Color: 18

Bin 2754: 86 of cap free
Amount of items: 2
Items: 
Size: 521461 Color: 2
Size: 478454 Color: 4

Bin 2755: 86 of cap free
Amount of items: 2
Items: 
Size: 510507 Color: 2
Size: 489408 Color: 12

Bin 2756: 86 of cap free
Amount of items: 2
Items: 
Size: 590798 Color: 17
Size: 409117 Color: 7

Bin 2757: 86 of cap free
Amount of items: 2
Items: 
Size: 596408 Color: 3
Size: 403507 Color: 2

Bin 2758: 86 of cap free
Amount of items: 2
Items: 
Size: 505694 Color: 14
Size: 494221 Color: 9

Bin 2759: 86 of cap free
Amount of items: 2
Items: 
Size: 525475 Color: 5
Size: 474440 Color: 10

Bin 2760: 86 of cap free
Amount of items: 2
Items: 
Size: 542264 Color: 17
Size: 457651 Color: 16

Bin 2761: 86 of cap free
Amount of items: 2
Items: 
Size: 562628 Color: 16
Size: 437287 Color: 15

Bin 2762: 86 of cap free
Amount of items: 2
Items: 
Size: 573246 Color: 4
Size: 426669 Color: 10

Bin 2763: 86 of cap free
Amount of items: 2
Items: 
Size: 671382 Color: 12
Size: 328533 Color: 1

Bin 2764: 86 of cap free
Amount of items: 2
Items: 
Size: 747187 Color: 11
Size: 252728 Color: 19

Bin 2765: 86 of cap free
Amount of items: 2
Items: 
Size: 567796 Color: 7
Size: 432119 Color: 1

Bin 2766: 86 of cap free
Amount of items: 3
Items: 
Size: 555545 Color: 17
Size: 250794 Color: 7
Size: 193576 Color: 19

Bin 2767: 86 of cap free
Amount of items: 2
Items: 
Size: 732022 Color: 9
Size: 267893 Color: 5

Bin 2768: 86 of cap free
Amount of items: 2
Items: 
Size: 631222 Color: 17
Size: 368693 Color: 19

Bin 2769: 86 of cap free
Amount of items: 2
Items: 
Size: 563856 Color: 13
Size: 436059 Color: 19

Bin 2770: 87 of cap free
Amount of items: 2
Items: 
Size: 546164 Color: 16
Size: 453750 Color: 11

Bin 2771: 87 of cap free
Amount of items: 2
Items: 
Size: 562136 Color: 17
Size: 437778 Color: 9

Bin 2772: 87 of cap free
Amount of items: 2
Items: 
Size: 589015 Color: 13
Size: 410899 Color: 0

Bin 2773: 87 of cap free
Amount of items: 2
Items: 
Size: 616329 Color: 11
Size: 383585 Color: 8

Bin 2774: 87 of cap free
Amount of items: 2
Items: 
Size: 640731 Color: 19
Size: 359183 Color: 11

Bin 2775: 87 of cap free
Amount of items: 2
Items: 
Size: 643243 Color: 6
Size: 356671 Color: 5

Bin 2776: 87 of cap free
Amount of items: 2
Items: 
Size: 709817 Color: 2
Size: 290097 Color: 15

Bin 2777: 87 of cap free
Amount of items: 2
Items: 
Size: 743858 Color: 10
Size: 256056 Color: 7

Bin 2778: 87 of cap free
Amount of items: 2
Items: 
Size: 757080 Color: 15
Size: 242834 Color: 6

Bin 2779: 87 of cap free
Amount of items: 2
Items: 
Size: 787479 Color: 15
Size: 212435 Color: 12

Bin 2780: 88 of cap free
Amount of items: 3
Items: 
Size: 619137 Color: 16
Size: 192053 Color: 6
Size: 188723 Color: 12

Bin 2781: 88 of cap free
Amount of items: 2
Items: 
Size: 790189 Color: 12
Size: 209724 Color: 15

Bin 2782: 88 of cap free
Amount of items: 2
Items: 
Size: 518213 Color: 14
Size: 481700 Color: 8

Bin 2783: 88 of cap free
Amount of items: 2
Items: 
Size: 547511 Color: 18
Size: 452402 Color: 4

Bin 2784: 88 of cap free
Amount of items: 2
Items: 
Size: 569503 Color: 0
Size: 430410 Color: 4

Bin 2785: 88 of cap free
Amount of items: 2
Items: 
Size: 648616 Color: 9
Size: 351297 Color: 6

Bin 2786: 88 of cap free
Amount of items: 2
Items: 
Size: 688474 Color: 4
Size: 311439 Color: 6

Bin 2787: 88 of cap free
Amount of items: 2
Items: 
Size: 761266 Color: 5
Size: 238647 Color: 4

Bin 2788: 88 of cap free
Amount of items: 2
Items: 
Size: 770860 Color: 6
Size: 229053 Color: 3

Bin 2789: 88 of cap free
Amount of items: 2
Items: 
Size: 634480 Color: 10
Size: 365433 Color: 11

Bin 2790: 88 of cap free
Amount of items: 2
Items: 
Size: 756512 Color: 8
Size: 243401 Color: 15

Bin 2791: 89 of cap free
Amount of items: 2
Items: 
Size: 784440 Color: 19
Size: 215472 Color: 5

Bin 2792: 89 of cap free
Amount of items: 2
Items: 
Size: 514992 Color: 16
Size: 484920 Color: 3

Bin 2793: 89 of cap free
Amount of items: 2
Items: 
Size: 542841 Color: 13
Size: 457071 Color: 3

Bin 2794: 89 of cap free
Amount of items: 2
Items: 
Size: 594704 Color: 9
Size: 405208 Color: 8

Bin 2795: 89 of cap free
Amount of items: 2
Items: 
Size: 668730 Color: 14
Size: 331182 Color: 8

Bin 2796: 89 of cap free
Amount of items: 2
Items: 
Size: 706512 Color: 19
Size: 293400 Color: 16

Bin 2797: 89 of cap free
Amount of items: 2
Items: 
Size: 768993 Color: 4
Size: 230919 Color: 16

Bin 2798: 89 of cap free
Amount of items: 2
Items: 
Size: 797647 Color: 18
Size: 202265 Color: 4

Bin 2799: 90 of cap free
Amount of items: 3
Items: 
Size: 718502 Color: 3
Size: 145147 Color: 7
Size: 136262 Color: 11

Bin 2800: 90 of cap free
Amount of items: 3
Items: 
Size: 659902 Color: 6
Size: 170089 Color: 5
Size: 169920 Color: 18

Bin 2801: 90 of cap free
Amount of items: 2
Items: 
Size: 502088 Color: 19
Size: 497823 Color: 9

Bin 2802: 90 of cap free
Amount of items: 2
Items: 
Size: 672593 Color: 9
Size: 327318 Color: 19

Bin 2803: 90 of cap free
Amount of items: 2
Items: 
Size: 679446 Color: 5
Size: 320465 Color: 7

Bin 2804: 90 of cap free
Amount of items: 2
Items: 
Size: 556080 Color: 1
Size: 443831 Color: 17

Bin 2805: 90 of cap free
Amount of items: 2
Items: 
Size: 528687 Color: 7
Size: 471224 Color: 4

Bin 2806: 91 of cap free
Amount of items: 2
Items: 
Size: 588069 Color: 18
Size: 411841 Color: 12

Bin 2807: 91 of cap free
Amount of items: 2
Items: 
Size: 506588 Color: 12
Size: 493322 Color: 19

Bin 2808: 91 of cap free
Amount of items: 2
Items: 
Size: 511710 Color: 17
Size: 488200 Color: 14

Bin 2809: 91 of cap free
Amount of items: 2
Items: 
Size: 516442 Color: 11
Size: 483468 Color: 4

Bin 2810: 91 of cap free
Amount of items: 2
Items: 
Size: 527587 Color: 9
Size: 472323 Color: 2

Bin 2811: 91 of cap free
Amount of items: 2
Items: 
Size: 609776 Color: 19
Size: 390134 Color: 6

Bin 2812: 91 of cap free
Amount of items: 2
Items: 
Size: 633483 Color: 3
Size: 366427 Color: 6

Bin 2813: 91 of cap free
Amount of items: 2
Items: 
Size: 645260 Color: 18
Size: 354650 Color: 8

Bin 2814: 91 of cap free
Amount of items: 2
Items: 
Size: 654328 Color: 10
Size: 345582 Color: 13

Bin 2815: 91 of cap free
Amount of items: 2
Items: 
Size: 663560 Color: 18
Size: 336350 Color: 2

Bin 2816: 91 of cap free
Amount of items: 3
Items: 
Size: 670134 Color: 14
Size: 166630 Color: 3
Size: 163146 Color: 11

Bin 2817: 91 of cap free
Amount of items: 2
Items: 
Size: 684208 Color: 6
Size: 315702 Color: 15

Bin 2818: 91 of cap free
Amount of items: 2
Items: 
Size: 521230 Color: 13
Size: 478680 Color: 17

Bin 2819: 91 of cap free
Amount of items: 3
Items: 
Size: 423378 Color: 15
Size: 301390 Color: 17
Size: 275142 Color: 18

Bin 2820: 91 of cap free
Amount of items: 2
Items: 
Size: 623818 Color: 3
Size: 376092 Color: 7

Bin 2821: 92 of cap free
Amount of items: 2
Items: 
Size: 502322 Color: 10
Size: 497587 Color: 9

Bin 2822: 92 of cap free
Amount of items: 2
Items: 
Size: 507433 Color: 3
Size: 492476 Color: 19

Bin 2823: 92 of cap free
Amount of items: 2
Items: 
Size: 559582 Color: 3
Size: 440327 Color: 14

Bin 2824: 92 of cap free
Amount of items: 2
Items: 
Size: 617906 Color: 16
Size: 382003 Color: 4

Bin 2825: 92 of cap free
Amount of items: 3
Items: 
Size: 652692 Color: 17
Size: 173706 Color: 16
Size: 173511 Color: 1

Bin 2826: 92 of cap free
Amount of items: 2
Items: 
Size: 652854 Color: 13
Size: 347055 Color: 14

Bin 2827: 92 of cap free
Amount of items: 2
Items: 
Size: 748130 Color: 16
Size: 251779 Color: 4

Bin 2828: 92 of cap free
Amount of items: 2
Items: 
Size: 762003 Color: 14
Size: 237906 Color: 16

Bin 2829: 92 of cap free
Amount of items: 2
Items: 
Size: 554840 Color: 13
Size: 445069 Color: 7

Bin 2830: 92 of cap free
Amount of items: 2
Items: 
Size: 750174 Color: 8
Size: 249735 Color: 4

Bin 2831: 92 of cap free
Amount of items: 2
Items: 
Size: 714692 Color: 1
Size: 285217 Color: 7

Bin 2832: 92 of cap free
Amount of items: 2
Items: 
Size: 773778 Color: 12
Size: 226131 Color: 18

Bin 2833: 93 of cap free
Amount of items: 2
Items: 
Size: 789713 Color: 18
Size: 210195 Color: 8

Bin 2834: 93 of cap free
Amount of items: 2
Items: 
Size: 506213 Color: 1
Size: 493695 Color: 3

Bin 2835: 93 of cap free
Amount of items: 2
Items: 
Size: 599320 Color: 12
Size: 400588 Color: 9

Bin 2836: 93 of cap free
Amount of items: 2
Items: 
Size: 608144 Color: 7
Size: 391764 Color: 2

Bin 2837: 93 of cap free
Amount of items: 2
Items: 
Size: 637789 Color: 10
Size: 362119 Color: 16

Bin 2838: 93 of cap free
Amount of items: 2
Items: 
Size: 503373 Color: 8
Size: 496535 Color: 16

Bin 2839: 94 of cap free
Amount of items: 2
Items: 
Size: 715733 Color: 16
Size: 284174 Color: 18

Bin 2840: 94 of cap free
Amount of items: 2
Items: 
Size: 613109 Color: 13
Size: 386798 Color: 7

Bin 2841: 94 of cap free
Amount of items: 2
Items: 
Size: 508685 Color: 0
Size: 491222 Color: 10

Bin 2842: 94 of cap free
Amount of items: 2
Items: 
Size: 535321 Color: 10
Size: 464586 Color: 18

Bin 2843: 94 of cap free
Amount of items: 2
Items: 
Size: 542473 Color: 19
Size: 457434 Color: 10

Bin 2844: 94 of cap free
Amount of items: 2
Items: 
Size: 604143 Color: 1
Size: 395764 Color: 17

Bin 2845: 94 of cap free
Amount of items: 2
Items: 
Size: 610012 Color: 12
Size: 389895 Color: 3

Bin 2846: 94 of cap free
Amount of items: 2
Items: 
Size: 636787 Color: 8
Size: 363120 Color: 15

Bin 2847: 94 of cap free
Amount of items: 2
Items: 
Size: 679743 Color: 5
Size: 320164 Color: 6

Bin 2848: 94 of cap free
Amount of items: 2
Items: 
Size: 761978 Color: 12
Size: 237929 Color: 14

Bin 2849: 94 of cap free
Amount of items: 2
Items: 
Size: 786980 Color: 19
Size: 212927 Color: 5

Bin 2850: 94 of cap free
Amount of items: 3
Items: 
Size: 385178 Color: 2
Size: 330276 Color: 2
Size: 284453 Color: 7

Bin 2851: 95 of cap free
Amount of items: 2
Items: 
Size: 516141 Color: 3
Size: 483765 Color: 0

Bin 2852: 95 of cap free
Amount of items: 2
Items: 
Size: 533137 Color: 1
Size: 466769 Color: 14

Bin 2853: 95 of cap free
Amount of items: 2
Items: 
Size: 537543 Color: 19
Size: 462363 Color: 6

Bin 2854: 95 of cap free
Amount of items: 2
Items: 
Size: 556174 Color: 7
Size: 443732 Color: 8

Bin 2855: 95 of cap free
Amount of items: 2
Items: 
Size: 574651 Color: 8
Size: 425255 Color: 9

Bin 2856: 95 of cap free
Amount of items: 2
Items: 
Size: 580304 Color: 13
Size: 419602 Color: 4

Bin 2857: 95 of cap free
Amount of items: 2
Items: 
Size: 587740 Color: 2
Size: 412166 Color: 0

Bin 2858: 95 of cap free
Amount of items: 2
Items: 
Size: 625848 Color: 1
Size: 374058 Color: 17

Bin 2859: 95 of cap free
Amount of items: 2
Items: 
Size: 521070 Color: 18
Size: 478836 Color: 13

Bin 2860: 96 of cap free
Amount of items: 2
Items: 
Size: 719559 Color: 1
Size: 280346 Color: 6

Bin 2861: 96 of cap free
Amount of items: 2
Items: 
Size: 565416 Color: 16
Size: 434489 Color: 0

Bin 2862: 96 of cap free
Amount of items: 2
Items: 
Size: 606402 Color: 14
Size: 393503 Color: 8

Bin 2863: 96 of cap free
Amount of items: 2
Items: 
Size: 607698 Color: 0
Size: 392207 Color: 10

Bin 2864: 96 of cap free
Amount of items: 2
Items: 
Size: 683254 Color: 15
Size: 316651 Color: 18

Bin 2865: 96 of cap free
Amount of items: 2
Items: 
Size: 728162 Color: 17
Size: 271743 Color: 10

Bin 2866: 96 of cap free
Amount of items: 2
Items: 
Size: 759492 Color: 3
Size: 240413 Color: 18

Bin 2867: 96 of cap free
Amount of items: 2
Items: 
Size: 537820 Color: 12
Size: 462085 Color: 18

Bin 2868: 97 of cap free
Amount of items: 2
Items: 
Size: 561246 Color: 4
Size: 438658 Color: 13

Bin 2869: 97 of cap free
Amount of items: 2
Items: 
Size: 794211 Color: 1
Size: 205693 Color: 8

Bin 2870: 97 of cap free
Amount of items: 2
Items: 
Size: 612427 Color: 13
Size: 387477 Color: 0

Bin 2871: 97 of cap free
Amount of items: 2
Items: 
Size: 660303 Color: 7
Size: 339601 Color: 14

Bin 2872: 97 of cap free
Amount of items: 2
Items: 
Size: 732027 Color: 5
Size: 267877 Color: 9

Bin 2873: 97 of cap free
Amount of items: 2
Items: 
Size: 760369 Color: 11
Size: 239535 Color: 0

Bin 2874: 97 of cap free
Amount of items: 2
Items: 
Size: 701563 Color: 1
Size: 298341 Color: 3

Bin 2875: 98 of cap free
Amount of items: 2
Items: 
Size: 751580 Color: 2
Size: 248323 Color: 4

Bin 2876: 98 of cap free
Amount of items: 2
Items: 
Size: 540378 Color: 7
Size: 459525 Color: 18

Bin 2877: 98 of cap free
Amount of items: 2
Items: 
Size: 575373 Color: 16
Size: 424530 Color: 10

Bin 2878: 98 of cap free
Amount of items: 2
Items: 
Size: 664474 Color: 4
Size: 335429 Color: 5

Bin 2879: 98 of cap free
Amount of items: 2
Items: 
Size: 711044 Color: 15
Size: 288859 Color: 10

Bin 2880: 98 of cap free
Amount of items: 2
Items: 
Size: 727264 Color: 18
Size: 272639 Color: 13

Bin 2881: 98 of cap free
Amount of items: 2
Items: 
Size: 766842 Color: 18
Size: 233061 Color: 4

Bin 2882: 98 of cap free
Amount of items: 2
Items: 
Size: 652481 Color: 9
Size: 347422 Color: 4

Bin 2883: 98 of cap free
Amount of items: 2
Items: 
Size: 744970 Color: 14
Size: 254933 Color: 13

Bin 2884: 99 of cap free
Amount of items: 2
Items: 
Size: 594060 Color: 7
Size: 405842 Color: 5

Bin 2885: 99 of cap free
Amount of items: 2
Items: 
Size: 609233 Color: 19
Size: 390669 Color: 12

Bin 2886: 99 of cap free
Amount of items: 2
Items: 
Size: 625145 Color: 10
Size: 374757 Color: 15

Bin 2887: 99 of cap free
Amount of items: 2
Items: 
Size: 642361 Color: 13
Size: 357541 Color: 18

Bin 2888: 99 of cap free
Amount of items: 2
Items: 
Size: 694714 Color: 10
Size: 305188 Color: 8

Bin 2889: 99 of cap free
Amount of items: 2
Items: 
Size: 720007 Color: 1
Size: 279895 Color: 0

Bin 2890: 99 of cap free
Amount of items: 2
Items: 
Size: 783455 Color: 10
Size: 216447 Color: 19

Bin 2891: 99 of cap free
Amount of items: 3
Items: 
Size: 350705 Color: 1
Size: 328077 Color: 13
Size: 321120 Color: 19

Bin 2892: 99 of cap free
Amount of items: 2
Items: 
Size: 613683 Color: 18
Size: 386219 Color: 10

Bin 2893: 100 of cap free
Amount of items: 2
Items: 
Size: 622669 Color: 11
Size: 377232 Color: 7

Bin 2894: 100 of cap free
Amount of items: 2
Items: 
Size: 669412 Color: 17
Size: 330489 Color: 19

Bin 2895: 100 of cap free
Amount of items: 2
Items: 
Size: 685355 Color: 0
Size: 314546 Color: 18

Bin 2896: 100 of cap free
Amount of items: 2
Items: 
Size: 690733 Color: 12
Size: 309168 Color: 13

Bin 2897: 100 of cap free
Amount of items: 2
Items: 
Size: 708776 Color: 6
Size: 291125 Color: 13

Bin 2898: 100 of cap free
Amount of items: 2
Items: 
Size: 711400 Color: 19
Size: 288501 Color: 14

Bin 2899: 100 of cap free
Amount of items: 2
Items: 
Size: 730616 Color: 12
Size: 269285 Color: 5

Bin 2900: 100 of cap free
Amount of items: 2
Items: 
Size: 746930 Color: 16
Size: 252971 Color: 15

Bin 2901: 100 of cap free
Amount of items: 2
Items: 
Size: 767060 Color: 12
Size: 232841 Color: 6

Bin 2902: 100 of cap free
Amount of items: 2
Items: 
Size: 541198 Color: 14
Size: 458703 Color: 5

Bin 2903: 100 of cap free
Amount of items: 2
Items: 
Size: 500986 Color: 5
Size: 498915 Color: 12

Bin 2904: 101 of cap free
Amount of items: 2
Items: 
Size: 504289 Color: 7
Size: 495611 Color: 0

Bin 2905: 101 of cap free
Amount of items: 2
Items: 
Size: 587178 Color: 2
Size: 412722 Color: 17

Bin 2906: 101 of cap free
Amount of items: 2
Items: 
Size: 588512 Color: 17
Size: 411388 Color: 3

Bin 2907: 101 of cap free
Amount of items: 2
Items: 
Size: 629859 Color: 5
Size: 370041 Color: 16

Bin 2908: 101 of cap free
Amount of items: 2
Items: 
Size: 644215 Color: 15
Size: 355685 Color: 2

Bin 2909: 101 of cap free
Amount of items: 2
Items: 
Size: 644854 Color: 14
Size: 355046 Color: 17

Bin 2910: 101 of cap free
Amount of items: 2
Items: 
Size: 708268 Color: 4
Size: 291632 Color: 15

Bin 2911: 101 of cap free
Amount of items: 2
Items: 
Size: 777176 Color: 15
Size: 222724 Color: 18

Bin 2912: 101 of cap free
Amount of items: 2
Items: 
Size: 628841 Color: 15
Size: 371059 Color: 0

Bin 2913: 101 of cap free
Amount of items: 2
Items: 
Size: 775844 Color: 18
Size: 224056 Color: 4

Bin 2914: 101 of cap free
Amount of items: 2
Items: 
Size: 571277 Color: 11
Size: 428623 Color: 0

Bin 2915: 101 of cap free
Amount of items: 2
Items: 
Size: 789511 Color: 8
Size: 210389 Color: 18

Bin 2916: 102 of cap free
Amount of items: 2
Items: 
Size: 763361 Color: 3
Size: 236538 Color: 1

Bin 2917: 102 of cap free
Amount of items: 2
Items: 
Size: 511545 Color: 18
Size: 488354 Color: 14

Bin 2918: 102 of cap free
Amount of items: 2
Items: 
Size: 589572 Color: 11
Size: 410327 Color: 16

Bin 2919: 102 of cap free
Amount of items: 2
Items: 
Size: 700764 Color: 8
Size: 299135 Color: 10

Bin 2920: 102 of cap free
Amount of items: 2
Items: 
Size: 705949 Color: 4
Size: 293950 Color: 15

Bin 2921: 102 of cap free
Amount of items: 2
Items: 
Size: 799500 Color: 11
Size: 200399 Color: 7

Bin 2922: 102 of cap free
Amount of items: 2
Items: 
Size: 538392 Color: 6
Size: 461507 Color: 11

Bin 2923: 103 of cap free
Amount of items: 2
Items: 
Size: 785165 Color: 15
Size: 214733 Color: 10

Bin 2924: 103 of cap free
Amount of items: 3
Items: 
Size: 619031 Color: 7
Size: 191159 Color: 17
Size: 189708 Color: 4

Bin 2925: 103 of cap free
Amount of items: 2
Items: 
Size: 530287 Color: 2
Size: 469611 Color: 12

Bin 2926: 103 of cap free
Amount of items: 2
Items: 
Size: 576902 Color: 18
Size: 422996 Color: 17

Bin 2927: 103 of cap free
Amount of items: 2
Items: 
Size: 614723 Color: 4
Size: 385175 Color: 12

Bin 2928: 103 of cap free
Amount of items: 2
Items: 
Size: 509646 Color: 4
Size: 490252 Color: 16

Bin 2929: 103 of cap free
Amount of items: 2
Items: 
Size: 648928 Color: 8
Size: 350970 Color: 4

Bin 2930: 103 of cap free
Amount of items: 2
Items: 
Size: 695834 Color: 18
Size: 304064 Color: 3

Bin 2931: 103 of cap free
Amount of items: 2
Items: 
Size: 732902 Color: 6
Size: 266996 Color: 5

Bin 2932: 103 of cap free
Amount of items: 2
Items: 
Size: 733521 Color: 0
Size: 266377 Color: 10

Bin 2933: 103 of cap free
Amount of items: 2
Items: 
Size: 796625 Color: 2
Size: 203273 Color: 19

Bin 2934: 103 of cap free
Amount of items: 2
Items: 
Size: 770998 Color: 0
Size: 228900 Color: 13

Bin 2935: 103 of cap free
Amount of items: 2
Items: 
Size: 754506 Color: 3
Size: 245392 Color: 8

Bin 2936: 104 of cap free
Amount of items: 2
Items: 
Size: 795448 Color: 12
Size: 204449 Color: 8

Bin 2937: 104 of cap free
Amount of items: 2
Items: 
Size: 522830 Color: 19
Size: 477067 Color: 11

Bin 2938: 104 of cap free
Amount of items: 2
Items: 
Size: 530142 Color: 0
Size: 469755 Color: 16

Bin 2939: 104 of cap free
Amount of items: 2
Items: 
Size: 653466 Color: 8
Size: 346431 Color: 5

Bin 2940: 104 of cap free
Amount of items: 2
Items: 
Size: 753932 Color: 18
Size: 245965 Color: 14

Bin 2941: 104 of cap free
Amount of items: 2
Items: 
Size: 676729 Color: 4
Size: 323168 Color: 18

Bin 2942: 105 of cap free
Amount of items: 2
Items: 
Size: 686067 Color: 5
Size: 313829 Color: 4

Bin 2943: 105 of cap free
Amount of items: 2
Items: 
Size: 797939 Color: 18
Size: 201957 Color: 7

Bin 2944: 105 of cap free
Amount of items: 2
Items: 
Size: 556542 Color: 8
Size: 443354 Color: 5

Bin 2945: 105 of cap free
Amount of items: 2
Items: 
Size: 578098 Color: 0
Size: 421798 Color: 18

Bin 2946: 105 of cap free
Amount of items: 2
Items: 
Size: 590677 Color: 3
Size: 409219 Color: 10

Bin 2947: 105 of cap free
Amount of items: 2
Items: 
Size: 641236 Color: 14
Size: 358660 Color: 4

Bin 2948: 105 of cap free
Amount of items: 2
Items: 
Size: 669991 Color: 5
Size: 329905 Color: 3

Bin 2949: 105 of cap free
Amount of items: 2
Items: 
Size: 770332 Color: 16
Size: 229564 Color: 5

Bin 2950: 105 of cap free
Amount of items: 3
Items: 
Size: 636786 Color: 7
Size: 181558 Color: 4
Size: 181552 Color: 1

Bin 2951: 106 of cap free
Amount of items: 2
Items: 
Size: 603993 Color: 18
Size: 395902 Color: 3

Bin 2952: 106 of cap free
Amount of items: 2
Items: 
Size: 784928 Color: 13
Size: 214967 Color: 14

Bin 2953: 106 of cap free
Amount of items: 2
Items: 
Size: 714960 Color: 0
Size: 284935 Color: 9

Bin 2954: 106 of cap free
Amount of items: 2
Items: 
Size: 514861 Color: 12
Size: 485034 Color: 9

Bin 2955: 106 of cap free
Amount of items: 2
Items: 
Size: 644582 Color: 8
Size: 355313 Color: 4

Bin 2956: 106 of cap free
Amount of items: 2
Items: 
Size: 713280 Color: 11
Size: 286615 Color: 3

Bin 2957: 106 of cap free
Amount of items: 2
Items: 
Size: 775998 Color: 2
Size: 223897 Color: 15

Bin 2958: 106 of cap free
Amount of items: 2
Items: 
Size: 679556 Color: 15
Size: 320339 Color: 9

Bin 2959: 107 of cap free
Amount of items: 2
Items: 
Size: 667054 Color: 2
Size: 332840 Color: 17

Bin 2960: 107 of cap free
Amount of items: 2
Items: 
Size: 514442 Color: 12
Size: 485452 Color: 14

Bin 2961: 107 of cap free
Amount of items: 2
Items: 
Size: 795862 Color: 7
Size: 204032 Color: 11

Bin 2962: 107 of cap free
Amount of items: 2
Items: 
Size: 569390 Color: 16
Size: 430504 Color: 7

Bin 2963: 107 of cap free
Amount of items: 2
Items: 
Size: 753603 Color: 16
Size: 246291 Color: 15

Bin 2964: 107 of cap free
Amount of items: 3
Items: 
Size: 668388 Color: 18
Size: 167183 Color: 3
Size: 164323 Color: 8

Bin 2965: 107 of cap free
Amount of items: 2
Items: 
Size: 520593 Color: 15
Size: 479301 Color: 18

Bin 2966: 107 of cap free
Amount of items: 2
Items: 
Size: 539032 Color: 2
Size: 460862 Color: 17

Bin 2967: 107 of cap free
Amount of items: 2
Items: 
Size: 586958 Color: 1
Size: 412936 Color: 14

Bin 2968: 107 of cap free
Amount of items: 2
Items: 
Size: 613858 Color: 1
Size: 386036 Color: 13

Bin 2969: 107 of cap free
Amount of items: 2
Items: 
Size: 631019 Color: 14
Size: 368875 Color: 7

Bin 2970: 107 of cap free
Amount of items: 2
Items: 
Size: 659091 Color: 12
Size: 340803 Color: 4

Bin 2971: 107 of cap free
Amount of items: 2
Items: 
Size: 688192 Color: 11
Size: 311702 Color: 15

Bin 2972: 107 of cap free
Amount of items: 2
Items: 
Size: 726891 Color: 0
Size: 273003 Color: 19

Bin 2973: 107 of cap free
Amount of items: 2
Items: 
Size: 763126 Color: 12
Size: 236768 Color: 10

Bin 2974: 108 of cap free
Amount of items: 2
Items: 
Size: 574857 Color: 11
Size: 425036 Color: 2

Bin 2975: 108 of cap free
Amount of items: 2
Items: 
Size: 582954 Color: 2
Size: 416939 Color: 14

Bin 2976: 108 of cap free
Amount of items: 2
Items: 
Size: 601856 Color: 19
Size: 398037 Color: 6

Bin 2977: 108 of cap free
Amount of items: 2
Items: 
Size: 780673 Color: 17
Size: 219220 Color: 15

Bin 2978: 108 of cap free
Amount of items: 2
Items: 
Size: 501120 Color: 17
Size: 498773 Color: 18

Bin 2979: 109 of cap free
Amount of items: 2
Items: 
Size: 583553 Color: 15
Size: 416339 Color: 16

Bin 2980: 109 of cap free
Amount of items: 2
Items: 
Size: 587441 Color: 15
Size: 412451 Color: 9

Bin 2981: 110 of cap free
Amount of items: 2
Items: 
Size: 651386 Color: 2
Size: 348505 Color: 19

Bin 2982: 110 of cap free
Amount of items: 2
Items: 
Size: 520047 Color: 1
Size: 479844 Color: 15

Bin 2983: 110 of cap free
Amount of items: 2
Items: 
Size: 552262 Color: 5
Size: 447629 Color: 10

Bin 2984: 110 of cap free
Amount of items: 2
Items: 
Size: 582680 Color: 8
Size: 417211 Color: 4

Bin 2985: 110 of cap free
Amount of items: 2
Items: 
Size: 610970 Color: 7
Size: 388921 Color: 9

Bin 2986: 110 of cap free
Amount of items: 2
Items: 
Size: 668098 Color: 0
Size: 331793 Color: 19

Bin 2987: 110 of cap free
Amount of items: 2
Items: 
Size: 755201 Color: 15
Size: 244690 Color: 2

Bin 2988: 110 of cap free
Amount of items: 2
Items: 
Size: 788609 Color: 12
Size: 211282 Color: 13

Bin 2989: 111 of cap free
Amount of items: 3
Items: 
Size: 559827 Color: 0
Size: 247906 Color: 3
Size: 192157 Color: 3

Bin 2990: 111 of cap free
Amount of items: 2
Items: 
Size: 561223 Color: 13
Size: 438667 Color: 4

Bin 2991: 111 of cap free
Amount of items: 2
Items: 
Size: 608140 Color: 11
Size: 391750 Color: 16

Bin 2992: 111 of cap free
Amount of items: 2
Items: 
Size: 644062 Color: 17
Size: 355828 Color: 19

Bin 2993: 111 of cap free
Amount of items: 2
Items: 
Size: 729348 Color: 12
Size: 270542 Color: 1

Bin 2994: 111 of cap free
Amount of items: 2
Items: 
Size: 751050 Color: 6
Size: 248840 Color: 7

Bin 2995: 111 of cap free
Amount of items: 2
Items: 
Size: 725959 Color: 1
Size: 273931 Color: 5

Bin 2996: 112 of cap free
Amount of items: 2
Items: 
Size: 503815 Color: 11
Size: 496074 Color: 19

Bin 2997: 112 of cap free
Amount of items: 2
Items: 
Size: 513221 Color: 16
Size: 486668 Color: 12

Bin 2998: 112 of cap free
Amount of items: 2
Items: 
Size: 528878 Color: 2
Size: 471011 Color: 15

Bin 2999: 112 of cap free
Amount of items: 2
Items: 
Size: 572271 Color: 9
Size: 427618 Color: 4

Bin 3000: 112 of cap free
Amount of items: 2
Items: 
Size: 584886 Color: 7
Size: 415003 Color: 3

Bin 3001: 112 of cap free
Amount of items: 2
Items: 
Size: 608786 Color: 15
Size: 391103 Color: 19

Bin 3002: 112 of cap free
Amount of items: 2
Items: 
Size: 610842 Color: 0
Size: 389047 Color: 6

Bin 3003: 112 of cap free
Amount of items: 2
Items: 
Size: 645506 Color: 11
Size: 354383 Color: 3

Bin 3004: 112 of cap free
Amount of items: 2
Items: 
Size: 722661 Color: 11
Size: 277228 Color: 13

Bin 3005: 112 of cap free
Amount of items: 2
Items: 
Size: 792980 Color: 4
Size: 206909 Color: 10

Bin 3006: 112 of cap free
Amount of items: 2
Items: 
Size: 709184 Color: 19
Size: 290705 Color: 15

Bin 3007: 112 of cap free
Amount of items: 2
Items: 
Size: 710504 Color: 9
Size: 289385 Color: 5

Bin 3008: 113 of cap free
Amount of items: 2
Items: 
Size: 563559 Color: 19
Size: 436329 Color: 13

Bin 3009: 113 of cap free
Amount of items: 2
Items: 
Size: 589567 Color: 9
Size: 410321 Color: 2

Bin 3010: 113 of cap free
Amount of items: 2
Items: 
Size: 635457 Color: 15
Size: 364431 Color: 10

Bin 3011: 113 of cap free
Amount of items: 2
Items: 
Size: 725028 Color: 19
Size: 274860 Color: 14

Bin 3012: 113 of cap free
Amount of items: 2
Items: 
Size: 730095 Color: 9
Size: 269793 Color: 1

Bin 3013: 113 of cap free
Amount of items: 2
Items: 
Size: 789239 Color: 16
Size: 210649 Color: 4

Bin 3014: 113 of cap free
Amount of items: 2
Items: 
Size: 659999 Color: 12
Size: 339889 Color: 16

Bin 3015: 113 of cap free
Amount of items: 2
Items: 
Size: 767495 Color: 13
Size: 232393 Color: 3

Bin 3016: 113 of cap free
Amount of items: 2
Items: 
Size: 788163 Color: 4
Size: 211725 Color: 9

Bin 3017: 114 of cap free
Amount of items: 2
Items: 
Size: 697372 Color: 19
Size: 302515 Color: 1

Bin 3018: 114 of cap free
Amount of items: 2
Items: 
Size: 528128 Color: 3
Size: 471759 Color: 13

Bin 3019: 114 of cap free
Amount of items: 2
Items: 
Size: 712875 Color: 12
Size: 287012 Color: 15

Bin 3020: 114 of cap free
Amount of items: 2
Items: 
Size: 724055 Color: 2
Size: 275832 Color: 4

Bin 3021: 114 of cap free
Amount of items: 2
Items: 
Size: 726877 Color: 5
Size: 273010 Color: 0

Bin 3022: 115 of cap free
Amount of items: 3
Items: 
Size: 630023 Color: 2
Size: 185015 Color: 8
Size: 184848 Color: 2

Bin 3023: 115 of cap free
Amount of items: 2
Items: 
Size: 504278 Color: 17
Size: 495608 Color: 16

Bin 3024: 115 of cap free
Amount of items: 2
Items: 
Size: 517548 Color: 4
Size: 482338 Color: 2

Bin 3025: 115 of cap free
Amount of items: 2
Items: 
Size: 541044 Color: 13
Size: 458842 Color: 12

Bin 3026: 115 of cap free
Amount of items: 2
Items: 
Size: 550897 Color: 19
Size: 448989 Color: 8

Bin 3027: 115 of cap free
Amount of items: 2
Items: 
Size: 561624 Color: 7
Size: 438262 Color: 1

Bin 3028: 115 of cap free
Amount of items: 2
Items: 
Size: 570116 Color: 10
Size: 429770 Color: 3

Bin 3029: 115 of cap free
Amount of items: 2
Items: 
Size: 590455 Color: 12
Size: 409431 Color: 1

Bin 3030: 115 of cap free
Amount of items: 2
Items: 
Size: 762248 Color: 9
Size: 237638 Color: 8

Bin 3031: 115 of cap free
Amount of items: 2
Items: 
Size: 506770 Color: 14
Size: 493116 Color: 15

Bin 3032: 116 of cap free
Amount of items: 2
Items: 
Size: 506564 Color: 6
Size: 493321 Color: 8

Bin 3033: 116 of cap free
Amount of items: 2
Items: 
Size: 524771 Color: 18
Size: 475114 Color: 8

Bin 3034: 116 of cap free
Amount of items: 2
Items: 
Size: 578379 Color: 18
Size: 421506 Color: 3

Bin 3035: 116 of cap free
Amount of items: 2
Items: 
Size: 650928 Color: 7
Size: 348957 Color: 8

Bin 3036: 116 of cap free
Amount of items: 2
Items: 
Size: 654428 Color: 16
Size: 345457 Color: 1

Bin 3037: 116 of cap free
Amount of items: 2
Items: 
Size: 585485 Color: 16
Size: 414400 Color: 15

Bin 3038: 116 of cap free
Amount of items: 2
Items: 
Size: 798359 Color: 2
Size: 201526 Color: 12

Bin 3039: 117 of cap free
Amount of items: 2
Items: 
Size: 786247 Color: 1
Size: 213637 Color: 3

Bin 3040: 117 of cap free
Amount of items: 2
Items: 
Size: 507254 Color: 5
Size: 492630 Color: 7

Bin 3041: 117 of cap free
Amount of items: 2
Items: 
Size: 517402 Color: 18
Size: 482482 Color: 16

Bin 3042: 117 of cap free
Amount of items: 2
Items: 
Size: 523042 Color: 15
Size: 476842 Color: 18

Bin 3043: 117 of cap free
Amount of items: 2
Items: 
Size: 531590 Color: 3
Size: 468294 Color: 18

Bin 3044: 117 of cap free
Amount of items: 2
Items: 
Size: 568071 Color: 12
Size: 431813 Color: 6

Bin 3045: 117 of cap free
Amount of items: 2
Items: 
Size: 591978 Color: 15
Size: 407906 Color: 17

Bin 3046: 117 of cap free
Amount of items: 2
Items: 
Size: 624032 Color: 14
Size: 375852 Color: 9

Bin 3047: 117 of cap free
Amount of items: 2
Items: 
Size: 700989 Color: 10
Size: 298895 Color: 11

Bin 3048: 117 of cap free
Amount of items: 2
Items: 
Size: 646755 Color: 8
Size: 353129 Color: 19

Bin 3049: 118 of cap free
Amount of items: 2
Items: 
Size: 587559 Color: 17
Size: 412324 Color: 18

Bin 3050: 118 of cap free
Amount of items: 2
Items: 
Size: 553089 Color: 14
Size: 446794 Color: 9

Bin 3051: 118 of cap free
Amount of items: 2
Items: 
Size: 517849 Color: 11
Size: 482034 Color: 13

Bin 3052: 118 of cap free
Amount of items: 2
Items: 
Size: 554149 Color: 15
Size: 445734 Color: 19

Bin 3053: 118 of cap free
Amount of items: 2
Items: 
Size: 572991 Color: 15
Size: 426892 Color: 11

Bin 3054: 118 of cap free
Amount of items: 2
Items: 
Size: 591648 Color: 11
Size: 408235 Color: 4

Bin 3055: 118 of cap free
Amount of items: 2
Items: 
Size: 634208 Color: 19
Size: 365675 Color: 6

Bin 3056: 118 of cap free
Amount of items: 2
Items: 
Size: 644868 Color: 17
Size: 355015 Color: 8

Bin 3057: 118 of cap free
Amount of items: 2
Items: 
Size: 698211 Color: 15
Size: 301672 Color: 19

Bin 3058: 119 of cap free
Amount of items: 2
Items: 
Size: 785556 Color: 16
Size: 214326 Color: 18

Bin 3059: 119 of cap free
Amount of items: 2
Items: 
Size: 746063 Color: 18
Size: 253819 Color: 1

Bin 3060: 119 of cap free
Amount of items: 2
Items: 
Size: 527195 Color: 14
Size: 472687 Color: 8

Bin 3061: 119 of cap free
Amount of items: 2
Items: 
Size: 782985 Color: 5
Size: 216897 Color: 6

Bin 3062: 119 of cap free
Amount of items: 2
Items: 
Size: 536431 Color: 9
Size: 463451 Color: 8

Bin 3063: 119 of cap free
Amount of items: 3
Items: 
Size: 338234 Color: 8
Size: 331962 Color: 17
Size: 329686 Color: 19

Bin 3064: 120 of cap free
Amount of items: 2
Items: 
Size: 543583 Color: 6
Size: 456298 Color: 14

Bin 3065: 120 of cap free
Amount of items: 2
Items: 
Size: 645745 Color: 12
Size: 354136 Color: 11

Bin 3066: 120 of cap free
Amount of items: 2
Items: 
Size: 503078 Color: 18
Size: 496803 Color: 12

Bin 3067: 120 of cap free
Amount of items: 2
Items: 
Size: 524518 Color: 15
Size: 475363 Color: 8

Bin 3068: 120 of cap free
Amount of items: 2
Items: 
Size: 542467 Color: 11
Size: 457414 Color: 4

Bin 3069: 120 of cap free
Amount of items: 2
Items: 
Size: 673607 Color: 4
Size: 326274 Color: 6

Bin 3070: 120 of cap free
Amount of items: 2
Items: 
Size: 740627 Color: 19
Size: 259254 Color: 6

Bin 3071: 121 of cap free
Amount of items: 2
Items: 
Size: 737794 Color: 3
Size: 262086 Color: 7

Bin 3072: 121 of cap free
Amount of items: 2
Items: 
Size: 601184 Color: 18
Size: 398696 Color: 9

Bin 3073: 121 of cap free
Amount of items: 2
Items: 
Size: 508686 Color: 10
Size: 491194 Color: 8

Bin 3074: 121 of cap free
Amount of items: 2
Items: 
Size: 590454 Color: 16
Size: 409426 Color: 0

Bin 3075: 121 of cap free
Amount of items: 2
Items: 
Size: 731477 Color: 18
Size: 268403 Color: 7

Bin 3076: 121 of cap free
Amount of items: 2
Items: 
Size: 767057 Color: 16
Size: 232823 Color: 10

Bin 3077: 121 of cap free
Amount of items: 2
Items: 
Size: 790641 Color: 5
Size: 209239 Color: 19

Bin 3078: 121 of cap free
Amount of items: 2
Items: 
Size: 627932 Color: 3
Size: 371948 Color: 12

Bin 3079: 122 of cap free
Amount of items: 2
Items: 
Size: 702318 Color: 2
Size: 297561 Color: 9

Bin 3080: 122 of cap free
Amount of items: 2
Items: 
Size: 535089 Color: 12
Size: 464790 Color: 2

Bin 3081: 122 of cap free
Amount of items: 2
Items: 
Size: 719985 Color: 5
Size: 279894 Color: 3

Bin 3082: 122 of cap free
Amount of items: 2
Items: 
Size: 759125 Color: 2
Size: 240754 Color: 8

Bin 3083: 123 of cap free
Amount of items: 2
Items: 
Size: 654979 Color: 4
Size: 344899 Color: 7

Bin 3084: 123 of cap free
Amount of items: 2
Items: 
Size: 714215 Color: 13
Size: 285663 Color: 16

Bin 3085: 123 of cap free
Amount of items: 2
Items: 
Size: 743838 Color: 19
Size: 256040 Color: 16

Bin 3086: 123 of cap free
Amount of items: 2
Items: 
Size: 503504 Color: 2
Size: 496374 Color: 0

Bin 3087: 124 of cap free
Amount of items: 3
Items: 
Size: 666422 Color: 0
Size: 167306 Color: 13
Size: 166149 Color: 15

Bin 3088: 124 of cap free
Amount of items: 2
Items: 
Size: 654950 Color: 3
Size: 344927 Color: 4

Bin 3089: 124 of cap free
Amount of items: 2
Items: 
Size: 610513 Color: 3
Size: 389364 Color: 8

Bin 3090: 124 of cap free
Amount of items: 2
Items: 
Size: 677968 Color: 8
Size: 321909 Color: 12

Bin 3091: 124 of cap free
Amount of items: 2
Items: 
Size: 570778 Color: 3
Size: 429099 Color: 17

Bin 3092: 124 of cap free
Amount of items: 2
Items: 
Size: 673102 Color: 10
Size: 326775 Color: 16

Bin 3093: 124 of cap free
Amount of items: 2
Items: 
Size: 648086 Color: 4
Size: 351791 Color: 1

Bin 3094: 124 of cap free
Amount of items: 2
Items: 
Size: 744822 Color: 14
Size: 255055 Color: 9

Bin 3095: 124 of cap free
Amount of items: 2
Items: 
Size: 741934 Color: 16
Size: 257943 Color: 7

Bin 3096: 125 of cap free
Amount of items: 2
Items: 
Size: 534685 Color: 14
Size: 465191 Color: 19

Bin 3097: 125 of cap free
Amount of items: 2
Items: 
Size: 578627 Color: 3
Size: 421249 Color: 5

Bin 3098: 125 of cap free
Amount of items: 2
Items: 
Size: 592281 Color: 1
Size: 407595 Color: 13

Bin 3099: 126 of cap free
Amount of items: 2
Items: 
Size: 738367 Color: 18
Size: 261508 Color: 12

Bin 3100: 126 of cap free
Amount of items: 2
Items: 
Size: 785803 Color: 9
Size: 214072 Color: 11

Bin 3101: 126 of cap free
Amount of items: 2
Items: 
Size: 672733 Color: 2
Size: 327142 Color: 17

Bin 3102: 126 of cap free
Amount of items: 2
Items: 
Size: 728498 Color: 15
Size: 271377 Color: 3

Bin 3103: 127 of cap free
Amount of items: 2
Items: 
Size: 682992 Color: 4
Size: 316882 Color: 9

Bin 3104: 127 of cap free
Amount of items: 2
Items: 
Size: 666620 Color: 18
Size: 333254 Color: 12

Bin 3105: 128 of cap free
Amount of items: 2
Items: 
Size: 700710 Color: 9
Size: 299163 Color: 8

Bin 3106: 128 of cap free
Amount of items: 2
Items: 
Size: 643857 Color: 12
Size: 356016 Color: 8

Bin 3107: 129 of cap free
Amount of items: 2
Items: 
Size: 534947 Color: 1
Size: 464925 Color: 11

Bin 3108: 129 of cap free
Amount of items: 2
Items: 
Size: 586016 Color: 0
Size: 413856 Color: 14

Bin 3109: 129 of cap free
Amount of items: 2
Items: 
Size: 632238 Color: 14
Size: 367634 Color: 0

Bin 3110: 129 of cap free
Amount of items: 2
Items: 
Size: 725648 Color: 6
Size: 274224 Color: 1

Bin 3111: 129 of cap free
Amount of items: 2
Items: 
Size: 798723 Color: 6
Size: 201149 Color: 14

Bin 3112: 129 of cap free
Amount of items: 2
Items: 
Size: 620938 Color: 0
Size: 378934 Color: 18

Bin 3113: 129 of cap free
Amount of items: 2
Items: 
Size: 500397 Color: 5
Size: 499475 Color: 13

Bin 3114: 131 of cap free
Amount of items: 2
Items: 
Size: 789676 Color: 12
Size: 210194 Color: 11

Bin 3115: 131 of cap free
Amount of items: 2
Items: 
Size: 501435 Color: 14
Size: 498435 Color: 1

Bin 3116: 131 of cap free
Amount of items: 2
Items: 
Size: 563252 Color: 3
Size: 436618 Color: 5

Bin 3117: 131 of cap free
Amount of items: 2
Items: 
Size: 659081 Color: 4
Size: 340789 Color: 11

Bin 3118: 131 of cap free
Amount of items: 2
Items: 
Size: 728935 Color: 18
Size: 270935 Color: 9

Bin 3119: 131 of cap free
Amount of items: 2
Items: 
Size: 765124 Color: 5
Size: 234746 Color: 0

Bin 3120: 131 of cap free
Amount of items: 2
Items: 
Size: 767397 Color: 18
Size: 232473 Color: 13

Bin 3121: 131 of cap free
Amount of items: 2
Items: 
Size: 788847 Color: 1
Size: 211023 Color: 6

Bin 3122: 131 of cap free
Amount of items: 2
Items: 
Size: 799242 Color: 8
Size: 200628 Color: 4

Bin 3123: 132 of cap free
Amount of items: 2
Items: 
Size: 724752 Color: 2
Size: 275117 Color: 3

Bin 3124: 132 of cap free
Amount of items: 2
Items: 
Size: 597014 Color: 9
Size: 402855 Color: 15

Bin 3125: 132 of cap free
Amount of items: 2
Items: 
Size: 612093 Color: 6
Size: 387776 Color: 4

Bin 3126: 132 of cap free
Amount of items: 2
Items: 
Size: 755903 Color: 9
Size: 243966 Color: 3

Bin 3127: 132 of cap free
Amount of items: 2
Items: 
Size: 763997 Color: 16
Size: 235872 Color: 12

Bin 3128: 132 of cap free
Amount of items: 2
Items: 
Size: 622272 Color: 1
Size: 377597 Color: 2

Bin 3129: 133 of cap free
Amount of items: 2
Items: 
Size: 695231 Color: 0
Size: 304637 Color: 5

Bin 3130: 133 of cap free
Amount of items: 2
Items: 
Size: 651336 Color: 8
Size: 348532 Color: 2

Bin 3131: 133 of cap free
Amount of items: 2
Items: 
Size: 734316 Color: 16
Size: 265552 Color: 0

Bin 3132: 133 of cap free
Amount of items: 2
Items: 
Size: 644595 Color: 4
Size: 355273 Color: 3

Bin 3133: 134 of cap free
Amount of items: 2
Items: 
Size: 532956 Color: 0
Size: 466911 Color: 14

Bin 3134: 134 of cap free
Amount of items: 2
Items: 
Size: 571004 Color: 8
Size: 428863 Color: 17

Bin 3135: 134 of cap free
Amount of items: 2
Items: 
Size: 682804 Color: 12
Size: 317063 Color: 13

Bin 3136: 134 of cap free
Amount of items: 2
Items: 
Size: 664471 Color: 8
Size: 335396 Color: 12

Bin 3137: 135 of cap free
Amount of items: 2
Items: 
Size: 665873 Color: 14
Size: 333993 Color: 9

Bin 3138: 135 of cap free
Amount of items: 2
Items: 
Size: 654551 Color: 13
Size: 345315 Color: 4

Bin 3139: 135 of cap free
Amount of items: 2
Items: 
Size: 599300 Color: 0
Size: 400566 Color: 19

Bin 3140: 135 of cap free
Amount of items: 2
Items: 
Size: 634760 Color: 10
Size: 365106 Color: 6

Bin 3141: 135 of cap free
Amount of items: 2
Items: 
Size: 576657 Color: 16
Size: 423209 Color: 10

Bin 3142: 135 of cap free
Amount of items: 2
Items: 
Size: 732261 Color: 18
Size: 267605 Color: 9

Bin 3143: 136 of cap free
Amount of items: 2
Items: 
Size: 522275 Color: 12
Size: 477590 Color: 1

Bin 3144: 136 of cap free
Amount of items: 2
Items: 
Size: 600847 Color: 18
Size: 399018 Color: 15

Bin 3145: 136 of cap free
Amount of items: 2
Items: 
Size: 615704 Color: 4
Size: 384161 Color: 14

Bin 3146: 136 of cap free
Amount of items: 2
Items: 
Size: 725321 Color: 7
Size: 274544 Color: 4

Bin 3147: 136 of cap free
Amount of items: 2
Items: 
Size: 746926 Color: 10
Size: 252939 Color: 7

Bin 3148: 136 of cap free
Amount of items: 2
Items: 
Size: 793495 Color: 11
Size: 206370 Color: 8

Bin 3149: 137 of cap free
Amount of items: 2
Items: 
Size: 525946 Color: 5
Size: 473918 Color: 3

Bin 3150: 137 of cap free
Amount of items: 2
Items: 
Size: 601519 Color: 19
Size: 398345 Color: 12

Bin 3151: 137 of cap free
Amount of items: 2
Items: 
Size: 698041 Color: 10
Size: 301823 Color: 13

Bin 3152: 137 of cap free
Amount of items: 2
Items: 
Size: 578370 Color: 13
Size: 421494 Color: 9

Bin 3153: 137 of cap free
Amount of items: 2
Items: 
Size: 581090 Color: 15
Size: 418774 Color: 12

Bin 3154: 137 of cap free
Amount of items: 2
Items: 
Size: 684936 Color: 1
Size: 314928 Color: 10

Bin 3155: 138 of cap free
Amount of items: 2
Items: 
Size: 579132 Color: 18
Size: 420731 Color: 8

Bin 3156: 138 of cap free
Amount of items: 2
Items: 
Size: 709180 Color: 3
Size: 290683 Color: 13

Bin 3157: 138 of cap free
Amount of items: 2
Items: 
Size: 542227 Color: 4
Size: 457636 Color: 0

Bin 3158: 138 of cap free
Amount of items: 2
Items: 
Size: 662796 Color: 1
Size: 337067 Color: 4

Bin 3159: 138 of cap free
Amount of items: 2
Items: 
Size: 682219 Color: 5
Size: 317644 Color: 8

Bin 3160: 138 of cap free
Amount of items: 2
Items: 
Size: 696140 Color: 19
Size: 303723 Color: 4

Bin 3161: 138 of cap free
Amount of items: 2
Items: 
Size: 731794 Color: 11
Size: 268069 Color: 9

Bin 3162: 138 of cap free
Amount of items: 2
Items: 
Size: 754813 Color: 4
Size: 245050 Color: 19

Bin 3163: 138 of cap free
Amount of items: 2
Items: 
Size: 549659 Color: 16
Size: 450204 Color: 19

Bin 3164: 138 of cap free
Amount of items: 2
Items: 
Size: 633763 Color: 1
Size: 366100 Color: 3

Bin 3165: 139 of cap free
Amount of items: 2
Items: 
Size: 705688 Color: 18
Size: 294174 Color: 6

Bin 3166: 139 of cap free
Amount of items: 2
Items: 
Size: 784683 Color: 15
Size: 215179 Color: 8

Bin 3167: 139 of cap free
Amount of items: 2
Items: 
Size: 555813 Color: 10
Size: 444049 Color: 7

Bin 3168: 140 of cap free
Amount of items: 2
Items: 
Size: 704265 Color: 14
Size: 295596 Color: 5

Bin 3169: 140 of cap free
Amount of items: 2
Items: 
Size: 530748 Color: 5
Size: 469113 Color: 1

Bin 3170: 140 of cap free
Amount of items: 2
Items: 
Size: 544902 Color: 17
Size: 454959 Color: 18

Bin 3171: 140 of cap free
Amount of items: 2
Items: 
Size: 579660 Color: 19
Size: 420201 Color: 5

Bin 3172: 140 of cap free
Amount of items: 2
Items: 
Size: 712441 Color: 0
Size: 287420 Color: 19

Bin 3173: 141 of cap free
Amount of items: 2
Items: 
Size: 522126 Color: 6
Size: 477734 Color: 4

Bin 3174: 141 of cap free
Amount of items: 2
Items: 
Size: 531747 Color: 14
Size: 468113 Color: 9

Bin 3175: 141 of cap free
Amount of items: 2
Items: 
Size: 639275 Color: 8
Size: 360585 Color: 4

Bin 3176: 141 of cap free
Amount of items: 2
Items: 
Size: 687346 Color: 1
Size: 312514 Color: 17

Bin 3177: 142 of cap free
Amount of items: 2
Items: 
Size: 737267 Color: 0
Size: 262592 Color: 1

Bin 3178: 142 of cap free
Amount of items: 2
Items: 
Size: 656381 Color: 0
Size: 343478 Color: 16

Bin 3179: 142 of cap free
Amount of items: 2
Items: 
Size: 778424 Color: 10
Size: 221435 Color: 2

Bin 3180: 143 of cap free
Amount of items: 2
Items: 
Size: 630478 Color: 12
Size: 369380 Color: 0

Bin 3181: 143 of cap free
Amount of items: 3
Items: 
Size: 770148 Color: 0
Size: 118112 Color: 2
Size: 111598 Color: 0

Bin 3182: 143 of cap free
Amount of items: 2
Items: 
Size: 784390 Color: 13
Size: 215468 Color: 6

Bin 3183: 143 of cap free
Amount of items: 2
Items: 
Size: 589338 Color: 7
Size: 410520 Color: 1

Bin 3184: 143 of cap free
Amount of items: 2
Items: 
Size: 672571 Color: 9
Size: 327287 Color: 17

Bin 3185: 143 of cap free
Amount of items: 2
Items: 
Size: 711238 Color: 16
Size: 288620 Color: 15

Bin 3186: 143 of cap free
Amount of items: 2
Items: 
Size: 772621 Color: 16
Size: 227237 Color: 18

Bin 3187: 143 of cap free
Amount of items: 2
Items: 
Size: 745124 Color: 10
Size: 254734 Color: 7

Bin 3188: 144 of cap free
Amount of items: 2
Items: 
Size: 640417 Color: 4
Size: 359440 Color: 0

Bin 3189: 144 of cap free
Amount of items: 2
Items: 
Size: 526479 Color: 6
Size: 473378 Color: 0

Bin 3190: 144 of cap free
Amount of items: 2
Items: 
Size: 573610 Color: 15
Size: 426247 Color: 11

Bin 3191: 144 of cap free
Amount of items: 2
Items: 
Size: 662793 Color: 8
Size: 337064 Color: 11

Bin 3192: 144 of cap free
Amount of items: 2
Items: 
Size: 697560 Color: 19
Size: 302297 Color: 8

Bin 3193: 145 of cap free
Amount of items: 2
Items: 
Size: 729857 Color: 10
Size: 269999 Color: 0

Bin 3194: 145 of cap free
Amount of items: 2
Items: 
Size: 774922 Color: 12
Size: 224934 Color: 8

Bin 3195: 145 of cap free
Amount of items: 2
Items: 
Size: 549216 Color: 17
Size: 450640 Color: 4

Bin 3196: 146 of cap free
Amount of items: 2
Items: 
Size: 704836 Color: 17
Size: 295019 Color: 18

Bin 3197: 146 of cap free
Amount of items: 2
Items: 
Size: 675930 Color: 13
Size: 323925 Color: 4

Bin 3198: 147 of cap free
Amount of items: 2
Items: 
Size: 694683 Color: 15
Size: 305171 Color: 6

Bin 3199: 148 of cap free
Amount of items: 2
Items: 
Size: 551947 Color: 8
Size: 447906 Color: 3

Bin 3200: 149 of cap free
Amount of items: 2
Items: 
Size: 674801 Color: 15
Size: 325051 Color: 10

Bin 3201: 149 of cap free
Amount of items: 2
Items: 
Size: 704522 Color: 0
Size: 295330 Color: 18

Bin 3202: 149 of cap free
Amount of items: 2
Items: 
Size: 724048 Color: 2
Size: 275804 Color: 13

Bin 3203: 149 of cap free
Amount of items: 2
Items: 
Size: 766824 Color: 9
Size: 233028 Color: 4

Bin 3204: 150 of cap free
Amount of items: 3
Items: 
Size: 411644 Color: 0
Size: 311950 Color: 17
Size: 276257 Color: 18

Bin 3205: 150 of cap free
Amount of items: 2
Items: 
Size: 571890 Color: 8
Size: 427961 Color: 15

Bin 3206: 150 of cap free
Amount of items: 2
Items: 
Size: 579966 Color: 12
Size: 419885 Color: 14

Bin 3207: 150 of cap free
Amount of items: 2
Items: 
Size: 650729 Color: 14
Size: 349122 Color: 13

Bin 3208: 151 of cap free
Amount of items: 2
Items: 
Size: 565453 Color: 0
Size: 434397 Color: 2

Bin 3209: 151 of cap free
Amount of items: 2
Items: 
Size: 543559 Color: 11
Size: 456291 Color: 4

Bin 3210: 151 of cap free
Amount of items: 2
Items: 
Size: 558222 Color: 8
Size: 441628 Color: 19

Bin 3211: 151 of cap free
Amount of items: 2
Items: 
Size: 606014 Color: 16
Size: 393836 Color: 6

Bin 3212: 151 of cap free
Amount of items: 2
Items: 
Size: 695052 Color: 16
Size: 304798 Color: 15

Bin 3213: 152 of cap free
Amount of items: 2
Items: 
Size: 693778 Color: 1
Size: 306071 Color: 17

Bin 3214: 152 of cap free
Amount of items: 2
Items: 
Size: 504120 Color: 6
Size: 495729 Color: 9

Bin 3215: 152 of cap free
Amount of items: 2
Items: 
Size: 526852 Color: 16
Size: 472997 Color: 7

Bin 3216: 152 of cap free
Amount of items: 2
Items: 
Size: 571262 Color: 17
Size: 428587 Color: 11

Bin 3217: 153 of cap free
Amount of items: 2
Items: 
Size: 563604 Color: 13
Size: 436244 Color: 3

Bin 3218: 153 of cap free
Amount of items: 2
Items: 
Size: 612089 Color: 5
Size: 387759 Color: 10

Bin 3219: 154 of cap free
Amount of items: 2
Items: 
Size: 608444 Color: 15
Size: 391403 Color: 13

Bin 3220: 154 of cap free
Amount of items: 2
Items: 
Size: 511947 Color: 17
Size: 487900 Color: 9

Bin 3221: 155 of cap free
Amount of items: 2
Items: 
Size: 503208 Color: 9
Size: 496638 Color: 17

Bin 3222: 155 of cap free
Amount of items: 2
Items: 
Size: 594506 Color: 2
Size: 405340 Color: 17

Bin 3223: 156 of cap free
Amount of items: 2
Items: 
Size: 696843 Color: 0
Size: 303002 Color: 2

Bin 3224: 156 of cap free
Amount of items: 2
Items: 
Size: 784892 Color: 5
Size: 214953 Color: 15

Bin 3225: 157 of cap free
Amount of items: 2
Items: 
Size: 571887 Color: 7
Size: 427957 Color: 6

Bin 3226: 157 of cap free
Amount of items: 2
Items: 
Size: 580289 Color: 10
Size: 419555 Color: 2

Bin 3227: 157 of cap free
Amount of items: 2
Items: 
Size: 647241 Color: 3
Size: 352603 Color: 12

Bin 3228: 157 of cap free
Amount of items: 2
Items: 
Size: 691391 Color: 0
Size: 308453 Color: 3

Bin 3229: 157 of cap free
Amount of items: 2
Items: 
Size: 776724 Color: 15
Size: 223120 Color: 19

Bin 3230: 158 of cap free
Amount of items: 2
Items: 
Size: 510859 Color: 17
Size: 488984 Color: 6

Bin 3231: 158 of cap free
Amount of items: 2
Items: 
Size: 560206 Color: 6
Size: 439637 Color: 16

Bin 3232: 158 of cap free
Amount of items: 2
Items: 
Size: 604836 Color: 0
Size: 395007 Color: 15

Bin 3233: 158 of cap free
Amount of items: 2
Items: 
Size: 618118 Color: 7
Size: 381725 Color: 14

Bin 3234: 158 of cap free
Amount of items: 2
Items: 
Size: 625834 Color: 1
Size: 374009 Color: 19

Bin 3235: 158 of cap free
Amount of items: 2
Items: 
Size: 653951 Color: 4
Size: 345892 Color: 3

Bin 3236: 158 of cap free
Amount of items: 2
Items: 
Size: 743124 Color: 12
Size: 256719 Color: 15

Bin 3237: 158 of cap free
Amount of items: 2
Items: 
Size: 727240 Color: 5
Size: 272603 Color: 15

Bin 3238: 158 of cap free
Amount of items: 2
Items: 
Size: 500386 Color: 5
Size: 499457 Color: 19

Bin 3239: 159 of cap free
Amount of items: 2
Items: 
Size: 748236 Color: 4
Size: 251606 Color: 14

Bin 3240: 159 of cap free
Amount of items: 2
Items: 
Size: 764722 Color: 3
Size: 235120 Color: 5

Bin 3241: 159 of cap free
Amount of items: 2
Items: 
Size: 558901 Color: 10
Size: 440941 Color: 13

Bin 3242: 159 of cap free
Amount of items: 2
Items: 
Size: 647422 Color: 18
Size: 352420 Color: 9

Bin 3243: 160 of cap free
Amount of items: 2
Items: 
Size: 688566 Color: 18
Size: 311275 Color: 5

Bin 3244: 160 of cap free
Amount of items: 2
Items: 
Size: 707224 Color: 14
Size: 292617 Color: 19

Bin 3245: 160 of cap free
Amount of items: 2
Items: 
Size: 706930 Color: 8
Size: 292911 Color: 0

Bin 3246: 160 of cap free
Amount of items: 2
Items: 
Size: 536868 Color: 5
Size: 462973 Color: 7

Bin 3247: 162 of cap free
Amount of items: 2
Items: 
Size: 769077 Color: 16
Size: 230762 Color: 8

Bin 3248: 162 of cap free
Amount of items: 2
Items: 
Size: 503807 Color: 18
Size: 496032 Color: 19

Bin 3249: 162 of cap free
Amount of items: 2
Items: 
Size: 715996 Color: 19
Size: 283843 Color: 5

Bin 3250: 162 of cap free
Amount of items: 2
Items: 
Size: 747579 Color: 11
Size: 252260 Color: 6

Bin 3251: 162 of cap free
Amount of items: 2
Items: 
Size: 792939 Color: 7
Size: 206900 Color: 5

Bin 3252: 162 of cap free
Amount of items: 2
Items: 
Size: 557800 Color: 6
Size: 442039 Color: 15

Bin 3253: 163 of cap free
Amount of items: 2
Items: 
Size: 731461 Color: 8
Size: 268377 Color: 17

Bin 3254: 164 of cap free
Amount of items: 2
Items: 
Size: 775808 Color: 0
Size: 224029 Color: 5

Bin 3255: 164 of cap free
Amount of items: 2
Items: 
Size: 522995 Color: 15
Size: 476842 Color: 10

Bin 3256: 164 of cap free
Amount of items: 2
Items: 
Size: 637781 Color: 9
Size: 362056 Color: 0

Bin 3257: 164 of cap free
Amount of items: 2
Items: 
Size: 673073 Color: 16
Size: 326764 Color: 13

Bin 3258: 164 of cap free
Amount of items: 2
Items: 
Size: 622631 Color: 11
Size: 377206 Color: 1

Bin 3259: 164 of cap free
Amount of items: 2
Items: 
Size: 534642 Color: 2
Size: 465195 Color: 14

Bin 3260: 165 of cap free
Amount of items: 2
Items: 
Size: 580462 Color: 17
Size: 419374 Color: 8

Bin 3261: 165 of cap free
Amount of items: 2
Items: 
Size: 500816 Color: 14
Size: 499020 Color: 5

Bin 3262: 165 of cap free
Amount of items: 2
Items: 
Size: 550446 Color: 8
Size: 449390 Color: 19

Bin 3263: 165 of cap free
Amount of items: 2
Items: 
Size: 572245 Color: 14
Size: 427591 Color: 9

Bin 3264: 165 of cap free
Amount of items: 2
Items: 
Size: 612591 Color: 3
Size: 387245 Color: 0

Bin 3265: 165 of cap free
Amount of items: 2
Items: 
Size: 643548 Color: 2
Size: 356288 Color: 14

Bin 3266: 165 of cap free
Amount of items: 2
Items: 
Size: 722801 Color: 17
Size: 277035 Color: 8

Bin 3267: 166 of cap free
Amount of items: 2
Items: 
Size: 530115 Color: 5
Size: 469720 Color: 18

Bin 3268: 166 of cap free
Amount of items: 2
Items: 
Size: 658758 Color: 10
Size: 341077 Color: 16

Bin 3269: 166 of cap free
Amount of items: 2
Items: 
Size: 655494 Color: 4
Size: 344341 Color: 1

Bin 3270: 167 of cap free
Amount of items: 2
Items: 
Size: 557251 Color: 14
Size: 442583 Color: 6

Bin 3271: 167 of cap free
Amount of items: 2
Items: 
Size: 506995 Color: 15
Size: 492839 Color: 2

Bin 3272: 167 of cap free
Amount of items: 2
Items: 
Size: 552243 Color: 3
Size: 447591 Color: 18

Bin 3273: 167 of cap free
Amount of items: 2
Items: 
Size: 565207 Color: 2
Size: 434627 Color: 10

Bin 3274: 167 of cap free
Amount of items: 2
Items: 
Size: 646233 Color: 17
Size: 353601 Color: 19

Bin 3275: 167 of cap free
Amount of items: 2
Items: 
Size: 797923 Color: 2
Size: 201911 Color: 3

Bin 3276: 168 of cap free
Amount of items: 2
Items: 
Size: 620910 Color: 12
Size: 378923 Color: 13

Bin 3277: 168 of cap free
Amount of items: 2
Items: 
Size: 520013 Color: 8
Size: 479820 Color: 3

Bin 3278: 168 of cap free
Amount of items: 2
Items: 
Size: 542823 Color: 13
Size: 457010 Color: 17

Bin 3279: 169 of cap free
Amount of items: 2
Items: 
Size: 512823 Color: 1
Size: 487009 Color: 2

Bin 3280: 169 of cap free
Amount of items: 2
Items: 
Size: 628531 Color: 2
Size: 371301 Color: 13

Bin 3281: 169 of cap free
Amount of items: 2
Items: 
Size: 677307 Color: 6
Size: 322525 Color: 14

Bin 3282: 169 of cap free
Amount of items: 2
Items: 
Size: 521396 Color: 11
Size: 478436 Color: 5

Bin 3283: 169 of cap free
Amount of items: 2
Items: 
Size: 712441 Color: 11
Size: 287391 Color: 13

Bin 3284: 169 of cap free
Amount of items: 2
Items: 
Size: 754489 Color: 9
Size: 245343 Color: 3

Bin 3285: 170 of cap free
Amount of items: 2
Items: 
Size: 658026 Color: 15
Size: 341805 Color: 8

Bin 3286: 170 of cap free
Amount of items: 2
Items: 
Size: 554768 Color: 13
Size: 445063 Color: 12

Bin 3287: 170 of cap free
Amount of items: 2
Items: 
Size: 502022 Color: 15
Size: 497809 Color: 17

Bin 3288: 171 of cap free
Amount of items: 2
Items: 
Size: 657379 Color: 13
Size: 342451 Color: 7

Bin 3289: 171 of cap free
Amount of items: 2
Items: 
Size: 632549 Color: 8
Size: 367281 Color: 13

Bin 3290: 171 of cap free
Amount of items: 2
Items: 
Size: 582286 Color: 3
Size: 417544 Color: 6

Bin 3291: 171 of cap free
Amount of items: 2
Items: 
Size: 707698 Color: 12
Size: 292132 Color: 7

Bin 3292: 172 of cap free
Amount of items: 2
Items: 
Size: 574225 Color: 5
Size: 425604 Color: 17

Bin 3293: 173 of cap free
Amount of items: 2
Items: 
Size: 762217 Color: 11
Size: 237611 Color: 8

Bin 3294: 174 of cap free
Amount of items: 2
Items: 
Size: 783648 Color: 6
Size: 216179 Color: 7

Bin 3295: 174 of cap free
Amount of items: 2
Items: 
Size: 541340 Color: 3
Size: 458487 Color: 6

Bin 3296: 174 of cap free
Amount of items: 2
Items: 
Size: 609406 Color: 17
Size: 390421 Color: 5

Bin 3297: 174 of cap free
Amount of items: 2
Items: 
Size: 631151 Color: 8
Size: 368676 Color: 16

Bin 3298: 174 of cap free
Amount of items: 2
Items: 
Size: 778955 Color: 13
Size: 220872 Color: 3

Bin 3299: 175 of cap free
Amount of items: 2
Items: 
Size: 791071 Color: 11
Size: 208755 Color: 15

Bin 3300: 175 of cap free
Amount of items: 2
Items: 
Size: 640096 Color: 11
Size: 359730 Color: 7

Bin 3301: 175 of cap free
Amount of items: 2
Items: 
Size: 652255 Color: 8
Size: 347571 Color: 15

Bin 3302: 175 of cap free
Amount of items: 2
Items: 
Size: 649771 Color: 0
Size: 350055 Color: 15

Bin 3303: 176 of cap free
Amount of items: 2
Items: 
Size: 512190 Color: 12
Size: 487635 Color: 17

Bin 3304: 176 of cap free
Amount of items: 2
Items: 
Size: 570764 Color: 5
Size: 429061 Color: 1

Bin 3305: 176 of cap free
Amount of items: 2
Items: 
Size: 715074 Color: 1
Size: 284751 Color: 2

Bin 3306: 176 of cap free
Amount of items: 2
Items: 
Size: 720257 Color: 17
Size: 279568 Color: 18

Bin 3307: 176 of cap free
Amount of items: 2
Items: 
Size: 746050 Color: 13
Size: 253775 Color: 14

Bin 3308: 177 of cap free
Amount of items: 2
Items: 
Size: 656197 Color: 18
Size: 343627 Color: 3

Bin 3309: 177 of cap free
Amount of items: 2
Items: 
Size: 659640 Color: 3
Size: 340184 Color: 18

Bin 3310: 178 of cap free
Amount of items: 2
Items: 
Size: 726376 Color: 13
Size: 273447 Color: 14

Bin 3311: 178 of cap free
Amount of items: 2
Items: 
Size: 761430 Color: 15
Size: 238393 Color: 9

Bin 3312: 178 of cap free
Amount of items: 2
Items: 
Size: 764759 Color: 5
Size: 235064 Color: 17

Bin 3313: 178 of cap free
Amount of items: 2
Items: 
Size: 782318 Color: 6
Size: 217505 Color: 19

Bin 3314: 179 of cap free
Amount of items: 2
Items: 
Size: 731769 Color: 12
Size: 268053 Color: 6

Bin 3315: 180 of cap free
Amount of items: 2
Items: 
Size: 641923 Color: 13
Size: 357898 Color: 1

Bin 3316: 180 of cap free
Amount of items: 2
Items: 
Size: 789032 Color: 16
Size: 210789 Color: 2

Bin 3317: 181 of cap free
Amount of items: 2
Items: 
Size: 746893 Color: 13
Size: 252927 Color: 9

Bin 3318: 181 of cap free
Amount of items: 2
Items: 
Size: 731981 Color: 1
Size: 267839 Color: 16

Bin 3319: 182 of cap free
Amount of items: 2
Items: 
Size: 573590 Color: 13
Size: 426229 Color: 17

Bin 3320: 182 of cap free
Amount of items: 2
Items: 
Size: 575621 Color: 19
Size: 424198 Color: 15

Bin 3321: 182 of cap free
Amount of items: 2
Items: 
Size: 722257 Color: 16
Size: 277562 Color: 7

Bin 3322: 183 of cap free
Amount of items: 2
Items: 
Size: 763063 Color: 5
Size: 236755 Color: 3

Bin 3323: 183 of cap free
Amount of items: 2
Items: 
Size: 522986 Color: 4
Size: 476832 Color: 0

Bin 3324: 183 of cap free
Amount of items: 2
Items: 
Size: 785762 Color: 3
Size: 214056 Color: 5

Bin 3325: 183 of cap free
Amount of items: 2
Items: 
Size: 782698 Color: 14
Size: 217120 Color: 9

Bin 3326: 184 of cap free
Amount of items: 2
Items: 
Size: 691382 Color: 5
Size: 308435 Color: 11

Bin 3327: 185 of cap free
Amount of items: 2
Items: 
Size: 663093 Color: 6
Size: 336723 Color: 8

Bin 3328: 186 of cap free
Amount of items: 2
Items: 
Size: 794347 Color: 18
Size: 205468 Color: 6

Bin 3329: 186 of cap free
Amount of items: 2
Items: 
Size: 732810 Color: 12
Size: 267005 Color: 6

Bin 3330: 186 of cap free
Amount of items: 2
Items: 
Size: 548784 Color: 0
Size: 451031 Color: 12

Bin 3331: 187 of cap free
Amount of items: 2
Items: 
Size: 547911 Color: 2
Size: 451903 Color: 11

Bin 3332: 187 of cap free
Amount of items: 2
Items: 
Size: 550847 Color: 6
Size: 448967 Color: 10

Bin 3333: 187 of cap free
Amount of items: 2
Items: 
Size: 596098 Color: 11
Size: 403716 Color: 17

Bin 3334: 187 of cap free
Amount of items: 2
Items: 
Size: 747790 Color: 4
Size: 252024 Color: 9

Bin 3335: 187 of cap free
Amount of items: 2
Items: 
Size: 640075 Color: 15
Size: 359739 Color: 11

Bin 3336: 188 of cap free
Amount of items: 2
Items: 
Size: 566841 Color: 3
Size: 432972 Color: 5

Bin 3337: 188 of cap free
Amount of items: 2
Items: 
Size: 723335 Color: 13
Size: 276478 Color: 4

Bin 3338: 188 of cap free
Amount of items: 2
Items: 
Size: 746048 Color: 2
Size: 253765 Color: 5

Bin 3339: 188 of cap free
Amount of items: 2
Items: 
Size: 761977 Color: 18
Size: 237836 Color: 6

Bin 3340: 188 of cap free
Amount of items: 2
Items: 
Size: 635743 Color: 15
Size: 364070 Color: 17

Bin 3341: 189 of cap free
Amount of items: 2
Items: 
Size: 578620 Color: 8
Size: 421192 Color: 1

Bin 3342: 190 of cap free
Amount of items: 2
Items: 
Size: 516579 Color: 14
Size: 483232 Color: 0

Bin 3343: 190 of cap free
Amount of items: 2
Items: 
Size: 586955 Color: 19
Size: 412856 Color: 12

Bin 3344: 190 of cap free
Amount of items: 2
Items: 
Size: 793405 Color: 9
Size: 206406 Color: 11

Bin 3345: 191 of cap free
Amount of items: 2
Items: 
Size: 715421 Color: 17
Size: 284389 Color: 13

Bin 3346: 191 of cap free
Amount of items: 2
Items: 
Size: 542432 Color: 13
Size: 457378 Color: 19

Bin 3347: 191 of cap free
Amount of items: 2
Items: 
Size: 585801 Color: 8
Size: 414009 Color: 9

Bin 3348: 191 of cap free
Amount of items: 2
Items: 
Size: 744447 Color: 15
Size: 255363 Color: 5

Bin 3349: 191 of cap free
Amount of items: 2
Items: 
Size: 578340 Color: 16
Size: 421470 Color: 15

Bin 3350: 192 of cap free
Amount of items: 2
Items: 
Size: 753539 Color: 1
Size: 246270 Color: 12

Bin 3351: 192 of cap free
Amount of items: 2
Items: 
Size: 588483 Color: 19
Size: 411326 Color: 5

Bin 3352: 192 of cap free
Amount of items: 2
Items: 
Size: 748679 Color: 17
Size: 251130 Color: 0

Bin 3353: 192 of cap free
Amount of items: 2
Items: 
Size: 590188 Color: 8
Size: 409621 Color: 0

Bin 3354: 192 of cap free
Amount of items: 2
Items: 
Size: 706644 Color: 12
Size: 293165 Color: 4

Bin 3355: 193 of cap free
Amount of items: 2
Items: 
Size: 712056 Color: 13
Size: 287752 Color: 5

Bin 3356: 193 of cap free
Amount of items: 2
Items: 
Size: 512370 Color: 13
Size: 487438 Color: 18

Bin 3357: 194 of cap free
Amount of items: 2
Items: 
Size: 510024 Color: 14
Size: 489783 Color: 0

Bin 3358: 194 of cap free
Amount of items: 2
Items: 
Size: 569904 Color: 15
Size: 429903 Color: 5

Bin 3359: 195 of cap free
Amount of items: 2
Items: 
Size: 674792 Color: 7
Size: 325014 Color: 19

Bin 3360: 196 of cap free
Amount of items: 2
Items: 
Size: 759106 Color: 4
Size: 240699 Color: 9

Bin 3361: 196 of cap free
Amount of items: 2
Items: 
Size: 507795 Color: 16
Size: 492010 Color: 0

Bin 3362: 196 of cap free
Amount of items: 2
Items: 
Size: 740112 Color: 19
Size: 259693 Color: 16

Bin 3363: 197 of cap free
Amount of items: 2
Items: 
Size: 764498 Color: 11
Size: 235306 Color: 10

Bin 3364: 197 of cap free
Amount of items: 2
Items: 
Size: 621340 Color: 8
Size: 378464 Color: 10

Bin 3365: 197 of cap free
Amount of items: 2
Items: 
Size: 508362 Color: 19
Size: 491442 Color: 14

Bin 3366: 197 of cap free
Amount of items: 2
Items: 
Size: 535330 Color: 18
Size: 464474 Color: 5

Bin 3367: 197 of cap free
Amount of items: 2
Items: 
Size: 612587 Color: 3
Size: 387217 Color: 6

Bin 3368: 197 of cap free
Amount of items: 2
Items: 
Size: 618436 Color: 19
Size: 381368 Color: 11

Bin 3369: 198 of cap free
Amount of items: 2
Items: 
Size: 613071 Color: 19
Size: 386732 Color: 3

Bin 3370: 198 of cap free
Amount of items: 2
Items: 
Size: 648491 Color: 3
Size: 351312 Color: 9

Bin 3371: 198 of cap free
Amount of items: 2
Items: 
Size: 752708 Color: 13
Size: 247095 Color: 6

Bin 3372: 199 of cap free
Amount of items: 3
Items: 
Size: 706912 Color: 15
Size: 148233 Color: 3
Size: 144657 Color: 6

Bin 3373: 200 of cap free
Amount of items: 2
Items: 
Size: 625532 Color: 19
Size: 374269 Color: 9

Bin 3374: 200 of cap free
Amount of items: 2
Items: 
Size: 505984 Color: 7
Size: 493817 Color: 10

Bin 3375: 200 of cap free
Amount of items: 2
Items: 
Size: 692790 Color: 17
Size: 307011 Color: 10

Bin 3376: 201 of cap free
Amount of items: 2
Items: 
Size: 693935 Color: 18
Size: 305865 Color: 15

Bin 3377: 201 of cap free
Amount of items: 2
Items: 
Size: 584459 Color: 3
Size: 415341 Color: 4

Bin 3378: 201 of cap free
Amount of items: 2
Items: 
Size: 593512 Color: 2
Size: 406288 Color: 8

Bin 3379: 201 of cap free
Amount of items: 2
Items: 
Size: 602999 Color: 8
Size: 396801 Color: 12

Bin 3380: 201 of cap free
Amount of items: 2
Items: 
Size: 608439 Color: 7
Size: 391361 Color: 16

Bin 3381: 201 of cap free
Amount of items: 2
Items: 
Size: 645506 Color: 9
Size: 354294 Color: 2

Bin 3382: 201 of cap free
Amount of items: 2
Items: 
Size: 660559 Color: 17
Size: 339241 Color: 10

Bin 3383: 201 of cap free
Amount of items: 2
Items: 
Size: 748882 Color: 17
Size: 250918 Color: 8

Bin 3384: 201 of cap free
Amount of items: 2
Items: 
Size: 505240 Color: 8
Size: 494560 Color: 17

Bin 3385: 201 of cap free
Amount of items: 2
Items: 
Size: 518716 Color: 8
Size: 481084 Color: 13

Bin 3386: 202 of cap free
Amount of items: 2
Items: 
Size: 749543 Color: 19
Size: 250256 Color: 8

Bin 3387: 202 of cap free
Amount of items: 2
Items: 
Size: 529171 Color: 9
Size: 470628 Color: 2

Bin 3388: 202 of cap free
Amount of items: 2
Items: 
Size: 532903 Color: 0
Size: 466896 Color: 13

Bin 3389: 202 of cap free
Amount of items: 2
Items: 
Size: 700698 Color: 2
Size: 299101 Color: 11

Bin 3390: 202 of cap free
Amount of items: 2
Items: 
Size: 610763 Color: 8
Size: 389036 Color: 11

Bin 3391: 203 of cap free
Amount of items: 2
Items: 
Size: 570232 Color: 7
Size: 429566 Color: 4

Bin 3392: 203 of cap free
Amount of items: 2
Items: 
Size: 626036 Color: 15
Size: 373762 Color: 18

Bin 3393: 204 of cap free
Amount of items: 2
Items: 
Size: 771770 Color: 17
Size: 228027 Color: 18

Bin 3394: 204 of cap free
Amount of items: 2
Items: 
Size: 559375 Color: 8
Size: 440422 Color: 10

Bin 3395: 205 of cap free
Amount of items: 2
Items: 
Size: 541041 Color: 19
Size: 458755 Color: 14

Bin 3396: 205 of cap free
Amount of items: 2
Items: 
Size: 578908 Color: 15
Size: 420888 Color: 11

Bin 3397: 205 of cap free
Amount of items: 2
Items: 
Size: 623287 Color: 15
Size: 376509 Color: 9

Bin 3398: 205 of cap free
Amount of items: 2
Items: 
Size: 711184 Color: 19
Size: 288612 Color: 5

Bin 3399: 205 of cap free
Amount of items: 2
Items: 
Size: 738134 Color: 5
Size: 261662 Color: 18

Bin 3400: 205 of cap free
Amount of items: 2
Items: 
Size: 636772 Color: 8
Size: 363024 Color: 3

Bin 3401: 206 of cap free
Amount of items: 2
Items: 
Size: 589562 Color: 12
Size: 410233 Color: 4

Bin 3402: 207 of cap free
Amount of items: 2
Items: 
Size: 679696 Color: 8
Size: 320098 Color: 12

Bin 3403: 208 of cap free
Amount of items: 2
Items: 
Size: 593007 Color: 13
Size: 406786 Color: 5

Bin 3404: 208 of cap free
Amount of items: 2
Items: 
Size: 681278 Color: 2
Size: 318515 Color: 9

Bin 3405: 209 of cap free
Amount of items: 2
Items: 
Size: 625567 Color: 9
Size: 374225 Color: 16

Bin 3406: 209 of cap free
Amount of items: 2
Items: 
Size: 680902 Color: 17
Size: 318890 Color: 18

Bin 3407: 209 of cap free
Amount of items: 2
Items: 
Size: 758374 Color: 16
Size: 241418 Color: 12

Bin 3408: 210 of cap free
Amount of items: 2
Items: 
Size: 763605 Color: 13
Size: 236186 Color: 16

Bin 3409: 210 of cap free
Amount of items: 2
Items: 
Size: 544841 Color: 3
Size: 454950 Color: 2

Bin 3410: 211 of cap free
Amount of items: 2
Items: 
Size: 601103 Color: 3
Size: 398687 Color: 0

Bin 3411: 211 of cap free
Amount of items: 2
Items: 
Size: 601826 Color: 17
Size: 397964 Color: 2

Bin 3412: 211 of cap free
Amount of items: 2
Items: 
Size: 647595 Color: 14
Size: 352195 Color: 1

Bin 3413: 212 of cap free
Amount of items: 2
Items: 
Size: 665797 Color: 1
Size: 333992 Color: 7

Bin 3414: 212 of cap free
Amount of items: 2
Items: 
Size: 527741 Color: 12
Size: 472048 Color: 16

Bin 3415: 212 of cap free
Amount of items: 2
Items: 
Size: 566825 Color: 5
Size: 432964 Color: 19

Bin 3416: 212 of cap free
Amount of items: 2
Items: 
Size: 577123 Color: 12
Size: 422666 Color: 1

Bin 3417: 212 of cap free
Amount of items: 2
Items: 
Size: 635023 Color: 19
Size: 364766 Color: 8

Bin 3418: 212 of cap free
Amount of items: 2
Items: 
Size: 697543 Color: 19
Size: 302246 Color: 2

Bin 3419: 213 of cap free
Amount of items: 2
Items: 
Size: 737727 Color: 13
Size: 262061 Color: 5

Bin 3420: 213 of cap free
Amount of items: 2
Items: 
Size: 580705 Color: 2
Size: 419083 Color: 13

Bin 3421: 213 of cap free
Amount of items: 2
Items: 
Size: 606007 Color: 6
Size: 393781 Color: 8

Bin 3422: 213 of cap free
Amount of items: 2
Items: 
Size: 750510 Color: 16
Size: 249278 Color: 6

Bin 3423: 214 of cap free
Amount of items: 2
Items: 
Size: 647207 Color: 10
Size: 352580 Color: 18

Bin 3424: 214 of cap free
Amount of items: 2
Items: 
Size: 785731 Color: 12
Size: 214056 Color: 15

Bin 3425: 215 of cap free
Amount of items: 2
Items: 
Size: 695001 Color: 0
Size: 304785 Color: 3

Bin 3426: 215 of cap free
Amount of items: 2
Items: 
Size: 550822 Color: 10
Size: 448964 Color: 18

Bin 3427: 216 of cap free
Amount of items: 2
Items: 
Size: 634742 Color: 8
Size: 365043 Color: 9

Bin 3428: 216 of cap free
Amount of items: 2
Items: 
Size: 714129 Color: 14
Size: 285656 Color: 8

Bin 3429: 216 of cap free
Amount of items: 2
Items: 
Size: 500048 Color: 5
Size: 499737 Color: 8

Bin 3430: 216 of cap free
Amount of items: 2
Items: 
Size: 513886 Color: 6
Size: 485899 Color: 12

Bin 3431: 217 of cap free
Amount of items: 2
Items: 
Size: 779202 Color: 3
Size: 220582 Color: 11

Bin 3432: 217 of cap free
Amount of items: 2
Items: 
Size: 575826 Color: 12
Size: 423958 Color: 6

Bin 3433: 217 of cap free
Amount of items: 2
Items: 
Size: 543544 Color: 6
Size: 456240 Color: 16

Bin 3434: 218 of cap free
Amount of items: 2
Items: 
Size: 565833 Color: 9
Size: 433950 Color: 0

Bin 3435: 218 of cap free
Amount of items: 2
Items: 
Size: 571601 Color: 5
Size: 428182 Color: 7

Bin 3436: 218 of cap free
Amount of items: 2
Items: 
Size: 639629 Color: 4
Size: 360154 Color: 13

Bin 3437: 218 of cap free
Amount of items: 2
Items: 
Size: 512366 Color: 17
Size: 487417 Color: 16

Bin 3438: 219 of cap free
Amount of items: 2
Items: 
Size: 755821 Color: 12
Size: 243961 Color: 7

Bin 3439: 220 of cap free
Amount of items: 2
Items: 
Size: 535311 Color: 8
Size: 464470 Color: 15

Bin 3440: 220 of cap free
Amount of items: 2
Items: 
Size: 585163 Color: 0
Size: 414618 Color: 13

Bin 3441: 220 of cap free
Amount of items: 2
Items: 
Size: 678675 Color: 15
Size: 321106 Color: 12

Bin 3442: 220 of cap free
Amount of items: 2
Items: 
Size: 545175 Color: 2
Size: 454606 Color: 13

Bin 3443: 221 of cap free
Amount of items: 2
Items: 
Size: 574222 Color: 11
Size: 425558 Color: 13

Bin 3444: 221 of cap free
Amount of items: 2
Items: 
Size: 586500 Color: 15
Size: 413280 Color: 17

Bin 3445: 221 of cap free
Amount of items: 2
Items: 
Size: 592648 Color: 5
Size: 407132 Color: 0

Bin 3446: 221 of cap free
Amount of items: 2
Items: 
Size: 619278 Color: 19
Size: 380502 Color: 0

Bin 3447: 221 of cap free
Amount of items: 2
Items: 
Size: 760944 Color: 12
Size: 238836 Color: 11

Bin 3448: 222 of cap free
Amount of items: 2
Items: 
Size: 618913 Color: 11
Size: 380866 Color: 6

Bin 3449: 222 of cap free
Amount of items: 2
Items: 
Size: 680567 Color: 8
Size: 319212 Color: 2

Bin 3450: 223 of cap free
Amount of items: 2
Items: 
Size: 564265 Color: 11
Size: 435513 Color: 16

Bin 3451: 223 of cap free
Amount of items: 2
Items: 
Size: 750129 Color: 12
Size: 249649 Color: 8

Bin 3452: 223 of cap free
Amount of items: 2
Items: 
Size: 538939 Color: 10
Size: 460839 Color: 11

Bin 3453: 223 of cap free
Amount of items: 2
Items: 
Size: 541669 Color: 5
Size: 458109 Color: 15

Bin 3454: 223 of cap free
Amount of items: 2
Items: 
Size: 617596 Color: 0
Size: 382182 Color: 13

Bin 3455: 223 of cap free
Amount of items: 2
Items: 
Size: 765571 Color: 3
Size: 234207 Color: 19

Bin 3456: 224 of cap free
Amount of items: 2
Items: 
Size: 786233 Color: 17
Size: 213544 Color: 19

Bin 3457: 224 of cap free
Amount of items: 2
Items: 
Size: 571873 Color: 15
Size: 427904 Color: 14

Bin 3458: 225 of cap free
Amount of items: 2
Items: 
Size: 618411 Color: 1
Size: 381365 Color: 19

Bin 3459: 225 of cap free
Amount of items: 2
Items: 
Size: 702838 Color: 6
Size: 296938 Color: 8

Bin 3460: 225 of cap free
Amount of items: 2
Items: 
Size: 712844 Color: 13
Size: 286932 Color: 4

Bin 3461: 225 of cap free
Amount of items: 2
Items: 
Size: 622235 Color: 17
Size: 377541 Color: 7

Bin 3462: 226 of cap free
Amount of items: 2
Items: 
Size: 695831 Color: 2
Size: 303944 Color: 6

Bin 3463: 226 of cap free
Amount of items: 2
Items: 
Size: 595068 Color: 8
Size: 404707 Color: 18

Bin 3464: 227 of cap free
Amount of items: 2
Items: 
Size: 581008 Color: 15
Size: 418766 Color: 5

Bin 3465: 227 of cap free
Amount of items: 2
Items: 
Size: 730087 Color: 17
Size: 269687 Color: 14

Bin 3466: 228 of cap free
Amount of items: 2
Items: 
Size: 773662 Color: 3
Size: 226111 Color: 6

Bin 3467: 229 of cap free
Amount of items: 2
Items: 
Size: 513878 Color: 10
Size: 485894 Color: 13

Bin 3468: 229 of cap free
Amount of items: 2
Items: 
Size: 598810 Color: 13
Size: 400962 Color: 7

Bin 3469: 230 of cap free
Amount of items: 2
Items: 
Size: 797910 Color: 8
Size: 201861 Color: 12

Bin 3470: 230 of cap free
Amount of items: 2
Items: 
Size: 544246 Color: 13
Size: 455525 Color: 5

Bin 3471: 231 of cap free
Amount of items: 2
Items: 
Size: 787750 Color: 5
Size: 212020 Color: 16

Bin 3472: 231 of cap free
Amount of items: 2
Items: 
Size: 528610 Color: 17
Size: 471160 Color: 0

Bin 3473: 231 of cap free
Amount of items: 2
Items: 
Size: 778574 Color: 2
Size: 221196 Color: 6

Bin 3474: 232 of cap free
Amount of items: 2
Items: 
Size: 641238 Color: 16
Size: 358531 Color: 14

Bin 3475: 232 of cap free
Amount of items: 2
Items: 
Size: 599855 Color: 4
Size: 399914 Color: 12

Bin 3476: 232 of cap free
Amount of items: 2
Items: 
Size: 604767 Color: 4
Size: 395002 Color: 2

Bin 3477: 233 of cap free
Amount of items: 2
Items: 
Size: 663542 Color: 1
Size: 336226 Color: 14

Bin 3478: 233 of cap free
Amount of items: 2
Items: 
Size: 602547 Color: 19
Size: 397221 Color: 9

Bin 3479: 233 of cap free
Amount of items: 2
Items: 
Size: 522734 Color: 11
Size: 477034 Color: 12

Bin 3480: 233 of cap free
Amount of items: 2
Items: 
Size: 525907 Color: 3
Size: 473861 Color: 18

Bin 3481: 233 of cap free
Amount of items: 2
Items: 
Size: 711603 Color: 15
Size: 288165 Color: 14

Bin 3482: 234 of cap free
Amount of items: 2
Items: 
Size: 519988 Color: 18
Size: 479779 Color: 9

Bin 3483: 234 of cap free
Amount of items: 2
Items: 
Size: 596086 Color: 8
Size: 403681 Color: 0

Bin 3484: 234 of cap free
Amount of items: 2
Items: 
Size: 546576 Color: 14
Size: 453191 Color: 15

Bin 3485: 234 of cap free
Amount of items: 2
Items: 
Size: 513456 Color: 15
Size: 486311 Color: 16

Bin 3486: 234 of cap free
Amount of items: 2
Items: 
Size: 724038 Color: 1
Size: 275729 Color: 4

Bin 3487: 235 of cap free
Amount of items: 2
Items: 
Size: 570237 Color: 4
Size: 429529 Color: 0

Bin 3488: 235 of cap free
Amount of items: 2
Items: 
Size: 560147 Color: 3
Size: 439619 Color: 14

Bin 3489: 235 of cap free
Amount of items: 2
Items: 
Size: 641904 Color: 19
Size: 357862 Color: 5

Bin 3490: 236 of cap free
Amount of items: 2
Items: 
Size: 600792 Color: 5
Size: 398973 Color: 9

Bin 3491: 236 of cap free
Amount of items: 2
Items: 
Size: 609395 Color: 11
Size: 390370 Color: 3

Bin 3492: 237 of cap free
Amount of items: 2
Items: 
Size: 705303 Color: 3
Size: 294461 Color: 2

Bin 3493: 237 of cap free
Amount of items: 2
Items: 
Size: 672055 Color: 10
Size: 327709 Color: 13

Bin 3494: 237 of cap free
Amount of items: 2
Items: 
Size: 531494 Color: 9
Size: 468270 Color: 0

Bin 3495: 237 of cap free
Amount of items: 2
Items: 
Size: 591956 Color: 13
Size: 407808 Color: 10

Bin 3496: 237 of cap free
Amount of items: 2
Items: 
Size: 597333 Color: 11
Size: 402431 Color: 5

Bin 3497: 237 of cap free
Amount of items: 2
Items: 
Size: 603234 Color: 16
Size: 396530 Color: 6

Bin 3498: 238 of cap free
Amount of items: 2
Items: 
Size: 707221 Color: 17
Size: 292542 Color: 12

Bin 3499: 239 of cap free
Amount of items: 2
Items: 
Size: 527751 Color: 17
Size: 472011 Color: 12

Bin 3500: 239 of cap free
Amount of items: 2
Items: 
Size: 555120 Color: 4
Size: 444642 Color: 1

Bin 3501: 239 of cap free
Amount of items: 2
Items: 
Size: 659613 Color: 7
Size: 340149 Color: 0

Bin 3502: 239 of cap free
Amount of items: 2
Items: 
Size: 518975 Color: 18
Size: 480787 Color: 12

Bin 3503: 239 of cap free
Amount of items: 2
Items: 
Size: 786853 Color: 4
Size: 212909 Color: 16

Bin 3504: 240 of cap free
Amount of items: 3
Items: 
Size: 680557 Color: 1
Size: 160275 Color: 16
Size: 158929 Color: 18

Bin 3505: 241 of cap free
Amount of items: 2
Items: 
Size: 617008 Color: 19
Size: 382752 Color: 7

Bin 3506: 241 of cap free
Amount of items: 2
Items: 
Size: 793822 Color: 6
Size: 205938 Color: 11

Bin 3507: 241 of cap free
Amount of items: 2
Items: 
Size: 782696 Color: 18
Size: 217064 Color: 12

Bin 3508: 242 of cap free
Amount of items: 2
Items: 
Size: 501996 Color: 4
Size: 497763 Color: 3

Bin 3509: 242 of cap free
Amount of items: 2
Items: 
Size: 680883 Color: 11
Size: 318876 Color: 10

Bin 3510: 242 of cap free
Amount of items: 2
Items: 
Size: 769442 Color: 15
Size: 230317 Color: 17

Bin 3511: 243 of cap free
Amount of items: 2
Items: 
Size: 724037 Color: 11
Size: 275721 Color: 17

Bin 3512: 243 of cap free
Amount of items: 2
Items: 
Size: 615682 Color: 5
Size: 384076 Color: 7

Bin 3513: 243 of cap free
Amount of items: 2
Items: 
Size: 712867 Color: 4
Size: 286891 Color: 0

Bin 3514: 243 of cap free
Amount of items: 2
Items: 
Size: 700281 Color: 18
Size: 299477 Color: 10

Bin 3515: 244 of cap free
Amount of items: 2
Items: 
Size: 749037 Color: 8
Size: 250720 Color: 14

Bin 3516: 244 of cap free
Amount of items: 2
Items: 
Size: 792935 Color: 12
Size: 206822 Color: 16

Bin 3517: 245 of cap free
Amount of items: 2
Items: 
Size: 783391 Color: 14
Size: 216365 Color: 17

Bin 3518: 245 of cap free
Amount of items: 2
Items: 
Size: 592644 Color: 6
Size: 407112 Color: 2

Bin 3519: 245 of cap free
Amount of items: 2
Items: 
Size: 728931 Color: 11
Size: 270825 Color: 5

Bin 3520: 245 of cap free
Amount of items: 2
Items: 
Size: 682667 Color: 1
Size: 317089 Color: 12

Bin 3521: 246 of cap free
Amount of items: 2
Items: 
Size: 583572 Color: 16
Size: 416183 Color: 9

Bin 3522: 246 of cap free
Amount of items: 2
Items: 
Size: 675831 Color: 9
Size: 323924 Color: 11

Bin 3523: 247 of cap free
Amount of items: 2
Items: 
Size: 788765 Color: 4
Size: 210989 Color: 5

Bin 3524: 247 of cap free
Amount of items: 2
Items: 
Size: 768347 Color: 10
Size: 231407 Color: 19

Bin 3525: 248 of cap free
Amount of items: 2
Items: 
Size: 776664 Color: 3
Size: 223089 Color: 19

Bin 3526: 248 of cap free
Amount of items: 2
Items: 
Size: 536367 Color: 0
Size: 463386 Color: 6

Bin 3527: 249 of cap free
Amount of items: 2
Items: 
Size: 767638 Color: 12
Size: 232114 Color: 14

Bin 3528: 249 of cap free
Amount of items: 2
Items: 
Size: 713147 Color: 13
Size: 286605 Color: 2

Bin 3529: 249 of cap free
Amount of items: 2
Items: 
Size: 663919 Color: 8
Size: 335833 Color: 2

Bin 3530: 250 of cap free
Amount of items: 2
Items: 
Size: 546069 Color: 10
Size: 453682 Color: 14

Bin 3531: 250 of cap free
Amount of items: 2
Items: 
Size: 714109 Color: 6
Size: 285642 Color: 4

Bin 3532: 251 of cap free
Amount of items: 2
Items: 
Size: 701540 Color: 19
Size: 298210 Color: 13

Bin 3533: 251 of cap free
Amount of items: 2
Items: 
Size: 623659 Color: 3
Size: 376091 Color: 9

Bin 3534: 252 of cap free
Amount of items: 2
Items: 
Size: 790562 Color: 8
Size: 209187 Color: 11

Bin 3535: 253 of cap free
Amount of items: 2
Items: 
Size: 773647 Color: 8
Size: 226101 Color: 16

Bin 3536: 253 of cap free
Amount of items: 2
Items: 
Size: 702189 Color: 2
Size: 297559 Color: 7

Bin 3537: 253 of cap free
Amount of items: 2
Items: 
Size: 531061 Color: 8
Size: 468687 Color: 12

Bin 3538: 253 of cap free
Amount of items: 2
Items: 
Size: 668041 Color: 16
Size: 331707 Color: 7

Bin 3539: 253 of cap free
Amount of items: 2
Items: 
Size: 691368 Color: 19
Size: 308380 Color: 7

Bin 3540: 254 of cap free
Amount of items: 2
Items: 
Size: 605986 Color: 12
Size: 393761 Color: 6

Bin 3541: 254 of cap free
Amount of items: 2
Items: 
Size: 629567 Color: 16
Size: 370180 Color: 7

Bin 3542: 254 of cap free
Amount of items: 2
Items: 
Size: 535561 Color: 9
Size: 464186 Color: 19

Bin 3543: 255 of cap free
Amount of items: 2
Items: 
Size: 735778 Color: 12
Size: 263968 Color: 7

Bin 3544: 255 of cap free
Amount of items: 2
Items: 
Size: 734622 Color: 7
Size: 265124 Color: 6

Bin 3545: 257 of cap free
Amount of items: 2
Items: 
Size: 538587 Color: 18
Size: 461157 Color: 19

Bin 3546: 258 of cap free
Amount of items: 2
Items: 
Size: 790988 Color: 4
Size: 208755 Color: 3

Bin 3547: 258 of cap free
Amount of items: 2
Items: 
Size: 502697 Color: 7
Size: 497046 Color: 4

Bin 3548: 259 of cap free
Amount of items: 2
Items: 
Size: 731421 Color: 7
Size: 268321 Color: 15

Bin 3549: 260 of cap free
Amount of items: 2
Items: 
Size: 701219 Color: 1
Size: 298522 Color: 10

Bin 3550: 261 of cap free
Amount of items: 2
Items: 
Size: 755818 Color: 10
Size: 243922 Color: 9

Bin 3551: 263 of cap free
Amount of items: 2
Items: 
Size: 501425 Color: 12
Size: 498313 Color: 2

Bin 3552: 263 of cap free
Amount of items: 2
Items: 
Size: 732773 Color: 17
Size: 266965 Color: 7

Bin 3553: 263 of cap free
Amount of items: 2
Items: 
Size: 625128 Color: 2
Size: 374610 Color: 18

Bin 3554: 264 of cap free
Amount of items: 3
Items: 
Size: 660886 Color: 6
Size: 171316 Color: 4
Size: 167535 Color: 4

Bin 3555: 264 of cap free
Amount of items: 2
Items: 
Size: 582923 Color: 12
Size: 416814 Color: 9

Bin 3556: 264 of cap free
Amount of items: 2
Items: 
Size: 702825 Color: 19
Size: 296912 Color: 5

Bin 3557: 264 of cap free
Amount of items: 2
Items: 
Size: 656911 Color: 11
Size: 342826 Color: 7

Bin 3558: 265 of cap free
Amount of items: 2
Items: 
Size: 759410 Color: 9
Size: 240326 Color: 3

Bin 3559: 265 of cap free
Amount of items: 2
Items: 
Size: 657946 Color: 8
Size: 341790 Color: 14

Bin 3560: 266 of cap free
Amount of items: 2
Items: 
Size: 686860 Color: 12
Size: 312875 Color: 0

Bin 3561: 267 of cap free
Amount of items: 2
Items: 
Size: 567632 Color: 2
Size: 432102 Color: 16

Bin 3562: 267 of cap free
Amount of items: 2
Items: 
Size: 511382 Color: 13
Size: 488352 Color: 3

Bin 3563: 267 of cap free
Amount of items: 2
Items: 
Size: 569842 Color: 2
Size: 429892 Color: 17

Bin 3564: 267 of cap free
Amount of items: 2
Items: 
Size: 576074 Color: 3
Size: 423660 Color: 4

Bin 3565: 267 of cap free
Amount of items: 2
Items: 
Size: 626578 Color: 17
Size: 373156 Color: 14

Bin 3566: 267 of cap free
Amount of items: 2
Items: 
Size: 674416 Color: 17
Size: 325318 Color: 12

Bin 3567: 268 of cap free
Amount of items: 2
Items: 
Size: 658975 Color: 8
Size: 340758 Color: 6

Bin 3568: 268 of cap free
Amount of items: 2
Items: 
Size: 705638 Color: 0
Size: 294095 Color: 14

Bin 3569: 269 of cap free
Amount of items: 2
Items: 
Size: 782684 Color: 0
Size: 217048 Color: 12

Bin 3570: 269 of cap free
Amount of items: 2
Items: 
Size: 569042 Color: 8
Size: 430690 Color: 18

Bin 3571: 270 of cap free
Amount of items: 2
Items: 
Size: 581467 Color: 0
Size: 418264 Color: 13

Bin 3572: 270 of cap free
Amount of items: 2
Items: 
Size: 582547 Color: 14
Size: 417184 Color: 15

Bin 3573: 271 of cap free
Amount of items: 2
Items: 
Size: 531061 Color: 4
Size: 468669 Color: 5

Bin 3574: 271 of cap free
Amount of items: 2
Items: 
Size: 662792 Color: 12
Size: 336938 Color: 9

Bin 3575: 271 of cap free
Amount of items: 2
Items: 
Size: 610229 Color: 18
Size: 389501 Color: 0

Bin 3576: 274 of cap free
Amount of items: 2
Items: 
Size: 752644 Color: 1
Size: 247083 Color: 16

Bin 3577: 274 of cap free
Amount of items: 2
Items: 
Size: 661888 Color: 8
Size: 337839 Color: 18

Bin 3578: 277 of cap free
Amount of items: 2
Items: 
Size: 524040 Color: 13
Size: 475684 Color: 19

Bin 3579: 278 of cap free
Amount of items: 2
Items: 
Size: 535550 Color: 2
Size: 464173 Color: 11

Bin 3580: 279 of cap free
Amount of items: 2
Items: 
Size: 698934 Color: 0
Size: 300788 Color: 3

Bin 3581: 279 of cap free
Amount of items: 2
Items: 
Size: 734612 Color: 13
Size: 265110 Color: 15

Bin 3582: 279 of cap free
Amount of items: 2
Items: 
Size: 566804 Color: 5
Size: 432918 Color: 4

Bin 3583: 279 of cap free
Amount of items: 2
Items: 
Size: 727714 Color: 1
Size: 272008 Color: 13

Bin 3584: 280 of cap free
Amount of items: 2
Items: 
Size: 785122 Color: 1
Size: 214599 Color: 5

Bin 3585: 281 of cap free
Amount of items: 2
Items: 
Size: 694678 Color: 17
Size: 305042 Color: 9

Bin 3586: 282 of cap free
Amount of items: 2
Items: 
Size: 580982 Color: 10
Size: 418737 Color: 18

Bin 3587: 282 of cap free
Amount of items: 2
Items: 
Size: 525879 Color: 18
Size: 473840 Color: 5

Bin 3588: 282 of cap free
Amount of items: 2
Items: 
Size: 562473 Color: 10
Size: 437246 Color: 13

Bin 3589: 282 of cap free
Amount of items: 2
Items: 
Size: 568196 Color: 8
Size: 431523 Color: 6

Bin 3590: 283 of cap free
Amount of items: 2
Items: 
Size: 700248 Color: 8
Size: 299470 Color: 2

Bin 3591: 284 of cap free
Amount of items: 2
Items: 
Size: 690657 Color: 16
Size: 309060 Color: 14

Bin 3592: 285 of cap free
Amount of items: 2
Items: 
Size: 553266 Color: 12
Size: 446450 Color: 10

Bin 3593: 285 of cap free
Amount of items: 2
Items: 
Size: 538893 Color: 5
Size: 460823 Color: 1

Bin 3594: 285 of cap free
Amount of items: 2
Items: 
Size: 602522 Color: 16
Size: 397194 Color: 15

Bin 3595: 285 of cap free
Amount of items: 2
Items: 
Size: 624590 Color: 5
Size: 375126 Color: 4

Bin 3596: 285 of cap free
Amount of items: 2
Items: 
Size: 753478 Color: 9
Size: 246238 Color: 6

Bin 3597: 286 of cap free
Amount of items: 2
Items: 
Size: 777735 Color: 11
Size: 221980 Color: 16

Bin 3598: 287 of cap free
Amount of items: 2
Items: 
Size: 526819 Color: 8
Size: 472895 Color: 6

Bin 3599: 288 of cap free
Amount of items: 2
Items: 
Size: 755815 Color: 3
Size: 243898 Color: 11

Bin 3600: 289 of cap free
Amount of items: 2
Items: 
Size: 636689 Color: 1
Size: 363023 Color: 3

Bin 3601: 289 of cap free
Amount of items: 2
Items: 
Size: 566358 Color: 0
Size: 433354 Color: 7

Bin 3602: 289 of cap free
Amount of items: 2
Items: 
Size: 626572 Color: 4
Size: 373140 Color: 17

Bin 3603: 289 of cap free
Amount of items: 2
Items: 
Size: 673563 Color: 5
Size: 326149 Color: 9

Bin 3604: 289 of cap free
Amount of items: 2
Items: 
Size: 715896 Color: 11
Size: 283816 Color: 5

Bin 3605: 290 of cap free
Amount of items: 2
Items: 
Size: 759387 Color: 4
Size: 240324 Color: 12

Bin 3606: 290 of cap free
Amount of items: 2
Items: 
Size: 707188 Color: 5
Size: 292523 Color: 12

Bin 3607: 291 of cap free
Amount of items: 2
Items: 
Size: 551241 Color: 15
Size: 448469 Color: 6

Bin 3608: 291 of cap free
Amount of items: 2
Items: 
Size: 735774 Color: 9
Size: 263936 Color: 12

Bin 3609: 291 of cap free
Amount of items: 2
Items: 
Size: 628425 Color: 0
Size: 371285 Color: 3

Bin 3610: 292 of cap free
Amount of items: 2
Items: 
Size: 648425 Color: 4
Size: 351284 Color: 5

Bin 3611: 292 of cap free
Amount of items: 2
Items: 
Size: 603551 Color: 4
Size: 396158 Color: 3

Bin 3612: 293 of cap free
Amount of items: 2
Items: 
Size: 503203 Color: 17
Size: 496505 Color: 19

Bin 3613: 294 of cap free
Amount of items: 2
Items: 
Size: 716225 Color: 17
Size: 283482 Color: 1

Bin 3614: 294 of cap free
Amount of items: 2
Items: 
Size: 738750 Color: 19
Size: 260957 Color: 8

Bin 3615: 294 of cap free
Amount of items: 2
Items: 
Size: 567613 Color: 14
Size: 432094 Color: 16

Bin 3616: 295 of cap free
Amount of items: 2
Items: 
Size: 798558 Color: 16
Size: 201148 Color: 9

Bin 3617: 296 of cap free
Amount of items: 2
Items: 
Size: 783639 Color: 4
Size: 216066 Color: 2

Bin 3618: 296 of cap free
Amount of items: 2
Items: 
Size: 780876 Color: 8
Size: 218829 Color: 15

Bin 3619: 298 of cap free
Amount of items: 3
Items: 
Size: 623964 Color: 1
Size: 188076 Color: 16
Size: 187663 Color: 17

Bin 3620: 298 of cap free
Amount of items: 2
Items: 
Size: 645072 Color: 8
Size: 354631 Color: 16

Bin 3621: 298 of cap free
Amount of items: 2
Items: 
Size: 603547 Color: 8
Size: 396156 Color: 9

Bin 3622: 299 of cap free
Amount of items: 2
Items: 
Size: 521372 Color: 6
Size: 478330 Color: 12

Bin 3623: 299 of cap free
Amount of items: 2
Items: 
Size: 668308 Color: 9
Size: 331394 Color: 8

Bin 3624: 299 of cap free
Amount of items: 2
Items: 
Size: 610209 Color: 9
Size: 389493 Color: 14

Bin 3625: 300 of cap free
Amount of items: 2
Items: 
Size: 657945 Color: 9
Size: 341756 Color: 13

Bin 3626: 301 of cap free
Amount of items: 2
Items: 
Size: 681856 Color: 5
Size: 317844 Color: 12

Bin 3627: 301 of cap free
Amount of items: 2
Items: 
Size: 593478 Color: 12
Size: 406222 Color: 3

Bin 3628: 302 of cap free
Amount of items: 2
Items: 
Size: 750119 Color: 8
Size: 249580 Color: 15

Bin 3629: 303 of cap free
Amount of items: 2
Items: 
Size: 784537 Color: 9
Size: 215161 Color: 7

Bin 3630: 305 of cap free
Amount of items: 2
Items: 
Size: 676652 Color: 9
Size: 323044 Color: 0

Bin 3631: 305 of cap free
Amount of items: 2
Items: 
Size: 556594 Color: 5
Size: 443102 Color: 4

Bin 3632: 305 of cap free
Amount of items: 2
Items: 
Size: 563201 Color: 8
Size: 436495 Color: 7

Bin 3633: 306 of cap free
Amount of items: 2
Items: 
Size: 521678 Color: 3
Size: 478017 Color: 2

Bin 3634: 306 of cap free
Amount of items: 2
Items: 
Size: 517520 Color: 18
Size: 482175 Color: 14

Bin 3635: 306 of cap free
Amount of items: 2
Items: 
Size: 644479 Color: 15
Size: 355216 Color: 17

Bin 3636: 307 of cap free
Amount of items: 2
Items: 
Size: 738051 Color: 4
Size: 261643 Color: 15

Bin 3637: 307 of cap free
Amount of items: 2
Items: 
Size: 537765 Color: 12
Size: 461929 Color: 15

Bin 3638: 309 of cap free
Amount of items: 2
Items: 
Size: 751413 Color: 18
Size: 248279 Color: 2

Bin 3639: 310 of cap free
Amount of items: 2
Items: 
Size: 758312 Color: 4
Size: 241379 Color: 15

Bin 3640: 310 of cap free
Amount of items: 2
Items: 
Size: 564759 Color: 13
Size: 434932 Color: 18

Bin 3641: 310 of cap free
Amount of items: 2
Items: 
Size: 706085 Color: 11
Size: 293606 Color: 0

Bin 3642: 310 of cap free
Amount of items: 2
Items: 
Size: 596083 Color: 10
Size: 403608 Color: 9

Bin 3643: 311 of cap free
Amount of items: 2
Items: 
Size: 608778 Color: 15
Size: 390912 Color: 10

Bin 3644: 311 of cap free
Amount of items: 2
Items: 
Size: 696721 Color: 3
Size: 302969 Color: 9

Bin 3645: 311 of cap free
Amount of items: 2
Items: 
Size: 510803 Color: 14
Size: 488887 Color: 8

Bin 3646: 312 of cap free
Amount of items: 2
Items: 
Size: 639552 Color: 12
Size: 360137 Color: 1

Bin 3647: 312 of cap free
Amount of items: 2
Items: 
Size: 500776 Color: 12
Size: 498913 Color: 6

Bin 3648: 313 of cap free
Amount of items: 2
Items: 
Size: 663497 Color: 16
Size: 336191 Color: 8

Bin 3649: 313 of cap free
Amount of items: 2
Items: 
Size: 625454 Color: 12
Size: 374234 Color: 9

Bin 3650: 314 of cap free
Amount of items: 2
Items: 
Size: 790028 Color: 13
Size: 209659 Color: 12

Bin 3651: 314 of cap free
Amount of items: 2
Items: 
Size: 601730 Color: 11
Size: 397957 Color: 1

Bin 3652: 316 of cap free
Amount of items: 2
Items: 
Size: 744337 Color: 9
Size: 255348 Color: 6

Bin 3653: 316 of cap free
Amount of items: 2
Items: 
Size: 794848 Color: 17
Size: 204837 Color: 13

Bin 3654: 317 of cap free
Amount of items: 2
Items: 
Size: 510443 Color: 14
Size: 489241 Color: 9

Bin 3655: 318 of cap free
Amount of items: 2
Items: 
Size: 598611 Color: 5
Size: 401072 Color: 13

Bin 3656: 319 of cap free
Amount of items: 2
Items: 
Size: 643041 Color: 8
Size: 356641 Color: 9

Bin 3657: 319 of cap free
Amount of items: 2
Items: 
Size: 730967 Color: 17
Size: 268715 Color: 1

Bin 3658: 320 of cap free
Amount of items: 2
Items: 
Size: 763912 Color: 7
Size: 235769 Color: 19

Bin 3659: 321 of cap free
Amount of items: 2
Items: 
Size: 505468 Color: 4
Size: 494212 Color: 6

Bin 3660: 322 of cap free
Amount of items: 2
Items: 
Size: 665821 Color: 7
Size: 333858 Color: 16

Bin 3661: 323 of cap free
Amount of items: 2
Items: 
Size: 610771 Color: 11
Size: 388907 Color: 7

Bin 3662: 324 of cap free
Amount of items: 2
Items: 
Size: 787697 Color: 10
Size: 211980 Color: 11

Bin 3663: 324 of cap free
Amount of items: 2
Items: 
Size: 659542 Color: 9
Size: 340135 Color: 3

Bin 3664: 324 of cap free
Amount of items: 2
Items: 
Size: 557161 Color: 8
Size: 442516 Color: 19

Bin 3665: 325 of cap free
Amount of items: 2
Items: 
Size: 552211 Color: 6
Size: 447465 Color: 8

Bin 3666: 325 of cap free
Amount of items: 2
Items: 
Size: 760177 Color: 14
Size: 239499 Color: 11

Bin 3667: 326 of cap free
Amount of items: 2
Items: 
Size: 609371 Color: 19
Size: 390304 Color: 3

Bin 3668: 326 of cap free
Amount of items: 2
Items: 
Size: 510738 Color: 9
Size: 488937 Color: 14

Bin 3669: 326 of cap free
Amount of items: 2
Items: 
Size: 552895 Color: 3
Size: 446780 Color: 8

Bin 3670: 328 of cap free
Amount of items: 2
Items: 
Size: 646589 Color: 8
Size: 353084 Color: 12

Bin 3671: 328 of cap free
Amount of items: 2
Items: 
Size: 711537 Color: 19
Size: 288136 Color: 16

Bin 3672: 328 of cap free
Amount of items: 2
Items: 
Size: 678143 Color: 8
Size: 321530 Color: 13

Bin 3673: 328 of cap free
Amount of items: 2
Items: 
Size: 591170 Color: 9
Size: 408503 Color: 0

Bin 3674: 329 of cap free
Amount of items: 2
Items: 
Size: 684863 Color: 10
Size: 314809 Color: 15

Bin 3675: 329 of cap free
Amount of items: 2
Items: 
Size: 792928 Color: 1
Size: 206744 Color: 5

Bin 3676: 330 of cap free
Amount of items: 2
Items: 
Size: 543440 Color: 13
Size: 456231 Color: 9

Bin 3677: 331 of cap free
Amount of items: 2
Items: 
Size: 752643 Color: 4
Size: 247027 Color: 14

Bin 3678: 331 of cap free
Amount of items: 2
Items: 
Size: 562021 Color: 9
Size: 437649 Color: 16

Bin 3679: 332 of cap free
Amount of items: 3
Items: 
Size: 344264 Color: 16
Size: 327708 Color: 7
Size: 327697 Color: 14

Bin 3680: 333 of cap free
Amount of items: 2
Items: 
Size: 605974 Color: 1
Size: 393694 Color: 10

Bin 3681: 334 of cap free
Amount of items: 2
Items: 
Size: 745588 Color: 2
Size: 254079 Color: 4

Bin 3682: 334 of cap free
Amount of items: 2
Items: 
Size: 732192 Color: 9
Size: 267475 Color: 0

Bin 3683: 335 of cap free
Amount of items: 2
Items: 
Size: 564121 Color: 16
Size: 435545 Color: 7

Bin 3684: 335 of cap free
Amount of items: 2
Items: 
Size: 622532 Color: 17
Size: 377134 Color: 11

Bin 3685: 336 of cap free
Amount of items: 2
Items: 
Size: 633303 Color: 17
Size: 366362 Color: 16

Bin 3686: 336 of cap free
Amount of items: 2
Items: 
Size: 597290 Color: 12
Size: 402375 Color: 7

Bin 3687: 336 of cap free
Amount of items: 2
Items: 
Size: 754704 Color: 3
Size: 244961 Color: 14

Bin 3688: 337 of cap free
Amount of items: 2
Items: 
Size: 692748 Color: 6
Size: 306916 Color: 5

Bin 3689: 338 of cap free
Amount of items: 2
Items: 
Size: 543901 Color: 2
Size: 455762 Color: 8

Bin 3690: 340 of cap free
Amount of items: 2
Items: 
Size: 690655 Color: 15
Size: 309006 Color: 0

Bin 3691: 340 of cap free
Amount of items: 2
Items: 
Size: 798276 Color: 8
Size: 201385 Color: 16

Bin 3692: 340 of cap free
Amount of items: 2
Items: 
Size: 579507 Color: 6
Size: 420154 Color: 0

Bin 3693: 341 of cap free
Amount of items: 2
Items: 
Size: 755812 Color: 9
Size: 243848 Color: 5

Bin 3694: 341 of cap free
Amount of items: 2
Items: 
Size: 790496 Color: 9
Size: 209164 Color: 12

Bin 3695: 343 of cap free
Amount of items: 2
Items: 
Size: 533927 Color: 16
Size: 465731 Color: 4

Bin 3696: 344 of cap free
Amount of items: 2
Items: 
Size: 527190 Color: 1
Size: 472467 Color: 6

Bin 3697: 344 of cap free
Amount of items: 2
Items: 
Size: 507709 Color: 13
Size: 491948 Color: 1

Bin 3698: 344 of cap free
Amount of items: 2
Items: 
Size: 523983 Color: 2
Size: 475674 Color: 13

Bin 3699: 344 of cap free
Amount of items: 2
Items: 
Size: 726697 Color: 18
Size: 272960 Color: 19

Bin 3700: 345 of cap free
Amount of items: 2
Items: 
Size: 513372 Color: 8
Size: 486284 Color: 16

Bin 3701: 347 of cap free
Amount of items: 2
Items: 
Size: 626539 Color: 16
Size: 373115 Color: 8

Bin 3702: 348 of cap free
Amount of items: 2
Items: 
Size: 790142 Color: 12
Size: 209511 Color: 17

Bin 3703: 348 of cap free
Amount of items: 2
Items: 
Size: 693883 Color: 13
Size: 305770 Color: 1

Bin 3704: 349 of cap free
Amount of items: 2
Items: 
Size: 689369 Color: 12
Size: 310283 Color: 10

Bin 3705: 349 of cap free
Amount of items: 2
Items: 
Size: 500754 Color: 2
Size: 498898 Color: 16

Bin 3706: 350 of cap free
Amount of items: 2
Items: 
Size: 575221 Color: 10
Size: 424430 Color: 15

Bin 3707: 350 of cap free
Amount of items: 2
Items: 
Size: 584755 Color: 4
Size: 414896 Color: 9

Bin 3708: 351 of cap free
Amount of items: 2
Items: 
Size: 778383 Color: 18
Size: 221267 Color: 2

Bin 3709: 351 of cap free
Amount of items: 2
Items: 
Size: 515918 Color: 12
Size: 483732 Color: 5

Bin 3710: 351 of cap free
Amount of items: 2
Items: 
Size: 536294 Color: 6
Size: 463356 Color: 13

Bin 3711: 352 of cap free
Amount of items: 2
Items: 
Size: 675757 Color: 8
Size: 323892 Color: 15

Bin 3712: 353 of cap free
Amount of items: 2
Items: 
Size: 545976 Color: 13
Size: 453672 Color: 4

Bin 3713: 354 of cap free
Amount of items: 2
Items: 
Size: 790492 Color: 5
Size: 209155 Color: 19

Bin 3714: 355 of cap free
Amount of items: 2
Items: 
Size: 701140 Color: 13
Size: 298506 Color: 18

Bin 3715: 357 of cap free
Amount of items: 2
Items: 
Size: 617512 Color: 9
Size: 382132 Color: 4

Bin 3716: 357 of cap free
Amount of items: 2
Items: 
Size: 725575 Color: 5
Size: 274069 Color: 11

Bin 3717: 358 of cap free
Amount of items: 2
Items: 
Size: 549377 Color: 0
Size: 450266 Color: 16

Bin 3718: 359 of cap free
Amount of items: 2
Items: 
Size: 793742 Color: 6
Size: 205900 Color: 18

Bin 3719: 359 of cap free
Amount of items: 2
Items: 
Size: 775804 Color: 9
Size: 223838 Color: 3

Bin 3720: 359 of cap free
Amount of items: 2
Items: 
Size: 636654 Color: 19
Size: 362988 Color: 2

Bin 3721: 359 of cap free
Amount of items: 2
Items: 
Size: 562459 Color: 15
Size: 437183 Color: 19

Bin 3722: 359 of cap free
Amount of items: 2
Items: 
Size: 676598 Color: 2
Size: 323044 Color: 9

Bin 3723: 360 of cap free
Amount of items: 2
Items: 
Size: 770595 Color: 15
Size: 229046 Color: 13

Bin 3724: 360 of cap free
Amount of items: 2
Items: 
Size: 737235 Color: 11
Size: 262406 Color: 1

Bin 3725: 362 of cap free
Amount of items: 2
Items: 
Size: 604029 Color: 3
Size: 395610 Color: 15

Bin 3726: 363 of cap free
Amount of items: 2
Items: 
Size: 560990 Color: 6
Size: 438648 Color: 14

Bin 3727: 363 of cap free
Amount of items: 2
Items: 
Size: 550714 Color: 10
Size: 448924 Color: 15

Bin 3728: 363 of cap free
Amount of items: 2
Items: 
Size: 736125 Color: 10
Size: 263513 Color: 4

Bin 3729: 364 of cap free
Amount of items: 2
Items: 
Size: 577013 Color: 1
Size: 422624 Color: 14

Bin 3730: 365 of cap free
Amount of items: 2
Items: 
Size: 661916 Color: 9
Size: 337720 Color: 8

Bin 3731: 365 of cap free
Amount of items: 2
Items: 
Size: 616147 Color: 4
Size: 383489 Color: 5

Bin 3732: 366 of cap free
Amount of items: 2
Items: 
Size: 657880 Color: 7
Size: 341755 Color: 16

Bin 3733: 367 of cap free
Amount of items: 2
Items: 
Size: 613666 Color: 16
Size: 385968 Color: 6

Bin 3734: 367 of cap free
Amount of items: 2
Items: 
Size: 597268 Color: 19
Size: 402366 Color: 7

Bin 3735: 370 of cap free
Amount of items: 2
Items: 
Size: 572598 Color: 7
Size: 427033 Color: 18

Bin 3736: 370 of cap free
Amount of items: 2
Items: 
Size: 591858 Color: 2
Size: 407773 Color: 15

Bin 3737: 370 of cap free
Amount of items: 2
Items: 
Size: 546546 Color: 9
Size: 453085 Color: 11

Bin 3738: 372 of cap free
Amount of items: 2
Items: 
Size: 749505 Color: 7
Size: 250124 Color: 13

Bin 3739: 373 of cap free
Amount of items: 2
Items: 
Size: 656859 Color: 0
Size: 342769 Color: 3

Bin 3740: 373 of cap free
Amount of items: 2
Items: 
Size: 758309 Color: 11
Size: 241319 Color: 5

Bin 3741: 374 of cap free
Amount of items: 2
Items: 
Size: 683956 Color: 15
Size: 315671 Color: 7

Bin 3742: 374 of cap free
Amount of items: 2
Items: 
Size: 733518 Color: 6
Size: 266109 Color: 0

Bin 3743: 374 of cap free
Amount of items: 2
Items: 
Size: 516564 Color: 18
Size: 483063 Color: 4

Bin 3744: 374 of cap free
Amount of items: 2
Items: 
Size: 531050 Color: 2
Size: 468577 Color: 12

Bin 3745: 375 of cap free
Amount of items: 2
Items: 
Size: 761427 Color: 11
Size: 238199 Color: 1

Bin 3746: 376 of cap free
Amount of items: 2
Items: 
Size: 518556 Color: 11
Size: 481069 Color: 13

Bin 3747: 377 of cap free
Amount of items: 2
Items: 
Size: 626534 Color: 2
Size: 373090 Color: 1

Bin 3748: 379 of cap free
Amount of items: 2
Items: 
Size: 652463 Color: 4
Size: 347159 Color: 8

Bin 3749: 379 of cap free
Amount of items: 2
Items: 
Size: 797807 Color: 15
Size: 201815 Color: 18

Bin 3750: 380 of cap free
Amount of items: 2
Items: 
Size: 727658 Color: 16
Size: 271963 Color: 4

Bin 3751: 380 of cap free
Amount of items: 2
Items: 
Size: 619358 Color: 0
Size: 380263 Color: 9

Bin 3752: 380 of cap free
Amount of items: 2
Items: 
Size: 754669 Color: 10
Size: 244952 Color: 19

Bin 3753: 380 of cap free
Amount of items: 2
Items: 
Size: 755784 Color: 18
Size: 243837 Color: 17

Bin 3754: 380 of cap free
Amount of items: 2
Items: 
Size: 574198 Color: 1
Size: 425423 Color: 6

Bin 3755: 380 of cap free
Amount of items: 2
Items: 
Size: 599831 Color: 4
Size: 399790 Color: 12

Bin 3756: 381 of cap free
Amount of items: 2
Items: 
Size: 576045 Color: 6
Size: 423575 Color: 13

Bin 3757: 381 of cap free
Amount of items: 2
Items: 
Size: 617505 Color: 2
Size: 382115 Color: 13

Bin 3758: 382 of cap free
Amount of items: 2
Items: 
Size: 636640 Color: 11
Size: 362979 Color: 14

Bin 3759: 383 of cap free
Amount of items: 2
Items: 
Size: 537739 Color: 11
Size: 461879 Color: 13

Bin 3760: 384 of cap free
Amount of items: 2
Items: 
Size: 707571 Color: 8
Size: 292046 Color: 18

Bin 3761: 385 of cap free
Amount of items: 2
Items: 
Size: 532868 Color: 15
Size: 466748 Color: 14

Bin 3762: 386 of cap free
Amount of items: 2
Items: 
Size: 618301 Color: 19
Size: 381314 Color: 1

Bin 3763: 387 of cap free
Amount of items: 2
Items: 
Size: 581406 Color: 6
Size: 418208 Color: 18

Bin 3764: 389 of cap free
Amount of items: 2
Items: 
Size: 681255 Color: 8
Size: 318357 Color: 10

Bin 3765: 389 of cap free
Amount of items: 2
Items: 
Size: 793289 Color: 11
Size: 206323 Color: 18

Bin 3766: 390 of cap free
Amount of items: 2
Items: 
Size: 710479 Color: 12
Size: 289132 Color: 14

Bin 3767: 391 of cap free
Amount of items: 2
Items: 
Size: 591854 Color: 6
Size: 407756 Color: 16

Bin 3768: 391 of cap free
Amount of items: 2
Items: 
Size: 623506 Color: 16
Size: 376104 Color: 3

Bin 3769: 392 of cap free
Amount of items: 3
Items: 
Size: 751344 Color: 18
Size: 128009 Color: 18
Size: 120256 Color: 5

Bin 3770: 392 of cap free
Amount of items: 2
Items: 
Size: 528608 Color: 12
Size: 471001 Color: 10

Bin 3771: 392 of cap free
Amount of items: 2
Items: 
Size: 566767 Color: 3
Size: 432842 Color: 4

Bin 3772: 394 of cap free
Amount of items: 2
Items: 
Size: 728898 Color: 19
Size: 270709 Color: 13

Bin 3773: 395 of cap free
Amount of items: 2
Items: 
Size: 730465 Color: 6
Size: 269141 Color: 13

Bin 3774: 396 of cap free
Amount of items: 2
Items: 
Size: 759321 Color: 8
Size: 240284 Color: 19

Bin 3775: 398 of cap free
Amount of items: 2
Items: 
Size: 680419 Color: 7
Size: 319184 Color: 4

Bin 3776: 398 of cap free
Amount of items: 2
Items: 
Size: 768240 Color: 17
Size: 231363 Color: 12

Bin 3777: 401 of cap free
Amount of items: 2
Items: 
Size: 702793 Color: 2
Size: 296807 Color: 13

Bin 3778: 404 of cap free
Amount of items: 2
Items: 
Size: 684853 Color: 13
Size: 314744 Color: 11

Bin 3779: 405 of cap free
Amount of items: 2
Items: 
Size: 723887 Color: 9
Size: 275709 Color: 15

Bin 3780: 405 of cap free
Amount of items: 2
Items: 
Size: 584754 Color: 7
Size: 414842 Color: 0

Bin 3781: 405 of cap free
Amount of items: 2
Items: 
Size: 787649 Color: 4
Size: 211947 Color: 1

Bin 3782: 406 of cap free
Amount of items: 2
Items: 
Size: 702036 Color: 0
Size: 297559 Color: 8

Bin 3783: 406 of cap free
Amount of items: 2
Items: 
Size: 765493 Color: 16
Size: 234102 Color: 19

Bin 3784: 408 of cap free
Amount of items: 2
Items: 
Size: 574192 Color: 8
Size: 425401 Color: 9

Bin 3785: 410 of cap free
Amount of items: 2
Items: 
Size: 623923 Color: 18
Size: 375668 Color: 4

Bin 3786: 410 of cap free
Amount of items: 2
Items: 
Size: 746890 Color: 8
Size: 252701 Color: 3

Bin 3787: 411 of cap free
Amount of items: 2
Items: 
Size: 557746 Color: 3
Size: 441844 Color: 9

Bin 3788: 413 of cap free
Amount of items: 2
Items: 
Size: 514784 Color: 5
Size: 484804 Color: 9

Bin 3789: 413 of cap free
Amount of items: 2
Items: 
Size: 710467 Color: 10
Size: 289121 Color: 12

Bin 3790: 415 of cap free
Amount of items: 2
Items: 
Size: 766407 Color: 7
Size: 233179 Color: 1

Bin 3791: 416 of cap free
Amount of items: 2
Items: 
Size: 706021 Color: 15
Size: 293564 Color: 3

Bin 3792: 416 of cap free
Amount of items: 2
Items: 
Size: 656821 Color: 19
Size: 342764 Color: 4

Bin 3793: 416 of cap free
Amount of items: 2
Items: 
Size: 624513 Color: 13
Size: 375072 Color: 12

Bin 3794: 418 of cap free
Amount of items: 2
Items: 
Size: 626981 Color: 4
Size: 372602 Color: 17

Bin 3795: 419 of cap free
Amount of items: 2
Items: 
Size: 567639 Color: 16
Size: 431943 Color: 6

Bin 3796: 420 of cap free
Amount of items: 2
Items: 
Size: 505464 Color: 10
Size: 494117 Color: 12

Bin 3797: 423 of cap free
Amount of items: 2
Items: 
Size: 518535 Color: 19
Size: 481043 Color: 0

Bin 3798: 427 of cap free
Amount of items: 2
Items: 
Size: 751871 Color: 1
Size: 247703 Color: 6

Bin 3799: 428 of cap free
Amount of items: 3
Items: 
Size: 740421 Color: 12
Size: 130310 Color: 13
Size: 128842 Color: 12

Bin 3800: 428 of cap free
Amount of items: 2
Items: 
Size: 653419 Color: 10
Size: 346154 Color: 14

Bin 3801: 430 of cap free
Amount of items: 2
Items: 
Size: 709645 Color: 0
Size: 289926 Color: 19

Bin 3802: 432 of cap free
Amount of items: 2
Items: 
Size: 509840 Color: 14
Size: 489729 Color: 9

Bin 3803: 432 of cap free
Amount of items: 2
Items: 
Size: 724734 Color: 5
Size: 274835 Color: 18

Bin 3804: 433 of cap free
Amount of items: 2
Items: 
Size: 558701 Color: 3
Size: 440867 Color: 5

Bin 3805: 433 of cap free
Amount of items: 2
Items: 
Size: 682499 Color: 9
Size: 317069 Color: 12

Bin 3806: 434 of cap free
Amount of items: 2
Items: 
Size: 628838 Color: 7
Size: 370729 Color: 1

Bin 3807: 436 of cap free
Amount of items: 2
Items: 
Size: 553901 Color: 18
Size: 445664 Color: 4

Bin 3808: 438 of cap free
Amount of items: 2
Items: 
Size: 773492 Color: 15
Size: 226071 Color: 9

Bin 3809: 439 of cap free
Amount of items: 2
Items: 
Size: 694974 Color: 5
Size: 304588 Color: 6

Bin 3810: 440 of cap free
Amount of items: 2
Items: 
Size: 695699 Color: 10
Size: 303862 Color: 19

Bin 3811: 441 of cap free
Amount of items: 2
Items: 
Size: 741396 Color: 3
Size: 258164 Color: 1

Bin 3812: 443 of cap free
Amount of items: 2
Items: 
Size: 525155 Color: 13
Size: 474403 Color: 1

Bin 3813: 444 of cap free
Amount of items: 2
Items: 
Size: 713526 Color: 2
Size: 286031 Color: 18

Bin 3814: 445 of cap free
Amount of items: 2
Items: 
Size: 692652 Color: 18
Size: 306904 Color: 7

Bin 3815: 445 of cap free
Amount of items: 2
Items: 
Size: 621094 Color: 12
Size: 378462 Color: 7

Bin 3816: 445 of cap free
Amount of items: 2
Items: 
Size: 568878 Color: 1
Size: 430678 Color: 17

Bin 3817: 445 of cap free
Amount of items: 2
Items: 
Size: 571154 Color: 19
Size: 428402 Color: 18

Bin 3818: 445 of cap free
Amount of items: 2
Items: 
Size: 556031 Color: 0
Size: 443525 Color: 8

Bin 3819: 446 of cap free
Amount of items: 2
Items: 
Size: 683910 Color: 9
Size: 315645 Color: 11

Bin 3820: 446 of cap free
Amount of items: 2
Items: 
Size: 528602 Color: 12
Size: 470953 Color: 17

Bin 3821: 447 of cap free
Amount of items: 2
Items: 
Size: 603512 Color: 10
Size: 396042 Color: 18

Bin 3822: 448 of cap free
Amount of items: 2
Items: 
Size: 505978 Color: 0
Size: 493575 Color: 4

Bin 3823: 448 of cap free
Amount of items: 2
Items: 
Size: 572525 Color: 14
Size: 427028 Color: 4

Bin 3824: 449 of cap free
Amount of items: 2
Items: 
Size: 646494 Color: 0
Size: 353058 Color: 6

Bin 3825: 449 of cap free
Amount of items: 2
Items: 
Size: 711588 Color: 11
Size: 287964 Color: 19

Bin 3826: 455 of cap free
Amount of items: 2
Items: 
Size: 679430 Color: 12
Size: 320116 Color: 8

Bin 3827: 456 of cap free
Amount of items: 2
Items: 
Size: 546535 Color: 8
Size: 453010 Color: 4

Bin 3828: 459 of cap free
Amount of items: 2
Items: 
Size: 669950 Color: 9
Size: 329592 Color: 19

Bin 3829: 462 of cap free
Amount of items: 2
Items: 
Size: 559356 Color: 6
Size: 440183 Color: 0

Bin 3830: 464 of cap free
Amount of items: 2
Items: 
Size: 741415 Color: 1
Size: 258122 Color: 9

Bin 3831: 467 of cap free
Amount of items: 2
Items: 
Size: 624484 Color: 13
Size: 375050 Color: 14

Bin 3832: 468 of cap free
Amount of items: 2
Items: 
Size: 518493 Color: 1
Size: 481040 Color: 10

Bin 3833: 468 of cap free
Amount of items: 2
Items: 
Size: 507613 Color: 12
Size: 491920 Color: 19

Bin 3834: 468 of cap free
Amount of items: 2
Items: 
Size: 557744 Color: 3
Size: 441789 Color: 2

Bin 3835: 469 of cap free
Amount of items: 2
Items: 
Size: 707494 Color: 4
Size: 292038 Color: 11

Bin 3836: 471 of cap free
Amount of items: 2
Items: 
Size: 539739 Color: 9
Size: 459791 Color: 17

Bin 3837: 471 of cap free
Amount of items: 2
Items: 
Size: 713407 Color: 16
Size: 286123 Color: 2

Bin 3838: 474 of cap free
Amount of items: 2
Items: 
Size: 532853 Color: 6
Size: 466674 Color: 9

Bin 3839: 475 of cap free
Amount of items: 2
Items: 
Size: 531492 Color: 9
Size: 468034 Color: 1

Bin 3840: 475 of cap free
Amount of items: 2
Items: 
Size: 550621 Color: 3
Size: 448905 Color: 17

Bin 3841: 477 of cap free
Amount of items: 2
Items: 
Size: 787639 Color: 19
Size: 211885 Color: 10

Bin 3842: 478 of cap free
Amount of items: 2
Items: 
Size: 747506 Color: 4
Size: 252017 Color: 9

Bin 3843: 478 of cap free
Amount of items: 2
Items: 
Size: 771106 Color: 16
Size: 228417 Color: 10

Bin 3844: 479 of cap free
Amount of items: 2
Items: 
Size: 536211 Color: 9
Size: 463311 Color: 10

Bin 3845: 481 of cap free
Amount of items: 2
Items: 
Size: 602410 Color: 12
Size: 397110 Color: 2

Bin 3846: 481 of cap free
Amount of items: 2
Items: 
Size: 534374 Color: 1
Size: 465146 Color: 5

Bin 3847: 481 of cap free
Amount of items: 2
Items: 
Size: 598506 Color: 17
Size: 401014 Color: 13

Bin 3848: 482 of cap free
Amount of items: 2
Items: 
Size: 591789 Color: 6
Size: 407730 Color: 4

Bin 3849: 482 of cap free
Amount of items: 2
Items: 
Size: 603503 Color: 5
Size: 396016 Color: 18

Bin 3850: 484 of cap free
Amount of items: 2
Items: 
Size: 613652 Color: 2
Size: 385865 Color: 15

Bin 3851: 484 of cap free
Amount of items: 2
Items: 
Size: 657875 Color: 0
Size: 341642 Color: 7

Bin 3852: 485 of cap free
Amount of items: 2
Items: 
Size: 708132 Color: 10
Size: 291384 Color: 5

Bin 3853: 486 of cap free
Amount of items: 2
Items: 
Size: 710398 Color: 9
Size: 289117 Color: 15

Bin 3854: 486 of cap free
Amount of items: 2
Items: 
Size: 722096 Color: 10
Size: 277419 Color: 12

Bin 3855: 489 of cap free
Amount of items: 2
Items: 
Size: 599827 Color: 11
Size: 399685 Color: 5

Bin 3856: 489 of cap free
Amount of items: 2
Items: 
Size: 557731 Color: 0
Size: 441781 Color: 6

Bin 3857: 491 of cap free
Amount of items: 2
Items: 
Size: 598556 Color: 13
Size: 400954 Color: 4

Bin 3858: 491 of cap free
Amount of items: 2
Items: 
Size: 711016 Color: 18
Size: 288494 Color: 7

Bin 3859: 492 of cap free
Amount of items: 2
Items: 
Size: 527559 Color: 18
Size: 471950 Color: 4

Bin 3860: 492 of cap free
Amount of items: 2
Items: 
Size: 587344 Color: 0
Size: 412165 Color: 5

Bin 3861: 493 of cap free
Amount of items: 2
Items: 
Size: 582885 Color: 4
Size: 416623 Color: 18

Bin 3862: 493 of cap free
Amount of items: 2
Items: 
Size: 603496 Color: 16
Size: 396012 Color: 8

Bin 3863: 494 of cap free
Amount of items: 2
Items: 
Size: 774832 Color: 8
Size: 224675 Color: 11

Bin 3864: 494 of cap free
Amount of items: 2
Items: 
Size: 597149 Color: 8
Size: 402358 Color: 1

Bin 3865: 495 of cap free
Amount of items: 2
Items: 
Size: 512187 Color: 16
Size: 487319 Color: 11

Bin 3866: 496 of cap free
Amount of items: 2
Items: 
Size: 552155 Color: 19
Size: 447350 Color: 8

Bin 3867: 499 of cap free
Amount of items: 2
Items: 
Size: 594202 Color: 13
Size: 405300 Color: 4

Bin 3868: 499 of cap free
Amount of items: 2
Items: 
Size: 704262 Color: 7
Size: 295240 Color: 15

Bin 3869: 499 of cap free
Amount of items: 2
Items: 
Size: 561881 Color: 19
Size: 437621 Color: 16

Bin 3870: 499 of cap free
Amount of items: 2
Items: 
Size: 758289 Color: 7
Size: 241213 Color: 2

Bin 3871: 502 of cap free
Amount of items: 2
Items: 
Size: 749451 Color: 14
Size: 250048 Color: 17

Bin 3872: 503 of cap free
Amount of items: 2
Items: 
Size: 533826 Color: 18
Size: 465672 Color: 12

Bin 3873: 504 of cap free
Amount of items: 2
Items: 
Size: 599822 Color: 15
Size: 399675 Color: 19

Bin 3874: 505 of cap free
Amount of items: 2
Items: 
Size: 629477 Color: 1
Size: 370019 Color: 18

Bin 3875: 506 of cap free
Amount of items: 2
Items: 
Size: 738046 Color: 13
Size: 261449 Color: 18

Bin 3876: 506 of cap free
Amount of items: 2
Items: 
Size: 508304 Color: 8
Size: 491191 Color: 15

Bin 3877: 513 of cap free
Amount of items: 2
Items: 
Size: 626928 Color: 2
Size: 372560 Color: 5

Bin 3878: 517 of cap free
Amount of items: 2
Items: 
Size: 730453 Color: 0
Size: 269031 Color: 17

Bin 3879: 517 of cap free
Amount of items: 2
Items: 
Size: 571621 Color: 7
Size: 427863 Color: 17

Bin 3880: 518 of cap free
Amount of items: 2
Items: 
Size: 726675 Color: 19
Size: 272808 Color: 14

Bin 3881: 521 of cap free
Amount of items: 2
Items: 
Size: 709564 Color: 14
Size: 289916 Color: 9

Bin 3882: 524 of cap free
Amount of items: 2
Items: 
Size: 785550 Color: 16
Size: 213927 Color: 2

Bin 3883: 525 of cap free
Amount of items: 2
Items: 
Size: 654174 Color: 16
Size: 345302 Color: 13

Bin 3884: 528 of cap free
Amount of items: 2
Items: 
Size: 663326 Color: 6
Size: 336147 Color: 12

Bin 3885: 531 of cap free
Amount of items: 2
Items: 
Size: 534338 Color: 10
Size: 465132 Color: 3

Bin 3886: 531 of cap free
Amount of items: 2
Items: 
Size: 765425 Color: 16
Size: 234045 Color: 15

Bin 3887: 532 of cap free
Amount of items: 2
Items: 
Size: 655220 Color: 9
Size: 344249 Color: 7

Bin 3888: 533 of cap free
Amount of items: 2
Items: 
Size: 514726 Color: 4
Size: 484742 Color: 1

Bin 3889: 536 of cap free
Amount of items: 2
Items: 
Size: 582285 Color: 12
Size: 417180 Color: 4

Bin 3890: 537 of cap free
Amount of items: 2
Items: 
Size: 690487 Color: 13
Size: 308977 Color: 10

Bin 3891: 538 of cap free
Amount of items: 3
Items: 
Size: 619965 Color: 14
Size: 191056 Color: 0
Size: 188442 Color: 0

Bin 3892: 538 of cap free
Amount of items: 2
Items: 
Size: 525074 Color: 11
Size: 474389 Color: 10

Bin 3893: 539 of cap free
Amount of items: 2
Items: 
Size: 723859 Color: 9
Size: 275603 Color: 11

Bin 3894: 539 of cap free
Amount of items: 2
Items: 
Size: 764477 Color: 5
Size: 234985 Color: 19

Bin 3895: 542 of cap free
Amount of items: 2
Items: 
Size: 643794 Color: 1
Size: 355665 Color: 8

Bin 3896: 542 of cap free
Amount of items: 2
Items: 
Size: 676542 Color: 19
Size: 322917 Color: 17

Bin 3897: 545 of cap free
Amount of items: 2
Items: 
Size: 507589 Color: 18
Size: 491867 Color: 16

Bin 3898: 546 of cap free
Amount of items: 2
Items: 
Size: 585759 Color: 6
Size: 413696 Color: 18

Bin 3899: 546 of cap free
Amount of items: 2
Items: 
Size: 755760 Color: 13
Size: 243695 Color: 15

Bin 3900: 547 of cap free
Amount of items: 2
Items: 
Size: 605772 Color: 4
Size: 393682 Color: 11

Bin 3901: 548 of cap free
Amount of items: 2
Items: 
Size: 544810 Color: 8
Size: 454643 Color: 2

Bin 3902: 548 of cap free
Amount of items: 2
Items: 
Size: 753351 Color: 16
Size: 246102 Color: 19

Bin 3903: 551 of cap free
Amount of items: 2
Items: 
Size: 611707 Color: 1
Size: 387743 Color: 9

Bin 3904: 553 of cap free
Amount of items: 2
Items: 
Size: 558670 Color: 18
Size: 440778 Color: 4

Bin 3905: 554 of cap free
Amount of items: 2
Items: 
Size: 616806 Color: 7
Size: 382641 Color: 14

Bin 3906: 555 of cap free
Amount of items: 3
Items: 
Size: 727567 Color: 14
Size: 138103 Color: 19
Size: 133776 Color: 6

Bin 3907: 563 of cap free
Amount of items: 2
Items: 
Size: 501421 Color: 0
Size: 498017 Color: 9

Bin 3908: 564 of cap free
Amount of items: 2
Items: 
Size: 782578 Color: 14
Size: 216859 Color: 5

Bin 3909: 564 of cap free
Amount of items: 2
Items: 
Size: 758229 Color: 8
Size: 241208 Color: 4

Bin 3910: 564 of cap free
Amount of items: 2
Items: 
Size: 657854 Color: 16
Size: 341583 Color: 10

Bin 3911: 565 of cap free
Amount of items: 2
Items: 
Size: 631889 Color: 1
Size: 367547 Color: 16

Bin 3912: 567 of cap free
Amount of items: 2
Items: 
Size: 514716 Color: 4
Size: 484718 Color: 15

Bin 3913: 572 of cap free
Amount of items: 2
Items: 
Size: 566088 Color: 9
Size: 433341 Color: 1

Bin 3914: 573 of cap free
Amount of items: 2
Items: 
Size: 636471 Color: 2
Size: 362957 Color: 11

Bin 3915: 573 of cap free
Amount of items: 2
Items: 
Size: 788817 Color: 5
Size: 210611 Color: 19

Bin 3916: 574 of cap free
Amount of items: 2
Items: 
Size: 674736 Color: 3
Size: 324691 Color: 10

Bin 3917: 577 of cap free
Amount of items: 2
Items: 
Size: 531465 Color: 6
Size: 467959 Color: 14

Bin 3918: 578 of cap free
Amount of items: 2
Items: 
Size: 773411 Color: 0
Size: 226012 Color: 9

Bin 3919: 578 of cap free
Amount of items: 2
Items: 
Size: 734596 Color: 15
Size: 264827 Color: 5

Bin 3920: 580 of cap free
Amount of items: 2
Items: 
Size: 555448 Color: 9
Size: 443973 Color: 11

Bin 3921: 581 of cap free
Amount of items: 2
Items: 
Size: 528574 Color: 12
Size: 470846 Color: 1

Bin 3922: 582 of cap free
Amount of items: 3
Items: 
Size: 649130 Color: 1
Size: 175406 Color: 6
Size: 174883 Color: 8

Bin 3923: 586 of cap free
Amount of items: 2
Items: 
Size: 592638 Color: 13
Size: 406777 Color: 2

Bin 3924: 587 of cap free
Amount of items: 2
Items: 
Size: 539708 Color: 10
Size: 459706 Color: 4

Bin 3925: 588 of cap free
Amount of items: 2
Items: 
Size: 643766 Color: 6
Size: 355647 Color: 4

Bin 3926: 590 of cap free
Amount of items: 2
Items: 
Size: 794601 Color: 13
Size: 204810 Color: 4

Bin 3927: 591 of cap free
Amount of items: 2
Items: 
Size: 709521 Color: 19
Size: 289889 Color: 17

Bin 3928: 591 of cap free
Amount of items: 2
Items: 
Size: 558653 Color: 9
Size: 440757 Color: 10

Bin 3929: 593 of cap free
Amount of items: 2
Items: 
Size: 500045 Color: 18
Size: 499363 Color: 6

Bin 3930: 597 of cap free
Amount of items: 2
Items: 
Size: 761241 Color: 6
Size: 238163 Color: 5

Bin 3931: 599 of cap free
Amount of items: 2
Items: 
Size: 713965 Color: 18
Size: 285437 Color: 4

Bin 3932: 601 of cap free
Amount of items: 2
Items: 
Size: 566131 Color: 1
Size: 433269 Color: 0

Bin 3933: 601 of cap free
Amount of items: 2
Items: 
Size: 783365 Color: 7
Size: 216035 Color: 13

Bin 3934: 605 of cap free
Amount of items: 2
Items: 
Size: 535299 Color: 0
Size: 464097 Color: 11

Bin 3935: 607 of cap free
Amount of items: 2
Items: 
Size: 611656 Color: 16
Size: 387738 Color: 11

Bin 3936: 609 of cap free
Amount of items: 2
Items: 
Size: 765352 Color: 13
Size: 234040 Color: 2

Bin 3937: 611 of cap free
Amount of items: 2
Items: 
Size: 515691 Color: 17
Size: 483699 Color: 10

Bin 3938: 612 of cap free
Amount of items: 2
Items: 
Size: 766364 Color: 5
Size: 233025 Color: 13

Bin 3939: 612 of cap free
Amount of items: 2
Items: 
Size: 558628 Color: 19
Size: 440761 Color: 9

Bin 3940: 613 of cap free
Amount of items: 2
Items: 
Size: 792668 Color: 15
Size: 206720 Color: 3

Bin 3941: 618 of cap free
Amount of items: 2
Items: 
Size: 523732 Color: 11
Size: 475651 Color: 16

Bin 3942: 620 of cap free
Amount of items: 2
Items: 
Size: 652431 Color: 1
Size: 346950 Color: 3

Bin 3943: 621 of cap free
Amount of items: 2
Items: 
Size: 553874 Color: 7
Size: 445506 Color: 1

Bin 3944: 624 of cap free
Amount of items: 2
Items: 
Size: 599702 Color: 19
Size: 399675 Color: 4

Bin 3945: 625 of cap free
Amount of items: 2
Items: 
Size: 536207 Color: 3
Size: 463169 Color: 0

Bin 3946: 627 of cap free
Amount of items: 2
Items: 
Size: 702761 Color: 10
Size: 296613 Color: 9

Bin 3947: 629 of cap free
Amount of items: 2
Items: 
Size: 782574 Color: 8
Size: 216798 Color: 4

Bin 3948: 629 of cap free
Amount of items: 2
Items: 
Size: 722013 Color: 16
Size: 277359 Color: 3

Bin 3949: 630 of cap free
Amount of items: 2
Items: 
Size: 669786 Color: 16
Size: 329585 Color: 14

Bin 3950: 636 of cap free
Amount of items: 2
Items: 
Size: 505920 Color: 1
Size: 493445 Color: 16

Bin 3951: 638 of cap free
Amount of items: 2
Items: 
Size: 798272 Color: 8
Size: 201091 Color: 3

Bin 3952: 640 of cap free
Amount of items: 2
Items: 
Size: 595061 Color: 19
Size: 404300 Color: 6

Bin 3953: 640 of cap free
Amount of items: 2
Items: 
Size: 710886 Color: 8
Size: 288475 Color: 10

Bin 3954: 642 of cap free
Amount of items: 2
Items: 
Size: 531459 Color: 4
Size: 467900 Color: 1

Bin 3955: 644 of cap free
Amount of items: 2
Items: 
Size: 618071 Color: 17
Size: 381286 Color: 16

Bin 3956: 646 of cap free
Amount of items: 2
Items: 
Size: 709469 Color: 4
Size: 289886 Color: 12

Bin 3957: 649 of cap free
Amount of items: 2
Items: 
Size: 581369 Color: 13
Size: 417983 Color: 16

Bin 3958: 655 of cap free
Amount of items: 2
Items: 
Size: 682473 Color: 4
Size: 316873 Color: 12

Bin 3959: 656 of cap free
Amount of items: 2
Items: 
Size: 597007 Color: 6
Size: 402338 Color: 19

Bin 3960: 656 of cap free
Amount of items: 2
Items: 
Size: 543824 Color: 2
Size: 455521 Color: 0

Bin 3961: 657 of cap free
Amount of items: 2
Items: 
Size: 616714 Color: 16
Size: 382630 Color: 8

Bin 3962: 660 of cap free
Amount of items: 2
Items: 
Size: 777422 Color: 1
Size: 221919 Color: 15

Bin 3963: 662 of cap free
Amount of items: 2
Items: 
Size: 657758 Color: 9
Size: 341581 Color: 2

Bin 3964: 666 of cap free
Amount of items: 2
Items: 
Size: 728800 Color: 5
Size: 270535 Color: 1

Bin 3965: 666 of cap free
Amount of items: 2
Items: 
Size: 774815 Color: 1
Size: 224520 Color: 16

Bin 3966: 666 of cap free
Amount of items: 2
Items: 
Size: 618054 Color: 10
Size: 381281 Color: 5

Bin 3967: 669 of cap free
Amount of items: 2
Items: 
Size: 664655 Color: 12
Size: 334677 Color: 11

Bin 3968: 670 of cap free
Amount of items: 2
Items: 
Size: 628069 Color: 2
Size: 371262 Color: 8

Bin 3969: 671 of cap free
Amount of items: 2
Items: 
Size: 749283 Color: 2
Size: 250047 Color: 16

Bin 3970: 672 of cap free
Amount of items: 2
Items: 
Size: 544741 Color: 16
Size: 454588 Color: 12

Bin 3971: 675 of cap free
Amount of items: 2
Items: 
Size: 747353 Color: 12
Size: 251973 Color: 4

Bin 3972: 675 of cap free
Amount of items: 2
Items: 
Size: 505228 Color: 13
Size: 494098 Color: 5

Bin 3973: 677 of cap free
Amount of items: 2
Items: 
Size: 528488 Color: 8
Size: 470836 Color: 4

Bin 3974: 680 of cap free
Amount of items: 2
Items: 
Size: 758203 Color: 19
Size: 241118 Color: 9

Bin 3975: 680 of cap free
Amount of items: 2
Items: 
Size: 500673 Color: 3
Size: 498648 Color: 0

Bin 3976: 690 of cap free
Amount of items: 2
Items: 
Size: 773508 Color: 9
Size: 225803 Color: 1

Bin 3977: 694 of cap free
Amount of items: 2
Items: 
Size: 565071 Color: 5
Size: 434236 Color: 18

Bin 3978: 694 of cap free
Amount of items: 2
Items: 
Size: 734551 Color: 3
Size: 264756 Color: 19

Bin 3979: 694 of cap free
Amount of items: 2
Items: 
Size: 574001 Color: 3
Size: 425306 Color: 8

Bin 3980: 695 of cap free
Amount of items: 2
Items: 
Size: 775627 Color: 12
Size: 223679 Color: 0

Bin 3981: 695 of cap free
Amount of items: 2
Items: 
Size: 514598 Color: 9
Size: 484708 Color: 7

Bin 3982: 700 of cap free
Amount of items: 2
Items: 
Size: 517367 Color: 10
Size: 481934 Color: 4

Bin 3983: 701 of cap free
Amount of items: 2
Items: 
Size: 552882 Color: 1
Size: 446418 Color: 13

Bin 3984: 701 of cap free
Amount of items: 2
Items: 
Size: 609222 Color: 11
Size: 390078 Color: 13

Bin 3985: 702 of cap free
Amount of items: 2
Items: 
Size: 764327 Color: 7
Size: 234972 Color: 16

Bin 3986: 702 of cap free
Amount of items: 2
Items: 
Size: 589260 Color: 17
Size: 410039 Color: 11

Bin 3987: 703 of cap free
Amount of items: 2
Items: 
Size: 582146 Color: 10
Size: 417152 Color: 8

Bin 3988: 704 of cap free
Amount of items: 2
Items: 
Size: 782510 Color: 13
Size: 216787 Color: 6

Bin 3989: 710 of cap free
Amount of items: 2
Items: 
Size: 622234 Color: 13
Size: 377057 Color: 7

Bin 3990: 712 of cap free
Amount of items: 2
Items: 
Size: 750112 Color: 13
Size: 249177 Color: 6

Bin 3991: 712 of cap free
Amount of items: 2
Items: 
Size: 711559 Color: 16
Size: 287730 Color: 17

Bin 3992: 713 of cap free
Amount of items: 2
Items: 
Size: 584731 Color: 19
Size: 414557 Color: 8

Bin 3993: 714 of cap free
Amount of items: 2
Items: 
Size: 549130 Color: 14
Size: 450157 Color: 4

Bin 3994: 719 of cap free
Amount of items: 2
Items: 
Size: 591773 Color: 13
Size: 407509 Color: 8

Bin 3995: 722 of cap free
Amount of items: 2
Items: 
Size: 794594 Color: 18
Size: 204685 Color: 7

Bin 3996: 722 of cap free
Amount of items: 2
Items: 
Size: 523721 Color: 4
Size: 475558 Color: 14

Bin 3997: 723 of cap free
Amount of items: 2
Items: 
Size: 517359 Color: 6
Size: 481919 Color: 2

Bin 3998: 724 of cap free
Amount of items: 2
Items: 
Size: 682444 Color: 6
Size: 316833 Color: 19

Bin 3999: 728 of cap free
Amount of items: 2
Items: 
Size: 508203 Color: 13
Size: 491070 Color: 17

Bin 4000: 729 of cap free
Amount of items: 2
Items: 
Size: 500669 Color: 10
Size: 498603 Color: 13

Bin 4001: 731 of cap free
Amount of items: 2
Items: 
Size: 676466 Color: 1
Size: 322804 Color: 0

Bin 4002: 737 of cap free
Amount of items: 2
Items: 
Size: 758178 Color: 16
Size: 241086 Color: 13

Bin 4003: 743 of cap free
Amount of items: 2
Items: 
Size: 544731 Color: 2
Size: 454527 Color: 8

Bin 4004: 748 of cap free
Amount of items: 2
Items: 
Size: 633986 Color: 6
Size: 365267 Color: 18

Bin 4005: 751 of cap free
Amount of items: 2
Items: 
Size: 552861 Color: 7
Size: 446389 Color: 18

Bin 4006: 752 of cap free
Amount of items: 2
Items: 
Size: 653172 Color: 0
Size: 346077 Color: 14

Bin 4007: 753 of cap free
Amount of items: 2
Items: 
Size: 764319 Color: 1
Size: 234929 Color: 17

Bin 4008: 753 of cap free
Amount of items: 2
Items: 
Size: 590157 Color: 18
Size: 409091 Color: 10

Bin 4009: 758 of cap free
Amount of items: 2
Items: 
Size: 576635 Color: 17
Size: 422608 Color: 9

Bin 4010: 762 of cap free
Amount of items: 2
Items: 
Size: 734567 Color: 19
Size: 264672 Color: 13

Bin 4011: 769 of cap free
Amount of items: 2
Items: 
Size: 538508 Color: 15
Size: 460724 Color: 17

Bin 4012: 770 of cap free
Amount of items: 2
Items: 
Size: 557687 Color: 3
Size: 441544 Color: 1

Bin 4013: 770 of cap free
Amount of items: 2
Items: 
Size: 594932 Color: 15
Size: 404299 Color: 3

Bin 4014: 770 of cap free
Amount of items: 2
Items: 
Size: 539553 Color: 6
Size: 459678 Color: 16

Bin 4015: 771 of cap free
Amount of items: 2
Items: 
Size: 690441 Color: 12
Size: 308789 Color: 11

Bin 4016: 772 of cap free
Amount of items: 2
Items: 
Size: 777318 Color: 8
Size: 221911 Color: 17

Bin 4017: 773 of cap free
Amount of items: 3
Items: 
Size: 679390 Color: 9
Size: 159924 Color: 19
Size: 159914 Color: 4

Bin 4018: 775 of cap free
Amount of items: 2
Items: 
Size: 549101 Color: 17
Size: 450125 Color: 15

Bin 4019: 777 of cap free
Amount of items: 2
Items: 
Size: 526684 Color: 18
Size: 472540 Color: 1

Bin 4020: 779 of cap free
Amount of items: 2
Items: 
Size: 636450 Color: 14
Size: 362772 Color: 6

Bin 4021: 782 of cap free
Amount of items: 2
Items: 
Size: 584676 Color: 10
Size: 414543 Color: 8

Bin 4022: 782 of cap free
Amount of items: 2
Items: 
Size: 625042 Color: 19
Size: 374177 Color: 2

Bin 4023: 782 of cap free
Amount of items: 2
Items: 
Size: 740096 Color: 6
Size: 259123 Color: 19

Bin 4024: 786 of cap free
Amount of items: 2
Items: 
Size: 605601 Color: 4
Size: 393614 Color: 16

Bin 4025: 788 of cap free
Amount of items: 2
Items: 
Size: 544704 Color: 18
Size: 454509 Color: 14

Bin 4026: 790 of cap free
Amount of items: 2
Items: 
Size: 775612 Color: 14
Size: 223599 Color: 6

Bin 4027: 792 of cap free
Amount of items: 2
Items: 
Size: 639119 Color: 15
Size: 360090 Color: 6

Bin 4028: 793 of cap free
Amount of items: 2
Items: 
Size: 663082 Color: 8
Size: 336126 Color: 10

Bin 4029: 796 of cap free
Amount of items: 2
Items: 
Size: 549097 Color: 19
Size: 450108 Color: 12

Bin 4030: 801 of cap free
Amount of items: 2
Items: 
Size: 697529 Color: 17
Size: 301671 Color: 2

Bin 4031: 802 of cap free
Amount of items: 2
Items: 
Size: 524970 Color: 4
Size: 474229 Color: 3

Bin 4032: 802 of cap free
Amount of items: 2
Items: 
Size: 600685 Color: 11
Size: 398514 Color: 6

Bin 4033: 804 of cap free
Amount of items: 2
Items: 
Size: 545658 Color: 5
Size: 453539 Color: 11

Bin 4034: 805 of cap free
Amount of items: 2
Items: 
Size: 653932 Color: 12
Size: 345264 Color: 11

Bin 4035: 806 of cap free
Amount of items: 2
Items: 
Size: 690423 Color: 12
Size: 308772 Color: 15

Bin 4036: 812 of cap free
Amount of items: 2
Items: 
Size: 682420 Color: 13
Size: 316769 Color: 8

Bin 4037: 812 of cap free
Amount of items: 2
Items: 
Size: 737204 Color: 5
Size: 261985 Color: 15

Bin 4038: 812 of cap free
Amount of items: 2
Items: 
Size: 707155 Color: 15
Size: 292034 Color: 13

Bin 4039: 814 of cap free
Amount of items: 2
Items: 
Size: 553804 Color: 6
Size: 445383 Color: 14

Bin 4040: 816 of cap free
Amount of items: 2
Items: 
Size: 731718 Color: 12
Size: 267467 Color: 7

Bin 4041: 818 of cap free
Amount of items: 2
Items: 
Size: 709413 Color: 14
Size: 289770 Color: 1

Bin 4042: 818 of cap free
Amount of items: 2
Items: 
Size: 589168 Color: 16
Size: 410015 Color: 17

Bin 4043: 819 of cap free
Amount of items: 2
Items: 
Size: 763056 Color: 11
Size: 236126 Color: 10

Bin 4044: 822 of cap free
Amount of items: 2
Items: 
Size: 759040 Color: 10
Size: 240139 Color: 12

Bin 4045: 822 of cap free
Amount of items: 2
Items: 
Size: 576625 Color: 12
Size: 422554 Color: 7

Bin 4046: 828 of cap free
Amount of items: 2
Items: 
Size: 744335 Color: 5
Size: 254838 Color: 10

Bin 4047: 833 of cap free
Amount of items: 2
Items: 
Size: 690402 Color: 3
Size: 308766 Color: 12

Bin 4048: 837 of cap free
Amount of items: 2
Items: 
Size: 623500 Color: 5
Size: 375664 Color: 17

Bin 4049: 839 of cap free
Amount of items: 2
Items: 
Size: 505904 Color: 15
Size: 493258 Color: 1

Bin 4050: 846 of cap free
Amount of items: 2
Items: 
Size: 657678 Color: 12
Size: 341477 Color: 7

Bin 4051: 846 of cap free
Amount of items: 2
Items: 
Size: 525855 Color: 3
Size: 473300 Color: 18

Bin 4052: 848 of cap free
Amount of items: 2
Items: 
Size: 750936 Color: 17
Size: 248217 Color: 18

Bin 4053: 848 of cap free
Amount of items: 2
Items: 
Size: 658662 Color: 12
Size: 340491 Color: 1

Bin 4054: 849 of cap free
Amount of items: 2
Items: 
Size: 549081 Color: 8
Size: 450071 Color: 14

Bin 4055: 849 of cap free
Amount of items: 2
Items: 
Size: 544696 Color: 8
Size: 454456 Color: 4

Bin 4056: 853 of cap free
Amount of items: 2
Items: 
Size: 589169 Color: 17
Size: 409979 Color: 9

Bin 4057: 855 of cap free
Amount of items: 2
Items: 
Size: 593976 Color: 8
Size: 405170 Color: 15

Bin 4058: 855 of cap free
Amount of items: 2
Items: 
Size: 616527 Color: 17
Size: 382619 Color: 14

Bin 4059: 859 of cap free
Amount of items: 2
Items: 
Size: 515679 Color: 4
Size: 483463 Color: 5

Bin 4060: 860 of cap free
Amount of items: 2
Items: 
Size: 599603 Color: 5
Size: 399538 Color: 11

Bin 4061: 861 of cap free
Amount of items: 2
Items: 
Size: 740025 Color: 19
Size: 259115 Color: 15

Bin 4062: 866 of cap free
Amount of items: 2
Items: 
Size: 552812 Color: 13
Size: 446323 Color: 16

Bin 4063: 871 of cap free
Amount of items: 2
Items: 
Size: 594896 Color: 10
Size: 404234 Color: 15

Bin 4064: 873 of cap free
Amount of items: 2
Items: 
Size: 787250 Color: 15
Size: 211878 Color: 10

Bin 4065: 877 of cap free
Amount of items: 2
Items: 
Size: 737147 Color: 7
Size: 261977 Color: 2

Bin 4066: 877 of cap free
Amount of items: 2
Items: 
Size: 510373 Color: 4
Size: 488751 Color: 19

Bin 4067: 879 of cap free
Amount of items: 2
Items: 
Size: 596906 Color: 12
Size: 402216 Color: 17

Bin 4068: 880 of cap free
Amount of items: 2
Items: 
Size: 517257 Color: 11
Size: 481864 Color: 3

Bin 4069: 884 of cap free
Amount of items: 2
Items: 
Size: 553771 Color: 0
Size: 445346 Color: 19

Bin 4070: 895 of cap free
Amount of items: 2
Items: 
Size: 636346 Color: 17
Size: 362760 Color: 14

Bin 4071: 900 of cap free
Amount of items: 2
Items: 
Size: 549033 Color: 8
Size: 450068 Color: 15

Bin 4072: 901 of cap free
Amount of items: 2
Items: 
Size: 544646 Color: 18
Size: 454454 Color: 0

Bin 4073: 902 of cap free
Amount of items: 2
Items: 
Size: 508140 Color: 3
Size: 490959 Color: 17

Bin 4074: 906 of cap free
Amount of items: 2
Items: 
Size: 639081 Color: 12
Size: 360014 Color: 19

Bin 4075: 906 of cap free
Amount of items: 2
Items: 
Size: 684676 Color: 13
Size: 314419 Color: 6

Bin 4076: 908 of cap free
Amount of items: 2
Items: 
Size: 692295 Color: 6
Size: 306798 Color: 11

Bin 4077: 913 of cap free
Amount of items: 2
Items: 
Size: 611624 Color: 18
Size: 387464 Color: 9

Bin 4078: 914 of cap free
Amount of items: 2
Items: 
Size: 647132 Color: 0
Size: 351955 Color: 1

Bin 4079: 918 of cap free
Amount of items: 2
Items: 
Size: 690340 Color: 1
Size: 308743 Color: 13

Bin 4080: 919 of cap free
Amount of items: 2
Items: 
Size: 618853 Color: 8
Size: 380229 Color: 15

Bin 4081: 924 of cap free
Amount of items: 2
Items: 
Size: 599573 Color: 5
Size: 399504 Color: 16

Bin 4082: 924 of cap free
Amount of items: 2
Items: 
Size: 652195 Color: 0
Size: 346882 Color: 16

Bin 4083: 930 of cap free
Amount of items: 2
Items: 
Size: 639077 Color: 13
Size: 359994 Color: 8

Bin 4084: 931 of cap free
Amount of items: 2
Items: 
Size: 715712 Color: 6
Size: 283358 Color: 4

Bin 4085: 932 of cap free
Amount of items: 2
Items: 
Size: 678135 Color: 2
Size: 320934 Color: 16

Bin 4086: 936 of cap free
Amount of items: 2
Items: 
Size: 578551 Color: 7
Size: 420514 Color: 19

Bin 4087: 952 of cap free
Amount of items: 2
Items: 
Size: 652178 Color: 7
Size: 346871 Color: 14

Bin 4088: 963 of cap free
Amount of items: 2
Items: 
Size: 596877 Color: 4
Size: 402161 Color: 15

Bin 4089: 974 of cap free
Amount of items: 2
Items: 
Size: 540926 Color: 8
Size: 458101 Color: 1

Bin 4090: 975 of cap free
Amount of items: 2
Items: 
Size: 639061 Color: 0
Size: 359965 Color: 9

Bin 4091: 978 of cap free
Amount of items: 2
Items: 
Size: 690307 Color: 11
Size: 308716 Color: 1

Bin 4092: 981 of cap free
Amount of items: 2
Items: 
Size: 741233 Color: 18
Size: 257787 Color: 17

Bin 4093: 987 of cap free
Amount of items: 2
Items: 
Size: 528456 Color: 11
Size: 470558 Color: 5

Bin 4094: 993 of cap free
Amount of items: 2
Items: 
Size: 764274 Color: 8
Size: 234734 Color: 10

Bin 4095: 993 of cap free
Amount of items: 2
Items: 
Size: 510302 Color: 12
Size: 488706 Color: 8

Bin 4096: 995 of cap free
Amount of items: 2
Items: 
Size: 773254 Color: 14
Size: 225752 Color: 17

Bin 4097: 1008 of cap free
Amount of items: 2
Items: 
Size: 573980 Color: 4
Size: 425013 Color: 19

Bin 4098: 1011 of cap free
Amount of items: 2
Items: 
Size: 765277 Color: 19
Size: 233713 Color: 6

Bin 4099: 1012 of cap free
Amount of items: 2
Items: 
Size: 555512 Color: 11
Size: 443477 Color: 15

Bin 4100: 1014 of cap free
Amount of items: 2
Items: 
Size: 630713 Color: 1
Size: 368274 Color: 0

Bin 4101: 1018 of cap free
Amount of items: 2
Items: 
Size: 777130 Color: 9
Size: 221853 Color: 11

Bin 4102: 1022 of cap free
Amount of items: 2
Items: 
Size: 584716 Color: 8
Size: 414263 Color: 18

Bin 4103: 1023 of cap free
Amount of items: 2
Items: 
Size: 589014 Color: 9
Size: 409964 Color: 2

Bin 4104: 1028 of cap free
Amount of items: 2
Items: 
Size: 521007 Color: 14
Size: 477966 Color: 1

Bin 4105: 1032 of cap free
Amount of items: 2
Items: 
Size: 739933 Color: 10
Size: 259036 Color: 8

Bin 4106: 1032 of cap free
Amount of items: 2
Items: 
Size: 773245 Color: 11
Size: 225724 Color: 0

Bin 4107: 1035 of cap free
Amount of items: 2
Items: 
Size: 510276 Color: 14
Size: 488690 Color: 12

Bin 4108: 1040 of cap free
Amount of items: 2
Items: 
Size: 684623 Color: 12
Size: 314338 Color: 4

Bin 4109: 1053 of cap free
Amount of items: 2
Items: 
Size: 784385 Color: 11
Size: 214563 Color: 18

Bin 4110: 1053 of cap free
Amount of items: 2
Items: 
Size: 571146 Color: 15
Size: 427802 Color: 10

Bin 4111: 1058 of cap free
Amount of items: 2
Items: 
Size: 788757 Color: 12
Size: 210186 Color: 6

Bin 4112: 1059 of cap free
Amount of items: 2
Items: 
Size: 566130 Color: 1
Size: 432812 Color: 10

Bin 4113: 1061 of cap free
Amount of items: 2
Items: 
Size: 650917 Color: 16
Size: 348023 Color: 10

Bin 4114: 1063 of cap free
Amount of items: 2
Items: 
Size: 539437 Color: 7
Size: 459501 Color: 6

Bin 4115: 1066 of cap free
Amount of items: 3
Items: 
Size: 664291 Color: 2
Size: 168597 Color: 1
Size: 166047 Color: 10

Bin 4116: 1067 of cap free
Amount of items: 2
Items: 
Size: 767007 Color: 4
Size: 231927 Color: 7

Bin 4117: 1070 of cap free
Amount of items: 2
Items: 
Size: 773225 Color: 4
Size: 225706 Color: 14

Bin 4118: 1075 of cap free
Amount of items: 2
Items: 
Size: 734280 Color: 2
Size: 264646 Color: 13

Bin 4119: 1077 of cap free
Amount of items: 2
Items: 
Size: 540845 Color: 6
Size: 458079 Color: 0

Bin 4120: 1079 of cap free
Amount of items: 2
Items: 
Size: 600603 Color: 12
Size: 398319 Color: 3

Bin 4121: 1080 of cap free
Amount of items: 2
Items: 
Size: 726343 Color: 12
Size: 272578 Color: 17

Bin 4122: 1086 of cap free
Amount of items: 2
Items: 
Size: 589012 Color: 0
Size: 409903 Color: 14

Bin 4123: 1096 of cap free
Amount of items: 2
Items: 
Size: 605585 Color: 14
Size: 393320 Color: 15

Bin 4124: 1098 of cap free
Amount of items: 2
Items: 
Size: 773225 Color: 16
Size: 225678 Color: 19

Bin 4125: 1100 of cap free
Amount of items: 2
Items: 
Size: 741181 Color: 18
Size: 257720 Color: 7

Bin 4126: 1106 of cap free
Amount of items: 2
Items: 
Size: 555420 Color: 0
Size: 443475 Color: 16

Bin 4127: 1114 of cap free
Amount of items: 2
Items: 
Size: 533826 Color: 16
Size: 465061 Color: 8

Bin 4128: 1116 of cap free
Amount of items: 2
Items: 
Size: 573876 Color: 14
Size: 425009 Color: 0

Bin 4129: 1120 of cap free
Amount of items: 2
Items: 
Size: 697234 Color: 7
Size: 301647 Color: 13

Bin 4130: 1129 of cap free
Amount of items: 2
Items: 
Size: 539434 Color: 6
Size: 459438 Color: 15

Bin 4131: 1134 of cap free
Amount of items: 2
Items: 
Size: 782128 Color: 1
Size: 216739 Color: 15

Bin 4132: 1134 of cap free
Amount of items: 2
Items: 
Size: 676369 Color: 0
Size: 322498 Color: 3

Bin 4133: 1144 of cap free
Amount of items: 2
Items: 
Size: 636204 Color: 14
Size: 362653 Color: 16

Bin 4134: 1146 of cap free
Amount of items: 2
Items: 
Size: 626425 Color: 17
Size: 372430 Color: 11

Bin 4135: 1150 of cap free
Amount of items: 2
Items: 
Size: 706843 Color: 8
Size: 292008 Color: 15

Bin 4136: 1153 of cap free
Amount of items: 2
Items: 
Size: 561798 Color: 10
Size: 437050 Color: 7

Bin 4137: 1155 of cap free
Amount of items: 2
Items: 
Size: 676375 Color: 3
Size: 322471 Color: 13

Bin 4138: 1156 of cap free
Amount of items: 2
Items: 
Size: 734204 Color: 14
Size: 264641 Color: 5

Bin 4139: 1162 of cap free
Amount of items: 2
Items: 
Size: 741164 Color: 10
Size: 257675 Color: 4

Bin 4140: 1164 of cap free
Amount of items: 2
Items: 
Size: 731376 Color: 16
Size: 267461 Color: 11

Bin 4141: 1165 of cap free
Amount of items: 2
Items: 
Size: 603232 Color: 3
Size: 395604 Color: 4

Bin 4142: 1172 of cap free
Amount of items: 2
Items: 
Size: 741156 Color: 13
Size: 257673 Color: 4

Bin 4143: 1174 of cap free
Amount of items: 2
Items: 
Size: 676362 Color: 19
Size: 322465 Color: 18

Bin 4144: 1180 of cap free
Amount of items: 2
Items: 
Size: 517247 Color: 4
Size: 481574 Color: 13

Bin 4145: 1182 of cap free
Amount of items: 2
Items: 
Size: 787145 Color: 0
Size: 211674 Color: 9

Bin 4146: 1183 of cap free
Amount of items: 2
Items: 
Size: 647114 Color: 14
Size: 351704 Color: 6

Bin 4147: 1185 of cap free
Amount of items: 2
Items: 
Size: 762691 Color: 15
Size: 236125 Color: 12

Bin 4148: 1196 of cap free
Amount of items: 2
Items: 
Size: 700639 Color: 3
Size: 298166 Color: 2

Bin 4149: 1204 of cap free
Amount of items: 2
Items: 
Size: 608721 Color: 16
Size: 390076 Color: 10

Bin 4150: 1207 of cap free
Amount of items: 2
Items: 
Size: 623192 Color: 19
Size: 375602 Color: 18

Bin 4151: 1210 of cap free
Amount of items: 2
Items: 
Size: 684456 Color: 3
Size: 314335 Color: 2

Bin 4152: 1219 of cap free
Amount of items: 2
Items: 
Size: 753065 Color: 6
Size: 245717 Color: 15

Bin 4153: 1223 of cap free
Amount of items: 2
Items: 
Size: 700612 Color: 12
Size: 298166 Color: 18

Bin 4154: 1224 of cap free
Amount of items: 2
Items: 
Size: 626391 Color: 14
Size: 372386 Color: 9

Bin 4155: 1228 of cap free
Amount of items: 2
Items: 
Size: 564593 Color: 14
Size: 434180 Color: 19

Bin 4156: 1248 of cap free
Amount of items: 2
Items: 
Size: 782091 Color: 17
Size: 216662 Color: 12

Bin 4157: 1248 of cap free
Amount of items: 2
Items: 
Size: 784301 Color: 2
Size: 214452 Color: 16

Bin 4158: 1254 of cap free
Amount of items: 2
Items: 
Size: 566069 Color: 1
Size: 432678 Color: 19

Bin 4159: 1256 of cap free
Amount of items: 2
Items: 
Size: 776835 Color: 19
Size: 221910 Color: 9

Bin 4160: 1262 of cap free
Amount of items: 2
Items: 
Size: 739822 Color: 7
Size: 258917 Color: 6

Bin 4161: 1266 of cap free
Amount of items: 2
Items: 
Size: 539421 Color: 14
Size: 459314 Color: 9

Bin 4162: 1276 of cap free
Amount of items: 2
Items: 
Size: 533748 Color: 16
Size: 464977 Color: 1

Bin 4163: 1276 of cap free
Amount of items: 2
Items: 
Size: 705199 Color: 15
Size: 293526 Color: 16

Bin 4164: 1280 of cap free
Amount of items: 2
Items: 
Size: 739820 Color: 4
Size: 258901 Color: 12

Bin 4165: 1288 of cap free
Amount of items: 2
Items: 
Size: 636134 Color: 9
Size: 362579 Color: 5

Bin 4166: 1290 of cap free
Amount of items: 2
Items: 
Size: 582105 Color: 10
Size: 416606 Color: 15

Bin 4167: 1295 of cap free
Amount of items: 2
Items: 
Size: 728392 Color: 14
Size: 270314 Color: 12

Bin 4168: 1296 of cap free
Amount of items: 2
Items: 
Size: 548693 Color: 18
Size: 450012 Color: 6

Bin 4169: 1308 of cap free
Amount of items: 2
Items: 
Size: 647053 Color: 1
Size: 351640 Color: 2

Bin 4170: 1308 of cap free
Amount of items: 2
Items: 
Size: 611478 Color: 3
Size: 387215 Color: 7

Bin 4171: 1316 of cap free
Amount of items: 2
Items: 
Size: 596811 Color: 11
Size: 401874 Color: 10

Bin 4172: 1328 of cap free
Amount of items: 2
Items: 
Size: 566084 Color: 19
Size: 432589 Color: 18

Bin 4173: 1336 of cap free
Amount of items: 2
Items: 
Size: 617500 Color: 16
Size: 381165 Color: 3

Bin 4174: 1338 of cap free
Amount of items: 2
Items: 
Size: 788566 Color: 15
Size: 210097 Color: 12

Bin 4175: 1362 of cap free
Amount of items: 2
Items: 
Size: 548600 Color: 1
Size: 450039 Color: 18

Bin 4176: 1369 of cap free
Amount of items: 2
Items: 
Size: 638916 Color: 0
Size: 359716 Color: 5

Bin 4177: 1371 of cap free
Amount of items: 2
Items: 
Size: 797800 Color: 15
Size: 200830 Color: 3

Bin 4178: 1376 of cap free
Amount of items: 2
Items: 
Size: 603144 Color: 12
Size: 395481 Color: 19

Bin 4179: 1382 of cap free
Amount of items: 2
Items: 
Size: 616099 Color: 6
Size: 382520 Color: 13

Bin 4180: 1400 of cap free
Amount of items: 2
Items: 
Size: 561595 Color: 8
Size: 437006 Color: 10

Bin 4181: 1408 of cap free
Amount of items: 2
Items: 
Size: 744026 Color: 8
Size: 254567 Color: 17

Bin 4182: 1415 of cap free
Amount of items: 2
Items: 
Size: 723820 Color: 6
Size: 274766 Color: 8

Bin 4183: 1419 of cap free
Amount of items: 2
Items: 
Size: 715273 Color: 13
Size: 283309 Color: 15

Bin 4184: 1441 of cap free
Amount of items: 2
Items: 
Size: 533636 Color: 1
Size: 464924 Color: 6

Bin 4185: 1455 of cap free
Amount of items: 2
Items: 
Size: 561563 Color: 16
Size: 436983 Color: 12

Bin 4186: 1457 of cap free
Amount of items: 2
Items: 
Size: 540745 Color: 18
Size: 457799 Color: 16

Bin 4187: 1458 of cap free
Amount of items: 2
Items: 
Size: 671523 Color: 5
Size: 327020 Color: 13

Bin 4188: 1469 of cap free
Amount of items: 2
Items: 
Size: 580591 Color: 8
Size: 417941 Color: 10

Bin 4189: 1473 of cap free
Amount of items: 2
Items: 
Size: 627889 Color: 4
Size: 370639 Color: 0

Bin 4190: 1473 of cap free
Amount of items: 2
Items: 
Size: 548566 Color: 9
Size: 449962 Color: 13

Bin 4191: 1483 of cap free
Amount of items: 2
Items: 
Size: 545572 Color: 19
Size: 452946 Color: 3

Bin 4192: 1491 of cap free
Amount of items: 2
Items: 
Size: 638866 Color: 15
Size: 359644 Color: 17

Bin 4193: 1504 of cap free
Amount of items: 2
Items: 
Size: 530743 Color: 4
Size: 467754 Color: 14

Bin 4194: 1510 of cap free
Amount of items: 2
Items: 
Size: 770107 Color: 11
Size: 228384 Color: 9

Bin 4195: 1513 of cap free
Amount of items: 2
Items: 
Size: 789993 Color: 10
Size: 208495 Color: 8

Bin 4196: 1520 of cap free
Amount of items: 2
Items: 
Size: 713122 Color: 3
Size: 285359 Color: 1

Bin 4197: 1524 of cap free
Amount of items: 2
Items: 
Size: 680871 Color: 10
Size: 317606 Color: 5

Bin 4198: 1525 of cap free
Amount of items: 2
Items: 
Size: 608421 Color: 12
Size: 390055 Color: 14

Bin 4199: 1527 of cap free
Amount of items: 2
Items: 
Size: 694619 Color: 16
Size: 303855 Color: 19

Bin 4200: 1545 of cap free
Amount of items: 2
Items: 
Size: 797435 Color: 12
Size: 201021 Color: 15

Bin 4201: 1556 of cap free
Amount of items: 2
Items: 
Size: 633715 Color: 6
Size: 364730 Color: 15

Bin 4202: 1570 of cap free
Amount of items: 2
Items: 
Size: 608404 Color: 2
Size: 390027 Color: 5

Bin 4203: 1571 of cap free
Amount of items: 2
Items: 
Size: 555411 Color: 17
Size: 443019 Color: 0

Bin 4204: 1582 of cap free
Amount of items: 2
Items: 
Size: 602974 Color: 3
Size: 395445 Color: 18

Bin 4205: 1608 of cap free
Amount of items: 2
Items: 
Size: 578245 Color: 9
Size: 420148 Color: 12

Bin 4206: 1611 of cap free
Amount of items: 2
Items: 
Size: 661164 Color: 19
Size: 337226 Color: 13

Bin 4207: 1612 of cap free
Amount of items: 2
Items: 
Size: 687616 Color: 12
Size: 310773 Color: 17

Bin 4208: 1617 of cap free
Amount of items: 2
Items: 
Size: 611275 Color: 0
Size: 387109 Color: 17

Bin 4209: 1624 of cap free
Amount of items: 2
Items: 
Size: 789902 Color: 12
Size: 208475 Color: 16

Bin 4210: 1654 of cap free
Amount of items: 2
Items: 
Size: 548472 Color: 13
Size: 449875 Color: 14

Bin 4211: 1656 of cap free
Amount of items: 2
Items: 
Size: 608336 Color: 10
Size: 390009 Color: 14

Bin 4212: 1658 of cap free
Amount of items: 2
Items: 
Size: 557623 Color: 9
Size: 440720 Color: 13

Bin 4213: 1663 of cap free
Amount of items: 2
Items: 
Size: 505094 Color: 19
Size: 493244 Color: 4

Bin 4214: 1670 of cap free
Amount of items: 2
Items: 
Size: 691652 Color: 0
Size: 306679 Color: 17

Bin 4215: 1680 of cap free
Amount of items: 2
Items: 
Size: 633701 Color: 15
Size: 364620 Color: 2

Bin 4216: 1694 of cap free
Amount of items: 2
Items: 
Size: 548465 Color: 16
Size: 449842 Color: 1

Bin 4217: 1713 of cap free
Amount of items: 2
Items: 
Size: 526395 Color: 18
Size: 471893 Color: 12

Bin 4218: 1727 of cap free
Amount of items: 2
Items: 
Size: 557605 Color: 18
Size: 440669 Color: 6

Bin 4219: 1731 of cap free
Amount of items: 2
Items: 
Size: 533528 Color: 16
Size: 464742 Color: 13

Bin 4220: 1740 of cap free
Amount of items: 2
Items: 
Size: 750044 Color: 17
Size: 248217 Color: 12

Bin 4221: 1742 of cap free
Amount of items: 2
Items: 
Size: 557592 Color: 19
Size: 440667 Color: 8

Bin 4222: 1756 of cap free
Amount of items: 2
Items: 
Size: 505006 Color: 11
Size: 493239 Color: 12

Bin 4223: 1756 of cap free
Amount of items: 2
Items: 
Size: 611238 Color: 2
Size: 387007 Color: 11

Bin 4224: 1766 of cap free
Amount of items: 2
Items: 
Size: 618009 Color: 3
Size: 380226 Color: 7

Bin 4225: 1771 of cap free
Amount of items: 2
Items: 
Size: 565827 Color: 10
Size: 432403 Color: 17

Bin 4226: 1785 of cap free
Amount of items: 2
Items: 
Size: 797388 Color: 10
Size: 200828 Color: 6

Bin 4227: 1799 of cap free
Amount of items: 2
Items: 
Size: 638766 Color: 19
Size: 359436 Color: 3

Bin 4228: 1800 of cap free
Amount of items: 2
Items: 
Size: 611207 Color: 15
Size: 386994 Color: 4

Bin 4229: 1818 of cap free
Amount of items: 2
Items: 
Size: 530512 Color: 2
Size: 467671 Color: 1

Bin 4230: 1826 of cap free
Amount of items: 2
Items: 
Size: 526334 Color: 18
Size: 471841 Color: 3

Bin 4231: 1840 of cap free
Amount of items: 2
Items: 
Size: 530514 Color: 12
Size: 467647 Color: 2

Bin 4232: 1857 of cap free
Amount of items: 2
Items: 
Size: 797353 Color: 5
Size: 200791 Color: 8

Bin 4233: 1865 of cap free
Amount of items: 2
Items: 
Size: 608139 Color: 5
Size: 389997 Color: 4

Bin 4234: 1871 of cap free
Amount of items: 2
Items: 
Size: 792291 Color: 14
Size: 205839 Color: 3

Bin 4235: 1878 of cap free
Amount of items: 2
Items: 
Size: 683851 Color: 4
Size: 314272 Color: 14

Bin 4236: 1895 of cap free
Amount of items: 2
Items: 
Size: 725631 Color: 11
Size: 272475 Color: 3

Bin 4237: 1908 of cap free
Amount of items: 2
Items: 
Size: 552775 Color: 9
Size: 445318 Color: 15

Bin 4238: 1909 of cap free
Amount of items: 2
Items: 
Size: 548257 Color: 0
Size: 449835 Color: 3

Bin 4239: 1911 of cap free
Amount of items: 2
Items: 
Size: 608094 Color: 12
Size: 389996 Color: 8

Bin 4240: 1915 of cap free
Amount of items: 2
Items: 
Size: 594031 Color: 15
Size: 404055 Color: 7

Bin 4241: 1917 of cap free
Amount of items: 2
Items: 
Size: 584446 Color: 12
Size: 413638 Color: 14

Bin 4242: 1920 of cap free
Amount of items: 2
Items: 
Size: 757965 Color: 3
Size: 240116 Color: 16

Bin 4243: 1931 of cap free
Amount of items: 2
Items: 
Size: 782068 Color: 16
Size: 216002 Color: 11

Bin 4244: 1938 of cap free
Amount of items: 2
Items: 
Size: 715301 Color: 15
Size: 282762 Color: 16

Bin 4245: 1954 of cap free
Amount of items: 2
Items: 
Size: 525764 Color: 3
Size: 472283 Color: 18

Bin 4246: 1977 of cap free
Amount of items: 2
Items: 
Size: 696400 Color: 14
Size: 301624 Color: 5

Bin 4247: 1979 of cap free
Amount of items: 2
Items: 
Size: 710291 Color: 18
Size: 287731 Color: 16

Bin 4248: 1985 of cap free
Amount of items: 2
Items: 
Size: 643745 Color: 14
Size: 354271 Color: 18

Bin 4249: 2001 of cap free
Amount of items: 2
Items: 
Size: 680367 Color: 1
Size: 317633 Color: 10

Bin 4250: 2005 of cap free
Amount of items: 2
Items: 
Size: 573153 Color: 15
Size: 424843 Color: 12

Bin 4251: 2009 of cap free
Amount of items: 2
Items: 
Size: 509796 Color: 13
Size: 488196 Color: 17

Bin 4252: 2018 of cap free
Amount of items: 2
Items: 
Size: 555037 Color: 11
Size: 442946 Color: 15

Bin 4253: 2028 of cap free
Amount of items: 2
Items: 
Size: 577859 Color: 10
Size: 420114 Color: 6

Bin 4254: 2031 of cap free
Amount of items: 2
Items: 
Size: 635938 Color: 5
Size: 362032 Color: 7

Bin 4255: 2034 of cap free
Amount of items: 2
Items: 
Size: 552671 Color: 9
Size: 445296 Color: 16

Bin 4256: 2040 of cap free
Amount of items: 2
Items: 
Size: 696391 Color: 11
Size: 301570 Color: 3

Bin 4257: 2068 of cap free
Amount of items: 2
Items: 
Size: 797333 Color: 8
Size: 200600 Color: 14

Bin 4258: 2073 of cap free
Amount of items: 2
Items: 
Size: 604673 Color: 3
Size: 393255 Color: 8

Bin 4259: 2074 of cap free
Amount of items: 2
Items: 
Size: 712827 Color: 11
Size: 285100 Color: 18

Bin 4260: 2079 of cap free
Amount of items: 2
Items: 
Size: 530477 Color: 3
Size: 467445 Color: 18

Bin 4261: 2085 of cap free
Amount of items: 2
Items: 
Size: 683743 Color: 16
Size: 314173 Color: 5

Bin 4262: 2093 of cap free
Amount of items: 2
Items: 
Size: 696351 Color: 14
Size: 301557 Color: 17

Bin 4263: 2093 of cap free
Amount of items: 2
Items: 
Size: 710255 Color: 19
Size: 287653 Color: 0

Bin 4264: 2100 of cap free
Amount of items: 2
Items: 
Size: 680314 Color: 15
Size: 317587 Color: 1

Bin 4265: 2111 of cap free
Amount of items: 2
Items: 
Size: 593959 Color: 4
Size: 403931 Color: 14

Bin 4266: 2114 of cap free
Amount of items: 2
Items: 
Size: 712807 Color: 16
Size: 285080 Color: 12

Bin 4267: 2121 of cap free
Amount of items: 2
Items: 
Size: 577787 Color: 18
Size: 420093 Color: 4

Bin 4268: 2126 of cap free
Amount of items: 2
Items: 
Size: 797307 Color: 19
Size: 200568 Color: 2

Bin 4269: 2134 of cap free
Amount of items: 2
Items: 
Size: 691344 Color: 11
Size: 306523 Color: 12

Bin 4270: 2142 of cap free
Amount of items: 2
Items: 
Size: 581292 Color: 10
Size: 416567 Color: 2

Bin 4271: 2148 of cap free
Amount of items: 2
Items: 
Size: 683738 Color: 17
Size: 314115 Color: 11

Bin 4272: 2170 of cap free
Amount of items: 2
Items: 
Size: 781982 Color: 9
Size: 215849 Color: 12

Bin 4273: 2179 of cap free
Amount of items: 2
Items: 
Size: 593897 Color: 2
Size: 403925 Color: 13

Bin 4274: 2192 of cap free
Amount of items: 2
Items: 
Size: 604631 Color: 4
Size: 393178 Color: 5

Bin 4275: 2196 of cap free
Amount of items: 2
Items: 
Size: 588861 Color: 19
Size: 408944 Color: 18

Bin 4276: 2208 of cap free
Amount of items: 2
Items: 
Size: 657663 Color: 5
Size: 340130 Color: 1

Bin 4277: 2242 of cap free
Amount of items: 2
Items: 
Size: 665425 Color: 17
Size: 332334 Color: 2

Bin 4278: 2249 of cap free
Amount of items: 2
Items: 
Size: 588851 Color: 7
Size: 408901 Color: 14

Bin 4279: 2257 of cap free
Amount of items: 2
Items: 
Size: 584126 Color: 0
Size: 413618 Color: 9

Bin 4280: 2260 of cap free
Amount of items: 2
Items: 
Size: 622152 Color: 9
Size: 375589 Color: 11

Bin 4281: 2263 of cap free
Amount of items: 2
Items: 
Size: 769847 Color: 13
Size: 227891 Color: 0

Bin 4282: 2276 of cap free
Amount of items: 2
Items: 
Size: 730422 Color: 14
Size: 267303 Color: 9

Bin 4283: 2279 of cap free
Amount of items: 2
Items: 
Size: 665420 Color: 14
Size: 332302 Color: 16

Bin 4284: 2280 of cap free
Amount of items: 2
Items: 
Size: 530282 Color: 12
Size: 467439 Color: 17

Bin 4285: 2281 of cap free
Amount of items: 2
Items: 
Size: 754451 Color: 6
Size: 243269 Color: 2

Bin 4286: 2282 of cap free
Amount of items: 2
Items: 
Size: 509589 Color: 18
Size: 488130 Color: 9

Bin 4287: 2295 of cap free
Amount of items: 2
Items: 
Size: 509583 Color: 19
Size: 488123 Color: 10

Bin 4288: 2295 of cap free
Amount of items: 2
Items: 
Size: 781898 Color: 15
Size: 215808 Color: 7

Bin 4289: 2298 of cap free
Amount of items: 2
Items: 
Size: 797162 Color: 1
Size: 200541 Color: 0

Bin 4290: 2303 of cap free
Amount of items: 2
Items: 
Size: 743632 Color: 19
Size: 254066 Color: 14

Bin 4291: 2309 of cap free
Amount of items: 2
Items: 
Size: 622127 Color: 2
Size: 375565 Color: 1

Bin 4292: 2321 of cap free
Amount of items: 2
Items: 
Size: 584097 Color: 19
Size: 413583 Color: 2

Bin 4293: 2322 of cap free
Amount of items: 2
Items: 
Size: 538391 Color: 14
Size: 459288 Color: 19

Bin 4294: 2328 of cap free
Amount of items: 2
Items: 
Size: 691336 Color: 8
Size: 306337 Color: 3

Bin 4295: 2331 of cap free
Amount of items: 2
Items: 
Size: 607676 Color: 3
Size: 389994 Color: 7

Bin 4296: 2362 of cap free
Amount of items: 2
Items: 
Size: 516992 Color: 6
Size: 480647 Color: 10

Bin 4297: 2366 of cap free
Amount of items: 2
Items: 
Size: 638483 Color: 14
Size: 359152 Color: 5

Bin 4298: 2370 of cap free
Amount of items: 2
Items: 
Size: 743566 Color: 17
Size: 254065 Color: 2

Bin 4299: 2383 of cap free
Amount of items: 2
Items: 
Size: 604544 Color: 18
Size: 393074 Color: 16

Bin 4300: 2405 of cap free
Amount of items: 2
Items: 
Size: 781863 Color: 1
Size: 215733 Color: 18

Bin 4301: 2412 of cap free
Amount of items: 2
Items: 
Size: 516947 Color: 6
Size: 480642 Color: 18

Bin 4302: 2417 of cap free
Amount of items: 2
Items: 
Size: 547807 Color: 10
Size: 449777 Color: 12

Bin 4303: 2432 of cap free
Amount of items: 2
Items: 
Size: 547793 Color: 13
Size: 449776 Color: 9

Bin 4304: 2441 of cap free
Amount of items: 2
Items: 
Size: 635710 Color: 5
Size: 361850 Color: 12

Bin 4305: 2458 of cap free
Amount of items: 2
Items: 
Size: 530110 Color: 10
Size: 467433 Color: 16

Bin 4306: 2472 of cap free
Amount of items: 2
Items: 
Size: 577778 Color: 7
Size: 419751 Color: 2

Bin 4307: 2477 of cap free
Amount of items: 2
Items: 
Size: 683698 Color: 1
Size: 313826 Color: 4

Bin 4308: 2497 of cap free
Amount of items: 2
Items: 
Size: 769842 Color: 13
Size: 227662 Color: 15

Bin 4309: 2502 of cap free
Amount of items: 2
Items: 
Size: 602123 Color: 10
Size: 395376 Color: 18

Bin 4310: 2517 of cap free
Amount of items: 2
Items: 
Size: 533410 Color: 17
Size: 464074 Color: 19

Bin 4311: 2523 of cap free
Amount of items: 2
Items: 
Size: 538199 Color: 19
Size: 459279 Color: 8

Bin 4312: 2533 of cap free
Amount of items: 2
Items: 
Size: 583939 Color: 16
Size: 413529 Color: 5

Bin 4313: 2536 of cap free
Amount of items: 2
Items: 
Size: 683687 Color: 16
Size: 313778 Color: 0

Bin 4314: 2538 of cap free
Amount of items: 2
Items: 
Size: 797095 Color: 18
Size: 200368 Color: 5

Bin 4315: 2549 of cap free
Amount of items: 2
Items: 
Size: 750037 Color: 10
Size: 247415 Color: 1

Bin 4316: 2554 of cap free
Amount of items: 2
Items: 
Size: 504608 Color: 9
Size: 492839 Color: 11

Bin 4317: 2573 of cap free
Amount of items: 2
Items: 
Size: 509532 Color: 0
Size: 487896 Color: 15

Bin 4318: 2612 of cap free
Amount of items: 2
Items: 
Size: 529960 Color: 9
Size: 467429 Color: 16

Bin 4319: 2616 of cap free
Amount of items: 2
Items: 
Size: 599464 Color: 1
Size: 397921 Color: 16

Bin 4320: 2620 of cap free
Amount of items: 2
Items: 
Size: 743404 Color: 5
Size: 253977 Color: 18

Bin 4321: 2628 of cap free
Amount of items: 2
Items: 
Size: 533336 Color: 1
Size: 464037 Color: 5

Bin 4322: 2630 of cap free
Amount of items: 2
Items: 
Size: 750009 Color: 16
Size: 247362 Color: 7

Bin 4323: 2644 of cap free
Amount of items: 2
Items: 
Size: 769780 Color: 2
Size: 227577 Color: 5

Bin 4324: 2648 of cap free
Amount of items: 2
Items: 
Size: 730421 Color: 7
Size: 266932 Color: 6

Bin 4325: 2654 of cap free
Amount of items: 2
Items: 
Size: 529954 Color: 18
Size: 467393 Color: 11

Bin 4326: 2655 of cap free
Amount of items: 2
Items: 
Size: 683670 Color: 7
Size: 313676 Color: 8

Bin 4327: 2661 of cap free
Amount of items: 2
Items: 
Size: 739777 Color: 10
Size: 257563 Color: 8

Bin 4328: 2668 of cap free
Amount of items: 2
Items: 
Size: 797066 Color: 5
Size: 200267 Color: 9

Bin 4329: 2670 of cap free
Amount of items: 2
Items: 
Size: 652135 Color: 12
Size: 345196 Color: 10

Bin 4330: 2699 of cap free
Amount of items: 2
Items: 
Size: 673483 Color: 12
Size: 323819 Color: 1

Bin 4331: 2705 of cap free
Amount of items: 2
Items: 
Size: 577451 Color: 18
Size: 419845 Color: 7

Bin 4332: 2752 of cap free
Amount of items: 2
Items: 
Size: 557069 Color: 8
Size: 440180 Color: 4

Bin 4333: 2773 of cap free
Amount of items: 2
Items: 
Size: 769740 Color: 1
Size: 227488 Color: 12

Bin 4334: 2802 of cap free
Amount of items: 2
Items: 
Size: 730383 Color: 19
Size: 266816 Color: 13

Bin 4335: 2806 of cap free
Amount of items: 2
Items: 
Size: 739666 Color: 17
Size: 257529 Color: 8

Bin 4336: 2815 of cap free
Amount of items: 2
Items: 
Size: 725475 Color: 14
Size: 271711 Color: 9

Bin 4337: 2832 of cap free
Amount of items: 2
Items: 
Size: 739658 Color: 6
Size: 257511 Color: 12

Bin 4338: 2856 of cap free
Amount of items: 2
Items: 
Size: 739636 Color: 11
Size: 257509 Color: 19

Bin 4339: 2881 of cap free
Amount of items: 2
Items: 
Size: 551842 Color: 3
Size: 445278 Color: 12

Bin 4340: 2881 of cap free
Amount of items: 2
Items: 
Size: 683651 Color: 3
Size: 313469 Color: 18

Bin 4341: 2885 of cap free
Amount of items: 2
Items: 
Size: 652062 Color: 14
Size: 345054 Color: 15

Bin 4342: 2908 of cap free
Amount of items: 2
Items: 
Size: 622063 Color: 15
Size: 375030 Color: 11

Bin 4343: 2923 of cap free
Amount of items: 2
Items: 
Size: 516551 Color: 4
Size: 480527 Color: 7

Bin 4344: 2941 of cap free
Amount of items: 2
Items: 
Size: 499348 Color: 18
Size: 497712 Color: 11

Bin 4345: 2973 of cap free
Amount of items: 2
Items: 
Size: 788747 Color: 12
Size: 208281 Color: 0

Bin 4346: 3009 of cap free
Amount of items: 2
Items: 
Size: 621973 Color: 5
Size: 375019 Color: 1

Bin 4347: 3037 of cap free
Amount of items: 2
Items: 
Size: 680213 Color: 8
Size: 316751 Color: 14

Bin 4348: 3057 of cap free
Amount of items: 2
Items: 
Size: 607600 Color: 8
Size: 389344 Color: 19

Bin 4349: 3062 of cap free
Amount of items: 2
Items: 
Size: 652062 Color: 15
Size: 344877 Color: 4

Bin 4350: 3071 of cap free
Amount of items: 2
Items: 
Size: 607631 Color: 19
Size: 389299 Color: 7

Bin 4351: 3073 of cap free
Amount of items: 2
Items: 
Size: 537666 Color: 18
Size: 459262 Color: 14

Bin 4352: 3080 of cap free
Amount of items: 2
Items: 
Size: 739607 Color: 4
Size: 257314 Color: 13

Bin 4353: 3105 of cap free
Amount of items: 2
Items: 
Size: 683595 Color: 1
Size: 313301 Color: 3

Bin 4354: 3113 of cap free
Amount of items: 2
Items: 
Size: 638631 Color: 5
Size: 358257 Color: 1

Bin 4355: 3158 of cap free
Amount of items: 2
Items: 
Size: 647050 Color: 18
Size: 349793 Color: 6

Bin 4356: 3170 of cap free
Amount of items: 2
Items: 
Size: 739534 Color: 12
Size: 257297 Color: 9

Bin 4357: 3205 of cap free
Amount of items: 2
Items: 
Size: 769357 Color: 7
Size: 227439 Color: 19

Bin 4358: 3214 of cap free
Amount of items: 2
Items: 
Size: 607531 Color: 8
Size: 389256 Color: 0

Bin 4359: 3240 of cap free
Amount of items: 2
Items: 
Size: 532759 Color: 4
Size: 464002 Color: 6

Bin 4360: 3294 of cap free
Amount of items: 2
Items: 
Size: 557092 Color: 4
Size: 439615 Color: 17

Bin 4361: 3295 of cap free
Amount of items: 2
Items: 
Size: 509391 Color: 7
Size: 487315 Color: 8

Bin 4362: 3295 of cap free
Amount of items: 2
Items: 
Size: 739476 Color: 14
Size: 257230 Color: 0

Bin 4363: 3325 of cap free
Amount of items: 2
Items: 
Size: 509387 Color: 12
Size: 487289 Color: 7

Bin 4364: 3385 of cap free
Amount of items: 2
Items: 
Size: 788546 Color: 7
Size: 208070 Color: 18

Bin 4365: 3438 of cap free
Amount of items: 2
Items: 
Size: 547744 Color: 17
Size: 448819 Color: 9

Bin 4366: 3445 of cap free
Amount of items: 2
Items: 
Size: 788504 Color: 12
Size: 208052 Color: 16

Bin 4367: 3451 of cap free
Amount of items: 2
Items: 
Size: 696312 Color: 15
Size: 300238 Color: 14

Bin 4368: 3453 of cap free
Amount of items: 2
Items: 
Size: 524933 Color: 19
Size: 471615 Color: 18

Bin 4369: 3499 of cap free
Amount of items: 2
Items: 
Size: 769277 Color: 2
Size: 227225 Color: 5

Bin 4370: 3500 of cap free
Amount of items: 2
Items: 
Size: 551227 Color: 4
Size: 445274 Color: 5

Bin 4371: 3545 of cap free
Amount of items: 2
Items: 
Size: 547646 Color: 17
Size: 448810 Color: 13

Bin 4372: 3562 of cap free
Amount of items: 2
Items: 
Size: 564504 Color: 11
Size: 431935 Color: 1

Bin 4373: 3582 of cap free
Amount of items: 2
Items: 
Size: 683560 Color: 11
Size: 312859 Color: 17

Bin 4374: 3603 of cap free
Amount of items: 2
Items: 
Size: 796169 Color: 1
Size: 200229 Color: 9

Bin 4375: 3606 of cap free
Amount of items: 2
Items: 
Size: 762691 Color: 0
Size: 233704 Color: 4

Bin 4376: 3607 of cap free
Amount of items: 2
Items: 
Size: 547604 Color: 12
Size: 448790 Color: 11

Bin 4377: 3608 of cap free
Amount of items: 3
Items: 
Size: 739468 Color: 8
Size: 133001 Color: 10
Size: 123924 Color: 15

Bin 4378: 3619 of cap free
Amount of items: 2
Items: 
Size: 607523 Color: 13
Size: 388859 Color: 1

Bin 4379: 3625 of cap free
Amount of items: 2
Items: 
Size: 683519 Color: 16
Size: 312857 Color: 17

Bin 4380: 3650 of cap free
Amount of items: 2
Items: 
Size: 607499 Color: 1
Size: 388852 Color: 15

Bin 4381: 3675 of cap free
Amount of items: 2
Items: 
Size: 588842 Color: 16
Size: 407484 Color: 12

Bin 4382: 3680 of cap free
Amount of items: 2
Items: 
Size: 524719 Color: 10
Size: 471602 Color: 3

Bin 4383: 3683 of cap free
Amount of items: 2
Items: 
Size: 543386 Color: 1
Size: 452932 Color: 2

Bin 4384: 3700 of cap free
Amount of items: 2
Items: 
Size: 509373 Color: 12
Size: 486928 Color: 8

Bin 4385: 3705 of cap free
Amount of items: 2
Items: 
Size: 762615 Color: 11
Size: 233681 Color: 1

Bin 4386: 3705 of cap free
Amount of items: 2
Items: 
Size: 769277 Color: 1
Size: 227019 Color: 12

Bin 4387: 3734 of cap free
Amount of items: 2
Items: 
Size: 652045 Color: 0
Size: 344222 Color: 18

Bin 4388: 3767 of cap free
Amount of items: 2
Items: 
Size: 588794 Color: 11
Size: 407440 Color: 2

Bin 4389: 3794 of cap free
Amount of items: 2
Items: 
Size: 588779 Color: 19
Size: 407428 Color: 16

Bin 4390: 3802 of cap free
Amount of items: 2
Items: 
Size: 651985 Color: 13
Size: 344214 Color: 9

Bin 4391: 3806 of cap free
Amount of items: 2
Items: 
Size: 762547 Color: 18
Size: 233648 Color: 13

Bin 4392: 3832 of cap free
Amount of items: 2
Items: 
Size: 615947 Color: 11
Size: 380222 Color: 12

Bin 4393: 3854 of cap free
Amount of items: 2
Items: 
Size: 543302 Color: 5
Size: 452845 Color: 3

Bin 4394: 3859 of cap free
Amount of items: 2
Items: 
Size: 607300 Color: 16
Size: 388842 Color: 1

Bin 4395: 3870 of cap free
Amount of items: 2
Items: 
Size: 615931 Color: 4
Size: 380200 Color: 5

Bin 4396: 3919 of cap free
Amount of items: 2
Items: 
Size: 547301 Color: 2
Size: 448781 Color: 1

Bin 4397: 3921 of cap free
Amount of items: 2
Items: 
Size: 651984 Color: 1
Size: 344096 Color: 0

Bin 4398: 3932 of cap free
Amount of items: 2
Items: 
Size: 796063 Color: 12
Size: 200006 Color: 18

Bin 4399: 3942 of cap free
Amount of items: 2
Items: 
Size: 756875 Color: 16
Size: 239184 Color: 1

Bin 4400: 4007 of cap free
Amount of items: 2
Items: 
Size: 509361 Color: 5
Size: 486633 Color: 6

Bin 4401: 4012 of cap free
Amount of items: 2
Items: 
Size: 796056 Color: 8
Size: 199933 Color: 12

Bin 4402: 4022 of cap free
Amount of items: 2
Items: 
Size: 543146 Color: 2
Size: 452833 Color: 7

Bin 4403: 4032 of cap free
Amount of items: 2
Items: 
Size: 734013 Color: 18
Size: 261956 Color: 14

Bin 4404: 4074 of cap free
Amount of items: 2
Items: 
Size: 768950 Color: 15
Size: 226977 Color: 7

Bin 4405: 4151 of cap free
Amount of items: 2
Items: 
Size: 615671 Color: 0
Size: 380179 Color: 9

Bin 4406: 4205 of cap free
Amount of items: 2
Items: 
Size: 756963 Color: 1
Size: 238833 Color: 13

Bin 4407: 4282 of cap free
Amount of items: 2
Items: 
Size: 781837 Color: 3
Size: 213882 Color: 2

Bin 4408: 4282 of cap free
Amount of items: 2
Items: 
Size: 563785 Color: 12
Size: 431934 Color: 18

Bin 4409: 4324 of cap free
Amount of items: 2
Items: 
Size: 615628 Color: 1
Size: 380049 Color: 7

Bin 4410: 4346 of cap free
Amount of items: 2
Items: 
Size: 795807 Color: 6
Size: 199848 Color: 0

Bin 4411: 4445 of cap free
Amount of items: 2
Items: 
Size: 795798 Color: 17
Size: 199758 Color: 15

Bin 4412: 4487 of cap free
Amount of items: 2
Items: 
Size: 615468 Color: 17
Size: 380046 Color: 0

Bin 4413: 4498 of cap free
Amount of items: 2
Items: 
Size: 781695 Color: 5
Size: 213808 Color: 3

Bin 4414: 4675 of cap free
Amount of items: 2
Items: 
Size: 509118 Color: 12
Size: 486208 Color: 18

Bin 4415: 4737 of cap free
Amount of items: 2
Items: 
Size: 672995 Color: 16
Size: 322269 Color: 1

Bin 4416: 4738 of cap free
Amount of items: 2
Items: 
Size: 509072 Color: 17
Size: 486191 Color: 6

Bin 4417: 4786 of cap free
Amount of items: 2
Items: 
Size: 497701 Color: 3
Size: 497514 Color: 1

Bin 4418: 4786 of cap free
Amount of items: 2
Items: 
Size: 795760 Color: 9
Size: 199455 Color: 19

Bin 4419: 4904 of cap free
Amount of items: 2
Items: 
Size: 781573 Color: 13
Size: 213524 Color: 2

Bin 4420: 4909 of cap free
Amount of items: 2
Items: 
Size: 537470 Color: 18
Size: 457622 Color: 3

Bin 4421: 4993 of cap free
Amount of items: 2
Items: 
Size: 795752 Color: 19
Size: 199256 Color: 1

Bin 4422: 5132 of cap free
Amount of items: 2
Items: 
Size: 563450 Color: 7
Size: 431419 Color: 2

Bin 4423: 5140 of cap free
Amount of items: 2
Items: 
Size: 497466 Color: 0
Size: 497395 Color: 2

Bin 4424: 5164 of cap free
Amount of items: 2
Items: 
Size: 615315 Color: 19
Size: 379522 Color: 14

Bin 4425: 5218 of cap free
Amount of items: 2
Items: 
Size: 781268 Color: 19
Size: 213515 Color: 12

Bin 4426: 5248 of cap free
Amount of items: 2
Items: 
Size: 537403 Color: 7
Size: 457350 Color: 5

Bin 4427: 5289 of cap free
Amount of items: 2
Items: 
Size: 537364 Color: 3
Size: 457348 Color: 6

Bin 4428: 5338 of cap free
Amount of items: 2
Items: 
Size: 712769 Color: 2
Size: 281894 Color: 1

Bin 4429: 5349 of cap free
Amount of items: 2
Items: 
Size: 537342 Color: 15
Size: 457310 Color: 18

Bin 4430: 5419 of cap free
Amount of items: 2
Items: 
Size: 563191 Color: 5
Size: 431391 Color: 6

Bin 4431: 5483 of cap free
Amount of items: 2
Items: 
Size: 563142 Color: 1
Size: 431376 Color: 18

Bin 4432: 5541 of cap free
Amount of items: 2
Items: 
Size: 563094 Color: 8
Size: 431366 Color: 13

Bin 4433: 5688 of cap free
Amount of items: 2
Items: 
Size: 563091 Color: 7
Size: 431222 Color: 9

Bin 4434: 5767 of cap free
Amount of items: 2
Items: 
Size: 607280 Color: 15
Size: 386954 Color: 12

Bin 4435: 5791 of cap free
Amount of items: 2
Items: 
Size: 756735 Color: 3
Size: 237475 Color: 19

Bin 4436: 5816 of cap free
Amount of items: 2
Items: 
Size: 712363 Color: 9
Size: 281822 Color: 11

Bin 4437: 5855 of cap free
Amount of items: 2
Items: 
Size: 615226 Color: 8
Size: 378920 Color: 2

Bin 4438: 5968 of cap free
Amount of items: 2
Items: 
Size: 554942 Color: 11
Size: 439091 Color: 17

Bin 4439: 5994 of cap free
Amount of items: 2
Items: 
Size: 615129 Color: 6
Size: 378878 Color: 12

Bin 4440: 5998 of cap free
Amount of items: 2
Items: 
Size: 650596 Color: 15
Size: 343407 Color: 11

Bin 4441: 5998 of cap free
Amount of items: 2
Items: 
Size: 780578 Color: 1
Size: 213425 Color: 16

Bin 4442: 6129 of cap free
Amount of items: 2
Items: 
Size: 780552 Color: 2
Size: 213320 Color: 14

Bin 4443: 6249 of cap free
Amount of items: 2
Items: 
Size: 529794 Color: 17
Size: 463958 Color: 4

Bin 4444: 6530 of cap free
Amount of items: 2
Items: 
Size: 529598 Color: 4
Size: 463873 Color: 0

Bin 4445: 6568 of cap free
Amount of items: 2
Items: 
Size: 507251 Color: 9
Size: 486182 Color: 19

Bin 4446: 6746 of cap free
Amount of items: 2
Items: 
Size: 780364 Color: 4
Size: 212891 Color: 9

Bin 4447: 7374 of cap free
Amount of items: 2
Items: 
Size: 529482 Color: 2
Size: 463145 Color: 14

Bin 4448: 7732 of cap free
Amount of items: 2
Items: 
Size: 746305 Color: 11
Size: 245964 Color: 6

Bin 4449: 7735 of cap free
Amount of items: 2
Items: 
Size: 599228 Color: 1
Size: 393038 Color: 14

Bin 4450: 7873 of cap free
Amount of items: 2
Items: 
Size: 795572 Color: 11
Size: 196556 Color: 2

Bin 4451: 8041 of cap free
Amount of items: 2
Items: 
Size: 621961 Color: 16
Size: 369999 Color: 19

Bin 4452: 8162 of cap free
Amount of items: 2
Items: 
Size: 780262 Color: 18
Size: 211577 Color: 1

Bin 4453: 8334 of cap free
Amount of items: 2
Items: 
Size: 730344 Color: 17
Size: 261323 Color: 3

Bin 4454: 8680 of cap free
Amount of items: 2
Items: 
Size: 633660 Color: 12
Size: 357661 Color: 18

Bin 4455: 8866 of cap free
Amount of items: 2
Items: 
Size: 598386 Color: 10
Size: 392749 Color: 9

Bin 4456: 9104 of cap free
Amount of items: 2
Items: 
Size: 730065 Color: 6
Size: 260832 Color: 14

Bin 4457: 9265 of cap free
Amount of items: 2
Items: 
Size: 504555 Color: 1
Size: 486181 Color: 12

Bin 4458: 9326 of cap free
Amount of items: 2
Items: 
Size: 504519 Color: 9
Size: 486156 Color: 14

Bin 4459: 9803 of cap free
Amount of items: 2
Items: 
Size: 504400 Color: 14
Size: 485798 Color: 0

Bin 4460: 9837 of cap free
Amount of items: 2
Items: 
Size: 504396 Color: 5
Size: 485768 Color: 15

Bin 4461: 10014 of cap free
Amount of items: 2
Items: 
Size: 621763 Color: 3
Size: 368224 Color: 1

Bin 4462: 10232 of cap free
Amount of items: 2
Items: 
Size: 633250 Color: 5
Size: 356519 Color: 10

Bin 4463: 10577 of cap free
Amount of items: 2
Items: 
Size: 503755 Color: 5
Size: 485669 Color: 16

Bin 4464: 10622 of cap free
Amount of items: 2
Items: 
Size: 709358 Color: 17
Size: 280021 Color: 1

Bin 4465: 10715 of cap free
Amount of items: 2
Items: 
Size: 503718 Color: 11
Size: 485568 Color: 8

Bin 4466: 10896 of cap free
Amount of items: 2
Items: 
Size: 709082 Color: 14
Size: 280023 Color: 17

Bin 4467: 11124 of cap free
Amount of items: 2
Items: 
Size: 709014 Color: 10
Size: 279863 Color: 8

Bin 4468: 12108 of cap free
Amount of items: 2
Items: 
Size: 708955 Color: 13
Size: 278938 Color: 11

Bin 4469: 12219 of cap free
Amount of items: 2
Items: 
Size: 503095 Color: 12
Size: 484687 Color: 10

Bin 4470: 12368 of cap free
Amount of items: 2
Items: 
Size: 524494 Color: 11
Size: 463139 Color: 3

Bin 4471: 12578 of cap free
Amount of items: 2
Items: 
Size: 754405 Color: 5
Size: 233018 Color: 15

Bin 4472: 13362 of cap free
Amount of items: 2
Items: 
Size: 523700 Color: 11
Size: 462939 Color: 5

Bin 4473: 13392 of cap free
Amount of items: 2
Items: 
Size: 523679 Color: 16
Size: 462930 Color: 14

Bin 4474: 13478 of cap free
Amount of items: 2
Items: 
Size: 794180 Color: 10
Size: 192343 Color: 18

Bin 4475: 13531 of cap free
Amount of items: 2
Items: 
Size: 523587 Color: 3
Size: 462883 Color: 12

Bin 4476: 13561 of cap free
Amount of items: 2
Items: 
Size: 547263 Color: 18
Size: 439177 Color: 11

Bin 4477: 14112 of cap free
Amount of items: 2
Items: 
Size: 547243 Color: 14
Size: 438646 Color: 19

Bin 4478: 14169 of cap free
Amount of items: 2
Items: 
Size: 547219 Color: 17
Size: 438613 Color: 1

Bin 4479: 15419 of cap free
Amount of items: 2
Items: 
Size: 706500 Color: 19
Size: 278082 Color: 16

Bin 4480: 17088 of cap free
Amount of items: 2
Items: 
Size: 497394 Color: 3
Size: 485519 Color: 12

Bin 4481: 17849 of cap free
Amount of items: 2
Items: 
Size: 687308 Color: 14
Size: 294844 Color: 7

Bin 4482: 18301 of cap free
Amount of items: 2
Items: 
Size: 543133 Color: 19
Size: 438567 Color: 5

Bin 4483: 25841 of cap free
Amount of items: 2
Items: 
Size: 537247 Color: 13
Size: 436913 Color: 1

Bin 4484: 28860 of cap free
Amount of items: 2
Items: 
Size: 621630 Color: 4
Size: 349511 Color: 19

Bin 4485: 29426 of cap free
Amount of items: 2
Items: 
Size: 739323 Color: 9
Size: 231252 Color: 6

Bin 4486: 31574 of cap free
Amount of items: 2
Items: 
Size: 537235 Color: 9
Size: 431192 Color: 17

Bin 4487: 35038 of cap free
Amount of items: 2
Items: 
Size: 484448 Color: 13
Size: 480515 Color: 11

Bin 4488: 38849 of cap free
Amount of items: 2
Items: 
Size: 621562 Color: 17
Size: 339590 Color: 8

Bin 4489: 39336 of cap free
Amount of items: 2
Items: 
Size: 480498 Color: 11
Size: 480167 Color: 17

Bin 4490: 39574 of cap free
Amount of items: 2
Items: 
Size: 683470 Color: 15
Size: 276957 Color: 11

Bin 4491: 40507 of cap free
Amount of items: 2
Items: 
Size: 479766 Color: 10
Size: 479728 Color: 12

Bin 4492: 40780 of cap free
Amount of items: 2
Items: 
Size: 479612 Color: 13
Size: 479609 Color: 19

Bin 4493: 40990 of cap free
Amount of items: 2
Items: 
Size: 479589 Color: 14
Size: 479422 Color: 12

Bin 4494: 41507 of cap free
Amount of items: 2
Items: 
Size: 479409 Color: 10
Size: 479085 Color: 3

Bin 4495: 42961 of cap free
Amount of items: 2
Items: 
Size: 479081 Color: 0
Size: 477959 Color: 16

Bin 4496: 43237 of cap free
Amount of items: 2
Items: 
Size: 729793 Color: 5
Size: 226971 Color: 7

Bin 4497: 44206 of cap free
Amount of items: 2
Items: 
Size: 477904 Color: 10
Size: 477891 Color: 6

Bin 4498: 45051 of cap free
Amount of items: 2
Items: 
Size: 477477 Color: 0
Size: 477473 Color: 17

Bin 4499: 45239 of cap free
Amount of items: 2
Items: 
Size: 477415 Color: 2
Size: 477347 Color: 0

Bin 4500: 45822 of cap free
Amount of items: 2
Items: 
Size: 615060 Color: 6
Size: 339119 Color: 18

Bin 4501: 46451 of cap free
Amount of items: 2
Items: 
Size: 792375 Color: 3
Size: 161175 Color: 16

Bin 4502: 51042 of cap free
Amount of items: 2
Items: 
Size: 723290 Color: 11
Size: 225669 Color: 6

Bin 4503: 64855 of cap free
Amount of items: 2
Items: 
Size: 523338 Color: 15
Size: 411808 Color: 12

Bin 4504: 69545 of cap free
Amount of items: 2
Items: 
Size: 598277 Color: 5
Size: 332179 Color: 4

Bin 4505: 75750 of cap free
Amount of items: 2
Items: 
Size: 593476 Color: 18
Size: 330775 Color: 3

Bin 4506: 94221 of cap free
Amount of items: 2
Items: 
Size: 680257 Color: 14
Size: 225523 Color: 11

Bin 4507: 207769 of cap free
Amount of items: 1
Items: 
Size: 792232 Color: 17

Bin 4508: 207916 of cap free
Amount of items: 1
Items: 
Size: 792085 Color: 16

Bin 4509: 211534 of cap free
Amount of items: 1
Items: 
Size: 788467 Color: 9

Bin 4510: 220223 of cap free
Amount of items: 1
Items: 
Size: 779778 Color: 15

Bin 4511: 221121 of cap free
Amount of items: 1
Items: 
Size: 778880 Color: 3

Total size: 4507090077
Total free space: 3914434

