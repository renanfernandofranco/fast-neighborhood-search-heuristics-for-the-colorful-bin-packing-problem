Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 10866 Color: 0
Size: 4522 Color: 4
Size: 900 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10948 Color: 2
Size: 5044 Color: 3
Size: 296 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11498 Color: 2
Size: 3994 Color: 4
Size: 796 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12072 Color: 1
Size: 3736 Color: 3
Size: 480 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 1
Size: 3528 Color: 3
Size: 272 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12596 Color: 4
Size: 3084 Color: 2
Size: 608 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12728 Color: 3
Size: 3072 Color: 4
Size: 488 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12991 Color: 3
Size: 1765 Color: 0
Size: 1532 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13022 Color: 3
Size: 1681 Color: 1
Size: 1585 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 0
Size: 2968 Color: 3
Size: 272 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 1
Size: 2676 Color: 3
Size: 528 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13310 Color: 3
Size: 2394 Color: 0
Size: 584 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13252 Color: 1
Size: 1692 Color: 2
Size: 1344 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13304 Color: 0
Size: 2040 Color: 4
Size: 944 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13457 Color: 3
Size: 2451 Color: 2
Size: 380 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 0
Size: 2712 Color: 4
Size: 160 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13418 Color: 4
Size: 2086 Color: 3
Size: 784 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13505 Color: 1
Size: 2233 Color: 3
Size: 550 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13537 Color: 0
Size: 2027 Color: 3
Size: 724 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13609 Color: 3
Size: 1799 Color: 4
Size: 880 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13644 Color: 3
Size: 2068 Color: 1
Size: 576 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13624 Color: 4
Size: 1512 Color: 3
Size: 1152 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13682 Color: 4
Size: 2204 Color: 3
Size: 402 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13712 Color: 0
Size: 2284 Color: 4
Size: 292 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13753 Color: 2
Size: 1911 Color: 3
Size: 624 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13772 Color: 4
Size: 1364 Color: 1
Size: 1152 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13841 Color: 4
Size: 2025 Color: 4
Size: 422 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 4
Size: 1528 Color: 1
Size: 912 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13873 Color: 4
Size: 1611 Color: 1
Size: 804 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 4
Size: 1622 Color: 2
Size: 726 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13970 Color: 4
Size: 2042 Color: 1
Size: 276 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13997 Color: 4
Size: 1859 Color: 2
Size: 432 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 4
Size: 1366 Color: 2
Size: 896 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14011 Color: 0
Size: 1819 Color: 2
Size: 458 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14038 Color: 4
Size: 1934 Color: 1
Size: 316 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14031 Color: 1
Size: 1891 Color: 4
Size: 366 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 3
Size: 1812 Color: 4
Size: 416 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14091 Color: 1
Size: 1849 Color: 2
Size: 348 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 0
Size: 1818 Color: 2
Size: 368 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14116 Color: 2
Size: 1580 Color: 4
Size: 592 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 0
Size: 1344 Color: 3
Size: 780 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 0
Size: 1500 Color: 3
Size: 620 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14206 Color: 4
Size: 1570 Color: 1
Size: 512 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 1
Size: 1356 Color: 2
Size: 672 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14239 Color: 4
Size: 1527 Color: 3
Size: 522 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14266 Color: 1
Size: 1582 Color: 3
Size: 440 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14340 Color: 4
Size: 1104 Color: 0
Size: 844 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14346 Color: 2
Size: 1174 Color: 3
Size: 768 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14387 Color: 0
Size: 1565 Color: 0
Size: 336 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14396 Color: 2
Size: 1356 Color: 4
Size: 536 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14433 Color: 4
Size: 1547 Color: 3
Size: 308 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14462 Color: 4
Size: 1186 Color: 3
Size: 640 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 2
Size: 1374 Color: 0
Size: 462 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14457 Color: 3
Size: 1461 Color: 1
Size: 370 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 1
Size: 1008 Color: 4
Size: 792 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14492 Color: 3
Size: 1476 Color: 2
Size: 320 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 2
Size: 1372 Color: 4
Size: 392 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 4
Size: 1352 Color: 3
Size: 320 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14642 Color: 4
Size: 1022 Color: 3
Size: 624 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14650 Color: 4
Size: 1016 Color: 2
Size: 622 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14618 Color: 2
Size: 1194 Color: 4
Size: 476 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 3
Size: 1356 Color: 4
Size: 280 Color: 1

Bin 63: 1 of cap free
Amount of items: 7
Items: 
Size: 8148 Color: 0
Size: 1848 Color: 2
Size: 1714 Color: 4
Size: 1686 Color: 3
Size: 1441 Color: 4
Size: 1092 Color: 0
Size: 358 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 9115 Color: 1
Size: 6756 Color: 4
Size: 416 Color: 0

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 9235 Color: 4
Size: 6606 Color: 3
Size: 446 Color: 0

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 9731 Color: 4
Size: 6184 Color: 0
Size: 372 Color: 1

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 10188 Color: 0
Size: 5811 Color: 4
Size: 288 Color: 3

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 10225 Color: 2
Size: 5782 Color: 0
Size: 280 Color: 1

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 11562 Color: 3
Size: 4413 Color: 0
Size: 312 Color: 1

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 3
Size: 3969 Color: 4
Size: 466 Color: 2

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12269 Color: 3
Size: 3602 Color: 1
Size: 416 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12549 Color: 0
Size: 2746 Color: 3
Size: 992 Color: 1

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13159 Color: 4
Size: 2408 Color: 4
Size: 720 Color: 3

Bin 74: 1 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 4
Size: 3111 Color: 1

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 13349 Color: 0
Size: 2938 Color: 4

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13355 Color: 1
Size: 1522 Color: 2
Size: 1410 Color: 3

Bin 77: 1 of cap free
Amount of items: 2
Items: 
Size: 13376 Color: 4
Size: 2911 Color: 1

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 13482 Color: 4
Size: 2333 Color: 3
Size: 472 Color: 4

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 13929 Color: 3
Size: 2062 Color: 3
Size: 296 Color: 4

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 14131 Color: 4
Size: 1964 Color: 3
Size: 192 Color: 1

Bin 81: 2 of cap free
Amount of items: 9
Items: 
Size: 8146 Color: 0
Size: 1400 Color: 3
Size: 1394 Color: 2
Size: 1328 Color: 3
Size: 1248 Color: 0
Size: 1010 Color: 3
Size: 1000 Color: 4
Size: 488 Color: 1
Size: 272 Color: 1

Bin 82: 2 of cap free
Amount of items: 5
Items: 
Size: 8145 Color: 3
Size: 2361 Color: 1
Size: 2321 Color: 4
Size: 2147 Color: 4
Size: 1312 Color: 0

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 9171 Color: 1
Size: 5979 Color: 0
Size: 1136 Color: 2

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 9318 Color: 4
Size: 6610 Color: 1
Size: 358 Color: 0

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 4
Size: 5084 Color: 3
Size: 272 Color: 4

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 11939 Color: 4
Size: 3345 Color: 3
Size: 1002 Color: 0

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 12766 Color: 3
Size: 3142 Color: 1
Size: 378 Color: 2

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 13396 Color: 1
Size: 2528 Color: 3
Size: 362 Color: 4

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 13944 Color: 1
Size: 2342 Color: 0

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 14059 Color: 3
Size: 2227 Color: 0

Bin 91: 2 of cap free
Amount of items: 2
Items: 
Size: 14598 Color: 2
Size: 1688 Color: 1

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 9331 Color: 1
Size: 6642 Color: 4
Size: 312 Color: 0

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 9354 Color: 4
Size: 5795 Color: 0
Size: 1136 Color: 3

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 10145 Color: 2
Size: 5788 Color: 0
Size: 352 Color: 2

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 10353 Color: 1
Size: 5528 Color: 3
Size: 404 Color: 0

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 3
Size: 5053 Color: 4
Size: 360 Color: 2

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 11453 Color: 4
Size: 4520 Color: 1
Size: 312 Color: 2

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 12275 Color: 3
Size: 3506 Color: 0
Size: 504 Color: 1

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 12292 Color: 4
Size: 3625 Color: 3
Size: 368 Color: 0

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 12522 Color: 1
Size: 3351 Color: 3
Size: 412 Color: 1

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 12983 Color: 4
Size: 2722 Color: 3
Size: 580 Color: 1

Bin 102: 4 of cap free
Amount of items: 19
Items: 
Size: 1170 Color: 0
Size: 1136 Color: 0
Size: 998 Color: 3
Size: 988 Color: 3
Size: 892 Color: 2
Size: 888 Color: 2
Size: 888 Color: 0
Size: 878 Color: 0
Size: 864 Color: 3
Size: 848 Color: 0
Size: 812 Color: 4
Size: 800 Color: 4
Size: 800 Color: 1
Size: 798 Color: 2
Size: 770 Color: 1
Size: 736 Color: 1
Size: 736 Color: 0
Size: 700 Color: 0
Size: 582 Color: 3

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 9113 Color: 3
Size: 5851 Color: 0
Size: 1320 Color: 4

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 9723 Color: 0
Size: 6177 Color: 1
Size: 384 Color: 4

Bin 105: 4 of cap free
Amount of items: 2
Items: 
Size: 11096 Color: 4
Size: 5188 Color: 1

Bin 106: 4 of cap free
Amount of items: 3
Items: 
Size: 11416 Color: 4
Size: 4462 Color: 3
Size: 406 Color: 0

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 12018 Color: 0
Size: 3194 Color: 1
Size: 1072 Color: 3

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 1
Size: 3528 Color: 4
Size: 364 Color: 3

Bin 109: 4 of cap free
Amount of items: 2
Items: 
Size: 14110 Color: 3
Size: 2174 Color: 1

Bin 110: 4 of cap free
Amount of items: 2
Items: 
Size: 14171 Color: 3
Size: 2113 Color: 1

Bin 111: 4 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 0
Size: 1878 Color: 2

Bin 112: 5 of cap free
Amount of items: 6
Items: 
Size: 8154 Color: 0
Size: 2084 Color: 1
Size: 2013 Color: 4
Size: 1960 Color: 4
Size: 1768 Color: 3
Size: 304 Color: 4

Bin 113: 5 of cap free
Amount of items: 3
Items: 
Size: 10988 Color: 4
Size: 5021 Color: 3
Size: 274 Color: 0

Bin 114: 5 of cap free
Amount of items: 3
Items: 
Size: 12797 Color: 4
Size: 2542 Color: 3
Size: 944 Color: 4

Bin 115: 5 of cap free
Amount of items: 2
Items: 
Size: 13838 Color: 1
Size: 2445 Color: 3

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 9276 Color: 3
Size: 5844 Color: 2
Size: 1162 Color: 0

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 11031 Color: 0
Size: 4947 Color: 1
Size: 304 Color: 3

Bin 118: 6 of cap free
Amount of items: 3
Items: 
Size: 11832 Color: 0
Size: 2232 Color: 3
Size: 2218 Color: 4

Bin 119: 7 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 2
Size: 6785 Color: 3
Size: 322 Color: 1

Bin 120: 7 of cap free
Amount of items: 3
Items: 
Size: 11527 Color: 3
Size: 4466 Color: 2
Size: 288 Color: 1

Bin 121: 7 of cap free
Amount of items: 3
Items: 
Size: 12458 Color: 4
Size: 3631 Color: 2
Size: 192 Color: 0

Bin 122: 7 of cap free
Amount of items: 2
Items: 
Size: 14490 Color: 1
Size: 1791 Color: 2

Bin 123: 8 of cap free
Amount of items: 3
Items: 
Size: 8776 Color: 2
Size: 3942 Color: 0
Size: 3562 Color: 2

Bin 124: 8 of cap free
Amount of items: 2
Items: 
Size: 14072 Color: 3
Size: 2208 Color: 1

Bin 125: 9 of cap free
Amount of items: 3
Items: 
Size: 10270 Color: 3
Size: 4993 Color: 0
Size: 1016 Color: 4

Bin 126: 9 of cap free
Amount of items: 2
Items: 
Size: 14139 Color: 0
Size: 2140 Color: 2

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 13529 Color: 2
Size: 2749 Color: 1

Bin 128: 10 of cap free
Amount of items: 2
Items: 
Size: 14456 Color: 1
Size: 1822 Color: 2

Bin 129: 11 of cap free
Amount of items: 3
Items: 
Size: 9211 Color: 4
Size: 6786 Color: 1
Size: 280 Color: 0

Bin 130: 11 of cap free
Amount of items: 2
Items: 
Size: 14355 Color: 3
Size: 1922 Color: 1

Bin 131: 12 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 1
Size: 2894 Color: 2
Size: 80 Color: 3

Bin 132: 12 of cap free
Amount of items: 2
Items: 
Size: 13788 Color: 1
Size: 2488 Color: 0

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 3
Size: 1886 Color: 2

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 10236 Color: 3
Size: 6039 Color: 4

Bin 135: 13 of cap free
Amount of items: 2
Items: 
Size: 13982 Color: 1
Size: 2293 Color: 2

Bin 136: 13 of cap free
Amount of items: 2
Items: 
Size: 14028 Color: 2
Size: 2247 Color: 0

Bin 137: 13 of cap free
Amount of items: 2
Items: 
Size: 14234 Color: 1
Size: 2041 Color: 2

Bin 138: 14 of cap free
Amount of items: 3
Items: 
Size: 9462 Color: 1
Size: 6264 Color: 2
Size: 548 Color: 3

Bin 139: 14 of cap free
Amount of items: 2
Items: 
Size: 14502 Color: 0
Size: 1772 Color: 1

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 14535 Color: 0
Size: 1738 Color: 1

Bin 141: 16 of cap free
Amount of items: 3
Items: 
Size: 8156 Color: 0
Size: 6764 Color: 1
Size: 1352 Color: 4

Bin 142: 16 of cap free
Amount of items: 3
Items: 
Size: 11144 Color: 1
Size: 4420 Color: 3
Size: 708 Color: 1

Bin 143: 16 of cap free
Amount of items: 2
Items: 
Size: 14644 Color: 0
Size: 1628 Color: 3

Bin 144: 17 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 0
Size: 5879 Color: 2
Size: 736 Color: 3

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 13786 Color: 4
Size: 2485 Color: 0

Bin 146: 17 of cap free
Amount of items: 2
Items: 
Size: 14271 Color: 3
Size: 2000 Color: 0

Bin 147: 17 of cap free
Amount of items: 2
Items: 
Size: 14411 Color: 2
Size: 1860 Color: 1

Bin 148: 18 of cap free
Amount of items: 33
Items: 
Size: 696 Color: 1
Size: 688 Color: 4
Size: 668 Color: 2
Size: 668 Color: 0
Size: 664 Color: 1
Size: 656 Color: 4
Size: 636 Color: 2
Size: 568 Color: 0
Size: 544 Color: 0
Size: 528 Color: 3
Size: 520 Color: 0
Size: 508 Color: 2
Size: 496 Color: 3
Size: 496 Color: 1
Size: 472 Color: 4
Size: 470 Color: 1
Size: 464 Color: 4
Size: 464 Color: 3
Size: 464 Color: 2
Size: 456 Color: 1
Size: 444 Color: 4
Size: 440 Color: 2
Size: 432 Color: 0
Size: 428 Color: 1
Size: 408 Color: 3
Size: 400 Color: 3
Size: 384 Color: 4
Size: 384 Color: 2
Size: 384 Color: 0
Size: 376 Color: 2
Size: 360 Color: 3
Size: 352 Color: 3
Size: 352 Color: 2

Bin 149: 18 of cap free
Amount of items: 3
Items: 
Size: 12090 Color: 2
Size: 4028 Color: 1
Size: 152 Color: 2

Bin 150: 19 of cap free
Amount of items: 2
Items: 
Size: 13857 Color: 0
Size: 2412 Color: 2

Bin 151: 20 of cap free
Amount of items: 2
Items: 
Size: 12548 Color: 1
Size: 3720 Color: 0

Bin 152: 20 of cap free
Amount of items: 2
Items: 
Size: 12632 Color: 2
Size: 3636 Color: 0

Bin 153: 20 of cap free
Amount of items: 2
Items: 
Size: 13151 Color: 0
Size: 3117 Color: 1

Bin 154: 21 of cap free
Amount of items: 2
Items: 
Size: 14107 Color: 0
Size: 2160 Color: 1

Bin 155: 22 of cap free
Amount of items: 2
Items: 
Size: 12818 Color: 4
Size: 3448 Color: 0

Bin 156: 23 of cap free
Amount of items: 2
Items: 
Size: 9348 Color: 2
Size: 6917 Color: 3

Bin 157: 23 of cap free
Amount of items: 2
Items: 
Size: 14559 Color: 2
Size: 1706 Color: 3

Bin 158: 24 of cap free
Amount of items: 4
Items: 
Size: 8362 Color: 1
Size: 7322 Color: 2
Size: 340 Color: 0
Size: 240 Color: 3

Bin 159: 24 of cap free
Amount of items: 2
Items: 
Size: 13008 Color: 2
Size: 3256 Color: 1

Bin 160: 25 of cap free
Amount of items: 2
Items: 
Size: 11816 Color: 2
Size: 4447 Color: 0

Bin 161: 26 of cap free
Amount of items: 2
Items: 
Size: 11966 Color: 4
Size: 4296 Color: 2

Bin 162: 26 of cap free
Amount of items: 2
Items: 
Size: 13814 Color: 2
Size: 2448 Color: 1

Bin 163: 28 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 2
Size: 6780 Color: 3
Size: 1232 Color: 2

Bin 164: 28 of cap free
Amount of items: 2
Items: 
Size: 9432 Color: 2
Size: 6828 Color: 3

Bin 165: 29 of cap free
Amount of items: 2
Items: 
Size: 9123 Color: 2
Size: 7136 Color: 3

Bin 166: 29 of cap free
Amount of items: 2
Items: 
Size: 11931 Color: 1
Size: 4328 Color: 4

Bin 167: 30 of cap free
Amount of items: 2
Items: 
Size: 10178 Color: 2
Size: 6080 Color: 3

Bin 168: 31 of cap free
Amount of items: 3
Items: 
Size: 10297 Color: 0
Size: 5720 Color: 4
Size: 240 Color: 3

Bin 169: 31 of cap free
Amount of items: 2
Items: 
Size: 12557 Color: 0
Size: 3700 Color: 2

Bin 170: 32 of cap free
Amount of items: 2
Items: 
Size: 8872 Color: 3
Size: 7384 Color: 2

Bin 171: 32 of cap free
Amount of items: 3
Items: 
Size: 10938 Color: 0
Size: 5094 Color: 4
Size: 224 Color: 1

Bin 172: 33 of cap free
Amount of items: 3
Items: 
Size: 14264 Color: 0
Size: 1967 Color: 2
Size: 24 Color: 3

Bin 173: 36 of cap free
Amount of items: 2
Items: 
Size: 11460 Color: 4
Size: 4792 Color: 1

Bin 174: 36 of cap free
Amount of items: 2
Items: 
Size: 12180 Color: 0
Size: 4072 Color: 1

Bin 175: 39 of cap free
Amount of items: 2
Items: 
Size: 12789 Color: 1
Size: 3460 Color: 4

Bin 176: 43 of cap free
Amount of items: 2
Items: 
Size: 13630 Color: 0
Size: 2615 Color: 4

Bin 177: 43 of cap free
Amount of items: 2
Items: 
Size: 13713 Color: 1
Size: 2532 Color: 2

Bin 178: 44 of cap free
Amount of items: 2
Items: 
Size: 13489 Color: 0
Size: 2755 Color: 2

Bin 179: 45 of cap free
Amount of items: 2
Items: 
Size: 13761 Color: 0
Size: 2482 Color: 4

Bin 180: 46 of cap free
Amount of items: 2
Items: 
Size: 10552 Color: 4
Size: 5690 Color: 3

Bin 181: 46 of cap free
Amount of items: 2
Items: 
Size: 13066 Color: 1
Size: 3176 Color: 0

Bin 182: 47 of cap free
Amount of items: 2
Items: 
Size: 10776 Color: 2
Size: 5465 Color: 1

Bin 183: 51 of cap free
Amount of items: 4
Items: 
Size: 8152 Color: 1
Size: 5018 Color: 4
Size: 1899 Color: 0
Size: 1168 Color: 4

Bin 184: 54 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 4
Size: 2686 Color: 1

Bin 185: 62 of cap free
Amount of items: 2
Items: 
Size: 13617 Color: 1
Size: 2609 Color: 4

Bin 186: 68 of cap free
Amount of items: 2
Items: 
Size: 8172 Color: 1
Size: 8048 Color: 3

Bin 187: 78 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 1
Size: 2876 Color: 4
Size: 96 Color: 3

Bin 188: 83 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 3
Size: 3995 Color: 0
Size: 128 Color: 4

Bin 189: 89 of cap free
Amount of items: 2
Items: 
Size: 12168 Color: 0
Size: 4031 Color: 2

Bin 190: 92 of cap free
Amount of items: 2
Items: 
Size: 10265 Color: 3
Size: 5931 Color: 2

Bin 191: 112 of cap free
Amount of items: 2
Items: 
Size: 12844 Color: 2
Size: 3332 Color: 1

Bin 192: 132 of cap free
Amount of items: 3
Items: 
Size: 8188 Color: 1
Size: 5368 Color: 0
Size: 2600 Color: 4

Bin 193: 153 of cap free
Amount of items: 2
Items: 
Size: 9848 Color: 1
Size: 6287 Color: 4

Bin 194: 186 of cap free
Amount of items: 2
Items: 
Size: 9315 Color: 2
Size: 6787 Color: 3

Bin 195: 193 of cap free
Amount of items: 2
Items: 
Size: 11495 Color: 1
Size: 4600 Color: 0

Bin 196: 214 of cap free
Amount of items: 2
Items: 
Size: 10953 Color: 1
Size: 5121 Color: 2

Bin 197: 220 of cap free
Amount of items: 4
Items: 
Size: 8147 Color: 1
Size: 3502 Color: 3
Size: 2917 Color: 1
Size: 1502 Color: 0

Bin 198: 229 of cap free
Amount of items: 2
Items: 
Size: 9267 Color: 4
Size: 6792 Color: 2

Bin 199: 12716 of cap free
Amount of items: 11
Items: 
Size: 376 Color: 0
Size: 360 Color: 1
Size: 344 Color: 4
Size: 336 Color: 3
Size: 336 Color: 3
Size: 336 Color: 2
Size: 316 Color: 4
Size: 304 Color: 4
Size: 304 Color: 2
Size: 288 Color: 1
Size: 272 Color: 3

Total size: 3225024
Total free space: 16288

