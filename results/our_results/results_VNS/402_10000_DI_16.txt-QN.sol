Capicity Bin: 8352
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 5694 Color: 304
Size: 1164 Color: 191
Size: 1162 Color: 190
Size: 188 Color: 46
Size: 144 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6358 Color: 324
Size: 1650 Color: 216
Size: 344 Color: 95

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 333
Size: 1612 Color: 214
Size: 136 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 334
Size: 1252 Color: 196
Size: 474 Color: 115

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 338
Size: 833 Color: 158
Size: 814 Color: 155

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 339
Size: 1442 Color: 206
Size: 202 Color: 51

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 343
Size: 1030 Color: 175
Size: 520 Color: 119

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 344
Size: 1422 Color: 204
Size: 106 Color: 7

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 349
Size: 718 Color: 141
Size: 688 Color: 132

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6964 Color: 350
Size: 1158 Color: 189
Size: 230 Color: 65

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 351
Size: 1174 Color: 192
Size: 212 Color: 56

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 355
Size: 860 Color: 161
Size: 472 Color: 114

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 360
Size: 1002 Color: 172
Size: 272 Color: 77

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7110 Color: 362
Size: 1084 Color: 182
Size: 158 Color: 27

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 366
Size: 778 Color: 151
Size: 424 Color: 106

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 367
Size: 916 Color: 166
Size: 280 Color: 78

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7213 Color: 369
Size: 1053 Color: 177
Size: 86 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 375
Size: 876 Color: 163
Size: 200 Color: 50

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7305 Color: 377
Size: 709 Color: 139
Size: 338 Color: 93

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7334 Color: 380
Size: 590 Color: 126
Size: 428 Color: 107

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7353 Color: 382
Size: 771 Color: 149
Size: 228 Color: 63

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7363 Color: 383
Size: 825 Color: 156
Size: 164 Color: 33

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7364 Color: 384
Size: 804 Color: 154
Size: 184 Color: 43

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7412 Color: 388
Size: 692 Color: 135
Size: 248 Color: 72

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7422 Color: 389
Size: 756 Color: 147
Size: 174 Color: 40

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7452 Color: 392
Size: 788 Color: 152
Size: 112 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7457 Color: 393
Size: 747 Color: 146
Size: 148 Color: 20

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7466 Color: 395
Size: 746 Color: 145
Size: 140 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7484 Color: 397
Size: 724 Color: 142
Size: 144 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7494 Color: 398
Size: 664 Color: 130
Size: 194 Color: 48

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7510 Color: 401
Size: 694 Color: 137
Size: 148 Color: 22

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7511 Color: 402
Size: 701 Color: 138
Size: 140 Color: 13

Bin 33: 1 of cap free
Amount of items: 7
Items: 
Size: 4178 Color: 274
Size: 1153 Color: 188
Size: 1056 Color: 178
Size: 828 Color: 157
Size: 688 Color: 131
Size: 224 Color: 62
Size: 224 Color: 61

Bin 34: 1 of cap free
Amount of items: 5
Items: 
Size: 4202 Color: 281
Size: 3477 Color: 265
Size: 284 Color: 80
Size: 200 Color: 49
Size: 188 Color: 45

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 6315 Color: 320
Size: 2036 Color: 229

Bin 36: 1 of cap free
Amount of items: 2
Items: 
Size: 7215 Color: 370
Size: 1136 Color: 186

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 7478 Color: 396
Size: 873 Color: 162

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 7503 Color: 400
Size: 846 Color: 159
Size: 2 Color: 0

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 5391 Color: 300
Size: 2959 Color: 252

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 5730 Color: 306
Size: 2620 Color: 243

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 6060 Color: 313
Size: 2218 Color: 237
Size: 72 Color: 3

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 6164 Color: 316
Size: 2186 Color: 235

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 6267 Color: 319
Size: 2083 Color: 231

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 6594 Color: 332
Size: 1756 Color: 221

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 7062 Color: 358
Size: 1288 Color: 198

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 7338 Color: 381
Size: 1012 Color: 173

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 7396 Color: 387
Size: 954 Color: 170

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 7500 Color: 399
Size: 850 Color: 160

Bin 49: 3 of cap free
Amount of items: 5
Items: 
Size: 4284 Color: 282
Size: 3481 Color: 266
Size: 228 Color: 64
Size: 180 Color: 42
Size: 176 Color: 41

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 314
Size: 2203 Color: 236
Size: 64 Color: 2

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 323
Size: 1815 Color: 222
Size: 192 Color: 47

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 328
Size: 1678 Color: 218
Size: 184 Color: 44

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 6650 Color: 336
Size: 1699 Color: 220

Bin 54: 3 of cap free
Amount of items: 2
Items: 
Size: 6883 Color: 347
Size: 1466 Color: 209

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 7075 Color: 359
Size: 694 Color: 136
Size: 580 Color: 125

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 7238 Color: 372
Size: 1111 Color: 184

Bin 57: 4 of cap free
Amount of items: 5
Items: 
Size: 4866 Color: 291
Size: 2894 Color: 249
Size: 268 Color: 76
Size: 160 Color: 31
Size: 160 Color: 30

Bin 58: 4 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 331
Size: 1453 Color: 207
Size: 320 Color: 87

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 7270 Color: 374
Size: 1078 Color: 181

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 7286 Color: 376
Size: 1062 Color: 180

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 7427 Color: 390
Size: 921 Color: 167

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 7458 Color: 394
Size: 890 Color: 164

Bin 63: 5 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 286
Size: 3431 Color: 260
Size: 168 Color: 36

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 330
Size: 1057 Color: 179
Size: 716 Color: 140

Bin 65: 6 of cap free
Amount of items: 17
Items: 
Size: 692 Color: 134
Size: 692 Color: 133
Size: 640 Color: 129
Size: 600 Color: 128
Size: 596 Color: 127
Size: 576 Color: 124
Size: 576 Color: 123
Size: 538 Color: 122
Size: 536 Color: 121
Size: 528 Color: 120
Size: 520 Color: 118
Size: 504 Color: 117
Size: 400 Color: 104
Size: 244 Color: 70
Size: 240 Color: 69
Size: 232 Color: 68
Size: 232 Color: 67

Bin 66: 6 of cap free
Amount of items: 5
Items: 
Size: 4194 Color: 280
Size: 3476 Color: 264
Size: 264 Color: 75
Size: 208 Color: 53
Size: 204 Color: 52

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 6982 Color: 353
Size: 1364 Color: 202

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 7052 Color: 357
Size: 1294 Color: 199

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 7204 Color: 368
Size: 1142 Color: 187

Bin 70: 7 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 365
Size: 1205 Color: 194

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 7324 Color: 379
Size: 1021 Color: 174

Bin 72: 8 of cap free
Amount of items: 3
Items: 
Size: 6330 Color: 321
Size: 1894 Color: 225
Size: 120 Color: 9

Bin 73: 8 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 322
Size: 1916 Color: 226
Size: 88 Color: 5

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 6516 Color: 329
Size: 1828 Color: 223

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 6724 Color: 340
Size: 1620 Color: 215

Bin 76: 8 of cap free
Amount of items: 2
Items: 
Size: 6789 Color: 342
Size: 1555 Color: 212

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 7308 Color: 378
Size: 1036 Color: 176

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 7395 Color: 386
Size: 949 Color: 169

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 7442 Color: 391
Size: 902 Color: 165

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 7085 Color: 361
Size: 1258 Color: 197

Bin 81: 9 of cap free
Amount of items: 2
Items: 
Size: 7118 Color: 363
Size: 1225 Color: 195

Bin 82: 10 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 327
Size: 1882 Color: 224

Bin 83: 10 of cap free
Amount of items: 2
Items: 
Size: 6680 Color: 337
Size: 1662 Color: 217

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 348
Size: 1426 Color: 205

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 7378 Color: 385
Size: 964 Color: 171

Bin 86: 11 of cap free
Amount of items: 2
Items: 
Size: 6969 Color: 352
Size: 1372 Color: 203

Bin 87: 11 of cap free
Amount of items: 2
Items: 
Size: 7247 Color: 373
Size: 1094 Color: 183

Bin 88: 12 of cap free
Amount of items: 5
Items: 
Size: 4188 Color: 279
Size: 3474 Color: 263
Size: 260 Color: 74
Size: 210 Color: 55
Size: 208 Color: 54

Bin 89: 12 of cap free
Amount of items: 3
Items: 
Size: 5505 Color: 303
Size: 2691 Color: 246
Size: 144 Color: 15

Bin 90: 13 of cap free
Amount of items: 2
Items: 
Size: 6175 Color: 317
Size: 2164 Color: 234

Bin 91: 13 of cap free
Amount of items: 2
Items: 
Size: 6998 Color: 354
Size: 1341 Color: 201

Bin 92: 14 of cap free
Amount of items: 4
Items: 
Size: 4340 Color: 283
Size: 3656 Color: 270
Size: 174 Color: 39
Size: 168 Color: 38

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 7222 Color: 371
Size: 1116 Color: 185

Bin 94: 16 of cap free
Amount of items: 5
Items: 
Size: 4186 Color: 278
Size: 3466 Color: 262
Size: 256 Color: 73
Size: 216 Color: 58
Size: 212 Color: 57

Bin 95: 16 of cap free
Amount of items: 2
Items: 
Size: 6412 Color: 326
Size: 1924 Color: 227

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 7033 Color: 356
Size: 1303 Color: 200

Bin 97: 18 of cap free
Amount of items: 2
Items: 
Size: 6852 Color: 346
Size: 1482 Color: 211

Bin 98: 19 of cap free
Amount of items: 2
Items: 
Size: 7129 Color: 364
Size: 1204 Color: 193

Bin 99: 22 of cap free
Amount of items: 3
Items: 
Size: 5330 Color: 298
Size: 2852 Color: 248
Size: 148 Color: 19

Bin 100: 24 of cap free
Amount of items: 2
Items: 
Size: 6642 Color: 335
Size: 1686 Color: 219

Bin 101: 25 of cap free
Amount of items: 5
Items: 
Size: 4185 Color: 277
Size: 3462 Color: 261
Size: 248 Color: 71
Size: 216 Color: 60
Size: 216 Color: 59

Bin 102: 25 of cap free
Amount of items: 2
Items: 
Size: 6846 Color: 345
Size: 1481 Color: 210

Bin 103: 27 of cap free
Amount of items: 2
Items: 
Size: 4181 Color: 276
Size: 4144 Color: 272

Bin 104: 29 of cap free
Amount of items: 2
Items: 
Size: 6743 Color: 341
Size: 1580 Color: 213

Bin 105: 30 of cap free
Amount of items: 2
Items: 
Size: 5709 Color: 305
Size: 2613 Color: 242

Bin 106: 31 of cap free
Amount of items: 3
Items: 
Size: 4803 Color: 290
Size: 3358 Color: 259
Size: 160 Color: 32

Bin 107: 32 of cap free
Amount of items: 2
Items: 
Size: 5916 Color: 311
Size: 2404 Color: 239

Bin 108: 35 of cap free
Amount of items: 2
Items: 
Size: 6374 Color: 325
Size: 1943 Color: 228

Bin 109: 37 of cap free
Amount of items: 3
Items: 
Size: 4882 Color: 293
Size: 3279 Color: 256
Size: 154 Color: 26

Bin 110: 38 of cap free
Amount of items: 3
Items: 
Size: 5318 Color: 297
Size: 2848 Color: 247
Size: 148 Color: 21

Bin 111: 39 of cap free
Amount of items: 2
Items: 
Size: 6252 Color: 318
Size: 2061 Color: 230

Bin 112: 40 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 295
Size: 3004 Color: 254
Size: 152 Color: 24

Bin 113: 40 of cap free
Amount of items: 3
Items: 
Size: 6094 Color: 315
Size: 2162 Color: 233
Size: 56 Color: 1

Bin 114: 42 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 289
Size: 3348 Color: 258
Size: 166 Color: 34

Bin 115: 42 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 302
Size: 2690 Color: 245
Size: 144 Color: 16

Bin 116: 50 of cap free
Amount of items: 2
Items: 
Size: 4762 Color: 287
Size: 3540 Color: 269

Bin 117: 53 of cap free
Amount of items: 3
Items: 
Size: 5786 Color: 309
Size: 2373 Color: 238
Size: 140 Color: 12

Bin 118: 58 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 288
Size: 3348 Color: 257
Size: 168 Color: 35

Bin 119: 60 of cap free
Amount of items: 4
Items: 
Size: 4868 Color: 292
Size: 3104 Color: 255
Size: 160 Color: 29
Size: 160 Color: 28

Bin 120: 60 of cap free
Amount of items: 2
Items: 
Size: 5762 Color: 308
Size: 2530 Color: 241

Bin 121: 65 of cap free
Amount of items: 3
Items: 
Size: 4637 Color: 284
Size: 3482 Color: 267
Size: 168 Color: 37

Bin 122: 71 of cap free
Amount of items: 3
Items: 
Size: 5879 Color: 310
Size: 1460 Color: 208
Size: 942 Color: 168

Bin 123: 73 of cap free
Amount of items: 2
Items: 
Size: 4747 Color: 285
Size: 3532 Color: 268

Bin 124: 82 of cap free
Amount of items: 3
Items: 
Size: 5212 Color: 296
Size: 2906 Color: 250
Size: 152 Color: 23

Bin 125: 82 of cap free
Amount of items: 2
Items: 
Size: 5362 Color: 299
Size: 2908 Color: 251

Bin 126: 83 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 294
Size: 2994 Color: 253
Size: 152 Color: 25

Bin 127: 85 of cap free
Amount of items: 3
Items: 
Size: 6021 Color: 312
Size: 2142 Color: 232
Size: 104 Color: 6

Bin 128: 88 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 301
Size: 2660 Color: 244
Size: 144 Color: 18

Bin 129: 102 of cap free
Amount of items: 2
Items: 
Size: 5756 Color: 307
Size: 2494 Color: 240

Bin 130: 111 of cap free
Amount of items: 2
Items: 
Size: 4180 Color: 275
Size: 4061 Color: 271

Bin 131: 132 of cap free
Amount of items: 7
Items: 
Size: 4177 Color: 273
Size: 799 Color: 153
Size: 778 Color: 150
Size: 762 Color: 148
Size: 742 Color: 144
Size: 730 Color: 143
Size: 232 Color: 66

Bin 132: 254 of cap free
Amount of items: 22
Items: 
Size: 496 Color: 116
Size: 440 Color: 113
Size: 440 Color: 112
Size: 440 Color: 111
Size: 436 Color: 110
Size: 436 Color: 109
Size: 432 Color: 108
Size: 412 Color: 105
Size: 400 Color: 103
Size: 388 Color: 102
Size: 376 Color: 101
Size: 332 Color: 91
Size: 332 Color: 90
Size: 332 Color: 89
Size: 328 Color: 88
Size: 312 Color: 86
Size: 310 Color: 85
Size: 296 Color: 84
Size: 296 Color: 83
Size: 292 Color: 82
Size: 288 Color: 81
Size: 284 Color: 79

Bin 133: 5850 of cap free
Amount of items: 7
Items: 
Size: 376 Color: 100
Size: 376 Color: 99
Size: 362 Color: 98
Size: 360 Color: 97
Size: 348 Color: 96
Size: 344 Color: 94
Size: 336 Color: 92

Total size: 1102464
Total free space: 8352

