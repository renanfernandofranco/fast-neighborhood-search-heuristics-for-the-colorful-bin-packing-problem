Capicity Bin: 16160
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8084 Color: 4
Size: 2724 Color: 15
Size: 1840 Color: 2
Size: 1792 Color: 8
Size: 1720 Color: 6

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 8088 Color: 19
Size: 4102 Color: 2
Size: 2826 Color: 15
Size: 632 Color: 0
Size: 512 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9148 Color: 2
Size: 6684 Color: 12
Size: 328 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9648 Color: 0
Size: 5748 Color: 11
Size: 764 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9560 Color: 4
Size: 5780 Color: 10
Size: 820 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 2
Size: 4948 Color: 1
Size: 1000 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10250 Color: 15
Size: 5414 Color: 17
Size: 496 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10260 Color: 16
Size: 5512 Color: 3
Size: 388 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11016 Color: 3
Size: 4856 Color: 9
Size: 288 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 18
Size: 4348 Color: 5
Size: 732 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11238 Color: 11
Size: 3832 Color: 15
Size: 1090 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11703 Color: 15
Size: 3715 Color: 5
Size: 742 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11568 Color: 14
Size: 4112 Color: 19
Size: 480 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11576 Color: 18
Size: 4276 Color: 3
Size: 308 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 1
Size: 4248 Color: 16
Size: 272 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11676 Color: 0
Size: 3804 Color: 16
Size: 680 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11704 Color: 1
Size: 3712 Color: 6
Size: 744 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11774 Color: 15
Size: 3830 Color: 4
Size: 556 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11824 Color: 7
Size: 3856 Color: 8
Size: 480 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 14
Size: 3720 Color: 1
Size: 412 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12062 Color: 12
Size: 3768 Color: 17
Size: 330 Color: 9

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 6
Size: 3352 Color: 3
Size: 704 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 7
Size: 3632 Color: 13
Size: 376 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12233 Color: 19
Size: 3355 Color: 2
Size: 572 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12557 Color: 15
Size: 2069 Color: 3
Size: 1534 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12272 Color: 14
Size: 3288 Color: 6
Size: 600 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 14
Size: 3212 Color: 1
Size: 640 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12432 Color: 6
Size: 3384 Color: 14
Size: 344 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12545 Color: 3
Size: 3137 Color: 3
Size: 478 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 12
Size: 3120 Color: 13
Size: 480 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12604 Color: 16
Size: 3332 Color: 12
Size: 224 Color: 5

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12778 Color: 15
Size: 2554 Color: 19
Size: 828 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 14
Size: 2768 Color: 14
Size: 428 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12984 Color: 5
Size: 2936 Color: 12
Size: 240 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13104 Color: 15
Size: 2776 Color: 1
Size: 280 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13018 Color: 12
Size: 1660 Color: 5
Size: 1482 Color: 7

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 6
Size: 2256 Color: 10
Size: 856 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13098 Color: 6
Size: 2778 Color: 9
Size: 284 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13165 Color: 15
Size: 2483 Color: 19
Size: 512 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13169 Color: 8
Size: 2493 Color: 4
Size: 498 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 0
Size: 1756 Color: 12
Size: 1136 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 10
Size: 1456 Color: 19
Size: 1432 Color: 11

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 11
Size: 2576 Color: 8
Size: 296 Color: 16

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13290 Color: 5
Size: 2394 Color: 11
Size: 476 Color: 8

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 4
Size: 2468 Color: 14
Size: 396 Color: 6

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13338 Color: 3
Size: 2162 Color: 7
Size: 660 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13352 Color: 8
Size: 2440 Color: 7
Size: 368 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13463 Color: 15
Size: 1945 Color: 19
Size: 752 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 8
Size: 2408 Color: 16
Size: 336 Color: 10

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13460 Color: 19
Size: 2448 Color: 3
Size: 252 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13468 Color: 8
Size: 2244 Color: 18
Size: 448 Color: 15

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13570 Color: 12
Size: 1862 Color: 0
Size: 728 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13675 Color: 6
Size: 1733 Color: 2
Size: 752 Color: 5

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13679 Color: 18
Size: 1841 Color: 14
Size: 640 Color: 7

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 12
Size: 1564 Color: 7
Size: 896 Color: 15

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13770 Color: 7
Size: 1788 Color: 12
Size: 602 Color: 13

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 15
Size: 1812 Color: 16
Size: 540 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 1
Size: 1798 Color: 0
Size: 508 Color: 12

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13954 Color: 6
Size: 1550 Color: 6
Size: 656 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13968 Color: 15
Size: 1344 Color: 9
Size: 848 Color: 13

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14021 Color: 2
Size: 1603 Color: 13
Size: 536 Color: 14

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 19
Size: 1072 Color: 16
Size: 1056 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14093 Color: 11
Size: 1723 Color: 16
Size: 344 Color: 8

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14160 Color: 18
Size: 1128 Color: 11
Size: 872 Color: 8

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 13
Size: 1384 Color: 15
Size: 608 Color: 14

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14206 Color: 12
Size: 1922 Color: 12
Size: 32 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 2
Size: 1080 Color: 7
Size: 864 Color: 17

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 17
Size: 1460 Color: 5
Size: 408 Color: 11

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 14412 Color: 1
Size: 1328 Color: 17
Size: 420 Color: 18

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 14446 Color: 1
Size: 976 Color: 16
Size: 738 Color: 12

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 14448 Color: 11
Size: 1360 Color: 16
Size: 352 Color: 15

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 11
Size: 1680 Color: 1
Size: 28 Color: 18

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 16
Size: 1192 Color: 12
Size: 454 Color: 6

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 15
Size: 1168 Color: 16
Size: 468 Color: 12

Bin 75: 1 of cap free
Amount of items: 7
Items: 
Size: 8092 Color: 0
Size: 1657 Color: 18
Size: 1428 Color: 19
Size: 1372 Color: 0
Size: 1328 Color: 16
Size: 1328 Color: 10
Size: 954 Color: 5

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 9163 Color: 18
Size: 6724 Color: 15
Size: 272 Color: 19

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 9167 Color: 6
Size: 6032 Color: 19
Size: 960 Color: 2

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 10435 Color: 3
Size: 5724 Color: 6

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 11036 Color: 18
Size: 4771 Color: 6
Size: 352 Color: 9

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 11596 Color: 14
Size: 4147 Color: 12
Size: 416 Color: 11

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 11763 Color: 15
Size: 3740 Color: 8
Size: 656 Color: 8

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 11731 Color: 5
Size: 3756 Color: 19
Size: 672 Color: 13

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 12196 Color: 18
Size: 3691 Color: 12
Size: 272 Color: 19

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 1
Size: 3665 Color: 14
Size: 250 Color: 3

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 12482 Color: 7
Size: 3293 Color: 4
Size: 384 Color: 5

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 12541 Color: 5
Size: 3066 Color: 10
Size: 552 Color: 1

Bin 87: 1 of cap free
Amount of items: 3
Items: 
Size: 12911 Color: 4
Size: 2104 Color: 3
Size: 1144 Color: 8

Bin 88: 1 of cap free
Amount of items: 3
Items: 
Size: 13431 Color: 15
Size: 2408 Color: 4
Size: 320 Color: 8

Bin 89: 1 of cap free
Amount of items: 2
Items: 
Size: 13647 Color: 4
Size: 2512 Color: 13

Bin 90: 1 of cap free
Amount of items: 3
Items: 
Size: 13893 Color: 15
Size: 1722 Color: 6
Size: 544 Color: 7

Bin 91: 1 of cap free
Amount of items: 3
Items: 
Size: 14033 Color: 10
Size: 1842 Color: 15
Size: 284 Color: 8

Bin 92: 1 of cap free
Amount of items: 3
Items: 
Size: 14236 Color: 12
Size: 1891 Color: 2
Size: 32 Color: 10

Bin 93: 1 of cap free
Amount of items: 2
Items: 
Size: 14270 Color: 4
Size: 1889 Color: 12

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 10206 Color: 5
Size: 5406 Color: 13
Size: 546 Color: 12

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 11566 Color: 7
Size: 4296 Color: 1
Size: 296 Color: 15

Bin 96: 2 of cap free
Amount of items: 3
Items: 
Size: 12186 Color: 16
Size: 3844 Color: 9
Size: 128 Color: 10

Bin 97: 2 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 16
Size: 3444 Color: 18
Size: 498 Color: 4

Bin 98: 2 of cap free
Amount of items: 3
Items: 
Size: 12830 Color: 9
Size: 3024 Color: 17
Size: 304 Color: 15

Bin 99: 2 of cap free
Amount of items: 3
Items: 
Size: 13181 Color: 6
Size: 2497 Color: 15
Size: 480 Color: 12

Bin 100: 2 of cap free
Amount of items: 3
Items: 
Size: 13634 Color: 1
Size: 1912 Color: 12
Size: 612 Color: 15

Bin 101: 2 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 7
Size: 2170 Color: 18

Bin 102: 2 of cap free
Amount of items: 3
Items: 
Size: 14096 Color: 16
Size: 1722 Color: 15
Size: 340 Color: 17

Bin 103: 2 of cap free
Amount of items: 3
Items: 
Size: 14302 Color: 7
Size: 1328 Color: 15
Size: 528 Color: 5

Bin 104: 2 of cap free
Amount of items: 2
Items: 
Size: 14382 Color: 5
Size: 1776 Color: 13

Bin 105: 2 of cap free
Amount of items: 2
Items: 
Size: 14504 Color: 17
Size: 1654 Color: 13

Bin 106: 3 of cap free
Amount of items: 3
Items: 
Size: 14233 Color: 3
Size: 1484 Color: 15
Size: 440 Color: 9

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 8148 Color: 6
Size: 6696 Color: 6
Size: 1312 Color: 19

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 9188 Color: 15
Size: 6664 Color: 11
Size: 304 Color: 10

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 9252 Color: 12
Size: 6648 Color: 18
Size: 256 Color: 1

Bin 110: 4 of cap free
Amount of items: 2
Items: 
Size: 10344 Color: 0
Size: 5812 Color: 16

Bin 111: 4 of cap free
Amount of items: 3
Items: 
Size: 10360 Color: 18
Size: 5450 Color: 6
Size: 346 Color: 0

Bin 112: 4 of cap free
Amount of items: 3
Items: 
Size: 10768 Color: 9
Size: 4924 Color: 16
Size: 464 Color: 12

Bin 113: 4 of cap free
Amount of items: 2
Items: 
Size: 11660 Color: 18
Size: 4496 Color: 16

Bin 114: 4 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 2
Size: 3418 Color: 4
Size: 458 Color: 10

Bin 115: 4 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 15
Size: 3360 Color: 16
Size: 128 Color: 7

Bin 116: 4 of cap free
Amount of items: 2
Items: 
Size: 13204 Color: 2
Size: 2952 Color: 8

Bin 117: 4 of cap free
Amount of items: 2
Items: 
Size: 13488 Color: 10
Size: 2668 Color: 1

Bin 118: 4 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 12
Size: 2516 Color: 5

Bin 119: 4 of cap free
Amount of items: 2
Items: 
Size: 13812 Color: 2
Size: 2344 Color: 4

Bin 120: 4 of cap free
Amount of items: 2
Items: 
Size: 14104 Color: 1
Size: 2052 Color: 10

Bin 121: 4 of cap free
Amount of items: 2
Items: 
Size: 14388 Color: 7
Size: 1768 Color: 16

Bin 122: 4 of cap free
Amount of items: 2
Items: 
Size: 14456 Color: 7
Size: 1700 Color: 2

Bin 123: 4 of cap free
Amount of items: 2
Items: 
Size: 14526 Color: 14
Size: 1630 Color: 3

Bin 124: 5 of cap free
Amount of items: 2
Items: 
Size: 10694 Color: 6
Size: 5461 Color: 14

Bin 125: 5 of cap free
Amount of items: 3
Items: 
Size: 10922 Color: 4
Size: 4745 Color: 2
Size: 488 Color: 4

Bin 126: 5 of cap free
Amount of items: 3
Items: 
Size: 11248 Color: 13
Size: 4197 Color: 6
Size: 710 Color: 6

Bin 127: 5 of cap free
Amount of items: 3
Items: 
Size: 12209 Color: 15
Size: 3658 Color: 5
Size: 288 Color: 11

Bin 128: 5 of cap free
Amount of items: 2
Items: 
Size: 13880 Color: 4
Size: 2275 Color: 19

Bin 129: 5 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 1
Size: 2095 Color: 19

Bin 130: 6 of cap free
Amount of items: 2
Items: 
Size: 12840 Color: 16
Size: 3314 Color: 5

Bin 131: 6 of cap free
Amount of items: 2
Items: 
Size: 13738 Color: 2
Size: 2416 Color: 4

Bin 132: 6 of cap free
Amount of items: 3
Items: 
Size: 14098 Color: 0
Size: 1956 Color: 19
Size: 100 Color: 7

Bin 133: 7 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 15
Size: 2091 Color: 13
Size: 1430 Color: 12

Bin 134: 7 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 9
Size: 1773 Color: 5

Bin 135: 8 of cap free
Amount of items: 9
Items: 
Size: 8082 Color: 17
Size: 1344 Color: 5
Size: 1204 Color: 0
Size: 1152 Color: 4
Size: 1104 Color: 7
Size: 984 Color: 16
Size: 984 Color: 0
Size: 658 Color: 10
Size: 640 Color: 8

Bin 136: 8 of cap free
Amount of items: 3
Items: 
Size: 9674 Color: 18
Size: 5982 Color: 0
Size: 496 Color: 18

Bin 137: 8 of cap free
Amount of items: 3
Items: 
Size: 10224 Color: 18
Size: 4840 Color: 19
Size: 1088 Color: 2

Bin 138: 8 of cap free
Amount of items: 2
Items: 
Size: 14006 Color: 11
Size: 2146 Color: 16

Bin 139: 8 of cap free
Amount of items: 2
Items: 
Size: 14081 Color: 12
Size: 2071 Color: 2

Bin 140: 9 of cap free
Amount of items: 3
Items: 
Size: 8986 Color: 17
Size: 6733 Color: 5
Size: 432 Color: 13

Bin 141: 9 of cap free
Amount of items: 2
Items: 
Size: 12883 Color: 0
Size: 3268 Color: 9

Bin 142: 9 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 0
Size: 3003 Color: 18

Bin 143: 10 of cap free
Amount of items: 3
Items: 
Size: 9622 Color: 17
Size: 5440 Color: 3
Size: 1088 Color: 6

Bin 144: 10 of cap free
Amount of items: 2
Items: 
Size: 13936 Color: 13
Size: 2214 Color: 9

Bin 145: 11 of cap free
Amount of items: 2
Items: 
Size: 11185 Color: 2
Size: 4964 Color: 11

Bin 146: 12 of cap free
Amount of items: 2
Items: 
Size: 12900 Color: 9
Size: 3248 Color: 3

Bin 147: 12 of cap free
Amount of items: 2
Items: 
Size: 13232 Color: 7
Size: 2916 Color: 8

Bin 148: 12 of cap free
Amount of items: 2
Items: 
Size: 14154 Color: 16
Size: 1994 Color: 2

Bin 149: 12 of cap free
Amount of items: 2
Items: 
Size: 14544 Color: 3
Size: 1604 Color: 16

Bin 150: 13 of cap free
Amount of items: 3
Items: 
Size: 9300 Color: 12
Size: 5363 Color: 5
Size: 1484 Color: 2

Bin 151: 13 of cap free
Amount of items: 2
Items: 
Size: 13651 Color: 7
Size: 2496 Color: 14

Bin 152: 14 of cap free
Amount of items: 34
Items: 
Size: 832 Color: 15
Size: 760 Color: 17
Size: 744 Color: 1
Size: 736 Color: 9
Size: 736 Color: 1
Size: 608 Color: 8
Size: 608 Color: 7
Size: 584 Color: 16
Size: 576 Color: 16
Size: 576 Color: 8
Size: 576 Color: 7
Size: 464 Color: 3
Size: 448 Color: 13
Size: 448 Color: 8
Size: 448 Color: 2
Size: 448 Color: 2
Size: 418 Color: 18
Size: 416 Color: 14
Size: 414 Color: 6
Size: 384 Color: 15
Size: 384 Color: 12
Size: 384 Color: 8
Size: 384 Color: 7
Size: 368 Color: 10
Size: 364 Color: 5
Size: 360 Color: 19
Size: 356 Color: 1
Size: 354 Color: 2
Size: 352 Color: 12
Size: 352 Color: 11
Size: 352 Color: 3
Size: 336 Color: 4
Size: 320 Color: 4
Size: 256 Color: 5

Bin 153: 14 of cap free
Amount of items: 3
Items: 
Size: 10948 Color: 19
Size: 4926 Color: 0
Size: 272 Color: 17

Bin 154: 14 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 3
Size: 2106 Color: 18

Bin 155: 14 of cap free
Amount of items: 2
Items: 
Size: 14178 Color: 14
Size: 1968 Color: 16

Bin 156: 16 of cap free
Amount of items: 2
Items: 
Size: 11168 Color: 17
Size: 4976 Color: 14

Bin 157: 16 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 11
Size: 2600 Color: 6

Bin 158: 16 of cap free
Amount of items: 2
Items: 
Size: 13732 Color: 14
Size: 2412 Color: 19

Bin 159: 16 of cap free
Amount of items: 2
Items: 
Size: 14020 Color: 14
Size: 2124 Color: 16

Bin 160: 19 of cap free
Amount of items: 2
Items: 
Size: 11129 Color: 17
Size: 5012 Color: 4

Bin 161: 19 of cap free
Amount of items: 2
Items: 
Size: 14237 Color: 12
Size: 1904 Color: 2

Bin 162: 20 of cap free
Amount of items: 2
Items: 
Size: 13168 Color: 6
Size: 2972 Color: 13

Bin 163: 20 of cap free
Amount of items: 2
Items: 
Size: 13888 Color: 10
Size: 2252 Color: 2

Bin 164: 20 of cap free
Amount of items: 2
Items: 
Size: 14516 Color: 5
Size: 1624 Color: 3

Bin 165: 21 of cap free
Amount of items: 7
Items: 
Size: 8085 Color: 11
Size: 1364 Color: 0
Size: 1346 Color: 1
Size: 1344 Color: 18
Size: 1344 Color: 14
Size: 1344 Color: 3
Size: 1312 Color: 11

Bin 166: 21 of cap free
Amount of items: 2
Items: 
Size: 13408 Color: 16
Size: 2731 Color: 5

Bin 167: 23 of cap free
Amount of items: 2
Items: 
Size: 9609 Color: 2
Size: 6528 Color: 14

Bin 168: 23 of cap free
Amount of items: 2
Items: 
Size: 14173 Color: 7
Size: 1964 Color: 10

Bin 169: 25 of cap free
Amount of items: 2
Items: 
Size: 13951 Color: 13
Size: 2184 Color: 19

Bin 170: 28 of cap free
Amount of items: 3
Items: 
Size: 8136 Color: 8
Size: 7724 Color: 0
Size: 272 Color: 11

Bin 171: 28 of cap free
Amount of items: 2
Items: 
Size: 14124 Color: 14
Size: 2008 Color: 18

Bin 172: 29 of cap free
Amount of items: 2
Items: 
Size: 13827 Color: 13
Size: 2304 Color: 4

Bin 173: 31 of cap free
Amount of items: 11
Items: 
Size: 8081 Color: 12
Size: 984 Color: 10
Size: 960 Color: 6
Size: 960 Color: 1
Size: 948 Color: 13
Size: 848 Color: 10
Size: 838 Color: 12
Size: 838 Color: 10
Size: 648 Color: 2
Size: 544 Color: 4
Size: 480 Color: 15

Bin 174: 32 of cap free
Amount of items: 2
Items: 
Size: 13506 Color: 5
Size: 2622 Color: 6

Bin 175: 35 of cap free
Amount of items: 4
Items: 
Size: 8104 Color: 9
Size: 4316 Color: 3
Size: 3017 Color: 15
Size: 688 Color: 17

Bin 176: 36 of cap free
Amount of items: 2
Items: 
Size: 14172 Color: 19
Size: 1952 Color: 10

Bin 177: 38 of cap free
Amount of items: 2
Items: 
Size: 13768 Color: 16
Size: 2354 Color: 10

Bin 178: 40 of cap free
Amount of items: 2
Items: 
Size: 12880 Color: 0
Size: 3240 Color: 3

Bin 179: 41 of cap free
Amount of items: 2
Items: 
Size: 13820 Color: 1
Size: 2299 Color: 0

Bin 180: 42 of cap free
Amount of items: 3
Items: 
Size: 11125 Color: 4
Size: 4193 Color: 7
Size: 800 Color: 0

Bin 181: 48 of cap free
Amount of items: 3
Items: 
Size: 8200 Color: 14
Size: 6728 Color: 19
Size: 1184 Color: 11

Bin 182: 48 of cap free
Amount of items: 2
Items: 
Size: 13403 Color: 8
Size: 2709 Color: 5

Bin 183: 58 of cap free
Amount of items: 4
Items: 
Size: 8112 Color: 15
Size: 5456 Color: 13
Size: 1374 Color: 10
Size: 1160 Color: 10

Bin 184: 68 of cap free
Amount of items: 3
Items: 
Size: 8944 Color: 16
Size: 6730 Color: 8
Size: 418 Color: 0

Bin 185: 69 of cap free
Amount of items: 3
Items: 
Size: 13790 Color: 10
Size: 2249 Color: 12
Size: 52 Color: 7

Bin 186: 75 of cap free
Amount of items: 6
Items: 
Size: 8086 Color: 16
Size: 1799 Color: 14
Size: 1792 Color: 4
Size: 1744 Color: 6
Size: 1672 Color: 19
Size: 992 Color: 17

Bin 187: 78 of cap free
Amount of items: 3
Items: 
Size: 9528 Color: 17
Size: 6034 Color: 14
Size: 520 Color: 15

Bin 188: 86 of cap free
Amount of items: 2
Items: 
Size: 11708 Color: 14
Size: 4366 Color: 4

Bin 189: 88 of cap free
Amount of items: 2
Items: 
Size: 10228 Color: 1
Size: 5844 Color: 8

Bin 190: 100 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 11
Size: 6728 Color: 17
Size: 1164 Color: 18

Bin 191: 123 of cap free
Amount of items: 3
Items: 
Size: 8922 Color: 19
Size: 6731 Color: 16
Size: 384 Color: 10

Bin 192: 148 of cap free
Amount of items: 2
Items: 
Size: 9725 Color: 2
Size: 6287 Color: 11

Bin 193: 158 of cap free
Amount of items: 2
Items: 
Size: 9268 Color: 0
Size: 6734 Color: 9

Bin 194: 165 of cap free
Amount of items: 2
Items: 
Size: 10467 Color: 10
Size: 5528 Color: 4

Bin 195: 183 of cap free
Amount of items: 2
Items: 
Size: 10148 Color: 19
Size: 5829 Color: 8

Bin 196: 200 of cap free
Amount of items: 2
Items: 
Size: 9224 Color: 3
Size: 6736 Color: 7

Bin 197: 210 of cap free
Amount of items: 2
Items: 
Size: 10988 Color: 12
Size: 4962 Color: 17

Bin 198: 232 of cap free
Amount of items: 2
Items: 
Size: 10144 Color: 18
Size: 5784 Color: 7

Bin 199: 12964 of cap free
Amount of items: 10
Items: 
Size: 328 Color: 7
Size: 324 Color: 17
Size: 320 Color: 19
Size: 320 Color: 16
Size: 320 Color: 13
Size: 320 Color: 12
Size: 320 Color: 11
Size: 320 Color: 4
Size: 320 Color: 0
Size: 304 Color: 15

Total size: 3199680
Total free space: 16160

