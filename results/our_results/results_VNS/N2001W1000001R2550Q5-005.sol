Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 359970 Color: 1
Size: 338343 Color: 2
Size: 301688 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 362110 Color: 4
Size: 326914 Color: 1
Size: 310977 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 362322 Color: 4
Size: 362096 Color: 1
Size: 275583 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 364270 Color: 2
Size: 357503 Color: 1
Size: 278228 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 365014 Color: 0
Size: 322934 Color: 0
Size: 312053 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 365871 Color: 3
Size: 332228 Color: 2
Size: 301902 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 368211 Color: 1
Size: 349869 Color: 4
Size: 281921 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 369710 Color: 3
Size: 327829 Color: 4
Size: 302462 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 369744 Color: 4
Size: 325259 Color: 2
Size: 304998 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 370295 Color: 0
Size: 364773 Color: 3
Size: 264933 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 370590 Color: 0
Size: 331841 Color: 3
Size: 297570 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 371584 Color: 3
Size: 325659 Color: 1
Size: 302758 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 372605 Color: 1
Size: 329311 Color: 0
Size: 298085 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 372839 Color: 3
Size: 352965 Color: 2
Size: 274197 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 373664 Color: 4
Size: 350563 Color: 3
Size: 275774 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 373842 Color: 2
Size: 348441 Color: 4
Size: 277718 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 374299 Color: 3
Size: 345751 Color: 4
Size: 279951 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 375382 Color: 3
Size: 340547 Color: 0
Size: 284072 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 375910 Color: 1
Size: 334411 Color: 4
Size: 289680 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 376703 Color: 3
Size: 312197 Color: 4
Size: 311101 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 378282 Color: 4
Size: 332208 Color: 0
Size: 289511 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 379714 Color: 0
Size: 343110 Color: 1
Size: 277177 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 379892 Color: 3
Size: 310866 Color: 4
Size: 309243 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 379497 Color: 2
Size: 326461 Color: 1
Size: 294043 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 380484 Color: 0
Size: 340043 Color: 2
Size: 279474 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 380306 Color: 2
Size: 312737 Color: 1
Size: 306958 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 380908 Color: 4
Size: 309663 Color: 2
Size: 309430 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 381031 Color: 3
Size: 309778 Color: 4
Size: 309192 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 381316 Color: 2
Size: 309438 Color: 0
Size: 309247 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 381430 Color: 3
Size: 356029 Color: 1
Size: 262542 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 381496 Color: 4
Size: 318488 Color: 3
Size: 300017 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 383851 Color: 2
Size: 355683 Color: 3
Size: 260467 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 383884 Color: 4
Size: 357017 Color: 0
Size: 259100 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 383950 Color: 0
Size: 341786 Color: 1
Size: 274265 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 384098 Color: 3
Size: 333092 Color: 4
Size: 282811 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 384220 Color: 1
Size: 333266 Color: 1
Size: 282515 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 384341 Color: 4
Size: 320259 Color: 4
Size: 295401 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 384473 Color: 3
Size: 316579 Color: 4
Size: 298949 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 385559 Color: 1
Size: 320207 Color: 2
Size: 294235 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 385961 Color: 4
Size: 346021 Color: 3
Size: 268019 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 385987 Color: 0
Size: 328559 Color: 3
Size: 285455 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 386130 Color: 4
Size: 333149 Color: 1
Size: 280722 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 386626 Color: 3
Size: 346147 Color: 1
Size: 267228 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 386853 Color: 0
Size: 313336 Color: 1
Size: 299812 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 386700 Color: 2
Size: 319183 Color: 0
Size: 294118 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 387264 Color: 3
Size: 308313 Color: 4
Size: 304424 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 388584 Color: 2
Size: 326129 Color: 3
Size: 285288 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 388496 Color: 3
Size: 325729 Color: 4
Size: 285776 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 389283 Color: 2
Size: 341606 Color: 4
Size: 269112 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 389114 Color: 3
Size: 309452 Color: 2
Size: 301435 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 389547 Color: 4
Size: 308125 Color: 1
Size: 302329 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 389963 Color: 2
Size: 350584 Color: 0
Size: 259454 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 390350 Color: 4
Size: 329056 Color: 3
Size: 280595 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 391246 Color: 0
Size: 321048 Color: 1
Size: 287707 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 391408 Color: 0
Size: 339193 Color: 2
Size: 269400 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 391417 Color: 2
Size: 319218 Color: 4
Size: 289366 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 391909 Color: 2
Size: 336393 Color: 4
Size: 271699 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 391505 Color: 3
Size: 319926 Color: 1
Size: 288570 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 392372 Color: 1
Size: 336473 Color: 4
Size: 271156 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 392363 Color: 2
Size: 311153 Color: 3
Size: 296485 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 392450 Color: 1
Size: 348738 Color: 3
Size: 258813 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 393609 Color: 1
Size: 336757 Color: 1
Size: 269635 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 393936 Color: 4
Size: 311528 Color: 4
Size: 294537 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 394534 Color: 2
Size: 340337 Color: 4
Size: 265130 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 394460 Color: 0
Size: 336921 Color: 0
Size: 268620 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 394935 Color: 4
Size: 339048 Color: 4
Size: 266018 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 395952 Color: 2
Size: 304890 Color: 3
Size: 299159 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 396047 Color: 0
Size: 310311 Color: 0
Size: 293643 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 396174 Color: 0
Size: 341150 Color: 2
Size: 262677 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 396358 Color: 4
Size: 335127 Color: 1
Size: 268516 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 396913 Color: 1
Size: 352241 Color: 0
Size: 250847 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 397019 Color: 1
Size: 311781 Color: 4
Size: 291201 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 398728 Color: 0
Size: 322254 Color: 4
Size: 279019 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 399251 Color: 1
Size: 305717 Color: 3
Size: 295033 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 399470 Color: 2
Size: 336828 Color: 2
Size: 263703 Color: 3

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 400157 Color: 0
Size: 303527 Color: 1
Size: 296317 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 400313 Color: 4
Size: 334877 Color: 1
Size: 264811 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 400872 Color: 0
Size: 309926 Color: 1
Size: 289203 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 401417 Color: 4
Size: 320762 Color: 1
Size: 277822 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 402545 Color: 3
Size: 338363 Color: 3
Size: 259093 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 402997 Color: 4
Size: 314637 Color: 4
Size: 282367 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 403229 Color: 2
Size: 326412 Color: 0
Size: 270360 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 403283 Color: 2
Size: 316133 Color: 3
Size: 280585 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 403220 Color: 1
Size: 334503 Color: 1
Size: 262278 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 403299 Color: 0
Size: 310224 Color: 1
Size: 286478 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 404116 Color: 2
Size: 305097 Color: 4
Size: 290788 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 403789 Color: 1
Size: 311461 Color: 3
Size: 284751 Color: 4

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 404464 Color: 2
Size: 339914 Color: 1
Size: 255623 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 403897 Color: 4
Size: 325135 Color: 1
Size: 270969 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 404811 Color: 3
Size: 317164 Color: 2
Size: 278026 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 405208 Color: 0
Size: 302726 Color: 3
Size: 292067 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 405652 Color: 4
Size: 319352 Color: 2
Size: 274997 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 405919 Color: 2
Size: 327516 Color: 1
Size: 266566 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 407607 Color: 2
Size: 332024 Color: 3
Size: 260370 Color: 4

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 407011 Color: 4
Size: 336191 Color: 4
Size: 256799 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 407797 Color: 2
Size: 301317 Color: 3
Size: 290887 Color: 4

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 407637 Color: 4
Size: 334497 Color: 0
Size: 257867 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 407963 Color: 0
Size: 300707 Color: 3
Size: 291331 Color: 4

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 408591 Color: 1
Size: 303492 Color: 2
Size: 287918 Color: 4

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 408916 Color: 2
Size: 307166 Color: 4
Size: 283919 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 408760 Color: 1
Size: 313957 Color: 1
Size: 277284 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 409565 Color: 0
Size: 338457 Color: 2
Size: 251979 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 409824 Color: 3
Size: 304567 Color: 3
Size: 285610 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 410795 Color: 3
Size: 305929 Color: 2
Size: 283277 Color: 4

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 412602 Color: 3
Size: 320350 Color: 1
Size: 267049 Color: 4

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 412301 Color: 4
Size: 305603 Color: 4
Size: 282097 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 413003 Color: 0
Size: 324813 Color: 1
Size: 262185 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 413005 Color: 2
Size: 314967 Color: 2
Size: 272029 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 413160 Color: 3
Size: 298744 Color: 1
Size: 288097 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 413566 Color: 1
Size: 325968 Color: 0
Size: 260467 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 414022 Color: 4
Size: 332972 Color: 0
Size: 253007 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 414512 Color: 2
Size: 292761 Color: 3
Size: 292728 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 414520 Color: 0
Size: 319086 Color: 4
Size: 266395 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 414623 Color: 1
Size: 306406 Color: 4
Size: 278972 Color: 3

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 416122 Color: 0
Size: 320931 Color: 0
Size: 262948 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 416732 Color: 4
Size: 330252 Color: 2
Size: 253017 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 417262 Color: 0
Size: 299787 Color: 0
Size: 282952 Color: 3

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 417344 Color: 0
Size: 319283 Color: 0
Size: 263374 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 418671 Color: 2
Size: 295622 Color: 3
Size: 285708 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 418827 Color: 0
Size: 317315 Color: 1
Size: 263859 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 418859 Color: 2
Size: 311269 Color: 1
Size: 269873 Color: 3

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 419169 Color: 2
Size: 308196 Color: 2
Size: 272636 Color: 3

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 419310 Color: 4
Size: 319902 Color: 3
Size: 260789 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 420416 Color: 4
Size: 314293 Color: 2
Size: 265292 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 421253 Color: 3
Size: 315238 Color: 1
Size: 263510 Color: 2

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 420538 Color: 1
Size: 303945 Color: 1
Size: 275518 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 421799 Color: 3
Size: 291996 Color: 4
Size: 286206 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 422126 Color: 0
Size: 307640 Color: 4
Size: 270235 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 422759 Color: 2
Size: 307104 Color: 4
Size: 270138 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 422875 Color: 3
Size: 311815 Color: 4
Size: 265311 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 422835 Color: 4
Size: 323687 Color: 4
Size: 253479 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 423662 Color: 3
Size: 325999 Color: 4
Size: 250340 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 423277 Color: 4
Size: 318832 Color: 0
Size: 257892 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 423819 Color: 3
Size: 295150 Color: 0
Size: 281032 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 423289 Color: 0
Size: 324177 Color: 4
Size: 252535 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 424872 Color: 3
Size: 311056 Color: 2
Size: 264073 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 425458 Color: 3
Size: 311864 Color: 0
Size: 262679 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 425282 Color: 1
Size: 291755 Color: 4
Size: 282964 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 425612 Color: 0
Size: 308278 Color: 3
Size: 266111 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 425702 Color: 2
Size: 289361 Color: 1
Size: 284938 Color: 3

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 425976 Color: 4
Size: 317738 Color: 2
Size: 256287 Color: 4

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 426281 Color: 3
Size: 306740 Color: 2
Size: 266980 Color: 2

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 426465 Color: 0
Size: 316397 Color: 1
Size: 257139 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 426733 Color: 1
Size: 300287 Color: 4
Size: 272981 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 426772 Color: 2
Size: 310588 Color: 3
Size: 262641 Color: 4

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 427600 Color: 3
Size: 313673 Color: 0
Size: 258728 Color: 4

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 427896 Color: 4
Size: 307032 Color: 0
Size: 265073 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 429047 Color: 0
Size: 290488 Color: 1
Size: 280466 Color: 3

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 429088 Color: 2
Size: 319898 Color: 0
Size: 251015 Color: 4

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 431249 Color: 3
Size: 292694 Color: 4
Size: 276058 Color: 2

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 429434 Color: 0
Size: 306069 Color: 2
Size: 264498 Color: 4

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 431294 Color: 3
Size: 290425 Color: 2
Size: 278282 Color: 2

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 431392 Color: 3
Size: 297342 Color: 4
Size: 271267 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 431419 Color: 3
Size: 294380 Color: 4
Size: 274202 Color: 2

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 431433 Color: 3
Size: 296163 Color: 1
Size: 272405 Color: 4

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 430451 Color: 1
Size: 293414 Color: 2
Size: 276136 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 431211 Color: 1
Size: 295447 Color: 1
Size: 273343 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 431924 Color: 3
Size: 284944 Color: 0
Size: 283133 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 431676 Color: 2
Size: 303159 Color: 4
Size: 265166 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 432193 Color: 3
Size: 315945 Color: 0
Size: 251863 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 432120 Color: 2
Size: 302470 Color: 4
Size: 265411 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 432923 Color: 4
Size: 313170 Color: 1
Size: 253908 Color: 4

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 433093 Color: 0
Size: 315034 Color: 1
Size: 251874 Color: 2

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 434874 Color: 2
Size: 295943 Color: 3
Size: 269184 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 434881 Color: 0
Size: 286671 Color: 4
Size: 278449 Color: 2

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 435609 Color: 3
Size: 307512 Color: 4
Size: 256880 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 435905 Color: 3
Size: 313756 Color: 0
Size: 250340 Color: 2

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 435399 Color: 1
Size: 304186 Color: 0
Size: 260416 Color: 4

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 436268 Color: 3
Size: 287348 Color: 4
Size: 276385 Color: 2

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 436413 Color: 3
Size: 303669 Color: 4
Size: 259919 Color: 2

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 436341 Color: 2
Size: 298444 Color: 4
Size: 265216 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 436502 Color: 2
Size: 287502 Color: 1
Size: 275997 Color: 3

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 436858 Color: 0
Size: 286882 Color: 3
Size: 276261 Color: 2

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 436926 Color: 2
Size: 307452 Color: 3
Size: 255623 Color: 4

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 437006 Color: 3
Size: 312578 Color: 2
Size: 250417 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 437015 Color: 1
Size: 311015 Color: 4
Size: 251971 Color: 4

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 438190 Color: 2
Size: 291502 Color: 0
Size: 270309 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 438632 Color: 1
Size: 301262 Color: 3
Size: 260107 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 439488 Color: 1
Size: 299445 Color: 4
Size: 261068 Color: 1

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 439718 Color: 3
Size: 308981 Color: 2
Size: 251302 Color: 4

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 439979 Color: 1
Size: 298357 Color: 4
Size: 261665 Color: 4

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 440225 Color: 3
Size: 299941 Color: 0
Size: 259835 Color: 2

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 440177 Color: 1
Size: 298199 Color: 4
Size: 261625 Color: 4

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 440330 Color: 1
Size: 298693 Color: 4
Size: 260978 Color: 3

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 440431 Color: 2
Size: 305086 Color: 0
Size: 254484 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 441209 Color: 3
Size: 284161 Color: 1
Size: 274631 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 441038 Color: 0
Size: 289224 Color: 2
Size: 269739 Color: 3

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 441807 Color: 3
Size: 280794 Color: 2
Size: 277400 Color: 4

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 441736 Color: 1
Size: 299527 Color: 4
Size: 258738 Color: 4

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 443033 Color: 3
Size: 301836 Color: 0
Size: 255132 Color: 2

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 443120 Color: 4
Size: 295522 Color: 0
Size: 261359 Color: 4

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 444327 Color: 0
Size: 284822 Color: 2
Size: 270852 Color: 3

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 444540 Color: 3
Size: 281605 Color: 1
Size: 273856 Color: 4

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 444427 Color: 2
Size: 296529 Color: 4
Size: 259045 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 444774 Color: 2
Size: 281919 Color: 4
Size: 273308 Color: 3

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 444952 Color: 3
Size: 284169 Color: 0
Size: 270880 Color: 4

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 444817 Color: 2
Size: 300998 Color: 2
Size: 254186 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 447891 Color: 1
Size: 293213 Color: 3
Size: 258897 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 448189 Color: 3
Size: 301740 Color: 0
Size: 250072 Color: 4

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 448227 Color: 4
Size: 278026 Color: 0
Size: 273748 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 448890 Color: 0
Size: 291975 Color: 1
Size: 259136 Color: 3

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 448929 Color: 2
Size: 293564 Color: 4
Size: 257508 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 449839 Color: 1
Size: 296817 Color: 0
Size: 253345 Color: 3

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 450016 Color: 3
Size: 279242 Color: 2
Size: 270743 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 449859 Color: 1
Size: 299877 Color: 4
Size: 250265 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 450166 Color: 3
Size: 299000 Color: 2
Size: 250835 Color: 4

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 449953 Color: 2
Size: 287749 Color: 4
Size: 262299 Color: 2

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 449977 Color: 0
Size: 289926 Color: 4
Size: 260098 Color: 3

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 450862 Color: 3
Size: 293432 Color: 0
Size: 255707 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 451116 Color: 2
Size: 288043 Color: 4
Size: 260842 Color: 2

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 451140 Color: 2
Size: 288516 Color: 3
Size: 260345 Color: 2

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 451141 Color: 3
Size: 284397 Color: 2
Size: 264463 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 451551 Color: 0
Size: 285199 Color: 0
Size: 263251 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 451597 Color: 2
Size: 290332 Color: 1
Size: 258072 Color: 3

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 451834 Color: 0
Size: 289963 Color: 4
Size: 258204 Color: 4

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 452284 Color: 1
Size: 277555 Color: 4
Size: 270162 Color: 3

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 452785 Color: 1
Size: 287977 Color: 1
Size: 259239 Color: 2

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 453014 Color: 3
Size: 296400 Color: 0
Size: 250587 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 452994 Color: 2
Size: 291361 Color: 1
Size: 255646 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 453583 Color: 1
Size: 289148 Color: 2
Size: 257270 Color: 3

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 453742 Color: 0
Size: 275098 Color: 3
Size: 271161 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 453894 Color: 1
Size: 281088 Color: 4
Size: 265019 Color: 4

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 454237 Color: 1
Size: 286987 Color: 1
Size: 258777 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 454389 Color: 1
Size: 275573 Color: 3
Size: 270039 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 454544 Color: 0
Size: 283092 Color: 1
Size: 262365 Color: 2

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 454666 Color: 2
Size: 285060 Color: 4
Size: 260275 Color: 3

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 454812 Color: 3
Size: 274784 Color: 2
Size: 270405 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 454716 Color: 1
Size: 292603 Color: 4
Size: 252682 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 455353 Color: 3
Size: 276385 Color: 2
Size: 268263 Color: 2

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 455233 Color: 1
Size: 274741 Color: 3
Size: 270027 Color: 4

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 455452 Color: 3
Size: 277284 Color: 1
Size: 267265 Color: 2

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 455577 Color: 1
Size: 291738 Color: 0
Size: 252686 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 455909 Color: 3
Size: 284339 Color: 4
Size: 259753 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 456022 Color: 3
Size: 292902 Color: 4
Size: 251077 Color: 4

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 456502 Color: 2
Size: 286427 Color: 1
Size: 257072 Color: 4

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 456630 Color: 2
Size: 272055 Color: 3
Size: 271316 Color: 4

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 457218 Color: 0
Size: 283023 Color: 3
Size: 259760 Color: 4

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 457456 Color: 2
Size: 284209 Color: 0
Size: 258336 Color: 2

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 457780 Color: 1
Size: 282772 Color: 4
Size: 259449 Color: 3

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 458600 Color: 4
Size: 273615 Color: 1
Size: 267786 Color: 4

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 458745 Color: 2
Size: 277326 Color: 0
Size: 263930 Color: 3

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 458754 Color: 1
Size: 274297 Color: 4
Size: 266950 Color: 2

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 458818 Color: 1
Size: 282640 Color: 0
Size: 258543 Color: 3

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 459253 Color: 4
Size: 270936 Color: 2
Size: 269812 Color: 3

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 459340 Color: 1
Size: 272861 Color: 2
Size: 267800 Color: 4

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 459909 Color: 3
Size: 277292 Color: 2
Size: 262800 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 459567 Color: 2
Size: 289599 Color: 1
Size: 250835 Color: 2

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 459619 Color: 0
Size: 278570 Color: 4
Size: 261812 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 460634 Color: 3
Size: 276220 Color: 2
Size: 263147 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 460275 Color: 4
Size: 276320 Color: 4
Size: 263406 Color: 3

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 461513 Color: 2
Size: 274527 Color: 2
Size: 263961 Color: 1

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 461881 Color: 2
Size: 276965 Color: 4
Size: 261155 Color: 3

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 462165 Color: 4
Size: 281533 Color: 2
Size: 256303 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 463091 Color: 1
Size: 273205 Color: 4
Size: 263705 Color: 3

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 463213 Color: 2
Size: 286117 Color: 0
Size: 250671 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 463346 Color: 4
Size: 273857 Color: 0
Size: 262798 Color: 3

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 463975 Color: 0
Size: 282177 Color: 3
Size: 253849 Color: 4

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 464100 Color: 1
Size: 269351 Color: 4
Size: 266550 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 464666 Color: 1
Size: 283568 Color: 3
Size: 251767 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 464890 Color: 3
Size: 282221 Color: 0
Size: 252890 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 466412 Color: 1
Size: 278039 Color: 2
Size: 255550 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 466413 Color: 0
Size: 269045 Color: 3
Size: 264543 Color: 2

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 466490 Color: 1
Size: 281167 Color: 0
Size: 252344 Color: 4

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 466546 Color: 1
Size: 276093 Color: 3
Size: 257362 Color: 2

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 466766 Color: 4
Size: 272539 Color: 4
Size: 260696 Color: 3

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 467004 Color: 1
Size: 267313 Color: 0
Size: 265684 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 467514 Color: 3
Size: 266617 Color: 2
Size: 265870 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 467554 Color: 3
Size: 277109 Color: 1
Size: 255338 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 467822 Color: 0
Size: 274825 Color: 4
Size: 257354 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 468308 Color: 3
Size: 268632 Color: 2
Size: 263061 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 469082 Color: 3
Size: 274641 Color: 2
Size: 256278 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 468763 Color: 4
Size: 272485 Color: 0
Size: 258753 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 469206 Color: 1
Size: 273366 Color: 3
Size: 257429 Color: 2

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 469344 Color: 4
Size: 274198 Color: 0
Size: 256459 Color: 4

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 469674 Color: 3
Size: 274915 Color: 2
Size: 255412 Color: 2

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 469348 Color: 2
Size: 276152 Color: 4
Size: 254501 Color: 4

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 469963 Color: 4
Size: 272537 Color: 0
Size: 257501 Color: 3

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 470291 Color: 0
Size: 277606 Color: 3
Size: 252104 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 470321 Color: 2
Size: 268745 Color: 0
Size: 260935 Color: 2

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 470570 Color: 4
Size: 269413 Color: 3
Size: 260018 Color: 2

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 470661 Color: 3
Size: 273684 Color: 1
Size: 255656 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 470628 Color: 1
Size: 265413 Color: 1
Size: 263960 Color: 4

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 471409 Color: 2
Size: 275803 Color: 3
Size: 252789 Color: 4

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 472091 Color: 4
Size: 264887 Color: 1
Size: 263023 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 472527 Color: 0
Size: 265825 Color: 1
Size: 261649 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 472640 Color: 0
Size: 268057 Color: 4
Size: 259304 Color: 3

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 472668 Color: 0
Size: 274222 Color: 1
Size: 253111 Color: 3

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 473448 Color: 0
Size: 269432 Color: 1
Size: 257121 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 473475 Color: 0
Size: 269732 Color: 4
Size: 256794 Color: 3

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 473877 Color: 2
Size: 269132 Color: 2
Size: 256992 Color: 3

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 475003 Color: 2
Size: 273770 Color: 0
Size: 251228 Color: 2

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 477120 Color: 0
Size: 271956 Color: 2
Size: 250925 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 477224 Color: 1
Size: 262553 Color: 2
Size: 260224 Color: 3

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 477276 Color: 0
Size: 266779 Color: 3
Size: 255946 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 478040 Color: 2
Size: 267357 Color: 1
Size: 254604 Color: 4

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 479214 Color: 0
Size: 261272 Color: 0
Size: 259515 Color: 3

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 479214 Color: 2
Size: 260626 Color: 0
Size: 260161 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 479765 Color: 4
Size: 264157 Color: 2
Size: 256079 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 480323 Color: 3
Size: 265986 Color: 4
Size: 253692 Color: 4

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 479890 Color: 4
Size: 268629 Color: 1
Size: 251482 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 480539 Color: 3
Size: 268832 Color: 2
Size: 250630 Color: 1

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 480798 Color: 2
Size: 260833 Color: 3
Size: 258370 Color: 2

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 480996 Color: 4
Size: 264631 Color: 4
Size: 254374 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 481084 Color: 2
Size: 264094 Color: 0
Size: 254823 Color: 3

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 481241 Color: 1
Size: 265571 Color: 4
Size: 253189 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 481553 Color: 4
Size: 260679 Color: 4
Size: 257769 Color: 3

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 482196 Color: 2
Size: 267168 Color: 2
Size: 250637 Color: 4

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 482973 Color: 4
Size: 260675 Color: 3
Size: 256353 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 483099 Color: 0
Size: 266846 Color: 3
Size: 250056 Color: 4

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 485737 Color: 2
Size: 257793 Color: 2
Size: 256471 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 486006 Color: 3
Size: 262249 Color: 0
Size: 251746 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 487768 Color: 1
Size: 258277 Color: 2
Size: 253956 Color: 3

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 487829 Color: 2
Size: 260198 Color: 2
Size: 251974 Color: 4

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 488416 Color: 3
Size: 260091 Color: 4
Size: 251494 Color: 4

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 488386 Color: 4
Size: 261404 Color: 4
Size: 250211 Color: 3

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 488760 Color: 2
Size: 260110 Color: 1
Size: 251131 Color: 4

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 488881 Color: 4
Size: 259819 Color: 4
Size: 251301 Color: 3

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 489316 Color: 4
Size: 258868 Color: 3
Size: 251817 Color: 1

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 490642 Color: 0
Size: 257299 Color: 4
Size: 252060 Color: 4

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 492472 Color: 2
Size: 254819 Color: 0
Size: 252710 Color: 2

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 493772 Color: 3
Size: 253475 Color: 2
Size: 252754 Color: 4

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 493969 Color: 1
Size: 254498 Color: 0
Size: 251534 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 494250 Color: 4
Size: 253004 Color: 4
Size: 252747 Color: 3

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 496065 Color: 1
Size: 252956 Color: 3
Size: 250980 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 496851 Color: 3
Size: 252881 Color: 0
Size: 250269 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 496571 Color: 1
Size: 252548 Color: 4
Size: 250882 Color: 1

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 497108 Color: 3
Size: 252197 Color: 4
Size: 250696 Color: 0

Bin 328: 1 of cap free
Amount of items: 3
Items: 
Size: 362334 Color: 1
Size: 345761 Color: 3
Size: 291905 Color: 2

Bin 329: 1 of cap free
Amount of items: 3
Items: 
Size: 364800 Color: 1
Size: 343918 Color: 4
Size: 291282 Color: 3

Bin 330: 1 of cap free
Amount of items: 3
Items: 
Size: 366201 Color: 0
Size: 324526 Color: 0
Size: 309273 Color: 3

Bin 331: 1 of cap free
Amount of items: 3
Items: 
Size: 366609 Color: 4
Size: 328968 Color: 1
Size: 304423 Color: 1

Bin 332: 1 of cap free
Amount of items: 3
Items: 
Size: 370052 Color: 3
Size: 329411 Color: 2
Size: 300537 Color: 3

Bin 333: 1 of cap free
Amount of items: 3
Items: 
Size: 370127 Color: 0
Size: 362107 Color: 4
Size: 267766 Color: 1

Bin 334: 1 of cap free
Amount of items: 3
Items: 
Size: 370763 Color: 0
Size: 327204 Color: 0
Size: 302033 Color: 2

Bin 335: 1 of cap free
Amount of items: 3
Items: 
Size: 370827 Color: 0
Size: 343509 Color: 3
Size: 285664 Color: 4

Bin 336: 1 of cap free
Amount of items: 3
Items: 
Size: 371137 Color: 1
Size: 320507 Color: 0
Size: 308356 Color: 4

Bin 337: 1 of cap free
Amount of items: 3
Items: 
Size: 371710 Color: 0
Size: 327773 Color: 3
Size: 300517 Color: 2

Bin 338: 1 of cap free
Amount of items: 3
Items: 
Size: 371937 Color: 3
Size: 340757 Color: 1
Size: 287306 Color: 3

Bin 339: 1 of cap free
Amount of items: 3
Items: 
Size: 374098 Color: 0
Size: 358704 Color: 3
Size: 267198 Color: 1

Bin 340: 1 of cap free
Amount of items: 3
Items: 
Size: 373923 Color: 2
Size: 332038 Color: 2
Size: 294039 Color: 3

Bin 341: 1 of cap free
Amount of items: 3
Items: 
Size: 374548 Color: 1
Size: 334639 Color: 2
Size: 290813 Color: 1

Bin 342: 1 of cap free
Amount of items: 3
Items: 
Size: 374618 Color: 0
Size: 330612 Color: 2
Size: 294770 Color: 3

Bin 343: 1 of cap free
Amount of items: 3
Items: 
Size: 376362 Color: 0
Size: 314968 Color: 2
Size: 308670 Color: 1

Bin 344: 1 of cap free
Amount of items: 3
Items: 
Size: 376285 Color: 1
Size: 323564 Color: 0
Size: 300151 Color: 2

Bin 345: 1 of cap free
Amount of items: 3
Items: 
Size: 376446 Color: 3
Size: 324577 Color: 3
Size: 298977 Color: 2

Bin 346: 1 of cap free
Amount of items: 3
Items: 
Size: 376586 Color: 2
Size: 322385 Color: 3
Size: 301029 Color: 1

Bin 347: 1 of cap free
Amount of items: 3
Items: 
Size: 377125 Color: 3
Size: 358439 Color: 3
Size: 264436 Color: 1

Bin 348: 1 of cap free
Amount of items: 3
Items: 
Size: 379204 Color: 0
Size: 325264 Color: 4
Size: 295532 Color: 2

Bin 349: 1 of cap free
Amount of items: 3
Items: 
Size: 378896 Color: 2
Size: 367844 Color: 4
Size: 253260 Color: 2

Bin 350: 1 of cap free
Amount of items: 3
Items: 
Size: 380477 Color: 1
Size: 311823 Color: 4
Size: 307700 Color: 0

Bin 351: 1 of cap free
Amount of items: 3
Items: 
Size: 380486 Color: 0
Size: 312638 Color: 3
Size: 306876 Color: 4

Bin 352: 1 of cap free
Amount of items: 3
Items: 
Size: 380900 Color: 2
Size: 368756 Color: 3
Size: 250344 Color: 0

Bin 353: 1 of cap free
Amount of items: 3
Items: 
Size: 385220 Color: 4
Size: 335301 Color: 1
Size: 279479 Color: 3

Bin 354: 1 of cap free
Amount of items: 3
Items: 
Size: 385424 Color: 2
Size: 364383 Color: 3
Size: 250193 Color: 0

Bin 355: 1 of cap free
Amount of items: 3
Items: 
Size: 385282 Color: 4
Size: 322887 Color: 1
Size: 291831 Color: 0

Bin 356: 1 of cap free
Amount of items: 3
Items: 
Size: 386100 Color: 4
Size: 311322 Color: 1
Size: 302578 Color: 2

Bin 357: 1 of cap free
Amount of items: 3
Items: 
Size: 388399 Color: 3
Size: 340898 Color: 0
Size: 270703 Color: 1

Bin 358: 1 of cap free
Amount of items: 3
Items: 
Size: 388529 Color: 4
Size: 328389 Color: 3
Size: 283082 Color: 1

Bin 359: 1 of cap free
Amount of items: 3
Items: 
Size: 389815 Color: 2
Size: 346673 Color: 1
Size: 263512 Color: 4

Bin 360: 1 of cap free
Amount of items: 3
Items: 
Size: 390752 Color: 3
Size: 344975 Color: 1
Size: 264273 Color: 0

Bin 361: 1 of cap free
Amount of items: 3
Items: 
Size: 391167 Color: 3
Size: 314927 Color: 2
Size: 293906 Color: 3

Bin 362: 1 of cap free
Amount of items: 3
Items: 
Size: 391492 Color: 3
Size: 326491 Color: 0
Size: 282017 Color: 3

Bin 363: 1 of cap free
Amount of items: 3
Items: 
Size: 392103 Color: 2
Size: 331588 Color: 4
Size: 276309 Color: 3

Bin 364: 1 of cap free
Amount of items: 3
Items: 
Size: 392756 Color: 1
Size: 311821 Color: 0
Size: 295423 Color: 1

Bin 365: 1 of cap free
Amount of items: 3
Items: 
Size: 393644 Color: 3
Size: 355340 Color: 0
Size: 251016 Color: 2

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 394493 Color: 2
Size: 320613 Color: 4
Size: 284894 Color: 3

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 394076 Color: 4
Size: 312047 Color: 1
Size: 293877 Color: 4

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 395196 Color: 3
Size: 348763 Color: 3
Size: 256041 Color: 1

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 396704 Color: 4
Size: 324829 Color: 2
Size: 278467 Color: 3

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 399322 Color: 4
Size: 327640 Color: 1
Size: 273038 Color: 3

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 401532 Color: 1
Size: 308549 Color: 3
Size: 289919 Color: 2

Bin 372: 1 of cap free
Amount of items: 3
Items: 
Size: 402104 Color: 0
Size: 328211 Color: 4
Size: 269685 Color: 3

Bin 373: 1 of cap free
Amount of items: 3
Items: 
Size: 402950 Color: 0
Size: 321698 Color: 3
Size: 275352 Color: 4

Bin 374: 1 of cap free
Amount of items: 3
Items: 
Size: 403013 Color: 2
Size: 338025 Color: 3
Size: 258962 Color: 4

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 403357 Color: 2
Size: 305527 Color: 2
Size: 291116 Color: 3

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 403853 Color: 2
Size: 344462 Color: 1
Size: 251685 Color: 0

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 406966 Color: 4
Size: 329887 Color: 0
Size: 263147 Color: 3

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 407205 Color: 1
Size: 307805 Color: 1
Size: 284990 Color: 2

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 408599 Color: 2
Size: 314466 Color: 1
Size: 276935 Color: 3

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 408644 Color: 2
Size: 330483 Color: 3
Size: 260873 Color: 3

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 408451 Color: 1
Size: 332082 Color: 1
Size: 259467 Color: 4

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 410268 Color: 2
Size: 319605 Color: 4
Size: 270127 Color: 0

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 410633 Color: 0
Size: 332346 Color: 4
Size: 257021 Color: 3

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 411169 Color: 2
Size: 317788 Color: 4
Size: 271043 Color: 4

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 411760 Color: 0
Size: 318991 Color: 2
Size: 269249 Color: 4

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 412999 Color: 3
Size: 336528 Color: 1
Size: 250473 Color: 1

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 413571 Color: 1
Size: 322390 Color: 3
Size: 264039 Color: 0

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 414144 Color: 0
Size: 313107 Color: 4
Size: 272749 Color: 3

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 416425 Color: 4
Size: 315450 Color: 3
Size: 268125 Color: 4

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 417225 Color: 3
Size: 329676 Color: 0
Size: 253099 Color: 2

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 417192 Color: 0
Size: 314546 Color: 2
Size: 268262 Color: 2

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 417547 Color: 0
Size: 331349 Color: 2
Size: 251104 Color: 4

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 417925 Color: 3
Size: 315637 Color: 4
Size: 266438 Color: 2

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 418907 Color: 0
Size: 322612 Color: 4
Size: 258481 Color: 4

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 419832 Color: 1
Size: 306518 Color: 1
Size: 273650 Color: 4

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 419683 Color: 3
Size: 318436 Color: 4
Size: 261881 Color: 0

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 421302 Color: 1
Size: 309982 Color: 2
Size: 268716 Color: 3

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 421723 Color: 1
Size: 303691 Color: 0
Size: 274586 Color: 4

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 421854 Color: 1
Size: 297787 Color: 3
Size: 280359 Color: 0

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 422592 Color: 0
Size: 290540 Color: 2
Size: 286868 Color: 1

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 424256 Color: 4
Size: 302547 Color: 0
Size: 273197 Color: 3

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 424610 Color: 4
Size: 303786 Color: 2
Size: 271604 Color: 0

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 425183 Color: 1
Size: 310500 Color: 1
Size: 264317 Color: 4

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 426279 Color: 2
Size: 323653 Color: 3
Size: 250068 Color: 2

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 428519 Color: 0
Size: 310378 Color: 3
Size: 261103 Color: 2

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 431332 Color: 3
Size: 289892 Color: 0
Size: 278776 Color: 1

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 429959 Color: 1
Size: 293687 Color: 1
Size: 276354 Color: 2

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 431912 Color: 1
Size: 292492 Color: 0
Size: 275596 Color: 3

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 432463 Color: 2
Size: 294726 Color: 3
Size: 272811 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 432518 Color: 4
Size: 315436 Color: 2
Size: 252046 Color: 3

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 435177 Color: 3
Size: 306646 Color: 2
Size: 258177 Color: 1

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 436896 Color: 4
Size: 303596 Color: 0
Size: 259508 Color: 1

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 437569 Color: 2
Size: 298699 Color: 4
Size: 263732 Color: 3

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 439457 Color: 0
Size: 289072 Color: 3
Size: 271471 Color: 0

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 439519 Color: 0
Size: 298322 Color: 3
Size: 262159 Color: 2

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 444939 Color: 0
Size: 300131 Color: 0
Size: 254930 Color: 1

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 446359 Color: 3
Size: 297067 Color: 2
Size: 256574 Color: 4

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 446414 Color: 4
Size: 278864 Color: 0
Size: 274722 Color: 3

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 446557 Color: 3
Size: 285276 Color: 0
Size: 268167 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 446839 Color: 4
Size: 289135 Color: 0
Size: 264026 Color: 2

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 452291 Color: 4
Size: 279955 Color: 3
Size: 267754 Color: 1

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 452964 Color: 4
Size: 282145 Color: 2
Size: 264891 Color: 3

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 454173 Color: 1
Size: 280709 Color: 1
Size: 265118 Color: 3

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 454728 Color: 2
Size: 292163 Color: 0
Size: 253109 Color: 1

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 455900 Color: 3
Size: 283929 Color: 2
Size: 260171 Color: 4

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 455774 Color: 1
Size: 273241 Color: 4
Size: 270985 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 459689 Color: 3
Size: 270821 Color: 0
Size: 269490 Color: 2

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 467059 Color: 4
Size: 280228 Color: 1
Size: 252713 Color: 4

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 467528 Color: 2
Size: 267736 Color: 3
Size: 264736 Color: 1

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 472436 Color: 1
Size: 274121 Color: 4
Size: 253443 Color: 3

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 475306 Color: 0
Size: 274279 Color: 3
Size: 250415 Color: 0

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 479032 Color: 3
Size: 263409 Color: 1
Size: 257559 Color: 4

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 480796 Color: 4
Size: 261183 Color: 1
Size: 258021 Color: 3

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 486013 Color: 4
Size: 258150 Color: 4
Size: 255837 Color: 1

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 486223 Color: 4
Size: 258769 Color: 4
Size: 255008 Color: 3

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 486916 Color: 1
Size: 257272 Color: 3
Size: 255812 Color: 2

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 486998 Color: 0
Size: 259365 Color: 4
Size: 253637 Color: 4

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 489716 Color: 0
Size: 259824 Color: 1
Size: 250460 Color: 2

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 490121 Color: 1
Size: 259635 Color: 3
Size: 250244 Color: 2

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 490579 Color: 1
Size: 257274 Color: 1
Size: 252147 Color: 3

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 494192 Color: 2
Size: 253025 Color: 3
Size: 252783 Color: 2

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 494355 Color: 1
Size: 255455 Color: 1
Size: 250190 Color: 0

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 496198 Color: 1
Size: 252639 Color: 0
Size: 251163 Color: 1

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 498205 Color: 3
Size: 251070 Color: 2
Size: 250725 Color: 1

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 499930 Color: 0
Size: 250041 Color: 3
Size: 250029 Color: 0

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 360442 Color: 3
Size: 359130 Color: 2
Size: 280427 Color: 3

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 360861 Color: 2
Size: 320518 Color: 0
Size: 318620 Color: 2

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 365614 Color: 0
Size: 344000 Color: 2
Size: 290385 Color: 2

Bin 449: 2 of cap free
Amount of items: 3
Items: 
Size: 365732 Color: 4
Size: 326695 Color: 1
Size: 307572 Color: 2

Bin 450: 2 of cap free
Amount of items: 3
Items: 
Size: 366991 Color: 2
Size: 330826 Color: 3
Size: 302182 Color: 3

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 367600 Color: 1
Size: 334094 Color: 3
Size: 298305 Color: 0

Bin 452: 2 of cap free
Amount of items: 3
Items: 
Size: 368977 Color: 2
Size: 351685 Color: 0
Size: 279337 Color: 3

Bin 453: 2 of cap free
Amount of items: 3
Items: 
Size: 369530 Color: 1
Size: 322187 Color: 3
Size: 308282 Color: 1

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 370371 Color: 3
Size: 318792 Color: 0
Size: 310836 Color: 0

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 371426 Color: 4
Size: 357865 Color: 3
Size: 270708 Color: 4

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 371204 Color: 0
Size: 333836 Color: 0
Size: 294959 Color: 3

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 371666 Color: 0
Size: 329184 Color: 4
Size: 299149 Color: 0

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 374461 Color: 4
Size: 369689 Color: 3
Size: 255849 Color: 0

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 375944 Color: 2
Size: 330794 Color: 0
Size: 293261 Color: 1

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 376419 Color: 4
Size: 322981 Color: 3
Size: 300599 Color: 4

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 377824 Color: 1
Size: 322200 Color: 1
Size: 299975 Color: 2

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 377931 Color: 1
Size: 370624 Color: 0
Size: 251444 Color: 4

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 378470 Color: 2
Size: 320177 Color: 3
Size: 301352 Color: 0

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 378349 Color: 0
Size: 340612 Color: 2
Size: 281038 Color: 3

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 378355 Color: 3
Size: 353085 Color: 3
Size: 268559 Color: 0

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 379681 Color: 3
Size: 344163 Color: 1
Size: 276155 Color: 0

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 380102 Color: 4
Size: 366820 Color: 3
Size: 253077 Color: 4

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 380495 Color: 4
Size: 321989 Color: 2
Size: 297515 Color: 4

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 381249 Color: 0
Size: 368735 Color: 1
Size: 250015 Color: 4

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 381602 Color: 0
Size: 314863 Color: 1
Size: 303534 Color: 2

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 381706 Color: 0
Size: 353976 Color: 3
Size: 264317 Color: 1

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 381638 Color: 2
Size: 340721 Color: 4
Size: 277640 Color: 1

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 382979 Color: 2
Size: 334189 Color: 1
Size: 282831 Color: 2

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 386150 Color: 1
Size: 360819 Color: 2
Size: 253030 Color: 3

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 387024 Color: 0
Size: 357946 Color: 3
Size: 255029 Color: 4

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 388771 Color: 4
Size: 312151 Color: 3
Size: 299077 Color: 0

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 389677 Color: 3
Size: 312199 Color: 0
Size: 298123 Color: 2

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 391592 Color: 0
Size: 340472 Color: 4
Size: 267935 Color: 2

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 391934 Color: 4
Size: 348697 Color: 0
Size: 259368 Color: 1

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 392553 Color: 3
Size: 336367 Color: 2
Size: 271079 Color: 0

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 397326 Color: 3
Size: 342826 Color: 3
Size: 259847 Color: 1

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 403532 Color: 2
Size: 299648 Color: 3
Size: 296819 Color: 1

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 408034 Color: 2
Size: 341498 Color: 1
Size: 250467 Color: 1

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 411064 Color: 0
Size: 329535 Color: 4
Size: 259400 Color: 3

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 421431 Color: 0
Size: 324989 Color: 1
Size: 253579 Color: 3

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 422175 Color: 0
Size: 312640 Color: 4
Size: 265184 Color: 3

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 425064 Color: 4
Size: 321606 Color: 0
Size: 253329 Color: 3

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 426470 Color: 1
Size: 315812 Color: 3
Size: 257717 Color: 4

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 429431 Color: 2
Size: 290189 Color: 4
Size: 280379 Color: 1

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 447394 Color: 1
Size: 292896 Color: 3
Size: 259709 Color: 2

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 448423 Color: 2
Size: 278081 Color: 4
Size: 273495 Color: 3

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 454474 Color: 3
Size: 292711 Color: 4
Size: 252814 Color: 1

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 461365 Color: 3
Size: 277876 Color: 4
Size: 260758 Color: 4

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 471383 Color: 1
Size: 276709 Color: 4
Size: 251907 Color: 3

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 481995 Color: 4
Size: 261838 Color: 3
Size: 256166 Color: 2

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 491620 Color: 4
Size: 257307 Color: 4
Size: 251072 Color: 3

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 498681 Color: 2
Size: 251172 Color: 3
Size: 250146 Color: 2

Bin 498: 3 of cap free
Amount of items: 3
Items: 
Size: 353441 Color: 3
Size: 337136 Color: 4
Size: 309421 Color: 3

Bin 499: 3 of cap free
Amount of items: 3
Items: 
Size: 370902 Color: 4
Size: 320266 Color: 2
Size: 308830 Color: 3

Bin 500: 3 of cap free
Amount of items: 3
Items: 
Size: 372868 Color: 2
Size: 348228 Color: 1
Size: 278902 Color: 0

Bin 501: 3 of cap free
Amount of items: 3
Items: 
Size: 373900 Color: 1
Size: 336636 Color: 1
Size: 289462 Color: 0

Bin 502: 3 of cap free
Amount of items: 3
Items: 
Size: 375047 Color: 2
Size: 360466 Color: 0
Size: 264485 Color: 4

Bin 503: 3 of cap free
Amount of items: 3
Items: 
Size: 375363 Color: 3
Size: 327242 Color: 1
Size: 297393 Color: 3

Bin 504: 3 of cap free
Amount of items: 3
Items: 
Size: 379004 Color: 2
Size: 347849 Color: 3
Size: 273145 Color: 0

Bin 505: 3 of cap free
Amount of items: 3
Items: 
Size: 386528 Color: 4
Size: 349254 Color: 2
Size: 264216 Color: 1

Bin 506: 3 of cap free
Amount of items: 3
Items: 
Size: 389595 Color: 2
Size: 359936 Color: 3
Size: 250467 Color: 1

Bin 507: 3 of cap free
Amount of items: 3
Items: 
Size: 390386 Color: 1
Size: 312939 Color: 3
Size: 296673 Color: 2

Bin 508: 3 of cap free
Amount of items: 3
Items: 
Size: 394576 Color: 4
Size: 330430 Color: 3
Size: 274992 Color: 2

Bin 509: 3 of cap free
Amount of items: 3
Items: 
Size: 395919 Color: 1
Size: 326448 Color: 1
Size: 277631 Color: 2

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 399215 Color: 3
Size: 336293 Color: 4
Size: 264490 Color: 2

Bin 511: 3 of cap free
Amount of items: 3
Items: 
Size: 403088 Color: 4
Size: 301734 Color: 4
Size: 295176 Color: 3

Bin 512: 3 of cap free
Amount of items: 3
Items: 
Size: 409340 Color: 3
Size: 308034 Color: 1
Size: 282624 Color: 2

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 412325 Color: 3
Size: 332483 Color: 3
Size: 255190 Color: 2

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 413206 Color: 0
Size: 313825 Color: 3
Size: 272967 Color: 0

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 415959 Color: 0
Size: 292321 Color: 3
Size: 291718 Color: 1

Bin 516: 3 of cap free
Amount of items: 3
Items: 
Size: 416731 Color: 2
Size: 307257 Color: 0
Size: 276010 Color: 3

Bin 517: 3 of cap free
Amount of items: 3
Items: 
Size: 447963 Color: 4
Size: 289646 Color: 4
Size: 262389 Color: 1

Bin 518: 3 of cap free
Amount of items: 3
Items: 
Size: 458340 Color: 4
Size: 288924 Color: 3
Size: 252734 Color: 1

Bin 519: 3 of cap free
Amount of items: 3
Items: 
Size: 461557 Color: 0
Size: 278812 Color: 3
Size: 259629 Color: 1

Bin 520: 3 of cap free
Amount of items: 3
Items: 
Size: 468831 Color: 2
Size: 275336 Color: 1
Size: 255831 Color: 3

Bin 521: 3 of cap free
Amount of items: 3
Items: 
Size: 479634 Color: 2
Size: 268293 Color: 4
Size: 252071 Color: 3

Bin 522: 3 of cap free
Amount of items: 3
Items: 
Size: 494484 Color: 0
Size: 254938 Color: 0
Size: 250576 Color: 3

Bin 523: 3 of cap free
Amount of items: 3
Items: 
Size: 495918 Color: 0
Size: 253835 Color: 3
Size: 250245 Color: 2

Bin 524: 4 of cap free
Amount of items: 3
Items: 
Size: 357543 Color: 4
Size: 346610 Color: 4
Size: 295844 Color: 0

Bin 525: 4 of cap free
Amount of items: 3
Items: 
Size: 362243 Color: 4
Size: 339614 Color: 0
Size: 298140 Color: 2

Bin 526: 4 of cap free
Amount of items: 3
Items: 
Size: 362857 Color: 3
Size: 333358 Color: 1
Size: 303782 Color: 0

Bin 527: 4 of cap free
Amount of items: 3
Items: 
Size: 367374 Color: 3
Size: 321249 Color: 2
Size: 311374 Color: 0

Bin 528: 4 of cap free
Amount of items: 3
Items: 
Size: 368639 Color: 2
Size: 351712 Color: 0
Size: 279646 Color: 3

Bin 529: 4 of cap free
Amount of items: 3
Items: 
Size: 368506 Color: 4
Size: 365776 Color: 1
Size: 265715 Color: 4

Bin 530: 4 of cap free
Amount of items: 3
Items: 
Size: 370449 Color: 2
Size: 324357 Color: 1
Size: 305191 Color: 1

Bin 531: 4 of cap free
Amount of items: 3
Items: 
Size: 370682 Color: 1
Size: 322800 Color: 4
Size: 306515 Color: 1

Bin 532: 4 of cap free
Amount of items: 3
Items: 
Size: 371347 Color: 0
Size: 371205 Color: 3
Size: 257445 Color: 4

Bin 533: 4 of cap free
Amount of items: 3
Items: 
Size: 373575 Color: 3
Size: 367299 Color: 2
Size: 259123 Color: 4

Bin 534: 4 of cap free
Amount of items: 3
Items: 
Size: 374272 Color: 2
Size: 356167 Color: 4
Size: 269558 Color: 3

Bin 535: 4 of cap free
Amount of items: 3
Items: 
Size: 378064 Color: 1
Size: 331617 Color: 0
Size: 290316 Color: 2

Bin 536: 4 of cap free
Amount of items: 3
Items: 
Size: 378512 Color: 2
Size: 362919 Color: 4
Size: 258566 Color: 4

Bin 537: 4 of cap free
Amount of items: 3
Items: 
Size: 379403 Color: 4
Size: 334649 Color: 1
Size: 285945 Color: 1

Bin 538: 4 of cap free
Amount of items: 3
Items: 
Size: 379479 Color: 4
Size: 312276 Color: 3
Size: 308242 Color: 0

Bin 539: 4 of cap free
Amount of items: 3
Items: 
Size: 382615 Color: 3
Size: 366382 Color: 1
Size: 251000 Color: 2

Bin 540: 4 of cap free
Amount of items: 3
Items: 
Size: 385167 Color: 1
Size: 334021 Color: 2
Size: 280809 Color: 0

Bin 541: 4 of cap free
Amount of items: 3
Items: 
Size: 387542 Color: 2
Size: 309916 Color: 0
Size: 302539 Color: 0

Bin 542: 4 of cap free
Amount of items: 3
Items: 
Size: 391233 Color: 4
Size: 335336 Color: 1
Size: 273428 Color: 2

Bin 543: 4 of cap free
Amount of items: 3
Items: 
Size: 397546 Color: 4
Size: 337572 Color: 1
Size: 264879 Color: 2

Bin 544: 4 of cap free
Amount of items: 3
Items: 
Size: 412660 Color: 4
Size: 323743 Color: 4
Size: 263594 Color: 3

Bin 545: 4 of cap free
Amount of items: 3
Items: 
Size: 423083 Color: 4
Size: 297868 Color: 3
Size: 279046 Color: 4

Bin 546: 4 of cap free
Amount of items: 3
Items: 
Size: 492690 Color: 0
Size: 257223 Color: 3
Size: 250084 Color: 2

Bin 547: 5 of cap free
Amount of items: 3
Items: 
Size: 356140 Color: 3
Size: 325729 Color: 2
Size: 318127 Color: 2

Bin 548: 5 of cap free
Amount of items: 3
Items: 
Size: 367000 Color: 2
Size: 344359 Color: 0
Size: 288637 Color: 3

Bin 549: 5 of cap free
Amount of items: 3
Items: 
Size: 370821 Color: 4
Size: 334377 Color: 3
Size: 294798 Color: 2

Bin 550: 5 of cap free
Amount of items: 3
Items: 
Size: 371652 Color: 2
Size: 334139 Color: 3
Size: 294205 Color: 0

Bin 551: 5 of cap free
Amount of items: 3
Items: 
Size: 376266 Color: 3
Size: 328001 Color: 2
Size: 295729 Color: 2

Bin 552: 5 of cap free
Amount of items: 3
Items: 
Size: 376988 Color: 3
Size: 327981 Color: 0
Size: 295027 Color: 2

Bin 553: 5 of cap free
Amount of items: 3
Items: 
Size: 379936 Color: 1
Size: 359585 Color: 0
Size: 260475 Color: 1

Bin 554: 5 of cap free
Amount of items: 3
Items: 
Size: 379981 Color: 3
Size: 356576 Color: 0
Size: 263439 Color: 1

Bin 555: 5 of cap free
Amount of items: 3
Items: 
Size: 397149 Color: 3
Size: 334002 Color: 0
Size: 268845 Color: 2

Bin 556: 5 of cap free
Amount of items: 3
Items: 
Size: 400529 Color: 4
Size: 308681 Color: 2
Size: 290786 Color: 1

Bin 557: 5 of cap free
Amount of items: 3
Items: 
Size: 424044 Color: 0
Size: 311486 Color: 2
Size: 264466 Color: 3

Bin 558: 6 of cap free
Amount of items: 3
Items: 
Size: 367830 Color: 2
Size: 360010 Color: 4
Size: 272155 Color: 4

Bin 559: 6 of cap free
Amount of items: 3
Items: 
Size: 369281 Color: 0
Size: 329621 Color: 4
Size: 301093 Color: 2

Bin 560: 6 of cap free
Amount of items: 3
Items: 
Size: 375488 Color: 1
Size: 350885 Color: 3
Size: 273622 Color: 4

Bin 561: 6 of cap free
Amount of items: 3
Items: 
Size: 380213 Color: 2
Size: 323524 Color: 0
Size: 296258 Color: 1

Bin 562: 6 of cap free
Amount of items: 3
Items: 
Size: 385265 Color: 3
Size: 353923 Color: 1
Size: 260807 Color: 2

Bin 563: 6 of cap free
Amount of items: 3
Items: 
Size: 404284 Color: 1
Size: 337460 Color: 2
Size: 258251 Color: 1

Bin 564: 6 of cap free
Amount of items: 3
Items: 
Size: 499392 Color: 2
Size: 250543 Color: 0
Size: 250060 Color: 3

Bin 565: 7 of cap free
Amount of items: 3
Items: 
Size: 363120 Color: 4
Size: 332726 Color: 4
Size: 304148 Color: 1

Bin 566: 7 of cap free
Amount of items: 3
Items: 
Size: 362810 Color: 1
Size: 340519 Color: 3
Size: 296665 Color: 3

Bin 567: 7 of cap free
Amount of items: 3
Items: 
Size: 363786 Color: 4
Size: 359736 Color: 0
Size: 276472 Color: 1

Bin 568: 7 of cap free
Amount of items: 3
Items: 
Size: 364105 Color: 0
Size: 322915 Color: 0
Size: 312974 Color: 2

Bin 569: 7 of cap free
Amount of items: 3
Items: 
Size: 369191 Color: 2
Size: 324658 Color: 4
Size: 306145 Color: 1

Bin 570: 7 of cap free
Amount of items: 3
Items: 
Size: 374100 Color: 2
Size: 320042 Color: 1
Size: 305852 Color: 0

Bin 571: 7 of cap free
Amount of items: 3
Items: 
Size: 374178 Color: 4
Size: 334710 Color: 0
Size: 291106 Color: 2

Bin 572: 7 of cap free
Amount of items: 3
Items: 
Size: 376585 Color: 0
Size: 324078 Color: 3
Size: 299331 Color: 2

Bin 573: 7 of cap free
Amount of items: 3
Items: 
Size: 379646 Color: 0
Size: 351556 Color: 0
Size: 268792 Color: 2

Bin 574: 7 of cap free
Amount of items: 3
Items: 
Size: 382008 Color: 1
Size: 320994 Color: 1
Size: 296992 Color: 0

Bin 575: 7 of cap free
Amount of items: 3
Items: 
Size: 394024 Color: 3
Size: 337938 Color: 2
Size: 268032 Color: 3

Bin 576: 7 of cap free
Amount of items: 3
Items: 
Size: 419888 Color: 2
Size: 323429 Color: 2
Size: 256677 Color: 3

Bin 577: 7 of cap free
Amount of items: 3
Items: 
Size: 495064 Color: 0
Size: 253201 Color: 1
Size: 251729 Color: 2

Bin 578: 8 of cap free
Amount of items: 3
Items: 
Size: 361050 Color: 3
Size: 333737 Color: 0
Size: 305206 Color: 4

Bin 579: 8 of cap free
Amount of items: 3
Items: 
Size: 368004 Color: 3
Size: 332401 Color: 4
Size: 299588 Color: 2

Bin 580: 8 of cap free
Amount of items: 3
Items: 
Size: 379079 Color: 4
Size: 352708 Color: 3
Size: 268206 Color: 2

Bin 581: 8 of cap free
Amount of items: 3
Items: 
Size: 384665 Color: 0
Size: 328501 Color: 4
Size: 286827 Color: 2

Bin 582: 8 of cap free
Amount of items: 3
Items: 
Size: 388253 Color: 4
Size: 360075 Color: 1
Size: 251665 Color: 2

Bin 583: 9 of cap free
Amount of items: 3
Items: 
Size: 352442 Color: 3
Size: 337660 Color: 0
Size: 309890 Color: 4

Bin 584: 9 of cap free
Amount of items: 3
Items: 
Size: 358416 Color: 1
Size: 350810 Color: 2
Size: 290766 Color: 4

Bin 585: 9 of cap free
Amount of items: 3
Items: 
Size: 378688 Color: 0
Size: 362195 Color: 3
Size: 259109 Color: 1

Bin 586: 9 of cap free
Amount of items: 3
Items: 
Size: 381452 Color: 0
Size: 344660 Color: 2
Size: 273880 Color: 1

Bin 587: 11 of cap free
Amount of items: 3
Items: 
Size: 363581 Color: 3
Size: 325339 Color: 3
Size: 311070 Color: 4

Bin 588: 11 of cap free
Amount of items: 3
Items: 
Size: 398105 Color: 0
Size: 310753 Color: 2
Size: 291132 Color: 0

Bin 589: 12 of cap free
Amount of items: 3
Items: 
Size: 363723 Color: 0
Size: 334698 Color: 3
Size: 301568 Color: 0

Bin 590: 12 of cap free
Amount of items: 3
Items: 
Size: 365852 Color: 4
Size: 338718 Color: 1
Size: 295419 Color: 2

Bin 591: 12 of cap free
Amount of items: 3
Items: 
Size: 369642 Color: 4
Size: 319866 Color: 3
Size: 310481 Color: 2

Bin 592: 13 of cap free
Amount of items: 3
Items: 
Size: 358999 Color: 4
Size: 328619 Color: 2
Size: 312370 Color: 3

Bin 593: 15 of cap free
Amount of items: 3
Items: 
Size: 365187 Color: 3
Size: 352531 Color: 0
Size: 282268 Color: 3

Bin 594: 15 of cap free
Amount of items: 3
Items: 
Size: 366021 Color: 0
Size: 351652 Color: 0
Size: 282313 Color: 2

Bin 595: 15 of cap free
Amount of items: 3
Items: 
Size: 373141 Color: 2
Size: 325612 Color: 2
Size: 301233 Color: 0

Bin 596: 15 of cap free
Amount of items: 3
Items: 
Size: 375811 Color: 4
Size: 354233 Color: 3
Size: 269942 Color: 0

Bin 597: 16 of cap free
Amount of items: 3
Items: 
Size: 363511 Color: 2
Size: 353110 Color: 3
Size: 283364 Color: 1

Bin 598: 16 of cap free
Amount of items: 3
Items: 
Size: 365155 Color: 0
Size: 317677 Color: 1
Size: 317153 Color: 0

Bin 599: 16 of cap free
Amount of items: 3
Items: 
Size: 368292 Color: 0
Size: 360634 Color: 2
Size: 271059 Color: 1

Bin 600: 17 of cap free
Amount of items: 3
Items: 
Size: 369164 Color: 4
Size: 347734 Color: 0
Size: 283086 Color: 0

Bin 601: 18 of cap free
Amount of items: 3
Items: 
Size: 368941 Color: 0
Size: 355471 Color: 4
Size: 275571 Color: 2

Bin 602: 18 of cap free
Amount of items: 3
Items: 
Size: 371252 Color: 4
Size: 339283 Color: 2
Size: 289448 Color: 4

Bin 603: 19 of cap free
Amount of items: 3
Items: 
Size: 357786 Color: 1
Size: 340216 Color: 2
Size: 301980 Color: 0

Bin 604: 20 of cap free
Amount of items: 3
Items: 
Size: 375689 Color: 1
Size: 368449 Color: 0
Size: 255843 Color: 3

Bin 605: 22 of cap free
Amount of items: 3
Items: 
Size: 356074 Color: 3
Size: 329009 Color: 2
Size: 314896 Color: 3

Bin 606: 23 of cap free
Amount of items: 3
Items: 
Size: 363639 Color: 2
Size: 333083 Color: 1
Size: 303256 Color: 4

Bin 607: 23 of cap free
Amount of items: 3
Items: 
Size: 365569 Color: 4
Size: 336215 Color: 0
Size: 298194 Color: 1

Bin 608: 24 of cap free
Amount of items: 3
Items: 
Size: 359829 Color: 3
Size: 356832 Color: 4
Size: 283316 Color: 1

Bin 609: 24 of cap free
Amount of items: 3
Items: 
Size: 362335 Color: 0
Size: 330967 Color: 0
Size: 306675 Color: 1

Bin 610: 30 of cap free
Amount of items: 3
Items: 
Size: 361302 Color: 0
Size: 336729 Color: 1
Size: 301940 Color: 2

Bin 611: 32 of cap free
Amount of items: 3
Items: 
Size: 361736 Color: 3
Size: 350972 Color: 4
Size: 287261 Color: 4

Bin 612: 34 of cap free
Amount of items: 3
Items: 
Size: 359038 Color: 3
Size: 337045 Color: 4
Size: 303884 Color: 1

Bin 613: 34 of cap free
Amount of items: 3
Items: 
Size: 359893 Color: 2
Size: 354085 Color: 3
Size: 285989 Color: 0

Bin 614: 37 of cap free
Amount of items: 3
Items: 
Size: 360180 Color: 3
Size: 333062 Color: 1
Size: 306722 Color: 0

Bin 615: 40 of cap free
Amount of items: 3
Items: 
Size: 361715 Color: 0
Size: 345200 Color: 2
Size: 293046 Color: 1

Bin 616: 48 of cap free
Amount of items: 3
Items: 
Size: 359526 Color: 0
Size: 321914 Color: 2
Size: 318513 Color: 0

Bin 617: 49 of cap free
Amount of items: 3
Items: 
Size: 362675 Color: 1
Size: 338549 Color: 2
Size: 298728 Color: 4

Bin 618: 55 of cap free
Amount of items: 3
Items: 
Size: 360458 Color: 3
Size: 358554 Color: 4
Size: 280934 Color: 2

Bin 619: 58 of cap free
Amount of items: 3
Items: 
Size: 359713 Color: 4
Size: 345495 Color: 1
Size: 294735 Color: 0

Bin 620: 60 of cap free
Amount of items: 3
Items: 
Size: 354015 Color: 0
Size: 331376 Color: 1
Size: 314550 Color: 3

Bin 621: 63 of cap free
Amount of items: 3
Items: 
Size: 351359 Color: 3
Size: 345337 Color: 0
Size: 303242 Color: 0

Bin 622: 76 of cap free
Amount of items: 3
Items: 
Size: 358402 Color: 4
Size: 354486 Color: 2
Size: 287037 Color: 1

Bin 623: 81 of cap free
Amount of items: 3
Items: 
Size: 364106 Color: 4
Size: 330069 Color: 4
Size: 305745 Color: 1

Bin 624: 88 of cap free
Amount of items: 3
Items: 
Size: 345328 Color: 3
Size: 330516 Color: 0
Size: 324069 Color: 4

Bin 625: 97 of cap free
Amount of items: 3
Items: 
Size: 352998 Color: 1
Size: 333847 Color: 4
Size: 313059 Color: 2

Bin 626: 99 of cap free
Amount of items: 3
Items: 
Size: 355588 Color: 1
Size: 339378 Color: 3
Size: 304936 Color: 4

Bin 627: 123 of cap free
Amount of items: 3
Items: 
Size: 350418 Color: 3
Size: 342334 Color: 1
Size: 307126 Color: 4

Bin 628: 152 of cap free
Amount of items: 3
Items: 
Size: 356887 Color: 0
Size: 346292 Color: 0
Size: 296670 Color: 1

Bin 629: 160 of cap free
Amount of items: 3
Items: 
Size: 355724 Color: 0
Size: 324065 Color: 1
Size: 320052 Color: 2

Bin 630: 171 of cap free
Amount of items: 3
Items: 
Size: 347349 Color: 4
Size: 329961 Color: 3
Size: 322520 Color: 2

Bin 631: 185 of cap free
Amount of items: 3
Items: 
Size: 356931 Color: 1
Size: 323288 Color: 0
Size: 319597 Color: 4

Bin 632: 194 of cap free
Amount of items: 3
Items: 
Size: 354712 Color: 4
Size: 323192 Color: 1
Size: 321903 Color: 2

Bin 633: 218 of cap free
Amount of items: 3
Items: 
Size: 355496 Color: 1
Size: 337097 Color: 3
Size: 307190 Color: 0

Bin 634: 238 of cap free
Amount of items: 3
Items: 
Size: 337044 Color: 3
Size: 332366 Color: 0
Size: 330353 Color: 0

Bin 635: 334 of cap free
Amount of items: 3
Items: 
Size: 354371 Color: 0
Size: 331867 Color: 1
Size: 313429 Color: 2

Bin 636: 434 of cap free
Amount of items: 3
Items: 
Size: 354715 Color: 1
Size: 335427 Color: 1
Size: 309425 Color: 4

Bin 637: 463 of cap free
Amount of items: 3
Items: 
Size: 356095 Color: 1
Size: 326019 Color: 4
Size: 317424 Color: 2

Bin 638: 494 of cap free
Amount of items: 3
Items: 
Size: 356650 Color: 2
Size: 331764 Color: 2
Size: 311093 Color: 1

Bin 639: 578 of cap free
Amount of items: 3
Items: 
Size: 355622 Color: 0
Size: 349182 Color: 1
Size: 294619 Color: 2

Bin 640: 664 of cap free
Amount of items: 3
Items: 
Size: 345949 Color: 4
Size: 335411 Color: 4
Size: 317977 Color: 0

Bin 641: 820 of cap free
Amount of items: 3
Items: 
Size: 345635 Color: 4
Size: 326892 Color: 2
Size: 326654 Color: 2

Bin 642: 1069 of cap free
Amount of items: 3
Items: 
Size: 338411 Color: 4
Size: 332574 Color: 2
Size: 327947 Color: 0

Bin 643: 1120 of cap free
Amount of items: 3
Items: 
Size: 341922 Color: 3
Size: 330345 Color: 1
Size: 326614 Color: 4

Bin 644: 1223 of cap free
Amount of items: 2
Items: 
Size: 499545 Color: 2
Size: 499233 Color: 0

Bin 645: 1301 of cap free
Amount of items: 3
Items: 
Size: 336804 Color: 4
Size: 331600 Color: 2
Size: 330296 Color: 0

Bin 646: 1476 of cap free
Amount of items: 3
Items: 
Size: 341282 Color: 3
Size: 329038 Color: 3
Size: 328205 Color: 0

Bin 647: 2113 of cap free
Amount of items: 2
Items: 
Size: 499184 Color: 2
Size: 498704 Color: 4

Bin 648: 3113 of cap free
Amount of items: 2
Items: 
Size: 498455 Color: 0
Size: 498433 Color: 1

Bin 649: 7565 of cap free
Amount of items: 3
Items: 
Size: 353711 Color: 2
Size: 329301 Color: 3
Size: 309424 Color: 4

Bin 650: 11522 of cap free
Amount of items: 3
Items: 
Size: 353581 Color: 2
Size: 352685 Color: 0
Size: 282213 Color: 3

Bin 651: 16758 of cap free
Amount of items: 3
Items: 
Size: 352852 Color: 1
Size: 349357 Color: 3
Size: 281034 Color: 4

Bin 652: 25065 of cap free
Amount of items: 3
Items: 
Size: 352563 Color: 1
Size: 350482 Color: 2
Size: 271891 Color: 3

Bin 653: 26699 of cap free
Amount of items: 3
Items: 
Size: 352301 Color: 1
Size: 352050 Color: 1
Size: 268951 Color: 3

Bin 654: 31603 of cap free
Amount of items: 3
Items: 
Size: 351634 Color: 1
Size: 350493 Color: 0
Size: 266271 Color: 2

Bin 655: 40786 of cap free
Amount of items: 3
Items: 
Size: 348081 Color: 0
Size: 346899 Color: 3
Size: 264235 Color: 1

Bin 656: 44085 of cap free
Amount of items: 3
Items: 
Size: 348487 Color: 2
Size: 343465 Color: 3
Size: 263964 Color: 4

Bin 657: 45040 of cap free
Amount of items: 3
Items: 
Size: 347973 Color: 2
Size: 343742 Color: 0
Size: 263246 Color: 4

Bin 658: 45489 of cap free
Amount of items: 3
Items: 
Size: 347350 Color: 0
Size: 343976 Color: 4
Size: 263186 Color: 2

Bin 659: 45568 of cap free
Amount of items: 3
Items: 
Size: 347250 Color: 1
Size: 346545 Color: 1
Size: 260638 Color: 4

Bin 660: 48450 of cap free
Amount of items: 3
Items: 
Size: 347322 Color: 2
Size: 341249 Color: 0
Size: 262980 Color: 1

Bin 661: 51504 of cap free
Amount of items: 3
Items: 
Size: 345752 Color: 2
Size: 340486 Color: 0
Size: 262259 Color: 1

Bin 662: 53614 of cap free
Amount of items: 3
Items: 
Size: 344653 Color: 1
Size: 341169 Color: 3
Size: 260565 Color: 4

Bin 663: 61582 of cap free
Amount of items: 3
Items: 
Size: 341307 Color: 1
Size: 340609 Color: 3
Size: 256503 Color: 0

Bin 664: 63477 of cap free
Amount of items: 3
Items: 
Size: 338523 Color: 4
Size: 337730 Color: 2
Size: 260271 Color: 2

Bin 665: 66882 of cap free
Amount of items: 3
Items: 
Size: 337647 Color: 3
Size: 337145 Color: 4
Size: 258327 Color: 2

Bin 666: 69444 of cap free
Amount of items: 3
Items: 
Size: 337301 Color: 1
Size: 335262 Color: 4
Size: 257994 Color: 3

Bin 667: 69854 of cap free
Amount of items: 3
Items: 
Size: 337725 Color: 3
Size: 332693 Color: 0
Size: 259729 Color: 1

Bin 668: 156347 of cap free
Amount of items: 3
Items: 
Size: 334775 Color: 4
Size: 257345 Color: 2
Size: 251534 Color: 3

Total size: 667000667
Total free space: 1000001

