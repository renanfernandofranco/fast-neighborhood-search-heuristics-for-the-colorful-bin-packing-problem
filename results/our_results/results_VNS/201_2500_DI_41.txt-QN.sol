Capicity Bin: 2052
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1485 Color: 162
Size: 389 Color: 103
Size: 178 Color: 70

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1493 Color: 163
Size: 473 Color: 112
Size: 86 Color: 46

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 166
Size: 385 Color: 102
Size: 124 Color: 59

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 170
Size: 357 Color: 99
Size: 104 Color: 52

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 171
Size: 382 Color: 101
Size: 72 Color: 38

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 178
Size: 262 Color: 84
Size: 112 Color: 54

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 179
Size: 362 Color: 100
Size: 4 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 180
Size: 287 Color: 90
Size: 64 Color: 31

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 184
Size: 174 Color: 69
Size: 152 Color: 66

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 185
Size: 271 Color: 87
Size: 52 Color: 24

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 188
Size: 218 Color: 73
Size: 92 Color: 47

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 189
Size: 239 Color: 80
Size: 68 Color: 32

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1772 Color: 192
Size: 240 Color: 81
Size: 40 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 195
Size: 219 Color: 74
Size: 42 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 197
Size: 148 Color: 64
Size: 106 Color: 53

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 199
Size: 187 Color: 71
Size: 36 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 200
Size: 148 Color: 65
Size: 62 Color: 30

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 201
Size: 136 Color: 63
Size: 70 Color: 35

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 168
Size: 481 Color: 114

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1709 Color: 182
Size: 342 Color: 97

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1737 Color: 187
Size: 314 Color: 94

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 196
Size: 257 Color: 83

Bin 23: 2 of cap free
Amount of items: 2
Items: 
Size: 1288 Color: 149
Size: 762 Color: 132

Bin 24: 2 of cap free
Amount of items: 2
Items: 
Size: 1583 Color: 169
Size: 467 Color: 110

Bin 25: 2 of cap free
Amount of items: 2
Items: 
Size: 1625 Color: 174
Size: 425 Color: 107

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 177
Size: 313 Color: 93
Size: 60 Color: 29

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1704 Color: 181
Size: 346 Color: 98

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1734 Color: 186
Size: 316 Color: 95

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 190
Size: 293 Color: 92
Size: 4 Color: 1

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1799 Color: 198
Size: 251 Color: 82

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1478 Color: 161
Size: 571 Color: 120

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 1535 Color: 165
Size: 514 Color: 116

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1618 Color: 173
Size: 431 Color: 109

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1718 Color: 183
Size: 331 Color: 96

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1767 Color: 191
Size: 282 Color: 89

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1775 Color: 193
Size: 274 Color: 88

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1783 Color: 194
Size: 266 Color: 86

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1359 Color: 152
Size: 649 Color: 125
Size: 40 Color: 11

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 158
Size: 593 Color: 123
Size: 32 Color: 5

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1546 Color: 167
Size: 502 Color: 115

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1657 Color: 176
Size: 391 Color: 104

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 1153 Color: 143
Size: 845 Color: 134
Size: 48 Color: 17

Bin 43: 7 of cap free
Amount of items: 2
Items: 
Size: 1353 Color: 151
Size: 692 Color: 129

Bin 44: 7 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 159
Size: 583 Color: 122
Size: 24 Color: 4

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 1617 Color: 172
Size: 428 Color: 108

Bin 46: 8 of cap free
Amount of items: 3
Items: 
Size: 1257 Color: 146
Size: 741 Color: 130
Size: 46 Color: 14

Bin 47: 10 of cap free
Amount of items: 4
Items: 
Size: 1477 Color: 160
Size: 525 Color: 117
Size: 24 Color: 3
Size: 16 Color: 2

Bin 48: 11 of cap free
Amount of items: 3
Items: 
Size: 1138 Color: 142
Size: 855 Color: 136
Size: 48 Color: 18

Bin 49: 12 of cap free
Amount of items: 2
Items: 
Size: 1638 Color: 175
Size: 402 Color: 105

Bin 50: 15 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 144
Size: 828 Color: 133
Size: 46 Color: 16

Bin 51: 15 of cap free
Amount of items: 3
Items: 
Size: 1267 Color: 147
Size: 478 Color: 113
Size: 292 Color: 91

Bin 52: 15 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 155
Size: 622 Color: 124
Size: 40 Color: 10

Bin 53: 15 of cap free
Amount of items: 2
Items: 
Size: 1506 Color: 164
Size: 531 Color: 118

Bin 54: 16 of cap free
Amount of items: 3
Items: 
Size: 1306 Color: 150
Size: 686 Color: 128
Size: 44 Color: 13

Bin 55: 19 of cap free
Amount of items: 2
Items: 
Size: 1370 Color: 154
Size: 663 Color: 127

Bin 56: 20 of cap free
Amount of items: 20
Items: 
Size: 170 Color: 68
Size: 168 Color: 67
Size: 132 Color: 62
Size: 130 Color: 61
Size: 128 Color: 60
Size: 124 Color: 58
Size: 116 Color: 57
Size: 114 Color: 56
Size: 114 Color: 55
Size: 100 Color: 51
Size: 96 Color: 50
Size: 94 Color: 49
Size: 76 Color: 40
Size: 76 Color: 39
Size: 72 Color: 37
Size: 72 Color: 36
Size: 68 Color: 34
Size: 68 Color: 33
Size: 58 Color: 28
Size: 56 Color: 27

Bin 57: 21 of cap free
Amount of items: 3
Items: 
Size: 1420 Color: 157
Size: 579 Color: 121
Size: 32 Color: 6

Bin 58: 22 of cap free
Amount of items: 7
Items: 
Size: 1027 Color: 138
Size: 228 Color: 77
Size: 228 Color: 76
Size: 225 Color: 75
Size: 214 Color: 72
Size: 56 Color: 26
Size: 52 Color: 25

Bin 59: 22 of cap free
Amount of items: 4
Items: 
Size: 1031 Color: 140
Size: 897 Color: 137
Size: 52 Color: 21
Size: 50 Color: 20

Bin 60: 26 of cap free
Amount of items: 2
Items: 
Size: 1275 Color: 148
Size: 751 Color: 131

Bin 61: 27 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 156
Size: 570 Color: 119
Size: 40 Color: 9

Bin 62: 30 of cap free
Amount of items: 2
Items: 
Size: 1367 Color: 153
Size: 655 Color: 126

Bin 63: 36 of cap free
Amount of items: 6
Items: 
Size: 1030 Color: 139
Size: 422 Color: 106
Size: 231 Color: 79
Size: 229 Color: 78
Size: 52 Color: 23
Size: 52 Color: 22

Bin 64: 39 of cap free
Amount of items: 3
Items: 
Size: 1111 Color: 141
Size: 854 Color: 135
Size: 48 Color: 19

Bin 65: 41 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 145
Size: 472 Color: 111
Size: 263 Color: 85
Size: 46 Color: 15

Bin 66: 1548 of cap free
Amount of items: 6
Items: 
Size: 94 Color: 48
Size: 84 Color: 45
Size: 84 Color: 44
Size: 84 Color: 43
Size: 80 Color: 42
Size: 78 Color: 41

Total size: 133380
Total free space: 2052

