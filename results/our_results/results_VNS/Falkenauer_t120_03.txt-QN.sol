Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 107
Size: 304 Color: 59
Size: 254 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 93
Size: 321 Color: 66
Size: 273 Color: 29

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 74
Size: 329 Color: 71
Size: 323 Color: 67

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 102
Size: 316 Color: 62
Size: 252 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 85
Size: 354 Color: 79
Size: 265 Color: 23

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 265 Color: 22
Size: 255 Color: 13
Size: 480 Color: 117

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 106
Size: 304 Color: 60
Size: 257 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 84
Size: 369 Color: 82
Size: 257 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 91
Size: 320 Color: 65
Size: 277 Color: 35

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 96
Size: 299 Color: 54
Size: 289 Color: 47

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 111
Size: 269 Color: 26
Size: 268 Color: 25

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 103
Size: 290 Color: 48
Size: 276 Color: 33

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 113
Size: 276 Color: 32
Size: 254 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 118
Size: 251 Color: 3
Size: 250 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 114
Size: 274 Color: 30
Size: 255 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 115
Size: 277 Color: 34
Size: 250 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 100
Size: 328 Color: 70
Size: 254 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 86
Size: 354 Color: 77
Size: 256 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 90
Size: 324 Color: 68
Size: 279 Color: 39

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 98
Size: 298 Color: 52
Size: 287 Color: 45

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 97
Size: 336 Color: 72
Size: 252 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 104
Size: 285 Color: 43
Size: 278 Color: 37

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 88
Size: 306 Color: 61
Size: 301 Color: 58

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 105
Size: 296 Color: 51
Size: 265 Color: 21

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 80
Size: 351 Color: 75
Size: 285 Color: 42

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 116
Size: 271 Color: 27
Size: 253 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 89
Size: 324 Color: 69
Size: 280 Color: 40

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 99
Size: 300 Color: 55
Size: 282 Color: 41

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 119
Size: 251 Color: 4
Size: 250 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 92
Size: 317 Color: 64
Size: 278 Color: 38

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 83
Size: 366 Color: 81
Size: 262 Color: 20

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 95
Size: 301 Color: 56
Size: 288 Color: 46

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 108
Size: 301 Color: 57
Size: 255 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 101
Size: 294 Color: 49
Size: 287 Color: 44

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 109
Size: 299 Color: 53
Size: 254 Color: 11

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 87
Size: 346 Color: 73
Size: 261 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 110
Size: 277 Color: 36
Size: 266 Color: 24

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 112
Size: 272 Color: 28
Size: 261 Color: 19

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 78
Size: 351 Color: 76
Size: 295 Color: 50

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 94
Size: 316 Color: 63
Size: 274 Color: 31

Total size: 40000
Total free space: 0

