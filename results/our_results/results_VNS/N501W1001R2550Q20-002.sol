Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 13
Size: 275 Color: 11
Size: 265 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 270 Color: 7
Size: 267 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 2
Size: 338 Color: 13
Size: 316 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 15
Size: 317 Color: 13
Size: 250 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 4
Size: 259 Color: 11
Size: 251 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 14
Size: 360 Color: 15
Size: 261 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 15
Size: 340 Color: 6
Size: 316 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 3
Size: 294 Color: 16
Size: 259 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 342 Color: 5
Size: 276 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 12
Size: 315 Color: 5
Size: 299 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 4
Size: 327 Color: 19
Size: 301 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 9
Size: 293 Color: 10
Size: 279 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 5
Size: 265 Color: 15
Size: 255 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 312 Color: 3
Size: 298 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 18
Size: 327 Color: 11
Size: 282 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 12
Size: 311 Color: 16
Size: 310 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 374 Color: 11
Size: 252 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 10
Size: 361 Color: 0
Size: 257 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 350 Color: 11
Size: 250 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 18
Size: 304 Color: 2
Size: 262 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7
Size: 345 Color: 19
Size: 281 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 17
Size: 299 Color: 14
Size: 278 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7
Size: 356 Color: 13
Size: 262 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 17
Size: 323 Color: 9
Size: 257 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 296 Color: 3
Size: 287 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 16
Size: 319 Color: 9
Size: 296 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 18
Size: 309 Color: 15
Size: 251 Color: 7

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 11
Size: 346 Color: 13
Size: 274 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 12
Size: 290 Color: 9
Size: 272 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 3
Size: 274 Color: 16
Size: 257 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 12
Size: 264 Color: 15
Size: 262 Color: 11

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 0
Size: 271 Color: 19
Size: 264 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 18
Size: 347 Color: 18
Size: 256 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 2
Size: 335 Color: 1
Size: 277 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 11
Size: 361 Color: 15
Size: 278 Color: 6

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 3
Size: 280 Color: 2
Size: 257 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 12
Size: 352 Color: 10
Size: 283 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 12
Size: 339 Color: 12
Size: 252 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 294 Color: 14
Size: 267 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 11
Size: 254 Color: 15
Size: 251 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 14
Size: 326 Color: 10
Size: 269 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 6
Size: 258 Color: 18
Size: 251 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 12
Size: 316 Color: 13
Size: 279 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 8
Size: 350 Color: 10
Size: 266 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 2
Size: 289 Color: 7
Size: 250 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 14
Size: 257 Color: 8
Size: 251 Color: 7

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 16
Size: 362 Color: 9
Size: 271 Color: 9

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7
Size: 338 Color: 1
Size: 285 Color: 15

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 6
Size: 281 Color: 2
Size: 256 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 11
Size: 322 Color: 7
Size: 288 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 16
Size: 340 Color: 16
Size: 318 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 11
Size: 268 Color: 6
Size: 261 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 326 Color: 9
Size: 297 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 13
Size: 258 Color: 2
Size: 256 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 18
Size: 349 Color: 13
Size: 271 Color: 13

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 310 Color: 13
Size: 288 Color: 7

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 15
Size: 307 Color: 2
Size: 293 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 11
Size: 310 Color: 19
Size: 298 Color: 15

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 9
Size: 282 Color: 16
Size: 282 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 4
Size: 279 Color: 15
Size: 266 Color: 13

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 11
Size: 335 Color: 13
Size: 257 Color: 8

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 12
Size: 353 Color: 7
Size: 251 Color: 8

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 19
Size: 339 Color: 14
Size: 310 Color: 6

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7
Size: 331 Color: 16
Size: 288 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 5
Size: 289 Color: 11
Size: 265 Color: 12

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 317 Color: 0
Size: 276 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 7
Size: 337 Color: 6
Size: 327 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 17
Size: 331 Color: 13
Size: 322 Color: 5

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 261 Color: 12
Size: 254 Color: 15

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 314 Color: 1
Size: 266 Color: 14

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 13
Size: 270 Color: 18
Size: 257 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 13
Size: 334 Color: 4
Size: 265 Color: 17

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 10
Size: 332 Color: 14
Size: 295 Color: 7

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 8
Size: 316 Color: 0
Size: 307 Color: 11

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 316 Color: 15
Size: 294 Color: 18

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 13
Size: 296 Color: 16
Size: 295 Color: 19

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9
Size: 297 Color: 13
Size: 260 Color: 18

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 16
Size: 335 Color: 1
Size: 296 Color: 18

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 6
Size: 315 Color: 12
Size: 288 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 4
Size: 291 Color: 8
Size: 283 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 336 Color: 9
Size: 306 Color: 18

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 6
Size: 348 Color: 12
Size: 276 Color: 12

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 360 Color: 16
Size: 265 Color: 6

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 6
Size: 335 Color: 14
Size: 268 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 0
Size: 319 Color: 3
Size: 275 Color: 11

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 18
Size: 329 Color: 7
Size: 321 Color: 11

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 5
Size: 295 Color: 10
Size: 260 Color: 8

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 12
Size: 338 Color: 17
Size: 251 Color: 2

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 9
Size: 373 Color: 8
Size: 250 Color: 14

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 13
Size: 256 Color: 7
Size: 383 Color: 14

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 8
Size: 254 Color: 13
Size: 250 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 14
Size: 253 Color: 12
Size: 251 Color: 7

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 9
Size: 367 Color: 10
Size: 251 Color: 9

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 347 Color: 15
Size: 275 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 5
Size: 331 Color: 6
Size: 306 Color: 6

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 19
Size: 282 Color: 4
Size: 260 Color: 15

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 9
Size: 321 Color: 5
Size: 296 Color: 10

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 0
Size: 327 Color: 15
Size: 274 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 9
Size: 330 Color: 16
Size: 254 Color: 2

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 8
Size: 334 Color: 3
Size: 282 Color: 15

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 18
Size: 260 Color: 6
Size: 256 Color: 10

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 3
Size: 273 Color: 3
Size: 265 Color: 13

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 15
Size: 252 Color: 12
Size: 250 Color: 7

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 18
Size: 322 Color: 11
Size: 283 Color: 5

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 4
Size: 258 Color: 9
Size: 251 Color: 6

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 9
Size: 285 Color: 4
Size: 276 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8
Size: 285 Color: 18
Size: 276 Color: 17

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 343 Color: 11
Size: 284 Color: 9

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 328 Color: 17
Size: 304 Color: 14

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 17
Size: 296 Color: 6
Size: 268 Color: 15

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 3
Size: 293 Color: 7
Size: 271 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 17
Size: 348 Color: 0
Size: 290 Color: 8

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 7
Size: 347 Color: 14
Size: 284 Color: 9

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 3
Size: 309 Color: 16
Size: 255 Color: 10

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 11
Size: 369 Color: 14
Size: 259 Color: 12

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 308 Color: 10
Size: 288 Color: 4

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 15
Size: 347 Color: 13
Size: 305 Color: 8

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 19
Size: 349 Color: 4
Size: 301 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 17
Size: 287 Color: 7
Size: 269 Color: 3

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 9
Size: 320 Color: 5
Size: 312 Color: 15

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 19
Size: 269 Color: 2
Size: 265 Color: 9

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 9
Size: 254 Color: 2
Size: 251 Color: 12

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8
Size: 301 Color: 15
Size: 275 Color: 17

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 18
Size: 349 Color: 12
Size: 265 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 2
Size: 304 Color: 14
Size: 275 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 6
Size: 306 Color: 7
Size: 298 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 15
Size: 251 Color: 13
Size: 250 Color: 8

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 347 Color: 12
Size: 262 Color: 18

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 13
Size: 316 Color: 16
Size: 297 Color: 9

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 5
Size: 293 Color: 10
Size: 267 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 8
Size: 338 Color: 8
Size: 301 Color: 14

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 7
Size: 299 Color: 17
Size: 296 Color: 5

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 14
Size: 320 Color: 1
Size: 318 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 4
Size: 347 Color: 15
Size: 290 Color: 11

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 17
Size: 340 Color: 17
Size: 295 Color: 4

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 5
Size: 328 Color: 6
Size: 306 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 2
Size: 328 Color: 3
Size: 304 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 360 Color: 12
Size: 275 Color: 6

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 331 Color: 11
Size: 295 Color: 6

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 6
Size: 354 Color: 19
Size: 282 Color: 13

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 5
Size: 328 Color: 2
Size: 291 Color: 18

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 342 Color: 2
Size: 271 Color: 4

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 10
Size: 318 Color: 8
Size: 288 Color: 6

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 14
Size: 336 Color: 10
Size: 295 Color: 17

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 252 Color: 6
Size: 252 Color: 3

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 19
Size: 331 Color: 17
Size: 302 Color: 19

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 10
Size: 319 Color: 6
Size: 269 Color: 17

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 12
Size: 325 Color: 11
Size: 261 Color: 8

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 316 Color: 19
Size: 267 Color: 15

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 10
Size: 304 Color: 14
Size: 274 Color: 11

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 316 Color: 16
Size: 259 Color: 11

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 16
Size: 310 Color: 8
Size: 258 Color: 5

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 309 Color: 4
Size: 255 Color: 18

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 15
Size: 281 Color: 0
Size: 271 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 2
Size: 276 Color: 9
Size: 272 Color: 13

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 16
Size: 284 Color: 4
Size: 262 Color: 6

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9
Size: 274 Color: 4
Size: 266 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 2
Size: 318 Color: 17
Size: 282 Color: 6

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 10
Size: 283 Color: 5
Size: 256 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 270 Color: 8
Size: 266 Color: 6

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 7
Size: 272 Color: 14
Size: 263 Color: 13

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 3
Size: 270 Color: 13
Size: 256 Color: 12

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 266 Color: 1
Size: 256 Color: 13

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 12
Size: 261 Color: 4
Size: 255 Color: 4

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 10
Size: 263 Color: 13
Size: 252 Color: 9

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 8
Size: 260 Color: 5
Size: 255 Color: 9

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 15
Size: 353 Color: 3
Size: 275 Color: 12

Total size: 167167
Total free space: 0

