Capicity Bin: 7928
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5734 Color: 314
Size: 1142 Color: 187
Size: 1052 Color: 179

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6203 Color: 329
Size: 1439 Color: 212
Size: 286 Color: 80

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6231 Color: 331
Size: 1415 Color: 207
Size: 282 Color: 79

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 332
Size: 1436 Color: 211
Size: 258 Color: 72

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6235 Color: 333
Size: 1429 Color: 209
Size: 264 Color: 74

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6375 Color: 338
Size: 1425 Color: 208
Size: 128 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 340
Size: 1414 Color: 206
Size: 116 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6399 Color: 341
Size: 941 Color: 168
Size: 588 Color: 127

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6414 Color: 344
Size: 866 Color: 159
Size: 648 Color: 131

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6423 Color: 345
Size: 1295 Color: 200
Size: 210 Color: 52

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6430 Color: 346
Size: 1098 Color: 185
Size: 400 Color: 102

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6431 Color: 347
Size: 1055 Color: 180
Size: 442 Color: 106

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6533 Color: 351
Size: 903 Color: 165
Size: 492 Color: 115

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 353
Size: 1159 Color: 189
Size: 214 Color: 55

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6558 Color: 354
Size: 1208 Color: 191
Size: 162 Color: 28

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 364
Size: 966 Color: 171
Size: 224 Color: 58

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6743 Color: 365
Size: 989 Color: 174
Size: 196 Color: 47

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6746 Color: 366
Size: 678 Color: 138
Size: 504 Color: 118

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 369
Size: 692 Color: 139
Size: 440 Color: 105

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6797 Color: 370
Size: 871 Color: 161
Size: 260 Color: 73

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6799 Color: 371
Size: 815 Color: 152
Size: 314 Color: 88

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6885 Color: 377
Size: 855 Color: 156
Size: 188 Color: 43

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6890 Color: 379
Size: 536 Color: 121
Size: 502 Color: 117

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6896 Color: 380
Size: 874 Color: 163
Size: 158 Color: 25

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6917 Color: 383
Size: 831 Color: 154
Size: 180 Color: 37

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6919 Color: 384
Size: 857 Color: 157
Size: 152 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 386
Size: 790 Color: 150
Size: 192 Color: 44

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6951 Color: 387
Size: 797 Color: 151
Size: 180 Color: 39

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6989 Color: 390
Size: 783 Color: 149
Size: 156 Color: 21

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6991 Color: 391
Size: 781 Color: 148
Size: 156 Color: 20

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6996 Color: 392
Size: 544 Color: 122
Size: 388 Color: 99

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7005 Color: 393
Size: 771 Color: 146
Size: 152 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7007 Color: 394
Size: 769 Color: 145
Size: 152 Color: 17

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 395
Size: 758 Color: 143
Size: 156 Color: 24

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 396
Size: 528 Color: 120
Size: 372 Color: 97

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 397
Size: 702 Color: 140
Size: 180 Color: 38

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 399
Size: 670 Color: 137
Size: 168 Color: 30

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7100 Color: 400
Size: 632 Color: 130
Size: 196 Color: 45

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7118 Color: 401
Size: 560 Color: 123
Size: 250 Color: 69

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7126 Color: 402
Size: 574 Color: 126
Size: 228 Color: 61

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 4917 Color: 293
Size: 2802 Color: 256
Size: 208 Color: 49

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6243 Color: 334
Size: 862 Color: 158
Size: 822 Color: 153

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6358 Color: 337
Size: 1405 Color: 204
Size: 164 Color: 29

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6383 Color: 339
Size: 1208 Color: 192
Size: 336 Color: 91

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6404 Color: 342
Size: 1411 Color: 205
Size: 112 Color: 5

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6496 Color: 350
Size: 1431 Color: 210

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6847 Color: 375
Size: 1080 Color: 183

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6882 Color: 376
Size: 1045 Color: 178

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6887 Color: 378
Size: 624 Color: 129
Size: 416 Color: 103

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 6933 Color: 385
Size: 994 Color: 175

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 7058 Color: 398
Size: 869 Color: 160

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 6037 Color: 323
Size: 1833 Color: 227
Size: 56 Color: 1

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6406 Color: 343
Size: 1520 Color: 215

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6476 Color: 349
Size: 1450 Color: 213

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6842 Color: 373
Size: 1084 Color: 184

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 382
Size: 1018 Color: 176

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 6663 Color: 360
Size: 1262 Color: 197

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 6675 Color: 362
Size: 1250 Color: 195

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 6982 Color: 389
Size: 943 Color: 169

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 5148 Color: 298
Size: 2604 Color: 252
Size: 172 Color: 36

Bin 61: 4 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 315
Size: 1618 Color: 220
Size: 564 Color: 124

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 6006 Color: 322
Size: 1822 Color: 226
Size: 96 Color: 2

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 6190 Color: 328
Size: 1574 Color: 218
Size: 160 Color: 27

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 6614 Color: 356
Size: 1310 Color: 201

Bin 65: 5 of cap free
Amount of items: 7
Items: 
Size: 3965 Color: 273
Size: 986 Color: 173
Size: 984 Color: 172
Size: 940 Color: 167
Size: 456 Color: 110
Size: 304 Color: 84
Size: 288 Color: 83

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 6180 Color: 327
Size: 1743 Color: 223

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 361
Size: 1255 Color: 196

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 6710 Color: 363
Size: 1212 Color: 193

Bin 69: 7 of cap free
Amount of items: 3
Items: 
Size: 4477 Color: 282
Size: 3204 Color: 263
Size: 240 Color: 64

Bin 70: 7 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 335
Size: 1645 Color: 221

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 6643 Color: 358
Size: 1278 Color: 199

Bin 72: 7 of cap free
Amount of items: 2
Items: 
Size: 6898 Color: 381
Size: 1023 Color: 177

Bin 73: 7 of cap free
Amount of items: 2
Items: 
Size: 6973 Color: 388
Size: 948 Color: 170

Bin 74: 8 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 304
Size: 2364 Color: 244
Size: 160 Color: 26

Bin 75: 8 of cap free
Amount of items: 3
Items: 
Size: 5569 Color: 308
Size: 1571 Color: 216
Size: 780 Color: 147

Bin 76: 8 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 317
Size: 2004 Color: 234
Size: 136 Color: 11

Bin 77: 8 of cap free
Amount of items: 3
Items: 
Size: 5837 Color: 319
Size: 1951 Color: 232
Size: 132 Color: 9

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 348
Size: 1460 Color: 214

Bin 79: 9 of cap free
Amount of items: 2
Items: 
Size: 6211 Color: 330
Size: 1708 Color: 222

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 6539 Color: 352
Size: 1380 Color: 203

Bin 81: 10 of cap free
Amount of items: 3
Items: 
Size: 4636 Color: 286
Size: 3052 Color: 261
Size: 230 Color: 62

Bin 82: 10 of cap free
Amount of items: 3
Items: 
Size: 4970 Color: 294
Size: 2748 Color: 255
Size: 200 Color: 48

Bin 83: 10 of cap free
Amount of items: 3
Items: 
Size: 5386 Color: 303
Size: 2364 Color: 243
Size: 168 Color: 31

Bin 84: 12 of cap free
Amount of items: 5
Items: 
Size: 3966 Color: 274
Size: 2211 Color: 238
Size: 1059 Color: 181
Size: 392 Color: 101
Size: 288 Color: 82

Bin 85: 12 of cap free
Amount of items: 3
Items: 
Size: 5686 Color: 312
Size: 2086 Color: 236
Size: 144 Color: 13

Bin 86: 12 of cap free
Amount of items: 2
Items: 
Size: 6845 Color: 374
Size: 1071 Color: 182

Bin 87: 13 of cap free
Amount of items: 2
Items: 
Size: 6045 Color: 326
Size: 1870 Color: 230

Bin 88: 13 of cap free
Amount of items: 2
Items: 
Size: 6770 Color: 368
Size: 1145 Color: 188

Bin 89: 14 of cap free
Amount of items: 3
Items: 
Size: 4526 Color: 284
Size: 3156 Color: 262
Size: 232 Color: 63

Bin 90: 14 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 313
Size: 1572 Color: 217
Size: 616 Color: 128

Bin 91: 14 of cap free
Amount of items: 2
Items: 
Size: 6804 Color: 372
Size: 1110 Color: 186

Bin 92: 16 of cap free
Amount of items: 3
Items: 
Size: 4756 Color: 289
Size: 2936 Color: 260
Size: 220 Color: 57

Bin 93: 16 of cap free
Amount of items: 3
Items: 
Size: 5831 Color: 318
Size: 1949 Color: 231
Size: 132 Color: 10

Bin 94: 16 of cap free
Amount of items: 3
Items: 
Size: 6042 Color: 324
Size: 1838 Color: 228
Size: 32 Color: 0

Bin 95: 16 of cap free
Amount of items: 2
Items: 
Size: 6636 Color: 357
Size: 1276 Color: 198

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 6749 Color: 367
Size: 1163 Color: 190

Bin 97: 17 of cap free
Amount of items: 4
Items: 
Size: 5010 Color: 295
Size: 2517 Color: 250
Size: 196 Color: 46
Size: 188 Color: 42

Bin 98: 17 of cap free
Amount of items: 3
Items: 
Size: 5277 Color: 302
Size: 2466 Color: 248
Size: 168 Color: 32

Bin 99: 17 of cap free
Amount of items: 2
Items: 
Size: 5587 Color: 309
Size: 2324 Color: 242

Bin 100: 20 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 306
Size: 2284 Color: 241
Size: 156 Color: 22

Bin 101: 20 of cap free
Amount of items: 2
Items: 
Size: 6598 Color: 355
Size: 1310 Color: 202

Bin 102: 20 of cap free
Amount of items: 2
Items: 
Size: 6659 Color: 359
Size: 1249 Color: 194

Bin 103: 22 of cap free
Amount of items: 2
Items: 
Size: 6044 Color: 325
Size: 1862 Color: 229

Bin 104: 23 of cap free
Amount of items: 3
Items: 
Size: 4812 Color: 290
Size: 2877 Color: 259
Size: 216 Color: 56

Bin 105: 23 of cap free
Amount of items: 3
Items: 
Size: 5188 Color: 299
Size: 2545 Color: 251
Size: 172 Color: 35

Bin 106: 26 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 297
Size: 2692 Color: 254
Size: 184 Color: 40

Bin 107: 31 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 307
Size: 2221 Color: 240
Size: 152 Color: 18

Bin 108: 31 of cap free
Amount of items: 2
Items: 
Size: 6320 Color: 336
Size: 1577 Color: 219

Bin 109: 32 of cap free
Amount of items: 4
Items: 
Size: 5884 Color: 320
Size: 1796 Color: 224
Size: 112 Color: 6
Size: 104 Color: 4

Bin 110: 38 of cap free
Amount of items: 4
Items: 
Size: 4092 Color: 280
Size: 3302 Color: 268
Size: 248 Color: 67
Size: 248 Color: 66

Bin 111: 42 of cap free
Amount of items: 3
Items: 
Size: 5620 Color: 311
Size: 2122 Color: 237
Size: 144 Color: 14

Bin 112: 53 of cap free
Amount of items: 3
Items: 
Size: 5269 Color: 301
Size: 2434 Color: 247
Size: 172 Color: 33

Bin 113: 57 of cap free
Amount of items: 2
Items: 
Size: 3967 Color: 275
Size: 3904 Color: 272

Bin 114: 59 of cap free
Amount of items: 2
Items: 
Size: 4566 Color: 285
Size: 3303 Color: 269

Bin 115: 59 of cap free
Amount of items: 3
Items: 
Size: 5766 Color: 316
Size: 1967 Color: 233
Size: 136 Color: 12

Bin 116: 67 of cap free
Amount of items: 3
Items: 
Size: 5263 Color: 300
Size: 2426 Color: 246
Size: 172 Color: 34

Bin 117: 67 of cap free
Amount of items: 3
Items: 
Size: 5955 Color: 321
Size: 1802 Color: 225
Size: 104 Color: 3

Bin 118: 68 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 281
Size: 3472 Color: 271
Size: 240 Color: 65

Bin 119: 77 of cap free
Amount of items: 2
Items: 
Size: 4485 Color: 283
Size: 3366 Color: 270

Bin 120: 82 of cap free
Amount of items: 3
Items: 
Size: 5018 Color: 296
Size: 2644 Color: 253
Size: 184 Color: 41

Bin 121: 91 of cap free
Amount of items: 4
Items: 
Size: 4036 Color: 279
Size: 3301 Color: 267
Size: 252 Color: 70
Size: 248 Color: 68

Bin 122: 92 of cap free
Amount of items: 4
Items: 
Size: 4909 Color: 292
Size: 2511 Color: 249
Size: 208 Color: 51
Size: 208 Color: 50

Bin 123: 112 of cap free
Amount of items: 4
Items: 
Size: 3974 Color: 277
Size: 3282 Color: 265
Size: 280 Color: 77
Size: 280 Color: 76

Bin 124: 114 of cap free
Amount of items: 4
Items: 
Size: 3990 Color: 278
Size: 3300 Color: 266
Size: 272 Color: 75
Size: 252 Color: 71

Bin 125: 117 of cap free
Amount of items: 3
Items: 
Size: 5591 Color: 310
Size: 2068 Color: 235
Size: 152 Color: 16

Bin 126: 129 of cap free
Amount of items: 3
Items: 
Size: 5426 Color: 305
Size: 2217 Color: 239
Size: 156 Color: 23

Bin 127: 133 of cap free
Amount of items: 3
Items: 
Size: 4700 Color: 288
Size: 2871 Color: 258
Size: 224 Color: 59

Bin 128: 144 of cap free
Amount of items: 4
Items: 
Size: 3972 Color: 276
Size: 3244 Color: 264
Size: 286 Color: 81
Size: 282 Color: 78

Bin 129: 181 of cap free
Amount of items: 4
Items: 
Size: 4903 Color: 291
Size: 2422 Color: 245
Size: 212 Color: 54
Size: 210 Color: 53

Bin 130: 210 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 287
Size: 2838 Color: 257
Size: 228 Color: 60

Bin 131: 217 of cap free
Amount of items: 12
Items: 
Size: 906 Color: 166
Size: 901 Color: 164
Size: 872 Color: 162
Size: 852 Color: 155
Size: 762 Color: 144
Size: 756 Color: 142
Size: 738 Color: 141
Size: 660 Color: 136
Size: 328 Color: 89
Size: 312 Color: 87
Size: 312 Color: 86
Size: 312 Color: 85

Bin 132: 240 of cap free
Amount of items: 16
Items: 
Size: 660 Color: 135
Size: 660 Color: 134
Size: 656 Color: 133
Size: 656 Color: 132
Size: 572 Color: 125
Size: 512 Color: 119
Size: 500 Color: 116
Size: 484 Color: 114
Size: 484 Color: 113
Size: 384 Color: 98
Size: 364 Color: 96
Size: 364 Color: 95
Size: 360 Color: 94
Size: 352 Color: 93
Size: 348 Color: 92
Size: 332 Color: 90

Bin 133: 4802 of cap free
Amount of items: 7
Items: 
Size: 480 Color: 112
Size: 480 Color: 111
Size: 456 Color: 109
Size: 456 Color: 108
Size: 444 Color: 107
Size: 420 Color: 104
Size: 390 Color: 100

Total size: 1046496
Total free space: 7928

