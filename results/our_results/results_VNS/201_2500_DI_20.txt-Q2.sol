Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 20
Items: 
Size: 168 Color: 0
Size: 168 Color: 0
Size: 150 Color: 1
Size: 150 Color: 0
Size: 148 Color: 1
Size: 148 Color: 0
Size: 144 Color: 1
Size: 132 Color: 1
Size: 130 Color: 1
Size: 128 Color: 1
Size: 120 Color: 1
Size: 114 Color: 1
Size: 104 Color: 1
Size: 104 Color: 1
Size: 96 Color: 0
Size: 96 Color: 0
Size: 90 Color: 0
Size: 90 Color: 0
Size: 88 Color: 0
Size: 88 Color: 0

Bin 2: 0 of cap free
Amount of items: 6
Items: 
Size: 1230 Color: 1
Size: 380 Color: 1
Size: 374 Color: 0
Size: 340 Color: 0
Size: 100 Color: 1
Size: 32 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 1018 Color: 0
Size: 200 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 0
Size: 1020 Color: 1
Size: 40 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 0
Size: 614 Color: 1
Size: 306 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 0
Size: 846 Color: 1
Size: 52 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 0
Size: 724 Color: 0
Size: 102 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 0
Size: 661 Color: 1
Size: 136 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 1
Size: 409 Color: 0
Size: 204 Color: 1

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 1907 Color: 0
Size: 445 Color: 1
Size: 64 Color: 1
Size: 40 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 0
Size: 254 Color: 1
Size: 112 Color: 0

Bin 12: 0 of cap free
Amount of items: 4
Items: 
Size: 2092 Color: 0
Size: 272 Color: 1
Size: 60 Color: 1
Size: 32 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 0
Size: 200 Color: 0
Size: 60 Color: 1

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 1545 Color: 1
Size: 814 Color: 1
Size: 96 Color: 0

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 1
Size: 484 Color: 0
Size: 206 Color: 1

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 1
Size: 577 Color: 0
Size: 80 Color: 1

Bin 17: 1 of cap free
Amount of items: 4
Items: 
Size: 2001 Color: 1
Size: 414 Color: 0
Size: 32 Color: 1
Size: 8 Color: 0

Bin 18: 2 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 0
Size: 845 Color: 1
Size: 56 Color: 1

Bin 19: 2 of cap free
Amount of items: 3
Items: 
Size: 1708 Color: 0
Size: 690 Color: 1
Size: 56 Color: 0

Bin 20: 2 of cap free
Amount of items: 4
Items: 
Size: 1915 Color: 0
Size: 459 Color: 1
Size: 48 Color: 0
Size: 32 Color: 1

Bin 21: 2 of cap free
Amount of items: 3
Items: 
Size: 2074 Color: 1
Size: 204 Color: 0
Size: 176 Color: 1

Bin 22: 2 of cap free
Amount of items: 4
Items: 
Size: 2106 Color: 0
Size: 284 Color: 1
Size: 48 Color: 1
Size: 16 Color: 0

Bin 23: 2 of cap free
Amount of items: 4
Items: 
Size: 2170 Color: 1
Size: 204 Color: 0
Size: 40 Color: 1
Size: 40 Color: 0

Bin 24: 2 of cap free
Amount of items: 4
Items: 
Size: 2180 Color: 1
Size: 250 Color: 0
Size: 16 Color: 1
Size: 8 Color: 0

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 0
Size: 236 Color: 1
Size: 8 Color: 1

Bin 26: 3 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 1
Size: 919 Color: 1
Size: 92 Color: 0

Bin 27: 3 of cap free
Amount of items: 2
Items: 
Size: 1722 Color: 1
Size: 731 Color: 0

Bin 28: 3 of cap free
Amount of items: 2
Items: 
Size: 1835 Color: 1
Size: 618 Color: 0

Bin 29: 3 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 1
Size: 505 Color: 0
Size: 64 Color: 0

Bin 30: 3 of cap free
Amount of items: 2
Items: 
Size: 1923 Color: 1
Size: 530 Color: 0

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1934 Color: 1
Size: 519 Color: 0

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 1942 Color: 1
Size: 511 Color: 0

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1967 Color: 0
Size: 486 Color: 1

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 1948 Color: 0
Size: 504 Color: 1

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 2158 Color: 0
Size: 294 Color: 1

Bin 36: 5 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 0
Size: 406 Color: 0
Size: 272 Color: 1

Bin 37: 5 of cap free
Amount of items: 2
Items: 
Size: 1804 Color: 0
Size: 647 Color: 1

Bin 38: 6 of cap free
Amount of items: 2
Items: 
Size: 1874 Color: 0
Size: 576 Color: 1

Bin 39: 7 of cap free
Amount of items: 2
Items: 
Size: 1901 Color: 1
Size: 548 Color: 0

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 1998 Color: 0
Size: 451 Color: 1

Bin 41: 10 of cap free
Amount of items: 4
Items: 
Size: 1231 Color: 0
Size: 665 Color: 1
Size: 430 Color: 1
Size: 120 Color: 0

Bin 42: 11 of cap free
Amount of items: 2
Items: 
Size: 1982 Color: 0
Size: 463 Color: 1

Bin 43: 12 of cap free
Amount of items: 2
Items: 
Size: 2006 Color: 1
Size: 438 Color: 0

Bin 44: 14 of cap free
Amount of items: 2
Items: 
Size: 1814 Color: 0
Size: 628 Color: 1

Bin 45: 15 of cap free
Amount of items: 2
Items: 
Size: 1688 Color: 1
Size: 753 Color: 0

Bin 46: 17 of cap free
Amount of items: 6
Items: 
Size: 1229 Color: 1
Size: 378 Color: 1
Size: 338 Color: 0
Size: 220 Color: 1
Size: 172 Color: 0
Size: 102 Color: 0

Bin 47: 18 of cap free
Amount of items: 2
Items: 
Size: 1410 Color: 1
Size: 1028 Color: 0

Bin 48: 20 of cap free
Amount of items: 2
Items: 
Size: 2052 Color: 1
Size: 384 Color: 0

Bin 49: 21 of cap free
Amount of items: 2
Items: 
Size: 1561 Color: 0
Size: 874 Color: 1

Bin 50: 21 of cap free
Amount of items: 2
Items: 
Size: 2054 Color: 0
Size: 381 Color: 1

Bin 51: 22 of cap free
Amount of items: 2
Items: 
Size: 1590 Color: 1
Size: 844 Color: 0

Bin 52: 22 of cap free
Amount of items: 2
Items: 
Size: 1673 Color: 0
Size: 761 Color: 1

Bin 53: 24 of cap free
Amount of items: 2
Items: 
Size: 1894 Color: 1
Size: 538 Color: 0

Bin 54: 24 of cap free
Amount of items: 2
Items: 
Size: 2004 Color: 1
Size: 428 Color: 0

Bin 55: 24 of cap free
Amount of items: 2
Items: 
Size: 2124 Color: 1
Size: 308 Color: 0

Bin 56: 25 of cap free
Amount of items: 2
Items: 
Size: 1681 Color: 1
Size: 750 Color: 0

Bin 57: 28 of cap free
Amount of items: 2
Items: 
Size: 1588 Color: 1
Size: 840 Color: 0

Bin 58: 29 of cap free
Amount of items: 2
Items: 
Size: 1443 Color: 1
Size: 984 Color: 0

Bin 59: 31 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 0
Size: 1021 Color: 0
Size: 168 Color: 1

Bin 60: 34 of cap free
Amount of items: 2
Items: 
Size: 1851 Color: 0
Size: 571 Color: 1

Bin 61: 42 of cap free
Amount of items: 2
Items: 
Size: 1391 Color: 1
Size: 1023 Color: 0

Bin 62: 43 of cap free
Amount of items: 2
Items: 
Size: 1444 Color: 0
Size: 969 Color: 1

Bin 63: 44 of cap free
Amount of items: 2
Items: 
Size: 1521 Color: 1
Size: 891 Color: 0

Bin 64: 44 of cap free
Amount of items: 2
Items: 
Size: 1665 Color: 0
Size: 747 Color: 1

Bin 65: 45 of cap free
Amount of items: 2
Items: 
Size: 1389 Color: 0
Size: 1022 Color: 1

Bin 66: 1734 of cap free
Amount of items: 10
Items: 
Size: 88 Color: 1
Size: 84 Color: 0
Size: 84 Color: 0
Size: 80 Color: 1
Size: 74 Color: 0
Size: 72 Color: 0
Size: 72 Color: 0
Size: 64 Color: 1
Size: 52 Color: 1
Size: 52 Color: 1

Total size: 159640
Total free space: 2456

