Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 300 Color: 2
Size: 251 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 4
Size: 286 Color: 1
Size: 260 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 267 Color: 0
Size: 250 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 3
Size: 250 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 4
Size: 351 Color: 0
Size: 270 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 306 Color: 3
Size: 256 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 4
Size: 326 Color: 3
Size: 319 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 360 Color: 2
Size: 263 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 334 Color: 1
Size: 277 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 262 Color: 3
Size: 262 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 260 Color: 2
Size: 250 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 3
Size: 263 Color: 0
Size: 250 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 4
Size: 295 Color: 3
Size: 255 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 270 Color: 2
Size: 250 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 313 Color: 1
Size: 307 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 3
Size: 267 Color: 3
Size: 265 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 298 Color: 4
Size: 260 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 3
Size: 334 Color: 3
Size: 264 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 295 Color: 3
Size: 254 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 317 Color: 1
Size: 317 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 2
Size: 346 Color: 4
Size: 269 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 324 Color: 2
Size: 288 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 268 Color: 4
Size: 257 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 349 Color: 1
Size: 256 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 4
Size: 276 Color: 4
Size: 275 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 363 Color: 2
Size: 254 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 2
Size: 266 Color: 1
Size: 252 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 320 Color: 2
Size: 275 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 4
Size: 357 Color: 2
Size: 281 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 266 Color: 3
Size: 253 Color: 0
Size: 481 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 4
Size: 348 Color: 1
Size: 251 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 3
Size: 317 Color: 4
Size: 312 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 2
Size: 287 Color: 4
Size: 278 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 305 Color: 0
Size: 293 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 275 Color: 1
Size: 259 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 336 Color: 3
Size: 269 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 308 Color: 1
Size: 263 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 3
Size: 255 Color: 0
Size: 252 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 3
Size: 283 Color: 3
Size: 258 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 257 Color: 2
Size: 250 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 339 Color: 4
Size: 270 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 351 Color: 3
Size: 278 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 3
Size: 359 Color: 4
Size: 263 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 4
Size: 270 Color: 0
Size: 251 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 4
Size: 331 Color: 4
Size: 266 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 2
Size: 252 Color: 2
Size: 250 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 258 Color: 3
Size: 254 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 3
Size: 264 Color: 4
Size: 254 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 363 Color: 0
Size: 255 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 319 Color: 4
Size: 275 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 286 Color: 3
Size: 278 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 338 Color: 0
Size: 261 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 3
Size: 260 Color: 4
Size: 252 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 268 Color: 0
Size: 267 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 283 Color: 1
Size: 275 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 350 Color: 4
Size: 285 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 4
Size: 320 Color: 3
Size: 278 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 279 Color: 0
Size: 252 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 2
Size: 334 Color: 0
Size: 262 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 3
Size: 286 Color: 4
Size: 259 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 2
Size: 275 Color: 3
Size: 261 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 2
Size: 279 Color: 2
Size: 276 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 274 Color: 4
Size: 267 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 4
Size: 325 Color: 1
Size: 314 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 363 Color: 4
Size: 255 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 335 Color: 4
Size: 269 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 2
Size: 340 Color: 2
Size: 313 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 367 Color: 0
Size: 264 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 271 Color: 3
Size: 250 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 4
Size: 345 Color: 1
Size: 257 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 3
Size: 358 Color: 4
Size: 264 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 300 Color: 4
Size: 289 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 4
Size: 309 Color: 0
Size: 308 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 320 Color: 0
Size: 273 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 259 Color: 2
Size: 258 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 325 Color: 1
Size: 271 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 3
Size: 301 Color: 4
Size: 263 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 0
Size: 284 Color: 4
Size: 269 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 343 Color: 1
Size: 271 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 346 Color: 0
Size: 276 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 291 Color: 2
Size: 261 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 317 Color: 4
Size: 275 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 334 Color: 0
Size: 270 Color: 1

Total size: 83000
Total free space: 0

