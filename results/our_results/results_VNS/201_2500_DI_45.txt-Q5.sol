Capicity Bin: 2404
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1206 Color: 4
Size: 1002 Color: 2
Size: 196 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 3
Size: 915 Color: 1
Size: 82 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 4
Size: 805 Color: 1
Size: 160 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 4
Size: 814 Color: 0
Size: 44 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1566 Color: 2
Size: 718 Color: 3
Size: 120 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 0
Size: 702 Color: 2
Size: 100 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 3
Size: 601 Color: 3
Size: 140 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 4
Size: 615 Color: 0
Size: 122 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 3
Size: 622 Color: 1
Size: 64 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 1
Size: 477 Color: 0
Size: 166 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 4
Size: 517 Color: 3
Size: 86 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 4
Size: 458 Color: 0
Size: 88 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 3
Size: 378 Color: 2
Size: 108 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 4
Size: 359 Color: 1
Size: 124 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 1
Size: 299 Color: 2
Size: 142 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 2
Size: 406 Color: 3
Size: 44 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1975 Color: 0
Size: 349 Color: 1
Size: 80 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2047 Color: 1
Size: 295 Color: 3
Size: 62 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2066 Color: 1
Size: 198 Color: 0
Size: 140 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 4
Size: 333 Color: 1
Size: 20 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 1
Size: 262 Color: 4
Size: 48 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2101 Color: 1
Size: 253 Color: 0
Size: 50 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2113 Color: 1
Size: 275 Color: 0
Size: 16 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 2
Size: 192 Color: 1
Size: 94 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 4
Size: 188 Color: 3
Size: 62 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 2
Size: 1001 Color: 4
Size: 64 Color: 2

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 2
Size: 558 Color: 1
Size: 296 Color: 2

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 4
Size: 502 Color: 3
Size: 72 Color: 1

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 2039 Color: 3
Size: 364 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 2083 Color: 1
Size: 226 Color: 4
Size: 94 Color: 0

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 2063 Color: 4
Size: 340 Color: 2

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 2134 Color: 4
Size: 269 Color: 2

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 3
Size: 859 Color: 2
Size: 168 Color: 1

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 1
Size: 481 Color: 3
Size: 128 Color: 4

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 1
Size: 456 Color: 2
Size: 144 Color: 4

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 0
Size: 521 Color: 4
Size: 68 Color: 1

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 1845 Color: 4
Size: 497 Color: 1
Size: 60 Color: 4

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 1909 Color: 1
Size: 493 Color: 0

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 2
Size: 349 Color: 1
Size: 36 Color: 2

Bin 40: 2 of cap free
Amount of items: 4
Items: 
Size: 2027 Color: 0
Size: 315 Color: 4
Size: 48 Color: 1
Size: 12 Color: 3

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 1
Size: 831 Color: 3
Size: 176 Color: 0

Bin 42: 3 of cap free
Amount of items: 2
Items: 
Size: 1430 Color: 2
Size: 971 Color: 0

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 1
Size: 403 Color: 2
Size: 16 Color: 2

Bin 44: 3 of cap free
Amount of items: 2
Items: 
Size: 2022 Color: 0
Size: 379 Color: 4

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 4
Size: 622 Color: 2
Size: 40 Color: 0

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 1781 Color: 2
Size: 619 Color: 4

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 1833 Color: 4
Size: 537 Color: 2
Size: 30 Color: 3

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 1987 Color: 3
Size: 413 Color: 4

Bin 49: 4 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 4
Size: 322 Color: 2
Size: 24 Color: 4

Bin 50: 5 of cap free
Amount of items: 4
Items: 
Size: 1205 Color: 2
Size: 730 Color: 3
Size: 354 Color: 0
Size: 110 Color: 0

Bin 51: 5 of cap free
Amount of items: 2
Items: 
Size: 1557 Color: 2
Size: 842 Color: 4

Bin 52: 5 of cap free
Amount of items: 2
Items: 
Size: 1932 Color: 0
Size: 467 Color: 2

Bin 53: 6 of cap free
Amount of items: 3
Items: 
Size: 1203 Color: 1
Size: 1135 Color: 4
Size: 60 Color: 1

Bin 54: 7 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 1
Size: 946 Color: 1
Size: 72 Color: 3

Bin 55: 8 of cap free
Amount of items: 2
Items: 
Size: 1683 Color: 1
Size: 713 Color: 2

Bin 56: 8 of cap free
Amount of items: 2
Items: 
Size: 1809 Color: 3
Size: 587 Color: 2

Bin 57: 8 of cap free
Amount of items: 2
Items: 
Size: 2114 Color: 4
Size: 282 Color: 2

Bin 58: 10 of cap free
Amount of items: 3
Items: 
Size: 2059 Color: 0
Size: 323 Color: 3
Size: 12 Color: 1

Bin 59: 11 of cap free
Amount of items: 3
Items: 
Size: 1188 Color: 1
Size: 1001 Color: 2
Size: 204 Color: 3

Bin 60: 11 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 2
Size: 890 Color: 3
Size: 294 Color: 3

Bin 61: 19 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 707 Color: 2
Size: 20 Color: 4

Bin 62: 20 of cap free
Amount of items: 3
Items: 
Size: 1270 Color: 1
Size: 904 Color: 1
Size: 210 Color: 4

Bin 63: 20 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 4
Size: 914 Color: 0

Bin 64: 21 of cap free
Amount of items: 18
Items: 
Size: 311 Color: 0
Size: 242 Color: 2
Size: 242 Color: 2
Size: 200 Color: 2
Size: 170 Color: 0
Size: 160 Color: 1
Size: 136 Color: 1
Size: 122 Color: 0
Size: 106 Color: 4
Size: 104 Color: 1
Size: 102 Color: 2
Size: 98 Color: 4
Size: 92 Color: 0
Size: 70 Color: 4
Size: 58 Color: 3
Size: 58 Color: 3
Size: 56 Color: 3
Size: 56 Color: 3

Bin 65: 21 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 2
Size: 1044 Color: 1
Size: 98 Color: 3

Bin 66: 2164 of cap free
Amount of items: 4
Items: 
Size: 80 Color: 2
Size: 68 Color: 4
Size: 52 Color: 3
Size: 40 Color: 4

Total size: 156260
Total free space: 2404

