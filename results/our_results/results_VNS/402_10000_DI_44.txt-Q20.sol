Capicity Bin: 8400
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4205 Color: 17
Size: 3497 Color: 6
Size: 698 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4206 Color: 2
Size: 3694 Color: 3
Size: 500 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4220 Color: 16
Size: 3484 Color: 17
Size: 696 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5638 Color: 6
Size: 2606 Color: 8
Size: 156 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5957 Color: 17
Size: 1979 Color: 7
Size: 464 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6230 Color: 7
Size: 2022 Color: 6
Size: 148 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6217 Color: 19
Size: 2037 Color: 4
Size: 146 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6476 Color: 0
Size: 1734 Color: 13
Size: 190 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 8
Size: 1537 Color: 2
Size: 202 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 18
Size: 1532 Color: 0
Size: 150 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6800 Color: 3
Size: 1364 Color: 2
Size: 236 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 2
Size: 1214 Color: 15
Size: 288 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 9
Size: 1068 Color: 7
Size: 376 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 3
Size: 812 Color: 19
Size: 512 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7095 Color: 14
Size: 1011 Color: 12
Size: 294 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 6
Size: 1061 Color: 0
Size: 212 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 3
Size: 794 Color: 11
Size: 456 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7234 Color: 8
Size: 782 Color: 7
Size: 384 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 8
Size: 964 Color: 3
Size: 192 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7253 Color: 13
Size: 795 Color: 5
Size: 352 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 12
Size: 754 Color: 15
Size: 356 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7319 Color: 2
Size: 755 Color: 14
Size: 326 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7338 Color: 0
Size: 862 Color: 2
Size: 200 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7356 Color: 11
Size: 604 Color: 9
Size: 440 Color: 5

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7365 Color: 1
Size: 769 Color: 10
Size: 266 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7402 Color: 13
Size: 834 Color: 8
Size: 164 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7404 Color: 12
Size: 696 Color: 7
Size: 300 Color: 19

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7450 Color: 5
Size: 688 Color: 17
Size: 262 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7466 Color: 12
Size: 766 Color: 11
Size: 168 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7492 Color: 6
Size: 688 Color: 16
Size: 220 Color: 13

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 4668 Color: 2
Size: 3493 Color: 10
Size: 238 Color: 8

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 18
Size: 2856 Color: 3
Size: 781 Color: 18

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 5204 Color: 0
Size: 2869 Color: 15
Size: 326 Color: 9

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 5484 Color: 7
Size: 2509 Color: 1
Size: 406 Color: 12

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6242 Color: 15
Size: 1200 Color: 11
Size: 957 Color: 17

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 16
Size: 2043 Color: 7
Size: 112 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 5
Size: 1804 Color: 17
Size: 156 Color: 3

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6455 Color: 13
Size: 1762 Color: 14
Size: 182 Color: 4

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 3
Size: 1604 Color: 19
Size: 166 Color: 11

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 12
Size: 1187 Color: 6
Size: 496 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 18
Size: 1477 Color: 4
Size: 158 Color: 14

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 2
Size: 1266 Color: 12
Size: 172 Color: 4

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 7303 Color: 1
Size: 924 Color: 4
Size: 172 Color: 15

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 7397 Color: 2
Size: 1002 Color: 4

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 7413 Color: 2
Size: 986 Color: 5

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 7495 Color: 9
Size: 696 Color: 12
Size: 208 Color: 5

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 7498 Color: 13
Size: 901 Color: 10

Bin 48: 2 of cap free
Amount of items: 17
Items: 
Size: 726 Color: 12
Size: 648 Color: 7
Size: 604 Color: 7
Size: 602 Color: 6
Size: 600 Color: 11
Size: 584 Color: 19
Size: 528 Color: 8
Size: 524 Color: 4
Size: 512 Color: 4
Size: 460 Color: 19
Size: 460 Color: 18
Size: 460 Color: 4
Size: 440 Color: 16
Size: 406 Color: 10
Size: 388 Color: 14
Size: 256 Color: 4
Size: 200 Color: 17

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 4209 Color: 15
Size: 3491 Color: 2
Size: 698 Color: 7

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 4262 Color: 3
Size: 4136 Color: 8

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 10
Size: 2576 Color: 12
Size: 168 Color: 7

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 8
Size: 2290 Color: 1
Size: 208 Color: 0

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 5962 Color: 10
Size: 2236 Color: 12
Size: 200 Color: 16

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 6196 Color: 15
Size: 2034 Color: 9
Size: 168 Color: 11

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 12
Size: 1802 Color: 9
Size: 290 Color: 5

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 6645 Color: 16
Size: 1201 Color: 0
Size: 552 Color: 19

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 7185 Color: 12
Size: 961 Color: 2
Size: 252 Color: 10

Bin 58: 3 of cap free
Amount of items: 7
Items: 
Size: 4201 Color: 16
Size: 961 Color: 5
Size: 836 Color: 5
Size: 823 Color: 5
Size: 764 Color: 12
Size: 572 Color: 6
Size: 240 Color: 14

Bin 59: 3 of cap free
Amount of items: 4
Items: 
Size: 4204 Color: 9
Size: 3501 Color: 14
Size: 528 Color: 5
Size: 164 Color: 3

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 5622 Color: 11
Size: 2627 Color: 15
Size: 148 Color: 12

Bin 61: 3 of cap free
Amount of items: 4
Items: 
Size: 6221 Color: 7
Size: 1382 Color: 8
Size: 698 Color: 10
Size: 96 Color: 7

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 6803 Color: 0
Size: 1402 Color: 12
Size: 192 Color: 11

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 2
Size: 1301 Color: 0
Size: 180 Color: 12

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 6946 Color: 4
Size: 1451 Color: 1

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 7349 Color: 7
Size: 1032 Color: 6
Size: 16 Color: 16

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 7482 Color: 2
Size: 915 Color: 6

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 6
Size: 3022 Color: 14
Size: 144 Color: 12

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 5249 Color: 10
Size: 3015 Color: 16
Size: 132 Color: 10

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 7354 Color: 17
Size: 1042 Color: 13

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 7383 Color: 10
Size: 1013 Color: 18

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 4959 Color: 13
Size: 3284 Color: 18
Size: 152 Color: 19

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 6530 Color: 18
Size: 1621 Color: 16
Size: 244 Color: 12

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6649 Color: 2
Size: 1746 Color: 10

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 7269 Color: 17
Size: 1126 Color: 3

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 6076 Color: 10
Size: 2318 Color: 3

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 7
Size: 1344 Color: 19

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 7063 Color: 2
Size: 1331 Color: 7

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 7444 Color: 8
Size: 926 Color: 3
Size: 24 Color: 9

Bin 79: 7 of cap free
Amount of items: 3
Items: 
Size: 4799 Color: 16
Size: 3450 Color: 10
Size: 144 Color: 16

Bin 80: 7 of cap free
Amount of items: 2
Items: 
Size: 5751 Color: 6
Size: 2642 Color: 4

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 10
Size: 1767 Color: 19
Size: 304 Color: 12

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 7189 Color: 16
Size: 1204 Color: 10

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 7463 Color: 17
Size: 930 Color: 14

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 7530 Color: 5
Size: 863 Color: 11

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 7556 Color: 3
Size: 837 Color: 15

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 1
Size: 1124 Color: 13
Size: 16 Color: 9

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 7370 Color: 7
Size: 974 Color: 14
Size: 48 Color: 8

Bin 88: 9 of cap free
Amount of items: 3
Items: 
Size: 4783 Color: 16
Size: 3128 Color: 12
Size: 480 Color: 17

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 18
Size: 1461 Color: 3

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 7202 Color: 6
Size: 1189 Color: 14

Bin 91: 9 of cap free
Amount of items: 3
Items: 
Size: 7300 Color: 17
Size: 1075 Color: 13
Size: 16 Color: 16

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 6546 Color: 1
Size: 1844 Color: 18

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 7447 Color: 5
Size: 943 Color: 13

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 5953 Color: 0
Size: 2436 Color: 13

Bin 95: 12 of cap free
Amount of items: 2
Items: 
Size: 7073 Color: 5
Size: 1315 Color: 19

Bin 96: 12 of cap free
Amount of items: 3
Items: 
Size: 7479 Color: 15
Size: 877 Color: 3
Size: 32 Color: 10

Bin 97: 15 of cap free
Amount of items: 2
Items: 
Size: 6564 Color: 15
Size: 1821 Color: 19

Bin 98: 15 of cap free
Amount of items: 2
Items: 
Size: 6823 Color: 3
Size: 1562 Color: 15

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 6839 Color: 11
Size: 1546 Color: 4

Bin 100: 15 of cap free
Amount of items: 3
Items: 
Size: 7111 Color: 16
Size: 1226 Color: 19
Size: 48 Color: 3

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 4468 Color: 12
Size: 3916 Color: 7

Bin 102: 16 of cap free
Amount of items: 2
Items: 
Size: 6100 Color: 14
Size: 2284 Color: 16

Bin 103: 16 of cap free
Amount of items: 2
Items: 
Size: 6258 Color: 15
Size: 2126 Color: 2

Bin 104: 17 of cap free
Amount of items: 2
Items: 
Size: 6443 Color: 13
Size: 1940 Color: 15

Bin 105: 17 of cap free
Amount of items: 3
Items: 
Size: 6730 Color: 13
Size: 1213 Color: 12
Size: 440 Color: 18

Bin 106: 19 of cap free
Amount of items: 3
Items: 
Size: 4213 Color: 6
Size: 3648 Color: 14
Size: 520 Color: 13

Bin 107: 19 of cap free
Amount of items: 2
Items: 
Size: 6746 Color: 1
Size: 1635 Color: 17

Bin 108: 19 of cap free
Amount of items: 2
Items: 
Size: 6977 Color: 13
Size: 1404 Color: 18

Bin 109: 22 of cap free
Amount of items: 3
Items: 
Size: 5724 Color: 19
Size: 2302 Color: 12
Size: 352 Color: 16

Bin 110: 23 of cap free
Amount of items: 2
Items: 
Size: 7218 Color: 6
Size: 1159 Color: 0

Bin 111: 25 of cap free
Amount of items: 2
Items: 
Size: 6744 Color: 3
Size: 1631 Color: 13

Bin 112: 26 of cap free
Amount of items: 2
Items: 
Size: 6290 Color: 5
Size: 2084 Color: 8

Bin 113: 28 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 0
Size: 1810 Color: 5

Bin 114: 32 of cap free
Amount of items: 2
Items: 
Size: 6882 Color: 17
Size: 1486 Color: 7

Bin 115: 32 of cap free
Amount of items: 2
Items: 
Size: 7124 Color: 4
Size: 1244 Color: 18

Bin 116: 39 of cap free
Amount of items: 3
Items: 
Size: 4769 Color: 9
Size: 2788 Color: 5
Size: 804 Color: 8

Bin 117: 39 of cap free
Amount of items: 2
Items: 
Size: 5060 Color: 15
Size: 3301 Color: 6

Bin 118: 40 of cap free
Amount of items: 28
Items: 
Size: 416 Color: 16
Size: 404 Color: 1
Size: 400 Color: 11
Size: 362 Color: 0
Size: 360 Color: 17
Size: 360 Color: 15
Size: 348 Color: 18
Size: 348 Color: 10
Size: 344 Color: 18
Size: 324 Color: 9
Size: 320 Color: 0
Size: 308 Color: 17
Size: 308 Color: 11
Size: 304 Color: 10
Size: 280 Color: 13
Size: 280 Color: 13
Size: 280 Color: 2
Size: 276 Color: 15
Size: 272 Color: 14
Size: 272 Color: 10
Size: 260 Color: 15
Size: 242 Color: 7
Size: 240 Color: 19
Size: 240 Color: 2
Size: 224 Color: 12
Size: 214 Color: 17
Size: 190 Color: 5
Size: 184 Color: 14

Bin 119: 43 of cap free
Amount of items: 2
Items: 
Size: 7249 Color: 10
Size: 1108 Color: 19

Bin 120: 46 of cap free
Amount of items: 2
Items: 
Size: 7247 Color: 9
Size: 1107 Color: 6

Bin 121: 54 of cap free
Amount of items: 2
Items: 
Size: 5312 Color: 18
Size: 3034 Color: 1

Bin 122: 61 of cap free
Amount of items: 2
Items: 
Size: 6945 Color: 13
Size: 1394 Color: 14

Bin 123: 65 of cap free
Amount of items: 5
Items: 
Size: 4202 Color: 10
Size: 1534 Color: 13
Size: 876 Color: 13
Size: 874 Color: 19
Size: 849 Color: 3

Bin 124: 77 of cap free
Amount of items: 2
Items: 
Size: 5391 Color: 0
Size: 2932 Color: 19

Bin 125: 78 of cap free
Amount of items: 3
Items: 
Size: 4884 Color: 6
Size: 3220 Color: 4
Size: 218 Color: 16

Bin 126: 78 of cap free
Amount of items: 2
Items: 
Size: 6281 Color: 12
Size: 2041 Color: 6

Bin 127: 87 of cap free
Amount of items: 3
Items: 
Size: 5604 Color: 1
Size: 2209 Color: 19
Size: 500 Color: 16

Bin 128: 90 of cap free
Amount of items: 2
Items: 
Size: 5978 Color: 18
Size: 2332 Color: 10

Bin 129: 99 of cap free
Amount of items: 2
Items: 
Size: 5274 Color: 15
Size: 3027 Color: 6

Bin 130: 99 of cap free
Amount of items: 2
Items: 
Size: 5633 Color: 19
Size: 2668 Color: 0

Bin 131: 120 of cap free
Amount of items: 2
Items: 
Size: 4778 Color: 7
Size: 3502 Color: 17

Bin 132: 124 of cap free
Amount of items: 2
Items: 
Size: 5969 Color: 15
Size: 2307 Color: 2

Bin 133: 6494 of cap free
Amount of items: 11
Items: 
Size: 196 Color: 8
Size: 196 Color: 1
Size: 188 Color: 12
Size: 184 Color: 16
Size: 176 Color: 13
Size: 176 Color: 11
Size: 174 Color: 7
Size: 160 Color: 18
Size: 152 Color: 14
Size: 152 Color: 6
Size: 152 Color: 5

Total size: 1108800
Total free space: 8400

