Capicity Bin: 1001
Lower Bound: 667

Bins used: 667
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 4
Size: 342 Color: 2
Size: 270 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 277 Color: 3
Size: 282 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 2
Size: 272 Color: 3
Size: 257 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 314 Color: 4
Size: 283 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 292 Color: 1
Size: 267 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 286 Color: 2
Size: 271 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 2
Size: 332 Color: 2
Size: 253 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 276 Color: 2
Size: 264 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 358 Color: 0
Size: 257 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 3
Size: 327 Color: 3
Size: 311 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 3
Size: 323 Color: 1
Size: 267 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 2
Size: 261 Color: 1
Size: 251 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 4
Size: 294 Color: 3
Size: 266 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 353 Color: 2
Size: 263 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 3
Size: 292 Color: 1
Size: 287 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 274 Color: 0
Size: 265 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 316 Color: 2
Size: 282 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 350 Color: 1
Size: 251 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 310 Color: 2
Size: 280 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 3
Size: 326 Color: 1
Size: 276 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 336 Color: 3
Size: 254 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 4
Size: 332 Color: 1
Size: 321 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 298 Color: 4
Size: 261 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 3
Size: 335 Color: 1
Size: 288 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 299 Color: 0
Size: 254 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 3
Size: 344 Color: 2
Size: 250 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 4
Size: 335 Color: 1
Size: 312 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 3
Size: 330 Color: 0
Size: 319 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 2
Size: 259 Color: 4
Size: 253 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 333 Color: 3
Size: 262 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 346 Color: 1
Size: 283 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 4
Size: 335 Color: 2
Size: 264 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 3
Size: 354 Color: 3
Size: 282 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 333 Color: 2
Size: 288 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 353 Color: 2
Size: 263 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 2
Size: 287 Color: 3
Size: 260 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 324 Color: 3
Size: 250 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 286 Color: 0
Size: 250 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 328 Color: 1
Size: 301 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 315 Color: 2
Size: 307 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 332 Color: 3
Size: 303 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 310 Color: 2
Size: 264 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 327 Color: 1
Size: 253 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 3
Size: 303 Color: 0
Size: 280 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 298 Color: 3
Size: 295 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 263 Color: 3
Size: 256 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 322 Color: 3
Size: 271 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 4
Size: 350 Color: 1
Size: 287 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 311 Color: 1
Size: 254 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 279 Color: 3
Size: 266 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 0
Size: 289 Color: 0
Size: 267 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 370 Color: 4
Size: 251 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 323 Color: 0
Size: 283 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 3
Size: 288 Color: 3
Size: 269 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 0
Size: 290 Color: 1
Size: 256 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 2
Size: 337 Color: 3
Size: 324 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 4
Size: 307 Color: 0
Size: 256 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 2
Size: 261 Color: 2
Size: 254 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 347 Color: 1
Size: 256 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 302 Color: 4
Size: 301 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 354 Color: 0
Size: 284 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 283 Color: 2
Size: 276 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 276 Color: 1
Size: 261 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 363 Color: 1
Size: 256 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 330 Color: 2
Size: 263 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 347 Color: 2
Size: 284 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 254 Color: 4
Size: 254 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 2
Size: 330 Color: 3
Size: 260 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 2
Size: 338 Color: 2
Size: 308 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 358 Color: 3
Size: 279 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 0
Size: 320 Color: 0
Size: 280 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 4
Size: 280 Color: 4
Size: 253 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 304 Color: 4
Size: 301 Color: 3

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 335 Color: 3
Size: 259 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 3
Size: 298 Color: 2
Size: 250 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 341 Color: 3
Size: 265 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 3
Size: 283 Color: 3
Size: 263 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 326 Color: 2
Size: 257 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 4
Size: 331 Color: 0
Size: 253 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 282 Color: 0
Size: 251 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 4
Size: 345 Color: 4
Size: 258 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 263 Color: 2
Size: 251 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 304 Color: 2
Size: 268 Color: 2

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 4
Size: 312 Color: 0
Size: 264 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 271 Color: 1
Size: 260 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 347 Color: 0
Size: 272 Color: 4

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 277 Color: 2
Size: 254 Color: 4

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 3
Size: 298 Color: 3
Size: 253 Color: 2

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 317 Color: 2
Size: 313 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 326 Color: 1
Size: 274 Color: 2

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 2
Size: 342 Color: 3
Size: 274 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 275 Color: 0
Size: 257 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 295 Color: 0
Size: 254 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 4
Size: 294 Color: 3
Size: 259 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 2
Size: 296 Color: 3
Size: 283 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 333 Color: 0
Size: 258 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 3
Size: 343 Color: 1
Size: 290 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 4
Size: 255 Color: 3
Size: 252 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 368 Color: 4
Size: 261 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 2
Size: 307 Color: 0
Size: 253 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 316 Color: 4
Size: 316 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 3
Size: 307 Color: 2
Size: 254 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 271 Color: 2
Size: 261 Color: 4

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 4
Size: 293 Color: 1
Size: 267 Color: 3

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 297 Color: 1
Size: 295 Color: 3

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 359 Color: 0
Size: 262 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 2
Size: 306 Color: 3
Size: 264 Color: 2

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 314 Color: 2
Size: 303 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 4
Size: 286 Color: 1
Size: 281 Color: 4

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 0
Size: 291 Color: 0
Size: 253 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 344 Color: 3
Size: 257 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 3
Size: 332 Color: 4
Size: 253 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 4
Size: 293 Color: 4
Size: 251 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 2
Size: 300 Color: 0
Size: 273 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 2
Size: 324 Color: 4
Size: 289 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 3
Size: 262 Color: 0
Size: 258 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 259 Color: 4
Size: 256 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 4
Size: 301 Color: 0
Size: 274 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 298 Color: 4
Size: 272 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 4
Size: 257 Color: 1
Size: 253 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 2
Size: 327 Color: 1
Size: 316 Color: 3

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 321 Color: 0
Size: 303 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 290 Color: 2
Size: 263 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 3
Size: 320 Color: 4
Size: 252 Color: 2

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 305 Color: 4
Size: 276 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 297 Color: 3
Size: 276 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 286 Color: 3
Size: 251 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 3
Size: 270 Color: 0
Size: 264 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 3
Size: 298 Color: 4
Size: 250 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 314 Color: 0
Size: 304 Color: 3

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 4
Size: 269 Color: 2
Size: 258 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 4
Size: 337 Color: 0
Size: 294 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 321 Color: 1
Size: 264 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 337 Color: 1
Size: 264 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 3
Size: 322 Color: 2
Size: 308 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 2
Size: 349 Color: 3
Size: 250 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 293 Color: 1
Size: 257 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 4
Size: 328 Color: 0
Size: 284 Color: 3

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 320 Color: 3
Size: 302 Color: 4

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 346 Color: 1
Size: 298 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 287 Color: 3
Size: 256 Color: 4

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 308 Color: 1
Size: 256 Color: 3

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 3
Size: 258 Color: 0
Size: 254 Color: 4

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 282 Color: 3
Size: 279 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 313 Color: 3
Size: 290 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 1
Size: 338 Color: 1
Size: 320 Color: 2

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 2
Size: 305 Color: 2
Size: 272 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 3
Size: 330 Color: 3
Size: 311 Color: 2

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 321 Color: 2
Size: 268 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 318 Color: 2
Size: 271 Color: 3

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 4
Size: 327 Color: 4
Size: 298 Color: 2

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 4
Size: 332 Color: 4
Size: 299 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 264 Color: 2
Size: 253 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 3
Size: 308 Color: 0
Size: 254 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 330 Color: 0
Size: 289 Color: 2

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 332 Color: 2
Size: 286 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 294 Color: 0
Size: 293 Color: 4

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 284 Color: 3
Size: 260 Color: 4

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 343 Color: 1
Size: 272 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 3
Size: 278 Color: 2
Size: 260 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 290 Color: 1
Size: 252 Color: 4

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 299 Color: 4
Size: 282 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 0
Size: 304 Color: 0
Size: 298 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 272 Color: 4
Size: 268 Color: 2

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 339 Color: 2
Size: 278 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 355 Color: 4
Size: 265 Color: 2

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 4
Size: 299 Color: 1
Size: 277 Color: 4

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 366 Color: 1
Size: 259 Color: 2

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 324 Color: 1
Size: 274 Color: 4

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 2
Size: 291 Color: 0
Size: 277 Color: 4

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 270 Color: 4
Size: 262 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 256 Color: 0
Size: 255 Color: 2

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 274 Color: 1
Size: 258 Color: 3

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 302 Color: 2
Size: 293 Color: 4

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 4
Size: 345 Color: 4
Size: 303 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 0
Size: 316 Color: 4
Size: 261 Color: 2

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 276 Color: 3
Size: 256 Color: 3

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 254 Color: 0
Size: 253 Color: 3

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 0
Size: 338 Color: 1
Size: 300 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 289 Color: 2
Size: 286 Color: 2

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 3
Size: 262 Color: 3
Size: 256 Color: 2

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 2
Size: 349 Color: 0
Size: 298 Color: 3

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 4
Size: 334 Color: 2
Size: 316 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 2
Size: 323 Color: 2
Size: 261 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 299 Color: 3
Size: 281 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 3
Size: 264 Color: 1
Size: 254 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 326 Color: 3
Size: 314 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 0
Size: 288 Color: 1
Size: 250 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 2
Size: 252 Color: 3
Size: 250 Color: 2

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 353 Color: 0
Size: 254 Color: 3

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 2
Size: 317 Color: 3
Size: 265 Color: 3

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 3
Size: 261 Color: 1
Size: 251 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 2
Size: 277 Color: 1
Size: 252 Color: 4

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 368 Color: 4
Size: 262 Color: 3

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 2
Size: 352 Color: 0
Size: 295 Color: 2

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 334 Color: 0
Size: 251 Color: 3

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 274 Color: 2
Size: 257 Color: 2

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 2
Size: 323 Color: 0
Size: 273 Color: 3

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 2
Size: 274 Color: 4
Size: 270 Color: 2

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 3
Size: 334 Color: 3
Size: 312 Color: 4

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 3
Size: 329 Color: 0
Size: 313 Color: 2

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 2
Size: 315 Color: 3
Size: 310 Color: 4

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 253 Color: 3
Size: 250 Color: 4

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 348 Color: 3
Size: 299 Color: 4

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 4
Size: 357 Color: 2
Size: 250 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 336 Color: 2
Size: 261 Color: 4

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 304 Color: 0
Size: 291 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 357 Color: 3
Size: 252 Color: 2

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 0
Size: 345 Color: 4
Size: 306 Color: 4

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 327 Color: 3
Size: 252 Color: 4

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 0
Size: 333 Color: 2
Size: 332 Color: 2

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 328 Color: 0
Size: 320 Color: 1

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 280 Color: 0
Size: 264 Color: 4

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 283 Color: 1
Size: 273 Color: 3

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 328 Color: 2
Size: 263 Color: 2

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 331 Color: 3
Size: 254 Color: 2

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 3
Size: 372 Color: 2
Size: 251 Color: 3

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 4
Size: 283 Color: 0
Size: 270 Color: 3

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 292 Color: 2
Size: 289 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 3
Size: 259 Color: 0
Size: 257 Color: 2

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 4
Size: 341 Color: 3
Size: 312 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 2
Size: 292 Color: 3
Size: 255 Color: 3

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 265 Color: 3
Size: 253 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 0
Size: 330 Color: 3
Size: 326 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 347 Color: 3
Size: 282 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 298 Color: 0
Size: 282 Color: 3

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 4
Size: 291 Color: 1
Size: 261 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 363 Color: 1
Size: 260 Color: 2

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 2
Size: 324 Color: 0
Size: 262 Color: 4

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 324 Color: 2
Size: 313 Color: 3

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 374 Color: 3
Size: 250 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 0
Size: 252 Color: 4
Size: 252 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 316 Color: 4
Size: 308 Color: 4

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 3
Size: 292 Color: 1
Size: 289 Color: 4

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 312 Color: 4
Size: 300 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 320 Color: 4
Size: 260 Color: 2

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 334 Color: 2
Size: 253 Color: 4

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 282 Color: 0
Size: 251 Color: 4

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 291 Color: 2
Size: 273 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 313 Color: 3
Size: 275 Color: 4

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 4
Size: 276 Color: 0
Size: 265 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 264 Color: 1
Size: 259 Color: 3

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 3
Size: 353 Color: 3
Size: 289 Color: 2

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 3
Size: 286 Color: 0
Size: 276 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 305 Color: 0
Size: 292 Color: 3

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 313 Color: 0
Size: 287 Color: 3

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 3
Size: 266 Color: 4
Size: 257 Color: 3

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 2
Size: 329 Color: 3
Size: 262 Color: 4

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 280 Color: 0
Size: 266 Color: 4

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 3
Size: 350 Color: 2
Size: 289 Color: 4

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 2
Size: 309 Color: 4
Size: 267 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 2
Size: 298 Color: 4
Size: 284 Color: 3

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 277 Color: 4
Size: 263 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 295 Color: 4
Size: 271 Color: 3

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 0
Size: 285 Color: 2
Size: 261 Color: 3

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 287 Color: 0
Size: 258 Color: 2

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 347 Color: 1
Size: 266 Color: 4

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 256 Color: 0
Size: 250 Color: 3

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 4
Size: 269 Color: 0
Size: 257 Color: 3

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 4
Size: 306 Color: 3
Size: 259 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 285 Color: 1
Size: 273 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 2
Size: 253 Color: 1
Size: 251 Color: 3

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 323 Color: 3
Size: 313 Color: 3

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 298 Color: 2
Size: 252 Color: 3

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 315 Color: 4
Size: 257 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 284 Color: 1
Size: 273 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 343 Color: 1
Size: 267 Color: 4

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 4
Size: 313 Color: 2
Size: 261 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 322 Color: 1
Size: 318 Color: 3

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 2
Size: 314 Color: 1
Size: 250 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 328 Color: 0
Size: 255 Color: 3

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 3
Size: 335 Color: 1
Size: 307 Color: 3

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 366 Color: 4
Size: 258 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 344 Color: 1
Size: 260 Color: 2

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 4
Size: 296 Color: 2
Size: 256 Color: 3

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 277 Color: 0
Size: 276 Color: 4

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 273 Color: 4
Size: 262 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 2
Size: 335 Color: 4
Size: 311 Color: 2

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 4
Size: 324 Color: 0
Size: 250 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 307 Color: 2
Size: 306 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 0
Size: 271 Color: 2
Size: 263 Color: 2

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 4
Size: 291 Color: 3
Size: 290 Color: 3

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 3
Size: 302 Color: 0
Size: 300 Color: 2

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 316 Color: 4
Size: 300 Color: 3

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 311 Color: 3
Size: 276 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 2
Size: 319 Color: 2
Size: 260 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 0
Size: 304 Color: 2
Size: 288 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 311 Color: 3
Size: 275 Color: 3

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 303 Color: 1
Size: 285 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 2
Size: 264 Color: 4
Size: 256 Color: 4

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 312 Color: 1
Size: 297 Color: 2

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 4
Size: 252 Color: 1
Size: 250 Color: 3

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 4
Size: 335 Color: 4
Size: 315 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 313 Color: 1
Size: 289 Color: 2

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 3
Size: 352 Color: 4
Size: 287 Color: 0

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 500 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 270 Color: 3
Size: 269 Color: 4

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 296 Color: 3
Size: 291 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 345 Color: 1
Size: 284 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 260 Color: 0
Size: 252 Color: 2

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 291 Color: 3
Size: 275 Color: 4

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 2
Size: 324 Color: 0
Size: 287 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 317 Color: 4
Size: 278 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 4
Size: 305 Color: 0
Size: 255 Color: 3

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 338 Color: 4
Size: 283 Color: 4

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 336 Color: 1
Size: 287 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 324 Color: 2
Size: 307 Color: 4

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 297 Color: 0
Size: 270 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 2
Size: 337 Color: 4
Size: 296 Color: 4

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 0
Size: 250 Color: 3
Size: 250 Color: 3

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 4
Size: 317 Color: 0
Size: 301 Color: 3

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 4
Size: 260 Color: 4
Size: 256 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 4
Size: 305 Color: 0
Size: 254 Color: 2

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 3
Size: 264 Color: 1
Size: 256 Color: 1

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 3
Size: 368 Color: 4
Size: 264 Color: 2

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 255 Color: 0
Size: 253 Color: 4

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 298 Color: 2
Size: 269 Color: 4

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 322 Color: 0
Size: 274 Color: 4

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 3
Size: 281 Color: 2
Size: 257 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 4
Size: 345 Color: 4
Size: 298 Color: 2

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 4
Size: 300 Color: 3
Size: 259 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 2
Size: 327 Color: 4
Size: 320 Color: 4

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 328 Color: 3
Size: 292 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 4
Size: 305 Color: 1
Size: 254 Color: 3

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 342 Color: 4
Size: 251 Color: 3

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 302 Color: 3
Size: 255 Color: 2

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 322 Color: 4
Size: 298 Color: 2

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1
Size: 342 Color: 1
Size: 309 Color: 3

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 342 Color: 1
Size: 251 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 263 Color: 3
Size: 253 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 263 Color: 0
Size: 254 Color: 2

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 360 Color: 0
Size: 264 Color: 1

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 0
Size: 369 Color: 3
Size: 252 Color: 3

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 257 Color: 3
Size: 255 Color: 2

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 331 Color: 0
Size: 263 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 2
Size: 312 Color: 4
Size: 297 Color: 2

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 344 Color: 4
Size: 255 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 308 Color: 1
Size: 307 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 324 Color: 1
Size: 290 Color: 4

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 2
Size: 339 Color: 1
Size: 312 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 306 Color: 0
Size: 273 Color: 3

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 340 Color: 1
Size: 257 Color: 2

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 4
Size: 273 Color: 2
Size: 270 Color: 1

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 1
Size: 341 Color: 0
Size: 317 Color: 2

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 287 Color: 2
Size: 263 Color: 1

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 322 Color: 1
Size: 314 Color: 3

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 257 Color: 4
Size: 254 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 3
Size: 351 Color: 2
Size: 261 Color: 2

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 3
Size: 312 Color: 1
Size: 280 Color: 4

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 4
Size: 323 Color: 2
Size: 319 Color: 2

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 310 Color: 1
Size: 283 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 4
Size: 339 Color: 4
Size: 250 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 355 Color: 0
Size: 291 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 4
Size: 250 Color: 2

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 0
Size: 251 Color: 0
Size: 250 Color: 2

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 307 Color: 4
Size: 256 Color: 2

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 326 Color: 2
Size: 285 Color: 2

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 2
Size: 263 Color: 0
Size: 254 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 332 Color: 2
Size: 292 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 2
Size: 263 Color: 4
Size: 254 Color: 4

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 341 Color: 1
Size: 257 Color: 2

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 358 Color: 3
Size: 252 Color: 2

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 3
Size: 280 Color: 1
Size: 277 Color: 2

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 2
Size: 262 Color: 0
Size: 251 Color: 4

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 346 Color: 1
Size: 274 Color: 4

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 348 Color: 2
Size: 260 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 287 Color: 1
Size: 267 Color: 1

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 345 Color: 1
Size: 290 Color: 3

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 3
Size: 300 Color: 3
Size: 250 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 309 Color: 0
Size: 279 Color: 2

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 352 Color: 2
Size: 254 Color: 3

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 289 Color: 2
Size: 263 Color: 3

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 1
Size: 335 Color: 3
Size: 330 Color: 4

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 310 Color: 0
Size: 272 Color: 4

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 318 Color: 4
Size: 299 Color: 3

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 327 Color: 3
Size: 317 Color: 1

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 3
Size: 324 Color: 4
Size: 255 Color: 4

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 320 Color: 1
Size: 296 Color: 3

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 321 Color: 3
Size: 310 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 309 Color: 1
Size: 299 Color: 2

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 371 Color: 2
Size: 255 Color: 4

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 2
Size: 262 Color: 1
Size: 251 Color: 1

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 4
Size: 257 Color: 1
Size: 252 Color: 3

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 3
Size: 309 Color: 0
Size: 271 Color: 2

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 3
Size: 276 Color: 2
Size: 274 Color: 2

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 2
Size: 254 Color: 0
Size: 251 Color: 3

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 2
Size: 298 Color: 0
Size: 273 Color: 2

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 2
Size: 306 Color: 1
Size: 263 Color: 3

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 321 Color: 4
Size: 274 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 3
Size: 271 Color: 2
Size: 257 Color: 3

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 300 Color: 4
Size: 265 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 280 Color: 2
Size: 253 Color: 3

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 320 Color: 3
Size: 303 Color: 4

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 297 Color: 1
Size: 261 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 1
Size: 336 Color: 4
Size: 318 Color: 1

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 3
Size: 295 Color: 0
Size: 268 Color: 2

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 333 Color: 2
Size: 290 Color: 3

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 312 Color: 2
Size: 285 Color: 3

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 335 Color: 2
Size: 294 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 316 Color: 4
Size: 284 Color: 3

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 312 Color: 3
Size: 271 Color: 3

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 346 Color: 3
Size: 260 Color: 3

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 4
Size: 263 Color: 2
Size: 260 Color: 1

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 320 Color: 2
Size: 254 Color: 2

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1
Size: 329 Color: 4
Size: 322 Color: 4

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 326 Color: 0
Size: 265 Color: 4

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 314 Color: 3
Size: 306 Color: 1

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 316 Color: 4
Size: 267 Color: 3

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 4
Size: 364 Color: 1
Size: 251 Color: 2

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 359 Color: 1
Size: 262 Color: 4

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 347 Color: 1
Size: 263 Color: 2

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 298 Color: 0
Size: 271 Color: 3

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 0
Size: 331 Color: 4
Size: 323 Color: 3

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 3
Size: 340 Color: 1
Size: 273 Color: 4

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 278 Color: 2
Size: 277 Color: 3

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 365 Color: 4
Size: 260 Color: 3

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 2
Size: 257 Color: 0
Size: 255 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 370 Color: 2
Size: 252 Color: 0

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 319 Color: 2
Size: 310 Color: 2

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 3
Size: 279 Color: 2
Size: 277 Color: 3

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 254 Color: 4
Size: 253 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 4
Size: 335 Color: 1
Size: 314 Color: 1

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 2
Size: 355 Color: 0
Size: 261 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 4
Size: 339 Color: 0
Size: 298 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 297 Color: 2
Size: 294 Color: 4

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 269 Color: 3
Size: 258 Color: 1

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 2
Size: 306 Color: 0
Size: 286 Color: 1

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 353 Color: 4
Size: 257 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 3
Size: 333 Color: 0
Size: 255 Color: 3

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 287 Color: 3
Size: 282 Color: 4

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 4
Size: 352 Color: 0
Size: 274 Color: 2

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 317 Color: 1
Size: 285 Color: 4

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 342 Color: 4
Size: 272 Color: 2

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 3
Size: 293 Color: 0
Size: 278 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 3
Size: 324 Color: 4
Size: 257 Color: 4

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 3
Size: 264 Color: 1
Size: 254 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 301 Color: 2
Size: 260 Color: 3

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 358 Color: 2
Size: 278 Color: 4

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 3
Size: 262 Color: 1
Size: 261 Color: 1

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 292 Color: 4
Size: 264 Color: 1

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 355 Color: 3
Size: 258 Color: 4

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 4
Size: 337 Color: 3
Size: 296 Color: 1

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 261 Color: 1
Size: 252 Color: 2

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 295 Color: 4
Size: 275 Color: 1

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 4
Size: 291 Color: 2
Size: 283 Color: 4

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 2
Size: 265 Color: 0
Size: 261 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 309 Color: 1
Size: 263 Color: 4

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 3
Size: 301 Color: 4
Size: 256 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 3
Size: 288 Color: 2
Size: 261 Color: 3

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 295 Color: 1
Size: 268 Color: 4

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 366 Color: 4
Size: 266 Color: 4

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 277 Color: 0
Size: 260 Color: 1

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 2
Size: 354 Color: 4
Size: 282 Color: 2

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 343 Color: 3
Size: 254 Color: 2

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 2
Size: 333 Color: 3
Size: 307 Color: 3

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 278 Color: 3
Size: 258 Color: 4

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 282 Color: 4
Size: 273 Color: 2

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 261 Color: 0
Size: 257 Color: 1

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 4
Size: 317 Color: 1
Size: 278 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 319 Color: 0
Size: 279 Color: 4

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 4
Size: 316 Color: 0
Size: 273 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 321 Color: 0
Size: 302 Color: 3

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 0
Size: 352 Color: 4
Size: 283 Color: 3

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 343 Color: 1
Size: 270 Color: 1

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 315 Color: 3
Size: 295 Color: 3

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 2
Size: 326 Color: 4
Size: 308 Color: 2

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 2
Size: 324 Color: 2
Size: 267 Color: 0

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 0
Size: 253 Color: 3
Size: 251 Color: 1

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 284 Color: 3
Size: 250 Color: 1

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 2
Size: 322 Color: 3
Size: 294 Color: 4

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 3
Size: 273 Color: 0
Size: 270 Color: 1

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 272 Color: 3
Size: 260 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 3
Size: 279 Color: 1
Size: 267 Color: 2

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 306 Color: 4
Size: 282 Color: 1

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 356 Color: 4
Size: 253 Color: 4

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 3
Size: 317 Color: 1
Size: 282 Color: 3

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 2
Size: 287 Color: 2
Size: 260 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 4
Size: 275 Color: 4
Size: 253 Color: 1

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 4
Size: 354 Color: 1
Size: 282 Color: 3

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 339 Color: 4
Size: 302 Color: 3

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 4
Size: 304 Color: 0
Size: 296 Color: 1

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 294 Color: 4
Size: 269 Color: 4

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 3
Size: 336 Color: 3
Size: 278 Color: 2

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 321 Color: 0
Size: 316 Color: 2

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 335 Color: 4
Size: 260 Color: 3

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 327 Color: 2
Size: 284 Color: 2

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 3
Size: 331 Color: 0
Size: 252 Color: 4

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 334 Color: 0
Size: 251 Color: 2

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 343 Color: 4
Size: 268 Color: 3

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 273 Color: 2
Size: 262 Color: 4

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 4
Size: 284 Color: 2
Size: 278 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 278 Color: 0
Size: 276 Color: 1

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 279 Color: 0
Size: 278 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 4
Size: 368 Color: 4
Size: 254 Color: 3

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 2
Size: 251 Color: 3
Size: 251 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 283 Color: 4
Size: 257 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 0
Size: 313 Color: 3
Size: 250 Color: 2

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 353 Color: 3
Size: 288 Color: 2

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 340 Color: 1
Size: 262 Color: 0

Bin 500: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 3
Size: 250 Color: 2
Size: 250 Color: 2
Size: 250 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 304 Color: 1
Size: 251 Color: 3

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 349 Color: 1
Size: 273 Color: 2

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 324 Color: 4
Size: 251 Color: 1

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 2
Size: 343 Color: 0
Size: 311 Color: 1

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 356 Color: 2
Size: 274 Color: 3

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 307 Color: 2
Size: 304 Color: 3

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 321 Color: 4
Size: 252 Color: 0

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 3
Size: 292 Color: 1
Size: 251 Color: 1

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 330 Color: 1
Size: 279 Color: 3

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 292 Color: 1
Size: 252 Color: 3

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 275 Color: 2
Size: 255 Color: 2

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 0
Size: 254 Color: 1
Size: 250 Color: 4

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 0
Size: 251 Color: 2

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 367 Color: 2
Size: 262 Color: 0

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 319 Color: 4
Size: 259 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 263 Color: 3
Size: 254 Color: 2

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 4
Size: 285 Color: 0
Size: 275 Color: 1

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 340 Color: 0
Size: 284 Color: 4

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 284 Color: 2
Size: 259 Color: 2

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 318 Color: 2
Size: 285 Color: 4

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 4
Size: 288 Color: 4
Size: 253 Color: 1

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 313 Color: 1
Size: 265 Color: 1

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 318 Color: 1
Size: 298 Color: 3

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 267 Color: 1
Size: 263 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 3
Size: 337 Color: 0
Size: 265 Color: 3

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 276 Color: 2
Size: 262 Color: 0

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 4
Size: 289 Color: 1
Size: 273 Color: 4

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 4
Size: 277 Color: 4
Size: 250 Color: 3

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 284 Color: 1
Size: 281 Color: 1

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 3
Size: 287 Color: 2
Size: 259 Color: 1

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 0
Size: 359 Color: 4
Size: 262 Color: 4

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 307 Color: 4
Size: 252 Color: 1

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 257 Color: 4
Size: 250 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 288 Color: 1
Size: 254 Color: 3

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 360 Color: 2
Size: 250 Color: 2

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 286 Color: 0
Size: 255 Color: 3

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 349 Color: 1
Size: 287 Color: 3

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 4
Size: 257 Color: 1
Size: 253 Color: 1

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 281 Color: 0
Size: 274 Color: 2

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 3
Size: 262 Color: 2
Size: 252 Color: 3

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 0
Size: 254 Color: 2
Size: 251 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 3
Size: 264 Color: 1
Size: 250 Color: 3

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 2
Size: 332 Color: 4
Size: 279 Color: 4

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 324 Color: 3
Size: 254 Color: 4

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 2
Size: 267 Color: 4
Size: 251 Color: 3

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 312 Color: 2
Size: 294 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 310 Color: 0
Size: 253 Color: 2

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 342 Color: 1
Size: 275 Color: 3

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 4
Size: 313 Color: 0
Size: 273 Color: 2

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 321 Color: 3
Size: 252 Color: 4

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 2
Size: 332 Color: 1
Size: 313 Color: 3

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 347 Color: 4
Size: 268 Color: 2

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 341 Color: 3
Size: 288 Color: 3

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 345 Color: 2
Size: 291 Color: 2

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 0
Size: 336 Color: 4
Size: 309 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 0
Size: 250 Color: 2

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 331 Color: 1
Size: 289 Color: 3

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 4
Size: 363 Color: 4
Size: 274 Color: 3

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 0
Size: 272 Color: 4
Size: 263 Color: 3

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 329 Color: 1
Size: 254 Color: 3

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 3
Size: 304 Color: 2
Size: 293 Color: 1

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 315 Color: 1
Size: 293 Color: 0

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 340 Color: 2
Size: 284 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 304 Color: 1
Size: 297 Color: 2

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 362 Color: 4
Size: 264 Color: 2

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 311 Color: 0
Size: 253 Color: 0

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 0
Size: 265 Color: 3
Size: 259 Color: 1

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 280 Color: 3
Size: 250 Color: 4

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 2
Size: 363 Color: 4
Size: 250 Color: 4

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 315 Color: 0
Size: 274 Color: 3

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 359 Color: 4
Size: 275 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 370 Color: 1
Size: 252 Color: 4

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 3
Size: 274 Color: 0
Size: 252 Color: 4

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 321 Color: 1
Size: 262 Color: 1

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 327 Color: 4
Size: 282 Color: 1

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 272 Color: 3
Size: 261 Color: 2

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 338 Color: 3
Size: 297 Color: 4

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 306 Color: 3
Size: 297 Color: 2

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 312 Color: 0
Size: 255 Color: 3

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 270 Color: 3
Size: 266 Color: 4

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 274 Color: 1
Size: 261 Color: 4

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 316 Color: 1
Size: 306 Color: 4

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 287 Color: 0
Size: 252 Color: 4

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 263 Color: 3
Size: 259 Color: 4

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 2
Size: 354 Color: 4
Size: 279 Color: 3

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 317 Color: 4
Size: 258 Color: 4

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 3
Size: 283 Color: 1
Size: 275 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 335 Color: 0
Size: 304 Color: 2

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 331 Color: 4
Size: 295 Color: 0

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 4
Size: 348 Color: 3
Size: 298 Color: 2

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 294 Color: 4
Size: 281 Color: 2

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 328 Color: 3
Size: 275 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 322 Color: 2
Size: 280 Color: 4

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 4
Size: 327 Color: 2
Size: 261 Color: 4

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 3
Size: 293 Color: 2
Size: 251 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 4
Size: 254 Color: 4
Size: 253 Color: 3

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 3
Size: 351 Color: 1
Size: 281 Color: 4

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 293 Color: 0
Size: 256 Color: 1

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 318 Color: 2
Size: 265 Color: 4

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 3
Size: 299 Color: 3
Size: 291 Color: 0

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 338 Color: 2
Size: 259 Color: 0

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 277 Color: 4
Size: 254 Color: 4

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 333 Color: 0
Size: 310 Color: 2

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 2
Size: 251 Color: 3

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 333 Color: 0
Size: 287 Color: 4

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 296 Color: 3
Size: 289 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 321 Color: 0
Size: 306 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 254 Color: 2
Size: 252 Color: 0

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 3
Size: 321 Color: 2
Size: 250 Color: 1

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 311 Color: 2
Size: 265 Color: 3

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 335 Color: 1
Size: 279 Color: 0

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 0
Size: 341 Color: 1
Size: 251 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 2
Size: 311 Color: 0
Size: 302 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 3
Size: 336 Color: 2
Size: 250 Color: 3

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 279 Color: 2
Size: 263 Color: 1

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 314 Color: 1
Size: 285 Color: 0

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 3
Size: 268 Color: 3
Size: 254 Color: 4

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 2
Size: 310 Color: 1
Size: 276 Color: 3

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 4
Size: 309 Color: 2
Size: 251 Color: 1

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 3
Size: 298 Color: 1
Size: 283 Color: 1

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 3
Size: 338 Color: 4
Size: 301 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 371 Color: 3
Size: 257 Color: 2

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 2
Size: 328 Color: 4
Size: 256 Color: 0

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 341 Color: 1
Size: 263 Color: 2

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 2
Size: 253 Color: 4
Size: 251 Color: 1

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 1
Size: 335 Color: 1
Size: 331 Color: 0

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 0
Size: 252 Color: 3
Size: 252 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 3
Size: 290 Color: 0
Size: 255 Color: 3

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 276 Color: 0
Size: 256 Color: 2

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 272 Color: 2
Size: 264 Color: 3

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 314 Color: 2
Size: 302 Color: 4

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 252 Color: 3
Size: 251 Color: 2

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 291 Color: 2
Size: 281 Color: 4

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 2
Size: 321 Color: 3
Size: 250 Color: 2

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 305 Color: 3
Size: 266 Color: 4

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 303 Color: 0
Size: 279 Color: 4

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 302 Color: 1
Size: 269 Color: 4

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 4
Size: 269 Color: 4
Size: 252 Color: 3

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 4
Size: 355 Color: 2
Size: 281 Color: 1

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 312 Color: 1
Size: 261 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 318 Color: 0
Size: 284 Color: 3

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 3
Size: 288 Color: 1
Size: 276 Color: 4

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 285 Color: 4
Size: 260 Color: 2

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 2
Size: 344 Color: 1
Size: 300 Color: 2

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 4
Size: 259 Color: 0
Size: 255 Color: 3

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 303 Color: 4
Size: 280 Color: 1

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 3
Size: 344 Color: 0
Size: 294 Color: 1

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 341 Color: 2
Size: 280 Color: 4

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 262 Color: 2
Size: 259 Color: 1

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 290 Color: 2
Size: 271 Color: 2

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 4
Size: 256 Color: 3
Size: 253 Color: 1

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 338 Color: 0
Size: 297 Color: 2

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 4
Size: 329 Color: 3
Size: 293 Color: 2

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 305 Color: 1
Size: 266 Color: 3

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 268 Color: 0
Size: 256 Color: 2

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 317 Color: 3
Size: 308 Color: 1

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 321 Color: 4
Size: 314 Color: 3

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 371 Color: 3
Size: 257 Color: 2

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 280 Color: 3
Size: 258 Color: 4

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 303 Color: 3
Size: 279 Color: 0

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 325 Color: 2
Size: 301 Color: 0

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 305 Color: 0
Size: 284 Color: 2

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 4
Size: 332 Color: 1
Size: 312 Color: 1

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 4
Size: 311 Color: 0
Size: 281 Color: 1

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 0
Size: 273 Color: 3
Size: 255 Color: 4

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 276 Color: 0
Size: 265 Color: 2

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 277 Color: 3
Size: 253 Color: 4

Total size: 667667
Total free space: 0

