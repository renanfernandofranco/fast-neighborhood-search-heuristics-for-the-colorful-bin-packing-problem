Capicity Bin: 5392
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 2724 Color: 4
Size: 2228 Color: 1
Size: 440 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3348 Color: 1
Size: 1764 Color: 0
Size: 280 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3416 Color: 1
Size: 1816 Color: 2
Size: 160 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3644 Color: 1
Size: 1396 Color: 0
Size: 352 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3699 Color: 1
Size: 1411 Color: 4
Size: 282 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3719 Color: 4
Size: 1385 Color: 0
Size: 288 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 3740 Color: 3
Size: 1484 Color: 1
Size: 168 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 3784 Color: 0
Size: 1436 Color: 1
Size: 172 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 3804 Color: 4
Size: 1500 Color: 0
Size: 88 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 3826 Color: 0
Size: 1306 Color: 2
Size: 260 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 3864 Color: 0
Size: 1420 Color: 4
Size: 108 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4020 Color: 0
Size: 1144 Color: 3
Size: 228 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 4
Size: 948 Color: 3
Size: 276 Color: 0

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 4280 Color: 0
Size: 996 Color: 4
Size: 64 Color: 3
Size: 52 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4318 Color: 0
Size: 982 Color: 4
Size: 92 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 3
Size: 592 Color: 1
Size: 470 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4420 Color: 3
Size: 764 Color: 0
Size: 208 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4388 Color: 1
Size: 812 Color: 2
Size: 192 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4506 Color: 4
Size: 742 Color: 1
Size: 144 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 4
Size: 740 Color: 0
Size: 100 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4526 Color: 1
Size: 522 Color: 1
Size: 344 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4556 Color: 4
Size: 516 Color: 0
Size: 320 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4578 Color: 0
Size: 638 Color: 3
Size: 176 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4630 Color: 4
Size: 610 Color: 0
Size: 152 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4611 Color: 3
Size: 651 Color: 4
Size: 130 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4615 Color: 1
Size: 649 Color: 3
Size: 128 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4648 Color: 4
Size: 496 Color: 2
Size: 248 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 4662 Color: 3
Size: 514 Color: 4
Size: 216 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4668 Color: 1
Size: 594 Color: 3
Size: 130 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4680 Color: 2
Size: 568 Color: 3
Size: 144 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4715 Color: 4
Size: 565 Color: 0
Size: 112 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4716 Color: 2
Size: 564 Color: 1
Size: 112 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4744 Color: 4
Size: 416 Color: 0
Size: 232 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 3
Size: 400 Color: 2
Size: 244 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 1
Size: 336 Color: 1
Size: 278 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4802 Color: 3
Size: 396 Color: 3
Size: 194 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4830 Color: 2
Size: 432 Color: 1
Size: 130 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4850 Color: 0
Size: 384 Color: 0
Size: 158 Color: 4

Bin 39: 1 of cap free
Amount of items: 4
Items: 
Size: 2712 Color: 2
Size: 1702 Color: 0
Size: 881 Color: 3
Size: 96 Color: 2

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 3010 Color: 0
Size: 2245 Color: 3
Size: 136 Color: 0

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 3180 Color: 2
Size: 1763 Color: 1
Size: 448 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 3346 Color: 1
Size: 1937 Color: 1
Size: 108 Color: 2

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 3431 Color: 2
Size: 1840 Color: 0
Size: 120 Color: 1

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 3685 Color: 2
Size: 1486 Color: 1
Size: 220 Color: 4

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 4085 Color: 3
Size: 1146 Color: 4
Size: 160 Color: 1

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 4218 Color: 3
Size: 877 Color: 4
Size: 296 Color: 4

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 4221 Color: 3
Size: 722 Color: 0
Size: 448 Color: 2

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 0
Size: 971 Color: 0
Size: 128 Color: 3

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 4341 Color: 4
Size: 898 Color: 3
Size: 152 Color: 4

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 4439 Color: 4
Size: 608 Color: 4
Size: 344 Color: 3

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 3
Size: 632 Color: 2
Size: 288 Color: 4

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 4543 Color: 2
Size: 848 Color: 0

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 4603 Color: 4
Size: 396 Color: 1
Size: 392 Color: 0

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 4780 Color: 3
Size: 611 Color: 2

Bin 55: 2 of cap free
Amount of items: 4
Items: 
Size: 2700 Color: 3
Size: 2242 Color: 4
Size: 334 Color: 0
Size: 114 Color: 2

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 3065 Color: 3
Size: 1941 Color: 1
Size: 384 Color: 0

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 3612 Color: 4
Size: 1722 Color: 1
Size: 56 Color: 2

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 3646 Color: 4
Size: 1448 Color: 3
Size: 296 Color: 2

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 4014 Color: 0
Size: 1130 Color: 4
Size: 246 Color: 3

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 4038 Color: 1
Size: 1248 Color: 2
Size: 104 Color: 0

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 4349 Color: 1
Size: 977 Color: 3
Size: 64 Color: 2

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 4
Size: 546 Color: 4
Size: 368 Color: 3

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 4738 Color: 1
Size: 652 Color: 3

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 4518 Color: 2
Size: 871 Color: 0

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 3988 Color: 0
Size: 1120 Color: 2
Size: 280 Color: 3

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 4488 Color: 2
Size: 900 Color: 0

Bin 67: 5 of cap free
Amount of items: 3
Items: 
Size: 3069 Color: 2
Size: 1227 Color: 2
Size: 1091 Color: 0

Bin 68: 5 of cap free
Amount of items: 3
Items: 
Size: 3383 Color: 1
Size: 1708 Color: 1
Size: 296 Color: 3

Bin 69: 5 of cap free
Amount of items: 3
Items: 
Size: 3656 Color: 2
Size: 1631 Color: 1
Size: 100 Color: 4

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 3731 Color: 3
Size: 1656 Color: 0

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 4006 Color: 4
Size: 1221 Color: 1
Size: 160 Color: 0

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 4335 Color: 3
Size: 1052 Color: 2

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 4525 Color: 0
Size: 862 Color: 1

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 4712 Color: 3
Size: 659 Color: 1
Size: 16 Color: 2

Bin 75: 6 of cap free
Amount of items: 3
Items: 
Size: 2702 Color: 2
Size: 2244 Color: 1
Size: 440 Color: 0

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 4376 Color: 2
Size: 1010 Color: 1

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 4450 Color: 2
Size: 936 Color: 4

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 4663 Color: 2
Size: 723 Color: 0

Bin 79: 7 of cap free
Amount of items: 2
Items: 
Size: 3927 Color: 0
Size: 1458 Color: 3

Bin 80: 8 of cap free
Amount of items: 3
Items: 
Size: 3224 Color: 2
Size: 1706 Color: 0
Size: 454 Color: 2

Bin 81: 8 of cap free
Amount of items: 3
Items: 
Size: 3676 Color: 1
Size: 1468 Color: 0
Size: 240 Color: 3

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 4589 Color: 3
Size: 795 Color: 1

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 4832 Color: 1
Size: 552 Color: 3

Bin 84: 9 of cap free
Amount of items: 9
Items: 
Size: 2697 Color: 3
Size: 492 Color: 2
Size: 448 Color: 3
Size: 386 Color: 4
Size: 344 Color: 3
Size: 336 Color: 4
Size: 296 Color: 3
Size: 192 Color: 0
Size: 192 Color: 0

Bin 85: 9 of cap free
Amount of items: 3
Items: 
Size: 2708 Color: 3
Size: 1966 Color: 1
Size: 709 Color: 4

Bin 86: 10 of cap free
Amount of items: 2
Items: 
Size: 4362 Color: 2
Size: 1020 Color: 4

Bin 87: 10 of cap free
Amount of items: 2
Items: 
Size: 4588 Color: 1
Size: 794 Color: 2

Bin 88: 10 of cap free
Amount of items: 2
Items: 
Size: 4682 Color: 2
Size: 700 Color: 1

Bin 89: 11 of cap free
Amount of items: 3
Items: 
Size: 3354 Color: 1
Size: 1931 Color: 2
Size: 96 Color: 4

Bin 90: 11 of cap free
Amount of items: 3
Items: 
Size: 3921 Color: 2
Size: 856 Color: 1
Size: 604 Color: 3

Bin 91: 11 of cap free
Amount of items: 3
Items: 
Size: 4204 Color: 0
Size: 791 Color: 4
Size: 386 Color: 3

Bin 92: 13 of cap free
Amount of items: 2
Items: 
Size: 4091 Color: 2
Size: 1288 Color: 1

Bin 93: 13 of cap free
Amount of items: 2
Items: 
Size: 4229 Color: 4
Size: 1150 Color: 1

Bin 94: 13 of cap free
Amount of items: 3
Items: 
Size: 4661 Color: 0
Size: 686 Color: 2
Size: 32 Color: 1

Bin 95: 13 of cap free
Amount of items: 2
Items: 
Size: 4770 Color: 0
Size: 609 Color: 3

Bin 96: 13 of cap free
Amount of items: 2
Items: 
Size: 4804 Color: 0
Size: 575 Color: 1

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 4612 Color: 1
Size: 766 Color: 3

Bin 98: 14 of cap free
Amount of items: 3
Items: 
Size: 4707 Color: 2
Size: 655 Color: 0
Size: 16 Color: 1

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 4345 Color: 2
Size: 1032 Color: 1

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 3284 Color: 3
Size: 2092 Color: 4

Bin 101: 16 of cap free
Amount of items: 3
Items: 
Size: 3316 Color: 1
Size: 1844 Color: 2
Size: 216 Color: 0

Bin 102: 16 of cap free
Amount of items: 2
Items: 
Size: 3443 Color: 3
Size: 1933 Color: 4

Bin 103: 16 of cap free
Amount of items: 3
Items: 
Size: 4132 Color: 2
Size: 1180 Color: 3
Size: 64 Color: 2

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 4607 Color: 2
Size: 769 Color: 1

Bin 105: 18 of cap free
Amount of items: 2
Items: 
Size: 4202 Color: 2
Size: 1172 Color: 4

Bin 106: 18 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 4
Size: 994 Color: 1
Size: 64 Color: 0

Bin 107: 18 of cap free
Amount of items: 2
Items: 
Size: 4703 Color: 1
Size: 671 Color: 4

Bin 108: 19 of cap free
Amount of items: 3
Items: 
Size: 2740 Color: 0
Size: 2247 Color: 2
Size: 386 Color: 4

Bin 109: 19 of cap free
Amount of items: 2
Items: 
Size: 3379 Color: 2
Size: 1994 Color: 4

Bin 110: 19 of cap free
Amount of items: 3
Items: 
Size: 3610 Color: 1
Size: 1423 Color: 3
Size: 340 Color: 2

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 4024 Color: 1
Size: 1348 Color: 3

Bin 112: 21 of cap free
Amount of items: 2
Items: 
Size: 3692 Color: 3
Size: 1679 Color: 2

Bin 113: 24 of cap free
Amount of items: 29
Items: 
Size: 324 Color: 4
Size: 284 Color: 4
Size: 244 Color: 4
Size: 224 Color: 4
Size: 224 Color: 3
Size: 208 Color: 4
Size: 200 Color: 1
Size: 200 Color: 1
Size: 196 Color: 3
Size: 192 Color: 3
Size: 192 Color: 2
Size: 192 Color: 1
Size: 190 Color: 0
Size: 184 Color: 2
Size: 176 Color: 4
Size: 176 Color: 4
Size: 176 Color: 2
Size: 176 Color: 1
Size: 174 Color: 2
Size: 174 Color: 1
Size: 160 Color: 0
Size: 158 Color: 2
Size: 148 Color: 0
Size: 144 Color: 3
Size: 144 Color: 3
Size: 140 Color: 0
Size: 132 Color: 2
Size: 120 Color: 0
Size: 116 Color: 0

Bin 114: 25 of cap free
Amount of items: 2
Items: 
Size: 4443 Color: 4
Size: 924 Color: 1

Bin 115: 26 of cap free
Amount of items: 3
Items: 
Size: 3002 Color: 3
Size: 2244 Color: 4
Size: 120 Color: 4

Bin 116: 27 of cap free
Amount of items: 2
Items: 
Size: 3633 Color: 0
Size: 1732 Color: 3

Bin 117: 28 of cap free
Amount of items: 5
Items: 
Size: 2699 Color: 1
Size: 873 Color: 3
Size: 776 Color: 4
Size: 760 Color: 0
Size: 256 Color: 1

Bin 118: 28 of cap free
Amount of items: 3
Items: 
Size: 2884 Color: 4
Size: 1395 Color: 2
Size: 1085 Color: 0

Bin 119: 28 of cap free
Amount of items: 2
Items: 
Size: 3916 Color: 0
Size: 1448 Color: 4

Bin 120: 30 of cap free
Amount of items: 2
Items: 
Size: 3376 Color: 2
Size: 1986 Color: 0

Bin 121: 38 of cap free
Amount of items: 2
Items: 
Size: 4052 Color: 2
Size: 1302 Color: 1

Bin 122: 38 of cap free
Amount of items: 2
Items: 
Size: 4182 Color: 4
Size: 1172 Color: 2

Bin 123: 41 of cap free
Amount of items: 6
Items: 
Size: 2698 Color: 4
Size: 600 Color: 0
Size: 571 Color: 3
Size: 540 Color: 1
Size: 494 Color: 1
Size: 448 Color: 1

Bin 124: 41 of cap free
Amount of items: 2
Items: 
Size: 3580 Color: 3
Size: 1771 Color: 4

Bin 125: 42 of cap free
Amount of items: 2
Items: 
Size: 3834 Color: 3
Size: 1516 Color: 0

Bin 126: 48 of cap free
Amount of items: 2
Items: 
Size: 3034 Color: 1
Size: 2310 Color: 4

Bin 127: 52 of cap free
Amount of items: 2
Items: 
Size: 2984 Color: 3
Size: 2356 Color: 1

Bin 128: 54 of cap free
Amount of items: 2
Items: 
Size: 4172 Color: 4
Size: 1166 Color: 2

Bin 129: 55 of cap free
Amount of items: 3
Items: 
Size: 3073 Color: 3
Size: 2144 Color: 4
Size: 120 Color: 4

Bin 130: 55 of cap free
Amount of items: 2
Items: 
Size: 3712 Color: 2
Size: 1625 Color: 3

Bin 131: 58 of cap free
Amount of items: 2
Items: 
Size: 3326 Color: 3
Size: 2008 Color: 1

Bin 132: 67 of cap free
Amount of items: 2
Items: 
Size: 3077 Color: 3
Size: 2248 Color: 0

Bin 133: 4018 of cap free
Amount of items: 12
Items: 
Size: 128 Color: 3
Size: 128 Color: 2
Size: 128 Color: 1
Size: 124 Color: 1
Size: 114 Color: 3
Size: 112 Color: 4
Size: 112 Color: 2
Size: 112 Color: 1
Size: 112 Color: 0
Size: 112 Color: 0
Size: 96 Color: 4
Size: 96 Color: 0

Total size: 711744
Total free space: 5392

