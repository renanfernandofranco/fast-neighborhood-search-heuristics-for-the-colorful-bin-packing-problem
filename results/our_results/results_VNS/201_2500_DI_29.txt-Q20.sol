Capicity Bin: 2064
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1033 Color: 0
Size: 461 Color: 16
Size: 428 Color: 2
Size: 106 Color: 8
Size: 36 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1315 Color: 2
Size: 717 Color: 9
Size: 32 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1424 Color: 13
Size: 606 Color: 10
Size: 34 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 18
Size: 434 Color: 2
Size: 120 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 3
Size: 379 Color: 9
Size: 136 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 16
Size: 414 Color: 2
Size: 80 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1615 Color: 12
Size: 293 Color: 15
Size: 156 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 0
Size: 378 Color: 6
Size: 52 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 14
Size: 331 Color: 9
Size: 64 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 15
Size: 303 Color: 15
Size: 44 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 7
Size: 226 Color: 0
Size: 120 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 5
Size: 223 Color: 13
Size: 90 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 2
Size: 258 Color: 9
Size: 48 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 16
Size: 244 Color: 6
Size: 40 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 15
Size: 195 Color: 19
Size: 58 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 19
Size: 202 Color: 12
Size: 44 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 6
Size: 168 Color: 9
Size: 74 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1106 Color: 11
Size: 857 Color: 17
Size: 100 Color: 19

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 12
Size: 853 Color: 6
Size: 44 Color: 5

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1326 Color: 14
Size: 737 Color: 4

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 4
Size: 618 Color: 13
Size: 38 Color: 14

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 15
Size: 202 Color: 14
Size: 148 Color: 8

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1773 Color: 5
Size: 290 Color: 18

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 6
Size: 271 Color: 18
Size: 16 Color: 14

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1831 Color: 8
Size: 168 Color: 10
Size: 64 Color: 11

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1839 Color: 14
Size: 132 Color: 19
Size: 92 Color: 11

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 10
Size: 211 Color: 2
Size: 6 Color: 14

Bin 28: 2 of cap free
Amount of items: 4
Items: 
Size: 1037 Color: 17
Size: 861 Color: 19
Size: 108 Color: 16
Size: 56 Color: 9

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1041 Color: 18
Size: 851 Color: 8
Size: 170 Color: 8

Bin 30: 2 of cap free
Amount of items: 4
Items: 
Size: 1341 Color: 16
Size: 539 Color: 9
Size: 146 Color: 11
Size: 36 Color: 19

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1459 Color: 0
Size: 603 Color: 14

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1738 Color: 19
Size: 324 Color: 4

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 11
Size: 237 Color: 15
Size: 44 Color: 2

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 15
Size: 232 Color: 12
Size: 4 Color: 19

Bin 35: 3 of cap free
Amount of items: 4
Items: 
Size: 1034 Color: 13
Size: 678 Color: 7
Size: 301 Color: 7
Size: 48 Color: 18

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 14
Size: 531 Color: 19
Size: 60 Color: 17

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1842 Color: 4
Size: 219 Color: 7

Bin 38: 5 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 9
Size: 257 Color: 10
Size: 8 Color: 19

Bin 39: 6 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 2
Size: 793 Color: 11
Size: 84 Color: 18

Bin 40: 6 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 3
Size: 691 Color: 6
Size: 130 Color: 19

Bin 41: 6 of cap free
Amount of items: 2
Items: 
Size: 1797 Color: 15
Size: 261 Color: 8

Bin 42: 8 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 19
Size: 696 Color: 16
Size: 106 Color: 11

Bin 43: 8 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 18
Size: 389 Color: 8
Size: 241 Color: 11

Bin 44: 8 of cap free
Amount of items: 2
Items: 
Size: 1594 Color: 17
Size: 462 Color: 9

Bin 45: 8 of cap free
Amount of items: 2
Items: 
Size: 1694 Color: 16
Size: 362 Color: 15

Bin 46: 9 of cap free
Amount of items: 18
Items: 
Size: 375 Color: 14
Size: 186 Color: 16
Size: 182 Color: 4
Size: 170 Color: 18
Size: 124 Color: 8
Size: 104 Color: 16
Size: 96 Color: 9
Size: 90 Color: 2
Size: 86 Color: 5
Size: 84 Color: 15
Size: 78 Color: 11
Size: 76 Color: 17
Size: 76 Color: 15
Size: 74 Color: 11
Size: 72 Color: 14
Size: 68 Color: 3
Size: 68 Color: 1
Size: 46 Color: 13

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1193 Color: 3
Size: 862 Color: 8

Bin 48: 9 of cap free
Amount of items: 2
Items: 
Size: 1614 Color: 11
Size: 441 Color: 10

Bin 49: 9 of cap free
Amount of items: 2
Items: 
Size: 1741 Color: 14
Size: 314 Color: 7

Bin 50: 11 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 2
Size: 802 Color: 2
Size: 206 Color: 13

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1427 Color: 1
Size: 625 Color: 14

Bin 52: 12 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 17
Size: 310 Color: 8
Size: 52 Color: 2

Bin 53: 13 of cap free
Amount of items: 2
Items: 
Size: 1517 Color: 9
Size: 534 Color: 16

Bin 54: 13 of cap free
Amount of items: 2
Items: 
Size: 1546 Color: 13
Size: 505 Color: 4

Bin 55: 13 of cap free
Amount of items: 2
Items: 
Size: 1777 Color: 4
Size: 274 Color: 11

Bin 56: 15 of cap free
Amount of items: 2
Items: 
Size: 1217 Color: 9
Size: 832 Color: 11

Bin 57: 15 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 2
Size: 476 Color: 17
Size: 60 Color: 19

Bin 58: 15 of cap free
Amount of items: 2
Items: 
Size: 1655 Color: 4
Size: 394 Color: 0

Bin 59: 18 of cap free
Amount of items: 2
Items: 
Size: 1296 Color: 19
Size: 750 Color: 4

Bin 60: 20 of cap free
Amount of items: 2
Items: 
Size: 1419 Color: 5
Size: 625 Color: 1

Bin 61: 20 of cap free
Amount of items: 2
Items: 
Size: 1703 Color: 19
Size: 341 Color: 9

Bin 62: 22 of cap free
Amount of items: 2
Items: 
Size: 1611 Color: 10
Size: 431 Color: 14

Bin 63: 23 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 6
Size: 244 Color: 4
Size: 170 Color: 2

Bin 64: 29 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 18
Size: 457 Color: 2
Size: 228 Color: 3

Bin 65: 29 of cap free
Amount of items: 2
Items: 
Size: 1537 Color: 12
Size: 498 Color: 5

Bin 66: 1660 of cap free
Amount of items: 9
Items: 
Size: 60 Color: 0
Size: 52 Color: 19
Size: 46 Color: 12
Size: 44 Color: 5
Size: 44 Color: 3
Size: 42 Color: 13
Size: 40 Color: 18
Size: 40 Color: 10
Size: 36 Color: 17

Total size: 134160
Total free space: 2064

