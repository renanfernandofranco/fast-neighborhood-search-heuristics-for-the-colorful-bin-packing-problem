Capicity Bin: 9824
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 4918 Color: 8
Size: 2374 Color: 6
Size: 2356 Color: 15
Size: 176 Color: 8

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 4952 Color: 12
Size: 4080 Color: 2
Size: 420 Color: 8
Size: 204 Color: 9
Size: 168 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5456 Color: 1
Size: 4088 Color: 18
Size: 280 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6144 Color: 17
Size: 3476 Color: 2
Size: 204 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6224 Color: 13
Size: 3496 Color: 9
Size: 104 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 2
Size: 2440 Color: 3
Size: 544 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6904 Color: 9
Size: 2544 Color: 15
Size: 376 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 10
Size: 2084 Color: 4
Size: 416 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7512 Color: 1
Size: 1764 Color: 2
Size: 548 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7610 Color: 2
Size: 2006 Color: 2
Size: 208 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7924 Color: 11
Size: 1084 Color: 14
Size: 816 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 8026 Color: 19
Size: 1578 Color: 1
Size: 220 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8040 Color: 16
Size: 1096 Color: 16
Size: 688 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 8062 Color: 12
Size: 1160 Color: 13
Size: 602 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8080 Color: 0
Size: 1120 Color: 11
Size: 624 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8208 Color: 7
Size: 1456 Color: 13
Size: 160 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 15
Size: 1316 Color: 10
Size: 292 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 8308 Color: 19
Size: 1268 Color: 14
Size: 248 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 8362 Color: 4
Size: 1290 Color: 2
Size: 172 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8412 Color: 16
Size: 712 Color: 8
Size: 700 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8426 Color: 14
Size: 918 Color: 17
Size: 480 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8432 Color: 6
Size: 1212 Color: 2
Size: 180 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 8500 Color: 6
Size: 828 Color: 15
Size: 496 Color: 5

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 15
Size: 704 Color: 8
Size: 600 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 8532 Color: 12
Size: 808 Color: 15
Size: 484 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8584 Color: 5
Size: 1208 Color: 0
Size: 32 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8610 Color: 15
Size: 702 Color: 8
Size: 512 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8596 Color: 14
Size: 1092 Color: 0
Size: 136 Color: 18

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 8720 Color: 6
Size: 944 Color: 10
Size: 160 Color: 5

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8774 Color: 9
Size: 822 Color: 19
Size: 228 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 8784 Color: 10
Size: 688 Color: 16
Size: 352 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 19
Size: 800 Color: 16
Size: 216 Color: 2

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 6171 Color: 1
Size: 3312 Color: 17
Size: 340 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 6514 Color: 5
Size: 3021 Color: 7
Size: 288 Color: 5

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 4
Size: 3013 Color: 5
Size: 256 Color: 18

Bin 36: 2 of cap free
Amount of items: 7
Items: 
Size: 4914 Color: 1
Size: 1088 Color: 13
Size: 1014 Color: 13
Size: 904 Color: 4
Size: 878 Color: 9
Size: 816 Color: 11
Size: 208 Color: 11

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 6078 Color: 0
Size: 3448 Color: 10
Size: 296 Color: 15

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 6204 Color: 10
Size: 3122 Color: 4
Size: 496 Color: 4

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 12
Size: 2048 Color: 1
Size: 818 Color: 17

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 7550 Color: 7
Size: 2272 Color: 10

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 7742 Color: 5
Size: 1808 Color: 13
Size: 272 Color: 18

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 7782 Color: 9
Size: 1848 Color: 15
Size: 192 Color: 10

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 7800 Color: 3
Size: 1702 Color: 2
Size: 320 Color: 17

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 7934 Color: 0
Size: 1648 Color: 18
Size: 240 Color: 4

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 8134 Color: 11
Size: 1688 Color: 1

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 8230 Color: 18
Size: 1360 Color: 2
Size: 232 Color: 11

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 8376 Color: 16
Size: 1446 Color: 11

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 8578 Color: 17
Size: 1168 Color: 4
Size: 76 Color: 19

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 8660 Color: 5
Size: 1162 Color: 16

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 8714 Color: 11
Size: 1108 Color: 12

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 8726 Color: 18
Size: 712 Color: 11
Size: 384 Color: 2

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 5601 Color: 12
Size: 4028 Color: 4
Size: 192 Color: 3

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 6776 Color: 16
Size: 3045 Color: 17

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 5956 Color: 12
Size: 3020 Color: 11
Size: 844 Color: 0

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 7184 Color: 4
Size: 2396 Color: 13
Size: 240 Color: 18

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 7266 Color: 16
Size: 2426 Color: 17
Size: 128 Color: 16

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 7716 Color: 2
Size: 2104 Color: 9

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 8180 Color: 12
Size: 1640 Color: 4

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 8232 Color: 13
Size: 1588 Color: 17

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 8340 Color: 7
Size: 856 Color: 18
Size: 624 Color: 11

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 8490 Color: 8
Size: 1330 Color: 14

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 8620 Color: 15
Size: 880 Color: 4
Size: 320 Color: 2

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 8836 Color: 0
Size: 968 Color: 5
Size: 16 Color: 3

Bin 64: 6 of cap free
Amount of items: 4
Items: 
Size: 5872 Color: 10
Size: 2118 Color: 3
Size: 1652 Color: 8
Size: 176 Color: 7

Bin 65: 6 of cap free
Amount of items: 3
Items: 
Size: 6954 Color: 11
Size: 2584 Color: 7
Size: 280 Color: 18

Bin 66: 6 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 2
Size: 2552 Color: 8
Size: 288 Color: 2

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 7056 Color: 15
Size: 2762 Color: 10

Bin 68: 6 of cap free
Amount of items: 3
Items: 
Size: 7440 Color: 10
Size: 2090 Color: 11
Size: 288 Color: 16

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 7594 Color: 13
Size: 2224 Color: 6

Bin 70: 6 of cap free
Amount of items: 3
Items: 
Size: 7624 Color: 6
Size: 2026 Color: 10
Size: 168 Color: 19

Bin 71: 6 of cap free
Amount of items: 3
Items: 
Size: 7844 Color: 5
Size: 1898 Color: 11
Size: 76 Color: 19

Bin 72: 6 of cap free
Amount of items: 3
Items: 
Size: 7988 Color: 16
Size: 1502 Color: 2
Size: 328 Color: 12

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 8496 Color: 14
Size: 1322 Color: 19

Bin 74: 6 of cap free
Amount of items: 3
Items: 
Size: 8744 Color: 13
Size: 1042 Color: 16
Size: 32 Color: 11

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 6296 Color: 4
Size: 3521 Color: 15

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 6784 Color: 3
Size: 3033 Color: 8

Bin 77: 8 of cap free
Amount of items: 7
Items: 
Size: 4916 Color: 12
Size: 1532 Color: 0
Size: 1116 Color: 4
Size: 1114 Color: 17
Size: 602 Color: 1
Size: 376 Color: 14
Size: 160 Color: 3

Bin 78: 8 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 2
Size: 4068 Color: 7
Size: 816 Color: 8

Bin 79: 8 of cap free
Amount of items: 4
Items: 
Size: 4944 Color: 10
Size: 4072 Color: 7
Size: 416 Color: 14
Size: 384 Color: 16

Bin 80: 8 of cap free
Amount of items: 3
Items: 
Size: 6728 Color: 16
Size: 3024 Color: 5
Size: 64 Color: 8

Bin 81: 8 of cap free
Amount of items: 3
Items: 
Size: 6878 Color: 18
Size: 2394 Color: 8
Size: 544 Color: 0

Bin 82: 8 of cap free
Amount of items: 3
Items: 
Size: 8088 Color: 1
Size: 928 Color: 14
Size: 800 Color: 2

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 8372 Color: 19
Size: 1444 Color: 14

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 8680 Color: 18
Size: 1136 Color: 3

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 8768 Color: 16
Size: 1048 Color: 10

Bin 86: 12 of cap free
Amount of items: 3
Items: 
Size: 6864 Color: 17
Size: 1478 Color: 4
Size: 1470 Color: 9

Bin 87: 12 of cap free
Amount of items: 3
Items: 
Size: 7284 Color: 19
Size: 2052 Color: 9
Size: 476 Color: 16

Bin 88: 12 of cap free
Amount of items: 3
Items: 
Size: 8092 Color: 15
Size: 1672 Color: 18
Size: 48 Color: 3

Bin 89: 12 of cap free
Amount of items: 2
Items: 
Size: 8440 Color: 4
Size: 1372 Color: 10

Bin 90: 12 of cap free
Amount of items: 2
Items: 
Size: 8632 Color: 15
Size: 1180 Color: 5

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 8812 Color: 0
Size: 1000 Color: 18

Bin 92: 14 of cap free
Amount of items: 2
Items: 
Size: 5550 Color: 3
Size: 4260 Color: 16

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 7318 Color: 19
Size: 2492 Color: 13

Bin 94: 14 of cap free
Amount of items: 2
Items: 
Size: 8838 Color: 11
Size: 972 Color: 1

Bin 95: 15 of cap free
Amount of items: 9
Items: 
Size: 4913 Color: 18
Size: 816 Color: 17
Size: 808 Color: 9
Size: 800 Color: 16
Size: 672 Color: 2
Size: 640 Color: 3
Size: 480 Color: 10
Size: 368 Color: 17
Size: 312 Color: 5

Bin 96: 16 of cap free
Amount of items: 3
Items: 
Size: 6232 Color: 13
Size: 3000 Color: 5
Size: 576 Color: 0

Bin 97: 16 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 7
Size: 2458 Color: 1
Size: 64 Color: 1

Bin 98: 17 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 5
Size: 3507 Color: 0
Size: 640 Color: 16

Bin 99: 18 of cap free
Amount of items: 23
Items: 
Size: 608 Color: 16
Size: 606 Color: 17
Size: 592 Color: 19
Size: 576 Color: 1
Size: 568 Color: 6
Size: 512 Color: 3
Size: 488 Color: 4
Size: 472 Color: 19
Size: 472 Color: 10
Size: 464 Color: 16
Size: 424 Color: 18
Size: 416 Color: 15
Size: 416 Color: 11
Size: 416 Color: 0
Size: 352 Color: 14
Size: 352 Color: 13
Size: 344 Color: 8
Size: 336 Color: 9
Size: 320 Color: 7
Size: 312 Color: 14
Size: 312 Color: 4
Size: 256 Color: 5
Size: 192 Color: 11

Bin 100: 20 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 7
Size: 2736 Color: 6
Size: 64 Color: 13

Bin 101: 20 of cap free
Amount of items: 3
Items: 
Size: 7604 Color: 12
Size: 2000 Color: 15
Size: 200 Color: 18

Bin 102: 20 of cap free
Amount of items: 2
Items: 
Size: 8268 Color: 8
Size: 1536 Color: 14

Bin 103: 22 of cap free
Amount of items: 2
Items: 
Size: 7668 Color: 3
Size: 2134 Color: 6

Bin 104: 22 of cap free
Amount of items: 2
Items: 
Size: 8756 Color: 7
Size: 1046 Color: 1

Bin 105: 26 of cap free
Amount of items: 5
Items: 
Size: 4920 Color: 9
Size: 3126 Color: 10
Size: 1336 Color: 3
Size: 224 Color: 13
Size: 192 Color: 4

Bin 106: 26 of cap free
Amount of items: 3
Items: 
Size: 5546 Color: 14
Size: 3900 Color: 15
Size: 352 Color: 16

Bin 107: 26 of cap free
Amount of items: 3
Items: 
Size: 6576 Color: 5
Size: 2726 Color: 10
Size: 496 Color: 0

Bin 108: 28 of cap free
Amount of items: 2
Items: 
Size: 8386 Color: 12
Size: 1410 Color: 19

Bin 109: 30 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 6
Size: 3521 Color: 2
Size: 88 Color: 13

Bin 110: 30 of cap free
Amount of items: 2
Items: 
Size: 8298 Color: 7
Size: 1496 Color: 3

Bin 111: 32 of cap free
Amount of items: 2
Items: 
Size: 7304 Color: 6
Size: 2488 Color: 1

Bin 112: 32 of cap free
Amount of items: 2
Items: 
Size: 7864 Color: 14
Size: 1928 Color: 6

Bin 113: 34 of cap free
Amount of items: 2
Items: 
Size: 6914 Color: 8
Size: 2876 Color: 3

Bin 114: 36 of cap free
Amount of items: 2
Items: 
Size: 6836 Color: 14
Size: 2952 Color: 11

Bin 115: 36 of cap free
Amount of items: 2
Items: 
Size: 7664 Color: 14
Size: 2124 Color: 19

Bin 116: 38 of cap free
Amount of items: 2
Items: 
Size: 8434 Color: 3
Size: 1352 Color: 14

Bin 117: 40 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 10
Size: 4084 Color: 2
Size: 704 Color: 16

Bin 118: 40 of cap free
Amount of items: 2
Items: 
Size: 6556 Color: 18
Size: 3228 Color: 8

Bin 119: 49 of cap free
Amount of items: 2
Items: 
Size: 6209 Color: 7
Size: 3566 Color: 16

Bin 120: 52 of cap free
Amount of items: 2
Items: 
Size: 7556 Color: 17
Size: 2216 Color: 13

Bin 121: 56 of cap free
Amount of items: 2
Items: 
Size: 7288 Color: 11
Size: 2480 Color: 13

Bin 122: 60 of cap free
Amount of items: 2
Items: 
Size: 7856 Color: 15
Size: 1908 Color: 12

Bin 123: 61 of cap free
Amount of items: 2
Items: 
Size: 6201 Color: 1
Size: 3562 Color: 18

Bin 124: 84 of cap free
Amount of items: 2
Items: 
Size: 6380 Color: 12
Size: 3360 Color: 4

Bin 125: 86 of cap free
Amount of items: 2
Items: 
Size: 5644 Color: 18
Size: 4094 Color: 10

Bin 126: 86 of cap free
Amount of items: 2
Items: 
Size: 6074 Color: 0
Size: 3664 Color: 18

Bin 127: 93 of cap free
Amount of items: 2
Items: 
Size: 5640 Color: 15
Size: 4091 Color: 18

Bin 128: 96 of cap free
Amount of items: 3
Items: 
Size: 5704 Color: 2
Size: 2724 Color: 1
Size: 1300 Color: 14

Bin 129: 97 of cap free
Amount of items: 3
Items: 
Size: 5599 Color: 7
Size: 3904 Color: 11
Size: 224 Color: 9

Bin 130: 114 of cap free
Amount of items: 2
Items: 
Size: 5617 Color: 18
Size: 4093 Color: 15

Bin 131: 129 of cap free
Amount of items: 5
Items: 
Size: 4917 Color: 2
Size: 1892 Color: 19
Size: 1846 Color: 12
Size: 892 Color: 14
Size: 148 Color: 17

Bin 132: 138 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 0
Size: 4090 Color: 12
Size: 648 Color: 13

Bin 133: 7636 of cap free
Amount of items: 10
Items: 
Size: 304 Color: 1
Size: 264 Color: 14
Size: 256 Color: 18
Size: 256 Color: 3
Size: 224 Color: 2
Size: 192 Color: 9
Size: 192 Color: 5
Size: 176 Color: 0
Size: 164 Color: 10
Size: 160 Color: 13

Total size: 1296768
Total free space: 9824

