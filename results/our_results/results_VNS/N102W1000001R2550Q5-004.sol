Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 436632 Color: 2
Size: 313367 Color: 3
Size: 250002 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 458931 Color: 0
Size: 280550 Color: 1
Size: 260520 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 368092 Color: 2
Size: 363013 Color: 1
Size: 268896 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 398225 Color: 2
Size: 315448 Color: 4
Size: 286328 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 372033 Color: 2
Size: 356665 Color: 4
Size: 271303 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 394122 Color: 0
Size: 345510 Color: 2
Size: 260369 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 416549 Color: 3
Size: 284085 Color: 0
Size: 299367 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 421144 Color: 0
Size: 316891 Color: 2
Size: 261966 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 418252 Color: 1
Size: 258528 Color: 1
Size: 323221 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 364179 Color: 4
Size: 359541 Color: 1
Size: 276281 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 318082 Color: 2
Size: 283801 Color: 0
Size: 398118 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 386919 Color: 4
Size: 320963 Color: 1
Size: 292119 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 377346 Color: 0
Size: 357933 Color: 3
Size: 264722 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 381390 Color: 2
Size: 295351 Color: 3
Size: 323260 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 367056 Color: 3
Size: 334728 Color: 2
Size: 298217 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 488287 Color: 2
Size: 256707 Color: 3
Size: 255007 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 368763 Color: 4
Size: 325765 Color: 1
Size: 305473 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 369439 Color: 1
Size: 331908 Color: 0
Size: 298654 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 355456 Color: 2
Size: 350211 Color: 3
Size: 294334 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 331712 Color: 0
Size: 317804 Color: 4
Size: 350485 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 335242 Color: 1
Size: 318239 Color: 2
Size: 346520 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 358891 Color: 1
Size: 363557 Color: 2
Size: 277553 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 388833 Color: 0
Size: 322410 Color: 3
Size: 288758 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 386115 Color: 0
Size: 320662 Color: 2
Size: 293224 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 389834 Color: 0
Size: 321940 Color: 1
Size: 288227 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 396916 Color: 3
Size: 320247 Color: 4
Size: 282838 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 401242 Color: 4
Size: 307772 Color: 1
Size: 290987 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 440994 Color: 1
Size: 288239 Color: 3
Size: 270768 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 447351 Color: 1
Size: 280720 Color: 1
Size: 271930 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 454621 Color: 2
Size: 294166 Color: 3
Size: 251214 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 456269 Color: 1
Size: 286376 Color: 0
Size: 257356 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 481040 Color: 1
Size: 267636 Color: 4
Size: 251325 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 491384 Color: 0
Size: 255773 Color: 3
Size: 252844 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 499297 Color: 0
Size: 250389 Color: 1
Size: 250315 Color: 2

Total size: 34000034
Total free space: 0

