Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 12
Items: 
Size: 8146 Color: 4
Size: 834 Color: 1
Size: 820 Color: 7
Size: 800 Color: 11
Size: 800 Color: 4
Size: 796 Color: 11
Size: 792 Color: 19
Size: 768 Color: 1
Size: 768 Color: 1
Size: 720 Color: 19
Size: 644 Color: 14
Size: 400 Color: 15

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 8154 Color: 19
Size: 3776 Color: 7
Size: 3222 Color: 18
Size: 792 Color: 2
Size: 344 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8228 Color: 4
Size: 6708 Color: 14
Size: 1352 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 18
Size: 6768 Color: 14
Size: 1036 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 1
Size: 5084 Color: 10
Size: 1608 Color: 12

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 10240 Color: 12
Size: 6048 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 8
Size: 4328 Color: 14
Size: 848 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 6
Size: 3928 Color: 15
Size: 888 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11540 Color: 7
Size: 3988 Color: 14
Size: 760 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11674 Color: 1
Size: 3994 Color: 2
Size: 620 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11723 Color: 12
Size: 3101 Color: 10
Size: 1464 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11792 Color: 5
Size: 3792 Color: 14
Size: 704 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12242 Color: 8
Size: 3374 Color: 4
Size: 672 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 0
Size: 3524 Color: 13
Size: 456 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 1
Size: 3568 Color: 16
Size: 324 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12432 Color: 5
Size: 3560 Color: 8
Size: 296 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 0
Size: 3220 Color: 17
Size: 476 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12700 Color: 16
Size: 3216 Color: 18
Size: 372 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12725 Color: 17
Size: 2971 Color: 5
Size: 592 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12804 Color: 9
Size: 2056 Color: 2
Size: 1428 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12910 Color: 0
Size: 1816 Color: 3
Size: 1562 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12912 Color: 11
Size: 2728 Color: 5
Size: 648 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 17
Size: 3244 Color: 8
Size: 120 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13172 Color: 19
Size: 1656 Color: 19
Size: 1460 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13173 Color: 12
Size: 2597 Color: 19
Size: 518 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 16
Size: 2152 Color: 13
Size: 944 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13314 Color: 16
Size: 2050 Color: 19
Size: 924 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13394 Color: 3
Size: 2482 Color: 9
Size: 412 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13397 Color: 9
Size: 2411 Color: 2
Size: 480 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13524 Color: 5
Size: 2188 Color: 4
Size: 576 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 0
Size: 2604 Color: 1
Size: 284 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 13
Size: 2212 Color: 1
Size: 576 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 19
Size: 2324 Color: 11
Size: 450 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13626 Color: 5
Size: 1670 Color: 14
Size: 992 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13649 Color: 19
Size: 2217 Color: 0
Size: 422 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 6
Size: 2108 Color: 19
Size: 512 Color: 11

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 5
Size: 2024 Color: 9
Size: 544 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 12
Size: 2044 Color: 19
Size: 480 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13830 Color: 19
Size: 1754 Color: 4
Size: 704 Color: 15

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13864 Color: 8
Size: 1512 Color: 7
Size: 912 Color: 5

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 13
Size: 2020 Color: 8
Size: 368 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13968 Color: 4
Size: 2008 Color: 5
Size: 312 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 3
Size: 1886 Color: 15
Size: 376 Color: 7

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 18
Size: 1884 Color: 12
Size: 376 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 5
Size: 1152 Color: 3
Size: 1016 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 1144 Color: 18
Size: 976 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14220 Color: 2
Size: 1508 Color: 2
Size: 560 Color: 19

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14224 Color: 15
Size: 1456 Color: 19
Size: 608 Color: 5

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14252 Color: 8
Size: 1588 Color: 4
Size: 448 Color: 17

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14348 Color: 5
Size: 1564 Color: 18
Size: 376 Color: 12

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 5
Size: 1400 Color: 10
Size: 528 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14366 Color: 15
Size: 1602 Color: 8
Size: 320 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 5
Size: 1356 Color: 9
Size: 544 Color: 10

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14458 Color: 16
Size: 1344 Color: 18
Size: 486 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 1
Size: 1744 Color: 8
Size: 60 Color: 10

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 19
Size: 1474 Color: 8
Size: 292 Color: 7

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14536 Color: 16
Size: 1336 Color: 2
Size: 416 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14580 Color: 4
Size: 960 Color: 9
Size: 748 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14578 Color: 8
Size: 1264 Color: 8
Size: 446 Color: 6

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 15
Size: 1344 Color: 18
Size: 332 Color: 13

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 14
Size: 1112 Color: 19
Size: 560 Color: 4

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 9113 Color: 4
Size: 6782 Color: 0
Size: 392 Color: 14

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10653 Color: 1
Size: 4450 Color: 1
Size: 1184 Color: 19

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 2
Size: 2801 Color: 6
Size: 1404 Color: 15

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 14
Size: 3495 Color: 18
Size: 592 Color: 5

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13018 Color: 10
Size: 2117 Color: 5
Size: 1152 Color: 17

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 8
Size: 2413 Color: 9
Size: 558 Color: 5

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13393 Color: 4
Size: 2210 Color: 6
Size: 684 Color: 19

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13577 Color: 18
Size: 1662 Color: 15
Size: 1048 Color: 19

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13601 Color: 16
Size: 2414 Color: 4
Size: 272 Color: 10

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13749 Color: 17
Size: 2010 Color: 10
Size: 528 Color: 15

Bin 72: 2 of cap free
Amount of items: 9
Items: 
Size: 8148 Color: 5
Size: 1352 Color: 14
Size: 1194 Color: 2
Size: 1180 Color: 3
Size: 1168 Color: 13
Size: 1088 Color: 6
Size: 848 Color: 1
Size: 796 Color: 13
Size: 512 Color: 19

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 10591 Color: 3
Size: 5247 Color: 13
Size: 448 Color: 12

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 17
Size: 5090 Color: 5
Size: 576 Color: 13

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 10836 Color: 18
Size: 4968 Color: 9
Size: 482 Color: 5

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 10838 Color: 13
Size: 4104 Color: 1
Size: 1344 Color: 6

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 14
Size: 3392 Color: 0
Size: 1526 Color: 7

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11478 Color: 8
Size: 4528 Color: 8
Size: 280 Color: 1

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 9
Size: 3972 Color: 13
Size: 784 Color: 8

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 12183 Color: 10
Size: 4103 Color: 18

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 14
Size: 2650 Color: 6

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 13878 Color: 2
Size: 2408 Color: 18

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 13978 Color: 1
Size: 2308 Color: 18

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 14334 Color: 14
Size: 1952 Color: 18

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 14562 Color: 13
Size: 1724 Color: 6

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 14634 Color: 15
Size: 1620 Color: 2
Size: 32 Color: 6

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 13017 Color: 4
Size: 2804 Color: 15
Size: 464 Color: 2

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 13110 Color: 3
Size: 3175 Color: 16

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 13313 Color: 6
Size: 2832 Color: 2
Size: 140 Color: 0

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 13921 Color: 3
Size: 1964 Color: 8
Size: 400 Color: 19

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 14312 Color: 11
Size: 1973 Color: 12

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 8452 Color: 14
Size: 6776 Color: 1
Size: 1056 Color: 19

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 4
Size: 5300 Color: 19
Size: 192 Color: 7

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 10
Size: 4604 Color: 3
Size: 484 Color: 6

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 11232 Color: 7
Size: 4620 Color: 5
Size: 432 Color: 1

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 11316 Color: 3
Size: 4968 Color: 9

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 11524 Color: 8
Size: 4584 Color: 11
Size: 176 Color: 3

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 12140 Color: 5
Size: 4010 Color: 6
Size: 134 Color: 11

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 17
Size: 2208 Color: 19
Size: 1060 Color: 13

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 13868 Color: 4
Size: 2416 Color: 7

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 17
Size: 2096 Color: 3

Bin 102: 5 of cap free
Amount of items: 11
Items: 
Size: 8147 Color: 3
Size: 976 Color: 12
Size: 976 Color: 10
Size: 948 Color: 10
Size: 938 Color: 16
Size: 920 Color: 0
Size: 908 Color: 0
Size: 896 Color: 10
Size: 848 Color: 16
Size: 442 Color: 17
Size: 284 Color: 1

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 10070 Color: 5
Size: 5981 Color: 2
Size: 232 Color: 9

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 10376 Color: 8
Size: 5907 Color: 0

Bin 105: 5 of cap free
Amount of items: 3
Items: 
Size: 12095 Color: 14
Size: 3964 Color: 1
Size: 224 Color: 5

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 13556 Color: 4
Size: 2727 Color: 12

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 13809 Color: 8
Size: 2474 Color: 7

Bin 108: 6 of cap free
Amount of items: 7
Items: 
Size: 8152 Color: 6
Size: 1442 Color: 12
Size: 1426 Color: 8
Size: 1382 Color: 9
Size: 1356 Color: 2
Size: 1352 Color: 17
Size: 1172 Color: 0

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 2
Size: 3506 Color: 12

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 13986 Color: 8
Size: 2296 Color: 9

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 16
Size: 2222 Color: 19

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 14092 Color: 13
Size: 2190 Color: 17

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 14286 Color: 7
Size: 1996 Color: 9

Bin 114: 6 of cap free
Amount of items: 2
Items: 
Size: 14540 Color: 8
Size: 1742 Color: 11

Bin 115: 7 of cap free
Amount of items: 14
Items: 
Size: 8145 Color: 15
Size: 704 Color: 6
Size: 704 Color: 2
Size: 700 Color: 18
Size: 698 Color: 10
Size: 672 Color: 1
Size: 656 Color: 11
Size: 640 Color: 19
Size: 640 Color: 11
Size: 634 Color: 19
Size: 624 Color: 5
Size: 520 Color: 3
Size: 512 Color: 14
Size: 432 Color: 17

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 9142 Color: 5
Size: 6787 Color: 1
Size: 352 Color: 18

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 11658 Color: 3
Size: 4623 Color: 19

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 8692 Color: 7
Size: 6772 Color: 17
Size: 816 Color: 4

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 10864 Color: 16
Size: 5416 Color: 17

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 13633 Color: 15
Size: 2647 Color: 12

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 13696 Color: 4
Size: 2584 Color: 7

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 14420 Color: 0
Size: 1860 Color: 18

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 12479 Color: 12
Size: 3440 Color: 5
Size: 360 Color: 14

Bin 124: 10 of cap free
Amount of items: 7
Items: 
Size: 8168 Color: 5
Size: 1902 Color: 8
Size: 1824 Color: 4
Size: 1468 Color: 5
Size: 1356 Color: 1
Size: 1188 Color: 17
Size: 372 Color: 9

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 18
Size: 2442 Color: 8

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 12534 Color: 17
Size: 3743 Color: 12

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 9744 Color: 2
Size: 6532 Color: 16

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 14576 Color: 5
Size: 1700 Color: 16

Bin 129: 13 of cap free
Amount of items: 3
Items: 
Size: 9201 Color: 19
Size: 6786 Color: 7
Size: 288 Color: 7

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 13365 Color: 17
Size: 2910 Color: 4

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 13818 Color: 4
Size: 2457 Color: 3

Bin 132: 14 of cap free
Amount of items: 4
Items: 
Size: 8164 Color: 16
Size: 5182 Color: 13
Size: 2528 Color: 15
Size: 400 Color: 5

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 12428 Color: 4
Size: 3846 Color: 14

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 14644 Color: 9
Size: 1630 Color: 6

Bin 135: 15 of cap free
Amount of items: 2
Items: 
Size: 13629 Color: 4
Size: 2644 Color: 19

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 14032 Color: 1
Size: 2241 Color: 0

Bin 137: 16 of cap free
Amount of items: 3
Items: 
Size: 8244 Color: 8
Size: 7676 Color: 1
Size: 352 Color: 5

Bin 138: 16 of cap free
Amount of items: 3
Items: 
Size: 10328 Color: 13
Size: 4936 Color: 11
Size: 1008 Color: 4

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 14504 Color: 15
Size: 1768 Color: 8

Bin 140: 17 of cap free
Amount of items: 2
Items: 
Size: 13362 Color: 17
Size: 2909 Color: 6

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 14010 Color: 10
Size: 2261 Color: 6

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 10408 Color: 16
Size: 5862 Color: 4

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 4
Size: 3130 Color: 0

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 9
Size: 2726 Color: 18

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 14110 Color: 17
Size: 2160 Color: 6

Bin 146: 19 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 15
Size: 6785 Color: 13
Size: 1304 Color: 9

Bin 147: 19 of cap free
Amount of items: 2
Items: 
Size: 11365 Color: 17
Size: 4904 Color: 16

Bin 148: 19 of cap free
Amount of items: 2
Items: 
Size: 13832 Color: 5
Size: 2437 Color: 13

Bin 149: 19 of cap free
Amount of items: 2
Items: 
Size: 14202 Color: 13
Size: 2067 Color: 2

Bin 150: 20 of cap free
Amount of items: 2
Items: 
Size: 9040 Color: 13
Size: 7228 Color: 6

Bin 151: 20 of cap free
Amount of items: 2
Items: 
Size: 12024 Color: 7
Size: 4244 Color: 13

Bin 152: 20 of cap free
Amount of items: 2
Items: 
Size: 13712 Color: 4
Size: 2556 Color: 19

Bin 153: 20 of cap free
Amount of items: 2
Items: 
Size: 13984 Color: 16
Size: 2284 Color: 9

Bin 154: 21 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 9
Size: 4542 Color: 19
Size: 2305 Color: 3

Bin 155: 25 of cap free
Amount of items: 2
Items: 
Size: 13296 Color: 14
Size: 2967 Color: 17

Bin 156: 28 of cap free
Amount of items: 3
Items: 
Size: 9932 Color: 14
Size: 5784 Color: 19
Size: 544 Color: 12

Bin 157: 28 of cap free
Amount of items: 2
Items: 
Size: 10196 Color: 18
Size: 6064 Color: 4

Bin 158: 29 of cap free
Amount of items: 2
Items: 
Size: 10950 Color: 7
Size: 5309 Color: 0

Bin 159: 30 of cap free
Amount of items: 2
Items: 
Size: 9262 Color: 2
Size: 6996 Color: 3

Bin 160: 30 of cap free
Amount of items: 2
Items: 
Size: 13322 Color: 18
Size: 2936 Color: 3

Bin 161: 31 of cap free
Amount of items: 2
Items: 
Size: 11508 Color: 9
Size: 4749 Color: 10

Bin 162: 31 of cap free
Amount of items: 2
Items: 
Size: 13261 Color: 8
Size: 2996 Color: 4

Bin 163: 33 of cap free
Amount of items: 3
Items: 
Size: 9993 Color: 14
Size: 5958 Color: 2
Size: 304 Color: 1

Bin 164: 34 of cap free
Amount of items: 2
Items: 
Size: 12568 Color: 13
Size: 3686 Color: 16

Bin 165: 35 of cap free
Amount of items: 2
Items: 
Size: 11277 Color: 0
Size: 4976 Color: 15

Bin 166: 35 of cap free
Amount of items: 2
Items: 
Size: 12929 Color: 4
Size: 3324 Color: 19

Bin 167: 37 of cap free
Amount of items: 2
Items: 
Size: 9919 Color: 3
Size: 6332 Color: 8

Bin 168: 37 of cap free
Amount of items: 2
Items: 
Size: 11797 Color: 9
Size: 4454 Color: 0

Bin 169: 39 of cap free
Amount of items: 2
Items: 
Size: 13341 Color: 3
Size: 2908 Color: 12

Bin 170: 40 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 3
Size: 2062 Color: 13

Bin 171: 44 of cap free
Amount of items: 3
Items: 
Size: 10344 Color: 12
Size: 5724 Color: 13
Size: 176 Color: 5

Bin 172: 44 of cap free
Amount of items: 2
Items: 
Size: 13616 Color: 13
Size: 2628 Color: 2

Bin 173: 46 of cap free
Amount of items: 2
Items: 
Size: 14418 Color: 15
Size: 1824 Color: 8

Bin 174: 46 of cap free
Amount of items: 2
Items: 
Size: 14486 Color: 1
Size: 1756 Color: 19

Bin 175: 48 of cap free
Amount of items: 2
Items: 
Size: 10440 Color: 11
Size: 5800 Color: 15

Bin 176: 50 of cap free
Amount of items: 2
Items: 
Size: 11956 Color: 14
Size: 4282 Color: 6

Bin 177: 54 of cap free
Amount of items: 2
Items: 
Size: 14416 Color: 8
Size: 1818 Color: 9

Bin 178: 58 of cap free
Amount of items: 2
Items: 
Size: 11506 Color: 3
Size: 4724 Color: 16

Bin 179: 60 of cap free
Amount of items: 2
Items: 
Size: 13116 Color: 13
Size: 3112 Color: 3

Bin 180: 61 of cap free
Amount of items: 2
Items: 
Size: 12422 Color: 3
Size: 3805 Color: 0

Bin 181: 68 of cap free
Amount of items: 2
Items: 
Size: 12799 Color: 10
Size: 3421 Color: 8

Bin 182: 70 of cap free
Amount of items: 2
Items: 
Size: 10746 Color: 7
Size: 5472 Color: 3

Bin 183: 70 of cap free
Amount of items: 2
Items: 
Size: 13706 Color: 15
Size: 2512 Color: 7

Bin 184: 74 of cap free
Amount of items: 2
Items: 
Size: 11592 Color: 11
Size: 4622 Color: 18

Bin 185: 74 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 8
Size: 3416 Color: 7

Bin 186: 78 of cap free
Amount of items: 2
Items: 
Size: 10352 Color: 11
Size: 5858 Color: 19

Bin 187: 78 of cap free
Amount of items: 2
Items: 
Size: 12194 Color: 6
Size: 4016 Color: 18

Bin 188: 78 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 11
Size: 2314 Color: 8

Bin 189: 91 of cap free
Amount of items: 2
Items: 
Size: 10741 Color: 15
Size: 5456 Color: 16

Bin 190: 93 of cap free
Amount of items: 2
Items: 
Size: 11498 Color: 9
Size: 4697 Color: 12

Bin 191: 95 of cap free
Amount of items: 2
Items: 
Size: 12016 Color: 0
Size: 4177 Color: 12

Bin 192: 109 of cap free
Amount of items: 2
Items: 
Size: 12567 Color: 0
Size: 3612 Color: 4

Bin 193: 118 of cap free
Amount of items: 2
Items: 
Size: 10182 Color: 6
Size: 5988 Color: 5

Bin 194: 132 of cap free
Amount of items: 3
Items: 
Size: 9352 Color: 4
Size: 3986 Color: 3
Size: 2818 Color: 17

Bin 195: 160 of cap free
Amount of items: 2
Items: 
Size: 9336 Color: 18
Size: 6792 Color: 2

Bin 196: 189 of cap free
Amount of items: 5
Items: 
Size: 8176 Color: 5
Size: 2505 Color: 7
Size: 1930 Color: 16
Size: 1904 Color: 18
Size: 1584 Color: 19

Bin 197: 206 of cap free
Amount of items: 38
Items: 
Size: 624 Color: 12
Size: 608 Color: 1
Size: 580 Color: 8
Size: 580 Color: 4
Size: 544 Color: 6
Size: 512 Color: 13
Size: 492 Color: 12
Size: 492 Color: 2
Size: 490 Color: 0
Size: 480 Color: 6
Size: 480 Color: 0
Size: 460 Color: 2
Size: 440 Color: 8
Size: 440 Color: 0
Size: 416 Color: 16
Size: 416 Color: 9
Size: 416 Color: 2
Size: 408 Color: 9
Size: 408 Color: 6
Size: 408 Color: 6
Size: 408 Color: 3
Size: 400 Color: 12
Size: 394 Color: 5
Size: 384 Color: 15
Size: 384 Color: 7
Size: 384 Color: 3
Size: 352 Color: 19
Size: 352 Color: 15
Size: 352 Color: 14
Size: 352 Color: 7
Size: 350 Color: 16
Size: 348 Color: 17
Size: 344 Color: 9
Size: 344 Color: 3
Size: 328 Color: 14
Size: 320 Color: 17
Size: 320 Color: 5
Size: 272 Color: 14

Bin 198: 246 of cap free
Amount of items: 2
Items: 
Size: 9254 Color: 4
Size: 6788 Color: 8

Bin 199: 12492 of cap free
Amount of items: 13
Items: 
Size: 336 Color: 16
Size: 320 Color: 19
Size: 320 Color: 15
Size: 320 Color: 4
Size: 308 Color: 2
Size: 304 Color: 7
Size: 288 Color: 11
Size: 288 Color: 9
Size: 272 Color: 17
Size: 272 Color: 5
Size: 256 Color: 17
Size: 256 Color: 15
Size: 256 Color: 14

Total size: 3225024
Total free space: 16288

