Capicity Bin: 8064
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4041 Color: 1
Size: 985 Color: 1
Size: 846 Color: 0
Size: 844 Color: 0
Size: 792 Color: 1
Size: 348 Color: 0
Size: 208 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4040 Color: 0
Size: 3248 Color: 1
Size: 776 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 0
Size: 3348 Color: 1
Size: 664 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4378 Color: 1
Size: 3362 Color: 1
Size: 324 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4536 Color: 1
Size: 3384 Color: 0
Size: 144 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4968 Color: 1
Size: 2952 Color: 0
Size: 144 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5046 Color: 0
Size: 2518 Color: 0
Size: 500 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 0
Size: 2756 Color: 0
Size: 152 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5182 Color: 0
Size: 2746 Color: 1
Size: 136 Color: 1

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 5336 Color: 0
Size: 1564 Color: 1
Size: 1020 Color: 1
Size: 144 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5639 Color: 0
Size: 2243 Color: 1
Size: 182 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 0
Size: 2018 Color: 1
Size: 400 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 1
Size: 1908 Color: 0
Size: 152 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6697 Color: 0
Size: 1075 Color: 1
Size: 292 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6774 Color: 0
Size: 978 Color: 1
Size: 312 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6799 Color: 0
Size: 945 Color: 0
Size: 320 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 0
Size: 664 Color: 1
Size: 576 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6870 Color: 1
Size: 686 Color: 0
Size: 508 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6904 Color: 1
Size: 964 Color: 1
Size: 196 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6894 Color: 0
Size: 880 Color: 0
Size: 290 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6958 Color: 1
Size: 694 Color: 1
Size: 412 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 0
Size: 524 Color: 0
Size: 450 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 0
Size: 636 Color: 1
Size: 280 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7210 Color: 1
Size: 714 Color: 1
Size: 140 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 4050 Color: 1
Size: 3357 Color: 1
Size: 656 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 4979 Color: 1
Size: 2720 Color: 0
Size: 364 Color: 1

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 5915 Color: 1
Size: 2148 Color: 0

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 6492 Color: 1
Size: 1571 Color: 0

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 6567 Color: 1
Size: 1496 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 0
Size: 1187 Color: 0
Size: 132 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 0
Size: 1078 Color: 1
Size: 224 Color: 1

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 4045 Color: 1
Size: 3353 Color: 1
Size: 664 Color: 0

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 1
Size: 2118 Color: 0
Size: 188 Color: 0

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 6102 Color: 1
Size: 1576 Color: 1
Size: 384 Color: 0

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 6798 Color: 0
Size: 960 Color: 1
Size: 304 Color: 1

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 1
Size: 1218 Color: 0

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 0
Size: 1154 Color: 1

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 7014 Color: 1
Size: 1048 Color: 0

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 7060 Color: 0
Size: 1002 Color: 1

Bin 40: 3 of cap free
Amount of items: 7
Items: 
Size: 4037 Color: 1
Size: 778 Color: 0
Size: 764 Color: 1
Size: 760 Color: 0
Size: 670 Color: 1
Size: 532 Color: 1
Size: 520 Color: 0

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 5640 Color: 1
Size: 2245 Color: 1
Size: 176 Color: 0

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 5663 Color: 1
Size: 2062 Color: 0
Size: 336 Color: 1

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 5992 Color: 1
Size: 1791 Color: 1
Size: 278 Color: 0

Bin 44: 4 of cap free
Amount of items: 12
Items: 
Size: 4034 Color: 1
Size: 480 Color: 0
Size: 480 Color: 0
Size: 448 Color: 0
Size: 440 Color: 0
Size: 424 Color: 0
Size: 400 Color: 0
Size: 364 Color: 1
Size: 256 Color: 1
Size: 256 Color: 1
Size: 254 Color: 1
Size: 224 Color: 1

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 4941 Color: 0
Size: 2943 Color: 0
Size: 176 Color: 1

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 6188 Color: 0
Size: 1872 Color: 1

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 6447 Color: 1
Size: 1249 Color: 0
Size: 364 Color: 0

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 6586 Color: 0
Size: 1474 Color: 1

Bin 49: 4 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 1
Size: 968 Color: 0

Bin 50: 4 of cap free
Amount of items: 3
Items: 
Size: 7234 Color: 0
Size: 802 Color: 1
Size: 24 Color: 0

Bin 51: 5 of cap free
Amount of items: 5
Items: 
Size: 4042 Color: 1
Size: 1141 Color: 0
Size: 1140 Color: 0
Size: 922 Color: 1
Size: 814 Color: 0

Bin 52: 5 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 1
Size: 3351 Color: 0
Size: 640 Color: 0

Bin 53: 5 of cap free
Amount of items: 3
Items: 
Size: 4506 Color: 1
Size: 3361 Color: 0
Size: 192 Color: 0

Bin 54: 5 of cap free
Amount of items: 3
Items: 
Size: 5568 Color: 1
Size: 1595 Color: 1
Size: 896 Color: 0

Bin 55: 5 of cap free
Amount of items: 2
Items: 
Size: 6323 Color: 1
Size: 1736 Color: 0

Bin 56: 5 of cap free
Amount of items: 2
Items: 
Size: 6847 Color: 0
Size: 1212 Color: 1

Bin 57: 6 of cap free
Amount of items: 2
Items: 
Size: 4910 Color: 1
Size: 3148 Color: 0

Bin 58: 6 of cap free
Amount of items: 3
Items: 
Size: 5003 Color: 0
Size: 2545 Color: 0
Size: 510 Color: 1

Bin 59: 6 of cap free
Amount of items: 2
Items: 
Size: 6440 Color: 1
Size: 1618 Color: 0

Bin 60: 6 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 1
Size: 1436 Color: 0
Size: 168 Color: 1

Bin 61: 6 of cap free
Amount of items: 4
Items: 
Size: 6552 Color: 0
Size: 1322 Color: 1
Size: 136 Color: 1
Size: 48 Color: 0

Bin 62: 7 of cap free
Amount of items: 3
Items: 
Size: 4533 Color: 1
Size: 3332 Color: 0
Size: 192 Color: 0

Bin 63: 7 of cap free
Amount of items: 2
Items: 
Size: 6298 Color: 0
Size: 1759 Color: 1

Bin 64: 7 of cap free
Amount of items: 2
Items: 
Size: 6606 Color: 1
Size: 1451 Color: 0

Bin 65: 7 of cap free
Amount of items: 2
Items: 
Size: 6931 Color: 1
Size: 1126 Color: 0

Bin 66: 8 of cap free
Amount of items: 2
Items: 
Size: 6944 Color: 0
Size: 1112 Color: 1

Bin 67: 9 of cap free
Amount of items: 2
Items: 
Size: 6391 Color: 1
Size: 1664 Color: 0

Bin 68: 9 of cap free
Amount of items: 2
Items: 
Size: 6971 Color: 0
Size: 1084 Color: 1

Bin 69: 9 of cap free
Amount of items: 2
Items: 
Size: 7144 Color: 1
Size: 911 Color: 0

Bin 70: 10 of cap free
Amount of items: 2
Items: 
Size: 6883 Color: 1
Size: 1171 Color: 0

Bin 71: 10 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 1
Size: 952 Color: 0

Bin 72: 10 of cap free
Amount of items: 2
Items: 
Size: 7242 Color: 0
Size: 812 Color: 1

Bin 73: 11 of cap free
Amount of items: 2
Items: 
Size: 6126 Color: 0
Size: 1927 Color: 1

Bin 74: 11 of cap free
Amount of items: 2
Items: 
Size: 6995 Color: 1
Size: 1058 Color: 0

Bin 75: 12 of cap free
Amount of items: 3
Items: 
Size: 4226 Color: 0
Size: 3666 Color: 1
Size: 160 Color: 0

Bin 76: 12 of cap free
Amount of items: 2
Items: 
Size: 7128 Color: 0
Size: 924 Color: 1

Bin 77: 13 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 1
Size: 2271 Color: 0

Bin 78: 13 of cap free
Amount of items: 2
Items: 
Size: 6348 Color: 1
Size: 1703 Color: 0

Bin 79: 14 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 1
Size: 3354 Color: 0
Size: 612 Color: 1

Bin 80: 14 of cap free
Amount of items: 3
Items: 
Size: 5418 Color: 1
Size: 2584 Color: 0
Size: 48 Color: 0

Bin 81: 14 of cap free
Amount of items: 3
Items: 
Size: 5526 Color: 0
Size: 2402 Color: 1
Size: 122 Color: 0

Bin 82: 14 of cap free
Amount of items: 2
Items: 
Size: 5898 Color: 1
Size: 2152 Color: 0

Bin 83: 14 of cap free
Amount of items: 2
Items: 
Size: 6682 Color: 0
Size: 1368 Color: 1

Bin 84: 14 of cap free
Amount of items: 2
Items: 
Size: 6775 Color: 1
Size: 1275 Color: 0

Bin 85: 14 of cap free
Amount of items: 2
Items: 
Size: 6858 Color: 1
Size: 1192 Color: 0

Bin 86: 16 of cap free
Amount of items: 2
Items: 
Size: 4724 Color: 0
Size: 3324 Color: 1

Bin 87: 16 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 1
Size: 2206 Color: 0
Size: 292 Color: 1

Bin 88: 16 of cap free
Amount of items: 2
Items: 
Size: 6047 Color: 1
Size: 2001 Color: 0

Bin 89: 16 of cap free
Amount of items: 2
Items: 
Size: 6347 Color: 0
Size: 1701 Color: 1

Bin 90: 18 of cap free
Amount of items: 3
Items: 
Size: 7134 Color: 1
Size: 896 Color: 0
Size: 16 Color: 1

Bin 91: 20 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 1
Size: 1272 Color: 0

Bin 92: 20 of cap free
Amount of items: 3
Items: 
Size: 7160 Color: 0
Size: 868 Color: 1
Size: 16 Color: 1

Bin 93: 21 of cap free
Amount of items: 2
Items: 
Size: 6648 Color: 0
Size: 1395 Color: 1

Bin 94: 21 of cap free
Amount of items: 2
Items: 
Size: 7028 Color: 1
Size: 1015 Color: 0

Bin 95: 21 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 0
Size: 993 Color: 1

Bin 96: 22 of cap free
Amount of items: 32
Items: 
Size: 360 Color: 0
Size: 358 Color: 0
Size: 336 Color: 0
Size: 336 Color: 0
Size: 312 Color: 0
Size: 304 Color: 0
Size: 288 Color: 0
Size: 282 Color: 0
Size: 268 Color: 0
Size: 268 Color: 0
Size: 264 Color: 0
Size: 256 Color: 0
Size: 248 Color: 1
Size: 248 Color: 1
Size: 240 Color: 1
Size: 240 Color: 1
Size: 240 Color: 1
Size: 236 Color: 1
Size: 236 Color: 1
Size: 228 Color: 0
Size: 226 Color: 0
Size: 224 Color: 0
Size: 224 Color: 0
Size: 212 Color: 1
Size: 210 Color: 1
Size: 208 Color: 1
Size: 208 Color: 1
Size: 202 Color: 1
Size: 200 Color: 1
Size: 196 Color: 1
Size: 192 Color: 1
Size: 192 Color: 1

Bin 97: 23 of cap free
Amount of items: 2
Items: 
Size: 5363 Color: 1
Size: 2678 Color: 0

Bin 98: 24 of cap free
Amount of items: 2
Items: 
Size: 4612 Color: 0
Size: 3428 Color: 1

Bin 99: 26 of cap free
Amount of items: 2
Items: 
Size: 5072 Color: 1
Size: 2966 Color: 0

Bin 100: 26 of cap free
Amount of items: 2
Items: 
Size: 6314 Color: 1
Size: 1724 Color: 0

Bin 101: 26 of cap free
Amount of items: 3
Items: 
Size: 6964 Color: 1
Size: 998 Color: 0
Size: 76 Color: 0

Bin 102: 27 of cap free
Amount of items: 2
Items: 
Size: 6203 Color: 0
Size: 1834 Color: 1

Bin 103: 28 of cap free
Amount of items: 3
Items: 
Size: 5011 Color: 1
Size: 2571 Color: 0
Size: 454 Color: 1

Bin 104: 29 of cap free
Amount of items: 2
Items: 
Size: 5484 Color: 0
Size: 2551 Color: 1

Bin 105: 30 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 1
Size: 1270 Color: 0
Size: 64 Color: 1

Bin 106: 31 of cap free
Amount of items: 2
Items: 
Size: 5753 Color: 0
Size: 2280 Color: 1

Bin 107: 34 of cap free
Amount of items: 2
Items: 
Size: 5874 Color: 0
Size: 2156 Color: 1

Bin 108: 34 of cap free
Amount of items: 2
Items: 
Size: 6714 Color: 1
Size: 1316 Color: 0

Bin 109: 34 of cap free
Amount of items: 2
Items: 
Size: 7024 Color: 1
Size: 1006 Color: 0

Bin 110: 38 of cap free
Amount of items: 9
Items: 
Size: 4036 Color: 1
Size: 668 Color: 0
Size: 588 Color: 0
Size: 544 Color: 0
Size: 514 Color: 0
Size: 512 Color: 0
Size: 420 Color: 1
Size: 376 Color: 1
Size: 368 Color: 1

Bin 111: 41 of cap free
Amount of items: 2
Items: 
Size: 6968 Color: 1
Size: 1055 Color: 0

Bin 112: 43 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 1
Size: 1248 Color: 0
Size: 592 Color: 0

Bin 113: 45 of cap free
Amount of items: 2
Items: 
Size: 5768 Color: 1
Size: 2251 Color: 0

Bin 114: 46 of cap free
Amount of items: 2
Items: 
Size: 5590 Color: 1
Size: 2428 Color: 0

Bin 115: 47 of cap free
Amount of items: 2
Items: 
Size: 6535 Color: 1
Size: 1482 Color: 0

Bin 116: 48 of cap free
Amount of items: 2
Items: 
Size: 5866 Color: 0
Size: 2150 Color: 1

Bin 117: 48 of cap free
Amount of items: 2
Items: 
Size: 5935 Color: 0
Size: 2081 Color: 1

Bin 118: 55 of cap free
Amount of items: 8
Items: 
Size: 4033 Color: 0
Size: 670 Color: 0
Size: 670 Color: 0
Size: 668 Color: 1
Size: 668 Color: 0
Size: 580 Color: 1
Size: 448 Color: 1
Size: 272 Color: 1

Bin 119: 63 of cap free
Amount of items: 2
Items: 
Size: 5371 Color: 0
Size: 2630 Color: 1

Bin 120: 74 of cap free
Amount of items: 2
Items: 
Size: 6184 Color: 1
Size: 1806 Color: 0

Bin 121: 74 of cap free
Amount of items: 2
Items: 
Size: 6641 Color: 0
Size: 1349 Color: 1

Bin 122: 82 of cap free
Amount of items: 2
Items: 
Size: 6344 Color: 1
Size: 1638 Color: 0

Bin 123: 89 of cap free
Amount of items: 2
Items: 
Size: 5070 Color: 0
Size: 2905 Color: 1

Bin 124: 93 of cap free
Amount of items: 2
Items: 
Size: 6290 Color: 0
Size: 1681 Color: 1

Bin 125: 98 of cap free
Amount of items: 2
Items: 
Size: 4764 Color: 1
Size: 3202 Color: 0

Bin 126: 110 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 0
Size: 1342 Color: 1

Bin 127: 113 of cap free
Amount of items: 2
Items: 
Size: 6023 Color: 1
Size: 1928 Color: 0

Bin 128: 113 of cap free
Amount of items: 2
Items: 
Size: 6125 Color: 0
Size: 1826 Color: 1

Bin 129: 121 of cap free
Amount of items: 2
Items: 
Size: 4579 Color: 0
Size: 3364 Color: 1

Bin 130: 122 of cap free
Amount of items: 2
Items: 
Size: 5339 Color: 0
Size: 2603 Color: 1

Bin 131: 124 of cap free
Amount of items: 2
Items: 
Size: 6478 Color: 1
Size: 1462 Color: 0

Bin 132: 136 of cap free
Amount of items: 2
Items: 
Size: 4854 Color: 0
Size: 3074 Color: 1

Bin 133: 5276 of cap free
Amount of items: 16
Items: 
Size: 214 Color: 0
Size: 208 Color: 0
Size: 200 Color: 0
Size: 192 Color: 1
Size: 184 Color: 0
Size: 182 Color: 1
Size: 176 Color: 1
Size: 168 Color: 1
Size: 160 Color: 1
Size: 160 Color: 1
Size: 160 Color: 1
Size: 160 Color: 0
Size: 160 Color: 0
Size: 160 Color: 0
Size: 160 Color: 0
Size: 144 Color: 1

Total size: 1064448
Total free space: 8064

