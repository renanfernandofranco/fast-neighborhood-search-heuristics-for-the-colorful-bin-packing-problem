Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 19
Size: 312 Color: 4
Size: 269 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 15
Size: 356 Color: 7
Size: 251 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 11
Size: 276 Color: 13
Size: 262 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7
Size: 318 Color: 0
Size: 290 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 16
Size: 259 Color: 19
Size: 250 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 14
Size: 350 Color: 12
Size: 250 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 6
Size: 271 Color: 12
Size: 259 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 14
Size: 333 Color: 17
Size: 255 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 11
Size: 363 Color: 19
Size: 271 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 14
Size: 275 Color: 11
Size: 259 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 14
Size: 266 Color: 4
Size: 253 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 17
Size: 351 Color: 3
Size: 282 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 5
Size: 272 Color: 13
Size: 255 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 10
Size: 252 Color: 9
Size: 250 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 19
Size: 313 Color: 12
Size: 282 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 16
Size: 308 Color: 11
Size: 295 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 7
Size: 287 Color: 3
Size: 267 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 14
Size: 314 Color: 19
Size: 301 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9
Size: 299 Color: 9
Size: 257 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 13
Size: 281 Color: 10
Size: 274 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 294 Color: 16
Size: 265 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 10
Size: 357 Color: 13
Size: 277 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 2
Size: 273 Color: 16
Size: 263 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 15
Size: 258 Color: 14
Size: 250 Color: 12

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 19
Size: 269 Color: 1
Size: 259 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 16
Size: 271 Color: 2
Size: 255 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 7
Size: 288 Color: 12
Size: 271 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 254 Color: 0
Size: 251 Color: 15

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 12
Size: 264 Color: 15
Size: 256 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 12
Size: 320 Color: 2
Size: 259 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8
Size: 293 Color: 6
Size: 287 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 6
Size: 252 Color: 12
Size: 252 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 2
Size: 329 Color: 0
Size: 314 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 352 Color: 18
Size: 262 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 330 Color: 16
Size: 298 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 13
Size: 307 Color: 2
Size: 263 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 260 Color: 17
Size: 255 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 5
Size: 351 Color: 6
Size: 294 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 18
Size: 276 Color: 5
Size: 251 Color: 8

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 11
Size: 262 Color: 0
Size: 258 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 14
Size: 253 Color: 11
Size: 250 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 5
Size: 298 Color: 5
Size: 252 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 13
Size: 252 Color: 0
Size: 251 Color: 14

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 16
Size: 293 Color: 18
Size: 292 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 2
Size: 350 Color: 4
Size: 255 Color: 10

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 18
Size: 275 Color: 10
Size: 254 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 9
Size: 294 Color: 18
Size: 287 Color: 10

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 15
Size: 261 Color: 3
Size: 254 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 318 Color: 19
Size: 276 Color: 6

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 14
Size: 325 Color: 11
Size: 253 Color: 15

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 9
Size: 365 Color: 11
Size: 259 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 14
Size: 350 Color: 0
Size: 268 Color: 18

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 12
Size: 361 Color: 2
Size: 269 Color: 17

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 16
Size: 302 Color: 15
Size: 272 Color: 14

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 17
Size: 305 Color: 14
Size: 269 Color: 13

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 18
Size: 310 Color: 11
Size: 253 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 12
Size: 273 Color: 15
Size: 258 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 8
Size: 291 Color: 9
Size: 275 Color: 9

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 16
Size: 342 Color: 1
Size: 308 Color: 10

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 337 Color: 2
Size: 253 Color: 17

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 357 Color: 11
Size: 251 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 19
Size: 261 Color: 4
Size: 254 Color: 7

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 11
Size: 344 Color: 6
Size: 264 Color: 9

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 12
Size: 263 Color: 11
Size: 252 Color: 12

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 363 Color: 7
Size: 271 Color: 14

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 5
Size: 253 Color: 1
Size: 250 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 6
Size: 303 Color: 8
Size: 258 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 9
Size: 346 Color: 18
Size: 250 Color: 9

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 17
Size: 281 Color: 4
Size: 274 Color: 7

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 13
Size: 315 Color: 10
Size: 278 Color: 18

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 4
Size: 355 Color: 13
Size: 283 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 2
Size: 359 Color: 9
Size: 274 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 2
Size: 267 Color: 3
Size: 254 Color: 6

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 17
Size: 260 Color: 14
Size: 250 Color: 18

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 272 Color: 18
Size: 250 Color: 5

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 5
Size: 258 Color: 12
Size: 251 Color: 10

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 5
Size: 347 Color: 19
Size: 297 Color: 17

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 15
Size: 255 Color: 7
Size: 250 Color: 11

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 3
Size: 260 Color: 18
Size: 252 Color: 12

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 336 Color: 8
Size: 298 Color: 19

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 366 Color: 2
Size: 264 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 7
Size: 333 Color: 8
Size: 253 Color: 18

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 8
Size: 280 Color: 10
Size: 274 Color: 15

Total size: 83000
Total free space: 0

