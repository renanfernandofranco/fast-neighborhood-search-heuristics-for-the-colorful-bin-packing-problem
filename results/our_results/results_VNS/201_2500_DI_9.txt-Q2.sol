Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 21
Items: 
Size: 308 Color: 0
Size: 236 Color: 0
Size: 226 Color: 0
Size: 152 Color: 0
Size: 148 Color: 0
Size: 120 Color: 1
Size: 108 Color: 1
Size: 102 Color: 1
Size: 100 Color: 1
Size: 96 Color: 1
Size: 96 Color: 0
Size: 94 Color: 1
Size: 88 Color: 1
Size: 88 Color: 0
Size: 86 Color: 0
Size: 80 Color: 0
Size: 76 Color: 0
Size: 72 Color: 1
Size: 72 Color: 1
Size: 68 Color: 0
Size: 48 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1234 Color: 1
Size: 576 Color: 0
Size: 452 Color: 0
Size: 120 Color: 1
Size: 82 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 0
Size: 1022 Color: 0
Size: 200 Color: 1

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 0
Size: 920 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 1
Size: 335 Color: 0
Size: 128 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 1
Size: 404 Color: 0
Size: 40 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 1
Size: 188 Color: 0
Size: 176 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 0
Size: 172 Color: 0
Size: 122 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 0
Size: 244 Color: 0
Size: 40 Color: 1

Bin 10: 1 of cap free
Amount of items: 5
Items: 
Size: 1233 Color: 0
Size: 604 Color: 0
Size: 252 Color: 1
Size: 238 Color: 1
Size: 136 Color: 0

Bin 11: 1 of cap free
Amount of items: 5
Items: 
Size: 1235 Color: 1
Size: 780 Color: 1
Size: 332 Color: 0
Size: 64 Color: 1
Size: 52 Color: 0

Bin 12: 1 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 0
Size: 882 Color: 1
Size: 246 Color: 1

Bin 13: 1 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 0
Size: 758 Color: 1
Size: 299 Color: 0

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 0
Size: 941 Color: 1
Size: 72 Color: 0

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1487 Color: 1
Size: 840 Color: 0
Size: 136 Color: 1

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 0
Size: 771 Color: 0
Size: 48 Color: 1

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 1674 Color: 0
Size: 789 Color: 1

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 1
Size: 734 Color: 0
Size: 72 Color: 1

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 0
Size: 685 Color: 1
Size: 60 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 0
Size: 630 Color: 0
Size: 108 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 0
Size: 617 Color: 1
Size: 48 Color: 0

Bin 22: 1 of cap free
Amount of items: 4
Items: 
Size: 1862 Color: 0
Size: 513 Color: 1
Size: 44 Color: 1
Size: 44 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 0
Size: 365 Color: 0
Size: 110 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 2115 Color: 0
Size: 204 Color: 1
Size: 144 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 2151 Color: 0
Size: 176 Color: 0
Size: 136 Color: 1

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 0
Size: 291 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 0
Size: 261 Color: 1
Size: 8 Color: 1

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 0
Size: 684 Color: 0
Size: 68 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 1
Size: 662 Color: 0
Size: 104 Color: 1

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1740 Color: 1
Size: 722 Color: 0

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 1
Size: 244 Color: 0
Size: 204 Color: 0

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 2090 Color: 0
Size: 372 Color: 1

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 0
Size: 272 Color: 1
Size: 26 Color: 1

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 0
Size: 441 Color: 1
Size: 200 Color: 1

Bin 35: 3 of cap free
Amount of items: 4
Items: 
Size: 2182 Color: 1
Size: 249 Color: 0
Size: 18 Color: 0
Size: 12 Color: 1

Bin 36: 4 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 1
Size: 874 Color: 1
Size: 168 Color: 0

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 1
Size: 342 Color: 0

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 2188 Color: 1
Size: 272 Color: 0

Bin 39: 5 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 1
Size: 869 Color: 0
Size: 58 Color: 0

Bin 40: 5 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 0
Size: 458 Color: 0
Size: 152 Color: 1

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 2045 Color: 0
Size: 414 Color: 1

Bin 42: 6 of cap free
Amount of items: 2
Items: 
Size: 1918 Color: 1
Size: 540 Color: 0

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 2107 Color: 0
Size: 351 Color: 1

Bin 44: 7 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 1
Size: 315 Color: 0
Size: 172 Color: 0

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 2167 Color: 1
Size: 290 Color: 0

Bin 46: 9 of cap free
Amount of items: 2
Items: 
Size: 1897 Color: 1
Size: 558 Color: 0

Bin 47: 11 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 0
Size: 1093 Color: 0
Size: 124 Color: 1

Bin 48: 11 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 0
Size: 484 Color: 1
Size: 176 Color: 1

Bin 49: 13 of cap free
Amount of items: 2
Items: 
Size: 1559 Color: 0
Size: 892 Color: 1

Bin 50: 14 of cap free
Amount of items: 2
Items: 
Size: 2063 Color: 0
Size: 387 Color: 1

Bin 51: 18 of cap free
Amount of items: 2
Items: 
Size: 2068 Color: 1
Size: 378 Color: 0

Bin 52: 19 of cap free
Amount of items: 2
Items: 
Size: 1558 Color: 1
Size: 887 Color: 0

Bin 53: 19 of cap free
Amount of items: 2
Items: 
Size: 1884 Color: 0
Size: 561 Color: 1

Bin 54: 19 of cap free
Amount of items: 3
Items: 
Size: 2123 Color: 0
Size: 314 Color: 1
Size: 8 Color: 1

Bin 55: 20 of cap free
Amount of items: 2
Items: 
Size: 1598 Color: 1
Size: 846 Color: 0

Bin 56: 20 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 1
Size: 504 Color: 0

Bin 57: 22 of cap free
Amount of items: 2
Items: 
Size: 1969 Color: 1
Size: 473 Color: 0

Bin 58: 24 of cap free
Amount of items: 2
Items: 
Size: 2027 Color: 1
Size: 413 Color: 0

Bin 59: 25 of cap free
Amount of items: 2
Items: 
Size: 1937 Color: 1
Size: 502 Color: 0

Bin 60: 26 of cap free
Amount of items: 2
Items: 
Size: 1410 Color: 1
Size: 1028 Color: 0

Bin 61: 26 of cap free
Amount of items: 2
Items: 
Size: 2054 Color: 0
Size: 384 Color: 1

Bin 62: 36 of cap free
Amount of items: 2
Items: 
Size: 1401 Color: 1
Size: 1027 Color: 0

Bin 63: 36 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 0
Size: 785 Color: 1

Bin 64: 41 of cap free
Amount of items: 2
Items: 
Size: 1541 Color: 0
Size: 882 Color: 1

Bin 65: 42 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 1
Size: 1026 Color: 0

Bin 66: 1924 of cap free
Amount of items: 9
Items: 
Size: 72 Color: 1
Size: 72 Color: 0
Size: 66 Color: 1
Size: 64 Color: 0
Size: 58 Color: 1
Size: 56 Color: 1
Size: 56 Color: 0
Size: 48 Color: 0
Size: 48 Color: 0

Total size: 160160
Total free space: 2464

