Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 115
Size: 253 Color: 10
Size: 253 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 84
Size: 283 Color: 46
Size: 349 Color: 73

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 94
Size: 320 Color: 64
Size: 268 Color: 29

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 98
Size: 315 Color: 63
Size: 258 Color: 21

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 86
Size: 330 Color: 67
Size: 300 Color: 58

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 100
Size: 294 Color: 54
Size: 277 Color: 39

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 109
Size: 274 Color: 35
Size: 263 Color: 25

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 99
Size: 300 Color: 57
Size: 272 Color: 33

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 96
Size: 323 Color: 65
Size: 251 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 93
Size: 311 Color: 62
Size: 290 Color: 53

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 92
Size: 329 Color: 66
Size: 273 Color: 34

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 117
Size: 255 Color: 15
Size: 250 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 88
Size: 362 Color: 81
Size: 265 Color: 27

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 97
Size: 297 Color: 55
Size: 277 Color: 40

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 85
Size: 344 Color: 71
Size: 286 Color: 48

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 87
Size: 358 Color: 78
Size: 272 Color: 32

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 288 Color: 51
Size: 267 Color: 28
Size: 445 Color: 106

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 119
Size: 251 Color: 6
Size: 250 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 114
Size: 259 Color: 22
Size: 250 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 80
Size: 360 Color: 79
Size: 279 Color: 41

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 95
Size: 332 Color: 68
Size: 255 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 112
Size: 270 Color: 30
Size: 250 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 104
Size: 304 Color: 59
Size: 256 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 82
Size: 347 Color: 72
Size: 286 Color: 49

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 74
Size: 344 Color: 70
Size: 306 Color: 60

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 110
Size: 282 Color: 44
Size: 254 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 108
Size: 281 Color: 43
Size: 261 Color: 23

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 91
Size: 351 Color: 76
Size: 254 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 89
Size: 350 Color: 75
Size: 274 Color: 36

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 101
Size: 287 Color: 50
Size: 283 Color: 45

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 83
Size: 343 Color: 69
Size: 289 Color: 52

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 102
Size: 286 Color: 47
Size: 280 Color: 42

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 105
Size: 299 Color: 56
Size: 257 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 116
Size: 255 Color: 14
Size: 250 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 90
Size: 357 Color: 77
Size: 262 Color: 24

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 118
Size: 252 Color: 8
Size: 250 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 111
Size: 271 Color: 31
Size: 263 Color: 26

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 113
Size: 258 Color: 20
Size: 257 Color: 19

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 107
Size: 275 Color: 38
Size: 274 Color: 37

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 103
Size: 309 Color: 61
Size: 256 Color: 16

Total size: 40000
Total free space: 0

