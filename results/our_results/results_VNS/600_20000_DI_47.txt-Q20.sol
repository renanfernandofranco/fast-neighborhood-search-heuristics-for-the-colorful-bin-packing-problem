Capicity Bin: 15200
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9290 Color: 8
Size: 5562 Color: 15
Size: 348 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 3
Size: 4926 Color: 17
Size: 954 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9938 Color: 15
Size: 4924 Color: 7
Size: 338 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9948 Color: 12
Size: 4380 Color: 11
Size: 872 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10108 Color: 14
Size: 4648 Color: 17
Size: 444 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10904 Color: 11
Size: 3592 Color: 7
Size: 704 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 4
Size: 3514 Color: 15
Size: 796 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11026 Color: 0
Size: 3598 Color: 8
Size: 576 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 10
Size: 3466 Color: 17
Size: 700 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11098 Color: 15
Size: 3602 Color: 14
Size: 500 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 11
Size: 3506 Color: 8
Size: 582 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11432 Color: 11
Size: 3416 Color: 6
Size: 352 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11434 Color: 1
Size: 3482 Color: 13
Size: 284 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11656 Color: 3
Size: 3096 Color: 18
Size: 448 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 2
Size: 3276 Color: 0
Size: 144 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11797 Color: 12
Size: 2837 Color: 2
Size: 566 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12020 Color: 14
Size: 2844 Color: 5
Size: 336 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12109 Color: 14
Size: 2577 Color: 8
Size: 514 Color: 5

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12160 Color: 10
Size: 2064 Color: 15
Size: 976 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12177 Color: 14
Size: 2521 Color: 16
Size: 502 Color: 6

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 7
Size: 2226 Color: 6
Size: 720 Color: 9

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 1
Size: 2568 Color: 19
Size: 352 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12284 Color: 5
Size: 2436 Color: 16
Size: 480 Color: 10

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 6
Size: 2290 Color: 9
Size: 512 Color: 7

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 6
Size: 2492 Color: 7
Size: 280 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12426 Color: 5
Size: 2126 Color: 3
Size: 648 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12482 Color: 2
Size: 2458 Color: 1
Size: 260 Color: 6

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12528 Color: 0
Size: 2256 Color: 16
Size: 416 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12530 Color: 1
Size: 1838 Color: 5
Size: 832 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12585 Color: 18
Size: 2163 Color: 0
Size: 452 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12605 Color: 9
Size: 1443 Color: 13
Size: 1152 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 11
Size: 2206 Color: 18
Size: 350 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12734 Color: 9
Size: 2002 Color: 12
Size: 464 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12772 Color: 18
Size: 1900 Color: 7
Size: 528 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12792 Color: 11
Size: 2008 Color: 11
Size: 400 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12858 Color: 7
Size: 1880 Color: 18
Size: 462 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12890 Color: 14
Size: 1434 Color: 5
Size: 876 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12986 Color: 7
Size: 1110 Color: 9
Size: 1104 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12998 Color: 14
Size: 1610 Color: 10
Size: 592 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 3
Size: 1324 Color: 11
Size: 856 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13029 Color: 17
Size: 1311 Color: 6
Size: 860 Color: 8

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 4
Size: 1428 Color: 19
Size: 688 Color: 14

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13094 Color: 7
Size: 1764 Color: 19
Size: 342 Color: 16

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 5
Size: 1461 Color: 17
Size: 576 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13183 Color: 12
Size: 1759 Color: 19
Size: 258 Color: 15

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13188 Color: 16
Size: 1516 Color: 17
Size: 496 Color: 7

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 10
Size: 1388 Color: 16
Size: 496 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13319 Color: 10
Size: 1481 Color: 8
Size: 400 Color: 15

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13322 Color: 6
Size: 1598 Color: 10
Size: 280 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13418 Color: 6
Size: 970 Color: 15
Size: 812 Color: 13

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 12
Size: 1048 Color: 4
Size: 700 Color: 14

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13462 Color: 5
Size: 1448 Color: 17
Size: 290 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 13
Size: 1302 Color: 9
Size: 384 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13540 Color: 16
Size: 1340 Color: 3
Size: 320 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13610 Color: 12
Size: 1494 Color: 8
Size: 96 Color: 18

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13620 Color: 2
Size: 960 Color: 2
Size: 620 Color: 16

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 9445 Color: 15
Size: 5320 Color: 3
Size: 434 Color: 12

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 10205 Color: 15
Size: 4386 Color: 17
Size: 608 Color: 11

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 10283 Color: 1
Size: 4916 Color: 6

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 10418 Color: 13
Size: 4781 Color: 3

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 10647 Color: 12
Size: 3986 Color: 13
Size: 566 Color: 15

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 10667 Color: 17
Size: 4244 Color: 8
Size: 288 Color: 10

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 12070 Color: 5
Size: 2841 Color: 6
Size: 288 Color: 19

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 9
Size: 2563 Color: 8
Size: 528 Color: 2

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12425 Color: 16
Size: 2196 Color: 19
Size: 578 Color: 16

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12521 Color: 19
Size: 2314 Color: 9
Size: 364 Color: 9

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 12692 Color: 10
Size: 2507 Color: 0

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12911 Color: 6
Size: 1342 Color: 2
Size: 946 Color: 17

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13143 Color: 5
Size: 1112 Color: 6
Size: 944 Color: 15

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13091 Color: 9
Size: 1464 Color: 7
Size: 644 Color: 3

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 13
Size: 1497 Color: 7
Size: 512 Color: 17

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13425 Color: 11
Size: 1486 Color: 12
Size: 288 Color: 10

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13629 Color: 4
Size: 1266 Color: 5
Size: 304 Color: 10

Bin 74: 2 of cap free
Amount of items: 7
Items: 
Size: 7604 Color: 15
Size: 1678 Color: 10
Size: 1460 Color: 13
Size: 1368 Color: 5
Size: 1284 Color: 14
Size: 1112 Color: 0
Size: 692 Color: 8

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 10026 Color: 7
Size: 4228 Color: 2
Size: 944 Color: 14

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 10376 Color: 10
Size: 3974 Color: 17
Size: 848 Color: 0

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 11180 Color: 3
Size: 3594 Color: 18
Size: 424 Color: 2

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11702 Color: 19
Size: 2440 Color: 15
Size: 1056 Color: 14

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11793 Color: 6
Size: 2917 Color: 0
Size: 488 Color: 16

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11796 Color: 17
Size: 1758 Color: 4
Size: 1644 Color: 10

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 17
Size: 2918 Color: 1
Size: 96 Color: 7

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 17
Size: 1406 Color: 10
Size: 328 Color: 18

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 13638 Color: 3
Size: 1560 Color: 16

Bin 84: 3 of cap free
Amount of items: 9
Items: 
Size: 7601 Color: 16
Size: 1216 Color: 2
Size: 1096 Color: 19
Size: 1076 Color: 17
Size: 1064 Color: 16
Size: 1016 Color: 7
Size: 1000 Color: 19
Size: 608 Color: 0
Size: 520 Color: 3

Bin 85: 3 of cap free
Amount of items: 5
Items: 
Size: 8533 Color: 12
Size: 3468 Color: 19
Size: 2092 Color: 4
Size: 648 Color: 11
Size: 456 Color: 12

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 9
Size: 5501 Color: 10
Size: 388 Color: 2

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 9640 Color: 5
Size: 5557 Color: 9

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 10978 Color: 6
Size: 4059 Color: 2
Size: 160 Color: 6

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 11701 Color: 10
Size: 3236 Color: 1
Size: 260 Color: 12

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 12545 Color: 16
Size: 2652 Color: 10

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 12554 Color: 9
Size: 2643 Color: 14

Bin 92: 3 of cap free
Amount of items: 2
Items: 
Size: 13139 Color: 18
Size: 2058 Color: 10

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 13449 Color: 9
Size: 1684 Color: 14
Size: 64 Color: 9

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 13482 Color: 4
Size: 1715 Color: 19

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 13557 Color: 13
Size: 1232 Color: 1
Size: 408 Color: 10

Bin 96: 4 of cap free
Amount of items: 21
Items: 
Size: 984 Color: 17
Size: 984 Color: 3
Size: 880 Color: 8
Size: 876 Color: 7
Size: 832 Color: 4
Size: 816 Color: 11
Size: 812 Color: 12
Size: 796 Color: 15
Size: 792 Color: 15
Size: 760 Color: 11
Size: 736 Color: 8
Size: 720 Color: 11
Size: 716 Color: 6
Size: 716 Color: 5
Size: 692 Color: 14
Size: 692 Color: 0
Size: 680 Color: 4
Size: 504 Color: 18
Size: 416 Color: 10
Size: 400 Color: 2
Size: 392 Color: 17

Bin 97: 4 of cap free
Amount of items: 9
Items: 
Size: 7602 Color: 6
Size: 1264 Color: 3
Size: 1264 Color: 1
Size: 1264 Color: 1
Size: 1248 Color: 15
Size: 952 Color: 11
Size: 912 Color: 12
Size: 432 Color: 12
Size: 258 Color: 17

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 10882 Color: 4
Size: 4314 Color: 13

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 14
Size: 1940 Color: 0

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 11721 Color: 9
Size: 3474 Color: 16

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 11974 Color: 3
Size: 3109 Color: 15
Size: 112 Color: 5

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 12372 Color: 4
Size: 2823 Color: 3

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 8522 Color: 15
Size: 6672 Color: 6

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 9472 Color: 11
Size: 4922 Color: 14
Size: 800 Color: 8

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 10042 Color: 5
Size: 4424 Color: 8
Size: 728 Color: 18

Bin 106: 6 of cap free
Amount of items: 3
Items: 
Size: 10216 Color: 19
Size: 4306 Color: 2
Size: 672 Color: 4

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 10410 Color: 6
Size: 4784 Color: 10

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 13374 Color: 11
Size: 1820 Color: 12

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 9377 Color: 0
Size: 5816 Color: 16

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 9529 Color: 10
Size: 5664 Color: 11

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 17
Size: 1569 Color: 19
Size: 28 Color: 11

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 13651 Color: 2
Size: 1522 Color: 16
Size: 20 Color: 12

Bin 113: 8 of cap free
Amount of items: 4
Items: 
Size: 7640 Color: 11
Size: 3461 Color: 19
Size: 3231 Color: 8
Size: 860 Color: 9

Bin 114: 8 of cap free
Amount of items: 3
Items: 
Size: 9164 Color: 14
Size: 5564 Color: 3
Size: 464 Color: 4

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 9465 Color: 17
Size: 5727 Color: 13

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 12212 Color: 5
Size: 2980 Color: 0

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 12876 Color: 2
Size: 2316 Color: 7

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 12828 Color: 1
Size: 2364 Color: 8

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 1
Size: 1532 Color: 19

Bin 120: 9 of cap free
Amount of items: 2
Items: 
Size: 13492 Color: 14
Size: 1699 Color: 9

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 9930 Color: 0
Size: 5260 Color: 13

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 12170 Color: 2
Size: 2852 Color: 3
Size: 168 Color: 10

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 12924 Color: 14
Size: 2266 Color: 12

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 13236 Color: 11
Size: 1954 Color: 19

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 12837 Color: 19
Size: 2352 Color: 9

Bin 126: 11 of cap free
Amount of items: 3
Items: 
Size: 13445 Color: 6
Size: 1672 Color: 18
Size: 72 Color: 6

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 7
Size: 1800 Color: 3

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 13469 Color: 4
Size: 1719 Color: 13

Bin 129: 13 of cap free
Amount of items: 3
Items: 
Size: 7603 Color: 4
Size: 7208 Color: 2
Size: 376 Color: 8

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 12493 Color: 0
Size: 2694 Color: 8

Bin 131: 15 of cap free
Amount of items: 3
Items: 
Size: 8617 Color: 10
Size: 6312 Color: 17
Size: 256 Color: 11

Bin 132: 15 of cap free
Amount of items: 3
Items: 
Size: 9513 Color: 17
Size: 5340 Color: 13
Size: 332 Color: 8

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 12650 Color: 10
Size: 2535 Color: 1

Bin 134: 16 of cap free
Amount of items: 3
Items: 
Size: 8532 Color: 2
Size: 6332 Color: 10
Size: 320 Color: 14

Bin 135: 16 of cap free
Amount of items: 3
Items: 
Size: 11042 Color: 0
Size: 4002 Color: 2
Size: 140 Color: 19

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 12920 Color: 7
Size: 2264 Color: 10

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 13048 Color: 8
Size: 2136 Color: 18

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 18
Size: 1624 Color: 12

Bin 139: 18 of cap free
Amount of items: 3
Items: 
Size: 9294 Color: 13
Size: 4904 Color: 18
Size: 984 Color: 4

Bin 140: 18 of cap free
Amount of items: 2
Items: 
Size: 12572 Color: 11
Size: 2610 Color: 15

Bin 141: 18 of cap free
Amount of items: 2
Items: 
Size: 13270 Color: 11
Size: 1912 Color: 4

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 13336 Color: 12
Size: 1846 Color: 1

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 11496 Color: 13
Size: 3684 Color: 5

Bin 144: 21 of cap free
Amount of items: 6
Items: 
Size: 7606 Color: 15
Size: 1980 Color: 7
Size: 1909 Color: 4
Size: 1888 Color: 6
Size: 1264 Color: 17
Size: 532 Color: 5

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 11325 Color: 19
Size: 3854 Color: 3

Bin 146: 21 of cap free
Amount of items: 2
Items: 
Size: 13208 Color: 0
Size: 1971 Color: 17

Bin 147: 22 of cap free
Amount of items: 5
Items: 
Size: 7607 Color: 8
Size: 2313 Color: 0
Size: 2132 Color: 15
Size: 2028 Color: 6
Size: 1098 Color: 4

Bin 148: 22 of cap free
Amount of items: 3
Items: 
Size: 9292 Color: 10
Size: 5566 Color: 12
Size: 320 Color: 14

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 11010 Color: 14
Size: 4168 Color: 17

Bin 150: 22 of cap free
Amount of items: 2
Items: 
Size: 11538 Color: 7
Size: 3640 Color: 5

Bin 151: 25 of cap free
Amount of items: 2
Items: 
Size: 13364 Color: 13
Size: 1811 Color: 2

Bin 152: 26 of cap free
Amount of items: 2
Items: 
Size: 10780 Color: 2
Size: 4394 Color: 12

Bin 153: 26 of cap free
Amount of items: 2
Items: 
Size: 12120 Color: 12
Size: 3054 Color: 5

Bin 154: 26 of cap free
Amount of items: 2
Items: 
Size: 12648 Color: 5
Size: 2526 Color: 15

Bin 155: 27 of cap free
Amount of items: 4
Items: 
Size: 7608 Color: 18
Size: 2864 Color: 7
Size: 2520 Color: 15
Size: 2181 Color: 2

Bin 156: 27 of cap free
Amount of items: 2
Items: 
Size: 10873 Color: 1
Size: 4300 Color: 9

Bin 157: 30 of cap free
Amount of items: 2
Items: 
Size: 10620 Color: 13
Size: 4550 Color: 0

Bin 158: 30 of cap free
Amount of items: 3
Items: 
Size: 10716 Color: 19
Size: 2882 Color: 13
Size: 1572 Color: 10

Bin 159: 30 of cap free
Amount of items: 3
Items: 
Size: 10986 Color: 6
Size: 4024 Color: 16
Size: 160 Color: 9

Bin 160: 31 of cap free
Amount of items: 2
Items: 
Size: 11813 Color: 14
Size: 3356 Color: 8

Bin 161: 32 of cap free
Amount of items: 3
Items: 
Size: 8526 Color: 5
Size: 6330 Color: 14
Size: 312 Color: 15

Bin 162: 32 of cap free
Amount of items: 3
Items: 
Size: 11471 Color: 4
Size: 2901 Color: 11
Size: 796 Color: 2

Bin 163: 33 of cap free
Amount of items: 2
Items: 
Size: 10314 Color: 1
Size: 4853 Color: 6

Bin 164: 35 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 6
Size: 2213 Color: 17

Bin 165: 36 of cap free
Amount of items: 2
Items: 
Size: 11742 Color: 4
Size: 3422 Color: 16

Bin 166: 39 of cap free
Amount of items: 2
Items: 
Size: 11276 Color: 3
Size: 3885 Color: 7

Bin 167: 39 of cap free
Amount of items: 2
Items: 
Size: 12193 Color: 8
Size: 2968 Color: 16

Bin 168: 40 of cap free
Amount of items: 2
Items: 
Size: 10044 Color: 19
Size: 5116 Color: 6

Bin 169: 40 of cap free
Amount of items: 2
Items: 
Size: 13594 Color: 7
Size: 1566 Color: 14

Bin 170: 41 of cap free
Amount of items: 3
Items: 
Size: 10402 Color: 9
Size: 3466 Color: 14
Size: 1291 Color: 16

Bin 171: 42 of cap free
Amount of items: 2
Items: 
Size: 8824 Color: 6
Size: 6334 Color: 10

Bin 172: 43 of cap free
Amount of items: 2
Items: 
Size: 10994 Color: 6
Size: 4163 Color: 12

Bin 173: 50 of cap free
Amount of items: 2
Items: 
Size: 11628 Color: 19
Size: 3522 Color: 1

Bin 174: 52 of cap free
Amount of items: 2
Items: 
Size: 7612 Color: 15
Size: 7536 Color: 18

Bin 175: 55 of cap free
Amount of items: 2
Items: 
Size: 12565 Color: 7
Size: 2580 Color: 12

Bin 176: 56 of cap free
Amount of items: 2
Items: 
Size: 11324 Color: 19
Size: 3820 Color: 4

Bin 177: 63 of cap free
Amount of items: 2
Items: 
Size: 11530 Color: 8
Size: 3607 Color: 18

Bin 178: 64 of cap free
Amount of items: 2
Items: 
Size: 11992 Color: 5
Size: 3144 Color: 10

Bin 179: 64 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 19
Size: 2338 Color: 4

Bin 180: 68 of cap free
Amount of items: 2
Items: 
Size: 11050 Color: 16
Size: 4082 Color: 8

Bin 181: 69 of cap free
Amount of items: 2
Items: 
Size: 12488 Color: 2
Size: 2643 Color: 5

Bin 182: 71 of cap free
Amount of items: 2
Items: 
Size: 13448 Color: 8
Size: 1681 Color: 9

Bin 183: 73 of cap free
Amount of items: 2
Items: 
Size: 8796 Color: 10
Size: 6331 Color: 5

Bin 184: 75 of cap free
Amount of items: 2
Items: 
Size: 10840 Color: 13
Size: 4285 Color: 15

Bin 185: 75 of cap free
Amount of items: 2
Items: 
Size: 12445 Color: 7
Size: 2680 Color: 4

Bin 186: 76 of cap free
Amount of items: 2
Items: 
Size: 8601 Color: 7
Size: 6523 Color: 3

Bin 187: 76 of cap free
Amount of items: 2
Items: 
Size: 9896 Color: 4
Size: 5228 Color: 19

Bin 188: 79 of cap free
Amount of items: 2
Items: 
Size: 11047 Color: 3
Size: 4074 Color: 18

Bin 189: 80 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 2
Size: 3994 Color: 3
Size: 692 Color: 4

Bin 190: 82 of cap free
Amount of items: 3
Items: 
Size: 9020 Color: 8
Size: 4727 Color: 7
Size: 1371 Color: 14

Bin 191: 84 of cap free
Amount of items: 33
Items: 
Size: 704 Color: 3
Size: 700 Color: 11
Size: 664 Color: 12
Size: 640 Color: 4
Size: 632 Color: 3
Size: 624 Color: 8
Size: 608 Color: 14
Size: 580 Color: 4
Size: 568 Color: 11
Size: 564 Color: 3
Size: 560 Color: 10
Size: 494 Color: 14
Size: 480 Color: 7
Size: 442 Color: 16
Size: 440 Color: 0
Size: 432 Color: 5
Size: 424 Color: 5
Size: 416 Color: 4
Size: 392 Color: 7
Size: 384 Color: 6
Size: 380 Color: 9
Size: 368 Color: 18
Size: 368 Color: 16
Size: 368 Color: 2
Size: 360 Color: 0
Size: 360 Color: 0
Size: 342 Color: 2
Size: 312 Color: 12
Size: 312 Color: 10
Size: 304 Color: 18
Size: 304 Color: 14
Size: 296 Color: 1
Size: 294 Color: 1

Bin 192: 91 of cap free
Amount of items: 3
Items: 
Size: 7804 Color: 9
Size: 6329 Color: 17
Size: 976 Color: 2

Bin 193: 108 of cap free
Amount of items: 2
Items: 
Size: 8232 Color: 7
Size: 6860 Color: 11

Bin 194: 126 of cap free
Amount of items: 2
Items: 
Size: 11044 Color: 8
Size: 4030 Color: 13

Bin 195: 130 of cap free
Amount of items: 2
Items: 
Size: 10034 Color: 9
Size: 5036 Color: 5

Bin 196: 144 of cap free
Amount of items: 2
Items: 
Size: 8892 Color: 7
Size: 6164 Color: 16

Bin 197: 153 of cap free
Amount of items: 2
Items: 
Size: 10306 Color: 1
Size: 4741 Color: 13

Bin 198: 185 of cap free
Amount of items: 3
Items: 
Size: 9068 Color: 12
Size: 5487 Color: 13
Size: 460 Color: 2

Bin 199: 11508 of cap free
Amount of items: 14
Items: 
Size: 320 Color: 2
Size: 296 Color: 13
Size: 296 Color: 5
Size: 288 Color: 1
Size: 272 Color: 19
Size: 272 Color: 18
Size: 272 Color: 8
Size: 264 Color: 19
Size: 264 Color: 10
Size: 256 Color: 18
Size: 256 Color: 17
Size: 224 Color: 17
Size: 208 Color: 14
Size: 204 Color: 12

Total size: 3009600
Total free space: 15200

