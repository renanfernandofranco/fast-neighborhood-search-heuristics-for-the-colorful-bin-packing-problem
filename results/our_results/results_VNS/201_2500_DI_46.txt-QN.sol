Capicity Bin: 2328
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 153
Size: 459 Color: 101
Size: 323 Color: 84

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 162
Size: 553 Color: 112
Size: 90 Color: 35

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 163
Size: 451 Color: 99
Size: 188 Color: 66

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 167
Size: 554 Color: 113
Size: 64 Color: 23

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 174
Size: 441 Color: 97
Size: 88 Color: 33

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 175
Size: 478 Color: 106
Size: 36 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1854 Color: 176
Size: 322 Color: 83
Size: 152 Color: 59

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 182
Size: 362 Color: 92
Size: 68 Color: 27

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 183
Size: 341 Color: 88
Size: 66 Color: 25

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 184
Size: 298 Color: 81
Size: 108 Color: 46

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 187
Size: 275 Color: 77
Size: 112 Color: 48

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 189
Size: 346 Color: 90
Size: 28 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 190
Size: 282 Color: 78
Size: 76 Color: 31

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 191
Size: 288 Color: 79
Size: 66 Color: 26

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 195
Size: 258 Color: 75
Size: 56 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 196
Size: 302 Color: 82
Size: 4 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 198
Size: 166 Color: 63
Size: 104 Color: 42

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 199
Size: 198 Color: 70
Size: 52 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 200
Size: 176 Color: 64
Size: 66 Color: 24

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 201
Size: 144 Color: 58
Size: 90 Color: 36

Bin 21: 1 of cap free
Amount of items: 5
Items: 
Size: 1325 Color: 145
Size: 850 Color: 129
Size: 56 Color: 18
Size: 48 Color: 13
Size: 48 Color: 12

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1673 Color: 161
Size: 654 Color: 121

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 164
Size: 531 Color: 107
Size: 106 Color: 44

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1858 Color: 178
Size: 469 Color: 104

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1861 Color: 179
Size: 466 Color: 103

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1929 Color: 186
Size: 398 Color: 96

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1979 Color: 192
Size: 348 Color: 91

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1994 Color: 193
Size: 333 Color: 86

Bin 29: 2 of cap free
Amount of items: 5
Items: 
Size: 1165 Color: 139
Size: 578 Color: 116
Size: 391 Color: 94
Size: 128 Color: 55
Size: 64 Color: 21

Bin 30: 2 of cap free
Amount of items: 5
Items: 
Size: 1167 Color: 141
Size: 969 Color: 134
Size: 76 Color: 29
Size: 58 Color: 20
Size: 56 Color: 17

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 147
Size: 947 Color: 133
Size: 44 Color: 10

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 169
Size: 561 Color: 114

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1855 Color: 177
Size: 471 Color: 105

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1864 Color: 180
Size: 462 Color: 102

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1871 Color: 181
Size: 455 Color: 100

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 158
Size: 448 Color: 98
Size: 226 Color: 73

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1694 Color: 166
Size: 631 Color: 118

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1787 Color: 173
Size: 538 Color: 111

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 2034 Color: 197
Size: 291 Color: 80

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1467 Color: 152
Size: 829 Color: 127
Size: 28 Color: 3

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1554 Color: 154
Size: 770 Color: 125

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1693 Color: 165
Size: 630 Color: 117

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1758 Color: 168
Size: 565 Color: 115

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1942 Color: 188
Size: 381 Color: 93

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 1999 Color: 194
Size: 324 Color: 85

Bin 46: 7 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 150
Size: 835 Color: 128
Size: 40 Color: 7

Bin 47: 7 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 155
Size: 738 Color: 123
Size: 28 Color: 2

Bin 48: 8 of cap free
Amount of items: 2
Items: 
Size: 1783 Color: 172
Size: 537 Color: 110

Bin 49: 9 of cap free
Amount of items: 2
Items: 
Size: 1925 Color: 185
Size: 394 Color: 95

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1166 Color: 140
Size: 1152 Color: 138

Bin 51: 11 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 157
Size: 723 Color: 122
Size: 20 Color: 1

Bin 52: 14 of cap free
Amount of items: 2
Items: 
Size: 1573 Color: 156
Size: 741 Color: 124

Bin 53: 15 of cap free
Amount of items: 2
Items: 
Size: 1779 Color: 171
Size: 534 Color: 109

Bin 54: 16 of cap free
Amount of items: 2
Items: 
Size: 1666 Color: 160
Size: 646 Color: 120

Bin 55: 17 of cap free
Amount of items: 2
Items: 
Size: 1778 Color: 170
Size: 533 Color: 108

Bin 56: 18 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 149
Size: 864 Color: 130
Size: 40 Color: 8

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 1665 Color: 159
Size: 645 Color: 119

Bin 58: 25 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 151
Size: 802 Color: 126
Size: 40 Color: 6

Bin 59: 26 of cap free
Amount of items: 18
Items: 
Size: 192 Color: 67
Size: 184 Color: 65
Size: 164 Color: 62
Size: 156 Color: 61
Size: 156 Color: 60
Size: 144 Color: 57
Size: 144 Color: 56
Size: 128 Color: 54
Size: 128 Color: 53
Size: 128 Color: 52
Size: 106 Color: 45
Size: 104 Color: 43
Size: 104 Color: 41
Size: 96 Color: 40
Size: 94 Color: 39
Size: 92 Color: 38
Size: 92 Color: 37
Size: 90 Color: 34

Bin 60: 27 of cap free
Amount of items: 3
Items: 
Size: 1327 Color: 146
Size: 926 Color: 132
Size: 48 Color: 11

Bin 61: 32 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 148
Size: 886 Color: 131
Size: 40 Color: 9

Bin 62: 35 of cap free
Amount of items: 2
Items: 
Size: 1322 Color: 144
Size: 971 Color: 136

Bin 63: 40 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 142
Size: 1016 Color: 137
Size: 54 Color: 16

Bin 64: 40 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 143
Size: 970 Color: 135
Size: 52 Color: 15

Bin 65: 41 of cap free
Amount of items: 12
Items: 
Size: 342 Color: 89
Size: 337 Color: 87
Size: 262 Color: 76
Size: 246 Color: 74
Size: 210 Color: 72
Size: 202 Color: 71
Size: 192 Color: 69
Size: 192 Color: 68
Size: 88 Color: 32
Size: 76 Color: 30
Size: 76 Color: 28
Size: 64 Color: 22

Bin 66: 1850 of cap free
Amount of items: 4
Items: 
Size: 124 Color: 51
Size: 124 Color: 50
Size: 120 Color: 49
Size: 110 Color: 47

Total size: 151320
Total free space: 2328

