Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 17
Size: 259 Color: 0
Size: 252 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 4
Size: 334 Color: 10
Size: 257 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 5
Size: 324 Color: 0
Size: 265 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 3
Size: 266 Color: 12
Size: 252 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 273 Color: 11
Size: 265 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 7
Size: 258 Color: 12
Size: 254 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 15
Size: 337 Color: 13
Size: 250 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 11
Size: 299 Color: 12
Size: 272 Color: 19

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 10
Size: 359 Color: 4
Size: 264 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 18
Size: 341 Color: 12
Size: 259 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7
Size: 317 Color: 4
Size: 311 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 7
Size: 285 Color: 4
Size: 272 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 255 Color: 8
Size: 253 Color: 13
Size: 492 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 14
Size: 338 Color: 9
Size: 268 Color: 16

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 8
Size: 315 Color: 17
Size: 254 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 16
Size: 261 Color: 17
Size: 260 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 2
Size: 261 Color: 6
Size: 258 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 310 Color: 10
Size: 265 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 10
Size: 327 Color: 18
Size: 264 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 17
Size: 258 Color: 14
Size: 250 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 341 Color: 16
Size: 263 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 10
Size: 251 Color: 8
Size: 250 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 16
Size: 278 Color: 12
Size: 263 Color: 8

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 7
Size: 295 Color: 18
Size: 254 Color: 5

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 10
Size: 260 Color: 9
Size: 258 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 18
Size: 318 Color: 17
Size: 253 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 11
Size: 333 Color: 17
Size: 256 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 6
Size: 309 Color: 11
Size: 267 Color: 10

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 9
Size: 356 Color: 3
Size: 255 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 339 Color: 18
Size: 265 Color: 9

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 282 Color: 2
Size: 269 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 5
Size: 277 Color: 4
Size: 257 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 2
Size: 347 Color: 1
Size: 256 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 17
Size: 354 Color: 0
Size: 257 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 15
Size: 299 Color: 14
Size: 254 Color: 12

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 10
Size: 274 Color: 14
Size: 257 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 15
Size: 279 Color: 7
Size: 256 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8
Size: 328 Color: 6
Size: 271 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 11
Size: 271 Color: 1
Size: 263 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 316 Color: 3
Size: 255 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 16
Size: 271 Color: 18
Size: 258 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9
Size: 271 Color: 18
Size: 261 Color: 13

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 309 Color: 11
Size: 281 Color: 6

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 285 Color: 8
Size: 283 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 9
Size: 304 Color: 9
Size: 290 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 16
Size: 294 Color: 7
Size: 274 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 333 Color: 11
Size: 304 Color: 17

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 16
Size: 370 Color: 13
Size: 255 Color: 17

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 340 Color: 9
Size: 296 Color: 11

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 10
Size: 252 Color: 7
Size: 250 Color: 10

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 2
Size: 260 Color: 11
Size: 252 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 15
Size: 330 Color: 2
Size: 273 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 17
Size: 264 Color: 14
Size: 259 Color: 5

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 13
Size: 369 Color: 17
Size: 254 Color: 16

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 9
Size: 322 Color: 6
Size: 283 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 14
Size: 268 Color: 12
Size: 253 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 9
Size: 308 Color: 0
Size: 251 Color: 18

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 6
Size: 353 Color: 12
Size: 264 Color: 9

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 18
Size: 255 Color: 6
Size: 251 Color: 11

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 7
Size: 273 Color: 2
Size: 263 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 11
Size: 368 Color: 1
Size: 259 Color: 18

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 5
Size: 357 Color: 12
Size: 264 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 10
Size: 251 Color: 0
Size: 250 Color: 5

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 289 Color: 13
Size: 274 Color: 16

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 3
Size: 265 Color: 14
Size: 254 Color: 9

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 257 Color: 9
Size: 252 Color: 7

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 6
Size: 263 Color: 6
Size: 259 Color: 13

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 312 Color: 7
Size: 274 Color: 8

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 6
Size: 280 Color: 17
Size: 250 Color: 13

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 19
Size: 273 Color: 2
Size: 265 Color: 19

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8
Size: 323 Color: 12
Size: 265 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 9
Size: 319 Color: 12
Size: 296 Color: 19

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 8
Size: 335 Color: 2
Size: 315 Color: 12

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 10
Size: 264 Color: 2
Size: 260 Color: 10

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 4
Size: 275 Color: 13
Size: 270 Color: 3

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 19
Size: 310 Color: 9
Size: 304 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 4
Size: 340 Color: 1
Size: 316 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 13
Size: 325 Color: 0
Size: 292 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 13
Size: 321 Color: 4
Size: 263 Color: 10

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 299 Color: 10
Size: 252 Color: 6

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 5
Size: 333 Color: 15
Size: 269 Color: 12

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 309 Color: 3
Size: 288 Color: 6

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 8
Size: 328 Color: 9
Size: 261 Color: 15

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 5
Size: 268 Color: 14
Size: 250 Color: 7

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 16
Size: 265 Color: 8
Size: 258 Color: 16

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 14
Size: 289 Color: 18
Size: 281 Color: 11

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 15
Size: 312 Color: 12
Size: 267 Color: 8

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 4
Size: 265 Color: 19
Size: 265 Color: 11

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 9
Size: 337 Color: 18
Size: 263 Color: 18

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 15
Size: 255 Color: 1
Size: 251 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 16
Size: 255 Color: 8
Size: 251 Color: 4

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 261 Color: 8
Size: 255 Color: 6

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 14
Size: 311 Color: 13
Size: 277 Color: 10

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 2
Size: 342 Color: 15
Size: 295 Color: 15

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 256 Color: 15
Size: 254 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 5
Size: 257 Color: 7
Size: 255 Color: 12

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 11
Size: 308 Color: 6
Size: 272 Color: 9

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 5
Size: 254 Color: 1
Size: 254 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 274 Color: 16
Size: 257 Color: 8

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 272 Color: 7
Size: 266 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 7
Size: 282 Color: 7
Size: 254 Color: 16

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 6
Size: 287 Color: 16
Size: 251 Color: 11

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 4
Size: 337 Color: 0
Size: 312 Color: 19

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 296 Color: 3
Size: 283 Color: 15

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 6
Size: 358 Color: 9
Size: 275 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 18
Size: 266 Color: 5
Size: 250 Color: 10

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 17
Size: 269 Color: 6
Size: 253 Color: 4

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 14
Size: 309 Color: 13
Size: 255 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 0
Size: 251 Color: 7
Size: 250 Color: 17

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 17
Size: 312 Color: 11
Size: 299 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 3
Size: 325 Color: 11
Size: 319 Color: 18

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 5
Size: 279 Color: 9
Size: 264 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 15
Size: 340 Color: 0
Size: 306 Color: 12

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7
Size: 336 Color: 2
Size: 289 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 12
Size: 267 Color: 8
Size: 252 Color: 17

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 7
Size: 282 Color: 5
Size: 264 Color: 19

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 15
Size: 318 Color: 14
Size: 264 Color: 16

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 13
Size: 269 Color: 8
Size: 251 Color: 12

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 297 Color: 16
Size: 272 Color: 8

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 17
Size: 317 Color: 5
Size: 267 Color: 18

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9
Size: 258 Color: 14
Size: 250 Color: 5

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 9
Size: 348 Color: 9
Size: 263 Color: 19

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 17
Size: 286 Color: 0
Size: 267 Color: 13

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 4
Size: 257 Color: 0
Size: 256 Color: 13

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 12
Size: 274 Color: 0
Size: 251 Color: 9

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 6
Size: 277 Color: 7
Size: 261 Color: 8

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 15
Size: 306 Color: 10
Size: 252 Color: 12

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 324 Color: 2
Size: 280 Color: 13

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 8
Size: 359 Color: 11
Size: 263 Color: 9

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 5
Size: 350 Color: 12
Size: 285 Color: 19

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 16
Size: 307 Color: 7
Size: 267 Color: 8

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 373 Color: 12
Size: 252 Color: 4

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 259 Color: 8
Size: 252 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 11
Size: 336 Color: 8
Size: 288 Color: 17

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 19
Size: 272 Color: 13
Size: 253 Color: 9

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 13
Size: 272 Color: 9
Size: 268 Color: 3

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 8
Size: 277 Color: 0
Size: 252 Color: 14

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 12
Size: 302 Color: 6
Size: 255 Color: 12

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 12
Size: 289 Color: 5
Size: 250 Color: 12

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 9
Size: 323 Color: 3
Size: 251 Color: 18

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 9
Size: 319 Color: 13
Size: 258 Color: 7

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 12
Size: 304 Color: 0
Size: 298 Color: 10

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 367 Color: 19
Size: 254 Color: 14

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 15
Size: 318 Color: 7
Size: 253 Color: 8

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 16
Size: 304 Color: 8
Size: 292 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 12
Size: 309 Color: 16
Size: 262 Color: 15

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 294 Color: 0
Size: 272 Color: 15

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 2
Size: 324 Color: 7
Size: 320 Color: 14

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 10
Size: 303 Color: 1
Size: 282 Color: 12

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 14
Size: 347 Color: 18
Size: 277 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 11
Size: 316 Color: 18
Size: 250 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 13
Size: 283 Color: 5
Size: 264 Color: 3

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 11
Size: 391 Color: 1
Size: 258 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 8
Size: 352 Color: 13
Size: 267 Color: 8

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 15
Size: 358 Color: 11
Size: 271 Color: 13

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 11
Size: 291 Color: 10
Size: 254 Color: 19

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 16
Size: 254 Color: 9
Size: 251 Color: 12

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 15
Size: 303 Color: 7
Size: 279 Color: 15

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 264 Color: 19
Size: 260 Color: 17

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 13
Size: 304 Color: 19
Size: 251 Color: 19

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 343 Color: 14
Size: 296 Color: 19

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 6
Size: 298 Color: 17
Size: 290 Color: 12

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 17
Size: 262 Color: 6
Size: 251 Color: 14

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 11
Size: 316 Color: 19
Size: 279 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 18
Size: 278 Color: 17
Size: 268 Color: 4

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 7
Size: 352 Color: 7
Size: 288 Color: 18

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 11
Size: 259 Color: 8
Size: 256 Color: 7

Total size: 167000
Total free space: 0

