Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 336613 Color: 1
Size: 333047 Color: 1
Size: 330341 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 342675 Color: 1
Size: 338650 Color: 1
Size: 318676 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 345540 Color: 1
Size: 338319 Color: 1
Size: 316142 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 349417 Color: 1
Size: 333030 Color: 1
Size: 317554 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 352101 Color: 1
Size: 326940 Color: 1
Size: 320960 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 352819 Color: 1
Size: 346627 Color: 1
Size: 300555 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 353108 Color: 1
Size: 339004 Color: 1
Size: 307889 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 353673 Color: 1
Size: 348010 Color: 1
Size: 298318 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 356642 Color: 1
Size: 355132 Color: 1
Size: 288227 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 357181 Color: 1
Size: 323834 Color: 1
Size: 318986 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 358127 Color: 1
Size: 349644 Color: 1
Size: 292230 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 362028 Color: 1
Size: 345954 Color: 1
Size: 292019 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 362949 Color: 1
Size: 360529 Color: 1
Size: 276523 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 366445 Color: 1
Size: 325790 Color: 1
Size: 307766 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 366682 Color: 1
Size: 331275 Color: 1
Size: 302044 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 367311 Color: 1
Size: 330536 Color: 1
Size: 302154 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 367426 Color: 1
Size: 321665 Color: 1
Size: 310910 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 367641 Color: 1
Size: 339444 Color: 1
Size: 292916 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 368460 Color: 1
Size: 346958 Color: 1
Size: 284583 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 368768 Color: 1
Size: 341649 Color: 1
Size: 289584 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 368835 Color: 1
Size: 345569 Color: 1
Size: 285597 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 369638 Color: 1
Size: 335761 Color: 1
Size: 294602 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 369865 Color: 1
Size: 360122 Color: 1
Size: 270014 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 370250 Color: 1
Size: 366175 Color: 1
Size: 263576 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 371230 Color: 1
Size: 347979 Color: 1
Size: 280792 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 371664 Color: 1
Size: 324243 Color: 1
Size: 304094 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 373643 Color: 1
Size: 347139 Color: 1
Size: 279219 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 374990 Color: 1
Size: 346768 Color: 1
Size: 278243 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 375360 Color: 1
Size: 340438 Color: 1
Size: 284203 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 375838 Color: 1
Size: 355962 Color: 1
Size: 268201 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 376506 Color: 1
Size: 354574 Color: 1
Size: 268921 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 377648 Color: 1
Size: 357070 Color: 1
Size: 265283 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 377933 Color: 1
Size: 368034 Color: 1
Size: 254034 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 377954 Color: 1
Size: 355410 Color: 1
Size: 266637 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 378375 Color: 1
Size: 363630 Color: 1
Size: 257996 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 380453 Color: 1
Size: 364055 Color: 1
Size: 255493 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 381072 Color: 1
Size: 322545 Color: 1
Size: 296384 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 383162 Color: 1
Size: 350177 Color: 1
Size: 266662 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 383446 Color: 1
Size: 341290 Color: 1
Size: 275265 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 384510 Color: 1
Size: 330081 Color: 1
Size: 285410 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 384795 Color: 1
Size: 342879 Color: 1
Size: 272327 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 385110 Color: 1
Size: 320556 Color: 1
Size: 294335 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 385264 Color: 1
Size: 342060 Color: 1
Size: 272677 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 385440 Color: 1
Size: 327371 Color: 1
Size: 287190 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 385681 Color: 1
Size: 339397 Color: 1
Size: 274923 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 387090 Color: 1
Size: 312898 Color: 1
Size: 300013 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 387183 Color: 1
Size: 360039 Color: 1
Size: 252779 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 387368 Color: 1
Size: 317991 Color: 1
Size: 294642 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 388311 Color: 1
Size: 316843 Color: 1
Size: 294847 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 388591 Color: 1
Size: 361383 Color: 1
Size: 250027 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 389340 Color: 1
Size: 333391 Color: 1
Size: 277270 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 390265 Color: 1
Size: 355526 Color: 1
Size: 254210 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 391435 Color: 1
Size: 326432 Color: 1
Size: 282134 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 392604 Color: 1
Size: 309476 Color: 1
Size: 297921 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 392605 Color: 1
Size: 328230 Color: 1
Size: 279166 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 392785 Color: 1
Size: 303625 Color: 1
Size: 303591 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 393150 Color: 1
Size: 317133 Color: 1
Size: 289718 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 393606 Color: 1
Size: 352275 Color: 1
Size: 254120 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 395284 Color: 1
Size: 334288 Color: 1
Size: 270429 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 395851 Color: 1
Size: 348082 Color: 1
Size: 256068 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 395891 Color: 1
Size: 335984 Color: 1
Size: 268126 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 396258 Color: 1
Size: 301954 Color: 1
Size: 301789 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 396518 Color: 1
Size: 323179 Color: 1
Size: 280304 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 396678 Color: 1
Size: 314401 Color: 1
Size: 288922 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 397824 Color: 1
Size: 334067 Color: 1
Size: 268110 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 398145 Color: 1
Size: 342425 Color: 1
Size: 259431 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 399124 Color: 1
Size: 341894 Color: 1
Size: 258983 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 399233 Color: 1
Size: 346960 Color: 1
Size: 253808 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 399421 Color: 1
Size: 323558 Color: 1
Size: 277022 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 400675 Color: 1
Size: 339580 Color: 1
Size: 259746 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 400850 Color: 1
Size: 336581 Color: 1
Size: 262570 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 400935 Color: 1
Size: 338714 Color: 1
Size: 260352 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 401261 Color: 1
Size: 306167 Color: 1
Size: 292573 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 404805 Color: 1
Size: 336927 Color: 1
Size: 258269 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 405691 Color: 1
Size: 318841 Color: 1
Size: 275469 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 407309 Color: 1
Size: 335587 Color: 1
Size: 257105 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 409090 Color: 1
Size: 309625 Color: 1
Size: 281286 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 410276 Color: 1
Size: 333575 Color: 1
Size: 256150 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 410954 Color: 1
Size: 312176 Color: 1
Size: 276871 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 411527 Color: 1
Size: 316349 Color: 1
Size: 272125 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 412018 Color: 1
Size: 335585 Color: 1
Size: 252398 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 413405 Color: 1
Size: 293345 Color: 1
Size: 293251 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 414051 Color: 1
Size: 316701 Color: 1
Size: 269249 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 414120 Color: 1
Size: 320572 Color: 1
Size: 265309 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 414791 Color: 1
Size: 322406 Color: 1
Size: 262804 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 415549 Color: 1
Size: 311175 Color: 1
Size: 273277 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 417688 Color: 1
Size: 325589 Color: 1
Size: 256724 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 418731 Color: 1
Size: 306930 Color: 1
Size: 274340 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 419598 Color: 1
Size: 295743 Color: 1
Size: 284660 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 420042 Color: 1
Size: 322407 Color: 1
Size: 257552 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 420261 Color: 1
Size: 297103 Color: 1
Size: 282637 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 420901 Color: 1
Size: 292266 Color: 1
Size: 286834 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 421036 Color: 1
Size: 307912 Color: 1
Size: 271053 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 421672 Color: 1
Size: 321502 Color: 1
Size: 256827 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 422289 Color: 1
Size: 289160 Color: 1
Size: 288552 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 423500 Color: 1
Size: 298310 Color: 1
Size: 278191 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 424096 Color: 1
Size: 309577 Color: 1
Size: 266328 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 425862 Color: 1
Size: 315096 Color: 1
Size: 259043 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 426864 Color: 1
Size: 306336 Color: 1
Size: 266801 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 427314 Color: 1
Size: 322450 Color: 1
Size: 250237 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 428376 Color: 1
Size: 296379 Color: 1
Size: 275246 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 428771 Color: 1
Size: 319847 Color: 1
Size: 251383 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 429401 Color: 1
Size: 309216 Color: 1
Size: 261384 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 431383 Color: 1
Size: 304755 Color: 1
Size: 263863 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 432312 Color: 1
Size: 306985 Color: 1
Size: 260704 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 432326 Color: 1
Size: 302914 Color: 1
Size: 264761 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 432429 Color: 1
Size: 312012 Color: 1
Size: 255560 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 432868 Color: 1
Size: 291356 Color: 1
Size: 275777 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 433067 Color: 1
Size: 298103 Color: 1
Size: 268831 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 434214 Color: 1
Size: 285481 Color: 1
Size: 280306 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 434860 Color: 1
Size: 299224 Color: 1
Size: 265917 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 435115 Color: 1
Size: 308556 Color: 1
Size: 256330 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 436245 Color: 1
Size: 313268 Color: 1
Size: 250488 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 436743 Color: 1
Size: 307494 Color: 1
Size: 255764 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 438049 Color: 1
Size: 287487 Color: 1
Size: 274465 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 438291 Color: 1
Size: 309457 Color: 1
Size: 252253 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 438596 Color: 1
Size: 304568 Color: 1
Size: 256837 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 438629 Color: 1
Size: 309368 Color: 1
Size: 252004 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 439861 Color: 1
Size: 300454 Color: 1
Size: 259686 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 439882 Color: 1
Size: 295923 Color: 1
Size: 264196 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 440713 Color: 1
Size: 280743 Color: 1
Size: 278545 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 441217 Color: 1
Size: 308277 Color: 1
Size: 250507 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 441443 Color: 1
Size: 304516 Color: 1
Size: 254042 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 441778 Color: 1
Size: 294650 Color: 1
Size: 263573 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 442070 Color: 1
Size: 302488 Color: 1
Size: 255443 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 443049 Color: 1
Size: 289385 Color: 1
Size: 267567 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 444089 Color: 1
Size: 293225 Color: 1
Size: 262687 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 446627 Color: 1
Size: 294766 Color: 1
Size: 258608 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 447525 Color: 1
Size: 284196 Color: 1
Size: 268280 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 449276 Color: 1
Size: 294735 Color: 1
Size: 255990 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 449350 Color: 1
Size: 296978 Color: 1
Size: 253673 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 451898 Color: 1
Size: 285478 Color: 1
Size: 262625 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 454601 Color: 1
Size: 289760 Color: 1
Size: 255640 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 454944 Color: 1
Size: 288654 Color: 1
Size: 256403 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 456260 Color: 1
Size: 290777 Color: 1
Size: 252964 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 459648 Color: 1
Size: 288131 Color: 1
Size: 252222 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 459938 Color: 1
Size: 286027 Color: 1
Size: 254036 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 460384 Color: 1
Size: 288121 Color: 1
Size: 251496 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 461850 Color: 1
Size: 286254 Color: 1
Size: 251897 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 463662 Color: 1
Size: 280510 Color: 1
Size: 255829 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 464907 Color: 1
Size: 276470 Color: 1
Size: 258624 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 468077 Color: 1
Size: 281913 Color: 1
Size: 250011 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 469399 Color: 1
Size: 265687 Color: 1
Size: 264915 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 470153 Color: 1
Size: 271356 Color: 1
Size: 258492 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 470576 Color: 1
Size: 272130 Color: 1
Size: 257295 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 470683 Color: 1
Size: 271911 Color: 1
Size: 257407 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 472708 Color: 1
Size: 272170 Color: 1
Size: 255123 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 473400 Color: 1
Size: 276512 Color: 1
Size: 250089 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 473567 Color: 1
Size: 274154 Color: 1
Size: 252280 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 473816 Color: 1
Size: 264467 Color: 1
Size: 261718 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 475820 Color: 1
Size: 263867 Color: 1
Size: 260314 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 476080 Color: 1
Size: 268361 Color: 1
Size: 255560 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 476585 Color: 1
Size: 264762 Color: 1
Size: 258654 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 481002 Color: 1
Size: 266058 Color: 1
Size: 252941 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 482523 Color: 1
Size: 265108 Color: 1
Size: 252370 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 482602 Color: 1
Size: 260612 Color: 1
Size: 256787 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 485368 Color: 1
Size: 261561 Color: 1
Size: 253072 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 485385 Color: 1
Size: 262858 Color: 1
Size: 251758 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 485812 Color: 1
Size: 263658 Color: 1
Size: 250531 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 488058 Color: 1
Size: 257810 Color: 1
Size: 254133 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 489907 Color: 1
Size: 257008 Color: 1
Size: 253086 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 490381 Color: 1
Size: 257588 Color: 1
Size: 252032 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 490765 Color: 1
Size: 255956 Color: 1
Size: 253280 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 492463 Color: 1
Size: 255821 Color: 1
Size: 251717 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 494024 Color: 1
Size: 254413 Color: 1
Size: 251564 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 494188 Color: 1
Size: 255654 Color: 1
Size: 250159 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 498201 Color: 1
Size: 251708 Color: 1
Size: 250092 Color: 0

Total size: 167000167
Total free space: 0

