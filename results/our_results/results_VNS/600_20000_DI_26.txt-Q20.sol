Capicity Bin: 16032
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8528 Color: 13
Size: 7120 Color: 19
Size: 384 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8898 Color: 8
Size: 6676 Color: 19
Size: 458 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10968 Color: 3
Size: 4744 Color: 19
Size: 320 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11043 Color: 14
Size: 3989 Color: 17
Size: 1000 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 0
Size: 4152 Color: 3
Size: 816 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11539 Color: 8
Size: 2473 Color: 4
Size: 2020 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11736 Color: 15
Size: 4024 Color: 9
Size: 272 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12338 Color: 2
Size: 3288 Color: 11
Size: 406 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12823 Color: 5
Size: 2649 Color: 1
Size: 560 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12852 Color: 7
Size: 2316 Color: 17
Size: 864 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13166 Color: 15
Size: 1454 Color: 0
Size: 1412 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13213 Color: 7
Size: 2323 Color: 4
Size: 496 Color: 18

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13311 Color: 8
Size: 1601 Color: 5
Size: 1120 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 6
Size: 2210 Color: 5
Size: 502 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13386 Color: 5
Size: 2264 Color: 0
Size: 382 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13359 Color: 8
Size: 2255 Color: 13
Size: 418 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13382 Color: 7
Size: 2004 Color: 17
Size: 646 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 5
Size: 2408 Color: 19
Size: 208 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13529 Color: 14
Size: 2035 Color: 15
Size: 468 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 19
Size: 2160 Color: 0
Size: 328 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13612 Color: 5
Size: 1428 Color: 4
Size: 992 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13591 Color: 1
Size: 1611 Color: 7
Size: 830 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13689 Color: 18
Size: 2009 Color: 11
Size: 334 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13702 Color: 12
Size: 2046 Color: 15
Size: 284 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 0
Size: 2008 Color: 2
Size: 304 Color: 5

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13765 Color: 10
Size: 1731 Color: 15
Size: 536 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13766 Color: 8
Size: 1896 Color: 12
Size: 370 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13873 Color: 17
Size: 1605 Color: 7
Size: 554 Color: 7

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 2
Size: 1328 Color: 10
Size: 826 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 19
Size: 1088 Color: 8
Size: 1052 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13910 Color: 3
Size: 1770 Color: 0
Size: 352 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13929 Color: 2
Size: 1753 Color: 1
Size: 350 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13957 Color: 5
Size: 1771 Color: 16
Size: 304 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 10
Size: 1328 Color: 13
Size: 712 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14002 Color: 4
Size: 1248 Color: 11
Size: 782 Color: 16

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14025 Color: 5
Size: 1663 Color: 13
Size: 344 Color: 19

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 17
Size: 1686 Color: 8
Size: 284 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14082 Color: 7
Size: 1334 Color: 9
Size: 616 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14111 Color: 17
Size: 1553 Color: 15
Size: 368 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 17
Size: 1784 Color: 19
Size: 128 Color: 5

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 7
Size: 1542 Color: 5
Size: 322 Color: 9

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14189 Color: 18
Size: 1537 Color: 14
Size: 306 Color: 12

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14150 Color: 19
Size: 1442 Color: 17
Size: 440 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 7
Size: 944 Color: 5
Size: 872 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14288 Color: 7
Size: 1456 Color: 8
Size: 288 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14290 Color: 1
Size: 1382 Color: 6
Size: 360 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14302 Color: 19
Size: 1332 Color: 5
Size: 398 Color: 10

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 12
Size: 1334 Color: 13
Size: 354 Color: 18

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 15
Size: 1188 Color: 7
Size: 488 Color: 10

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14358 Color: 14
Size: 850 Color: 5
Size: 824 Color: 14

Bin 51: 1 of cap free
Amount of items: 7
Items: 
Size: 8020 Color: 8
Size: 1740 Color: 4
Size: 1560 Color: 16
Size: 1528 Color: 7
Size: 1497 Color: 10
Size: 1334 Color: 0
Size: 352 Color: 6

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 8893 Color: 2
Size: 6674 Color: 8
Size: 464 Color: 11

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 11031 Color: 9
Size: 4232 Color: 0
Size: 768 Color: 9

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 11428 Color: 14
Size: 4411 Color: 11
Size: 192 Color: 10

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 11932 Color: 19
Size: 4099 Color: 3

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 12306 Color: 5
Size: 2891 Color: 8
Size: 834 Color: 6

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12331 Color: 0
Size: 3268 Color: 16
Size: 432 Color: 15

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12602 Color: 1
Size: 3085 Color: 16
Size: 344 Color: 8

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12737 Color: 2
Size: 1724 Color: 8
Size: 1570 Color: 11

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 13041 Color: 3
Size: 2602 Color: 0
Size: 388 Color: 11

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13065 Color: 9
Size: 1508 Color: 2
Size: 1458 Color: 19

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 13356 Color: 6
Size: 2675 Color: 14

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13626 Color: 15
Size: 2229 Color: 5
Size: 176 Color: 15

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13944 Color: 19
Size: 2087 Color: 2

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 14095 Color: 13
Size: 1556 Color: 3
Size: 380 Color: 19

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 14233 Color: 8
Size: 1798 Color: 9

Bin 67: 2 of cap free
Amount of items: 5
Items: 
Size: 8025 Color: 2
Size: 4151 Color: 3
Size: 1782 Color: 1
Size: 1400 Color: 1
Size: 672 Color: 6

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 9208 Color: 2
Size: 6146 Color: 14
Size: 676 Color: 9

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 11019 Color: 6
Size: 4179 Color: 1
Size: 832 Color: 18

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 11051 Color: 12
Size: 4219 Color: 8
Size: 760 Color: 11

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 11075 Color: 2
Size: 3799 Color: 19
Size: 1156 Color: 1

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 11683 Color: 19
Size: 4131 Color: 11
Size: 216 Color: 9

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 11971 Color: 10
Size: 3771 Color: 13
Size: 288 Color: 16

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 4
Size: 3322 Color: 13
Size: 312 Color: 2

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 5
Size: 2652 Color: 1
Size: 358 Color: 14

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13283 Color: 9
Size: 2747 Color: 15

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 17
Size: 2390 Color: 7

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13780 Color: 12
Size: 1404 Color: 5
Size: 846 Color: 7

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13948 Color: 13
Size: 2082 Color: 9

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14326 Color: 16
Size: 1704 Color: 2

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 10820 Color: 19
Size: 5209 Color: 5

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 11127 Color: 19
Size: 4902 Color: 5

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 11442 Color: 4
Size: 4159 Color: 3
Size: 428 Color: 16

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 12185 Color: 18
Size: 3380 Color: 13
Size: 464 Color: 19

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 13513 Color: 6
Size: 2516 Color: 19

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 14228 Color: 17
Size: 1801 Color: 16

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 8034 Color: 3
Size: 7786 Color: 16
Size: 208 Color: 1

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 12
Size: 5044 Color: 15
Size: 212 Color: 6

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 11475 Color: 0
Size: 3625 Color: 2
Size: 928 Color: 17

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 13623 Color: 14
Size: 1997 Color: 19
Size: 408 Color: 0

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 9
Size: 1984 Color: 2

Bin 92: 5 of cap free
Amount of items: 5
Items: 
Size: 8026 Color: 17
Size: 4641 Color: 10
Size: 2006 Color: 2
Size: 754 Color: 0
Size: 600 Color: 9

Bin 93: 5 of cap free
Amount of items: 3
Items: 
Size: 8024 Color: 14
Size: 6671 Color: 15
Size: 1332 Color: 7

Bin 94: 5 of cap free
Amount of items: 3
Items: 
Size: 10955 Color: 19
Size: 4348 Color: 4
Size: 724 Color: 8

Bin 95: 5 of cap free
Amount of items: 3
Items: 
Size: 11939 Color: 16
Size: 3800 Color: 9
Size: 288 Color: 6

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 12
Size: 3411 Color: 18

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 14099 Color: 12
Size: 1928 Color: 10

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 8028 Color: 13
Size: 6666 Color: 18
Size: 1332 Color: 8

Bin 99: 6 of cap free
Amount of items: 3
Items: 
Size: 12732 Color: 6
Size: 2862 Color: 2
Size: 432 Color: 15

Bin 100: 7 of cap free
Amount of items: 3
Items: 
Size: 12855 Color: 5
Size: 2290 Color: 19
Size: 880 Color: 2

Bin 101: 7 of cap free
Amount of items: 2
Items: 
Size: 13578 Color: 0
Size: 2447 Color: 3

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 13861 Color: 4
Size: 2164 Color: 19

Bin 103: 7 of cap free
Amount of items: 2
Items: 
Size: 14108 Color: 16
Size: 1917 Color: 11

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 14172 Color: 15
Size: 1853 Color: 2

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 14237 Color: 4
Size: 1788 Color: 11

Bin 106: 8 of cap free
Amount of items: 37
Items: 
Size: 568 Color: 6
Size: 548 Color: 1
Size: 534 Color: 5
Size: 528 Color: 18
Size: 528 Color: 6
Size: 520 Color: 13
Size: 512 Color: 19
Size: 512 Color: 14
Size: 504 Color: 15
Size: 498 Color: 18
Size: 494 Color: 17
Size: 488 Color: 9
Size: 476 Color: 6
Size: 456 Color: 13
Size: 456 Color: 11
Size: 450 Color: 7
Size: 448 Color: 11
Size: 444 Color: 2
Size: 440 Color: 16
Size: 440 Color: 10
Size: 418 Color: 12
Size: 416 Color: 4
Size: 400 Color: 14
Size: 400 Color: 10
Size: 392 Color: 10
Size: 390 Color: 17
Size: 388 Color: 0
Size: 384 Color: 3
Size: 376 Color: 12
Size: 356 Color: 10
Size: 352 Color: 19
Size: 352 Color: 11
Size: 344 Color: 5
Size: 336 Color: 0
Size: 298 Color: 4
Size: 298 Color: 2
Size: 280 Color: 9

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 10923 Color: 10
Size: 5101 Color: 19

Bin 108: 8 of cap free
Amount of items: 3
Items: 
Size: 12180 Color: 5
Size: 2236 Color: 16
Size: 1608 Color: 10

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 13562 Color: 12
Size: 2462 Color: 9

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 13733 Color: 12
Size: 2291 Color: 8

Bin 111: 9 of cap free
Amount of items: 3
Items: 
Size: 12705 Color: 14
Size: 2962 Color: 5
Size: 356 Color: 9

Bin 112: 9 of cap free
Amount of items: 2
Items: 
Size: 13455 Color: 8
Size: 2568 Color: 1

Bin 113: 10 of cap free
Amount of items: 3
Items: 
Size: 9100 Color: 16
Size: 6682 Color: 10
Size: 240 Color: 2

Bin 114: 10 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 13
Size: 3582 Color: 14

Bin 115: 10 of cap free
Amount of items: 2
Items: 
Size: 12810 Color: 7
Size: 3212 Color: 4

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 12910 Color: 5
Size: 3112 Color: 3

Bin 117: 11 of cap free
Amount of items: 8
Items: 
Size: 8018 Color: 10
Size: 1501 Color: 18
Size: 1416 Color: 13
Size: 1370 Color: 14
Size: 1332 Color: 0
Size: 1328 Color: 3
Size: 752 Color: 11
Size: 304 Color: 8

Bin 118: 11 of cap free
Amount of items: 3
Items: 
Size: 12533 Color: 16
Size: 3272 Color: 13
Size: 216 Color: 19

Bin 119: 11 of cap free
Amount of items: 2
Items: 
Size: 12636 Color: 13
Size: 3385 Color: 7

Bin 120: 11 of cap free
Amount of items: 2
Items: 
Size: 13097 Color: 0
Size: 2924 Color: 9

Bin 121: 11 of cap free
Amount of items: 3
Items: 
Size: 13220 Color: 6
Size: 2521 Color: 15
Size: 280 Color: 2

Bin 122: 12 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 19
Size: 3804 Color: 15
Size: 236 Color: 3

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 14360 Color: 13
Size: 1660 Color: 15

Bin 124: 13 of cap free
Amount of items: 3
Items: 
Size: 10245 Color: 17
Size: 5464 Color: 6
Size: 310 Color: 2

Bin 125: 13 of cap free
Amount of items: 2
Items: 
Size: 13964 Color: 6
Size: 2055 Color: 17

Bin 126: 13 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 7
Size: 1833 Color: 9

Bin 127: 14 of cap free
Amount of items: 2
Items: 
Size: 12680 Color: 3
Size: 3338 Color: 15

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 13245 Color: 8
Size: 2773 Color: 14

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 14324 Color: 1
Size: 1694 Color: 13

Bin 130: 15 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 17
Size: 4089 Color: 8

Bin 131: 15 of cap free
Amount of items: 2
Items: 
Size: 13009 Color: 5
Size: 3008 Color: 6

Bin 132: 15 of cap free
Amount of items: 2
Items: 
Size: 13833 Color: 10
Size: 2184 Color: 6

Bin 133: 16 of cap free
Amount of items: 3
Items: 
Size: 10298 Color: 7
Size: 5266 Color: 5
Size: 452 Color: 1

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 10344 Color: 3
Size: 5672 Color: 4

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 13260 Color: 7
Size: 2756 Color: 18

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 16
Size: 1626 Color: 8

Bin 137: 17 of cap free
Amount of items: 9
Items: 
Size: 8017 Color: 17
Size: 1200 Color: 3
Size: 1188 Color: 6
Size: 1152 Color: 16
Size: 1104 Color: 15
Size: 1068 Color: 1
Size: 912 Color: 16
Size: 830 Color: 9
Size: 544 Color: 11

Bin 138: 17 of cap free
Amount of items: 3
Items: 
Size: 10741 Color: 2
Size: 4122 Color: 9
Size: 1152 Color: 1

Bin 139: 17 of cap free
Amount of items: 3
Items: 
Size: 12357 Color: 1
Size: 3082 Color: 4
Size: 576 Color: 19

Bin 140: 17 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 11
Size: 3063 Color: 17

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 13809 Color: 4
Size: 2206 Color: 18

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 13286 Color: 10
Size: 2728 Color: 19

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 13521 Color: 0
Size: 2493 Color: 16

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 13745 Color: 1
Size: 2269 Color: 7

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 14107 Color: 10
Size: 1907 Color: 4

Bin 146: 19 of cap free
Amount of items: 2
Items: 
Size: 10662 Color: 8
Size: 5351 Color: 7

Bin 147: 19 of cap free
Amount of items: 2
Items: 
Size: 13327 Color: 8
Size: 2686 Color: 17

Bin 148: 19 of cap free
Amount of items: 2
Items: 
Size: 14340 Color: 8
Size: 1673 Color: 18

Bin 149: 23 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 17
Size: 3233 Color: 6

Bin 150: 25 of cap free
Amount of items: 2
Items: 
Size: 12800 Color: 11
Size: 3207 Color: 15

Bin 151: 26 of cap free
Amount of items: 2
Items: 
Size: 13905 Color: 18
Size: 2101 Color: 10

Bin 152: 28 of cap free
Amount of items: 3
Items: 
Size: 9480 Color: 19
Size: 6158 Color: 14
Size: 366 Color: 2

Bin 153: 29 of cap free
Amount of items: 3
Items: 
Size: 10732 Color: 3
Size: 4231 Color: 9
Size: 1040 Color: 8

Bin 154: 31 of cap free
Amount of items: 2
Items: 
Size: 10213 Color: 4
Size: 5788 Color: 2

Bin 155: 31 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 11
Size: 2149 Color: 10
Size: 1604 Color: 9

Bin 156: 31 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 13
Size: 3036 Color: 8
Size: 400 Color: 9

Bin 157: 32 of cap free
Amount of items: 7
Items: 
Size: 8021 Color: 15
Size: 1890 Color: 16
Size: 1884 Color: 17
Size: 1773 Color: 1
Size: 1422 Color: 11
Size: 818 Color: 14
Size: 192 Color: 18

Bin 158: 32 of cap free
Amount of items: 3
Items: 
Size: 9095 Color: 18
Size: 6681 Color: 13
Size: 224 Color: 12

Bin 159: 32 of cap free
Amount of items: 2
Items: 
Size: 13144 Color: 18
Size: 2856 Color: 15

Bin 160: 33 of cap free
Amount of items: 2
Items: 
Size: 10463 Color: 11
Size: 5536 Color: 3

Bin 161: 33 of cap free
Amount of items: 2
Items: 
Size: 13082 Color: 0
Size: 2917 Color: 18

Bin 162: 35 of cap free
Amount of items: 2
Items: 
Size: 11738 Color: 19
Size: 4259 Color: 4

Bin 163: 36 of cap free
Amount of items: 2
Items: 
Size: 13732 Color: 19
Size: 2264 Color: 0

Bin 164: 37 of cap free
Amount of items: 2
Items: 
Size: 14053 Color: 15
Size: 1942 Color: 13

Bin 165: 38 of cap free
Amount of items: 2
Items: 
Size: 9522 Color: 6
Size: 6472 Color: 9

Bin 166: 39 of cap free
Amount of items: 2
Items: 
Size: 14378 Color: 13
Size: 1615 Color: 1

Bin 167: 42 of cap free
Amount of items: 2
Items: 
Size: 14037 Color: 11
Size: 1953 Color: 15

Bin 168: 44 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 18
Size: 2092 Color: 10

Bin 169: 45 of cap free
Amount of items: 2
Items: 
Size: 10036 Color: 3
Size: 5951 Color: 16

Bin 170: 45 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 14
Size: 2351 Color: 13

Bin 171: 45 of cap free
Amount of items: 2
Items: 
Size: 13894 Color: 2
Size: 2093 Color: 18

Bin 172: 47 of cap free
Amount of items: 2
Items: 
Size: 11507 Color: 5
Size: 4478 Color: 10

Bin 173: 47 of cap free
Amount of items: 2
Items: 
Size: 13637 Color: 13
Size: 2348 Color: 6

Bin 174: 48 of cap free
Amount of items: 24
Items: 
Size: 960 Color: 1
Size: 892 Color: 13
Size: 880 Color: 1
Size: 816 Color: 3
Size: 764 Color: 18
Size: 758 Color: 3
Size: 752 Color: 12
Size: 752 Color: 2
Size: 718 Color: 6
Size: 682 Color: 8
Size: 672 Color: 9
Size: 664 Color: 11
Size: 640 Color: 19
Size: 640 Color: 14
Size: 640 Color: 3
Size: 612 Color: 19
Size: 612 Color: 5
Size: 588 Color: 9
Size: 584 Color: 0
Size: 582 Color: 6
Size: 576 Color: 1
Size: 528 Color: 14
Size: 480 Color: 0
Size: 192 Color: 5

Bin 175: 50 of cap free
Amount of items: 3
Items: 
Size: 8029 Color: 16
Size: 6673 Color: 8
Size: 1280 Color: 6

Bin 176: 52 of cap free
Amount of items: 3
Items: 
Size: 9092 Color: 3
Size: 6684 Color: 1
Size: 204 Color: 15

Bin 177: 52 of cap free
Amount of items: 2
Items: 
Size: 14169 Color: 10
Size: 1811 Color: 4

Bin 178: 53 of cap free
Amount of items: 2
Items: 
Size: 12153 Color: 9
Size: 3826 Color: 5

Bin 179: 54 of cap free
Amount of items: 3
Items: 
Size: 12482 Color: 18
Size: 3400 Color: 9
Size: 96 Color: 5

Bin 180: 61 of cap free
Amount of items: 2
Items: 
Size: 11115 Color: 12
Size: 4856 Color: 4

Bin 181: 68 of cap free
Amount of items: 2
Items: 
Size: 13436 Color: 16
Size: 2528 Color: 15

Bin 182: 70 of cap free
Amount of items: 2
Items: 
Size: 8042 Color: 18
Size: 7920 Color: 8

Bin 183: 76 of cap free
Amount of items: 2
Items: 
Size: 12524 Color: 8
Size: 3432 Color: 3

Bin 184: 84 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 2
Size: 3844 Color: 9

Bin 185: 91 of cap free
Amount of items: 3
Items: 
Size: 9824 Color: 7
Size: 5781 Color: 5
Size: 336 Color: 1

Bin 186: 98 of cap free
Amount of items: 2
Items: 
Size: 9988 Color: 5
Size: 5946 Color: 9

Bin 187: 102 of cap free
Amount of items: 2
Items: 
Size: 12046 Color: 10
Size: 3884 Color: 15

Bin 188: 105 of cap free
Amount of items: 3
Items: 
Size: 9783 Color: 1
Size: 5904 Color: 16
Size: 240 Color: 6

Bin 189: 111 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 8
Size: 6677 Color: 16
Size: 964 Color: 2

Bin 190: 112 of cap free
Amount of items: 2
Items: 
Size: 9240 Color: 5
Size: 6680 Color: 8

Bin 191: 123 of cap free
Amount of items: 2
Items: 
Size: 11086 Color: 6
Size: 4823 Color: 5

Bin 192: 132 of cap free
Amount of items: 2
Items: 
Size: 11480 Color: 18
Size: 4420 Color: 5

Bin 193: 140 of cap free
Amount of items: 3
Items: 
Size: 10216 Color: 5
Size: 5004 Color: 3
Size: 672 Color: 14

Bin 194: 150 of cap free
Amount of items: 3
Items: 
Size: 9714 Color: 11
Size: 5780 Color: 18
Size: 388 Color: 8

Bin 195: 163 of cap free
Amount of items: 2
Items: 
Size: 9613 Color: 14
Size: 6256 Color: 6

Bin 196: 166 of cap free
Amount of items: 2
Items: 
Size: 9618 Color: 10
Size: 6248 Color: 13

Bin 197: 168 of cap free
Amount of items: 2
Items: 
Size: 11476 Color: 11
Size: 4388 Color: 16

Bin 198: 172 of cap free
Amount of items: 2
Items: 
Size: 10362 Color: 12
Size: 5498 Color: 7

Bin 199: 11898 of cap free
Amount of items: 14
Items: 
Size: 332 Color: 13
Size: 324 Color: 19
Size: 322 Color: 11
Size: 320 Color: 14
Size: 320 Color: 14
Size: 304 Color: 0
Size: 296 Color: 16
Size: 288 Color: 13
Size: 272 Color: 17
Size: 272 Color: 9
Size: 272 Color: 8
Size: 272 Color: 4
Size: 272 Color: 2
Size: 268 Color: 7

Total size: 3174336
Total free space: 16032

