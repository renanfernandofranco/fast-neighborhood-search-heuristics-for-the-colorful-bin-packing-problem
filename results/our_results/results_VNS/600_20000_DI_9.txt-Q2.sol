Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11153 Color: 1
Size: 7073 Color: 0
Size: 1422 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 1
Size: 7820 Color: 0
Size: 664 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11628 Color: 0
Size: 7052 Color: 0
Size: 968 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11680 Color: 1
Size: 6968 Color: 0
Size: 1000 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13792 Color: 1
Size: 5408 Color: 1
Size: 448 Color: 0

Bin 6: 0 of cap free
Amount of items: 5
Items: 
Size: 14248 Color: 1
Size: 1862 Color: 0
Size: 1856 Color: 0
Size: 858 Color: 0
Size: 824 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 14288 Color: 1
Size: 4368 Color: 1
Size: 992 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14259 Color: 0
Size: 4797 Color: 1
Size: 592 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14519 Color: 1
Size: 4275 Color: 1
Size: 854 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 15216 Color: 1
Size: 3280 Color: 1
Size: 1152 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15478 Color: 1
Size: 3416 Color: 1
Size: 754 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15496 Color: 0
Size: 3260 Color: 1
Size: 892 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15939 Color: 1
Size: 3091 Color: 1
Size: 618 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15990 Color: 0
Size: 2976 Color: 1
Size: 682 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 16136 Color: 0
Size: 1768 Color: 0
Size: 1744 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 16224 Color: 1
Size: 2992 Color: 1
Size: 432 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 16146 Color: 0
Size: 2204 Color: 1
Size: 1298 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 16231 Color: 1
Size: 2393 Color: 0
Size: 1024 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 16258 Color: 0
Size: 3018 Color: 1
Size: 372 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16432 Color: 0
Size: 1808 Color: 0
Size: 1408 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 16636 Color: 0
Size: 1636 Color: 0
Size: 1376 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16798 Color: 0
Size: 1810 Color: 1
Size: 1040 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16788 Color: 1
Size: 2174 Color: 1
Size: 686 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16840 Color: 0
Size: 2528 Color: 0
Size: 280 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16800 Color: 1
Size: 1648 Color: 0
Size: 1200 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16904 Color: 1
Size: 1736 Color: 0
Size: 1008 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 17388 Color: 1
Size: 1884 Color: 0
Size: 376 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 17404 Color: 1
Size: 1408 Color: 0
Size: 836 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 17464 Color: 1
Size: 1536 Color: 0
Size: 648 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 17478 Color: 0
Size: 1636 Color: 0
Size: 534 Color: 1

Bin 31: 1 of cap free
Amount of items: 9
Items: 
Size: 9827 Color: 0
Size: 1632 Color: 1
Size: 1632 Color: 1
Size: 1420 Color: 1
Size: 1184 Color: 0
Size: 1160 Color: 0
Size: 1104 Color: 0
Size: 1088 Color: 1
Size: 600 Color: 0

Bin 32: 1 of cap free
Amount of items: 4
Items: 
Size: 11865 Color: 1
Size: 7122 Color: 0
Size: 340 Color: 1
Size: 320 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 13303 Color: 1
Size: 5992 Color: 1
Size: 352 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 0
Size: 5289 Color: 1
Size: 530 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 14872 Color: 1
Size: 3775 Color: 0
Size: 1000 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 15907 Color: 0
Size: 3220 Color: 1
Size: 520 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 15915 Color: 0
Size: 1900 Color: 0
Size: 1832 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 16453 Color: 0
Size: 2576 Color: 1
Size: 618 Color: 0

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 17226 Color: 0
Size: 2421 Color: 1

Bin 40: 2 of cap free
Amount of items: 5
Items: 
Size: 9888 Color: 1
Size: 6100 Color: 1
Size: 1632 Color: 0
Size: 1020 Color: 1
Size: 1006 Color: 0

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 11129 Color: 0
Size: 7093 Color: 0
Size: 1424 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 11833 Color: 1
Size: 7121 Color: 0
Size: 692 Color: 0

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 11857 Color: 0
Size: 6493 Color: 1
Size: 1296 Color: 0

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 13234 Color: 0
Size: 5548 Color: 1
Size: 864 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 1
Size: 5818 Color: 1
Size: 444 Color: 0

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 13568 Color: 0
Size: 5346 Color: 1
Size: 732 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 14054 Color: 0
Size: 5200 Color: 1
Size: 392 Color: 0

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 16236 Color: 1
Size: 3410 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 16252 Color: 1
Size: 1718 Color: 0
Size: 1676 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 16274 Color: 1
Size: 3036 Color: 1
Size: 336 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 16436 Color: 1
Size: 2922 Color: 0
Size: 288 Color: 1

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 16504 Color: 1
Size: 3142 Color: 0

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 17144 Color: 1
Size: 2502 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 14480 Color: 1
Size: 4269 Color: 1
Size: 896 Color: 0

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 15364 Color: 0
Size: 4281 Color: 1

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 16565 Color: 1
Size: 3080 Color: 0

Bin 57: 4 of cap free
Amount of items: 3
Items: 
Size: 13448 Color: 0
Size: 5236 Color: 1
Size: 960 Color: 0

Bin 58: 4 of cap free
Amount of items: 3
Items: 
Size: 14228 Color: 1
Size: 5224 Color: 1
Size: 192 Color: 0

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 0
Size: 4884 Color: 1
Size: 560 Color: 0

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 15246 Color: 1
Size: 3790 Color: 0
Size: 608 Color: 0

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 16096 Color: 1
Size: 3548 Color: 0

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 16441 Color: 0
Size: 2387 Color: 1
Size: 816 Color: 0

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 16533 Color: 1
Size: 3111 Color: 0

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 0
Size: 2516 Color: 1
Size: 352 Color: 1

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 17012 Color: 0
Size: 2632 Color: 1

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 17418 Color: 1
Size: 2226 Color: 0

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 15119 Color: 1
Size: 4524 Color: 0

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 15131 Color: 0
Size: 4512 Color: 1

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 15878 Color: 0
Size: 3765 Color: 1

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 15947 Color: 0
Size: 3696 Color: 1

Bin 71: 6 of cap free
Amount of items: 5
Items: 
Size: 9848 Color: 0
Size: 3435 Color: 1
Size: 3415 Color: 1
Size: 1632 Color: 0
Size: 1312 Color: 0

Bin 72: 6 of cap free
Amount of items: 3
Items: 
Size: 11952 Color: 1
Size: 7122 Color: 0
Size: 568 Color: 1

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 16592 Color: 0
Size: 3050 Color: 1

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 17622 Color: 0
Size: 2020 Color: 1

Bin 75: 8 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 0
Size: 7016 Color: 1
Size: 336 Color: 0

Bin 76: 8 of cap free
Amount of items: 3
Items: 
Size: 13956 Color: 1
Size: 5428 Color: 0
Size: 256 Color: 0

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 15960 Color: 1
Size: 3680 Color: 0

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 17544 Color: 1
Size: 2096 Color: 0

Bin 79: 9 of cap free
Amount of items: 3
Items: 
Size: 11137 Color: 1
Size: 8182 Color: 0
Size: 320 Color: 1

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 17042 Color: 1
Size: 2597 Color: 0

Bin 81: 10 of cap free
Amount of items: 3
Items: 
Size: 15931 Color: 1
Size: 2571 Color: 0
Size: 1136 Color: 0

Bin 82: 10 of cap free
Amount of items: 2
Items: 
Size: 17356 Color: 0
Size: 2282 Color: 1

Bin 83: 10 of cap free
Amount of items: 2
Items: 
Size: 17462 Color: 1
Size: 2176 Color: 0

Bin 84: 10 of cap free
Amount of items: 4
Items: 
Size: 17596 Color: 1
Size: 1946 Color: 0
Size: 64 Color: 1
Size: 32 Color: 0

Bin 85: 10 of cap free
Amount of items: 4
Items: 
Size: 17672 Color: 0
Size: 1870 Color: 1
Size: 64 Color: 0
Size: 32 Color: 1

Bin 86: 12 of cap free
Amount of items: 4
Items: 
Size: 10848 Color: 0
Size: 6112 Color: 1
Size: 2224 Color: 1
Size: 452 Color: 0

Bin 87: 12 of cap free
Amount of items: 3
Items: 
Size: 14285 Color: 1
Size: 4295 Color: 1
Size: 1056 Color: 0

Bin 88: 13 of cap free
Amount of items: 14
Items: 
Size: 9825 Color: 1
Size: 896 Color: 1
Size: 896 Color: 1
Size: 852 Color: 1
Size: 770 Color: 1
Size: 768 Color: 1
Size: 756 Color: 0
Size: 752 Color: 1
Size: 704 Color: 0
Size: 704 Color: 0
Size: 704 Color: 0
Size: 704 Color: 0
Size: 688 Color: 0
Size: 616 Color: 0

Bin 89: 13 of cap free
Amount of items: 3
Items: 
Size: 13424 Color: 1
Size: 5683 Color: 1
Size: 528 Color: 0

Bin 90: 13 of cap free
Amount of items: 3
Items: 
Size: 16461 Color: 1
Size: 2936 Color: 0
Size: 238 Color: 1

Bin 91: 14 of cap free
Amount of items: 3
Items: 
Size: 11106 Color: 0
Size: 8050 Color: 0
Size: 478 Color: 1

Bin 92: 14 of cap free
Amount of items: 2
Items: 
Size: 14738 Color: 1
Size: 4896 Color: 0

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 16650 Color: 1
Size: 2984 Color: 0

Bin 94: 14 of cap free
Amount of items: 2
Items: 
Size: 16785 Color: 1
Size: 2849 Color: 0

Bin 95: 14 of cap free
Amount of items: 2
Items: 
Size: 17256 Color: 1
Size: 2378 Color: 0

Bin 96: 14 of cap free
Amount of items: 2
Items: 
Size: 17590 Color: 0
Size: 2044 Color: 1

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 17652 Color: 0
Size: 1982 Color: 1

Bin 98: 15 of cap free
Amount of items: 6
Items: 
Size: 9836 Color: 0
Size: 2960 Color: 1
Size: 2919 Color: 1
Size: 1336 Color: 1
Size: 1302 Color: 0
Size: 1280 Color: 0

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 14626 Color: 1
Size: 5007 Color: 0

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 12944 Color: 0
Size: 6688 Color: 1

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 13216 Color: 1
Size: 6416 Color: 0

Bin 102: 18 of cap free
Amount of items: 2
Items: 
Size: 13641 Color: 1
Size: 5989 Color: 0

Bin 103: 19 of cap free
Amount of items: 3
Items: 
Size: 11596 Color: 1
Size: 7101 Color: 0
Size: 932 Color: 1

Bin 104: 19 of cap free
Amount of items: 2
Items: 
Size: 16510 Color: 1
Size: 3119 Color: 0

Bin 105: 20 of cap free
Amount of items: 2
Items: 
Size: 11440 Color: 0
Size: 8188 Color: 1

Bin 106: 20 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 0
Size: 5424 Color: 1
Size: 832 Color: 0

Bin 107: 20 of cap free
Amount of items: 4
Items: 
Size: 15558 Color: 0
Size: 3670 Color: 1
Size: 208 Color: 1
Size: 192 Color: 0

Bin 108: 20 of cap free
Amount of items: 2
Items: 
Size: 17536 Color: 1
Size: 2092 Color: 0

Bin 109: 22 of cap free
Amount of items: 8
Items: 
Size: 9828 Color: 0
Size: 1780 Color: 1
Size: 1768 Color: 1
Size: 1636 Color: 1
Size: 1272 Color: 0
Size: 1272 Color: 0
Size: 1216 Color: 0
Size: 854 Color: 1

Bin 110: 22 of cap free
Amount of items: 2
Items: 
Size: 16978 Color: 1
Size: 2648 Color: 0

Bin 111: 22 of cap free
Amount of items: 2
Items: 
Size: 17008 Color: 1
Size: 2618 Color: 0

Bin 112: 23 of cap free
Amount of items: 3
Items: 
Size: 11105 Color: 0
Size: 8180 Color: 0
Size: 340 Color: 1

Bin 113: 24 of cap free
Amount of items: 2
Items: 
Size: 17616 Color: 1
Size: 2008 Color: 0

Bin 114: 26 of cap free
Amount of items: 2
Items: 
Size: 17326 Color: 0
Size: 2296 Color: 1

Bin 115: 27 of cap free
Amount of items: 3
Items: 
Size: 16433 Color: 1
Size: 2912 Color: 0
Size: 276 Color: 1

Bin 116: 28 of cap free
Amount of items: 3
Items: 
Size: 11156 Color: 0
Size: 8144 Color: 1
Size: 320 Color: 0

Bin 117: 28 of cap free
Amount of items: 2
Items: 
Size: 15740 Color: 0
Size: 3880 Color: 1

Bin 118: 28 of cap free
Amount of items: 4
Items: 
Size: 17576 Color: 1
Size: 1916 Color: 0
Size: 80 Color: 1
Size: 48 Color: 0

Bin 119: 29 of cap free
Amount of items: 6
Items: 
Size: 9840 Color: 0
Size: 3085 Color: 1
Size: 3072 Color: 1
Size: 1414 Color: 0
Size: 1344 Color: 0
Size: 864 Color: 1

Bin 120: 29 of cap free
Amount of items: 4
Items: 
Size: 13296 Color: 1
Size: 3443 Color: 1
Size: 1824 Color: 0
Size: 1056 Color: 0

Bin 121: 30 of cap free
Amount of items: 2
Items: 
Size: 13578 Color: 1
Size: 6040 Color: 0

Bin 122: 30 of cap free
Amount of items: 3
Items: 
Size: 14864 Color: 0
Size: 4490 Color: 1
Size: 264 Color: 0

Bin 123: 30 of cap free
Amount of items: 2
Items: 
Size: 17680 Color: 0
Size: 1938 Color: 1

Bin 124: 32 of cap free
Amount of items: 2
Items: 
Size: 15520 Color: 0
Size: 4096 Color: 1

Bin 125: 34 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 0
Size: 7026 Color: 1
Size: 256 Color: 1

Bin 126: 34 of cap free
Amount of items: 3
Items: 
Size: 16008 Color: 1
Size: 3478 Color: 0
Size: 128 Color: 1

Bin 127: 35 of cap free
Amount of items: 2
Items: 
Size: 16777 Color: 0
Size: 2836 Color: 1

Bin 128: 36 of cap free
Amount of items: 2
Items: 
Size: 16928 Color: 0
Size: 2684 Color: 1

Bin 129: 36 of cap free
Amount of items: 2
Items: 
Size: 17372 Color: 0
Size: 2240 Color: 1

Bin 130: 37 of cap free
Amount of items: 2
Items: 
Size: 15517 Color: 0
Size: 4094 Color: 1

Bin 131: 42 of cap free
Amount of items: 2
Items: 
Size: 17606 Color: 0
Size: 2000 Color: 1

Bin 132: 44 of cap free
Amount of items: 2
Items: 
Size: 17196 Color: 0
Size: 2408 Color: 1

Bin 133: 44 of cap free
Amount of items: 2
Items: 
Size: 17516 Color: 0
Size: 2088 Color: 1

Bin 134: 46 of cap free
Amount of items: 2
Items: 
Size: 14940 Color: 1
Size: 4662 Color: 0

Bin 135: 47 of cap free
Amount of items: 2
Items: 
Size: 13088 Color: 1
Size: 6513 Color: 0

Bin 136: 48 of cap free
Amount of items: 2
Items: 
Size: 12384 Color: 1
Size: 7216 Color: 0

Bin 137: 48 of cap free
Amount of items: 2
Items: 
Size: 14304 Color: 1
Size: 5296 Color: 0

Bin 138: 50 of cap free
Amount of items: 2
Items: 
Size: 15102 Color: 1
Size: 4496 Color: 0

Bin 139: 50 of cap free
Amount of items: 2
Items: 
Size: 16784 Color: 1
Size: 2814 Color: 0

Bin 140: 52 of cap free
Amount of items: 3
Items: 
Size: 14262 Color: 1
Size: 3632 Color: 1
Size: 1702 Color: 0

Bin 141: 53 of cap free
Amount of items: 2
Items: 
Size: 11408 Color: 1
Size: 8187 Color: 0

Bin 142: 53 of cap free
Amount of items: 2
Items: 
Size: 16914 Color: 1
Size: 2681 Color: 0

Bin 143: 55 of cap free
Amount of items: 2
Items: 
Size: 13609 Color: 1
Size: 5984 Color: 0

Bin 144: 56 of cap free
Amount of items: 2
Items: 
Size: 14416 Color: 1
Size: 5176 Color: 0

Bin 145: 57 of cap free
Amount of items: 2
Items: 
Size: 15047 Color: 0
Size: 4544 Color: 1

Bin 146: 57 of cap free
Amount of items: 3
Items: 
Size: 16478 Color: 1
Size: 2969 Color: 0
Size: 144 Color: 1

Bin 147: 59 of cap free
Amount of items: 2
Items: 
Size: 14527 Color: 0
Size: 5062 Color: 1

Bin 148: 60 of cap free
Amount of items: 3
Items: 
Size: 13796 Color: 1
Size: 5616 Color: 0
Size: 176 Color: 0

Bin 149: 60 of cap free
Amount of items: 2
Items: 
Size: 15396 Color: 1
Size: 4192 Color: 0

Bin 150: 64 of cap free
Amount of items: 2
Items: 
Size: 17184 Color: 0
Size: 2400 Color: 1

Bin 151: 64 of cap free
Amount of items: 2
Items: 
Size: 17504 Color: 0
Size: 2080 Color: 1

Bin 152: 65 of cap free
Amount of items: 2
Items: 
Size: 15728 Color: 0
Size: 3855 Color: 1

Bin 153: 66 of cap free
Amount of items: 2
Items: 
Size: 17274 Color: 1
Size: 2308 Color: 0

Bin 154: 73 of cap free
Amount of items: 2
Items: 
Size: 15559 Color: 1
Size: 4016 Color: 0

Bin 155: 74 of cap free
Amount of items: 5
Items: 
Size: 9834 Color: 1
Size: 3409 Color: 1
Size: 3099 Color: 1
Size: 1632 Color: 0
Size: 1600 Color: 0

Bin 156: 78 of cap free
Amount of items: 2
Items: 
Size: 16012 Color: 1
Size: 3558 Color: 0

Bin 157: 80 of cap free
Amount of items: 2
Items: 
Size: 15382 Color: 1
Size: 4186 Color: 0

Bin 158: 80 of cap free
Amount of items: 2
Items: 
Size: 16080 Color: 0
Size: 3488 Color: 1

Bin 159: 82 of cap free
Amount of items: 2
Items: 
Size: 12670 Color: 0
Size: 6896 Color: 1

Bin 160: 92 of cap free
Amount of items: 2
Items: 
Size: 12472 Color: 0
Size: 7084 Color: 1

Bin 161: 92 of cap free
Amount of items: 2
Items: 
Size: 17488 Color: 0
Size: 2068 Color: 1

Bin 162: 93 of cap free
Amount of items: 2
Items: 
Size: 15551 Color: 1
Size: 4004 Color: 0

Bin 163: 96 of cap free
Amount of items: 2
Items: 
Size: 15000 Color: 1
Size: 4552 Color: 0

Bin 164: 96 of cap free
Amount of items: 2
Items: 
Size: 17136 Color: 1
Size: 2416 Color: 0

Bin 165: 99 of cap free
Amount of items: 2
Items: 
Size: 12829 Color: 0
Size: 6720 Color: 1

Bin 166: 106 of cap free
Amount of items: 2
Items: 
Size: 17264 Color: 0
Size: 2278 Color: 1

Bin 167: 107 of cap free
Amount of items: 2
Items: 
Size: 16884 Color: 0
Size: 2657 Color: 1

Bin 168: 108 of cap free
Amount of items: 2
Items: 
Size: 14688 Color: 1
Size: 4852 Color: 0

Bin 169: 112 of cap free
Amount of items: 2
Items: 
Size: 16072 Color: 0
Size: 3464 Color: 1

Bin 170: 112 of cap free
Amount of items: 2
Items: 
Size: 16488 Color: 1
Size: 3048 Color: 0

Bin 171: 114 of cap free
Amount of items: 11
Items: 
Size: 9826 Color: 0
Size: 1392 Color: 1
Size: 1280 Color: 1
Size: 1068 Color: 1
Size: 1056 Color: 1
Size: 1024 Color: 1
Size: 784 Color: 0
Size: 784 Color: 0
Size: 784 Color: 0
Size: 768 Color: 0
Size: 768 Color: 0

Bin 172: 114 of cap free
Amount of items: 2
Items: 
Size: 13168 Color: 0
Size: 6366 Color: 1

Bin 173: 120 of cap free
Amount of items: 2
Items: 
Size: 11304 Color: 0
Size: 8224 Color: 1

Bin 174: 120 of cap free
Amount of items: 2
Items: 
Size: 14495 Color: 0
Size: 5033 Color: 1

Bin 175: 120 of cap free
Amount of items: 2
Items: 
Size: 15536 Color: 1
Size: 3992 Color: 0

Bin 176: 120 of cap free
Amount of items: 2
Items: 
Size: 17140 Color: 0
Size: 2388 Color: 1

Bin 177: 121 of cap free
Amount of items: 2
Items: 
Size: 15023 Color: 0
Size: 4504 Color: 1

Bin 178: 127 of cap free
Amount of items: 2
Items: 
Size: 16745 Color: 1
Size: 2776 Color: 0

Bin 179: 144 of cap free
Amount of items: 2
Items: 
Size: 11696 Color: 0
Size: 7808 Color: 1

Bin 180: 148 of cap free
Amount of items: 2
Items: 
Size: 14684 Color: 1
Size: 4816 Color: 0

Bin 181: 159 of cap free
Amount of items: 2
Items: 
Size: 12408 Color: 0
Size: 7081 Color: 1

Bin 182: 159 of cap free
Amount of items: 2
Items: 
Size: 12805 Color: 0
Size: 6684 Color: 1

Bin 183: 164 of cap free
Amount of items: 2
Items: 
Size: 15560 Color: 0
Size: 3924 Color: 1

Bin 184: 165 of cap free
Amount of items: 2
Items: 
Size: 12996 Color: 1
Size: 6487 Color: 0

Bin 185: 193 of cap free
Amount of items: 2
Items: 
Size: 11996 Color: 1
Size: 7459 Color: 0

Bin 186: 193 of cap free
Amount of items: 2
Items: 
Size: 15527 Color: 1
Size: 3928 Color: 0

Bin 187: 212 of cap free
Amount of items: 34
Items: 
Size: 736 Color: 1
Size: 708 Color: 1
Size: 688 Color: 0
Size: 680 Color: 1
Size: 680 Color: 0
Size: 672 Color: 1
Size: 640 Color: 1
Size: 640 Color: 0
Size: 628 Color: 1
Size: 622 Color: 1
Size: 622 Color: 1
Size: 608 Color: 1
Size: 592 Color: 1
Size: 592 Color: 1
Size: 580 Color: 1
Size: 576 Color: 1
Size: 576 Color: 0
Size: 576 Color: 0
Size: 576 Color: 0
Size: 560 Color: 0
Size: 518 Color: 0
Size: 512 Color: 1
Size: 512 Color: 0
Size: 512 Color: 0
Size: 512 Color: 0
Size: 496 Color: 0
Size: 496 Color: 0
Size: 482 Color: 0
Size: 480 Color: 0
Size: 480 Color: 0
Size: 476 Color: 0
Size: 472 Color: 1
Size: 472 Color: 1
Size: 464 Color: 1

Bin 188: 223 of cap free
Amount of items: 2
Items: 
Size: 11240 Color: 1
Size: 8185 Color: 0

Bin 189: 228 of cap free
Amount of items: 2
Items: 
Size: 11234 Color: 0
Size: 8186 Color: 1

Bin 190: 234 of cap free
Amount of items: 9
Items: 
Size: 9832 Color: 1
Size: 1418 Color: 1
Size: 1418 Color: 1
Size: 1414 Color: 1
Size: 1404 Color: 1
Size: 1040 Color: 0
Size: 1024 Color: 0
Size: 968 Color: 0
Size: 896 Color: 0

Bin 191: 241 of cap free
Amount of items: 2
Items: 
Size: 14936 Color: 1
Size: 4471 Color: 0

Bin 192: 242 of cap free
Amount of items: 3
Items: 
Size: 9904 Color: 1
Size: 6380 Color: 0
Size: 3122 Color: 1

Bin 193: 244 of cap free
Amount of items: 2
Items: 
Size: 15264 Color: 1
Size: 4140 Color: 0

Bin 194: 246 of cap free
Amount of items: 2
Items: 
Size: 12010 Color: 0
Size: 7392 Color: 1

Bin 195: 254 of cap free
Amount of items: 2
Items: 
Size: 11218 Color: 0
Size: 8176 Color: 1

Bin 196: 274 of cap free
Amount of items: 3
Items: 
Size: 11102 Color: 0
Size: 6640 Color: 1
Size: 1632 Color: 0

Bin 197: 303 of cap free
Amount of items: 2
Items: 
Size: 11161 Color: 1
Size: 8184 Color: 0

Bin 198: 311 of cap free
Amount of items: 2
Items: 
Size: 11169 Color: 0
Size: 8168 Color: 1

Bin 199: 10304 of cap free
Amount of items: 24
Items: 
Size: 456 Color: 1
Size: 448 Color: 1
Size: 448 Color: 0
Size: 432 Color: 0
Size: 416 Color: 1
Size: 416 Color: 0
Size: 416 Color: 0
Size: 416 Color: 0
Size: 408 Color: 1
Size: 384 Color: 1
Size: 384 Color: 1
Size: 384 Color: 1
Size: 384 Color: 1
Size: 384 Color: 1
Size: 384 Color: 0
Size: 376 Color: 1
Size: 376 Color: 0
Size: 368 Color: 0
Size: 364 Color: 1
Size: 360 Color: 0
Size: 352 Color: 0
Size: 352 Color: 0
Size: 320 Color: 1
Size: 316 Color: 0

Total size: 3890304
Total free space: 19648

