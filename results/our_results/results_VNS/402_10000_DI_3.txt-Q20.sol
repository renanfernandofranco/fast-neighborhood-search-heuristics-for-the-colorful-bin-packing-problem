Capicity Bin: 7360
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 4608 Color: 14
Size: 2752 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4821 Color: 12
Size: 2117 Color: 2
Size: 422 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5032 Color: 11
Size: 2088 Color: 17
Size: 240 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 7
Size: 1704 Color: 19
Size: 380 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5292 Color: 16
Size: 1460 Color: 14
Size: 608 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5305 Color: 16
Size: 1897 Color: 19
Size: 158 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5308 Color: 10
Size: 1716 Color: 14
Size: 336 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5320 Color: 14
Size: 1536 Color: 17
Size: 504 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5448 Color: 17
Size: 1608 Color: 7
Size: 304 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5584 Color: 18
Size: 1488 Color: 19
Size: 288 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 11
Size: 1374 Color: 19
Size: 272 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5748 Color: 11
Size: 1252 Color: 16
Size: 360 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5848 Color: 12
Size: 948 Color: 0
Size: 564 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5944 Color: 6
Size: 1080 Color: 12
Size: 336 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5950 Color: 6
Size: 1178 Color: 14
Size: 232 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6007 Color: 4
Size: 699 Color: 13
Size: 654 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6037 Color: 19
Size: 785 Color: 13
Size: 538 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6085 Color: 7
Size: 1117 Color: 5
Size: 158 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 17
Size: 1036 Color: 7
Size: 176 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6218 Color: 5
Size: 758 Color: 7
Size: 384 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6228 Color: 5
Size: 780 Color: 1
Size: 352 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6275 Color: 16
Size: 799 Color: 6
Size: 286 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6296 Color: 10
Size: 942 Color: 9
Size: 122 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 7
Size: 964 Color: 5
Size: 56 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6382 Color: 8
Size: 504 Color: 16
Size: 474 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6403 Color: 14
Size: 771 Color: 5
Size: 186 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 4
Size: 705 Color: 14
Size: 220 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 13
Size: 642 Color: 15
Size: 264 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6456 Color: 8
Size: 612 Color: 5
Size: 292 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6486 Color: 3
Size: 730 Color: 2
Size: 144 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6488 Color: 15
Size: 528 Color: 7
Size: 344 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6540 Color: 7
Size: 524 Color: 0
Size: 296 Color: 10

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 13
Size: 695 Color: 0
Size: 110 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6578 Color: 14
Size: 478 Color: 3
Size: 304 Color: 2

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 4820 Color: 6
Size: 2393 Color: 3
Size: 146 Color: 12

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5418 Color: 18
Size: 1701 Color: 12
Size: 240 Color: 5

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5607 Color: 4
Size: 1516 Color: 12
Size: 236 Color: 12

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5768 Color: 14
Size: 1423 Color: 11
Size: 168 Color: 17

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6157 Color: 9
Size: 1026 Color: 14
Size: 176 Color: 8

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6531 Color: 5
Size: 492 Color: 9
Size: 336 Color: 14

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 4546 Color: 5
Size: 2548 Color: 6
Size: 264 Color: 16

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 5071 Color: 8
Size: 2103 Color: 7
Size: 184 Color: 10

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 5321 Color: 6
Size: 1909 Color: 7
Size: 128 Color: 8

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 5897 Color: 0
Size: 1461 Color: 3

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 4
Size: 665 Color: 6
Size: 520 Color: 3

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 6197 Color: 17
Size: 1129 Color: 2
Size: 32 Color: 5

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 6515 Color: 13
Size: 843 Color: 3

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 6616 Color: 7
Size: 742 Color: 6

Bin 49: 3 of cap free
Amount of items: 8
Items: 
Size: 3683 Color: 7
Size: 731 Color: 8
Size: 719 Color: 0
Size: 612 Color: 2
Size: 612 Color: 1
Size: 608 Color: 16
Size: 248 Color: 6
Size: 144 Color: 3

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 4386 Color: 18
Size: 2699 Color: 6
Size: 272 Color: 18

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 5653 Color: 6
Size: 1544 Color: 2
Size: 160 Color: 10

Bin 52: 3 of cap free
Amount of items: 2
Items: 
Size: 6021 Color: 16
Size: 1336 Color: 19

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 6
Size: 1031 Color: 10
Size: 180 Color: 14

Bin 54: 3 of cap free
Amount of items: 2
Items: 
Size: 6366 Color: 9
Size: 991 Color: 13

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 6401 Color: 0
Size: 684 Color: 2
Size: 272 Color: 12

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 4794 Color: 11
Size: 2346 Color: 11
Size: 216 Color: 10

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 5660 Color: 14
Size: 1696 Color: 17

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 5860 Color: 11
Size: 1496 Color: 8

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 12
Size: 1124 Color: 5
Size: 48 Color: 2

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 15
Size: 940 Color: 5
Size: 182 Color: 14

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 6372 Color: 18
Size: 984 Color: 1

Bin 62: 5 of cap free
Amount of items: 2
Items: 
Size: 4521 Color: 13
Size: 2834 Color: 12

Bin 63: 5 of cap free
Amount of items: 2
Items: 
Size: 6499 Color: 19
Size: 856 Color: 2

Bin 64: 5 of cap free
Amount of items: 2
Items: 
Size: 6563 Color: 16
Size: 792 Color: 0

Bin 65: 6 of cap free
Amount of items: 2
Items: 
Size: 4872 Color: 13
Size: 2482 Color: 8

Bin 66: 6 of cap free
Amount of items: 3
Items: 
Size: 5142 Color: 2
Size: 2100 Color: 3
Size: 112 Color: 19

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 6419 Color: 4
Size: 935 Color: 6

Bin 68: 7 of cap free
Amount of items: 2
Items: 
Size: 5501 Color: 9
Size: 1852 Color: 11

Bin 69: 7 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 15
Size: 1221 Color: 13

Bin 70: 7 of cap free
Amount of items: 2
Items: 
Size: 6523 Color: 11
Size: 830 Color: 19

Bin 71: 8 of cap free
Amount of items: 4
Items: 
Size: 3692 Color: 19
Size: 3060 Color: 3
Size: 376 Color: 6
Size: 224 Color: 8

Bin 72: 8 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 1
Size: 1348 Color: 3
Size: 128 Color: 14

Bin 73: 8 of cap free
Amount of items: 3
Items: 
Size: 6349 Color: 17
Size: 971 Color: 12
Size: 32 Color: 9

Bin 74: 8 of cap free
Amount of items: 3
Items: 
Size: 6470 Color: 19
Size: 866 Color: 17
Size: 16 Color: 14

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 6483 Color: 9
Size: 869 Color: 6

Bin 76: 9 of cap free
Amount of items: 3
Items: 
Size: 4123 Color: 10
Size: 3064 Color: 4
Size: 164 Color: 7

Bin 77: 9 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 3
Size: 2367 Color: 9
Size: 432 Color: 2

Bin 78: 9 of cap free
Amount of items: 2
Items: 
Size: 6020 Color: 2
Size: 1331 Color: 8

Bin 79: 10 of cap free
Amount of items: 2
Items: 
Size: 5637 Color: 14
Size: 1713 Color: 5

Bin 80: 10 of cap free
Amount of items: 2
Items: 
Size: 5913 Color: 2
Size: 1437 Color: 4

Bin 81: 10 of cap free
Amount of items: 2
Items: 
Size: 6590 Color: 0
Size: 760 Color: 9

Bin 82: 11 of cap free
Amount of items: 3
Items: 
Size: 4489 Color: 8
Size: 2568 Color: 10
Size: 292 Color: 13

Bin 83: 11 of cap free
Amount of items: 2
Items: 
Size: 5101 Color: 15
Size: 2248 Color: 10

Bin 84: 11 of cap free
Amount of items: 3
Items: 
Size: 5470 Color: 17
Size: 1687 Color: 19
Size: 192 Color: 8

Bin 85: 11 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 10
Size: 1192 Color: 17
Size: 32 Color: 4

Bin 86: 12 of cap free
Amount of items: 3
Items: 
Size: 4139 Color: 18
Size: 3065 Color: 19
Size: 144 Color: 8

Bin 87: 12 of cap free
Amount of items: 2
Items: 
Size: 4280 Color: 11
Size: 3068 Color: 14

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 4844 Color: 18
Size: 2504 Color: 7

Bin 89: 12 of cap free
Amount of items: 2
Items: 
Size: 5512 Color: 17
Size: 1836 Color: 13

Bin 90: 13 of cap free
Amount of items: 2
Items: 
Size: 6428 Color: 2
Size: 919 Color: 11

Bin 91: 13 of cap free
Amount of items: 2
Items: 
Size: 6459 Color: 3
Size: 888 Color: 19

Bin 92: 14 of cap free
Amount of items: 7
Items: 
Size: 3684 Color: 3
Size: 905 Color: 7
Size: 816 Color: 0
Size: 801 Color: 8
Size: 444 Color: 12
Size: 376 Color: 9
Size: 320 Color: 13

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 6283 Color: 5
Size: 1063 Color: 10

Bin 94: 15 of cap free
Amount of items: 2
Items: 
Size: 5767 Color: 5
Size: 1578 Color: 18

Bin 95: 16 of cap free
Amount of items: 30
Items: 
Size: 416 Color: 8
Size: 400 Color: 8
Size: 368 Color: 18
Size: 368 Color: 15
Size: 338 Color: 1
Size: 328 Color: 18
Size: 312 Color: 4
Size: 284 Color: 4
Size: 276 Color: 18
Size: 256 Color: 10
Size: 242 Color: 9
Size: 240 Color: 12
Size: 240 Color: 0
Size: 224 Color: 8
Size: 222 Color: 4
Size: 212 Color: 7
Size: 208 Color: 15
Size: 208 Color: 10
Size: 204 Color: 14
Size: 204 Color: 14
Size: 200 Color: 9
Size: 196 Color: 5
Size: 192 Color: 16
Size: 192 Color: 14
Size: 192 Color: 13
Size: 192 Color: 6
Size: 188 Color: 11
Size: 156 Color: 19
Size: 148 Color: 6
Size: 138 Color: 13

Bin 96: 16 of cap free
Amount of items: 3
Items: 
Size: 4121 Color: 14
Size: 2532 Color: 15
Size: 691 Color: 9

Bin 97: 16 of cap free
Amount of items: 3
Items: 
Size: 4324 Color: 6
Size: 2712 Color: 3
Size: 308 Color: 10

Bin 98: 16 of cap free
Amount of items: 2
Items: 
Size: 6072 Color: 16
Size: 1272 Color: 19

Bin 99: 17 of cap free
Amount of items: 3
Items: 
Size: 5082 Color: 14
Size: 1883 Color: 3
Size: 378 Color: 9

Bin 100: 18 of cap free
Amount of items: 2
Items: 
Size: 6239 Color: 12
Size: 1103 Color: 19

Bin 101: 19 of cap free
Amount of items: 4
Items: 
Size: 3682 Color: 13
Size: 1850 Color: 17
Size: 1329 Color: 5
Size: 480 Color: 5

Bin 102: 19 of cap free
Amount of items: 3
Items: 
Size: 4120 Color: 19
Size: 2685 Color: 5
Size: 536 Color: 3

Bin 103: 19 of cap free
Amount of items: 2
Items: 
Size: 6424 Color: 6
Size: 917 Color: 18

Bin 104: 20 of cap free
Amount of items: 3
Items: 
Size: 3928 Color: 12
Size: 2444 Color: 2
Size: 968 Color: 5

Bin 105: 20 of cap free
Amount of items: 2
Items: 
Size: 3962 Color: 0
Size: 3378 Color: 10

Bin 106: 20 of cap free
Amount of items: 2
Items: 
Size: 6259 Color: 15
Size: 1081 Color: 11

Bin 107: 23 of cap free
Amount of items: 2
Items: 
Size: 6130 Color: 17
Size: 1207 Color: 10

Bin 108: 24 of cap free
Amount of items: 2
Items: 
Size: 6200 Color: 3
Size: 1136 Color: 19

Bin 109: 25 of cap free
Amount of items: 10
Items: 
Size: 3681 Color: 15
Size: 560 Color: 3
Size: 512 Color: 2
Size: 468 Color: 2
Size: 420 Color: 7
Size: 416 Color: 18
Size: 344 Color: 9
Size: 342 Color: 14
Size: 304 Color: 10
Size: 288 Color: 13

Bin 110: 26 of cap free
Amount of items: 5
Items: 
Size: 3688 Color: 11
Size: 1244 Color: 17
Size: 954 Color: 16
Size: 816 Color: 18
Size: 632 Color: 2

Bin 111: 26 of cap free
Amount of items: 3
Items: 
Size: 5880 Color: 13
Size: 1190 Color: 7
Size: 264 Color: 11

Bin 112: 27 of cap free
Amount of items: 2
Items: 
Size: 4505 Color: 3
Size: 2828 Color: 16

Bin 113: 27 of cap free
Amount of items: 2
Items: 
Size: 4837 Color: 5
Size: 2496 Color: 17

Bin 114: 31 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 13
Size: 2381 Color: 0
Size: 608 Color: 8

Bin 115: 34 of cap free
Amount of items: 2
Items: 
Size: 4682 Color: 18
Size: 2644 Color: 15

Bin 116: 34 of cap free
Amount of items: 2
Items: 
Size: 5088 Color: 18
Size: 2238 Color: 16

Bin 117: 38 of cap free
Amount of items: 2
Items: 
Size: 4680 Color: 11
Size: 2642 Color: 18

Bin 118: 40 of cap free
Amount of items: 2
Items: 
Size: 5698 Color: 4
Size: 1622 Color: 17

Bin 119: 40 of cap free
Amount of items: 2
Items: 
Size: 5934 Color: 5
Size: 1386 Color: 12

Bin 120: 41 of cap free
Amount of items: 2
Items: 
Size: 5085 Color: 6
Size: 2234 Color: 14

Bin 121: 44 of cap free
Amount of items: 2
Items: 
Size: 5576 Color: 2
Size: 1740 Color: 12

Bin 122: 44 of cap free
Amount of items: 2
Items: 
Size: 5765 Color: 7
Size: 1551 Color: 13

Bin 123: 46 of cap free
Amount of items: 4
Items: 
Size: 3690 Color: 10
Size: 1832 Color: 17
Size: 1320 Color: 6
Size: 472 Color: 11

Bin 124: 52 of cap free
Amount of items: 2
Items: 
Size: 4436 Color: 2
Size: 2872 Color: 12

Bin 125: 54 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 17
Size: 1902 Color: 11
Size: 264 Color: 8

Bin 126: 60 of cap free
Amount of items: 2
Items: 
Size: 5176 Color: 11
Size: 2124 Color: 5

Bin 127: 66 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 12
Size: 1102 Color: 2
Size: 1028 Color: 19

Bin 128: 77 of cap free
Amount of items: 2
Items: 
Size: 4308 Color: 2
Size: 2975 Color: 9

Bin 129: 79 of cap free
Amount of items: 2
Items: 
Size: 5337 Color: 19
Size: 1944 Color: 15

Bin 130: 88 of cap free
Amount of items: 2
Items: 
Size: 5548 Color: 6
Size: 1724 Color: 12

Bin 131: 97 of cap free
Amount of items: 2
Items: 
Size: 4196 Color: 15
Size: 3067 Color: 14

Bin 132: 100 of cap free
Amount of items: 2
Items: 
Size: 4194 Color: 19
Size: 3066 Color: 11

Bin 133: 5522 of cap free
Amount of items: 13
Items: 
Size: 184 Color: 16
Size: 160 Color: 9
Size: 154 Color: 2
Size: 152 Color: 19
Size: 148 Color: 5
Size: 142 Color: 0
Size: 140 Color: 1
Size: 138 Color: 3
Size: 136 Color: 10
Size: 132 Color: 6
Size: 128 Color: 13
Size: 112 Color: 15
Size: 112 Color: 12

Total size: 971520
Total free space: 7360

