Capicity Bin: 1000001
Lower Bound: 44

Bins used: 45
Amount of Colors: 5

Bin 1: 2 of cap free
Amount of items: 2
Items: 
Size: 657970 Color: 1
Size: 342029 Color: 0

Bin 2: 46 of cap free
Amount of items: 3
Items: 
Size: 512541 Color: 4
Size: 340383 Color: 4
Size: 147031 Color: 0

Bin 3: 144 of cap free
Amount of items: 4
Items: 
Size: 547495 Color: 0
Size: 183710 Color: 4
Size: 167239 Color: 2
Size: 101413 Color: 4

Bin 4: 165 of cap free
Amount of items: 4
Items: 
Size: 519131 Color: 2
Size: 195029 Color: 4
Size: 152619 Color: 2
Size: 133057 Color: 0

Bin 5: 207 of cap free
Amount of items: 3
Items: 
Size: 664875 Color: 0
Size: 186930 Color: 3
Size: 147989 Color: 1

Bin 6: 214 of cap free
Amount of items: 2
Items: 
Size: 608745 Color: 0
Size: 391042 Color: 4

Bin 7: 248 of cap free
Amount of items: 3
Items: 
Size: 705431 Color: 1
Size: 191634 Color: 0
Size: 102688 Color: 2

Bin 8: 295 of cap free
Amount of items: 3
Items: 
Size: 698250 Color: 3
Size: 151905 Color: 4
Size: 149551 Color: 3

Bin 9: 338 of cap free
Amount of items: 2
Items: 
Size: 575367 Color: 4
Size: 424296 Color: 3

Bin 10: 364 of cap free
Amount of items: 3
Items: 
Size: 719092 Color: 0
Size: 154149 Color: 2
Size: 126396 Color: 4

Bin 11: 437 of cap free
Amount of items: 2
Items: 
Size: 539215 Color: 1
Size: 460349 Color: 4

Bin 12: 671 of cap free
Amount of items: 3
Items: 
Size: 654993 Color: 2
Size: 215295 Color: 2
Size: 129042 Color: 4

Bin 13: 759 of cap free
Amount of items: 2
Items: 
Size: 712025 Color: 2
Size: 287217 Color: 1

Bin 14: 1025 of cap free
Amount of items: 3
Items: 
Size: 778569 Color: 4
Size: 118845 Color: 4
Size: 101562 Color: 0

Bin 15: 1278 of cap free
Amount of items: 2
Items: 
Size: 672290 Color: 1
Size: 326433 Color: 2

Bin 16: 1376 of cap free
Amount of items: 2
Items: 
Size: 603262 Color: 0
Size: 395363 Color: 1

Bin 17: 1708 of cap free
Amount of items: 2
Items: 
Size: 695780 Color: 1
Size: 302513 Color: 4

Bin 18: 1946 of cap free
Amount of items: 2
Items: 
Size: 507224 Color: 1
Size: 490831 Color: 3

Bin 19: 2233 of cap free
Amount of items: 3
Items: 
Size: 482893 Color: 0
Size: 381652 Color: 3
Size: 133223 Color: 3

Bin 20: 2268 of cap free
Amount of items: 3
Items: 
Size: 701862 Color: 2
Size: 183370 Color: 0
Size: 112501 Color: 0

Bin 21: 2362 of cap free
Amount of items: 2
Items: 
Size: 538780 Color: 1
Size: 458859 Color: 0

Bin 22: 2705 of cap free
Amount of items: 2
Items: 
Size: 635483 Color: 2
Size: 361813 Color: 3

Bin 23: 3032 of cap free
Amount of items: 2
Items: 
Size: 558757 Color: 3
Size: 438212 Color: 1

Bin 24: 4023 of cap free
Amount of items: 2
Items: 
Size: 627082 Color: 0
Size: 368896 Color: 1

Bin 25: 4271 of cap free
Amount of items: 2
Items: 
Size: 545585 Color: 1
Size: 450145 Color: 2

Bin 26: 4335 of cap free
Amount of items: 2
Items: 
Size: 776927 Color: 2
Size: 218739 Color: 3

Bin 27: 5267 of cap free
Amount of items: 2
Items: 
Size: 531195 Color: 1
Size: 463539 Color: 2

Bin 28: 5343 of cap free
Amount of items: 2
Items: 
Size: 564731 Color: 1
Size: 429927 Color: 3

Bin 29: 7661 of cap free
Amount of items: 3
Items: 
Size: 736370 Color: 1
Size: 146257 Color: 1
Size: 109713 Color: 4

Bin 30: 8182 of cap free
Amount of items: 2
Items: 
Size: 706899 Color: 1
Size: 284920 Color: 0

Bin 31: 9254 of cap free
Amount of items: 2
Items: 
Size: 558464 Color: 4
Size: 432283 Color: 1

Bin 32: 10571 of cap free
Amount of items: 2
Items: 
Size: 601715 Color: 4
Size: 387715 Color: 0

Bin 33: 11822 of cap free
Amount of items: 2
Items: 
Size: 526656 Color: 2
Size: 461523 Color: 0

Bin 34: 13022 of cap free
Amount of items: 2
Items: 
Size: 500653 Color: 1
Size: 486326 Color: 4

Bin 35: 16146 of cap free
Amount of items: 2
Items: 
Size: 754193 Color: 3
Size: 229662 Color: 0

Bin 36: 18947 of cap free
Amount of items: 2
Items: 
Size: 644538 Color: 1
Size: 336516 Color: 0

Bin 37: 35791 of cap free
Amount of items: 2
Items: 
Size: 597984 Color: 3
Size: 366226 Color: 4

Bin 38: 40724 of cap free
Amount of items: 2
Items: 
Size: 678802 Color: 2
Size: 280475 Color: 0

Bin 39: 59700 of cap free
Amount of items: 2
Items: 
Size: 687811 Color: 0
Size: 252490 Color: 2

Bin 40: 62218 of cap free
Amount of items: 2
Items: 
Size: 686068 Color: 3
Size: 251715 Color: 2

Bin 41: 66763 of cap free
Amount of items: 2
Items: 
Size: 518293 Color: 3
Size: 414945 Color: 1

Bin 42: 69252 of cap free
Amount of items: 2
Items: 
Size: 518019 Color: 0
Size: 412730 Color: 1

Bin 43: 179048 of cap free
Amount of items: 2
Items: 
Size: 596500 Color: 2
Size: 224453 Color: 0

Bin 44: 208982 of cap free
Amount of items: 1
Items: 
Size: 791019 Color: 0

Bin 45: 216011 of cap free
Amount of items: 1
Items: 
Size: 783990 Color: 4

Total size: 43918889
Total free space: 1081156

