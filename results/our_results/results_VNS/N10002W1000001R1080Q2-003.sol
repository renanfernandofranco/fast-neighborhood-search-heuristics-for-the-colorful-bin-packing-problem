Capicity Bin: 1000001
Lower Bound: 4527

Bins used: 4531
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 213784 Color: 0
Size: 199180 Color: 1
Size: 198526 Color: 1
Size: 197870 Color: 0
Size: 190641 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 428524 Color: 1
Size: 404336 Color: 0
Size: 167141 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 445627 Color: 1
Size: 362178 Color: 0
Size: 192196 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 445627 Color: 1
Size: 369701 Color: 0
Size: 184673 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 447005 Color: 1
Size: 376293 Color: 0
Size: 176703 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 448901 Color: 1
Size: 370792 Color: 0
Size: 180308 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 456164 Color: 0
Size: 347462 Color: 1
Size: 196375 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 464975 Color: 1
Size: 408556 Color: 1
Size: 126470 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 477225 Color: 1
Size: 368136 Color: 0
Size: 154640 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 478392 Color: 0
Size: 346022 Color: 0
Size: 175587 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 478985 Color: 0
Size: 373083 Color: 0
Size: 147933 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 477891 Color: 1
Size: 381867 Color: 0
Size: 140243 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 479199 Color: 0
Size: 377013 Color: 0
Size: 143789 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 478505 Color: 1
Size: 346439 Color: 0
Size: 175057 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 479442 Color: 0
Size: 355177 Color: 1
Size: 165382 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 478952 Color: 1
Size: 359638 Color: 1
Size: 161411 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 479623 Color: 1
Size: 392846 Color: 0
Size: 127532 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 494596 Color: 0
Size: 357012 Color: 1
Size: 148393 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 496572 Color: 0
Size: 350137 Color: 1
Size: 153292 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 497082 Color: 0
Size: 319103 Color: 1
Size: 183816 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 505774 Color: 1
Size: 369658 Color: 1
Size: 124569 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 514607 Color: 0
Size: 352950 Color: 1
Size: 132444 Color: 0

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 518401 Color: 1
Size: 481600 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 523428 Color: 1
Size: 279267 Color: 1
Size: 197306 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 523930 Color: 1
Size: 290400 Color: 0
Size: 185671 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 524403 Color: 1
Size: 331729 Color: 0
Size: 143869 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 524492 Color: 1
Size: 322513 Color: 1
Size: 152996 Color: 0

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 531037 Color: 0
Size: 468964 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 538012 Color: 0
Size: 309614 Color: 1
Size: 152375 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 538060 Color: 0
Size: 270049 Color: 0
Size: 191892 Color: 1

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 539219 Color: 0
Size: 460782 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 541128 Color: 0
Size: 302933 Color: 1
Size: 155940 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 541397 Color: 0
Size: 314098 Color: 1
Size: 144506 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 544011 Color: 1
Size: 275871 Color: 0
Size: 180119 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 544065 Color: 1
Size: 312427 Color: 1
Size: 143509 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 545053 Color: 1
Size: 331488 Color: 0
Size: 123460 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 549541 Color: 1
Size: 255757 Color: 0
Size: 194703 Color: 0

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 551981 Color: 1
Size: 448020 Color: 0

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 556905 Color: 1
Size: 443096 Color: 0

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 558019 Color: 1
Size: 441982 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 558646 Color: 0
Size: 269464 Color: 0
Size: 171891 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 558597 Color: 1
Size: 256956 Color: 0
Size: 184448 Color: 1

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 559604 Color: 0
Size: 440397 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 564436 Color: 0
Size: 306276 Color: 1
Size: 129289 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 564464 Color: 0
Size: 255820 Color: 0
Size: 179717 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 564388 Color: 1
Size: 276640 Color: 1
Size: 158973 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 572462 Color: 0
Size: 289857 Color: 0
Size: 137682 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 574958 Color: 0
Size: 275953 Color: 0
Size: 149090 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 577998 Color: 1
Size: 254435 Color: 0
Size: 167568 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 582021 Color: 1
Size: 219552 Color: 0
Size: 198428 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 582226 Color: 0
Size: 257835 Color: 0
Size: 159940 Color: 1

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 583085 Color: 0
Size: 416916 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 586391 Color: 0
Size: 252445 Color: 0
Size: 161165 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 586414 Color: 1
Size: 273233 Color: 1
Size: 140354 Color: 0

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 590585 Color: 1
Size: 409416 Color: 0

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 591621 Color: 0
Size: 408380 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 593727 Color: 0
Size: 252822 Color: 0
Size: 153452 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 596834 Color: 1
Size: 233610 Color: 1
Size: 169557 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 597094 Color: 1
Size: 273925 Color: 1
Size: 128982 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 598114 Color: 0
Size: 255856 Color: 0
Size: 146031 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 598660 Color: 1
Size: 249779 Color: 1
Size: 151562 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 605943 Color: 0
Size: 197832 Color: 1
Size: 196226 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 606135 Color: 0
Size: 275263 Color: 1
Size: 118603 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 609485 Color: 1
Size: 224163 Color: 0
Size: 166353 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 609676 Color: 0
Size: 195746 Color: 1
Size: 194579 Color: 1

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 611796 Color: 1
Size: 388205 Color: 0

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 619794 Color: 1
Size: 380207 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 620160 Color: 0
Size: 255291 Color: 0
Size: 124550 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 620362 Color: 0
Size: 194766 Color: 1
Size: 184873 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 624010 Color: 1
Size: 192759 Color: 0
Size: 183232 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 624059 Color: 1
Size: 188228 Color: 0
Size: 187714 Color: 0

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 625570 Color: 1
Size: 374431 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 625518 Color: 0
Size: 195601 Color: 1
Size: 178882 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 625973 Color: 1
Size: 195166 Color: 0
Size: 178862 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 626140 Color: 0
Size: 187760 Color: 0
Size: 186101 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 626315 Color: 0
Size: 198854 Color: 0
Size: 174832 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 626303 Color: 1
Size: 194351 Color: 0
Size: 179347 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 626431 Color: 0
Size: 196766 Color: 0
Size: 176804 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 626506 Color: 1
Size: 196441 Color: 0
Size: 177054 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 631762 Color: 0
Size: 234914 Color: 1
Size: 133325 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 631842 Color: 0
Size: 186916 Color: 1
Size: 181243 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 631837 Color: 1
Size: 187118 Color: 0
Size: 181046 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 631864 Color: 0
Size: 229738 Color: 0
Size: 138399 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 631872 Color: 0
Size: 191439 Color: 0
Size: 176690 Color: 1

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 636174 Color: 0
Size: 363827 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 638809 Color: 0
Size: 183611 Color: 0
Size: 177581 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 638998 Color: 1
Size: 182279 Color: 1
Size: 178724 Color: 0

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 640139 Color: 1
Size: 359862 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 640498 Color: 0
Size: 195347 Color: 0
Size: 164156 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 640700 Color: 1
Size: 194639 Color: 0
Size: 164662 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 640705 Color: 1
Size: 191349 Color: 0
Size: 167947 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 641065 Color: 0
Size: 195498 Color: 1
Size: 163438 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 641176 Color: 1
Size: 193400 Color: 1
Size: 165425 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 641294 Color: 1
Size: 199215 Color: 0
Size: 159492 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 641313 Color: 1
Size: 183395 Color: 1
Size: 175293 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 641334 Color: 0
Size: 184521 Color: 1
Size: 174146 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 641363 Color: 1
Size: 183462 Color: 1
Size: 175176 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 643890 Color: 0
Size: 178338 Color: 1
Size: 177773 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 644608 Color: 0
Size: 207267 Color: 1
Size: 148126 Color: 0

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 647249 Color: 1
Size: 352752 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 648423 Color: 0
Size: 182962 Color: 1
Size: 168616 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 648468 Color: 0
Size: 223738 Color: 0
Size: 127795 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 648576 Color: 1
Size: 195186 Color: 1
Size: 156239 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 648473 Color: 0
Size: 182159 Color: 1
Size: 169369 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 648821 Color: 1
Size: 192162 Color: 0
Size: 159018 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 648857 Color: 1
Size: 188177 Color: 1
Size: 162967 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 648766 Color: 0
Size: 197694 Color: 0
Size: 153541 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 648869 Color: 1
Size: 199190 Color: 1
Size: 151942 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 648963 Color: 1
Size: 191507 Color: 1
Size: 159531 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 649076 Color: 1
Size: 193573 Color: 0
Size: 157352 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 649388 Color: 0
Size: 181607 Color: 0
Size: 169006 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 649382 Color: 1
Size: 181782 Color: 0
Size: 168837 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 649585 Color: 1
Size: 223981 Color: 0
Size: 126435 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 651774 Color: 0
Size: 186205 Color: 0
Size: 162022 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 653072 Color: 1
Size: 192774 Color: 1
Size: 154155 Color: 0

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 653214 Color: 1
Size: 346787 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 654134 Color: 0
Size: 190919 Color: 1
Size: 154948 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 654182 Color: 1
Size: 180763 Color: 0
Size: 165056 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 654449 Color: 1
Size: 197627 Color: 0
Size: 147925 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 654772 Color: 1
Size: 183959 Color: 0
Size: 161270 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 654920 Color: 1
Size: 179088 Color: 0
Size: 165993 Color: 1

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 654962 Color: 1
Size: 345039 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 655416 Color: 1
Size: 188507 Color: 1
Size: 156078 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 656037 Color: 1
Size: 175838 Color: 1
Size: 168126 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 656791 Color: 1
Size: 182549 Color: 1
Size: 160661 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 657106 Color: 1
Size: 193369 Color: 0
Size: 149526 Color: 0

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 657739 Color: 0
Size: 342262 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 658945 Color: 1
Size: 186381 Color: 1
Size: 154675 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 659051 Color: 0
Size: 177372 Color: 1
Size: 163578 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 659055 Color: 1
Size: 192373 Color: 1
Size: 148573 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 659173 Color: 0
Size: 170545 Color: 1
Size: 170283 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 659153 Color: 1
Size: 193776 Color: 1
Size: 147072 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 659245 Color: 0
Size: 191166 Color: 0
Size: 149590 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 659188 Color: 1
Size: 196390 Color: 0
Size: 144423 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 659276 Color: 0
Size: 188681 Color: 0
Size: 152044 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 659333 Color: 1
Size: 193085 Color: 0
Size: 147583 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 659286 Color: 0
Size: 185249 Color: 0
Size: 155466 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 660907 Color: 0
Size: 207217 Color: 1
Size: 131877 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 660974 Color: 0
Size: 174731 Color: 1
Size: 164296 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 660924 Color: 1
Size: 182368 Color: 0
Size: 156709 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 661206 Color: 0
Size: 186511 Color: 1
Size: 152284 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 661358 Color: 1
Size: 191567 Color: 1
Size: 147076 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 661394 Color: 1
Size: 170605 Color: 0
Size: 168002 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 661665 Color: 0
Size: 191167 Color: 1
Size: 147169 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 662386 Color: 0
Size: 169583 Color: 1
Size: 168032 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 662422 Color: 0
Size: 177090 Color: 1
Size: 160489 Color: 0

Bin 147: 0 of cap free
Amount of items: 2
Items: 
Size: 664510 Color: 0
Size: 335491 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 666024 Color: 0
Size: 170588 Color: 0
Size: 163389 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 666064 Color: 0
Size: 170908 Color: 1
Size: 163029 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 666166 Color: 0
Size: 175064 Color: 0
Size: 158771 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 666229 Color: 0
Size: 175756 Color: 1
Size: 158016 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 666302 Color: 0
Size: 188366 Color: 0
Size: 145333 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 666324 Color: 0
Size: 181174 Color: 1
Size: 152503 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 666349 Color: 0
Size: 183941 Color: 1
Size: 149711 Color: 0

Bin 155: 0 of cap free
Amount of items: 2
Items: 
Size: 668341 Color: 0
Size: 331660 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 668448 Color: 0
Size: 169281 Color: 0
Size: 162272 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 668550 Color: 1
Size: 195726 Color: 1
Size: 135725 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 670982 Color: 0
Size: 168538 Color: 1
Size: 160481 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 671045 Color: 1
Size: 172448 Color: 0
Size: 156508 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 671096 Color: 0
Size: 166305 Color: 0
Size: 162600 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 671053 Color: 1
Size: 193299 Color: 1
Size: 135649 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 671385 Color: 0
Size: 189664 Color: 0
Size: 138952 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 671604 Color: 1
Size: 178200 Color: 1
Size: 150197 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 672410 Color: 0
Size: 170447 Color: 1
Size: 157144 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 672424 Color: 1
Size: 189296 Color: 1
Size: 138281 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 672438 Color: 0
Size: 194112 Color: 1
Size: 133451 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 673345 Color: 1
Size: 187811 Color: 0
Size: 138845 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 673665 Color: 0
Size: 173442 Color: 1
Size: 152894 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 673588 Color: 1
Size: 166453 Color: 1
Size: 159960 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 678532 Color: 0
Size: 192114 Color: 0
Size: 129355 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 678709 Color: 1
Size: 194133 Color: 0
Size: 127159 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 678745 Color: 0
Size: 164466 Color: 1
Size: 156790 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 679425 Color: 1
Size: 161998 Color: 0
Size: 158578 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 679425 Color: 1
Size: 163200 Color: 0
Size: 157376 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 679622 Color: 1
Size: 163307 Color: 0
Size: 157072 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 679662 Color: 1
Size: 173516 Color: 1
Size: 146823 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 679708 Color: 1
Size: 187287 Color: 0
Size: 133006 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 680576 Color: 1
Size: 175203 Color: 1
Size: 144222 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 680718 Color: 1
Size: 184301 Color: 0
Size: 134982 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 683282 Color: 1
Size: 175848 Color: 0
Size: 140871 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 684251 Color: 1
Size: 160964 Color: 0
Size: 154786 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 684706 Color: 1
Size: 159954 Color: 0
Size: 155341 Color: 1

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 684810 Color: 1
Size: 169757 Color: 0
Size: 145434 Color: 0

Bin 184: 0 of cap free
Amount of items: 2
Items: 
Size: 689138 Color: 1
Size: 310863 Color: 0

Bin 185: 0 of cap free
Amount of items: 2
Items: 
Size: 694434 Color: 0
Size: 305567 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 699941 Color: 0
Size: 161736 Color: 1
Size: 138324 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 702934 Color: 0
Size: 164960 Color: 1
Size: 132107 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 703872 Color: 0
Size: 161790 Color: 1
Size: 134339 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 704008 Color: 0
Size: 158304 Color: 0
Size: 137689 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 704049 Color: 0
Size: 172622 Color: 1
Size: 123330 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 705150 Color: 0
Size: 168595 Color: 0
Size: 126256 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 705225 Color: 0
Size: 161448 Color: 1
Size: 133328 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 705243 Color: 1
Size: 166608 Color: 1
Size: 128150 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 705548 Color: 1
Size: 168747 Color: 0
Size: 125706 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 707105 Color: 0
Size: 149674 Color: 0
Size: 143222 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 707048 Color: 1
Size: 155132 Color: 0
Size: 137821 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 707064 Color: 1
Size: 147343 Color: 0
Size: 145594 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 707314 Color: 0
Size: 165355 Color: 1
Size: 127332 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 707497 Color: 0
Size: 170693 Color: 1
Size: 121811 Color: 0

Bin 200: 0 of cap free
Amount of items: 2
Items: 
Size: 708440 Color: 1
Size: 291561 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 708833 Color: 0
Size: 154575 Color: 1
Size: 136593 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 709128 Color: 0
Size: 167549 Color: 1
Size: 123324 Color: 1

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 709344 Color: 1
Size: 157774 Color: 0
Size: 132883 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 709406 Color: 0
Size: 157406 Color: 1
Size: 133189 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 709429 Color: 1
Size: 145876 Color: 0
Size: 144696 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 709421 Color: 0
Size: 162335 Color: 1
Size: 128245 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 709466 Color: 1
Size: 150934 Color: 1
Size: 139601 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 709535 Color: 0
Size: 162151 Color: 0
Size: 128315 Color: 1

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 710054 Color: 1
Size: 166216 Color: 0
Size: 123731 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 710309 Color: 1
Size: 146730 Color: 0
Size: 142962 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 710423 Color: 1
Size: 164124 Color: 0
Size: 125454 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 710548 Color: 0
Size: 152094 Color: 0
Size: 137359 Color: 1

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 710524 Color: 1
Size: 156578 Color: 0
Size: 132899 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 710661 Color: 0
Size: 168575 Color: 1
Size: 120765 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 710667 Color: 1
Size: 151347 Color: 1
Size: 137987 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 710735 Color: 0
Size: 147395 Color: 1
Size: 141871 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 711019 Color: 1
Size: 146607 Color: 0
Size: 142375 Color: 1

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 711093 Color: 1
Size: 150588 Color: 0
Size: 138320 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 711673 Color: 0
Size: 163604 Color: 1
Size: 124724 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 711686 Color: 0
Size: 145426 Color: 0
Size: 142889 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 711702 Color: 0
Size: 144484 Color: 1
Size: 143815 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 711831 Color: 1
Size: 154030 Color: 0
Size: 134140 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 712378 Color: 1
Size: 154691 Color: 0
Size: 132932 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 712625 Color: 1
Size: 154061 Color: 1
Size: 133315 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 712786 Color: 1
Size: 146892 Color: 1
Size: 140323 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 712726 Color: 0
Size: 161101 Color: 1
Size: 126174 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 714841 Color: 1
Size: 152799 Color: 0
Size: 132361 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 715289 Color: 0
Size: 159295 Color: 1
Size: 125417 Color: 0

Bin 229: 0 of cap free
Amount of items: 2
Items: 
Size: 715573 Color: 0
Size: 284428 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 724151 Color: 1
Size: 140248 Color: 0
Size: 135602 Color: 1

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 727610 Color: 0
Size: 272391 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 728427 Color: 0
Size: 149412 Color: 1
Size: 122162 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 731598 Color: 0
Size: 148507 Color: 1
Size: 119896 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 731681 Color: 1
Size: 136332 Color: 1
Size: 131988 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 731820 Color: 0
Size: 141259 Color: 1
Size: 126922 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 731845 Color: 1
Size: 141331 Color: 1
Size: 126825 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 732094 Color: 1
Size: 136305 Color: 0
Size: 131602 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 732169 Color: 1
Size: 144172 Color: 0
Size: 123660 Color: 1

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 732202 Color: 0
Size: 147392 Color: 0
Size: 120407 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 732235 Color: 1
Size: 153148 Color: 1
Size: 114618 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 732839 Color: 1
Size: 147198 Color: 0
Size: 119964 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 732945 Color: 1
Size: 140517 Color: 1
Size: 126539 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 733027 Color: 0
Size: 135202 Color: 1
Size: 131772 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 732953 Color: 1
Size: 135035 Color: 1
Size: 132013 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 733157 Color: 0
Size: 140007 Color: 0
Size: 126837 Color: 1

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 733073 Color: 1
Size: 136123 Color: 0
Size: 130805 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 733555 Color: 0
Size: 149737 Color: 0
Size: 116709 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 733464 Color: 1
Size: 135594 Color: 1
Size: 130943 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 733796 Color: 0
Size: 141920 Color: 0
Size: 124285 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 734020 Color: 0
Size: 140764 Color: 1
Size: 125217 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 734051 Color: 0
Size: 138084 Color: 1
Size: 127866 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 734013 Color: 1
Size: 136287 Color: 1
Size: 129701 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 734392 Color: 1
Size: 138843 Color: 0
Size: 126766 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 734465 Color: 1
Size: 140807 Color: 0
Size: 124729 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 734418 Color: 0
Size: 144387 Color: 1
Size: 121196 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 734572 Color: 1
Size: 146028 Color: 0
Size: 119401 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 735406 Color: 1
Size: 140406 Color: 0
Size: 124189 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 735548 Color: 1
Size: 134923 Color: 0
Size: 129530 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 735581 Color: 1
Size: 132644 Color: 0
Size: 131776 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 735594 Color: 1
Size: 141182 Color: 0
Size: 123225 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 736269 Color: 1
Size: 132359 Color: 0
Size: 131373 Color: 0

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 739651 Color: 0
Size: 260350 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 741269 Color: 1
Size: 136686 Color: 1
Size: 122046 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 741656 Color: 0
Size: 129804 Color: 1
Size: 128541 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 741706 Color: 0
Size: 134801 Color: 0
Size: 123494 Color: 1

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 741801 Color: 0
Size: 134128 Color: 1
Size: 124072 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 741878 Color: 1
Size: 144389 Color: 0
Size: 113734 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 741827 Color: 0
Size: 131240 Color: 0
Size: 126934 Color: 1

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 741974 Color: 1
Size: 142134 Color: 1
Size: 115893 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 742016 Color: 0
Size: 145275 Color: 1
Size: 112710 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 742117 Color: 0
Size: 142882 Color: 0
Size: 115002 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 743175 Color: 0
Size: 138481 Color: 1
Size: 118345 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 743459 Color: 1
Size: 130688 Color: 1
Size: 125854 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 743661 Color: 0
Size: 134330 Color: 0
Size: 122010 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 743962 Color: 0
Size: 135849 Color: 1
Size: 120190 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 744213 Color: 0
Size: 131499 Color: 1
Size: 124289 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 744279 Color: 0
Size: 130967 Color: 0
Size: 124755 Color: 1

Bin 278: 0 of cap free
Amount of items: 2
Items: 
Size: 744296 Color: 0
Size: 255705 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 748650 Color: 0
Size: 129086 Color: 0
Size: 122265 Color: 1

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 750296 Color: 0
Size: 128856 Color: 1
Size: 120849 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 754880 Color: 0
Size: 131174 Color: 1
Size: 113947 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 754934 Color: 1
Size: 124967 Color: 1
Size: 120100 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 757743 Color: 0
Size: 126019 Color: 1
Size: 116239 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 757755 Color: 0
Size: 124835 Color: 0
Size: 117411 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 757981 Color: 1
Size: 126253 Color: 0
Size: 115767 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 758255 Color: 1
Size: 124304 Color: 1
Size: 117442 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 763433 Color: 1
Size: 120327 Color: 1
Size: 116241 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 765153 Color: 1
Size: 119170 Color: 0
Size: 115678 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 765530 Color: 1
Size: 117460 Color: 1
Size: 117011 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 768995 Color: 0
Size: 115802 Color: 1
Size: 115204 Color: 0

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 774349 Color: 0
Size: 225652 Color: 1

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 776864 Color: 0
Size: 223137 Color: 1

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 777318 Color: 0
Size: 222683 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 783245 Color: 0
Size: 115263 Color: 0
Size: 101493 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 787467 Color: 1
Size: 107194 Color: 1
Size: 105340 Color: 0

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 791171 Color: 0
Size: 208830 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 792311 Color: 1
Size: 107195 Color: 0
Size: 100495 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 793415 Color: 0
Size: 104755 Color: 0
Size: 101831 Color: 1

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 793468 Color: 0
Size: 206533 Color: 1

Bin 300: 0 of cap free
Amount of items: 2
Items: 
Size: 795376 Color: 1
Size: 204625 Color: 0

Bin 301: 1 of cap free
Amount of items: 5
Items: 
Size: 219717 Color: 0
Size: 219607 Color: 0
Size: 207086 Color: 1
Size: 180033 Color: 1
Size: 173557 Color: 0

Bin 302: 1 of cap free
Amount of items: 3
Items: 
Size: 428705 Color: 1
Size: 425080 Color: 0
Size: 146215 Color: 1

Bin 303: 1 of cap free
Amount of items: 3
Items: 
Size: 445782 Color: 1
Size: 320627 Color: 0
Size: 233591 Color: 1

Bin 304: 1 of cap free
Amount of items: 3
Items: 
Size: 450516 Color: 0
Size: 357082 Color: 1
Size: 192402 Color: 0

Bin 305: 1 of cap free
Amount of items: 3
Items: 
Size: 454326 Color: 0
Size: 406592 Color: 1
Size: 139082 Color: 0

Bin 306: 1 of cap free
Amount of items: 3
Items: 
Size: 455815 Color: 0
Size: 368258 Color: 1
Size: 175927 Color: 0

Bin 307: 1 of cap free
Amount of items: 3
Items: 
Size: 456155 Color: 0
Size: 381382 Color: 1
Size: 162463 Color: 1

Bin 308: 1 of cap free
Amount of items: 3
Items: 
Size: 463766 Color: 1
Size: 392858 Color: 0
Size: 143376 Color: 1

Bin 309: 1 of cap free
Amount of items: 3
Items: 
Size: 463783 Color: 1
Size: 382754 Color: 0
Size: 153463 Color: 0

Bin 310: 1 of cap free
Amount of items: 3
Items: 
Size: 467347 Color: 1
Size: 345625 Color: 0
Size: 187028 Color: 1

Bin 311: 1 of cap free
Amount of items: 3
Items: 
Size: 478078 Color: 1
Size: 369050 Color: 0
Size: 152872 Color: 1

Bin 312: 1 of cap free
Amount of items: 3
Items: 
Size: 479635 Color: 1
Size: 368317 Color: 0
Size: 152048 Color: 0

Bin 313: 1 of cap free
Amount of items: 3
Items: 
Size: 483698 Color: 1
Size: 354659 Color: 1
Size: 161643 Color: 0

Bin 314: 1 of cap free
Amount of items: 3
Items: 
Size: 496931 Color: 0
Size: 331544 Color: 0
Size: 171525 Color: 1

Bin 315: 1 of cap free
Amount of items: 3
Items: 
Size: 505892 Color: 0
Size: 357354 Color: 1
Size: 136754 Color: 0

Bin 316: 1 of cap free
Amount of items: 2
Items: 
Size: 508646 Color: 1
Size: 491354 Color: 0

Bin 317: 1 of cap free
Amount of items: 2
Items: 
Size: 511641 Color: 1
Size: 488359 Color: 0

Bin 318: 1 of cap free
Amount of items: 2
Items: 
Size: 512139 Color: 0
Size: 487861 Color: 1

Bin 319: 1 of cap free
Amount of items: 3
Items: 
Size: 513355 Color: 1
Size: 338420 Color: 0
Size: 148225 Color: 1

Bin 320: 1 of cap free
Amount of items: 3
Items: 
Size: 514359 Color: 1
Size: 347354 Color: 1
Size: 138287 Color: 0

Bin 321: 1 of cap free
Amount of items: 3
Items: 
Size: 514064 Color: 0
Size: 309728 Color: 1
Size: 176208 Color: 0

Bin 322: 1 of cap free
Amount of items: 3
Items: 
Size: 514575 Color: 1
Size: 314124 Color: 1
Size: 171301 Color: 0

Bin 323: 1 of cap free
Amount of items: 3
Items: 
Size: 514638 Color: 1
Size: 353506 Color: 1
Size: 131856 Color: 0

Bin 324: 1 of cap free
Amount of items: 3
Items: 
Size: 515333 Color: 1
Size: 323428 Color: 1
Size: 161239 Color: 0

Bin 325: 1 of cap free
Amount of items: 2
Items: 
Size: 515860 Color: 1
Size: 484140 Color: 0

Bin 326: 1 of cap free
Amount of items: 3
Items: 
Size: 524618 Color: 1
Size: 323133 Color: 1
Size: 152249 Color: 0

Bin 327: 1 of cap free
Amount of items: 2
Items: 
Size: 532413 Color: 0
Size: 467587 Color: 1

Bin 328: 1 of cap free
Amount of items: 3
Items: 
Size: 534518 Color: 0
Size: 315123 Color: 1
Size: 150359 Color: 0

Bin 329: 1 of cap free
Amount of items: 2
Items: 
Size: 536501 Color: 0
Size: 463499 Color: 1

Bin 330: 1 of cap free
Amount of items: 2
Items: 
Size: 538198 Color: 0
Size: 461802 Color: 1

Bin 331: 1 of cap free
Amount of items: 3
Items: 
Size: 538637 Color: 0
Size: 274434 Color: 1
Size: 186929 Color: 1

Bin 332: 1 of cap free
Amount of items: 2
Items: 
Size: 538649 Color: 0
Size: 461351 Color: 1

Bin 333: 1 of cap free
Amount of items: 3
Items: 
Size: 538914 Color: 0
Size: 231352 Color: 1
Size: 229734 Color: 0

Bin 334: 1 of cap free
Amount of items: 3
Items: 
Size: 544170 Color: 0
Size: 314766 Color: 1
Size: 141064 Color: 0

Bin 335: 1 of cap free
Amount of items: 3
Items: 
Size: 544479 Color: 1
Size: 279749 Color: 1
Size: 175772 Color: 0

Bin 336: 1 of cap free
Amount of items: 2
Items: 
Size: 550396 Color: 1
Size: 449604 Color: 0

Bin 337: 1 of cap free
Amount of items: 2
Items: 
Size: 552753 Color: 1
Size: 447247 Color: 0

Bin 338: 1 of cap free
Amount of items: 2
Items: 
Size: 554165 Color: 1
Size: 445835 Color: 0

Bin 339: 1 of cap free
Amount of items: 3
Items: 
Size: 564321 Color: 1
Size: 261115 Color: 1
Size: 174564 Color: 0

Bin 340: 1 of cap free
Amount of items: 3
Items: 
Size: 564512 Color: 1
Size: 256983 Color: 0
Size: 178505 Color: 1

Bin 341: 1 of cap free
Amount of items: 3
Items: 
Size: 569252 Color: 0
Size: 312096 Color: 1
Size: 118652 Color: 0

Bin 342: 1 of cap free
Amount of items: 3
Items: 
Size: 572549 Color: 1
Size: 272762 Color: 1
Size: 154689 Color: 0

Bin 343: 1 of cap free
Amount of items: 3
Items: 
Size: 574784 Color: 0
Size: 251355 Color: 0
Size: 173861 Color: 1

Bin 344: 1 of cap free
Amount of items: 2
Items: 
Size: 577652 Color: 0
Size: 422348 Color: 1

Bin 345: 1 of cap free
Amount of items: 3
Items: 
Size: 582227 Color: 1
Size: 259908 Color: 0
Size: 157865 Color: 1

Bin 346: 1 of cap free
Amount of items: 3
Items: 
Size: 586409 Color: 1
Size: 252675 Color: 0
Size: 160916 Color: 1

Bin 347: 1 of cap free
Amount of items: 2
Items: 
Size: 587376 Color: 0
Size: 412624 Color: 1

Bin 348: 1 of cap free
Amount of items: 3
Items: 
Size: 594318 Color: 0
Size: 240370 Color: 1
Size: 165312 Color: 0

Bin 349: 1 of cap free
Amount of items: 3
Items: 
Size: 596877 Color: 1
Size: 219833 Color: 0
Size: 183290 Color: 0

Bin 350: 1 of cap free
Amount of items: 3
Items: 
Size: 598321 Color: 1
Size: 231279 Color: 1
Size: 170400 Color: 0

Bin 351: 1 of cap free
Amount of items: 3
Items: 
Size: 598722 Color: 1
Size: 213845 Color: 0
Size: 187433 Color: 1

Bin 352: 1 of cap free
Amount of items: 2
Items: 
Size: 600016 Color: 0
Size: 399984 Color: 1

Bin 353: 1 of cap free
Amount of items: 2
Items: 
Size: 600496 Color: 1
Size: 399504 Color: 0

Bin 354: 1 of cap free
Amount of items: 2
Items: 
Size: 603764 Color: 0
Size: 396236 Color: 1

Bin 355: 1 of cap free
Amount of items: 3
Items: 
Size: 605949 Color: 1
Size: 198708 Color: 0
Size: 195343 Color: 1

Bin 356: 1 of cap free
Amount of items: 3
Items: 
Size: 606151 Color: 0
Size: 197861 Color: 0
Size: 195988 Color: 1

Bin 357: 1 of cap free
Amount of items: 3
Items: 
Size: 607256 Color: 1
Size: 234573 Color: 1
Size: 158171 Color: 0

Bin 358: 1 of cap free
Amount of items: 3
Items: 
Size: 609409 Color: 0
Size: 241754 Color: 1
Size: 148837 Color: 0

Bin 359: 1 of cap free
Amount of items: 3
Items: 
Size: 609317 Color: 1
Size: 264188 Color: 1
Size: 126495 Color: 0

Bin 360: 1 of cap free
Amount of items: 3
Items: 
Size: 609654 Color: 0
Size: 263539 Color: 1
Size: 126807 Color: 0

Bin 361: 1 of cap free
Amount of items: 2
Items: 
Size: 610767 Color: 0
Size: 389233 Color: 1

Bin 362: 1 of cap free
Amount of items: 2
Items: 
Size: 618328 Color: 1
Size: 381672 Color: 0

Bin 363: 1 of cap free
Amount of items: 3
Items: 
Size: 619680 Color: 0
Size: 193630 Color: 1
Size: 186690 Color: 0

Bin 364: 1 of cap free
Amount of items: 3
Items: 
Size: 620338 Color: 0
Size: 195437 Color: 1
Size: 184225 Color: 1

Bin 365: 1 of cap free
Amount of items: 3
Items: 
Size: 623822 Color: 0
Size: 188357 Color: 1
Size: 187821 Color: 1

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 623899 Color: 0
Size: 192535 Color: 1
Size: 183566 Color: 0

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 625579 Color: 1
Size: 193633 Color: 1
Size: 180788 Color: 0

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 626102 Color: 0
Size: 188976 Color: 1
Size: 184922 Color: 0

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 626298 Color: 1
Size: 190191 Color: 1
Size: 183511 Color: 0

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 626459 Color: 0
Size: 192550 Color: 1
Size: 180991 Color: 0

Bin 371: 1 of cap free
Amount of items: 2
Items: 
Size: 628952 Color: 0
Size: 371048 Color: 1

Bin 372: 1 of cap free
Amount of items: 2
Items: 
Size: 630264 Color: 1
Size: 369736 Color: 0

Bin 373: 1 of cap free
Amount of items: 3
Items: 
Size: 631888 Color: 1
Size: 194224 Color: 1
Size: 173888 Color: 0

Bin 374: 1 of cap free
Amount of items: 2
Items: 
Size: 632860 Color: 0
Size: 367140 Color: 1

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 640723 Color: 0
Size: 180475 Color: 0
Size: 178802 Color: 1

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 641161 Color: 0
Size: 189625 Color: 1
Size: 169214 Color: 0

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 641228 Color: 0
Size: 181650 Color: 1
Size: 177122 Color: 0

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 644001 Color: 1
Size: 185701 Color: 0
Size: 170298 Color: 1

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 644565 Color: 1
Size: 189898 Color: 0
Size: 165537 Color: 1

Bin 380: 1 of cap free
Amount of items: 2
Items: 
Size: 646043 Color: 1
Size: 353957 Color: 0

Bin 381: 1 of cap free
Amount of items: 2
Items: 
Size: 647687 Color: 0
Size: 352313 Color: 1

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 648519 Color: 1
Size: 177948 Color: 1
Size: 173533 Color: 0

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 648788 Color: 1
Size: 196332 Color: 0
Size: 154880 Color: 1

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 649618 Color: 0
Size: 197348 Color: 1
Size: 153034 Color: 0

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 652948 Color: 0
Size: 185351 Color: 1
Size: 161701 Color: 0

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 654169 Color: 1
Size: 191320 Color: 1
Size: 154511 Color: 0

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 654833 Color: 0
Size: 194772 Color: 1
Size: 150395 Color: 0

Bin 388: 1 of cap free
Amount of items: 2
Items: 
Size: 655184 Color: 0
Size: 344816 Color: 1

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 655629 Color: 1
Size: 192376 Color: 0
Size: 151995 Color: 0

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 661469 Color: 0
Size: 191522 Color: 1
Size: 147009 Color: 0

Bin 391: 1 of cap free
Amount of items: 2
Items: 
Size: 666996 Color: 0
Size: 333004 Color: 1

Bin 392: 1 of cap free
Amount of items: 2
Items: 
Size: 668734 Color: 0
Size: 331266 Color: 1

Bin 393: 1 of cap free
Amount of items: 2
Items: 
Size: 670788 Color: 1
Size: 329212 Color: 0

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 671171 Color: 0
Size: 171971 Color: 0
Size: 156858 Color: 1

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 671117 Color: 1
Size: 178443 Color: 1
Size: 150440 Color: 0

Bin 396: 1 of cap free
Amount of items: 2
Items: 
Size: 679010 Color: 1
Size: 320990 Color: 0

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 679450 Color: 1
Size: 175993 Color: 1
Size: 144557 Color: 0

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 683453 Color: 1
Size: 176674 Color: 0
Size: 139873 Color: 0

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 684519 Color: 1
Size: 189378 Color: 0
Size: 126103 Color: 0

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 700212 Color: 1
Size: 160876 Color: 1
Size: 138912 Color: 0

Bin 401: 1 of cap free
Amount of items: 2
Items: 
Size: 700411 Color: 0
Size: 299589 Color: 1

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 702727 Color: 0
Size: 149367 Color: 1
Size: 147906 Color: 0

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 705417 Color: 0
Size: 166494 Color: 0
Size: 128089 Color: 1

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 707246 Color: 1
Size: 159093 Color: 0
Size: 133661 Color: 1

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 708992 Color: 0
Size: 154249 Color: 1
Size: 136759 Color: 0

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 709307 Color: 0
Size: 149997 Color: 1
Size: 140696 Color: 0

Bin 407: 1 of cap free
Amount of items: 2
Items: 
Size: 709853 Color: 1
Size: 290147 Color: 0

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 710657 Color: 0
Size: 170116 Color: 1
Size: 119227 Color: 0

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 711645 Color: 0
Size: 161464 Color: 0
Size: 126891 Color: 1

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 712576 Color: 1
Size: 153953 Color: 0
Size: 133471 Color: 0

Bin 411: 1 of cap free
Amount of items: 2
Items: 
Size: 721287 Color: 0
Size: 278713 Color: 1

Bin 412: 1 of cap free
Amount of items: 2
Items: 
Size: 721488 Color: 1
Size: 278512 Color: 0

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 724016 Color: 0
Size: 144384 Color: 1
Size: 131600 Color: 0

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 727623 Color: 0
Size: 140307 Color: 0
Size: 132070 Color: 1

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 728106 Color: 0
Size: 142166 Color: 1
Size: 129728 Color: 1

Bin 416: 1 of cap free
Amount of items: 2
Items: 
Size: 728965 Color: 1
Size: 271035 Color: 0

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 731887 Color: 0
Size: 147795 Color: 1
Size: 120318 Color: 0

Bin 418: 1 of cap free
Amount of items: 2
Items: 
Size: 732462 Color: 1
Size: 267538 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 732945 Color: 0
Size: 148263 Color: 1
Size: 118792 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 733363 Color: 0
Size: 138776 Color: 0
Size: 127861 Color: 1

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 733272 Color: 1
Size: 140463 Color: 1
Size: 126265 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 733951 Color: 0
Size: 135860 Color: 0
Size: 130189 Color: 1

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 736101 Color: 1
Size: 149715 Color: 0
Size: 114184 Color: 1

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 738930 Color: 1
Size: 135886 Color: 1
Size: 125184 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 741604 Color: 0
Size: 135282 Color: 0
Size: 123114 Color: 1

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 742049 Color: 1
Size: 139478 Color: 0
Size: 118473 Color: 1

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 744268 Color: 1
Size: 142854 Color: 1
Size: 112878 Color: 0

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 750388 Color: 0
Size: 129208 Color: 1
Size: 120404 Color: 1

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 753022 Color: 1
Size: 127352 Color: 0
Size: 119626 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 753080 Color: 0
Size: 124180 Color: 1
Size: 122740 Color: 1

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 754465 Color: 1
Size: 127872 Color: 0
Size: 117663 Color: 0

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 757897 Color: 0
Size: 126651 Color: 0
Size: 115452 Color: 1

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 758157 Color: 1
Size: 124448 Color: 1
Size: 117395 Color: 0

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 758521 Color: 1
Size: 126677 Color: 1
Size: 114802 Color: 0

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 759858 Color: 0
Size: 125855 Color: 1
Size: 114287 Color: 1

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 762209 Color: 0
Size: 121463 Color: 0
Size: 116328 Color: 1

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 765130 Color: 1
Size: 118417 Color: 1
Size: 116453 Color: 0

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 765386 Color: 1
Size: 119268 Color: 0
Size: 115346 Color: 0

Bin 439: 1 of cap free
Amount of items: 2
Items: 
Size: 766371 Color: 1
Size: 233629 Color: 0

Bin 440: 1 of cap free
Amount of items: 2
Items: 
Size: 784849 Color: 0
Size: 215151 Color: 1

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 786376 Color: 0
Size: 108552 Color: 1
Size: 105072 Color: 1

Bin 442: 1 of cap free
Amount of items: 2
Items: 
Size: 793498 Color: 0
Size: 206502 Color: 1

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 793932 Color: 1
Size: 105038 Color: 0
Size: 101030 Color: 1

Bin 444: 1 of cap free
Amount of items: 2
Items: 
Size: 797890 Color: 1
Size: 202110 Color: 0

Bin 445: 2 of cap free
Amount of items: 3
Items: 
Size: 418717 Color: 1
Size: 414005 Color: 0
Size: 167277 Color: 1

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 421716 Color: 0
Size: 418062 Color: 1
Size: 160221 Color: 0

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 421867 Color: 0
Size: 397255 Color: 1
Size: 180877 Color: 0

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 429556 Color: 1
Size: 382860 Color: 0
Size: 187583 Color: 0

Bin 449: 2 of cap free
Amount of items: 3
Items: 
Size: 430018 Color: 1
Size: 380768 Color: 0
Size: 189213 Color: 0

Bin 450: 2 of cap free
Amount of items: 3
Items: 
Size: 446689 Color: 1
Size: 362370 Color: 0
Size: 190940 Color: 0

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 454253 Color: 0
Size: 348227 Color: 1
Size: 197519 Color: 1

Bin 452: 2 of cap free
Amount of items: 3
Items: 
Size: 454458 Color: 0
Size: 354124 Color: 1
Size: 191417 Color: 1

Bin 453: 2 of cap free
Amount of items: 3
Items: 
Size: 462283 Color: 1
Size: 370490 Color: 0
Size: 167226 Color: 0

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 471015 Color: 1
Size: 363357 Color: 0
Size: 165627 Color: 1

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 471548 Color: 1
Size: 378421 Color: 0
Size: 150030 Color: 1

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 477006 Color: 0
Size: 369057 Color: 0
Size: 153936 Color: 1

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 478977 Color: 0
Size: 367835 Color: 0
Size: 153187 Color: 1

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 479422 Color: 0
Size: 323578 Color: 1
Size: 196999 Color: 0

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 506069 Color: 1
Size: 355835 Color: 1
Size: 138095 Color: 0

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 506972 Color: 1
Size: 273461 Color: 1
Size: 219566 Color: 0

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 512829 Color: 1
Size: 345854 Color: 0
Size: 141316 Color: 1

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 514525 Color: 1
Size: 322397 Color: 1
Size: 163077 Color: 0

Bin 463: 2 of cap free
Amount of items: 2
Items: 
Size: 520950 Color: 0
Size: 479049 Color: 1

Bin 464: 2 of cap free
Amount of items: 2
Items: 
Size: 529240 Color: 1
Size: 470759 Color: 0

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 534436 Color: 0
Size: 314818 Color: 1
Size: 150745 Color: 0

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 542873 Color: 1
Size: 289993 Color: 0
Size: 167133 Color: 0

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 543967 Color: 1
Size: 290555 Color: 0
Size: 165477 Color: 1

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 543904 Color: 0
Size: 277582 Color: 1
Size: 178513 Color: 0

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 543952 Color: 0
Size: 270004 Color: 0
Size: 186043 Color: 1

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 544368 Color: 0
Size: 272994 Color: 1
Size: 182637 Color: 0

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 544928 Color: 1
Size: 264326 Color: 1
Size: 190745 Color: 0

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 545258 Color: 0
Size: 331661 Color: 0
Size: 123080 Color: 1

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 550779 Color: 1
Size: 299790 Color: 0
Size: 149430 Color: 0

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 550953 Color: 1
Size: 299845 Color: 0
Size: 149201 Color: 0

Bin 475: 2 of cap free
Amount of items: 2
Items: 
Size: 555197 Color: 0
Size: 444802 Color: 1

Bin 476: 2 of cap free
Amount of items: 2
Items: 
Size: 563633 Color: 1
Size: 436366 Color: 0

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 564760 Color: 0
Size: 255814 Color: 0
Size: 179425 Color: 1

Bin 478: 2 of cap free
Amount of items: 2
Items: 
Size: 564660 Color: 1
Size: 435339 Color: 0

Bin 479: 2 of cap free
Amount of items: 2
Items: 
Size: 567725 Color: 0
Size: 432274 Color: 1

Bin 480: 2 of cap free
Amount of items: 2
Items: 
Size: 569471 Color: 0
Size: 430528 Color: 1

Bin 481: 2 of cap free
Amount of items: 2
Items: 
Size: 570781 Color: 0
Size: 429218 Color: 1

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 574922 Color: 1
Size: 302684 Color: 1
Size: 122393 Color: 0

Bin 483: 2 of cap free
Amount of items: 2
Items: 
Size: 577289 Color: 1
Size: 422710 Color: 0

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 582035 Color: 0
Size: 229948 Color: 0
Size: 188016 Color: 1

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 582799 Color: 0
Size: 276581 Color: 1
Size: 140619 Color: 0

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 586301 Color: 0
Size: 273222 Color: 1
Size: 140476 Color: 0

Bin 487: 2 of cap free
Amount of items: 2
Items: 
Size: 591151 Color: 1
Size: 408848 Color: 0

Bin 488: 2 of cap free
Amount of items: 2
Items: 
Size: 591430 Color: 1
Size: 408569 Color: 0

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 596652 Color: 1
Size: 256873 Color: 0
Size: 146474 Color: 1

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 596928 Color: 1
Size: 231104 Color: 1
Size: 171967 Color: 0

Bin 491: 2 of cap free
Amount of items: 2
Items: 
Size: 599092 Color: 1
Size: 400907 Color: 0

Bin 492: 2 of cap free
Amount of items: 2
Items: 
Size: 599728 Color: 0
Size: 400271 Color: 1

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 606141 Color: 1
Size: 202772 Color: 1
Size: 191086 Color: 0

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 607376 Color: 1
Size: 213796 Color: 0
Size: 178827 Color: 0

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 609253 Color: 1
Size: 199190 Color: 1
Size: 191556 Color: 0

Bin 496: 2 of cap free
Amount of items: 2
Items: 
Size: 611814 Color: 1
Size: 388185 Color: 0

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 623642 Color: 1
Size: 226070 Color: 0
Size: 150287 Color: 0

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 623943 Color: 1
Size: 195248 Color: 1
Size: 180808 Color: 0

Bin 499: 2 of cap free
Amount of items: 2
Items: 
Size: 625623 Color: 0
Size: 374376 Color: 1

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 648881 Color: 0
Size: 188643 Color: 1
Size: 162475 Color: 0

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 648973 Color: 0
Size: 183503 Color: 0
Size: 167523 Color: 1

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 654885 Color: 1
Size: 198221 Color: 0
Size: 146893 Color: 0

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 656114 Color: 1
Size: 196346 Color: 0
Size: 147539 Color: 0

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 666385 Color: 0
Size: 193694 Color: 1
Size: 139920 Color: 1

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 672454 Color: 1
Size: 165735 Color: 1
Size: 161810 Color: 0

Bin 506: 2 of cap free
Amount of items: 2
Items: 
Size: 686791 Color: 1
Size: 313208 Color: 0

Bin 507: 2 of cap free
Amount of items: 3
Items: 
Size: 699896 Color: 0
Size: 170935 Color: 0
Size: 129168 Color: 1

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 700066 Color: 0
Size: 175858 Color: 0
Size: 124075 Color: 1

Bin 509: 2 of cap free
Amount of items: 3
Items: 
Size: 703973 Color: 0
Size: 161205 Color: 1
Size: 134821 Color: 1

Bin 510: 2 of cap free
Amount of items: 3
Items: 
Size: 705293 Color: 0
Size: 148501 Color: 1
Size: 146205 Color: 0

Bin 511: 2 of cap free
Amount of items: 3
Items: 
Size: 707108 Color: 0
Size: 165513 Color: 0
Size: 127378 Color: 1

Bin 512: 2 of cap free
Amount of items: 3
Items: 
Size: 708324 Color: 1
Size: 169666 Color: 0
Size: 122009 Color: 0

Bin 513: 2 of cap free
Amount of items: 3
Items: 
Size: 712642 Color: 1
Size: 163120 Color: 0
Size: 124237 Color: 0

Bin 514: 2 of cap free
Amount of items: 2
Items: 
Size: 717210 Color: 0
Size: 282789 Color: 1

Bin 515: 2 of cap free
Amount of items: 2
Items: 
Size: 722687 Color: 0
Size: 277312 Color: 1

Bin 516: 2 of cap free
Amount of items: 3
Items: 
Size: 732063 Color: 0
Size: 142573 Color: 0
Size: 125363 Color: 1

Bin 517: 2 of cap free
Amount of items: 3
Items: 
Size: 732524 Color: 0
Size: 144073 Color: 1
Size: 123402 Color: 0

Bin 518: 2 of cap free
Amount of items: 3
Items: 
Size: 732524 Color: 1
Size: 152737 Color: 0
Size: 114738 Color: 1

Bin 519: 2 of cap free
Amount of items: 3
Items: 
Size: 734664 Color: 1
Size: 148700 Color: 0
Size: 116635 Color: 0

Bin 520: 2 of cap free
Amount of items: 2
Items: 
Size: 737416 Color: 1
Size: 262583 Color: 0

Bin 521: 2 of cap free
Amount of items: 3
Items: 
Size: 741767 Color: 1
Size: 131948 Color: 1
Size: 126284 Color: 0

Bin 522: 2 of cap free
Amount of items: 3
Items: 
Size: 743808 Color: 1
Size: 139150 Color: 1
Size: 117041 Color: 0

Bin 523: 2 of cap free
Amount of items: 3
Items: 
Size: 744228 Color: 1
Size: 132386 Color: 1
Size: 123385 Color: 0

Bin 524: 2 of cap free
Amount of items: 2
Items: 
Size: 745988 Color: 1
Size: 254011 Color: 0

Bin 525: 2 of cap free
Amount of items: 3
Items: 
Size: 748689 Color: 1
Size: 131200 Color: 0
Size: 120110 Color: 1

Bin 526: 2 of cap free
Amount of items: 3
Items: 
Size: 754545 Color: 1
Size: 129169 Color: 0
Size: 116285 Color: 1

Bin 527: 2 of cap free
Amount of items: 2
Items: 
Size: 755839 Color: 0
Size: 244160 Color: 1

Bin 528: 2 of cap free
Amount of items: 3
Items: 
Size: 757901 Color: 1
Size: 123810 Color: 1
Size: 118288 Color: 0

Bin 529: 2 of cap free
Amount of items: 3
Items: 
Size: 759820 Color: 0
Size: 125054 Color: 0
Size: 115125 Color: 1

Bin 530: 2 of cap free
Amount of items: 3
Items: 
Size: 764146 Color: 1
Size: 118284 Color: 1
Size: 117569 Color: 0

Bin 531: 2 of cap free
Amount of items: 3
Items: 
Size: 764206 Color: 1
Size: 121513 Color: 0
Size: 114280 Color: 0

Bin 532: 2 of cap free
Amount of items: 3
Items: 
Size: 764570 Color: 1
Size: 122878 Color: 1
Size: 112551 Color: 0

Bin 533: 2 of cap free
Amount of items: 2
Items: 
Size: 767313 Color: 0
Size: 232686 Color: 1

Bin 534: 2 of cap free
Amount of items: 2
Items: 
Size: 781920 Color: 0
Size: 218079 Color: 1

Bin 535: 2 of cap free
Amount of items: 2
Items: 
Size: 783021 Color: 1
Size: 216978 Color: 0

Bin 536: 2 of cap free
Amount of items: 2
Items: 
Size: 786916 Color: 0
Size: 213083 Color: 1

Bin 537: 2 of cap free
Amount of items: 2
Items: 
Size: 791336 Color: 0
Size: 208663 Color: 1

Bin 538: 2 of cap free
Amount of items: 3
Items: 
Size: 793052 Color: 0
Size: 104924 Color: 0
Size: 102023 Color: 1

Bin 539: 2 of cap free
Amount of items: 3
Items: 
Size: 793721 Color: 1
Size: 104968 Color: 0
Size: 101310 Color: 0

Bin 540: 2 of cap free
Amount of items: 3
Items: 
Size: 793724 Color: 1
Size: 106049 Color: 0
Size: 100226 Color: 1

Bin 541: 2 of cap free
Amount of items: 3
Items: 
Size: 794011 Color: 1
Size: 103800 Color: 0
Size: 102188 Color: 0

Bin 542: 3 of cap free
Amount of items: 3
Items: 
Size: 446719 Color: 1
Size: 381862 Color: 0
Size: 171417 Color: 1

Bin 543: 3 of cap free
Amount of items: 3
Items: 
Size: 455494 Color: 0
Size: 381677 Color: 0
Size: 162827 Color: 1

Bin 544: 3 of cap free
Amount of items: 3
Items: 
Size: 462188 Color: 1
Size: 381854 Color: 0
Size: 155956 Color: 1

Bin 545: 3 of cap free
Amount of items: 3
Items: 
Size: 477242 Color: 1
Size: 372309 Color: 0
Size: 150447 Color: 1

Bin 546: 3 of cap free
Amount of items: 3
Items: 
Size: 478918 Color: 0
Size: 368238 Color: 0
Size: 152842 Color: 1

Bin 547: 3 of cap free
Amount of items: 2
Items: 
Size: 502458 Color: 1
Size: 497540 Color: 0

Bin 548: 3 of cap free
Amount of items: 2
Items: 
Size: 504064 Color: 0
Size: 495934 Color: 1

Bin 549: 3 of cap free
Amount of items: 3
Items: 
Size: 512548 Color: 0
Size: 324653 Color: 1
Size: 162797 Color: 0

Bin 550: 3 of cap free
Amount of items: 3
Items: 
Size: 512714 Color: 1
Size: 352998 Color: 1
Size: 134286 Color: 0

Bin 551: 3 of cap free
Amount of items: 2
Items: 
Size: 518239 Color: 0
Size: 481759 Color: 1

Bin 552: 3 of cap free
Amount of items: 2
Items: 
Size: 523425 Color: 1
Size: 476573 Color: 0

Bin 553: 3 of cap free
Amount of items: 3
Items: 
Size: 540165 Color: 0
Size: 309041 Color: 1
Size: 150792 Color: 0

Bin 554: 3 of cap free
Amount of items: 3
Items: 
Size: 541545 Color: 0
Size: 317009 Color: 1
Size: 141444 Color: 1

Bin 555: 3 of cap free
Amount of items: 3
Items: 
Size: 544055 Color: 1
Size: 269316 Color: 0
Size: 186627 Color: 1

Bin 556: 3 of cap free
Amount of items: 3
Items: 
Size: 544250 Color: 1
Size: 290015 Color: 0
Size: 165733 Color: 1

Bin 557: 3 of cap free
Amount of items: 3
Items: 
Size: 549259 Color: 1
Size: 256965 Color: 0
Size: 193774 Color: 1

Bin 558: 3 of cap free
Amount of items: 2
Items: 
Size: 554530 Color: 1
Size: 445468 Color: 0

Bin 559: 3 of cap free
Amount of items: 2
Items: 
Size: 555541 Color: 1
Size: 444457 Color: 0

Bin 560: 3 of cap free
Amount of items: 3
Items: 
Size: 564638 Color: 1
Size: 309567 Color: 1
Size: 125793 Color: 0

Bin 561: 3 of cap free
Amount of items: 2
Items: 
Size: 573096 Color: 0
Size: 426902 Color: 1

Bin 562: 3 of cap free
Amount of items: 3
Items: 
Size: 595260 Color: 0
Size: 279448 Color: 1
Size: 125290 Color: 0

Bin 563: 3 of cap free
Amount of items: 3
Items: 
Size: 597038 Color: 0
Size: 255814 Color: 0
Size: 147146 Color: 1

Bin 564: 3 of cap free
Amount of items: 3
Items: 
Size: 598096 Color: 1
Size: 213773 Color: 0
Size: 188129 Color: 1

Bin 565: 3 of cap free
Amount of items: 3
Items: 
Size: 598188 Color: 0
Size: 274168 Color: 1
Size: 127642 Color: 0

Bin 566: 3 of cap free
Amount of items: 3
Items: 
Size: 606005 Color: 0
Size: 269360 Color: 0
Size: 124633 Color: 1

Bin 567: 3 of cap free
Amount of items: 3
Items: 
Size: 609562 Color: 0
Size: 236500 Color: 0
Size: 153936 Color: 1

Bin 568: 3 of cap free
Amount of items: 2
Items: 
Size: 609594 Color: 0
Size: 390404 Color: 1

Bin 569: 3 of cap free
Amount of items: 2
Items: 
Size: 610133 Color: 1
Size: 389865 Color: 0

Bin 570: 3 of cap free
Amount of items: 2
Items: 
Size: 613368 Color: 1
Size: 386630 Color: 0

Bin 571: 3 of cap free
Amount of items: 2
Items: 
Size: 617330 Color: 0
Size: 382668 Color: 1

Bin 572: 3 of cap free
Amount of items: 3
Items: 
Size: 623904 Color: 0
Size: 226061 Color: 0
Size: 150033 Color: 1

Bin 573: 3 of cap free
Amount of items: 3
Items: 
Size: 626431 Color: 0
Size: 190711 Color: 1
Size: 182856 Color: 0

Bin 574: 3 of cap free
Amount of items: 2
Items: 
Size: 634297 Color: 1
Size: 365701 Color: 0

Bin 575: 3 of cap free
Amount of items: 2
Items: 
Size: 635228 Color: 1
Size: 364770 Color: 0

Bin 576: 3 of cap free
Amount of items: 2
Items: 
Size: 655995 Color: 1
Size: 344003 Color: 0

Bin 577: 3 of cap free
Amount of items: 3
Items: 
Size: 707436 Color: 1
Size: 171349 Color: 1
Size: 121213 Color: 0

Bin 578: 3 of cap free
Amount of items: 2
Items: 
Size: 711116 Color: 1
Size: 288882 Color: 0

Bin 579: 3 of cap free
Amount of items: 2
Items: 
Size: 722874 Color: 1
Size: 277124 Color: 0

Bin 580: 3 of cap free
Amount of items: 3
Items: 
Size: 731544 Color: 1
Size: 143245 Color: 1
Size: 125209 Color: 0

Bin 581: 3 of cap free
Amount of items: 3
Items: 
Size: 731742 Color: 0
Size: 148134 Color: 1
Size: 120122 Color: 0

Bin 582: 3 of cap free
Amount of items: 3
Items: 
Size: 732721 Color: 1
Size: 140183 Color: 0
Size: 127094 Color: 0

Bin 583: 3 of cap free
Amount of items: 2
Items: 
Size: 733414 Color: 0
Size: 266584 Color: 1

Bin 584: 3 of cap free
Amount of items: 3
Items: 
Size: 738901 Color: 0
Size: 147388 Color: 1
Size: 113709 Color: 0

Bin 585: 3 of cap free
Amount of items: 3
Items: 
Size: 743146 Color: 1
Size: 136465 Color: 0
Size: 120387 Color: 1

Bin 586: 3 of cap free
Amount of items: 3
Items: 
Size: 758577 Color: 0
Size: 127122 Color: 1
Size: 114299 Color: 0

Bin 587: 3 of cap free
Amount of items: 3
Items: 
Size: 762833 Color: 0
Size: 123364 Color: 0
Size: 113801 Color: 1

Bin 588: 3 of cap free
Amount of items: 3
Items: 
Size: 765665 Color: 1
Size: 118127 Color: 0
Size: 116206 Color: 0

Bin 589: 3 of cap free
Amount of items: 2
Items: 
Size: 774890 Color: 1
Size: 225108 Color: 0

Bin 590: 3 of cap free
Amount of items: 3
Items: 
Size: 781070 Color: 0
Size: 110104 Color: 0
Size: 108824 Color: 1

Bin 591: 3 of cap free
Amount of items: 2
Items: 
Size: 786225 Color: 1
Size: 213773 Color: 0

Bin 592: 3 of cap free
Amount of items: 3
Items: 
Size: 787737 Color: 1
Size: 111835 Color: 0
Size: 100426 Color: 1

Bin 593: 3 of cap free
Amount of items: 2
Items: 
Size: 790555 Color: 0
Size: 209443 Color: 1

Bin 594: 3 of cap free
Amount of items: 2
Items: 
Size: 792025 Color: 1
Size: 207973 Color: 0

Bin 595: 3 of cap free
Amount of items: 2
Items: 
Size: 794253 Color: 1
Size: 205745 Color: 0

Bin 596: 4 of cap free
Amount of items: 3
Items: 
Size: 418642 Color: 1
Size: 406801 Color: 1
Size: 174554 Color: 0

Bin 597: 4 of cap free
Amount of items: 3
Items: 
Size: 425040 Color: 0
Size: 418477 Color: 1
Size: 156480 Color: 0

Bin 598: 4 of cap free
Amount of items: 3
Items: 
Size: 425128 Color: 0
Size: 421942 Color: 0
Size: 152927 Color: 1

Bin 599: 4 of cap free
Amount of items: 3
Items: 
Size: 427768 Color: 1
Size: 425100 Color: 0
Size: 147129 Color: 0

Bin 600: 4 of cap free
Amount of items: 3
Items: 
Size: 444797 Color: 1
Size: 428013 Color: 1
Size: 127187 Color: 0

Bin 601: 4 of cap free
Amount of items: 3
Items: 
Size: 467477 Color: 1
Size: 362428 Color: 0
Size: 170092 Color: 1

Bin 602: 4 of cap free
Amount of items: 3
Items: 
Size: 467589 Color: 1
Size: 383237 Color: 0
Size: 149171 Color: 0

Bin 603: 4 of cap free
Amount of items: 3
Items: 
Size: 479468 Color: 0
Size: 373065 Color: 0
Size: 147464 Color: 1

Bin 604: 4 of cap free
Amount of items: 2
Items: 
Size: 501124 Color: 0
Size: 498873 Color: 1

Bin 605: 4 of cap free
Amount of items: 2
Items: 
Size: 509425 Color: 1
Size: 490572 Color: 0

Bin 606: 4 of cap free
Amount of items: 3
Items: 
Size: 513161 Color: 0
Size: 348402 Color: 1
Size: 138434 Color: 0

Bin 607: 4 of cap free
Amount of items: 3
Items: 
Size: 513700 Color: 0
Size: 337166 Color: 0
Size: 149131 Color: 1

Bin 608: 4 of cap free
Amount of items: 3
Items: 
Size: 514420 Color: 0
Size: 312144 Color: 1
Size: 173433 Color: 0

Bin 609: 4 of cap free
Amount of items: 2
Items: 
Size: 519240 Color: 1
Size: 480757 Color: 0

Bin 610: 4 of cap free
Amount of items: 2
Items: 
Size: 521192 Color: 1
Size: 478805 Color: 0

Bin 611: 4 of cap free
Amount of items: 2
Items: 
Size: 521574 Color: 1
Size: 478423 Color: 0

Bin 612: 4 of cap free
Amount of items: 3
Items: 
Size: 540204 Color: 0
Size: 273207 Color: 1
Size: 186586 Color: 1

Bin 613: 4 of cap free
Amount of items: 3
Items: 
Size: 550035 Color: 1
Size: 290484 Color: 0
Size: 159478 Color: 1

Bin 614: 4 of cap free
Amount of items: 2
Items: 
Size: 566345 Color: 1
Size: 433652 Color: 0

Bin 615: 4 of cap free
Amount of items: 2
Items: 
Size: 574189 Color: 1
Size: 425808 Color: 0

Bin 616: 4 of cap free
Amount of items: 2
Items: 
Size: 575454 Color: 0
Size: 424543 Color: 1

Bin 617: 4 of cap free
Amount of items: 3
Items: 
Size: 594683 Color: 0
Size: 266502 Color: 1
Size: 138812 Color: 1

Bin 618: 4 of cap free
Amount of items: 2
Items: 
Size: 597178 Color: 0
Size: 402819 Color: 1

Bin 619: 4 of cap free
Amount of items: 3
Items: 
Size: 598814 Color: 0
Size: 207142 Color: 1
Size: 194041 Color: 0

Bin 620: 4 of cap free
Amount of items: 2
Items: 
Size: 599106 Color: 0
Size: 400891 Color: 1

Bin 621: 4 of cap free
Amount of items: 2
Items: 
Size: 604949 Color: 0
Size: 395048 Color: 1

Bin 622: 4 of cap free
Amount of items: 2
Items: 
Size: 657591 Color: 0
Size: 342406 Color: 1

Bin 623: 4 of cap free
Amount of items: 2
Items: 
Size: 667718 Color: 1
Size: 332279 Color: 0

Bin 624: 4 of cap free
Amount of items: 2
Items: 
Size: 669634 Color: 0
Size: 330363 Color: 1

Bin 625: 4 of cap free
Amount of items: 2
Items: 
Size: 674168 Color: 1
Size: 325829 Color: 0

Bin 626: 4 of cap free
Amount of items: 2
Items: 
Size: 678696 Color: 1
Size: 321301 Color: 0

Bin 627: 4 of cap free
Amount of items: 2
Items: 
Size: 680565 Color: 0
Size: 319432 Color: 1

Bin 628: 4 of cap free
Amount of items: 2
Items: 
Size: 686931 Color: 0
Size: 313066 Color: 1

Bin 629: 4 of cap free
Amount of items: 3
Items: 
Size: 710630 Color: 0
Size: 167850 Color: 1
Size: 121517 Color: 1

Bin 630: 4 of cap free
Amount of items: 2
Items: 
Size: 716473 Color: 1
Size: 283524 Color: 0

Bin 631: 4 of cap free
Amount of items: 2
Items: 
Size: 720826 Color: 1
Size: 279171 Color: 0

Bin 632: 4 of cap free
Amount of items: 3
Items: 
Size: 727628 Color: 0
Size: 145570 Color: 1
Size: 126799 Color: 1

Bin 633: 4 of cap free
Amount of items: 2
Items: 
Size: 732595 Color: 1
Size: 267402 Color: 0

Bin 634: 4 of cap free
Amount of items: 2
Items: 
Size: 765969 Color: 0
Size: 234028 Color: 1

Bin 635: 4 of cap free
Amount of items: 3
Items: 
Size: 781708 Color: 0
Size: 110301 Color: 1
Size: 107988 Color: 1

Bin 636: 4 of cap free
Amount of items: 3
Items: 
Size: 783595 Color: 0
Size: 109147 Color: 1
Size: 107255 Color: 1

Bin 637: 4 of cap free
Amount of items: 3
Items: 
Size: 787626 Color: 0
Size: 107860 Color: 0
Size: 104511 Color: 1

Bin 638: 4 of cap free
Amount of items: 3
Items: 
Size: 792359 Color: 1
Size: 106420 Color: 1
Size: 101218 Color: 0

Bin 639: 4 of cap free
Amount of items: 3
Items: 
Size: 792701 Color: 1
Size: 105250 Color: 0
Size: 102046 Color: 1

Bin 640: 4 of cap free
Amount of items: 3
Items: 
Size: 793387 Color: 1
Size: 104772 Color: 1
Size: 101838 Color: 0

Bin 641: 4 of cap free
Amount of items: 3
Items: 
Size: 793487 Color: 1
Size: 105911 Color: 0
Size: 100599 Color: 1

Bin 642: 4 of cap free
Amount of items: 3
Items: 
Size: 793430 Color: 0
Size: 104571 Color: 0
Size: 101996 Color: 1

Bin 643: 4 of cap free
Amount of items: 2
Items: 
Size: 797637 Color: 1
Size: 202360 Color: 0

Bin 644: 5 of cap free
Amount of items: 7
Items: 
Size: 149623 Color: 0
Size: 149261 Color: 0
Size: 143718 Color: 1
Size: 143547 Color: 1
Size: 142626 Color: 1
Size: 142498 Color: 0
Size: 128723 Color: 1

Bin 645: 5 of cap free
Amount of items: 5
Items: 
Size: 231160 Color: 1
Size: 223460 Color: 0
Size: 222594 Color: 0
Size: 169291 Color: 0
Size: 153491 Color: 1

Bin 646: 5 of cap free
Amount of items: 3
Items: 
Size: 445020 Color: 1
Size: 356217 Color: 1
Size: 198759 Color: 0

Bin 647: 5 of cap free
Amount of items: 3
Items: 
Size: 448866 Color: 1
Size: 367163 Color: 0
Size: 183967 Color: 1

Bin 648: 5 of cap free
Amount of items: 3
Items: 
Size: 456222 Color: 0
Size: 393141 Color: 0
Size: 150633 Color: 1

Bin 649: 5 of cap free
Amount of items: 3
Items: 
Size: 476384 Color: 0
Size: 345879 Color: 0
Size: 177733 Color: 1

Bin 650: 5 of cap free
Amount of items: 3
Items: 
Size: 477354 Color: 1
Size: 368057 Color: 0
Size: 154585 Color: 1

Bin 651: 5 of cap free
Amount of items: 3
Items: 
Size: 478523 Color: 0
Size: 354010 Color: 1
Size: 167463 Color: 0

Bin 652: 5 of cap free
Amount of items: 3
Items: 
Size: 479184 Color: 0
Size: 331703 Color: 0
Size: 189109 Color: 1

Bin 653: 5 of cap free
Amount of items: 3
Items: 
Size: 479443 Color: 0
Size: 361205 Color: 0
Size: 159348 Color: 1

Bin 654: 5 of cap free
Amount of items: 3
Items: 
Size: 496429 Color: 0
Size: 381392 Color: 1
Size: 122175 Color: 0

Bin 655: 5 of cap free
Amount of items: 2
Items: 
Size: 507025 Color: 0
Size: 492971 Color: 1

Bin 656: 5 of cap free
Amount of items: 3
Items: 
Size: 507044 Color: 0
Size: 345658 Color: 0
Size: 147294 Color: 1

Bin 657: 5 of cap free
Amount of items: 2
Items: 
Size: 515247 Color: 0
Size: 484749 Color: 1

Bin 658: 5 of cap free
Amount of items: 2
Items: 
Size: 531971 Color: 0
Size: 468025 Color: 1

Bin 659: 5 of cap free
Amount of items: 2
Items: 
Size: 533173 Color: 0
Size: 466823 Color: 1

Bin 660: 5 of cap free
Amount of items: 3
Items: 
Size: 540476 Color: 0
Size: 315343 Color: 1
Size: 144177 Color: 1

Bin 661: 5 of cap free
Amount of items: 3
Items: 
Size: 541607 Color: 0
Size: 277629 Color: 1
Size: 180760 Color: 0

Bin 662: 5 of cap free
Amount of items: 3
Items: 
Size: 541680 Color: 0
Size: 321302 Color: 1
Size: 137014 Color: 1

Bin 663: 5 of cap free
Amount of items: 2
Items: 
Size: 546407 Color: 1
Size: 453589 Color: 0

Bin 664: 5 of cap free
Amount of items: 3
Items: 
Size: 550951 Color: 1
Size: 273225 Color: 1
Size: 175820 Color: 0

Bin 665: 5 of cap free
Amount of items: 2
Items: 
Size: 553991 Color: 0
Size: 446005 Color: 1

Bin 666: 5 of cap free
Amount of items: 2
Items: 
Size: 555488 Color: 1
Size: 444508 Color: 0

Bin 667: 5 of cap free
Amount of items: 3
Items: 
Size: 564585 Color: 0
Size: 317289 Color: 1
Size: 118122 Color: 0

Bin 668: 5 of cap free
Amount of items: 2
Items: 
Size: 566455 Color: 1
Size: 433541 Color: 0

Bin 669: 5 of cap free
Amount of items: 3
Items: 
Size: 574546 Color: 0
Size: 264187 Color: 1
Size: 161263 Color: 1

Bin 670: 5 of cap free
Amount of items: 2
Items: 
Size: 583124 Color: 0
Size: 416872 Color: 1

Bin 671: 5 of cap free
Amount of items: 2
Items: 
Size: 592805 Color: 0
Size: 407191 Color: 1

Bin 672: 5 of cap free
Amount of items: 3
Items: 
Size: 596151 Color: 0
Size: 268820 Color: 0
Size: 135025 Color: 1

Bin 673: 5 of cap free
Amount of items: 3
Items: 
Size: 598793 Color: 0
Size: 223137 Color: 0
Size: 178066 Color: 1

Bin 674: 5 of cap free
Amount of items: 2
Items: 
Size: 603944 Color: 1
Size: 396052 Color: 0

Bin 675: 5 of cap free
Amount of items: 2
Items: 
Size: 613136 Color: 0
Size: 386860 Color: 1

Bin 676: 5 of cap free
Amount of items: 3
Items: 
Size: 626462 Color: 1
Size: 222613 Color: 0
Size: 150921 Color: 1

Bin 677: 5 of cap free
Amount of items: 2
Items: 
Size: 637444 Color: 1
Size: 362552 Color: 0

Bin 678: 5 of cap free
Amount of items: 2
Items: 
Size: 637948 Color: 1
Size: 362048 Color: 0

Bin 679: 5 of cap free
Amount of items: 3
Items: 
Size: 673287 Color: 1
Size: 197460 Color: 0
Size: 129249 Color: 0

Bin 680: 5 of cap free
Amount of items: 2
Items: 
Size: 675541 Color: 1
Size: 324455 Color: 0

Bin 681: 5 of cap free
Amount of items: 2
Items: 
Size: 675553 Color: 1
Size: 324443 Color: 0

Bin 682: 5 of cap free
Amount of items: 2
Items: 
Size: 684005 Color: 1
Size: 315991 Color: 0

Bin 683: 5 of cap free
Amount of items: 2
Items: 
Size: 694653 Color: 1
Size: 305343 Color: 0

Bin 684: 5 of cap free
Amount of items: 3
Items: 
Size: 711498 Color: 0
Size: 154999 Color: 1
Size: 133499 Color: 1

Bin 685: 5 of cap free
Amount of items: 3
Items: 
Size: 728371 Color: 1
Size: 141008 Color: 0
Size: 130617 Color: 0

Bin 686: 5 of cap free
Amount of items: 3
Items: 
Size: 765308 Color: 1
Size: 119496 Color: 1
Size: 115192 Color: 0

Bin 687: 5 of cap free
Amount of items: 3
Items: 
Size: 766134 Color: 1
Size: 119976 Color: 0
Size: 113886 Color: 0

Bin 688: 5 of cap free
Amount of items: 2
Items: 
Size: 766417 Color: 0
Size: 233579 Color: 1

Bin 689: 5 of cap free
Amount of items: 3
Items: 
Size: 768967 Color: 1
Size: 116981 Color: 0
Size: 114048 Color: 1

Bin 690: 5 of cap free
Amount of items: 2
Items: 
Size: 791579 Color: 0
Size: 208417 Color: 1

Bin 691: 5 of cap free
Amount of items: 3
Items: 
Size: 792188 Color: 1
Size: 104786 Color: 1
Size: 103022 Color: 0

Bin 692: 5 of cap free
Amount of items: 3
Items: 
Size: 793115 Color: 1
Size: 105693 Color: 1
Size: 101188 Color: 0

Bin 693: 6 of cap free
Amount of items: 3
Items: 
Size: 417976 Color: 1
Size: 382434 Color: 0
Size: 199585 Color: 0

Bin 694: 6 of cap free
Amount of items: 3
Items: 
Size: 424731 Color: 0
Size: 394010 Color: 1
Size: 181254 Color: 0

Bin 695: 6 of cap free
Amount of items: 3
Items: 
Size: 478579 Color: 1
Size: 350138 Color: 1
Size: 171278 Color: 0

Bin 696: 6 of cap free
Amount of items: 3
Items: 
Size: 479182 Color: 1
Size: 357474 Color: 1
Size: 163339 Color: 0

Bin 697: 6 of cap free
Amount of items: 3
Items: 
Size: 479208 Color: 1
Size: 369748 Color: 0
Size: 151039 Color: 1

Bin 698: 6 of cap free
Amount of items: 2
Items: 
Size: 501136 Color: 1
Size: 498859 Color: 0

Bin 699: 6 of cap free
Amount of items: 3
Items: 
Size: 506201 Color: 0
Size: 367777 Color: 0
Size: 126017 Color: 1

Bin 700: 6 of cap free
Amount of items: 2
Items: 
Size: 513197 Color: 1
Size: 486798 Color: 0

Bin 701: 6 of cap free
Amount of items: 2
Items: 
Size: 516604 Color: 1
Size: 483391 Color: 0

Bin 702: 6 of cap free
Amount of items: 3
Items: 
Size: 534494 Color: 1
Size: 322320 Color: 1
Size: 143181 Color: 0

Bin 703: 6 of cap free
Amount of items: 3
Items: 
Size: 537447 Color: 0
Size: 310593 Color: 0
Size: 151955 Color: 1

Bin 704: 6 of cap free
Amount of items: 3
Items: 
Size: 538370 Color: 0
Size: 310324 Color: 0
Size: 151301 Color: 1

Bin 705: 6 of cap free
Amount of items: 3
Items: 
Size: 539019 Color: 0
Size: 324645 Color: 1
Size: 136331 Color: 1

Bin 706: 6 of cap free
Amount of items: 3
Items: 
Size: 540324 Color: 0
Size: 306294 Color: 1
Size: 153377 Color: 0

Bin 707: 6 of cap free
Amount of items: 3
Items: 
Size: 544904 Color: 0
Size: 312277 Color: 1
Size: 142814 Color: 0

Bin 708: 6 of cap free
Amount of items: 2
Items: 
Size: 547666 Color: 1
Size: 452329 Color: 0

Bin 709: 6 of cap free
Amount of items: 3
Items: 
Size: 550685 Color: 1
Size: 322474 Color: 1
Size: 126836 Color: 0

Bin 710: 6 of cap free
Amount of items: 2
Items: 
Size: 550967 Color: 0
Size: 449028 Color: 1

Bin 711: 6 of cap free
Amount of items: 2
Items: 
Size: 555477 Color: 1
Size: 444518 Color: 0

Bin 712: 6 of cap free
Amount of items: 2
Items: 
Size: 562250 Color: 0
Size: 437745 Color: 1

Bin 713: 6 of cap free
Amount of items: 2
Items: 
Size: 564195 Color: 1
Size: 435800 Color: 0

Bin 714: 6 of cap free
Amount of items: 3
Items: 
Size: 582785 Color: 1
Size: 260219 Color: 0
Size: 156991 Color: 1

Bin 715: 6 of cap free
Amount of items: 2
Items: 
Size: 582850 Color: 0
Size: 417145 Color: 1

Bin 716: 6 of cap free
Amount of items: 3
Items: 
Size: 596835 Color: 0
Size: 260250 Color: 0
Size: 142910 Color: 1

Bin 717: 6 of cap free
Amount of items: 3
Items: 
Size: 626048 Color: 0
Size: 192048 Color: 1
Size: 181899 Color: 1

Bin 718: 6 of cap free
Amount of items: 2
Items: 
Size: 630273 Color: 0
Size: 369722 Color: 1

Bin 719: 6 of cap free
Amount of items: 2
Items: 
Size: 637058 Color: 1
Size: 362937 Color: 0

Bin 720: 6 of cap free
Amount of items: 2
Items: 
Size: 646271 Color: 0
Size: 353724 Color: 1

Bin 721: 6 of cap free
Amount of items: 2
Items: 
Size: 653174 Color: 0
Size: 346821 Color: 1

Bin 722: 6 of cap free
Amount of items: 2
Items: 
Size: 655567 Color: 0
Size: 344428 Color: 1

Bin 723: 6 of cap free
Amount of items: 2
Items: 
Size: 664225 Color: 0
Size: 335770 Color: 1

Bin 724: 6 of cap free
Amount of items: 2
Items: 
Size: 669226 Color: 0
Size: 330769 Color: 1

Bin 725: 6 of cap free
Amount of items: 2
Items: 
Size: 680734 Color: 1
Size: 319261 Color: 0

Bin 726: 6 of cap free
Amount of items: 2
Items: 
Size: 702729 Color: 1
Size: 297266 Color: 0

Bin 727: 6 of cap free
Amount of items: 3
Items: 
Size: 728048 Color: 1
Size: 149419 Color: 0
Size: 122528 Color: 0

Bin 728: 6 of cap free
Amount of items: 2
Items: 
Size: 728378 Color: 1
Size: 271617 Color: 0

Bin 729: 6 of cap free
Amount of items: 3
Items: 
Size: 758137 Color: 0
Size: 130696 Color: 1
Size: 111162 Color: 0

Bin 730: 6 of cap free
Amount of items: 3
Items: 
Size: 764575 Color: 1
Size: 120325 Color: 0
Size: 115095 Color: 0

Bin 731: 6 of cap free
Amount of items: 3
Items: 
Size: 766016 Color: 1
Size: 121211 Color: 1
Size: 112768 Color: 0

Bin 732: 6 of cap free
Amount of items: 2
Items: 
Size: 768601 Color: 1
Size: 231394 Color: 0

Bin 733: 6 of cap free
Amount of items: 3
Items: 
Size: 781659 Color: 0
Size: 109211 Color: 1
Size: 109125 Color: 1

Bin 734: 6 of cap free
Amount of items: 3
Items: 
Size: 786126 Color: 0
Size: 107806 Color: 0
Size: 106063 Color: 1

Bin 735: 6 of cap free
Amount of items: 3
Items: 
Size: 787497 Color: 0
Size: 112201 Color: 0
Size: 100297 Color: 1

Bin 736: 6 of cap free
Amount of items: 3
Items: 
Size: 788628 Color: 0
Size: 108494 Color: 1
Size: 102873 Color: 1

Bin 737: 6 of cap free
Amount of items: 2
Items: 
Size: 791702 Color: 0
Size: 208293 Color: 1

Bin 738: 6 of cap free
Amount of items: 3
Items: 
Size: 793799 Color: 1
Size: 104447 Color: 0
Size: 101749 Color: 0

Bin 739: 7 of cap free
Amount of items: 5
Items: 
Size: 226064 Color: 1
Size: 222591 Color: 0
Size: 219905 Color: 0
Size: 169584 Color: 1
Size: 161850 Color: 0

Bin 740: 7 of cap free
Amount of items: 3
Items: 
Size: 455497 Color: 0
Size: 354113 Color: 1
Size: 190384 Color: 1

Bin 741: 7 of cap free
Amount of items: 3
Items: 
Size: 455913 Color: 0
Size: 350155 Color: 1
Size: 193926 Color: 1

Bin 742: 7 of cap free
Amount of items: 2
Items: 
Size: 500779 Color: 1
Size: 499215 Color: 0

Bin 743: 7 of cap free
Amount of items: 2
Items: 
Size: 501229 Color: 1
Size: 498765 Color: 0

Bin 744: 7 of cap free
Amount of items: 2
Items: 
Size: 510364 Color: 0
Size: 489630 Color: 1

Bin 745: 7 of cap free
Amount of items: 3
Items: 
Size: 514177 Color: 0
Size: 353479 Color: 1
Size: 132338 Color: 0

Bin 746: 7 of cap free
Amount of items: 2
Items: 
Size: 519973 Color: 1
Size: 480021 Color: 0

Bin 747: 7 of cap free
Amount of items: 2
Items: 
Size: 520579 Color: 0
Size: 479415 Color: 1

Bin 748: 7 of cap free
Amount of items: 2
Items: 
Size: 524596 Color: 1
Size: 475398 Color: 0

Bin 749: 7 of cap free
Amount of items: 3
Items: 
Size: 550047 Color: 1
Size: 255807 Color: 0
Size: 194140 Color: 0

Bin 750: 7 of cap free
Amount of items: 2
Items: 
Size: 577687 Color: 1
Size: 422307 Color: 0

Bin 751: 7 of cap free
Amount of items: 2
Items: 
Size: 585230 Color: 1
Size: 414764 Color: 0

Bin 752: 7 of cap free
Amount of items: 2
Items: 
Size: 593509 Color: 0
Size: 406485 Color: 1

Bin 753: 7 of cap free
Amount of items: 3
Items: 
Size: 595436 Color: 0
Size: 272973 Color: 1
Size: 131585 Color: 1

Bin 754: 7 of cap free
Amount of items: 2
Items: 
Size: 599804 Color: 1
Size: 400190 Color: 0

Bin 755: 7 of cap free
Amount of items: 2
Items: 
Size: 602316 Color: 1
Size: 397678 Color: 0

Bin 756: 7 of cap free
Amount of items: 2
Items: 
Size: 612040 Color: 1
Size: 387954 Color: 0

Bin 757: 7 of cap free
Amount of items: 3
Items: 
Size: 620486 Color: 0
Size: 207024 Color: 1
Size: 172484 Color: 1

Bin 758: 7 of cap free
Amount of items: 2
Items: 
Size: 630194 Color: 1
Size: 369800 Color: 0

Bin 759: 7 of cap free
Amount of items: 2
Items: 
Size: 632610 Color: 0
Size: 367384 Color: 1

Bin 760: 7 of cap free
Amount of items: 2
Items: 
Size: 640320 Color: 1
Size: 359674 Color: 0

Bin 761: 7 of cap free
Amount of items: 2
Items: 
Size: 647560 Color: 1
Size: 352434 Color: 0

Bin 762: 7 of cap free
Amount of items: 2
Items: 
Size: 673726 Color: 0
Size: 326268 Color: 1

Bin 763: 7 of cap free
Amount of items: 2
Items: 
Size: 683107 Color: 0
Size: 316887 Color: 1

Bin 764: 7 of cap free
Amount of items: 2
Items: 
Size: 693913 Color: 1
Size: 306081 Color: 0

Bin 765: 7 of cap free
Amount of items: 2
Items: 
Size: 697014 Color: 1
Size: 302980 Color: 0

Bin 766: 7 of cap free
Amount of items: 2
Items: 
Size: 701022 Color: 0
Size: 298972 Color: 1

Bin 767: 7 of cap free
Amount of items: 2
Items: 
Size: 714521 Color: 1
Size: 285473 Color: 0

Bin 768: 7 of cap free
Amount of items: 2
Items: 
Size: 722696 Color: 0
Size: 277298 Color: 1

Bin 769: 7 of cap free
Amount of items: 2
Items: 
Size: 753285 Color: 0
Size: 246709 Color: 1

Bin 770: 7 of cap free
Amount of items: 2
Items: 
Size: 761118 Color: 1
Size: 238876 Color: 0

Bin 771: 7 of cap free
Amount of items: 3
Items: 
Size: 764265 Color: 1
Size: 119474 Color: 1
Size: 116255 Color: 0

Bin 772: 7 of cap free
Amount of items: 2
Items: 
Size: 780351 Color: 1
Size: 219643 Color: 0

Bin 773: 7 of cap free
Amount of items: 3
Items: 
Size: 781038 Color: 0
Size: 111106 Color: 1
Size: 107850 Color: 1

Bin 774: 7 of cap free
Amount of items: 3
Items: 
Size: 781130 Color: 1
Size: 110784 Color: 0
Size: 108080 Color: 1

Bin 775: 7 of cap free
Amount of items: 3
Items: 
Size: 787461 Color: 0
Size: 106854 Color: 0
Size: 105679 Color: 1

Bin 776: 7 of cap free
Amount of items: 2
Items: 
Size: 795747 Color: 1
Size: 204247 Color: 0

Bin 777: 7 of cap free
Amount of items: 2
Items: 
Size: 795854 Color: 0
Size: 204140 Color: 1

Bin 778: 8 of cap free
Amount of items: 5
Items: 
Size: 273148 Color: 1
Size: 230459 Color: 0
Size: 229728 Color: 0
Size: 134093 Color: 0
Size: 132565 Color: 1

Bin 779: 8 of cap free
Amount of items: 3
Items: 
Size: 467454 Color: 1
Size: 345953 Color: 0
Size: 186586 Color: 0

Bin 780: 8 of cap free
Amount of items: 3
Items: 
Size: 471433 Color: 1
Size: 290117 Color: 0
Size: 238443 Color: 0

Bin 781: 8 of cap free
Amount of items: 3
Items: 
Size: 478825 Color: 0
Size: 359692 Color: 1
Size: 161476 Color: 1

Bin 782: 8 of cap free
Amount of items: 3
Items: 
Size: 478933 Color: 0
Size: 322216 Color: 1
Size: 198844 Color: 1

Bin 783: 8 of cap free
Amount of items: 2
Items: 
Size: 502066 Color: 1
Size: 497927 Color: 0

Bin 784: 8 of cap free
Amount of items: 3
Items: 
Size: 515342 Color: 0
Size: 306161 Color: 1
Size: 178490 Color: 0

Bin 785: 8 of cap free
Amount of items: 2
Items: 
Size: 536725 Color: 1
Size: 463268 Color: 0

Bin 786: 8 of cap free
Amount of items: 3
Items: 
Size: 544178 Color: 0
Size: 322120 Color: 0
Size: 133695 Color: 1

Bin 787: 8 of cap free
Amount of items: 2
Items: 
Size: 593584 Color: 0
Size: 406409 Color: 1

Bin 788: 8 of cap free
Amount of items: 2
Items: 
Size: 596243 Color: 0
Size: 403750 Color: 1

Bin 789: 8 of cap free
Amount of items: 2
Items: 
Size: 611893 Color: 1
Size: 388100 Color: 0

Bin 790: 8 of cap free
Amount of items: 2
Items: 
Size: 618596 Color: 0
Size: 381397 Color: 1

Bin 791: 8 of cap free
Amount of items: 2
Items: 
Size: 620741 Color: 0
Size: 379252 Color: 1

Bin 792: 8 of cap free
Amount of items: 2
Items: 
Size: 629157 Color: 0
Size: 370836 Color: 1

Bin 793: 8 of cap free
Amount of items: 2
Items: 
Size: 655855 Color: 0
Size: 344138 Color: 1

Bin 794: 8 of cap free
Amount of items: 2
Items: 
Size: 671465 Color: 1
Size: 328528 Color: 0

Bin 795: 8 of cap free
Amount of items: 2
Items: 
Size: 683975 Color: 1
Size: 316018 Color: 0

Bin 796: 8 of cap free
Amount of items: 2
Items: 
Size: 695455 Color: 1
Size: 304538 Color: 0

Bin 797: 8 of cap free
Amount of items: 2
Items: 
Size: 699978 Color: 0
Size: 300015 Color: 1

Bin 798: 8 of cap free
Amount of items: 2
Items: 
Size: 736080 Color: 0
Size: 263913 Color: 1

Bin 799: 8 of cap free
Amount of items: 2
Items: 
Size: 752005 Color: 1
Size: 247988 Color: 0

Bin 800: 8 of cap free
Amount of items: 3
Items: 
Size: 761854 Color: 0
Size: 123829 Color: 1
Size: 114310 Color: 1

Bin 801: 8 of cap free
Amount of items: 3
Items: 
Size: 764291 Color: 1
Size: 122299 Color: 0
Size: 113403 Color: 0

Bin 802: 8 of cap free
Amount of items: 2
Items: 
Size: 770125 Color: 1
Size: 229868 Color: 0

Bin 803: 8 of cap free
Amount of items: 2
Items: 
Size: 781304 Color: 1
Size: 218689 Color: 0

Bin 804: 8 of cap free
Amount of items: 3
Items: 
Size: 788066 Color: 1
Size: 110324 Color: 0
Size: 101603 Color: 1

Bin 805: 8 of cap free
Amount of items: 2
Items: 
Size: 795329 Color: 0
Size: 204664 Color: 1

Bin 806: 9 of cap free
Amount of items: 3
Items: 
Size: 414016 Color: 0
Size: 321643 Color: 0
Size: 264333 Color: 1

Bin 807: 9 of cap free
Amount of items: 3
Items: 
Size: 427601 Color: 1
Size: 382491 Color: 0
Size: 189900 Color: 1

Bin 808: 9 of cap free
Amount of items: 3
Items: 
Size: 429598 Color: 1
Size: 382746 Color: 0
Size: 187648 Color: 1

Bin 809: 9 of cap free
Amount of items: 3
Items: 
Size: 445673 Color: 1
Size: 355046 Color: 1
Size: 199273 Color: 0

Bin 810: 9 of cap free
Amount of items: 3
Items: 
Size: 462090 Color: 1
Size: 362770 Color: 0
Size: 175132 Color: 0

Bin 811: 9 of cap free
Amount of items: 3
Items: 
Size: 466969 Color: 1
Size: 346358 Color: 0
Size: 186665 Color: 0

Bin 812: 9 of cap free
Amount of items: 3
Items: 
Size: 477251 Color: 1
Size: 404190 Color: 0
Size: 118551 Color: 1

Bin 813: 9 of cap free
Amount of items: 3
Items: 
Size: 514558 Color: 1
Size: 321476 Color: 1
Size: 163958 Color: 0

Bin 814: 9 of cap free
Amount of items: 3
Items: 
Size: 515306 Color: 0
Size: 357475 Color: 1
Size: 127211 Color: 0

Bin 815: 9 of cap free
Amount of items: 2
Items: 
Size: 521110 Color: 1
Size: 478882 Color: 0

Bin 816: 9 of cap free
Amount of items: 3
Items: 
Size: 524636 Color: 1
Size: 308657 Color: 0
Size: 166699 Color: 0

Bin 817: 9 of cap free
Amount of items: 2
Items: 
Size: 525898 Color: 1
Size: 474094 Color: 0

Bin 818: 9 of cap free
Amount of items: 2
Items: 
Size: 531297 Color: 1
Size: 468695 Color: 0

Bin 819: 9 of cap free
Amount of items: 3
Items: 
Size: 540997 Color: 0
Size: 308751 Color: 0
Size: 150244 Color: 1

Bin 820: 9 of cap free
Amount of items: 2
Items: 
Size: 543903 Color: 1
Size: 456089 Color: 0

Bin 821: 9 of cap free
Amount of items: 2
Items: 
Size: 552515 Color: 1
Size: 447477 Color: 0

Bin 822: 9 of cap free
Amount of items: 2
Items: 
Size: 553121 Color: 0
Size: 446871 Color: 1

Bin 823: 9 of cap free
Amount of items: 2
Items: 
Size: 556328 Color: 0
Size: 443664 Color: 1

Bin 824: 9 of cap free
Amount of items: 2
Items: 
Size: 558977 Color: 0
Size: 441015 Color: 1

Bin 825: 9 of cap free
Amount of items: 2
Items: 
Size: 563791 Color: 1
Size: 436201 Color: 0

Bin 826: 9 of cap free
Amount of items: 3
Items: 
Size: 564401 Color: 1
Size: 291030 Color: 1
Size: 144561 Color: 0

Bin 827: 9 of cap free
Amount of items: 2
Items: 
Size: 583222 Color: 0
Size: 416770 Color: 1

Bin 828: 9 of cap free
Amount of items: 2
Items: 
Size: 583900 Color: 0
Size: 416092 Color: 1

Bin 829: 9 of cap free
Amount of items: 3
Items: 
Size: 609615 Color: 0
Size: 202578 Color: 1
Size: 187799 Color: 0

Bin 830: 9 of cap free
Amount of items: 2
Items: 
Size: 631111 Color: 0
Size: 368881 Color: 1

Bin 831: 9 of cap free
Amount of items: 2
Items: 
Size: 653640 Color: 0
Size: 346352 Color: 1

Bin 832: 9 of cap free
Amount of items: 2
Items: 
Size: 656055 Color: 0
Size: 343937 Color: 1

Bin 833: 9 of cap free
Amount of items: 2
Items: 
Size: 657440 Color: 1
Size: 342552 Color: 0

Bin 834: 9 of cap free
Amount of items: 2
Items: 
Size: 659788 Color: 0
Size: 340204 Color: 1

Bin 835: 9 of cap free
Amount of items: 2
Items: 
Size: 669903 Color: 1
Size: 330089 Color: 0

Bin 836: 9 of cap free
Amount of items: 2
Items: 
Size: 676876 Color: 0
Size: 323116 Color: 1

Bin 837: 9 of cap free
Amount of items: 2
Items: 
Size: 677888 Color: 1
Size: 322104 Color: 0

Bin 838: 9 of cap free
Amount of items: 2
Items: 
Size: 701156 Color: 0
Size: 298836 Color: 1

Bin 839: 9 of cap free
Amount of items: 2
Items: 
Size: 707638 Color: 1
Size: 292354 Color: 0

Bin 840: 9 of cap free
Amount of items: 2
Items: 
Size: 729983 Color: 1
Size: 270009 Color: 0

Bin 841: 9 of cap free
Amount of items: 2
Items: 
Size: 732858 Color: 1
Size: 267134 Color: 0

Bin 842: 9 of cap free
Amount of items: 3
Items: 
Size: 776599 Color: 1
Size: 112746 Color: 1
Size: 110647 Color: 0

Bin 843: 9 of cap free
Amount of items: 3
Items: 
Size: 781339 Color: 0
Size: 111559 Color: 1
Size: 107094 Color: 0

Bin 844: 9 of cap free
Amount of items: 3
Items: 
Size: 782283 Color: 0
Size: 112734 Color: 0
Size: 104975 Color: 1

Bin 845: 9 of cap free
Amount of items: 3
Items: 
Size: 786779 Color: 0
Size: 110563 Color: 0
Size: 102650 Color: 1

Bin 846: 9 of cap free
Amount of items: 3
Items: 
Size: 793298 Color: 0
Size: 105113 Color: 0
Size: 101581 Color: 1

Bin 847: 9 of cap free
Amount of items: 3
Items: 
Size: 793718 Color: 1
Size: 104498 Color: 1
Size: 101776 Color: 0

Bin 848: 10 of cap free
Amount of items: 2
Items: 
Size: 518219 Color: 0
Size: 481772 Color: 1

Bin 849: 10 of cap free
Amount of items: 2
Items: 
Size: 518916 Color: 1
Size: 481075 Color: 0

Bin 850: 10 of cap free
Amount of items: 2
Items: 
Size: 521374 Color: 1
Size: 478617 Color: 0

Bin 851: 10 of cap free
Amount of items: 3
Items: 
Size: 524453 Color: 1
Size: 289975 Color: 0
Size: 185563 Color: 0

Bin 852: 10 of cap free
Amount of items: 3
Items: 
Size: 544177 Color: 0
Size: 305934 Color: 1
Size: 149880 Color: 0

Bin 853: 10 of cap free
Amount of items: 2
Items: 
Size: 573275 Color: 0
Size: 426716 Color: 1

Bin 854: 10 of cap free
Amount of items: 2
Items: 
Size: 580998 Color: 1
Size: 418993 Color: 0

Bin 855: 10 of cap free
Amount of items: 2
Items: 
Size: 588547 Color: 1
Size: 411444 Color: 0

Bin 856: 10 of cap free
Amount of items: 2
Items: 
Size: 594574 Color: 1
Size: 405417 Color: 0

Bin 857: 10 of cap free
Amount of items: 2
Items: 
Size: 597703 Color: 1
Size: 402288 Color: 0

Bin 858: 10 of cap free
Amount of items: 2
Items: 
Size: 597819 Color: 0
Size: 402172 Color: 1

Bin 859: 10 of cap free
Amount of items: 2
Items: 
Size: 622171 Color: 0
Size: 377820 Color: 1

Bin 860: 10 of cap free
Amount of items: 2
Items: 
Size: 622456 Color: 1
Size: 377535 Color: 0

Bin 861: 10 of cap free
Amount of items: 2
Items: 
Size: 625012 Color: 0
Size: 374979 Color: 1

Bin 862: 10 of cap free
Amount of items: 2
Items: 
Size: 630701 Color: 0
Size: 369290 Color: 1

Bin 863: 10 of cap free
Amount of items: 2
Items: 
Size: 641809 Color: 1
Size: 358182 Color: 0

Bin 864: 10 of cap free
Amount of items: 2
Items: 
Size: 642745 Color: 0
Size: 357246 Color: 1

Bin 865: 10 of cap free
Amount of items: 2
Items: 
Size: 648285 Color: 1
Size: 351706 Color: 0

Bin 866: 10 of cap free
Amount of items: 2
Items: 
Size: 654996 Color: 1
Size: 344995 Color: 0

Bin 867: 10 of cap free
Amount of items: 2
Items: 
Size: 659860 Color: 1
Size: 340131 Color: 0

Bin 868: 10 of cap free
Amount of items: 2
Items: 
Size: 668238 Color: 0
Size: 331753 Color: 1

Bin 869: 10 of cap free
Amount of items: 2
Items: 
Size: 682499 Color: 1
Size: 317492 Color: 0

Bin 870: 10 of cap free
Amount of items: 2
Items: 
Size: 682772 Color: 1
Size: 317219 Color: 0

Bin 871: 10 of cap free
Amount of items: 2
Items: 
Size: 686515 Color: 0
Size: 313476 Color: 1

Bin 872: 10 of cap free
Amount of items: 2
Items: 
Size: 691217 Color: 1
Size: 308774 Color: 0

Bin 873: 10 of cap free
Amount of items: 2
Items: 
Size: 722936 Color: 0
Size: 277055 Color: 1

Bin 874: 10 of cap free
Amount of items: 2
Items: 
Size: 729840 Color: 1
Size: 270151 Color: 0

Bin 875: 10 of cap free
Amount of items: 2
Items: 
Size: 738167 Color: 1
Size: 261824 Color: 0

Bin 876: 10 of cap free
Amount of items: 2
Items: 
Size: 743108 Color: 0
Size: 256883 Color: 1

Bin 877: 10 of cap free
Amount of items: 2
Items: 
Size: 774098 Color: 1
Size: 225893 Color: 0

Bin 878: 10 of cap free
Amount of items: 3
Items: 
Size: 780655 Color: 0
Size: 110231 Color: 1
Size: 109105 Color: 1

Bin 879: 10 of cap free
Amount of items: 2
Items: 
Size: 781996 Color: 0
Size: 217995 Color: 1

Bin 880: 10 of cap free
Amount of items: 2
Items: 
Size: 783710 Color: 0
Size: 216281 Color: 1

Bin 881: 10 of cap free
Amount of items: 2
Items: 
Size: 784754 Color: 0
Size: 215237 Color: 1

Bin 882: 10 of cap free
Amount of items: 3
Items: 
Size: 790929 Color: 1
Size: 105565 Color: 0
Size: 103497 Color: 0

Bin 883: 11 of cap free
Amount of items: 3
Items: 
Size: 378708 Color: 0
Size: 348259 Color: 1
Size: 273023 Color: 1

Bin 884: 11 of cap free
Amount of items: 3
Items: 
Size: 477709 Color: 1
Size: 362180 Color: 0
Size: 160101 Color: 1

Bin 885: 11 of cap free
Amount of items: 2
Items: 
Size: 515224 Color: 0
Size: 484766 Color: 1

Bin 886: 11 of cap free
Amount of items: 2
Items: 
Size: 528180 Color: 1
Size: 471810 Color: 0

Bin 887: 11 of cap free
Amount of items: 3
Items: 
Size: 545506 Color: 0
Size: 258079 Color: 0
Size: 196405 Color: 1

Bin 888: 11 of cap free
Amount of items: 2
Items: 
Size: 551010 Color: 1
Size: 448980 Color: 0

Bin 889: 11 of cap free
Amount of items: 2
Items: 
Size: 555037 Color: 0
Size: 444953 Color: 1

Bin 890: 11 of cap free
Amount of items: 2
Items: 
Size: 559441 Color: 1
Size: 440549 Color: 0

Bin 891: 11 of cap free
Amount of items: 2
Items: 
Size: 563045 Color: 1
Size: 436945 Color: 0

Bin 892: 11 of cap free
Amount of items: 2
Items: 
Size: 564974 Color: 1
Size: 435016 Color: 0

Bin 893: 11 of cap free
Amount of items: 2
Items: 
Size: 567296 Color: 0
Size: 432694 Color: 1

Bin 894: 11 of cap free
Amount of items: 2
Items: 
Size: 570648 Color: 0
Size: 429342 Color: 1

Bin 895: 11 of cap free
Amount of items: 2
Items: 
Size: 588849 Color: 0
Size: 411141 Color: 1

Bin 896: 11 of cap free
Amount of items: 2
Items: 
Size: 609892 Color: 1
Size: 390098 Color: 0

Bin 897: 11 of cap free
Amount of items: 2
Items: 
Size: 658908 Color: 1
Size: 341082 Color: 0

Bin 898: 11 of cap free
Amount of items: 2
Items: 
Size: 672662 Color: 1
Size: 327328 Color: 0

Bin 899: 11 of cap free
Amount of items: 2
Items: 
Size: 732274 Color: 1
Size: 267716 Color: 0

Bin 900: 11 of cap free
Amount of items: 2
Items: 
Size: 743091 Color: 0
Size: 256899 Color: 1

Bin 901: 11 of cap free
Amount of items: 2
Items: 
Size: 785764 Color: 0
Size: 214226 Color: 1

Bin 902: 11 of cap free
Amount of items: 2
Items: 
Size: 792025 Color: 0
Size: 207965 Color: 1

Bin 903: 12 of cap free
Amount of items: 5
Items: 
Size: 233543 Color: 1
Size: 223725 Color: 0
Size: 223650 Color: 0
Size: 181064 Color: 1
Size: 138007 Color: 0

Bin 904: 12 of cap free
Amount of items: 2
Items: 
Size: 510567 Color: 1
Size: 489422 Color: 0

Bin 905: 12 of cap free
Amount of items: 3
Items: 
Size: 514674 Color: 1
Size: 348336 Color: 1
Size: 136979 Color: 0

Bin 906: 12 of cap free
Amount of items: 2
Items: 
Size: 516372 Color: 0
Size: 483617 Color: 1

Bin 907: 12 of cap free
Amount of items: 2
Items: 
Size: 529607 Color: 0
Size: 470382 Color: 1

Bin 908: 12 of cap free
Amount of items: 2
Items: 
Size: 532842 Color: 1
Size: 467147 Color: 0

Bin 909: 12 of cap free
Amount of items: 2
Items: 
Size: 537303 Color: 1
Size: 462686 Color: 0

Bin 910: 12 of cap free
Amount of items: 2
Items: 
Size: 550567 Color: 0
Size: 449422 Color: 1

Bin 911: 12 of cap free
Amount of items: 3
Items: 
Size: 564530 Color: 0
Size: 274107 Color: 1
Size: 161352 Color: 0

Bin 912: 12 of cap free
Amount of items: 2
Items: 
Size: 573642 Color: 1
Size: 426347 Color: 0

Bin 913: 12 of cap free
Amount of items: 3
Items: 
Size: 578354 Color: 1
Size: 260246 Color: 0
Size: 161389 Color: 0

Bin 914: 12 of cap free
Amount of items: 2
Items: 
Size: 599894 Color: 0
Size: 400095 Color: 1

Bin 915: 12 of cap free
Amount of items: 2
Items: 
Size: 606732 Color: 0
Size: 393257 Color: 1

Bin 916: 12 of cap free
Amount of items: 2
Items: 
Size: 613463 Color: 1
Size: 386526 Color: 0

Bin 917: 12 of cap free
Amount of items: 2
Items: 
Size: 632120 Color: 0
Size: 367869 Color: 1

Bin 918: 12 of cap free
Amount of items: 2
Items: 
Size: 652346 Color: 1
Size: 347643 Color: 0

Bin 919: 12 of cap free
Amount of items: 2
Items: 
Size: 665516 Color: 1
Size: 334473 Color: 0

Bin 920: 12 of cap free
Amount of items: 2
Items: 
Size: 674765 Color: 1
Size: 325224 Color: 0

Bin 921: 12 of cap free
Amount of items: 2
Items: 
Size: 679159 Color: 0
Size: 320830 Color: 1

Bin 922: 12 of cap free
Amount of items: 2
Items: 
Size: 725997 Color: 0
Size: 273992 Color: 1

Bin 923: 12 of cap free
Amount of items: 2
Items: 
Size: 740338 Color: 1
Size: 259651 Color: 0

Bin 924: 12 of cap free
Amount of items: 2
Items: 
Size: 745309 Color: 0
Size: 254680 Color: 1

Bin 925: 12 of cap free
Amount of items: 2
Items: 
Size: 754135 Color: 1
Size: 245854 Color: 0

Bin 926: 12 of cap free
Amount of items: 2
Items: 
Size: 763343 Color: 0
Size: 236646 Color: 1

Bin 927: 12 of cap free
Amount of items: 3
Items: 
Size: 763508 Color: 1
Size: 118679 Color: 0
Size: 117802 Color: 0

Bin 928: 12 of cap free
Amount of items: 3
Items: 
Size: 773993 Color: 0
Size: 113964 Color: 1
Size: 112032 Color: 0

Bin 929: 12 of cap free
Amount of items: 2
Items: 
Size: 777684 Color: 0
Size: 222305 Color: 1

Bin 930: 12 of cap free
Amount of items: 2
Items: 
Size: 780719 Color: 0
Size: 219270 Color: 1

Bin 931: 12 of cap free
Amount of items: 3
Items: 
Size: 787446 Color: 1
Size: 111490 Color: 0
Size: 101053 Color: 1

Bin 932: 13 of cap free
Amount of items: 3
Items: 
Size: 424902 Color: 0
Size: 418439 Color: 1
Size: 156647 Color: 1

Bin 933: 13 of cap free
Amount of items: 3
Items: 
Size: 455929 Color: 0
Size: 358470 Color: 1
Size: 185589 Color: 0

Bin 934: 13 of cap free
Amount of items: 3
Items: 
Size: 456195 Color: 0
Size: 408233 Color: 1
Size: 135560 Color: 1

Bin 935: 13 of cap free
Amount of items: 2
Items: 
Size: 503910 Color: 0
Size: 496078 Color: 1

Bin 936: 13 of cap free
Amount of items: 2
Items: 
Size: 509951 Color: 1
Size: 490037 Color: 0

Bin 937: 13 of cap free
Amount of items: 2
Items: 
Size: 523231 Color: 1
Size: 476757 Color: 0

Bin 938: 13 of cap free
Amount of items: 2
Items: 
Size: 537775 Color: 1
Size: 462213 Color: 0

Bin 939: 13 of cap free
Amount of items: 3
Items: 
Size: 538132 Color: 0
Size: 347164 Color: 1
Size: 114692 Color: 1

Bin 940: 13 of cap free
Amount of items: 3
Items: 
Size: 545468 Color: 1
Size: 312339 Color: 1
Size: 142181 Color: 0

Bin 941: 13 of cap free
Amount of items: 2
Items: 
Size: 554730 Color: 1
Size: 445258 Color: 0

Bin 942: 13 of cap free
Amount of items: 2
Items: 
Size: 562679 Color: 1
Size: 437309 Color: 0

Bin 943: 13 of cap free
Amount of items: 2
Items: 
Size: 565655 Color: 1
Size: 434333 Color: 0

Bin 944: 13 of cap free
Amount of items: 2
Items: 
Size: 581343 Color: 1
Size: 418645 Color: 0

Bin 945: 13 of cap free
Amount of items: 2
Items: 
Size: 582780 Color: 1
Size: 417208 Color: 0

Bin 946: 13 of cap free
Amount of items: 2
Items: 
Size: 589225 Color: 1
Size: 410763 Color: 0

Bin 947: 13 of cap free
Amount of items: 3
Items: 
Size: 596781 Color: 1
Size: 253992 Color: 0
Size: 149215 Color: 0

Bin 948: 13 of cap free
Amount of items: 2
Items: 
Size: 604716 Color: 1
Size: 395272 Color: 0

Bin 949: 13 of cap free
Amount of items: 3
Items: 
Size: 619951 Color: 0
Size: 196328 Color: 1
Size: 183709 Color: 1

Bin 950: 13 of cap free
Amount of items: 2
Items: 
Size: 643441 Color: 1
Size: 356547 Color: 0

Bin 951: 13 of cap free
Amount of items: 2
Items: 
Size: 643611 Color: 0
Size: 356377 Color: 1

Bin 952: 13 of cap free
Amount of items: 2
Items: 
Size: 649443 Color: 1
Size: 350545 Color: 0

Bin 953: 13 of cap free
Amount of items: 2
Items: 
Size: 663243 Color: 1
Size: 336745 Color: 0

Bin 954: 13 of cap free
Amount of items: 2
Items: 
Size: 674161 Color: 1
Size: 325827 Color: 0

Bin 955: 13 of cap free
Amount of items: 2
Items: 
Size: 678608 Color: 1
Size: 321380 Color: 0

Bin 956: 13 of cap free
Amount of items: 2
Items: 
Size: 687975 Color: 1
Size: 312013 Color: 0

Bin 957: 13 of cap free
Amount of items: 2
Items: 
Size: 693595 Color: 1
Size: 306393 Color: 0

Bin 958: 13 of cap free
Amount of items: 2
Items: 
Size: 694021 Color: 0
Size: 305967 Color: 1

Bin 959: 13 of cap free
Amount of items: 2
Items: 
Size: 716596 Color: 0
Size: 283392 Color: 1

Bin 960: 13 of cap free
Amount of items: 2
Items: 
Size: 721769 Color: 0
Size: 278219 Color: 1

Bin 961: 13 of cap free
Amount of items: 2
Items: 
Size: 724179 Color: 1
Size: 275809 Color: 0

Bin 962: 13 of cap free
Amount of items: 2
Items: 
Size: 731507 Color: 0
Size: 268481 Color: 1

Bin 963: 13 of cap free
Amount of items: 2
Items: 
Size: 736362 Color: 0
Size: 263626 Color: 1

Bin 964: 13 of cap free
Amount of items: 2
Items: 
Size: 739618 Color: 0
Size: 260370 Color: 1

Bin 965: 13 of cap free
Amount of items: 2
Items: 
Size: 745854 Color: 0
Size: 254134 Color: 1

Bin 966: 13 of cap free
Amount of items: 2
Items: 
Size: 750916 Color: 1
Size: 249072 Color: 0

Bin 967: 13 of cap free
Amount of items: 2
Items: 
Size: 756036 Color: 1
Size: 243952 Color: 0

Bin 968: 13 of cap free
Amount of items: 2
Items: 
Size: 766694 Color: 1
Size: 233294 Color: 0

Bin 969: 13 of cap free
Amount of items: 2
Items: 
Size: 772467 Color: 0
Size: 227521 Color: 1

Bin 970: 13 of cap free
Amount of items: 2
Items: 
Size: 797889 Color: 1
Size: 202099 Color: 0

Bin 971: 14 of cap free
Amount of items: 3
Items: 
Size: 378232 Color: 0
Size: 357902 Color: 1
Size: 263853 Color: 1

Bin 972: 14 of cap free
Amount of items: 2
Items: 
Size: 506558 Color: 1
Size: 493429 Color: 0

Bin 973: 14 of cap free
Amount of items: 2
Items: 
Size: 513453 Color: 0
Size: 486534 Color: 1

Bin 974: 14 of cap free
Amount of items: 3
Items: 
Size: 524123 Color: 1
Size: 305950 Color: 1
Size: 169914 Color: 0

Bin 975: 14 of cap free
Amount of items: 2
Items: 
Size: 531417 Color: 1
Size: 468570 Color: 0

Bin 976: 14 of cap free
Amount of items: 3
Items: 
Size: 534396 Color: 1
Size: 274135 Color: 1
Size: 191456 Color: 0

Bin 977: 14 of cap free
Amount of items: 2
Items: 
Size: 539804 Color: 1
Size: 460183 Color: 0

Bin 978: 14 of cap free
Amount of items: 2
Items: 
Size: 543704 Color: 1
Size: 456283 Color: 0

Bin 979: 14 of cap free
Amount of items: 2
Items: 
Size: 593614 Color: 1
Size: 406373 Color: 0

Bin 980: 14 of cap free
Amount of items: 2
Items: 
Size: 612657 Color: 1
Size: 387330 Color: 0

Bin 981: 14 of cap free
Amount of items: 2
Items: 
Size: 622667 Color: 0
Size: 377320 Color: 1

Bin 982: 14 of cap free
Amount of items: 2
Items: 
Size: 626755 Color: 1
Size: 373232 Color: 0

Bin 983: 14 of cap free
Amount of items: 2
Items: 
Size: 664307 Color: 0
Size: 335680 Color: 1

Bin 984: 14 of cap free
Amount of items: 2
Items: 
Size: 678945 Color: 1
Size: 321042 Color: 0

Bin 985: 14 of cap free
Amount of items: 2
Items: 
Size: 686983 Color: 1
Size: 313004 Color: 0

Bin 986: 14 of cap free
Amount of items: 2
Items: 
Size: 695096 Color: 0
Size: 304891 Color: 1

Bin 987: 14 of cap free
Amount of items: 2
Items: 
Size: 702374 Color: 1
Size: 297613 Color: 0

Bin 988: 14 of cap free
Amount of items: 2
Items: 
Size: 702690 Color: 1
Size: 297297 Color: 0

Bin 989: 14 of cap free
Amount of items: 2
Items: 
Size: 709654 Color: 1
Size: 290333 Color: 0

Bin 990: 14 of cap free
Amount of items: 2
Items: 
Size: 723884 Color: 0
Size: 276103 Color: 1

Bin 991: 14 of cap free
Amount of items: 2
Items: 
Size: 724299 Color: 0
Size: 275688 Color: 1

Bin 992: 14 of cap free
Amount of items: 2
Items: 
Size: 728234 Color: 1
Size: 271753 Color: 0

Bin 993: 14 of cap free
Amount of items: 2
Items: 
Size: 742884 Color: 1
Size: 257103 Color: 0

Bin 994: 14 of cap free
Amount of items: 2
Items: 
Size: 744641 Color: 0
Size: 255346 Color: 1

Bin 995: 14 of cap free
Amount of items: 2
Items: 
Size: 767695 Color: 1
Size: 232292 Color: 0

Bin 996: 14 of cap free
Amount of items: 2
Items: 
Size: 767751 Color: 1
Size: 232236 Color: 0

Bin 997: 14 of cap free
Amount of items: 2
Items: 
Size: 787907 Color: 0
Size: 212080 Color: 1

Bin 998: 14 of cap free
Amount of items: 3
Items: 
Size: 789198 Color: 1
Size: 110180 Color: 0
Size: 100609 Color: 1

Bin 999: 14 of cap free
Amount of items: 2
Items: 
Size: 795745 Color: 1
Size: 204242 Color: 0

Bin 1000: 15 of cap free
Amount of items: 3
Items: 
Size: 446549 Color: 1
Size: 384586 Color: 0
Size: 168851 Color: 1

Bin 1001: 15 of cap free
Amount of items: 3
Items: 
Size: 479462 Color: 0
Size: 324679 Color: 1
Size: 195845 Color: 0

Bin 1002: 15 of cap free
Amount of items: 2
Items: 
Size: 515632 Color: 0
Size: 484354 Color: 1

Bin 1003: 15 of cap free
Amount of items: 2
Items: 
Size: 531746 Color: 1
Size: 468240 Color: 0

Bin 1004: 15 of cap free
Amount of items: 2
Items: 
Size: 557829 Color: 1
Size: 442157 Color: 0

Bin 1005: 15 of cap free
Amount of items: 2
Items: 
Size: 565395 Color: 1
Size: 434591 Color: 0

Bin 1006: 15 of cap free
Amount of items: 2
Items: 
Size: 566001 Color: 0
Size: 433985 Color: 1

Bin 1007: 15 of cap free
Amount of items: 2
Items: 
Size: 569389 Color: 0
Size: 430597 Color: 1

Bin 1008: 15 of cap free
Amount of items: 2
Items: 
Size: 571585 Color: 0
Size: 428401 Color: 1

Bin 1009: 15 of cap free
Amount of items: 2
Items: 
Size: 583434 Color: 1
Size: 416552 Color: 0

Bin 1010: 15 of cap free
Amount of items: 2
Items: 
Size: 621020 Color: 0
Size: 378966 Color: 1

Bin 1011: 15 of cap free
Amount of items: 2
Items: 
Size: 625775 Color: 0
Size: 374211 Color: 1

Bin 1012: 15 of cap free
Amount of items: 2
Items: 
Size: 635862 Color: 1
Size: 364124 Color: 0

Bin 1013: 15 of cap free
Amount of items: 2
Items: 
Size: 645166 Color: 1
Size: 354820 Color: 0

Bin 1014: 15 of cap free
Amount of items: 2
Items: 
Size: 653303 Color: 0
Size: 346683 Color: 1

Bin 1015: 15 of cap free
Amount of items: 2
Items: 
Size: 654590 Color: 1
Size: 345396 Color: 0

Bin 1016: 15 of cap free
Amount of items: 2
Items: 
Size: 664812 Color: 0
Size: 335174 Color: 1

Bin 1017: 15 of cap free
Amount of items: 2
Items: 
Size: 675781 Color: 0
Size: 324205 Color: 1

Bin 1018: 15 of cap free
Amount of items: 2
Items: 
Size: 687890 Color: 0
Size: 312096 Color: 1

Bin 1019: 15 of cap free
Amount of items: 2
Items: 
Size: 708216 Color: 0
Size: 291770 Color: 1

Bin 1020: 15 of cap free
Amount of items: 2
Items: 
Size: 739861 Color: 0
Size: 260125 Color: 1

Bin 1021: 15 of cap free
Amount of items: 3
Items: 
Size: 761778 Color: 0
Size: 123929 Color: 1
Size: 114279 Color: 0

Bin 1022: 15 of cap free
Amount of items: 3
Items: 
Size: 781685 Color: 0
Size: 113256 Color: 1
Size: 105045 Color: 0

Bin 1023: 15 of cap free
Amount of items: 2
Items: 
Size: 785355 Color: 1
Size: 214631 Color: 0

Bin 1024: 15 of cap free
Amount of items: 3
Items: 
Size: 786627 Color: 0
Size: 109239 Color: 1
Size: 104120 Color: 1

Bin 1025: 15 of cap free
Amount of items: 3
Items: 
Size: 788028 Color: 0
Size: 106573 Color: 0
Size: 105385 Color: 1

Bin 1026: 16 of cap free
Amount of items: 3
Items: 
Size: 417975 Color: 1
Size: 394245 Color: 1
Size: 187765 Color: 0

Bin 1027: 16 of cap free
Amount of items: 3
Items: 
Size: 446446 Color: 1
Size: 401166 Color: 0
Size: 152373 Color: 0

Bin 1028: 16 of cap free
Amount of items: 2
Items: 
Size: 501800 Color: 1
Size: 498185 Color: 0

Bin 1029: 16 of cap free
Amount of items: 2
Items: 
Size: 507943 Color: 1
Size: 492042 Color: 0

Bin 1030: 16 of cap free
Amount of items: 2
Items: 
Size: 533853 Color: 1
Size: 466132 Color: 0

Bin 1031: 16 of cap free
Amount of items: 2
Items: 
Size: 543532 Color: 1
Size: 456453 Color: 0

Bin 1032: 16 of cap free
Amount of items: 2
Items: 
Size: 546032 Color: 0
Size: 453953 Color: 1

Bin 1033: 16 of cap free
Amount of items: 2
Items: 
Size: 546157 Color: 0
Size: 453828 Color: 1

Bin 1034: 16 of cap free
Amount of items: 2
Items: 
Size: 570060 Color: 0
Size: 429925 Color: 1

Bin 1035: 16 of cap free
Amount of items: 2
Items: 
Size: 592745 Color: 1
Size: 407240 Color: 0

Bin 1036: 16 of cap free
Amount of items: 2
Items: 
Size: 593996 Color: 1
Size: 405989 Color: 0

Bin 1037: 16 of cap free
Amount of items: 2
Items: 
Size: 598536 Color: 0
Size: 401449 Color: 1

Bin 1038: 16 of cap free
Amount of items: 2
Items: 
Size: 602751 Color: 0
Size: 397234 Color: 1

Bin 1039: 16 of cap free
Amount of items: 2
Items: 
Size: 609051 Color: 0
Size: 390934 Color: 1

Bin 1040: 16 of cap free
Amount of items: 2
Items: 
Size: 617607 Color: 0
Size: 382378 Color: 1

Bin 1041: 16 of cap free
Amount of items: 2
Items: 
Size: 633263 Color: 0
Size: 366722 Color: 1

Bin 1042: 16 of cap free
Amount of items: 2
Items: 
Size: 635286 Color: 1
Size: 364699 Color: 0

Bin 1043: 16 of cap free
Amount of items: 2
Items: 
Size: 638105 Color: 0
Size: 361880 Color: 1

Bin 1044: 16 of cap free
Amount of items: 2
Items: 
Size: 664563 Color: 0
Size: 335422 Color: 1

Bin 1045: 16 of cap free
Amount of items: 2
Items: 
Size: 666837 Color: 0
Size: 333148 Color: 1

Bin 1046: 16 of cap free
Amount of items: 3
Items: 
Size: 733896 Color: 0
Size: 143507 Color: 1
Size: 122582 Color: 1

Bin 1047: 16 of cap free
Amount of items: 2
Items: 
Size: 763546 Color: 0
Size: 236439 Color: 1

Bin 1048: 16 of cap free
Amount of items: 2
Items: 
Size: 764272 Color: 0
Size: 235713 Color: 1

Bin 1049: 16 of cap free
Amount of items: 2
Items: 
Size: 773133 Color: 1
Size: 226852 Color: 0

Bin 1050: 16 of cap free
Amount of items: 2
Items: 
Size: 775965 Color: 0
Size: 224020 Color: 1

Bin 1051: 16 of cap free
Amount of items: 3
Items: 
Size: 781363 Color: 0
Size: 113826 Color: 1
Size: 104796 Color: 1

Bin 1052: 16 of cap free
Amount of items: 2
Items: 
Size: 781575 Color: 0
Size: 218410 Color: 1

Bin 1053: 16 of cap free
Amount of items: 2
Items: 
Size: 781938 Color: 0
Size: 218047 Color: 1

Bin 1054: 16 of cap free
Amount of items: 3
Items: 
Size: 782947 Color: 1
Size: 113423 Color: 0
Size: 103615 Color: 1

Bin 1055: 17 of cap free
Amount of items: 2
Items: 
Size: 518769 Color: 0
Size: 481215 Color: 1

Bin 1056: 17 of cap free
Amount of items: 2
Items: 
Size: 519084 Color: 1
Size: 480900 Color: 0

Bin 1057: 17 of cap free
Amount of items: 2
Items: 
Size: 527033 Color: 0
Size: 472951 Color: 1

Bin 1058: 17 of cap free
Amount of items: 2
Items: 
Size: 532177 Color: 0
Size: 467807 Color: 1

Bin 1059: 17 of cap free
Amount of items: 2
Items: 
Size: 532697 Color: 0
Size: 467287 Color: 1

Bin 1060: 17 of cap free
Amount of items: 2
Items: 
Size: 538627 Color: 1
Size: 461357 Color: 0

Bin 1061: 17 of cap free
Amount of items: 2
Items: 
Size: 566764 Color: 0
Size: 433220 Color: 1

Bin 1062: 17 of cap free
Amount of items: 2
Items: 
Size: 572237 Color: 0
Size: 427747 Color: 1

Bin 1063: 17 of cap free
Amount of items: 2
Items: 
Size: 587887 Color: 0
Size: 412097 Color: 1

Bin 1064: 17 of cap free
Amount of items: 2
Items: 
Size: 606346 Color: 0
Size: 393638 Color: 1

Bin 1065: 17 of cap free
Amount of items: 2
Items: 
Size: 607141 Color: 0
Size: 392843 Color: 1

Bin 1066: 17 of cap free
Amount of items: 2
Items: 
Size: 608724 Color: 0
Size: 391260 Color: 1

Bin 1067: 17 of cap free
Amount of items: 2
Items: 
Size: 621463 Color: 1
Size: 378521 Color: 0

Bin 1068: 17 of cap free
Amount of items: 2
Items: 
Size: 642168 Color: 1
Size: 357816 Color: 0

Bin 1069: 17 of cap free
Amount of items: 2
Items: 
Size: 644793 Color: 1
Size: 355191 Color: 0

Bin 1070: 17 of cap free
Amount of items: 2
Items: 
Size: 653063 Color: 1
Size: 346921 Color: 0

Bin 1071: 17 of cap free
Amount of items: 2
Items: 
Size: 665459 Color: 1
Size: 334525 Color: 0

Bin 1072: 17 of cap free
Amount of items: 2
Items: 
Size: 668310 Color: 1
Size: 331674 Color: 0

Bin 1073: 17 of cap free
Amount of items: 2
Items: 
Size: 677456 Color: 1
Size: 322528 Color: 0

Bin 1074: 17 of cap free
Amount of items: 2
Items: 
Size: 683931 Color: 1
Size: 316053 Color: 0

Bin 1075: 17 of cap free
Amount of items: 2
Items: 
Size: 712635 Color: 0
Size: 287349 Color: 1

Bin 1076: 17 of cap free
Amount of items: 2
Items: 
Size: 716797 Color: 1
Size: 283187 Color: 0

Bin 1077: 17 of cap free
Amount of items: 2
Items: 
Size: 717025 Color: 0
Size: 282959 Color: 1

Bin 1078: 17 of cap free
Amount of items: 2
Items: 
Size: 722865 Color: 1
Size: 277119 Color: 0

Bin 1079: 17 of cap free
Amount of items: 2
Items: 
Size: 751125 Color: 1
Size: 248859 Color: 0

Bin 1080: 17 of cap free
Amount of items: 2
Items: 
Size: 760578 Color: 0
Size: 239406 Color: 1

Bin 1081: 17 of cap free
Amount of items: 2
Items: 
Size: 765779 Color: 1
Size: 234205 Color: 0

Bin 1082: 17 of cap free
Amount of items: 2
Items: 
Size: 784470 Color: 0
Size: 215514 Color: 1

Bin 1083: 17 of cap free
Amount of items: 2
Items: 
Size: 798663 Color: 1
Size: 201321 Color: 0

Bin 1084: 18 of cap free
Amount of items: 2
Items: 
Size: 508106 Color: 0
Size: 491877 Color: 1

Bin 1085: 18 of cap free
Amount of items: 2
Items: 
Size: 508222 Color: 1
Size: 491761 Color: 0

Bin 1086: 18 of cap free
Amount of items: 2
Items: 
Size: 509614 Color: 1
Size: 490369 Color: 0

Bin 1087: 18 of cap free
Amount of items: 2
Items: 
Size: 532834 Color: 0
Size: 467149 Color: 1

Bin 1088: 18 of cap free
Amount of items: 2
Items: 
Size: 534072 Color: 0
Size: 465911 Color: 1

Bin 1089: 18 of cap free
Amount of items: 2
Items: 
Size: 542078 Color: 1
Size: 457905 Color: 0

Bin 1090: 18 of cap free
Amount of items: 2
Items: 
Size: 555993 Color: 1
Size: 443990 Color: 0

Bin 1091: 18 of cap free
Amount of items: 2
Items: 
Size: 579070 Color: 0
Size: 420913 Color: 1

Bin 1092: 18 of cap free
Amount of items: 2
Items: 
Size: 602037 Color: 0
Size: 397946 Color: 1

Bin 1093: 18 of cap free
Amount of items: 2
Items: 
Size: 612722 Color: 0
Size: 387261 Color: 1

Bin 1094: 18 of cap free
Amount of items: 2
Items: 
Size: 614302 Color: 1
Size: 385681 Color: 0

Bin 1095: 18 of cap free
Amount of items: 2
Items: 
Size: 617220 Color: 1
Size: 382763 Color: 0

Bin 1096: 18 of cap free
Amount of items: 2
Items: 
Size: 647507 Color: 1
Size: 352476 Color: 0

Bin 1097: 18 of cap free
Amount of items: 2
Items: 
Size: 650619 Color: 1
Size: 349364 Color: 0

Bin 1098: 18 of cap free
Amount of items: 2
Items: 
Size: 658276 Color: 0
Size: 341707 Color: 1

Bin 1099: 18 of cap free
Amount of items: 2
Items: 
Size: 690300 Color: 1
Size: 309683 Color: 0

Bin 1100: 18 of cap free
Amount of items: 2
Items: 
Size: 713407 Color: 0
Size: 286576 Color: 1

Bin 1101: 18 of cap free
Amount of items: 2
Items: 
Size: 721606 Color: 1
Size: 278377 Color: 0

Bin 1102: 18 of cap free
Amount of items: 2
Items: 
Size: 743756 Color: 0
Size: 256227 Color: 1

Bin 1103: 18 of cap free
Amount of items: 2
Items: 
Size: 744898 Color: 0
Size: 255085 Color: 1

Bin 1104: 18 of cap free
Amount of items: 2
Items: 
Size: 755713 Color: 1
Size: 244270 Color: 0

Bin 1105: 18 of cap free
Amount of items: 2
Items: 
Size: 759438 Color: 1
Size: 240545 Color: 0

Bin 1106: 18 of cap free
Amount of items: 2
Items: 
Size: 775033 Color: 1
Size: 224950 Color: 0

Bin 1107: 18 of cap free
Amount of items: 2
Items: 
Size: 789106 Color: 1
Size: 210877 Color: 0

Bin 1108: 19 of cap free
Amount of items: 3
Items: 
Size: 393799 Color: 0
Size: 348327 Color: 1
Size: 257856 Color: 0

Bin 1109: 19 of cap free
Amount of items: 2
Items: 
Size: 504284 Color: 1
Size: 495698 Color: 0

Bin 1110: 19 of cap free
Amount of items: 2
Items: 
Size: 505773 Color: 0
Size: 494209 Color: 1

Bin 1111: 19 of cap free
Amount of items: 2
Items: 
Size: 517205 Color: 0
Size: 482777 Color: 1

Bin 1112: 19 of cap free
Amount of items: 3
Items: 
Size: 524494 Color: 1
Size: 321246 Color: 0
Size: 154242 Color: 0

Bin 1113: 19 of cap free
Amount of items: 2
Items: 
Size: 531028 Color: 1
Size: 468954 Color: 0

Bin 1114: 19 of cap free
Amount of items: 2
Items: 
Size: 546818 Color: 0
Size: 453164 Color: 1

Bin 1115: 19 of cap free
Amount of items: 2
Items: 
Size: 557365 Color: 0
Size: 442617 Color: 1

Bin 1116: 19 of cap free
Amount of items: 2
Items: 
Size: 573838 Color: 0
Size: 426144 Color: 1

Bin 1117: 19 of cap free
Amount of items: 2
Items: 
Size: 593290 Color: 1
Size: 406692 Color: 0

Bin 1118: 19 of cap free
Amount of items: 2
Items: 
Size: 594791 Color: 1
Size: 405191 Color: 0

Bin 1119: 19 of cap free
Amount of items: 2
Items: 
Size: 600639 Color: 1
Size: 399343 Color: 0

Bin 1120: 19 of cap free
Amount of items: 2
Items: 
Size: 601229 Color: 1
Size: 398753 Color: 0

Bin 1121: 19 of cap free
Amount of items: 2
Items: 
Size: 603242 Color: 0
Size: 396740 Color: 1

Bin 1122: 19 of cap free
Amount of items: 2
Items: 
Size: 621749 Color: 1
Size: 378233 Color: 0

Bin 1123: 19 of cap free
Amount of items: 2
Items: 
Size: 623733 Color: 1
Size: 376249 Color: 0

Bin 1124: 19 of cap free
Amount of items: 2
Items: 
Size: 632500 Color: 1
Size: 367482 Color: 0

Bin 1125: 19 of cap free
Amount of items: 2
Items: 
Size: 658187 Color: 0
Size: 341795 Color: 1

Bin 1126: 19 of cap free
Amount of items: 2
Items: 
Size: 659132 Color: 0
Size: 340850 Color: 1

Bin 1127: 19 of cap free
Amount of items: 2
Items: 
Size: 664040 Color: 0
Size: 335942 Color: 1

Bin 1128: 19 of cap free
Amount of items: 2
Items: 
Size: 670199 Color: 0
Size: 329783 Color: 1

Bin 1129: 19 of cap free
Amount of items: 2
Items: 
Size: 692580 Color: 0
Size: 307402 Color: 1

Bin 1130: 19 of cap free
Amount of items: 2
Items: 
Size: 703433 Color: 0
Size: 296549 Color: 1

Bin 1131: 19 of cap free
Amount of items: 2
Items: 
Size: 713297 Color: 0
Size: 286685 Color: 1

Bin 1132: 19 of cap free
Amount of items: 2
Items: 
Size: 724846 Color: 0
Size: 275136 Color: 1

Bin 1133: 19 of cap free
Amount of items: 2
Items: 
Size: 727441 Color: 0
Size: 272541 Color: 1

Bin 1134: 19 of cap free
Amount of items: 2
Items: 
Size: 759483 Color: 0
Size: 240499 Color: 1

Bin 1135: 19 of cap free
Amount of items: 3
Items: 
Size: 762548 Color: 0
Size: 122092 Color: 1
Size: 115342 Color: 1

Bin 1136: 19 of cap free
Amount of items: 2
Items: 
Size: 763343 Color: 0
Size: 236639 Color: 1

Bin 1137: 19 of cap free
Amount of items: 2
Items: 
Size: 768564 Color: 0
Size: 231418 Color: 1

Bin 1138: 19 of cap free
Amount of items: 2
Items: 
Size: 770869 Color: 1
Size: 229113 Color: 0

Bin 1139: 19 of cap free
Amount of items: 3
Items: 
Size: 790916 Color: 1
Size: 107022 Color: 0
Size: 102044 Color: 1

Bin 1140: 20 of cap free
Amount of items: 3
Items: 
Size: 413786 Color: 0
Size: 322199 Color: 1
Size: 263996 Color: 1

Bin 1141: 20 of cap free
Amount of items: 3
Items: 
Size: 428621 Color: 1
Size: 428415 Color: 0
Size: 142945 Color: 0

Bin 1142: 20 of cap free
Amount of items: 2
Items: 
Size: 504884 Color: 1
Size: 495097 Color: 0

Bin 1143: 20 of cap free
Amount of items: 2
Items: 
Size: 511094 Color: 1
Size: 488887 Color: 0

Bin 1144: 20 of cap free
Amount of items: 2
Items: 
Size: 516477 Color: 1
Size: 483504 Color: 0

Bin 1145: 20 of cap free
Amount of items: 2
Items: 
Size: 529154 Color: 1
Size: 470827 Color: 0

Bin 1146: 20 of cap free
Amount of items: 2
Items: 
Size: 530303 Color: 0
Size: 469678 Color: 1

Bin 1147: 20 of cap free
Amount of items: 2
Items: 
Size: 530455 Color: 1
Size: 469526 Color: 0

Bin 1148: 20 of cap free
Amount of items: 2
Items: 
Size: 574067 Color: 1
Size: 425914 Color: 0

Bin 1149: 20 of cap free
Amount of items: 2
Items: 
Size: 578261 Color: 0
Size: 421720 Color: 1

Bin 1150: 20 of cap free
Amount of items: 2
Items: 
Size: 584794 Color: 1
Size: 415187 Color: 0

Bin 1151: 20 of cap free
Amount of items: 2
Items: 
Size: 600715 Color: 1
Size: 399266 Color: 0

Bin 1152: 20 of cap free
Amount of items: 2
Items: 
Size: 602694 Color: 1
Size: 397287 Color: 0

Bin 1153: 20 of cap free
Amount of items: 2
Items: 
Size: 634549 Color: 0
Size: 365432 Color: 1

Bin 1154: 20 of cap free
Amount of items: 2
Items: 
Size: 638752 Color: 1
Size: 361229 Color: 0

Bin 1155: 20 of cap free
Amount of items: 2
Items: 
Size: 663617 Color: 0
Size: 336364 Color: 1

Bin 1156: 20 of cap free
Amount of items: 2
Items: 
Size: 677917 Color: 1
Size: 322064 Color: 0

Bin 1157: 20 of cap free
Amount of items: 2
Items: 
Size: 707271 Color: 0
Size: 292710 Color: 1

Bin 1158: 20 of cap free
Amount of items: 2
Items: 
Size: 708058 Color: 1
Size: 291923 Color: 0

Bin 1159: 20 of cap free
Amount of items: 2
Items: 
Size: 724237 Color: 1
Size: 275744 Color: 0

Bin 1160: 20 of cap free
Amount of items: 2
Items: 
Size: 726571 Color: 1
Size: 273410 Color: 0

Bin 1161: 20 of cap free
Amount of items: 2
Items: 
Size: 729855 Color: 1
Size: 270126 Color: 0

Bin 1162: 20 of cap free
Amount of items: 2
Items: 
Size: 733589 Color: 1
Size: 266392 Color: 0

Bin 1163: 20 of cap free
Amount of items: 2
Items: 
Size: 764003 Color: 1
Size: 235978 Color: 0

Bin 1164: 20 of cap free
Amount of items: 2
Items: 
Size: 778060 Color: 1
Size: 221921 Color: 0

Bin 1165: 20 of cap free
Amount of items: 3
Items: 
Size: 786563 Color: 0
Size: 111899 Color: 0
Size: 101519 Color: 1

Bin 1166: 21 of cap free
Amount of items: 3
Items: 
Size: 445056 Color: 1
Size: 392847 Color: 0
Size: 162077 Color: 0

Bin 1167: 21 of cap free
Amount of items: 2
Items: 
Size: 500521 Color: 0
Size: 499459 Color: 1

Bin 1168: 21 of cap free
Amount of items: 2
Items: 
Size: 520889 Color: 1
Size: 479091 Color: 0

Bin 1169: 21 of cap free
Amount of items: 2
Items: 
Size: 528532 Color: 1
Size: 471448 Color: 0

Bin 1170: 21 of cap free
Amount of items: 2
Items: 
Size: 540244 Color: 0
Size: 459736 Color: 1

Bin 1171: 21 of cap free
Amount of items: 2
Items: 
Size: 542626 Color: 0
Size: 457354 Color: 1

Bin 1172: 21 of cap free
Amount of items: 2
Items: 
Size: 542649 Color: 0
Size: 457331 Color: 1

Bin 1173: 21 of cap free
Amount of items: 2
Items: 
Size: 555524 Color: 0
Size: 444456 Color: 1

Bin 1174: 21 of cap free
Amount of items: 2
Items: 
Size: 568814 Color: 1
Size: 431166 Color: 0

Bin 1175: 21 of cap free
Amount of items: 2
Items: 
Size: 612267 Color: 1
Size: 387713 Color: 0

Bin 1176: 21 of cap free
Amount of items: 2
Items: 
Size: 655667 Color: 0
Size: 344313 Color: 1

Bin 1177: 21 of cap free
Amount of items: 2
Items: 
Size: 663605 Color: 1
Size: 336375 Color: 0

Bin 1178: 21 of cap free
Amount of items: 2
Items: 
Size: 680279 Color: 0
Size: 319701 Color: 1

Bin 1179: 21 of cap free
Amount of items: 2
Items: 
Size: 681046 Color: 0
Size: 318934 Color: 1

Bin 1180: 21 of cap free
Amount of items: 2
Items: 
Size: 684828 Color: 0
Size: 315152 Color: 1

Bin 1181: 21 of cap free
Amount of items: 2
Items: 
Size: 703151 Color: 1
Size: 296829 Color: 0

Bin 1182: 21 of cap free
Amount of items: 2
Items: 
Size: 707831 Color: 0
Size: 292149 Color: 1

Bin 1183: 21 of cap free
Amount of items: 2
Items: 
Size: 731218 Color: 1
Size: 268762 Color: 0

Bin 1184: 21 of cap free
Amount of items: 2
Items: 
Size: 731913 Color: 1
Size: 268067 Color: 0

Bin 1185: 21 of cap free
Amount of items: 2
Items: 
Size: 732477 Color: 1
Size: 267503 Color: 0

Bin 1186: 21 of cap free
Amount of items: 2
Items: 
Size: 737343 Color: 1
Size: 262637 Color: 0

Bin 1187: 21 of cap free
Amount of items: 2
Items: 
Size: 749260 Color: 0
Size: 250720 Color: 1

Bin 1188: 21 of cap free
Amount of items: 2
Items: 
Size: 749463 Color: 1
Size: 250517 Color: 0

Bin 1189: 21 of cap free
Amount of items: 2
Items: 
Size: 759645 Color: 1
Size: 240335 Color: 0

Bin 1190: 21 of cap free
Amount of items: 2
Items: 
Size: 760669 Color: 1
Size: 239311 Color: 0

Bin 1191: 22 of cap free
Amount of items: 2
Items: 
Size: 511473 Color: 1
Size: 488506 Color: 0

Bin 1192: 22 of cap free
Amount of items: 2
Items: 
Size: 518588 Color: 0
Size: 481391 Color: 1

Bin 1193: 22 of cap free
Amount of items: 2
Items: 
Size: 520409 Color: 1
Size: 479570 Color: 0

Bin 1194: 22 of cap free
Amount of items: 2
Items: 
Size: 521074 Color: 0
Size: 478905 Color: 1

Bin 1195: 22 of cap free
Amount of items: 2
Items: 
Size: 529553 Color: 0
Size: 470426 Color: 1

Bin 1196: 22 of cap free
Amount of items: 2
Items: 
Size: 542833 Color: 0
Size: 457146 Color: 1

Bin 1197: 22 of cap free
Amount of items: 2
Items: 
Size: 549446 Color: 0
Size: 450533 Color: 1

Bin 1198: 22 of cap free
Amount of items: 2
Items: 
Size: 554075 Color: 0
Size: 445904 Color: 1

Bin 1199: 22 of cap free
Amount of items: 2
Items: 
Size: 568516 Color: 1
Size: 431463 Color: 0

Bin 1200: 22 of cap free
Amount of items: 2
Items: 
Size: 570994 Color: 0
Size: 428985 Color: 1

Bin 1201: 22 of cap free
Amount of items: 2
Items: 
Size: 586382 Color: 1
Size: 413597 Color: 0

Bin 1202: 22 of cap free
Amount of items: 2
Items: 
Size: 601992 Color: 1
Size: 397987 Color: 0

Bin 1203: 22 of cap free
Amount of items: 2
Items: 
Size: 603508 Color: 0
Size: 396471 Color: 1

Bin 1204: 22 of cap free
Amount of items: 2
Items: 
Size: 607059 Color: 0
Size: 392920 Color: 1

Bin 1205: 22 of cap free
Amount of items: 2
Items: 
Size: 611960 Color: 1
Size: 388019 Color: 0

Bin 1206: 22 of cap free
Amount of items: 2
Items: 
Size: 625087 Color: 0
Size: 374892 Color: 1

Bin 1207: 22 of cap free
Amount of items: 2
Items: 
Size: 643176 Color: 0
Size: 356803 Color: 1

Bin 1208: 22 of cap free
Amount of items: 2
Items: 
Size: 651172 Color: 1
Size: 348807 Color: 0

Bin 1209: 22 of cap free
Amount of items: 2
Items: 
Size: 654962 Color: 0
Size: 345017 Color: 1

Bin 1210: 22 of cap free
Amount of items: 2
Items: 
Size: 679957 Color: 1
Size: 320022 Color: 0

Bin 1211: 22 of cap free
Amount of items: 2
Items: 
Size: 698100 Color: 1
Size: 301879 Color: 0

Bin 1212: 22 of cap free
Amount of items: 2
Items: 
Size: 721429 Color: 0
Size: 278550 Color: 1

Bin 1213: 22 of cap free
Amount of items: 2
Items: 
Size: 741940 Color: 0
Size: 258039 Color: 1

Bin 1214: 22 of cap free
Amount of items: 2
Items: 
Size: 761183 Color: 1
Size: 238796 Color: 0

Bin 1215: 22 of cap free
Amount of items: 2
Items: 
Size: 767190 Color: 1
Size: 232789 Color: 0

Bin 1216: 22 of cap free
Amount of items: 2
Items: 
Size: 775791 Color: 0
Size: 224188 Color: 1

Bin 1217: 22 of cap free
Amount of items: 3
Items: 
Size: 786786 Color: 0
Size: 109618 Color: 1
Size: 103575 Color: 1

Bin 1218: 22 of cap free
Amount of items: 2
Items: 
Size: 787676 Color: 0
Size: 212303 Color: 1

Bin 1219: 22 of cap free
Amount of items: 2
Items: 
Size: 791494 Color: 1
Size: 208485 Color: 0

Bin 1220: 22 of cap free
Amount of items: 2
Items: 
Size: 795816 Color: 0
Size: 204163 Color: 1

Bin 1221: 23 of cap free
Amount of items: 2
Items: 
Size: 500972 Color: 1
Size: 499006 Color: 0

Bin 1222: 23 of cap free
Amount of items: 2
Items: 
Size: 522954 Color: 1
Size: 477024 Color: 0

Bin 1223: 23 of cap free
Amount of items: 2
Items: 
Size: 540155 Color: 0
Size: 459823 Color: 1

Bin 1224: 23 of cap free
Amount of items: 2
Items: 
Size: 551503 Color: 1
Size: 448475 Color: 0

Bin 1225: 23 of cap free
Amount of items: 2
Items: 
Size: 563742 Color: 1
Size: 436236 Color: 0

Bin 1226: 23 of cap free
Amount of items: 2
Items: 
Size: 586583 Color: 1
Size: 413395 Color: 0

Bin 1227: 23 of cap free
Amount of items: 2
Items: 
Size: 594749 Color: 0
Size: 405229 Color: 1

Bin 1228: 23 of cap free
Amount of items: 2
Items: 
Size: 605615 Color: 1
Size: 394363 Color: 0

Bin 1229: 23 of cap free
Amount of items: 2
Items: 
Size: 647487 Color: 0
Size: 352491 Color: 1

Bin 1230: 23 of cap free
Amount of items: 2
Items: 
Size: 653093 Color: 1
Size: 346885 Color: 0

Bin 1231: 23 of cap free
Amount of items: 2
Items: 
Size: 678896 Color: 1
Size: 321082 Color: 0

Bin 1232: 23 of cap free
Amount of items: 2
Items: 
Size: 724815 Color: 0
Size: 275163 Color: 1

Bin 1233: 23 of cap free
Amount of items: 2
Items: 
Size: 725677 Color: 1
Size: 274301 Color: 0

Bin 1234: 23 of cap free
Amount of items: 2
Items: 
Size: 730650 Color: 1
Size: 269328 Color: 0

Bin 1235: 23 of cap free
Amount of items: 2
Items: 
Size: 740710 Color: 1
Size: 259268 Color: 0

Bin 1236: 23 of cap free
Amount of items: 2
Items: 
Size: 749704 Color: 0
Size: 250274 Color: 1

Bin 1237: 23 of cap free
Amount of items: 2
Items: 
Size: 767427 Color: 0
Size: 232551 Color: 1

Bin 1238: 23 of cap free
Amount of items: 2
Items: 
Size: 771954 Color: 0
Size: 228024 Color: 1

Bin 1239: 23 of cap free
Amount of items: 2
Items: 
Size: 778050 Color: 0
Size: 221928 Color: 1

Bin 1240: 24 of cap free
Amount of items: 2
Items: 
Size: 500910 Color: 1
Size: 499067 Color: 0

Bin 1241: 24 of cap free
Amount of items: 2
Items: 
Size: 510604 Color: 0
Size: 489373 Color: 1

Bin 1242: 24 of cap free
Amount of items: 2
Items: 
Size: 519520 Color: 1
Size: 480457 Color: 0

Bin 1243: 24 of cap free
Amount of items: 2
Items: 
Size: 520712 Color: 0
Size: 479265 Color: 1

Bin 1244: 24 of cap free
Amount of items: 2
Items: 
Size: 529115 Color: 1
Size: 470862 Color: 0

Bin 1245: 24 of cap free
Amount of items: 2
Items: 
Size: 531272 Color: 1
Size: 468705 Color: 0

Bin 1246: 24 of cap free
Amount of items: 2
Items: 
Size: 542130 Color: 0
Size: 457847 Color: 1

Bin 1247: 24 of cap free
Amount of items: 2
Items: 
Size: 550377 Color: 1
Size: 449600 Color: 0

Bin 1248: 24 of cap free
Amount of items: 2
Items: 
Size: 551342 Color: 1
Size: 448635 Color: 0

Bin 1249: 24 of cap free
Amount of items: 2
Items: 
Size: 551627 Color: 0
Size: 448350 Color: 1

Bin 1250: 24 of cap free
Amount of items: 2
Items: 
Size: 567927 Color: 1
Size: 432050 Color: 0

Bin 1251: 24 of cap free
Amount of items: 2
Items: 
Size: 568898 Color: 0
Size: 431079 Color: 1

Bin 1252: 24 of cap free
Amount of items: 2
Items: 
Size: 587236 Color: 0
Size: 412741 Color: 1

Bin 1253: 24 of cap free
Amount of items: 2
Items: 
Size: 588770 Color: 1
Size: 411207 Color: 0

Bin 1254: 24 of cap free
Amount of items: 2
Items: 
Size: 589480 Color: 1
Size: 410497 Color: 0

Bin 1255: 24 of cap free
Amount of items: 2
Items: 
Size: 616037 Color: 1
Size: 383940 Color: 0

Bin 1256: 24 of cap free
Amount of items: 2
Items: 
Size: 624772 Color: 1
Size: 375205 Color: 0

Bin 1257: 24 of cap free
Amount of items: 2
Items: 
Size: 630143 Color: 1
Size: 369834 Color: 0

Bin 1258: 24 of cap free
Amount of items: 2
Items: 
Size: 648323 Color: 1
Size: 351654 Color: 0

Bin 1259: 24 of cap free
Amount of items: 2
Items: 
Size: 651595 Color: 1
Size: 348382 Color: 0

Bin 1260: 24 of cap free
Amount of items: 2
Items: 
Size: 658807 Color: 1
Size: 341170 Color: 0

Bin 1261: 24 of cap free
Amount of items: 2
Items: 
Size: 672360 Color: 0
Size: 327617 Color: 1

Bin 1262: 24 of cap free
Amount of items: 2
Items: 
Size: 704098 Color: 0
Size: 295879 Color: 1

Bin 1263: 24 of cap free
Amount of items: 2
Items: 
Size: 737884 Color: 1
Size: 262093 Color: 0

Bin 1264: 24 of cap free
Amount of items: 2
Items: 
Size: 750913 Color: 1
Size: 249064 Color: 0

Bin 1265: 24 of cap free
Amount of items: 2
Items: 
Size: 769681 Color: 0
Size: 230296 Color: 1

Bin 1266: 24 of cap free
Amount of items: 2
Items: 
Size: 778288 Color: 1
Size: 221689 Color: 0

Bin 1267: 24 of cap free
Amount of items: 2
Items: 
Size: 794058 Color: 0
Size: 205919 Color: 1

Bin 1268: 24 of cap free
Amount of items: 2
Items: 
Size: 795377 Color: 0
Size: 204600 Color: 1

Bin 1269: 25 of cap free
Amount of items: 3
Items: 
Size: 421807 Color: 0
Size: 407274 Color: 1
Size: 170895 Color: 1

Bin 1270: 25 of cap free
Amount of items: 3
Items: 
Size: 476123 Color: 0
Size: 369150 Color: 1
Size: 154703 Color: 1

Bin 1271: 25 of cap free
Amount of items: 2
Items: 
Size: 501505 Color: 1
Size: 498471 Color: 0

Bin 1272: 25 of cap free
Amount of items: 2
Items: 
Size: 509745 Color: 0
Size: 490231 Color: 1

Bin 1273: 25 of cap free
Amount of items: 2
Items: 
Size: 517437 Color: 0
Size: 482539 Color: 1

Bin 1274: 25 of cap free
Amount of items: 2
Items: 
Size: 530408 Color: 1
Size: 469568 Color: 0

Bin 1275: 25 of cap free
Amount of items: 2
Items: 
Size: 540673 Color: 1
Size: 459303 Color: 0

Bin 1276: 25 of cap free
Amount of items: 2
Items: 
Size: 553837 Color: 1
Size: 446139 Color: 0

Bin 1277: 25 of cap free
Amount of items: 2
Items: 
Size: 559866 Color: 1
Size: 440110 Color: 0

Bin 1278: 25 of cap free
Amount of items: 2
Items: 
Size: 560439 Color: 1
Size: 439537 Color: 0

Bin 1279: 25 of cap free
Amount of items: 2
Items: 
Size: 565281 Color: 1
Size: 434695 Color: 0

Bin 1280: 25 of cap free
Amount of items: 2
Items: 
Size: 569073 Color: 1
Size: 430903 Color: 0

Bin 1281: 25 of cap free
Amount of items: 2
Items: 
Size: 572814 Color: 0
Size: 427162 Color: 1

Bin 1282: 25 of cap free
Amount of items: 2
Items: 
Size: 615696 Color: 1
Size: 384280 Color: 0

Bin 1283: 25 of cap free
Amount of items: 2
Items: 
Size: 631938 Color: 0
Size: 368038 Color: 1

Bin 1284: 25 of cap free
Amount of items: 2
Items: 
Size: 650628 Color: 0
Size: 349348 Color: 1

Bin 1285: 25 of cap free
Amount of items: 2
Items: 
Size: 669691 Color: 1
Size: 330285 Color: 0

Bin 1286: 25 of cap free
Amount of items: 2
Items: 
Size: 676290 Color: 1
Size: 323686 Color: 0

Bin 1287: 25 of cap free
Amount of items: 2
Items: 
Size: 681394 Color: 1
Size: 318582 Color: 0

Bin 1288: 25 of cap free
Amount of items: 2
Items: 
Size: 690523 Color: 1
Size: 309453 Color: 0

Bin 1289: 25 of cap free
Amount of items: 2
Items: 
Size: 704959 Color: 1
Size: 295017 Color: 0

Bin 1290: 25 of cap free
Amount of items: 2
Items: 
Size: 709035 Color: 0
Size: 290941 Color: 1

Bin 1291: 25 of cap free
Amount of items: 2
Items: 
Size: 729137 Color: 0
Size: 270839 Color: 1

Bin 1292: 25 of cap free
Amount of items: 2
Items: 
Size: 761388 Color: 0
Size: 238588 Color: 1

Bin 1293: 25 of cap free
Amount of items: 3
Items: 
Size: 782504 Color: 0
Size: 112214 Color: 1
Size: 105258 Color: 0

Bin 1294: 25 of cap free
Amount of items: 2
Items: 
Size: 784613 Color: 0
Size: 215363 Color: 1

Bin 1295: 25 of cap free
Amount of items: 2
Items: 
Size: 787614 Color: 1
Size: 212362 Color: 0

Bin 1296: 26 of cap free
Amount of items: 2
Items: 
Size: 517027 Color: 1
Size: 482948 Color: 0

Bin 1297: 26 of cap free
Amount of items: 2
Items: 
Size: 523880 Color: 0
Size: 476095 Color: 1

Bin 1298: 26 of cap free
Amount of items: 2
Items: 
Size: 525163 Color: 1
Size: 474812 Color: 0

Bin 1299: 26 of cap free
Amount of items: 2
Items: 
Size: 550020 Color: 1
Size: 449955 Color: 0

Bin 1300: 26 of cap free
Amount of items: 2
Items: 
Size: 551405 Color: 0
Size: 448570 Color: 1

Bin 1301: 26 of cap free
Amount of items: 2
Items: 
Size: 571822 Color: 1
Size: 428153 Color: 0

Bin 1302: 26 of cap free
Amount of items: 2
Items: 
Size: 580593 Color: 1
Size: 419382 Color: 0

Bin 1303: 26 of cap free
Amount of items: 2
Items: 
Size: 596509 Color: 0
Size: 403466 Color: 1

Bin 1304: 26 of cap free
Amount of items: 2
Items: 
Size: 611677 Color: 0
Size: 388298 Color: 1

Bin 1305: 26 of cap free
Amount of items: 2
Items: 
Size: 613567 Color: 1
Size: 386408 Color: 0

Bin 1306: 26 of cap free
Amount of items: 2
Items: 
Size: 617650 Color: 0
Size: 382325 Color: 1

Bin 1307: 26 of cap free
Amount of items: 2
Items: 
Size: 618165 Color: 0
Size: 381810 Color: 1

Bin 1308: 26 of cap free
Amount of items: 2
Items: 
Size: 628018 Color: 0
Size: 371957 Color: 1

Bin 1309: 26 of cap free
Amount of items: 2
Items: 
Size: 631422 Color: 1
Size: 368553 Color: 0

Bin 1310: 26 of cap free
Amount of items: 2
Items: 
Size: 641037 Color: 0
Size: 358938 Color: 1

Bin 1311: 26 of cap free
Amount of items: 2
Items: 
Size: 661887 Color: 1
Size: 338088 Color: 0

Bin 1312: 26 of cap free
Amount of items: 2
Items: 
Size: 676010 Color: 0
Size: 323965 Color: 1

Bin 1313: 26 of cap free
Amount of items: 2
Items: 
Size: 703700 Color: 1
Size: 296275 Color: 0

Bin 1314: 26 of cap free
Amount of items: 2
Items: 
Size: 728452 Color: 0
Size: 271523 Color: 1

Bin 1315: 26 of cap free
Amount of items: 2
Items: 
Size: 735988 Color: 1
Size: 263987 Color: 0

Bin 1316: 26 of cap free
Amount of items: 2
Items: 
Size: 738651 Color: 0
Size: 261324 Color: 1

Bin 1317: 26 of cap free
Amount of items: 2
Items: 
Size: 774193 Color: 1
Size: 225782 Color: 0

Bin 1318: 26 of cap free
Amount of items: 2
Items: 
Size: 788491 Color: 1
Size: 211484 Color: 0

Bin 1319: 27 of cap free
Amount of items: 2
Items: 
Size: 506385 Color: 1
Size: 493589 Color: 0

Bin 1320: 27 of cap free
Amount of items: 2
Items: 
Size: 516783 Color: 0
Size: 483191 Color: 1

Bin 1321: 27 of cap free
Amount of items: 2
Items: 
Size: 525203 Color: 0
Size: 474771 Color: 1

Bin 1322: 27 of cap free
Amount of items: 2
Items: 
Size: 547548 Color: 0
Size: 452426 Color: 1

Bin 1323: 27 of cap free
Amount of items: 2
Items: 
Size: 548214 Color: 1
Size: 451760 Color: 0

Bin 1324: 27 of cap free
Amount of items: 2
Items: 
Size: 550226 Color: 1
Size: 449748 Color: 0

Bin 1325: 27 of cap free
Amount of items: 2
Items: 
Size: 567341 Color: 1
Size: 432633 Color: 0

Bin 1326: 27 of cap free
Amount of items: 2
Items: 
Size: 587838 Color: 0
Size: 412136 Color: 1

Bin 1327: 27 of cap free
Amount of items: 2
Items: 
Size: 624874 Color: 1
Size: 375100 Color: 0

Bin 1328: 27 of cap free
Amount of items: 2
Items: 
Size: 635482 Color: 1
Size: 364492 Color: 0

Bin 1329: 27 of cap free
Amount of items: 2
Items: 
Size: 636907 Color: 0
Size: 363067 Color: 1

Bin 1330: 27 of cap free
Amount of items: 2
Items: 
Size: 662552 Color: 1
Size: 337422 Color: 0

Bin 1331: 27 of cap free
Amount of items: 2
Items: 
Size: 663043 Color: 1
Size: 336931 Color: 0

Bin 1332: 27 of cap free
Amount of items: 2
Items: 
Size: 666089 Color: 0
Size: 333885 Color: 1

Bin 1333: 27 of cap free
Amount of items: 2
Items: 
Size: 674349 Color: 1
Size: 325625 Color: 0

Bin 1334: 27 of cap free
Amount of items: 2
Items: 
Size: 682120 Color: 1
Size: 317854 Color: 0

Bin 1335: 27 of cap free
Amount of items: 2
Items: 
Size: 686924 Color: 1
Size: 313050 Color: 0

Bin 1336: 27 of cap free
Amount of items: 2
Items: 
Size: 688501 Color: 1
Size: 311473 Color: 0

Bin 1337: 27 of cap free
Amount of items: 2
Items: 
Size: 694846 Color: 0
Size: 305128 Color: 1

Bin 1338: 27 of cap free
Amount of items: 2
Items: 
Size: 709908 Color: 0
Size: 290066 Color: 1

Bin 1339: 27 of cap free
Amount of items: 2
Items: 
Size: 713785 Color: 1
Size: 286189 Color: 0

Bin 1340: 27 of cap free
Amount of items: 2
Items: 
Size: 727855 Color: 1
Size: 272119 Color: 0

Bin 1341: 27 of cap free
Amount of items: 2
Items: 
Size: 767364 Color: 1
Size: 232610 Color: 0

Bin 1342: 27 of cap free
Amount of items: 2
Items: 
Size: 770427 Color: 1
Size: 229547 Color: 0

Bin 1343: 27 of cap free
Amount of items: 2
Items: 
Size: 774781 Color: 0
Size: 225193 Color: 1

Bin 1344: 27 of cap free
Amount of items: 2
Items: 
Size: 776927 Color: 1
Size: 223047 Color: 0

Bin 1345: 27 of cap free
Amount of items: 3
Items: 
Size: 792730 Color: 1
Size: 103729 Color: 0
Size: 103515 Color: 0

Bin 1346: 27 of cap free
Amount of items: 2
Items: 
Size: 797472 Color: 0
Size: 202502 Color: 1

Bin 1347: 28 of cap free
Amount of items: 2
Items: 
Size: 517994 Color: 1
Size: 481979 Color: 0

Bin 1348: 28 of cap free
Amount of items: 2
Items: 
Size: 531452 Color: 0
Size: 468521 Color: 1

Bin 1349: 28 of cap free
Amount of items: 2
Items: 
Size: 557493 Color: 1
Size: 442480 Color: 0

Bin 1350: 28 of cap free
Amount of items: 2
Items: 
Size: 579743 Color: 0
Size: 420230 Color: 1

Bin 1351: 28 of cap free
Amount of items: 2
Items: 
Size: 580712 Color: 1
Size: 419261 Color: 0

Bin 1352: 28 of cap free
Amount of items: 2
Items: 
Size: 613596 Color: 0
Size: 386377 Color: 1

Bin 1353: 28 of cap free
Amount of items: 2
Items: 
Size: 614515 Color: 0
Size: 385458 Color: 1

Bin 1354: 28 of cap free
Amount of items: 2
Items: 
Size: 620774 Color: 0
Size: 379199 Color: 1

Bin 1355: 28 of cap free
Amount of items: 2
Items: 
Size: 629083 Color: 0
Size: 370890 Color: 1

Bin 1356: 28 of cap free
Amount of items: 2
Items: 
Size: 650651 Color: 1
Size: 349322 Color: 0

Bin 1357: 28 of cap free
Amount of items: 2
Items: 
Size: 679293 Color: 1
Size: 320680 Color: 0

Bin 1358: 28 of cap free
Amount of items: 2
Items: 
Size: 710986 Color: 1
Size: 288987 Color: 0

Bin 1359: 28 of cap free
Amount of items: 2
Items: 
Size: 711970 Color: 0
Size: 288003 Color: 1

Bin 1360: 28 of cap free
Amount of items: 2
Items: 
Size: 713196 Color: 0
Size: 286777 Color: 1

Bin 1361: 28 of cap free
Amount of items: 2
Items: 
Size: 734128 Color: 0
Size: 265845 Color: 1

Bin 1362: 28 of cap free
Amount of items: 2
Items: 
Size: 743220 Color: 0
Size: 256753 Color: 1

Bin 1363: 28 of cap free
Amount of items: 3
Items: 
Size: 778387 Color: 1
Size: 112010 Color: 1
Size: 109576 Color: 0

Bin 1364: 29 of cap free
Amount of items: 3
Items: 
Size: 444985 Color: 1
Size: 361620 Color: 0
Size: 193367 Color: 0

Bin 1365: 29 of cap free
Amount of items: 3
Items: 
Size: 445695 Color: 1
Size: 428002 Color: 0
Size: 126275 Color: 0

Bin 1366: 29 of cap free
Amount of items: 2
Items: 
Size: 546001 Color: 1
Size: 453971 Color: 0

Bin 1367: 29 of cap free
Amount of items: 2
Items: 
Size: 547756 Color: 0
Size: 452216 Color: 1

Bin 1368: 29 of cap free
Amount of items: 2
Items: 
Size: 563490 Color: 1
Size: 436482 Color: 0

Bin 1369: 29 of cap free
Amount of items: 2
Items: 
Size: 564970 Color: 0
Size: 435002 Color: 1

Bin 1370: 29 of cap free
Amount of items: 2
Items: 
Size: 574917 Color: 0
Size: 425055 Color: 1

Bin 1371: 29 of cap free
Amount of items: 2
Items: 
Size: 588737 Color: 0
Size: 411235 Color: 1

Bin 1372: 29 of cap free
Amount of items: 2
Items: 
Size: 601002 Color: 0
Size: 398970 Color: 1

Bin 1373: 29 of cap free
Amount of items: 2
Items: 
Size: 621618 Color: 0
Size: 378354 Color: 1

Bin 1374: 29 of cap free
Amount of items: 2
Items: 
Size: 624067 Color: 1
Size: 375905 Color: 0

Bin 1375: 29 of cap free
Amount of items: 2
Items: 
Size: 640049 Color: 0
Size: 359923 Color: 1

Bin 1376: 29 of cap free
Amount of items: 2
Items: 
Size: 645499 Color: 1
Size: 354473 Color: 0

Bin 1377: 29 of cap free
Amount of items: 2
Items: 
Size: 657877 Color: 0
Size: 342095 Color: 1

Bin 1378: 29 of cap free
Amount of items: 2
Items: 
Size: 693879 Color: 0
Size: 306093 Color: 1

Bin 1379: 29 of cap free
Amount of items: 2
Items: 
Size: 703537 Color: 1
Size: 296435 Color: 0

Bin 1380: 29 of cap free
Amount of items: 2
Items: 
Size: 706617 Color: 0
Size: 293355 Color: 1

Bin 1381: 29 of cap free
Amount of items: 2
Items: 
Size: 736975 Color: 0
Size: 262997 Color: 1

Bin 1382: 29 of cap free
Amount of items: 2
Items: 
Size: 747459 Color: 1
Size: 252513 Color: 0

Bin 1383: 29 of cap free
Amount of items: 2
Items: 
Size: 753217 Color: 0
Size: 246755 Color: 1

Bin 1384: 29 of cap free
Amount of items: 2
Items: 
Size: 777674 Color: 0
Size: 222298 Color: 1

Bin 1385: 29 of cap free
Amount of items: 2
Items: 
Size: 777926 Color: 0
Size: 222046 Color: 1

Bin 1386: 29 of cap free
Amount of items: 2
Items: 
Size: 778764 Color: 0
Size: 221208 Color: 1

Bin 1387: 29 of cap free
Amount of items: 2
Items: 
Size: 784347 Color: 1
Size: 215625 Color: 0

Bin 1388: 29 of cap free
Amount of items: 2
Items: 
Size: 784730 Color: 1
Size: 215242 Color: 0

Bin 1389: 29 of cap free
Amount of items: 2
Items: 
Size: 788557 Color: 0
Size: 211415 Color: 1

Bin 1390: 29 of cap free
Amount of items: 3
Items: 
Size: 792502 Color: 1
Size: 106288 Color: 0
Size: 101182 Color: 0

Bin 1391: 29 of cap free
Amount of items: 2
Items: 
Size: 794827 Color: 0
Size: 205145 Color: 1

Bin 1392: 30 of cap free
Amount of items: 2
Items: 
Size: 508887 Color: 0
Size: 491084 Color: 1

Bin 1393: 30 of cap free
Amount of items: 2
Items: 
Size: 521294 Color: 0
Size: 478677 Color: 1

Bin 1394: 30 of cap free
Amount of items: 2
Items: 
Size: 533653 Color: 0
Size: 466318 Color: 1

Bin 1395: 30 of cap free
Amount of items: 2
Items: 
Size: 549351 Color: 0
Size: 450620 Color: 1

Bin 1396: 30 of cap free
Amount of items: 2
Items: 
Size: 561573 Color: 0
Size: 438398 Color: 1

Bin 1397: 30 of cap free
Amount of items: 2
Items: 
Size: 563690 Color: 1
Size: 436281 Color: 0

Bin 1398: 30 of cap free
Amount of items: 2
Items: 
Size: 569545 Color: 0
Size: 430426 Color: 1

Bin 1399: 30 of cap free
Amount of items: 2
Items: 
Size: 585925 Color: 0
Size: 414046 Color: 1

Bin 1400: 30 of cap free
Amount of items: 2
Items: 
Size: 587438 Color: 0
Size: 412533 Color: 1

Bin 1401: 30 of cap free
Amount of items: 2
Items: 
Size: 590477 Color: 1
Size: 409494 Color: 0

Bin 1402: 30 of cap free
Amount of items: 2
Items: 
Size: 592044 Color: 1
Size: 407927 Color: 0

Bin 1403: 30 of cap free
Amount of items: 2
Items: 
Size: 605555 Color: 1
Size: 394416 Color: 0

Bin 1404: 30 of cap free
Amount of items: 2
Items: 
Size: 608011 Color: 0
Size: 391960 Color: 1

Bin 1405: 30 of cap free
Amount of items: 2
Items: 
Size: 637665 Color: 1
Size: 362306 Color: 0

Bin 1406: 30 of cap free
Amount of items: 2
Items: 
Size: 641969 Color: 0
Size: 358002 Color: 1

Bin 1407: 30 of cap free
Amount of items: 2
Items: 
Size: 668181 Color: 1
Size: 331790 Color: 0

Bin 1408: 30 of cap free
Amount of items: 2
Items: 
Size: 690502 Color: 0
Size: 309469 Color: 1

Bin 1409: 30 of cap free
Amount of items: 2
Items: 
Size: 704474 Color: 1
Size: 295497 Color: 0

Bin 1410: 30 of cap free
Amount of items: 2
Items: 
Size: 717240 Color: 1
Size: 282731 Color: 0

Bin 1411: 30 of cap free
Amount of items: 2
Items: 
Size: 726143 Color: 0
Size: 273828 Color: 1

Bin 1412: 30 of cap free
Amount of items: 2
Items: 
Size: 760626 Color: 1
Size: 239345 Color: 0

Bin 1413: 30 of cap free
Amount of items: 2
Items: 
Size: 764970 Color: 0
Size: 235001 Color: 1

Bin 1414: 30 of cap free
Amount of items: 2
Items: 
Size: 781568 Color: 1
Size: 218403 Color: 0

Bin 1415: 30 of cap free
Amount of items: 3
Items: 
Size: 793186 Color: 1
Size: 104251 Color: 1
Size: 102534 Color: 0

Bin 1416: 30 of cap free
Amount of items: 3
Items: 
Size: 794066 Color: 1
Size: 103593 Color: 0
Size: 102312 Color: 1

Bin 1417: 30 of cap free
Amount of items: 2
Items: 
Size: 799404 Color: 1
Size: 200567 Color: 0

Bin 1418: 31 of cap free
Amount of items: 2
Items: 
Size: 512783 Color: 1
Size: 487187 Color: 0

Bin 1419: 31 of cap free
Amount of items: 3
Items: 
Size: 514328 Color: 1
Size: 254967 Color: 0
Size: 230675 Color: 0

Bin 1420: 31 of cap free
Amount of items: 2
Items: 
Size: 527774 Color: 0
Size: 472196 Color: 1

Bin 1421: 31 of cap free
Amount of items: 2
Items: 
Size: 537135 Color: 0
Size: 462835 Color: 1

Bin 1422: 31 of cap free
Amount of items: 2
Items: 
Size: 550299 Color: 1
Size: 449671 Color: 0

Bin 1423: 31 of cap free
Amount of items: 2
Items: 
Size: 556706 Color: 1
Size: 443264 Color: 0

Bin 1424: 31 of cap free
Amount of items: 2
Items: 
Size: 569677 Color: 0
Size: 430293 Color: 1

Bin 1425: 31 of cap free
Amount of items: 2
Items: 
Size: 584237 Color: 0
Size: 415733 Color: 1

Bin 1426: 31 of cap free
Amount of items: 2
Items: 
Size: 589560 Color: 1
Size: 410410 Color: 0

Bin 1427: 31 of cap free
Amount of items: 2
Items: 
Size: 592522 Color: 1
Size: 407448 Color: 0

Bin 1428: 31 of cap free
Amount of items: 2
Items: 
Size: 620343 Color: 1
Size: 379627 Color: 0

Bin 1429: 31 of cap free
Amount of items: 2
Items: 
Size: 666518 Color: 0
Size: 333452 Color: 1

Bin 1430: 31 of cap free
Amount of items: 2
Items: 
Size: 685327 Color: 1
Size: 314643 Color: 0

Bin 1431: 31 of cap free
Amount of items: 2
Items: 
Size: 699073 Color: 1
Size: 300897 Color: 0

Bin 1432: 31 of cap free
Amount of items: 2
Items: 
Size: 730138 Color: 1
Size: 269832 Color: 0

Bin 1433: 31 of cap free
Amount of items: 2
Items: 
Size: 750991 Color: 0
Size: 248979 Color: 1

Bin 1434: 31 of cap free
Amount of items: 2
Items: 
Size: 754043 Color: 0
Size: 245927 Color: 1

Bin 1435: 31 of cap free
Amount of items: 2
Items: 
Size: 779172 Color: 0
Size: 220798 Color: 1

Bin 1436: 31 of cap free
Amount of items: 3
Items: 
Size: 782507 Color: 0
Size: 109214 Color: 1
Size: 108249 Color: 1

Bin 1437: 31 of cap free
Amount of items: 3
Items: 
Size: 786440 Color: 0
Size: 107930 Color: 1
Size: 105600 Color: 0

Bin 1438: 31 of cap free
Amount of items: 2
Items: 
Size: 787561 Color: 1
Size: 212409 Color: 0

Bin 1439: 31 of cap free
Amount of items: 2
Items: 
Size: 789474 Color: 0
Size: 210496 Color: 1

Bin 1440: 32 of cap free
Amount of items: 3
Items: 
Size: 424512 Color: 0
Size: 301974 Color: 1
Size: 273483 Color: 1

Bin 1441: 32 of cap free
Amount of items: 2
Items: 
Size: 502930 Color: 1
Size: 497039 Color: 0

Bin 1442: 32 of cap free
Amount of items: 2
Items: 
Size: 543449 Color: 0
Size: 456520 Color: 1

Bin 1443: 32 of cap free
Amount of items: 2
Items: 
Size: 559057 Color: 1
Size: 440912 Color: 0

Bin 1444: 32 of cap free
Amount of items: 2
Items: 
Size: 579304 Color: 0
Size: 420665 Color: 1

Bin 1445: 32 of cap free
Amount of items: 2
Items: 
Size: 593614 Color: 1
Size: 406355 Color: 0

Bin 1446: 32 of cap free
Amount of items: 2
Items: 
Size: 595944 Color: 1
Size: 404025 Color: 0

Bin 1447: 32 of cap free
Amount of items: 2
Items: 
Size: 597045 Color: 1
Size: 402924 Color: 0

Bin 1448: 32 of cap free
Amount of items: 2
Items: 
Size: 608034 Color: 1
Size: 391935 Color: 0

Bin 1449: 32 of cap free
Amount of items: 2
Items: 
Size: 612199 Color: 1
Size: 387770 Color: 0

Bin 1450: 32 of cap free
Amount of items: 2
Items: 
Size: 614896 Color: 0
Size: 385073 Color: 1

Bin 1451: 32 of cap free
Amount of items: 2
Items: 
Size: 618366 Color: 1
Size: 381603 Color: 0

Bin 1452: 32 of cap free
Amount of items: 2
Items: 
Size: 637871 Color: 0
Size: 362098 Color: 1

Bin 1453: 32 of cap free
Amount of items: 2
Items: 
Size: 675978 Color: 1
Size: 323991 Color: 0

Bin 1454: 32 of cap free
Amount of items: 2
Items: 
Size: 691711 Color: 1
Size: 308258 Color: 0

Bin 1455: 32 of cap free
Amount of items: 2
Items: 
Size: 694142 Color: 1
Size: 305827 Color: 0

Bin 1456: 32 of cap free
Amount of items: 2
Items: 
Size: 696575 Color: 0
Size: 303394 Color: 1

Bin 1457: 32 of cap free
Amount of items: 2
Items: 
Size: 702058 Color: 0
Size: 297911 Color: 1

Bin 1458: 32 of cap free
Amount of items: 2
Items: 
Size: 711329 Color: 0
Size: 288640 Color: 1

Bin 1459: 32 of cap free
Amount of items: 2
Items: 
Size: 738923 Color: 0
Size: 261046 Color: 1

Bin 1460: 32 of cap free
Amount of items: 2
Items: 
Size: 745040 Color: 0
Size: 254929 Color: 1

Bin 1461: 32 of cap free
Amount of items: 2
Items: 
Size: 748521 Color: 0
Size: 251448 Color: 1

Bin 1462: 32 of cap free
Amount of items: 2
Items: 
Size: 753663 Color: 1
Size: 246306 Color: 0

Bin 1463: 32 of cap free
Amount of items: 3
Items: 
Size: 781032 Color: 0
Size: 112331 Color: 0
Size: 106606 Color: 1

Bin 1464: 33 of cap free
Amount of items: 2
Items: 
Size: 506171 Color: 1
Size: 493797 Color: 0

Bin 1465: 33 of cap free
Amount of items: 2
Items: 
Size: 511040 Color: 0
Size: 488928 Color: 1

Bin 1466: 33 of cap free
Amount of items: 2
Items: 
Size: 511969 Color: 1
Size: 487999 Color: 0

Bin 1467: 33 of cap free
Amount of items: 3
Items: 
Size: 514509 Color: 1
Size: 289738 Color: 0
Size: 195721 Color: 0

Bin 1468: 33 of cap free
Amount of items: 2
Items: 
Size: 544854 Color: 1
Size: 455114 Color: 0

Bin 1469: 33 of cap free
Amount of items: 2
Items: 
Size: 556115 Color: 0
Size: 443853 Color: 1

Bin 1470: 33 of cap free
Amount of items: 2
Items: 
Size: 590770 Color: 1
Size: 409198 Color: 0

Bin 1471: 33 of cap free
Amount of items: 2
Items: 
Size: 598657 Color: 1
Size: 401311 Color: 0

Bin 1472: 33 of cap free
Amount of items: 2
Items: 
Size: 615909 Color: 0
Size: 384059 Color: 1

Bin 1473: 33 of cap free
Amount of items: 2
Items: 
Size: 619532 Color: 1
Size: 380436 Color: 0

Bin 1474: 33 of cap free
Amount of items: 2
Items: 
Size: 625773 Color: 0
Size: 374195 Color: 1

Bin 1475: 33 of cap free
Amount of items: 2
Items: 
Size: 662328 Color: 0
Size: 337640 Color: 1

Bin 1476: 33 of cap free
Amount of items: 2
Items: 
Size: 665109 Color: 1
Size: 334859 Color: 0

Bin 1477: 33 of cap free
Amount of items: 2
Items: 
Size: 745289 Color: 0
Size: 254679 Color: 1

Bin 1478: 33 of cap free
Amount of items: 2
Items: 
Size: 753067 Color: 1
Size: 246901 Color: 0

Bin 1479: 33 of cap free
Amount of items: 2
Items: 
Size: 759002 Color: 0
Size: 240966 Color: 1

Bin 1480: 33 of cap free
Amount of items: 2
Items: 
Size: 772178 Color: 1
Size: 227790 Color: 0

Bin 1481: 33 of cap free
Amount of items: 2
Items: 
Size: 775887 Color: 0
Size: 224081 Color: 1

Bin 1482: 33 of cap free
Amount of items: 2
Items: 
Size: 789150 Color: 1
Size: 210818 Color: 0

Bin 1483: 34 of cap free
Amount of items: 7
Items: 
Size: 149193 Color: 0
Size: 148452 Color: 0
Size: 148202 Color: 0
Size: 142570 Color: 1
Size: 140448 Color: 1
Size: 139055 Color: 0
Size: 132047 Color: 1

Bin 1484: 34 of cap free
Amount of items: 3
Items: 
Size: 421702 Color: 0
Size: 419106 Color: 1
Size: 159159 Color: 1

Bin 1485: 34 of cap free
Amount of items: 2
Items: 
Size: 574178 Color: 1
Size: 425789 Color: 0

Bin 1486: 34 of cap free
Amount of items: 2
Items: 
Size: 621384 Color: 1
Size: 378583 Color: 0

Bin 1487: 34 of cap free
Amount of items: 2
Items: 
Size: 686217 Color: 1
Size: 313750 Color: 0

Bin 1488: 34 of cap free
Amount of items: 2
Items: 
Size: 717292 Color: 1
Size: 282675 Color: 0

Bin 1489: 34 of cap free
Amount of items: 2
Items: 
Size: 730385 Color: 1
Size: 269582 Color: 0

Bin 1490: 34 of cap free
Amount of items: 2
Items: 
Size: 742397 Color: 1
Size: 257570 Color: 0

Bin 1491: 34 of cap free
Amount of items: 2
Items: 
Size: 766497 Color: 0
Size: 233470 Color: 1

Bin 1492: 34 of cap free
Amount of items: 2
Items: 
Size: 797928 Color: 1
Size: 202039 Color: 0

Bin 1493: 35 of cap free
Amount of items: 2
Items: 
Size: 513685 Color: 1
Size: 486281 Color: 0

Bin 1494: 35 of cap free
Amount of items: 2
Items: 
Size: 547806 Color: 0
Size: 452160 Color: 1

Bin 1495: 35 of cap free
Amount of items: 2
Items: 
Size: 559726 Color: 0
Size: 440240 Color: 1

Bin 1496: 35 of cap free
Amount of items: 2
Items: 
Size: 581861 Color: 0
Size: 418105 Color: 1

Bin 1497: 35 of cap free
Amount of items: 2
Items: 
Size: 592246 Color: 1
Size: 407720 Color: 0

Bin 1498: 35 of cap free
Amount of items: 2
Items: 
Size: 599763 Color: 0
Size: 400203 Color: 1

Bin 1499: 35 of cap free
Amount of items: 2
Items: 
Size: 619329 Color: 1
Size: 380637 Color: 0

Bin 1500: 35 of cap free
Amount of items: 2
Items: 
Size: 638386 Color: 1
Size: 361580 Color: 0

Bin 1501: 35 of cap free
Amount of items: 2
Items: 
Size: 686157 Color: 1
Size: 313809 Color: 0

Bin 1502: 35 of cap free
Amount of items: 2
Items: 
Size: 694931 Color: 0
Size: 305035 Color: 1

Bin 1503: 35 of cap free
Amount of items: 2
Items: 
Size: 697984 Color: 1
Size: 301982 Color: 0

Bin 1504: 35 of cap free
Amount of items: 2
Items: 
Size: 762935 Color: 1
Size: 237031 Color: 0

Bin 1505: 35 of cap free
Amount of items: 2
Items: 
Size: 765089 Color: 1
Size: 234877 Color: 0

Bin 1506: 35 of cap free
Amount of items: 2
Items: 
Size: 775008 Color: 0
Size: 224958 Color: 1

Bin 1507: 35 of cap free
Amount of items: 3
Items: 
Size: 782535 Color: 0
Size: 113518 Color: 0
Size: 103913 Color: 1

Bin 1508: 35 of cap free
Amount of items: 2
Items: 
Size: 791938 Color: 1
Size: 208028 Color: 0

Bin 1509: 35 of cap free
Amount of items: 2
Items: 
Size: 797589 Color: 0
Size: 202377 Color: 1

Bin 1510: 36 of cap free
Amount of items: 2
Items: 
Size: 515823 Color: 0
Size: 484142 Color: 1

Bin 1511: 36 of cap free
Amount of items: 3
Items: 
Size: 524234 Color: 1
Size: 320756 Color: 0
Size: 154975 Color: 0

Bin 1512: 36 of cap free
Amount of items: 2
Items: 
Size: 528773 Color: 1
Size: 471192 Color: 0

Bin 1513: 36 of cap free
Amount of items: 2
Items: 
Size: 583273 Color: 1
Size: 416692 Color: 0

Bin 1514: 36 of cap free
Amount of items: 2
Items: 
Size: 587785 Color: 1
Size: 412180 Color: 0

Bin 1515: 36 of cap free
Amount of items: 2
Items: 
Size: 588734 Color: 0
Size: 411231 Color: 1

Bin 1516: 36 of cap free
Amount of items: 2
Items: 
Size: 612353 Color: 0
Size: 387612 Color: 1

Bin 1517: 36 of cap free
Amount of items: 2
Items: 
Size: 621012 Color: 0
Size: 378953 Color: 1

Bin 1518: 36 of cap free
Amount of items: 2
Items: 
Size: 631084 Color: 1
Size: 368881 Color: 0

Bin 1519: 36 of cap free
Amount of items: 2
Items: 
Size: 641216 Color: 1
Size: 358749 Color: 0

Bin 1520: 36 of cap free
Amount of items: 2
Items: 
Size: 670267 Color: 0
Size: 329698 Color: 1

Bin 1521: 36 of cap free
Amount of items: 2
Items: 
Size: 701521 Color: 1
Size: 298444 Color: 0

Bin 1522: 36 of cap free
Amount of items: 2
Items: 
Size: 702523 Color: 1
Size: 297442 Color: 0

Bin 1523: 36 of cap free
Amount of items: 2
Items: 
Size: 712034 Color: 1
Size: 287931 Color: 0

Bin 1524: 36 of cap free
Amount of items: 2
Items: 
Size: 740533 Color: 0
Size: 259432 Color: 1

Bin 1525: 36 of cap free
Amount of items: 2
Items: 
Size: 763091 Color: 0
Size: 236874 Color: 1

Bin 1526: 36 of cap free
Amount of items: 2
Items: 
Size: 767476 Color: 0
Size: 232489 Color: 1

Bin 1527: 36 of cap free
Amount of items: 2
Items: 
Size: 780517 Color: 0
Size: 219448 Color: 1

Bin 1528: 37 of cap free
Amount of items: 2
Items: 
Size: 507398 Color: 1
Size: 492566 Color: 0

Bin 1529: 37 of cap free
Amount of items: 2
Items: 
Size: 546624 Color: 1
Size: 453340 Color: 0

Bin 1530: 37 of cap free
Amount of items: 2
Items: 
Size: 548808 Color: 1
Size: 451156 Color: 0

Bin 1531: 37 of cap free
Amount of items: 2
Items: 
Size: 570483 Color: 0
Size: 429481 Color: 1

Bin 1532: 37 of cap free
Amount of items: 2
Items: 
Size: 621843 Color: 1
Size: 378121 Color: 0

Bin 1533: 37 of cap free
Amount of items: 2
Items: 
Size: 627233 Color: 0
Size: 372731 Color: 1

Bin 1534: 37 of cap free
Amount of items: 2
Items: 
Size: 634004 Color: 0
Size: 365960 Color: 1

Bin 1535: 37 of cap free
Amount of items: 2
Items: 
Size: 647188 Color: 0
Size: 352776 Color: 1

Bin 1536: 37 of cap free
Amount of items: 2
Items: 
Size: 674079 Color: 0
Size: 325885 Color: 1

Bin 1537: 37 of cap free
Amount of items: 2
Items: 
Size: 693038 Color: 0
Size: 306926 Color: 1

Bin 1538: 37 of cap free
Amount of items: 2
Items: 
Size: 711151 Color: 1
Size: 288813 Color: 0

Bin 1539: 37 of cap free
Amount of items: 2
Items: 
Size: 734859 Color: 1
Size: 265105 Color: 0

Bin 1540: 37 of cap free
Amount of items: 2
Items: 
Size: 790341 Color: 1
Size: 209623 Color: 0

Bin 1541: 37 of cap free
Amount of items: 2
Items: 
Size: 797374 Color: 0
Size: 202590 Color: 1

Bin 1542: 38 of cap free
Amount of items: 2
Items: 
Size: 502109 Color: 1
Size: 497854 Color: 0

Bin 1543: 38 of cap free
Amount of items: 2
Items: 
Size: 521780 Color: 1
Size: 478183 Color: 0

Bin 1544: 38 of cap free
Amount of items: 2
Items: 
Size: 561150 Color: 1
Size: 438813 Color: 0

Bin 1545: 38 of cap free
Amount of items: 2
Items: 
Size: 571461 Color: 1
Size: 428502 Color: 0

Bin 1546: 38 of cap free
Amount of items: 2
Items: 
Size: 578424 Color: 0
Size: 421539 Color: 1

Bin 1547: 38 of cap free
Amount of items: 2
Items: 
Size: 579186 Color: 1
Size: 420777 Color: 0

Bin 1548: 38 of cap free
Amount of items: 2
Items: 
Size: 583103 Color: 0
Size: 416860 Color: 1

Bin 1549: 38 of cap free
Amount of items: 2
Items: 
Size: 593103 Color: 0
Size: 406860 Color: 1

Bin 1550: 38 of cap free
Amount of items: 2
Items: 
Size: 639228 Color: 1
Size: 360735 Color: 0

Bin 1551: 38 of cap free
Amount of items: 2
Items: 
Size: 667366 Color: 1
Size: 332597 Color: 0

Bin 1552: 38 of cap free
Amount of items: 2
Items: 
Size: 677424 Color: 0
Size: 322539 Color: 1

Bin 1553: 38 of cap free
Amount of items: 2
Items: 
Size: 696297 Color: 0
Size: 303666 Color: 1

Bin 1554: 38 of cap free
Amount of items: 2
Items: 
Size: 739018 Color: 0
Size: 260945 Color: 1

Bin 1555: 38 of cap free
Amount of items: 2
Items: 
Size: 739333 Color: 0
Size: 260630 Color: 1

Bin 1556: 38 of cap free
Amount of items: 2
Items: 
Size: 753615 Color: 1
Size: 246348 Color: 0

Bin 1557: 38 of cap free
Amount of items: 2
Items: 
Size: 773175 Color: 1
Size: 226788 Color: 0

Bin 1558: 38 of cap free
Amount of items: 2
Items: 
Size: 774491 Color: 0
Size: 225472 Color: 1

Bin 1559: 38 of cap free
Amount of items: 2
Items: 
Size: 785282 Color: 0
Size: 214681 Color: 1

Bin 1560: 39 of cap free
Amount of items: 2
Items: 
Size: 517361 Color: 0
Size: 482601 Color: 1

Bin 1561: 39 of cap free
Amount of items: 2
Items: 
Size: 521229 Color: 1
Size: 478733 Color: 0

Bin 1562: 39 of cap free
Amount of items: 2
Items: 
Size: 528889 Color: 1
Size: 471073 Color: 0

Bin 1563: 39 of cap free
Amount of items: 2
Items: 
Size: 579781 Color: 0
Size: 420181 Color: 1

Bin 1564: 39 of cap free
Amount of items: 2
Items: 
Size: 587225 Color: 0
Size: 412737 Color: 1

Bin 1565: 39 of cap free
Amount of items: 2
Items: 
Size: 609238 Color: 1
Size: 390724 Color: 0

Bin 1566: 39 of cap free
Amount of items: 2
Items: 
Size: 628410 Color: 1
Size: 371552 Color: 0

Bin 1567: 39 of cap free
Amount of items: 2
Items: 
Size: 629576 Color: 1
Size: 370386 Color: 0

Bin 1568: 39 of cap free
Amount of items: 2
Items: 
Size: 637294 Color: 0
Size: 362668 Color: 1

Bin 1569: 39 of cap free
Amount of items: 2
Items: 
Size: 642155 Color: 0
Size: 357807 Color: 1

Bin 1570: 39 of cap free
Amount of items: 2
Items: 
Size: 663681 Color: 1
Size: 336281 Color: 0

Bin 1571: 39 of cap free
Amount of items: 2
Items: 
Size: 667434 Color: 0
Size: 332528 Color: 1

Bin 1572: 39 of cap free
Amount of items: 2
Items: 
Size: 668164 Color: 0
Size: 331798 Color: 1

Bin 1573: 39 of cap free
Amount of items: 2
Items: 
Size: 712870 Color: 1
Size: 287092 Color: 0

Bin 1574: 39 of cap free
Amount of items: 2
Items: 
Size: 713230 Color: 0
Size: 286732 Color: 1

Bin 1575: 39 of cap free
Amount of items: 2
Items: 
Size: 749205 Color: 1
Size: 250757 Color: 0

Bin 1576: 39 of cap free
Amount of items: 2
Items: 
Size: 768133 Color: 1
Size: 231829 Color: 0

Bin 1577: 39 of cap free
Amount of items: 3
Items: 
Size: 781636 Color: 0
Size: 111756 Color: 1
Size: 106570 Color: 0

Bin 1578: 39 of cap free
Amount of items: 2
Items: 
Size: 788869 Color: 1
Size: 211093 Color: 0

Bin 1579: 40 of cap free
Amount of items: 2
Items: 
Size: 553374 Color: 0
Size: 446587 Color: 1

Bin 1580: 40 of cap free
Amount of items: 2
Items: 
Size: 594667 Color: 1
Size: 405294 Color: 0

Bin 1581: 40 of cap free
Amount of items: 2
Items: 
Size: 601323 Color: 1
Size: 398638 Color: 0

Bin 1582: 40 of cap free
Amount of items: 2
Items: 
Size: 627653 Color: 0
Size: 372308 Color: 1

Bin 1583: 40 of cap free
Amount of items: 2
Items: 
Size: 648968 Color: 1
Size: 350993 Color: 0

Bin 1584: 40 of cap free
Amount of items: 2
Items: 
Size: 658526 Color: 1
Size: 341435 Color: 0

Bin 1585: 40 of cap free
Amount of items: 2
Items: 
Size: 689214 Color: 1
Size: 310747 Color: 0

Bin 1586: 40 of cap free
Amount of items: 2
Items: 
Size: 699650 Color: 0
Size: 300311 Color: 1

Bin 1587: 40 of cap free
Amount of items: 2
Items: 
Size: 744788 Color: 1
Size: 255173 Color: 0

Bin 1588: 40 of cap free
Amount of items: 2
Items: 
Size: 762921 Color: 0
Size: 237040 Color: 1

Bin 1589: 40 of cap free
Amount of items: 2
Items: 
Size: 789698 Color: 1
Size: 210263 Color: 0

Bin 1590: 41 of cap free
Amount of items: 2
Items: 
Size: 507412 Color: 0
Size: 492548 Color: 1

Bin 1591: 41 of cap free
Amount of items: 2
Items: 
Size: 508836 Color: 0
Size: 491124 Color: 1

Bin 1592: 41 of cap free
Amount of items: 2
Items: 
Size: 525703 Color: 0
Size: 474257 Color: 1

Bin 1593: 41 of cap free
Amount of items: 2
Items: 
Size: 530796 Color: 0
Size: 469164 Color: 1

Bin 1594: 41 of cap free
Amount of items: 2
Items: 
Size: 536244 Color: 1
Size: 463716 Color: 0

Bin 1595: 41 of cap free
Amount of items: 2
Items: 
Size: 563277 Color: 0
Size: 436683 Color: 1

Bin 1596: 41 of cap free
Amount of items: 2
Items: 
Size: 564566 Color: 1
Size: 435394 Color: 0

Bin 1597: 41 of cap free
Amount of items: 2
Items: 
Size: 608176 Color: 0
Size: 391784 Color: 1

Bin 1598: 41 of cap free
Amount of items: 2
Items: 
Size: 614994 Color: 0
Size: 384966 Color: 1

Bin 1599: 41 of cap free
Amount of items: 2
Items: 
Size: 619086 Color: 1
Size: 380874 Color: 0

Bin 1600: 41 of cap free
Amount of items: 2
Items: 
Size: 650845 Color: 0
Size: 349115 Color: 1

Bin 1601: 41 of cap free
Amount of items: 2
Items: 
Size: 667203 Color: 1
Size: 332757 Color: 0

Bin 1602: 41 of cap free
Amount of items: 2
Items: 
Size: 676149 Color: 0
Size: 323811 Color: 1

Bin 1603: 41 of cap free
Amount of items: 2
Items: 
Size: 687850 Color: 1
Size: 312110 Color: 0

Bin 1604: 41 of cap free
Amount of items: 2
Items: 
Size: 703109 Color: 1
Size: 296851 Color: 0

Bin 1605: 41 of cap free
Amount of items: 2
Items: 
Size: 726341 Color: 0
Size: 273619 Color: 1

Bin 1606: 41 of cap free
Amount of items: 2
Items: 
Size: 736058 Color: 1
Size: 263902 Color: 0

Bin 1607: 41 of cap free
Amount of items: 2
Items: 
Size: 756447 Color: 0
Size: 243513 Color: 1

Bin 1608: 41 of cap free
Amount of items: 2
Items: 
Size: 759551 Color: 1
Size: 240409 Color: 0

Bin 1609: 41 of cap free
Amount of items: 2
Items: 
Size: 795615 Color: 0
Size: 204345 Color: 1

Bin 1610: 41 of cap free
Amount of items: 2
Items: 
Size: 798698 Color: 1
Size: 201262 Color: 0

Bin 1611: 41 of cap free
Amount of items: 2
Items: 
Size: 798932 Color: 1
Size: 201028 Color: 0

Bin 1612: 42 of cap free
Amount of items: 2
Items: 
Size: 501265 Color: 0
Size: 498694 Color: 1

Bin 1613: 42 of cap free
Amount of items: 2
Items: 
Size: 540027 Color: 0
Size: 459932 Color: 1

Bin 1614: 42 of cap free
Amount of items: 2
Items: 
Size: 558289 Color: 0
Size: 441670 Color: 1

Bin 1615: 42 of cap free
Amount of items: 2
Items: 
Size: 581844 Color: 1
Size: 418115 Color: 0

Bin 1616: 42 of cap free
Amount of items: 2
Items: 
Size: 606876 Color: 1
Size: 393083 Color: 0

Bin 1617: 42 of cap free
Amount of items: 2
Items: 
Size: 608788 Color: 0
Size: 391171 Color: 1

Bin 1618: 42 of cap free
Amount of items: 2
Items: 
Size: 629324 Color: 1
Size: 370635 Color: 0

Bin 1619: 42 of cap free
Amount of items: 2
Items: 
Size: 631354 Color: 0
Size: 368605 Color: 1

Bin 1620: 42 of cap free
Amount of items: 2
Items: 
Size: 633471 Color: 0
Size: 366488 Color: 1

Bin 1621: 42 of cap free
Amount of items: 2
Items: 
Size: 670637 Color: 1
Size: 329322 Color: 0

Bin 1622: 42 of cap free
Amount of items: 2
Items: 
Size: 685907 Color: 0
Size: 314052 Color: 1

Bin 1623: 42 of cap free
Amount of items: 2
Items: 
Size: 695715 Color: 0
Size: 304244 Color: 1

Bin 1624: 42 of cap free
Amount of items: 2
Items: 
Size: 699702 Color: 1
Size: 300257 Color: 0

Bin 1625: 42 of cap free
Amount of items: 2
Items: 
Size: 712830 Color: 0
Size: 287129 Color: 1

Bin 1626: 42 of cap free
Amount of items: 2
Items: 
Size: 740263 Color: 1
Size: 259696 Color: 0

Bin 1627: 42 of cap free
Amount of items: 2
Items: 
Size: 758416 Color: 1
Size: 241543 Color: 0

Bin 1628: 42 of cap free
Amount of items: 2
Items: 
Size: 761585 Color: 0
Size: 238374 Color: 1

Bin 1629: 42 of cap free
Amount of items: 2
Items: 
Size: 784112 Color: 1
Size: 215847 Color: 0

Bin 1630: 43 of cap free
Amount of items: 2
Items: 
Size: 554912 Color: 1
Size: 445046 Color: 0

Bin 1631: 43 of cap free
Amount of items: 2
Items: 
Size: 583568 Color: 0
Size: 416390 Color: 1

Bin 1632: 43 of cap free
Amount of items: 2
Items: 
Size: 628555 Color: 1
Size: 371403 Color: 0

Bin 1633: 43 of cap free
Amount of items: 2
Items: 
Size: 637019 Color: 0
Size: 362939 Color: 1

Bin 1634: 43 of cap free
Amount of items: 2
Items: 
Size: 651828 Color: 1
Size: 348130 Color: 0

Bin 1635: 43 of cap free
Amount of items: 2
Items: 
Size: 657866 Color: 1
Size: 342092 Color: 0

Bin 1636: 43 of cap free
Amount of items: 2
Items: 
Size: 735793 Color: 1
Size: 264165 Color: 0

Bin 1637: 43 of cap free
Amount of items: 2
Items: 
Size: 750862 Color: 1
Size: 249096 Color: 0

Bin 1638: 43 of cap free
Amount of items: 2
Items: 
Size: 751708 Color: 0
Size: 248250 Color: 1

Bin 1639: 43 of cap free
Amount of items: 2
Items: 
Size: 774273 Color: 0
Size: 225685 Color: 1

Bin 1640: 43 of cap free
Amount of items: 2
Items: 
Size: 778568 Color: 1
Size: 221390 Color: 0

Bin 1641: 43 of cap free
Amount of items: 2
Items: 
Size: 792964 Color: 1
Size: 206994 Color: 0

Bin 1642: 44 of cap free
Amount of items: 2
Items: 
Size: 500906 Color: 0
Size: 499051 Color: 1

Bin 1643: 44 of cap free
Amount of items: 2
Items: 
Size: 504068 Color: 1
Size: 495889 Color: 0

Bin 1644: 44 of cap free
Amount of items: 2
Items: 
Size: 511366 Color: 0
Size: 488591 Color: 1

Bin 1645: 44 of cap free
Amount of items: 2
Items: 
Size: 516233 Color: 0
Size: 483724 Color: 1

Bin 1646: 44 of cap free
Amount of items: 2
Items: 
Size: 517265 Color: 0
Size: 482692 Color: 1

Bin 1647: 44 of cap free
Amount of items: 2
Items: 
Size: 525835 Color: 1
Size: 474122 Color: 0

Bin 1648: 44 of cap free
Amount of items: 2
Items: 
Size: 543699 Color: 0
Size: 456258 Color: 1

Bin 1649: 44 of cap free
Amount of items: 2
Items: 
Size: 567685 Color: 0
Size: 432272 Color: 1

Bin 1650: 44 of cap free
Amount of items: 2
Items: 
Size: 573616 Color: 1
Size: 426341 Color: 0

Bin 1651: 44 of cap free
Amount of items: 2
Items: 
Size: 575955 Color: 1
Size: 424002 Color: 0

Bin 1652: 44 of cap free
Amount of items: 2
Items: 
Size: 576901 Color: 0
Size: 423056 Color: 1

Bin 1653: 44 of cap free
Amount of items: 2
Items: 
Size: 580836 Color: 1
Size: 419121 Color: 0

Bin 1654: 44 of cap free
Amount of items: 2
Items: 
Size: 595137 Color: 1
Size: 404820 Color: 0

Bin 1655: 44 of cap free
Amount of items: 2
Items: 
Size: 597510 Color: 0
Size: 402447 Color: 1

Bin 1656: 44 of cap free
Amount of items: 2
Items: 
Size: 606296 Color: 0
Size: 393661 Color: 1

Bin 1657: 44 of cap free
Amount of items: 2
Items: 
Size: 620996 Color: 1
Size: 378961 Color: 0

Bin 1658: 44 of cap free
Amount of items: 2
Items: 
Size: 625109 Color: 1
Size: 374848 Color: 0

Bin 1659: 44 of cap free
Amount of items: 2
Items: 
Size: 627906 Color: 1
Size: 372051 Color: 0

Bin 1660: 44 of cap free
Amount of items: 2
Items: 
Size: 666401 Color: 0
Size: 333556 Color: 1

Bin 1661: 44 of cap free
Amount of items: 2
Items: 
Size: 680453 Color: 1
Size: 319504 Color: 0

Bin 1662: 44 of cap free
Amount of items: 2
Items: 
Size: 686105 Color: 0
Size: 313852 Color: 1

Bin 1663: 44 of cap free
Amount of items: 2
Items: 
Size: 699350 Color: 1
Size: 300607 Color: 0

Bin 1664: 44 of cap free
Amount of items: 2
Items: 
Size: 728155 Color: 1
Size: 271802 Color: 0

Bin 1665: 44 of cap free
Amount of items: 2
Items: 
Size: 742716 Color: 0
Size: 257241 Color: 1

Bin 1666: 44 of cap free
Amount of items: 2
Items: 
Size: 750740 Color: 1
Size: 249217 Color: 0

Bin 1667: 44 of cap free
Amount of items: 2
Items: 
Size: 793979 Color: 0
Size: 205978 Color: 1

Bin 1668: 45 of cap free
Amount of items: 2
Items: 
Size: 518192 Color: 0
Size: 481764 Color: 1

Bin 1669: 45 of cap free
Amount of items: 2
Items: 
Size: 568589 Color: 0
Size: 431367 Color: 1

Bin 1670: 45 of cap free
Amount of items: 2
Items: 
Size: 590666 Color: 1
Size: 409290 Color: 0

Bin 1671: 45 of cap free
Amount of items: 2
Items: 
Size: 606302 Color: 1
Size: 393654 Color: 0

Bin 1672: 45 of cap free
Amount of items: 2
Items: 
Size: 620361 Color: 0
Size: 379595 Color: 1

Bin 1673: 45 of cap free
Amount of items: 2
Items: 
Size: 623316 Color: 1
Size: 376640 Color: 0

Bin 1674: 45 of cap free
Amount of items: 2
Items: 
Size: 633854 Color: 0
Size: 366102 Color: 1

Bin 1675: 45 of cap free
Amount of items: 2
Items: 
Size: 643051 Color: 1
Size: 356905 Color: 0

Bin 1676: 45 of cap free
Amount of items: 2
Items: 
Size: 654653 Color: 0
Size: 345303 Color: 1

Bin 1677: 45 of cap free
Amount of items: 2
Items: 
Size: 654911 Color: 1
Size: 345045 Color: 0

Bin 1678: 45 of cap free
Amount of items: 2
Items: 
Size: 666822 Color: 0
Size: 333134 Color: 1

Bin 1679: 45 of cap free
Amount of items: 2
Items: 
Size: 705021 Color: 1
Size: 294935 Color: 0

Bin 1680: 45 of cap free
Amount of items: 2
Items: 
Size: 717661 Color: 0
Size: 282295 Color: 1

Bin 1681: 45 of cap free
Amount of items: 2
Items: 
Size: 721613 Color: 0
Size: 278343 Color: 1

Bin 1682: 45 of cap free
Amount of items: 2
Items: 
Size: 731345 Color: 1
Size: 268611 Color: 0

Bin 1683: 45 of cap free
Amount of items: 2
Items: 
Size: 734706 Color: 0
Size: 265250 Color: 1

Bin 1684: 45 of cap free
Amount of items: 2
Items: 
Size: 738571 Color: 0
Size: 261385 Color: 1

Bin 1685: 45 of cap free
Amount of items: 2
Items: 
Size: 769190 Color: 0
Size: 230766 Color: 1

Bin 1686: 45 of cap free
Amount of items: 2
Items: 
Size: 777857 Color: 1
Size: 222099 Color: 0

Bin 1687: 45 of cap free
Amount of items: 2
Items: 
Size: 782750 Color: 0
Size: 217206 Color: 1

Bin 1688: 45 of cap free
Amount of items: 2
Items: 
Size: 791525 Color: 0
Size: 208431 Color: 1

Bin 1689: 46 of cap free
Amount of items: 2
Items: 
Size: 499986 Color: 1
Size: 499969 Color: 0

Bin 1690: 46 of cap free
Amount of items: 2
Items: 
Size: 521783 Color: 0
Size: 478172 Color: 1

Bin 1691: 46 of cap free
Amount of items: 2
Items: 
Size: 522479 Color: 0
Size: 477476 Color: 1

Bin 1692: 46 of cap free
Amount of items: 2
Items: 
Size: 551589 Color: 1
Size: 448366 Color: 0

Bin 1693: 46 of cap free
Amount of items: 2
Items: 
Size: 563096 Color: 1
Size: 436859 Color: 0

Bin 1694: 46 of cap free
Amount of items: 2
Items: 
Size: 580755 Color: 1
Size: 419200 Color: 0

Bin 1695: 46 of cap free
Amount of items: 2
Items: 
Size: 606691 Color: 1
Size: 393264 Color: 0

Bin 1696: 46 of cap free
Amount of items: 2
Items: 
Size: 641024 Color: 0
Size: 358931 Color: 1

Bin 1697: 46 of cap free
Amount of items: 2
Items: 
Size: 647549 Color: 0
Size: 352406 Color: 1

Bin 1698: 46 of cap free
Amount of items: 2
Items: 
Size: 676762 Color: 1
Size: 323193 Color: 0

Bin 1699: 46 of cap free
Amount of items: 2
Items: 
Size: 677266 Color: 1
Size: 322689 Color: 0

Bin 1700: 46 of cap free
Amount of items: 2
Items: 
Size: 692863 Color: 0
Size: 307092 Color: 1

Bin 1701: 46 of cap free
Amount of items: 2
Items: 
Size: 718797 Color: 0
Size: 281158 Color: 1

Bin 1702: 46 of cap free
Amount of items: 2
Items: 
Size: 719903 Color: 1
Size: 280052 Color: 0

Bin 1703: 46 of cap free
Amount of items: 2
Items: 
Size: 730031 Color: 1
Size: 269924 Color: 0

Bin 1704: 46 of cap free
Amount of items: 2
Items: 
Size: 734966 Color: 0
Size: 264989 Color: 1

Bin 1705: 46 of cap free
Amount of items: 2
Items: 
Size: 744602 Color: 1
Size: 255353 Color: 0

Bin 1706: 46 of cap free
Amount of items: 2
Items: 
Size: 750482 Color: 1
Size: 249473 Color: 0

Bin 1707: 46 of cap free
Amount of items: 2
Items: 
Size: 752150 Color: 0
Size: 247805 Color: 1

Bin 1708: 46 of cap free
Amount of items: 2
Items: 
Size: 777782 Color: 0
Size: 222173 Color: 1

Bin 1709: 46 of cap free
Amount of items: 2
Items: 
Size: 779559 Color: 1
Size: 220396 Color: 0

Bin 1710: 46 of cap free
Amount of items: 2
Items: 
Size: 789190 Color: 1
Size: 210765 Color: 0

Bin 1711: 47 of cap free
Amount of items: 2
Items: 
Size: 526004 Color: 1
Size: 473950 Color: 0

Bin 1712: 47 of cap free
Amount of items: 2
Items: 
Size: 533333 Color: 1
Size: 466621 Color: 0

Bin 1713: 47 of cap free
Amount of items: 2
Items: 
Size: 646413 Color: 1
Size: 353541 Color: 0

Bin 1714: 47 of cap free
Amount of items: 2
Items: 
Size: 669272 Color: 1
Size: 330682 Color: 0

Bin 1715: 47 of cap free
Amount of items: 2
Items: 
Size: 680137 Color: 1
Size: 319817 Color: 0

Bin 1716: 47 of cap free
Amount of items: 2
Items: 
Size: 710682 Color: 0
Size: 289272 Color: 1

Bin 1717: 47 of cap free
Amount of items: 2
Items: 
Size: 731647 Color: 0
Size: 268307 Color: 1

Bin 1718: 47 of cap free
Amount of items: 2
Items: 
Size: 743589 Color: 0
Size: 256365 Color: 1

Bin 1719: 47 of cap free
Amount of items: 2
Items: 
Size: 753831 Color: 0
Size: 246123 Color: 1

Bin 1720: 47 of cap free
Amount of items: 2
Items: 
Size: 773498 Color: 1
Size: 226456 Color: 0

Bin 1721: 47 of cap free
Amount of items: 2
Items: 
Size: 773570 Color: 1
Size: 226384 Color: 0

Bin 1722: 47 of cap free
Amount of items: 2
Items: 
Size: 793780 Color: 0
Size: 206174 Color: 1

Bin 1723: 48 of cap free
Amount of items: 2
Items: 
Size: 500293 Color: 1
Size: 499660 Color: 0

Bin 1724: 48 of cap free
Amount of items: 2
Items: 
Size: 542716 Color: 1
Size: 457237 Color: 0

Bin 1725: 48 of cap free
Amount of items: 2
Items: 
Size: 579172 Color: 0
Size: 420781 Color: 1

Bin 1726: 48 of cap free
Amount of items: 2
Items: 
Size: 586603 Color: 0
Size: 413350 Color: 1

Bin 1727: 48 of cap free
Amount of items: 2
Items: 
Size: 595913 Color: 0
Size: 404040 Color: 1

Bin 1728: 48 of cap free
Amount of items: 2
Items: 
Size: 631639 Color: 0
Size: 368314 Color: 1

Bin 1729: 48 of cap free
Amount of items: 2
Items: 
Size: 635099 Color: 1
Size: 364854 Color: 0

Bin 1730: 48 of cap free
Amount of items: 2
Items: 
Size: 724792 Color: 1
Size: 275161 Color: 0

Bin 1731: 48 of cap free
Amount of items: 2
Items: 
Size: 754194 Color: 0
Size: 245759 Color: 1

Bin 1732: 48 of cap free
Amount of items: 2
Items: 
Size: 767892 Color: 0
Size: 232061 Color: 1

Bin 1733: 48 of cap free
Amount of items: 2
Items: 
Size: 791356 Color: 0
Size: 208597 Color: 1

Bin 1734: 49 of cap free
Amount of items: 3
Items: 
Size: 418715 Color: 1
Size: 396898 Color: 0
Size: 184339 Color: 0

Bin 1735: 49 of cap free
Amount of items: 2
Items: 
Size: 501493 Color: 0
Size: 498459 Color: 1

Bin 1736: 49 of cap free
Amount of items: 2
Items: 
Size: 502351 Color: 0
Size: 497601 Color: 1

Bin 1737: 49 of cap free
Amount of items: 2
Items: 
Size: 520195 Color: 1
Size: 479757 Color: 0

Bin 1738: 49 of cap free
Amount of items: 2
Items: 
Size: 551656 Color: 1
Size: 448296 Color: 0

Bin 1739: 49 of cap free
Amount of items: 2
Items: 
Size: 565097 Color: 0
Size: 434855 Color: 1

Bin 1740: 49 of cap free
Amount of items: 2
Items: 
Size: 600807 Color: 0
Size: 399145 Color: 1

Bin 1741: 49 of cap free
Amount of items: 2
Items: 
Size: 603710 Color: 1
Size: 396242 Color: 0

Bin 1742: 49 of cap free
Amount of items: 2
Items: 
Size: 607007 Color: 0
Size: 392945 Color: 1

Bin 1743: 49 of cap free
Amount of items: 2
Items: 
Size: 640980 Color: 1
Size: 358972 Color: 0

Bin 1744: 49 of cap free
Amount of items: 2
Items: 
Size: 660481 Color: 0
Size: 339471 Color: 1

Bin 1745: 49 of cap free
Amount of items: 2
Items: 
Size: 662116 Color: 0
Size: 337836 Color: 1

Bin 1746: 49 of cap free
Amount of items: 2
Items: 
Size: 672950 Color: 0
Size: 327002 Color: 1

Bin 1747: 49 of cap free
Amount of items: 2
Items: 
Size: 681298 Color: 1
Size: 318654 Color: 0

Bin 1748: 49 of cap free
Amount of items: 2
Items: 
Size: 683773 Color: 0
Size: 316179 Color: 1

Bin 1749: 49 of cap free
Amount of items: 2
Items: 
Size: 690110 Color: 1
Size: 309842 Color: 0

Bin 1750: 49 of cap free
Amount of items: 2
Items: 
Size: 750915 Color: 0
Size: 249037 Color: 1

Bin 1751: 49 of cap free
Amount of items: 2
Items: 
Size: 752004 Color: 0
Size: 247948 Color: 1

Bin 1752: 49 of cap free
Amount of items: 2
Items: 
Size: 763281 Color: 1
Size: 236671 Color: 0

Bin 1753: 49 of cap free
Amount of items: 3
Items: 
Size: 792245 Color: 1
Size: 107570 Color: 0
Size: 100137 Color: 0

Bin 1754: 50 of cap free
Amount of items: 2
Items: 
Size: 528162 Color: 0
Size: 471789 Color: 1

Bin 1755: 50 of cap free
Amount of items: 2
Items: 
Size: 538849 Color: 0
Size: 461102 Color: 1

Bin 1756: 50 of cap free
Amount of items: 2
Items: 
Size: 558112 Color: 0
Size: 441839 Color: 1

Bin 1757: 50 of cap free
Amount of items: 2
Items: 
Size: 563529 Color: 1
Size: 436422 Color: 0

Bin 1758: 50 of cap free
Amount of items: 2
Items: 
Size: 588682 Color: 1
Size: 411269 Color: 0

Bin 1759: 50 of cap free
Amount of items: 2
Items: 
Size: 599217 Color: 1
Size: 400734 Color: 0

Bin 1760: 50 of cap free
Amount of items: 2
Items: 
Size: 599886 Color: 1
Size: 400065 Color: 0

Bin 1761: 50 of cap free
Amount of items: 2
Items: 
Size: 601621 Color: 1
Size: 398330 Color: 0

Bin 1762: 50 of cap free
Amount of items: 2
Items: 
Size: 603145 Color: 0
Size: 396806 Color: 1

Bin 1763: 50 of cap free
Amount of items: 2
Items: 
Size: 616908 Color: 1
Size: 383043 Color: 0

Bin 1764: 50 of cap free
Amount of items: 2
Items: 
Size: 645095 Color: 0
Size: 354856 Color: 1

Bin 1765: 50 of cap free
Amount of items: 2
Items: 
Size: 658561 Color: 0
Size: 341390 Color: 1

Bin 1766: 50 of cap free
Amount of items: 2
Items: 
Size: 672179 Color: 0
Size: 327772 Color: 1

Bin 1767: 50 of cap free
Amount of items: 2
Items: 
Size: 689609 Color: 1
Size: 310342 Color: 0

Bin 1768: 50 of cap free
Amount of items: 2
Items: 
Size: 717715 Color: 1
Size: 282236 Color: 0

Bin 1769: 50 of cap free
Amount of items: 2
Items: 
Size: 722748 Color: 0
Size: 277203 Color: 1

Bin 1770: 50 of cap free
Amount of items: 2
Items: 
Size: 741228 Color: 1
Size: 258723 Color: 0

Bin 1771: 50 of cap free
Amount of items: 2
Items: 
Size: 778491 Color: 0
Size: 221460 Color: 1

Bin 1772: 50 of cap free
Amount of items: 3
Items: 
Size: 782560 Color: 0
Size: 112346 Color: 1
Size: 105045 Color: 1

Bin 1773: 50 of cap free
Amount of items: 2
Items: 
Size: 785726 Color: 1
Size: 214225 Color: 0

Bin 1774: 50 of cap free
Amount of items: 2
Items: 
Size: 798264 Color: 0
Size: 201687 Color: 1

Bin 1775: 51 of cap free
Amount of items: 2
Items: 
Size: 545578 Color: 0
Size: 454372 Color: 1

Bin 1776: 51 of cap free
Amount of items: 2
Items: 
Size: 547684 Color: 1
Size: 452266 Color: 0

Bin 1777: 51 of cap free
Amount of items: 2
Items: 
Size: 553915 Color: 1
Size: 446035 Color: 0

Bin 1778: 51 of cap free
Amount of items: 2
Items: 
Size: 557823 Color: 0
Size: 442127 Color: 1

Bin 1779: 51 of cap free
Amount of items: 2
Items: 
Size: 621684 Color: 0
Size: 378266 Color: 1

Bin 1780: 51 of cap free
Amount of items: 2
Items: 
Size: 628791 Color: 0
Size: 371159 Color: 1

Bin 1781: 51 of cap free
Amount of items: 2
Items: 
Size: 688522 Color: 0
Size: 311428 Color: 1

Bin 1782: 51 of cap free
Amount of items: 2
Items: 
Size: 696393 Color: 0
Size: 303557 Color: 1

Bin 1783: 51 of cap free
Amount of items: 2
Items: 
Size: 708221 Color: 1
Size: 291729 Color: 0

Bin 1784: 51 of cap free
Amount of items: 2
Items: 
Size: 724654 Color: 0
Size: 275296 Color: 1

Bin 1785: 51 of cap free
Amount of items: 2
Items: 
Size: 769172 Color: 1
Size: 230778 Color: 0

Bin 1786: 52 of cap free
Amount of items: 2
Items: 
Size: 505495 Color: 1
Size: 494454 Color: 0

Bin 1787: 52 of cap free
Amount of items: 2
Items: 
Size: 518980 Color: 0
Size: 480969 Color: 1

Bin 1788: 52 of cap free
Amount of items: 2
Items: 
Size: 521546 Color: 1
Size: 478403 Color: 0

Bin 1789: 52 of cap free
Amount of items: 2
Items: 
Size: 580701 Color: 0
Size: 419248 Color: 1

Bin 1790: 52 of cap free
Amount of items: 2
Items: 
Size: 609651 Color: 1
Size: 390298 Color: 0

Bin 1791: 52 of cap free
Amount of items: 2
Items: 
Size: 654348 Color: 0
Size: 345601 Color: 1

Bin 1792: 52 of cap free
Amount of items: 2
Items: 
Size: 656277 Color: 0
Size: 343672 Color: 1

Bin 1793: 52 of cap free
Amount of items: 2
Items: 
Size: 696015 Color: 0
Size: 303934 Color: 1

Bin 1794: 52 of cap free
Amount of items: 2
Items: 
Size: 736734 Color: 0
Size: 263215 Color: 1

Bin 1795: 52 of cap free
Amount of items: 2
Items: 
Size: 739028 Color: 1
Size: 260921 Color: 0

Bin 1796: 52 of cap free
Amount of items: 2
Items: 
Size: 745209 Color: 0
Size: 254740 Color: 1

Bin 1797: 52 of cap free
Amount of items: 2
Items: 
Size: 748899 Color: 1
Size: 251050 Color: 0

Bin 1798: 52 of cap free
Amount of items: 2
Items: 
Size: 797648 Color: 1
Size: 202301 Color: 0

Bin 1799: 53 of cap free
Amount of items: 2
Items: 
Size: 536332 Color: 1
Size: 463616 Color: 0

Bin 1800: 53 of cap free
Amount of items: 2
Items: 
Size: 539160 Color: 0
Size: 460788 Color: 1

Bin 1801: 53 of cap free
Amount of items: 2
Items: 
Size: 566928 Color: 1
Size: 433020 Color: 0

Bin 1802: 53 of cap free
Amount of items: 2
Items: 
Size: 575774 Color: 0
Size: 424174 Color: 1

Bin 1803: 53 of cap free
Amount of items: 2
Items: 
Size: 601757 Color: 0
Size: 398191 Color: 1

Bin 1804: 53 of cap free
Amount of items: 2
Items: 
Size: 607376 Color: 0
Size: 392572 Color: 1

Bin 1805: 53 of cap free
Amount of items: 2
Items: 
Size: 657748 Color: 0
Size: 342200 Color: 1

Bin 1806: 53 of cap free
Amount of items: 2
Items: 
Size: 686330 Color: 0
Size: 313618 Color: 1

Bin 1807: 53 of cap free
Amount of items: 2
Items: 
Size: 745345 Color: 1
Size: 254603 Color: 0

Bin 1808: 53 of cap free
Amount of items: 2
Items: 
Size: 751985 Color: 1
Size: 247963 Color: 0

Bin 1809: 54 of cap free
Amount of items: 6
Items: 
Size: 168369 Color: 0
Size: 168263 Color: 0
Size: 167796 Color: 1
Size: 167673 Color: 1
Size: 167613 Color: 1
Size: 160233 Color: 0

Bin 1810: 54 of cap free
Amount of items: 2
Items: 
Size: 532805 Color: 1
Size: 467142 Color: 0

Bin 1811: 54 of cap free
Amount of items: 2
Items: 
Size: 547139 Color: 0
Size: 452808 Color: 1

Bin 1812: 54 of cap free
Amount of items: 2
Items: 
Size: 574544 Color: 0
Size: 425403 Color: 1

Bin 1813: 54 of cap free
Amount of items: 2
Items: 
Size: 627092 Color: 1
Size: 372855 Color: 0

Bin 1814: 54 of cap free
Amount of items: 2
Items: 
Size: 638694 Color: 0
Size: 361253 Color: 1

Bin 1815: 54 of cap free
Amount of items: 2
Items: 
Size: 674438 Color: 1
Size: 325509 Color: 0

Bin 1816: 54 of cap free
Amount of items: 2
Items: 
Size: 694606 Color: 0
Size: 305341 Color: 1

Bin 1817: 54 of cap free
Amount of items: 2
Items: 
Size: 695160 Color: 1
Size: 304787 Color: 0

Bin 1818: 54 of cap free
Amount of items: 2
Items: 
Size: 698914 Color: 0
Size: 301033 Color: 1

Bin 1819: 54 of cap free
Amount of items: 2
Items: 
Size: 750778 Color: 0
Size: 249169 Color: 1

Bin 1820: 54 of cap free
Amount of items: 2
Items: 
Size: 766204 Color: 0
Size: 233743 Color: 1

Bin 1821: 54 of cap free
Amount of items: 2
Items: 
Size: 790828 Color: 0
Size: 209119 Color: 1

Bin 1822: 54 of cap free
Amount of items: 2
Items: 
Size: 794340 Color: 0
Size: 205607 Color: 1

Bin 1823: 55 of cap free
Amount of items: 2
Items: 
Size: 507540 Color: 1
Size: 492406 Color: 0

Bin 1824: 55 of cap free
Amount of items: 2
Items: 
Size: 518341 Color: 0
Size: 481605 Color: 1

Bin 1825: 55 of cap free
Amount of items: 2
Items: 
Size: 520190 Color: 1
Size: 479756 Color: 0

Bin 1826: 55 of cap free
Amount of items: 2
Items: 
Size: 562961 Color: 0
Size: 436985 Color: 1

Bin 1827: 55 of cap free
Amount of items: 2
Items: 
Size: 576436 Color: 1
Size: 423510 Color: 0

Bin 1828: 55 of cap free
Amount of items: 2
Items: 
Size: 578720 Color: 0
Size: 421226 Color: 1

Bin 1829: 55 of cap free
Amount of items: 2
Items: 
Size: 585651 Color: 1
Size: 414295 Color: 0

Bin 1830: 55 of cap free
Amount of items: 2
Items: 
Size: 668654 Color: 1
Size: 331292 Color: 0

Bin 1831: 55 of cap free
Amount of items: 2
Items: 
Size: 710138 Color: 0
Size: 289808 Color: 1

Bin 1832: 55 of cap free
Amount of items: 2
Items: 
Size: 718921 Color: 1
Size: 281025 Color: 0

Bin 1833: 55 of cap free
Amount of items: 2
Items: 
Size: 763808 Color: 0
Size: 236138 Color: 1

Bin 1834: 55 of cap free
Amount of items: 2
Items: 
Size: 774265 Color: 0
Size: 225681 Color: 1

Bin 1835: 55 of cap free
Amount of items: 2
Items: 
Size: 785342 Color: 0
Size: 214604 Color: 1

Bin 1836: 55 of cap free
Amount of items: 2
Items: 
Size: 790538 Color: 0
Size: 209408 Color: 1

Bin 1837: 56 of cap free
Amount of items: 3
Items: 
Size: 405966 Color: 1
Size: 404381 Color: 0
Size: 189598 Color: 0

Bin 1838: 56 of cap free
Amount of items: 2
Items: 
Size: 504453 Color: 0
Size: 495492 Color: 1

Bin 1839: 56 of cap free
Amount of items: 2
Items: 
Size: 518524 Color: 1
Size: 481421 Color: 0

Bin 1840: 56 of cap free
Amount of items: 2
Items: 
Size: 523661 Color: 0
Size: 476284 Color: 1

Bin 1841: 56 of cap free
Amount of items: 2
Items: 
Size: 528687 Color: 1
Size: 471258 Color: 0

Bin 1842: 56 of cap free
Amount of items: 2
Items: 
Size: 579872 Color: 1
Size: 420073 Color: 0

Bin 1843: 56 of cap free
Amount of items: 2
Items: 
Size: 602160 Color: 0
Size: 397785 Color: 1

Bin 1844: 56 of cap free
Amount of items: 2
Items: 
Size: 620818 Color: 0
Size: 379127 Color: 1

Bin 1845: 56 of cap free
Amount of items: 2
Items: 
Size: 654425 Color: 0
Size: 345520 Color: 1

Bin 1846: 56 of cap free
Amount of items: 2
Items: 
Size: 698587 Color: 0
Size: 301358 Color: 1

Bin 1847: 56 of cap free
Amount of items: 2
Items: 
Size: 713528 Color: 0
Size: 286417 Color: 1

Bin 1848: 56 of cap free
Amount of items: 2
Items: 
Size: 727326 Color: 1
Size: 272619 Color: 0

Bin 1849: 56 of cap free
Amount of items: 2
Items: 
Size: 749385 Color: 1
Size: 250560 Color: 0

Bin 1850: 56 of cap free
Amount of items: 2
Items: 
Size: 777419 Color: 1
Size: 222526 Color: 0

Bin 1851: 57 of cap free
Amount of items: 2
Items: 
Size: 522198 Color: 0
Size: 477746 Color: 1

Bin 1852: 57 of cap free
Amount of items: 2
Items: 
Size: 522872 Color: 1
Size: 477072 Color: 0

Bin 1853: 57 of cap free
Amount of items: 2
Items: 
Size: 526433 Color: 1
Size: 473511 Color: 0

Bin 1854: 57 of cap free
Amount of items: 2
Items: 
Size: 536797 Color: 1
Size: 463147 Color: 0

Bin 1855: 57 of cap free
Amount of items: 2
Items: 
Size: 557243 Color: 1
Size: 442701 Color: 0

Bin 1856: 57 of cap free
Amount of items: 2
Items: 
Size: 570716 Color: 1
Size: 429228 Color: 0

Bin 1857: 57 of cap free
Amount of items: 2
Items: 
Size: 576985 Color: 0
Size: 422959 Color: 1

Bin 1858: 57 of cap free
Amount of items: 2
Items: 
Size: 581956 Color: 0
Size: 417988 Color: 1

Bin 1859: 57 of cap free
Amount of items: 2
Items: 
Size: 589746 Color: 0
Size: 410198 Color: 1

Bin 1860: 57 of cap free
Amount of items: 2
Items: 
Size: 604012 Color: 0
Size: 395932 Color: 1

Bin 1861: 57 of cap free
Amount of items: 2
Items: 
Size: 625038 Color: 1
Size: 374906 Color: 0

Bin 1862: 57 of cap free
Amount of items: 2
Items: 
Size: 660405 Color: 1
Size: 339539 Color: 0

Bin 1863: 57 of cap free
Amount of items: 2
Items: 
Size: 663198 Color: 0
Size: 336746 Color: 1

Bin 1864: 57 of cap free
Amount of items: 2
Items: 
Size: 667758 Color: 0
Size: 332186 Color: 1

Bin 1865: 57 of cap free
Amount of items: 2
Items: 
Size: 672551 Color: 0
Size: 327393 Color: 1

Bin 1866: 57 of cap free
Amount of items: 2
Items: 
Size: 679281 Color: 1
Size: 320663 Color: 0

Bin 1867: 57 of cap free
Amount of items: 2
Items: 
Size: 693361 Color: 0
Size: 306583 Color: 1

Bin 1868: 57 of cap free
Amount of items: 2
Items: 
Size: 735079 Color: 0
Size: 264865 Color: 1

Bin 1869: 57 of cap free
Amount of items: 2
Items: 
Size: 782746 Color: 1
Size: 217198 Color: 0

Bin 1870: 58 of cap free
Amount of items: 2
Items: 
Size: 539406 Color: 1
Size: 460537 Color: 0

Bin 1871: 58 of cap free
Amount of items: 2
Items: 
Size: 577199 Color: 1
Size: 422744 Color: 0

Bin 1872: 58 of cap free
Amount of items: 2
Items: 
Size: 592564 Color: 1
Size: 407379 Color: 0

Bin 1873: 58 of cap free
Amount of items: 2
Items: 
Size: 645433 Color: 0
Size: 354510 Color: 1

Bin 1874: 58 of cap free
Amount of items: 2
Items: 
Size: 660207 Color: 0
Size: 339736 Color: 1

Bin 1875: 58 of cap free
Amount of items: 2
Items: 
Size: 672287 Color: 1
Size: 327656 Color: 0

Bin 1876: 58 of cap free
Amount of items: 2
Items: 
Size: 675010 Color: 1
Size: 324933 Color: 0

Bin 1877: 58 of cap free
Amount of items: 2
Items: 
Size: 690192 Color: 0
Size: 309751 Color: 1

Bin 1878: 58 of cap free
Amount of items: 2
Items: 
Size: 736261 Color: 0
Size: 263682 Color: 1

Bin 1879: 58 of cap free
Amount of items: 2
Items: 
Size: 737713 Color: 0
Size: 262230 Color: 1

Bin 1880: 58 of cap free
Amount of items: 2
Items: 
Size: 739093 Color: 0
Size: 260850 Color: 1

Bin 1881: 58 of cap free
Amount of items: 2
Items: 
Size: 786684 Color: 1
Size: 213259 Color: 0

Bin 1882: 59 of cap free
Amount of items: 2
Items: 
Size: 504771 Color: 1
Size: 495171 Color: 0

Bin 1883: 59 of cap free
Amount of items: 2
Items: 
Size: 511662 Color: 1
Size: 488280 Color: 0

Bin 1884: 59 of cap free
Amount of items: 2
Items: 
Size: 532128 Color: 1
Size: 467814 Color: 0

Bin 1885: 59 of cap free
Amount of items: 2
Items: 
Size: 542445 Color: 0
Size: 457497 Color: 1

Bin 1886: 59 of cap free
Amount of items: 2
Items: 
Size: 563399 Color: 1
Size: 436543 Color: 0

Bin 1887: 59 of cap free
Amount of items: 2
Items: 
Size: 583894 Color: 0
Size: 416048 Color: 1

Bin 1888: 59 of cap free
Amount of items: 2
Items: 
Size: 614000 Color: 1
Size: 385942 Color: 0

Bin 1889: 59 of cap free
Amount of items: 2
Items: 
Size: 639450 Color: 1
Size: 360492 Color: 0

Bin 1890: 59 of cap free
Amount of items: 2
Items: 
Size: 649892 Color: 0
Size: 350050 Color: 1

Bin 1891: 59 of cap free
Amount of items: 2
Items: 
Size: 674747 Color: 0
Size: 325195 Color: 1

Bin 1892: 59 of cap free
Amount of items: 2
Items: 
Size: 690196 Color: 1
Size: 309746 Color: 0

Bin 1893: 60 of cap free
Amount of items: 2
Items: 
Size: 528685 Color: 1
Size: 471256 Color: 0

Bin 1894: 60 of cap free
Amount of items: 2
Items: 
Size: 531653 Color: 1
Size: 468288 Color: 0

Bin 1895: 60 of cap free
Amount of items: 2
Items: 
Size: 591558 Color: 0
Size: 408383 Color: 1

Bin 1896: 60 of cap free
Amount of items: 2
Items: 
Size: 592525 Color: 0
Size: 407416 Color: 1

Bin 1897: 60 of cap free
Amount of items: 2
Items: 
Size: 612823 Color: 0
Size: 387118 Color: 1

Bin 1898: 60 of cap free
Amount of items: 2
Items: 
Size: 690392 Color: 1
Size: 309549 Color: 0

Bin 1899: 60 of cap free
Amount of items: 2
Items: 
Size: 718604 Color: 0
Size: 281337 Color: 1

Bin 1900: 60 of cap free
Amount of items: 2
Items: 
Size: 737532 Color: 1
Size: 262409 Color: 0

Bin 1901: 60 of cap free
Amount of items: 2
Items: 
Size: 756691 Color: 1
Size: 243250 Color: 0

Bin 1902: 60 of cap free
Amount of items: 2
Items: 
Size: 772914 Color: 0
Size: 227027 Color: 1

Bin 1903: 60 of cap free
Amount of items: 3
Items: 
Size: 776193 Color: 0
Size: 112744 Color: 1
Size: 111004 Color: 0

Bin 1904: 60 of cap free
Amount of items: 2
Items: 
Size: 780965 Color: 0
Size: 218976 Color: 1

Bin 1905: 61 of cap free
Amount of items: 2
Items: 
Size: 504095 Color: 0
Size: 495845 Color: 1

Bin 1906: 61 of cap free
Amount of items: 2
Items: 
Size: 504295 Color: 0
Size: 495645 Color: 1

Bin 1907: 61 of cap free
Amount of items: 2
Items: 
Size: 551239 Color: 0
Size: 448701 Color: 1

Bin 1908: 61 of cap free
Amount of items: 2
Items: 
Size: 569305 Color: 0
Size: 430635 Color: 1

Bin 1909: 61 of cap free
Amount of items: 2
Items: 
Size: 590201 Color: 0
Size: 409739 Color: 1

Bin 1910: 61 of cap free
Amount of items: 2
Items: 
Size: 590858 Color: 0
Size: 409082 Color: 1

Bin 1911: 61 of cap free
Amount of items: 2
Items: 
Size: 620123 Color: 0
Size: 379817 Color: 1

Bin 1912: 61 of cap free
Amount of items: 2
Items: 
Size: 645274 Color: 1
Size: 354666 Color: 0

Bin 1913: 61 of cap free
Amount of items: 2
Items: 
Size: 687011 Color: 0
Size: 312929 Color: 1

Bin 1914: 61 of cap free
Amount of items: 2
Items: 
Size: 747032 Color: 1
Size: 252908 Color: 0

Bin 1915: 61 of cap free
Amount of items: 2
Items: 
Size: 756068 Color: 1
Size: 243872 Color: 0

Bin 1916: 62 of cap free
Amount of items: 2
Items: 
Size: 511819 Color: 1
Size: 488120 Color: 0

Bin 1917: 62 of cap free
Amount of items: 2
Items: 
Size: 551954 Color: 0
Size: 447985 Color: 1

Bin 1918: 62 of cap free
Amount of items: 2
Items: 
Size: 572122 Color: 1
Size: 427817 Color: 0

Bin 1919: 62 of cap free
Amount of items: 2
Items: 
Size: 575399 Color: 1
Size: 424540 Color: 0

Bin 1920: 62 of cap free
Amount of items: 2
Items: 
Size: 602804 Color: 1
Size: 397135 Color: 0

Bin 1921: 62 of cap free
Amount of items: 2
Items: 
Size: 605268 Color: 0
Size: 394671 Color: 1

Bin 1922: 62 of cap free
Amount of items: 2
Items: 
Size: 639439 Color: 0
Size: 360500 Color: 1

Bin 1923: 62 of cap free
Amount of items: 2
Items: 
Size: 658846 Color: 0
Size: 341093 Color: 1

Bin 1924: 62 of cap free
Amount of items: 2
Items: 
Size: 667640 Color: 0
Size: 332299 Color: 1

Bin 1925: 62 of cap free
Amount of items: 2
Items: 
Size: 680269 Color: 0
Size: 319670 Color: 1

Bin 1926: 62 of cap free
Amount of items: 2
Items: 
Size: 734499 Color: 1
Size: 265440 Color: 0

Bin 1927: 62 of cap free
Amount of items: 2
Items: 
Size: 737926 Color: 0
Size: 262013 Color: 1

Bin 1928: 62 of cap free
Amount of items: 2
Items: 
Size: 740895 Color: 0
Size: 259044 Color: 1

Bin 1929: 62 of cap free
Amount of items: 2
Items: 
Size: 749905 Color: 1
Size: 250034 Color: 0

Bin 1930: 62 of cap free
Amount of items: 2
Items: 
Size: 757728 Color: 0
Size: 242211 Color: 1

Bin 1931: 62 of cap free
Amount of items: 2
Items: 
Size: 758836 Color: 0
Size: 241103 Color: 1

Bin 1932: 62 of cap free
Amount of items: 2
Items: 
Size: 786814 Color: 1
Size: 213125 Color: 0

Bin 1933: 63 of cap free
Amount of items: 2
Items: 
Size: 530631 Color: 1
Size: 469307 Color: 0

Bin 1934: 63 of cap free
Amount of items: 2
Items: 
Size: 548466 Color: 1
Size: 451472 Color: 0

Bin 1935: 63 of cap free
Amount of items: 2
Items: 
Size: 570111 Color: 1
Size: 429827 Color: 0

Bin 1936: 63 of cap free
Amount of items: 2
Items: 
Size: 570832 Color: 1
Size: 429106 Color: 0

Bin 1937: 63 of cap free
Amount of items: 2
Items: 
Size: 584471 Color: 0
Size: 415467 Color: 1

Bin 1938: 63 of cap free
Amount of items: 2
Items: 
Size: 589046 Color: 0
Size: 410892 Color: 1

Bin 1939: 63 of cap free
Amount of items: 2
Items: 
Size: 591604 Color: 1
Size: 408334 Color: 0

Bin 1940: 63 of cap free
Amount of items: 2
Items: 
Size: 592228 Color: 0
Size: 407710 Color: 1

Bin 1941: 63 of cap free
Amount of items: 3
Items: 
Size: 594078 Color: 0
Size: 273680 Color: 1
Size: 132180 Color: 1

Bin 1942: 63 of cap free
Amount of items: 2
Items: 
Size: 611409 Color: 0
Size: 388529 Color: 1

Bin 1943: 63 of cap free
Amount of items: 2
Items: 
Size: 640493 Color: 0
Size: 359445 Color: 1

Bin 1944: 63 of cap free
Amount of items: 2
Items: 
Size: 647327 Color: 0
Size: 352611 Color: 1

Bin 1945: 63 of cap free
Amount of items: 2
Items: 
Size: 656880 Color: 0
Size: 343058 Color: 1

Bin 1946: 63 of cap free
Amount of items: 2
Items: 
Size: 717137 Color: 1
Size: 282801 Color: 0

Bin 1947: 63 of cap free
Amount of items: 2
Items: 
Size: 742389 Color: 1
Size: 257549 Color: 0

Bin 1948: 63 of cap free
Amount of items: 2
Items: 
Size: 777368 Color: 0
Size: 222570 Color: 1

Bin 1949: 64 of cap free
Amount of items: 3
Items: 
Size: 413972 Color: 0
Size: 383161 Color: 0
Size: 202804 Color: 1

Bin 1950: 64 of cap free
Amount of items: 2
Items: 
Size: 562170 Color: 1
Size: 437767 Color: 0

Bin 1951: 64 of cap free
Amount of items: 2
Items: 
Size: 571226 Color: 1
Size: 428711 Color: 0

Bin 1952: 64 of cap free
Amount of items: 2
Items: 
Size: 578907 Color: 1
Size: 421030 Color: 0

Bin 1953: 64 of cap free
Amount of items: 2
Items: 
Size: 586147 Color: 1
Size: 413790 Color: 0

Bin 1954: 64 of cap free
Amount of items: 2
Items: 
Size: 615159 Color: 1
Size: 384778 Color: 0

Bin 1955: 64 of cap free
Amount of items: 2
Items: 
Size: 624985 Color: 0
Size: 374952 Color: 1

Bin 1956: 64 of cap free
Amount of items: 2
Items: 
Size: 629190 Color: 0
Size: 370747 Color: 1

Bin 1957: 64 of cap free
Amount of items: 2
Items: 
Size: 634556 Color: 1
Size: 365381 Color: 0

Bin 1958: 64 of cap free
Amount of items: 2
Items: 
Size: 636429 Color: 0
Size: 363508 Color: 1

Bin 1959: 64 of cap free
Amount of items: 2
Items: 
Size: 703633 Color: 1
Size: 296304 Color: 0

Bin 1960: 64 of cap free
Amount of items: 2
Items: 
Size: 711811 Color: 0
Size: 288126 Color: 1

Bin 1961: 64 of cap free
Amount of items: 2
Items: 
Size: 736398 Color: 1
Size: 263539 Color: 0

Bin 1962: 64 of cap free
Amount of items: 2
Items: 
Size: 771345 Color: 1
Size: 228592 Color: 0

Bin 1963: 65 of cap free
Amount of items: 2
Items: 
Size: 525324 Color: 0
Size: 474612 Color: 1

Bin 1964: 65 of cap free
Amount of items: 2
Items: 
Size: 535655 Color: 1
Size: 464281 Color: 0

Bin 1965: 65 of cap free
Amount of items: 2
Items: 
Size: 536151 Color: 1
Size: 463785 Color: 0

Bin 1966: 65 of cap free
Amount of items: 2
Items: 
Size: 576163 Color: 0
Size: 423773 Color: 1

Bin 1967: 65 of cap free
Amount of items: 2
Items: 
Size: 597869 Color: 0
Size: 402067 Color: 1

Bin 1968: 65 of cap free
Amount of items: 2
Items: 
Size: 677193 Color: 0
Size: 322743 Color: 1

Bin 1969: 65 of cap free
Amount of items: 2
Items: 
Size: 682212 Color: 1
Size: 317724 Color: 0

Bin 1970: 65 of cap free
Amount of items: 2
Items: 
Size: 713835 Color: 1
Size: 286101 Color: 0

Bin 1971: 65 of cap free
Amount of items: 2
Items: 
Size: 745075 Color: 1
Size: 254861 Color: 0

Bin 1972: 65 of cap free
Amount of items: 2
Items: 
Size: 753718 Color: 0
Size: 246218 Color: 1

Bin 1973: 65 of cap free
Amount of items: 3
Items: 
Size: 794125 Color: 1
Size: 105417 Color: 0
Size: 100394 Color: 0

Bin 1974: 65 of cap free
Amount of items: 2
Items: 
Size: 794569 Color: 0
Size: 205367 Color: 1

Bin 1975: 66 of cap free
Amount of items: 2
Items: 
Size: 503321 Color: 1
Size: 496614 Color: 0

Bin 1976: 66 of cap free
Amount of items: 2
Items: 
Size: 604558 Color: 0
Size: 395377 Color: 1

Bin 1977: 66 of cap free
Amount of items: 2
Items: 
Size: 696893 Color: 0
Size: 303042 Color: 1

Bin 1978: 66 of cap free
Amount of items: 2
Items: 
Size: 738833 Color: 1
Size: 261102 Color: 0

Bin 1979: 66 of cap free
Amount of items: 2
Items: 
Size: 764873 Color: 0
Size: 235062 Color: 1

Bin 1980: 66 of cap free
Amount of items: 2
Items: 
Size: 779817 Color: 1
Size: 220118 Color: 0

Bin 1981: 66 of cap free
Amount of items: 3
Items: 
Size: 786519 Color: 0
Size: 111119 Color: 1
Size: 102297 Color: 1

Bin 1982: 66 of cap free
Amount of items: 2
Items: 
Size: 788538 Color: 0
Size: 211397 Color: 1

Bin 1983: 67 of cap free
Amount of items: 2
Items: 
Size: 515525 Color: 1
Size: 484409 Color: 0

Bin 1984: 67 of cap free
Amount of items: 2
Items: 
Size: 531793 Color: 1
Size: 468141 Color: 0

Bin 1985: 67 of cap free
Amount of items: 2
Items: 
Size: 541530 Color: 1
Size: 458404 Color: 0

Bin 1986: 67 of cap free
Amount of items: 2
Items: 
Size: 554044 Color: 1
Size: 445890 Color: 0

Bin 1987: 67 of cap free
Amount of items: 2
Items: 
Size: 581709 Color: 0
Size: 418225 Color: 1

Bin 1988: 67 of cap free
Amount of items: 2
Items: 
Size: 585878 Color: 1
Size: 414056 Color: 0

Bin 1989: 67 of cap free
Amount of items: 2
Items: 
Size: 691041 Color: 1
Size: 308893 Color: 0

Bin 1990: 67 of cap free
Amount of items: 2
Items: 
Size: 700034 Color: 1
Size: 299900 Color: 0

Bin 1991: 67 of cap free
Amount of items: 2
Items: 
Size: 718415 Color: 1
Size: 281519 Color: 0

Bin 1992: 67 of cap free
Amount of items: 2
Items: 
Size: 733092 Color: 0
Size: 266842 Color: 1

Bin 1993: 67 of cap free
Amount of items: 2
Items: 
Size: 754656 Color: 0
Size: 245278 Color: 1

Bin 1994: 67 of cap free
Amount of items: 2
Items: 
Size: 778929 Color: 0
Size: 221005 Color: 1

Bin 1995: 68 of cap free
Amount of items: 2
Items: 
Size: 556493 Color: 0
Size: 443440 Color: 1

Bin 1996: 68 of cap free
Amount of items: 2
Items: 
Size: 559325 Color: 0
Size: 440608 Color: 1

Bin 1997: 68 of cap free
Amount of items: 2
Items: 
Size: 615293 Color: 0
Size: 384640 Color: 1

Bin 1998: 68 of cap free
Amount of items: 2
Items: 
Size: 646161 Color: 0
Size: 353772 Color: 1

Bin 1999: 68 of cap free
Amount of items: 2
Items: 
Size: 651162 Color: 0
Size: 348771 Color: 1

Bin 2000: 68 of cap free
Amount of items: 2
Items: 
Size: 651977 Color: 1
Size: 347956 Color: 0

Bin 2001: 68 of cap free
Amount of items: 2
Items: 
Size: 656029 Color: 0
Size: 343904 Color: 1

Bin 2002: 68 of cap free
Amount of items: 2
Items: 
Size: 672732 Color: 0
Size: 327201 Color: 1

Bin 2003: 68 of cap free
Amount of items: 2
Items: 
Size: 705727 Color: 0
Size: 294206 Color: 1

Bin 2004: 68 of cap free
Amount of items: 2
Items: 
Size: 719703 Color: 1
Size: 280230 Color: 0

Bin 2005: 68 of cap free
Amount of items: 2
Items: 
Size: 745944 Color: 0
Size: 253989 Color: 1

Bin 2006: 68 of cap free
Amount of items: 2
Items: 
Size: 758906 Color: 1
Size: 241027 Color: 0

Bin 2007: 68 of cap free
Amount of items: 2
Items: 
Size: 764723 Color: 1
Size: 235210 Color: 0

Bin 2008: 68 of cap free
Amount of items: 2
Items: 
Size: 765945 Color: 1
Size: 233988 Color: 0

Bin 2009: 68 of cap free
Amount of items: 2
Items: 
Size: 777095 Color: 1
Size: 222838 Color: 0

Bin 2010: 68 of cap free
Amount of items: 2
Items: 
Size: 780979 Color: 1
Size: 218954 Color: 0

Bin 2011: 68 of cap free
Amount of items: 2
Items: 
Size: 792570 Color: 0
Size: 207363 Color: 1

Bin 2012: 69 of cap free
Amount of items: 2
Items: 
Size: 505569 Color: 1
Size: 494363 Color: 0

Bin 2013: 69 of cap free
Amount of items: 2
Items: 
Size: 531718 Color: 1
Size: 468214 Color: 0

Bin 2014: 69 of cap free
Amount of items: 2
Items: 
Size: 555335 Color: 0
Size: 444597 Color: 1

Bin 2015: 69 of cap free
Amount of items: 2
Items: 
Size: 583821 Color: 0
Size: 416111 Color: 1

Bin 2016: 69 of cap free
Amount of items: 2
Items: 
Size: 591720 Color: 1
Size: 408212 Color: 0

Bin 2017: 69 of cap free
Amount of items: 2
Items: 
Size: 596191 Color: 1
Size: 403741 Color: 0

Bin 2018: 69 of cap free
Amount of items: 2
Items: 
Size: 616071 Color: 1
Size: 383861 Color: 0

Bin 2019: 69 of cap free
Amount of items: 2
Items: 
Size: 622200 Color: 0
Size: 377732 Color: 1

Bin 2020: 69 of cap free
Amount of items: 2
Items: 
Size: 625274 Color: 0
Size: 374658 Color: 1

Bin 2021: 69 of cap free
Amount of items: 2
Items: 
Size: 692487 Color: 1
Size: 307445 Color: 0

Bin 2022: 69 of cap free
Amount of items: 2
Items: 
Size: 732260 Color: 1
Size: 267672 Color: 0

Bin 2023: 69 of cap free
Amount of items: 2
Items: 
Size: 746351 Color: 1
Size: 253581 Color: 0

Bin 2024: 69 of cap free
Amount of items: 2
Items: 
Size: 760505 Color: 0
Size: 239427 Color: 1

Bin 2025: 69 of cap free
Amount of items: 2
Items: 
Size: 784229 Color: 0
Size: 215703 Color: 1

Bin 2026: 69 of cap free
Amount of items: 2
Items: 
Size: 793532 Color: 1
Size: 206400 Color: 0

Bin 2027: 70 of cap free
Amount of items: 3
Items: 
Size: 406720 Color: 1
Size: 404236 Color: 0
Size: 188975 Color: 1

Bin 2028: 70 of cap free
Amount of items: 2
Items: 
Size: 509093 Color: 1
Size: 490838 Color: 0

Bin 2029: 70 of cap free
Amount of items: 2
Items: 
Size: 525039 Color: 0
Size: 474892 Color: 1

Bin 2030: 70 of cap free
Amount of items: 2
Items: 
Size: 606761 Color: 1
Size: 393170 Color: 0

Bin 2031: 70 of cap free
Amount of items: 2
Items: 
Size: 661248 Color: 1
Size: 338683 Color: 0

Bin 2032: 70 of cap free
Amount of items: 2
Items: 
Size: 717853 Color: 0
Size: 282078 Color: 1

Bin 2033: 70 of cap free
Amount of items: 2
Items: 
Size: 720471 Color: 1
Size: 279460 Color: 0

Bin 2034: 70 of cap free
Amount of items: 2
Items: 
Size: 721108 Color: 0
Size: 278823 Color: 1

Bin 2035: 70 of cap free
Amount of items: 2
Items: 
Size: 723243 Color: 1
Size: 276688 Color: 0

Bin 2036: 70 of cap free
Amount of items: 2
Items: 
Size: 738284 Color: 0
Size: 261647 Color: 1

Bin 2037: 70 of cap free
Amount of items: 2
Items: 
Size: 775761 Color: 0
Size: 224170 Color: 1

Bin 2038: 70 of cap free
Amount of items: 2
Items: 
Size: 798202 Color: 1
Size: 201729 Color: 0

Bin 2039: 71 of cap free
Amount of items: 2
Items: 
Size: 516153 Color: 1
Size: 483777 Color: 0

Bin 2040: 71 of cap free
Amount of items: 2
Items: 
Size: 520851 Color: 1
Size: 479079 Color: 0

Bin 2041: 71 of cap free
Amount of items: 2
Items: 
Size: 591802 Color: 1
Size: 408128 Color: 0

Bin 2042: 71 of cap free
Amount of items: 2
Items: 
Size: 646758 Color: 0
Size: 353172 Color: 1

Bin 2043: 71 of cap free
Amount of items: 2
Items: 
Size: 677404 Color: 0
Size: 322526 Color: 1

Bin 2044: 71 of cap free
Amount of items: 2
Items: 
Size: 730309 Color: 0
Size: 269621 Color: 1

Bin 2045: 71 of cap free
Amount of items: 2
Items: 
Size: 745195 Color: 0
Size: 254735 Color: 1

Bin 2046: 72 of cap free
Amount of items: 2
Items: 
Size: 515856 Color: 1
Size: 484073 Color: 0

Bin 2047: 72 of cap free
Amount of items: 2
Items: 
Size: 521507 Color: 0
Size: 478422 Color: 1

Bin 2048: 72 of cap free
Amount of items: 2
Items: 
Size: 535275 Color: 0
Size: 464654 Color: 1

Bin 2049: 72 of cap free
Amount of items: 2
Items: 
Size: 539068 Color: 0
Size: 460861 Color: 1

Bin 2050: 72 of cap free
Amount of items: 2
Items: 
Size: 546783 Color: 1
Size: 453146 Color: 0

Bin 2051: 72 of cap free
Amount of items: 2
Items: 
Size: 555246 Color: 0
Size: 444683 Color: 1

Bin 2052: 72 of cap free
Amount of items: 2
Items: 
Size: 588337 Color: 0
Size: 411592 Color: 1

Bin 2053: 72 of cap free
Amount of items: 2
Items: 
Size: 591952 Color: 0
Size: 407977 Color: 1

Bin 2054: 72 of cap free
Amount of items: 2
Items: 
Size: 627042 Color: 0
Size: 372887 Color: 1

Bin 2055: 72 of cap free
Amount of items: 2
Items: 
Size: 657242 Color: 0
Size: 342687 Color: 1

Bin 2056: 72 of cap free
Amount of items: 2
Items: 
Size: 683816 Color: 1
Size: 316113 Color: 0

Bin 2057: 72 of cap free
Amount of items: 2
Items: 
Size: 693327 Color: 1
Size: 306602 Color: 0

Bin 2058: 72 of cap free
Amount of items: 2
Items: 
Size: 697415 Color: 1
Size: 302514 Color: 0

Bin 2059: 72 of cap free
Amount of items: 2
Items: 
Size: 784571 Color: 1
Size: 215358 Color: 0

Bin 2060: 72 of cap free
Amount of items: 2
Items: 
Size: 793683 Color: 0
Size: 206246 Color: 1

Bin 2061: 73 of cap free
Amount of items: 2
Items: 
Size: 503178 Color: 1
Size: 496750 Color: 0

Bin 2062: 73 of cap free
Amount of items: 2
Items: 
Size: 558675 Color: 0
Size: 441253 Color: 1

Bin 2063: 73 of cap free
Amount of items: 2
Items: 
Size: 588084 Color: 0
Size: 411844 Color: 1

Bin 2064: 73 of cap free
Amount of items: 2
Items: 
Size: 777293 Color: 1
Size: 222635 Color: 0

Bin 2065: 73 of cap free
Amount of items: 2
Items: 
Size: 785537 Color: 0
Size: 214391 Color: 1

Bin 2066: 74 of cap free
Amount of items: 2
Items: 
Size: 535339 Color: 1
Size: 464588 Color: 0

Bin 2067: 74 of cap free
Amount of items: 2
Items: 
Size: 567796 Color: 1
Size: 432131 Color: 0

Bin 2068: 74 of cap free
Amount of items: 2
Items: 
Size: 569394 Color: 1
Size: 430533 Color: 0

Bin 2069: 74 of cap free
Amount of items: 2
Items: 
Size: 580557 Color: 1
Size: 419370 Color: 0

Bin 2070: 74 of cap free
Amount of items: 2
Items: 
Size: 585094 Color: 0
Size: 414833 Color: 1

Bin 2071: 74 of cap free
Amount of items: 2
Items: 
Size: 588288 Color: 1
Size: 411639 Color: 0

Bin 2072: 74 of cap free
Amount of items: 2
Items: 
Size: 634720 Color: 1
Size: 365207 Color: 0

Bin 2073: 74 of cap free
Amount of items: 2
Items: 
Size: 653921 Color: 0
Size: 346006 Color: 1

Bin 2074: 74 of cap free
Amount of items: 2
Items: 
Size: 660241 Color: 1
Size: 339686 Color: 0

Bin 2075: 74 of cap free
Amount of items: 2
Items: 
Size: 730058 Color: 0
Size: 269869 Color: 1

Bin 2076: 74 of cap free
Amount of items: 2
Items: 
Size: 788425 Color: 0
Size: 211502 Color: 1

Bin 2077: 75 of cap free
Amount of items: 2
Items: 
Size: 503410 Color: 0
Size: 496516 Color: 1

Bin 2078: 75 of cap free
Amount of items: 2
Items: 
Size: 552881 Color: 0
Size: 447045 Color: 1

Bin 2079: 75 of cap free
Amount of items: 2
Items: 
Size: 615442 Color: 0
Size: 384484 Color: 1

Bin 2080: 75 of cap free
Amount of items: 2
Items: 
Size: 661955 Color: 1
Size: 337971 Color: 0

Bin 2081: 75 of cap free
Amount of items: 2
Items: 
Size: 678414 Color: 1
Size: 321512 Color: 0

Bin 2082: 75 of cap free
Amount of items: 2
Items: 
Size: 698396 Color: 1
Size: 301530 Color: 0

Bin 2083: 75 of cap free
Amount of items: 2
Items: 
Size: 707783 Color: 0
Size: 292143 Color: 1

Bin 2084: 75 of cap free
Amount of items: 2
Items: 
Size: 720458 Color: 0
Size: 279468 Color: 1

Bin 2085: 75 of cap free
Amount of items: 2
Items: 
Size: 726399 Color: 0
Size: 273527 Color: 1

Bin 2086: 75 of cap free
Amount of items: 2
Items: 
Size: 772009 Color: 0
Size: 227917 Color: 1

Bin 2087: 75 of cap free
Amount of items: 2
Items: 
Size: 779777 Color: 0
Size: 220149 Color: 1

Bin 2088: 75 of cap free
Amount of items: 3
Items: 
Size: 788765 Color: 1
Size: 108303 Color: 0
Size: 102858 Color: 0

Bin 2089: 75 of cap free
Amount of items: 2
Items: 
Size: 793604 Color: 0
Size: 206322 Color: 1

Bin 2090: 76 of cap free
Amount of items: 2
Items: 
Size: 510463 Color: 0
Size: 489462 Color: 1

Bin 2091: 76 of cap free
Amount of items: 2
Items: 
Size: 514751 Color: 1
Size: 485174 Color: 0

Bin 2092: 76 of cap free
Amount of items: 2
Items: 
Size: 573955 Color: 0
Size: 425970 Color: 1

Bin 2093: 76 of cap free
Amount of items: 2
Items: 
Size: 574221 Color: 1
Size: 425704 Color: 0

Bin 2094: 76 of cap free
Amount of items: 2
Items: 
Size: 605471 Color: 1
Size: 394454 Color: 0

Bin 2095: 76 of cap free
Amount of items: 2
Items: 
Size: 691422 Color: 1
Size: 308503 Color: 0

Bin 2096: 76 of cap free
Amount of items: 2
Items: 
Size: 694631 Color: 1
Size: 305294 Color: 0

Bin 2097: 76 of cap free
Amount of items: 2
Items: 
Size: 696010 Color: 0
Size: 303915 Color: 1

Bin 2098: 76 of cap free
Amount of items: 2
Items: 
Size: 722096 Color: 1
Size: 277829 Color: 0

Bin 2099: 76 of cap free
Amount of items: 2
Items: 
Size: 780847 Color: 0
Size: 219078 Color: 1

Bin 2100: 76 of cap free
Amount of items: 2
Items: 
Size: 799522 Color: 1
Size: 200403 Color: 0

Bin 2101: 77 of cap free
Amount of items: 2
Items: 
Size: 541273 Color: 1
Size: 458651 Color: 0

Bin 2102: 77 of cap free
Amount of items: 2
Items: 
Size: 566261 Color: 1
Size: 433663 Color: 0

Bin 2103: 77 of cap free
Amount of items: 2
Items: 
Size: 572127 Color: 0
Size: 427797 Color: 1

Bin 2104: 77 of cap free
Amount of items: 2
Items: 
Size: 602804 Color: 0
Size: 397120 Color: 1

Bin 2105: 77 of cap free
Amount of items: 2
Items: 
Size: 610134 Color: 0
Size: 389790 Color: 1

Bin 2106: 77 of cap free
Amount of items: 2
Items: 
Size: 679756 Color: 1
Size: 320168 Color: 0

Bin 2107: 77 of cap free
Amount of items: 2
Items: 
Size: 696928 Color: 1
Size: 302996 Color: 0

Bin 2108: 77 of cap free
Amount of items: 2
Items: 
Size: 718790 Color: 1
Size: 281134 Color: 0

Bin 2109: 77 of cap free
Amount of items: 2
Items: 
Size: 740098 Color: 0
Size: 259826 Color: 1

Bin 2110: 77 of cap free
Amount of items: 2
Items: 
Size: 759058 Color: 0
Size: 240866 Color: 1

Bin 2111: 77 of cap free
Amount of items: 2
Items: 
Size: 769353 Color: 1
Size: 230571 Color: 0

Bin 2112: 77 of cap free
Amount of items: 2
Items: 
Size: 772996 Color: 0
Size: 226928 Color: 1

Bin 2113: 78 of cap free
Amount of items: 2
Items: 
Size: 506465 Color: 0
Size: 493458 Color: 1

Bin 2114: 78 of cap free
Amount of items: 2
Items: 
Size: 573826 Color: 1
Size: 426097 Color: 0

Bin 2115: 78 of cap free
Amount of items: 2
Items: 
Size: 608685 Color: 1
Size: 391238 Color: 0

Bin 2116: 78 of cap free
Amount of items: 2
Items: 
Size: 630337 Color: 1
Size: 369586 Color: 0

Bin 2117: 78 of cap free
Amount of items: 2
Items: 
Size: 634025 Color: 1
Size: 365898 Color: 0

Bin 2118: 78 of cap free
Amount of items: 2
Items: 
Size: 637438 Color: 0
Size: 362485 Color: 1

Bin 2119: 78 of cap free
Amount of items: 2
Items: 
Size: 644317 Color: 0
Size: 355606 Color: 1

Bin 2120: 78 of cap free
Amount of items: 2
Items: 
Size: 669121 Color: 1
Size: 330802 Color: 0

Bin 2121: 78 of cap free
Amount of items: 2
Items: 
Size: 671715 Color: 1
Size: 328208 Color: 0

Bin 2122: 78 of cap free
Amount of items: 2
Items: 
Size: 743850 Color: 0
Size: 256073 Color: 1

Bin 2123: 78 of cap free
Amount of items: 2
Items: 
Size: 788726 Color: 0
Size: 211197 Color: 1

Bin 2124: 79 of cap free
Amount of items: 2
Items: 
Size: 512077 Color: 1
Size: 487845 Color: 0

Bin 2125: 79 of cap free
Amount of items: 2
Items: 
Size: 532045 Color: 1
Size: 467877 Color: 0

Bin 2126: 79 of cap free
Amount of items: 2
Items: 
Size: 539854 Color: 0
Size: 460068 Color: 1

Bin 2127: 79 of cap free
Amount of items: 2
Items: 
Size: 591930 Color: 1
Size: 407992 Color: 0

Bin 2128: 79 of cap free
Amount of items: 2
Items: 
Size: 635267 Color: 1
Size: 364655 Color: 0

Bin 2129: 79 of cap free
Amount of items: 2
Items: 
Size: 642043 Color: 1
Size: 357879 Color: 0

Bin 2130: 79 of cap free
Amount of items: 2
Items: 
Size: 651002 Color: 0
Size: 348920 Color: 1

Bin 2131: 79 of cap free
Amount of items: 2
Items: 
Size: 769998 Color: 0
Size: 229924 Color: 1

Bin 2132: 80 of cap free
Amount of items: 2
Items: 
Size: 503183 Color: 0
Size: 496738 Color: 1

Bin 2133: 80 of cap free
Amount of items: 2
Items: 
Size: 531048 Color: 1
Size: 468873 Color: 0

Bin 2134: 80 of cap free
Amount of items: 2
Items: 
Size: 580388 Color: 0
Size: 419533 Color: 1

Bin 2135: 80 of cap free
Amount of items: 2
Items: 
Size: 630438 Color: 0
Size: 369483 Color: 1

Bin 2136: 80 of cap free
Amount of items: 2
Items: 
Size: 636655 Color: 0
Size: 363266 Color: 1

Bin 2137: 80 of cap free
Amount of items: 2
Items: 
Size: 686372 Color: 1
Size: 313549 Color: 0

Bin 2138: 80 of cap free
Amount of items: 2
Items: 
Size: 701707 Color: 0
Size: 298214 Color: 1

Bin 2139: 80 of cap free
Amount of items: 2
Items: 
Size: 796126 Color: 0
Size: 203795 Color: 1

Bin 2140: 81 of cap free
Amount of items: 2
Items: 
Size: 500316 Color: 0
Size: 499604 Color: 1

Bin 2141: 81 of cap free
Amount of items: 2
Items: 
Size: 505649 Color: 0
Size: 494271 Color: 1

Bin 2142: 81 of cap free
Amount of items: 2
Items: 
Size: 515943 Color: 0
Size: 483977 Color: 1

Bin 2143: 81 of cap free
Amount of items: 2
Items: 
Size: 528021 Color: 1
Size: 471899 Color: 0

Bin 2144: 81 of cap free
Amount of items: 2
Items: 
Size: 530092 Color: 0
Size: 469828 Color: 1

Bin 2145: 81 of cap free
Amount of items: 2
Items: 
Size: 585299 Color: 0
Size: 414621 Color: 1

Bin 2146: 81 of cap free
Amount of items: 2
Items: 
Size: 597674 Color: 0
Size: 402246 Color: 1

Bin 2147: 81 of cap free
Amount of items: 2
Items: 
Size: 627452 Color: 1
Size: 372468 Color: 0

Bin 2148: 81 of cap free
Amount of items: 2
Items: 
Size: 674561 Color: 1
Size: 325359 Color: 0

Bin 2149: 81 of cap free
Amount of items: 2
Items: 
Size: 688405 Color: 0
Size: 311515 Color: 1

Bin 2150: 81 of cap free
Amount of items: 2
Items: 
Size: 700032 Color: 0
Size: 299888 Color: 1

Bin 2151: 81 of cap free
Amount of items: 2
Items: 
Size: 706900 Color: 0
Size: 293020 Color: 1

Bin 2152: 81 of cap free
Amount of items: 2
Items: 
Size: 755394 Color: 1
Size: 244526 Color: 0

Bin 2153: 81 of cap free
Amount of items: 2
Items: 
Size: 770558 Color: 0
Size: 229362 Color: 1

Bin 2154: 81 of cap free
Amount of items: 2
Items: 
Size: 789505 Color: 1
Size: 210415 Color: 0

Bin 2155: 82 of cap free
Amount of items: 3
Items: 
Size: 370103 Color: 0
Size: 320642 Color: 0
Size: 309174 Color: 1

Bin 2156: 82 of cap free
Amount of items: 2
Items: 
Size: 520662 Color: 0
Size: 479257 Color: 1

Bin 2157: 82 of cap free
Amount of items: 2
Items: 
Size: 545972 Color: 0
Size: 453947 Color: 1

Bin 2158: 82 of cap free
Amount of items: 2
Items: 
Size: 601390 Color: 0
Size: 398529 Color: 1

Bin 2159: 82 of cap free
Amount of items: 2
Items: 
Size: 619074 Color: 0
Size: 380845 Color: 1

Bin 2160: 82 of cap free
Amount of items: 2
Items: 
Size: 630164 Color: 0
Size: 369755 Color: 1

Bin 2161: 82 of cap free
Amount of items: 2
Items: 
Size: 727325 Color: 1
Size: 272594 Color: 0

Bin 2162: 82 of cap free
Amount of items: 2
Items: 
Size: 730715 Color: 1
Size: 269204 Color: 0

Bin 2163: 82 of cap free
Amount of items: 2
Items: 
Size: 738526 Color: 1
Size: 261393 Color: 0

Bin 2164: 82 of cap free
Amount of items: 2
Items: 
Size: 779559 Color: 0
Size: 220360 Color: 1

Bin 2165: 83 of cap free
Amount of items: 2
Items: 
Size: 526405 Color: 0
Size: 473513 Color: 1

Bin 2166: 83 of cap free
Amount of items: 2
Items: 
Size: 541751 Color: 0
Size: 458167 Color: 1

Bin 2167: 83 of cap free
Amount of items: 2
Items: 
Size: 594956 Color: 1
Size: 404962 Color: 0

Bin 2168: 83 of cap free
Amount of items: 2
Items: 
Size: 651553 Color: 1
Size: 348365 Color: 0

Bin 2169: 83 of cap free
Amount of items: 2
Items: 
Size: 670734 Color: 0
Size: 329184 Color: 1

Bin 2170: 83 of cap free
Amount of items: 2
Items: 
Size: 693449 Color: 1
Size: 306469 Color: 0

Bin 2171: 83 of cap free
Amount of items: 2
Items: 
Size: 727092 Color: 0
Size: 272826 Color: 1

Bin 2172: 83 of cap free
Amount of items: 2
Items: 
Size: 757148 Color: 1
Size: 242770 Color: 0

Bin 2173: 83 of cap free
Amount of items: 2
Items: 
Size: 791058 Color: 0
Size: 208860 Color: 1

Bin 2174: 83 of cap free
Amount of items: 2
Items: 
Size: 797860 Color: 0
Size: 202058 Color: 1

Bin 2175: 84 of cap free
Amount of items: 2
Items: 
Size: 531261 Color: 0
Size: 468656 Color: 1

Bin 2176: 84 of cap free
Amount of items: 2
Items: 
Size: 532624 Color: 1
Size: 467293 Color: 0

Bin 2177: 84 of cap free
Amount of items: 2
Items: 
Size: 533512 Color: 0
Size: 466405 Color: 1

Bin 2178: 84 of cap free
Amount of items: 2
Items: 
Size: 578580 Color: 1
Size: 421337 Color: 0

Bin 2179: 84 of cap free
Amount of items: 2
Items: 
Size: 631330 Color: 0
Size: 368587 Color: 1

Bin 2180: 84 of cap free
Amount of items: 2
Items: 
Size: 665389 Color: 0
Size: 334528 Color: 1

Bin 2181: 84 of cap free
Amount of items: 2
Items: 
Size: 675943 Color: 1
Size: 323974 Color: 0

Bin 2182: 84 of cap free
Amount of items: 2
Items: 
Size: 776558 Color: 1
Size: 223359 Color: 0

Bin 2183: 85 of cap free
Amount of items: 2
Items: 
Size: 556360 Color: 0
Size: 443556 Color: 1

Bin 2184: 85 of cap free
Amount of items: 2
Items: 
Size: 567247 Color: 0
Size: 432669 Color: 1

Bin 2185: 85 of cap free
Amount of items: 2
Items: 
Size: 582485 Color: 1
Size: 417431 Color: 0

Bin 2186: 85 of cap free
Amount of items: 2
Items: 
Size: 584327 Color: 1
Size: 415589 Color: 0

Bin 2187: 85 of cap free
Amount of items: 2
Items: 
Size: 584369 Color: 0
Size: 415547 Color: 1

Bin 2188: 85 of cap free
Amount of items: 2
Items: 
Size: 629622 Color: 1
Size: 370294 Color: 0

Bin 2189: 85 of cap free
Amount of items: 2
Items: 
Size: 677494 Color: 1
Size: 322422 Color: 0

Bin 2190: 85 of cap free
Amount of items: 2
Items: 
Size: 702688 Color: 1
Size: 297228 Color: 0

Bin 2191: 85 of cap free
Amount of items: 2
Items: 
Size: 718399 Color: 0
Size: 281517 Color: 1

Bin 2192: 85 of cap free
Amount of items: 2
Items: 
Size: 730876 Color: 1
Size: 269040 Color: 0

Bin 2193: 85 of cap free
Amount of items: 2
Items: 
Size: 779371 Color: 0
Size: 220545 Color: 1

Bin 2194: 85 of cap free
Amount of items: 3
Items: 
Size: 783014 Color: 0
Size: 113544 Color: 0
Size: 103358 Color: 1

Bin 2195: 85 of cap free
Amount of items: 2
Items: 
Size: 791112 Color: 1
Size: 208804 Color: 0

Bin 2196: 86 of cap free
Amount of items: 2
Items: 
Size: 565627 Color: 1
Size: 434288 Color: 0

Bin 2197: 86 of cap free
Amount of items: 2
Items: 
Size: 571733 Color: 0
Size: 428182 Color: 1

Bin 2198: 86 of cap free
Amount of items: 2
Items: 
Size: 615825 Color: 1
Size: 384090 Color: 0

Bin 2199: 86 of cap free
Amount of items: 2
Items: 
Size: 650704 Color: 0
Size: 349211 Color: 1

Bin 2200: 86 of cap free
Amount of items: 2
Items: 
Size: 652595 Color: 1
Size: 347320 Color: 0

Bin 2201: 86 of cap free
Amount of items: 2
Items: 
Size: 663433 Color: 1
Size: 336482 Color: 0

Bin 2202: 86 of cap free
Amount of items: 2
Items: 
Size: 716366 Color: 0
Size: 283549 Color: 1

Bin 2203: 86 of cap free
Amount of items: 2
Items: 
Size: 717956 Color: 0
Size: 281959 Color: 1

Bin 2204: 86 of cap free
Amount of items: 2
Items: 
Size: 733750 Color: 1
Size: 266165 Color: 0

Bin 2205: 86 of cap free
Amount of items: 2
Items: 
Size: 761470 Color: 0
Size: 238445 Color: 1

Bin 2206: 86 of cap free
Amount of items: 2
Items: 
Size: 770214 Color: 0
Size: 229701 Color: 1

Bin 2207: 86 of cap free
Amount of items: 2
Items: 
Size: 797681 Color: 0
Size: 202234 Color: 1

Bin 2208: 87 of cap free
Amount of items: 2
Items: 
Size: 538590 Color: 1
Size: 461324 Color: 0

Bin 2209: 87 of cap free
Amount of items: 2
Items: 
Size: 554278 Color: 1
Size: 445636 Color: 0

Bin 2210: 87 of cap free
Amount of items: 2
Items: 
Size: 579585 Color: 0
Size: 420329 Color: 1

Bin 2211: 87 of cap free
Amount of items: 2
Items: 
Size: 634002 Color: 0
Size: 365912 Color: 1

Bin 2212: 87 of cap free
Amount of items: 2
Items: 
Size: 634508 Color: 0
Size: 365406 Color: 1

Bin 2213: 87 of cap free
Amount of items: 2
Items: 
Size: 669282 Color: 0
Size: 330632 Color: 1

Bin 2214: 87 of cap free
Amount of items: 2
Items: 
Size: 698881 Color: 1
Size: 301033 Color: 0

Bin 2215: 87 of cap free
Amount of items: 2
Items: 
Size: 763782 Color: 1
Size: 236132 Color: 0

Bin 2216: 87 of cap free
Amount of items: 2
Items: 
Size: 768690 Color: 1
Size: 231224 Color: 0

Bin 2217: 87 of cap free
Amount of items: 2
Items: 
Size: 773554 Color: 0
Size: 226360 Color: 1

Bin 2218: 87 of cap free
Amount of items: 2
Items: 
Size: 795691 Color: 0
Size: 204223 Color: 1

Bin 2219: 88 of cap free
Amount of items: 2
Items: 
Size: 503933 Color: 1
Size: 495980 Color: 0

Bin 2220: 88 of cap free
Amount of items: 2
Items: 
Size: 529510 Color: 0
Size: 470403 Color: 1

Bin 2221: 88 of cap free
Amount of items: 2
Items: 
Size: 548515 Color: 0
Size: 451398 Color: 1

Bin 2222: 88 of cap free
Amount of items: 2
Items: 
Size: 610432 Color: 1
Size: 389481 Color: 0

Bin 2223: 88 of cap free
Amount of items: 2
Items: 
Size: 613266 Color: 0
Size: 386647 Color: 1

Bin 2224: 88 of cap free
Amount of items: 2
Items: 
Size: 626713 Color: 0
Size: 373200 Color: 1

Bin 2225: 88 of cap free
Amount of items: 2
Items: 
Size: 642485 Color: 1
Size: 357428 Color: 0

Bin 2226: 88 of cap free
Amount of items: 2
Items: 
Size: 667542 Color: 0
Size: 332371 Color: 1

Bin 2227: 88 of cap free
Amount of items: 2
Items: 
Size: 679060 Color: 1
Size: 320853 Color: 0

Bin 2228: 88 of cap free
Amount of items: 2
Items: 
Size: 696095 Color: 1
Size: 303818 Color: 0

Bin 2229: 88 of cap free
Amount of items: 2
Items: 
Size: 711309 Color: 1
Size: 288604 Color: 0

Bin 2230: 88 of cap free
Amount of items: 2
Items: 
Size: 719199 Color: 0
Size: 280714 Color: 1

Bin 2231: 88 of cap free
Amount of items: 2
Items: 
Size: 752528 Color: 0
Size: 247385 Color: 1

Bin 2232: 89 of cap free
Amount of items: 2
Items: 
Size: 551849 Color: 1
Size: 448063 Color: 0

Bin 2233: 89 of cap free
Amount of items: 2
Items: 
Size: 552348 Color: 0
Size: 447564 Color: 1

Bin 2234: 89 of cap free
Amount of items: 2
Items: 
Size: 565540 Color: 0
Size: 434372 Color: 1

Bin 2235: 89 of cap free
Amount of items: 2
Items: 
Size: 569846 Color: 1
Size: 430066 Color: 0

Bin 2236: 89 of cap free
Amount of items: 2
Items: 
Size: 597370 Color: 0
Size: 402542 Color: 1

Bin 2237: 89 of cap free
Amount of items: 2
Items: 
Size: 603525 Color: 1
Size: 396387 Color: 0

Bin 2238: 89 of cap free
Amount of items: 2
Items: 
Size: 643642 Color: 1
Size: 356270 Color: 0

Bin 2239: 89 of cap free
Amount of items: 2
Items: 
Size: 646326 Color: 0
Size: 353586 Color: 1

Bin 2240: 89 of cap free
Amount of items: 2
Items: 
Size: 674121 Color: 0
Size: 325791 Color: 1

Bin 2241: 89 of cap free
Amount of items: 2
Items: 
Size: 688875 Color: 1
Size: 311037 Color: 0

Bin 2242: 89 of cap free
Amount of items: 2
Items: 
Size: 702907 Color: 1
Size: 297005 Color: 0

Bin 2243: 89 of cap free
Amount of items: 2
Items: 
Size: 723034 Color: 0
Size: 276878 Color: 1

Bin 2244: 89 of cap free
Amount of items: 2
Items: 
Size: 739468 Color: 1
Size: 260444 Color: 0

Bin 2245: 89 of cap free
Amount of items: 2
Items: 
Size: 781410 Color: 0
Size: 218502 Color: 1

Bin 2246: 90 of cap free
Amount of items: 2
Items: 
Size: 518314 Color: 1
Size: 481597 Color: 0

Bin 2247: 90 of cap free
Amount of items: 2
Items: 
Size: 550387 Color: 0
Size: 449524 Color: 1

Bin 2248: 90 of cap free
Amount of items: 2
Items: 
Size: 578070 Color: 0
Size: 421841 Color: 1

Bin 2249: 90 of cap free
Amount of items: 2
Items: 
Size: 613125 Color: 1
Size: 386786 Color: 0

Bin 2250: 90 of cap free
Amount of items: 2
Items: 
Size: 693243 Color: 0
Size: 306668 Color: 1

Bin 2251: 90 of cap free
Amount of items: 2
Items: 
Size: 749027 Color: 0
Size: 250884 Color: 1

Bin 2252: 90 of cap free
Amount of items: 2
Items: 
Size: 751391 Color: 0
Size: 248520 Color: 1

Bin 2253: 91 of cap free
Amount of items: 2
Items: 
Size: 527170 Color: 0
Size: 472740 Color: 1

Bin 2254: 91 of cap free
Amount of items: 2
Items: 
Size: 539701 Color: 1
Size: 460209 Color: 0

Bin 2255: 91 of cap free
Amount of items: 2
Items: 
Size: 587221 Color: 1
Size: 412689 Color: 0

Bin 2256: 91 of cap free
Amount of items: 2
Items: 
Size: 610938 Color: 0
Size: 388972 Color: 1

Bin 2257: 91 of cap free
Amount of items: 2
Items: 
Size: 611524 Color: 0
Size: 388386 Color: 1

Bin 2258: 91 of cap free
Amount of items: 2
Items: 
Size: 619499 Color: 1
Size: 380411 Color: 0

Bin 2259: 91 of cap free
Amount of items: 2
Items: 
Size: 621945 Color: 1
Size: 377965 Color: 0

Bin 2260: 91 of cap free
Amount of items: 2
Items: 
Size: 705249 Color: 0
Size: 294661 Color: 1

Bin 2261: 91 of cap free
Amount of items: 2
Items: 
Size: 716656 Color: 0
Size: 283254 Color: 1

Bin 2262: 91 of cap free
Amount of items: 2
Items: 
Size: 723716 Color: 1
Size: 276194 Color: 0

Bin 2263: 91 of cap free
Amount of items: 2
Items: 
Size: 727189 Color: 1
Size: 272721 Color: 0

Bin 2264: 91 of cap free
Amount of items: 2
Items: 
Size: 766816 Color: 0
Size: 233094 Color: 1

Bin 2265: 91 of cap free
Amount of items: 2
Items: 
Size: 775632 Color: 0
Size: 224278 Color: 1

Bin 2266: 91 of cap free
Amount of items: 2
Items: 
Size: 776603 Color: 0
Size: 223307 Color: 1

Bin 2267: 92 of cap free
Amount of items: 2
Items: 
Size: 577719 Color: 0
Size: 422190 Color: 1

Bin 2268: 92 of cap free
Amount of items: 2
Items: 
Size: 580518 Color: 0
Size: 419391 Color: 1

Bin 2269: 92 of cap free
Amount of items: 2
Items: 
Size: 701099 Color: 0
Size: 298810 Color: 1

Bin 2270: 92 of cap free
Amount of items: 2
Items: 
Size: 714473 Color: 0
Size: 285436 Color: 1

Bin 2271: 92 of cap free
Amount of items: 2
Items: 
Size: 731335 Color: 1
Size: 268574 Color: 0

Bin 2272: 93 of cap free
Amount of items: 2
Items: 
Size: 511484 Color: 0
Size: 488424 Color: 1

Bin 2273: 93 of cap free
Amount of items: 2
Items: 
Size: 547654 Color: 1
Size: 452254 Color: 0

Bin 2274: 93 of cap free
Amount of items: 2
Items: 
Size: 557821 Color: 1
Size: 442087 Color: 0

Bin 2275: 93 of cap free
Amount of items: 2
Items: 
Size: 568695 Color: 0
Size: 431213 Color: 1

Bin 2276: 93 of cap free
Amount of items: 2
Items: 
Size: 574163 Color: 0
Size: 425745 Color: 1

Bin 2277: 93 of cap free
Amount of items: 2
Items: 
Size: 645110 Color: 1
Size: 354798 Color: 0

Bin 2278: 93 of cap free
Amount of items: 2
Items: 
Size: 690586 Color: 0
Size: 309322 Color: 1

Bin 2279: 93 of cap free
Amount of items: 2
Items: 
Size: 696464 Color: 0
Size: 303444 Color: 1

Bin 2280: 93 of cap free
Amount of items: 2
Items: 
Size: 746475 Color: 1
Size: 253433 Color: 0

Bin 2281: 94 of cap free
Amount of items: 2
Items: 
Size: 528354 Color: 0
Size: 471553 Color: 1

Bin 2282: 94 of cap free
Amount of items: 2
Items: 
Size: 530396 Color: 0
Size: 469511 Color: 1

Bin 2283: 94 of cap free
Amount of items: 2
Items: 
Size: 530400 Color: 1
Size: 469507 Color: 0

Bin 2284: 94 of cap free
Amount of items: 2
Items: 
Size: 554419 Color: 1
Size: 445488 Color: 0

Bin 2285: 94 of cap free
Amount of items: 2
Items: 
Size: 688861 Color: 0
Size: 311046 Color: 1

Bin 2286: 94 of cap free
Amount of items: 2
Items: 
Size: 764459 Color: 1
Size: 235448 Color: 0

Bin 2287: 94 of cap free
Amount of items: 2
Items: 
Size: 774067 Color: 0
Size: 225840 Color: 1

Bin 2288: 95 of cap free
Amount of items: 2
Items: 
Size: 516510 Color: 0
Size: 483396 Color: 1

Bin 2289: 95 of cap free
Amount of items: 2
Items: 
Size: 538423 Color: 1
Size: 461483 Color: 0

Bin 2290: 95 of cap free
Amount of items: 2
Items: 
Size: 590991 Color: 1
Size: 408915 Color: 0

Bin 2291: 95 of cap free
Amount of items: 2
Items: 
Size: 630501 Color: 1
Size: 369405 Color: 0

Bin 2292: 95 of cap free
Amount of items: 2
Items: 
Size: 660374 Color: 0
Size: 339532 Color: 1

Bin 2293: 95 of cap free
Amount of items: 2
Items: 
Size: 692337 Color: 0
Size: 307569 Color: 1

Bin 2294: 95 of cap free
Amount of items: 2
Items: 
Size: 784871 Color: 0
Size: 215035 Color: 1

Bin 2295: 96 of cap free
Amount of items: 2
Items: 
Size: 529086 Color: 1
Size: 470819 Color: 0

Bin 2296: 96 of cap free
Amount of items: 2
Items: 
Size: 535410 Color: 0
Size: 464495 Color: 1

Bin 2297: 96 of cap free
Amount of items: 2
Items: 
Size: 553943 Color: 0
Size: 445962 Color: 1

Bin 2298: 96 of cap free
Amount of items: 2
Items: 
Size: 564687 Color: 0
Size: 435218 Color: 1

Bin 2299: 96 of cap free
Amount of items: 2
Items: 
Size: 567926 Color: 0
Size: 431979 Color: 1

Bin 2300: 96 of cap free
Amount of items: 2
Items: 
Size: 645659 Color: 1
Size: 354246 Color: 0

Bin 2301: 96 of cap free
Amount of items: 2
Items: 
Size: 658405 Color: 0
Size: 341500 Color: 1

Bin 2302: 96 of cap free
Amount of items: 2
Items: 
Size: 662303 Color: 0
Size: 337602 Color: 1

Bin 2303: 96 of cap free
Amount of items: 2
Items: 
Size: 662948 Color: 0
Size: 336957 Color: 1

Bin 2304: 96 of cap free
Amount of items: 2
Items: 
Size: 787931 Color: 0
Size: 211974 Color: 1

Bin 2305: 97 of cap free
Amount of items: 2
Items: 
Size: 574803 Color: 0
Size: 425101 Color: 1

Bin 2306: 97 of cap free
Amount of items: 2
Items: 
Size: 603699 Color: 0
Size: 396205 Color: 1

Bin 2307: 97 of cap free
Amount of items: 2
Items: 
Size: 658697 Color: 1
Size: 341207 Color: 0

Bin 2308: 97 of cap free
Amount of items: 2
Items: 
Size: 666721 Color: 1
Size: 333183 Color: 0

Bin 2309: 97 of cap free
Amount of items: 2
Items: 
Size: 686671 Color: 1
Size: 313233 Color: 0

Bin 2310: 97 of cap free
Amount of items: 2
Items: 
Size: 701245 Color: 0
Size: 298659 Color: 1

Bin 2311: 97 of cap free
Amount of items: 2
Items: 
Size: 703505 Color: 0
Size: 296399 Color: 1

Bin 2312: 97 of cap free
Amount of items: 2
Items: 
Size: 704547 Color: 1
Size: 295357 Color: 0

Bin 2313: 97 of cap free
Amount of items: 2
Items: 
Size: 739418 Color: 0
Size: 260486 Color: 1

Bin 2314: 98 of cap free
Amount of items: 2
Items: 
Size: 501253 Color: 0
Size: 498650 Color: 1

Bin 2315: 98 of cap free
Amount of items: 2
Items: 
Size: 522371 Color: 0
Size: 477532 Color: 1

Bin 2316: 98 of cap free
Amount of items: 2
Items: 
Size: 526652 Color: 1
Size: 473251 Color: 0

Bin 2317: 98 of cap free
Amount of items: 2
Items: 
Size: 621048 Color: 1
Size: 378855 Color: 0

Bin 2318: 98 of cap free
Amount of items: 2
Items: 
Size: 676585 Color: 1
Size: 323318 Color: 0

Bin 2319: 98 of cap free
Amount of items: 2
Items: 
Size: 718756 Color: 0
Size: 281147 Color: 1

Bin 2320: 98 of cap free
Amount of items: 2
Items: 
Size: 742622 Color: 1
Size: 257281 Color: 0

Bin 2321: 98 of cap free
Amount of items: 2
Items: 
Size: 799246 Color: 1
Size: 200657 Color: 0

Bin 2322: 99 of cap free
Amount of items: 2
Items: 
Size: 509454 Color: 0
Size: 490448 Color: 1

Bin 2323: 99 of cap free
Amount of items: 2
Items: 
Size: 537179 Color: 1
Size: 462723 Color: 0

Bin 2324: 99 of cap free
Amount of items: 2
Items: 
Size: 561926 Color: 0
Size: 437976 Color: 1

Bin 2325: 99 of cap free
Amount of items: 2
Items: 
Size: 575983 Color: 0
Size: 423919 Color: 1

Bin 2326: 99 of cap free
Amount of items: 2
Items: 
Size: 597021 Color: 1
Size: 402881 Color: 0

Bin 2327: 99 of cap free
Amount of items: 2
Items: 
Size: 629612 Color: 0
Size: 370290 Color: 1

Bin 2328: 99 of cap free
Amount of items: 2
Items: 
Size: 641818 Color: 0
Size: 358084 Color: 1

Bin 2329: 99 of cap free
Amount of items: 2
Items: 
Size: 652967 Color: 0
Size: 346935 Color: 1

Bin 2330: 99 of cap free
Amount of items: 2
Items: 
Size: 675821 Color: 1
Size: 324081 Color: 0

Bin 2331: 99 of cap free
Amount of items: 2
Items: 
Size: 689187 Color: 0
Size: 310715 Color: 1

Bin 2332: 99 of cap free
Amount of items: 2
Items: 
Size: 708626 Color: 1
Size: 291276 Color: 0

Bin 2333: 99 of cap free
Amount of items: 2
Items: 
Size: 744831 Color: 1
Size: 255071 Color: 0

Bin 2334: 100 of cap free
Amount of items: 7
Items: 
Size: 151328 Color: 0
Size: 150443 Color: 0
Size: 150305 Color: 0
Size: 144183 Color: 1
Size: 143988 Color: 1
Size: 133324 Color: 1
Size: 126330 Color: 0

Bin 2335: 100 of cap free
Amount of items: 2
Items: 
Size: 522492 Color: 1
Size: 477409 Color: 0

Bin 2336: 100 of cap free
Amount of items: 2
Items: 
Size: 539988 Color: 1
Size: 459913 Color: 0

Bin 2337: 100 of cap free
Amount of items: 2
Items: 
Size: 572504 Color: 0
Size: 427397 Color: 1

Bin 2338: 100 of cap free
Amount of items: 2
Items: 
Size: 596086 Color: 0
Size: 403815 Color: 1

Bin 2339: 100 of cap free
Amount of items: 2
Items: 
Size: 615553 Color: 1
Size: 384348 Color: 0

Bin 2340: 100 of cap free
Amount of items: 2
Items: 
Size: 628008 Color: 1
Size: 371893 Color: 0

Bin 2341: 100 of cap free
Amount of items: 2
Items: 
Size: 630021 Color: 1
Size: 369880 Color: 0

Bin 2342: 100 of cap free
Amount of items: 2
Items: 
Size: 637257 Color: 1
Size: 362644 Color: 0

Bin 2343: 100 of cap free
Amount of items: 2
Items: 
Size: 655894 Color: 0
Size: 344007 Color: 1

Bin 2344: 100 of cap free
Amount of items: 2
Items: 
Size: 706884 Color: 1
Size: 293017 Color: 0

Bin 2345: 100 of cap free
Amount of items: 2
Items: 
Size: 775247 Color: 1
Size: 224654 Color: 0

Bin 2346: 101 of cap free
Amount of items: 2
Items: 
Size: 629435 Color: 0
Size: 370465 Color: 1

Bin 2347: 101 of cap free
Amount of items: 2
Items: 
Size: 676739 Color: 1
Size: 323161 Color: 0

Bin 2348: 101 of cap free
Amount of items: 2
Items: 
Size: 751279 Color: 1
Size: 248621 Color: 0

Bin 2349: 101 of cap free
Amount of items: 2
Items: 
Size: 757139 Color: 0
Size: 242761 Color: 1

Bin 2350: 101 of cap free
Amount of items: 2
Items: 
Size: 761068 Color: 1
Size: 238832 Color: 0

Bin 2351: 101 of cap free
Amount of items: 2
Items: 
Size: 771296 Color: 0
Size: 228604 Color: 1

Bin 2352: 101 of cap free
Amount of items: 2
Items: 
Size: 781923 Color: 0
Size: 217977 Color: 1

Bin 2353: 102 of cap free
Amount of items: 2
Items: 
Size: 527299 Color: 1
Size: 472600 Color: 0

Bin 2354: 102 of cap free
Amount of items: 2
Items: 
Size: 544779 Color: 0
Size: 455120 Color: 1

Bin 2355: 102 of cap free
Amount of items: 2
Items: 
Size: 555322 Color: 1
Size: 444577 Color: 0

Bin 2356: 102 of cap free
Amount of items: 2
Items: 
Size: 602903 Color: 0
Size: 396996 Color: 1

Bin 2357: 102 of cap free
Amount of items: 2
Items: 
Size: 613408 Color: 1
Size: 386491 Color: 0

Bin 2358: 102 of cap free
Amount of items: 2
Items: 
Size: 632448 Color: 0
Size: 367451 Color: 1

Bin 2359: 102 of cap free
Amount of items: 2
Items: 
Size: 650176 Color: 0
Size: 349723 Color: 1

Bin 2360: 102 of cap free
Amount of items: 2
Items: 
Size: 678872 Color: 0
Size: 321027 Color: 1

Bin 2361: 102 of cap free
Amount of items: 2
Items: 
Size: 696179 Color: 0
Size: 303720 Color: 1

Bin 2362: 102 of cap free
Amount of items: 2
Items: 
Size: 751059 Color: 0
Size: 248840 Color: 1

Bin 2363: 102 of cap free
Amount of items: 2
Items: 
Size: 777092 Color: 1
Size: 222807 Color: 0

Bin 2364: 102 of cap free
Amount of items: 2
Items: 
Size: 788230 Color: 1
Size: 211669 Color: 0

Bin 2365: 103 of cap free
Amount of items: 7
Items: 
Size: 155396 Color: 0
Size: 155388 Color: 0
Size: 152218 Color: 1
Size: 150879 Color: 1
Size: 150747 Color: 1
Size: 120899 Color: 1
Size: 114371 Color: 0

Bin 2366: 103 of cap free
Amount of items: 4
Items: 
Size: 319039 Color: 1
Size: 311938 Color: 1
Size: 252798 Color: 0
Size: 116123 Color: 0

Bin 2367: 103 of cap free
Amount of items: 2
Items: 
Size: 513449 Color: 1
Size: 486449 Color: 0

Bin 2368: 103 of cap free
Amount of items: 2
Items: 
Size: 548975 Color: 1
Size: 450923 Color: 0

Bin 2369: 103 of cap free
Amount of items: 2
Items: 
Size: 595099 Color: 0
Size: 404799 Color: 1

Bin 2370: 103 of cap free
Amount of items: 2
Items: 
Size: 601062 Color: 1
Size: 398836 Color: 0

Bin 2371: 103 of cap free
Amount of items: 2
Items: 
Size: 624735 Color: 1
Size: 375163 Color: 0

Bin 2372: 103 of cap free
Amount of items: 2
Items: 
Size: 673510 Color: 0
Size: 326388 Color: 1

Bin 2373: 103 of cap free
Amount of items: 2
Items: 
Size: 684529 Color: 0
Size: 315369 Color: 1

Bin 2374: 103 of cap free
Amount of items: 2
Items: 
Size: 734239 Color: 0
Size: 265659 Color: 1

Bin 2375: 103 of cap free
Amount of items: 2
Items: 
Size: 768840 Color: 1
Size: 231058 Color: 0

Bin 2376: 104 of cap free
Amount of items: 2
Items: 
Size: 564035 Color: 0
Size: 435862 Color: 1

Bin 2377: 104 of cap free
Amount of items: 2
Items: 
Size: 577391 Color: 0
Size: 422506 Color: 1

Bin 2378: 104 of cap free
Amount of items: 2
Items: 
Size: 585024 Color: 1
Size: 414873 Color: 0

Bin 2379: 104 of cap free
Amount of items: 2
Items: 
Size: 663304 Color: 0
Size: 336593 Color: 1

Bin 2380: 104 of cap free
Amount of items: 2
Items: 
Size: 665039 Color: 0
Size: 334858 Color: 1

Bin 2381: 104 of cap free
Amount of items: 2
Items: 
Size: 673918 Color: 0
Size: 325979 Color: 1

Bin 2382: 104 of cap free
Amount of items: 2
Items: 
Size: 787867 Color: 1
Size: 212030 Color: 0

Bin 2383: 105 of cap free
Amount of items: 2
Items: 
Size: 515480 Color: 0
Size: 484416 Color: 1

Bin 2384: 105 of cap free
Amount of items: 2
Items: 
Size: 571678 Color: 1
Size: 428218 Color: 0

Bin 2385: 105 of cap free
Amount of items: 2
Items: 
Size: 593667 Color: 1
Size: 406229 Color: 0

Bin 2386: 105 of cap free
Amount of items: 2
Items: 
Size: 621417 Color: 0
Size: 378479 Color: 1

Bin 2387: 105 of cap free
Amount of items: 2
Items: 
Size: 674557 Color: 0
Size: 325339 Color: 1

Bin 2388: 106 of cap free
Amount of items: 2
Items: 
Size: 508937 Color: 0
Size: 490958 Color: 1

Bin 2389: 106 of cap free
Amount of items: 2
Items: 
Size: 529785 Color: 0
Size: 470110 Color: 1

Bin 2390: 106 of cap free
Amount of items: 2
Items: 
Size: 531370 Color: 1
Size: 468525 Color: 0

Bin 2391: 106 of cap free
Amount of items: 2
Items: 
Size: 581374 Color: 0
Size: 418521 Color: 1

Bin 2392: 106 of cap free
Amount of items: 2
Items: 
Size: 582934 Color: 0
Size: 416961 Color: 1

Bin 2393: 106 of cap free
Amount of items: 2
Items: 
Size: 744322 Color: 1
Size: 255573 Color: 0

Bin 2394: 106 of cap free
Amount of items: 3
Items: 
Size: 780766 Color: 1
Size: 110588 Color: 1
Size: 108541 Color: 0

Bin 2395: 107 of cap free
Amount of items: 2
Items: 
Size: 540380 Color: 1
Size: 459514 Color: 0

Bin 2396: 107 of cap free
Amount of items: 2
Items: 
Size: 549253 Color: 1
Size: 450641 Color: 0

Bin 2397: 107 of cap free
Amount of items: 2
Items: 
Size: 593950 Color: 1
Size: 405944 Color: 0

Bin 2398: 107 of cap free
Amount of items: 2
Items: 
Size: 645979 Color: 1
Size: 353915 Color: 0

Bin 2399: 107 of cap free
Amount of items: 2
Items: 
Size: 661901 Color: 0
Size: 337993 Color: 1

Bin 2400: 107 of cap free
Amount of items: 2
Items: 
Size: 692847 Color: 1
Size: 307047 Color: 0

Bin 2401: 107 of cap free
Amount of items: 2
Items: 
Size: 698374 Color: 0
Size: 301520 Color: 1

Bin 2402: 107 of cap free
Amount of items: 2
Items: 
Size: 726866 Color: 0
Size: 273028 Color: 1

Bin 2403: 107 of cap free
Amount of items: 2
Items: 
Size: 770101 Color: 1
Size: 229793 Color: 0

Bin 2404: 108 of cap free
Amount of items: 2
Items: 
Size: 552129 Color: 1
Size: 447764 Color: 0

Bin 2405: 108 of cap free
Amount of items: 2
Items: 
Size: 621878 Color: 0
Size: 378015 Color: 1

Bin 2406: 108 of cap free
Amount of items: 2
Items: 
Size: 625502 Color: 1
Size: 374391 Color: 0

Bin 2407: 108 of cap free
Amount of items: 2
Items: 
Size: 706593 Color: 0
Size: 293300 Color: 1

Bin 2408: 108 of cap free
Amount of items: 2
Items: 
Size: 738158 Color: 0
Size: 261735 Color: 1

Bin 2409: 108 of cap free
Amount of items: 3
Items: 
Size: 773899 Color: 1
Size: 113607 Color: 1
Size: 112387 Color: 0

Bin 2410: 108 of cap free
Amount of items: 2
Items: 
Size: 778000 Color: 0
Size: 221893 Color: 1

Bin 2411: 108 of cap free
Amount of items: 2
Items: 
Size: 796557 Color: 0
Size: 203336 Color: 1

Bin 2412: 109 of cap free
Amount of items: 2
Items: 
Size: 594271 Color: 0
Size: 405621 Color: 1

Bin 2413: 109 of cap free
Amount of items: 2
Items: 
Size: 595897 Color: 0
Size: 403995 Color: 1

Bin 2414: 109 of cap free
Amount of items: 2
Items: 
Size: 688126 Color: 0
Size: 311766 Color: 1

Bin 2415: 109 of cap free
Amount of items: 2
Items: 
Size: 708321 Color: 1
Size: 291571 Color: 0

Bin 2416: 109 of cap free
Amount of items: 2
Items: 
Size: 711317 Color: 0
Size: 288575 Color: 1

Bin 2417: 109 of cap free
Amount of items: 2
Items: 
Size: 721761 Color: 1
Size: 278131 Color: 0

Bin 2418: 109 of cap free
Amount of items: 2
Items: 
Size: 749788 Color: 1
Size: 250104 Color: 0

Bin 2419: 109 of cap free
Amount of items: 2
Items: 
Size: 769599 Color: 1
Size: 230293 Color: 0

Bin 2420: 109 of cap free
Amount of items: 3
Items: 
Size: 782502 Color: 0
Size: 115899 Color: 1
Size: 101491 Color: 1

Bin 2421: 109 of cap free
Amount of items: 2
Items: 
Size: 783187 Color: 1
Size: 216705 Color: 0

Bin 2422: 110 of cap free
Amount of items: 2
Items: 
Size: 533997 Color: 0
Size: 465894 Color: 1

Bin 2423: 110 of cap free
Amount of items: 2
Items: 
Size: 549825 Color: 1
Size: 450066 Color: 0

Bin 2424: 110 of cap free
Amount of items: 2
Items: 
Size: 550566 Color: 1
Size: 449325 Color: 0

Bin 2425: 110 of cap free
Amount of items: 2
Items: 
Size: 560864 Color: 1
Size: 439027 Color: 0

Bin 2426: 110 of cap free
Amount of items: 2
Items: 
Size: 609633 Color: 1
Size: 390258 Color: 0

Bin 2427: 110 of cap free
Amount of items: 2
Items: 
Size: 721576 Color: 0
Size: 278315 Color: 1

Bin 2428: 110 of cap free
Amount of items: 2
Items: 
Size: 758272 Color: 1
Size: 241619 Color: 0

Bin 2429: 110 of cap free
Amount of items: 2
Items: 
Size: 776050 Color: 0
Size: 223841 Color: 1

Bin 2430: 110 of cap free
Amount of items: 3
Items: 
Size: 789444 Color: 1
Size: 107094 Color: 0
Size: 103353 Color: 0

Bin 2431: 111 of cap free
Amount of items: 2
Items: 
Size: 557668 Color: 1
Size: 442222 Color: 0

Bin 2432: 111 of cap free
Amount of items: 2
Items: 
Size: 575450 Color: 0
Size: 424440 Color: 1

Bin 2433: 111 of cap free
Amount of items: 2
Items: 
Size: 623498 Color: 1
Size: 376392 Color: 0

Bin 2434: 111 of cap free
Amount of items: 2
Items: 
Size: 646614 Color: 0
Size: 353276 Color: 1

Bin 2435: 111 of cap free
Amount of items: 2
Items: 
Size: 655232 Color: 0
Size: 344658 Color: 1

Bin 2436: 111 of cap free
Amount of items: 2
Items: 
Size: 683948 Color: 0
Size: 315942 Color: 1

Bin 2437: 111 of cap free
Amount of items: 2
Items: 
Size: 687699 Color: 1
Size: 312191 Color: 0

Bin 2438: 111 of cap free
Amount of items: 2
Items: 
Size: 725916 Color: 0
Size: 273974 Color: 1

Bin 2439: 111 of cap free
Amount of items: 2
Items: 
Size: 726669 Color: 1
Size: 273221 Color: 0

Bin 2440: 111 of cap free
Amount of items: 2
Items: 
Size: 729406 Color: 0
Size: 270484 Color: 1

Bin 2441: 111 of cap free
Amount of items: 2
Items: 
Size: 737765 Color: 1
Size: 262125 Color: 0

Bin 2442: 111 of cap free
Amount of items: 2
Items: 
Size: 738636 Color: 1
Size: 261254 Color: 0

Bin 2443: 111 of cap free
Amount of items: 2
Items: 
Size: 753660 Color: 1
Size: 246230 Color: 0

Bin 2444: 111 of cap free
Amount of items: 2
Items: 
Size: 780713 Color: 0
Size: 219177 Color: 1

Bin 2445: 111 of cap free
Amount of items: 2
Items: 
Size: 790286 Color: 0
Size: 209604 Color: 1

Bin 2446: 111 of cap free
Amount of items: 2
Items: 
Size: 799075 Color: 1
Size: 200815 Color: 0

Bin 2447: 112 of cap free
Amount of items: 2
Items: 
Size: 510575 Color: 0
Size: 489314 Color: 1

Bin 2448: 112 of cap free
Amount of items: 2
Items: 
Size: 517496 Color: 1
Size: 482393 Color: 0

Bin 2449: 112 of cap free
Amount of items: 2
Items: 
Size: 554508 Color: 0
Size: 445381 Color: 1

Bin 2450: 112 of cap free
Amount of items: 2
Items: 
Size: 640097 Color: 1
Size: 359792 Color: 0

Bin 2451: 112 of cap free
Amount of items: 2
Items: 
Size: 669808 Color: 1
Size: 330081 Color: 0

Bin 2452: 112 of cap free
Amount of items: 2
Items: 
Size: 671977 Color: 1
Size: 327912 Color: 0

Bin 2453: 112 of cap free
Amount of items: 2
Items: 
Size: 680300 Color: 1
Size: 319589 Color: 0

Bin 2454: 112 of cap free
Amount of items: 2
Items: 
Size: 747026 Color: 1
Size: 252863 Color: 0

Bin 2455: 112 of cap free
Amount of items: 2
Items: 
Size: 759935 Color: 0
Size: 239954 Color: 1

Bin 2456: 112 of cap free
Amount of items: 2
Items: 
Size: 778815 Color: 0
Size: 221074 Color: 1

Bin 2457: 112 of cap free
Amount of items: 2
Items: 
Size: 796295 Color: 1
Size: 203594 Color: 0

Bin 2458: 113 of cap free
Amount of items: 3
Items: 
Size: 382419 Color: 0
Size: 338308 Color: 0
Size: 279161 Color: 1

Bin 2459: 113 of cap free
Amount of items: 2
Items: 
Size: 513612 Color: 0
Size: 486276 Color: 1

Bin 2460: 113 of cap free
Amount of items: 2
Items: 
Size: 603365 Color: 0
Size: 396523 Color: 1

Bin 2461: 113 of cap free
Amount of items: 2
Items: 
Size: 613383 Color: 0
Size: 386505 Color: 1

Bin 2462: 113 of cap free
Amount of items: 2
Items: 
Size: 639600 Color: 0
Size: 360288 Color: 1

Bin 2463: 113 of cap free
Amount of items: 2
Items: 
Size: 650487 Color: 0
Size: 349401 Color: 1

Bin 2464: 113 of cap free
Amount of items: 2
Items: 
Size: 657427 Color: 0
Size: 342461 Color: 1

Bin 2465: 113 of cap free
Amount of items: 2
Items: 
Size: 682770 Color: 0
Size: 317118 Color: 1

Bin 2466: 113 of cap free
Amount of items: 2
Items: 
Size: 683541 Color: 0
Size: 316347 Color: 1

Bin 2467: 113 of cap free
Amount of items: 2
Items: 
Size: 736741 Color: 1
Size: 263147 Color: 0

Bin 2468: 113 of cap free
Amount of items: 2
Items: 
Size: 745444 Color: 0
Size: 254444 Color: 1

Bin 2469: 113 of cap free
Amount of items: 2
Items: 
Size: 767101 Color: 1
Size: 232787 Color: 0

Bin 2470: 113 of cap free
Amount of items: 2
Items: 
Size: 770728 Color: 0
Size: 229160 Color: 1

Bin 2471: 114 of cap free
Amount of items: 2
Items: 
Size: 519475 Color: 1
Size: 480412 Color: 0

Bin 2472: 114 of cap free
Amount of items: 2
Items: 
Size: 549942 Color: 0
Size: 449945 Color: 1

Bin 2473: 114 of cap free
Amount of items: 2
Items: 
Size: 558663 Color: 1
Size: 441224 Color: 0

Bin 2474: 114 of cap free
Amount of items: 2
Items: 
Size: 589208 Color: 1
Size: 410679 Color: 0

Bin 2475: 114 of cap free
Amount of items: 2
Items: 
Size: 599639 Color: 0
Size: 400248 Color: 1

Bin 2476: 115 of cap free
Amount of items: 2
Items: 
Size: 539865 Color: 1
Size: 460021 Color: 0

Bin 2477: 115 of cap free
Amount of items: 2
Items: 
Size: 546313 Color: 1
Size: 453573 Color: 0

Bin 2478: 115 of cap free
Amount of items: 2
Items: 
Size: 576576 Color: 1
Size: 423310 Color: 0

Bin 2479: 115 of cap free
Amount of items: 2
Items: 
Size: 618787 Color: 0
Size: 381099 Color: 1

Bin 2480: 115 of cap free
Amount of items: 2
Items: 
Size: 641014 Color: 0
Size: 358872 Color: 1

Bin 2481: 115 of cap free
Amount of items: 2
Items: 
Size: 669265 Color: 1
Size: 330621 Color: 0

Bin 2482: 115 of cap free
Amount of items: 2
Items: 
Size: 682582 Color: 1
Size: 317304 Color: 0

Bin 2483: 115 of cap free
Amount of items: 2
Items: 
Size: 750255 Color: 1
Size: 249631 Color: 0

Bin 2484: 115 of cap free
Amount of items: 2
Items: 
Size: 755928 Color: 0
Size: 243958 Color: 1

Bin 2485: 115 of cap free
Amount of items: 2
Items: 
Size: 768105 Color: 1
Size: 231781 Color: 0

Bin 2486: 115 of cap free
Amount of items: 2
Items: 
Size: 797703 Color: 1
Size: 202183 Color: 0

Bin 2487: 116 of cap free
Amount of items: 2
Items: 
Size: 507593 Color: 0
Size: 492292 Color: 1

Bin 2488: 116 of cap free
Amount of items: 2
Items: 
Size: 519075 Color: 1
Size: 480810 Color: 0

Bin 2489: 116 of cap free
Amount of items: 2
Items: 
Size: 522276 Color: 1
Size: 477609 Color: 0

Bin 2490: 116 of cap free
Amount of items: 2
Items: 
Size: 545370 Color: 0
Size: 454515 Color: 1

Bin 2491: 116 of cap free
Amount of items: 2
Items: 
Size: 581152 Color: 1
Size: 418733 Color: 0

Bin 2492: 116 of cap free
Amount of items: 2
Items: 
Size: 617527 Color: 0
Size: 382358 Color: 1

Bin 2493: 116 of cap free
Amount of items: 2
Items: 
Size: 670042 Color: 0
Size: 329843 Color: 1

Bin 2494: 116 of cap free
Amount of items: 2
Items: 
Size: 673073 Color: 1
Size: 326812 Color: 0

Bin 2495: 116 of cap free
Amount of items: 2
Items: 
Size: 740626 Color: 0
Size: 259259 Color: 1

Bin 2496: 116 of cap free
Amount of items: 2
Items: 
Size: 756718 Color: 0
Size: 243167 Color: 1

Bin 2497: 116 of cap free
Amount of items: 2
Items: 
Size: 756991 Color: 0
Size: 242894 Color: 1

Bin 2498: 116 of cap free
Amount of items: 2
Items: 
Size: 759409 Color: 1
Size: 240476 Color: 0

Bin 2499: 116 of cap free
Amount of items: 2
Items: 
Size: 764561 Color: 0
Size: 235324 Color: 1

Bin 2500: 116 of cap free
Amount of items: 2
Items: 
Size: 767259 Color: 0
Size: 232626 Color: 1

Bin 2501: 116 of cap free
Amount of items: 2
Items: 
Size: 781227 Color: 1
Size: 218658 Color: 0

Bin 2502: 116 of cap free
Amount of items: 2
Items: 
Size: 788726 Color: 0
Size: 211159 Color: 1

Bin 2503: 117 of cap free
Amount of items: 2
Items: 
Size: 516090 Color: 0
Size: 483794 Color: 1

Bin 2504: 117 of cap free
Amount of items: 2
Items: 
Size: 560220 Color: 1
Size: 439664 Color: 0

Bin 2505: 117 of cap free
Amount of items: 2
Items: 
Size: 586010 Color: 0
Size: 413874 Color: 1

Bin 2506: 117 of cap free
Amount of items: 2
Items: 
Size: 693850 Color: 1
Size: 306034 Color: 0

Bin 2507: 117 of cap free
Amount of items: 2
Items: 
Size: 731171 Color: 0
Size: 268713 Color: 1

Bin 2508: 117 of cap free
Amount of items: 2
Items: 
Size: 786852 Color: 0
Size: 213032 Color: 1

Bin 2509: 118 of cap free
Amount of items: 2
Items: 
Size: 522724 Color: 1
Size: 477159 Color: 0

Bin 2510: 118 of cap free
Amount of items: 2
Items: 
Size: 541135 Color: 1
Size: 458748 Color: 0

Bin 2511: 118 of cap free
Amount of items: 2
Items: 
Size: 565164 Color: 0
Size: 434719 Color: 1

Bin 2512: 118 of cap free
Amount of items: 2
Items: 
Size: 584028 Color: 0
Size: 415855 Color: 1

Bin 2513: 118 of cap free
Amount of items: 2
Items: 
Size: 672813 Color: 0
Size: 327070 Color: 1

Bin 2514: 118 of cap free
Amount of items: 2
Items: 
Size: 723446 Color: 0
Size: 276437 Color: 1

Bin 2515: 118 of cap free
Amount of items: 2
Items: 
Size: 728763 Color: 0
Size: 271120 Color: 1

Bin 2516: 118 of cap free
Amount of items: 2
Items: 
Size: 738363 Color: 1
Size: 261520 Color: 0

Bin 2517: 119 of cap free
Amount of items: 2
Items: 
Size: 504527 Color: 1
Size: 495355 Color: 0

Bin 2518: 119 of cap free
Amount of items: 2
Items: 
Size: 504840 Color: 1
Size: 495042 Color: 0

Bin 2519: 119 of cap free
Amount of items: 2
Items: 
Size: 514391 Color: 1
Size: 485491 Color: 0

Bin 2520: 119 of cap free
Amount of items: 2
Items: 
Size: 520648 Color: 0
Size: 479234 Color: 1

Bin 2521: 119 of cap free
Amount of items: 2
Items: 
Size: 566047 Color: 0
Size: 433835 Color: 1

Bin 2522: 119 of cap free
Amount of items: 2
Items: 
Size: 584233 Color: 0
Size: 415649 Color: 1

Bin 2523: 119 of cap free
Amount of items: 2
Items: 
Size: 600667 Color: 0
Size: 399215 Color: 1

Bin 2524: 119 of cap free
Amount of items: 2
Items: 
Size: 672859 Color: 1
Size: 327023 Color: 0

Bin 2525: 119 of cap free
Amount of items: 2
Items: 
Size: 675011 Color: 0
Size: 324871 Color: 1

Bin 2526: 119 of cap free
Amount of items: 2
Items: 
Size: 685035 Color: 0
Size: 314847 Color: 1

Bin 2527: 119 of cap free
Amount of items: 2
Items: 
Size: 734466 Color: 1
Size: 265416 Color: 0

Bin 2528: 119 of cap free
Amount of items: 2
Items: 
Size: 734647 Color: 0
Size: 265235 Color: 1

Bin 2529: 119 of cap free
Amount of items: 2
Items: 
Size: 776556 Color: 1
Size: 223326 Color: 0

Bin 2530: 119 of cap free
Amount of items: 2
Items: 
Size: 782132 Color: 1
Size: 217750 Color: 0

Bin 2531: 120 of cap free
Amount of items: 2
Items: 
Size: 568564 Color: 0
Size: 431317 Color: 1

Bin 2532: 120 of cap free
Amount of items: 2
Items: 
Size: 674186 Color: 1
Size: 325695 Color: 0

Bin 2533: 120 of cap free
Amount of items: 2
Items: 
Size: 709735 Color: 1
Size: 290146 Color: 0

Bin 2534: 121 of cap free
Amount of items: 2
Items: 
Size: 524784 Color: 1
Size: 475096 Color: 0

Bin 2535: 121 of cap free
Amount of items: 2
Items: 
Size: 581125 Color: 0
Size: 418755 Color: 1

Bin 2536: 121 of cap free
Amount of items: 2
Items: 
Size: 598821 Color: 0
Size: 401059 Color: 1

Bin 2537: 121 of cap free
Amount of items: 2
Items: 
Size: 653206 Color: 0
Size: 346674 Color: 1

Bin 2538: 122 of cap free
Amount of items: 2
Items: 
Size: 508631 Color: 1
Size: 491248 Color: 0

Bin 2539: 122 of cap free
Amount of items: 2
Items: 
Size: 516457 Color: 1
Size: 483422 Color: 0

Bin 2540: 122 of cap free
Amount of items: 2
Items: 
Size: 519555 Color: 0
Size: 480324 Color: 1

Bin 2541: 122 of cap free
Amount of items: 2
Items: 
Size: 548331 Color: 0
Size: 451548 Color: 1

Bin 2542: 122 of cap free
Amount of items: 2
Items: 
Size: 585643 Color: 1
Size: 414236 Color: 0

Bin 2543: 122 of cap free
Amount of items: 2
Items: 
Size: 653087 Color: 1
Size: 346792 Color: 0

Bin 2544: 122 of cap free
Amount of items: 2
Items: 
Size: 667194 Color: 0
Size: 332685 Color: 1

Bin 2545: 122 of cap free
Amount of items: 2
Items: 
Size: 677194 Color: 1
Size: 322685 Color: 0

Bin 2546: 122 of cap free
Amount of items: 2
Items: 
Size: 717219 Color: 0
Size: 282660 Color: 1

Bin 2547: 122 of cap free
Amount of items: 2
Items: 
Size: 726891 Color: 1
Size: 272988 Color: 0

Bin 2548: 122 of cap free
Amount of items: 2
Items: 
Size: 780963 Color: 1
Size: 218916 Color: 0

Bin 2549: 122 of cap free
Amount of items: 2
Items: 
Size: 793096 Color: 1
Size: 206783 Color: 0

Bin 2550: 123 of cap free
Amount of items: 2
Items: 
Size: 633443 Color: 0
Size: 366435 Color: 1

Bin 2551: 123 of cap free
Amount of items: 2
Items: 
Size: 685556 Color: 0
Size: 314322 Color: 1

Bin 2552: 123 of cap free
Amount of items: 2
Items: 
Size: 721950 Color: 0
Size: 277928 Color: 1

Bin 2553: 123 of cap free
Amount of items: 2
Items: 
Size: 752164 Color: 1
Size: 247714 Color: 0

Bin 2554: 123 of cap free
Amount of items: 2
Items: 
Size: 756103 Color: 0
Size: 243775 Color: 1

Bin 2555: 123 of cap free
Amount of items: 2
Items: 
Size: 790283 Color: 0
Size: 209595 Color: 1

Bin 2556: 124 of cap free
Amount of items: 2
Items: 
Size: 542547 Color: 0
Size: 457330 Color: 1

Bin 2557: 124 of cap free
Amount of items: 2
Items: 
Size: 570505 Color: 1
Size: 429372 Color: 0

Bin 2558: 124 of cap free
Amount of items: 2
Items: 
Size: 590159 Color: 1
Size: 409718 Color: 0

Bin 2559: 124 of cap free
Amount of items: 2
Items: 
Size: 615768 Color: 0
Size: 384109 Color: 1

Bin 2560: 124 of cap free
Amount of items: 2
Items: 
Size: 638104 Color: 0
Size: 361773 Color: 1

Bin 2561: 124 of cap free
Amount of items: 2
Items: 
Size: 727816 Color: 0
Size: 272061 Color: 1

Bin 2562: 124 of cap free
Amount of items: 2
Items: 
Size: 730993 Color: 1
Size: 268884 Color: 0

Bin 2563: 124 of cap free
Amount of items: 2
Items: 
Size: 744007 Color: 1
Size: 255870 Color: 0

Bin 2564: 124 of cap free
Amount of items: 2
Items: 
Size: 745750 Color: 1
Size: 254127 Color: 0

Bin 2565: 124 of cap free
Amount of items: 2
Items: 
Size: 766968 Color: 1
Size: 232909 Color: 0

Bin 2566: 124 of cap free
Amount of items: 2
Items: 
Size: 781047 Color: 0
Size: 218830 Color: 1

Bin 2567: 125 of cap free
Amount of items: 2
Items: 
Size: 606962 Color: 1
Size: 392914 Color: 0

Bin 2568: 125 of cap free
Amount of items: 2
Items: 
Size: 663777 Color: 1
Size: 336099 Color: 0

Bin 2569: 126 of cap free
Amount of items: 2
Items: 
Size: 548731 Color: 1
Size: 451144 Color: 0

Bin 2570: 126 of cap free
Amount of items: 2
Items: 
Size: 583488 Color: 1
Size: 416387 Color: 0

Bin 2571: 126 of cap free
Amount of items: 2
Items: 
Size: 708240 Color: 0
Size: 291635 Color: 1

Bin 2572: 127 of cap free
Amount of items: 2
Items: 
Size: 587404 Color: 0
Size: 412470 Color: 1

Bin 2573: 127 of cap free
Amount of items: 2
Items: 
Size: 601751 Color: 1
Size: 398123 Color: 0

Bin 2574: 127 of cap free
Amount of items: 2
Items: 
Size: 686450 Color: 0
Size: 313424 Color: 1

Bin 2575: 127 of cap free
Amount of items: 2
Items: 
Size: 687314 Color: 1
Size: 312560 Color: 0

Bin 2576: 127 of cap free
Amount of items: 2
Items: 
Size: 694328 Color: 1
Size: 305546 Color: 0

Bin 2577: 127 of cap free
Amount of items: 2
Items: 
Size: 737217 Color: 0
Size: 262657 Color: 1

Bin 2578: 127 of cap free
Amount of items: 2
Items: 
Size: 766463 Color: 1
Size: 233411 Color: 0

Bin 2579: 128 of cap free
Amount of items: 2
Items: 
Size: 508487 Color: 0
Size: 491386 Color: 1

Bin 2580: 128 of cap free
Amount of items: 2
Items: 
Size: 510155 Color: 0
Size: 489718 Color: 1

Bin 2581: 128 of cap free
Amount of items: 2
Items: 
Size: 570242 Color: 0
Size: 429631 Color: 1

Bin 2582: 128 of cap free
Amount of items: 2
Items: 
Size: 612631 Color: 0
Size: 387242 Color: 1

Bin 2583: 128 of cap free
Amount of items: 2
Items: 
Size: 612948 Color: 0
Size: 386925 Color: 1

Bin 2584: 128 of cap free
Amount of items: 2
Items: 
Size: 684579 Color: 1
Size: 315294 Color: 0

Bin 2585: 129 of cap free
Amount of items: 2
Items: 
Size: 546565 Color: 0
Size: 453307 Color: 1

Bin 2586: 129 of cap free
Amount of items: 2
Items: 
Size: 651420 Color: 1
Size: 348452 Color: 0

Bin 2587: 129 of cap free
Amount of items: 2
Items: 
Size: 676899 Color: 0
Size: 322973 Color: 1

Bin 2588: 129 of cap free
Amount of items: 2
Items: 
Size: 716505 Color: 0
Size: 283367 Color: 1

Bin 2589: 129 of cap free
Amount of items: 2
Items: 
Size: 731908 Color: 1
Size: 267964 Color: 0

Bin 2590: 130 of cap free
Amount of items: 2
Items: 
Size: 608167 Color: 1
Size: 391704 Color: 0

Bin 2591: 130 of cap free
Amount of items: 2
Items: 
Size: 633229 Color: 0
Size: 366642 Color: 1

Bin 2592: 130 of cap free
Amount of items: 2
Items: 
Size: 690571 Color: 1
Size: 309300 Color: 0

Bin 2593: 130 of cap free
Amount of items: 2
Items: 
Size: 745170 Color: 1
Size: 254701 Color: 0

Bin 2594: 130 of cap free
Amount of items: 2
Items: 
Size: 784291 Color: 1
Size: 215580 Color: 0

Bin 2595: 131 of cap free
Amount of items: 2
Items: 
Size: 570096 Color: 1
Size: 429774 Color: 0

Bin 2596: 131 of cap free
Amount of items: 2
Items: 
Size: 574257 Color: 0
Size: 425613 Color: 1

Bin 2597: 131 of cap free
Amount of items: 2
Items: 
Size: 592928 Color: 1
Size: 406942 Color: 0

Bin 2598: 131 of cap free
Amount of items: 2
Items: 
Size: 623202 Color: 0
Size: 376668 Color: 1

Bin 2599: 131 of cap free
Amount of items: 2
Items: 
Size: 644387 Color: 1
Size: 355483 Color: 0

Bin 2600: 131 of cap free
Amount of items: 2
Items: 
Size: 664243 Color: 1
Size: 335627 Color: 0

Bin 2601: 131 of cap free
Amount of items: 2
Items: 
Size: 725633 Color: 0
Size: 274237 Color: 1

Bin 2602: 131 of cap free
Amount of items: 2
Items: 
Size: 741107 Color: 0
Size: 258763 Color: 1

Bin 2603: 131 of cap free
Amount of items: 2
Items: 
Size: 748080 Color: 0
Size: 251790 Color: 1

Bin 2604: 131 of cap free
Amount of items: 2
Items: 
Size: 769289 Color: 0
Size: 230581 Color: 1

Bin 2605: 132 of cap free
Amount of items: 2
Items: 
Size: 524210 Color: 0
Size: 475659 Color: 1

Bin 2606: 132 of cap free
Amount of items: 2
Items: 
Size: 570825 Color: 1
Size: 429044 Color: 0

Bin 2607: 132 of cap free
Amount of items: 2
Items: 
Size: 576838 Color: 0
Size: 423031 Color: 1

Bin 2608: 132 of cap free
Amount of items: 2
Items: 
Size: 582207 Color: 1
Size: 417662 Color: 0

Bin 2609: 132 of cap free
Amount of items: 2
Items: 
Size: 595713 Color: 0
Size: 404156 Color: 1

Bin 2610: 132 of cap free
Amount of items: 2
Items: 
Size: 643383 Color: 1
Size: 356486 Color: 0

Bin 2611: 132 of cap free
Amount of items: 2
Items: 
Size: 720608 Color: 1
Size: 279261 Color: 0

Bin 2612: 132 of cap free
Amount of items: 2
Items: 
Size: 782695 Color: 1
Size: 217174 Color: 0

Bin 2613: 133 of cap free
Amount of items: 2
Items: 
Size: 506255 Color: 0
Size: 493613 Color: 1

Bin 2614: 133 of cap free
Amount of items: 2
Items: 
Size: 525134 Color: 0
Size: 474734 Color: 1

Bin 2615: 133 of cap free
Amount of items: 2
Items: 
Size: 549294 Color: 0
Size: 450574 Color: 1

Bin 2616: 133 of cap free
Amount of items: 2
Items: 
Size: 671268 Color: 1
Size: 328600 Color: 0

Bin 2617: 133 of cap free
Amount of items: 2
Items: 
Size: 698368 Color: 1
Size: 301500 Color: 0

Bin 2618: 133 of cap free
Amount of items: 2
Items: 
Size: 762129 Color: 0
Size: 237739 Color: 1

Bin 2619: 134 of cap free
Amount of items: 2
Items: 
Size: 503162 Color: 0
Size: 496705 Color: 1

Bin 2620: 134 of cap free
Amount of items: 2
Items: 
Size: 539377 Color: 1
Size: 460490 Color: 0

Bin 2621: 134 of cap free
Amount of items: 2
Items: 
Size: 543972 Color: 0
Size: 455895 Color: 1

Bin 2622: 134 of cap free
Amount of items: 2
Items: 
Size: 594762 Color: 1
Size: 405105 Color: 0

Bin 2623: 134 of cap free
Amount of items: 2
Items: 
Size: 681847 Color: 1
Size: 318020 Color: 0

Bin 2624: 134 of cap free
Amount of items: 2
Items: 
Size: 746575 Color: 1
Size: 253292 Color: 0

Bin 2625: 135 of cap free
Amount of items: 2
Items: 
Size: 604818 Color: 1
Size: 395048 Color: 0

Bin 2626: 135 of cap free
Amount of items: 2
Items: 
Size: 620792 Color: 1
Size: 379074 Color: 0

Bin 2627: 135 of cap free
Amount of items: 2
Items: 
Size: 638370 Color: 0
Size: 361496 Color: 1

Bin 2628: 135 of cap free
Amount of items: 2
Items: 
Size: 655962 Color: 1
Size: 343904 Color: 0

Bin 2629: 135 of cap free
Amount of items: 2
Items: 
Size: 682433 Color: 1
Size: 317433 Color: 0

Bin 2630: 136 of cap free
Amount of items: 2
Items: 
Size: 591540 Color: 0
Size: 408325 Color: 1

Bin 2631: 136 of cap free
Amount of items: 2
Items: 
Size: 615278 Color: 0
Size: 384587 Color: 1

Bin 2632: 136 of cap free
Amount of items: 2
Items: 
Size: 640484 Color: 0
Size: 359381 Color: 1

Bin 2633: 136 of cap free
Amount of items: 2
Items: 
Size: 663539 Color: 0
Size: 336326 Color: 1

Bin 2634: 136 of cap free
Amount of items: 2
Items: 
Size: 696513 Color: 1
Size: 303352 Color: 0

Bin 2635: 136 of cap free
Amount of items: 2
Items: 
Size: 697882 Color: 0
Size: 301983 Color: 1

Bin 2636: 136 of cap free
Amount of items: 2
Items: 
Size: 739896 Color: 1
Size: 259969 Color: 0

Bin 2637: 137 of cap free
Amount of items: 2
Items: 
Size: 501338 Color: 1
Size: 498526 Color: 0

Bin 2638: 137 of cap free
Amount of items: 2
Items: 
Size: 512562 Color: 1
Size: 487302 Color: 0

Bin 2639: 137 of cap free
Amount of items: 2
Items: 
Size: 516085 Color: 0
Size: 483779 Color: 1

Bin 2640: 137 of cap free
Amount of items: 2
Items: 
Size: 595441 Color: 0
Size: 404423 Color: 1

Bin 2641: 137 of cap free
Amount of items: 2
Items: 
Size: 614495 Color: 1
Size: 385369 Color: 0

Bin 2642: 137 of cap free
Amount of items: 2
Items: 
Size: 627371 Color: 0
Size: 372493 Color: 1

Bin 2643: 137 of cap free
Amount of items: 2
Items: 
Size: 637246 Color: 1
Size: 362618 Color: 0

Bin 2644: 137 of cap free
Amount of items: 2
Items: 
Size: 663945 Color: 0
Size: 335919 Color: 1

Bin 2645: 137 of cap free
Amount of items: 2
Items: 
Size: 676896 Color: 0
Size: 322968 Color: 1

Bin 2646: 137 of cap free
Amount of items: 2
Items: 
Size: 679650 Color: 0
Size: 320214 Color: 1

Bin 2647: 137 of cap free
Amount of items: 2
Items: 
Size: 694572 Color: 1
Size: 305292 Color: 0

Bin 2648: 137 of cap free
Amount of items: 2
Items: 
Size: 744461 Color: 1
Size: 255403 Color: 0

Bin 2649: 137 of cap free
Amount of items: 2
Items: 
Size: 799337 Color: 0
Size: 200527 Color: 1

Bin 2650: 138 of cap free
Amount of items: 2
Items: 
Size: 521967 Color: 0
Size: 477896 Color: 1

Bin 2651: 138 of cap free
Amount of items: 2
Items: 
Size: 557946 Color: 1
Size: 441917 Color: 0

Bin 2652: 138 of cap free
Amount of items: 2
Items: 
Size: 579801 Color: 1
Size: 420062 Color: 0

Bin 2653: 138 of cap free
Amount of items: 2
Items: 
Size: 607909 Color: 0
Size: 391954 Color: 1

Bin 2654: 138 of cap free
Amount of items: 2
Items: 
Size: 625879 Color: 1
Size: 373984 Color: 0

Bin 2655: 138 of cap free
Amount of items: 2
Items: 
Size: 683786 Color: 1
Size: 316077 Color: 0

Bin 2656: 138 of cap free
Amount of items: 2
Items: 
Size: 706890 Color: 0
Size: 292973 Color: 1

Bin 2657: 138 of cap free
Amount of items: 2
Items: 
Size: 716957 Color: 0
Size: 282906 Color: 1

Bin 2658: 138 of cap free
Amount of items: 2
Items: 
Size: 758560 Color: 1
Size: 241303 Color: 0

Bin 2659: 138 of cap free
Amount of items: 2
Items: 
Size: 787124 Color: 0
Size: 212739 Color: 1

Bin 2660: 139 of cap free
Amount of items: 2
Items: 
Size: 501072 Color: 1
Size: 498790 Color: 0

Bin 2661: 139 of cap free
Amount of items: 2
Items: 
Size: 518470 Color: 1
Size: 481392 Color: 0

Bin 2662: 139 of cap free
Amount of items: 2
Items: 
Size: 550984 Color: 1
Size: 448878 Color: 0

Bin 2663: 139 of cap free
Amount of items: 2
Items: 
Size: 573599 Color: 1
Size: 426263 Color: 0

Bin 2664: 139 of cap free
Amount of items: 2
Items: 
Size: 597507 Color: 0
Size: 402355 Color: 1

Bin 2665: 139 of cap free
Amount of items: 2
Items: 
Size: 604850 Color: 0
Size: 395012 Color: 1

Bin 2666: 139 of cap free
Amount of items: 2
Items: 
Size: 659706 Color: 0
Size: 340156 Color: 1

Bin 2667: 139 of cap free
Amount of items: 2
Items: 
Size: 712901 Color: 0
Size: 286961 Color: 1

Bin 2668: 139 of cap free
Amount of items: 2
Items: 
Size: 737507 Color: 0
Size: 262355 Color: 1

Bin 2669: 139 of cap free
Amount of items: 2
Items: 
Size: 743281 Color: 1
Size: 256581 Color: 0

Bin 2670: 140 of cap free
Amount of items: 2
Items: 
Size: 519463 Color: 1
Size: 480398 Color: 0

Bin 2671: 140 of cap free
Amount of items: 2
Items: 
Size: 533023 Color: 1
Size: 466838 Color: 0

Bin 2672: 140 of cap free
Amount of items: 2
Items: 
Size: 543022 Color: 1
Size: 456839 Color: 0

Bin 2673: 140 of cap free
Amount of items: 2
Items: 
Size: 560545 Color: 1
Size: 439316 Color: 0

Bin 2674: 140 of cap free
Amount of items: 2
Items: 
Size: 682970 Color: 1
Size: 316891 Color: 0

Bin 2675: 140 of cap free
Amount of items: 2
Items: 
Size: 704744 Color: 0
Size: 295117 Color: 1

Bin 2676: 140 of cap free
Amount of items: 2
Items: 
Size: 762140 Color: 1
Size: 237721 Color: 0

Bin 2677: 140 of cap free
Amount of items: 2
Items: 
Size: 763490 Color: 0
Size: 236371 Color: 1

Bin 2678: 140 of cap free
Amount of items: 2
Items: 
Size: 799577 Color: 0
Size: 200284 Color: 1

Bin 2679: 141 of cap free
Amount of items: 2
Items: 
Size: 512531 Color: 0
Size: 487329 Color: 1

Bin 2680: 141 of cap free
Amount of items: 2
Items: 
Size: 572916 Color: 1
Size: 426944 Color: 0

Bin 2681: 141 of cap free
Amount of items: 2
Items: 
Size: 639030 Color: 1
Size: 360830 Color: 0

Bin 2682: 141 of cap free
Amount of items: 2
Items: 
Size: 690545 Color: 0
Size: 309315 Color: 1

Bin 2683: 141 of cap free
Amount of items: 2
Items: 
Size: 722568 Color: 1
Size: 277292 Color: 0

Bin 2684: 142 of cap free
Amount of items: 2
Items: 
Size: 523990 Color: 1
Size: 475869 Color: 0

Bin 2685: 142 of cap free
Amount of items: 2
Items: 
Size: 540824 Color: 1
Size: 459035 Color: 0

Bin 2686: 142 of cap free
Amount of items: 2
Items: 
Size: 608318 Color: 1
Size: 391541 Color: 0

Bin 2687: 142 of cap free
Amount of items: 2
Items: 
Size: 742490 Color: 0
Size: 257369 Color: 1

Bin 2688: 142 of cap free
Amount of items: 2
Items: 
Size: 768099 Color: 1
Size: 231760 Color: 0

Bin 2689: 142 of cap free
Amount of items: 3
Items: 
Size: 776268 Color: 0
Size: 112931 Color: 1
Size: 110660 Color: 0

Bin 2690: 142 of cap free
Amount of items: 2
Items: 
Size: 795873 Color: 0
Size: 203986 Color: 1

Bin 2691: 143 of cap free
Amount of items: 2
Items: 
Size: 512050 Color: 1
Size: 487808 Color: 0

Bin 2692: 143 of cap free
Amount of items: 2
Items: 
Size: 530387 Color: 1
Size: 469471 Color: 0

Bin 2693: 143 of cap free
Amount of items: 2
Items: 
Size: 627207 Color: 1
Size: 372651 Color: 0

Bin 2694: 143 of cap free
Amount of items: 2
Items: 
Size: 682342 Color: 0
Size: 317516 Color: 1

Bin 2695: 143 of cap free
Amount of items: 2
Items: 
Size: 688163 Color: 1
Size: 311695 Color: 0

Bin 2696: 143 of cap free
Amount of items: 2
Items: 
Size: 689120 Color: 1
Size: 310738 Color: 0

Bin 2697: 143 of cap free
Amount of items: 2
Items: 
Size: 707828 Color: 1
Size: 292030 Color: 0

Bin 2698: 143 of cap free
Amount of items: 2
Items: 
Size: 734919 Color: 0
Size: 264939 Color: 1

Bin 2699: 144 of cap free
Amount of items: 2
Items: 
Size: 541712 Color: 1
Size: 458145 Color: 0

Bin 2700: 144 of cap free
Amount of items: 2
Items: 
Size: 554830 Color: 1
Size: 445027 Color: 0

Bin 2701: 144 of cap free
Amount of items: 2
Items: 
Size: 638474 Color: 1
Size: 361383 Color: 0

Bin 2702: 144 of cap free
Amount of items: 2
Items: 
Size: 746062 Color: 0
Size: 253795 Color: 1

Bin 2703: 145 of cap free
Amount of items: 2
Items: 
Size: 585400 Color: 1
Size: 414456 Color: 0

Bin 2704: 145 of cap free
Amount of items: 2
Items: 
Size: 656843 Color: 0
Size: 343013 Color: 1

Bin 2705: 146 of cap free
Amount of items: 2
Items: 
Size: 502516 Color: 0
Size: 497339 Color: 1

Bin 2706: 146 of cap free
Amount of items: 2
Items: 
Size: 506111 Color: 1
Size: 493744 Color: 0

Bin 2707: 146 of cap free
Amount of items: 2
Items: 
Size: 523203 Color: 1
Size: 476652 Color: 0

Bin 2708: 146 of cap free
Amount of items: 2
Items: 
Size: 560112 Color: 0
Size: 439743 Color: 1

Bin 2709: 146 of cap free
Amount of items: 2
Items: 
Size: 609040 Color: 1
Size: 390815 Color: 0

Bin 2710: 146 of cap free
Amount of items: 2
Items: 
Size: 635089 Color: 0
Size: 364766 Color: 1

Bin 2711: 146 of cap free
Amount of items: 2
Items: 
Size: 648334 Color: 0
Size: 351521 Color: 1

Bin 2712: 146 of cap free
Amount of items: 2
Items: 
Size: 713741 Color: 0
Size: 286114 Color: 1

Bin 2713: 146 of cap free
Amount of items: 2
Items: 
Size: 727410 Color: 0
Size: 272445 Color: 1

Bin 2714: 146 of cap free
Amount of items: 2
Items: 
Size: 787738 Color: 0
Size: 212117 Color: 1

Bin 2715: 147 of cap free
Amount of items: 2
Items: 
Size: 509184 Color: 0
Size: 490670 Color: 1

Bin 2716: 147 of cap free
Amount of items: 2
Items: 
Size: 512879 Color: 1
Size: 486975 Color: 0

Bin 2717: 147 of cap free
Amount of items: 2
Items: 
Size: 569210 Color: 1
Size: 430644 Color: 0

Bin 2718: 147 of cap free
Amount of items: 2
Items: 
Size: 617222 Color: 0
Size: 382632 Color: 1

Bin 2719: 147 of cap free
Amount of items: 2
Items: 
Size: 638097 Color: 1
Size: 361757 Color: 0

Bin 2720: 147 of cap free
Amount of items: 2
Items: 
Size: 680294 Color: 1
Size: 319560 Color: 0

Bin 2721: 147 of cap free
Amount of items: 2
Items: 
Size: 691710 Color: 1
Size: 308144 Color: 0

Bin 2722: 148 of cap free
Amount of items: 2
Items: 
Size: 519235 Color: 0
Size: 480618 Color: 1

Bin 2723: 148 of cap free
Amount of items: 2
Items: 
Size: 531008 Color: 1
Size: 468845 Color: 0

Bin 2724: 148 of cap free
Amount of items: 2
Items: 
Size: 581134 Color: 1
Size: 418719 Color: 0

Bin 2725: 148 of cap free
Amount of items: 2
Items: 
Size: 602322 Color: 0
Size: 397531 Color: 1

Bin 2726: 148 of cap free
Amount of items: 2
Items: 
Size: 614246 Color: 1
Size: 385607 Color: 0

Bin 2727: 148 of cap free
Amount of items: 2
Items: 
Size: 641491 Color: 1
Size: 358362 Color: 0

Bin 2728: 148 of cap free
Amount of items: 2
Items: 
Size: 653223 Color: 1
Size: 346630 Color: 0

Bin 2729: 148 of cap free
Amount of items: 2
Items: 
Size: 715838 Color: 1
Size: 284015 Color: 0

Bin 2730: 149 of cap free
Amount of items: 2
Items: 
Size: 500570 Color: 0
Size: 499282 Color: 1

Bin 2731: 149 of cap free
Amount of items: 2
Items: 
Size: 507368 Color: 1
Size: 492484 Color: 0

Bin 2732: 149 of cap free
Amount of items: 2
Items: 
Size: 634830 Color: 1
Size: 365022 Color: 0

Bin 2733: 149 of cap free
Amount of items: 2
Items: 
Size: 644446 Color: 0
Size: 355406 Color: 1

Bin 2734: 149 of cap free
Amount of items: 2
Items: 
Size: 713173 Color: 1
Size: 286679 Color: 0

Bin 2735: 149 of cap free
Amount of items: 2
Items: 
Size: 761391 Color: 1
Size: 238461 Color: 0

Bin 2736: 150 of cap free
Amount of items: 2
Items: 
Size: 562355 Color: 1
Size: 437496 Color: 0

Bin 2737: 150 of cap free
Amount of items: 2
Items: 
Size: 604112 Color: 0
Size: 395739 Color: 1

Bin 2738: 150 of cap free
Amount of items: 2
Items: 
Size: 616767 Color: 0
Size: 383084 Color: 1

Bin 2739: 150 of cap free
Amount of items: 2
Items: 
Size: 630536 Color: 0
Size: 369315 Color: 1

Bin 2740: 150 of cap free
Amount of items: 2
Items: 
Size: 635092 Color: 1
Size: 364759 Color: 0

Bin 2741: 150 of cap free
Amount of items: 2
Items: 
Size: 639711 Color: 1
Size: 360140 Color: 0

Bin 2742: 150 of cap free
Amount of items: 2
Items: 
Size: 657358 Color: 1
Size: 342493 Color: 0

Bin 2743: 150 of cap free
Amount of items: 2
Items: 
Size: 771723 Color: 0
Size: 228128 Color: 1

Bin 2744: 150 of cap free
Amount of items: 2
Items: 
Size: 774250 Color: 1
Size: 225601 Color: 0

Bin 2745: 150 of cap free
Amount of items: 2
Items: 
Size: 782118 Color: 1
Size: 217733 Color: 0

Bin 2746: 151 of cap free
Amount of items: 2
Items: 
Size: 691977 Color: 0
Size: 307873 Color: 1

Bin 2747: 151 of cap free
Amount of items: 2
Items: 
Size: 716323 Color: 0
Size: 283527 Color: 1

Bin 2748: 151 of cap free
Amount of items: 2
Items: 
Size: 725271 Color: 1
Size: 274579 Color: 0

Bin 2749: 151 of cap free
Amount of items: 2
Items: 
Size: 793967 Color: 0
Size: 205883 Color: 1

Bin 2750: 151 of cap free
Amount of items: 2
Items: 
Size: 795638 Color: 1
Size: 204212 Color: 0

Bin 2751: 152 of cap free
Amount of items: 2
Items: 
Size: 580163 Color: 1
Size: 419686 Color: 0

Bin 2752: 152 of cap free
Amount of items: 2
Items: 
Size: 601071 Color: 0
Size: 398778 Color: 1

Bin 2753: 152 of cap free
Amount of items: 2
Items: 
Size: 611674 Color: 0
Size: 388175 Color: 1

Bin 2754: 152 of cap free
Amount of items: 2
Items: 
Size: 705337 Color: 1
Size: 294512 Color: 0

Bin 2755: 152 of cap free
Amount of items: 2
Items: 
Size: 764976 Color: 1
Size: 234873 Color: 0

Bin 2756: 153 of cap free
Amount of items: 2
Items: 
Size: 698663 Color: 1
Size: 301185 Color: 0

Bin 2757: 153 of cap free
Amount of items: 2
Items: 
Size: 779176 Color: 1
Size: 220672 Color: 0

Bin 2758: 154 of cap free
Amount of items: 2
Items: 
Size: 624149 Color: 0
Size: 375698 Color: 1

Bin 2759: 154 of cap free
Amount of items: 2
Items: 
Size: 633625 Color: 1
Size: 366222 Color: 0

Bin 2760: 154 of cap free
Amount of items: 2
Items: 
Size: 720568 Color: 0
Size: 279279 Color: 1

Bin 2761: 154 of cap free
Amount of items: 2
Items: 
Size: 734160 Color: 1
Size: 265687 Color: 0

Bin 2762: 154 of cap free
Amount of items: 3
Items: 
Size: 775249 Color: 1
Size: 112878 Color: 1
Size: 111720 Color: 0

Bin 2763: 155 of cap free
Amount of items: 2
Items: 
Size: 585832 Color: 0
Size: 414014 Color: 1

Bin 2764: 155 of cap free
Amount of items: 2
Items: 
Size: 665430 Color: 1
Size: 334416 Color: 0

Bin 2765: 155 of cap free
Amount of items: 2
Items: 
Size: 686429 Color: 0
Size: 313417 Color: 1

Bin 2766: 155 of cap free
Amount of items: 2
Items: 
Size: 713455 Color: 0
Size: 286391 Color: 1

Bin 2767: 155 of cap free
Amount of items: 2
Items: 
Size: 748589 Color: 0
Size: 251257 Color: 1

Bin 2768: 156 of cap free
Amount of items: 2
Items: 
Size: 585285 Color: 0
Size: 414560 Color: 1

Bin 2769: 156 of cap free
Amount of items: 2
Items: 
Size: 600181 Color: 0
Size: 399664 Color: 1

Bin 2770: 156 of cap free
Amount of items: 2
Items: 
Size: 628216 Color: 1
Size: 371629 Color: 0

Bin 2771: 156 of cap free
Amount of items: 2
Items: 
Size: 636640 Color: 0
Size: 363205 Color: 1

Bin 2772: 156 of cap free
Amount of items: 2
Items: 
Size: 665382 Color: 0
Size: 334463 Color: 1

Bin 2773: 156 of cap free
Amount of items: 2
Items: 
Size: 666907 Color: 0
Size: 332938 Color: 1

Bin 2774: 156 of cap free
Amount of items: 3
Items: 
Size: 787310 Color: 1
Size: 108948 Color: 0
Size: 103587 Color: 0

Bin 2775: 157 of cap free
Amount of items: 3
Items: 
Size: 378322 Color: 0
Size: 371883 Color: 0
Size: 249639 Color: 1

Bin 2776: 157 of cap free
Amount of items: 3
Items: 
Size: 396785 Color: 1
Size: 341965 Color: 1
Size: 261094 Color: 0

Bin 2777: 157 of cap free
Amount of items: 2
Items: 
Size: 502608 Color: 1
Size: 497236 Color: 0

Bin 2778: 157 of cap free
Amount of items: 2
Items: 
Size: 562677 Color: 1
Size: 437167 Color: 0

Bin 2779: 157 of cap free
Amount of items: 2
Items: 
Size: 632673 Color: 0
Size: 367171 Color: 1

Bin 2780: 157 of cap free
Amount of items: 2
Items: 
Size: 674494 Color: 1
Size: 325350 Color: 0

Bin 2781: 157 of cap free
Amount of items: 2
Items: 
Size: 714227 Color: 0
Size: 285617 Color: 1

Bin 2782: 158 of cap free
Amount of items: 2
Items: 
Size: 528725 Color: 0
Size: 471118 Color: 1

Bin 2783: 158 of cap free
Amount of items: 2
Items: 
Size: 699163 Color: 1
Size: 300680 Color: 0

Bin 2784: 158 of cap free
Amount of items: 2
Items: 
Size: 701077 Color: 0
Size: 298766 Color: 1

Bin 2785: 158 of cap free
Amount of items: 2
Items: 
Size: 713534 Color: 1
Size: 286309 Color: 0

Bin 2786: 158 of cap free
Amount of items: 2
Items: 
Size: 746238 Color: 0
Size: 253605 Color: 1

Bin 2787: 159 of cap free
Amount of items: 2
Items: 
Size: 512114 Color: 0
Size: 487728 Color: 1

Bin 2788: 159 of cap free
Amount of items: 2
Items: 
Size: 516147 Color: 1
Size: 483695 Color: 0

Bin 2789: 159 of cap free
Amount of items: 2
Items: 
Size: 519226 Color: 0
Size: 480616 Color: 1

Bin 2790: 159 of cap free
Amount of items: 2
Items: 
Size: 698661 Color: 1
Size: 301181 Color: 0

Bin 2791: 159 of cap free
Amount of items: 2
Items: 
Size: 728797 Color: 1
Size: 271045 Color: 0

Bin 2792: 160 of cap free
Amount of items: 2
Items: 
Size: 579069 Color: 0
Size: 420772 Color: 1

Bin 2793: 161 of cap free
Amount of items: 2
Items: 
Size: 627399 Color: 1
Size: 372441 Color: 0

Bin 2794: 162 of cap free
Amount of items: 2
Items: 
Size: 561008 Color: 0
Size: 438831 Color: 1

Bin 2795: 162 of cap free
Amount of items: 2
Items: 
Size: 610311 Color: 0
Size: 389528 Color: 1

Bin 2796: 162 of cap free
Amount of items: 2
Items: 
Size: 683746 Color: 0
Size: 316093 Color: 1

Bin 2797: 163 of cap free
Amount of items: 2
Items: 
Size: 543600 Color: 1
Size: 456238 Color: 0

Bin 2798: 163 of cap free
Amount of items: 2
Items: 
Size: 597375 Color: 1
Size: 402463 Color: 0

Bin 2799: 163 of cap free
Amount of items: 2
Items: 
Size: 613958 Color: 1
Size: 385880 Color: 0

Bin 2800: 163 of cap free
Amount of items: 2
Items: 
Size: 704161 Color: 1
Size: 295677 Color: 0

Bin 2801: 163 of cap free
Amount of items: 2
Items: 
Size: 723355 Color: 1
Size: 276483 Color: 0

Bin 2802: 163 of cap free
Amount of items: 2
Items: 
Size: 757367 Color: 0
Size: 242471 Color: 1

Bin 2803: 164 of cap free
Amount of items: 2
Items: 
Size: 569797 Color: 1
Size: 430040 Color: 0

Bin 2804: 164 of cap free
Amount of items: 2
Items: 
Size: 589663 Color: 0
Size: 410174 Color: 1

Bin 2805: 164 of cap free
Amount of items: 2
Items: 
Size: 657758 Color: 1
Size: 342079 Color: 0

Bin 2806: 164 of cap free
Amount of items: 2
Items: 
Size: 740080 Color: 0
Size: 259757 Color: 1

Bin 2807: 164 of cap free
Amount of items: 2
Items: 
Size: 763903 Color: 1
Size: 235934 Color: 0

Bin 2808: 164 of cap free
Amount of items: 2
Items: 
Size: 778049 Color: 1
Size: 221788 Color: 0

Bin 2809: 165 of cap free
Amount of items: 2
Items: 
Size: 518762 Color: 1
Size: 481074 Color: 0

Bin 2810: 165 of cap free
Amount of items: 2
Items: 
Size: 553412 Color: 1
Size: 446424 Color: 0

Bin 2811: 165 of cap free
Amount of items: 2
Items: 
Size: 617220 Color: 0
Size: 382616 Color: 1

Bin 2812: 165 of cap free
Amount of items: 2
Items: 
Size: 651165 Color: 1
Size: 348671 Color: 0

Bin 2813: 165 of cap free
Amount of items: 2
Items: 
Size: 654504 Color: 1
Size: 345332 Color: 0

Bin 2814: 165 of cap free
Amount of items: 2
Items: 
Size: 667332 Color: 1
Size: 332504 Color: 0

Bin 2815: 165 of cap free
Amount of items: 2
Items: 
Size: 709629 Color: 0
Size: 290207 Color: 1

Bin 2816: 166 of cap free
Amount of items: 2
Items: 
Size: 612419 Color: 0
Size: 387416 Color: 1

Bin 2817: 166 of cap free
Amount of items: 2
Items: 
Size: 647690 Color: 1
Size: 352145 Color: 0

Bin 2818: 166 of cap free
Amount of items: 2
Items: 
Size: 664638 Color: 0
Size: 335197 Color: 1

Bin 2819: 166 of cap free
Amount of items: 2
Items: 
Size: 679380 Color: 0
Size: 320455 Color: 1

Bin 2820: 166 of cap free
Amount of items: 2
Items: 
Size: 719541 Color: 0
Size: 280294 Color: 1

Bin 2821: 166 of cap free
Amount of items: 2
Items: 
Size: 731401 Color: 0
Size: 268434 Color: 1

Bin 2822: 166 of cap free
Amount of items: 2
Items: 
Size: 733570 Color: 0
Size: 266265 Color: 1

Bin 2823: 167 of cap free
Amount of items: 2
Items: 
Size: 500265 Color: 0
Size: 499569 Color: 1

Bin 2824: 167 of cap free
Amount of items: 2
Items: 
Size: 636849 Color: 1
Size: 362985 Color: 0

Bin 2825: 167 of cap free
Amount of items: 2
Items: 
Size: 651408 Color: 0
Size: 348426 Color: 1

Bin 2826: 167 of cap free
Amount of items: 2
Items: 
Size: 770679 Color: 0
Size: 229155 Color: 1

Bin 2827: 167 of cap free
Amount of items: 2
Items: 
Size: 799919 Color: 0
Size: 199915 Color: 1

Bin 2828: 168 of cap free
Amount of items: 2
Items: 
Size: 513815 Color: 0
Size: 486018 Color: 1

Bin 2829: 169 of cap free
Amount of items: 2
Items: 
Size: 518564 Color: 0
Size: 481268 Color: 1

Bin 2830: 169 of cap free
Amount of items: 2
Items: 
Size: 539634 Color: 1
Size: 460198 Color: 0

Bin 2831: 169 of cap free
Amount of items: 2
Items: 
Size: 543867 Color: 1
Size: 455965 Color: 0

Bin 2832: 169 of cap free
Amount of items: 2
Items: 
Size: 589341 Color: 1
Size: 410491 Color: 0

Bin 2833: 169 of cap free
Amount of items: 2
Items: 
Size: 617929 Color: 1
Size: 381903 Color: 0

Bin 2834: 169 of cap free
Amount of items: 2
Items: 
Size: 684130 Color: 0
Size: 315702 Color: 1

Bin 2835: 169 of cap free
Amount of items: 2
Items: 
Size: 735749 Color: 1
Size: 264083 Color: 0

Bin 2836: 170 of cap free
Amount of items: 2
Items: 
Size: 651804 Color: 0
Size: 348027 Color: 1

Bin 2837: 170 of cap free
Amount of items: 2
Items: 
Size: 768463 Color: 0
Size: 231368 Color: 1

Bin 2838: 171 of cap free
Amount of items: 2
Items: 
Size: 530805 Color: 1
Size: 469025 Color: 0

Bin 2839: 171 of cap free
Amount of items: 2
Items: 
Size: 619060 Color: 0
Size: 380770 Color: 1

Bin 2840: 171 of cap free
Amount of items: 2
Items: 
Size: 784287 Color: 1
Size: 215543 Color: 0

Bin 2841: 171 of cap free
Amount of items: 2
Items: 
Size: 795242 Color: 1
Size: 204588 Color: 0

Bin 2842: 172 of cap free
Amount of items: 2
Items: 
Size: 687193 Color: 0
Size: 312636 Color: 1

Bin 2843: 173 of cap free
Amount of items: 2
Items: 
Size: 540551 Color: 1
Size: 459277 Color: 0

Bin 2844: 173 of cap free
Amount of items: 2
Items: 
Size: 552028 Color: 0
Size: 447800 Color: 1

Bin 2845: 173 of cap free
Amount of items: 2
Items: 
Size: 553836 Color: 1
Size: 445992 Color: 0

Bin 2846: 173 of cap free
Amount of items: 2
Items: 
Size: 581080 Color: 0
Size: 418748 Color: 1

Bin 2847: 173 of cap free
Amount of items: 2
Items: 
Size: 646567 Color: 0
Size: 353261 Color: 1

Bin 2848: 173 of cap free
Amount of items: 2
Items: 
Size: 689748 Color: 0
Size: 310080 Color: 1

Bin 2849: 173 of cap free
Amount of items: 2
Items: 
Size: 712977 Color: 1
Size: 286851 Color: 0

Bin 2850: 173 of cap free
Amount of items: 3
Items: 
Size: 775478 Color: 1
Size: 113126 Color: 1
Size: 111224 Color: 0

Bin 2851: 174 of cap free
Amount of items: 2
Items: 
Size: 523021 Color: 1
Size: 476806 Color: 0

Bin 2852: 174 of cap free
Amount of items: 2
Items: 
Size: 604594 Color: 1
Size: 395233 Color: 0

Bin 2853: 174 of cap free
Amount of items: 2
Items: 
Size: 635483 Color: 0
Size: 364344 Color: 1

Bin 2854: 174 of cap free
Amount of items: 2
Items: 
Size: 684973 Color: 1
Size: 314854 Color: 0

Bin 2855: 174 of cap free
Amount of items: 2
Items: 
Size: 750850 Color: 0
Size: 248977 Color: 1

Bin 2856: 174 of cap free
Amount of items: 2
Items: 
Size: 755529 Color: 0
Size: 244298 Color: 1

Bin 2857: 174 of cap free
Amount of items: 2
Items: 
Size: 791722 Color: 0
Size: 208105 Color: 1

Bin 2858: 175 of cap free
Amount of items: 2
Items: 
Size: 554212 Color: 1
Size: 445614 Color: 0

Bin 2859: 175 of cap free
Amount of items: 2
Items: 
Size: 697544 Color: 1
Size: 302282 Color: 0

Bin 2860: 176 of cap free
Amount of items: 2
Items: 
Size: 557926 Color: 1
Size: 441899 Color: 0

Bin 2861: 176 of cap free
Amount of items: 2
Items: 
Size: 569374 Color: 1
Size: 430451 Color: 0

Bin 2862: 176 of cap free
Amount of items: 2
Items: 
Size: 606446 Color: 0
Size: 393379 Color: 1

Bin 2863: 176 of cap free
Amount of items: 2
Items: 
Size: 642233 Color: 1
Size: 357592 Color: 0

Bin 2864: 176 of cap free
Amount of items: 2
Items: 
Size: 659994 Color: 0
Size: 339831 Color: 1

Bin 2865: 177 of cap free
Amount of items: 2
Items: 
Size: 520617 Color: 1
Size: 479207 Color: 0

Bin 2866: 177 of cap free
Amount of items: 2
Items: 
Size: 539775 Color: 0
Size: 460049 Color: 1

Bin 2867: 177 of cap free
Amount of items: 2
Items: 
Size: 571523 Color: 0
Size: 428301 Color: 1

Bin 2868: 177 of cap free
Amount of items: 2
Items: 
Size: 593896 Color: 1
Size: 405928 Color: 0

Bin 2869: 177 of cap free
Amount of items: 2
Items: 
Size: 658891 Color: 1
Size: 340933 Color: 0

Bin 2870: 177 of cap free
Amount of items: 2
Items: 
Size: 767651 Color: 1
Size: 232173 Color: 0

Bin 2871: 178 of cap free
Amount of items: 2
Items: 
Size: 529083 Color: 1
Size: 470740 Color: 0

Bin 2872: 178 of cap free
Amount of items: 2
Items: 
Size: 757383 Color: 1
Size: 242440 Color: 0

Bin 2873: 179 of cap free
Amount of items: 7
Items: 
Size: 152630 Color: 0
Size: 151369 Color: 0
Size: 147028 Color: 1
Size: 146971 Color: 1
Size: 146243 Color: 1
Size: 128971 Color: 1
Size: 126610 Color: 0

Bin 2874: 179 of cap free
Amount of items: 2
Items: 
Size: 511022 Color: 1
Size: 488800 Color: 0

Bin 2875: 179 of cap free
Amount of items: 2
Items: 
Size: 559912 Color: 1
Size: 439910 Color: 0

Bin 2876: 179 of cap free
Amount of items: 2
Items: 
Size: 575942 Color: 0
Size: 423880 Color: 1

Bin 2877: 179 of cap free
Amount of items: 2
Items: 
Size: 582886 Color: 0
Size: 416936 Color: 1

Bin 2878: 179 of cap free
Amount of items: 2
Items: 
Size: 634783 Color: 0
Size: 365039 Color: 1

Bin 2879: 179 of cap free
Amount of items: 2
Items: 
Size: 748057 Color: 0
Size: 251765 Color: 1

Bin 2880: 179 of cap free
Amount of items: 2
Items: 
Size: 756925 Color: 1
Size: 242897 Color: 0

Bin 2881: 179 of cap free
Amount of items: 2
Items: 
Size: 769931 Color: 0
Size: 229891 Color: 1

Bin 2882: 179 of cap free
Amount of items: 2
Items: 
Size: 788784 Color: 1
Size: 211038 Color: 0

Bin 2883: 180 of cap free
Amount of items: 2
Items: 
Size: 501047 Color: 1
Size: 498774 Color: 0

Bin 2884: 180 of cap free
Amount of items: 2
Items: 
Size: 522215 Color: 1
Size: 477606 Color: 0

Bin 2885: 180 of cap free
Amount of items: 2
Items: 
Size: 568843 Color: 1
Size: 430978 Color: 0

Bin 2886: 180 of cap free
Amount of items: 2
Items: 
Size: 665106 Color: 1
Size: 334715 Color: 0

Bin 2887: 180 of cap free
Amount of items: 2
Items: 
Size: 730257 Color: 1
Size: 269564 Color: 0

Bin 2888: 181 of cap free
Amount of items: 2
Items: 
Size: 505491 Color: 1
Size: 494329 Color: 0

Bin 2889: 181 of cap free
Amount of items: 2
Items: 
Size: 538538 Color: 1
Size: 461282 Color: 0

Bin 2890: 181 of cap free
Amount of items: 2
Items: 
Size: 568508 Color: 0
Size: 431312 Color: 1

Bin 2891: 181 of cap free
Amount of items: 2
Items: 
Size: 572080 Color: 1
Size: 427740 Color: 0

Bin 2892: 181 of cap free
Amount of items: 2
Items: 
Size: 586603 Color: 0
Size: 413217 Color: 1

Bin 2893: 181 of cap free
Amount of items: 2
Items: 
Size: 719642 Color: 1
Size: 280178 Color: 0

Bin 2894: 182 of cap free
Amount of items: 2
Items: 
Size: 528662 Color: 1
Size: 471157 Color: 0

Bin 2895: 182 of cap free
Amount of items: 2
Items: 
Size: 584099 Color: 1
Size: 415720 Color: 0

Bin 2896: 182 of cap free
Amount of items: 2
Items: 
Size: 621673 Color: 0
Size: 378146 Color: 1

Bin 2897: 182 of cap free
Amount of items: 2
Items: 
Size: 678028 Color: 1
Size: 321791 Color: 0

Bin 2898: 183 of cap free
Amount of items: 2
Items: 
Size: 519047 Color: 1
Size: 480771 Color: 0

Bin 2899: 183 of cap free
Amount of items: 2
Items: 
Size: 561578 Color: 1
Size: 438240 Color: 0

Bin 2900: 183 of cap free
Amount of items: 2
Items: 
Size: 566856 Color: 1
Size: 432962 Color: 0

Bin 2901: 183 of cap free
Amount of items: 2
Items: 
Size: 572899 Color: 1
Size: 426919 Color: 0

Bin 2902: 183 of cap free
Amount of items: 2
Items: 
Size: 652067 Color: 0
Size: 347751 Color: 1

Bin 2903: 183 of cap free
Amount of items: 2
Items: 
Size: 664982 Color: 0
Size: 334836 Color: 1

Bin 2904: 183 of cap free
Amount of items: 2
Items: 
Size: 669589 Color: 0
Size: 330229 Color: 1

Bin 2905: 183 of cap free
Amount of items: 2
Items: 
Size: 713520 Color: 1
Size: 286298 Color: 0

Bin 2906: 184 of cap free
Amount of items: 2
Items: 
Size: 619860 Color: 1
Size: 379957 Color: 0

Bin 2907: 184 of cap free
Amount of items: 3
Items: 
Size: 778471 Color: 0
Size: 111323 Color: 1
Size: 110023 Color: 0

Bin 2908: 185 of cap free
Amount of items: 2
Items: 
Size: 506441 Color: 0
Size: 493375 Color: 1

Bin 2909: 185 of cap free
Amount of items: 2
Items: 
Size: 555989 Color: 0
Size: 443827 Color: 1

Bin 2910: 185 of cap free
Amount of items: 2
Items: 
Size: 562653 Color: 0
Size: 437163 Color: 1

Bin 2911: 185 of cap free
Amount of items: 2
Items: 
Size: 666903 Color: 0
Size: 332913 Color: 1

Bin 2912: 186 of cap free
Amount of items: 2
Items: 
Size: 505058 Color: 0
Size: 494757 Color: 1

Bin 2913: 186 of cap free
Amount of items: 2
Items: 
Size: 534828 Color: 0
Size: 464987 Color: 1

Bin 2914: 186 of cap free
Amount of items: 2
Items: 
Size: 550834 Color: 0
Size: 448981 Color: 1

Bin 2915: 186 of cap free
Amount of items: 2
Items: 
Size: 770509 Color: 1
Size: 229306 Color: 0

Bin 2916: 186 of cap free
Amount of items: 2
Items: 
Size: 789250 Color: 0
Size: 210565 Color: 1

Bin 2917: 187 of cap free
Amount of items: 2
Items: 
Size: 573430 Color: 0
Size: 426384 Color: 1

Bin 2918: 187 of cap free
Amount of items: 2
Items: 
Size: 578202 Color: 0
Size: 421612 Color: 1

Bin 2919: 187 of cap free
Amount of items: 2
Items: 
Size: 706155 Color: 1
Size: 293659 Color: 0

Bin 2920: 187 of cap free
Amount of items: 3
Items: 
Size: 776868 Color: 0
Size: 112147 Color: 1
Size: 110799 Color: 0

Bin 2921: 187 of cap free
Amount of items: 2
Items: 
Size: 789131 Color: 1
Size: 210683 Color: 0

Bin 2922: 188 of cap free
Amount of items: 2
Items: 
Size: 500362 Color: 1
Size: 499451 Color: 0

Bin 2923: 188 of cap free
Amount of items: 2
Items: 
Size: 516455 Color: 1
Size: 483358 Color: 0

Bin 2924: 188 of cap free
Amount of items: 2
Items: 
Size: 540720 Color: 0
Size: 459093 Color: 1

Bin 2925: 188 of cap free
Amount of items: 2
Items: 
Size: 541282 Color: 0
Size: 458531 Color: 1

Bin 2926: 188 of cap free
Amount of items: 2
Items: 
Size: 634121 Color: 1
Size: 365692 Color: 0

Bin 2927: 188 of cap free
Amount of items: 2
Items: 
Size: 679607 Color: 0
Size: 320206 Color: 1

Bin 2928: 188 of cap free
Amount of items: 2
Items: 
Size: 695150 Color: 0
Size: 304663 Color: 1

Bin 2929: 188 of cap free
Amount of items: 2
Items: 
Size: 744530 Color: 0
Size: 255283 Color: 1

Bin 2930: 188 of cap free
Amount of items: 2
Items: 
Size: 764684 Color: 0
Size: 235129 Color: 1

Bin 2931: 189 of cap free
Amount of items: 2
Items: 
Size: 508199 Color: 0
Size: 491613 Color: 1

Bin 2932: 189 of cap free
Amount of items: 2
Items: 
Size: 655473 Color: 0
Size: 344339 Color: 1

Bin 2933: 189 of cap free
Amount of items: 2
Items: 
Size: 703480 Color: 0
Size: 296332 Color: 1

Bin 2934: 189 of cap free
Amount of items: 2
Items: 
Size: 722892 Color: 1
Size: 276920 Color: 0

Bin 2935: 189 of cap free
Amount of items: 2
Items: 
Size: 725271 Color: 0
Size: 274541 Color: 1

Bin 2936: 189 of cap free
Amount of items: 2
Items: 
Size: 782607 Color: 0
Size: 217205 Color: 1

Bin 2937: 190 of cap free
Amount of items: 2
Items: 
Size: 544996 Color: 0
Size: 454815 Color: 1

Bin 2938: 190 of cap free
Amount of items: 2
Items: 
Size: 616144 Color: 0
Size: 383667 Color: 1

Bin 2939: 191 of cap free
Amount of items: 6
Items: 
Size: 168641 Color: 0
Size: 168494 Color: 0
Size: 167880 Color: 1
Size: 167868 Color: 1
Size: 167809 Color: 1
Size: 159118 Color: 0

Bin 2940: 191 of cap free
Amount of items: 2
Items: 
Size: 556956 Color: 1
Size: 442854 Color: 0

Bin 2941: 191 of cap free
Amount of items: 2
Items: 
Size: 581501 Color: 1
Size: 418309 Color: 0

Bin 2942: 191 of cap free
Amount of items: 2
Items: 
Size: 709028 Color: 0
Size: 290782 Color: 1

Bin 2943: 191 of cap free
Amount of items: 2
Items: 
Size: 714987 Color: 0
Size: 284823 Color: 1

Bin 2944: 192 of cap free
Amount of items: 2
Items: 
Size: 577509 Color: 0
Size: 422300 Color: 1

Bin 2945: 192 of cap free
Amount of items: 2
Items: 
Size: 589644 Color: 0
Size: 410165 Color: 1

Bin 2946: 192 of cap free
Amount of items: 2
Items: 
Size: 613785 Color: 0
Size: 386024 Color: 1

Bin 2947: 192 of cap free
Amount of items: 2
Items: 
Size: 774707 Color: 0
Size: 225102 Color: 1

Bin 2948: 193 of cap free
Amount of items: 2
Items: 
Size: 607610 Color: 0
Size: 392198 Color: 1

Bin 2949: 193 of cap free
Amount of items: 2
Items: 
Size: 632342 Color: 1
Size: 367466 Color: 0

Bin 2950: 194 of cap free
Amount of items: 2
Items: 
Size: 592308 Color: 0
Size: 407499 Color: 1

Bin 2951: 194 of cap free
Amount of items: 2
Items: 
Size: 618736 Color: 0
Size: 381071 Color: 1

Bin 2952: 194 of cap free
Amount of items: 2
Items: 
Size: 756060 Color: 1
Size: 243747 Color: 0

Bin 2953: 195 of cap free
Amount of items: 2
Items: 
Size: 520320 Color: 1
Size: 479486 Color: 0

Bin 2954: 195 of cap free
Amount of items: 2
Items: 
Size: 563740 Color: 0
Size: 436066 Color: 1

Bin 2955: 195 of cap free
Amount of items: 2
Items: 
Size: 687189 Color: 0
Size: 312617 Color: 1

Bin 2956: 195 of cap free
Amount of items: 2
Items: 
Size: 710274 Color: 0
Size: 289532 Color: 1

Bin 2957: 195 of cap free
Amount of items: 2
Items: 
Size: 712423 Color: 0
Size: 287383 Color: 1

Bin 2958: 195 of cap free
Amount of items: 2
Items: 
Size: 768096 Color: 1
Size: 231710 Color: 0

Bin 2959: 195 of cap free
Amount of items: 2
Items: 
Size: 785468 Color: 0
Size: 214338 Color: 1

Bin 2960: 196 of cap free
Amount of items: 2
Items: 
Size: 560670 Color: 0
Size: 439135 Color: 1

Bin 2961: 196 of cap free
Amount of items: 2
Items: 
Size: 584773 Color: 1
Size: 415032 Color: 0

Bin 2962: 196 of cap free
Amount of items: 2
Items: 
Size: 589329 Color: 1
Size: 410476 Color: 0

Bin 2963: 196 of cap free
Amount of items: 2
Items: 
Size: 589859 Color: 1
Size: 409946 Color: 0

Bin 2964: 196 of cap free
Amount of items: 2
Items: 
Size: 596010 Color: 0
Size: 403795 Color: 1

Bin 2965: 196 of cap free
Amount of items: 2
Items: 
Size: 619994 Color: 0
Size: 379811 Color: 1

Bin 2966: 196 of cap free
Amount of items: 2
Items: 
Size: 680251 Color: 1
Size: 319554 Color: 0

Bin 2967: 196 of cap free
Amount of items: 2
Items: 
Size: 701199 Color: 1
Size: 298606 Color: 0

Bin 2968: 196 of cap free
Amount of items: 2
Items: 
Size: 722832 Color: 0
Size: 276973 Color: 1

Bin 2969: 196 of cap free
Amount of items: 2
Items: 
Size: 756625 Color: 1
Size: 243180 Color: 0

Bin 2970: 196 of cap free
Amount of items: 2
Items: 
Size: 765212 Color: 0
Size: 234593 Color: 1

Bin 2971: 196 of cap free
Amount of items: 2
Items: 
Size: 788526 Color: 0
Size: 211279 Color: 1

Bin 2972: 197 of cap free
Amount of items: 2
Items: 
Size: 661892 Color: 0
Size: 337912 Color: 1

Bin 2973: 197 of cap free
Amount of items: 2
Items: 
Size: 681269 Color: 1
Size: 318535 Color: 0

Bin 2974: 197 of cap free
Amount of items: 2
Items: 
Size: 785868 Color: 0
Size: 213936 Color: 1

Bin 2975: 198 of cap free
Amount of items: 2
Items: 
Size: 646050 Color: 0
Size: 353753 Color: 1

Bin 2976: 198 of cap free
Amount of items: 2
Items: 
Size: 651397 Color: 0
Size: 348406 Color: 1

Bin 2977: 199 of cap free
Amount of items: 2
Items: 
Size: 539625 Color: 1
Size: 460177 Color: 0

Bin 2978: 199 of cap free
Amount of items: 2
Items: 
Size: 603296 Color: 1
Size: 396506 Color: 0

Bin 2979: 199 of cap free
Amount of items: 2
Items: 
Size: 604203 Color: 1
Size: 395599 Color: 0

Bin 2980: 199 of cap free
Amount of items: 2
Items: 
Size: 639389 Color: 0
Size: 360413 Color: 1

Bin 2981: 199 of cap free
Amount of items: 2
Items: 
Size: 752252 Color: 0
Size: 247550 Color: 1

Bin 2982: 200 of cap free
Amount of items: 2
Items: 
Size: 654513 Color: 0
Size: 345288 Color: 1

Bin 2983: 200 of cap free
Amount of items: 2
Items: 
Size: 718680 Color: 1
Size: 281121 Color: 0

Bin 2984: 200 of cap free
Amount of items: 2
Items: 
Size: 730454 Color: 0
Size: 269347 Color: 1

Bin 2985: 200 of cap free
Amount of items: 2
Items: 
Size: 764659 Color: 1
Size: 235142 Color: 0

Bin 2986: 200 of cap free
Amount of items: 2
Items: 
Size: 798279 Color: 1
Size: 201522 Color: 0

Bin 2987: 201 of cap free
Amount of items: 2
Items: 
Size: 512262 Color: 1
Size: 487538 Color: 0

Bin 2988: 201 of cap free
Amount of items: 2
Items: 
Size: 547643 Color: 0
Size: 452157 Color: 1

Bin 2989: 201 of cap free
Amount of items: 2
Items: 
Size: 586594 Color: 0
Size: 413206 Color: 1

Bin 2990: 201 of cap free
Amount of items: 2
Items: 
Size: 673491 Color: 0
Size: 326309 Color: 1

Bin 2991: 201 of cap free
Amount of items: 2
Items: 
Size: 675175 Color: 1
Size: 324625 Color: 0

Bin 2992: 201 of cap free
Amount of items: 2
Items: 
Size: 699015 Color: 0
Size: 300785 Color: 1

Bin 2993: 202 of cap free
Amount of items: 2
Items: 
Size: 550203 Color: 1
Size: 449596 Color: 0

Bin 2994: 202 of cap free
Amount of items: 2
Items: 
Size: 620963 Color: 1
Size: 378836 Color: 0

Bin 2995: 202 of cap free
Amount of items: 2
Items: 
Size: 729965 Color: 0
Size: 269834 Color: 1

Bin 2996: 202 of cap free
Amount of items: 2
Items: 
Size: 788111 Color: 0
Size: 211688 Color: 1

Bin 2997: 203 of cap free
Amount of items: 2
Items: 
Size: 537175 Color: 1
Size: 462623 Color: 0

Bin 2998: 203 of cap free
Amount of items: 2
Items: 
Size: 575880 Color: 1
Size: 423918 Color: 0

Bin 2999: 203 of cap free
Amount of items: 2
Items: 
Size: 743326 Color: 0
Size: 256472 Color: 1

Bin 3000: 204 of cap free
Amount of items: 2
Items: 
Size: 508814 Color: 1
Size: 490983 Color: 0

Bin 3001: 204 of cap free
Amount of items: 2
Items: 
Size: 553687 Color: 0
Size: 446110 Color: 1

Bin 3002: 204 of cap free
Amount of items: 2
Items: 
Size: 590332 Color: 0
Size: 409465 Color: 1

Bin 3003: 204 of cap free
Amount of items: 2
Items: 
Size: 637711 Color: 0
Size: 362086 Color: 1

Bin 3004: 204 of cap free
Amount of items: 2
Items: 
Size: 710267 Color: 0
Size: 289530 Color: 1

Bin 3005: 205 of cap free
Amount of items: 2
Items: 
Size: 522657 Color: 1
Size: 477139 Color: 0

Bin 3006: 205 of cap free
Amount of items: 2
Items: 
Size: 575560 Color: 1
Size: 424236 Color: 0

Bin 3007: 205 of cap free
Amount of items: 2
Items: 
Size: 606254 Color: 1
Size: 393542 Color: 0

Bin 3008: 205 of cap free
Amount of items: 2
Items: 
Size: 646560 Color: 1
Size: 353236 Color: 0

Bin 3009: 205 of cap free
Amount of items: 2
Items: 
Size: 650164 Color: 0
Size: 349632 Color: 1

Bin 3010: 206 of cap free
Amount of items: 2
Items: 
Size: 519514 Color: 0
Size: 480281 Color: 1

Bin 3011: 206 of cap free
Amount of items: 2
Items: 
Size: 525680 Color: 0
Size: 474115 Color: 1

Bin 3012: 206 of cap free
Amount of items: 2
Items: 
Size: 687930 Color: 1
Size: 311865 Color: 0

Bin 3013: 206 of cap free
Amount of items: 2
Items: 
Size: 717506 Color: 0
Size: 282289 Color: 1

Bin 3014: 206 of cap free
Amount of items: 2
Items: 
Size: 751058 Color: 1
Size: 248737 Color: 0

Bin 3015: 206 of cap free
Amount of items: 2
Items: 
Size: 774390 Color: 0
Size: 225405 Color: 1

Bin 3016: 206 of cap free
Amount of items: 2
Items: 
Size: 786801 Color: 1
Size: 212994 Color: 0

Bin 3017: 207 of cap free
Amount of items: 2
Items: 
Size: 588064 Color: 1
Size: 411730 Color: 0

Bin 3018: 207 of cap free
Amount of items: 2
Items: 
Size: 654910 Color: 0
Size: 344884 Color: 1

Bin 3019: 208 of cap free
Amount of items: 2
Items: 
Size: 546561 Color: 1
Size: 453232 Color: 0

Bin 3020: 208 of cap free
Amount of items: 2
Items: 
Size: 556404 Color: 1
Size: 443389 Color: 0

Bin 3021: 208 of cap free
Amount of items: 2
Items: 
Size: 595259 Color: 1
Size: 404534 Color: 0

Bin 3022: 208 of cap free
Amount of items: 2
Items: 
Size: 667675 Color: 1
Size: 332118 Color: 0

Bin 3023: 209 of cap free
Amount of items: 2
Items: 
Size: 597851 Color: 0
Size: 401941 Color: 1

Bin 3024: 209 of cap free
Amount of items: 2
Items: 
Size: 701690 Color: 0
Size: 298102 Color: 1

Bin 3025: 209 of cap free
Amount of items: 2
Items: 
Size: 702330 Color: 0
Size: 297462 Color: 1

Bin 3026: 209 of cap free
Amount of items: 2
Items: 
Size: 720978 Color: 0
Size: 278814 Color: 1

Bin 3027: 209 of cap free
Amount of items: 2
Items: 
Size: 740248 Color: 0
Size: 259544 Color: 1

Bin 3028: 209 of cap free
Amount of items: 2
Items: 
Size: 756427 Color: 0
Size: 243365 Color: 1

Bin 3029: 210 of cap free
Amount of items: 2
Items: 
Size: 612400 Color: 0
Size: 387391 Color: 1

Bin 3030: 210 of cap free
Amount of items: 2
Items: 
Size: 717194 Color: 0
Size: 282597 Color: 1

Bin 3031: 211 of cap free
Amount of items: 2
Items: 
Size: 581933 Color: 1
Size: 417857 Color: 0

Bin 3032: 211 of cap free
Amount of items: 2
Items: 
Size: 690272 Color: 1
Size: 309518 Color: 0

Bin 3033: 211 of cap free
Amount of items: 2
Items: 
Size: 775622 Color: 0
Size: 224168 Color: 1

Bin 3034: 212 of cap free
Amount of items: 2
Items: 
Size: 533705 Color: 0
Size: 466084 Color: 1

Bin 3035: 212 of cap free
Amount of items: 2
Items: 
Size: 605464 Color: 1
Size: 394325 Color: 0

Bin 3036: 212 of cap free
Amount of items: 2
Items: 
Size: 657740 Color: 0
Size: 342049 Color: 1

Bin 3037: 212 of cap free
Amount of items: 2
Items: 
Size: 671636 Color: 1
Size: 328153 Color: 0

Bin 3038: 212 of cap free
Amount of items: 2
Items: 
Size: 681998 Color: 0
Size: 317791 Color: 1

Bin 3039: 213 of cap free
Amount of items: 2
Items: 
Size: 543162 Color: 0
Size: 456626 Color: 1

Bin 3040: 213 of cap free
Amount of items: 2
Items: 
Size: 780577 Color: 1
Size: 219211 Color: 0

Bin 3041: 214 of cap free
Amount of items: 2
Items: 
Size: 501290 Color: 1
Size: 498497 Color: 0

Bin 3042: 214 of cap free
Amount of items: 2
Items: 
Size: 557497 Color: 0
Size: 442290 Color: 1

Bin 3043: 216 of cap free
Amount of items: 2
Items: 
Size: 538283 Color: 0
Size: 461502 Color: 1

Bin 3044: 216 of cap free
Amount of items: 2
Items: 
Size: 568025 Color: 0
Size: 431760 Color: 1

Bin 3045: 216 of cap free
Amount of items: 2
Items: 
Size: 598983 Color: 0
Size: 400802 Color: 1

Bin 3046: 216 of cap free
Amount of items: 2
Items: 
Size: 628367 Color: 0
Size: 371418 Color: 1

Bin 3047: 216 of cap free
Amount of items: 2
Items: 
Size: 641825 Color: 1
Size: 357960 Color: 0

Bin 3048: 216 of cap free
Amount of items: 2
Items: 
Size: 645659 Color: 0
Size: 354126 Color: 1

Bin 3049: 216 of cap free
Amount of items: 2
Items: 
Size: 649784 Color: 1
Size: 350001 Color: 0

Bin 3050: 217 of cap free
Amount of items: 2
Items: 
Size: 582405 Color: 0
Size: 417379 Color: 1

Bin 3051: 217 of cap free
Amount of items: 2
Items: 
Size: 601471 Color: 1
Size: 398313 Color: 0

Bin 3052: 217 of cap free
Amount of items: 2
Items: 
Size: 695605 Color: 1
Size: 304179 Color: 0

Bin 3053: 217 of cap free
Amount of items: 3
Items: 
Size: 780925 Color: 0
Size: 110951 Color: 1
Size: 107908 Color: 0

Bin 3054: 218 of cap free
Amount of items: 2
Items: 
Size: 588664 Color: 1
Size: 411119 Color: 0

Bin 3055: 218 of cap free
Amount of items: 2
Items: 
Size: 674919 Color: 0
Size: 324864 Color: 1

Bin 3056: 219 of cap free
Amount of items: 2
Items: 
Size: 544395 Color: 0
Size: 455387 Color: 1

Bin 3057: 219 of cap free
Amount of items: 2
Items: 
Size: 590736 Color: 1
Size: 409046 Color: 0

Bin 3058: 219 of cap free
Amount of items: 2
Items: 
Size: 596228 Color: 0
Size: 403554 Color: 1

Bin 3059: 219 of cap free
Amount of items: 2
Items: 
Size: 637874 Color: 1
Size: 361908 Color: 0

Bin 3060: 220 of cap free
Amount of items: 2
Items: 
Size: 727748 Color: 0
Size: 272033 Color: 1

Bin 3061: 221 of cap free
Amount of items: 2
Items: 
Size: 628175 Color: 1
Size: 371605 Color: 0

Bin 3062: 221 of cap free
Amount of items: 2
Items: 
Size: 672842 Color: 1
Size: 326938 Color: 0

Bin 3063: 221 of cap free
Amount of items: 2
Items: 
Size: 696498 Color: 1
Size: 303282 Color: 0

Bin 3064: 221 of cap free
Amount of items: 2
Items: 
Size: 733634 Color: 1
Size: 266146 Color: 0

Bin 3065: 221 of cap free
Amount of items: 2
Items: 
Size: 754642 Color: 1
Size: 245138 Color: 0

Bin 3066: 222 of cap free
Amount of items: 2
Items: 
Size: 501589 Color: 0
Size: 498190 Color: 1

Bin 3067: 222 of cap free
Amount of items: 2
Items: 
Size: 553595 Color: 1
Size: 446184 Color: 0

Bin 3068: 222 of cap free
Amount of items: 2
Items: 
Size: 622108 Color: 0
Size: 377671 Color: 1

Bin 3069: 222 of cap free
Amount of items: 2
Items: 
Size: 638652 Color: 0
Size: 361127 Color: 1

Bin 3070: 222 of cap free
Amount of items: 2
Items: 
Size: 662893 Color: 0
Size: 336886 Color: 1

Bin 3071: 222 of cap free
Amount of items: 2
Items: 
Size: 735084 Color: 1
Size: 264695 Color: 0

Bin 3072: 223 of cap free
Amount of items: 2
Items: 
Size: 501281 Color: 1
Size: 498497 Color: 0

Bin 3073: 223 of cap free
Amount of items: 2
Items: 
Size: 509967 Color: 1
Size: 489811 Color: 0

Bin 3074: 223 of cap free
Amount of items: 2
Items: 
Size: 582152 Color: 1
Size: 417626 Color: 0

Bin 3075: 223 of cap free
Amount of items: 2
Items: 
Size: 640732 Color: 1
Size: 359046 Color: 0

Bin 3076: 223 of cap free
Amount of items: 2
Items: 
Size: 667490 Color: 0
Size: 332288 Color: 1

Bin 3077: 223 of cap free
Amount of items: 2
Items: 
Size: 727497 Color: 1
Size: 272281 Color: 0

Bin 3078: 223 of cap free
Amount of items: 2
Items: 
Size: 781758 Color: 1
Size: 218020 Color: 0

Bin 3079: 224 of cap free
Amount of items: 2
Items: 
Size: 798895 Color: 0
Size: 200882 Color: 1

Bin 3080: 225 of cap free
Amount of items: 2
Items: 
Size: 581593 Color: 0
Size: 418183 Color: 1

Bin 3081: 225 of cap free
Amount of items: 2
Items: 
Size: 590107 Color: 1
Size: 409669 Color: 0

Bin 3082: 225 of cap free
Amount of items: 2
Items: 
Size: 610803 Color: 1
Size: 388973 Color: 0

Bin 3083: 225 of cap free
Amount of items: 2
Items: 
Size: 643259 Color: 0
Size: 356517 Color: 1

Bin 3084: 225 of cap free
Amount of items: 2
Items: 
Size: 736871 Color: 0
Size: 262905 Color: 1

Bin 3085: 226 of cap free
Amount of items: 2
Items: 
Size: 512483 Color: 0
Size: 487292 Color: 1

Bin 3086: 226 of cap free
Amount of items: 2
Items: 
Size: 587364 Color: 0
Size: 412411 Color: 1

Bin 3087: 226 of cap free
Amount of items: 2
Items: 
Size: 694921 Color: 0
Size: 304854 Color: 1

Bin 3088: 226 of cap free
Amount of items: 2
Items: 
Size: 703625 Color: 1
Size: 296150 Color: 0

Bin 3089: 226 of cap free
Amount of items: 2
Items: 
Size: 725516 Color: 1
Size: 274259 Color: 0

Bin 3090: 227 of cap free
Amount of items: 2
Items: 
Size: 570012 Color: 0
Size: 429762 Color: 1

Bin 3091: 227 of cap free
Amount of items: 2
Items: 
Size: 622634 Color: 1
Size: 377140 Color: 0

Bin 3092: 228 of cap free
Amount of items: 2
Items: 
Size: 564928 Color: 1
Size: 434845 Color: 0

Bin 3093: 228 of cap free
Amount of items: 2
Items: 
Size: 614731 Color: 0
Size: 385042 Color: 1

Bin 3094: 228 of cap free
Amount of items: 2
Items: 
Size: 641813 Color: 0
Size: 357960 Color: 1

Bin 3095: 228 of cap free
Amount of items: 2
Items: 
Size: 664134 Color: 0
Size: 335639 Color: 1

Bin 3096: 228 of cap free
Amount of items: 2
Items: 
Size: 672101 Color: 0
Size: 327672 Color: 1

Bin 3097: 228 of cap free
Amount of items: 2
Items: 
Size: 721496 Color: 1
Size: 278277 Color: 0

Bin 3098: 228 of cap free
Amount of items: 2
Items: 
Size: 765855 Color: 0
Size: 233918 Color: 1

Bin 3099: 229 of cap free
Amount of items: 2
Items: 
Size: 648080 Color: 1
Size: 351692 Color: 0

Bin 3100: 229 of cap free
Amount of items: 2
Items: 
Size: 755477 Color: 1
Size: 244295 Color: 0

Bin 3101: 229 of cap free
Amount of items: 2
Items: 
Size: 796210 Color: 0
Size: 203562 Color: 1

Bin 3102: 230 of cap free
Amount of items: 2
Items: 
Size: 517744 Color: 0
Size: 482027 Color: 1

Bin 3103: 230 of cap free
Amount of items: 2
Items: 
Size: 536878 Color: 1
Size: 462893 Color: 0

Bin 3104: 230 of cap free
Amount of items: 2
Items: 
Size: 767325 Color: 1
Size: 232446 Color: 0

Bin 3105: 230 of cap free
Amount of items: 2
Items: 
Size: 783177 Color: 0
Size: 216594 Color: 1

Bin 3106: 231 of cap free
Amount of items: 2
Items: 
Size: 562960 Color: 1
Size: 436810 Color: 0

Bin 3107: 231 of cap free
Amount of items: 2
Items: 
Size: 575380 Color: 0
Size: 424390 Color: 1

Bin 3108: 231 of cap free
Amount of items: 2
Items: 
Size: 594128 Color: 1
Size: 405642 Color: 0

Bin 3109: 232 of cap free
Amount of items: 2
Items: 
Size: 688110 Color: 0
Size: 311659 Color: 1

Bin 3110: 232 of cap free
Amount of items: 2
Items: 
Size: 743717 Color: 0
Size: 256052 Color: 1

Bin 3111: 232 of cap free
Amount of items: 2
Items: 
Size: 768564 Color: 1
Size: 231205 Color: 0

Bin 3112: 233 of cap free
Amount of items: 2
Items: 
Size: 506356 Color: 1
Size: 493412 Color: 0

Bin 3113: 233 of cap free
Amount of items: 2
Items: 
Size: 514890 Color: 1
Size: 484878 Color: 0

Bin 3114: 233 of cap free
Amount of items: 2
Items: 
Size: 528675 Color: 0
Size: 471093 Color: 1

Bin 3115: 233 of cap free
Amount of items: 2
Items: 
Size: 550325 Color: 0
Size: 449443 Color: 1

Bin 3116: 233 of cap free
Amount of items: 2
Items: 
Size: 607114 Color: 0
Size: 392654 Color: 1

Bin 3117: 233 of cap free
Amount of items: 2
Items: 
Size: 723647 Color: 0
Size: 276121 Color: 1

Bin 3118: 233 of cap free
Amount of items: 2
Items: 
Size: 797642 Color: 1
Size: 202126 Color: 0

Bin 3119: 234 of cap free
Amount of items: 2
Items: 
Size: 704112 Color: 1
Size: 295655 Color: 0

Bin 3120: 235 of cap free
Amount of items: 2
Items: 
Size: 510317 Color: 0
Size: 489449 Color: 1

Bin 3121: 235 of cap free
Amount of items: 2
Items: 
Size: 623457 Color: 0
Size: 376309 Color: 1

Bin 3122: 235 of cap free
Amount of items: 2
Items: 
Size: 704666 Color: 0
Size: 295100 Color: 1

Bin 3123: 236 of cap free
Amount of items: 2
Items: 
Size: 594208 Color: 0
Size: 405557 Color: 1

Bin 3124: 236 of cap free
Amount of items: 2
Items: 
Size: 598514 Color: 1
Size: 401251 Color: 0

Bin 3125: 236 of cap free
Amount of items: 2
Items: 
Size: 743436 Color: 1
Size: 256329 Color: 0

Bin 3126: 238 of cap free
Amount of items: 2
Items: 
Size: 504722 Color: 1
Size: 495041 Color: 0

Bin 3127: 238 of cap free
Amount of items: 2
Items: 
Size: 605195 Color: 0
Size: 394568 Color: 1

Bin 3128: 238 of cap free
Amount of items: 2
Items: 
Size: 693834 Color: 1
Size: 305929 Color: 0

Bin 3129: 239 of cap free
Amount of items: 2
Items: 
Size: 794413 Color: 1
Size: 205349 Color: 0

Bin 3130: 240 of cap free
Amount of items: 2
Items: 
Size: 526706 Color: 0
Size: 473055 Color: 1

Bin 3131: 240 of cap free
Amount of items: 2
Items: 
Size: 528415 Color: 1
Size: 471346 Color: 0

Bin 3132: 240 of cap free
Amount of items: 2
Items: 
Size: 628162 Color: 1
Size: 371599 Color: 0

Bin 3133: 241 of cap free
Amount of items: 2
Items: 
Size: 620911 Color: 0
Size: 378849 Color: 1

Bin 3134: 241 of cap free
Amount of items: 2
Items: 
Size: 654247 Color: 0
Size: 345513 Color: 1

Bin 3135: 241 of cap free
Amount of items: 2
Items: 
Size: 680180 Color: 0
Size: 319580 Color: 1

Bin 3136: 241 of cap free
Amount of items: 2
Items: 
Size: 713703 Color: 0
Size: 286057 Color: 1

Bin 3137: 241 of cap free
Amount of items: 2
Items: 
Size: 787091 Color: 0
Size: 212669 Color: 1

Bin 3138: 242 of cap free
Amount of items: 2
Items: 
Size: 603242 Color: 0
Size: 396517 Color: 1

Bin 3139: 243 of cap free
Amount of items: 2
Items: 
Size: 531948 Color: 1
Size: 467810 Color: 0

Bin 3140: 243 of cap free
Amount of items: 2
Items: 
Size: 553587 Color: 1
Size: 446171 Color: 0

Bin 3141: 243 of cap free
Amount of items: 2
Items: 
Size: 660555 Color: 1
Size: 339203 Color: 0

Bin 3142: 243 of cap free
Amount of items: 2
Items: 
Size: 747591 Color: 1
Size: 252167 Color: 0

Bin 3143: 244 of cap free
Amount of items: 2
Items: 
Size: 758552 Color: 0
Size: 241205 Color: 1

Bin 3144: 244 of cap free
Amount of items: 2
Items: 
Size: 770301 Color: 0
Size: 229456 Color: 1

Bin 3145: 245 of cap free
Amount of items: 2
Items: 
Size: 554829 Color: 1
Size: 444927 Color: 0

Bin 3146: 245 of cap free
Amount of items: 2
Items: 
Size: 640059 Color: 1
Size: 359697 Color: 0

Bin 3147: 245 of cap free
Amount of items: 2
Items: 
Size: 670843 Color: 0
Size: 328913 Color: 1

Bin 3148: 245 of cap free
Amount of items: 2
Items: 
Size: 712386 Color: 0
Size: 287370 Color: 1

Bin 3149: 245 of cap free
Amount of items: 2
Items: 
Size: 734897 Color: 0
Size: 264859 Color: 1

Bin 3150: 246 of cap free
Amount of items: 2
Items: 
Size: 604199 Color: 1
Size: 395556 Color: 0

Bin 3151: 246 of cap free
Amount of items: 2
Items: 
Size: 718157 Color: 1
Size: 281598 Color: 0

Bin 3152: 246 of cap free
Amount of items: 2
Items: 
Size: 749509 Color: 1
Size: 250246 Color: 0

Bin 3153: 246 of cap free
Amount of items: 2
Items: 
Size: 775937 Color: 0
Size: 223818 Color: 1

Bin 3154: 247 of cap free
Amount of items: 2
Items: 
Size: 526372 Color: 0
Size: 473382 Color: 1

Bin 3155: 248 of cap free
Amount of items: 2
Items: 
Size: 518555 Color: 0
Size: 481198 Color: 1

Bin 3156: 248 of cap free
Amount of items: 2
Items: 
Size: 797799 Color: 0
Size: 201954 Color: 1

Bin 3157: 249 of cap free
Amount of items: 2
Items: 
Size: 622324 Color: 1
Size: 377428 Color: 0

Bin 3158: 249 of cap free
Amount of items: 2
Items: 
Size: 644038 Color: 1
Size: 355714 Color: 0

Bin 3159: 250 of cap free
Amount of items: 2
Items: 
Size: 749556 Color: 0
Size: 250195 Color: 1

Bin 3160: 251 of cap free
Amount of items: 2
Items: 
Size: 517385 Color: 1
Size: 482365 Color: 0

Bin 3161: 251 of cap free
Amount of items: 2
Items: 
Size: 603666 Color: 0
Size: 396084 Color: 1

Bin 3162: 252 of cap free
Amount of items: 2
Items: 
Size: 708201 Color: 1
Size: 291548 Color: 0

Bin 3163: 253 of cap free
Amount of items: 2
Items: 
Size: 559451 Color: 0
Size: 440297 Color: 1

Bin 3164: 253 of cap free
Amount of items: 2
Items: 
Size: 612115 Color: 1
Size: 387633 Color: 0

Bin 3165: 253 of cap free
Amount of items: 2
Items: 
Size: 629994 Color: 1
Size: 369754 Color: 0

Bin 3166: 253 of cap free
Amount of items: 2
Items: 
Size: 703048 Color: 1
Size: 296700 Color: 0

Bin 3167: 253 of cap free
Amount of items: 2
Items: 
Size: 751528 Color: 0
Size: 248220 Color: 1

Bin 3168: 254 of cap free
Amount of items: 2
Items: 
Size: 566242 Color: 1
Size: 433505 Color: 0

Bin 3169: 255 of cap free
Amount of items: 3
Items: 
Size: 385062 Color: 0
Size: 350817 Color: 1
Size: 263867 Color: 1

Bin 3170: 255 of cap free
Amount of items: 2
Items: 
Size: 620258 Color: 1
Size: 379488 Color: 0

Bin 3171: 255 of cap free
Amount of items: 2
Items: 
Size: 696381 Color: 0
Size: 303365 Color: 1

Bin 3172: 256 of cap free
Amount of items: 2
Items: 
Size: 501096 Color: 0
Size: 498649 Color: 1

Bin 3173: 256 of cap free
Amount of items: 2
Items: 
Size: 546510 Color: 0
Size: 453235 Color: 1

Bin 3174: 256 of cap free
Amount of items: 2
Items: 
Size: 639634 Color: 1
Size: 360111 Color: 0

Bin 3175: 258 of cap free
Amount of items: 2
Items: 
Size: 590840 Color: 0
Size: 408903 Color: 1

Bin 3176: 258 of cap free
Amount of items: 2
Items: 
Size: 602092 Color: 1
Size: 397651 Color: 0

Bin 3177: 259 of cap free
Amount of items: 2
Items: 
Size: 540807 Color: 1
Size: 458935 Color: 0

Bin 3178: 259 of cap free
Amount of items: 2
Items: 
Size: 764000 Color: 0
Size: 235742 Color: 1

Bin 3179: 260 of cap free
Amount of items: 2
Items: 
Size: 517839 Color: 1
Size: 481902 Color: 0

Bin 3180: 260 of cap free
Amount of items: 2
Items: 
Size: 621839 Color: 1
Size: 377902 Color: 0

Bin 3181: 260 of cap free
Amount of items: 2
Items: 
Size: 631031 Color: 0
Size: 368710 Color: 1

Bin 3182: 260 of cap free
Amount of items: 2
Items: 
Size: 741147 Color: 1
Size: 258594 Color: 0

Bin 3183: 261 of cap free
Amount of items: 2
Items: 
Size: 530988 Color: 1
Size: 468752 Color: 0

Bin 3184: 261 of cap free
Amount of items: 2
Items: 
Size: 624107 Color: 1
Size: 375633 Color: 0

Bin 3185: 262 of cap free
Amount of items: 2
Items: 
Size: 515686 Color: 1
Size: 484053 Color: 0

Bin 3186: 262 of cap free
Amount of items: 2
Items: 
Size: 619984 Color: 0
Size: 379755 Color: 1

Bin 3187: 263 of cap free
Amount of items: 2
Items: 
Size: 715310 Color: 0
Size: 284428 Color: 1

Bin 3188: 263 of cap free
Amount of items: 2
Items: 
Size: 736711 Color: 1
Size: 263027 Color: 0

Bin 3189: 264 of cap free
Amount of items: 2
Items: 
Size: 538261 Color: 0
Size: 461476 Color: 1

Bin 3190: 264 of cap free
Amount of items: 2
Items: 
Size: 789824 Color: 1
Size: 209913 Color: 0

Bin 3191: 265 of cap free
Amount of items: 2
Items: 
Size: 505746 Color: 0
Size: 493990 Color: 1

Bin 3192: 266 of cap free
Amount of items: 2
Items: 
Size: 533929 Color: 0
Size: 465806 Color: 1

Bin 3193: 266 of cap free
Amount of items: 2
Items: 
Size: 676855 Color: 1
Size: 322880 Color: 0

Bin 3194: 266 of cap free
Amount of items: 2
Items: 
Size: 737473 Color: 1
Size: 262262 Color: 0

Bin 3195: 267 of cap free
Amount of items: 2
Items: 
Size: 728498 Color: 1
Size: 271236 Color: 0

Bin 3196: 267 of cap free
Amount of items: 2
Items: 
Size: 757273 Color: 0
Size: 242461 Color: 1

Bin 3197: 269 of cap free
Amount of items: 2
Items: 
Size: 527660 Color: 0
Size: 472072 Color: 1

Bin 3198: 269 of cap free
Amount of items: 2
Items: 
Size: 691584 Color: 0
Size: 308148 Color: 1

Bin 3199: 269 of cap free
Amount of items: 2
Items: 
Size: 777008 Color: 1
Size: 222724 Color: 0

Bin 3200: 270 of cap free
Amount of items: 2
Items: 
Size: 689392 Color: 0
Size: 310339 Color: 1

Bin 3201: 271 of cap free
Amount of items: 2
Items: 
Size: 513506 Color: 0
Size: 486224 Color: 1

Bin 3202: 271 of cap free
Amount of items: 2
Items: 
Size: 561896 Color: 1
Size: 437834 Color: 0

Bin 3203: 271 of cap free
Amount of items: 2
Items: 
Size: 604175 Color: 1
Size: 395555 Color: 0

Bin 3204: 272 of cap free
Amount of items: 2
Items: 
Size: 577294 Color: 1
Size: 422435 Color: 0

Bin 3205: 272 of cap free
Amount of items: 2
Items: 
Size: 791650 Color: 0
Size: 208079 Color: 1

Bin 3206: 273 of cap free
Amount of items: 2
Items: 
Size: 702623 Color: 1
Size: 297105 Color: 0

Bin 3207: 274 of cap free
Amount of items: 2
Items: 
Size: 636276 Color: 0
Size: 363451 Color: 1

Bin 3208: 275 of cap free
Amount of items: 2
Items: 
Size: 547649 Color: 1
Size: 452077 Color: 0

Bin 3209: 275 of cap free
Amount of items: 2
Items: 
Size: 717147 Color: 0
Size: 282579 Color: 1

Bin 3210: 275 of cap free
Amount of items: 2
Items: 
Size: 792243 Color: 0
Size: 207483 Color: 1

Bin 3211: 277 of cap free
Amount of items: 2
Items: 
Size: 522897 Color: 0
Size: 476827 Color: 1

Bin 3212: 277 of cap free
Amount of items: 2
Items: 
Size: 532480 Color: 1
Size: 467244 Color: 0

Bin 3213: 277 of cap free
Amount of items: 2
Items: 
Size: 549837 Color: 0
Size: 449887 Color: 1

Bin 3214: 277 of cap free
Amount of items: 2
Items: 
Size: 604170 Color: 1
Size: 395554 Color: 0

Bin 3215: 277 of cap free
Amount of items: 2
Items: 
Size: 724940 Color: 0
Size: 274784 Color: 1

Bin 3216: 278 of cap free
Amount of items: 2
Items: 
Size: 573472 Color: 1
Size: 426251 Color: 0

Bin 3217: 278 of cap free
Amount of items: 2
Items: 
Size: 575363 Color: 0
Size: 424360 Color: 1

Bin 3218: 278 of cap free
Amount of items: 2
Items: 
Size: 699010 Color: 0
Size: 300713 Color: 1

Bin 3219: 278 of cap free
Amount of items: 2
Items: 
Size: 724458 Color: 0
Size: 275265 Color: 1

Bin 3220: 278 of cap free
Amount of items: 2
Items: 
Size: 725183 Color: 1
Size: 274540 Color: 0

Bin 3221: 281 of cap free
Amount of items: 2
Items: 
Size: 702280 Color: 1
Size: 297440 Color: 0

Bin 3222: 282 of cap free
Amount of items: 2
Items: 
Size: 552041 Color: 1
Size: 447678 Color: 0

Bin 3223: 283 of cap free
Amount of items: 2
Items: 
Size: 586919 Color: 1
Size: 412799 Color: 0

Bin 3224: 283 of cap free
Amount of items: 2
Items: 
Size: 606813 Color: 0
Size: 392905 Color: 1

Bin 3225: 283 of cap free
Amount of items: 2
Items: 
Size: 614322 Color: 0
Size: 385396 Color: 1

Bin 3226: 283 of cap free
Amount of items: 2
Items: 
Size: 714182 Color: 0
Size: 285536 Color: 1

Bin 3227: 283 of cap free
Amount of items: 2
Items: 
Size: 737153 Color: 1
Size: 262565 Color: 0

Bin 3228: 283 of cap free
Amount of items: 2
Items: 
Size: 740384 Color: 1
Size: 259334 Color: 0

Bin 3229: 284 of cap free
Amount of items: 2
Items: 
Size: 569222 Color: 0
Size: 430495 Color: 1

Bin 3230: 284 of cap free
Amount of items: 2
Items: 
Size: 619394 Color: 0
Size: 380323 Color: 1

Bin 3231: 285 of cap free
Amount of items: 2
Items: 
Size: 655274 Color: 1
Size: 344442 Color: 0

Bin 3232: 285 of cap free
Amount of items: 2
Items: 
Size: 763153 Color: 1
Size: 236563 Color: 0

Bin 3233: 285 of cap free
Amount of items: 2
Items: 
Size: 767974 Color: 0
Size: 231742 Color: 1

Bin 3234: 286 of cap free
Amount of items: 2
Items: 
Size: 507351 Color: 1
Size: 492364 Color: 0

Bin 3235: 286 of cap free
Amount of items: 2
Items: 
Size: 772839 Color: 1
Size: 226876 Color: 0

Bin 3236: 287 of cap free
Amount of items: 2
Items: 
Size: 610253 Color: 0
Size: 389461 Color: 1

Bin 3237: 287 of cap free
Amount of items: 2
Items: 
Size: 661784 Color: 1
Size: 337930 Color: 0

Bin 3238: 287 of cap free
Amount of items: 2
Items: 
Size: 672728 Color: 0
Size: 326986 Color: 1

Bin 3239: 288 of cap free
Amount of items: 2
Items: 
Size: 571164 Color: 1
Size: 428549 Color: 0

Bin 3240: 288 of cap free
Amount of items: 2
Items: 
Size: 586466 Color: 1
Size: 413247 Color: 0

Bin 3241: 288 of cap free
Amount of items: 2
Items: 
Size: 653493 Color: 0
Size: 346220 Color: 1

Bin 3242: 288 of cap free
Amount of items: 2
Items: 
Size: 698258 Color: 1
Size: 301455 Color: 0

Bin 3243: 289 of cap free
Amount of items: 2
Items: 
Size: 561325 Color: 0
Size: 438387 Color: 1

Bin 3244: 289 of cap free
Amount of items: 2
Items: 
Size: 562588 Color: 1
Size: 437124 Color: 0

Bin 3245: 289 of cap free
Amount of items: 2
Items: 
Size: 658365 Color: 1
Size: 341347 Color: 0

Bin 3246: 289 of cap free
Amount of items: 2
Items: 
Size: 722478 Color: 1
Size: 277234 Color: 0

Bin 3247: 290 of cap free
Amount of items: 2
Items: 
Size: 705405 Color: 0
Size: 294306 Color: 1

Bin 3248: 290 of cap free
Amount of items: 3
Items: 
Size: 792332 Color: 1
Size: 105931 Color: 0
Size: 101448 Color: 0

Bin 3249: 291 of cap free
Amount of items: 2
Items: 
Size: 588974 Color: 0
Size: 410736 Color: 1

Bin 3250: 291 of cap free
Amount of items: 2
Items: 
Size: 678943 Color: 1
Size: 320767 Color: 0

Bin 3251: 291 of cap free
Amount of items: 2
Items: 
Size: 686592 Color: 1
Size: 313118 Color: 0

Bin 3252: 292 of cap free
Amount of items: 2
Items: 
Size: 618702 Color: 1
Size: 381007 Color: 0

Bin 3253: 292 of cap free
Amount of items: 2
Items: 
Size: 700470 Color: 0
Size: 299239 Color: 1

Bin 3254: 292 of cap free
Amount of items: 2
Items: 
Size: 772824 Color: 0
Size: 226885 Color: 1

Bin 3255: 293 of cap free
Amount of items: 2
Items: 
Size: 599562 Color: 1
Size: 400146 Color: 0

Bin 3256: 293 of cap free
Amount of items: 2
Items: 
Size: 728063 Color: 1
Size: 271645 Color: 0

Bin 3257: 294 of cap free
Amount of items: 2
Items: 
Size: 570617 Color: 0
Size: 429090 Color: 1

Bin 3258: 295 of cap free
Amount of items: 2
Items: 
Size: 524114 Color: 0
Size: 475592 Color: 1

Bin 3259: 295 of cap free
Amount of items: 2
Items: 
Size: 566263 Color: 0
Size: 433443 Color: 1

Bin 3260: 295 of cap free
Amount of items: 2
Items: 
Size: 633255 Color: 1
Size: 366451 Color: 0

Bin 3261: 295 of cap free
Amount of items: 2
Items: 
Size: 731739 Color: 0
Size: 267967 Color: 1

Bin 3262: 296 of cap free
Amount of items: 2
Items: 
Size: 535580 Color: 1
Size: 464125 Color: 0

Bin 3263: 296 of cap free
Amount of items: 2
Items: 
Size: 549088 Color: 1
Size: 450617 Color: 0

Bin 3264: 296 of cap free
Amount of items: 2
Items: 
Size: 746809 Color: 0
Size: 252896 Color: 1

Bin 3265: 297 of cap free
Amount of items: 2
Items: 
Size: 503679 Color: 0
Size: 496025 Color: 1

Bin 3266: 297 of cap free
Amount of items: 2
Items: 
Size: 636857 Color: 0
Size: 362847 Color: 1

Bin 3267: 297 of cap free
Amount of items: 2
Items: 
Size: 656258 Color: 1
Size: 343446 Color: 0

Bin 3268: 297 of cap free
Amount of items: 2
Items: 
Size: 761145 Color: 0
Size: 238559 Color: 1

Bin 3269: 297 of cap free
Amount of items: 2
Items: 
Size: 777029 Color: 0
Size: 222675 Color: 1

Bin 3270: 298 of cap free
Amount of items: 2
Items: 
Size: 556633 Color: 0
Size: 443070 Color: 1

Bin 3271: 298 of cap free
Amount of items: 2
Items: 
Size: 577896 Color: 0
Size: 421807 Color: 1

Bin 3272: 298 of cap free
Amount of items: 2
Items: 
Size: 667652 Color: 1
Size: 332051 Color: 0

Bin 3273: 298 of cap free
Amount of items: 2
Items: 
Size: 711304 Color: 1
Size: 288399 Color: 0

Bin 3274: 298 of cap free
Amount of items: 2
Items: 
Size: 729362 Color: 0
Size: 270341 Color: 1

Bin 3275: 298 of cap free
Amount of items: 2
Items: 
Size: 762107 Color: 0
Size: 237596 Color: 1

Bin 3276: 299 of cap free
Amount of items: 2
Items: 
Size: 619388 Color: 0
Size: 380314 Color: 1

Bin 3277: 299 of cap free
Amount of items: 2
Items: 
Size: 665324 Color: 1
Size: 334378 Color: 0

Bin 3278: 300 of cap free
Amount of items: 2
Items: 
Size: 557468 Color: 0
Size: 442233 Color: 1

Bin 3279: 300 of cap free
Amount of items: 2
Items: 
Size: 727478 Color: 1
Size: 272223 Color: 0

Bin 3280: 300 of cap free
Amount of items: 2
Items: 
Size: 774334 Color: 0
Size: 225367 Color: 1

Bin 3281: 301 of cap free
Amount of items: 2
Items: 
Size: 772123 Color: 1
Size: 227577 Color: 0

Bin 3282: 302 of cap free
Amount of items: 2
Items: 
Size: 682894 Color: 1
Size: 316805 Color: 0

Bin 3283: 303 of cap free
Amount of items: 2
Items: 
Size: 734090 Color: 1
Size: 265608 Color: 0

Bin 3284: 304 of cap free
Amount of items: 2
Items: 
Size: 543465 Color: 1
Size: 456232 Color: 0

Bin 3285: 304 of cap free
Amount of items: 2
Items: 
Size: 753611 Color: 1
Size: 246086 Color: 0

Bin 3286: 304 of cap free
Amount of items: 2
Items: 
Size: 765105 Color: 0
Size: 234592 Color: 1

Bin 3287: 305 of cap free
Amount of items: 2
Items: 
Size: 690447 Color: 0
Size: 309249 Color: 1

Bin 3288: 306 of cap free
Amount of items: 2
Items: 
Size: 650116 Color: 0
Size: 349579 Color: 1

Bin 3289: 306 of cap free
Amount of items: 2
Items: 
Size: 794520 Color: 0
Size: 205175 Color: 1

Bin 3290: 307 of cap free
Amount of items: 2
Items: 
Size: 676129 Color: 1
Size: 323565 Color: 0

Bin 3291: 308 of cap free
Amount of items: 2
Items: 
Size: 629562 Color: 1
Size: 370131 Color: 0

Bin 3292: 309 of cap free
Amount of items: 2
Items: 
Size: 574132 Color: 0
Size: 425560 Color: 1

Bin 3293: 310 of cap free
Amount of items: 2
Items: 
Size: 712774 Color: 0
Size: 286917 Color: 1

Bin 3294: 311 of cap free
Amount of items: 2
Items: 
Size: 619323 Color: 1
Size: 380367 Color: 0

Bin 3295: 311 of cap free
Amount of items: 2
Items: 
Size: 664113 Color: 0
Size: 335577 Color: 1

Bin 3296: 311 of cap free
Amount of items: 2
Items: 
Size: 775563 Color: 0
Size: 224127 Color: 1

Bin 3297: 312 of cap free
Amount of items: 2
Items: 
Size: 516611 Color: 0
Size: 483078 Color: 1

Bin 3298: 312 of cap free
Amount of items: 2
Items: 
Size: 795153 Color: 1
Size: 204536 Color: 0

Bin 3299: 313 of cap free
Amount of items: 2
Items: 
Size: 568271 Color: 1
Size: 431417 Color: 0

Bin 3300: 314 of cap free
Amount of items: 2
Items: 
Size: 781859 Color: 0
Size: 217828 Color: 1

Bin 3301: 315 of cap free
Amount of items: 2
Items: 
Size: 666556 Color: 0
Size: 333130 Color: 1

Bin 3302: 315 of cap free
Amount of items: 2
Items: 
Size: 781689 Color: 1
Size: 217997 Color: 0

Bin 3303: 316 of cap free
Amount of items: 2
Items: 
Size: 720899 Color: 0
Size: 278786 Color: 1

Bin 3304: 316 of cap free
Amount of items: 2
Items: 
Size: 736805 Color: 0
Size: 262880 Color: 1

Bin 3305: 317 of cap free
Amount of items: 2
Items: 
Size: 563777 Color: 1
Size: 435907 Color: 0

Bin 3306: 318 of cap free
Amount of items: 2
Items: 
Size: 592782 Color: 0
Size: 406901 Color: 1

Bin 3307: 318 of cap free
Amount of items: 2
Items: 
Size: 779356 Color: 0
Size: 220327 Color: 1

Bin 3308: 319 of cap free
Amount of items: 2
Items: 
Size: 739300 Color: 1
Size: 260382 Color: 0

Bin 3309: 320 of cap free
Amount of items: 2
Items: 
Size: 557596 Color: 1
Size: 442085 Color: 0

Bin 3310: 321 of cap free
Amount of items: 2
Items: 
Size: 515385 Color: 0
Size: 484295 Color: 1

Bin 3311: 321 of cap free
Amount of items: 2
Items: 
Size: 717063 Color: 1
Size: 282617 Color: 0

Bin 3312: 322 of cap free
Amount of items: 2
Items: 
Size: 510714 Color: 0
Size: 488965 Color: 1

Bin 3313: 322 of cap free
Amount of items: 2
Items: 
Size: 621784 Color: 1
Size: 377895 Color: 0

Bin 3314: 322 of cap free
Amount of items: 2
Items: 
Size: 638426 Color: 1
Size: 361253 Color: 0

Bin 3315: 322 of cap free
Amount of items: 2
Items: 
Size: 701623 Color: 1
Size: 298056 Color: 0

Bin 3316: 323 of cap free
Amount of items: 2
Items: 
Size: 673088 Color: 0
Size: 326590 Color: 1

Bin 3317: 324 of cap free
Amount of items: 2
Items: 
Size: 511362 Color: 0
Size: 488315 Color: 1

Bin 3318: 324 of cap free
Amount of items: 2
Items: 
Size: 677159 Color: 1
Size: 322518 Color: 0

Bin 3319: 324 of cap free
Amount of items: 2
Items: 
Size: 724624 Color: 1
Size: 275053 Color: 0

Bin 3320: 325 of cap free
Amount of items: 2
Items: 
Size: 642626 Color: 1
Size: 357050 Color: 0

Bin 3321: 325 of cap free
Amount of items: 2
Items: 
Size: 784024 Color: 0
Size: 215652 Color: 1

Bin 3322: 326 of cap free
Amount of items: 2
Items: 
Size: 605165 Color: 0
Size: 394510 Color: 1

Bin 3323: 326 of cap free
Amount of items: 2
Items: 
Size: 631346 Color: 1
Size: 368329 Color: 0

Bin 3324: 326 of cap free
Amount of items: 2
Items: 
Size: 785119 Color: 0
Size: 214556 Color: 1

Bin 3325: 327 of cap free
Amount of items: 2
Items: 
Size: 550304 Color: 0
Size: 449370 Color: 1

Bin 3326: 327 of cap free
Amount of items: 2
Items: 
Size: 742378 Color: 0
Size: 257296 Color: 1

Bin 3327: 328 of cap free
Amount of items: 2
Items: 
Size: 750235 Color: 1
Size: 249438 Color: 0

Bin 3328: 329 of cap free
Amount of items: 2
Items: 
Size: 509578 Color: 0
Size: 490094 Color: 1

Bin 3329: 329 of cap free
Amount of items: 2
Items: 
Size: 593444 Color: 0
Size: 406228 Color: 1

Bin 3330: 329 of cap free
Amount of items: 2
Items: 
Size: 655839 Color: 0
Size: 343833 Color: 1

Bin 3331: 331 of cap free
Amount of items: 2
Items: 
Size: 531385 Color: 0
Size: 468285 Color: 1

Bin 3332: 331 of cap free
Amount of items: 2
Items: 
Size: 658302 Color: 0
Size: 341368 Color: 1

Bin 3333: 331 of cap free
Amount of items: 2
Items: 
Size: 799180 Color: 0
Size: 200490 Color: 1

Bin 3334: 332 of cap free
Amount of items: 2
Items: 
Size: 556291 Color: 0
Size: 443378 Color: 1

Bin 3335: 333 of cap free
Amount of items: 2
Items: 
Size: 576524 Color: 1
Size: 423144 Color: 0

Bin 3336: 333 of cap free
Amount of items: 2
Items: 
Size: 598443 Color: 1
Size: 401225 Color: 0

Bin 3337: 333 of cap free
Amount of items: 2
Items: 
Size: 795570 Color: 1
Size: 204098 Color: 0

Bin 3338: 334 of cap free
Amount of items: 2
Items: 
Size: 576650 Color: 0
Size: 423017 Color: 1

Bin 3339: 334 of cap free
Amount of items: 2
Items: 
Size: 581051 Color: 1
Size: 418616 Color: 0

Bin 3340: 334 of cap free
Amount of items: 2
Items: 
Size: 780541 Color: 1
Size: 219126 Color: 0

Bin 3341: 335 of cap free
Amount of items: 2
Items: 
Size: 554780 Color: 1
Size: 444886 Color: 0

Bin 3342: 335 of cap free
Amount of items: 2
Items: 
Size: 616224 Color: 1
Size: 383442 Color: 0

Bin 3343: 335 of cap free
Amount of items: 2
Items: 
Size: 622588 Color: 1
Size: 377078 Color: 0

Bin 3344: 335 of cap free
Amount of items: 2
Items: 
Size: 771528 Color: 1
Size: 228138 Color: 0

Bin 3345: 336 of cap free
Amount of items: 2
Items: 
Size: 607435 Color: 1
Size: 392230 Color: 0

Bin 3346: 336 of cap free
Amount of items: 2
Items: 
Size: 714595 Color: 1
Size: 285070 Color: 0

Bin 3347: 336 of cap free
Amount of items: 2
Items: 
Size: 796832 Color: 0
Size: 202833 Color: 1

Bin 3348: 337 of cap free
Amount of items: 2
Items: 
Size: 761115 Color: 0
Size: 238549 Color: 1

Bin 3349: 338 of cap free
Amount of items: 2
Items: 
Size: 616753 Color: 0
Size: 382910 Color: 1

Bin 3350: 338 of cap free
Amount of items: 2
Items: 
Size: 632294 Color: 1
Size: 367369 Color: 0

Bin 3351: 338 of cap free
Amount of items: 2
Items: 
Size: 681649 Color: 1
Size: 318014 Color: 0

Bin 3352: 338 of cap free
Amount of items: 2
Items: 
Size: 710855 Color: 0
Size: 288808 Color: 1

Bin 3353: 339 of cap free
Amount of items: 2
Items: 
Size: 674549 Color: 0
Size: 325113 Color: 1

Bin 3354: 340 of cap free
Amount of items: 2
Items: 
Size: 618044 Color: 0
Size: 381617 Color: 1

Bin 3355: 340 of cap free
Amount of items: 2
Items: 
Size: 688348 Color: 0
Size: 311313 Color: 1

Bin 3356: 342 of cap free
Amount of items: 2
Items: 
Size: 550159 Color: 1
Size: 449500 Color: 0

Bin 3357: 342 of cap free
Amount of items: 2
Items: 
Size: 618946 Color: 0
Size: 380713 Color: 1

Bin 3358: 342 of cap free
Amount of items: 2
Items: 
Size: 716636 Color: 1
Size: 283023 Color: 0

Bin 3359: 343 of cap free
Amount of items: 2
Items: 
Size: 598364 Color: 0
Size: 401294 Color: 1

Bin 3360: 343 of cap free
Amount of items: 2
Items: 
Size: 793035 Color: 1
Size: 206623 Color: 0

Bin 3361: 345 of cap free
Amount of items: 2
Items: 
Size: 529687 Color: 0
Size: 469969 Color: 1

Bin 3362: 346 of cap free
Amount of items: 2
Items: 
Size: 682887 Color: 1
Size: 316768 Color: 0

Bin 3363: 347 of cap free
Amount of items: 2
Items: 
Size: 594913 Color: 0
Size: 404741 Color: 1

Bin 3364: 348 of cap free
Amount of items: 2
Items: 
Size: 624136 Color: 0
Size: 375517 Color: 1

Bin 3365: 348 of cap free
Amount of items: 2
Items: 
Size: 639612 Color: 1
Size: 360041 Color: 0

Bin 3366: 348 of cap free
Amount of items: 2
Items: 
Size: 692818 Color: 0
Size: 306835 Color: 1

Bin 3367: 349 of cap free
Amount of items: 2
Items: 
Size: 560603 Color: 0
Size: 439049 Color: 1

Bin 3368: 351 of cap free
Amount of items: 7
Items: 
Size: 155318 Color: 0
Size: 154270 Color: 0
Size: 154134 Color: 0
Size: 149960 Color: 1
Size: 149514 Color: 1
Size: 120566 Color: 1
Size: 115888 Color: 0

Bin 3369: 351 of cap free
Amount of items: 2
Items: 
Size: 532443 Color: 1
Size: 467207 Color: 0

Bin 3370: 353 of cap free
Amount of items: 2
Items: 
Size: 695992 Color: 0
Size: 303656 Color: 1

Bin 3371: 353 of cap free
Amount of items: 2
Items: 
Size: 798552 Color: 1
Size: 201096 Color: 0

Bin 3372: 355 of cap free
Amount of items: 2
Items: 
Size: 503637 Color: 0
Size: 496009 Color: 1

Bin 3373: 355 of cap free
Amount of items: 2
Items: 
Size: 539839 Color: 1
Size: 459807 Color: 0

Bin 3374: 355 of cap free
Amount of items: 2
Items: 
Size: 759335 Color: 1
Size: 240311 Color: 0

Bin 3375: 356 of cap free
Amount of items: 2
Items: 
Size: 522103 Color: 1
Size: 477542 Color: 0

Bin 3376: 356 of cap free
Amount of items: 2
Items: 
Size: 639018 Color: 0
Size: 360627 Color: 1

Bin 3377: 356 of cap free
Amount of items: 2
Items: 
Size: 737439 Color: 0
Size: 262206 Color: 1

Bin 3378: 356 of cap free
Amount of items: 2
Items: 
Size: 784013 Color: 0
Size: 215632 Color: 1

Bin 3379: 356 of cap free
Amount of items: 2
Items: 
Size: 791056 Color: 0
Size: 208589 Color: 1

Bin 3380: 357 of cap free
Amount of items: 2
Items: 
Size: 566207 Color: 1
Size: 433437 Color: 0

Bin 3381: 357 of cap free
Amount of items: 2
Items: 
Size: 590806 Color: 0
Size: 408838 Color: 1

Bin 3382: 357 of cap free
Amount of items: 2
Items: 
Size: 733068 Color: 0
Size: 266576 Color: 1

Bin 3383: 357 of cap free
Amount of items: 2
Items: 
Size: 757266 Color: 0
Size: 242378 Color: 1

Bin 3384: 358 of cap free
Amount of items: 2
Items: 
Size: 588630 Color: 1
Size: 411013 Color: 0

Bin 3385: 358 of cap free
Amount of items: 2
Items: 
Size: 667369 Color: 0
Size: 332274 Color: 1

Bin 3386: 358 of cap free
Amount of items: 2
Items: 
Size: 719102 Color: 0
Size: 280541 Color: 1

Bin 3387: 358 of cap free
Amount of items: 2
Items: 
Size: 755926 Color: 0
Size: 243717 Color: 1

Bin 3388: 359 of cap free
Amount of items: 2
Items: 
Size: 527534 Color: 1
Size: 472108 Color: 0

Bin 3389: 360 of cap free
Amount of items: 2
Items: 
Size: 504372 Color: 0
Size: 495269 Color: 1

Bin 3390: 360 of cap free
Amount of items: 2
Items: 
Size: 647582 Color: 1
Size: 352059 Color: 0

Bin 3391: 360 of cap free
Amount of items: 2
Items: 
Size: 762114 Color: 1
Size: 237527 Color: 0

Bin 3392: 361 of cap free
Amount of items: 2
Items: 
Size: 534741 Color: 1
Size: 464899 Color: 0

Bin 3393: 361 of cap free
Amount of items: 2
Items: 
Size: 785652 Color: 1
Size: 213988 Color: 0

Bin 3394: 363 of cap free
Amount of items: 2
Items: 
Size: 599438 Color: 0
Size: 400200 Color: 1

Bin 3395: 363 of cap free
Amount of items: 2
Items: 
Size: 655214 Color: 1
Size: 344424 Color: 0

Bin 3396: 365 of cap free
Amount of items: 2
Items: 
Size: 680127 Color: 0
Size: 319509 Color: 1

Bin 3397: 366 of cap free
Amount of items: 2
Items: 
Size: 554485 Color: 0
Size: 445150 Color: 1

Bin 3398: 367 of cap free
Amount of items: 2
Items: 
Size: 746800 Color: 0
Size: 252834 Color: 1

Bin 3399: 368 of cap free
Amount of items: 2
Items: 
Size: 603519 Color: 1
Size: 396114 Color: 0

Bin 3400: 368 of cap free
Amount of items: 2
Items: 
Size: 786763 Color: 1
Size: 212870 Color: 0

Bin 3401: 369 of cap free
Amount of items: 2
Items: 
Size: 555851 Color: 1
Size: 443781 Color: 0

Bin 3402: 369 of cap free
Amount of items: 2
Items: 
Size: 584063 Color: 1
Size: 415569 Color: 0

Bin 3403: 369 of cap free
Amount of items: 2
Items: 
Size: 631970 Color: 0
Size: 367662 Color: 1

Bin 3404: 369 of cap free
Amount of items: 2
Items: 
Size: 702239 Color: 0
Size: 297393 Color: 1

Bin 3405: 370 of cap free
Amount of items: 2
Items: 
Size: 548017 Color: 1
Size: 451614 Color: 0

Bin 3406: 370 of cap free
Amount of items: 2
Items: 
Size: 702204 Color: 1
Size: 297427 Color: 0

Bin 3407: 371 of cap free
Amount of items: 2
Items: 
Size: 542170 Color: 0
Size: 457460 Color: 1

Bin 3408: 371 of cap free
Amount of items: 2
Items: 
Size: 663036 Color: 1
Size: 336594 Color: 0

Bin 3409: 371 of cap free
Amount of items: 2
Items: 
Size: 692281 Color: 0
Size: 307349 Color: 1

Bin 3410: 371 of cap free
Amount of items: 2
Items: 
Size: 734685 Color: 1
Size: 264945 Color: 0

Bin 3411: 372 of cap free
Amount of items: 2
Items: 
Size: 645867 Color: 1
Size: 353762 Color: 0

Bin 3412: 372 of cap free
Amount of items: 2
Items: 
Size: 778384 Color: 1
Size: 221245 Color: 0

Bin 3413: 372 of cap free
Amount of items: 2
Items: 
Size: 781810 Color: 0
Size: 217819 Color: 1

Bin 3414: 373 of cap free
Amount of items: 2
Items: 
Size: 504964 Color: 0
Size: 494664 Color: 1

Bin 3415: 373 of cap free
Amount of items: 2
Items: 
Size: 720598 Color: 1
Size: 279030 Color: 0

Bin 3416: 374 of cap free
Amount of items: 2
Items: 
Size: 760247 Color: 0
Size: 239380 Color: 1

Bin 3417: 375 of cap free
Amount of items: 2
Items: 
Size: 675447 Color: 0
Size: 324179 Color: 1

Bin 3418: 375 of cap free
Amount of items: 2
Items: 
Size: 779321 Color: 0
Size: 220305 Color: 1

Bin 3419: 376 of cap free
Amount of items: 2
Items: 
Size: 694488 Color: 1
Size: 305137 Color: 0

Bin 3420: 376 of cap free
Amount of items: 2
Items: 
Size: 765818 Color: 0
Size: 233807 Color: 1

Bin 3421: 377 of cap free
Amount of items: 2
Items: 
Size: 515919 Color: 0
Size: 483705 Color: 1

Bin 3422: 377 of cap free
Amount of items: 2
Items: 
Size: 734232 Color: 0
Size: 265392 Color: 1

Bin 3423: 378 of cap free
Amount of items: 2
Items: 
Size: 741068 Color: 0
Size: 258555 Color: 1

Bin 3424: 379 of cap free
Amount of items: 2
Items: 
Size: 590201 Color: 0
Size: 409421 Color: 1

Bin 3425: 379 of cap free
Amount of items: 2
Items: 
Size: 608910 Color: 1
Size: 390712 Color: 0

Bin 3426: 379 of cap free
Amount of items: 2
Items: 
Size: 762049 Color: 0
Size: 237573 Color: 1

Bin 3427: 380 of cap free
Amount of items: 2
Items: 
Size: 787003 Color: 0
Size: 212618 Color: 1

Bin 3428: 382 of cap free
Amount of items: 3
Items: 
Size: 401137 Color: 0
Size: 352899 Color: 1
Size: 245583 Color: 0

Bin 3429: 382 of cap free
Amount of items: 2
Items: 
Size: 773413 Color: 0
Size: 226206 Color: 1

Bin 3430: 383 of cap free
Amount of items: 2
Items: 
Size: 767879 Color: 0
Size: 231739 Color: 1

Bin 3431: 384 of cap free
Amount of items: 2
Items: 
Size: 517773 Color: 1
Size: 481844 Color: 0

Bin 3432: 384 of cap free
Amount of items: 2
Items: 
Size: 541687 Color: 1
Size: 457930 Color: 0

Bin 3433: 384 of cap free
Amount of items: 2
Items: 
Size: 639596 Color: 1
Size: 360021 Color: 0

Bin 3434: 385 of cap free
Amount of items: 2
Items: 
Size: 691578 Color: 0
Size: 308038 Color: 1

Bin 3435: 386 of cap free
Amount of items: 2
Items: 
Size: 703530 Color: 1
Size: 296085 Color: 0

Bin 3436: 386 of cap free
Amount of items: 2
Items: 
Size: 747918 Color: 1
Size: 251697 Color: 0

Bin 3437: 388 of cap free
Amount of items: 2
Items: 
Size: 561397 Color: 1
Size: 438216 Color: 0

Bin 3438: 388 of cap free
Amount of items: 2
Items: 
Size: 669427 Color: 0
Size: 330186 Color: 1

Bin 3439: 388 of cap free
Amount of items: 2
Items: 
Size: 790219 Color: 0
Size: 209394 Color: 1

Bin 3440: 389 of cap free
Amount of items: 2
Items: 
Size: 558385 Color: 0
Size: 441227 Color: 1

Bin 3441: 389 of cap free
Amount of items: 2
Items: 
Size: 571490 Color: 0
Size: 428122 Color: 1

Bin 3442: 390 of cap free
Amount of items: 2
Items: 
Size: 513665 Color: 1
Size: 485946 Color: 0

Bin 3443: 390 of cap free
Amount of items: 2
Items: 
Size: 561776 Color: 0
Size: 437835 Color: 1

Bin 3444: 391 of cap free
Amount of items: 2
Items: 
Size: 517768 Color: 1
Size: 481842 Color: 0

Bin 3445: 391 of cap free
Amount of items: 2
Items: 
Size: 540584 Color: 0
Size: 459026 Color: 1

Bin 3446: 391 of cap free
Amount of items: 2
Items: 
Size: 559729 Color: 1
Size: 439881 Color: 0

Bin 3447: 391 of cap free
Amount of items: 2
Items: 
Size: 774132 Color: 1
Size: 225478 Color: 0

Bin 3448: 392 of cap free
Amount of items: 2
Items: 
Size: 531364 Color: 0
Size: 468245 Color: 1

Bin 3449: 392 of cap free
Amount of items: 2
Items: 
Size: 536286 Color: 0
Size: 463323 Color: 1

Bin 3450: 393 of cap free
Amount of items: 2
Items: 
Size: 670430 Color: 1
Size: 329178 Color: 0

Bin 3451: 394 of cap free
Amount of items: 2
Items: 
Size: 781630 Color: 1
Size: 217977 Color: 0

Bin 3452: 396 of cap free
Amount of items: 2
Items: 
Size: 743249 Color: 0
Size: 256356 Color: 1

Bin 3453: 397 of cap free
Amount of items: 2
Items: 
Size: 745153 Color: 1
Size: 254451 Color: 0

Bin 3454: 398 of cap free
Amount of items: 2
Items: 
Size: 656255 Color: 1
Size: 343348 Color: 0

Bin 3455: 399 of cap free
Amount of items: 2
Items: 
Size: 714000 Color: 1
Size: 285602 Color: 0

Bin 3456: 399 of cap free
Amount of items: 2
Items: 
Size: 769155 Color: 0
Size: 230447 Color: 1

Bin 3457: 400 of cap free
Amount of items: 2
Items: 
Size: 563606 Color: 0
Size: 435995 Color: 1

Bin 3458: 401 of cap free
Amount of items: 7
Items: 
Size: 153699 Color: 0
Size: 153551 Color: 0
Size: 148848 Color: 1
Size: 148394 Color: 1
Size: 148203 Color: 1
Size: 129404 Color: 1
Size: 117501 Color: 0

Bin 3459: 401 of cap free
Amount of items: 2
Items: 
Size: 760221 Color: 0
Size: 239379 Color: 1

Bin 3460: 402 of cap free
Amount of items: 2
Items: 
Size: 546527 Color: 1
Size: 453072 Color: 0

Bin 3461: 402 of cap free
Amount of items: 2
Items: 
Size: 621781 Color: 1
Size: 377818 Color: 0

Bin 3462: 403 of cap free
Amount of items: 2
Items: 
Size: 624714 Color: 0
Size: 374884 Color: 1

Bin 3463: 403 of cap free
Amount of items: 2
Items: 
Size: 744514 Color: 0
Size: 255084 Color: 1

Bin 3464: 404 of cap free
Amount of items: 2
Items: 
Size: 759311 Color: 1
Size: 240286 Color: 0

Bin 3465: 405 of cap free
Amount of items: 2
Items: 
Size: 789717 Color: 0
Size: 209879 Color: 1

Bin 3466: 406 of cap free
Amount of items: 2
Items: 
Size: 641755 Color: 1
Size: 357840 Color: 0

Bin 3467: 406 of cap free
Amount of items: 2
Items: 
Size: 700313 Color: 1
Size: 299282 Color: 0

Bin 3468: 407 of cap free
Amount of items: 2
Items: 
Size: 713515 Color: 1
Size: 286079 Color: 0

Bin 3469: 408 of cap free
Amount of items: 2
Items: 
Size: 574090 Color: 0
Size: 425503 Color: 1

Bin 3470: 408 of cap free
Amount of items: 2
Items: 
Size: 663024 Color: 1
Size: 336569 Color: 0

Bin 3471: 408 of cap free
Amount of items: 2
Items: 
Size: 750765 Color: 0
Size: 248828 Color: 1

Bin 3472: 408 of cap free
Amount of items: 2
Items: 
Size: 795566 Color: 1
Size: 204027 Color: 0

Bin 3473: 409 of cap free
Amount of items: 2
Items: 
Size: 611588 Color: 1
Size: 388004 Color: 0

Bin 3474: 409 of cap free
Amount of items: 2
Items: 
Size: 718150 Color: 0
Size: 281442 Color: 1

Bin 3475: 409 of cap free
Amount of items: 2
Items: 
Size: 791623 Color: 1
Size: 207969 Color: 0

Bin 3476: 410 of cap free
Amount of items: 2
Items: 
Size: 644251 Color: 0
Size: 355340 Color: 1

Bin 3477: 410 of cap free
Amount of items: 2
Items: 
Size: 772115 Color: 1
Size: 227476 Color: 0

Bin 3478: 411 of cap free
Amount of items: 2
Items: 
Size: 560552 Color: 0
Size: 439038 Color: 1

Bin 3479: 414 of cap free
Amount of items: 2
Items: 
Size: 640710 Color: 1
Size: 358877 Color: 0

Bin 3480: 415 of cap free
Amount of items: 2
Items: 
Size: 650962 Color: 1
Size: 348624 Color: 0

Bin 3481: 415 of cap free
Amount of items: 2
Items: 
Size: 656421 Color: 0
Size: 343165 Color: 1

Bin 3482: 416 of cap free
Amount of items: 2
Items: 
Size: 501410 Color: 0
Size: 498175 Color: 1

Bin 3483: 416 of cap free
Amount of items: 2
Items: 
Size: 585728 Color: 0
Size: 413857 Color: 1

Bin 3484: 417 of cap free
Amount of items: 2
Items: 
Size: 635600 Color: 1
Size: 363984 Color: 0

Bin 3485: 417 of cap free
Amount of items: 2
Items: 
Size: 682308 Color: 1
Size: 317276 Color: 0

Bin 3486: 418 of cap free
Amount of items: 2
Items: 
Size: 799638 Color: 1
Size: 199945 Color: 0

Bin 3487: 421 of cap free
Amount of items: 2
Items: 
Size: 511811 Color: 1
Size: 487769 Color: 0

Bin 3488: 421 of cap free
Amount of items: 2
Items: 
Size: 667584 Color: 1
Size: 331996 Color: 0

Bin 3489: 422 of cap free
Amount of items: 2
Items: 
Size: 778960 Color: 1
Size: 220619 Color: 0

Bin 3490: 423 of cap free
Amount of items: 2
Items: 
Size: 512387 Color: 0
Size: 487191 Color: 1

Bin 3491: 423 of cap free
Amount of items: 2
Items: 
Size: 798483 Color: 1
Size: 201095 Color: 0

Bin 3492: 424 of cap free
Amount of items: 2
Items: 
Size: 567235 Color: 0
Size: 432342 Color: 1

Bin 3493: 425 of cap free
Amount of items: 2
Items: 
Size: 508601 Color: 1
Size: 490975 Color: 0

Bin 3494: 425 of cap free
Amount of items: 2
Items: 
Size: 511322 Color: 0
Size: 488254 Color: 1

Bin 3495: 425 of cap free
Amount of items: 2
Items: 
Size: 630366 Color: 0
Size: 369210 Color: 1

Bin 3496: 426 of cap free
Amount of items: 2
Items: 
Size: 710113 Color: 0
Size: 289462 Color: 1

Bin 3497: 426 of cap free
Amount of items: 2
Items: 
Size: 733484 Color: 1
Size: 266091 Color: 0

Bin 3498: 426 of cap free
Amount of items: 2
Items: 
Size: 759310 Color: 1
Size: 240265 Color: 0

Bin 3499: 427 of cap free
Amount of items: 2
Items: 
Size: 655176 Color: 1
Size: 344398 Color: 0

Bin 3500: 427 of cap free
Amount of items: 2
Items: 
Size: 709593 Color: 0
Size: 289981 Color: 1

Bin 3501: 429 of cap free
Amount of items: 2
Items: 
Size: 550124 Color: 1
Size: 449448 Color: 0

Bin 3502: 429 of cap free
Amount of items: 2
Items: 
Size: 604308 Color: 0
Size: 395264 Color: 1

Bin 3503: 431 of cap free
Amount of items: 2
Items: 
Size: 685607 Color: 1
Size: 313963 Color: 0

Bin 3504: 433 of cap free
Amount of items: 2
Items: 
Size: 530550 Color: 0
Size: 469018 Color: 1

Bin 3505: 433 of cap free
Amount of items: 2
Items: 
Size: 592443 Color: 1
Size: 407125 Color: 0

Bin 3506: 433 of cap free
Amount of items: 2
Items: 
Size: 674960 Color: 1
Size: 324608 Color: 0

Bin 3507: 433 of cap free
Amount of items: 2
Items: 
Size: 788223 Color: 1
Size: 211345 Color: 0

Bin 3508: 435 of cap free
Amount of items: 2
Items: 
Size: 600443 Color: 1
Size: 399123 Color: 0

Bin 3509: 437 of cap free
Amount of items: 2
Items: 
Size: 611575 Color: 1
Size: 387989 Color: 0

Bin 3510: 441 of cap free
Amount of items: 2
Items: 
Size: 582330 Color: 0
Size: 417230 Color: 1

Bin 3511: 441 of cap free
Amount of items: 2
Items: 
Size: 683648 Color: 1
Size: 315912 Color: 0

Bin 3512: 442 of cap free
Amount of items: 2
Items: 
Size: 534708 Color: 1
Size: 464851 Color: 0

Bin 3513: 442 of cap free
Amount of items: 2
Items: 
Size: 555523 Color: 0
Size: 444036 Color: 1

Bin 3514: 442 of cap free
Amount of items: 2
Items: 
Size: 730269 Color: 0
Size: 269290 Color: 1

Bin 3515: 442 of cap free
Amount of items: 2
Items: 
Size: 755851 Color: 0
Size: 243708 Color: 1

Bin 3516: 442 of cap free
Amount of items: 2
Items: 
Size: 763251 Color: 0
Size: 236308 Color: 1

Bin 3517: 444 of cap free
Amount of items: 2
Items: 
Size: 580060 Color: 0
Size: 419497 Color: 1

Bin 3518: 445 of cap free
Amount of items: 2
Items: 
Size: 538851 Color: 1
Size: 460705 Color: 0

Bin 3519: 445 of cap free
Amount of items: 2
Items: 
Size: 633152 Color: 0
Size: 366404 Color: 1

Bin 3520: 446 of cap free
Amount of items: 2
Items: 
Size: 614205 Color: 1
Size: 385350 Color: 0

Bin 3521: 446 of cap free
Amount of items: 2
Items: 
Size: 628312 Color: 0
Size: 371243 Color: 1

Bin 3522: 447 of cap free
Amount of items: 2
Items: 
Size: 557365 Color: 0
Size: 442189 Color: 1

Bin 3523: 447 of cap free
Amount of items: 2
Items: 
Size: 704642 Color: 0
Size: 294912 Color: 1

Bin 3524: 447 of cap free
Amount of items: 2
Items: 
Size: 739205 Color: 1
Size: 260349 Color: 0

Bin 3525: 448 of cap free
Amount of items: 2
Items: 
Size: 519425 Color: 0
Size: 480128 Color: 1

Bin 3526: 448 of cap free
Amount of items: 2
Items: 
Size: 540561 Color: 0
Size: 458992 Color: 1

Bin 3527: 448 of cap free
Amount of items: 2
Items: 
Size: 677862 Color: 0
Size: 321691 Color: 1

Bin 3528: 448 of cap free
Amount of items: 2
Items: 
Size: 739254 Color: 0
Size: 260299 Color: 1

Bin 3529: 449 of cap free
Amount of items: 2
Items: 
Size: 600424 Color: 0
Size: 399128 Color: 1

Bin 3530: 450 of cap free
Amount of items: 2
Items: 
Size: 560365 Color: 1
Size: 439186 Color: 0

Bin 3531: 450 of cap free
Amount of items: 2
Items: 
Size: 736578 Color: 1
Size: 262973 Color: 0

Bin 3532: 450 of cap free
Amount of items: 2
Items: 
Size: 777752 Color: 0
Size: 221799 Color: 1

Bin 3533: 452 of cap free
Amount of items: 2
Items: 
Size: 546494 Color: 0
Size: 453055 Color: 1

Bin 3534: 452 of cap free
Amount of items: 2
Items: 
Size: 561871 Color: 1
Size: 437678 Color: 0

Bin 3535: 452 of cap free
Amount of items: 2
Items: 
Size: 681637 Color: 1
Size: 317912 Color: 0

Bin 3536: 452 of cap free
Amount of items: 2
Items: 
Size: 730023 Color: 1
Size: 269526 Color: 0

Bin 3537: 453 of cap free
Amount of items: 2
Items: 
Size: 716529 Color: 1
Size: 283019 Color: 0

Bin 3538: 454 of cap free
Amount of items: 2
Items: 
Size: 636179 Color: 0
Size: 363368 Color: 1

Bin 3539: 460 of cap free
Amount of items: 2
Items: 
Size: 515273 Color: 0
Size: 484268 Color: 1

Bin 3540: 460 of cap free
Amount of items: 2
Items: 
Size: 740323 Color: 1
Size: 259218 Color: 0

Bin 3541: 460 of cap free
Amount of items: 2
Items: 
Size: 748463 Color: 0
Size: 251078 Color: 1

Bin 3542: 461 of cap free
Amount of items: 2
Items: 
Size: 776332 Color: 0
Size: 223208 Color: 1

Bin 3543: 463 of cap free
Amount of items: 2
Items: 
Size: 777018 Color: 0
Size: 222520 Color: 1

Bin 3544: 464 of cap free
Amount of items: 2
Items: 
Size: 604589 Color: 1
Size: 394948 Color: 0

Bin 3545: 465 of cap free
Amount of items: 2
Items: 
Size: 616734 Color: 0
Size: 382802 Color: 1

Bin 3546: 465 of cap free
Amount of items: 2
Items: 
Size: 749508 Color: 1
Size: 250028 Color: 0

Bin 3547: 466 of cap free
Amount of items: 2
Items: 
Size: 594115 Color: 0
Size: 405420 Color: 1

Bin 3548: 467 of cap free
Amount of items: 2
Items: 
Size: 559658 Color: 1
Size: 439876 Color: 0

Bin 3549: 469 of cap free
Amount of items: 2
Items: 
Size: 667734 Color: 0
Size: 331798 Color: 1

Bin 3550: 470 of cap free
Amount of items: 2
Items: 
Size: 670392 Color: 1
Size: 329139 Color: 0

Bin 3551: 470 of cap free
Amount of items: 2
Items: 
Size: 675403 Color: 0
Size: 324128 Color: 1

Bin 3552: 472 of cap free
Amount of items: 2
Items: 
Size: 502432 Color: 1
Size: 497097 Color: 0

Bin 3553: 472 of cap free
Amount of items: 2
Items: 
Size: 567899 Color: 0
Size: 431630 Color: 1

Bin 3554: 472 of cap free
Amount of items: 2
Items: 
Size: 568386 Color: 0
Size: 431143 Color: 1

Bin 3555: 472 of cap free
Amount of items: 2
Items: 
Size: 573234 Color: 0
Size: 426295 Color: 1

Bin 3556: 472 of cap free
Amount of items: 2
Items: 
Size: 693716 Color: 1
Size: 305813 Color: 0

Bin 3557: 473 of cap free
Amount of items: 2
Items: 
Size: 505542 Color: 0
Size: 493986 Color: 1

Bin 3558: 473 of cap free
Amount of items: 2
Items: 
Size: 649637 Color: 1
Size: 349891 Color: 0

Bin 3559: 473 of cap free
Amount of items: 2
Items: 
Size: 668429 Color: 0
Size: 331099 Color: 1

Bin 3560: 474 of cap free
Amount of items: 2
Items: 
Size: 576580 Color: 0
Size: 422947 Color: 1

Bin 3561: 474 of cap free
Amount of items: 2
Items: 
Size: 729451 Color: 1
Size: 270076 Color: 0

Bin 3562: 475 of cap free
Amount of items: 2
Items: 
Size: 620206 Color: 1
Size: 379320 Color: 0

Bin 3563: 477 of cap free
Amount of items: 2
Items: 
Size: 579118 Color: 1
Size: 420406 Color: 0

Bin 3564: 477 of cap free
Amount of items: 2
Items: 
Size: 699424 Color: 0
Size: 300100 Color: 1

Bin 3565: 481 of cap free
Amount of items: 2
Items: 
Size: 680099 Color: 1
Size: 319421 Color: 0

Bin 3566: 481 of cap free
Amount of items: 2
Items: 
Size: 776903 Color: 1
Size: 222617 Color: 0

Bin 3567: 482 of cap free
Amount of items: 2
Items: 
Size: 654906 Color: 0
Size: 344613 Color: 1

Bin 3568: 483 of cap free
Amount of items: 2
Items: 
Size: 630315 Color: 1
Size: 369203 Color: 0

Bin 3569: 484 of cap free
Amount of items: 2
Items: 
Size: 634526 Color: 1
Size: 364991 Color: 0

Bin 3570: 484 of cap free
Amount of items: 2
Items: 
Size: 705889 Color: 1
Size: 293628 Color: 0

Bin 3571: 485 of cap free
Amount of items: 2
Items: 
Size: 587443 Color: 1
Size: 412073 Color: 0

Bin 3572: 485 of cap free
Amount of items: 2
Items: 
Size: 643217 Color: 0
Size: 356299 Color: 1

Bin 3573: 486 of cap free
Amount of items: 6
Items: 
Size: 171024 Color: 0
Size: 170998 Color: 0
Size: 169197 Color: 1
Size: 168902 Color: 1
Size: 168588 Color: 1
Size: 150806 Color: 0

Bin 3574: 487 of cap free
Amount of items: 2
Items: 
Size: 718523 Color: 1
Size: 280991 Color: 0

Bin 3575: 488 of cap free
Amount of items: 2
Items: 
Size: 628515 Color: 1
Size: 370998 Color: 0

Bin 3576: 490 of cap free
Amount of items: 2
Items: 
Size: 648065 Color: 0
Size: 351446 Color: 1

Bin 3577: 491 of cap free
Amount of items: 2
Items: 
Size: 508552 Color: 1
Size: 490958 Color: 0

Bin 3578: 492 of cap free
Amount of items: 2
Items: 
Size: 513606 Color: 1
Size: 485903 Color: 0

Bin 3579: 492 of cap free
Amount of items: 2
Items: 
Size: 565503 Color: 0
Size: 434006 Color: 1

Bin 3580: 492 of cap free
Amount of items: 2
Items: 
Size: 685593 Color: 1
Size: 313916 Color: 0

Bin 3581: 493 of cap free
Amount of items: 2
Items: 
Size: 721222 Color: 0
Size: 278286 Color: 1

Bin 3582: 493 of cap free
Amount of items: 2
Items: 
Size: 762576 Color: 0
Size: 236932 Color: 1

Bin 3583: 495 of cap free
Amount of items: 2
Items: 
Size: 506177 Color: 0
Size: 493329 Color: 1

Bin 3584: 495 of cap free
Amount of items: 2
Items: 
Size: 531902 Color: 1
Size: 467604 Color: 0

Bin 3585: 495 of cap free
Amount of items: 2
Items: 
Size: 551956 Color: 1
Size: 447550 Color: 0

Bin 3586: 496 of cap free
Amount of items: 2
Items: 
Size: 674952 Color: 1
Size: 324553 Color: 0

Bin 3587: 496 of cap free
Amount of items: 2
Items: 
Size: 749545 Color: 0
Size: 249960 Color: 1

Bin 3588: 499 of cap free
Amount of items: 2
Items: 
Size: 629372 Color: 0
Size: 370130 Color: 1

Bin 3589: 499 of cap free
Amount of items: 2
Items: 
Size: 645261 Color: 1
Size: 354241 Color: 0

Bin 3590: 499 of cap free
Amount of items: 2
Items: 
Size: 667561 Color: 1
Size: 331941 Color: 0

Bin 3591: 499 of cap free
Amount of items: 2
Items: 
Size: 732372 Color: 0
Size: 267130 Color: 1

Bin 3592: 500 of cap free
Amount of items: 2
Items: 
Size: 519459 Color: 1
Size: 480042 Color: 0

Bin 3593: 500 of cap free
Amount of items: 2
Items: 
Size: 540511 Color: 0
Size: 458990 Color: 1

Bin 3594: 500 of cap free
Amount of items: 2
Items: 
Size: 593436 Color: 0
Size: 406065 Color: 1

Bin 3595: 503 of cap free
Amount of items: 2
Items: 
Size: 617964 Color: 0
Size: 381534 Color: 1

Bin 3596: 504 of cap free
Amount of items: 2
Items: 
Size: 544763 Color: 1
Size: 454734 Color: 0

Bin 3597: 504 of cap free
Amount of items: 2
Items: 
Size: 608896 Color: 1
Size: 390601 Color: 0

Bin 3598: 505 of cap free
Amount of items: 2
Items: 
Size: 511255 Color: 0
Size: 488241 Color: 1

Bin 3599: 507 of cap free
Amount of items: 2
Items: 
Size: 580900 Color: 1
Size: 418594 Color: 0

Bin 3600: 507 of cap free
Amount of items: 2
Items: 
Size: 665826 Color: 1
Size: 333668 Color: 0

Bin 3601: 509 of cap free
Amount of items: 2
Items: 
Size: 588289 Color: 0
Size: 411203 Color: 1

Bin 3602: 509 of cap free
Amount of items: 2
Items: 
Size: 790919 Color: 0
Size: 208573 Color: 1

Bin 3603: 510 of cap free
Amount of items: 2
Items: 
Size: 526189 Color: 0
Size: 473302 Color: 1

Bin 3604: 510 of cap free
Amount of items: 2
Items: 
Size: 661889 Color: 0
Size: 337602 Color: 1

Bin 3605: 511 of cap free
Amount of items: 2
Items: 
Size: 702071 Color: 1
Size: 297419 Color: 0

Bin 3606: 513 of cap free
Amount of items: 2
Items: 
Size: 517209 Color: 1
Size: 482279 Color: 0

Bin 3607: 513 of cap free
Amount of items: 2
Items: 
Size: 735629 Color: 1
Size: 263859 Color: 0

Bin 3608: 514 of cap free
Amount of items: 2
Items: 
Size: 610257 Color: 1
Size: 389230 Color: 0

Bin 3609: 514 of cap free
Amount of items: 2
Items: 
Size: 702104 Color: 0
Size: 297383 Color: 1

Bin 3610: 515 of cap free
Amount of items: 2
Items: 
Size: 622070 Color: 0
Size: 377416 Color: 1

Bin 3611: 515 of cap free
Amount of items: 2
Items: 
Size: 646535 Color: 1
Size: 352951 Color: 0

Bin 3612: 516 of cap free
Amount of items: 2
Items: 
Size: 782029 Color: 1
Size: 217456 Color: 0

Bin 3613: 517 of cap free
Amount of items: 2
Items: 
Size: 576399 Color: 1
Size: 423085 Color: 0

Bin 3614: 517 of cap free
Amount of items: 2
Items: 
Size: 624426 Color: 1
Size: 375058 Color: 0

Bin 3615: 519 of cap free
Amount of items: 2
Items: 
Size: 698796 Color: 0
Size: 300686 Color: 1

Bin 3616: 520 of cap free
Amount of items: 2
Items: 
Size: 748449 Color: 0
Size: 251032 Color: 1

Bin 3617: 523 of cap free
Amount of items: 6
Items: 
Size: 178206 Color: 0
Size: 176341 Color: 0
Size: 173021 Color: 1
Size: 172996 Color: 1
Size: 171637 Color: 1
Size: 127277 Color: 0

Bin 3618: 523 of cap free
Amount of items: 2
Items: 
Size: 627044 Color: 1
Size: 372434 Color: 0

Bin 3619: 523 of cap free
Amount of items: 2
Items: 
Size: 671358 Color: 0
Size: 328120 Color: 1

Bin 3620: 524 of cap free
Amount of items: 2
Items: 
Size: 535547 Color: 0
Size: 463930 Color: 1

Bin 3621: 524 of cap free
Amount of items: 2
Items: 
Size: 539231 Color: 0
Size: 460246 Color: 1

Bin 3622: 524 of cap free
Amount of items: 2
Items: 
Size: 593841 Color: 1
Size: 405636 Color: 0

Bin 3623: 524 of cap free
Amount of items: 2
Items: 
Size: 644217 Color: 0
Size: 355260 Color: 1

Bin 3624: 525 of cap free
Amount of items: 2
Items: 
Size: 683386 Color: 0
Size: 316090 Color: 1

Bin 3625: 525 of cap free
Amount of items: 2
Items: 
Size: 782985 Color: 0
Size: 216491 Color: 1

Bin 3626: 526 of cap free
Amount of items: 6
Items: 
Size: 168225 Color: 0
Size: 167953 Color: 0
Size: 167290 Color: 1
Size: 167206 Color: 1
Size: 167011 Color: 1
Size: 161790 Color: 0

Bin 3627: 526 of cap free
Amount of items: 2
Items: 
Size: 633096 Color: 0
Size: 366379 Color: 1

Bin 3628: 527 of cap free
Amount of items: 2
Items: 
Size: 513779 Color: 0
Size: 485695 Color: 1

Bin 3629: 529 of cap free
Amount of items: 2
Items: 
Size: 584008 Color: 1
Size: 415464 Color: 0

Bin 3630: 529 of cap free
Amount of items: 2
Items: 
Size: 610064 Color: 0
Size: 389408 Color: 1

Bin 3631: 530 of cap free
Amount of items: 2
Items: 
Size: 586786 Color: 1
Size: 412685 Color: 0

Bin 3632: 530 of cap free
Amount of items: 2
Items: 
Size: 644579 Color: 1
Size: 354892 Color: 0

Bin 3633: 530 of cap free
Amount of items: 2
Items: 
Size: 699380 Color: 0
Size: 300091 Color: 1

Bin 3634: 531 of cap free
Amount of items: 2
Items: 
Size: 712160 Color: 0
Size: 287310 Color: 1

Bin 3635: 531 of cap free
Amount of items: 2
Items: 
Size: 751346 Color: 0
Size: 248124 Color: 1

Bin 3636: 532 of cap free
Amount of items: 2
Items: 
Size: 716247 Color: 0
Size: 283222 Color: 1

Bin 3637: 533 of cap free
Amount of items: 2
Items: 
Size: 762057 Color: 1
Size: 237411 Color: 0

Bin 3638: 534 of cap free
Amount of items: 2
Items: 
Size: 540546 Color: 1
Size: 458921 Color: 0

Bin 3639: 534 of cap free
Amount of items: 2
Items: 
Size: 789665 Color: 0
Size: 209802 Color: 1

Bin 3640: 535 of cap free
Amount of items: 2
Items: 
Size: 538265 Color: 1
Size: 461201 Color: 0

Bin 3641: 535 of cap free
Amount of items: 2
Items: 
Size: 658680 Color: 1
Size: 340786 Color: 0

Bin 3642: 535 of cap free
Amount of items: 2
Items: 
Size: 694460 Color: 1
Size: 305006 Color: 0

Bin 3643: 535 of cap free
Amount of items: 2
Items: 
Size: 716526 Color: 1
Size: 282940 Color: 0

Bin 3644: 535 of cap free
Amount of items: 2
Items: 
Size: 763782 Color: 0
Size: 235684 Color: 1

Bin 3645: 536 of cap free
Amount of items: 2
Items: 
Size: 645838 Color: 1
Size: 353627 Color: 0

Bin 3646: 537 of cap free
Amount of items: 2
Items: 
Size: 675393 Color: 0
Size: 324071 Color: 1

Bin 3647: 537 of cap free
Amount of items: 2
Items: 
Size: 736561 Color: 1
Size: 262903 Color: 0

Bin 3648: 538 of cap free
Amount of items: 2
Items: 
Size: 612283 Color: 0
Size: 387180 Color: 1

Bin 3649: 538 of cap free
Amount of items: 2
Items: 
Size: 758368 Color: 0
Size: 241095 Color: 1

Bin 3650: 539 of cap free
Amount of items: 2
Items: 
Size: 598265 Color: 1
Size: 401197 Color: 0

Bin 3651: 540 of cap free
Amount of items: 2
Items: 
Size: 600349 Color: 1
Size: 399112 Color: 0

Bin 3652: 540 of cap free
Amount of items: 2
Items: 
Size: 611120 Color: 0
Size: 388341 Color: 1

Bin 3653: 540 of cap free
Amount of items: 2
Items: 
Size: 742288 Color: 1
Size: 257173 Color: 0

Bin 3654: 542 of cap free
Amount of items: 2
Items: 
Size: 705850 Color: 1
Size: 293609 Color: 0

Bin 3655: 543 of cap free
Amount of items: 2
Items: 
Size: 577875 Color: 0
Size: 421583 Color: 1

Bin 3656: 543 of cap free
Amount of items: 2
Items: 
Size: 665312 Color: 0
Size: 334146 Color: 1

Bin 3657: 544 of cap free
Amount of items: 2
Items: 
Size: 592045 Color: 0
Size: 407412 Color: 1

Bin 3658: 545 of cap free
Amount of items: 6
Items: 
Size: 174986 Color: 0
Size: 174731 Color: 0
Size: 170566 Color: 1
Size: 170058 Color: 1
Size: 169853 Color: 1
Size: 139262 Color: 0

Bin 3659: 546 of cap free
Amount of items: 2
Items: 
Size: 527564 Color: 0
Size: 471891 Color: 1

Bin 3660: 548 of cap free
Amount of items: 2
Items: 
Size: 516460 Color: 0
Size: 482993 Color: 1

Bin 3661: 549 of cap free
Amount of items: 2
Items: 
Size: 531210 Color: 0
Size: 468242 Color: 1

Bin 3662: 549 of cap free
Amount of items: 2
Items: 
Size: 790913 Color: 0
Size: 208539 Color: 1

Bin 3663: 550 of cap free
Amount of items: 2
Items: 
Size: 540502 Color: 0
Size: 458949 Color: 1

Bin 3664: 550 of cap free
Amount of items: 2
Items: 
Size: 662713 Color: 0
Size: 336738 Color: 1

Bin 3665: 551 of cap free
Amount of items: 2
Items: 
Size: 593825 Color: 1
Size: 405625 Color: 0

Bin 3666: 551 of cap free
Amount of items: 2
Items: 
Size: 608590 Color: 0
Size: 390860 Color: 1

Bin 3667: 551 of cap free
Amount of items: 2
Items: 
Size: 634503 Color: 1
Size: 364947 Color: 0

Bin 3668: 551 of cap free
Amount of items: 2
Items: 
Size: 763147 Color: 0
Size: 236303 Color: 1

Bin 3669: 552 of cap free
Amount of items: 2
Items: 
Size: 752668 Color: 1
Size: 246781 Color: 0

Bin 3670: 553 of cap free
Amount of items: 2
Items: 
Size: 576370 Color: 1
Size: 423078 Color: 0

Bin 3671: 554 of cap free
Amount of items: 2
Items: 
Size: 595908 Color: 1
Size: 403539 Color: 0

Bin 3672: 555 of cap free
Amount of items: 2
Items: 
Size: 525978 Color: 1
Size: 473468 Color: 0

Bin 3673: 555 of cap free
Amount of items: 2
Items: 
Size: 700369 Color: 0
Size: 299077 Color: 1

Bin 3674: 556 of cap free
Amount of items: 2
Items: 
Size: 598263 Color: 1
Size: 401182 Color: 0

Bin 3675: 557 of cap free
Amount of items: 2
Items: 
Size: 530191 Color: 1
Size: 469253 Color: 0

Bin 3676: 557 of cap free
Amount of items: 2
Items: 
Size: 647175 Color: 0
Size: 352269 Color: 1

Bin 3677: 557 of cap free
Amount of items: 2
Items: 
Size: 774077 Color: 1
Size: 225367 Color: 0

Bin 3678: 558 of cap free
Amount of items: 2
Items: 
Size: 736798 Color: 0
Size: 262645 Color: 1

Bin 3679: 558 of cap free
Amount of items: 2
Items: 
Size: 772700 Color: 1
Size: 226743 Color: 0

Bin 3680: 559 of cap free
Amount of items: 2
Items: 
Size: 594098 Color: 0
Size: 405344 Color: 1

Bin 3681: 559 of cap free
Amount of items: 2
Items: 
Size: 664236 Color: 1
Size: 335206 Color: 0

Bin 3682: 560 of cap free
Amount of items: 2
Items: 
Size: 786606 Color: 1
Size: 212835 Color: 0

Bin 3683: 563 of cap free
Amount of items: 2
Items: 
Size: 588830 Color: 0
Size: 410608 Color: 1

Bin 3684: 566 of cap free
Amount of items: 2
Items: 
Size: 563682 Color: 1
Size: 435753 Color: 0

Bin 3685: 566 of cap free
Amount of items: 2
Items: 
Size: 744446 Color: 0
Size: 254989 Color: 1

Bin 3686: 567 of cap free
Amount of items: 2
Items: 
Size: 794456 Color: 0
Size: 204978 Color: 1

Bin 3687: 569 of cap free
Amount of items: 2
Items: 
Size: 658220 Color: 0
Size: 341212 Color: 1

Bin 3688: 569 of cap free
Amount of items: 2
Items: 
Size: 691404 Color: 0
Size: 308028 Color: 1

Bin 3689: 572 of cap free
Amount of items: 2
Items: 
Size: 569134 Color: 1
Size: 430295 Color: 0

Bin 3690: 572 of cap free
Amount of items: 2
Items: 
Size: 601455 Color: 1
Size: 397974 Color: 0

Bin 3691: 574 of cap free
Amount of items: 2
Items: 
Size: 697373 Color: 1
Size: 302054 Color: 0

Bin 3692: 575 of cap free
Amount of items: 2
Items: 
Size: 630971 Color: 0
Size: 368455 Color: 1

Bin 3693: 576 of cap free
Amount of items: 2
Items: 
Size: 570593 Color: 0
Size: 428832 Color: 1

Bin 3694: 576 of cap free
Amount of items: 2
Items: 
Size: 686316 Color: 0
Size: 313109 Color: 1

Bin 3695: 581 of cap free
Amount of items: 2
Items: 
Size: 512369 Color: 0
Size: 487051 Color: 1

Bin 3696: 581 of cap free
Amount of items: 2
Items: 
Size: 753789 Color: 0
Size: 245631 Color: 1

Bin 3697: 582 of cap free
Amount of items: 2
Items: 
Size: 533630 Color: 0
Size: 465789 Color: 1

Bin 3698: 582 of cap free
Amount of items: 2
Items: 
Size: 615117 Color: 0
Size: 384302 Color: 1

Bin 3699: 584 of cap free
Amount of items: 2
Items: 
Size: 557349 Color: 0
Size: 442068 Color: 1

Bin 3700: 584 of cap free
Amount of items: 2
Items: 
Size: 700352 Color: 0
Size: 299065 Color: 1

Bin 3701: 585 of cap free
Amount of items: 2
Items: 
Size: 594082 Color: 0
Size: 405334 Color: 1

Bin 3702: 585 of cap free
Amount of items: 2
Items: 
Size: 599281 Color: 0
Size: 400135 Color: 1

Bin 3703: 587 of cap free
Amount of items: 2
Items: 
Size: 713365 Color: 1
Size: 286049 Color: 0

Bin 3704: 588 of cap free
Amount of items: 2
Items: 
Size: 509711 Color: 1
Size: 489702 Color: 0

Bin 3705: 590 of cap free
Amount of items: 2
Items: 
Size: 768384 Color: 1
Size: 231027 Color: 0

Bin 3706: 592 of cap free
Amount of items: 2
Items: 
Size: 736532 Color: 1
Size: 262877 Color: 0

Bin 3707: 593 of cap free
Amount of items: 2
Items: 
Size: 693644 Color: 1
Size: 305764 Color: 0

Bin 3708: 593 of cap free
Amount of items: 2
Items: 
Size: 751292 Color: 0
Size: 248116 Color: 1

Bin 3709: 595 of cap free
Amount of items: 2
Items: 
Size: 691710 Color: 1
Size: 307696 Color: 0

Bin 3710: 595 of cap free
Amount of items: 2
Items: 
Size: 713449 Color: 0
Size: 285957 Color: 1

Bin 3711: 598 of cap free
Amount of items: 2
Items: 
Size: 706204 Color: 0
Size: 293199 Color: 1

Bin 3712: 600 of cap free
Amount of items: 2
Items: 
Size: 624385 Color: 1
Size: 375016 Color: 0

Bin 3713: 602 of cap free
Amount of items: 2
Items: 
Size: 526642 Color: 1
Size: 472757 Color: 0

Bin 3714: 602 of cap free
Amount of items: 2
Items: 
Size: 677841 Color: 0
Size: 321558 Color: 1

Bin 3715: 602 of cap free
Amount of items: 2
Items: 
Size: 796520 Color: 1
Size: 202879 Color: 0

Bin 3716: 604 of cap free
Amount of items: 2
Items: 
Size: 525021 Color: 0
Size: 474376 Color: 1

Bin 3717: 607 of cap free
Amount of items: 2
Items: 
Size: 620076 Color: 1
Size: 379318 Color: 0

Bin 3718: 608 of cap free
Amount of items: 2
Items: 
Size: 515213 Color: 0
Size: 484180 Color: 1

Bin 3719: 608 of cap free
Amount of items: 2
Items: 
Size: 627118 Color: 0
Size: 372275 Color: 1

Bin 3720: 608 of cap free
Amount of items: 2
Items: 
Size: 759189 Color: 1
Size: 240204 Color: 0

Bin 3721: 612 of cap free
Amount of items: 2
Items: 
Size: 713448 Color: 0
Size: 285941 Color: 1

Bin 3722: 613 of cap free
Amount of items: 2
Items: 
Size: 546437 Color: 0
Size: 452951 Color: 1

Bin 3723: 613 of cap free
Amount of items: 2
Items: 
Size: 728403 Color: 1
Size: 270985 Color: 0

Bin 3724: 615 of cap free
Amount of items: 2
Items: 
Size: 681624 Color: 0
Size: 317762 Color: 1

Bin 3725: 618 of cap free
Amount of items: 2
Items: 
Size: 622998 Color: 1
Size: 376385 Color: 0

Bin 3726: 620 of cap free
Amount of items: 2
Items: 
Size: 625450 Color: 1
Size: 373931 Color: 0

Bin 3727: 620 of cap free
Amount of items: 2
Items: 
Size: 784840 Color: 0
Size: 214541 Color: 1

Bin 3728: 621 of cap free
Amount of items: 2
Items: 
Size: 771709 Color: 0
Size: 227671 Color: 1

Bin 3729: 623 of cap free
Amount of items: 2
Items: 
Size: 558237 Color: 0
Size: 441141 Color: 1

Bin 3730: 624 of cap free
Amount of items: 2
Items: 
Size: 516041 Color: 1
Size: 483336 Color: 0

Bin 3731: 624 of cap free
Amount of items: 2
Items: 
Size: 580803 Color: 1
Size: 418574 Color: 0

Bin 3732: 624 of cap free
Amount of items: 2
Items: 
Size: 683524 Color: 1
Size: 315853 Color: 0

Bin 3733: 626 of cap free
Amount of items: 2
Items: 
Size: 746717 Color: 0
Size: 252658 Color: 1

Bin 3734: 629 of cap free
Amount of items: 2
Items: 
Size: 627017 Color: 1
Size: 372355 Color: 0

Bin 3735: 630 of cap free
Amount of items: 2
Items: 
Size: 638274 Color: 0
Size: 361097 Color: 1

Bin 3736: 630 of cap free
Amount of items: 2
Items: 
Size: 738271 Color: 1
Size: 261100 Color: 0

Bin 3737: 631 of cap free
Amount of items: 2
Items: 
Size: 677149 Color: 1
Size: 322221 Color: 0

Bin 3738: 632 of cap free
Amount of items: 2
Items: 
Size: 760203 Color: 0
Size: 239166 Color: 1

Bin 3739: 634 of cap free
Amount of items: 2
Items: 
Size: 671282 Color: 0
Size: 328085 Color: 1

Bin 3740: 634 of cap free
Amount of items: 2
Items: 
Size: 761003 Color: 0
Size: 238364 Color: 1

Bin 3741: 635 of cap free
Amount of items: 2
Items: 
Size: 560194 Color: 1
Size: 439172 Color: 0

Bin 3742: 635 of cap free
Amount of items: 2
Items: 
Size: 674267 Color: 0
Size: 325099 Color: 1

Bin 3743: 637 of cap free
Amount of items: 2
Items: 
Size: 599362 Color: 1
Size: 400002 Color: 0

Bin 3744: 637 of cap free
Amount of items: 2
Items: 
Size: 621360 Color: 0
Size: 378004 Color: 1

Bin 3745: 638 of cap free
Amount of items: 2
Items: 
Size: 656199 Color: 1
Size: 343164 Color: 0

Bin 3746: 638 of cap free
Amount of items: 2
Items: 
Size: 786816 Color: 0
Size: 212547 Color: 1

Bin 3747: 639 of cap free
Amount of items: 2
Items: 
Size: 680054 Color: 0
Size: 319308 Color: 1

Bin 3748: 639 of cap free
Amount of items: 2
Items: 
Size: 771448 Color: 1
Size: 227914 Color: 0

Bin 3749: 640 of cap free
Amount of items: 2
Items: 
Size: 553373 Color: 1
Size: 445988 Color: 0

Bin 3750: 641 of cap free
Amount of items: 2
Items: 
Size: 563679 Color: 1
Size: 435681 Color: 0

Bin 3751: 644 of cap free
Amount of items: 2
Items: 
Size: 646492 Color: 1
Size: 352865 Color: 0

Bin 3752: 644 of cap free
Amount of items: 2
Items: 
Size: 763713 Color: 0
Size: 235644 Color: 1

Bin 3753: 646 of cap free
Amount of items: 2
Items: 
Size: 774818 Color: 1
Size: 224537 Color: 0

Bin 3754: 647 of cap free
Amount of items: 2
Items: 
Size: 547980 Color: 0
Size: 451374 Color: 1

Bin 3755: 648 of cap free
Amount of items: 2
Items: 
Size: 734896 Color: 0
Size: 264457 Color: 1

Bin 3756: 650 of cap free
Amount of items: 2
Items: 
Size: 634781 Color: 0
Size: 364570 Color: 1

Bin 3757: 650 of cap free
Amount of items: 2
Items: 
Size: 759177 Color: 1
Size: 240174 Color: 0

Bin 3758: 652 of cap free
Amount of items: 2
Items: 
Size: 527288 Color: 1
Size: 472061 Color: 0

Bin 3759: 653 of cap free
Amount of items: 2
Items: 
Size: 722582 Color: 0
Size: 276766 Color: 1

Bin 3760: 653 of cap free
Amount of items: 2
Items: 
Size: 746297 Color: 1
Size: 253051 Color: 0

Bin 3761: 655 of cap free
Amount of items: 2
Items: 
Size: 599271 Color: 0
Size: 400075 Color: 1

Bin 3762: 655 of cap free
Amount of items: 2
Items: 
Size: 612054 Color: 1
Size: 387292 Color: 0

Bin 3763: 655 of cap free
Amount of items: 2
Items: 
Size: 763521 Color: 1
Size: 235825 Color: 0

Bin 3764: 657 of cap free
Amount of items: 2
Items: 
Size: 502260 Color: 1
Size: 497084 Color: 0

Bin 3765: 657 of cap free
Amount of items: 2
Items: 
Size: 550070 Color: 1
Size: 449274 Color: 0

Bin 3766: 658 of cap free
Amount of items: 2
Items: 
Size: 605151 Color: 1
Size: 394192 Color: 0

Bin 3767: 660 of cap free
Amount of items: 2
Items: 
Size: 528320 Color: 1
Size: 471021 Color: 0

Bin 3768: 660 of cap free
Amount of items: 2
Items: 
Size: 723619 Color: 0
Size: 275722 Color: 1

Bin 3769: 663 of cap free
Amount of items: 2
Items: 
Size: 683282 Color: 0
Size: 316056 Color: 1

Bin 3770: 664 of cap free
Amount of items: 2
Items: 
Size: 662615 Color: 0
Size: 336722 Color: 1

Bin 3771: 665 of cap free
Amount of items: 2
Items: 
Size: 705843 Color: 1
Size: 293493 Color: 0

Bin 3772: 665 of cap free
Amount of items: 2
Items: 
Size: 714484 Color: 1
Size: 284852 Color: 0

Bin 3773: 666 of cap free
Amount of items: 2
Items: 
Size: 511122 Color: 0
Size: 488213 Color: 1

Bin 3774: 667 of cap free
Amount of items: 2
Items: 
Size: 573122 Color: 0
Size: 426212 Color: 1

Bin 3775: 670 of cap free
Amount of items: 2
Items: 
Size: 768332 Color: 1
Size: 230999 Color: 0

Bin 3776: 672 of cap free
Amount of items: 3
Items: 
Size: 384659 Color: 0
Size: 323437 Color: 1
Size: 291233 Color: 1

Bin 3777: 674 of cap free
Amount of items: 2
Items: 
Size: 536739 Color: 0
Size: 462588 Color: 1

Bin 3778: 674 of cap free
Amount of items: 2
Items: 
Size: 628100 Color: 0
Size: 371227 Color: 1

Bin 3779: 675 of cap free
Amount of items: 2
Items: 
Size: 786566 Color: 1
Size: 212760 Color: 0

Bin 3780: 681 of cap free
Amount of items: 2
Items: 
Size: 734167 Color: 0
Size: 265153 Color: 1

Bin 3781: 682 of cap free
Amount of items: 2
Items: 
Size: 774785 Color: 1
Size: 224534 Color: 0

Bin 3782: 683 of cap free
Amount of items: 2
Items: 
Size: 653403 Color: 0
Size: 345915 Color: 1

Bin 3783: 683 of cap free
Amount of items: 2
Items: 
Size: 744425 Color: 0
Size: 254893 Color: 1

Bin 3784: 683 of cap free
Amount of items: 2
Items: 
Size: 792952 Color: 1
Size: 206366 Color: 0

Bin 3785: 687 of cap free
Amount of items: 2
Items: 
Size: 669579 Color: 1
Size: 329735 Color: 0

Bin 3786: 687 of cap free
Amount of items: 2
Items: 
Size: 686309 Color: 0
Size: 313005 Color: 1

Bin 3787: 692 of cap free
Amount of items: 2
Items: 
Size: 706130 Color: 0
Size: 293179 Color: 1

Bin 3788: 692 of cap free
Amount of items: 2
Items: 
Size: 771673 Color: 0
Size: 227636 Color: 1

Bin 3789: 693 of cap free
Amount of items: 2
Items: 
Size: 742212 Color: 1
Size: 257096 Color: 0

Bin 3790: 694 of cap free
Amount of items: 2
Items: 
Size: 527425 Color: 0
Size: 471882 Color: 1

Bin 3791: 695 of cap free
Amount of items: 2
Items: 
Size: 583537 Color: 0
Size: 415769 Color: 1

Bin 3792: 695 of cap free
Amount of items: 2
Items: 
Size: 647384 Color: 1
Size: 351922 Color: 0

Bin 3793: 697 of cap free
Amount of items: 2
Items: 
Size: 739004 Color: 1
Size: 260300 Color: 0

Bin 3794: 698 of cap free
Amount of items: 2
Items: 
Size: 590597 Color: 0
Size: 408706 Color: 1

Bin 3795: 701 of cap free
Amount of items: 2
Items: 
Size: 565897 Color: 1
Size: 433403 Color: 0

Bin 3796: 701 of cap free
Amount of items: 2
Items: 
Size: 750210 Color: 1
Size: 249090 Color: 0

Bin 3797: 703 of cap free
Amount of items: 2
Items: 
Size: 693613 Color: 1
Size: 305685 Color: 0

Bin 3798: 704 of cap free
Amount of items: 2
Items: 
Size: 765657 Color: 0
Size: 233640 Color: 1

Bin 3799: 705 of cap free
Amount of items: 2
Items: 
Size: 515159 Color: 0
Size: 484137 Color: 1

Bin 3800: 705 of cap free
Amount of items: 2
Items: 
Size: 647939 Color: 0
Size: 351357 Color: 1

Bin 3801: 706 of cap free
Amount of items: 2
Items: 
Size: 656388 Color: 0
Size: 342907 Color: 1

Bin 3802: 710 of cap free
Amount of items: 2
Items: 
Size: 547942 Color: 1
Size: 451349 Color: 0

Bin 3803: 710 of cap free
Amount of items: 2
Items: 
Size: 698836 Color: 1
Size: 300455 Color: 0

Bin 3804: 711 of cap free
Amount of items: 2
Items: 
Size: 687628 Color: 1
Size: 311662 Color: 0

Bin 3805: 714 of cap free
Amount of items: 2
Items: 
Size: 557247 Color: 0
Size: 442040 Color: 1

Bin 3806: 716 of cap free
Amount of items: 2
Items: 
Size: 623928 Color: 0
Size: 375357 Color: 1

Bin 3807: 716 of cap free
Amount of items: 2
Items: 
Size: 740311 Color: 1
Size: 258974 Color: 0

Bin 3808: 718 of cap free
Amount of items: 2
Items: 
Size: 790903 Color: 0
Size: 208380 Color: 1

Bin 3809: 721 of cap free
Amount of items: 2
Items: 
Size: 618493 Color: 1
Size: 380787 Color: 0

Bin 3810: 722 of cap free
Amount of items: 2
Items: 
Size: 793567 Color: 0
Size: 205712 Color: 1

Bin 3811: 725 of cap free
Amount of items: 2
Items: 
Size: 617844 Color: 0
Size: 381432 Color: 1

Bin 3812: 726 of cap free
Amount of items: 2
Items: 
Size: 570794 Color: 1
Size: 428481 Color: 0

Bin 3813: 726 of cap free
Amount of items: 2
Items: 
Size: 679994 Color: 0
Size: 319281 Color: 1

Bin 3814: 727 of cap free
Amount of items: 2
Items: 
Size: 595858 Color: 0
Size: 403416 Color: 1

Bin 3815: 729 of cap free
Amount of items: 2
Items: 
Size: 710813 Color: 0
Size: 288459 Color: 1

Bin 3816: 730 of cap free
Amount of items: 2
Items: 
Size: 703008 Color: 0
Size: 296263 Color: 1

Bin 3817: 730 of cap free
Amount of items: 2
Items: 
Size: 742345 Color: 0
Size: 256926 Color: 1

Bin 3818: 730 of cap free
Amount of items: 2
Items: 
Size: 795477 Color: 1
Size: 203794 Color: 0

Bin 3819: 731 of cap free
Amount of items: 2
Items: 
Size: 515137 Color: 0
Size: 484133 Color: 1

Bin 3820: 732 of cap free
Amount of items: 2
Items: 
Size: 523261 Color: 0
Size: 476008 Color: 1

Bin 3821: 733 of cap free
Amount of items: 2
Items: 
Size: 750455 Color: 0
Size: 248813 Color: 1

Bin 3822: 734 of cap free
Amount of items: 2
Items: 
Size: 530362 Color: 0
Size: 468905 Color: 1

Bin 3823: 735 of cap free
Amount of items: 2
Items: 
Size: 563599 Color: 0
Size: 435667 Color: 1

Bin 3824: 735 of cap free
Amount of items: 2
Items: 
Size: 796465 Color: 1
Size: 202801 Color: 0

Bin 3825: 737 of cap free
Amount of items: 2
Items: 
Size: 551829 Color: 1
Size: 447435 Color: 0

Bin 3826: 738 of cap free
Amount of items: 2
Items: 
Size: 590584 Color: 0
Size: 408679 Color: 1

Bin 3827: 741 of cap free
Amount of items: 2
Items: 
Size: 577030 Color: 1
Size: 422230 Color: 0

Bin 3828: 743 of cap free
Amount of items: 2
Items: 
Size: 580689 Color: 1
Size: 418569 Color: 0

Bin 3829: 744 of cap free
Amount of items: 2
Items: 
Size: 519167 Color: 0
Size: 480090 Color: 1

Bin 3830: 744 of cap free
Amount of items: 2
Items: 
Size: 661777 Color: 1
Size: 337480 Color: 0

Bin 3831: 746 of cap free
Amount of items: 2
Items: 
Size: 759964 Color: 1
Size: 239291 Color: 0

Bin 3832: 746 of cap free
Amount of items: 2
Items: 
Size: 766715 Color: 0
Size: 232540 Color: 1

Bin 3833: 747 of cap free
Amount of items: 2
Items: 
Size: 599349 Color: 1
Size: 399905 Color: 0

Bin 3834: 749 of cap free
Amount of items: 2
Items: 
Size: 659525 Color: 0
Size: 339727 Color: 1

Bin 3835: 750 of cap free
Amount of items: 2
Items: 
Size: 724211 Color: 1
Size: 275040 Color: 0

Bin 3836: 755 of cap free
Amount of items: 2
Items: 
Size: 673918 Color: 1
Size: 325328 Color: 0

Bin 3837: 756 of cap free
Amount of items: 2
Items: 
Size: 707745 Color: 1
Size: 291500 Color: 0

Bin 3838: 756 of cap free
Amount of items: 2
Items: 
Size: 724211 Color: 1
Size: 275034 Color: 0

Bin 3839: 757 of cap free
Amount of items: 2
Items: 
Size: 517581 Color: 0
Size: 481663 Color: 1

Bin 3840: 758 of cap free
Amount of items: 2
Items: 
Size: 518197 Color: 1
Size: 481046 Color: 0

Bin 3841: 758 of cap free
Amount of items: 2
Items: 
Size: 585572 Color: 0
Size: 413671 Color: 1

Bin 3842: 759 of cap free
Amount of items: 2
Items: 
Size: 767667 Color: 0
Size: 231575 Color: 1

Bin 3843: 761 of cap free
Amount of items: 2
Items: 
Size: 544745 Color: 1
Size: 454495 Color: 0

Bin 3844: 761 of cap free
Amount of items: 2
Items: 
Size: 692677 Color: 0
Size: 306563 Color: 1

Bin 3845: 763 of cap free
Amount of items: 2
Items: 
Size: 717936 Color: 0
Size: 281302 Color: 1

Bin 3846: 763 of cap free
Amount of items: 2
Items: 
Size: 720352 Color: 1
Size: 278886 Color: 0

Bin 3847: 763 of cap free
Amount of items: 2
Items: 
Size: 742191 Color: 1
Size: 257047 Color: 0

Bin 3848: 764 of cap free
Amount of items: 2
Items: 
Size: 607111 Color: 0
Size: 392126 Color: 1

Bin 3849: 764 of cap free
Amount of items: 2
Items: 
Size: 685555 Color: 1
Size: 313682 Color: 0

Bin 3850: 764 of cap free
Amount of items: 2
Items: 
Size: 789815 Color: 1
Size: 209422 Color: 0

Bin 3851: 771 of cap free
Amount of items: 2
Items: 
Size: 600881 Color: 0
Size: 398349 Color: 1

Bin 3852: 772 of cap free
Amount of items: 2
Items: 
Size: 695467 Color: 1
Size: 303762 Color: 0

Bin 3853: 773 of cap free
Amount of items: 2
Items: 
Size: 745791 Color: 0
Size: 253437 Color: 1

Bin 3854: 773 of cap free
Amount of items: 2
Items: 
Size: 786493 Color: 1
Size: 212735 Color: 0

Bin 3855: 777 of cap free
Amount of items: 2
Items: 
Size: 713345 Color: 0
Size: 285879 Color: 1

Bin 3856: 778 of cap free
Amount of items: 2
Items: 
Size: 573235 Color: 1
Size: 425988 Color: 0

Bin 3857: 779 of cap free
Amount of items: 2
Items: 
Size: 704348 Color: 0
Size: 294874 Color: 1

Bin 3858: 781 of cap free
Amount of items: 2
Items: 
Size: 635475 Color: 1
Size: 363745 Color: 0

Bin 3859: 781 of cap free
Amount of items: 2
Items: 
Size: 774699 Color: 1
Size: 224521 Color: 0

Bin 3860: 785 of cap free
Amount of items: 2
Items: 
Size: 726514 Color: 1
Size: 272702 Color: 0

Bin 3861: 785 of cap free
Amount of items: 2
Items: 
Size: 771604 Color: 0
Size: 227612 Color: 1

Bin 3862: 787 of cap free
Amount of items: 2
Items: 
Size: 565400 Color: 0
Size: 433814 Color: 1

Bin 3863: 787 of cap free
Amount of items: 2
Items: 
Size: 668736 Color: 1
Size: 330478 Color: 0

Bin 3864: 787 of cap free
Amount of items: 2
Items: 
Size: 710776 Color: 0
Size: 288438 Color: 1

Bin 3865: 792 of cap free
Amount of items: 2
Items: 
Size: 562517 Color: 1
Size: 436692 Color: 0

Bin 3866: 794 of cap free
Amount of items: 2
Items: 
Size: 784095 Color: 1
Size: 215112 Color: 0

Bin 3867: 795 of cap free
Amount of items: 2
Items: 
Size: 610244 Color: 1
Size: 388962 Color: 0

Bin 3868: 798 of cap free
Amount of items: 2
Items: 
Size: 646490 Color: 1
Size: 352713 Color: 0

Bin 3869: 798 of cap free
Amount of items: 2
Items: 
Size: 753262 Color: 1
Size: 245941 Color: 0

Bin 3870: 806 of cap free
Amount of items: 2
Items: 
Size: 725094 Color: 1
Size: 274101 Color: 0

Bin 3871: 806 of cap free
Amount of items: 2
Items: 
Size: 789786 Color: 1
Size: 209409 Color: 0

Bin 3872: 807 of cap free
Amount of items: 2
Items: 
Size: 695928 Color: 0
Size: 303266 Color: 1

Bin 3873: 807 of cap free
Amount of items: 2
Items: 
Size: 700266 Color: 1
Size: 298928 Color: 0

Bin 3874: 808 of cap free
Amount of items: 2
Items: 
Size: 593601 Color: 1
Size: 405592 Color: 0

Bin 3875: 809 of cap free
Amount of items: 2
Items: 
Size: 605107 Color: 1
Size: 394085 Color: 0

Bin 3876: 810 of cap free
Amount of items: 2
Items: 
Size: 778920 Color: 1
Size: 220271 Color: 0

Bin 3877: 812 of cap free
Amount of items: 2
Items: 
Size: 527405 Color: 0
Size: 471784 Color: 1

Bin 3878: 812 of cap free
Amount of items: 2
Items: 
Size: 557173 Color: 0
Size: 442016 Color: 1

Bin 3879: 814 of cap free
Amount of items: 2
Items: 
Size: 561764 Color: 0
Size: 437423 Color: 1

Bin 3880: 815 of cap free
Amount of items: 2
Items: 
Size: 577004 Color: 1
Size: 422182 Color: 0

Bin 3881: 815 of cap free
Amount of items: 2
Items: 
Size: 613870 Color: 1
Size: 385316 Color: 0

Bin 3882: 818 of cap free
Amount of items: 2
Items: 
Size: 628091 Color: 0
Size: 371092 Color: 1

Bin 3883: 825 of cap free
Amount of items: 2
Items: 
Size: 660395 Color: 1
Size: 338781 Color: 0

Bin 3884: 825 of cap free
Amount of items: 2
Items: 
Size: 753668 Color: 0
Size: 245508 Color: 1

Bin 3885: 831 of cap free
Amount of items: 2
Items: 
Size: 624646 Color: 0
Size: 374524 Color: 1

Bin 3886: 831 of cap free
Amount of items: 2
Items: 
Size: 669424 Color: 0
Size: 329746 Color: 1

Bin 3887: 833 of cap free
Amount of items: 2
Items: 
Size: 542941 Color: 1
Size: 456227 Color: 0

Bin 3888: 833 of cap free
Amount of items: 2
Items: 
Size: 578777 Color: 1
Size: 420391 Color: 0

Bin 3889: 835 of cap free
Amount of items: 2
Items: 
Size: 555781 Color: 1
Size: 443385 Color: 0

Bin 3890: 836 of cap free
Amount of items: 2
Items: 
Size: 536665 Color: 1
Size: 462500 Color: 0

Bin 3891: 837 of cap free
Amount of items: 2
Items: 
Size: 499788 Color: 1
Size: 499376 Color: 0

Bin 3892: 841 of cap free
Amount of items: 2
Items: 
Size: 739217 Color: 0
Size: 259943 Color: 1

Bin 3893: 848 of cap free
Amount of items: 2
Items: 
Size: 746294 Color: 1
Size: 252859 Color: 0

Bin 3894: 848 of cap free
Amount of items: 2
Items: 
Size: 794307 Color: 0
Size: 204846 Color: 1

Bin 3895: 851 of cap free
Amount of items: 2
Items: 
Size: 774049 Color: 0
Size: 225101 Color: 1

Bin 3896: 852 of cap free
Amount of items: 2
Items: 
Size: 675387 Color: 0
Size: 323762 Color: 1

Bin 3897: 853 of cap free
Amount of items: 2
Items: 
Size: 791241 Color: 1
Size: 207907 Color: 0

Bin 3898: 856 of cap free
Amount of items: 2
Items: 
Size: 511396 Color: 1
Size: 487749 Color: 0

Bin 3899: 856 of cap free
Amount of items: 2
Items: 
Size: 771573 Color: 0
Size: 227572 Color: 1

Bin 3900: 858 of cap free
Amount of items: 7
Items: 
Size: 153308 Color: 0
Size: 153236 Color: 0
Size: 153008 Color: 0
Size: 147868 Color: 1
Size: 147564 Color: 1
Size: 127907 Color: 0
Size: 116252 Color: 1

Bin 3901: 858 of cap free
Amount of items: 2
Items: 
Size: 683502 Color: 1
Size: 315641 Color: 0

Bin 3902: 862 of cap free
Amount of items: 2
Items: 
Size: 508199 Color: 1
Size: 490940 Color: 0

Bin 3903: 863 of cap free
Amount of items: 2
Items: 
Size: 788354 Color: 0
Size: 210784 Color: 1

Bin 3904: 865 of cap free
Amount of items: 2
Items: 
Size: 634395 Color: 1
Size: 364741 Color: 0

Bin 3905: 865 of cap free
Amount of items: 2
Items: 
Size: 667252 Color: 1
Size: 331884 Color: 0

Bin 3906: 867 of cap free
Amount of items: 2
Items: 
Size: 603978 Color: 0
Size: 395156 Color: 1

Bin 3907: 873 of cap free
Amount of items: 2
Items: 
Size: 511089 Color: 0
Size: 488039 Color: 1

Bin 3908: 873 of cap free
Amount of items: 2
Items: 
Size: 695408 Color: 1
Size: 303720 Color: 0

Bin 3909: 877 of cap free
Amount of items: 2
Items: 
Size: 752001 Color: 0
Size: 247123 Color: 1

Bin 3910: 878 of cap free
Amount of items: 2
Items: 
Size: 530225 Color: 0
Size: 468898 Color: 1

Bin 3911: 878 of cap free
Amount of items: 2
Items: 
Size: 591834 Color: 0
Size: 407289 Color: 1

Bin 3912: 879 of cap free
Amount of items: 2
Items: 
Size: 748333 Color: 0
Size: 250789 Color: 1

Bin 3913: 883 of cap free
Amount of items: 2
Items: 
Size: 615808 Color: 1
Size: 383310 Color: 0

Bin 3914: 883 of cap free
Amount of items: 2
Items: 
Size: 796416 Color: 1
Size: 202702 Color: 0

Bin 3915: 884 of cap free
Amount of items: 2
Items: 
Size: 705972 Color: 0
Size: 293145 Color: 1

Bin 3916: 886 of cap free
Amount of items: 2
Items: 
Size: 608360 Color: 0
Size: 390755 Color: 1

Bin 3917: 899 of cap free
Amount of items: 2
Items: 
Size: 694531 Color: 0
Size: 304571 Color: 1

Bin 3918: 899 of cap free
Amount of items: 3
Items: 
Size: 780564 Color: 0
Size: 110279 Color: 1
Size: 108259 Color: 0

Bin 3919: 904 of cap free
Amount of items: 2
Items: 
Size: 689922 Color: 1
Size: 309175 Color: 0

Bin 3920: 908 of cap free
Amount of items: 2
Items: 
Size: 533368 Color: 0
Size: 465725 Color: 1

Bin 3921: 909 of cap free
Amount of items: 2
Items: 
Size: 619797 Color: 1
Size: 379295 Color: 0

Bin 3922: 913 of cap free
Amount of items: 2
Items: 
Size: 656225 Color: 0
Size: 342863 Color: 1

Bin 3923: 916 of cap free
Amount of items: 2
Items: 
Size: 551677 Color: 0
Size: 447408 Color: 1

Bin 3924: 918 of cap free
Amount of items: 2
Items: 
Size: 679949 Color: 0
Size: 319134 Color: 1

Bin 3925: 921 of cap free
Amount of items: 2
Items: 
Size: 536589 Color: 1
Size: 462491 Color: 0

Bin 3926: 922 of cap free
Amount of items: 2
Items: 
Size: 500808 Color: 1
Size: 498271 Color: 0

Bin 3927: 923 of cap free
Amount of items: 2
Items: 
Size: 771167 Color: 1
Size: 227911 Color: 0

Bin 3928: 924 of cap free
Amount of items: 2
Items: 
Size: 771534 Color: 0
Size: 227543 Color: 1

Bin 3929: 927 of cap free
Amount of items: 2
Items: 
Size: 662561 Color: 0
Size: 336513 Color: 1

Bin 3930: 929 of cap free
Amount of items: 2
Items: 
Size: 619782 Color: 1
Size: 379290 Color: 0

Bin 3931: 931 of cap free
Amount of items: 2
Items: 
Size: 664819 Color: 1
Size: 334251 Color: 0

Bin 3932: 935 of cap free
Amount of items: 2
Items: 
Size: 766631 Color: 0
Size: 232435 Color: 1

Bin 3933: 937 of cap free
Amount of items: 2
Items: 
Size: 612172 Color: 0
Size: 386892 Color: 1

Bin 3934: 937 of cap free
Amount of items: 2
Items: 
Size: 633048 Color: 0
Size: 366016 Color: 1

Bin 3935: 938 of cap free
Amount of items: 2
Items: 
Size: 521718 Color: 1
Size: 477345 Color: 0

Bin 3936: 939 of cap free
Amount of items: 2
Items: 
Size: 573209 Color: 1
Size: 425853 Color: 0

Bin 3937: 940 of cap free
Amount of items: 2
Items: 
Size: 544588 Color: 1
Size: 454473 Color: 0

Bin 3938: 943 of cap free
Amount of items: 2
Items: 
Size: 679723 Color: 1
Size: 319335 Color: 0

Bin 3939: 944 of cap free
Amount of items: 2
Items: 
Size: 506158 Color: 0
Size: 492899 Color: 1

Bin 3940: 945 of cap free
Amount of items: 2
Items: 
Size: 639400 Color: 1
Size: 359656 Color: 0

Bin 3941: 945 of cap free
Amount of items: 2
Items: 
Size: 689093 Color: 0
Size: 309963 Color: 1

Bin 3942: 947 of cap free
Amount of items: 2
Items: 
Size: 587333 Color: 1
Size: 411721 Color: 0

Bin 3943: 950 of cap free
Amount of items: 2
Items: 
Size: 581810 Color: 1
Size: 417241 Color: 0

Bin 3944: 956 of cap free
Amount of items: 2
Items: 
Size: 615793 Color: 1
Size: 383252 Color: 0

Bin 3945: 958 of cap free
Amount of items: 2
Items: 
Size: 504269 Color: 1
Size: 494774 Color: 0

Bin 3946: 958 of cap free
Amount of items: 2
Items: 
Size: 535142 Color: 0
Size: 463901 Color: 1

Bin 3947: 959 of cap free
Amount of items: 2
Items: 
Size: 667327 Color: 0
Size: 331715 Color: 1

Bin 3948: 961 of cap free
Amount of items: 2
Items: 
Size: 539303 Color: 1
Size: 459737 Color: 0

Bin 3949: 961 of cap free
Amount of items: 2
Items: 
Size: 593570 Color: 1
Size: 405470 Color: 0

Bin 3950: 965 of cap free
Amount of items: 3
Items: 
Size: 358275 Color: 1
Size: 350202 Color: 1
Size: 290559 Color: 0

Bin 3951: 966 of cap free
Amount of items: 2
Items: 
Size: 795791 Color: 0
Size: 203244 Color: 1

Bin 3952: 969 of cap free
Amount of items: 2
Items: 
Size: 597254 Color: 1
Size: 401778 Color: 0

Bin 3953: 969 of cap free
Amount of items: 2
Items: 
Size: 759848 Color: 1
Size: 239184 Color: 0

Bin 3954: 970 of cap free
Amount of items: 2
Items: 
Size: 718523 Color: 1
Size: 280508 Color: 0

Bin 3955: 971 of cap free
Amount of items: 2
Items: 
Size: 517537 Color: 0
Size: 481493 Color: 1

Bin 3956: 974 of cap free
Amount of items: 2
Items: 
Size: 558931 Color: 0
Size: 440096 Color: 1

Bin 3957: 975 of cap free
Amount of items: 2
Items: 
Size: 660390 Color: 1
Size: 338636 Color: 0

Bin 3958: 975 of cap free
Amount of items: 2
Items: 
Size: 695815 Color: 0
Size: 303211 Color: 1

Bin 3959: 975 of cap free
Amount of items: 2
Items: 
Size: 744374 Color: 0
Size: 254652 Color: 1

Bin 3960: 979 of cap free
Amount of items: 2
Items: 
Size: 583766 Color: 1
Size: 415256 Color: 0

Bin 3961: 982 of cap free
Amount of items: 2
Items: 
Size: 508187 Color: 1
Size: 490832 Color: 0

Bin 3962: 989 of cap free
Amount of items: 2
Items: 
Size: 610054 Color: 1
Size: 388958 Color: 0

Bin 3963: 993 of cap free
Amount of items: 2
Items: 
Size: 683082 Color: 0
Size: 315926 Color: 1

Bin 3964: 999 of cap free
Amount of items: 2
Items: 
Size: 581808 Color: 1
Size: 417194 Color: 0

Bin 3965: 999 of cap free
Amount of items: 2
Items: 
Size: 642589 Color: 1
Size: 356413 Color: 0

Bin 3966: 1002 of cap free
Amount of items: 2
Items: 
Size: 506142 Color: 0
Size: 492857 Color: 1

Bin 3967: 1002 of cap free
Amount of items: 2
Items: 
Size: 612158 Color: 0
Size: 386841 Color: 1

Bin 3968: 1004 of cap free
Amount of items: 2
Items: 
Size: 661776 Color: 1
Size: 337221 Color: 0

Bin 3969: 1013 of cap free
Amount of items: 2
Items: 
Size: 766856 Color: 1
Size: 232132 Color: 0

Bin 3970: 1014 of cap free
Amount of items: 2
Items: 
Size: 578776 Color: 1
Size: 420211 Color: 0

Bin 3971: 1016 of cap free
Amount of items: 2
Items: 
Size: 504369 Color: 0
Size: 494616 Color: 1

Bin 3972: 1016 of cap free
Amount of items: 2
Items: 
Size: 555671 Color: 1
Size: 443314 Color: 0

Bin 3973: 1020 of cap free
Amount of items: 2
Items: 
Size: 751888 Color: 0
Size: 247093 Color: 1

Bin 3974: 1024 of cap free
Amount of items: 2
Items: 
Size: 558915 Color: 0
Size: 440062 Color: 1

Bin 3975: 1024 of cap free
Amount of items: 2
Items: 
Size: 664856 Color: 0
Size: 334121 Color: 1

Bin 3976: 1025 of cap free
Amount of items: 2
Items: 
Size: 695799 Color: 0
Size: 303177 Color: 1

Bin 3977: 1025 of cap free
Amount of items: 2
Items: 
Size: 781553 Color: 1
Size: 217423 Color: 0

Bin 3978: 1027 of cap free
Amount of items: 2
Items: 
Size: 740513 Color: 0
Size: 258461 Color: 1

Bin 3979: 1028 of cap free
Amount of items: 2
Items: 
Size: 565624 Color: 1
Size: 433349 Color: 0

Bin 3980: 1028 of cap free
Amount of items: 2
Items: 
Size: 755328 Color: 0
Size: 243645 Color: 1

Bin 3981: 1029 of cap free
Amount of items: 2
Items: 
Size: 608583 Color: 1
Size: 390389 Color: 0

Bin 3982: 1030 of cap free
Amount of items: 2
Items: 
Size: 559975 Color: 0
Size: 438996 Color: 1

Bin 3983: 1033 of cap free
Amount of items: 2
Items: 
Size: 554637 Color: 1
Size: 444331 Color: 0

Bin 3984: 1033 of cap free
Amount of items: 2
Items: 
Size: 659500 Color: 0
Size: 339468 Color: 1

Bin 3985: 1035 of cap free
Amount of items: 2
Items: 
Size: 694318 Color: 1
Size: 304648 Color: 0

Bin 3986: 1038 of cap free
Amount of items: 2
Items: 
Size: 526045 Color: 0
Size: 472918 Color: 1

Bin 3987: 1043 of cap free
Amount of items: 2
Items: 
Size: 628122 Color: 1
Size: 370836 Color: 0

Bin 3988: 1045 of cap free
Amount of items: 2
Items: 
Size: 592143 Color: 1
Size: 406813 Color: 0

Bin 3989: 1048 of cap free
Amount of items: 2
Items: 
Size: 518909 Color: 0
Size: 480044 Color: 1

Bin 3990: 1050 of cap free
Amount of items: 2
Items: 
Size: 722472 Color: 1
Size: 276479 Color: 0

Bin 3991: 1056 of cap free
Amount of items: 2
Items: 
Size: 683459 Color: 1
Size: 315486 Color: 0

Bin 3992: 1057 of cap free
Amount of items: 2
Items: 
Size: 683045 Color: 0
Size: 315899 Color: 1

Bin 3993: 1057 of cap free
Amount of items: 2
Items: 
Size: 705803 Color: 0
Size: 293141 Color: 1

Bin 3994: 1058 of cap free
Amount of items: 2
Items: 
Size: 662438 Color: 0
Size: 336505 Color: 1

Bin 3995: 1060 of cap free
Amount of items: 2
Items: 
Size: 661766 Color: 1
Size: 337175 Color: 0

Bin 3996: 1061 of cap free
Amount of items: 2
Items: 
Size: 619672 Color: 1
Size: 379268 Color: 0

Bin 3997: 1068 of cap free
Amount of items: 2
Items: 
Size: 625194 Color: 1
Size: 373739 Color: 0

Bin 3998: 1068 of cap free
Amount of items: 2
Items: 
Size: 791026 Color: 1
Size: 207907 Color: 0

Bin 3999: 1070 of cap free
Amount of items: 2
Items: 
Size: 634368 Color: 1
Size: 364563 Color: 0

Bin 4000: 1070 of cap free
Amount of items: 2
Items: 
Size: 778794 Color: 0
Size: 220137 Color: 1

Bin 4001: 1071 of cap free
Amount of items: 2
Items: 
Size: 515611 Color: 1
Size: 483319 Color: 0

Bin 4002: 1073 of cap free
Amount of items: 2
Items: 
Size: 704094 Color: 0
Size: 294834 Color: 1

Bin 4003: 1074 of cap free
Amount of items: 2
Items: 
Size: 698556 Color: 1
Size: 300371 Color: 0

Bin 4004: 1077 of cap free
Amount of items: 2
Items: 
Size: 683041 Color: 0
Size: 315883 Color: 1

Bin 4005: 1083 of cap free
Amount of items: 2
Items: 
Size: 674730 Color: 1
Size: 324188 Color: 0

Bin 4006: 1084 of cap free
Amount of items: 6
Items: 
Size: 178681 Color: 0
Size: 178412 Color: 0
Size: 174166 Color: 1
Size: 173625 Color: 1
Size: 173543 Color: 1
Size: 120490 Color: 0

Bin 4007: 1089 of cap free
Amount of items: 2
Items: 
Size: 601309 Color: 1
Size: 397603 Color: 0

Bin 4008: 1092 of cap free
Amount of items: 2
Items: 
Size: 612110 Color: 0
Size: 386799 Color: 1

Bin 4009: 1096 of cap free
Amount of items: 2
Items: 
Size: 535106 Color: 0
Size: 463799 Color: 1

Bin 4010: 1096 of cap free
Amount of items: 2
Items: 
Size: 755315 Color: 0
Size: 243590 Color: 1

Bin 4011: 1097 of cap free
Amount of items: 2
Items: 
Size: 786282 Color: 1
Size: 212622 Color: 0

Bin 4012: 1100 of cap free
Amount of items: 2
Items: 
Size: 533365 Color: 0
Size: 465536 Color: 1

Bin 4013: 1102 of cap free
Amount of items: 2
Items: 
Size: 664834 Color: 0
Size: 334065 Color: 1

Bin 4014: 1107 of cap free
Amount of items: 2
Items: 
Size: 748279 Color: 0
Size: 250615 Color: 1

Bin 4015: 1107 of cap free
Amount of items: 2
Items: 
Size: 753410 Color: 0
Size: 245484 Color: 1

Bin 4016: 1108 of cap free
Amount of items: 2
Items: 
Size: 728034 Color: 1
Size: 270859 Color: 0

Bin 4017: 1115 of cap free
Amount of items: 2
Items: 
Size: 608504 Color: 1
Size: 390382 Color: 0

Bin 4018: 1117 of cap free
Amount of items: 2
Items: 
Size: 504154 Color: 1
Size: 494730 Color: 0

Bin 4019: 1121 of cap free
Amount of items: 2
Items: 
Size: 508062 Color: 1
Size: 490818 Color: 0

Bin 4020: 1121 of cap free
Amount of items: 2
Items: 
Size: 616608 Color: 0
Size: 382272 Color: 1

Bin 4021: 1125 of cap free
Amount of items: 2
Items: 
Size: 506107 Color: 0
Size: 492769 Color: 1

Bin 4022: 1126 of cap free
Amount of items: 2
Items: 
Size: 663679 Color: 0
Size: 335196 Color: 1

Bin 4023: 1130 of cap free
Amount of items: 2
Items: 
Size: 581711 Color: 1
Size: 417160 Color: 0

Bin 4024: 1133 of cap free
Amount of items: 2
Items: 
Size: 789665 Color: 1
Size: 209203 Color: 0

Bin 4025: 1137 of cap free
Amount of items: 2
Items: 
Size: 720349 Color: 1
Size: 278515 Color: 0

Bin 4026: 1143 of cap free
Amount of items: 2
Items: 
Size: 640366 Color: 0
Size: 358492 Color: 1

Bin 4027: 1144 of cap free
Amount of items: 2
Items: 
Size: 689829 Color: 1
Size: 309028 Color: 0

Bin 4028: 1144 of cap free
Amount of items: 2
Items: 
Size: 784087 Color: 1
Size: 214770 Color: 0

Bin 4029: 1146 of cap free
Amount of items: 2
Items: 
Size: 595644 Color: 0
Size: 403211 Color: 1

Bin 4030: 1146 of cap free
Amount of items: 2
Items: 
Size: 729772 Color: 0
Size: 269083 Color: 1

Bin 4031: 1158 of cap free
Amount of items: 2
Items: 
Size: 636070 Color: 0
Size: 362773 Color: 1

Bin 4032: 1160 of cap free
Amount of items: 2
Items: 
Size: 705701 Color: 1
Size: 293140 Color: 0

Bin 4033: 1160 of cap free
Amount of items: 2
Items: 
Size: 736662 Color: 0
Size: 262179 Color: 1

Bin 4034: 1162 of cap free
Amount of items: 2
Items: 
Size: 795063 Color: 1
Size: 203776 Color: 0

Bin 4035: 1165 of cap free
Amount of items: 2
Items: 
Size: 568610 Color: 1
Size: 430226 Color: 0

Bin 4036: 1169 of cap free
Amount of items: 2
Items: 
Size: 540516 Color: 1
Size: 458316 Color: 0

Bin 4037: 1169 of cap free
Amount of items: 2
Items: 
Size: 771409 Color: 0
Size: 227423 Color: 1

Bin 4038: 1172 of cap free
Amount of items: 2
Items: 
Size: 588277 Color: 0
Size: 410552 Color: 1

Bin 4039: 1175 of cap free
Amount of items: 6
Items: 
Size: 173166 Color: 0
Size: 172878 Color: 0
Size: 169615 Color: 1
Size: 169443 Color: 1
Size: 169297 Color: 1
Size: 144427 Color: 0

Bin 4040: 1180 of cap free
Amount of items: 2
Items: 
Size: 585279 Color: 0
Size: 413542 Color: 1

Bin 4041: 1186 of cap free
Amount of items: 2
Items: 
Size: 728019 Color: 1
Size: 270796 Color: 0

Bin 4042: 1187 of cap free
Amount of items: 2
Items: 
Size: 590190 Color: 0
Size: 408624 Color: 1

Bin 4043: 1191 of cap free
Amount of items: 2
Items: 
Size: 694383 Color: 0
Size: 304427 Color: 1

Bin 4044: 1195 of cap free
Amount of items: 2
Items: 
Size: 681595 Color: 1
Size: 317211 Color: 0

Bin 4045: 1200 of cap free
Amount of items: 2
Items: 
Size: 559639 Color: 1
Size: 439162 Color: 0

Bin 4046: 1204 of cap free
Amount of items: 2
Items: 
Size: 553319 Color: 1
Size: 445478 Color: 0

Bin 4047: 1207 of cap free
Amount of items: 2
Items: 
Size: 570358 Color: 1
Size: 428436 Color: 0

Bin 4048: 1207 of cap free
Amount of items: 2
Items: 
Size: 798000 Color: 1
Size: 200794 Color: 0

Bin 4049: 1208 of cap free
Amount of items: 2
Items: 
Size: 756679 Color: 0
Size: 242114 Color: 1

Bin 4050: 1210 of cap free
Amount of items: 2
Items: 
Size: 695668 Color: 0
Size: 303123 Color: 1

Bin 4051: 1212 of cap free
Amount of items: 2
Items: 
Size: 756035 Color: 1
Size: 242754 Color: 0

Bin 4052: 1214 of cap free
Amount of items: 2
Items: 
Size: 618667 Color: 0
Size: 380120 Color: 1

Bin 4053: 1215 of cap free
Amount of items: 2
Items: 
Size: 549806 Color: 0
Size: 448980 Color: 1

Bin 4054: 1216 of cap free
Amount of items: 2
Items: 
Size: 703440 Color: 1
Size: 295345 Color: 0

Bin 4055: 1224 of cap free
Amount of items: 2
Items: 
Size: 551401 Color: 0
Size: 447376 Color: 1

Bin 4056: 1227 of cap free
Amount of items: 2
Items: 
Size: 687157 Color: 1
Size: 311617 Color: 0

Bin 4057: 1234 of cap free
Amount of items: 2
Items: 
Size: 609821 Color: 1
Size: 388946 Color: 0

Bin 4058: 1235 of cap free
Amount of items: 2
Items: 
Size: 707419 Color: 0
Size: 291347 Color: 1

Bin 4059: 1236 of cap free
Amount of items: 2
Items: 
Size: 598818 Color: 0
Size: 399947 Color: 1

Bin 4060: 1246 of cap free
Amount of items: 2
Items: 
Size: 744346 Color: 0
Size: 254409 Color: 1

Bin 4061: 1249 of cap free
Amount of items: 2
Items: 
Size: 588210 Color: 0
Size: 410542 Color: 1

Bin 4062: 1271 of cap free
Amount of items: 3
Items: 
Size: 384676 Color: 0
Size: 355929 Color: 1
Size: 258125 Color: 0

Bin 4063: 1271 of cap free
Amount of items: 2
Items: 
Size: 536318 Color: 1
Size: 462412 Color: 0

Bin 4064: 1273 of cap free
Amount of items: 2
Items: 
Size: 761624 Color: 1
Size: 237104 Color: 0

Bin 4065: 1274 of cap free
Amount of items: 2
Items: 
Size: 681358 Color: 0
Size: 317369 Color: 1

Bin 4066: 1274 of cap free
Amount of items: 2
Items: 
Size: 756642 Color: 0
Size: 242085 Color: 1

Bin 4067: 1275 of cap free
Amount of items: 2
Items: 
Size: 612083 Color: 0
Size: 386643 Color: 1

Bin 4068: 1276 of cap free
Amount of items: 2
Items: 
Size: 717795 Color: 0
Size: 280930 Color: 1

Bin 4069: 1277 of cap free
Amount of items: 2
Items: 
Size: 583484 Color: 0
Size: 415240 Color: 1

Bin 4070: 1278 of cap free
Amount of items: 2
Items: 
Size: 726180 Color: 1
Size: 272543 Color: 0

Bin 4071: 1280 of cap free
Amount of items: 2
Items: 
Size: 601271 Color: 1
Size: 397450 Color: 0

Bin 4072: 1280 of cap free
Amount of items: 2
Items: 
Size: 676526 Color: 1
Size: 322195 Color: 0

Bin 4073: 1281 of cap free
Amount of items: 2
Items: 
Size: 521712 Color: 1
Size: 477008 Color: 0

Bin 4074: 1283 of cap free
Amount of items: 2
Items: 
Size: 583693 Color: 1
Size: 415025 Color: 0

Bin 4075: 1283 of cap free
Amount of items: 2
Items: 
Size: 685256 Color: 1
Size: 313462 Color: 0

Bin 4076: 1290 of cap free
Amount of items: 2
Items: 
Size: 564959 Color: 0
Size: 433752 Color: 1

Bin 4077: 1292 of cap free
Amount of items: 2
Items: 
Size: 547372 Color: 1
Size: 451337 Color: 0

Bin 4078: 1292 of cap free
Amount of items: 2
Items: 
Size: 700337 Color: 0
Size: 298372 Color: 1

Bin 4079: 1292 of cap free
Amount of items: 2
Items: 
Size: 713333 Color: 0
Size: 285376 Color: 1

Bin 4080: 1296 of cap free
Amount of items: 2
Items: 
Size: 572862 Color: 1
Size: 425843 Color: 0

Bin 4081: 1297 of cap free
Amount of items: 2
Items: 
Size: 513170 Color: 0
Size: 485534 Color: 1

Bin 4082: 1298 of cap free
Amount of items: 2
Items: 
Size: 500743 Color: 1
Size: 497960 Color: 0

Bin 4083: 1299 of cap free
Amount of items: 2
Items: 
Size: 589063 Color: 1
Size: 409639 Color: 0

Bin 4084: 1300 of cap free
Amount of items: 2
Items: 
Size: 694316 Color: 0
Size: 304385 Color: 1

Bin 4085: 1301 of cap free
Amount of items: 2
Items: 
Size: 798837 Color: 0
Size: 199863 Color: 1

Bin 4086: 1302 of cap free
Amount of items: 2
Items: 
Size: 766714 Color: 1
Size: 231985 Color: 0

Bin 4087: 1306 of cap free
Amount of items: 2
Items: 
Size: 526638 Color: 1
Size: 472057 Color: 0

Bin 4088: 1313 of cap free
Amount of items: 2
Items: 
Size: 529995 Color: 1
Size: 468693 Color: 0

Bin 4089: 1313 of cap free
Amount of items: 2
Items: 
Size: 609745 Color: 1
Size: 388943 Color: 0

Bin 4090: 1324 of cap free
Amount of items: 2
Items: 
Size: 611441 Color: 1
Size: 387236 Color: 0

Bin 4091: 1324 of cap free
Amount of items: 2
Items: 
Size: 674716 Color: 1
Size: 323961 Color: 0

Bin 4092: 1325 of cap free
Amount of items: 2
Items: 
Size: 688715 Color: 0
Size: 309961 Color: 1

Bin 4093: 1325 of cap free
Amount of items: 2
Items: 
Size: 700309 Color: 0
Size: 298367 Color: 1

Bin 4094: 1326 of cap free
Amount of items: 2
Items: 
Size: 771264 Color: 0
Size: 227411 Color: 1

Bin 4095: 1336 of cap free
Amount of items: 2
Items: 
Size: 589029 Color: 1
Size: 409636 Color: 0

Bin 4096: 1342 of cap free
Amount of items: 2
Items: 
Size: 746683 Color: 0
Size: 251976 Color: 1

Bin 4097: 1343 of cap free
Amount of items: 2
Items: 
Size: 577221 Color: 0
Size: 421437 Color: 1

Bin 4098: 1344 of cap free
Amount of items: 2
Items: 
Size: 559637 Color: 1
Size: 439020 Color: 0

Bin 4099: 1344 of cap free
Amount of items: 2
Items: 
Size: 660138 Color: 1
Size: 338519 Color: 0

Bin 4100: 1349 of cap free
Amount of items: 2
Items: 
Size: 686221 Color: 0
Size: 312431 Color: 1

Bin 4101: 1349 of cap free
Amount of items: 2
Items: 
Size: 695623 Color: 0
Size: 303029 Color: 1

Bin 4102: 1359 of cap free
Amount of items: 2
Items: 
Size: 658190 Color: 1
Size: 340452 Color: 0

Bin 4103: 1363 of cap free
Amount of items: 2
Items: 
Size: 572808 Color: 1
Size: 425830 Color: 0

Bin 4104: 1365 of cap free
Amount of items: 2
Items: 
Size: 551363 Color: 0
Size: 447273 Color: 1

Bin 4105: 1366 of cap free
Amount of items: 2
Items: 
Size: 500680 Color: 1
Size: 497955 Color: 0

Bin 4106: 1368 of cap free
Amount of items: 2
Items: 
Size: 771263 Color: 0
Size: 227370 Color: 1

Bin 4107: 1373 of cap free
Amount of items: 2
Items: 
Size: 659467 Color: 0
Size: 339161 Color: 1

Bin 4108: 1374 of cap free
Amount of items: 2
Items: 
Size: 595441 Color: 0
Size: 403186 Color: 1

Bin 4109: 1377 of cap free
Amount of items: 2
Items: 
Size: 639191 Color: 1
Size: 359433 Color: 0

Bin 4110: 1385 of cap free
Amount of items: 2
Items: 
Size: 716439 Color: 1
Size: 282177 Color: 0

Bin 4111: 1389 of cap free
Amount of items: 2
Items: 
Size: 736622 Color: 0
Size: 261990 Color: 1

Bin 4112: 1390 of cap free
Amount of items: 2
Items: 
Size: 583404 Color: 0
Size: 415207 Color: 1

Bin 4113: 1396 of cap free
Amount of items: 2
Items: 
Size: 559618 Color: 1
Size: 438987 Color: 0

Bin 4114: 1402 of cap free
Amount of items: 2
Items: 
Size: 701597 Color: 1
Size: 297002 Color: 0

Bin 4115: 1405 of cap free
Amount of items: 3
Items: 
Size: 373144 Color: 0
Size: 323223 Color: 1
Size: 302229 Color: 1

Bin 4116: 1406 of cap free
Amount of items: 2
Items: 
Size: 621215 Color: 0
Size: 377380 Color: 1

Bin 4117: 1409 of cap free
Amount of items: 2
Items: 
Size: 526553 Color: 1
Size: 472039 Color: 0

Bin 4118: 1414 of cap free
Amount of items: 2
Items: 
Size: 753212 Color: 0
Size: 245375 Color: 1

Bin 4119: 1420 of cap free
Amount of items: 2
Items: 
Size: 784798 Color: 0
Size: 213783 Color: 1

Bin 4120: 1422 of cap free
Amount of items: 2
Items: 
Size: 740211 Color: 1
Size: 258368 Color: 0

Bin 4121: 1423 of cap free
Amount of items: 2
Items: 
Size: 797973 Color: 1
Size: 200605 Color: 0

Bin 4122: 1424 of cap free
Amount of items: 2
Items: 
Size: 635853 Color: 0
Size: 362724 Color: 1

Bin 4123: 1425 of cap free
Amount of items: 2
Items: 
Size: 542113 Color: 0
Size: 456463 Color: 1

Bin 4124: 1428 of cap free
Amount of items: 2
Items: 
Size: 577166 Color: 0
Size: 421407 Color: 1

Bin 4125: 1429 of cap free
Amount of items: 2
Items: 
Size: 794819 Color: 1
Size: 203753 Color: 0

Bin 4126: 1430 of cap free
Amount of items: 2
Items: 
Size: 625013 Color: 1
Size: 373558 Color: 0

Bin 4127: 1433 of cap free
Amount of items: 2
Items: 
Size: 783951 Color: 1
Size: 214617 Color: 0

Bin 4128: 1444 of cap free
Amount of items: 2
Items: 
Size: 642421 Color: 1
Size: 356136 Color: 0

Bin 4129: 1453 of cap free
Amount of items: 2
Items: 
Size: 720898 Color: 0
Size: 277650 Color: 1

Bin 4130: 1457 of cap free
Amount of items: 2
Items: 
Size: 559559 Color: 1
Size: 438985 Color: 0

Bin 4131: 1458 of cap free
Amount of items: 2
Items: 
Size: 769091 Color: 0
Size: 229452 Color: 1

Bin 4132: 1460 of cap free
Amount of items: 2
Items: 
Size: 632642 Color: 0
Size: 365899 Color: 1

Bin 4133: 1461 of cap free
Amount of items: 2
Items: 
Size: 689710 Color: 1
Size: 308830 Color: 0

Bin 4134: 1468 of cap free
Amount of items: 2
Items: 
Size: 585078 Color: 0
Size: 413455 Color: 1

Bin 4135: 1468 of cap free
Amount of items: 2
Items: 
Size: 650415 Color: 1
Size: 348118 Color: 0

Bin 4136: 1473 of cap free
Amount of items: 2
Items: 
Size: 517041 Color: 0
Size: 481487 Color: 1

Bin 4137: 1475 of cap free
Amount of items: 2
Items: 
Size: 761582 Color: 1
Size: 236944 Color: 0

Bin 4138: 1480 of cap free
Amount of items: 2
Items: 
Size: 789445 Color: 1
Size: 209076 Color: 0

Bin 4139: 1481 of cap free
Amount of items: 2
Items: 
Size: 660021 Color: 1
Size: 338499 Color: 0

Bin 4140: 1497 of cap free
Amount of items: 2
Items: 
Size: 638273 Color: 0
Size: 360231 Color: 1

Bin 4141: 1503 of cap free
Amount of items: 2
Items: 
Size: 536138 Color: 1
Size: 462360 Color: 0

Bin 4142: 1503 of cap free
Amount of items: 2
Items: 
Size: 771126 Color: 1
Size: 227372 Color: 0

Bin 4143: 1504 of cap free
Amount of items: 2
Items: 
Size: 660019 Color: 1
Size: 338478 Color: 0

Bin 4144: 1511 of cap free
Amount of items: 2
Items: 
Size: 718047 Color: 1
Size: 280443 Color: 0

Bin 4145: 1519 of cap free
Amount of items: 2
Items: 
Size: 585052 Color: 0
Size: 413430 Color: 1

Bin 4146: 1520 of cap free
Amount of items: 2
Items: 
Size: 604501 Color: 1
Size: 393980 Color: 0

Bin 4147: 1521 of cap free
Amount of items: 2
Items: 
Size: 749485 Color: 1
Size: 248995 Color: 0

Bin 4148: 1527 of cap free
Amount of items: 2
Items: 
Size: 573003 Color: 0
Size: 425471 Color: 1

Bin 4149: 1528 of cap free
Amount of items: 2
Items: 
Size: 591923 Color: 1
Size: 406550 Color: 0

Bin 4150: 1535 of cap free
Amount of items: 2
Items: 
Size: 798623 Color: 0
Size: 199843 Color: 1

Bin 4151: 1540 of cap free
Amount of items: 2
Items: 
Size: 503833 Color: 1
Size: 494628 Color: 0

Bin 4152: 1545 of cap free
Amount of items: 2
Items: 
Size: 542110 Color: 0
Size: 456346 Color: 1

Bin 4153: 1548 of cap free
Amount of items: 2
Items: 
Size: 533227 Color: 0
Size: 465226 Color: 1

Bin 4154: 1549 of cap free
Amount of items: 2
Items: 
Size: 707265 Color: 1
Size: 291187 Color: 0

Bin 4155: 1559 of cap free
Amount of items: 2
Items: 
Size: 568232 Color: 1
Size: 430210 Color: 0

Bin 4156: 1568 of cap free
Amount of items: 2
Items: 
Size: 698258 Color: 1
Size: 300175 Color: 0

Bin 4157: 1571 of cap free
Amount of items: 2
Items: 
Size: 655763 Color: 0
Size: 342667 Color: 1

Bin 4158: 1582 of cap free
Amount of items: 2
Items: 
Size: 531617 Color: 1
Size: 466802 Color: 0

Bin 4159: 1588 of cap free
Amount of items: 2
Items: 
Size: 547534 Color: 0
Size: 450879 Color: 1

Bin 4160: 1588 of cap free
Amount of items: 2
Items: 
Size: 716320 Color: 1
Size: 282093 Color: 0

Bin 4161: 1589 of cap free
Amount of items: 2
Items: 
Size: 600364 Color: 0
Size: 398048 Color: 1

Bin 4162: 1592 of cap free
Amount of items: 2
Items: 
Size: 572805 Color: 1
Size: 425604 Color: 0

Bin 4163: 1593 of cap free
Amount of items: 2
Items: 
Size: 720317 Color: 1
Size: 278091 Color: 0

Bin 4164: 1594 of cap free
Amount of items: 2
Items: 
Size: 645782 Color: 1
Size: 352625 Color: 0

Bin 4165: 1597 of cap free
Amount of items: 2
Items: 
Size: 589024 Color: 1
Size: 409380 Color: 0

Bin 4166: 1601 of cap free
Amount of items: 2
Items: 
Size: 687086 Color: 1
Size: 311314 Color: 0

Bin 4167: 1607 of cap free
Amount of items: 2
Items: 
Size: 736374 Color: 1
Size: 262020 Color: 0

Bin 4168: 1610 of cap free
Amount of items: 2
Items: 
Size: 793562 Color: 0
Size: 204829 Color: 1

Bin 4169: 1613 of cap free
Amount of items: 2
Items: 
Size: 507736 Color: 1
Size: 490652 Color: 0

Bin 4170: 1613 of cap free
Amount of items: 2
Items: 
Size: 760128 Color: 0
Size: 238260 Color: 1

Bin 4171: 1614 of cap free
Amount of items: 2
Items: 
Size: 576307 Color: 1
Size: 422080 Color: 0

Bin 4172: 1629 of cap free
Amount of items: 2
Items: 
Size: 551146 Color: 0
Size: 447226 Color: 1

Bin 4173: 1630 of cap free
Amount of items: 2
Items: 
Size: 503754 Color: 1
Size: 494617 Color: 0

Bin 4174: 1643 of cap free
Amount of items: 2
Items: 
Size: 783782 Color: 1
Size: 214576 Color: 0

Bin 4175: 1653 of cap free
Amount of items: 2
Items: 
Size: 729284 Color: 0
Size: 269064 Color: 1

Bin 4176: 1656 of cap free
Amount of items: 2
Items: 
Size: 547225 Color: 1
Size: 451120 Color: 0

Bin 4177: 1658 of cap free
Amount of items: 2
Items: 
Size: 578501 Color: 1
Size: 419842 Color: 0

Bin 4178: 1660 of cap free
Amount of items: 2
Items: 
Size: 746605 Color: 0
Size: 251736 Color: 1

Bin 4179: 1660 of cap free
Amount of items: 2
Items: 
Size: 781175 Color: 1
Size: 217166 Color: 0

Bin 4180: 1665 of cap free
Amount of items: 2
Items: 
Size: 668722 Color: 1
Size: 329614 Color: 0

Bin 4181: 1669 of cap free
Amount of items: 2
Items: 
Size: 691364 Color: 1
Size: 306968 Color: 0

Bin 4182: 1688 of cap free
Amount of items: 2
Items: 
Size: 565532 Color: 1
Size: 432781 Color: 0

Bin 4183: 1712 of cap free
Amount of items: 2
Items: 
Size: 650378 Color: 1
Size: 347911 Color: 0

Bin 4184: 1714 of cap free
Amount of items: 2
Items: 
Size: 729228 Color: 0
Size: 269059 Color: 1

Bin 4185: 1719 of cap free
Amount of items: 2
Items: 
Size: 793551 Color: 0
Size: 204731 Color: 1

Bin 4186: 1728 of cap free
Amount of items: 2
Items: 
Size: 681579 Color: 1
Size: 316694 Color: 0

Bin 4187: 1732 of cap free
Amount of items: 2
Items: 
Size: 635808 Color: 0
Size: 362461 Color: 1

Bin 4188: 1733 of cap free
Amount of items: 2
Items: 
Size: 547151 Color: 1
Size: 451117 Color: 0

Bin 4189: 1739 of cap free
Amount of items: 2
Items: 
Size: 611326 Color: 1
Size: 386936 Color: 0

Bin 4190: 1742 of cap free
Amount of items: 2
Items: 
Size: 681573 Color: 1
Size: 316686 Color: 0

Bin 4191: 1747 of cap free
Amount of items: 2
Items: 
Size: 752520 Color: 1
Size: 245734 Color: 0

Bin 4192: 1769 of cap free
Amount of items: 2
Items: 
Size: 533040 Color: 0
Size: 465192 Color: 1

Bin 4193: 1774 of cap free
Amount of items: 2
Items: 
Size: 577146 Color: 0
Size: 421081 Color: 1

Bin 4194: 1777 of cap free
Amount of items: 2
Items: 
Size: 572763 Color: 1
Size: 425461 Color: 0

Bin 4195: 1778 of cap free
Amount of items: 2
Items: 
Size: 547474 Color: 0
Size: 450749 Color: 1

Bin 4196: 1781 of cap free
Amount of items: 2
Items: 
Size: 507663 Color: 1
Size: 490557 Color: 0

Bin 4197: 1782 of cap free
Amount of items: 2
Items: 
Size: 667179 Color: 0
Size: 331040 Color: 1

Bin 4198: 1789 of cap free
Amount of items: 2
Items: 
Size: 647142 Color: 0
Size: 351070 Color: 1

Bin 4199: 1789 of cap free
Amount of items: 2
Items: 
Size: 705361 Color: 0
Size: 292851 Color: 1

Bin 4200: 1801 of cap free
Amount of items: 2
Items: 
Size: 504254 Color: 0
Size: 493946 Color: 1

Bin 4201: 1809 of cap free
Amount of items: 2
Items: 
Size: 547112 Color: 1
Size: 451080 Color: 0

Bin 4202: 1811 of cap free
Amount of items: 2
Items: 
Size: 568200 Color: 1
Size: 429990 Color: 0

Bin 4203: 1817 of cap free
Amount of items: 2
Items: 
Size: 713360 Color: 1
Size: 284824 Color: 0

Bin 4204: 1821 of cap free
Amount of items: 2
Items: 
Size: 583374 Color: 1
Size: 414806 Color: 0

Bin 4205: 1824 of cap free
Amount of items: 2
Items: 
Size: 720162 Color: 1
Size: 278015 Color: 0

Bin 4206: 1825 of cap free
Amount of items: 2
Items: 
Size: 586459 Color: 1
Size: 411717 Color: 0

Bin 4207: 1828 of cap free
Amount of items: 2
Items: 
Size: 793531 Color: 0
Size: 204642 Color: 1

Bin 4208: 1834 of cap free
Amount of items: 2
Items: 
Size: 517143 Color: 1
Size: 481024 Color: 0

Bin 4209: 1846 of cap free
Amount of items: 3
Items: 
Size: 373107 Color: 0
Size: 315314 Color: 1
Size: 309734 Color: 1

Bin 4210: 1848 of cap free
Amount of items: 2
Items: 
Size: 638096 Color: 0
Size: 360057 Color: 1

Bin 4211: 1851 of cap free
Amount of items: 2
Items: 
Size: 671216 Color: 0
Size: 326934 Color: 1

Bin 4212: 1861 of cap free
Amount of items: 2
Items: 
Size: 701462 Color: 1
Size: 296678 Color: 0

Bin 4213: 1871 of cap free
Amount of items: 2
Items: 
Size: 628015 Color: 0
Size: 370115 Color: 1

Bin 4214: 1871 of cap free
Amount of items: 2
Items: 
Size: 681470 Color: 1
Size: 316660 Color: 0

Bin 4215: 1873 of cap free
Amount of items: 2
Items: 
Size: 716129 Color: 1
Size: 281999 Color: 0

Bin 4216: 1881 of cap free
Amount of items: 2
Items: 
Size: 559212 Color: 1
Size: 438908 Color: 0

Bin 4217: 1885 of cap free
Amount of items: 2
Items: 
Size: 565363 Color: 1
Size: 432753 Color: 0

Bin 4218: 1885 of cap free
Amount of items: 2
Items: 
Size: 603624 Color: 0
Size: 394492 Color: 1

Bin 4219: 1905 of cap free
Amount of items: 2
Items: 
Size: 755783 Color: 1
Size: 242313 Color: 0

Bin 4220: 1916 of cap free
Amount of items: 2
Items: 
Size: 771208 Color: 0
Size: 226877 Color: 1

Bin 4221: 1919 of cap free
Amount of items: 2
Items: 
Size: 783542 Color: 1
Size: 214540 Color: 0

Bin 4222: 1925 of cap free
Amount of items: 2
Items: 
Size: 638094 Color: 0
Size: 359982 Color: 1

Bin 4223: 1926 of cap free
Amount of items: 2
Items: 
Size: 736495 Color: 0
Size: 261580 Color: 1

Bin 4224: 1937 of cap free
Amount of items: 2
Items: 
Size: 650313 Color: 1
Size: 347751 Color: 0

Bin 4225: 1937 of cap free
Amount of items: 2
Items: 
Size: 712742 Color: 0
Size: 285322 Color: 1

Bin 4226: 1941 of cap free
Amount of items: 2
Items: 
Size: 632401 Color: 0
Size: 365659 Color: 1

Bin 4227: 1942 of cap free
Amount of items: 2
Items: 
Size: 565342 Color: 1
Size: 432717 Color: 0

Bin 4228: 1948 of cap free
Amount of items: 2
Items: 
Size: 777832 Color: 1
Size: 220221 Color: 0

Bin 4229: 1956 of cap free
Amount of items: 3
Items: 
Size: 373284 Color: 0
Size: 363322 Color: 0
Size: 261439 Color: 1

Bin 4230: 1968 of cap free
Amount of items: 2
Items: 
Size: 612078 Color: 0
Size: 385955 Color: 1

Bin 4231: 1970 of cap free
Amount of items: 2
Items: 
Size: 620724 Color: 0
Size: 377307 Color: 1

Bin 4232: 1974 of cap free
Amount of items: 2
Items: 
Size: 568111 Color: 1
Size: 429916 Color: 0

Bin 4233: 1981 of cap free
Amount of items: 2
Items: 
Size: 716061 Color: 1
Size: 281959 Color: 0

Bin 4234: 1995 of cap free
Amount of items: 2
Items: 
Size: 510471 Color: 1
Size: 487535 Color: 0

Bin 4235: 2003 of cap free
Amount of items: 2
Items: 
Size: 572638 Color: 0
Size: 425360 Color: 1

Bin 4236: 2010 of cap free
Amount of items: 2
Items: 
Size: 619280 Color: 1
Size: 378711 Color: 0

Bin 4237: 2013 of cap free
Amount of items: 2
Items: 
Size: 627918 Color: 0
Size: 370070 Color: 1

Bin 4238: 2014 of cap free
Amount of items: 2
Items: 
Size: 657580 Color: 1
Size: 340407 Color: 0

Bin 4239: 2020 of cap free
Amount of items: 6
Items: 
Size: 167933 Color: 0
Size: 167517 Color: 0
Size: 167174 Color: 0
Size: 166748 Color: 1
Size: 164864 Color: 1
Size: 163745 Color: 1

Bin 4240: 2020 of cap free
Amount of items: 2
Items: 
Size: 778479 Color: 0
Size: 219502 Color: 1

Bin 4241: 2022 of cap free
Amount of items: 7
Items: 
Size: 147874 Color: 0
Size: 147687 Color: 0
Size: 147416 Color: 0
Size: 139321 Color: 1
Size: 139157 Color: 1
Size: 138993 Color: 1
Size: 137531 Color: 1

Bin 4242: 2040 of cap free
Amount of items: 2
Items: 
Size: 583158 Color: 1
Size: 414803 Color: 0

Bin 4243: 2046 of cap free
Amount of items: 2
Items: 
Size: 771119 Color: 0
Size: 226836 Color: 1

Bin 4244: 2049 of cap free
Amount of items: 2
Items: 
Size: 650229 Color: 1
Size: 347723 Color: 0

Bin 4245: 2055 of cap free
Amount of items: 2
Items: 
Size: 566836 Color: 0
Size: 431110 Color: 1

Bin 4246: 2065 of cap free
Amount of items: 2
Items: 
Size: 627872 Color: 0
Size: 370064 Color: 1

Bin 4247: 2072 of cap free
Amount of items: 2
Items: 
Size: 748967 Color: 1
Size: 248962 Color: 0

Bin 4248: 2077 of cap free
Amount of items: 2
Items: 
Size: 559187 Color: 1
Size: 438737 Color: 0

Bin 4249: 2092 of cap free
Amount of items: 2
Items: 
Size: 627858 Color: 0
Size: 370051 Color: 1

Bin 4250: 2094 of cap free
Amount of items: 2
Items: 
Size: 516450 Color: 0
Size: 481457 Color: 1

Bin 4251: 2102 of cap free
Amount of items: 2
Items: 
Size: 611993 Color: 0
Size: 385906 Color: 1

Bin 4252: 2119 of cap free
Amount of items: 2
Items: 
Size: 720159 Color: 1
Size: 277723 Color: 0

Bin 4253: 2125 of cap free
Amount of items: 2
Items: 
Size: 583154 Color: 1
Size: 414722 Color: 0

Bin 4254: 2137 of cap free
Amount of items: 2
Items: 
Size: 541740 Color: 0
Size: 456124 Color: 1

Bin 4255: 2143 of cap free
Amount of items: 2
Items: 
Size: 797971 Color: 1
Size: 199887 Color: 0

Bin 4256: 2146 of cap free
Amount of items: 2
Items: 
Size: 529648 Color: 0
Size: 468207 Color: 1

Bin 4257: 2148 of cap free
Amount of items: 2
Items: 
Size: 798504 Color: 0
Size: 199349 Color: 1

Bin 4258: 2149 of cap free
Amount of items: 2
Items: 
Size: 510392 Color: 1
Size: 487460 Color: 0

Bin 4259: 2163 of cap free
Amount of items: 2
Items: 
Size: 758803 Color: 1
Size: 239035 Color: 0

Bin 4260: 2167 of cap free
Amount of items: 2
Items: 
Size: 766478 Color: 0
Size: 231356 Color: 1

Bin 4261: 2169 of cap free
Amount of items: 2
Items: 
Size: 603905 Color: 1
Size: 393927 Color: 0

Bin 4262: 2179 of cap free
Amount of items: 2
Items: 
Size: 650134 Color: 1
Size: 347688 Color: 0

Bin 4263: 2182 of cap free
Amount of items: 2
Items: 
Size: 611923 Color: 0
Size: 385896 Color: 1

Bin 4264: 2186 of cap free
Amount of items: 2
Items: 
Size: 611199 Color: 1
Size: 386616 Color: 0

Bin 4265: 2211 of cap free
Amount of items: 2
Items: 
Size: 664221 Color: 1
Size: 333569 Color: 0

Bin 4266: 2213 of cap free
Amount of items: 2
Items: 
Size: 565240 Color: 1
Size: 432548 Color: 0

Bin 4267: 2216 of cap free
Amount of items: 2
Items: 
Size: 568026 Color: 1
Size: 429759 Color: 0

Bin 4268: 2217 of cap free
Amount of items: 2
Items: 
Size: 720147 Color: 1
Size: 277637 Color: 0

Bin 4269: 2218 of cap free
Amount of items: 2
Items: 
Size: 524947 Color: 0
Size: 472836 Color: 1

Bin 4270: 2218 of cap free
Amount of items: 2
Items: 
Size: 752052 Color: 1
Size: 245731 Color: 0

Bin 4271: 2237 of cap free
Amount of items: 2
Items: 
Size: 638010 Color: 0
Size: 359754 Color: 1

Bin 4272: 2249 of cap free
Amount of items: 2
Items: 
Size: 531528 Color: 1
Size: 466224 Color: 0

Bin 4273: 2261 of cap free
Amount of items: 2
Items: 
Size: 777624 Color: 1
Size: 220116 Color: 0

Bin 4274: 2265 of cap free
Amount of items: 2
Items: 
Size: 583041 Color: 1
Size: 414695 Color: 0

Bin 4275: 2272 of cap free
Amount of items: 2
Items: 
Size: 547319 Color: 0
Size: 450410 Color: 1

Bin 4276: 2276 of cap free
Amount of items: 2
Items: 
Size: 565207 Color: 1
Size: 432518 Color: 0

Bin 4277: 2313 of cap free
Amount of items: 2
Items: 
Size: 620718 Color: 0
Size: 376970 Color: 1

Bin 4278: 2315 of cap free
Amount of items: 2
Items: 
Size: 664154 Color: 1
Size: 333532 Color: 0

Bin 4279: 2316 of cap free
Amount of items: 2
Items: 
Size: 547305 Color: 0
Size: 450380 Color: 1

Bin 4280: 2326 of cap free
Amount of items: 2
Items: 
Size: 712856 Color: 1
Size: 284819 Color: 0

Bin 4281: 2328 of cap free
Amount of items: 2
Items: 
Size: 564361 Color: 0
Size: 433312 Color: 1

Bin 4282: 2333 of cap free
Amount of items: 2
Items: 
Size: 516712 Color: 1
Size: 480956 Color: 0

Bin 4283: 2346 of cap free
Amount of items: 2
Items: 
Size: 770968 Color: 0
Size: 226687 Color: 1

Bin 4284: 2370 of cap free
Amount of items: 2
Items: 
Size: 587154 Color: 0
Size: 410477 Color: 1

Bin 4285: 2375 of cap free
Amount of items: 2
Items: 
Size: 728924 Color: 0
Size: 268702 Color: 1

Bin 4286: 2377 of cap free
Amount of items: 3
Items: 
Size: 370024 Color: 0
Size: 326155 Color: 1
Size: 301445 Color: 1

Bin 4287: 2377 of cap free
Amount of items: 2
Items: 
Size: 766693 Color: 1
Size: 230931 Color: 0

Bin 4288: 2393 of cap free
Amount of items: 2
Items: 
Size: 566822 Color: 0
Size: 430786 Color: 1

Bin 4289: 2397 of cap free
Amount of items: 2
Items: 
Size: 673694 Color: 1
Size: 323910 Color: 0

Bin 4290: 2402 of cap free
Amount of items: 2
Items: 
Size: 755365 Color: 1
Size: 242234 Color: 0

Bin 4291: 2403 of cap free
Amount of items: 2
Items: 
Size: 657283 Color: 1
Size: 340315 Color: 0

Bin 4292: 2414 of cap free
Amount of items: 2
Items: 
Size: 783909 Color: 0
Size: 213678 Color: 1

Bin 4293: 2449 of cap free
Amount of items: 3
Items: 
Size: 348918 Color: 1
Size: 347299 Color: 0
Size: 301335 Color: 1

Bin 4294: 2449 of cap free
Amount of items: 2
Items: 
Size: 712826 Color: 1
Size: 284726 Color: 0

Bin 4295: 2456 of cap free
Amount of items: 2
Items: 
Size: 600303 Color: 1
Size: 397242 Color: 0

Bin 4296: 2461 of cap free
Amount of items: 2
Items: 
Size: 770819 Color: 1
Size: 226721 Color: 0

Bin 4297: 2480 of cap free
Amount of items: 2
Items: 
Size: 616110 Color: 0
Size: 381411 Color: 1

Bin 4298: 2483 of cap free
Amount of items: 2
Items: 
Size: 507104 Color: 1
Size: 490414 Color: 0

Bin 4299: 2489 of cap free
Amount of items: 2
Items: 
Size: 580529 Color: 1
Size: 416983 Color: 0

Bin 4300: 2501 of cap free
Amount of items: 2
Items: 
Size: 507098 Color: 1
Size: 490402 Color: 0

Bin 4301: 2504 of cap free
Amount of items: 2
Items: 
Size: 751845 Color: 1
Size: 245652 Color: 0

Bin 4302: 2511 of cap free
Amount of items: 2
Items: 
Size: 524751 Color: 0
Size: 472739 Color: 1

Bin 4303: 2513 of cap free
Amount of items: 2
Items: 
Size: 575536 Color: 1
Size: 421952 Color: 0

Bin 4304: 2523 of cap free
Amount of items: 2
Items: 
Size: 635472 Color: 0
Size: 362006 Color: 1

Bin 4305: 2526 of cap free
Amount of items: 2
Items: 
Size: 600241 Color: 1
Size: 397234 Color: 0

Bin 4306: 2527 of cap free
Amount of items: 2
Items: 
Size: 798170 Color: 0
Size: 199304 Color: 1

Bin 4307: 2528 of cap free
Amount of items: 2
Items: 
Size: 525676 Color: 1
Size: 471797 Color: 0

Bin 4308: 2537 of cap free
Amount of items: 2
Items: 
Size: 770860 Color: 0
Size: 226604 Color: 1

Bin 4309: 2541 of cap free
Amount of items: 2
Items: 
Size: 783793 Color: 0
Size: 213667 Color: 1

Bin 4310: 2545 of cap free
Amount of items: 2
Items: 
Size: 666495 Color: 0
Size: 330961 Color: 1

Bin 4311: 2548 of cap free
Amount of items: 2
Items: 
Size: 641444 Color: 1
Size: 356009 Color: 0

Bin 4312: 2559 of cap free
Amount of items: 2
Items: 
Size: 797624 Color: 1
Size: 199818 Color: 0

Bin 4313: 2560 of cap free
Amount of items: 2
Items: 
Size: 738993 Color: 0
Size: 258448 Color: 1

Bin 4314: 2576 of cap free
Amount of items: 2
Items: 
Size: 588567 Color: 1
Size: 408858 Color: 0

Bin 4315: 2579 of cap free
Amount of items: 2
Items: 
Size: 531318 Color: 1
Size: 466104 Color: 0

Bin 4316: 2580 of cap free
Amount of items: 2
Items: 
Size: 738987 Color: 0
Size: 258434 Color: 1

Bin 4317: 2606 of cap free
Amount of items: 2
Items: 
Size: 524671 Color: 0
Size: 472724 Color: 1

Bin 4318: 2615 of cap free
Amount of items: 2
Items: 
Size: 770749 Color: 1
Size: 226637 Color: 0

Bin 4319: 2632 of cap free
Amount of items: 2
Items: 
Size: 716845 Color: 0
Size: 280524 Color: 1

Bin 4320: 2632 of cap free
Amount of items: 2
Items: 
Size: 798067 Color: 0
Size: 199302 Color: 1

Bin 4321: 2633 of cap free
Amount of items: 2
Items: 
Size: 777415 Color: 1
Size: 219953 Color: 0

Bin 4322: 2640 of cap free
Amount of items: 2
Items: 
Size: 600063 Color: 0
Size: 397298 Color: 1

Bin 4323: 2656 of cap free
Amount of items: 2
Items: 
Size: 606657 Color: 0
Size: 390688 Color: 1

Bin 4324: 2663 of cap free
Amount of items: 2
Items: 
Size: 666466 Color: 0
Size: 330872 Color: 1

Bin 4325: 2691 of cap free
Amount of items: 2
Items: 
Size: 663982 Color: 1
Size: 333328 Color: 0

Bin 4326: 2704 of cap free
Amount of items: 2
Items: 
Size: 770699 Color: 1
Size: 226598 Color: 0

Bin 4327: 2720 of cap free
Amount of items: 2
Items: 
Size: 770699 Color: 1
Size: 226582 Color: 0

Bin 4328: 2743 of cap free
Amount of items: 2
Items: 
Size: 525563 Color: 1
Size: 471695 Color: 0

Bin 4329: 2759 of cap free
Amount of items: 2
Items: 
Size: 588479 Color: 1
Size: 408763 Color: 0

Bin 4330: 2770 of cap free
Amount of items: 2
Items: 
Size: 725060 Color: 1
Size: 272171 Color: 0

Bin 4331: 2770 of cap free
Amount of items: 2
Items: 
Size: 755236 Color: 0
Size: 241995 Color: 1

Bin 4332: 2772 of cap free
Amount of items: 2
Items: 
Size: 620589 Color: 0
Size: 376640 Color: 1

Bin 4333: 2780 of cap free
Amount of items: 2
Items: 
Size: 751620 Color: 1
Size: 245601 Color: 0

Bin 4334: 2831 of cap free
Amount of items: 2
Items: 
Size: 529187 Color: 0
Size: 467983 Color: 1

Bin 4335: 2831 of cap free
Amount of items: 2
Items: 
Size: 670647 Color: 0
Size: 326523 Color: 1

Bin 4336: 2831 of cap free
Amount of items: 2
Items: 
Size: 766451 Color: 1
Size: 230719 Color: 0

Bin 4337: 2837 of cap free
Amount of items: 2
Items: 
Size: 700015 Color: 0
Size: 297149 Color: 1

Bin 4338: 2842 of cap free
Amount of items: 2
Items: 
Size: 576560 Color: 0
Size: 420599 Color: 1

Bin 4339: 2844 of cap free
Amount of items: 2
Items: 
Size: 697306 Color: 1
Size: 299851 Color: 0

Bin 4340: 2847 of cap free
Amount of items: 2
Items: 
Size: 748700 Color: 1
Size: 248454 Color: 0

Bin 4341: 2866 of cap free
Amount of items: 2
Items: 
Size: 754956 Color: 1
Size: 242179 Color: 0

Bin 4342: 2877 of cap free
Amount of items: 2
Items: 
Size: 728695 Color: 0
Size: 268429 Color: 1

Bin 4343: 2882 of cap free
Amount of items: 2
Items: 
Size: 725011 Color: 1
Size: 272108 Color: 0

Bin 4344: 2885 of cap free
Amount of items: 2
Items: 
Size: 531316 Color: 1
Size: 465800 Color: 0

Bin 4345: 2886 of cap free
Amount of items: 2
Items: 
Size: 591717 Color: 1
Size: 405398 Color: 0

Bin 4346: 2893 of cap free
Amount of items: 2
Items: 
Size: 694144 Color: 0
Size: 302964 Color: 1

Bin 4347: 2935 of cap free
Amount of items: 2
Items: 
Size: 576491 Color: 0
Size: 420575 Color: 1

Bin 4348: 2936 of cap free
Amount of items: 2
Items: 
Size: 587108 Color: 0
Size: 409957 Color: 1

Bin 4349: 2950 of cap free
Amount of items: 2
Items: 
Size: 716808 Color: 0
Size: 280243 Color: 1

Bin 4350: 2980 of cap free
Amount of items: 2
Items: 
Size: 546474 Color: 1
Size: 450547 Color: 0

Bin 4351: 2991 of cap free
Amount of items: 2
Items: 
Size: 627027 Color: 0
Size: 369983 Color: 1

Bin 4352: 3008 of cap free
Amount of items: 2
Items: 
Size: 529113 Color: 0
Size: 467880 Color: 1

Bin 4353: 3010 of cap free
Amount of items: 2
Items: 
Size: 627009 Color: 0
Size: 369982 Color: 1

Bin 4354: 3044 of cap free
Amount of items: 2
Items: 
Size: 564707 Color: 1
Size: 432250 Color: 0

Bin 4355: 3053 of cap free
Amount of items: 2
Items: 
Size: 558230 Color: 0
Size: 438718 Color: 1

Bin 4356: 3062 of cap free
Amount of items: 3
Items: 
Size: 373124 Color: 0
Size: 362605 Color: 0
Size: 261210 Color: 1

Bin 4357: 3091 of cap free
Amount of items: 2
Items: 
Size: 794294 Color: 1
Size: 202616 Color: 0

Bin 4358: 3094 of cap free
Amount of items: 2
Items: 
Size: 576459 Color: 0
Size: 420448 Color: 1

Bin 4359: 3109 of cap free
Amount of items: 2
Items: 
Size: 564663 Color: 1
Size: 432229 Color: 0

Bin 4360: 3120 of cap free
Amount of items: 2
Items: 
Size: 633145 Color: 1
Size: 363736 Color: 0

Bin 4361: 3124 of cap free
Amount of items: 2
Items: 
Size: 712644 Color: 1
Size: 284233 Color: 0

Bin 4362: 3190 of cap free
Amount of items: 2
Items: 
Size: 529089 Color: 0
Size: 467722 Color: 1

Bin 4363: 3194 of cap free
Amount of items: 2
Items: 
Size: 782918 Color: 1
Size: 213889 Color: 0

Bin 4364: 3198 of cap free
Amount of items: 2
Items: 
Size: 610911 Color: 0
Size: 385892 Color: 1

Bin 4365: 3213 of cap free
Amount of items: 2
Items: 
Size: 633100 Color: 1
Size: 363688 Color: 0

Bin 4366: 3225 of cap free
Amount of items: 2
Items: 
Size: 529060 Color: 0
Size: 467716 Color: 1

Bin 4367: 3231 of cap free
Amount of items: 2
Items: 
Size: 607870 Color: 1
Size: 388900 Color: 0

Bin 4368: 3248 of cap free
Amount of items: 2
Items: 
Size: 794185 Color: 1
Size: 202568 Color: 0

Bin 4369: 3252 of cap free
Amount of items: 2
Items: 
Size: 701417 Color: 1
Size: 295332 Color: 0

Bin 4370: 3280 of cap free
Amount of items: 2
Items: 
Size: 770555 Color: 0
Size: 226166 Color: 1

Bin 4371: 3281 of cap free
Amount of items: 2
Items: 
Size: 645214 Color: 1
Size: 351506 Color: 0

Bin 4372: 3284 of cap free
Amount of items: 2
Items: 
Size: 634714 Color: 0
Size: 362003 Color: 1

Bin 4373: 3288 of cap free
Amount of items: 2
Items: 
Size: 627001 Color: 0
Size: 369712 Color: 1

Bin 4374: 3301 of cap free
Amount of items: 2
Items: 
Size: 610829 Color: 0
Size: 385871 Color: 1

Bin 4375: 3328 of cap free
Amount of items: 2
Items: 
Size: 546312 Color: 0
Size: 450361 Color: 1

Bin 4376: 3340 of cap free
Amount of items: 2
Items: 
Size: 754729 Color: 0
Size: 241932 Color: 1

Bin 4377: 3340 of cap free
Amount of items: 2
Items: 
Size: 794151 Color: 1
Size: 202510 Color: 0

Bin 4378: 3343 of cap free
Amount of items: 2
Items: 
Size: 663558 Color: 1
Size: 333100 Color: 0

Bin 4379: 3354 of cap free
Amount of items: 2
Items: 
Size: 538813 Color: 1
Size: 457834 Color: 0

Bin 4380: 3418 of cap free
Amount of items: 2
Items: 
Size: 546223 Color: 0
Size: 450360 Color: 1

Bin 4381: 3469 of cap free
Amount of items: 2
Items: 
Size: 632938 Color: 1
Size: 363594 Color: 0

Bin 4382: 3485 of cap free
Amount of items: 2
Items: 
Size: 504027 Color: 0
Size: 492489 Color: 1

Bin 4383: 3496 of cap free
Amount of items: 2
Items: 
Size: 525522 Color: 1
Size: 470983 Color: 0

Bin 4384: 3531 of cap free
Amount of items: 2
Items: 
Size: 728526 Color: 0
Size: 267944 Color: 1

Bin 4385: 3572 of cap free
Amount of items: 2
Items: 
Size: 516392 Color: 0
Size: 480037 Color: 1

Bin 4386: 3572 of cap free
Amount of items: 2
Items: 
Size: 610739 Color: 0
Size: 385690 Color: 1

Bin 4387: 3618 of cap free
Amount of items: 2
Items: 
Size: 583353 Color: 0
Size: 413030 Color: 1

Bin 4388: 3652 of cap free
Amount of items: 2
Items: 
Size: 524621 Color: 0
Size: 471728 Color: 1

Bin 4389: 3664 of cap free
Amount of items: 2
Items: 
Size: 701093 Color: 1
Size: 295244 Color: 0

Bin 4390: 3667 of cap free
Amount of items: 2
Items: 
Size: 516357 Color: 0
Size: 479977 Color: 1

Bin 4391: 3755 of cap free
Amount of items: 2
Items: 
Size: 553020 Color: 1
Size: 443226 Color: 0

Bin 4392: 3756 of cap free
Amount of items: 2
Items: 
Size: 715833 Color: 1
Size: 280412 Color: 0

Bin 4393: 3791 of cap free
Amount of items: 2
Items: 
Size: 524504 Color: 0
Size: 471706 Color: 1

Bin 4394: 3799 of cap free
Amount of items: 2
Items: 
Size: 553000 Color: 1
Size: 443202 Color: 0

Bin 4395: 3816 of cap free
Amount of items: 2
Items: 
Size: 701092 Color: 1
Size: 295093 Color: 0

Bin 4396: 3822 of cap free
Amount of items: 2
Items: 
Size: 516329 Color: 0
Size: 479850 Color: 1

Bin 4397: 3828 of cap free
Amount of items: 2
Items: 
Size: 591263 Color: 1
Size: 404910 Color: 0

Bin 4398: 3915 of cap free
Amount of items: 2
Items: 
Size: 583076 Color: 0
Size: 413010 Color: 1

Bin 4399: 3917 of cap free
Amount of items: 2
Items: 
Size: 719985 Color: 1
Size: 276099 Color: 0

Bin 4400: 3918 of cap free
Amount of items: 2
Items: 
Size: 552966 Color: 1
Size: 443117 Color: 0

Bin 4401: 3922 of cap free
Amount of items: 2
Items: 
Size: 575773 Color: 0
Size: 420306 Color: 1

Bin 4402: 3999 of cap free
Amount of items: 2
Items: 
Size: 516328 Color: 0
Size: 479674 Color: 1

Bin 4403: 4037 of cap free
Amount of items: 2
Items: 
Size: 610626 Color: 0
Size: 385338 Color: 1

Bin 4404: 4059 of cap free
Amount of items: 2
Items: 
Size: 715749 Color: 0
Size: 280193 Color: 1

Bin 4405: 4067 of cap free
Amount of items: 2
Items: 
Size: 552837 Color: 1
Size: 443097 Color: 0

Bin 4406: 4069 of cap free
Amount of items: 2
Items: 
Size: 525280 Color: 1
Size: 470652 Color: 0

Bin 4407: 4072 of cap free
Amount of items: 2
Items: 
Size: 719872 Color: 1
Size: 276057 Color: 0

Bin 4408: 4125 of cap free
Amount of items: 2
Items: 
Size: 719846 Color: 1
Size: 276030 Color: 0

Bin 4409: 4184 of cap free
Amount of items: 2
Items: 
Size: 559128 Color: 1
Size: 436689 Color: 0

Bin 4410: 4249 of cap free
Amount of items: 2
Items: 
Size: 545542 Color: 0
Size: 450210 Color: 1

Bin 4411: 4271 of cap free
Amount of items: 2
Items: 
Size: 724985 Color: 1
Size: 270745 Color: 0

Bin 4412: 4290 of cap free
Amount of items: 2
Items: 
Size: 537954 Color: 1
Size: 457757 Color: 0

Bin 4413: 4305 of cap free
Amount of items: 2
Items: 
Size: 662943 Color: 1
Size: 332753 Color: 0

Bin 4414: 4357 of cap free
Amount of items: 2
Items: 
Size: 591242 Color: 1
Size: 404402 Color: 0

Bin 4415: 4394 of cap free
Amount of items: 2
Items: 
Size: 552798 Color: 1
Size: 442809 Color: 0

Bin 4416: 4409 of cap free
Amount of items: 2
Items: 
Size: 529890 Color: 1
Size: 465702 Color: 0

Bin 4417: 4427 of cap free
Amount of items: 2
Items: 
Size: 558935 Color: 1
Size: 436639 Color: 0

Bin 4418: 4483 of cap free
Amount of items: 2
Items: 
Size: 537857 Color: 1
Size: 457661 Color: 0

Bin 4419: 4510 of cap free
Amount of items: 2
Items: 
Size: 669212 Color: 0
Size: 326279 Color: 1

Bin 4420: 4544 of cap free
Amount of items: 2
Items: 
Size: 525032 Color: 1
Size: 470425 Color: 0

Bin 4421: 4552 of cap free
Amount of items: 2
Items: 
Size: 669172 Color: 0
Size: 326277 Color: 1

Bin 4422: 4553 of cap free
Amount of items: 2
Items: 
Size: 503527 Color: 0
Size: 491921 Color: 1

Bin 4423: 4562 of cap free
Amount of items: 2
Items: 
Size: 745622 Color: 0
Size: 249817 Color: 1

Bin 4424: 4619 of cap free
Amount of items: 2
Items: 
Size: 575321 Color: 0
Size: 420061 Color: 1

Bin 4425: 4629 of cap free
Amount of items: 2
Items: 
Size: 525029 Color: 1
Size: 470343 Color: 0

Bin 4426: 4636 of cap free
Amount of items: 2
Items: 
Size: 552686 Color: 1
Size: 442679 Color: 0

Bin 4427: 4666 of cap free
Amount of items: 2
Items: 
Size: 503495 Color: 0
Size: 491840 Color: 1

Bin 4428: 4764 of cap free
Amount of items: 2
Items: 
Size: 524984 Color: 1
Size: 470253 Color: 0

Bin 4429: 4800 of cap free
Amount of items: 2
Items: 
Size: 552600 Color: 1
Size: 442601 Color: 0

Bin 4430: 4836 of cap free
Amount of items: 2
Items: 
Size: 609877 Color: 0
Size: 385288 Color: 1

Bin 4431: 4879 of cap free
Amount of items: 2
Items: 
Size: 524916 Color: 1
Size: 470206 Color: 0

Bin 4432: 4879 of cap free
Amount of items: 2
Items: 
Size: 558781 Color: 1
Size: 436341 Color: 0

Bin 4433: 4947 of cap free
Amount of items: 2
Items: 
Size: 609774 Color: 0
Size: 385280 Color: 1

Bin 4434: 5066 of cap free
Amount of items: 7
Items: 
Size: 147077 Color: 0
Size: 146850 Color: 0
Size: 146197 Color: 0
Size: 145743 Color: 0
Size: 136702 Color: 1
Size: 136321 Color: 1
Size: 136045 Color: 1

Bin 4435: 5132 of cap free
Amount of items: 2
Items: 
Size: 523208 Color: 0
Size: 471661 Color: 1

Bin 4436: 5302 of cap free
Amount of items: 2
Items: 
Size: 662920 Color: 1
Size: 331779 Color: 0

Bin 4437: 5345 of cap free
Amount of items: 2
Items: 
Size: 609692 Color: 0
Size: 384964 Color: 1

Bin 4438: 5506 of cap free
Amount of items: 2
Items: 
Size: 557120 Color: 0
Size: 437375 Color: 1

Bin 4439: 5507 of cap free
Amount of items: 2
Items: 
Size: 575175 Color: 0
Size: 419319 Color: 1

Bin 4440: 5565 of cap free
Amount of items: 2
Items: 
Size: 575119 Color: 0
Size: 419317 Color: 1

Bin 4441: 5650 of cap free
Amount of items: 2
Items: 
Size: 551779 Color: 1
Size: 442572 Color: 0

Bin 4442: 5665 of cap free
Amount of items: 2
Items: 
Size: 556974 Color: 0
Size: 437362 Color: 1

Bin 4443: 5702 of cap free
Amount of items: 2
Items: 
Size: 607863 Color: 1
Size: 386436 Color: 0

Bin 4444: 5769 of cap free
Amount of items: 2
Items: 
Size: 514284 Color: 1
Size: 479948 Color: 0

Bin 4445: 5847 of cap free
Amount of items: 2
Items: 
Size: 634423 Color: 0
Size: 359731 Color: 1

Bin 4446: 5913 of cap free
Amount of items: 2
Items: 
Size: 607835 Color: 1
Size: 386253 Color: 0

Bin 4447: 6082 of cap free
Amount of items: 2
Items: 
Size: 556940 Color: 0
Size: 436979 Color: 1

Bin 4448: 6270 of cap free
Amount of items: 2
Items: 
Size: 536086 Color: 1
Size: 457645 Color: 0

Bin 4449: 6283 of cap free
Amount of items: 2
Items: 
Size: 536082 Color: 1
Size: 457636 Color: 0

Bin 4450: 6328 of cap free
Amount of items: 5
Items: 
Size: 199564 Color: 0
Size: 199560 Color: 0
Size: 199181 Color: 0
Size: 198485 Color: 1
Size: 196883 Color: 1

Bin 4451: 6440 of cap free
Amount of items: 2
Items: 
Size: 551727 Color: 1
Size: 441834 Color: 0

Bin 4452: 6543 of cap free
Amount of items: 2
Items: 
Size: 535935 Color: 1
Size: 457523 Color: 0

Bin 4453: 6590 of cap free
Amount of items: 2
Items: 
Size: 535892 Color: 1
Size: 457519 Color: 0

Bin 4454: 6824 of cap free
Amount of items: 2
Items: 
Size: 551464 Color: 1
Size: 441713 Color: 0

Bin 4455: 6867 of cap free
Amount of items: 2
Items: 
Size: 502514 Color: 0
Size: 490620 Color: 1

Bin 4456: 6906 of cap free
Amount of items: 2
Items: 
Size: 556265 Color: 0
Size: 436830 Color: 1

Bin 4457: 6926 of cap free
Amount of items: 2
Items: 
Size: 578444 Color: 1
Size: 414631 Color: 0

Bin 4458: 6961 of cap free
Amount of items: 2
Items: 
Size: 513161 Color: 1
Size: 479879 Color: 0

Bin 4459: 7041 of cap free
Amount of items: 2
Items: 
Size: 551367 Color: 1
Size: 441593 Color: 0

Bin 4460: 7055 of cap free
Amount of items: 2
Items: 
Size: 607775 Color: 1
Size: 385171 Color: 0

Bin 4461: 7071 of cap free
Amount of items: 2
Items: 
Size: 502499 Color: 0
Size: 490431 Color: 1

Bin 4462: 7075 of cap free
Amount of items: 2
Items: 
Size: 535515 Color: 1
Size: 457411 Color: 0

Bin 4463: 7141 of cap free
Amount of items: 3
Items: 
Size: 348875 Color: 1
Size: 322825 Color: 1
Size: 321160 Color: 0

Bin 4464: 7187 of cap free
Amount of items: 2
Items: 
Size: 582858 Color: 0
Size: 409956 Color: 1

Bin 4465: 7257 of cap free
Amount of items: 2
Items: 
Size: 582828 Color: 0
Size: 409916 Color: 1

Bin 4466: 7469 of cap free
Amount of items: 2
Items: 
Size: 550970 Color: 1
Size: 441562 Color: 0

Bin 4467: 7485 of cap free
Amount of items: 2
Items: 
Size: 502476 Color: 0
Size: 490040 Color: 1

Bin 4468: 7577 of cap free
Amount of items: 2
Items: 
Size: 550970 Color: 1
Size: 441454 Color: 0

Bin 4469: 7778 of cap free
Amount of items: 2
Items: 
Size: 502213 Color: 0
Size: 490010 Color: 1

Bin 4470: 7926 of cap free
Amount of items: 2
Items: 
Size: 502194 Color: 0
Size: 489881 Color: 1

Bin 4471: 7962 of cap free
Amount of items: 2
Items: 
Size: 555490 Color: 0
Size: 436549 Color: 1

Bin 4472: 9124 of cap free
Amount of items: 2
Items: 
Size: 501959 Color: 0
Size: 488918 Color: 1

Bin 4473: 9405 of cap free
Amount of items: 6
Items: 
Size: 167147 Color: 0
Size: 166992 Color: 0
Size: 165965 Color: 0
Size: 163519 Color: 1
Size: 163509 Color: 1
Size: 163464 Color: 1

Bin 4474: 9568 of cap free
Amount of items: 2
Items: 
Size: 524752 Color: 1
Size: 465681 Color: 0

Bin 4475: 9742 of cap free
Amount of items: 2
Items: 
Size: 524653 Color: 1
Size: 465606 Color: 0

Bin 4476: 10038 of cap free
Amount of items: 3
Items: 
Size: 367288 Color: 0
Size: 331567 Color: 0
Size: 291108 Color: 1

Bin 4477: 11068 of cap free
Amount of items: 2
Items: 
Size: 778457 Color: 0
Size: 210476 Color: 1

Bin 4478: 11145 of cap free
Amount of items: 2
Items: 
Size: 778426 Color: 0
Size: 210430 Color: 1

Bin 4479: 11586 of cap free
Amount of items: 2
Items: 
Size: 501384 Color: 0
Size: 487031 Color: 1

Bin 4480: 11875 of cap free
Amount of items: 2
Items: 
Size: 778325 Color: 0
Size: 209801 Color: 1

Bin 4481: 12353 of cap free
Amount of items: 2
Items: 
Size: 778268 Color: 0
Size: 209380 Color: 1

Bin 4482: 12899 of cap free
Amount of items: 2
Items: 
Size: 777740 Color: 0
Size: 209362 Color: 1

Bin 4483: 13174 of cap free
Amount of items: 2
Items: 
Size: 777620 Color: 0
Size: 209207 Color: 1

Bin 4484: 13450 of cap free
Amount of items: 2
Items: 
Size: 501054 Color: 0
Size: 485497 Color: 1

Bin 4485: 13484 of cap free
Amount of items: 5
Items: 
Size: 199180 Color: 0
Size: 198638 Color: 0
Size: 196819 Color: 1
Size: 196003 Color: 1
Size: 195877 Color: 1

Bin 4486: 14229 of cap free
Amount of items: 2
Items: 
Size: 500892 Color: 0
Size: 484880 Color: 1

Bin 4487: 14331 of cap free
Amount of items: 2
Items: 
Size: 500799 Color: 0
Size: 484871 Color: 1

Bin 4488: 14716 of cap free
Amount of items: 2
Items: 
Size: 777521 Color: 0
Size: 207764 Color: 1

Bin 4489: 14897 of cap free
Amount of items: 5
Items: 
Size: 197905 Color: 0
Size: 197901 Color: 0
Size: 197887 Color: 0
Size: 195747 Color: 1
Size: 195664 Color: 1

Bin 4490: 15513 of cap free
Amount of items: 4
Items: 
Size: 273178 Color: 1
Size: 252612 Color: 0
Size: 251442 Color: 0
Size: 207256 Color: 1

Bin 4491: 15850 of cap free
Amount of items: 2
Items: 
Size: 776818 Color: 0
Size: 207333 Color: 1

Bin 4492: 17345 of cap free
Amount of items: 2
Items: 
Size: 775335 Color: 0
Size: 207321 Color: 1

Bin 4493: 17466 of cap free
Amount of items: 3
Items: 
Size: 345893 Color: 0
Size: 345560 Color: 0
Size: 291082 Color: 1

Bin 4494: 18224 of cap free
Amount of items: 6
Items: 
Size: 165905 Color: 0
Size: 164994 Color: 0
Size: 164970 Color: 0
Size: 162480 Color: 1
Size: 161924 Color: 1
Size: 161504 Color: 1

Bin 4495: 18351 of cap free
Amount of items: 5
Items: 
Size: 197813 Color: 0
Size: 197674 Color: 0
Size: 195566 Color: 1
Size: 195513 Color: 1
Size: 195084 Color: 1

Bin 4496: 19027 of cap free
Amount of items: 5
Items: 
Size: 197540 Color: 0
Size: 197257 Color: 0
Size: 197044 Color: 0
Size: 194958 Color: 1
Size: 194175 Color: 1

Bin 4497: 26124 of cap free
Amount of items: 5
Items: 
Size: 196541 Color: 0
Size: 196536 Color: 0
Size: 193954 Color: 1
Size: 193530 Color: 1
Size: 193316 Color: 1

Bin 4498: 26601 of cap free
Amount of items: 7
Items: 
Size: 144423 Color: 0
Size: 144373 Color: 0
Size: 143250 Color: 0
Size: 135913 Color: 1
Size: 135398 Color: 1
Size: 135349 Color: 1
Size: 134694 Color: 1

Bin 4499: 27049 of cap free
Amount of items: 5
Items: 
Size: 196506 Color: 0
Size: 196400 Color: 0
Size: 195776 Color: 0
Size: 193059 Color: 1
Size: 191211 Color: 1

Bin 4500: 29212 of cap free
Amount of items: 7
Items: 
Size: 142397 Color: 0
Size: 141912 Color: 0
Size: 141801 Color: 0
Size: 141787 Color: 0
Size: 134647 Color: 1
Size: 134151 Color: 1
Size: 134094 Color: 1

Bin 4501: 32326 of cap free
Amount of items: 6
Items: 
Size: 164358 Color: 0
Size: 162719 Color: 0
Size: 161271 Color: 1
Size: 160605 Color: 0
Size: 159493 Color: 1
Size: 159229 Color: 1

Bin 4502: 39468 of cap free
Amount of items: 5
Items: 
Size: 194891 Color: 0
Size: 193849 Color: 0
Size: 193774 Color: 0
Size: 189201 Color: 1
Size: 188818 Color: 1

Bin 4503: 39942 of cap free
Amount of items: 5
Items: 
Size: 195429 Color: 0
Size: 194968 Color: 0
Size: 190804 Color: 1
Size: 189591 Color: 1
Size: 189267 Color: 1

Bin 4504: 43871 of cap free
Amount of items: 3
Items: 
Size: 348220 Color: 1
Size: 347720 Color: 1
Size: 260190 Color: 0

Bin 4505: 44846 of cap free
Amount of items: 6
Items: 
Size: 160200 Color: 0
Size: 160032 Color: 0
Size: 160031 Color: 0
Size: 158372 Color: 1
Size: 158282 Color: 1
Size: 158238 Color: 1

Bin 4506: 48952 of cap free
Amount of items: 6
Items: 
Size: 159768 Color: 0
Size: 159498 Color: 0
Size: 159023 Color: 0
Size: 158055 Color: 1
Size: 157904 Color: 1
Size: 156801 Color: 1

Bin 4507: 49944 of cap free
Amount of items: 5
Items: 
Size: 193739 Color: 0
Size: 191472 Color: 0
Size: 188778 Color: 1
Size: 188392 Color: 1
Size: 187676 Color: 1

Bin 4508: 51238 of cap free
Amount of items: 7
Items: 
Size: 141750 Color: 0
Size: 141638 Color: 0
Size: 141406 Color: 0
Size: 134008 Color: 1
Size: 133290 Color: 1
Size: 128552 Color: 1
Size: 128119 Color: 1

Bin 4509: 55115 of cap free
Amount of items: 5
Items: 
Size: 190895 Color: 0
Size: 190448 Color: 0
Size: 189276 Color: 0
Size: 187450 Color: 1
Size: 186817 Color: 1

Bin 4510: 55660 of cap free
Amount of items: 6
Items: 
Size: 158852 Color: 0
Size: 158256 Color: 0
Size: 158162 Color: 0
Size: 156501 Color: 1
Size: 156477 Color: 1
Size: 156093 Color: 1

Bin 4511: 59448 of cap free
Amount of items: 6
Items: 
Size: 158159 Color: 0
Size: 158062 Color: 0
Size: 157458 Color: 0
Size: 155766 Color: 1
Size: 155629 Color: 1
Size: 155479 Color: 1

Bin 4512: 62328 of cap free
Amount of items: 5
Items: 
Size: 189245 Color: 0
Size: 188711 Color: 0
Size: 186777 Color: 1
Size: 186579 Color: 1
Size: 186361 Color: 1

Bin 4513: 64754 of cap free
Amount of items: 5
Items: 
Size: 188352 Color: 0
Size: 188003 Color: 0
Size: 187814 Color: 0
Size: 185669 Color: 1
Size: 185409 Color: 1

Bin 4514: 65751 of cap free
Amount of items: 7
Items: 
Size: 140378 Color: 0
Size: 138720 Color: 0
Size: 138645 Color: 0
Size: 136233 Color: 0
Size: 127366 Color: 1
Size: 127028 Color: 1
Size: 125880 Color: 1

Bin 4515: 69531 of cap free
Amount of items: 6
Items: 
Size: 156901 Color: 0
Size: 156714 Color: 0
Size: 156702 Color: 0
Size: 154431 Color: 1
Size: 152893 Color: 1
Size: 152829 Color: 1

Bin 4516: 70561 of cap free
Amount of items: 5
Items: 
Size: 187338 Color: 0
Size: 186855 Color: 0
Size: 186770 Color: 0
Size: 184277 Color: 1
Size: 184200 Color: 1

Bin 4517: 71110 of cap free
Amount of items: 5
Items: 
Size: 187522 Color: 0
Size: 187398 Color: 0
Size: 185127 Color: 1
Size: 184455 Color: 1
Size: 184389 Color: 1

Bin 4518: 74352 of cap free
Amount of items: 5
Items: 
Size: 186476 Color: 0
Size: 186362 Color: 0
Size: 186303 Color: 0
Size: 183303 Color: 1
Size: 183205 Color: 1

Bin 4519: 76012 of cap free
Amount of items: 5
Items: 
Size: 186626 Color: 0
Size: 186532 Color: 0
Size: 183761 Color: 1
Size: 183540 Color: 1
Size: 183530 Color: 1

Bin 4520: 79197 of cap free
Amount of items: 5
Items: 
Size: 186243 Color: 0
Size: 185936 Color: 0
Size: 183192 Color: 1
Size: 183079 Color: 1
Size: 182354 Color: 1

Bin 4521: 80974 of cap free
Amount of items: 5
Items: 
Size: 185697 Color: 0
Size: 185119 Color: 0
Size: 185012 Color: 0
Size: 182141 Color: 1
Size: 181058 Color: 1

Bin 4522: 90400 of cap free
Amount of items: 5
Items: 
Size: 184049 Color: 0
Size: 183749 Color: 0
Size: 183339 Color: 0
Size: 179321 Color: 1
Size: 179143 Color: 1

Bin 4523: 90850 of cap free
Amount of items: 5
Items: 
Size: 184478 Color: 0
Size: 184082 Color: 0
Size: 180553 Color: 1
Size: 180144 Color: 1
Size: 179894 Color: 1

Bin 4524: 97659 of cap free
Amount of items: 5
Items: 
Size: 182837 Color: 0
Size: 182478 Color: 0
Size: 179113 Color: 1
Size: 179067 Color: 1
Size: 178847 Color: 1

Bin 4525: 100432 of cap free
Amount of items: 5
Items: 
Size: 182272 Color: 0
Size: 182187 Color: 0
Size: 181057 Color: 0
Size: 177096 Color: 1
Size: 176957 Color: 1

Bin 4526: 101723 of cap free
Amount of items: 3
Items: 
Size: 322392 Color: 1
Size: 321510 Color: 1
Size: 254376 Color: 0

Bin 4527: 102498 of cap free
Amount of items: 7
Items: 
Size: 134580 Color: 0
Size: 132045 Color: 0
Size: 131460 Color: 0
Size: 125537 Color: 1
Size: 125455 Color: 1
Size: 124442 Color: 1
Size: 123984 Color: 1

Bin 4528: 107702 of cap free
Amount of items: 3
Items: 
Size: 319120 Color: 1
Size: 319110 Color: 1
Size: 254069 Color: 0

Bin 4529: 111331 of cap free
Amount of items: 5
Items: 
Size: 180963 Color: 0
Size: 180373 Color: 0
Size: 176127 Color: 1
Size: 175959 Color: 1
Size: 175248 Color: 1

Bin 4530: 113456 of cap free
Amount of items: 5
Items: 
Size: 179658 Color: 0
Size: 178810 Color: 0
Size: 178705 Color: 0
Size: 175027 Color: 1
Size: 174345 Color: 1

Bin 4531: 512969 of cap free
Amount of items: 4
Items: 
Size: 126261 Color: 0
Size: 121922 Color: 0
Size: 120078 Color: 1
Size: 118771 Color: 1

Total size: 4526052282
Total free space: 4952249

