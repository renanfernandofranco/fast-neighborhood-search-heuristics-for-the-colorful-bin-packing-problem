Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 315 Color: 1
Size: 267 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 288 Color: 1
Size: 251 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 358 Color: 1
Size: 264 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 277 Color: 1
Size: 263 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 346 Color: 1
Size: 284 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 322 Color: 1
Size: 251 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 268 Color: 1
Size: 257 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 263 Color: 1
Size: 254 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 334 Color: 1
Size: 309 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 268 Color: 1
Size: 261 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 284 Color: 1
Size: 280 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 315 Color: 1
Size: 288 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 321 Color: 1
Size: 271 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 289 Color: 1
Size: 268 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 268 Color: 1
Size: 251 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 290 Color: 1
Size: 261 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 290 Color: 1
Size: 250 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 1
Size: 402 Color: 1
Size: 252 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 317 Color: 1
Size: 259 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 354 Color: 1
Size: 264 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 266 Color: 1
Size: 251 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 269 Color: 1
Size: 255 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 351 Color: 1
Size: 281 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 316 Color: 1
Size: 312 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 304 Color: 1
Size: 285 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 297 Color: 1
Size: 285 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 364 Color: 1
Size: 257 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 289 Color: 1
Size: 257 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 1
Size: 251 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 336 Color: 1
Size: 289 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 280 Color: 1
Size: 258 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 360 Color: 1
Size: 271 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 273 Color: 1
Size: 250 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 267 Color: 0
Size: 286 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 301 Color: 1
Size: 269 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 326 Color: 1
Size: 276 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 301 Color: 1
Size: 270 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 309 Color: 1
Size: 296 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 305 Color: 1
Size: 284 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 325 Color: 1
Size: 269 Color: 0

Total size: 40000
Total free space: 0

