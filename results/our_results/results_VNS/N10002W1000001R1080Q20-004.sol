Capicity Bin: 1000001
Lower Bound: 4483

Bins used: 4492
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 371421 Color: 17
Size: 369386 Color: 0
Size: 259194 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 374975 Color: 13
Size: 369852 Color: 9
Size: 255174 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 375445 Color: 7
Size: 337993 Color: 6
Size: 286563 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 375464 Color: 7
Size: 354814 Color: 5
Size: 269723 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 376059 Color: 8
Size: 362608 Color: 14
Size: 261334 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 376067 Color: 15
Size: 363817 Color: 10
Size: 260117 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 375847 Color: 11
Size: 337705 Color: 14
Size: 286449 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 376916 Color: 19
Size: 362372 Color: 18
Size: 260713 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 376330 Color: 18
Size: 368354 Color: 4
Size: 255317 Color: 16

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 378712 Color: 13
Size: 345978 Color: 4
Size: 275311 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 378813 Color: 19
Size: 341563 Color: 6
Size: 279625 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 379248 Color: 4
Size: 361564 Color: 10
Size: 259189 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 380048 Color: 11
Size: 350884 Color: 8
Size: 269069 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 389221 Color: 7
Size: 350907 Color: 9
Size: 259873 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 391942 Color: 0
Size: 349077 Color: 7
Size: 258982 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 396371 Color: 18
Size: 324007 Color: 5
Size: 279623 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 396766 Color: 10
Size: 343479 Color: 3
Size: 259756 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 462600 Color: 16
Size: 278495 Color: 8
Size: 258906 Color: 5

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 505391 Color: 3
Size: 494610 Color: 19

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 515534 Color: 4
Size: 484467 Color: 12

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 526810 Color: 6
Size: 473191 Color: 16

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 533428 Color: 9
Size: 466573 Color: 4

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 537512 Color: 16
Size: 462489 Color: 19

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 538130 Color: 10
Size: 461871 Color: 18

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 549106 Color: 7
Size: 450895 Color: 9

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 550940 Color: 17
Size: 449061 Color: 9

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 551057 Color: 15
Size: 448944 Color: 17

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 554257 Color: 6
Size: 445744 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 558847 Color: 0
Size: 221198 Color: 5
Size: 219956 Color: 12

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 570946 Color: 14
Size: 429055 Color: 17

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 573519 Color: 17
Size: 426482 Color: 6

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 577725 Color: 16
Size: 422276 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 577947 Color: 18
Size: 224044 Color: 15
Size: 198010 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 578301 Color: 17
Size: 222806 Color: 7
Size: 198894 Color: 18

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 578548 Color: 9
Size: 421453 Color: 17

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 589476 Color: 19
Size: 410525 Color: 10

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 593458 Color: 15
Size: 406543 Color: 3

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 619861 Color: 9
Size: 380140 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 621654 Color: 14
Size: 197937 Color: 5
Size: 180410 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 623543 Color: 1
Size: 201906 Color: 4
Size: 174552 Color: 8

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 624516 Color: 8
Size: 195265 Color: 11
Size: 180220 Color: 9

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 626232 Color: 6
Size: 192570 Color: 15
Size: 181199 Color: 16

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 627812 Color: 16
Size: 192180 Color: 15
Size: 180009 Color: 11

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 629162 Color: 13
Size: 212579 Color: 11
Size: 158260 Color: 10

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 630503 Color: 8
Size: 198813 Color: 17
Size: 170685 Color: 18

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 630835 Color: 19
Size: 187568 Color: 0
Size: 181598 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 631109 Color: 13
Size: 185541 Color: 11
Size: 183351 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 632085 Color: 17
Size: 192787 Color: 18
Size: 175129 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 632743 Color: 17
Size: 194677 Color: 17
Size: 172581 Color: 10

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 632873 Color: 16
Size: 197402 Color: 10
Size: 169726 Color: 12

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 633243 Color: 8
Size: 197792 Color: 1
Size: 168966 Color: 1

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 633533 Color: 9
Size: 366468 Color: 17

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 633647 Color: 9
Size: 187067 Color: 17
Size: 179287 Color: 17

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 633671 Color: 5
Size: 195053 Color: 17
Size: 171277 Color: 9

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 634008 Color: 16
Size: 195187 Color: 7
Size: 170806 Color: 10

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 634375 Color: 9
Size: 189466 Color: 15
Size: 176160 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 634411 Color: 13
Size: 189631 Color: 13
Size: 175959 Color: 7

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 634735 Color: 17
Size: 183462 Color: 5
Size: 181804 Color: 6

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 634810 Color: 17
Size: 184095 Color: 1
Size: 181096 Color: 11

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 635004 Color: 2
Size: 182793 Color: 3
Size: 182204 Color: 12

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 635989 Color: 12
Size: 188628 Color: 6
Size: 175384 Color: 5

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 636011 Color: 0
Size: 192500 Color: 5
Size: 171490 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 636729 Color: 11
Size: 187949 Color: 12
Size: 175323 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 636967 Color: 2
Size: 186399 Color: 18
Size: 176635 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 636987 Color: 18
Size: 189026 Color: 15
Size: 173988 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 636981 Color: 2
Size: 189053 Color: 0
Size: 173967 Color: 11

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 637225 Color: 4
Size: 194995 Color: 1
Size: 167781 Color: 18

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 637299 Color: 0
Size: 198960 Color: 6
Size: 163742 Color: 19

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 637388 Color: 5
Size: 194381 Color: 4
Size: 168232 Color: 9

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 637413 Color: 13
Size: 189219 Color: 19
Size: 173369 Color: 14

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 637862 Color: 14
Size: 184143 Color: 5
Size: 177996 Color: 17

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 638182 Color: 7
Size: 198021 Color: 1
Size: 163798 Color: 5

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 638904 Color: 17
Size: 187718 Color: 13
Size: 173379 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 638935 Color: 9
Size: 192221 Color: 17
Size: 168845 Color: 16

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 639087 Color: 15
Size: 197716 Color: 9
Size: 163198 Color: 18

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 639272 Color: 2
Size: 194519 Color: 5
Size: 166210 Color: 16

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 639189 Color: 18
Size: 197621 Color: 8
Size: 163191 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 639436 Color: 0
Size: 180928 Color: 9
Size: 179637 Color: 19

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 639952 Color: 14
Size: 186884 Color: 0
Size: 173165 Color: 6

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 640047 Color: 13
Size: 180941 Color: 18
Size: 179013 Color: 18

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 640615 Color: 6
Size: 185757 Color: 17
Size: 173629 Color: 18

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 640628 Color: 7
Size: 186445 Color: 11
Size: 172928 Color: 16

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 640909 Color: 1
Size: 198280 Color: 3
Size: 160812 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 641004 Color: 14
Size: 185406 Color: 11
Size: 173591 Color: 6

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 641193 Color: 2
Size: 185361 Color: 0
Size: 173447 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 641074 Color: 16
Size: 190278 Color: 15
Size: 168649 Color: 19

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 641125 Color: 13
Size: 193829 Color: 12
Size: 165047 Color: 19

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 641364 Color: 9
Size: 180351 Color: 14
Size: 178286 Color: 19

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 641422 Color: 11
Size: 195701 Color: 17
Size: 162878 Color: 6

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 641805 Color: 6
Size: 193978 Color: 5
Size: 164218 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 642402 Color: 0
Size: 194776 Color: 3
Size: 162823 Color: 9

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 643093 Color: 13
Size: 190909 Color: 16
Size: 165999 Color: 11

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 643385 Color: 5
Size: 356616 Color: 19

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 643584 Color: 1
Size: 188667 Color: 18
Size: 167750 Color: 9

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 643937 Color: 14
Size: 179317 Color: 11
Size: 176747 Color: 10

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 644327 Color: 4
Size: 181475 Color: 17
Size: 174199 Color: 6

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 644350 Color: 1
Size: 178550 Color: 14
Size: 177101 Color: 16

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 644971 Color: 10
Size: 179602 Color: 5
Size: 175428 Color: 18

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 645493 Color: 17
Size: 181988 Color: 11
Size: 172520 Color: 8

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 645507 Color: 18
Size: 184457 Color: 8
Size: 170037 Color: 17

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 645847 Color: 11
Size: 190929 Color: 5
Size: 163225 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 646127 Color: 1
Size: 185175 Color: 6
Size: 168699 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 646391 Color: 17
Size: 193958 Color: 2
Size: 159652 Color: 9

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 646596 Color: 4
Size: 190681 Color: 1
Size: 162724 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 646908 Color: 16
Size: 189216 Color: 10
Size: 163877 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 646937 Color: 3
Size: 184333 Color: 8
Size: 168731 Color: 19

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 647301 Color: 1
Size: 185499 Color: 15
Size: 167201 Color: 14

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 647522 Color: 9
Size: 193986 Color: 12
Size: 158493 Color: 10

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 648116 Color: 0
Size: 176821 Color: 4
Size: 175064 Color: 11

Bin 110: 0 of cap free
Amount of items: 2
Items: 
Size: 648483 Color: 19
Size: 351518 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 648505 Color: 16
Size: 190459 Color: 17
Size: 161037 Color: 10

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 648785 Color: 7
Size: 182555 Color: 14
Size: 168661 Color: 8

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 649368 Color: 7
Size: 186855 Color: 1
Size: 163778 Color: 10

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 650086 Color: 9
Size: 188083 Color: 15
Size: 161832 Color: 17

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 650196 Color: 8
Size: 182569 Color: 9
Size: 167236 Color: 14

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 650314 Color: 8
Size: 183802 Color: 6
Size: 165885 Color: 12

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 650215 Color: 0
Size: 190196 Color: 18
Size: 159590 Color: 12

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 650940 Color: 17
Size: 180660 Color: 0
Size: 168401 Color: 11

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 650994 Color: 14
Size: 182945 Color: 8
Size: 166062 Color: 17

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 651422 Color: 7
Size: 181296 Color: 5
Size: 167283 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 651605 Color: 10
Size: 184670 Color: 2
Size: 163726 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 651986 Color: 2
Size: 177009 Color: 19
Size: 171006 Color: 10

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 652066 Color: 10
Size: 180339 Color: 14
Size: 167596 Color: 5

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 652120 Color: 7
Size: 177515 Color: 19
Size: 170366 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 652604 Color: 1
Size: 184776 Color: 7
Size: 162621 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 652608 Color: 5
Size: 181271 Color: 13
Size: 166122 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 653059 Color: 3
Size: 182626 Color: 19
Size: 164316 Color: 14

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 654946 Color: 13
Size: 182572 Color: 7
Size: 162483 Color: 16

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 655191 Color: 18
Size: 178475 Color: 11
Size: 166335 Color: 14

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 655221 Color: 5
Size: 176907 Color: 15
Size: 167873 Color: 12

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 655955 Color: 8
Size: 178978 Color: 14
Size: 165068 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 657876 Color: 8
Size: 174114 Color: 0
Size: 168011 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 658663 Color: 18
Size: 176909 Color: 1
Size: 164429 Color: 6

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 658663 Color: 17
Size: 183649 Color: 3
Size: 157689 Color: 16

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 658860 Color: 8
Size: 172506 Color: 9
Size: 168635 Color: 3

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 658951 Color: 11
Size: 172502 Color: 9
Size: 168548 Color: 12

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 659036 Color: 12
Size: 178972 Color: 2
Size: 161993 Color: 15

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 659331 Color: 11
Size: 176543 Color: 6
Size: 164127 Color: 16

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 659397 Color: 19
Size: 172104 Color: 0
Size: 168500 Color: 4

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 659693 Color: 10
Size: 177703 Color: 12
Size: 162605 Color: 15

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 660630 Color: 10
Size: 169709 Color: 11
Size: 169662 Color: 9

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 660628 Color: 13
Size: 180354 Color: 7
Size: 159019 Color: 4

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 661213 Color: 8
Size: 173434 Color: 12
Size: 165354 Color: 16

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 661235 Color: 3
Size: 172414 Color: 3
Size: 166352 Color: 18

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 661260 Color: 17
Size: 169846 Color: 2
Size: 168895 Color: 7

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 661739 Color: 6
Size: 170422 Color: 18
Size: 167840 Color: 11

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 662548 Color: 0
Size: 179231 Color: 6
Size: 158222 Color: 16

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 663078 Color: 12
Size: 171129 Color: 18
Size: 165794 Color: 18

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 663132 Color: 13
Size: 172338 Color: 14
Size: 164531 Color: 14

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 663499 Color: 18
Size: 174946 Color: 1
Size: 161556 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 663454 Color: 13
Size: 176680 Color: 12
Size: 159867 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 663984 Color: 8
Size: 178497 Color: 9
Size: 157520 Color: 7

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 665230 Color: 8
Size: 171668 Color: 11
Size: 163103 Color: 19

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 667765 Color: 13
Size: 174409 Color: 5
Size: 157827 Color: 9

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 668596 Color: 0
Size: 166862 Color: 8
Size: 164543 Color: 3

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 668617 Color: 16
Size: 165767 Color: 15
Size: 165617 Color: 15

Bin 157: 0 of cap free
Amount of items: 2
Items: 
Size: 671050 Color: 12
Size: 328951 Color: 4

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 671653 Color: 17
Size: 328348 Color: 3

Bin 159: 0 of cap free
Amount of items: 2
Items: 
Size: 676891 Color: 16
Size: 323110 Color: 11

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 678225 Color: 2
Size: 163640 Color: 4
Size: 158136 Color: 17

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 678846 Color: 9
Size: 164569 Color: 11
Size: 156586 Color: 18

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 679919 Color: 8
Size: 165342 Color: 19
Size: 154740 Color: 3

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 681221 Color: 17
Size: 162237 Color: 11
Size: 156543 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 681308 Color: 19
Size: 162150 Color: 6
Size: 156543 Color: 7

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 682321 Color: 16
Size: 163779 Color: 8
Size: 153901 Color: 16

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 684083 Color: 7
Size: 161712 Color: 4
Size: 154206 Color: 3

Bin 167: 0 of cap free
Amount of items: 2
Items: 
Size: 692707 Color: 10
Size: 307294 Color: 4

Bin 168: 0 of cap free
Amount of items: 2
Items: 
Size: 693166 Color: 0
Size: 306835 Color: 4

Bin 169: 0 of cap free
Amount of items: 2
Items: 
Size: 694891 Color: 6
Size: 305110 Color: 18

Bin 170: 0 of cap free
Amount of items: 2
Items: 
Size: 695813 Color: 12
Size: 304188 Color: 9

Bin 171: 0 of cap free
Amount of items: 2
Items: 
Size: 705190 Color: 14
Size: 294811 Color: 16

Bin 172: 0 of cap free
Amount of items: 2
Items: 
Size: 710505 Color: 18
Size: 289496 Color: 8

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 711308 Color: 18
Size: 147322 Color: 9
Size: 141371 Color: 18

Bin 174: 0 of cap free
Amount of items: 2
Items: 
Size: 711518 Color: 9
Size: 288483 Color: 3

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 715218 Color: 5
Size: 145037 Color: 17
Size: 139746 Color: 19

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 715241 Color: 2
Size: 147645 Color: 16
Size: 137115 Color: 10

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 715595 Color: 17
Size: 150649 Color: 15
Size: 133757 Color: 10

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 718913 Color: 12
Size: 142514 Color: 5
Size: 138574 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 719232 Color: 17
Size: 145631 Color: 12
Size: 135138 Color: 6

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 719902 Color: 10
Size: 140631 Color: 10
Size: 139468 Color: 17

Bin 181: 0 of cap free
Amount of items: 2
Items: 
Size: 720072 Color: 11
Size: 279929 Color: 9

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 720263 Color: 16
Size: 144013 Color: 15
Size: 135725 Color: 4

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 720332 Color: 12
Size: 139859 Color: 14
Size: 139810 Color: 11

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 720715 Color: 14
Size: 147328 Color: 2
Size: 131958 Color: 11

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 721319 Color: 19
Size: 145049 Color: 3
Size: 133633 Color: 10

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 722067 Color: 6
Size: 145780 Color: 3
Size: 132154 Color: 3

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 722797 Color: 6
Size: 146479 Color: 19
Size: 130725 Color: 10

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 723324 Color: 11
Size: 141062 Color: 11
Size: 135615 Color: 3

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 724533 Color: 1
Size: 151020 Color: 5
Size: 124448 Color: 17

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 725822 Color: 10
Size: 144348 Color: 4
Size: 129831 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 726219 Color: 18
Size: 143472 Color: 11
Size: 130310 Color: 18

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 727492 Color: 2
Size: 137761 Color: 15
Size: 134748 Color: 18

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 728154 Color: 5
Size: 149491 Color: 16
Size: 122356 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 728787 Color: 0
Size: 146681 Color: 6
Size: 124533 Color: 6

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 729584 Color: 9
Size: 144626 Color: 9
Size: 125791 Color: 14

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 729606 Color: 5
Size: 151130 Color: 10
Size: 119265 Color: 10

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 729675 Color: 5
Size: 137490 Color: 16
Size: 132836 Color: 11

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 730053 Color: 1
Size: 141037 Color: 14
Size: 128911 Color: 7

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 730278 Color: 6
Size: 148550 Color: 17
Size: 121173 Color: 17

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 730498 Color: 8
Size: 146054 Color: 13
Size: 123449 Color: 5

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 730371 Color: 5
Size: 150275 Color: 2
Size: 119355 Color: 17

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 731196 Color: 3
Size: 268805 Color: 18

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 732699 Color: 10
Size: 137983 Color: 19
Size: 129319 Color: 4

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 732756 Color: 19
Size: 139306 Color: 17
Size: 127939 Color: 4

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 732791 Color: 14
Size: 142449 Color: 6
Size: 124761 Color: 4

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 732840 Color: 14
Size: 142635 Color: 6
Size: 124526 Color: 6

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 732860 Color: 0
Size: 143917 Color: 1
Size: 123224 Color: 10

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 733211 Color: 8
Size: 141277 Color: 1
Size: 125513 Color: 14

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 733218 Color: 10
Size: 140727 Color: 0
Size: 126056 Color: 19

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 733734 Color: 16
Size: 142461 Color: 9
Size: 123806 Color: 14

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 733792 Color: 7
Size: 144542 Color: 13
Size: 121667 Color: 9

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 734961 Color: 2
Size: 143684 Color: 11
Size: 121356 Color: 5

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 734980 Color: 7
Size: 134381 Color: 1
Size: 130640 Color: 16

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 735754 Color: 8
Size: 143502 Color: 10
Size: 120745 Color: 19

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 735953 Color: 11
Size: 139906 Color: 4
Size: 124142 Color: 16

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 736426 Color: 6
Size: 139843 Color: 19
Size: 123732 Color: 7

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 737196 Color: 1
Size: 133999 Color: 5
Size: 128806 Color: 8

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 737566 Color: 7
Size: 141238 Color: 5
Size: 121197 Color: 12

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 738377 Color: 17
Size: 136875 Color: 1
Size: 124749 Color: 2

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 738823 Color: 11
Size: 140213 Color: 4
Size: 120965 Color: 11

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 739273 Color: 5
Size: 134986 Color: 7
Size: 125742 Color: 3

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 739507 Color: 9
Size: 137513 Color: 8
Size: 122981 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 741018 Color: 4
Size: 130734 Color: 15
Size: 128249 Color: 2

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 741026 Color: 15
Size: 138819 Color: 9
Size: 120156 Color: 11

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 741096 Color: 7
Size: 130722 Color: 17
Size: 128183 Color: 16

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 741209 Color: 1
Size: 137970 Color: 11
Size: 120822 Color: 18

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 741924 Color: 16
Size: 130907 Color: 12
Size: 127170 Color: 12

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 741931 Color: 10
Size: 136123 Color: 7
Size: 121947 Color: 6

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 742662 Color: 18
Size: 139250 Color: 10
Size: 118089 Color: 2

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 743250 Color: 3
Size: 128821 Color: 4
Size: 127930 Color: 10

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 743293 Color: 0
Size: 132043 Color: 11
Size: 124665 Color: 6

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 743442 Color: 19
Size: 128552 Color: 7
Size: 128007 Color: 19

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 743626 Color: 13
Size: 137121 Color: 19
Size: 119254 Color: 14

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 743735 Color: 7
Size: 135309 Color: 3
Size: 120957 Color: 2

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 744001 Color: 15
Size: 136408 Color: 17
Size: 119592 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 744003 Color: 18
Size: 131674 Color: 9
Size: 124324 Color: 16

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 744079 Color: 2
Size: 134538 Color: 11
Size: 121384 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 744152 Color: 17
Size: 134437 Color: 10
Size: 121412 Color: 9

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 744535 Color: 13
Size: 133070 Color: 6
Size: 122396 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 744190 Color: 12
Size: 138534 Color: 2
Size: 117277 Color: 8

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 744240 Color: 15
Size: 128826 Color: 17
Size: 126935 Color: 11

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 744351 Color: 9
Size: 136950 Color: 17
Size: 118700 Color: 9

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 744578 Color: 13
Size: 134441 Color: 6
Size: 120982 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 744661 Color: 3
Size: 128200 Color: 17
Size: 127140 Color: 9

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 744681 Color: 18
Size: 134915 Color: 6
Size: 120405 Color: 7

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 745065 Color: 11
Size: 135010 Color: 3
Size: 119926 Color: 14

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 745226 Color: 1
Size: 130791 Color: 14
Size: 123984 Color: 16

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 745278 Color: 5
Size: 131791 Color: 7
Size: 122932 Color: 5

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 745355 Color: 5
Size: 127859 Color: 17
Size: 126787 Color: 3

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 745579 Color: 13
Size: 134220 Color: 11
Size: 120202 Color: 8

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 745952 Color: 15
Size: 128280 Color: 18
Size: 125769 Color: 5

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 745974 Color: 2
Size: 130271 Color: 9
Size: 123756 Color: 5

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 745995 Color: 6
Size: 129340 Color: 4
Size: 124666 Color: 18

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 746432 Color: 9
Size: 131475 Color: 2
Size: 122094 Color: 4

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 746538 Color: 1
Size: 129383 Color: 17
Size: 124080 Color: 9

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 746855 Color: 18
Size: 128115 Color: 16
Size: 125031 Color: 8

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 746882 Color: 5
Size: 133608 Color: 1
Size: 119511 Color: 9

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 748019 Color: 5
Size: 127190 Color: 15
Size: 124792 Color: 18

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 752761 Color: 17
Size: 124362 Color: 2
Size: 122878 Color: 8

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 752769 Color: 1
Size: 127308 Color: 17
Size: 119924 Color: 14

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 752849 Color: 12
Size: 127250 Color: 0
Size: 119902 Color: 2

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 753548 Color: 4
Size: 126913 Color: 3
Size: 119540 Color: 14

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 754174 Color: 8
Size: 123878 Color: 3
Size: 121949 Color: 2

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 754616 Color: 1
Size: 127300 Color: 3
Size: 118085 Color: 4

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 754638 Color: 3
Size: 127083 Color: 2
Size: 118280 Color: 1

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 755471 Color: 11
Size: 244530 Color: 1

Bin 267: 0 of cap free
Amount of items: 2
Items: 
Size: 758155 Color: 1
Size: 241846 Color: 5

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 773493 Color: 2
Size: 113681 Color: 10
Size: 112827 Color: 10

Bin 269: 0 of cap free
Amount of items: 2
Items: 
Size: 775663 Color: 3
Size: 224338 Color: 6

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 776824 Color: 7
Size: 112200 Color: 11
Size: 110977 Color: 16

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 779260 Color: 11
Size: 111003 Color: 9
Size: 109738 Color: 17

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 779690 Color: 19
Size: 112667 Color: 9
Size: 107644 Color: 17

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 781501 Color: 3
Size: 218500 Color: 16

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 784354 Color: 12
Size: 108894 Color: 4
Size: 106753 Color: 17

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 785474 Color: 16
Size: 107561 Color: 1
Size: 106966 Color: 5

Bin 276: 0 of cap free
Amount of items: 2
Items: 
Size: 788397 Color: 6
Size: 211604 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 789955 Color: 12
Size: 105234 Color: 17
Size: 104812 Color: 12

Bin 278: 0 of cap free
Amount of items: 2
Items: 
Size: 791692 Color: 0
Size: 208309 Color: 12

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 794436 Color: 5
Size: 102821 Color: 16
Size: 102744 Color: 2

Bin 280: 1 of cap free
Amount of items: 3
Items: 
Size: 349891 Color: 10
Size: 341003 Color: 18
Size: 309106 Color: 12

Bin 281: 1 of cap free
Amount of items: 3
Items: 
Size: 368745 Color: 18
Size: 345623 Color: 13
Size: 285632 Color: 10

Bin 282: 1 of cap free
Amount of items: 3
Items: 
Size: 371338 Color: 12
Size: 351230 Color: 2
Size: 277432 Color: 5

Bin 283: 1 of cap free
Amount of items: 3
Items: 
Size: 371391 Color: 8
Size: 370102 Color: 19
Size: 258507 Color: 14

Bin 284: 1 of cap free
Amount of items: 3
Items: 
Size: 371894 Color: 15
Size: 351955 Color: 19
Size: 276151 Color: 16

Bin 285: 1 of cap free
Amount of items: 3
Items: 
Size: 374988 Color: 4
Size: 350437 Color: 11
Size: 274575 Color: 10

Bin 286: 1 of cap free
Amount of items: 3
Items: 
Size: 376222 Color: 4
Size: 351921 Color: 13
Size: 271857 Color: 10

Bin 287: 1 of cap free
Amount of items: 3
Items: 
Size: 376966 Color: 3
Size: 351406 Color: 2
Size: 271628 Color: 16

Bin 288: 1 of cap free
Amount of items: 3
Items: 
Size: 378705 Color: 3
Size: 353804 Color: 2
Size: 267491 Color: 0

Bin 289: 1 of cap free
Amount of items: 3
Items: 
Size: 380052 Color: 15
Size: 365134 Color: 7
Size: 254814 Color: 5

Bin 290: 1 of cap free
Amount of items: 3
Items: 
Size: 388897 Color: 17
Size: 358373 Color: 12
Size: 252730 Color: 6

Bin 291: 1 of cap free
Amount of items: 2
Items: 
Size: 501650 Color: 19
Size: 498350 Color: 14

Bin 292: 1 of cap free
Amount of items: 2
Items: 
Size: 514127 Color: 10
Size: 485873 Color: 18

Bin 293: 1 of cap free
Amount of items: 2
Items: 
Size: 516415 Color: 11
Size: 483585 Color: 13

Bin 294: 1 of cap free
Amount of items: 2
Items: 
Size: 531774 Color: 15
Size: 468226 Color: 2

Bin 295: 1 of cap free
Amount of items: 2
Items: 
Size: 532369 Color: 12
Size: 467631 Color: 14

Bin 296: 1 of cap free
Amount of items: 2
Items: 
Size: 552302 Color: 14
Size: 447698 Color: 2

Bin 297: 1 of cap free
Amount of items: 2
Items: 
Size: 553316 Color: 10
Size: 446684 Color: 14

Bin 298: 1 of cap free
Amount of items: 2
Items: 
Size: 558436 Color: 17
Size: 441564 Color: 19

Bin 299: 1 of cap free
Amount of items: 3
Items: 
Size: 558591 Color: 12
Size: 221458 Color: 0
Size: 219951 Color: 19

Bin 300: 1 of cap free
Amount of items: 2
Items: 
Size: 560277 Color: 17
Size: 439723 Color: 1

Bin 301: 1 of cap free
Amount of items: 2
Items: 
Size: 569313 Color: 10
Size: 430687 Color: 16

Bin 302: 1 of cap free
Amount of items: 2
Items: 
Size: 572226 Color: 8
Size: 427774 Color: 13

Bin 303: 1 of cap free
Amount of items: 2
Items: 
Size: 574421 Color: 17
Size: 425579 Color: 10

Bin 304: 1 of cap free
Amount of items: 2
Items: 
Size: 587724 Color: 18
Size: 412276 Color: 8

Bin 305: 1 of cap free
Amount of items: 2
Items: 
Size: 588495 Color: 12
Size: 411505 Color: 6

Bin 306: 1 of cap free
Amount of items: 2
Items: 
Size: 588739 Color: 8
Size: 411261 Color: 4

Bin 307: 1 of cap free
Amount of items: 2
Items: 
Size: 596534 Color: 17
Size: 403466 Color: 1

Bin 308: 1 of cap free
Amount of items: 2
Items: 
Size: 597230 Color: 15
Size: 402770 Color: 14

Bin 309: 1 of cap free
Amount of items: 2
Items: 
Size: 598391 Color: 0
Size: 401609 Color: 9

Bin 310: 1 of cap free
Amount of items: 2
Items: 
Size: 604819 Color: 0
Size: 395181 Color: 15

Bin 311: 1 of cap free
Amount of items: 2
Items: 
Size: 607676 Color: 9
Size: 392324 Color: 16

Bin 312: 1 of cap free
Amount of items: 2
Items: 
Size: 612717 Color: 5
Size: 387283 Color: 7

Bin 313: 1 of cap free
Amount of items: 2
Items: 
Size: 616649 Color: 8
Size: 383351 Color: 1

Bin 314: 1 of cap free
Amount of items: 3
Items: 
Size: 620508 Color: 4
Size: 211983 Color: 12
Size: 167509 Color: 7

Bin 315: 1 of cap free
Amount of items: 2
Items: 
Size: 622310 Color: 12
Size: 377690 Color: 14

Bin 316: 1 of cap free
Amount of items: 3
Items: 
Size: 622418 Color: 14
Size: 190003 Color: 15
Size: 187579 Color: 10

Bin 317: 1 of cap free
Amount of items: 3
Items: 
Size: 626917 Color: 0
Size: 195237 Color: 13
Size: 177846 Color: 19

Bin 318: 1 of cap free
Amount of items: 3
Items: 
Size: 627803 Color: 1
Size: 189124 Color: 13
Size: 183073 Color: 1

Bin 319: 1 of cap free
Amount of items: 3
Items: 
Size: 630492 Color: 4
Size: 185768 Color: 19
Size: 183740 Color: 8

Bin 320: 1 of cap free
Amount of items: 3
Items: 
Size: 632888 Color: 14
Size: 183899 Color: 10
Size: 183213 Color: 12

Bin 321: 1 of cap free
Amount of items: 3
Items: 
Size: 633012 Color: 12
Size: 189796 Color: 8
Size: 177192 Color: 5

Bin 322: 1 of cap free
Amount of items: 3
Items: 
Size: 633327 Color: 2
Size: 183697 Color: 12
Size: 182976 Color: 18

Bin 323: 1 of cap free
Amount of items: 3
Items: 
Size: 634748 Color: 1
Size: 189671 Color: 10
Size: 175581 Color: 10

Bin 324: 1 of cap free
Amount of items: 3
Items: 
Size: 635255 Color: 2
Size: 191494 Color: 18
Size: 173251 Color: 16

Bin 325: 1 of cap free
Amount of items: 3
Items: 
Size: 637613 Color: 2
Size: 183716 Color: 0
Size: 178671 Color: 19

Bin 326: 1 of cap free
Amount of items: 3
Items: 
Size: 637629 Color: 12
Size: 188964 Color: 18
Size: 173407 Color: 6

Bin 327: 1 of cap free
Amount of items: 3
Items: 
Size: 640960 Color: 13
Size: 187927 Color: 0
Size: 171113 Color: 11

Bin 328: 1 of cap free
Amount of items: 3
Items: 
Size: 640989 Color: 16
Size: 184127 Color: 0
Size: 174884 Color: 16

Bin 329: 1 of cap free
Amount of items: 3
Items: 
Size: 641219 Color: 18
Size: 183162 Color: 0
Size: 175619 Color: 17

Bin 330: 1 of cap free
Amount of items: 3
Items: 
Size: 641919 Color: 2
Size: 195830 Color: 16
Size: 162251 Color: 3

Bin 331: 1 of cap free
Amount of items: 3
Items: 
Size: 641674 Color: 4
Size: 185516 Color: 4
Size: 172810 Color: 1

Bin 332: 1 of cap free
Amount of items: 3
Items: 
Size: 641874 Color: 3
Size: 189764 Color: 19
Size: 168362 Color: 16

Bin 333: 1 of cap free
Amount of items: 3
Items: 
Size: 642576 Color: 6
Size: 188299 Color: 18
Size: 169125 Color: 15

Bin 334: 1 of cap free
Amount of items: 3
Items: 
Size: 643051 Color: 17
Size: 178621 Color: 18
Size: 178328 Color: 6

Bin 335: 1 of cap free
Amount of items: 3
Items: 
Size: 643533 Color: 1
Size: 183821 Color: 0
Size: 172646 Color: 3

Bin 336: 1 of cap free
Amount of items: 3
Items: 
Size: 643899 Color: 12
Size: 186153 Color: 12
Size: 169948 Color: 8

Bin 337: 1 of cap free
Amount of items: 3
Items: 
Size: 644011 Color: 0
Size: 191158 Color: 13
Size: 164831 Color: 12

Bin 338: 1 of cap free
Amount of items: 3
Items: 
Size: 645006 Color: 5
Size: 188812 Color: 8
Size: 166182 Color: 4

Bin 339: 1 of cap free
Amount of items: 3
Items: 
Size: 647074 Color: 2
Size: 183066 Color: 8
Size: 169860 Color: 12

Bin 340: 1 of cap free
Amount of items: 3
Items: 
Size: 649394 Color: 14
Size: 184002 Color: 3
Size: 166604 Color: 6

Bin 341: 1 of cap free
Amount of items: 3
Items: 
Size: 650368 Color: 19
Size: 180973 Color: 16
Size: 168659 Color: 4

Bin 342: 1 of cap free
Amount of items: 3
Items: 
Size: 650630 Color: 13
Size: 185430 Color: 14
Size: 163940 Color: 8

Bin 343: 1 of cap free
Amount of items: 2
Items: 
Size: 650850 Color: 1
Size: 349150 Color: 12

Bin 344: 1 of cap free
Amount of items: 3
Items: 
Size: 651695 Color: 19
Size: 182392 Color: 8
Size: 165913 Color: 19

Bin 345: 1 of cap free
Amount of items: 2
Items: 
Size: 652421 Color: 17
Size: 347579 Color: 7

Bin 346: 1 of cap free
Amount of items: 3
Items: 
Size: 653618 Color: 18
Size: 175737 Color: 12
Size: 170645 Color: 6

Bin 347: 1 of cap free
Amount of items: 3
Items: 
Size: 654192 Color: 14
Size: 179086 Color: 10
Size: 166722 Color: 0

Bin 348: 1 of cap free
Amount of items: 3
Items: 
Size: 654280 Color: 17
Size: 176189 Color: 18
Size: 169531 Color: 10

Bin 349: 1 of cap free
Amount of items: 3
Items: 
Size: 654949 Color: 10
Size: 180202 Color: 17
Size: 164849 Color: 16

Bin 350: 1 of cap free
Amount of items: 3
Items: 
Size: 655993 Color: 10
Size: 174665 Color: 11
Size: 169342 Color: 8

Bin 351: 1 of cap free
Amount of items: 2
Items: 
Size: 656823 Color: 10
Size: 343177 Color: 18

Bin 352: 1 of cap free
Amount of items: 3
Items: 
Size: 657149 Color: 12
Size: 184940 Color: 13
Size: 157911 Color: 12

Bin 353: 1 of cap free
Amount of items: 3
Items: 
Size: 657374 Color: 4
Size: 180517 Color: 12
Size: 162109 Color: 2

Bin 354: 1 of cap free
Amount of items: 2
Items: 
Size: 658535 Color: 13
Size: 341465 Color: 9

Bin 355: 1 of cap free
Amount of items: 3
Items: 
Size: 658725 Color: 13
Size: 175012 Color: 2
Size: 166263 Color: 3

Bin 356: 1 of cap free
Amount of items: 3
Items: 
Size: 658839 Color: 3
Size: 172938 Color: 1
Size: 168223 Color: 0

Bin 357: 1 of cap free
Amount of items: 3
Items: 
Size: 661603 Color: 10
Size: 174219 Color: 18
Size: 164178 Color: 7

Bin 358: 1 of cap free
Amount of items: 3
Items: 
Size: 667464 Color: 5
Size: 175429 Color: 1
Size: 157107 Color: 1

Bin 359: 1 of cap free
Amount of items: 3
Items: 
Size: 667486 Color: 0
Size: 169261 Color: 7
Size: 163253 Color: 1

Bin 360: 1 of cap free
Amount of items: 2
Items: 
Size: 668391 Color: 16
Size: 331609 Color: 12

Bin 361: 1 of cap free
Amount of items: 3
Items: 
Size: 669546 Color: 13
Size: 171811 Color: 17
Size: 158643 Color: 12

Bin 362: 1 of cap free
Amount of items: 2
Items: 
Size: 671796 Color: 2
Size: 328204 Color: 0

Bin 363: 1 of cap free
Amount of items: 2
Items: 
Size: 673870 Color: 12
Size: 326130 Color: 15

Bin 364: 1 of cap free
Amount of items: 2
Items: 
Size: 676674 Color: 16
Size: 323326 Color: 18

Bin 365: 1 of cap free
Amount of items: 3
Items: 
Size: 677215 Color: 17
Size: 163025 Color: 0
Size: 159760 Color: 1

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 678180 Color: 8
Size: 163112 Color: 15
Size: 158708 Color: 18

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 679115 Color: 2
Size: 162276 Color: 8
Size: 158609 Color: 3

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 679403 Color: 5
Size: 161923 Color: 6
Size: 158674 Color: 14

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 680921 Color: 1
Size: 163485 Color: 17
Size: 155594 Color: 0

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 681052 Color: 17
Size: 162022 Color: 7
Size: 156926 Color: 17

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 681530 Color: 11
Size: 159988 Color: 10
Size: 158482 Color: 10

Bin 372: 1 of cap free
Amount of items: 3
Items: 
Size: 681643 Color: 3
Size: 160910 Color: 10
Size: 157447 Color: 6

Bin 373: 1 of cap free
Amount of items: 3
Items: 
Size: 682384 Color: 5
Size: 158999 Color: 14
Size: 158617 Color: 14

Bin 374: 1 of cap free
Amount of items: 3
Items: 
Size: 683002 Color: 6
Size: 158972 Color: 19
Size: 158026 Color: 11

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 684210 Color: 7
Size: 158377 Color: 16
Size: 157413 Color: 1

Bin 376: 1 of cap free
Amount of items: 2
Items: 
Size: 697394 Color: 14
Size: 302606 Color: 12

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 698015 Color: 2
Size: 151314 Color: 9
Size: 150671 Color: 10

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 704162 Color: 13
Size: 147975 Color: 12
Size: 147863 Color: 15

Bin 379: 1 of cap free
Amount of items: 2
Items: 
Size: 709813 Color: 6
Size: 290187 Color: 5

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 712630 Color: 0
Size: 146462 Color: 3
Size: 140908 Color: 14

Bin 381: 1 of cap free
Amount of items: 2
Items: 
Size: 713429 Color: 13
Size: 286571 Color: 1

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 714919 Color: 14
Size: 148613 Color: 1
Size: 136468 Color: 4

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 714947 Color: 9
Size: 151089 Color: 16
Size: 133964 Color: 15

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 716254 Color: 1
Size: 145517 Color: 6
Size: 138229 Color: 1

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 716760 Color: 6
Size: 144870 Color: 4
Size: 138370 Color: 18

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 718426 Color: 2
Size: 145418 Color: 14
Size: 136156 Color: 15

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 718441 Color: 16
Size: 141621 Color: 3
Size: 139938 Color: 6

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 719933 Color: 0
Size: 145922 Color: 6
Size: 134145 Color: 7

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 723962 Color: 4
Size: 140428 Color: 1
Size: 135610 Color: 19

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 723981 Color: 19
Size: 145129 Color: 15
Size: 130890 Color: 15

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 725884 Color: 2
Size: 137355 Color: 18
Size: 136761 Color: 18

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 728165 Color: 4
Size: 140775 Color: 10
Size: 131060 Color: 18

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 728802 Color: 13
Size: 149029 Color: 17
Size: 122169 Color: 10

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 729226 Color: 8
Size: 148409 Color: 5
Size: 122365 Color: 11

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 729641 Color: 3
Size: 149846 Color: 15
Size: 120513 Color: 16

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 729817 Color: 3
Size: 136903 Color: 5
Size: 133280 Color: 5

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 729870 Color: 16
Size: 139328 Color: 19
Size: 130802 Color: 11

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 730179 Color: 8
Size: 138517 Color: 19
Size: 131304 Color: 4

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 729931 Color: 19
Size: 136909 Color: 5
Size: 133160 Color: 6

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 730108 Color: 16
Size: 147524 Color: 17
Size: 122368 Color: 2

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 730554 Color: 5
Size: 150445 Color: 5
Size: 119001 Color: 7

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 731920 Color: 13
Size: 138088 Color: 16
Size: 129992 Color: 11

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 732776 Color: 1
Size: 145509 Color: 4
Size: 121715 Color: 18

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 733764 Color: 14
Size: 139156 Color: 3
Size: 127080 Color: 5

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 734003 Color: 11
Size: 145480 Color: 9
Size: 120517 Color: 4

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 734210 Color: 8
Size: 145465 Color: 12
Size: 120325 Color: 17

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 735359 Color: 6
Size: 135308 Color: 17
Size: 129333 Color: 15

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 741538 Color: 5
Size: 133146 Color: 14
Size: 125316 Color: 12

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 742666 Color: 17
Size: 136041 Color: 2
Size: 121293 Color: 9

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 743304 Color: 12
Size: 133869 Color: 16
Size: 122827 Color: 3

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 743702 Color: 17
Size: 134989 Color: 15
Size: 121309 Color: 19

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 745228 Color: 16
Size: 131544 Color: 6
Size: 123228 Color: 10

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 745581 Color: 15
Size: 129012 Color: 3
Size: 125407 Color: 8

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 748059 Color: 7
Size: 126541 Color: 2
Size: 125400 Color: 3

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 748167 Color: 13
Size: 128043 Color: 16
Size: 123790 Color: 12

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 748659 Color: 13
Size: 133359 Color: 13
Size: 117982 Color: 5

Bin 417: 1 of cap free
Amount of items: 2
Items: 
Size: 749822 Color: 16
Size: 250178 Color: 9

Bin 418: 1 of cap free
Amount of items: 2
Items: 
Size: 758706 Color: 5
Size: 241294 Color: 16

Bin 419: 1 of cap free
Amount of items: 2
Items: 
Size: 759996 Color: 14
Size: 240004 Color: 8

Bin 420: 1 of cap free
Amount of items: 2
Items: 
Size: 762013 Color: 11
Size: 237987 Color: 2

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 770572 Color: 15
Size: 115738 Color: 1
Size: 113690 Color: 6

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 774017 Color: 4
Size: 114077 Color: 14
Size: 111906 Color: 9

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 777283 Color: 7
Size: 115107 Color: 2
Size: 107610 Color: 9

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 777779 Color: 13
Size: 112347 Color: 14
Size: 109874 Color: 7

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 783027 Color: 8
Size: 109265 Color: 14
Size: 107708 Color: 3

Bin 426: 1 of cap free
Amount of items: 2
Items: 
Size: 783741 Color: 3
Size: 216259 Color: 14

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 786006 Color: 10
Size: 107329 Color: 10
Size: 106665 Color: 7

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 786948 Color: 10
Size: 107003 Color: 0
Size: 106049 Color: 9

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 796061 Color: 4
Size: 102038 Color: 10
Size: 101901 Color: 4

Bin 430: 1 of cap free
Amount of items: 2
Items: 
Size: 799273 Color: 0
Size: 200727 Color: 19

Bin 431: 2 of cap free
Amount of items: 3
Items: 
Size: 363997 Color: 13
Size: 352434 Color: 9
Size: 283568 Color: 3

Bin 432: 2 of cap free
Amount of items: 3
Items: 
Size: 369428 Color: 5
Size: 352538 Color: 5
Size: 278033 Color: 12

Bin 433: 2 of cap free
Amount of items: 3
Items: 
Size: 369995 Color: 14
Size: 344144 Color: 8
Size: 285860 Color: 16

Bin 434: 2 of cap free
Amount of items: 3
Items: 
Size: 370134 Color: 17
Size: 369307 Color: 7
Size: 260558 Color: 6

Bin 435: 2 of cap free
Amount of items: 3
Items: 
Size: 371548 Color: 3
Size: 360058 Color: 14
Size: 268393 Color: 2

Bin 436: 2 of cap free
Amount of items: 3
Items: 
Size: 375991 Color: 4
Size: 338022 Color: 0
Size: 285986 Color: 4

Bin 437: 2 of cap free
Amount of items: 3
Items: 
Size: 376633 Color: 12
Size: 356894 Color: 14
Size: 266472 Color: 2

Bin 438: 2 of cap free
Amount of items: 3
Items: 
Size: 396325 Color: 17
Size: 343543 Color: 10
Size: 260131 Color: 17

Bin 439: 2 of cap free
Amount of items: 3
Items: 
Size: 397424 Color: 5
Size: 341885 Color: 7
Size: 260690 Color: 2

Bin 440: 2 of cap free
Amount of items: 3
Items: 
Size: 413141 Color: 19
Size: 309232 Color: 4
Size: 277626 Color: 0

Bin 441: 2 of cap free
Amount of items: 3
Items: 
Size: 416738 Color: 6
Size: 307861 Color: 14
Size: 275400 Color: 4

Bin 442: 2 of cap free
Amount of items: 3
Items: 
Size: 462540 Color: 11
Size: 275874 Color: 2
Size: 261585 Color: 19

Bin 443: 2 of cap free
Amount of items: 2
Items: 
Size: 511289 Color: 8
Size: 488710 Color: 18

Bin 444: 2 of cap free
Amount of items: 2
Items: 
Size: 524192 Color: 6
Size: 475807 Color: 8

Bin 445: 2 of cap free
Amount of items: 2
Items: 
Size: 534231 Color: 7
Size: 465768 Color: 1

Bin 446: 2 of cap free
Amount of items: 2
Items: 
Size: 548897 Color: 10
Size: 451102 Color: 1

Bin 447: 2 of cap free
Amount of items: 2
Items: 
Size: 549389 Color: 19
Size: 450610 Color: 1

Bin 448: 2 of cap free
Amount of items: 2
Items: 
Size: 559063 Color: 15
Size: 440936 Color: 2

Bin 449: 2 of cap free
Amount of items: 2
Items: 
Size: 560886 Color: 11
Size: 439113 Color: 13

Bin 450: 2 of cap free
Amount of items: 2
Items: 
Size: 567579 Color: 4
Size: 432420 Color: 6

Bin 451: 2 of cap free
Amount of items: 2
Items: 
Size: 569407 Color: 1
Size: 430592 Color: 5

Bin 452: 2 of cap free
Amount of items: 2
Items: 
Size: 578480 Color: 4
Size: 421519 Color: 18

Bin 453: 2 of cap free
Amount of items: 2
Items: 
Size: 579795 Color: 16
Size: 420204 Color: 14

Bin 454: 2 of cap free
Amount of items: 2
Items: 
Size: 588409 Color: 18
Size: 411590 Color: 13

Bin 455: 2 of cap free
Amount of items: 2
Items: 
Size: 613587 Color: 18
Size: 386412 Color: 9

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 620494 Color: 4
Size: 190554 Color: 9
Size: 188951 Color: 14

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 623058 Color: 12
Size: 195360 Color: 6
Size: 181581 Color: 8

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 623635 Color: 13
Size: 191229 Color: 15
Size: 185135 Color: 18

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 632299 Color: 2
Size: 191854 Color: 17
Size: 175846 Color: 9

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 632397 Color: 18
Size: 190087 Color: 10
Size: 177515 Color: 8

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 632530 Color: 14
Size: 188113 Color: 8
Size: 179356 Color: 15

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 632664 Color: 11
Size: 195705 Color: 8
Size: 171630 Color: 4

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 632762 Color: 0
Size: 188889 Color: 6
Size: 178348 Color: 10

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 634414 Color: 17
Size: 185713 Color: 11
Size: 179872 Color: 17

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 635219 Color: 0
Size: 196055 Color: 14
Size: 168725 Color: 6

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 636783 Color: 10
Size: 182255 Color: 8
Size: 180961 Color: 10

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 636842 Color: 5
Size: 186186 Color: 0
Size: 176971 Color: 14

Bin 468: 2 of cap free
Amount of items: 2
Items: 
Size: 638864 Color: 4
Size: 361135 Color: 0

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 650005 Color: 4
Size: 175290 Color: 10
Size: 174704 Color: 10

Bin 470: 2 of cap free
Amount of items: 2
Items: 
Size: 650088 Color: 13
Size: 349911 Color: 17

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 653737 Color: 6
Size: 181983 Color: 2
Size: 164279 Color: 15

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 657939 Color: 11
Size: 183644 Color: 2
Size: 158416 Color: 5

Bin 473: 2 of cap free
Amount of items: 2
Items: 
Size: 657994 Color: 18
Size: 342005 Color: 3

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 663260 Color: 12
Size: 171838 Color: 2
Size: 164901 Color: 8

Bin 475: 2 of cap free
Amount of items: 2
Items: 
Size: 667630 Color: 10
Size: 332369 Color: 5

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 678907 Color: 5
Size: 164509 Color: 3
Size: 156583 Color: 16

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 679482 Color: 6
Size: 163737 Color: 16
Size: 156780 Color: 0

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 681992 Color: 12
Size: 162649 Color: 18
Size: 155358 Color: 7

Bin 479: 2 of cap free
Amount of items: 2
Items: 
Size: 682349 Color: 10
Size: 317650 Color: 9

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 683731 Color: 0
Size: 159801 Color: 12
Size: 156467 Color: 14

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 689950 Color: 3
Size: 157152 Color: 14
Size: 152897 Color: 19

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 691435 Color: 13
Size: 156524 Color: 1
Size: 152040 Color: 15

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 711859 Color: 9
Size: 153291 Color: 10
Size: 134849 Color: 10

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 712837 Color: 0
Size: 152659 Color: 6
Size: 134503 Color: 16

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 713646 Color: 13
Size: 154212 Color: 3
Size: 132141 Color: 17

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 714236 Color: 16
Size: 146309 Color: 16
Size: 139454 Color: 2

Bin 487: 2 of cap free
Amount of items: 2
Items: 
Size: 715528 Color: 9
Size: 284471 Color: 10

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 716684 Color: 15
Size: 145897 Color: 6
Size: 137418 Color: 4

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 717820 Color: 2
Size: 147699 Color: 14
Size: 134480 Color: 17

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 718177 Color: 9
Size: 151360 Color: 8
Size: 130462 Color: 15

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 718375 Color: 9
Size: 147035 Color: 5
Size: 134589 Color: 1

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 718912 Color: 5
Size: 143817 Color: 5
Size: 137270 Color: 17

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 719132 Color: 3
Size: 152788 Color: 6
Size: 128079 Color: 0

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 719149 Color: 18
Size: 141500 Color: 3
Size: 139350 Color: 7

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 719651 Color: 16
Size: 149239 Color: 11
Size: 131109 Color: 17

Bin 496: 2 of cap free
Amount of items: 2
Items: 
Size: 720563 Color: 2
Size: 279436 Color: 11

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 720869 Color: 15
Size: 145985 Color: 9
Size: 133145 Color: 7

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 721345 Color: 11
Size: 155353 Color: 17
Size: 123301 Color: 17

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 722363 Color: 13
Size: 147332 Color: 14
Size: 130304 Color: 9

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 722694 Color: 7
Size: 140929 Color: 1
Size: 136376 Color: 7

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 726130 Color: 1
Size: 144057 Color: 16
Size: 129812 Color: 7

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 726300 Color: 8
Size: 143076 Color: 17
Size: 130623 Color: 10

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 726566 Color: 7
Size: 152108 Color: 0
Size: 121325 Color: 7

Bin 504: 2 of cap free
Amount of items: 2
Items: 
Size: 728418 Color: 14
Size: 271581 Color: 9

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 730846 Color: 10
Size: 139906 Color: 12
Size: 129247 Color: 11

Bin 506: 2 of cap free
Amount of items: 2
Items: 
Size: 735315 Color: 7
Size: 264684 Color: 1

Bin 507: 2 of cap free
Amount of items: 3
Items: 
Size: 735498 Color: 14
Size: 132949 Color: 17
Size: 131552 Color: 3

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 737138 Color: 18
Size: 136720 Color: 19
Size: 126141 Color: 16

Bin 509: 2 of cap free
Amount of items: 3
Items: 
Size: 740003 Color: 2
Size: 139241 Color: 8
Size: 120755 Color: 16

Bin 510: 2 of cap free
Amount of items: 3
Items: 
Size: 740560 Color: 5
Size: 139890 Color: 14
Size: 119549 Color: 8

Bin 511: 2 of cap free
Amount of items: 2
Items: 
Size: 740728 Color: 2
Size: 259271 Color: 9

Bin 512: 2 of cap free
Amount of items: 3
Items: 
Size: 741402 Color: 8
Size: 138951 Color: 11
Size: 119646 Color: 12

Bin 513: 2 of cap free
Amount of items: 3
Items: 
Size: 741353 Color: 7
Size: 134998 Color: 10
Size: 123648 Color: 14

Bin 514: 2 of cap free
Amount of items: 3
Items: 
Size: 743980 Color: 2
Size: 135769 Color: 7
Size: 120250 Color: 5

Bin 515: 2 of cap free
Amount of items: 3
Items: 
Size: 743993 Color: 8
Size: 132848 Color: 19
Size: 123158 Color: 10

Bin 516: 2 of cap free
Amount of items: 3
Items: 
Size: 744691 Color: 13
Size: 129394 Color: 1
Size: 125914 Color: 7

Bin 517: 2 of cap free
Amount of items: 2
Items: 
Size: 746627 Color: 10
Size: 253372 Color: 6

Bin 518: 2 of cap free
Amount of items: 2
Items: 
Size: 750553 Color: 3
Size: 249446 Color: 7

Bin 519: 2 of cap free
Amount of items: 2
Items: 
Size: 750780 Color: 4
Size: 249219 Color: 12

Bin 520: 2 of cap free
Amount of items: 2
Items: 
Size: 751397 Color: 3
Size: 248602 Color: 15

Bin 521: 2 of cap free
Amount of items: 3
Items: 
Size: 754210 Color: 15
Size: 123312 Color: 7
Size: 122477 Color: 6

Bin 522: 2 of cap free
Amount of items: 2
Items: 
Size: 763312 Color: 0
Size: 236687 Color: 11

Bin 523: 2 of cap free
Amount of items: 2
Items: 
Size: 767078 Color: 16
Size: 232921 Color: 0

Bin 524: 2 of cap free
Amount of items: 2
Items: 
Size: 770364 Color: 19
Size: 229635 Color: 17

Bin 525: 2 of cap free
Amount of items: 2
Items: 
Size: 770800 Color: 16
Size: 229199 Color: 7

Bin 526: 2 of cap free
Amount of items: 2
Items: 
Size: 772517 Color: 4
Size: 227482 Color: 8

Bin 527: 2 of cap free
Amount of items: 3
Items: 
Size: 773214 Color: 4
Size: 113429 Color: 0
Size: 113356 Color: 11

Bin 528: 2 of cap free
Amount of items: 3
Items: 
Size: 775021 Color: 12
Size: 115421 Color: 14
Size: 109557 Color: 12

Bin 529: 2 of cap free
Amount of items: 2
Items: 
Size: 775140 Color: 3
Size: 224859 Color: 18

Bin 530: 2 of cap free
Amount of items: 3
Items: 
Size: 777262 Color: 12
Size: 111941 Color: 17
Size: 110796 Color: 8

Bin 531: 2 of cap free
Amount of items: 3
Items: 
Size: 778613 Color: 3
Size: 111326 Color: 10
Size: 110060 Color: 7

Bin 532: 2 of cap free
Amount of items: 2
Items: 
Size: 784856 Color: 3
Size: 215143 Color: 15

Bin 533: 2 of cap free
Amount of items: 3
Items: 
Size: 787647 Color: 17
Size: 106331 Color: 4
Size: 106021 Color: 18

Bin 534: 2 of cap free
Amount of items: 3
Items: 
Size: 791458 Color: 6
Size: 104279 Color: 8
Size: 104262 Color: 3

Bin 535: 2 of cap free
Amount of items: 2
Items: 
Size: 793540 Color: 3
Size: 206459 Color: 8

Bin 536: 2 of cap free
Amount of items: 2
Items: 
Size: 799279 Color: 12
Size: 200720 Color: 14

Bin 537: 3 of cap free
Amount of items: 3
Items: 
Size: 362037 Color: 14
Size: 328203 Color: 2
Size: 309758 Color: 16

Bin 538: 3 of cap free
Amount of items: 3
Items: 
Size: 369057 Color: 3
Size: 347324 Color: 16
Size: 283617 Color: 19

Bin 539: 3 of cap free
Amount of items: 3
Items: 
Size: 369170 Color: 3
Size: 345440 Color: 6
Size: 285388 Color: 10

Bin 540: 3 of cap free
Amount of items: 3
Items: 
Size: 376140 Color: 1
Size: 362509 Color: 11
Size: 261349 Color: 15

Bin 541: 3 of cap free
Amount of items: 3
Items: 
Size: 377096 Color: 1
Size: 352638 Color: 16
Size: 270264 Color: 3

Bin 542: 3 of cap free
Amount of items: 3
Items: 
Size: 379139 Color: 3
Size: 349250 Color: 12
Size: 271609 Color: 16

Bin 543: 3 of cap free
Amount of items: 3
Items: 
Size: 379152 Color: 7
Size: 346156 Color: 1
Size: 274690 Color: 15

Bin 544: 3 of cap free
Amount of items: 3
Items: 
Size: 389073 Color: 10
Size: 351307 Color: 13
Size: 259618 Color: 0

Bin 545: 3 of cap free
Amount of items: 3
Items: 
Size: 390141 Color: 1
Size: 337919 Color: 17
Size: 271938 Color: 16

Bin 546: 3 of cap free
Amount of items: 3
Items: 
Size: 409381 Color: 7
Size: 338439 Color: 2
Size: 252178 Color: 9

Bin 547: 3 of cap free
Amount of items: 3
Items: 
Size: 414362 Color: 19
Size: 336576 Color: 10
Size: 249060 Color: 12

Bin 548: 3 of cap free
Amount of items: 3
Items: 
Size: 500397 Color: 11
Size: 250486 Color: 16
Size: 249115 Color: 9

Bin 549: 3 of cap free
Amount of items: 2
Items: 
Size: 507695 Color: 3
Size: 492303 Color: 16

Bin 550: 3 of cap free
Amount of items: 2
Items: 
Size: 515176 Color: 3
Size: 484822 Color: 7

Bin 551: 3 of cap free
Amount of items: 2
Items: 
Size: 521187 Color: 13
Size: 478811 Color: 4

Bin 552: 3 of cap free
Amount of items: 2
Items: 
Size: 522672 Color: 3
Size: 477326 Color: 5

Bin 553: 3 of cap free
Amount of items: 2
Items: 
Size: 524753 Color: 7
Size: 475245 Color: 18

Bin 554: 3 of cap free
Amount of items: 2
Items: 
Size: 526041 Color: 11
Size: 473957 Color: 3

Bin 555: 3 of cap free
Amount of items: 2
Items: 
Size: 537047 Color: 13
Size: 462951 Color: 8

Bin 556: 3 of cap free
Amount of items: 2
Items: 
Size: 548854 Color: 13
Size: 451144 Color: 0

Bin 557: 3 of cap free
Amount of items: 2
Items: 
Size: 549697 Color: 9
Size: 450301 Color: 14

Bin 558: 3 of cap free
Amount of items: 2
Items: 
Size: 551071 Color: 5
Size: 448927 Color: 7

Bin 559: 3 of cap free
Amount of items: 2
Items: 
Size: 561173 Color: 6
Size: 438825 Color: 18

Bin 560: 3 of cap free
Amount of items: 2
Items: 
Size: 568663 Color: 8
Size: 431335 Color: 2

Bin 561: 3 of cap free
Amount of items: 3
Items: 
Size: 577276 Color: 16
Size: 226005 Color: 5
Size: 196717 Color: 1

Bin 562: 3 of cap free
Amount of items: 3
Items: 
Size: 578655 Color: 11
Size: 222927 Color: 9
Size: 198416 Color: 0

Bin 563: 3 of cap free
Amount of items: 2
Items: 
Size: 579830 Color: 11
Size: 420168 Color: 1

Bin 564: 3 of cap free
Amount of items: 2
Items: 
Size: 579947 Color: 10
Size: 420051 Color: 3

Bin 565: 3 of cap free
Amount of items: 2
Items: 
Size: 592132 Color: 3
Size: 407866 Color: 8

Bin 566: 3 of cap free
Amount of items: 2
Items: 
Size: 593089 Color: 19
Size: 406909 Color: 14

Bin 567: 3 of cap free
Amount of items: 2
Items: 
Size: 599556 Color: 11
Size: 400442 Color: 12

Bin 568: 3 of cap free
Amount of items: 2
Items: 
Size: 616560 Color: 17
Size: 383438 Color: 8

Bin 569: 3 of cap free
Amount of items: 3
Items: 
Size: 620941 Color: 13
Size: 212017 Color: 11
Size: 167040 Color: 16

Bin 570: 3 of cap free
Amount of items: 2
Items: 
Size: 622976 Color: 14
Size: 377022 Color: 9

Bin 571: 3 of cap free
Amount of items: 3
Items: 
Size: 625207 Color: 3
Size: 192480 Color: 12
Size: 182311 Color: 12

Bin 572: 3 of cap free
Amount of items: 3
Items: 
Size: 629200 Color: 8
Size: 211758 Color: 19
Size: 159040 Color: 12

Bin 573: 3 of cap free
Amount of items: 3
Items: 
Size: 630504 Color: 4
Size: 193889 Color: 19
Size: 175605 Color: 14

Bin 574: 3 of cap free
Amount of items: 2
Items: 
Size: 631531 Color: 17
Size: 368467 Color: 0

Bin 575: 3 of cap free
Amount of items: 3
Items: 
Size: 633125 Color: 2
Size: 191471 Color: 10
Size: 175402 Color: 14

Bin 576: 3 of cap free
Amount of items: 3
Items: 
Size: 634249 Color: 12
Size: 185462 Color: 4
Size: 180287 Color: 7

Bin 577: 3 of cap free
Amount of items: 3
Items: 
Size: 634634 Color: 2
Size: 198050 Color: 18
Size: 167314 Color: 15

Bin 578: 3 of cap free
Amount of items: 2
Items: 
Size: 636899 Color: 4
Size: 363099 Color: 18

Bin 579: 3 of cap free
Amount of items: 3
Items: 
Size: 636976 Color: 4
Size: 191528 Color: 11
Size: 171494 Color: 10

Bin 580: 3 of cap free
Amount of items: 2
Items: 
Size: 638766 Color: 5
Size: 361232 Color: 12

Bin 581: 3 of cap free
Amount of items: 3
Items: 
Size: 639213 Color: 8
Size: 195012 Color: 0
Size: 165773 Color: 7

Bin 582: 3 of cap free
Amount of items: 2
Items: 
Size: 639697 Color: 14
Size: 360301 Color: 15

Bin 583: 3 of cap free
Amount of items: 2
Items: 
Size: 648346 Color: 14
Size: 351652 Color: 17

Bin 584: 3 of cap free
Amount of items: 3
Items: 
Size: 649975 Color: 11
Size: 175525 Color: 16
Size: 174498 Color: 12

Bin 585: 3 of cap free
Amount of items: 2
Items: 
Size: 658404 Color: 10
Size: 341594 Color: 16

Bin 586: 3 of cap free
Amount of items: 2
Items: 
Size: 665025 Color: 7
Size: 334973 Color: 13

Bin 587: 3 of cap free
Amount of items: 3
Items: 
Size: 681787 Color: 5
Size: 164351 Color: 16
Size: 153860 Color: 4

Bin 588: 3 of cap free
Amount of items: 3
Items: 
Size: 681809 Color: 6
Size: 162122 Color: 4
Size: 156067 Color: 6

Bin 589: 3 of cap free
Amount of items: 3
Items: 
Size: 684096 Color: 14
Size: 161125 Color: 4
Size: 154777 Color: 10

Bin 590: 3 of cap free
Amount of items: 2
Items: 
Size: 693760 Color: 8
Size: 306238 Color: 16

Bin 591: 3 of cap free
Amount of items: 2
Items: 
Size: 704208 Color: 6
Size: 295790 Color: 12

Bin 592: 3 of cap free
Amount of items: 2
Items: 
Size: 710013 Color: 10
Size: 289985 Color: 7

Bin 593: 3 of cap free
Amount of items: 3
Items: 
Size: 713730 Color: 15
Size: 150762 Color: 11
Size: 135506 Color: 0

Bin 594: 3 of cap free
Amount of items: 3
Items: 
Size: 714418 Color: 19
Size: 153822 Color: 12
Size: 131758 Color: 1

Bin 595: 3 of cap free
Amount of items: 3
Items: 
Size: 715741 Color: 7
Size: 142530 Color: 14
Size: 141727 Color: 6

Bin 596: 3 of cap free
Amount of items: 2
Items: 
Size: 718770 Color: 7
Size: 281228 Color: 17

Bin 597: 3 of cap free
Amount of items: 3
Items: 
Size: 719645 Color: 19
Size: 146811 Color: 3
Size: 133542 Color: 2

Bin 598: 3 of cap free
Amount of items: 3
Items: 
Size: 722242 Color: 4
Size: 148261 Color: 11
Size: 129495 Color: 12

Bin 599: 3 of cap free
Amount of items: 3
Items: 
Size: 722273 Color: 1
Size: 151129 Color: 10
Size: 126596 Color: 17

Bin 600: 3 of cap free
Amount of items: 2
Items: 
Size: 727385 Color: 4
Size: 272613 Color: 6

Bin 601: 3 of cap free
Amount of items: 3
Items: 
Size: 728980 Color: 19
Size: 138826 Color: 4
Size: 132192 Color: 18

Bin 602: 3 of cap free
Amount of items: 3
Items: 
Size: 731092 Color: 14
Size: 142772 Color: 14
Size: 126134 Color: 4

Bin 603: 3 of cap free
Amount of items: 3
Items: 
Size: 732071 Color: 7
Size: 137351 Color: 4
Size: 130576 Color: 16

Bin 604: 3 of cap free
Amount of items: 2
Items: 
Size: 733612 Color: 4
Size: 266386 Color: 7

Bin 605: 3 of cap free
Amount of items: 3
Items: 
Size: 735548 Color: 3
Size: 145166 Color: 6
Size: 119284 Color: 1

Bin 606: 3 of cap free
Amount of items: 2
Items: 
Size: 737085 Color: 1
Size: 262913 Color: 4

Bin 607: 3 of cap free
Amount of items: 2
Items: 
Size: 754903 Color: 3
Size: 245095 Color: 11

Bin 608: 3 of cap free
Amount of items: 3
Items: 
Size: 774522 Color: 13
Size: 113397 Color: 8
Size: 112079 Color: 19

Bin 609: 3 of cap free
Amount of items: 2
Items: 
Size: 775607 Color: 14
Size: 224391 Color: 3

Bin 610: 3 of cap free
Amount of items: 3
Items: 
Size: 779233 Color: 19
Size: 112307 Color: 4
Size: 108458 Color: 4

Bin 611: 3 of cap free
Amount of items: 3
Items: 
Size: 782032 Color: 4
Size: 109948 Color: 7
Size: 108018 Color: 11

Bin 612: 3 of cap free
Amount of items: 2
Items: 
Size: 782320 Color: 0
Size: 217678 Color: 1

Bin 613: 3 of cap free
Amount of items: 3
Items: 
Size: 784324 Color: 4
Size: 108693 Color: 7
Size: 106981 Color: 6

Bin 614: 3 of cap free
Amount of items: 3
Items: 
Size: 784465 Color: 13
Size: 108718 Color: 3
Size: 106815 Color: 14

Bin 615: 3 of cap free
Amount of items: 2
Items: 
Size: 790892 Color: 3
Size: 209106 Color: 0

Bin 616: 3 of cap free
Amount of items: 3
Items: 
Size: 792661 Color: 2
Size: 103752 Color: 4
Size: 103585 Color: 3

Bin 617: 3 of cap free
Amount of items: 3
Items: 
Size: 795031 Color: 13
Size: 102528 Color: 17
Size: 102439 Color: 10

Bin 618: 3 of cap free
Amount of items: 2
Items: 
Size: 795371 Color: 3
Size: 204627 Color: 15

Bin 619: 3 of cap free
Amount of items: 3
Items: 
Size: 799281 Color: 5
Size: 100475 Color: 19
Size: 100242 Color: 9

Bin 620: 3 of cap free
Amount of items: 2
Items: 
Size: 799960 Color: 0
Size: 200038 Color: 8

Bin 621: 4 of cap free
Amount of items: 3
Items: 
Size: 363778 Color: 8
Size: 348422 Color: 5
Size: 287797 Color: 19

Bin 622: 4 of cap free
Amount of items: 3
Items: 
Size: 365287 Color: 17
Size: 359872 Color: 13
Size: 274838 Color: 2

Bin 623: 4 of cap free
Amount of items: 3
Items: 
Size: 378337 Color: 1
Size: 360004 Color: 17
Size: 261656 Color: 14

Bin 624: 4 of cap free
Amount of items: 3
Items: 
Size: 388867 Color: 10
Size: 340955 Color: 9
Size: 270175 Color: 2

Bin 625: 4 of cap free
Amount of items: 2
Items: 
Size: 511024 Color: 3
Size: 488973 Color: 11

Bin 626: 4 of cap free
Amount of items: 2
Items: 
Size: 511352 Color: 9
Size: 488645 Color: 8

Bin 627: 4 of cap free
Amount of items: 3
Items: 
Size: 519074 Color: 10
Size: 253433 Color: 3
Size: 227490 Color: 12

Bin 628: 4 of cap free
Amount of items: 2
Items: 
Size: 521003 Color: 19
Size: 478994 Color: 3

Bin 629: 4 of cap free
Amount of items: 2
Items: 
Size: 563807 Color: 12
Size: 436190 Color: 19

Bin 630: 4 of cap free
Amount of items: 2
Items: 
Size: 587143 Color: 5
Size: 412854 Color: 3

Bin 631: 4 of cap free
Amount of items: 2
Items: 
Size: 597237 Color: 17
Size: 402760 Color: 15

Bin 632: 4 of cap free
Amount of items: 2
Items: 
Size: 598018 Color: 2
Size: 401979 Color: 8

Bin 633: 4 of cap free
Amount of items: 2
Items: 
Size: 598039 Color: 10
Size: 401958 Color: 5

Bin 634: 4 of cap free
Amount of items: 2
Items: 
Size: 598603 Color: 5
Size: 401394 Color: 13

Bin 635: 4 of cap free
Amount of items: 2
Items: 
Size: 603532 Color: 14
Size: 396465 Color: 19

Bin 636: 4 of cap free
Amount of items: 2
Items: 
Size: 607387 Color: 17
Size: 392610 Color: 16

Bin 637: 4 of cap free
Amount of items: 3
Items: 
Size: 621522 Color: 14
Size: 196366 Color: 7
Size: 182109 Color: 1

Bin 638: 4 of cap free
Amount of items: 3
Items: 
Size: 632166 Color: 18
Size: 192488 Color: 16
Size: 175343 Color: 7

Bin 639: 4 of cap free
Amount of items: 3
Items: 
Size: 633070 Color: 16
Size: 186958 Color: 8
Size: 179969 Color: 19

Bin 640: 4 of cap free
Amount of items: 3
Items: 
Size: 634028 Color: 2
Size: 187330 Color: 8
Size: 178639 Color: 4

Bin 641: 4 of cap free
Amount of items: 2
Items: 
Size: 636974 Color: 3
Size: 363023 Color: 4

Bin 642: 4 of cap free
Amount of items: 2
Items: 
Size: 639154 Color: 17
Size: 360843 Color: 9

Bin 643: 4 of cap free
Amount of items: 3
Items: 
Size: 645969 Color: 13
Size: 187444 Color: 2
Size: 166584 Color: 4

Bin 644: 4 of cap free
Amount of items: 2
Items: 
Size: 651353 Color: 7
Size: 348644 Color: 19

Bin 645: 4 of cap free
Amount of items: 3
Items: 
Size: 652127 Color: 9
Size: 187053 Color: 16
Size: 160817 Color: 8

Bin 646: 4 of cap free
Amount of items: 2
Items: 
Size: 654686 Color: 7
Size: 345311 Color: 17

Bin 647: 4 of cap free
Amount of items: 3
Items: 
Size: 660667 Color: 1
Size: 177517 Color: 2
Size: 161813 Color: 5

Bin 648: 4 of cap free
Amount of items: 2
Items: 
Size: 664996 Color: 13
Size: 335001 Color: 3

Bin 649: 4 of cap free
Amount of items: 2
Items: 
Size: 676988 Color: 18
Size: 323009 Color: 4

Bin 650: 4 of cap free
Amount of items: 3
Items: 
Size: 680467 Color: 18
Size: 165783 Color: 13
Size: 153747 Color: 4

Bin 651: 4 of cap free
Amount of items: 3
Items: 
Size: 689998 Color: 8
Size: 156108 Color: 5
Size: 153891 Color: 3

Bin 652: 4 of cap free
Amount of items: 3
Items: 
Size: 692506 Color: 5
Size: 154056 Color: 16
Size: 153435 Color: 9

Bin 653: 4 of cap free
Amount of items: 2
Items: 
Size: 696950 Color: 12
Size: 303047 Color: 7

Bin 654: 4 of cap free
Amount of items: 3
Items: 
Size: 718600 Color: 13
Size: 143360 Color: 14
Size: 138037 Color: 5

Bin 655: 4 of cap free
Amount of items: 2
Items: 
Size: 719770 Color: 18
Size: 280227 Color: 16

Bin 656: 4 of cap free
Amount of items: 3
Items: 
Size: 726733 Color: 6
Size: 149223 Color: 0
Size: 124041 Color: 18

Bin 657: 4 of cap free
Amount of items: 3
Items: 
Size: 738403 Color: 11
Size: 140458 Color: 17
Size: 121136 Color: 19

Bin 658: 4 of cap free
Amount of items: 2
Items: 
Size: 740335 Color: 15
Size: 259662 Color: 19

Bin 659: 4 of cap free
Amount of items: 3
Items: 
Size: 740397 Color: 2
Size: 131705 Color: 14
Size: 127895 Color: 5

Bin 660: 4 of cap free
Amount of items: 3
Items: 
Size: 745580 Color: 5
Size: 135563 Color: 5
Size: 118854 Color: 7

Bin 661: 4 of cap free
Amount of items: 2
Items: 
Size: 747936 Color: 12
Size: 252061 Color: 19

Bin 662: 4 of cap free
Amount of items: 2
Items: 
Size: 748960 Color: 9
Size: 251037 Color: 4

Bin 663: 4 of cap free
Amount of items: 2
Items: 
Size: 751772 Color: 19
Size: 248225 Color: 2

Bin 664: 4 of cap free
Amount of items: 3
Items: 
Size: 752834 Color: 7
Size: 127469 Color: 18
Size: 119694 Color: 1

Bin 665: 4 of cap free
Amount of items: 2
Items: 
Size: 762801 Color: 18
Size: 237196 Color: 6

Bin 666: 4 of cap free
Amount of items: 2
Items: 
Size: 765179 Color: 10
Size: 234818 Color: 13

Bin 667: 4 of cap free
Amount of items: 2
Items: 
Size: 768077 Color: 0
Size: 231920 Color: 3

Bin 668: 4 of cap free
Amount of items: 3
Items: 
Size: 773529 Color: 10
Size: 117194 Color: 12
Size: 109274 Color: 15

Bin 669: 4 of cap free
Amount of items: 3
Items: 
Size: 775029 Color: 4
Size: 113766 Color: 4
Size: 111202 Color: 17

Bin 670: 4 of cap free
Amount of items: 2
Items: 
Size: 776470 Color: 6
Size: 223527 Color: 8

Bin 671: 4 of cap free
Amount of items: 2
Items: 
Size: 778791 Color: 17
Size: 221206 Color: 5

Bin 672: 4 of cap free
Amount of items: 3
Items: 
Size: 779270 Color: 15
Size: 112052 Color: 19
Size: 108675 Color: 8

Bin 673: 4 of cap free
Amount of items: 2
Items: 
Size: 782011 Color: 2
Size: 217986 Color: 17

Bin 674: 4 of cap free
Amount of items: 3
Items: 
Size: 783695 Color: 13
Size: 109375 Color: 17
Size: 106927 Color: 0

Bin 675: 4 of cap free
Amount of items: 2
Items: 
Size: 788544 Color: 12
Size: 211453 Color: 10

Bin 676: 4 of cap free
Amount of items: 3
Items: 
Size: 792256 Color: 0
Size: 103907 Color: 7
Size: 103834 Color: 2

Bin 677: 4 of cap free
Amount of items: 2
Items: 
Size: 793019 Color: 19
Size: 206978 Color: 1

Bin 678: 4 of cap free
Amount of items: 3
Items: 
Size: 795389 Color: 12
Size: 102445 Color: 7
Size: 102163 Color: 4

Bin 679: 5 of cap free
Amount of items: 3
Items: 
Size: 359604 Color: 1
Size: 359115 Color: 0
Size: 281277 Color: 5

Bin 680: 5 of cap free
Amount of items: 3
Items: 
Size: 371807 Color: 0
Size: 357141 Color: 8
Size: 271048 Color: 16

Bin 681: 5 of cap free
Amount of items: 3
Items: 
Size: 374226 Color: 12
Size: 349598 Color: 4
Size: 276172 Color: 9

Bin 682: 5 of cap free
Amount of items: 3
Items: 
Size: 431343 Color: 11
Size: 284625 Color: 2
Size: 284028 Color: 2

Bin 683: 5 of cap free
Amount of items: 2
Items: 
Size: 500586 Color: 1
Size: 499410 Color: 8

Bin 684: 5 of cap free
Amount of items: 2
Items: 
Size: 502762 Color: 17
Size: 497234 Color: 12

Bin 685: 5 of cap free
Amount of items: 2
Items: 
Size: 505120 Color: 5
Size: 494876 Color: 18

Bin 686: 5 of cap free
Amount of items: 2
Items: 
Size: 510528 Color: 7
Size: 489468 Color: 10

Bin 687: 5 of cap free
Amount of items: 2
Items: 
Size: 512421 Color: 4
Size: 487575 Color: 0

Bin 688: 5 of cap free
Amount of items: 2
Items: 
Size: 515356 Color: 4
Size: 484640 Color: 18

Bin 689: 5 of cap free
Amount of items: 2
Items: 
Size: 519750 Color: 15
Size: 480246 Color: 10

Bin 690: 5 of cap free
Amount of items: 2
Items: 
Size: 523758 Color: 16
Size: 476238 Color: 2

Bin 691: 5 of cap free
Amount of items: 2
Items: 
Size: 540607 Color: 11
Size: 459389 Color: 9

Bin 692: 5 of cap free
Amount of items: 2
Items: 
Size: 548301 Color: 14
Size: 451695 Color: 13

Bin 693: 5 of cap free
Amount of items: 2
Items: 
Size: 551098 Color: 15
Size: 448898 Color: 12

Bin 694: 5 of cap free
Amount of items: 2
Items: 
Size: 553648 Color: 5
Size: 446348 Color: 1

Bin 695: 5 of cap free
Amount of items: 2
Items: 
Size: 576745 Color: 17
Size: 423251 Color: 18

Bin 696: 5 of cap free
Amount of items: 2
Items: 
Size: 579521 Color: 17
Size: 420475 Color: 13

Bin 697: 5 of cap free
Amount of items: 2
Items: 
Size: 580860 Color: 8
Size: 419136 Color: 7

Bin 698: 5 of cap free
Amount of items: 2
Items: 
Size: 588443 Color: 6
Size: 411553 Color: 7

Bin 699: 5 of cap free
Amount of items: 2
Items: 
Size: 589380 Color: 8
Size: 410616 Color: 2

Bin 700: 5 of cap free
Amount of items: 2
Items: 
Size: 589601 Color: 6
Size: 410395 Color: 13

Bin 701: 5 of cap free
Amount of items: 2
Items: 
Size: 617946 Color: 13
Size: 382050 Color: 0

Bin 702: 5 of cap free
Amount of items: 2
Items: 
Size: 619314 Color: 6
Size: 380682 Color: 0

Bin 703: 5 of cap free
Amount of items: 2
Items: 
Size: 622218 Color: 12
Size: 377778 Color: 13

Bin 704: 5 of cap free
Amount of items: 2
Items: 
Size: 624443 Color: 16
Size: 375553 Color: 6

Bin 705: 5 of cap free
Amount of items: 3
Items: 
Size: 625478 Color: 16
Size: 194106 Color: 17
Size: 180412 Color: 12

Bin 706: 5 of cap free
Amount of items: 2
Items: 
Size: 627737 Color: 1
Size: 372259 Color: 19

Bin 707: 5 of cap free
Amount of items: 2
Items: 
Size: 628250 Color: 4
Size: 371746 Color: 3

Bin 708: 5 of cap free
Amount of items: 3
Items: 
Size: 629769 Color: 17
Size: 190874 Color: 12
Size: 179353 Color: 16

Bin 709: 5 of cap free
Amount of items: 3
Items: 
Size: 631013 Color: 2
Size: 191275 Color: 19
Size: 177708 Color: 18

Bin 710: 5 of cap free
Amount of items: 3
Items: 
Size: 631575 Color: 12
Size: 190394 Color: 5
Size: 178027 Color: 0

Bin 711: 5 of cap free
Amount of items: 3
Items: 
Size: 645920 Color: 0
Size: 195612 Color: 19
Size: 158464 Color: 14

Bin 712: 5 of cap free
Amount of items: 3
Items: 
Size: 653637 Color: 0
Size: 185978 Color: 13
Size: 160381 Color: 0

Bin 713: 5 of cap free
Amount of items: 3
Items: 
Size: 655472 Color: 16
Size: 174739 Color: 17
Size: 169785 Color: 11

Bin 714: 5 of cap free
Amount of items: 2
Items: 
Size: 683628 Color: 9
Size: 316368 Color: 3

Bin 715: 5 of cap free
Amount of items: 2
Items: 
Size: 685421 Color: 12
Size: 314575 Color: 14

Bin 716: 5 of cap free
Amount of items: 3
Items: 
Size: 691713 Color: 4
Size: 156634 Color: 14
Size: 151649 Color: 6

Bin 717: 5 of cap free
Amount of items: 2
Items: 
Size: 695118 Color: 10
Size: 304878 Color: 1

Bin 718: 5 of cap free
Amount of items: 2
Items: 
Size: 702058 Color: 10
Size: 297938 Color: 17

Bin 719: 5 of cap free
Amount of items: 2
Items: 
Size: 703045 Color: 6
Size: 296951 Color: 19

Bin 720: 5 of cap free
Amount of items: 2
Items: 
Size: 708301 Color: 4
Size: 291695 Color: 17

Bin 721: 5 of cap free
Amount of items: 3
Items: 
Size: 712787 Color: 13
Size: 150394 Color: 19
Size: 136815 Color: 11

Bin 722: 5 of cap free
Amount of items: 2
Items: 
Size: 714559 Color: 1
Size: 285437 Color: 16

Bin 723: 5 of cap free
Amount of items: 3
Items: 
Size: 716048 Color: 8
Size: 155229 Color: 10
Size: 128719 Color: 11

Bin 724: 5 of cap free
Amount of items: 2
Items: 
Size: 716071 Color: 17
Size: 283925 Color: 2

Bin 725: 5 of cap free
Amount of items: 3
Items: 
Size: 718125 Color: 14
Size: 143046 Color: 4
Size: 138825 Color: 14

Bin 726: 5 of cap free
Amount of items: 2
Items: 
Size: 718754 Color: 16
Size: 281242 Color: 11

Bin 727: 5 of cap free
Amount of items: 2
Items: 
Size: 727662 Color: 18
Size: 272334 Color: 10

Bin 728: 5 of cap free
Amount of items: 3
Items: 
Size: 732253 Color: 0
Size: 143744 Color: 8
Size: 123999 Color: 19

Bin 729: 5 of cap free
Amount of items: 2
Items: 
Size: 743042 Color: 10
Size: 256954 Color: 15

Bin 730: 5 of cap free
Amount of items: 2
Items: 
Size: 749207 Color: 14
Size: 250789 Color: 17

Bin 731: 5 of cap free
Amount of items: 2
Items: 
Size: 756073 Color: 8
Size: 243923 Color: 10

Bin 732: 5 of cap free
Amount of items: 2
Items: 
Size: 756405 Color: 6
Size: 243591 Color: 18

Bin 733: 5 of cap free
Amount of items: 2
Items: 
Size: 769620 Color: 9
Size: 230376 Color: 15

Bin 734: 5 of cap free
Amount of items: 3
Items: 
Size: 775414 Color: 0
Size: 113866 Color: 3
Size: 110716 Color: 11

Bin 735: 5 of cap free
Amount of items: 3
Items: 
Size: 777779 Color: 0
Size: 112538 Color: 14
Size: 109679 Color: 9

Bin 736: 5 of cap free
Amount of items: 3
Items: 
Size: 783019 Color: 7
Size: 109226 Color: 12
Size: 107751 Color: 5

Bin 737: 5 of cap free
Amount of items: 2
Items: 
Size: 784062 Color: 5
Size: 215934 Color: 9

Bin 738: 5 of cap free
Amount of items: 3
Items: 
Size: 790353 Color: 10
Size: 104863 Color: 0
Size: 104780 Color: 15

Bin 739: 5 of cap free
Amount of items: 2
Items: 
Size: 798393 Color: 17
Size: 201603 Color: 15

Bin 740: 6 of cap free
Amount of items: 3
Items: 
Size: 363861 Color: 0
Size: 328017 Color: 12
Size: 308117 Color: 2

Bin 741: 6 of cap free
Amount of items: 3
Items: 
Size: 361734 Color: 18
Size: 350444 Color: 15
Size: 287817 Color: 6

Bin 742: 6 of cap free
Amount of items: 3
Items: 
Size: 380077 Color: 0
Size: 361378 Color: 18
Size: 258540 Color: 3

Bin 743: 6 of cap free
Amount of items: 3
Items: 
Size: 431194 Color: 9
Size: 308354 Color: 16
Size: 260447 Color: 8

Bin 744: 6 of cap free
Amount of items: 2
Items: 
Size: 504074 Color: 17
Size: 495921 Color: 16

Bin 745: 6 of cap free
Amount of items: 2
Items: 
Size: 526872 Color: 5
Size: 473123 Color: 18

Bin 746: 6 of cap free
Amount of items: 2
Items: 
Size: 529981 Color: 7
Size: 470014 Color: 13

Bin 747: 6 of cap free
Amount of items: 2
Items: 
Size: 530924 Color: 6
Size: 469071 Color: 13

Bin 748: 6 of cap free
Amount of items: 3
Items: 
Size: 538494 Color: 15
Size: 233715 Color: 10
Size: 227786 Color: 15

Bin 749: 6 of cap free
Amount of items: 2
Items: 
Size: 541483 Color: 15
Size: 458512 Color: 19

Bin 750: 6 of cap free
Amount of items: 2
Items: 
Size: 544431 Color: 6
Size: 455564 Color: 8

Bin 751: 6 of cap free
Amount of items: 2
Items: 
Size: 545188 Color: 11
Size: 454807 Color: 16

Bin 752: 6 of cap free
Amount of items: 2
Items: 
Size: 545346 Color: 12
Size: 454649 Color: 7

Bin 753: 6 of cap free
Amount of items: 3
Items: 
Size: 558846 Color: 1
Size: 222836 Color: 18
Size: 218313 Color: 17

Bin 754: 6 of cap free
Amount of items: 2
Items: 
Size: 561632 Color: 18
Size: 438363 Color: 15

Bin 755: 6 of cap free
Amount of items: 2
Items: 
Size: 568648 Color: 6
Size: 431347 Color: 9

Bin 756: 6 of cap free
Amount of items: 2
Items: 
Size: 569485 Color: 9
Size: 430510 Color: 7

Bin 757: 6 of cap free
Amount of items: 2
Items: 
Size: 569743 Color: 2
Size: 430252 Color: 3

Bin 758: 6 of cap free
Amount of items: 2
Items: 
Size: 575750 Color: 2
Size: 424245 Color: 19

Bin 759: 6 of cap free
Amount of items: 2
Items: 
Size: 581331 Color: 5
Size: 418664 Color: 11

Bin 760: 6 of cap free
Amount of items: 2
Items: 
Size: 582742 Color: 15
Size: 417253 Color: 16

Bin 761: 6 of cap free
Amount of items: 2
Items: 
Size: 589134 Color: 19
Size: 410861 Color: 17

Bin 762: 6 of cap free
Amount of items: 2
Items: 
Size: 590306 Color: 9
Size: 409689 Color: 12

Bin 763: 6 of cap free
Amount of items: 2
Items: 
Size: 595447 Color: 17
Size: 404548 Color: 3

Bin 764: 6 of cap free
Amount of items: 2
Items: 
Size: 595568 Color: 5
Size: 404427 Color: 9

Bin 765: 6 of cap free
Amount of items: 3
Items: 
Size: 601344 Color: 14
Size: 201579 Color: 8
Size: 197072 Color: 4

Bin 766: 6 of cap free
Amount of items: 2
Items: 
Size: 603722 Color: 14
Size: 396273 Color: 17

Bin 767: 6 of cap free
Amount of items: 2
Items: 
Size: 604382 Color: 1
Size: 395613 Color: 4

Bin 768: 6 of cap free
Amount of items: 2
Items: 
Size: 609607 Color: 12
Size: 390388 Color: 8

Bin 769: 6 of cap free
Amount of items: 3
Items: 
Size: 612394 Color: 16
Size: 194503 Color: 9
Size: 193098 Color: 19

Bin 770: 6 of cap free
Amount of items: 2
Items: 
Size: 613084 Color: 1
Size: 386911 Color: 4

Bin 771: 6 of cap free
Amount of items: 2
Items: 
Size: 613112 Color: 16
Size: 386883 Color: 2

Bin 772: 6 of cap free
Amount of items: 2
Items: 
Size: 616658 Color: 0
Size: 383337 Color: 14

Bin 773: 6 of cap free
Amount of items: 2
Items: 
Size: 618992 Color: 6
Size: 381003 Color: 0

Bin 774: 6 of cap free
Amount of items: 3
Items: 
Size: 619582 Color: 7
Size: 196710 Color: 16
Size: 183703 Color: 15

Bin 775: 6 of cap free
Amount of items: 3
Items: 
Size: 620952 Color: 15
Size: 192264 Color: 8
Size: 186779 Color: 3

Bin 776: 6 of cap free
Amount of items: 3
Items: 
Size: 624644 Color: 18
Size: 188570 Color: 11
Size: 186781 Color: 9

Bin 777: 6 of cap free
Amount of items: 3
Items: 
Size: 624735 Color: 3
Size: 190748 Color: 3
Size: 184512 Color: 0

Bin 778: 6 of cap free
Amount of items: 2
Items: 
Size: 628291 Color: 8
Size: 371704 Color: 5

Bin 779: 6 of cap free
Amount of items: 2
Items: 
Size: 629380 Color: 10
Size: 370615 Color: 1

Bin 780: 6 of cap free
Amount of items: 3
Items: 
Size: 629629 Color: 12
Size: 189731 Color: 17
Size: 180635 Color: 4

Bin 781: 6 of cap free
Amount of items: 3
Items: 
Size: 631591 Color: 12
Size: 187466 Color: 6
Size: 180938 Color: 6

Bin 782: 6 of cap free
Amount of items: 2
Items: 
Size: 637560 Color: 0
Size: 362435 Color: 18

Bin 783: 6 of cap free
Amount of items: 3
Items: 
Size: 648847 Color: 15
Size: 186983 Color: 13
Size: 164165 Color: 5

Bin 784: 6 of cap free
Amount of items: 2
Items: 
Size: 653473 Color: 15
Size: 346522 Color: 19

Bin 785: 6 of cap free
Amount of items: 2
Items: 
Size: 653755 Color: 16
Size: 346240 Color: 3

Bin 786: 6 of cap free
Amount of items: 2
Items: 
Size: 654013 Color: 11
Size: 345982 Color: 1

Bin 787: 6 of cap free
Amount of items: 3
Items: 
Size: 657134 Color: 15
Size: 178544 Color: 17
Size: 164317 Color: 12

Bin 788: 6 of cap free
Amount of items: 2
Items: 
Size: 660947 Color: 1
Size: 339048 Color: 10

Bin 789: 6 of cap free
Amount of items: 2
Items: 
Size: 679799 Color: 16
Size: 320196 Color: 18

Bin 790: 6 of cap free
Amount of items: 2
Items: 
Size: 682346 Color: 10
Size: 317649 Color: 12

Bin 791: 6 of cap free
Amount of items: 2
Items: 
Size: 682687 Color: 9
Size: 317308 Color: 15

Bin 792: 6 of cap free
Amount of items: 2
Items: 
Size: 683228 Color: 18
Size: 316767 Color: 14

Bin 793: 6 of cap free
Amount of items: 2
Items: 
Size: 686644 Color: 6
Size: 313351 Color: 3

Bin 794: 6 of cap free
Amount of items: 2
Items: 
Size: 688414 Color: 18
Size: 311581 Color: 5

Bin 795: 6 of cap free
Amount of items: 2
Items: 
Size: 697160 Color: 17
Size: 302835 Color: 0

Bin 796: 6 of cap free
Amount of items: 2
Items: 
Size: 711858 Color: 7
Size: 288137 Color: 11

Bin 797: 6 of cap free
Amount of items: 3
Items: 
Size: 715909 Color: 19
Size: 144647 Color: 18
Size: 139439 Color: 14

Bin 798: 6 of cap free
Amount of items: 3
Items: 
Size: 717032 Color: 15
Size: 142666 Color: 7
Size: 140297 Color: 10

Bin 799: 6 of cap free
Amount of items: 3
Items: 
Size: 723403 Color: 1
Size: 149754 Color: 16
Size: 126838 Color: 14

Bin 800: 6 of cap free
Amount of items: 2
Items: 
Size: 732678 Color: 18
Size: 267317 Color: 12

Bin 801: 6 of cap free
Amount of items: 2
Items: 
Size: 736218 Color: 13
Size: 263777 Color: 18

Bin 802: 6 of cap free
Amount of items: 3
Items: 
Size: 739166 Color: 18
Size: 140391 Color: 15
Size: 120438 Color: 15

Bin 803: 6 of cap free
Amount of items: 3
Items: 
Size: 739887 Color: 0
Size: 135344 Color: 5
Size: 124764 Color: 11

Bin 804: 6 of cap free
Amount of items: 2
Items: 
Size: 764412 Color: 15
Size: 235583 Color: 3

Bin 805: 6 of cap free
Amount of items: 2
Items: 
Size: 767728 Color: 12
Size: 232267 Color: 11

Bin 806: 6 of cap free
Amount of items: 3
Items: 
Size: 773097 Color: 0
Size: 116995 Color: 1
Size: 109903 Color: 10

Bin 807: 6 of cap free
Amount of items: 3
Items: 
Size: 774618 Color: 18
Size: 114851 Color: 12
Size: 110526 Color: 10

Bin 808: 6 of cap free
Amount of items: 3
Items: 
Size: 776221 Color: 4
Size: 115077 Color: 8
Size: 108697 Color: 3

Bin 809: 6 of cap free
Amount of items: 3
Items: 
Size: 779621 Color: 14
Size: 111285 Color: 10
Size: 109089 Color: 10

Bin 810: 6 of cap free
Amount of items: 3
Items: 
Size: 783748 Color: 8
Size: 110185 Color: 1
Size: 106062 Color: 19

Bin 811: 6 of cap free
Amount of items: 2
Items: 
Size: 786067 Color: 6
Size: 213928 Color: 12

Bin 812: 6 of cap free
Amount of items: 2
Items: 
Size: 786214 Color: 19
Size: 213781 Color: 18

Bin 813: 6 of cap free
Amount of items: 3
Items: 
Size: 786340 Color: 17
Size: 107273 Color: 1
Size: 106382 Color: 18

Bin 814: 6 of cap free
Amount of items: 3
Items: 
Size: 796104 Color: 2
Size: 102021 Color: 9
Size: 101870 Color: 7

Bin 815: 7 of cap free
Amount of items: 3
Items: 
Size: 350877 Color: 8
Size: 341346 Color: 3
Size: 307771 Color: 11

Bin 816: 7 of cap free
Amount of items: 3
Items: 
Size: 359844 Color: 18
Size: 353128 Color: 12
Size: 287022 Color: 14

Bin 817: 7 of cap free
Amount of items: 3
Items: 
Size: 365310 Color: 12
Size: 354668 Color: 5
Size: 280016 Color: 4

Bin 818: 7 of cap free
Amount of items: 3
Items: 
Size: 369953 Color: 7
Size: 368358 Color: 1
Size: 261683 Color: 12

Bin 819: 7 of cap free
Amount of items: 3
Items: 
Size: 371450 Color: 7
Size: 357461 Color: 11
Size: 271083 Color: 5

Bin 820: 7 of cap free
Amount of items: 3
Items: 
Size: 374129 Color: 8
Size: 360371 Color: 1
Size: 265494 Color: 3

Bin 821: 7 of cap free
Amount of items: 3
Items: 
Size: 390180 Color: 12
Size: 344451 Color: 3
Size: 265363 Color: 0

Bin 822: 7 of cap free
Amount of items: 3
Items: 
Size: 414544 Color: 19
Size: 311035 Color: 8
Size: 274415 Color: 3

Bin 823: 7 of cap free
Amount of items: 3
Items: 
Size: 431377 Color: 17
Size: 304606 Color: 1
Size: 264011 Color: 1

Bin 824: 7 of cap free
Amount of items: 2
Items: 
Size: 501729 Color: 6
Size: 498265 Color: 13

Bin 825: 7 of cap free
Amount of items: 2
Items: 
Size: 501997 Color: 17
Size: 497997 Color: 13

Bin 826: 7 of cap free
Amount of items: 2
Items: 
Size: 502982 Color: 9
Size: 497012 Color: 3

Bin 827: 7 of cap free
Amount of items: 2
Items: 
Size: 516442 Color: 5
Size: 483552 Color: 1

Bin 828: 7 of cap free
Amount of items: 2
Items: 
Size: 516744 Color: 16
Size: 483250 Color: 15

Bin 829: 7 of cap free
Amount of items: 2
Items: 
Size: 517227 Color: 13
Size: 482767 Color: 1

Bin 830: 7 of cap free
Amount of items: 2
Items: 
Size: 553067 Color: 6
Size: 446927 Color: 12

Bin 831: 7 of cap free
Amount of items: 2
Items: 
Size: 559652 Color: 19
Size: 440342 Color: 2

Bin 832: 7 of cap free
Amount of items: 2
Items: 
Size: 560130 Color: 9
Size: 439864 Color: 16

Bin 833: 7 of cap free
Amount of items: 3
Items: 
Size: 563021 Color: 2
Size: 218745 Color: 3
Size: 218228 Color: 12

Bin 834: 7 of cap free
Amount of items: 2
Items: 
Size: 572407 Color: 11
Size: 427587 Color: 14

Bin 835: 7 of cap free
Amount of items: 2
Items: 
Size: 580363 Color: 1
Size: 419631 Color: 0

Bin 836: 7 of cap free
Amount of items: 2
Items: 
Size: 582083 Color: 11
Size: 417911 Color: 13

Bin 837: 7 of cap free
Amount of items: 2
Items: 
Size: 586941 Color: 10
Size: 413053 Color: 13

Bin 838: 7 of cap free
Amount of items: 2
Items: 
Size: 587443 Color: 0
Size: 412551 Color: 3

Bin 839: 7 of cap free
Amount of items: 2
Items: 
Size: 597592 Color: 6
Size: 402402 Color: 10

Bin 840: 7 of cap free
Amount of items: 3
Items: 
Size: 624543 Color: 13
Size: 192592 Color: 17
Size: 182859 Color: 15

Bin 841: 7 of cap free
Amount of items: 3
Items: 
Size: 628408 Color: 5
Size: 189253 Color: 0
Size: 182333 Color: 8

Bin 842: 7 of cap free
Amount of items: 3
Items: 
Size: 630027 Color: 9
Size: 195794 Color: 12
Size: 174173 Color: 15

Bin 843: 7 of cap free
Amount of items: 2
Items: 
Size: 635558 Color: 7
Size: 364436 Color: 3

Bin 844: 7 of cap free
Amount of items: 2
Items: 
Size: 637021 Color: 7
Size: 362973 Color: 0

Bin 845: 7 of cap free
Amount of items: 2
Items: 
Size: 649917 Color: 5
Size: 350077 Color: 0

Bin 846: 7 of cap free
Amount of items: 3
Items: 
Size: 650991 Color: 2
Size: 189318 Color: 13
Size: 159685 Color: 11

Bin 847: 7 of cap free
Amount of items: 3
Items: 
Size: 653086 Color: 16
Size: 182236 Color: 13
Size: 164672 Color: 19

Bin 848: 7 of cap free
Amount of items: 2
Items: 
Size: 658731 Color: 10
Size: 341263 Color: 12

Bin 849: 7 of cap free
Amount of items: 3
Items: 
Size: 679955 Color: 18
Size: 163087 Color: 14
Size: 156952 Color: 18

Bin 850: 7 of cap free
Amount of items: 2
Items: 
Size: 687059 Color: 1
Size: 312935 Color: 0

Bin 851: 7 of cap free
Amount of items: 2
Items: 
Size: 703120 Color: 2
Size: 296874 Color: 5

Bin 852: 7 of cap free
Amount of items: 2
Items: 
Size: 740104 Color: 18
Size: 259890 Color: 15

Bin 853: 7 of cap free
Amount of items: 2
Items: 
Size: 747791 Color: 4
Size: 252203 Color: 3

Bin 854: 7 of cap free
Amount of items: 2
Items: 
Size: 753081 Color: 16
Size: 246913 Color: 15

Bin 855: 7 of cap free
Amount of items: 2
Items: 
Size: 753373 Color: 4
Size: 246621 Color: 9

Bin 856: 7 of cap free
Amount of items: 2
Items: 
Size: 764734 Color: 17
Size: 235260 Color: 2

Bin 857: 7 of cap free
Amount of items: 2
Items: 
Size: 767473 Color: 6
Size: 232521 Color: 19

Bin 858: 7 of cap free
Amount of items: 2
Items: 
Size: 769153 Color: 5
Size: 230841 Color: 0

Bin 859: 7 of cap free
Amount of items: 3
Items: 
Size: 773218 Color: 17
Size: 115178 Color: 4
Size: 111598 Color: 8

Bin 860: 7 of cap free
Amount of items: 2
Items: 
Size: 775249 Color: 10
Size: 224745 Color: 7

Bin 861: 7 of cap free
Amount of items: 2
Items: 
Size: 777086 Color: 1
Size: 222908 Color: 2

Bin 862: 7 of cap free
Amount of items: 2
Items: 
Size: 787790 Color: 4
Size: 212204 Color: 0

Bin 863: 7 of cap free
Amount of items: 3
Items: 
Size: 796880 Color: 3
Size: 101560 Color: 12
Size: 101554 Color: 16

Bin 864: 8 of cap free
Amount of items: 3
Items: 
Size: 362238 Color: 0
Size: 351384 Color: 13
Size: 286371 Color: 6

Bin 865: 8 of cap free
Amount of items: 3
Items: 
Size: 363840 Color: 11
Size: 327501 Color: 15
Size: 308652 Color: 16

Bin 866: 8 of cap free
Amount of items: 3
Items: 
Size: 369950 Color: 15
Size: 343159 Color: 2
Size: 286884 Color: 12

Bin 867: 8 of cap free
Amount of items: 2
Items: 
Size: 503698 Color: 4
Size: 496295 Color: 14

Bin 868: 8 of cap free
Amount of items: 2
Items: 
Size: 523487 Color: 3
Size: 476506 Color: 8

Bin 869: 8 of cap free
Amount of items: 2
Items: 
Size: 531715 Color: 13
Size: 468278 Color: 8

Bin 870: 8 of cap free
Amount of items: 2
Items: 
Size: 552463 Color: 6
Size: 447530 Color: 19

Bin 871: 8 of cap free
Amount of items: 2
Items: 
Size: 565923 Color: 8
Size: 434070 Color: 4

Bin 872: 8 of cap free
Amount of items: 3
Items: 
Size: 578571 Color: 16
Size: 224097 Color: 19
Size: 197325 Color: 10

Bin 873: 8 of cap free
Amount of items: 2
Items: 
Size: 587834 Color: 18
Size: 412159 Color: 4

Bin 874: 8 of cap free
Amount of items: 2
Items: 
Size: 598970 Color: 8
Size: 401023 Color: 16

Bin 875: 8 of cap free
Amount of items: 2
Items: 
Size: 602897 Color: 6
Size: 397096 Color: 9

Bin 876: 8 of cap free
Amount of items: 2
Items: 
Size: 618788 Color: 3
Size: 381205 Color: 7

Bin 877: 8 of cap free
Amount of items: 3
Items: 
Size: 621766 Color: 7
Size: 190016 Color: 3
Size: 188211 Color: 9

Bin 878: 8 of cap free
Amount of items: 3
Items: 
Size: 622390 Color: 13
Size: 219503 Color: 18
Size: 158100 Color: 11

Bin 879: 8 of cap free
Amount of items: 3
Items: 
Size: 626920 Color: 15
Size: 198395 Color: 5
Size: 174678 Color: 11

Bin 880: 8 of cap free
Amount of items: 3
Items: 
Size: 630684 Color: 6
Size: 188137 Color: 10
Size: 181172 Color: 12

Bin 881: 8 of cap free
Amount of items: 3
Items: 
Size: 632495 Color: 13
Size: 197531 Color: 8
Size: 169967 Color: 8

Bin 882: 8 of cap free
Amount of items: 3
Items: 
Size: 633412 Color: 7
Size: 184865 Color: 6
Size: 181716 Color: 8

Bin 883: 8 of cap free
Amount of items: 2
Items: 
Size: 640838 Color: 10
Size: 359155 Color: 12

Bin 884: 8 of cap free
Amount of items: 3
Items: 
Size: 656618 Color: 6
Size: 177666 Color: 9
Size: 165709 Color: 17

Bin 885: 8 of cap free
Amount of items: 3
Items: 
Size: 656786 Color: 0
Size: 177133 Color: 5
Size: 166074 Color: 2

Bin 886: 8 of cap free
Amount of items: 3
Items: 
Size: 657894 Color: 5
Size: 173435 Color: 18
Size: 168664 Color: 17

Bin 887: 8 of cap free
Amount of items: 2
Items: 
Size: 659562 Color: 16
Size: 340431 Color: 15

Bin 888: 8 of cap free
Amount of items: 3
Items: 
Size: 663763 Color: 0
Size: 178866 Color: 11
Size: 157364 Color: 2

Bin 889: 8 of cap free
Amount of items: 2
Items: 
Size: 676021 Color: 19
Size: 323972 Color: 3

Bin 890: 8 of cap free
Amount of items: 2
Items: 
Size: 686139 Color: 4
Size: 313854 Color: 13

Bin 891: 8 of cap free
Amount of items: 2
Items: 
Size: 686441 Color: 4
Size: 313552 Color: 19

Bin 892: 8 of cap free
Amount of items: 3
Items: 
Size: 692475 Color: 18
Size: 154210 Color: 4
Size: 153308 Color: 12

Bin 893: 8 of cap free
Amount of items: 2
Items: 
Size: 696549 Color: 1
Size: 303444 Color: 6

Bin 894: 8 of cap free
Amount of items: 2
Items: 
Size: 714686 Color: 13
Size: 285307 Color: 10

Bin 895: 8 of cap free
Amount of items: 3
Items: 
Size: 736020 Color: 4
Size: 140727 Color: 9
Size: 123246 Color: 16

Bin 896: 8 of cap free
Amount of items: 2
Items: 
Size: 766755 Color: 11
Size: 233238 Color: 5

Bin 897: 8 of cap free
Amount of items: 2
Items: 
Size: 770709 Color: 4
Size: 229284 Color: 19

Bin 898: 8 of cap free
Amount of items: 2
Items: 
Size: 772145 Color: 10
Size: 227848 Color: 16

Bin 899: 8 of cap free
Amount of items: 2
Items: 
Size: 772814 Color: 6
Size: 227179 Color: 17

Bin 900: 8 of cap free
Amount of items: 2
Items: 
Size: 777840 Color: 8
Size: 222153 Color: 16

Bin 901: 8 of cap free
Amount of items: 3
Items: 
Size: 779448 Color: 13
Size: 111813 Color: 11
Size: 108732 Color: 5

Bin 902: 8 of cap free
Amount of items: 2
Items: 
Size: 783830 Color: 1
Size: 216163 Color: 8

Bin 903: 8 of cap free
Amount of items: 2
Items: 
Size: 791558 Color: 18
Size: 208435 Color: 2

Bin 904: 8 of cap free
Amount of items: 2
Items: 
Size: 792566 Color: 17
Size: 207427 Color: 2

Bin 905: 9 of cap free
Amount of items: 3
Items: 
Size: 371512 Color: 4
Size: 350448 Color: 14
Size: 278032 Color: 6

Bin 906: 9 of cap free
Amount of items: 3
Items: 
Size: 373572 Color: 8
Size: 354890 Color: 16
Size: 271530 Color: 9

Bin 907: 9 of cap free
Amount of items: 3
Items: 
Size: 416797 Color: 17
Size: 308136 Color: 3
Size: 275059 Color: 5

Bin 908: 9 of cap free
Amount of items: 3
Items: 
Size: 422409 Color: 8
Size: 309078 Color: 4
Size: 268505 Color: 2

Bin 909: 9 of cap free
Amount of items: 2
Items: 
Size: 508547 Color: 4
Size: 491445 Color: 0

Bin 910: 9 of cap free
Amount of items: 2
Items: 
Size: 510273 Color: 17
Size: 489719 Color: 19

Bin 911: 9 of cap free
Amount of items: 2
Items: 
Size: 510829 Color: 4
Size: 489163 Color: 7

Bin 912: 9 of cap free
Amount of items: 2
Items: 
Size: 514933 Color: 12
Size: 485059 Color: 3

Bin 913: 9 of cap free
Amount of items: 2
Items: 
Size: 515721 Color: 10
Size: 484271 Color: 16

Bin 914: 9 of cap free
Amount of items: 2
Items: 
Size: 543746 Color: 3
Size: 456246 Color: 13

Bin 915: 9 of cap free
Amount of items: 2
Items: 
Size: 564796 Color: 3
Size: 435196 Color: 6

Bin 916: 9 of cap free
Amount of items: 2
Items: 
Size: 576724 Color: 14
Size: 423268 Color: 6

Bin 917: 9 of cap free
Amount of items: 2
Items: 
Size: 578656 Color: 5
Size: 421336 Color: 6

Bin 918: 9 of cap free
Amount of items: 2
Items: 
Size: 613928 Color: 10
Size: 386064 Color: 1

Bin 919: 9 of cap free
Amount of items: 2
Items: 
Size: 615529 Color: 12
Size: 384463 Color: 16

Bin 920: 9 of cap free
Amount of items: 2
Items: 
Size: 616483 Color: 8
Size: 383509 Color: 15

Bin 921: 9 of cap free
Amount of items: 2
Items: 
Size: 619290 Color: 5
Size: 380702 Color: 11

Bin 922: 9 of cap free
Amount of items: 2
Items: 
Size: 623260 Color: 0
Size: 376732 Color: 1

Bin 923: 9 of cap free
Amount of items: 3
Items: 
Size: 630728 Color: 13
Size: 196676 Color: 6
Size: 172588 Color: 8

Bin 924: 9 of cap free
Amount of items: 2
Items: 
Size: 637202 Color: 13
Size: 362790 Color: 6

Bin 925: 9 of cap free
Amount of items: 2
Items: 
Size: 649340 Color: 3
Size: 350652 Color: 9

Bin 926: 9 of cap free
Amount of items: 2
Items: 
Size: 650677 Color: 13
Size: 349315 Color: 3

Bin 927: 9 of cap free
Amount of items: 2
Items: 
Size: 653885 Color: 14
Size: 346107 Color: 15

Bin 928: 9 of cap free
Amount of items: 2
Items: 
Size: 655015 Color: 15
Size: 344977 Color: 11

Bin 929: 9 of cap free
Amount of items: 3
Items: 
Size: 661791 Color: 14
Size: 169213 Color: 17
Size: 168988 Color: 16

Bin 930: 9 of cap free
Amount of items: 2
Items: 
Size: 665808 Color: 10
Size: 334184 Color: 15

Bin 931: 9 of cap free
Amount of items: 2
Items: 
Size: 667714 Color: 7
Size: 332278 Color: 10

Bin 932: 9 of cap free
Amount of items: 2
Items: 
Size: 668190 Color: 6
Size: 331802 Color: 17

Bin 933: 9 of cap free
Amount of items: 2
Items: 
Size: 684466 Color: 16
Size: 315526 Color: 1

Bin 934: 9 of cap free
Amount of items: 2
Items: 
Size: 706502 Color: 16
Size: 293490 Color: 2

Bin 935: 9 of cap free
Amount of items: 3
Items: 
Size: 712668 Color: 11
Size: 144577 Color: 18
Size: 142747 Color: 5

Bin 936: 9 of cap free
Amount of items: 3
Items: 
Size: 713039 Color: 8
Size: 148945 Color: 14
Size: 138008 Color: 17

Bin 937: 9 of cap free
Amount of items: 2
Items: 
Size: 712945 Color: 4
Size: 287047 Color: 7

Bin 938: 9 of cap free
Amount of items: 3
Items: 
Size: 712963 Color: 17
Size: 148574 Color: 7
Size: 138455 Color: 5

Bin 939: 9 of cap free
Amount of items: 3
Items: 
Size: 713145 Color: 18
Size: 152446 Color: 15
Size: 134401 Color: 10

Bin 940: 9 of cap free
Amount of items: 3
Items: 
Size: 720875 Color: 3
Size: 155852 Color: 13
Size: 123265 Color: 6

Bin 941: 9 of cap free
Amount of items: 2
Items: 
Size: 727638 Color: 7
Size: 272354 Color: 19

Bin 942: 9 of cap free
Amount of items: 2
Items: 
Size: 737406 Color: 18
Size: 262586 Color: 5

Bin 943: 9 of cap free
Amount of items: 3
Items: 
Size: 737685 Color: 16
Size: 137550 Color: 3
Size: 124757 Color: 8

Bin 944: 9 of cap free
Amount of items: 2
Items: 
Size: 739759 Color: 17
Size: 260233 Color: 11

Bin 945: 9 of cap free
Amount of items: 2
Items: 
Size: 740624 Color: 19
Size: 259368 Color: 9

Bin 946: 9 of cap free
Amount of items: 2
Items: 
Size: 765107 Color: 12
Size: 234885 Color: 10

Bin 947: 9 of cap free
Amount of items: 2
Items: 
Size: 768152 Color: 14
Size: 231840 Color: 11

Bin 948: 9 of cap free
Amount of items: 3
Items: 
Size: 774594 Color: 2
Size: 116773 Color: 19
Size: 108625 Color: 8

Bin 949: 10 of cap free
Amount of items: 3
Items: 
Size: 371414 Color: 12
Size: 360713 Color: 18
Size: 267864 Color: 12

Bin 950: 10 of cap free
Amount of items: 2
Items: 
Size: 506888 Color: 17
Size: 493103 Color: 5

Bin 951: 10 of cap free
Amount of items: 2
Items: 
Size: 517400 Color: 10
Size: 482591 Color: 3

Bin 952: 10 of cap free
Amount of items: 2
Items: 
Size: 523655 Color: 9
Size: 476336 Color: 2

Bin 953: 10 of cap free
Amount of items: 2
Items: 
Size: 523911 Color: 13
Size: 476080 Color: 5

Bin 954: 10 of cap free
Amount of items: 2
Items: 
Size: 527705 Color: 9
Size: 472286 Color: 8

Bin 955: 10 of cap free
Amount of items: 2
Items: 
Size: 530103 Color: 6
Size: 469888 Color: 15

Bin 956: 10 of cap free
Amount of items: 2
Items: 
Size: 533093 Color: 7
Size: 466898 Color: 6

Bin 957: 10 of cap free
Amount of items: 2
Items: 
Size: 536345 Color: 12
Size: 463646 Color: 0

Bin 958: 10 of cap free
Amount of items: 3
Items: 
Size: 547586 Color: 13
Size: 228604 Color: 18
Size: 223801 Color: 0

Bin 959: 10 of cap free
Amount of items: 2
Items: 
Size: 550138 Color: 10
Size: 449853 Color: 1

Bin 960: 10 of cap free
Amount of items: 2
Items: 
Size: 553203 Color: 8
Size: 446788 Color: 18

Bin 961: 10 of cap free
Amount of items: 2
Items: 
Size: 599459 Color: 10
Size: 400532 Color: 15

Bin 962: 10 of cap free
Amount of items: 2
Items: 
Size: 605926 Color: 12
Size: 394065 Color: 4

Bin 963: 10 of cap free
Amount of items: 2
Items: 
Size: 612371 Color: 12
Size: 387620 Color: 11

Bin 964: 10 of cap free
Amount of items: 2
Items: 
Size: 616394 Color: 8
Size: 383597 Color: 1

Bin 965: 10 of cap free
Amount of items: 2
Items: 
Size: 617440 Color: 4
Size: 382551 Color: 7

Bin 966: 10 of cap free
Amount of items: 2
Items: 
Size: 621421 Color: 7
Size: 378570 Color: 13

Bin 967: 10 of cap free
Amount of items: 2
Items: 
Size: 623280 Color: 17
Size: 376711 Color: 9

Bin 968: 10 of cap free
Amount of items: 2
Items: 
Size: 624510 Color: 4
Size: 375481 Color: 11

Bin 969: 10 of cap free
Amount of items: 3
Items: 
Size: 631200 Color: 16
Size: 185623 Color: 0
Size: 183168 Color: 14

Bin 970: 10 of cap free
Amount of items: 3
Items: 
Size: 633734 Color: 13
Size: 184982 Color: 19
Size: 181275 Color: 8

Bin 971: 10 of cap free
Amount of items: 2
Items: 
Size: 642708 Color: 7
Size: 357283 Color: 14

Bin 972: 10 of cap free
Amount of items: 2
Items: 
Size: 643366 Color: 16
Size: 356625 Color: 18

Bin 973: 10 of cap free
Amount of items: 3
Items: 
Size: 656029 Color: 1
Size: 176001 Color: 17
Size: 167961 Color: 3

Bin 974: 10 of cap free
Amount of items: 3
Items: 
Size: 661942 Color: 1
Size: 174345 Color: 7
Size: 163704 Color: 2

Bin 975: 10 of cap free
Amount of items: 2
Items: 
Size: 666615 Color: 1
Size: 333376 Color: 15

Bin 976: 10 of cap free
Amount of items: 2
Items: 
Size: 671605 Color: 5
Size: 328386 Color: 10

Bin 977: 10 of cap free
Amount of items: 2
Items: 
Size: 672144 Color: 8
Size: 327847 Color: 17

Bin 978: 10 of cap free
Amount of items: 2
Items: 
Size: 676582 Color: 2
Size: 323409 Color: 9

Bin 979: 10 of cap free
Amount of items: 2
Items: 
Size: 679048 Color: 4
Size: 320943 Color: 6

Bin 980: 10 of cap free
Amount of items: 2
Items: 
Size: 682427 Color: 2
Size: 317564 Color: 13

Bin 981: 10 of cap free
Amount of items: 3
Items: 
Size: 691874 Color: 9
Size: 154107 Color: 7
Size: 154010 Color: 4

Bin 982: 10 of cap free
Amount of items: 2
Items: 
Size: 695807 Color: 19
Size: 304184 Color: 5

Bin 983: 10 of cap free
Amount of items: 2
Items: 
Size: 698264 Color: 17
Size: 301727 Color: 9

Bin 984: 10 of cap free
Amount of items: 2
Items: 
Size: 699186 Color: 3
Size: 300805 Color: 2

Bin 985: 10 of cap free
Amount of items: 2
Items: 
Size: 704528 Color: 9
Size: 295463 Color: 2

Bin 986: 10 of cap free
Amount of items: 2
Items: 
Size: 713376 Color: 18
Size: 286615 Color: 6

Bin 987: 10 of cap free
Amount of items: 2
Items: 
Size: 719557 Color: 12
Size: 280434 Color: 11

Bin 988: 10 of cap free
Amount of items: 2
Items: 
Size: 738754 Color: 0
Size: 261237 Color: 4

Bin 989: 10 of cap free
Amount of items: 2
Items: 
Size: 741309 Color: 13
Size: 258682 Color: 2

Bin 990: 10 of cap free
Amount of items: 2
Items: 
Size: 753819 Color: 2
Size: 246172 Color: 10

Bin 991: 10 of cap free
Amount of items: 3
Items: 
Size: 773500 Color: 13
Size: 114685 Color: 2
Size: 111806 Color: 5

Bin 992: 10 of cap free
Amount of items: 3
Items: 
Size: 777738 Color: 16
Size: 114640 Color: 0
Size: 107613 Color: 18

Bin 993: 10 of cap free
Amount of items: 2
Items: 
Size: 782024 Color: 2
Size: 217967 Color: 12

Bin 994: 10 of cap free
Amount of items: 2
Items: 
Size: 783330 Color: 12
Size: 216661 Color: 8

Bin 995: 10 of cap free
Amount of items: 2
Items: 
Size: 796963 Color: 10
Size: 203028 Color: 2

Bin 996: 10 of cap free
Amount of items: 2
Items: 
Size: 797363 Color: 0
Size: 202628 Color: 6

Bin 997: 10 of cap free
Amount of items: 2
Items: 
Size: 799928 Color: 4
Size: 200063 Color: 19

Bin 998: 11 of cap free
Amount of items: 3
Items: 
Size: 358096 Color: 7
Size: 337075 Color: 15
Size: 304819 Color: 9

Bin 999: 11 of cap free
Amount of items: 3
Items: 
Size: 391822 Color: 7
Size: 348394 Color: 1
Size: 259774 Color: 4

Bin 1000: 11 of cap free
Amount of items: 2
Items: 
Size: 504769 Color: 18
Size: 495221 Color: 4

Bin 1001: 11 of cap free
Amount of items: 2
Items: 
Size: 521352 Color: 8
Size: 478638 Color: 15

Bin 1002: 11 of cap free
Amount of items: 2
Items: 
Size: 524280 Color: 10
Size: 475710 Color: 0

Bin 1003: 11 of cap free
Amount of items: 2
Items: 
Size: 525042 Color: 0
Size: 474948 Color: 16

Bin 1004: 11 of cap free
Amount of items: 2
Items: 
Size: 534280 Color: 6
Size: 465710 Color: 8

Bin 1005: 11 of cap free
Amount of items: 2
Items: 
Size: 539168 Color: 8
Size: 460822 Color: 12

Bin 1006: 11 of cap free
Amount of items: 2
Items: 
Size: 590109 Color: 18
Size: 409881 Color: 2

Bin 1007: 11 of cap free
Amount of items: 2
Items: 
Size: 595231 Color: 6
Size: 404759 Color: 16

Bin 1008: 11 of cap free
Amount of items: 2
Items: 
Size: 597573 Color: 14
Size: 402417 Color: 7

Bin 1009: 11 of cap free
Amount of items: 2
Items: 
Size: 602434 Color: 16
Size: 397556 Color: 6

Bin 1010: 11 of cap free
Amount of items: 2
Items: 
Size: 617747 Color: 8
Size: 382243 Color: 17

Bin 1011: 11 of cap free
Amount of items: 3
Items: 
Size: 625195 Color: 5
Size: 217036 Color: 14
Size: 157759 Color: 19

Bin 1012: 11 of cap free
Amount of items: 2
Items: 
Size: 625795 Color: 17
Size: 374195 Color: 14

Bin 1013: 11 of cap free
Amount of items: 2
Items: 
Size: 643043 Color: 18
Size: 356947 Color: 4

Bin 1014: 11 of cap free
Amount of items: 3
Items: 
Size: 645243 Color: 4
Size: 181117 Color: 2
Size: 173630 Color: 1

Bin 1015: 11 of cap free
Amount of items: 3
Items: 
Size: 648302 Color: 5
Size: 185169 Color: 2
Size: 166519 Color: 8

Bin 1016: 11 of cap free
Amount of items: 3
Items: 
Size: 653149 Color: 19
Size: 183300 Color: 2
Size: 163541 Color: 5

Bin 1017: 11 of cap free
Amount of items: 3
Items: 
Size: 659473 Color: 14
Size: 178980 Color: 17
Size: 161537 Color: 11

Bin 1018: 11 of cap free
Amount of items: 3
Items: 
Size: 662624 Color: 4
Size: 177298 Color: 17
Size: 160068 Color: 8

Bin 1019: 11 of cap free
Amount of items: 2
Items: 
Size: 670386 Color: 4
Size: 329604 Color: 1

Bin 1020: 11 of cap free
Amount of items: 2
Items: 
Size: 674288 Color: 16
Size: 325702 Color: 10

Bin 1021: 11 of cap free
Amount of items: 2
Items: 
Size: 674388 Color: 18
Size: 325602 Color: 11

Bin 1022: 11 of cap free
Amount of items: 2
Items: 
Size: 675647 Color: 3
Size: 324343 Color: 8

Bin 1023: 11 of cap free
Amount of items: 3
Items: 
Size: 689817 Color: 16
Size: 156631 Color: 2
Size: 153542 Color: 17

Bin 1024: 11 of cap free
Amount of items: 3
Items: 
Size: 691178 Color: 0
Size: 155411 Color: 9
Size: 153401 Color: 11

Bin 1025: 11 of cap free
Amount of items: 2
Items: 
Size: 721114 Color: 8
Size: 278876 Color: 1

Bin 1026: 11 of cap free
Amount of items: 2
Items: 
Size: 721756 Color: 13
Size: 278234 Color: 10

Bin 1027: 11 of cap free
Amount of items: 2
Items: 
Size: 726056 Color: 19
Size: 273934 Color: 16

Bin 1028: 11 of cap free
Amount of items: 2
Items: 
Size: 732322 Color: 3
Size: 267668 Color: 5

Bin 1029: 11 of cap free
Amount of items: 2
Items: 
Size: 734896 Color: 9
Size: 265094 Color: 0

Bin 1030: 11 of cap free
Amount of items: 2
Items: 
Size: 752490 Color: 16
Size: 247500 Color: 9

Bin 1031: 11 of cap free
Amount of items: 3
Items: 
Size: 775372 Color: 15
Size: 115263 Color: 12
Size: 109355 Color: 17

Bin 1032: 11 of cap free
Amount of items: 3
Items: 
Size: 786357 Color: 5
Size: 107783 Color: 2
Size: 105850 Color: 15

Bin 1033: 11 of cap free
Amount of items: 2
Items: 
Size: 788290 Color: 6
Size: 211700 Color: 18

Bin 1034: 11 of cap free
Amount of items: 3
Items: 
Size: 798851 Color: 14
Size: 100823 Color: 2
Size: 100316 Color: 14

Bin 1035: 12 of cap free
Amount of items: 3
Items: 
Size: 369003 Color: 12
Size: 343247 Color: 13
Size: 287739 Color: 6

Bin 1036: 12 of cap free
Amount of items: 3
Items: 
Size: 370751 Color: 14
Size: 359080 Color: 1
Size: 270158 Color: 13

Bin 1037: 12 of cap free
Amount of items: 3
Items: 
Size: 374167 Color: 8
Size: 372989 Color: 1
Size: 252833 Color: 14

Bin 1038: 12 of cap free
Amount of items: 3
Items: 
Size: 390181 Color: 9
Size: 354952 Color: 2
Size: 254856 Color: 11

Bin 1039: 12 of cap free
Amount of items: 2
Items: 
Size: 508974 Color: 8
Size: 491015 Color: 9

Bin 1040: 12 of cap free
Amount of items: 2
Items: 
Size: 529222 Color: 5
Size: 470767 Color: 9

Bin 1041: 12 of cap free
Amount of items: 2
Items: 
Size: 534550 Color: 12
Size: 465439 Color: 16

Bin 1042: 12 of cap free
Amount of items: 2
Items: 
Size: 536827 Color: 14
Size: 463162 Color: 19

Bin 1043: 12 of cap free
Amount of items: 2
Items: 
Size: 547168 Color: 18
Size: 452821 Color: 8

Bin 1044: 12 of cap free
Amount of items: 2
Items: 
Size: 561987 Color: 12
Size: 438002 Color: 16

Bin 1045: 12 of cap free
Amount of items: 2
Items: 
Size: 580731 Color: 12
Size: 419258 Color: 2

Bin 1046: 12 of cap free
Amount of items: 2
Items: 
Size: 587969 Color: 6
Size: 412020 Color: 15

Bin 1047: 12 of cap free
Amount of items: 2
Items: 
Size: 591722 Color: 19
Size: 408267 Color: 17

Bin 1048: 12 of cap free
Amount of items: 2
Items: 
Size: 607970 Color: 18
Size: 392019 Color: 17

Bin 1049: 12 of cap free
Amount of items: 2
Items: 
Size: 611419 Color: 6
Size: 388570 Color: 13

Bin 1050: 12 of cap free
Amount of items: 2
Items: 
Size: 615269 Color: 3
Size: 384720 Color: 17

Bin 1051: 12 of cap free
Amount of items: 2
Items: 
Size: 615697 Color: 15
Size: 384292 Color: 17

Bin 1052: 12 of cap free
Amount of items: 2
Items: 
Size: 616125 Color: 7
Size: 383864 Color: 18

Bin 1053: 12 of cap free
Amount of items: 3
Items: 
Size: 623043 Color: 13
Size: 188658 Color: 17
Size: 188288 Color: 0

Bin 1054: 12 of cap free
Amount of items: 2
Items: 
Size: 659593 Color: 15
Size: 340396 Color: 14

Bin 1055: 12 of cap free
Amount of items: 3
Items: 
Size: 659723 Color: 5
Size: 170296 Color: 17
Size: 169970 Color: 3

Bin 1056: 12 of cap free
Amount of items: 2
Items: 
Size: 665243 Color: 7
Size: 334746 Color: 9

Bin 1057: 12 of cap free
Amount of items: 2
Items: 
Size: 666287 Color: 6
Size: 333702 Color: 15

Bin 1058: 12 of cap free
Amount of items: 2
Items: 
Size: 674516 Color: 18
Size: 325473 Color: 17

Bin 1059: 12 of cap free
Amount of items: 3
Items: 
Size: 681805 Color: 17
Size: 159507 Color: 7
Size: 158677 Color: 7

Bin 1060: 12 of cap free
Amount of items: 2
Items: 
Size: 694368 Color: 2
Size: 305621 Color: 15

Bin 1061: 12 of cap free
Amount of items: 2
Items: 
Size: 698646 Color: 18
Size: 301343 Color: 4

Bin 1062: 12 of cap free
Amount of items: 2
Items: 
Size: 704619 Color: 16
Size: 295370 Color: 4

Bin 1063: 12 of cap free
Amount of items: 2
Items: 
Size: 709517 Color: 12
Size: 290472 Color: 3

Bin 1064: 12 of cap free
Amount of items: 2
Items: 
Size: 711483 Color: 3
Size: 288506 Color: 12

Bin 1065: 12 of cap free
Amount of items: 2
Items: 
Size: 718324 Color: 1
Size: 281665 Color: 10

Bin 1066: 12 of cap free
Amount of items: 3
Items: 
Size: 718939 Color: 8
Size: 155913 Color: 13
Size: 125137 Color: 1

Bin 1067: 12 of cap free
Amount of items: 2
Items: 
Size: 722547 Color: 0
Size: 277442 Color: 19

Bin 1068: 12 of cap free
Amount of items: 2
Items: 
Size: 726103 Color: 4
Size: 273886 Color: 13

Bin 1069: 12 of cap free
Amount of items: 3
Items: 
Size: 738850 Color: 3
Size: 135836 Color: 11
Size: 125303 Color: 8

Bin 1070: 12 of cap free
Amount of items: 2
Items: 
Size: 755512 Color: 8
Size: 244477 Color: 9

Bin 1071: 12 of cap free
Amount of items: 2
Items: 
Size: 773482 Color: 2
Size: 226507 Color: 3

Bin 1072: 12 of cap free
Amount of items: 3
Items: 
Size: 778589 Color: 2
Size: 112695 Color: 3
Size: 108705 Color: 3

Bin 1073: 12 of cap free
Amount of items: 3
Items: 
Size: 785522 Color: 4
Size: 108314 Color: 15
Size: 106153 Color: 5

Bin 1074: 12 of cap free
Amount of items: 2
Items: 
Size: 786649 Color: 12
Size: 213340 Color: 7

Bin 1075: 12 of cap free
Amount of items: 2
Items: 
Size: 796431 Color: 14
Size: 203558 Color: 2

Bin 1076: 13 of cap free
Amount of items: 3
Items: 
Size: 361260 Color: 10
Size: 327777 Color: 2
Size: 310951 Color: 6

Bin 1077: 13 of cap free
Amount of items: 3
Items: 
Size: 361543 Color: 8
Size: 352102 Color: 9
Size: 286343 Color: 14

Bin 1078: 13 of cap free
Amount of items: 3
Items: 
Size: 365158 Color: 8
Size: 346995 Color: 6
Size: 287835 Color: 3

Bin 1079: 13 of cap free
Amount of items: 3
Items: 
Size: 369071 Color: 1
Size: 353705 Color: 13
Size: 277212 Color: 2

Bin 1080: 13 of cap free
Amount of items: 3
Items: 
Size: 416861 Color: 10
Size: 307776 Color: 7
Size: 275351 Color: 2

Bin 1081: 13 of cap free
Amount of items: 2
Items: 
Size: 521352 Color: 7
Size: 478636 Color: 16

Bin 1082: 13 of cap free
Amount of items: 2
Items: 
Size: 528760 Color: 6
Size: 471228 Color: 9

Bin 1083: 13 of cap free
Amount of items: 2
Items: 
Size: 544424 Color: 8
Size: 455564 Color: 11

Bin 1084: 13 of cap free
Amount of items: 2
Items: 
Size: 544957 Color: 1
Size: 455031 Color: 18

Bin 1085: 13 of cap free
Amount of items: 2
Items: 
Size: 549309 Color: 14
Size: 450679 Color: 6

Bin 1086: 13 of cap free
Amount of items: 2
Items: 
Size: 550893 Color: 11
Size: 449095 Color: 2

Bin 1087: 13 of cap free
Amount of items: 2
Items: 
Size: 559639 Color: 7
Size: 440349 Color: 0

Bin 1088: 13 of cap free
Amount of items: 2
Items: 
Size: 562546 Color: 12
Size: 437442 Color: 10

Bin 1089: 13 of cap free
Amount of items: 2
Items: 
Size: 572898 Color: 5
Size: 427090 Color: 15

Bin 1090: 13 of cap free
Amount of items: 2
Items: 
Size: 574563 Color: 7
Size: 425425 Color: 2

Bin 1091: 13 of cap free
Amount of items: 2
Items: 
Size: 575428 Color: 4
Size: 424560 Color: 17

Bin 1092: 13 of cap free
Amount of items: 2
Items: 
Size: 613577 Color: 7
Size: 386411 Color: 19

Bin 1093: 13 of cap free
Amount of items: 3
Items: 
Size: 622144 Color: 19
Size: 217184 Color: 8
Size: 160660 Color: 7

Bin 1094: 13 of cap free
Amount of items: 2
Items: 
Size: 622267 Color: 18
Size: 377721 Color: 14

Bin 1095: 13 of cap free
Amount of items: 3
Items: 
Size: 628440 Color: 5
Size: 191010 Color: 19
Size: 180538 Color: 18

Bin 1096: 13 of cap free
Amount of items: 2
Items: 
Size: 633984 Color: 2
Size: 366004 Color: 3

Bin 1097: 13 of cap free
Amount of items: 2
Items: 
Size: 639785 Color: 14
Size: 360203 Color: 12

Bin 1098: 13 of cap free
Amount of items: 2
Items: 
Size: 639914 Color: 16
Size: 360074 Color: 19

Bin 1099: 13 of cap free
Amount of items: 2
Items: 
Size: 640819 Color: 4
Size: 359169 Color: 19

Bin 1100: 13 of cap free
Amount of items: 2
Items: 
Size: 655030 Color: 19
Size: 344958 Color: 5

Bin 1101: 13 of cap free
Amount of items: 2
Items: 
Size: 680069 Color: 10
Size: 319919 Color: 17

Bin 1102: 13 of cap free
Amount of items: 2
Items: 
Size: 696178 Color: 3
Size: 303810 Color: 17

Bin 1103: 13 of cap free
Amount of items: 2
Items: 
Size: 698842 Color: 14
Size: 301146 Color: 10

Bin 1104: 13 of cap free
Amount of items: 3
Items: 
Size: 740478 Color: 6
Size: 141779 Color: 13
Size: 117731 Color: 11

Bin 1105: 13 of cap free
Amount of items: 2
Items: 
Size: 753142 Color: 2
Size: 246846 Color: 19

Bin 1106: 13 of cap free
Amount of items: 2
Items: 
Size: 773287 Color: 9
Size: 226701 Color: 1

Bin 1107: 13 of cap free
Amount of items: 3
Items: 
Size: 779582 Color: 4
Size: 112436 Color: 10
Size: 107970 Color: 6

Bin 1108: 13 of cap free
Amount of items: 2
Items: 
Size: 788302 Color: 8
Size: 211686 Color: 7

Bin 1109: 13 of cap free
Amount of items: 3
Items: 
Size: 798127 Color: 7
Size: 101057 Color: 16
Size: 100804 Color: 17

Bin 1110: 14 of cap free
Amount of items: 3
Items: 
Size: 338582 Color: 18
Size: 337241 Color: 19
Size: 324164 Color: 8

Bin 1111: 14 of cap free
Amount of items: 3
Items: 
Size: 379127 Color: 12
Size: 371985 Color: 4
Size: 248875 Color: 14

Bin 1112: 14 of cap free
Amount of items: 3
Items: 
Size: 414767 Color: 12
Size: 309033 Color: 2
Size: 276187 Color: 5

Bin 1113: 14 of cap free
Amount of items: 3
Items: 
Size: 420969 Color: 5
Size: 308979 Color: 3
Size: 270039 Color: 8

Bin 1114: 14 of cap free
Amount of items: 2
Items: 
Size: 501847 Color: 18
Size: 498140 Color: 6

Bin 1115: 14 of cap free
Amount of items: 2
Items: 
Size: 503328 Color: 19
Size: 496659 Color: 5

Bin 1116: 14 of cap free
Amount of items: 2
Items: 
Size: 510907 Color: 14
Size: 489080 Color: 9

Bin 1117: 14 of cap free
Amount of items: 2
Items: 
Size: 527886 Color: 3
Size: 472101 Color: 18

Bin 1118: 14 of cap free
Amount of items: 2
Items: 
Size: 532862 Color: 14
Size: 467125 Color: 4

Bin 1119: 14 of cap free
Amount of items: 2
Items: 
Size: 532925 Color: 10
Size: 467062 Color: 5

Bin 1120: 14 of cap free
Amount of items: 2
Items: 
Size: 544171 Color: 16
Size: 455816 Color: 19

Bin 1121: 14 of cap free
Amount of items: 2
Items: 
Size: 561364 Color: 17
Size: 438623 Color: 11

Bin 1122: 14 of cap free
Amount of items: 2
Items: 
Size: 561933 Color: 6
Size: 438054 Color: 1

Bin 1123: 14 of cap free
Amount of items: 2
Items: 
Size: 584055 Color: 15
Size: 415932 Color: 19

Bin 1124: 14 of cap free
Amount of items: 2
Items: 
Size: 588880 Color: 9
Size: 411107 Color: 1

Bin 1125: 14 of cap free
Amount of items: 2
Items: 
Size: 610360 Color: 10
Size: 389627 Color: 5

Bin 1126: 14 of cap free
Amount of items: 2
Items: 
Size: 611210 Color: 8
Size: 388777 Color: 17

Bin 1127: 14 of cap free
Amount of items: 2
Items: 
Size: 616509 Color: 7
Size: 383478 Color: 12

Bin 1128: 14 of cap free
Amount of items: 3
Items: 
Size: 627395 Color: 11
Size: 188130 Color: 8
Size: 184462 Color: 4

Bin 1129: 14 of cap free
Amount of items: 3
Items: 
Size: 644497 Color: 14
Size: 190062 Color: 2
Size: 165428 Color: 3

Bin 1130: 14 of cap free
Amount of items: 2
Items: 
Size: 652662 Color: 16
Size: 347325 Color: 7

Bin 1131: 14 of cap free
Amount of items: 2
Items: 
Size: 673749 Color: 15
Size: 326238 Color: 0

Bin 1132: 14 of cap free
Amount of items: 2
Items: 
Size: 677354 Color: 6
Size: 322633 Color: 7

Bin 1133: 14 of cap free
Amount of items: 2
Items: 
Size: 677847 Color: 4
Size: 322140 Color: 6

Bin 1134: 14 of cap free
Amount of items: 2
Items: 
Size: 678789 Color: 16
Size: 321198 Color: 17

Bin 1135: 14 of cap free
Amount of items: 2
Items: 
Size: 684335 Color: 9
Size: 315652 Color: 7

Bin 1136: 14 of cap free
Amount of items: 2
Items: 
Size: 687595 Color: 5
Size: 312392 Color: 0

Bin 1137: 14 of cap free
Amount of items: 2
Items: 
Size: 688613 Color: 16
Size: 311374 Color: 2

Bin 1138: 14 of cap free
Amount of items: 2
Items: 
Size: 690292 Color: 15
Size: 309695 Color: 0

Bin 1139: 14 of cap free
Amount of items: 2
Items: 
Size: 696825 Color: 7
Size: 303162 Color: 5

Bin 1140: 14 of cap free
Amount of items: 2
Items: 
Size: 711104 Color: 15
Size: 288883 Color: 0

Bin 1141: 14 of cap free
Amount of items: 2
Items: 
Size: 712229 Color: 0
Size: 287758 Color: 14

Bin 1142: 14 of cap free
Amount of items: 2
Items: 
Size: 716958 Color: 15
Size: 283029 Color: 5

Bin 1143: 14 of cap free
Amount of items: 2
Items: 
Size: 718606 Color: 18
Size: 281381 Color: 9

Bin 1144: 14 of cap free
Amount of items: 2
Items: 
Size: 719715 Color: 12
Size: 280272 Color: 16

Bin 1145: 14 of cap free
Amount of items: 2
Items: 
Size: 732352 Color: 9
Size: 267635 Color: 13

Bin 1146: 14 of cap free
Amount of items: 2
Items: 
Size: 752763 Color: 15
Size: 247224 Color: 1

Bin 1147: 14 of cap free
Amount of items: 2
Items: 
Size: 766570 Color: 11
Size: 233417 Color: 17

Bin 1148: 14 of cap free
Amount of items: 3
Items: 
Size: 770486 Color: 9
Size: 116276 Color: 2
Size: 113225 Color: 9

Bin 1149: 14 of cap free
Amount of items: 2
Items: 
Size: 770765 Color: 0
Size: 229222 Color: 10

Bin 1150: 14 of cap free
Amount of items: 2
Items: 
Size: 775679 Color: 8
Size: 224308 Color: 2

Bin 1151: 14 of cap free
Amount of items: 2
Items: 
Size: 781998 Color: 8
Size: 217989 Color: 2

Bin 1152: 14 of cap free
Amount of items: 2
Items: 
Size: 781906 Color: 13
Size: 218081 Color: 18

Bin 1153: 14 of cap free
Amount of items: 2
Items: 
Size: 786836 Color: 6
Size: 213151 Color: 5

Bin 1154: 14 of cap free
Amount of items: 3
Items: 
Size: 788661 Color: 14
Size: 106070 Color: 15
Size: 105256 Color: 4

Bin 1155: 14 of cap free
Amount of items: 3
Items: 
Size: 791020 Color: 19
Size: 104710 Color: 3
Size: 104257 Color: 11

Bin 1156: 14 of cap free
Amount of items: 2
Items: 
Size: 792218 Color: 2
Size: 207769 Color: 10

Bin 1157: 14 of cap free
Amount of items: 2
Items: 
Size: 796838 Color: 8
Size: 203149 Color: 9

Bin 1158: 15 of cap free
Amount of items: 2
Items: 
Size: 522797 Color: 16
Size: 477189 Color: 3

Bin 1159: 15 of cap free
Amount of items: 2
Items: 
Size: 532321 Color: 11
Size: 467665 Color: 12

Bin 1160: 15 of cap free
Amount of items: 2
Items: 
Size: 537640 Color: 9
Size: 462346 Color: 2

Bin 1161: 15 of cap free
Amount of items: 3
Items: 
Size: 547653 Color: 15
Size: 228490 Color: 16
Size: 223843 Color: 14

Bin 1162: 15 of cap free
Amount of items: 2
Items: 
Size: 557880 Color: 3
Size: 442106 Color: 13

Bin 1163: 15 of cap free
Amount of items: 2
Items: 
Size: 576398 Color: 0
Size: 423588 Color: 15

Bin 1164: 15 of cap free
Amount of items: 2
Items: 
Size: 590492 Color: 19
Size: 409494 Color: 8

Bin 1165: 15 of cap free
Amount of items: 3
Items: 
Size: 619759 Color: 8
Size: 194464 Color: 3
Size: 185763 Color: 5

Bin 1166: 15 of cap free
Amount of items: 3
Items: 
Size: 622438 Color: 7
Size: 218492 Color: 3
Size: 159056 Color: 0

Bin 1167: 15 of cap free
Amount of items: 2
Items: 
Size: 633530 Color: 12
Size: 366456 Color: 17

Bin 1168: 15 of cap free
Amount of items: 3
Items: 
Size: 644384 Color: 2
Size: 189642 Color: 13
Size: 165960 Color: 7

Bin 1169: 15 of cap free
Amount of items: 2
Items: 
Size: 644765 Color: 14
Size: 355221 Color: 18

Bin 1170: 15 of cap free
Amount of items: 2
Items: 
Size: 645629 Color: 14
Size: 354357 Color: 12

Bin 1171: 15 of cap free
Amount of items: 2
Items: 
Size: 645824 Color: 0
Size: 354162 Color: 18

Bin 1172: 15 of cap free
Amount of items: 2
Items: 
Size: 653864 Color: 4
Size: 346122 Color: 0

Bin 1173: 15 of cap free
Amount of items: 2
Items: 
Size: 654830 Color: 16
Size: 345156 Color: 14

Bin 1174: 15 of cap free
Amount of items: 2
Items: 
Size: 655149 Color: 5
Size: 344837 Color: 11

Bin 1175: 15 of cap free
Amount of items: 2
Items: 
Size: 659142 Color: 9
Size: 340844 Color: 12

Bin 1176: 15 of cap free
Amount of items: 2
Items: 
Size: 666257 Color: 16
Size: 333729 Color: 17

Bin 1177: 15 of cap free
Amount of items: 2
Items: 
Size: 681361 Color: 9
Size: 318625 Color: 6

Bin 1178: 15 of cap free
Amount of items: 2
Items: 
Size: 682182 Color: 10
Size: 317804 Color: 6

Bin 1179: 15 of cap free
Amount of items: 2
Items: 
Size: 684804 Color: 17
Size: 315182 Color: 5

Bin 1180: 15 of cap free
Amount of items: 2
Items: 
Size: 686410 Color: 8
Size: 313576 Color: 3

Bin 1181: 15 of cap free
Amount of items: 2
Items: 
Size: 704444 Color: 14
Size: 295542 Color: 10

Bin 1182: 15 of cap free
Amount of items: 2
Items: 
Size: 708035 Color: 0
Size: 291951 Color: 12

Bin 1183: 15 of cap free
Amount of items: 3
Items: 
Size: 716236 Color: 14
Size: 145013 Color: 10
Size: 138737 Color: 17

Bin 1184: 15 of cap free
Amount of items: 3
Items: 
Size: 724667 Color: 8
Size: 144711 Color: 13
Size: 130608 Color: 12

Bin 1185: 15 of cap free
Amount of items: 2
Items: 
Size: 730700 Color: 17
Size: 269286 Color: 11

Bin 1186: 15 of cap free
Amount of items: 2
Items: 
Size: 749907 Color: 4
Size: 250079 Color: 12

Bin 1187: 15 of cap free
Amount of items: 2
Items: 
Size: 758494 Color: 8
Size: 241492 Color: 6

Bin 1188: 15 of cap free
Amount of items: 2
Items: 
Size: 759233 Color: 6
Size: 240753 Color: 12

Bin 1189: 15 of cap free
Amount of items: 2
Items: 
Size: 762042 Color: 8
Size: 237944 Color: 9

Bin 1190: 15 of cap free
Amount of items: 2
Items: 
Size: 773071 Color: 14
Size: 226915 Color: 3

Bin 1191: 15 of cap free
Amount of items: 2
Items: 
Size: 778001 Color: 1
Size: 221985 Color: 4

Bin 1192: 15 of cap free
Amount of items: 2
Items: 
Size: 783995 Color: 7
Size: 215991 Color: 5

Bin 1193: 15 of cap free
Amount of items: 2
Items: 
Size: 792268 Color: 6
Size: 207718 Color: 16

Bin 1194: 15 of cap free
Amount of items: 2
Items: 
Size: 794406 Color: 16
Size: 205580 Color: 19

Bin 1195: 15 of cap free
Amount of items: 2
Items: 
Size: 799512 Color: 6
Size: 200474 Color: 19

Bin 1196: 16 of cap free
Amount of items: 3
Items: 
Size: 380039 Color: 8
Size: 345288 Color: 2
Size: 274658 Color: 16

Bin 1197: 16 of cap free
Amount of items: 3
Items: 
Size: 416077 Color: 0
Size: 328020 Color: 1
Size: 255888 Color: 19

Bin 1198: 16 of cap free
Amount of items: 3
Items: 
Size: 422973 Color: 6
Size: 309660 Color: 9
Size: 267352 Color: 10

Bin 1199: 16 of cap free
Amount of items: 2
Items: 
Size: 502932 Color: 19
Size: 497053 Color: 2

Bin 1200: 16 of cap free
Amount of items: 2
Items: 
Size: 504171 Color: 4
Size: 495814 Color: 5

Bin 1201: 16 of cap free
Amount of items: 2
Items: 
Size: 506836 Color: 8
Size: 493149 Color: 11

Bin 1202: 16 of cap free
Amount of items: 2
Items: 
Size: 513830 Color: 16
Size: 486155 Color: 2

Bin 1203: 16 of cap free
Amount of items: 2
Items: 
Size: 514611 Color: 18
Size: 485374 Color: 12

Bin 1204: 16 of cap free
Amount of items: 2
Items: 
Size: 520696 Color: 9
Size: 479289 Color: 13

Bin 1205: 16 of cap free
Amount of items: 2
Items: 
Size: 538921 Color: 5
Size: 461064 Color: 6

Bin 1206: 16 of cap free
Amount of items: 2
Items: 
Size: 543469 Color: 0
Size: 456516 Color: 8

Bin 1207: 16 of cap free
Amount of items: 2
Items: 
Size: 551567 Color: 10
Size: 448418 Color: 19

Bin 1208: 16 of cap free
Amount of items: 2
Items: 
Size: 560220 Color: 0
Size: 439765 Color: 13

Bin 1209: 16 of cap free
Amount of items: 2
Items: 
Size: 575338 Color: 3
Size: 424647 Color: 14

Bin 1210: 16 of cap free
Amount of items: 2
Items: 
Size: 588130 Color: 19
Size: 411855 Color: 5

Bin 1211: 16 of cap free
Amount of items: 2
Items: 
Size: 590396 Color: 19
Size: 409589 Color: 8

Bin 1212: 16 of cap free
Amount of items: 2
Items: 
Size: 594125 Color: 4
Size: 405860 Color: 1

Bin 1213: 16 of cap free
Amount of items: 2
Items: 
Size: 596580 Color: 17
Size: 403405 Color: 13

Bin 1214: 16 of cap free
Amount of items: 2
Items: 
Size: 599810 Color: 3
Size: 400175 Color: 18

Bin 1215: 16 of cap free
Amount of items: 2
Items: 
Size: 611741 Color: 13
Size: 388244 Color: 6

Bin 1216: 16 of cap free
Amount of items: 3
Items: 
Size: 620159 Color: 16
Size: 192841 Color: 7
Size: 186985 Color: 16

Bin 1217: 16 of cap free
Amount of items: 2
Items: 
Size: 631679 Color: 13
Size: 368306 Color: 18

Bin 1218: 16 of cap free
Amount of items: 2
Items: 
Size: 633869 Color: 11
Size: 366116 Color: 6

Bin 1219: 16 of cap free
Amount of items: 2
Items: 
Size: 640748 Color: 14
Size: 359237 Color: 12

Bin 1220: 16 of cap free
Amount of items: 2
Items: 
Size: 649503 Color: 10
Size: 350482 Color: 8

Bin 1221: 16 of cap free
Amount of items: 2
Items: 
Size: 652330 Color: 16
Size: 347655 Color: 13

Bin 1222: 16 of cap free
Amount of items: 2
Items: 
Size: 685827 Color: 16
Size: 314158 Color: 9

Bin 1223: 16 of cap free
Amount of items: 2
Items: 
Size: 688776 Color: 16
Size: 311209 Color: 9

Bin 1224: 16 of cap free
Amount of items: 2
Items: 
Size: 689070 Color: 5
Size: 310915 Color: 8

Bin 1225: 16 of cap free
Amount of items: 2
Items: 
Size: 689796 Color: 18
Size: 310189 Color: 12

Bin 1226: 16 of cap free
Amount of items: 2
Items: 
Size: 693750 Color: 7
Size: 306235 Color: 10

Bin 1227: 16 of cap free
Amount of items: 2
Items: 
Size: 704945 Color: 13
Size: 295040 Color: 15

Bin 1228: 16 of cap free
Amount of items: 3
Items: 
Size: 712650 Color: 7
Size: 146189 Color: 15
Size: 141146 Color: 18

Bin 1229: 16 of cap free
Amount of items: 2
Items: 
Size: 718957 Color: 6
Size: 281028 Color: 9

Bin 1230: 16 of cap free
Amount of items: 2
Items: 
Size: 723460 Color: 19
Size: 276525 Color: 3

Bin 1231: 16 of cap free
Amount of items: 2
Items: 
Size: 731517 Color: 1
Size: 268468 Color: 0

Bin 1232: 16 of cap free
Amount of items: 2
Items: 
Size: 735718 Color: 15
Size: 264267 Color: 13

Bin 1233: 16 of cap free
Amount of items: 2
Items: 
Size: 751617 Color: 0
Size: 248368 Color: 1

Bin 1234: 16 of cap free
Amount of items: 2
Items: 
Size: 751943 Color: 17
Size: 248042 Color: 8

Bin 1235: 16 of cap free
Amount of items: 2
Items: 
Size: 755125 Color: 17
Size: 244860 Color: 3

Bin 1236: 16 of cap free
Amount of items: 2
Items: 
Size: 758151 Color: 11
Size: 241834 Color: 19

Bin 1237: 16 of cap free
Amount of items: 2
Items: 
Size: 760238 Color: 18
Size: 239747 Color: 10

Bin 1238: 16 of cap free
Amount of items: 2
Items: 
Size: 761183 Color: 18
Size: 238802 Color: 11

Bin 1239: 16 of cap free
Amount of items: 3
Items: 
Size: 773852 Color: 0
Size: 117983 Color: 5
Size: 108150 Color: 0

Bin 1240: 16 of cap free
Amount of items: 3
Items: 
Size: 774007 Color: 16
Size: 114349 Color: 18
Size: 111629 Color: 17

Bin 1241: 16 of cap free
Amount of items: 2
Items: 
Size: 784960 Color: 4
Size: 215025 Color: 0

Bin 1242: 16 of cap free
Amount of items: 2
Items: 
Size: 792150 Color: 17
Size: 207835 Color: 2

Bin 1243: 16 of cap free
Amount of items: 3
Items: 
Size: 792273 Color: 18
Size: 104113 Color: 19
Size: 103599 Color: 7

Bin 1244: 17 of cap free
Amount of items: 3
Items: 
Size: 465386 Color: 14
Size: 271304 Color: 8
Size: 263294 Color: 17

Bin 1245: 17 of cap free
Amount of items: 2
Items: 
Size: 521098 Color: 14
Size: 478886 Color: 2

Bin 1246: 17 of cap free
Amount of items: 2
Items: 
Size: 521657 Color: 5
Size: 478327 Color: 8

Bin 1247: 17 of cap free
Amount of items: 2
Items: 
Size: 529092 Color: 12
Size: 470892 Color: 18

Bin 1248: 17 of cap free
Amount of items: 2
Items: 
Size: 533744 Color: 0
Size: 466240 Color: 14

Bin 1249: 17 of cap free
Amount of items: 2
Items: 
Size: 543623 Color: 18
Size: 456361 Color: 16

Bin 1250: 17 of cap free
Amount of items: 2
Items: 
Size: 553567 Color: 18
Size: 446417 Color: 16

Bin 1251: 17 of cap free
Amount of items: 2
Items: 
Size: 556923 Color: 10
Size: 443061 Color: 15

Bin 1252: 17 of cap free
Amount of items: 2
Items: 
Size: 565988 Color: 19
Size: 433996 Color: 10

Bin 1253: 17 of cap free
Amount of items: 2
Items: 
Size: 584443 Color: 18
Size: 415541 Color: 12

Bin 1254: 17 of cap free
Amount of items: 2
Items: 
Size: 591549 Color: 13
Size: 408435 Color: 0

Bin 1255: 17 of cap free
Amount of items: 2
Items: 
Size: 591934 Color: 3
Size: 408050 Color: 7

Bin 1256: 17 of cap free
Amount of items: 2
Items: 
Size: 602793 Color: 13
Size: 397191 Color: 16

Bin 1257: 17 of cap free
Amount of items: 2
Items: 
Size: 605805 Color: 8
Size: 394179 Color: 17

Bin 1258: 17 of cap free
Amount of items: 2
Items: 
Size: 621702 Color: 10
Size: 378282 Color: 14

Bin 1259: 17 of cap free
Amount of items: 2
Items: 
Size: 629555 Color: 7
Size: 370429 Color: 18

Bin 1260: 17 of cap free
Amount of items: 2
Items: 
Size: 644827 Color: 16
Size: 355157 Color: 19

Bin 1261: 17 of cap free
Amount of items: 3
Items: 
Size: 654990 Color: 19
Size: 175523 Color: 2
Size: 169471 Color: 15

Bin 1262: 17 of cap free
Amount of items: 2
Items: 
Size: 659853 Color: 2
Size: 340131 Color: 10

Bin 1263: 17 of cap free
Amount of items: 2
Items: 
Size: 672600 Color: 4
Size: 327384 Color: 18

Bin 1264: 17 of cap free
Amount of items: 2
Items: 
Size: 695438 Color: 11
Size: 304546 Color: 4

Bin 1265: 17 of cap free
Amount of items: 3
Items: 
Size: 696993 Color: 18
Size: 152725 Color: 18
Size: 150266 Color: 3

Bin 1266: 17 of cap free
Amount of items: 2
Items: 
Size: 708137 Color: 6
Size: 291847 Color: 11

Bin 1267: 17 of cap free
Amount of items: 3
Items: 
Size: 713143 Color: 10
Size: 152463 Color: 11
Size: 134378 Color: 7

Bin 1268: 17 of cap free
Amount of items: 3
Items: 
Size: 713931 Color: 6
Size: 144149 Color: 17
Size: 141904 Color: 5

Bin 1269: 17 of cap free
Amount of items: 3
Items: 
Size: 726761 Color: 10
Size: 145709 Color: 13
Size: 127514 Color: 19

Bin 1270: 17 of cap free
Amount of items: 2
Items: 
Size: 735081 Color: 6
Size: 264903 Color: 5

Bin 1271: 17 of cap free
Amount of items: 2
Items: 
Size: 739732 Color: 0
Size: 260252 Color: 15

Bin 1272: 17 of cap free
Amount of items: 2
Items: 
Size: 745594 Color: 1
Size: 254390 Color: 11

Bin 1273: 17 of cap free
Amount of items: 2
Items: 
Size: 749570 Color: 6
Size: 250414 Color: 9

Bin 1274: 17 of cap free
Amount of items: 2
Items: 
Size: 762891 Color: 10
Size: 237093 Color: 14

Bin 1275: 17 of cap free
Amount of items: 2
Items: 
Size: 764974 Color: 15
Size: 235010 Color: 18

Bin 1276: 17 of cap free
Amount of items: 2
Items: 
Size: 774820 Color: 15
Size: 225164 Color: 11

Bin 1277: 17 of cap free
Amount of items: 3
Items: 
Size: 783209 Color: 2
Size: 108477 Color: 9
Size: 108298 Color: 5

Bin 1278: 17 of cap free
Amount of items: 2
Items: 
Size: 790120 Color: 18
Size: 209864 Color: 6

Bin 1279: 17 of cap free
Amount of items: 3
Items: 
Size: 792670 Color: 10
Size: 103660 Color: 18
Size: 103654 Color: 19

Bin 1280: 18 of cap free
Amount of items: 2
Items: 
Size: 500762 Color: 10
Size: 499221 Color: 12

Bin 1281: 18 of cap free
Amount of items: 2
Items: 
Size: 521715 Color: 18
Size: 478268 Color: 3

Bin 1282: 18 of cap free
Amount of items: 2
Items: 
Size: 528004 Color: 12
Size: 471979 Color: 19

Bin 1283: 18 of cap free
Amount of items: 2
Items: 
Size: 538820 Color: 13
Size: 461163 Color: 4

Bin 1284: 18 of cap free
Amount of items: 2
Items: 
Size: 546450 Color: 14
Size: 453533 Color: 0

Bin 1285: 18 of cap free
Amount of items: 2
Items: 
Size: 551904 Color: 1
Size: 448079 Color: 8

Bin 1286: 18 of cap free
Amount of items: 2
Items: 
Size: 553129 Color: 11
Size: 446854 Color: 9

Bin 1287: 18 of cap free
Amount of items: 2
Items: 
Size: 553863 Color: 1
Size: 446120 Color: 6

Bin 1288: 18 of cap free
Amount of items: 2
Items: 
Size: 554694 Color: 18
Size: 445289 Color: 3

Bin 1289: 18 of cap free
Amount of items: 3
Items: 
Size: 576881 Color: 15
Size: 226594 Color: 15
Size: 196508 Color: 17

Bin 1290: 18 of cap free
Amount of items: 3
Items: 
Size: 578407 Color: 9
Size: 225106 Color: 2
Size: 196470 Color: 14

Bin 1291: 18 of cap free
Amount of items: 2
Items: 
Size: 595201 Color: 3
Size: 404782 Color: 7

Bin 1292: 18 of cap free
Amount of items: 2
Items: 
Size: 596102 Color: 2
Size: 403881 Color: 18

Bin 1293: 18 of cap free
Amount of items: 2
Items: 
Size: 607031 Color: 11
Size: 392952 Color: 14

Bin 1294: 18 of cap free
Amount of items: 2
Items: 
Size: 609334 Color: 11
Size: 390649 Color: 3

Bin 1295: 18 of cap free
Amount of items: 2
Items: 
Size: 628343 Color: 6
Size: 371640 Color: 16

Bin 1296: 18 of cap free
Amount of items: 2
Items: 
Size: 655661 Color: 1
Size: 344322 Color: 9

Bin 1297: 18 of cap free
Amount of items: 2
Items: 
Size: 656495 Color: 2
Size: 343488 Color: 11

Bin 1298: 18 of cap free
Amount of items: 2
Items: 
Size: 671341 Color: 2
Size: 328642 Color: 5

Bin 1299: 18 of cap free
Amount of items: 2
Items: 
Size: 672008 Color: 4
Size: 327975 Color: 2

Bin 1300: 18 of cap free
Amount of items: 2
Items: 
Size: 691363 Color: 8
Size: 308620 Color: 0

Bin 1301: 18 of cap free
Amount of items: 2
Items: 
Size: 691672 Color: 0
Size: 308311 Color: 19

Bin 1302: 18 of cap free
Amount of items: 3
Items: 
Size: 692451 Color: 17
Size: 153861 Color: 19
Size: 153671 Color: 11

Bin 1303: 18 of cap free
Amount of items: 2
Items: 
Size: 695333 Color: 9
Size: 304650 Color: 10

Bin 1304: 18 of cap free
Amount of items: 2
Items: 
Size: 701805 Color: 18
Size: 298178 Color: 5

Bin 1305: 18 of cap free
Amount of items: 3
Items: 
Size: 707834 Color: 8
Size: 147086 Color: 17
Size: 145063 Color: 5

Bin 1306: 18 of cap free
Amount of items: 3
Items: 
Size: 708378 Color: 14
Size: 146214 Color: 0
Size: 145391 Color: 5

Bin 1307: 18 of cap free
Amount of items: 2
Items: 
Size: 712199 Color: 19
Size: 287784 Color: 8

Bin 1308: 18 of cap free
Amount of items: 2
Items: 
Size: 721899 Color: 15
Size: 278084 Color: 10

Bin 1309: 18 of cap free
Amount of items: 2
Items: 
Size: 748461 Color: 17
Size: 251522 Color: 14

Bin 1310: 18 of cap free
Amount of items: 2
Items: 
Size: 761088 Color: 5
Size: 238895 Color: 19

Bin 1311: 18 of cap free
Amount of items: 2
Items: 
Size: 765856 Color: 10
Size: 234127 Color: 6

Bin 1312: 18 of cap free
Amount of items: 2
Items: 
Size: 773093 Color: 17
Size: 226890 Color: 4

Bin 1313: 18 of cap free
Amount of items: 2
Items: 
Size: 777132 Color: 0
Size: 222851 Color: 16

Bin 1314: 18 of cap free
Amount of items: 3
Items: 
Size: 786887 Color: 12
Size: 107234 Color: 3
Size: 105862 Color: 18

Bin 1315: 18 of cap free
Amount of items: 3
Items: 
Size: 790066 Color: 8
Size: 105675 Color: 2
Size: 104242 Color: 13

Bin 1316: 18 of cap free
Amount of items: 2
Items: 
Size: 791666 Color: 3
Size: 208317 Color: 4

Bin 1317: 18 of cap free
Amount of items: 2
Items: 
Size: 792156 Color: 13
Size: 207827 Color: 6

Bin 1318: 19 of cap free
Amount of items: 3
Items: 
Size: 414472 Color: 11
Size: 304336 Color: 8
Size: 281174 Color: 4

Bin 1319: 19 of cap free
Amount of items: 2
Items: 
Size: 545932 Color: 15
Size: 454050 Color: 17

Bin 1320: 19 of cap free
Amount of items: 2
Items: 
Size: 546497 Color: 18
Size: 453485 Color: 13

Bin 1321: 19 of cap free
Amount of items: 2
Items: 
Size: 547610 Color: 13
Size: 452372 Color: 1

Bin 1322: 19 of cap free
Amount of items: 2
Items: 
Size: 549672 Color: 8
Size: 450310 Color: 3

Bin 1323: 19 of cap free
Amount of items: 2
Items: 
Size: 552566 Color: 9
Size: 447416 Color: 6

Bin 1324: 19 of cap free
Amount of items: 2
Items: 
Size: 564793 Color: 5
Size: 435189 Color: 13

Bin 1325: 19 of cap free
Amount of items: 2
Items: 
Size: 571122 Color: 5
Size: 428860 Color: 10

Bin 1326: 19 of cap free
Amount of items: 2
Items: 
Size: 589676 Color: 9
Size: 410306 Color: 2

Bin 1327: 19 of cap free
Amount of items: 2
Items: 
Size: 595887 Color: 9
Size: 404095 Color: 4

Bin 1328: 19 of cap free
Amount of items: 3
Items: 
Size: 621619 Color: 13
Size: 191223 Color: 9
Size: 187140 Color: 4

Bin 1329: 19 of cap free
Amount of items: 2
Items: 
Size: 624814 Color: 3
Size: 375168 Color: 6

Bin 1330: 19 of cap free
Amount of items: 2
Items: 
Size: 630182 Color: 15
Size: 369800 Color: 5

Bin 1331: 19 of cap free
Amount of items: 2
Items: 
Size: 636988 Color: 11
Size: 362994 Color: 1

Bin 1332: 19 of cap free
Amount of items: 2
Items: 
Size: 664960 Color: 4
Size: 335022 Color: 2

Bin 1333: 19 of cap free
Amount of items: 2
Items: 
Size: 671427 Color: 12
Size: 328555 Color: 13

Bin 1334: 19 of cap free
Amount of items: 2
Items: 
Size: 689171 Color: 0
Size: 310811 Color: 9

Bin 1335: 19 of cap free
Amount of items: 2
Items: 
Size: 689216 Color: 9
Size: 310766 Color: 8

Bin 1336: 19 of cap free
Amount of items: 2
Items: 
Size: 702001 Color: 14
Size: 297981 Color: 9

Bin 1337: 19 of cap free
Amount of items: 2
Items: 
Size: 702352 Color: 11
Size: 297630 Color: 2

Bin 1338: 19 of cap free
Amount of items: 3
Items: 
Size: 711220 Color: 6
Size: 148070 Color: 17
Size: 140692 Color: 18

Bin 1339: 19 of cap free
Amount of items: 2
Items: 
Size: 721626 Color: 1
Size: 278356 Color: 0

Bin 1340: 19 of cap free
Amount of items: 2
Items: 
Size: 749654 Color: 7
Size: 250328 Color: 19

Bin 1341: 19 of cap free
Amount of items: 3
Items: 
Size: 751176 Color: 13
Size: 130581 Color: 13
Size: 118225 Color: 9

Bin 1342: 19 of cap free
Amount of items: 2
Items: 
Size: 754465 Color: 2
Size: 245517 Color: 9

Bin 1343: 19 of cap free
Amount of items: 2
Items: 
Size: 757926 Color: 3
Size: 242056 Color: 17

Bin 1344: 19 of cap free
Amount of items: 2
Items: 
Size: 762190 Color: 8
Size: 237792 Color: 7

Bin 1345: 19 of cap free
Amount of items: 2
Items: 
Size: 766662 Color: 12
Size: 233320 Color: 2

Bin 1346: 19 of cap free
Amount of items: 2
Items: 
Size: 772325 Color: 11
Size: 227657 Color: 2

Bin 1347: 20 of cap free
Amount of items: 3
Items: 
Size: 416088 Color: 8
Size: 328251 Color: 10
Size: 255642 Color: 0

Bin 1348: 20 of cap free
Amount of items: 2
Items: 
Size: 516846 Color: 12
Size: 483135 Color: 10

Bin 1349: 20 of cap free
Amount of items: 3
Items: 
Size: 526933 Color: 12
Size: 250726 Color: 14
Size: 222322 Color: 17

Bin 1350: 20 of cap free
Amount of items: 2
Items: 
Size: 538773 Color: 14
Size: 461208 Color: 3

Bin 1351: 20 of cap free
Amount of items: 2
Items: 
Size: 558509 Color: 2
Size: 441472 Color: 1

Bin 1352: 20 of cap free
Amount of items: 2
Items: 
Size: 571478 Color: 9
Size: 428503 Color: 18

Bin 1353: 20 of cap free
Amount of items: 2
Items: 
Size: 577082 Color: 17
Size: 422899 Color: 12

Bin 1354: 20 of cap free
Amount of items: 2
Items: 
Size: 579995 Color: 18
Size: 419986 Color: 16

Bin 1355: 20 of cap free
Amount of items: 2
Items: 
Size: 588516 Color: 4
Size: 411465 Color: 8

Bin 1356: 20 of cap free
Amount of items: 2
Items: 
Size: 599941 Color: 12
Size: 400040 Color: 19

Bin 1357: 20 of cap free
Amount of items: 2
Items: 
Size: 601581 Color: 8
Size: 398400 Color: 18

Bin 1358: 20 of cap free
Amount of items: 2
Items: 
Size: 605091 Color: 4
Size: 394890 Color: 1

Bin 1359: 20 of cap free
Amount of items: 2
Items: 
Size: 610227 Color: 9
Size: 389754 Color: 18

Bin 1360: 20 of cap free
Amount of items: 2
Items: 
Size: 627066 Color: 8
Size: 372915 Color: 2

Bin 1361: 20 of cap free
Amount of items: 2
Items: 
Size: 653613 Color: 8
Size: 346368 Color: 9

Bin 1362: 20 of cap free
Amount of items: 3
Items: 
Size: 654569 Color: 17
Size: 180875 Color: 2
Size: 164537 Color: 12

Bin 1363: 20 of cap free
Amount of items: 2
Items: 
Size: 655978 Color: 2
Size: 344003 Color: 9

Bin 1364: 20 of cap free
Amount of items: 2
Items: 
Size: 659820 Color: 4
Size: 340161 Color: 7

Bin 1365: 20 of cap free
Amount of items: 2
Items: 
Size: 672350 Color: 14
Size: 327631 Color: 8

Bin 1366: 20 of cap free
Amount of items: 2
Items: 
Size: 673465 Color: 16
Size: 326516 Color: 4

Bin 1367: 20 of cap free
Amount of items: 2
Items: 
Size: 676118 Color: 14
Size: 323863 Color: 17

Bin 1368: 20 of cap free
Amount of items: 2
Items: 
Size: 681594 Color: 4
Size: 318387 Color: 2

Bin 1369: 20 of cap free
Amount of items: 3
Items: 
Size: 691261 Color: 11
Size: 154997 Color: 9
Size: 153723 Color: 19

Bin 1370: 20 of cap free
Amount of items: 2
Items: 
Size: 704306 Color: 6
Size: 295675 Color: 15

Bin 1371: 20 of cap free
Amount of items: 3
Items: 
Size: 712960 Color: 8
Size: 149323 Color: 4
Size: 137698 Color: 0

Bin 1372: 20 of cap free
Amount of items: 2
Items: 
Size: 713912 Color: 16
Size: 286069 Color: 17

Bin 1373: 20 of cap free
Amount of items: 3
Items: 
Size: 717040 Color: 11
Size: 152745 Color: 16
Size: 130196 Color: 2

Bin 1374: 20 of cap free
Amount of items: 2
Items: 
Size: 747404 Color: 2
Size: 252577 Color: 6

Bin 1375: 20 of cap free
Amount of items: 2
Items: 
Size: 750720 Color: 10
Size: 249261 Color: 4

Bin 1376: 20 of cap free
Amount of items: 2
Items: 
Size: 754294 Color: 8
Size: 245687 Color: 2

Bin 1377: 20 of cap free
Amount of items: 2
Items: 
Size: 755923 Color: 6
Size: 244058 Color: 1

Bin 1378: 20 of cap free
Amount of items: 2
Items: 
Size: 756248 Color: 19
Size: 243733 Color: 17

Bin 1379: 20 of cap free
Amount of items: 2
Items: 
Size: 762255 Color: 1
Size: 237726 Color: 15

Bin 1380: 20 of cap free
Amount of items: 3
Items: 
Size: 771098 Color: 3
Size: 117494 Color: 5
Size: 111389 Color: 14

Bin 1381: 20 of cap free
Amount of items: 2
Items: 
Size: 773750 Color: 11
Size: 226231 Color: 2

Bin 1382: 20 of cap free
Amount of items: 2
Items: 
Size: 784907 Color: 17
Size: 215074 Color: 9

Bin 1383: 20 of cap free
Amount of items: 2
Items: 
Size: 793331 Color: 15
Size: 206650 Color: 18

Bin 1384: 21 of cap free
Amount of items: 2
Items: 
Size: 500811 Color: 9
Size: 499169 Color: 19

Bin 1385: 21 of cap free
Amount of items: 2
Items: 
Size: 506538 Color: 7
Size: 493442 Color: 19

Bin 1386: 21 of cap free
Amount of items: 2
Items: 
Size: 502372 Color: 2
Size: 497608 Color: 1

Bin 1387: 21 of cap free
Amount of items: 2
Items: 
Size: 512227 Color: 9
Size: 487753 Color: 17

Bin 1388: 21 of cap free
Amount of items: 2
Items: 
Size: 520921 Color: 9
Size: 479059 Color: 14

Bin 1389: 21 of cap free
Amount of items: 2
Items: 
Size: 530715 Color: 1
Size: 469265 Color: 5

Bin 1390: 21 of cap free
Amount of items: 2
Items: 
Size: 531959 Color: 15
Size: 468021 Color: 6

Bin 1391: 21 of cap free
Amount of items: 2
Items: 
Size: 538535 Color: 5
Size: 461445 Color: 12

Bin 1392: 21 of cap free
Amount of items: 2
Items: 
Size: 545875 Color: 19
Size: 454105 Color: 10

Bin 1393: 21 of cap free
Amount of items: 2
Items: 
Size: 547225 Color: 7
Size: 452755 Color: 0

Bin 1394: 21 of cap free
Amount of items: 2
Items: 
Size: 559294 Color: 1
Size: 440686 Color: 16

Bin 1395: 21 of cap free
Amount of items: 2
Items: 
Size: 567788 Color: 0
Size: 432192 Color: 11

Bin 1396: 21 of cap free
Amount of items: 2
Items: 
Size: 568819 Color: 17
Size: 431161 Color: 1

Bin 1397: 21 of cap free
Amount of items: 2
Items: 
Size: 603289 Color: 10
Size: 396691 Color: 5

Bin 1398: 21 of cap free
Amount of items: 2
Items: 
Size: 609048 Color: 5
Size: 390932 Color: 19

Bin 1399: 21 of cap free
Amount of items: 2
Items: 
Size: 619999 Color: 3
Size: 379981 Color: 5

Bin 1400: 21 of cap free
Amount of items: 3
Items: 
Size: 626406 Color: 16
Size: 192739 Color: 17
Size: 180835 Color: 12

Bin 1401: 21 of cap free
Amount of items: 2
Items: 
Size: 657405 Color: 8
Size: 342575 Color: 16

Bin 1402: 21 of cap free
Amount of items: 2
Items: 
Size: 658810 Color: 16
Size: 341170 Color: 7

Bin 1403: 21 of cap free
Amount of items: 2
Items: 
Size: 665042 Color: 13
Size: 334938 Color: 14

Bin 1404: 21 of cap free
Amount of items: 2
Items: 
Size: 666252 Color: 7
Size: 333728 Color: 4

Bin 1405: 21 of cap free
Amount of items: 2
Items: 
Size: 666800 Color: 19
Size: 333180 Color: 10

Bin 1406: 21 of cap free
Amount of items: 2
Items: 
Size: 675025 Color: 12
Size: 324955 Color: 7

Bin 1407: 21 of cap free
Amount of items: 2
Items: 
Size: 681669 Color: 6
Size: 318311 Color: 15

Bin 1408: 21 of cap free
Amount of items: 2
Items: 
Size: 686477 Color: 13
Size: 313503 Color: 0

Bin 1409: 21 of cap free
Amount of items: 2
Items: 
Size: 703991 Color: 12
Size: 295989 Color: 2

Bin 1410: 21 of cap free
Amount of items: 3
Items: 
Size: 710091 Color: 19
Size: 149284 Color: 6
Size: 140605 Color: 0

Bin 1411: 21 of cap free
Amount of items: 2
Items: 
Size: 710597 Color: 15
Size: 289383 Color: 5

Bin 1412: 21 of cap free
Amount of items: 2
Items: 
Size: 752857 Color: 1
Size: 247123 Color: 18

Bin 1413: 21 of cap free
Amount of items: 3
Items: 
Size: 776832 Color: 16
Size: 113507 Color: 9
Size: 109641 Color: 17

Bin 1414: 21 of cap free
Amount of items: 3
Items: 
Size: 787661 Color: 7
Size: 106295 Color: 9
Size: 106024 Color: 5

Bin 1415: 21 of cap free
Amount of items: 3
Items: 
Size: 796438 Color: 17
Size: 102088 Color: 16
Size: 101454 Color: 12

Bin 1416: 22 of cap free
Amount of items: 3
Items: 
Size: 368791 Color: 10
Size: 347126 Color: 9
Size: 284062 Color: 0

Bin 1417: 22 of cap free
Amount of items: 3
Items: 
Size: 417213 Color: 5
Size: 305423 Color: 4
Size: 277343 Color: 6

Bin 1418: 22 of cap free
Amount of items: 3
Items: 
Size: 462440 Color: 19
Size: 270530 Color: 8
Size: 267009 Color: 19

Bin 1419: 22 of cap free
Amount of items: 2
Items: 
Size: 515810 Color: 16
Size: 484169 Color: 17

Bin 1420: 22 of cap free
Amount of items: 2
Items: 
Size: 527719 Color: 17
Size: 472260 Color: 9

Bin 1421: 22 of cap free
Amount of items: 2
Items: 
Size: 538644 Color: 8
Size: 461335 Color: 4

Bin 1422: 22 of cap free
Amount of items: 2
Items: 
Size: 540392 Color: 0
Size: 459587 Color: 17

Bin 1423: 22 of cap free
Amount of items: 2
Items: 
Size: 547906 Color: 16
Size: 452073 Color: 6

Bin 1424: 22 of cap free
Amount of items: 2
Items: 
Size: 551487 Color: 16
Size: 448492 Color: 10

Bin 1425: 22 of cap free
Amount of items: 2
Items: 
Size: 556422 Color: 18
Size: 443557 Color: 16

Bin 1426: 22 of cap free
Amount of items: 2
Items: 
Size: 559054 Color: 7
Size: 440925 Color: 15

Bin 1427: 22 of cap free
Amount of items: 2
Items: 
Size: 561873 Color: 15
Size: 438106 Color: 16

Bin 1428: 22 of cap free
Amount of items: 2
Items: 
Size: 583451 Color: 0
Size: 416528 Color: 11

Bin 1429: 22 of cap free
Amount of items: 2
Items: 
Size: 588632 Color: 13
Size: 411347 Color: 7

Bin 1430: 22 of cap free
Amount of items: 2
Items: 
Size: 603821 Color: 0
Size: 396158 Color: 2

Bin 1431: 22 of cap free
Amount of items: 2
Items: 
Size: 608298 Color: 19
Size: 391681 Color: 3

Bin 1432: 22 of cap free
Amount of items: 2
Items: 
Size: 614260 Color: 19
Size: 385719 Color: 3

Bin 1433: 22 of cap free
Amount of items: 3
Items: 
Size: 619082 Color: 2
Size: 212562 Color: 0
Size: 168335 Color: 19

Bin 1434: 22 of cap free
Amount of items: 3
Items: 
Size: 643700 Color: 6
Size: 184844 Color: 2
Size: 171435 Color: 11

Bin 1435: 22 of cap free
Amount of items: 3
Items: 
Size: 647550 Color: 3
Size: 186857 Color: 13
Size: 165572 Color: 0

Bin 1436: 22 of cap free
Amount of items: 2
Items: 
Size: 655107 Color: 5
Size: 344872 Color: 15

Bin 1437: 22 of cap free
Amount of items: 2
Items: 
Size: 685773 Color: 4
Size: 314206 Color: 5

Bin 1438: 22 of cap free
Amount of items: 2
Items: 
Size: 692947 Color: 11
Size: 307032 Color: 13

Bin 1439: 22 of cap free
Amount of items: 3
Items: 
Size: 697903 Color: 15
Size: 151235 Color: 9
Size: 150841 Color: 19

Bin 1440: 22 of cap free
Amount of items: 2
Items: 
Size: 705512 Color: 5
Size: 294467 Color: 12

Bin 1441: 22 of cap free
Amount of items: 2
Items: 
Size: 756705 Color: 9
Size: 243274 Color: 0

Bin 1442: 22 of cap free
Amount of items: 3
Items: 
Size: 764459 Color: 13
Size: 118859 Color: 13
Size: 116661 Color: 18

Bin 1443: 22 of cap free
Amount of items: 3
Items: 
Size: 770946 Color: 7
Size: 114947 Color: 6
Size: 114086 Color: 8

Bin 1444: 23 of cap free
Amount of items: 3
Items: 
Size: 370334 Color: 7
Size: 344445 Color: 17
Size: 285199 Color: 3

Bin 1445: 23 of cap free
Amount of items: 2
Items: 
Size: 500897 Color: 9
Size: 499081 Color: 17

Bin 1446: 23 of cap free
Amount of items: 2
Items: 
Size: 505806 Color: 4
Size: 494172 Color: 8

Bin 1447: 23 of cap free
Amount of items: 2
Items: 
Size: 510798 Color: 7
Size: 489180 Color: 3

Bin 1448: 23 of cap free
Amount of items: 2
Items: 
Size: 518839 Color: 18
Size: 481139 Color: 5

Bin 1449: 23 of cap free
Amount of items: 2
Items: 
Size: 527825 Color: 9
Size: 472153 Color: 1

Bin 1450: 23 of cap free
Amount of items: 3
Items: 
Size: 531213 Color: 5
Size: 250599 Color: 2
Size: 218166 Color: 19

Bin 1451: 23 of cap free
Amount of items: 3
Items: 
Size: 547082 Color: 6
Size: 231773 Color: 17
Size: 221123 Color: 4

Bin 1452: 23 of cap free
Amount of items: 2
Items: 
Size: 564583 Color: 14
Size: 435395 Color: 15

Bin 1453: 23 of cap free
Amount of items: 2
Items: 
Size: 568170 Color: 12
Size: 431808 Color: 11

Bin 1454: 23 of cap free
Amount of items: 2
Items: 
Size: 578783 Color: 16
Size: 421195 Color: 1

Bin 1455: 23 of cap free
Amount of items: 2
Items: 
Size: 584497 Color: 12
Size: 415481 Color: 18

Bin 1456: 23 of cap free
Amount of items: 2
Items: 
Size: 586368 Color: 2
Size: 413610 Color: 12

Bin 1457: 23 of cap free
Amount of items: 2
Items: 
Size: 588235 Color: 4
Size: 411743 Color: 1

Bin 1458: 23 of cap free
Amount of items: 3
Items: 
Size: 601310 Color: 11
Size: 200853 Color: 13
Size: 197815 Color: 19

Bin 1459: 23 of cap free
Amount of items: 2
Items: 
Size: 605392 Color: 16
Size: 394586 Color: 19

Bin 1460: 23 of cap free
Amount of items: 2
Items: 
Size: 609979 Color: 14
Size: 389999 Color: 16

Bin 1461: 23 of cap free
Amount of items: 2
Items: 
Size: 612461 Color: 1
Size: 387517 Color: 8

Bin 1462: 23 of cap free
Amount of items: 3
Items: 
Size: 617647 Color: 19
Size: 191868 Color: 1
Size: 190463 Color: 19

Bin 1463: 23 of cap free
Amount of items: 2
Items: 
Size: 621234 Color: 15
Size: 378744 Color: 6

Bin 1464: 23 of cap free
Amount of items: 2
Items: 
Size: 621353 Color: 1
Size: 378625 Color: 14

Bin 1465: 23 of cap free
Amount of items: 2
Items: 
Size: 633795 Color: 10
Size: 366183 Color: 14

Bin 1466: 23 of cap free
Amount of items: 3
Items: 
Size: 656696 Color: 0
Size: 184344 Color: 13
Size: 158938 Color: 19

Bin 1467: 23 of cap free
Amount of items: 3
Items: 
Size: 659756 Color: 10
Size: 180769 Color: 13
Size: 159453 Color: 14

Bin 1468: 23 of cap free
Amount of items: 2
Items: 
Size: 671668 Color: 7
Size: 328310 Color: 18

Bin 1469: 23 of cap free
Amount of items: 2
Items: 
Size: 685316 Color: 8
Size: 314662 Color: 11

Bin 1470: 23 of cap free
Amount of items: 3
Items: 
Size: 697941 Color: 17
Size: 151322 Color: 19
Size: 150715 Color: 15

Bin 1471: 23 of cap free
Amount of items: 2
Items: 
Size: 714772 Color: 19
Size: 285206 Color: 11

Bin 1472: 23 of cap free
Amount of items: 2
Items: 
Size: 718370 Color: 11
Size: 281608 Color: 2

Bin 1473: 23 of cap free
Amount of items: 3
Items: 
Size: 745587 Color: 7
Size: 136940 Color: 13
Size: 117451 Color: 18

Bin 1474: 23 of cap free
Amount of items: 2
Items: 
Size: 757723 Color: 6
Size: 242255 Color: 10

Bin 1475: 23 of cap free
Amount of items: 2
Items: 
Size: 760437 Color: 1
Size: 239541 Color: 13

Bin 1476: 23 of cap free
Amount of items: 2
Items: 
Size: 772158 Color: 11
Size: 227820 Color: 1

Bin 1477: 23 of cap free
Amount of items: 2
Items: 
Size: 776580 Color: 5
Size: 223398 Color: 4

Bin 1478: 24 of cap free
Amount of items: 2
Items: 
Size: 521090 Color: 16
Size: 478887 Color: 7

Bin 1479: 24 of cap free
Amount of items: 2
Items: 
Size: 541157 Color: 1
Size: 458820 Color: 6

Bin 1480: 24 of cap free
Amount of items: 2
Items: 
Size: 542536 Color: 1
Size: 457441 Color: 18

Bin 1481: 24 of cap free
Amount of items: 2
Items: 
Size: 557999 Color: 13
Size: 441978 Color: 5

Bin 1482: 24 of cap free
Amount of items: 2
Items: 
Size: 567452 Color: 14
Size: 432525 Color: 0

Bin 1483: 24 of cap free
Amount of items: 2
Items: 
Size: 568248 Color: 9
Size: 431729 Color: 10

Bin 1484: 24 of cap free
Amount of items: 2
Items: 
Size: 570766 Color: 1
Size: 429211 Color: 10

Bin 1485: 24 of cap free
Amount of items: 2
Items: 
Size: 573927 Color: 4
Size: 426050 Color: 17

Bin 1486: 24 of cap free
Amount of items: 3
Items: 
Size: 575976 Color: 13
Size: 227431 Color: 4
Size: 196570 Color: 15

Bin 1487: 24 of cap free
Amount of items: 2
Items: 
Size: 588672 Color: 6
Size: 411305 Color: 16

Bin 1488: 24 of cap free
Amount of items: 2
Items: 
Size: 605441 Color: 18
Size: 394536 Color: 1

Bin 1489: 24 of cap free
Amount of items: 2
Items: 
Size: 609205 Color: 6
Size: 390772 Color: 14

Bin 1490: 24 of cap free
Amount of items: 2
Items: 
Size: 611670 Color: 8
Size: 388307 Color: 13

Bin 1491: 24 of cap free
Amount of items: 3
Items: 
Size: 625506 Color: 13
Size: 191702 Color: 14
Size: 182769 Color: 9

Bin 1492: 24 of cap free
Amount of items: 2
Items: 
Size: 627265 Color: 8
Size: 372712 Color: 14

Bin 1493: 24 of cap free
Amount of items: 2
Items: 
Size: 671481 Color: 6
Size: 328496 Color: 15

Bin 1494: 24 of cap free
Amount of items: 2
Items: 
Size: 676788 Color: 10
Size: 323189 Color: 14

Bin 1495: 24 of cap free
Amount of items: 2
Items: 
Size: 678621 Color: 7
Size: 321356 Color: 5

Bin 1496: 24 of cap free
Amount of items: 2
Items: 
Size: 679073 Color: 15
Size: 320904 Color: 3

Bin 1497: 24 of cap free
Amount of items: 2
Items: 
Size: 697729 Color: 6
Size: 302248 Color: 13

Bin 1498: 24 of cap free
Amount of items: 2
Items: 
Size: 702199 Color: 11
Size: 297778 Color: 8

Bin 1499: 24 of cap free
Amount of items: 2
Items: 
Size: 723076 Color: 3
Size: 276901 Color: 9

Bin 1500: 24 of cap free
Amount of items: 2
Items: 
Size: 738918 Color: 14
Size: 261059 Color: 6

Bin 1501: 24 of cap free
Amount of items: 2
Items: 
Size: 754087 Color: 10
Size: 245890 Color: 6

Bin 1502: 24 of cap free
Amount of items: 2
Items: 
Size: 779365 Color: 5
Size: 220612 Color: 7

Bin 1503: 24 of cap free
Amount of items: 2
Items: 
Size: 780437 Color: 17
Size: 219540 Color: 18

Bin 1504: 24 of cap free
Amount of items: 3
Items: 
Size: 785641 Color: 1
Size: 108179 Color: 13
Size: 106157 Color: 9

Bin 1505: 24 of cap free
Amount of items: 3
Items: 
Size: 789411 Color: 0
Size: 105724 Color: 12
Size: 104842 Color: 13

Bin 1506: 24 of cap free
Amount of items: 2
Items: 
Size: 795023 Color: 19
Size: 204954 Color: 4

Bin 1507: 25 of cap free
Amount of items: 3
Items: 
Size: 366808 Color: 14
Size: 354915 Color: 17
Size: 278253 Color: 11

Bin 1508: 25 of cap free
Amount of items: 3
Items: 
Size: 373413 Color: 8
Size: 341948 Color: 18
Size: 284615 Color: 5

Bin 1509: 25 of cap free
Amount of items: 3
Items: 
Size: 376403 Color: 13
Size: 359487 Color: 8
Size: 264086 Color: 0

Bin 1510: 25 of cap free
Amount of items: 2
Items: 
Size: 501613 Color: 19
Size: 498363 Color: 7

Bin 1511: 25 of cap free
Amount of items: 2
Items: 
Size: 509912 Color: 17
Size: 490064 Color: 16

Bin 1512: 25 of cap free
Amount of items: 2
Items: 
Size: 532157 Color: 2
Size: 467819 Color: 1

Bin 1513: 25 of cap free
Amount of items: 2
Items: 
Size: 542129 Color: 11
Size: 457847 Color: 9

Bin 1514: 25 of cap free
Amount of items: 2
Items: 
Size: 543959 Color: 11
Size: 456017 Color: 4

Bin 1515: 25 of cap free
Amount of items: 2
Items: 
Size: 547285 Color: 18
Size: 452691 Color: 4

Bin 1516: 25 of cap free
Amount of items: 3
Items: 
Size: 557917 Color: 11
Size: 223874 Color: 9
Size: 218185 Color: 15

Bin 1517: 25 of cap free
Amount of items: 2
Items: 
Size: 596822 Color: 18
Size: 403154 Color: 11

Bin 1518: 25 of cap free
Amount of items: 2
Items: 
Size: 609000 Color: 15
Size: 390976 Color: 8

Bin 1519: 25 of cap free
Amount of items: 2
Items: 
Size: 612233 Color: 6
Size: 387743 Color: 17

Bin 1520: 25 of cap free
Amount of items: 2
Items: 
Size: 614510 Color: 9
Size: 385466 Color: 11

Bin 1521: 25 of cap free
Amount of items: 2
Items: 
Size: 650690 Color: 17
Size: 349286 Color: 12

Bin 1522: 25 of cap free
Amount of items: 3
Items: 
Size: 652614 Color: 6
Size: 183761 Color: 15
Size: 163601 Color: 2

Bin 1523: 25 of cap free
Amount of items: 2
Items: 
Size: 658754 Color: 14
Size: 341222 Color: 4

Bin 1524: 25 of cap free
Amount of items: 2
Items: 
Size: 664190 Color: 16
Size: 335786 Color: 13

Bin 1525: 25 of cap free
Amount of items: 2
Items: 
Size: 667008 Color: 4
Size: 332968 Color: 5

Bin 1526: 25 of cap free
Amount of items: 2
Items: 
Size: 676507 Color: 10
Size: 323469 Color: 15

Bin 1527: 25 of cap free
Amount of items: 2
Items: 
Size: 677944 Color: 10
Size: 322032 Color: 19

Bin 1528: 25 of cap free
Amount of items: 2
Items: 
Size: 684244 Color: 13
Size: 315732 Color: 16

Bin 1529: 25 of cap free
Amount of items: 3
Items: 
Size: 712574 Color: 2
Size: 144645 Color: 12
Size: 142757 Color: 12

Bin 1530: 25 of cap free
Amount of items: 2
Items: 
Size: 713413 Color: 12
Size: 286563 Color: 11

Bin 1531: 25 of cap free
Amount of items: 2
Items: 
Size: 715589 Color: 9
Size: 284387 Color: 14

Bin 1532: 25 of cap free
Amount of items: 2
Items: 
Size: 728576 Color: 5
Size: 271400 Color: 3

Bin 1533: 25 of cap free
Amount of items: 3
Items: 
Size: 735024 Color: 11
Size: 133788 Color: 12
Size: 131164 Color: 8

Bin 1534: 25 of cap free
Amount of items: 2
Items: 
Size: 760002 Color: 4
Size: 239974 Color: 7

Bin 1535: 25 of cap free
Amount of items: 2
Items: 
Size: 769216 Color: 6
Size: 230760 Color: 7

Bin 1536: 25 of cap free
Amount of items: 3
Items: 
Size: 780110 Color: 6
Size: 113335 Color: 13
Size: 106531 Color: 16

Bin 1537: 25 of cap free
Amount of items: 3
Items: 
Size: 793219 Color: 0
Size: 103679 Color: 2
Size: 103078 Color: 14

Bin 1538: 25 of cap free
Amount of items: 3
Items: 
Size: 797945 Color: 6
Size: 101268 Color: 7
Size: 100763 Color: 5

Bin 1539: 26 of cap free
Amount of items: 2
Items: 
Size: 529490 Color: 11
Size: 470485 Color: 4

Bin 1540: 26 of cap free
Amount of items: 2
Items: 
Size: 535070 Color: 5
Size: 464905 Color: 13

Bin 1541: 26 of cap free
Amount of items: 2
Items: 
Size: 536052 Color: 10
Size: 463923 Color: 9

Bin 1542: 26 of cap free
Amount of items: 2
Items: 
Size: 554378 Color: 0
Size: 445597 Color: 2

Bin 1543: 26 of cap free
Amount of items: 2
Items: 
Size: 576533 Color: 18
Size: 423442 Color: 15

Bin 1544: 26 of cap free
Amount of items: 2
Items: 
Size: 613815 Color: 8
Size: 386160 Color: 9

Bin 1545: 26 of cap free
Amount of items: 3
Items: 
Size: 615096 Color: 4
Size: 192579 Color: 13
Size: 192300 Color: 5

Bin 1546: 26 of cap free
Amount of items: 2
Items: 
Size: 616338 Color: 15
Size: 383637 Color: 6

Bin 1547: 26 of cap free
Amount of items: 2
Items: 
Size: 617046 Color: 9
Size: 382929 Color: 15

Bin 1548: 26 of cap free
Amount of items: 2
Items: 
Size: 658833 Color: 18
Size: 341142 Color: 4

Bin 1549: 26 of cap free
Amount of items: 2
Items: 
Size: 663226 Color: 16
Size: 336749 Color: 5

Bin 1550: 26 of cap free
Amount of items: 2
Items: 
Size: 665921 Color: 11
Size: 334054 Color: 15

Bin 1551: 26 of cap free
Amount of items: 2
Items: 
Size: 676682 Color: 5
Size: 323293 Color: 17

Bin 1552: 26 of cap free
Amount of items: 2
Items: 
Size: 677721 Color: 18
Size: 322254 Color: 8

Bin 1553: 26 of cap free
Amount of items: 2
Items: 
Size: 683410 Color: 13
Size: 316565 Color: 6

Bin 1554: 26 of cap free
Amount of items: 2
Items: 
Size: 685887 Color: 19
Size: 314088 Color: 18

Bin 1555: 26 of cap free
Amount of items: 2
Items: 
Size: 686660 Color: 18
Size: 313315 Color: 8

Bin 1556: 26 of cap free
Amount of items: 2
Items: 
Size: 687131 Color: 8
Size: 312844 Color: 13

Bin 1557: 26 of cap free
Amount of items: 2
Items: 
Size: 689551 Color: 16
Size: 310424 Color: 2

Bin 1558: 26 of cap free
Amount of items: 2
Items: 
Size: 692868 Color: 7
Size: 307107 Color: 14

Bin 1559: 26 of cap free
Amount of items: 2
Items: 
Size: 696010 Color: 0
Size: 303965 Color: 8

Bin 1560: 26 of cap free
Amount of items: 2
Items: 
Size: 698567 Color: 8
Size: 301408 Color: 16

Bin 1561: 26 of cap free
Amount of items: 2
Items: 
Size: 720005 Color: 12
Size: 279970 Color: 18

Bin 1562: 26 of cap free
Amount of items: 2
Items: 
Size: 737809 Color: 14
Size: 262166 Color: 11

Bin 1563: 26 of cap free
Amount of items: 2
Items: 
Size: 739038 Color: 12
Size: 260937 Color: 15

Bin 1564: 26 of cap free
Amount of items: 2
Items: 
Size: 755249 Color: 1
Size: 244726 Color: 7

Bin 1565: 26 of cap free
Amount of items: 2
Items: 
Size: 765315 Color: 5
Size: 234660 Color: 19

Bin 1566: 26 of cap free
Amount of items: 2
Items: 
Size: 777370 Color: 16
Size: 222605 Color: 2

Bin 1567: 26 of cap free
Amount of items: 2
Items: 
Size: 785002 Color: 3
Size: 214973 Color: 18

Bin 1568: 26 of cap free
Amount of items: 2
Items: 
Size: 798685 Color: 19
Size: 201290 Color: 8

Bin 1569: 27 of cap free
Amount of items: 3
Items: 
Size: 365324 Color: 1
Size: 362178 Color: 1
Size: 272472 Color: 18

Bin 1570: 27 of cap free
Amount of items: 3
Items: 
Size: 417096 Color: 9
Size: 323767 Color: 5
Size: 259111 Color: 1

Bin 1571: 27 of cap free
Amount of items: 2
Items: 
Size: 553671 Color: 9
Size: 446303 Color: 5

Bin 1572: 27 of cap free
Amount of items: 2
Items: 
Size: 555121 Color: 5
Size: 444853 Color: 14

Bin 1573: 27 of cap free
Amount of items: 2
Items: 
Size: 571209 Color: 4
Size: 428765 Color: 10

Bin 1574: 27 of cap free
Amount of items: 2
Items: 
Size: 573813 Color: 17
Size: 426161 Color: 18

Bin 1575: 27 of cap free
Amount of items: 2
Items: 
Size: 578594 Color: 3
Size: 421380 Color: 17

Bin 1576: 27 of cap free
Amount of items: 2
Items: 
Size: 588981 Color: 4
Size: 410993 Color: 9

Bin 1577: 27 of cap free
Amount of items: 2
Items: 
Size: 590930 Color: 6
Size: 409044 Color: 1

Bin 1578: 27 of cap free
Amount of items: 2
Items: 
Size: 596657 Color: 5
Size: 403317 Color: 3

Bin 1579: 27 of cap free
Amount of items: 2
Items: 
Size: 619286 Color: 6
Size: 380688 Color: 14

Bin 1580: 27 of cap free
Amount of items: 3
Items: 
Size: 626958 Color: 0
Size: 189619 Color: 6
Size: 183397 Color: 16

Bin 1581: 27 of cap free
Amount of items: 2
Items: 
Size: 628929 Color: 8
Size: 371045 Color: 13

Bin 1582: 27 of cap free
Amount of items: 2
Items: 
Size: 669933 Color: 8
Size: 330041 Color: 0

Bin 1583: 27 of cap free
Amount of items: 2
Items: 
Size: 670796 Color: 9
Size: 329178 Color: 10

Bin 1584: 27 of cap free
Amount of items: 2
Items: 
Size: 680877 Color: 0
Size: 319097 Color: 16

Bin 1585: 27 of cap free
Amount of items: 2
Items: 
Size: 692282 Color: 19
Size: 307692 Color: 14

Bin 1586: 27 of cap free
Amount of items: 2
Items: 
Size: 702039 Color: 3
Size: 297935 Color: 18

Bin 1587: 27 of cap free
Amount of items: 3
Items: 
Size: 712439 Color: 17
Size: 150679 Color: 0
Size: 136856 Color: 12

Bin 1588: 27 of cap free
Amount of items: 2
Items: 
Size: 722586 Color: 5
Size: 277388 Color: 7

Bin 1589: 27 of cap free
Amount of items: 2
Items: 
Size: 735262 Color: 7
Size: 264712 Color: 4

Bin 1590: 27 of cap free
Amount of items: 2
Items: 
Size: 742874 Color: 7
Size: 257100 Color: 1

Bin 1591: 27 of cap free
Amount of items: 2
Items: 
Size: 745759 Color: 19
Size: 254215 Color: 7

Bin 1592: 27 of cap free
Amount of items: 2
Items: 
Size: 762436 Color: 4
Size: 237538 Color: 15

Bin 1593: 27 of cap free
Amount of items: 2
Items: 
Size: 762549 Color: 6
Size: 237425 Color: 18

Bin 1594: 27 of cap free
Amount of items: 2
Items: 
Size: 769722 Color: 14
Size: 230252 Color: 4

Bin 1595: 27 of cap free
Amount of items: 2
Items: 
Size: 797518 Color: 14
Size: 202456 Color: 8

Bin 1596: 27 of cap free
Amount of items: 3
Items: 
Size: 797675 Color: 0
Size: 101152 Color: 5
Size: 101147 Color: 9

Bin 1597: 28 of cap free
Amount of items: 3
Items: 
Size: 363642 Color: 0
Size: 348445 Color: 7
Size: 287886 Color: 1

Bin 1598: 28 of cap free
Amount of items: 3
Items: 
Size: 462700 Color: 5
Size: 271652 Color: 1
Size: 265621 Color: 10

Bin 1599: 28 of cap free
Amount of items: 2
Items: 
Size: 528461 Color: 15
Size: 471512 Color: 11

Bin 1600: 28 of cap free
Amount of items: 2
Items: 
Size: 532250 Color: 5
Size: 467723 Color: 18

Bin 1601: 28 of cap free
Amount of items: 2
Items: 
Size: 535804 Color: 2
Size: 464169 Color: 3

Bin 1602: 28 of cap free
Amount of items: 2
Items: 
Size: 566448 Color: 15
Size: 433525 Color: 14

Bin 1603: 28 of cap free
Amount of items: 2
Items: 
Size: 582406 Color: 15
Size: 417567 Color: 9

Bin 1604: 28 of cap free
Amount of items: 2
Items: 
Size: 592167 Color: 18
Size: 407806 Color: 14

Bin 1605: 28 of cap free
Amount of items: 2
Items: 
Size: 600746 Color: 4
Size: 399227 Color: 2

Bin 1606: 28 of cap free
Amount of items: 2
Items: 
Size: 620050 Color: 8
Size: 379923 Color: 7

Bin 1607: 28 of cap free
Amount of items: 2
Items: 
Size: 620082 Color: 10
Size: 379891 Color: 9

Bin 1608: 28 of cap free
Amount of items: 2
Items: 
Size: 635165 Color: 7
Size: 364808 Color: 17

Bin 1609: 28 of cap free
Amount of items: 2
Items: 
Size: 635405 Color: 13
Size: 364568 Color: 15

Bin 1610: 28 of cap free
Amount of items: 2
Items: 
Size: 636375 Color: 0
Size: 363598 Color: 4

Bin 1611: 28 of cap free
Amount of items: 2
Items: 
Size: 666337 Color: 19
Size: 333636 Color: 2

Bin 1612: 28 of cap free
Amount of items: 2
Items: 
Size: 688042 Color: 12
Size: 311931 Color: 18

Bin 1613: 28 of cap free
Amount of items: 2
Items: 
Size: 707020 Color: 18
Size: 292953 Color: 0

Bin 1614: 28 of cap free
Amount of items: 2
Items: 
Size: 723493 Color: 3
Size: 276480 Color: 5

Bin 1615: 28 of cap free
Amount of items: 3
Items: 
Size: 724032 Color: 1
Size: 151261 Color: 13
Size: 124680 Color: 18

Bin 1616: 28 of cap free
Amount of items: 2
Items: 
Size: 768950 Color: 0
Size: 231023 Color: 15

Bin 1617: 29 of cap free
Amount of items: 3
Items: 
Size: 464703 Color: 3
Size: 268516 Color: 10
Size: 266753 Color: 10

Bin 1618: 29 of cap free
Amount of items: 2
Items: 
Size: 501136 Color: 18
Size: 498836 Color: 15

Bin 1619: 29 of cap free
Amount of items: 2
Items: 
Size: 505669 Color: 10
Size: 494303 Color: 9

Bin 1620: 29 of cap free
Amount of items: 2
Items: 
Size: 511243 Color: 4
Size: 488729 Color: 8

Bin 1621: 29 of cap free
Amount of items: 2
Items: 
Size: 521717 Color: 15
Size: 478255 Color: 4

Bin 1622: 29 of cap free
Amount of items: 2
Items: 
Size: 522759 Color: 9
Size: 477213 Color: 15

Bin 1623: 29 of cap free
Amount of items: 2
Items: 
Size: 531638 Color: 12
Size: 468334 Color: 6

Bin 1624: 29 of cap free
Amount of items: 2
Items: 
Size: 553918 Color: 11
Size: 446054 Color: 17

Bin 1625: 29 of cap free
Amount of items: 2
Items: 
Size: 555592 Color: 5
Size: 444380 Color: 4

Bin 1626: 29 of cap free
Amount of items: 2
Items: 
Size: 559248 Color: 0
Size: 440724 Color: 7

Bin 1627: 29 of cap free
Amount of items: 2
Items: 
Size: 567137 Color: 1
Size: 432835 Color: 10

Bin 1628: 29 of cap free
Amount of items: 3
Items: 
Size: 579105 Color: 10
Size: 223151 Color: 4
Size: 197716 Color: 15

Bin 1629: 29 of cap free
Amount of items: 2
Items: 
Size: 591212 Color: 13
Size: 408760 Color: 16

Bin 1630: 29 of cap free
Amount of items: 2
Items: 
Size: 592760 Color: 18
Size: 407212 Color: 19

Bin 1631: 29 of cap free
Amount of items: 2
Items: 
Size: 597370 Color: 11
Size: 402602 Color: 0

Bin 1632: 29 of cap free
Amount of items: 2
Items: 
Size: 599365 Color: 7
Size: 400607 Color: 4

Bin 1633: 29 of cap free
Amount of items: 2
Items: 
Size: 613142 Color: 18
Size: 386830 Color: 17

Bin 1634: 29 of cap free
Amount of items: 2
Items: 
Size: 640356 Color: 13
Size: 359616 Color: 10

Bin 1635: 29 of cap free
Amount of items: 2
Items: 
Size: 656557 Color: 13
Size: 343415 Color: 3

Bin 1636: 29 of cap free
Amount of items: 2
Items: 
Size: 665750 Color: 7
Size: 334222 Color: 3

Bin 1637: 29 of cap free
Amount of items: 2
Items: 
Size: 680664 Color: 3
Size: 319308 Color: 0

Bin 1638: 29 of cap free
Amount of items: 2
Items: 
Size: 686254 Color: 8
Size: 313718 Color: 9

Bin 1639: 29 of cap free
Amount of items: 2
Items: 
Size: 687946 Color: 7
Size: 312026 Color: 4

Bin 1640: 29 of cap free
Amount of items: 2
Items: 
Size: 699108 Color: 4
Size: 300864 Color: 19

Bin 1641: 29 of cap free
Amount of items: 2
Items: 
Size: 738816 Color: 15
Size: 261156 Color: 4

Bin 1642: 29 of cap free
Amount of items: 2
Items: 
Size: 761697 Color: 4
Size: 238275 Color: 1

Bin 1643: 29 of cap free
Amount of items: 2
Items: 
Size: 783297 Color: 7
Size: 216675 Color: 18

Bin 1644: 29 of cap free
Amount of items: 2
Items: 
Size: 794923 Color: 16
Size: 205049 Color: 4

Bin 1645: 29 of cap free
Amount of items: 2
Items: 
Size: 799729 Color: 14
Size: 200243 Color: 5

Bin 1646: 30 of cap free
Amount of items: 2
Items: 
Size: 537098 Color: 3
Size: 462873 Color: 13

Bin 1647: 30 of cap free
Amount of items: 3
Items: 
Size: 577941 Color: 7
Size: 223024 Color: 19
Size: 199006 Color: 5

Bin 1648: 30 of cap free
Amount of items: 2
Items: 
Size: 578069 Color: 17
Size: 421902 Color: 16

Bin 1649: 30 of cap free
Amount of items: 2
Items: 
Size: 586005 Color: 8
Size: 413966 Color: 5

Bin 1650: 30 of cap free
Amount of items: 2
Items: 
Size: 591435 Color: 8
Size: 408536 Color: 0

Bin 1651: 30 of cap free
Amount of items: 2
Items: 
Size: 608856 Color: 14
Size: 391115 Color: 18

Bin 1652: 30 of cap free
Amount of items: 2
Items: 
Size: 613274 Color: 6
Size: 386697 Color: 14

Bin 1653: 30 of cap free
Amount of items: 2
Items: 
Size: 613713 Color: 5
Size: 386258 Color: 14

Bin 1654: 30 of cap free
Amount of items: 2
Items: 
Size: 614790 Color: 18
Size: 385181 Color: 8

Bin 1655: 30 of cap free
Amount of items: 2
Items: 
Size: 629261 Color: 10
Size: 370710 Color: 17

Bin 1656: 30 of cap free
Amount of items: 2
Items: 
Size: 656208 Color: 1
Size: 343763 Color: 7

Bin 1657: 30 of cap free
Amount of items: 2
Items: 
Size: 661272 Color: 7
Size: 338699 Color: 12

Bin 1658: 30 of cap free
Amount of items: 2
Items: 
Size: 670599 Color: 1
Size: 329372 Color: 16

Bin 1659: 30 of cap free
Amount of items: 2
Items: 
Size: 686590 Color: 13
Size: 313381 Color: 8

Bin 1660: 30 of cap free
Amount of items: 2
Items: 
Size: 694207 Color: 11
Size: 305764 Color: 13

Bin 1661: 30 of cap free
Amount of items: 2
Items: 
Size: 706408 Color: 9
Size: 293563 Color: 5

Bin 1662: 30 of cap free
Amount of items: 2
Items: 
Size: 723585 Color: 16
Size: 276386 Color: 6

Bin 1663: 30 of cap free
Amount of items: 2
Items: 
Size: 729411 Color: 2
Size: 270560 Color: 13

Bin 1664: 30 of cap free
Amount of items: 2
Items: 
Size: 741889 Color: 10
Size: 258082 Color: 18

Bin 1665: 30 of cap free
Amount of items: 2
Items: 
Size: 747723 Color: 17
Size: 252248 Color: 16

Bin 1666: 30 of cap free
Amount of items: 2
Items: 
Size: 761327 Color: 1
Size: 238644 Color: 9

Bin 1667: 30 of cap free
Amount of items: 2
Items: 
Size: 770782 Color: 7
Size: 229189 Color: 10

Bin 1668: 30 of cap free
Amount of items: 2
Items: 
Size: 783106 Color: 18
Size: 216865 Color: 12

Bin 1669: 30 of cap free
Amount of items: 2
Items: 
Size: 791484 Color: 2
Size: 208487 Color: 12

Bin 1670: 30 of cap free
Amount of items: 3
Items: 
Size: 795732 Color: 14
Size: 102239 Color: 5
Size: 102000 Color: 10

Bin 1671: 31 of cap free
Amount of items: 3
Items: 
Size: 517441 Color: 11
Size: 254654 Color: 12
Size: 227875 Color: 14

Bin 1672: 31 of cap free
Amount of items: 2
Items: 
Size: 524686 Color: 15
Size: 475284 Color: 8

Bin 1673: 31 of cap free
Amount of items: 2
Items: 
Size: 545768 Color: 10
Size: 454202 Color: 5

Bin 1674: 31 of cap free
Amount of items: 2
Items: 
Size: 557472 Color: 2
Size: 442498 Color: 18

Bin 1675: 31 of cap free
Amount of items: 2
Items: 
Size: 599630 Color: 17
Size: 400340 Color: 9

Bin 1676: 31 of cap free
Amount of items: 2
Items: 
Size: 603984 Color: 2
Size: 395986 Color: 0

Bin 1677: 31 of cap free
Amount of items: 2
Items: 
Size: 617677 Color: 11
Size: 382293 Color: 10

Bin 1678: 31 of cap free
Amount of items: 2
Items: 
Size: 626178 Color: 9
Size: 373792 Color: 3

Bin 1679: 31 of cap free
Amount of items: 2
Items: 
Size: 671692 Color: 4
Size: 328278 Color: 9

Bin 1680: 31 of cap free
Amount of items: 2
Items: 
Size: 675202 Color: 3
Size: 324768 Color: 10

Bin 1681: 31 of cap free
Amount of items: 2
Items: 
Size: 678684 Color: 9
Size: 321286 Color: 15

Bin 1682: 31 of cap free
Amount of items: 2
Items: 
Size: 683167 Color: 16
Size: 316803 Color: 4

Bin 1683: 31 of cap free
Amount of items: 3
Items: 
Size: 710348 Color: 14
Size: 145722 Color: 0
Size: 143900 Color: 2

Bin 1684: 31 of cap free
Amount of items: 2
Items: 
Size: 725365 Color: 0
Size: 274605 Color: 2

Bin 1685: 31 of cap free
Amount of items: 3
Items: 
Size: 738666 Color: 9
Size: 140243 Color: 8
Size: 121061 Color: 7

Bin 1686: 31 of cap free
Amount of items: 2
Items: 
Size: 746293 Color: 0
Size: 253677 Color: 10

Bin 1687: 31 of cap free
Amount of items: 3
Items: 
Size: 769663 Color: 7
Size: 116304 Color: 17
Size: 114003 Color: 7

Bin 1688: 31 of cap free
Amount of items: 2
Items: 
Size: 777023 Color: 19
Size: 222947 Color: 1

Bin 1689: 32 of cap free
Amount of items: 3
Items: 
Size: 369856 Color: 11
Size: 346496 Color: 0
Size: 283617 Color: 18

Bin 1690: 32 of cap free
Amount of items: 2
Items: 
Size: 501679 Color: 16
Size: 498290 Color: 19

Bin 1691: 32 of cap free
Amount of items: 2
Items: 
Size: 515279 Color: 15
Size: 484690 Color: 16

Bin 1692: 32 of cap free
Amount of items: 2
Items: 
Size: 526239 Color: 16
Size: 473730 Color: 17

Bin 1693: 32 of cap free
Amount of items: 2
Items: 
Size: 538735 Color: 1
Size: 461234 Color: 14

Bin 1694: 32 of cap free
Amount of items: 2
Items: 
Size: 544561 Color: 16
Size: 455408 Color: 8

Bin 1695: 32 of cap free
Amount of items: 2
Items: 
Size: 569298 Color: 8
Size: 430671 Color: 11

Bin 1696: 32 of cap free
Amount of items: 2
Items: 
Size: 577834 Color: 0
Size: 422135 Color: 8

Bin 1697: 32 of cap free
Amount of items: 2
Items: 
Size: 612283 Color: 11
Size: 387686 Color: 14

Bin 1698: 32 of cap free
Amount of items: 2
Items: 
Size: 651937 Color: 5
Size: 348032 Color: 11

Bin 1699: 32 of cap free
Amount of items: 2
Items: 
Size: 656870 Color: 8
Size: 343099 Color: 16

Bin 1700: 32 of cap free
Amount of items: 2
Items: 
Size: 682818 Color: 10
Size: 317151 Color: 6

Bin 1701: 32 of cap free
Amount of items: 2
Items: 
Size: 704341 Color: 1
Size: 295628 Color: 7

Bin 1702: 32 of cap free
Amount of items: 2
Items: 
Size: 715083 Color: 7
Size: 284886 Color: 2

Bin 1703: 32 of cap free
Amount of items: 2
Items: 
Size: 717088 Color: 2
Size: 282881 Color: 4

Bin 1704: 32 of cap free
Amount of items: 2
Items: 
Size: 751211 Color: 8
Size: 248758 Color: 3

Bin 1705: 32 of cap free
Amount of items: 2
Items: 
Size: 755015 Color: 0
Size: 244954 Color: 2

Bin 1706: 32 of cap free
Amount of items: 2
Items: 
Size: 764597 Color: 4
Size: 235372 Color: 14

Bin 1707: 32 of cap free
Amount of items: 2
Items: 
Size: 764961 Color: 9
Size: 235008 Color: 16

Bin 1708: 32 of cap free
Amount of items: 3
Items: 
Size: 775819 Color: 15
Size: 116864 Color: 11
Size: 107286 Color: 6

Bin 1709: 32 of cap free
Amount of items: 3
Items: 
Size: 794255 Color: 13
Size: 102871 Color: 3
Size: 102843 Color: 9

Bin 1710: 33 of cap free
Amount of items: 3
Items: 
Size: 397596 Color: 8
Size: 341716 Color: 16
Size: 260656 Color: 4

Bin 1711: 33 of cap free
Amount of items: 2
Items: 
Size: 514021 Color: 0
Size: 485947 Color: 10

Bin 1712: 33 of cap free
Amount of items: 2
Items: 
Size: 515006 Color: 12
Size: 484962 Color: 0

Bin 1713: 33 of cap free
Amount of items: 3
Items: 
Size: 520617 Color: 1
Size: 258843 Color: 4
Size: 220508 Color: 14

Bin 1714: 33 of cap free
Amount of items: 2
Items: 
Size: 554054 Color: 18
Size: 445914 Color: 4

Bin 1715: 33 of cap free
Amount of items: 2
Items: 
Size: 557058 Color: 1
Size: 442910 Color: 17

Bin 1716: 33 of cap free
Amount of items: 2
Items: 
Size: 557626 Color: 7
Size: 442342 Color: 10

Bin 1717: 33 of cap free
Amount of items: 2
Items: 
Size: 576720 Color: 16
Size: 423248 Color: 12

Bin 1718: 33 of cap free
Amount of items: 2
Items: 
Size: 582711 Color: 7
Size: 417257 Color: 6

Bin 1719: 33 of cap free
Amount of items: 3
Items: 
Size: 620144 Color: 7
Size: 211773 Color: 17
Size: 168051 Color: 14

Bin 1720: 33 of cap free
Amount of items: 3
Items: 
Size: 624959 Color: 2
Size: 211777 Color: 4
Size: 163232 Color: 16

Bin 1721: 33 of cap free
Amount of items: 2
Items: 
Size: 650880 Color: 9
Size: 349088 Color: 11

Bin 1722: 33 of cap free
Amount of items: 2
Items: 
Size: 651246 Color: 0
Size: 348722 Color: 10

Bin 1723: 33 of cap free
Amount of items: 2
Items: 
Size: 651973 Color: 13
Size: 347995 Color: 9

Bin 1724: 33 of cap free
Amount of items: 2
Items: 
Size: 670481 Color: 3
Size: 329487 Color: 16

Bin 1725: 33 of cap free
Amount of items: 2
Items: 
Size: 673647 Color: 7
Size: 326321 Color: 2

Bin 1726: 33 of cap free
Amount of items: 2
Items: 
Size: 674624 Color: 13
Size: 325344 Color: 2

Bin 1727: 33 of cap free
Amount of items: 2
Items: 
Size: 677991 Color: 9
Size: 321977 Color: 11

Bin 1728: 33 of cap free
Amount of items: 2
Items: 
Size: 687402 Color: 16
Size: 312566 Color: 14

Bin 1729: 33 of cap free
Amount of items: 2
Items: 
Size: 688047 Color: 5
Size: 311921 Color: 13

Bin 1730: 33 of cap free
Amount of items: 2
Items: 
Size: 692658 Color: 13
Size: 307310 Color: 4

Bin 1731: 33 of cap free
Amount of items: 3
Items: 
Size: 710898 Color: 18
Size: 153146 Color: 5
Size: 135924 Color: 4

Bin 1732: 33 of cap free
Amount of items: 3
Items: 
Size: 712189 Color: 16
Size: 147986 Color: 6
Size: 139793 Color: 11

Bin 1733: 33 of cap free
Amount of items: 2
Items: 
Size: 716050 Color: 16
Size: 283918 Color: 13

Bin 1734: 33 of cap free
Amount of items: 2
Items: 
Size: 743847 Color: 3
Size: 256121 Color: 7

Bin 1735: 33 of cap free
Amount of items: 3
Items: 
Size: 750811 Color: 8
Size: 132001 Color: 13
Size: 117156 Color: 10

Bin 1736: 33 of cap free
Amount of items: 2
Items: 
Size: 763878 Color: 8
Size: 236090 Color: 18

Bin 1737: 33 of cap free
Amount of items: 2
Items: 
Size: 765020 Color: 10
Size: 234948 Color: 16

Bin 1738: 33 of cap free
Amount of items: 3
Items: 
Size: 770384 Color: 13
Size: 115518 Color: 6
Size: 114066 Color: 10

Bin 1739: 34 of cap free
Amount of items: 3
Items: 
Size: 422086 Color: 8
Size: 310453 Color: 6
Size: 267428 Color: 1

Bin 1740: 34 of cap free
Amount of items: 2
Items: 
Size: 503995 Color: 17
Size: 495972 Color: 13

Bin 1741: 34 of cap free
Amount of items: 2
Items: 
Size: 514478 Color: 5
Size: 485489 Color: 6

Bin 1742: 34 of cap free
Amount of items: 2
Items: 
Size: 523310 Color: 13
Size: 476657 Color: 14

Bin 1743: 34 of cap free
Amount of items: 2
Items: 
Size: 524749 Color: 9
Size: 475218 Color: 19

Bin 1744: 34 of cap free
Amount of items: 2
Items: 
Size: 552397 Color: 13
Size: 447570 Color: 5

Bin 1745: 34 of cap free
Amount of items: 2
Items: 
Size: 581031 Color: 13
Size: 418936 Color: 3

Bin 1746: 34 of cap free
Amount of items: 2
Items: 
Size: 614166 Color: 6
Size: 385801 Color: 18

Bin 1747: 34 of cap free
Amount of items: 2
Items: 
Size: 633430 Color: 6
Size: 366537 Color: 2

Bin 1748: 34 of cap free
Amount of items: 2
Items: 
Size: 636458 Color: 15
Size: 363509 Color: 3

Bin 1749: 34 of cap free
Amount of items: 2
Items: 
Size: 636579 Color: 0
Size: 363388 Color: 16

Bin 1750: 34 of cap free
Amount of items: 2
Items: 
Size: 640167 Color: 4
Size: 359800 Color: 18

Bin 1751: 34 of cap free
Amount of items: 2
Items: 
Size: 645293 Color: 12
Size: 354674 Color: 9

Bin 1752: 34 of cap free
Amount of items: 3
Items: 
Size: 645580 Color: 11
Size: 186010 Color: 2
Size: 168377 Color: 10

Bin 1753: 34 of cap free
Amount of items: 3
Items: 
Size: 648260 Color: 0
Size: 189961 Color: 13
Size: 161746 Color: 9

Bin 1754: 34 of cap free
Amount of items: 2
Items: 
Size: 648430 Color: 8
Size: 351537 Color: 9

Bin 1755: 34 of cap free
Amount of items: 2
Items: 
Size: 665627 Color: 7
Size: 334340 Color: 15

Bin 1756: 34 of cap free
Amount of items: 2
Items: 
Size: 685085 Color: 16
Size: 314882 Color: 15

Bin 1757: 34 of cap free
Amount of items: 2
Items: 
Size: 688413 Color: 18
Size: 311554 Color: 4

Bin 1758: 34 of cap free
Amount of items: 2
Items: 
Size: 726126 Color: 12
Size: 273841 Color: 10

Bin 1759: 34 of cap free
Amount of items: 2
Items: 
Size: 736625 Color: 17
Size: 263342 Color: 4

Bin 1760: 34 of cap free
Amount of items: 2
Items: 
Size: 755791 Color: 17
Size: 244176 Color: 5

Bin 1761: 34 of cap free
Amount of items: 2
Items: 
Size: 764330 Color: 6
Size: 235637 Color: 5

Bin 1762: 34 of cap free
Amount of items: 2
Items: 
Size: 769961 Color: 19
Size: 230006 Color: 17

Bin 1763: 34 of cap free
Amount of items: 2
Items: 
Size: 785379 Color: 8
Size: 214588 Color: 1

Bin 1764: 34 of cap free
Amount of items: 2
Items: 
Size: 787265 Color: 11
Size: 212702 Color: 6

Bin 1765: 35 of cap free
Amount of items: 2
Items: 
Size: 506332 Color: 16
Size: 493634 Color: 3

Bin 1766: 35 of cap free
Amount of items: 2
Items: 
Size: 508800 Color: 1
Size: 491166 Color: 3

Bin 1767: 35 of cap free
Amount of items: 2
Items: 
Size: 508946 Color: 1
Size: 491020 Color: 13

Bin 1768: 35 of cap free
Amount of items: 2
Items: 
Size: 520536 Color: 3
Size: 479430 Color: 16

Bin 1769: 35 of cap free
Amount of items: 2
Items: 
Size: 533223 Color: 14
Size: 466743 Color: 8

Bin 1770: 35 of cap free
Amount of items: 2
Items: 
Size: 541001 Color: 10
Size: 458965 Color: 16

Bin 1771: 35 of cap free
Amount of items: 2
Items: 
Size: 597737 Color: 7
Size: 402229 Color: 6

Bin 1772: 35 of cap free
Amount of items: 2
Items: 
Size: 599225 Color: 11
Size: 400741 Color: 7

Bin 1773: 35 of cap free
Amount of items: 3
Items: 
Size: 614103 Color: 17
Size: 193577 Color: 9
Size: 192286 Color: 8

Bin 1774: 35 of cap free
Amount of items: 2
Items: 
Size: 634482 Color: 9
Size: 365484 Color: 14

Bin 1775: 35 of cap free
Amount of items: 2
Items: 
Size: 671596 Color: 14
Size: 328370 Color: 15

Bin 1776: 35 of cap free
Amount of items: 2
Items: 
Size: 674969 Color: 5
Size: 324997 Color: 7

Bin 1777: 35 of cap free
Amount of items: 2
Items: 
Size: 679679 Color: 6
Size: 320287 Color: 4

Bin 1778: 35 of cap free
Amount of items: 2
Items: 
Size: 680364 Color: 16
Size: 319602 Color: 13

Bin 1779: 35 of cap free
Amount of items: 2
Items: 
Size: 688457 Color: 16
Size: 311509 Color: 2

Bin 1780: 35 of cap free
Amount of items: 2
Items: 
Size: 689237 Color: 6
Size: 310729 Color: 0

Bin 1781: 35 of cap free
Amount of items: 2
Items: 
Size: 694864 Color: 6
Size: 305102 Color: 3

Bin 1782: 35 of cap free
Amount of items: 2
Items: 
Size: 698930 Color: 17
Size: 301036 Color: 9

Bin 1783: 35 of cap free
Amount of items: 2
Items: 
Size: 755723 Color: 16
Size: 244243 Color: 12

Bin 1784: 35 of cap free
Amount of items: 2
Items: 
Size: 758406 Color: 16
Size: 241560 Color: 12

Bin 1785: 35 of cap free
Amount of items: 2
Items: 
Size: 782403 Color: 2
Size: 217563 Color: 11

Bin 1786: 35 of cap free
Amount of items: 2
Items: 
Size: 786827 Color: 2
Size: 213139 Color: 5

Bin 1787: 35 of cap free
Amount of items: 3
Items: 
Size: 795468 Color: 13
Size: 102531 Color: 18
Size: 101967 Color: 5

Bin 1788: 36 of cap free
Amount of items: 3
Items: 
Size: 422106 Color: 5
Size: 309591 Color: 0
Size: 268268 Color: 13

Bin 1789: 36 of cap free
Amount of items: 2
Items: 
Size: 505989 Color: 13
Size: 493976 Color: 4

Bin 1790: 36 of cap free
Amount of items: 2
Items: 
Size: 508183 Color: 3
Size: 491782 Color: 11

Bin 1791: 36 of cap free
Amount of items: 2
Items: 
Size: 557670 Color: 15
Size: 442295 Color: 18

Bin 1792: 36 of cap free
Amount of items: 2
Items: 
Size: 560848 Color: 11
Size: 439117 Color: 6

Bin 1793: 36 of cap free
Amount of items: 2
Items: 
Size: 563419 Color: 5
Size: 436546 Color: 17

Bin 1794: 36 of cap free
Amount of items: 2
Items: 
Size: 600087 Color: 17
Size: 399878 Color: 13

Bin 1795: 36 of cap free
Amount of items: 2
Items: 
Size: 601672 Color: 12
Size: 398293 Color: 10

Bin 1796: 36 of cap free
Amount of items: 3
Items: 
Size: 614634 Color: 1
Size: 193260 Color: 7
Size: 192071 Color: 19

Bin 1797: 36 of cap free
Amount of items: 2
Items: 
Size: 618080 Color: 8
Size: 381885 Color: 18

Bin 1798: 36 of cap free
Amount of items: 3
Items: 
Size: 618103 Color: 7
Size: 196166 Color: 0
Size: 185696 Color: 5

Bin 1799: 36 of cap free
Amount of items: 2
Items: 
Size: 661374 Color: 17
Size: 338591 Color: 11

Bin 1800: 36 of cap free
Amount of items: 2
Items: 
Size: 679168 Color: 1
Size: 320797 Color: 14

Bin 1801: 36 of cap free
Amount of items: 2
Items: 
Size: 699273 Color: 16
Size: 300692 Color: 12

Bin 1802: 36 of cap free
Amount of items: 2
Items: 
Size: 713850 Color: 16
Size: 286115 Color: 3

Bin 1803: 36 of cap free
Amount of items: 2
Items: 
Size: 715304 Color: 8
Size: 284661 Color: 5

Bin 1804: 36 of cap free
Amount of items: 2
Items: 
Size: 740184 Color: 15
Size: 259781 Color: 6

Bin 1805: 36 of cap free
Amount of items: 2
Items: 
Size: 753891 Color: 3
Size: 246074 Color: 4

Bin 1806: 36 of cap free
Amount of items: 2
Items: 
Size: 764248 Color: 5
Size: 235717 Color: 8

Bin 1807: 36 of cap free
Amount of items: 2
Items: 
Size: 774362 Color: 15
Size: 225603 Color: 16

Bin 1808: 36 of cap free
Amount of items: 2
Items: 
Size: 781217 Color: 10
Size: 218748 Color: 3

Bin 1809: 37 of cap free
Amount of items: 3
Items: 
Size: 361216 Color: 11
Size: 360920 Color: 6
Size: 277828 Color: 18

Bin 1810: 37 of cap free
Amount of items: 3
Items: 
Size: 431377 Color: 5
Size: 286507 Color: 2
Size: 282080 Color: 15

Bin 1811: 37 of cap free
Amount of items: 2
Items: 
Size: 514432 Color: 15
Size: 485532 Color: 17

Bin 1812: 37 of cap free
Amount of items: 2
Items: 
Size: 515356 Color: 16
Size: 484608 Color: 13

Bin 1813: 37 of cap free
Amount of items: 2
Items: 
Size: 517429 Color: 12
Size: 482535 Color: 6

Bin 1814: 37 of cap free
Amount of items: 2
Items: 
Size: 533678 Color: 17
Size: 466286 Color: 4

Bin 1815: 37 of cap free
Amount of items: 2
Items: 
Size: 548539 Color: 8
Size: 451425 Color: 1

Bin 1816: 37 of cap free
Amount of items: 2
Items: 
Size: 588875 Color: 6
Size: 411089 Color: 13

Bin 1817: 37 of cap free
Amount of items: 2
Items: 
Size: 597636 Color: 17
Size: 402328 Color: 16

Bin 1818: 37 of cap free
Amount of items: 2
Items: 
Size: 608198 Color: 7
Size: 391766 Color: 14

Bin 1819: 37 of cap free
Amount of items: 2
Items: 
Size: 612495 Color: 14
Size: 387469 Color: 5

Bin 1820: 37 of cap free
Amount of items: 2
Items: 
Size: 633917 Color: 5
Size: 366047 Color: 7

Bin 1821: 37 of cap free
Amount of items: 2
Items: 
Size: 645432 Color: 17
Size: 354532 Color: 8

Bin 1822: 37 of cap free
Amount of items: 2
Items: 
Size: 678303 Color: 8
Size: 321661 Color: 12

Bin 1823: 37 of cap free
Amount of items: 2
Items: 
Size: 682501 Color: 3
Size: 317463 Color: 18

Bin 1824: 37 of cap free
Amount of items: 2
Items: 
Size: 700553 Color: 1
Size: 299411 Color: 8

Bin 1825: 37 of cap free
Amount of items: 2
Items: 
Size: 713313 Color: 12
Size: 286651 Color: 8

Bin 1826: 37 of cap free
Amount of items: 2
Items: 
Size: 757445 Color: 17
Size: 242519 Color: 7

Bin 1827: 37 of cap free
Amount of items: 2
Items: 
Size: 777718 Color: 14
Size: 222246 Color: 15

Bin 1828: 37 of cap free
Amount of items: 3
Items: 
Size: 790813 Color: 4
Size: 104609 Color: 12
Size: 104542 Color: 2

Bin 1829: 38 of cap free
Amount of items: 2
Items: 
Size: 514614 Color: 12
Size: 485349 Color: 3

Bin 1830: 38 of cap free
Amount of items: 2
Items: 
Size: 560149 Color: 18
Size: 439814 Color: 15

Bin 1831: 38 of cap free
Amount of items: 2
Items: 
Size: 571601 Color: 19
Size: 428362 Color: 6

Bin 1832: 38 of cap free
Amount of items: 2
Items: 
Size: 594922 Color: 4
Size: 405041 Color: 11

Bin 1833: 38 of cap free
Amount of items: 2
Items: 
Size: 617299 Color: 9
Size: 382664 Color: 19

Bin 1834: 38 of cap free
Amount of items: 2
Items: 
Size: 619320 Color: 19
Size: 380643 Color: 5

Bin 1835: 38 of cap free
Amount of items: 2
Items: 
Size: 634042 Color: 17
Size: 365921 Color: 10

Bin 1836: 38 of cap free
Amount of items: 2
Items: 
Size: 671065 Color: 0
Size: 328898 Color: 11

Bin 1837: 38 of cap free
Amount of items: 2
Items: 
Size: 697547 Color: 13
Size: 302416 Color: 10

Bin 1838: 38 of cap free
Amount of items: 2
Items: 
Size: 759140 Color: 19
Size: 240823 Color: 16

Bin 1839: 38 of cap free
Amount of items: 2
Items: 
Size: 772583 Color: 1
Size: 227380 Color: 8

Bin 1840: 38 of cap free
Amount of items: 3
Items: 
Size: 788302 Color: 2
Size: 105905 Color: 10
Size: 105756 Color: 17

Bin 1841: 39 of cap free
Amount of items: 2
Items: 
Size: 507868 Color: 7
Size: 492094 Color: 3

Bin 1842: 39 of cap free
Amount of items: 2
Items: 
Size: 521336 Color: 15
Size: 478626 Color: 16

Bin 1843: 39 of cap free
Amount of items: 2
Items: 
Size: 524615 Color: 19
Size: 475347 Color: 7

Bin 1844: 39 of cap free
Amount of items: 2
Items: 
Size: 533989 Color: 15
Size: 465973 Color: 5

Bin 1845: 39 of cap free
Amount of items: 2
Items: 
Size: 564617 Color: 7
Size: 435345 Color: 8

Bin 1846: 39 of cap free
Amount of items: 2
Items: 
Size: 575009 Color: 1
Size: 424953 Color: 7

Bin 1847: 39 of cap free
Amount of items: 3
Items: 
Size: 575961 Color: 0
Size: 227134 Color: 9
Size: 196867 Color: 19

Bin 1848: 39 of cap free
Amount of items: 2
Items: 
Size: 578767 Color: 13
Size: 421195 Color: 12

Bin 1849: 39 of cap free
Amount of items: 2
Items: 
Size: 584968 Color: 16
Size: 414994 Color: 13

Bin 1850: 39 of cap free
Amount of items: 2
Items: 
Size: 616035 Color: 9
Size: 383927 Color: 5

Bin 1851: 39 of cap free
Amount of items: 2
Items: 
Size: 618642 Color: 6
Size: 381320 Color: 2

Bin 1852: 39 of cap free
Amount of items: 2
Items: 
Size: 630324 Color: 19
Size: 369638 Color: 17

Bin 1853: 39 of cap free
Amount of items: 2
Items: 
Size: 637101 Color: 18
Size: 362861 Color: 6

Bin 1854: 39 of cap free
Amount of items: 2
Items: 
Size: 638820 Color: 9
Size: 361142 Color: 18

Bin 1855: 39 of cap free
Amount of items: 2
Items: 
Size: 652962 Color: 9
Size: 347000 Color: 19

Bin 1856: 39 of cap free
Amount of items: 2
Items: 
Size: 656084 Color: 4
Size: 343878 Color: 19

Bin 1857: 39 of cap free
Amount of items: 2
Items: 
Size: 705201 Color: 6
Size: 294761 Color: 19

Bin 1858: 39 of cap free
Amount of items: 2
Items: 
Size: 735254 Color: 18
Size: 264708 Color: 16

Bin 1859: 39 of cap free
Amount of items: 2
Items: 
Size: 735644 Color: 9
Size: 264318 Color: 13

Bin 1860: 39 of cap free
Amount of items: 2
Items: 
Size: 743556 Color: 8
Size: 256406 Color: 7

Bin 1861: 39 of cap free
Amount of items: 2
Items: 
Size: 769290 Color: 6
Size: 230672 Color: 4

Bin 1862: 39 of cap free
Amount of items: 2
Items: 
Size: 776348 Color: 16
Size: 223614 Color: 15

Bin 1863: 39 of cap free
Amount of items: 3
Items: 
Size: 797970 Color: 13
Size: 101053 Color: 17
Size: 100939 Color: 18

Bin 1864: 40 of cap free
Amount of items: 2
Items: 
Size: 505398 Color: 4
Size: 494563 Color: 19

Bin 1865: 40 of cap free
Amount of items: 2
Items: 
Size: 534678 Color: 11
Size: 465283 Color: 13

Bin 1866: 40 of cap free
Amount of items: 2
Items: 
Size: 583752 Color: 7
Size: 416209 Color: 18

Bin 1867: 40 of cap free
Amount of items: 2
Items: 
Size: 588422 Color: 9
Size: 411539 Color: 11

Bin 1868: 40 of cap free
Amount of items: 2
Items: 
Size: 596496 Color: 9
Size: 403465 Color: 19

Bin 1869: 40 of cap free
Amount of items: 2
Items: 
Size: 611612 Color: 1
Size: 388349 Color: 5

Bin 1870: 40 of cap free
Amount of items: 3
Items: 
Size: 620480 Color: 1
Size: 196203 Color: 12
Size: 183278 Color: 14

Bin 1871: 40 of cap free
Amount of items: 2
Items: 
Size: 694477 Color: 1
Size: 305484 Color: 4

Bin 1872: 40 of cap free
Amount of items: 3
Items: 
Size: 703475 Color: 2
Size: 148332 Color: 2
Size: 148154 Color: 14

Bin 1873: 40 of cap free
Amount of items: 2
Items: 
Size: 706286 Color: 16
Size: 293675 Color: 8

Bin 1874: 40 of cap free
Amount of items: 2
Items: 
Size: 736957 Color: 7
Size: 263004 Color: 2

Bin 1875: 40 of cap free
Amount of items: 2
Items: 
Size: 751798 Color: 1
Size: 248163 Color: 8

Bin 1876: 40 of cap free
Amount of items: 2
Items: 
Size: 754775 Color: 11
Size: 245186 Color: 13

Bin 1877: 40 of cap free
Amount of items: 2
Items: 
Size: 758886 Color: 2
Size: 241075 Color: 17

Bin 1878: 40 of cap free
Amount of items: 2
Items: 
Size: 781283 Color: 0
Size: 218678 Color: 1

Bin 1879: 40 of cap free
Amount of items: 2
Items: 
Size: 790167 Color: 17
Size: 209794 Color: 16

Bin 1880: 41 of cap free
Amount of items: 2
Items: 
Size: 519487 Color: 15
Size: 480473 Color: 9

Bin 1881: 41 of cap free
Amount of items: 2
Items: 
Size: 521859 Color: 8
Size: 478101 Color: 5

Bin 1882: 41 of cap free
Amount of items: 2
Items: 
Size: 531447 Color: 19
Size: 468513 Color: 8

Bin 1883: 41 of cap free
Amount of items: 2
Items: 
Size: 555118 Color: 17
Size: 444842 Color: 18

Bin 1884: 41 of cap free
Amount of items: 2
Items: 
Size: 588217 Color: 2
Size: 411743 Color: 5

Bin 1885: 41 of cap free
Amount of items: 2
Items: 
Size: 663949 Color: 17
Size: 336011 Color: 11

Bin 1886: 41 of cap free
Amount of items: 2
Items: 
Size: 682212 Color: 19
Size: 317748 Color: 11

Bin 1887: 41 of cap free
Amount of items: 2
Items: 
Size: 684724 Color: 1
Size: 315236 Color: 13

Bin 1888: 41 of cap free
Amount of items: 2
Items: 
Size: 696228 Color: 16
Size: 303732 Color: 10

Bin 1889: 41 of cap free
Amount of items: 3
Items: 
Size: 712794 Color: 2
Size: 150911 Color: 10
Size: 136255 Color: 14

Bin 1890: 41 of cap free
Amount of items: 2
Items: 
Size: 713838 Color: 1
Size: 286122 Color: 16

Bin 1891: 41 of cap free
Amount of items: 2
Items: 
Size: 717623 Color: 7
Size: 282337 Color: 2

Bin 1892: 41 of cap free
Amount of items: 3
Items: 
Size: 731151 Color: 14
Size: 144024 Color: 8
Size: 124785 Color: 12

Bin 1893: 41 of cap free
Amount of items: 2
Items: 
Size: 758760 Color: 18
Size: 241200 Color: 13

Bin 1894: 41 of cap free
Amount of items: 2
Items: 
Size: 771912 Color: 4
Size: 228048 Color: 15

Bin 1895: 41 of cap free
Amount of items: 2
Items: 
Size: 780717 Color: 9
Size: 219243 Color: 11

Bin 1896: 41 of cap free
Amount of items: 2
Items: 
Size: 789745 Color: 4
Size: 210215 Color: 8

Bin 1897: 41 of cap free
Amount of items: 2
Items: 
Size: 796814 Color: 3
Size: 203146 Color: 8

Bin 1898: 42 of cap free
Amount of items: 3
Items: 
Size: 517976 Color: 17
Size: 259577 Color: 2
Size: 222406 Color: 0

Bin 1899: 42 of cap free
Amount of items: 2
Items: 
Size: 539030 Color: 9
Size: 460929 Color: 0

Bin 1900: 42 of cap free
Amount of items: 2
Items: 
Size: 549369 Color: 9
Size: 450590 Color: 1

Bin 1901: 42 of cap free
Amount of items: 2
Items: 
Size: 551894 Color: 3
Size: 448065 Color: 13

Bin 1902: 42 of cap free
Amount of items: 2
Items: 
Size: 554138 Color: 8
Size: 445821 Color: 14

Bin 1903: 42 of cap free
Amount of items: 2
Items: 
Size: 563881 Color: 11
Size: 436078 Color: 14

Bin 1904: 42 of cap free
Amount of items: 2
Items: 
Size: 580044 Color: 13
Size: 419915 Color: 8

Bin 1905: 42 of cap free
Amount of items: 2
Items: 
Size: 581178 Color: 4
Size: 418781 Color: 7

Bin 1906: 42 of cap free
Amount of items: 2
Items: 
Size: 603486 Color: 5
Size: 396473 Color: 3

Bin 1907: 42 of cap free
Amount of items: 2
Items: 
Size: 645766 Color: 5
Size: 354193 Color: 11

Bin 1908: 42 of cap free
Amount of items: 2
Items: 
Size: 665476 Color: 7
Size: 334483 Color: 17

Bin 1909: 42 of cap free
Amount of items: 2
Items: 
Size: 707287 Color: 19
Size: 292672 Color: 4

Bin 1910: 42 of cap free
Amount of items: 3
Items: 
Size: 712649 Color: 19
Size: 147738 Color: 12
Size: 139572 Color: 11

Bin 1911: 42 of cap free
Amount of items: 2
Items: 
Size: 718997 Color: 12
Size: 280962 Color: 0

Bin 1912: 42 of cap free
Amount of items: 2
Items: 
Size: 731544 Color: 2
Size: 268415 Color: 5

Bin 1913: 42 of cap free
Amount of items: 3
Items: 
Size: 736024 Color: 2
Size: 142000 Color: 8
Size: 121935 Color: 12

Bin 1914: 42 of cap free
Amount of items: 2
Items: 
Size: 757881 Color: 8
Size: 242078 Color: 7

Bin 1915: 42 of cap free
Amount of items: 2
Items: 
Size: 763925 Color: 13
Size: 236034 Color: 16

Bin 1916: 42 of cap free
Amount of items: 2
Items: 
Size: 765269 Color: 18
Size: 234690 Color: 17

Bin 1917: 42 of cap free
Amount of items: 3
Items: 
Size: 773845 Color: 4
Size: 114991 Color: 15
Size: 111123 Color: 17

Bin 1918: 42 of cap free
Amount of items: 2
Items: 
Size: 794484 Color: 18
Size: 205475 Color: 11

Bin 1919: 43 of cap free
Amount of items: 3
Items: 
Size: 396428 Color: 5
Size: 324022 Color: 11
Size: 279508 Color: 12

Bin 1920: 43 of cap free
Amount of items: 2
Items: 
Size: 515590 Color: 14
Size: 484368 Color: 11

Bin 1921: 43 of cap free
Amount of items: 2
Items: 
Size: 516474 Color: 4
Size: 483484 Color: 15

Bin 1922: 43 of cap free
Amount of items: 2
Items: 
Size: 516607 Color: 7
Size: 483351 Color: 15

Bin 1923: 43 of cap free
Amount of items: 2
Items: 
Size: 522035 Color: 16
Size: 477923 Color: 18

Bin 1924: 43 of cap free
Amount of items: 2
Items: 
Size: 569629 Color: 2
Size: 430329 Color: 16

Bin 1925: 43 of cap free
Amount of items: 2
Items: 
Size: 586316 Color: 19
Size: 413642 Color: 14

Bin 1926: 43 of cap free
Amount of items: 2
Items: 
Size: 587646 Color: 14
Size: 412312 Color: 12

Bin 1927: 43 of cap free
Amount of items: 2
Items: 
Size: 593376 Color: 7
Size: 406582 Color: 1

Bin 1928: 43 of cap free
Amount of items: 2
Items: 
Size: 611320 Color: 1
Size: 388638 Color: 2

Bin 1929: 43 of cap free
Amount of items: 2
Items: 
Size: 621406 Color: 4
Size: 378552 Color: 17

Bin 1930: 43 of cap free
Amount of items: 2
Items: 
Size: 624101 Color: 8
Size: 375857 Color: 15

Bin 1931: 43 of cap free
Amount of items: 2
Items: 
Size: 631294 Color: 19
Size: 368664 Color: 14

Bin 1932: 43 of cap free
Amount of items: 2
Items: 
Size: 649315 Color: 10
Size: 350643 Color: 5

Bin 1933: 43 of cap free
Amount of items: 2
Items: 
Size: 672806 Color: 3
Size: 327152 Color: 15

Bin 1934: 43 of cap free
Amount of items: 2
Items: 
Size: 675862 Color: 6
Size: 324096 Color: 9

Bin 1935: 43 of cap free
Amount of items: 2
Items: 
Size: 684451 Color: 4
Size: 315507 Color: 15

Bin 1936: 43 of cap free
Amount of items: 2
Items: 
Size: 722539 Color: 5
Size: 277419 Color: 4

Bin 1937: 43 of cap free
Amount of items: 2
Items: 
Size: 751721 Color: 5
Size: 248237 Color: 12

Bin 1938: 43 of cap free
Amount of items: 2
Items: 
Size: 782621 Color: 13
Size: 217337 Color: 9

Bin 1939: 44 of cap free
Amount of items: 2
Items: 
Size: 508450 Color: 5
Size: 491507 Color: 3

Bin 1940: 44 of cap free
Amount of items: 2
Items: 
Size: 508575 Color: 4
Size: 491382 Color: 11

Bin 1941: 44 of cap free
Amount of items: 2
Items: 
Size: 509984 Color: 4
Size: 489973 Color: 16

Bin 1942: 44 of cap free
Amount of items: 2
Items: 
Size: 512517 Color: 17
Size: 487440 Color: 7

Bin 1943: 44 of cap free
Amount of items: 2
Items: 
Size: 519739 Color: 16
Size: 480218 Color: 11

Bin 1944: 44 of cap free
Amount of items: 2
Items: 
Size: 535326 Color: 12
Size: 464631 Color: 7

Bin 1945: 44 of cap free
Amount of items: 2
Items: 
Size: 550538 Color: 13
Size: 449419 Color: 17

Bin 1946: 44 of cap free
Amount of items: 2
Items: 
Size: 551153 Color: 3
Size: 448804 Color: 17

Bin 1947: 44 of cap free
Amount of items: 2
Items: 
Size: 588162 Color: 5
Size: 411795 Color: 9

Bin 1948: 44 of cap free
Amount of items: 2
Items: 
Size: 596418 Color: 15
Size: 403539 Color: 10

Bin 1949: 44 of cap free
Amount of items: 2
Items: 
Size: 631515 Color: 5
Size: 368442 Color: 16

Bin 1950: 44 of cap free
Amount of items: 2
Items: 
Size: 670675 Color: 14
Size: 329282 Color: 10

Bin 1951: 44 of cap free
Amount of items: 2
Items: 
Size: 682587 Color: 15
Size: 317370 Color: 5

Bin 1952: 44 of cap free
Amount of items: 2
Items: 
Size: 689087 Color: 14
Size: 310870 Color: 0

Bin 1953: 44 of cap free
Amount of items: 2
Items: 
Size: 694509 Color: 4
Size: 305448 Color: 3

Bin 1954: 44 of cap free
Amount of items: 2
Items: 
Size: 699414 Color: 14
Size: 300543 Color: 12

Bin 1955: 44 of cap free
Amount of items: 2
Items: 
Size: 723161 Color: 5
Size: 276796 Color: 2

Bin 1956: 44 of cap free
Amount of items: 2
Items: 
Size: 724241 Color: 6
Size: 275716 Color: 19

Bin 1957: 44 of cap free
Amount of items: 2
Items: 
Size: 785189 Color: 1
Size: 214768 Color: 19

Bin 1958: 44 of cap free
Amount of items: 2
Items: 
Size: 790340 Color: 10
Size: 209617 Color: 14

Bin 1959: 44 of cap free
Amount of items: 2
Items: 
Size: 795176 Color: 3
Size: 204781 Color: 15

Bin 1960: 45 of cap free
Amount of items: 2
Items: 
Size: 529257 Color: 15
Size: 470699 Color: 5

Bin 1961: 45 of cap free
Amount of items: 2
Items: 
Size: 534471 Color: 7
Size: 465485 Color: 11

Bin 1962: 45 of cap free
Amount of items: 2
Items: 
Size: 535100 Color: 6
Size: 464856 Color: 18

Bin 1963: 45 of cap free
Amount of items: 2
Items: 
Size: 545434 Color: 11
Size: 454522 Color: 17

Bin 1964: 45 of cap free
Amount of items: 2
Items: 
Size: 568307 Color: 15
Size: 431649 Color: 7

Bin 1965: 45 of cap free
Amount of items: 2
Items: 
Size: 573976 Color: 10
Size: 425980 Color: 19

Bin 1966: 45 of cap free
Amount of items: 2
Items: 
Size: 596202 Color: 13
Size: 403754 Color: 17

Bin 1967: 45 of cap free
Amount of items: 2
Items: 
Size: 635556 Color: 18
Size: 364400 Color: 0

Bin 1968: 45 of cap free
Amount of items: 2
Items: 
Size: 650826 Color: 14
Size: 349130 Color: 17

Bin 1969: 45 of cap free
Amount of items: 2
Items: 
Size: 675016 Color: 0
Size: 324940 Color: 4

Bin 1970: 45 of cap free
Amount of items: 2
Items: 
Size: 706583 Color: 6
Size: 293373 Color: 18

Bin 1971: 45 of cap free
Amount of items: 3
Items: 
Size: 728274 Color: 12
Size: 141964 Color: 15
Size: 129718 Color: 8

Bin 1972: 45 of cap free
Amount of items: 2
Items: 
Size: 748748 Color: 18
Size: 251208 Color: 13

Bin 1973: 45 of cap free
Amount of items: 2
Items: 
Size: 789114 Color: 6
Size: 210842 Color: 18

Bin 1974: 46 of cap free
Amount of items: 2
Items: 
Size: 513641 Color: 6
Size: 486314 Color: 17

Bin 1975: 46 of cap free
Amount of items: 2
Items: 
Size: 528333 Color: 1
Size: 471622 Color: 15

Bin 1976: 46 of cap free
Amount of items: 2
Items: 
Size: 567222 Color: 13
Size: 432733 Color: 12

Bin 1977: 46 of cap free
Amount of items: 2
Items: 
Size: 582659 Color: 7
Size: 417296 Color: 1

Bin 1978: 46 of cap free
Amount of items: 2
Items: 
Size: 584168 Color: 9
Size: 415787 Color: 19

Bin 1979: 46 of cap free
Amount of items: 2
Items: 
Size: 584271 Color: 19
Size: 415684 Color: 15

Bin 1980: 46 of cap free
Amount of items: 2
Items: 
Size: 584681 Color: 6
Size: 415274 Color: 0

Bin 1981: 46 of cap free
Amount of items: 2
Items: 
Size: 604898 Color: 10
Size: 395057 Color: 2

Bin 1982: 46 of cap free
Amount of items: 2
Items: 
Size: 626713 Color: 19
Size: 373242 Color: 15

Bin 1983: 46 of cap free
Amount of items: 2
Items: 
Size: 626824 Color: 12
Size: 373131 Color: 18

Bin 1984: 46 of cap free
Amount of items: 2
Items: 
Size: 644251 Color: 1
Size: 355704 Color: 3

Bin 1985: 46 of cap free
Amount of items: 2
Items: 
Size: 648891 Color: 16
Size: 351064 Color: 4

Bin 1986: 46 of cap free
Amount of items: 3
Items: 
Size: 649500 Color: 1
Size: 185505 Color: 2
Size: 164950 Color: 19

Bin 1987: 46 of cap free
Amount of items: 2
Items: 
Size: 670198 Color: 7
Size: 329757 Color: 13

Bin 1988: 46 of cap free
Amount of items: 2
Items: 
Size: 693115 Color: 11
Size: 306840 Color: 9

Bin 1989: 46 of cap free
Amount of items: 2
Items: 
Size: 693445 Color: 1
Size: 306510 Color: 2

Bin 1990: 46 of cap free
Amount of items: 2
Items: 
Size: 702779 Color: 16
Size: 297176 Color: 18

Bin 1991: 46 of cap free
Amount of items: 2
Items: 
Size: 716509 Color: 17
Size: 283446 Color: 5

Bin 1992: 46 of cap free
Amount of items: 3
Items: 
Size: 718181 Color: 18
Size: 161278 Color: 13
Size: 120496 Color: 19

Bin 1993: 46 of cap free
Amount of items: 2
Items: 
Size: 725486 Color: 9
Size: 274469 Color: 16

Bin 1994: 46 of cap free
Amount of items: 2
Items: 
Size: 737734 Color: 4
Size: 262221 Color: 15

Bin 1995: 46 of cap free
Amount of items: 2
Items: 
Size: 742628 Color: 3
Size: 257327 Color: 1

Bin 1996: 46 of cap free
Amount of items: 2
Items: 
Size: 748846 Color: 5
Size: 251109 Color: 1

Bin 1997: 46 of cap free
Amount of items: 2
Items: 
Size: 766766 Color: 16
Size: 233189 Color: 10

Bin 1998: 46 of cap free
Amount of items: 2
Items: 
Size: 781595 Color: 15
Size: 218360 Color: 5

Bin 1999: 46 of cap free
Amount of items: 2
Items: 
Size: 794699 Color: 4
Size: 205256 Color: 5

Bin 2000: 47 of cap free
Amount of items: 2
Items: 
Size: 542015 Color: 9
Size: 457939 Color: 6

Bin 2001: 47 of cap free
Amount of items: 2
Items: 
Size: 562268 Color: 1
Size: 437686 Color: 16

Bin 2002: 47 of cap free
Amount of items: 2
Items: 
Size: 562903 Color: 16
Size: 437051 Color: 12

Bin 2003: 47 of cap free
Amount of items: 2
Items: 
Size: 574790 Color: 15
Size: 425164 Color: 13

Bin 2004: 47 of cap free
Amount of items: 2
Items: 
Size: 581876 Color: 11
Size: 418078 Color: 16

Bin 2005: 47 of cap free
Amount of items: 2
Items: 
Size: 582533 Color: 16
Size: 417421 Color: 11

Bin 2006: 47 of cap free
Amount of items: 2
Items: 
Size: 604105 Color: 19
Size: 395849 Color: 14

Bin 2007: 47 of cap free
Amount of items: 2
Items: 
Size: 612742 Color: 8
Size: 387212 Color: 2

Bin 2008: 47 of cap free
Amount of items: 2
Items: 
Size: 635869 Color: 7
Size: 364085 Color: 9

Bin 2009: 47 of cap free
Amount of items: 2
Items: 
Size: 647181 Color: 12
Size: 352773 Color: 16

Bin 2010: 47 of cap free
Amount of items: 2
Items: 
Size: 659532 Color: 9
Size: 340422 Color: 13

Bin 2011: 47 of cap free
Amount of items: 2
Items: 
Size: 678314 Color: 7
Size: 321640 Color: 15

Bin 2012: 47 of cap free
Amount of items: 2
Items: 
Size: 679996 Color: 9
Size: 319958 Color: 11

Bin 2013: 47 of cap free
Amount of items: 2
Items: 
Size: 686127 Color: 13
Size: 313827 Color: 7

Bin 2014: 47 of cap free
Amount of items: 2
Items: 
Size: 693197 Color: 16
Size: 306757 Color: 17

Bin 2015: 47 of cap free
Amount of items: 2
Items: 
Size: 700817 Color: 14
Size: 299137 Color: 16

Bin 2016: 47 of cap free
Amount of items: 2
Items: 
Size: 729148 Color: 16
Size: 270806 Color: 3

Bin 2017: 47 of cap free
Amount of items: 2
Items: 
Size: 740307 Color: 9
Size: 259647 Color: 19

Bin 2018: 47 of cap free
Amount of items: 2
Items: 
Size: 749684 Color: 14
Size: 250270 Color: 5

Bin 2019: 47 of cap free
Amount of items: 2
Items: 
Size: 750392 Color: 19
Size: 249562 Color: 11

Bin 2020: 47 of cap free
Amount of items: 2
Items: 
Size: 754926 Color: 13
Size: 245028 Color: 17

Bin 2021: 47 of cap free
Amount of items: 2
Items: 
Size: 761313 Color: 15
Size: 238641 Color: 18

Bin 2022: 47 of cap free
Amount of items: 3
Items: 
Size: 770183 Color: 11
Size: 115636 Color: 9
Size: 114135 Color: 6

Bin 2023: 47 of cap free
Amount of items: 2
Items: 
Size: 790523 Color: 1
Size: 209431 Color: 0

Bin 2024: 48 of cap free
Amount of items: 2
Items: 
Size: 519310 Color: 15
Size: 480643 Color: 17

Bin 2025: 48 of cap free
Amount of items: 2
Items: 
Size: 541238 Color: 18
Size: 458715 Color: 2

Bin 2026: 48 of cap free
Amount of items: 2
Items: 
Size: 543812 Color: 11
Size: 456141 Color: 15

Bin 2027: 48 of cap free
Amount of items: 2
Items: 
Size: 546492 Color: 17
Size: 453461 Color: 6

Bin 2028: 48 of cap free
Amount of items: 2
Items: 
Size: 556303 Color: 2
Size: 443650 Color: 11

Bin 2029: 48 of cap free
Amount of items: 2
Items: 
Size: 615839 Color: 0
Size: 384114 Color: 8

Bin 2030: 48 of cap free
Amount of items: 2
Items: 
Size: 617684 Color: 10
Size: 382269 Color: 0

Bin 2031: 48 of cap free
Amount of items: 2
Items: 
Size: 617914 Color: 14
Size: 382039 Color: 15

Bin 2032: 48 of cap free
Amount of items: 2
Items: 
Size: 634241 Color: 4
Size: 365712 Color: 10

Bin 2033: 48 of cap free
Amount of items: 2
Items: 
Size: 635732 Color: 8
Size: 364221 Color: 13

Bin 2034: 48 of cap free
Amount of items: 2
Items: 
Size: 652769 Color: 6
Size: 347184 Color: 18

Bin 2035: 48 of cap free
Amount of items: 2
Items: 
Size: 662507 Color: 13
Size: 337446 Color: 11

Bin 2036: 48 of cap free
Amount of items: 2
Items: 
Size: 664294 Color: 3
Size: 335659 Color: 19

Bin 2037: 48 of cap free
Amount of items: 3
Items: 
Size: 736769 Color: 5
Size: 139345 Color: 8
Size: 123839 Color: 3

Bin 2038: 48 of cap free
Amount of items: 2
Items: 
Size: 745008 Color: 9
Size: 254945 Color: 4

Bin 2039: 48 of cap free
Amount of items: 2
Items: 
Size: 759130 Color: 6
Size: 240823 Color: 9

Bin 2040: 48 of cap free
Amount of items: 2
Items: 
Size: 762240 Color: 1
Size: 237713 Color: 14

Bin 2041: 48 of cap free
Amount of items: 2
Items: 
Size: 765885 Color: 1
Size: 234068 Color: 14

Bin 2042: 48 of cap free
Amount of items: 2
Items: 
Size: 781370 Color: 0
Size: 218583 Color: 14

Bin 2043: 48 of cap free
Amount of items: 2
Items: 
Size: 788415 Color: 14
Size: 211538 Color: 8

Bin 2044: 48 of cap free
Amount of items: 2
Items: 
Size: 797846 Color: 5
Size: 202107 Color: 16

Bin 2045: 48 of cap free
Amount of items: 2
Items: 
Size: 799403 Color: 16
Size: 200550 Color: 12

Bin 2046: 49 of cap free
Amount of items: 2
Items: 
Size: 517148 Color: 6
Size: 482804 Color: 14

Bin 2047: 49 of cap free
Amount of items: 2
Items: 
Size: 521651 Color: 1
Size: 478301 Color: 14

Bin 2048: 49 of cap free
Amount of items: 2
Items: 
Size: 533981 Color: 0
Size: 465971 Color: 13

Bin 2049: 49 of cap free
Amount of items: 2
Items: 
Size: 536905 Color: 6
Size: 463047 Color: 2

Bin 2050: 49 of cap free
Amount of items: 2
Items: 
Size: 548808 Color: 15
Size: 451144 Color: 17

Bin 2051: 49 of cap free
Amount of items: 2
Items: 
Size: 575131 Color: 16
Size: 424821 Color: 17

Bin 2052: 49 of cap free
Amount of items: 2
Items: 
Size: 576270 Color: 4
Size: 423682 Color: 15

Bin 2053: 49 of cap free
Amount of items: 2
Items: 
Size: 598146 Color: 3
Size: 401806 Color: 6

Bin 2054: 49 of cap free
Amount of items: 2
Items: 
Size: 607030 Color: 17
Size: 392922 Color: 7

Bin 2055: 49 of cap free
Amount of items: 2
Items: 
Size: 609104 Color: 8
Size: 390848 Color: 12

Bin 2056: 49 of cap free
Amount of items: 2
Items: 
Size: 612660 Color: 3
Size: 387292 Color: 14

Bin 2057: 49 of cap free
Amount of items: 2
Items: 
Size: 613950 Color: 10
Size: 386002 Color: 19

Bin 2058: 49 of cap free
Amount of items: 2
Items: 
Size: 614424 Color: 2
Size: 385528 Color: 3

Bin 2059: 49 of cap free
Amount of items: 2
Items: 
Size: 620634 Color: 2
Size: 379318 Color: 17

Bin 2060: 49 of cap free
Amount of items: 2
Items: 
Size: 632331 Color: 1
Size: 367621 Color: 19

Bin 2061: 49 of cap free
Amount of items: 2
Items: 
Size: 643882 Color: 7
Size: 356070 Color: 8

Bin 2062: 49 of cap free
Amount of items: 3
Items: 
Size: 646169 Color: 18
Size: 190246 Color: 13
Size: 163537 Color: 9

Bin 2063: 49 of cap free
Amount of items: 2
Items: 
Size: 656793 Color: 18
Size: 343159 Color: 0

Bin 2064: 49 of cap free
Amount of items: 2
Items: 
Size: 666099 Color: 15
Size: 333853 Color: 9

Bin 2065: 49 of cap free
Amount of items: 2
Items: 
Size: 677468 Color: 2
Size: 322484 Color: 7

Bin 2066: 49 of cap free
Amount of items: 2
Items: 
Size: 687859 Color: 11
Size: 312093 Color: 17

Bin 2067: 49 of cap free
Amount of items: 2
Items: 
Size: 697169 Color: 11
Size: 302783 Color: 3

Bin 2068: 49 of cap free
Amount of items: 3
Items: 
Size: 710348 Color: 16
Size: 144893 Color: 7
Size: 144711 Color: 19

Bin 2069: 49 of cap free
Amount of items: 2
Items: 
Size: 753972 Color: 16
Size: 245980 Color: 18

Bin 2070: 49 of cap free
Amount of items: 2
Items: 
Size: 783375 Color: 7
Size: 216577 Color: 19

Bin 2071: 50 of cap free
Amount of items: 2
Items: 
Size: 504801 Color: 17
Size: 495150 Color: 12

Bin 2072: 50 of cap free
Amount of items: 2
Items: 
Size: 506641 Color: 0
Size: 493310 Color: 8

Bin 2073: 50 of cap free
Amount of items: 2
Items: 
Size: 503377 Color: 2
Size: 496574 Color: 1

Bin 2074: 50 of cap free
Amount of items: 2
Items: 
Size: 511322 Color: 18
Size: 488629 Color: 16

Bin 2075: 50 of cap free
Amount of items: 2
Items: 
Size: 533914 Color: 8
Size: 466037 Color: 2

Bin 2076: 50 of cap free
Amount of items: 2
Items: 
Size: 544367 Color: 19
Size: 455584 Color: 2

Bin 2077: 50 of cap free
Amount of items: 2
Items: 
Size: 557546 Color: 4
Size: 442405 Color: 9

Bin 2078: 50 of cap free
Amount of items: 2
Items: 
Size: 615663 Color: 0
Size: 384288 Color: 18

Bin 2079: 50 of cap free
Amount of items: 2
Items: 
Size: 669823 Color: 17
Size: 330128 Color: 12

Bin 2080: 50 of cap free
Amount of items: 2
Items: 
Size: 713359 Color: 13
Size: 286592 Color: 10

Bin 2081: 50 of cap free
Amount of items: 2
Items: 
Size: 735939 Color: 5
Size: 264012 Color: 9

Bin 2082: 50 of cap free
Amount of items: 2
Items: 
Size: 750565 Color: 19
Size: 249386 Color: 9

Bin 2083: 50 of cap free
Amount of items: 2
Items: 
Size: 772643 Color: 6
Size: 227308 Color: 17

Bin 2084: 50 of cap free
Amount of items: 3
Items: 
Size: 791949 Color: 6
Size: 104049 Color: 10
Size: 103953 Color: 10

Bin 2085: 50 of cap free
Amount of items: 2
Items: 
Size: 797398 Color: 8
Size: 202553 Color: 7

Bin 2086: 51 of cap free
Amount of items: 2
Items: 
Size: 543663 Color: 7
Size: 456287 Color: 13

Bin 2087: 51 of cap free
Amount of items: 2
Items: 
Size: 549937 Color: 3
Size: 450013 Color: 16

Bin 2088: 51 of cap free
Amount of items: 2
Items: 
Size: 559116 Color: 3
Size: 440834 Color: 10

Bin 2089: 51 of cap free
Amount of items: 2
Items: 
Size: 562456 Color: 6
Size: 437494 Color: 0

Bin 2090: 51 of cap free
Amount of items: 2
Items: 
Size: 569615 Color: 5
Size: 430335 Color: 11

Bin 2091: 51 of cap free
Amount of items: 2
Items: 
Size: 592759 Color: 15
Size: 407191 Color: 10

Bin 2092: 51 of cap free
Amount of items: 2
Items: 
Size: 593476 Color: 15
Size: 406474 Color: 1

Bin 2093: 51 of cap free
Amount of items: 2
Items: 
Size: 614368 Color: 13
Size: 385582 Color: 18

Bin 2094: 51 of cap free
Amount of items: 2
Items: 
Size: 621148 Color: 19
Size: 378802 Color: 13

Bin 2095: 51 of cap free
Amount of items: 3
Items: 
Size: 628639 Color: 15
Size: 211876 Color: 14
Size: 159435 Color: 6

Bin 2096: 51 of cap free
Amount of items: 2
Items: 
Size: 691084 Color: 19
Size: 308866 Color: 3

Bin 2097: 51 of cap free
Amount of items: 2
Items: 
Size: 758697 Color: 11
Size: 241253 Color: 3

Bin 2098: 51 of cap free
Amount of items: 2
Items: 
Size: 763476 Color: 13
Size: 236474 Color: 12

Bin 2099: 51 of cap free
Amount of items: 2
Items: 
Size: 780108 Color: 7
Size: 219842 Color: 14

Bin 2100: 51 of cap free
Amount of items: 2
Items: 
Size: 787835 Color: 16
Size: 212115 Color: 0

Bin 2101: 52 of cap free
Amount of items: 2
Items: 
Size: 505492 Color: 4
Size: 494457 Color: 16

Bin 2102: 52 of cap free
Amount of items: 2
Items: 
Size: 533086 Color: 4
Size: 466863 Color: 18

Bin 2103: 52 of cap free
Amount of items: 2
Items: 
Size: 548218 Color: 10
Size: 451731 Color: 0

Bin 2104: 52 of cap free
Amount of items: 3
Items: 
Size: 576084 Color: 17
Size: 227193 Color: 17
Size: 196672 Color: 10

Bin 2105: 52 of cap free
Amount of items: 2
Items: 
Size: 576950 Color: 4
Size: 422999 Color: 14

Bin 2106: 52 of cap free
Amount of items: 2
Items: 
Size: 588963 Color: 15
Size: 410986 Color: 19

Bin 2107: 52 of cap free
Amount of items: 2
Items: 
Size: 595631 Color: 5
Size: 404318 Color: 1

Bin 2108: 52 of cap free
Amount of items: 3
Items: 
Size: 608833 Color: 2
Size: 196944 Color: 16
Size: 194172 Color: 4

Bin 2109: 52 of cap free
Amount of items: 2
Items: 
Size: 634241 Color: 4
Size: 365708 Color: 5

Bin 2110: 52 of cap free
Amount of items: 2
Items: 
Size: 675764 Color: 16
Size: 324185 Color: 1

Bin 2111: 52 of cap free
Amount of items: 2
Items: 
Size: 679063 Color: 0
Size: 320886 Color: 9

Bin 2112: 52 of cap free
Amount of items: 2
Items: 
Size: 683468 Color: 15
Size: 316481 Color: 17

Bin 2113: 52 of cap free
Amount of items: 2
Items: 
Size: 684695 Color: 4
Size: 315254 Color: 8

Bin 2114: 52 of cap free
Amount of items: 2
Items: 
Size: 686194 Color: 17
Size: 313755 Color: 8

Bin 2115: 52 of cap free
Amount of items: 2
Items: 
Size: 701552 Color: 2
Size: 298397 Color: 14

Bin 2116: 52 of cap free
Amount of items: 2
Items: 
Size: 703613 Color: 19
Size: 296336 Color: 3

Bin 2117: 52 of cap free
Amount of items: 2
Items: 
Size: 751403 Color: 11
Size: 248546 Color: 4

Bin 2118: 52 of cap free
Amount of items: 2
Items: 
Size: 795443 Color: 15
Size: 204506 Color: 10

Bin 2119: 52 of cap free
Amount of items: 3
Items: 
Size: 796876 Color: 18
Size: 101614 Color: 16
Size: 101459 Color: 8

Bin 2120: 52 of cap free
Amount of items: 2
Items: 
Size: 799492 Color: 19
Size: 200457 Color: 0

Bin 2121: 53 of cap free
Amount of items: 2
Items: 
Size: 501713 Color: 19
Size: 498235 Color: 9

Bin 2122: 53 of cap free
Amount of items: 2
Items: 
Size: 547203 Color: 13
Size: 452745 Color: 19

Bin 2123: 53 of cap free
Amount of items: 2
Items: 
Size: 564069 Color: 19
Size: 435879 Color: 11

Bin 2124: 53 of cap free
Amount of items: 2
Items: 
Size: 579280 Color: 13
Size: 420668 Color: 2

Bin 2125: 53 of cap free
Amount of items: 2
Items: 
Size: 590371 Color: 7
Size: 409577 Color: 15

Bin 2126: 53 of cap free
Amount of items: 2
Items: 
Size: 595108 Color: 2
Size: 404840 Color: 15

Bin 2127: 53 of cap free
Amount of items: 2
Items: 
Size: 680486 Color: 15
Size: 319462 Color: 18

Bin 2128: 53 of cap free
Amount of items: 2
Items: 
Size: 703217 Color: 8
Size: 296731 Color: 17

Bin 2129: 53 of cap free
Amount of items: 2
Items: 
Size: 716638 Color: 14
Size: 283310 Color: 7

Bin 2130: 53 of cap free
Amount of items: 2
Items: 
Size: 725589 Color: 2
Size: 274359 Color: 17

Bin 2131: 53 of cap free
Amount of items: 2
Items: 
Size: 757637 Color: 2
Size: 242311 Color: 7

Bin 2132: 53 of cap free
Amount of items: 2
Items: 
Size: 790684 Color: 11
Size: 209264 Color: 16

Bin 2133: 54 of cap free
Amount of items: 2
Items: 
Size: 507026 Color: 11
Size: 492921 Color: 9

Bin 2134: 54 of cap free
Amount of items: 2
Items: 
Size: 517964 Color: 2
Size: 481983 Color: 4

Bin 2135: 54 of cap free
Amount of items: 2
Items: 
Size: 582112 Color: 13
Size: 417835 Color: 6

Bin 2136: 54 of cap free
Amount of items: 2
Items: 
Size: 584956 Color: 19
Size: 414991 Color: 13

Bin 2137: 54 of cap free
Amount of items: 2
Items: 
Size: 589907 Color: 5
Size: 410040 Color: 2

Bin 2138: 54 of cap free
Amount of items: 2
Items: 
Size: 610349 Color: 15
Size: 389598 Color: 12

Bin 2139: 54 of cap free
Amount of items: 2
Items: 
Size: 668095 Color: 8
Size: 331852 Color: 3

Bin 2140: 54 of cap free
Amount of items: 2
Items: 
Size: 673172 Color: 13
Size: 326775 Color: 14

Bin 2141: 54 of cap free
Amount of items: 2
Items: 
Size: 697354 Color: 4
Size: 302593 Color: 9

Bin 2142: 54 of cap free
Amount of items: 3
Items: 
Size: 715243 Color: 7
Size: 148875 Color: 11
Size: 135829 Color: 16

Bin 2143: 54 of cap free
Amount of items: 2
Items: 
Size: 717029 Color: 3
Size: 282918 Color: 16

Bin 2144: 54 of cap free
Amount of items: 2
Items: 
Size: 741933 Color: 12
Size: 258014 Color: 15

Bin 2145: 54 of cap free
Amount of items: 2
Items: 
Size: 743199 Color: 11
Size: 256748 Color: 17

Bin 2146: 54 of cap free
Amount of items: 2
Items: 
Size: 754724 Color: 4
Size: 245223 Color: 5

Bin 2147: 54 of cap free
Amount of items: 2
Items: 
Size: 784631 Color: 10
Size: 215316 Color: 15

Bin 2148: 55 of cap free
Amount of items: 2
Items: 
Size: 518370 Color: 19
Size: 481576 Color: 4

Bin 2149: 55 of cap free
Amount of items: 2
Items: 
Size: 529373 Color: 15
Size: 470573 Color: 14

Bin 2150: 55 of cap free
Amount of items: 3
Items: 
Size: 620759 Color: 2
Size: 198781 Color: 17
Size: 180406 Color: 18

Bin 2151: 55 of cap free
Amount of items: 2
Items: 
Size: 622936 Color: 14
Size: 377010 Color: 13

Bin 2152: 55 of cap free
Amount of items: 2
Items: 
Size: 627419 Color: 4
Size: 372527 Color: 13

Bin 2153: 55 of cap free
Amount of items: 2
Items: 
Size: 655905 Color: 19
Size: 344041 Color: 13

Bin 2154: 55 of cap free
Amount of items: 2
Items: 
Size: 660437 Color: 4
Size: 339509 Color: 12

Bin 2155: 55 of cap free
Amount of items: 2
Items: 
Size: 694064 Color: 5
Size: 305882 Color: 14

Bin 2156: 55 of cap free
Amount of items: 2
Items: 
Size: 712881 Color: 13
Size: 287065 Color: 6

Bin 2157: 55 of cap free
Amount of items: 2
Items: 
Size: 732351 Color: 5
Size: 267595 Color: 6

Bin 2158: 55 of cap free
Amount of items: 2
Items: 
Size: 779918 Color: 19
Size: 220028 Color: 2

Bin 2159: 56 of cap free
Amount of items: 3
Items: 
Size: 500250 Color: 9
Size: 250613 Color: 18
Size: 249082 Color: 6

Bin 2160: 56 of cap free
Amount of items: 2
Items: 
Size: 505060 Color: 9
Size: 494885 Color: 7

Bin 2161: 56 of cap free
Amount of items: 2
Items: 
Size: 537970 Color: 8
Size: 461975 Color: 0

Bin 2162: 56 of cap free
Amount of items: 2
Items: 
Size: 547456 Color: 1
Size: 452489 Color: 2

Bin 2163: 56 of cap free
Amount of items: 2
Items: 
Size: 575497 Color: 18
Size: 424448 Color: 9

Bin 2164: 56 of cap free
Amount of items: 2
Items: 
Size: 591660 Color: 0
Size: 408285 Color: 18

Bin 2165: 56 of cap free
Amount of items: 2
Items: 
Size: 606144 Color: 19
Size: 393801 Color: 4

Bin 2166: 56 of cap free
Amount of items: 2
Items: 
Size: 639188 Color: 14
Size: 360757 Color: 8

Bin 2167: 56 of cap free
Amount of items: 2
Items: 
Size: 648488 Color: 9
Size: 351457 Color: 19

Bin 2168: 56 of cap free
Amount of items: 2
Items: 
Size: 668282 Color: 10
Size: 331663 Color: 19

Bin 2169: 56 of cap free
Amount of items: 2
Items: 
Size: 674966 Color: 8
Size: 324979 Color: 0

Bin 2170: 56 of cap free
Amount of items: 2
Items: 
Size: 683624 Color: 2
Size: 316321 Color: 18

Bin 2171: 56 of cap free
Amount of items: 2
Items: 
Size: 684270 Color: 3
Size: 315675 Color: 9

Bin 2172: 56 of cap free
Amount of items: 2
Items: 
Size: 708108 Color: 15
Size: 291837 Color: 19

Bin 2173: 56 of cap free
Amount of items: 2
Items: 
Size: 719050 Color: 6
Size: 280895 Color: 18

Bin 2174: 56 of cap free
Amount of items: 3
Items: 
Size: 770424 Color: 6
Size: 115226 Color: 4
Size: 114295 Color: 9

Bin 2175: 56 of cap free
Amount of items: 2
Items: 
Size: 789615 Color: 8
Size: 210330 Color: 16

Bin 2176: 56 of cap free
Amount of items: 3
Items: 
Size: 791243 Color: 9
Size: 104395 Color: 18
Size: 104307 Color: 15

Bin 2177: 57 of cap free
Amount of items: 2
Items: 
Size: 502670 Color: 9
Size: 497274 Color: 16

Bin 2178: 57 of cap free
Amount of items: 2
Items: 
Size: 516264 Color: 6
Size: 483680 Color: 13

Bin 2179: 57 of cap free
Amount of items: 2
Items: 
Size: 537332 Color: 10
Size: 462612 Color: 15

Bin 2180: 57 of cap free
Amount of items: 2
Items: 
Size: 549364 Color: 9
Size: 450580 Color: 5

Bin 2181: 57 of cap free
Amount of items: 2
Items: 
Size: 554735 Color: 11
Size: 445209 Color: 16

Bin 2182: 57 of cap free
Amount of items: 2
Items: 
Size: 560554 Color: 15
Size: 439390 Color: 3

Bin 2183: 57 of cap free
Amount of items: 2
Items: 
Size: 571783 Color: 7
Size: 428161 Color: 11

Bin 2184: 57 of cap free
Amount of items: 2
Items: 
Size: 585568 Color: 10
Size: 414376 Color: 4

Bin 2185: 57 of cap free
Amount of items: 2
Items: 
Size: 610607 Color: 5
Size: 389337 Color: 7

Bin 2186: 57 of cap free
Amount of items: 2
Items: 
Size: 613205 Color: 12
Size: 386739 Color: 5

Bin 2187: 57 of cap free
Amount of items: 2
Items: 
Size: 646119 Color: 9
Size: 353825 Color: 14

Bin 2188: 57 of cap free
Amount of items: 2
Items: 
Size: 662839 Color: 8
Size: 337105 Color: 1

Bin 2189: 57 of cap free
Amount of items: 2
Items: 
Size: 667910 Color: 19
Size: 332034 Color: 11

Bin 2190: 57 of cap free
Amount of items: 2
Items: 
Size: 684866 Color: 8
Size: 315078 Color: 12

Bin 2191: 57 of cap free
Amount of items: 2
Items: 
Size: 687615 Color: 14
Size: 312329 Color: 16

Bin 2192: 57 of cap free
Amount of items: 2
Items: 
Size: 786258 Color: 12
Size: 213686 Color: 16

Bin 2193: 57 of cap free
Amount of items: 2
Items: 
Size: 795535 Color: 16
Size: 204409 Color: 18

Bin 2194: 58 of cap free
Amount of items: 2
Items: 
Size: 500642 Color: 9
Size: 499301 Color: 15

Bin 2195: 58 of cap free
Amount of items: 2
Items: 
Size: 538303 Color: 2
Size: 461640 Color: 16

Bin 2196: 58 of cap free
Amount of items: 2
Items: 
Size: 543655 Color: 16
Size: 456288 Color: 7

Bin 2197: 58 of cap free
Amount of items: 2
Items: 
Size: 574864 Color: 3
Size: 425079 Color: 1

Bin 2198: 58 of cap free
Amount of items: 2
Items: 
Size: 588862 Color: 9
Size: 411081 Color: 12

Bin 2199: 58 of cap free
Amount of items: 2
Items: 
Size: 629008 Color: 15
Size: 370935 Color: 13

Bin 2200: 58 of cap free
Amount of items: 2
Items: 
Size: 630235 Color: 19
Size: 369708 Color: 11

Bin 2201: 58 of cap free
Amount of items: 2
Items: 
Size: 632397 Color: 10
Size: 367546 Color: 1

Bin 2202: 58 of cap free
Amount of items: 2
Items: 
Size: 676342 Color: 19
Size: 323601 Color: 3

Bin 2203: 58 of cap free
Amount of items: 2
Items: 
Size: 709633 Color: 6
Size: 290310 Color: 17

Bin 2204: 58 of cap free
Amount of items: 2
Items: 
Size: 717232 Color: 5
Size: 282711 Color: 3

Bin 2205: 58 of cap free
Amount of items: 2
Items: 
Size: 787361 Color: 3
Size: 212582 Color: 19

Bin 2206: 58 of cap free
Amount of items: 3
Items: 
Size: 798820 Color: 8
Size: 100679 Color: 4
Size: 100444 Color: 11

Bin 2207: 58 of cap free
Amount of items: 2
Items: 
Size: 799149 Color: 2
Size: 200794 Color: 0

Bin 2208: 59 of cap free
Amount of items: 2
Items: 
Size: 532600 Color: 14
Size: 467342 Color: 18

Bin 2209: 59 of cap free
Amount of items: 2
Items: 
Size: 544459 Color: 8
Size: 455483 Color: 2

Bin 2210: 59 of cap free
Amount of items: 2
Items: 
Size: 566911 Color: 5
Size: 433031 Color: 18

Bin 2211: 59 of cap free
Amount of items: 3
Items: 
Size: 581857 Color: 14
Size: 223089 Color: 1
Size: 194996 Color: 11

Bin 2212: 59 of cap free
Amount of items: 2
Items: 
Size: 584838 Color: 1
Size: 415104 Color: 4

Bin 2213: 59 of cap free
Amount of items: 2
Items: 
Size: 585332 Color: 4
Size: 414610 Color: 5

Bin 2214: 59 of cap free
Amount of items: 2
Items: 
Size: 600348 Color: 7
Size: 399594 Color: 18

Bin 2215: 59 of cap free
Amount of items: 3
Items: 
Size: 600908 Color: 10
Size: 201559 Color: 9
Size: 197475 Color: 7

Bin 2216: 59 of cap free
Amount of items: 2
Items: 
Size: 615305 Color: 0
Size: 384637 Color: 4

Bin 2217: 59 of cap free
Amount of items: 2
Items: 
Size: 627588 Color: 9
Size: 372354 Color: 13

Bin 2218: 59 of cap free
Amount of items: 2
Items: 
Size: 629144 Color: 15
Size: 370798 Color: 9

Bin 2219: 59 of cap free
Amount of items: 2
Items: 
Size: 639477 Color: 15
Size: 360465 Color: 8

Bin 2220: 59 of cap free
Amount of items: 2
Items: 
Size: 658194 Color: 14
Size: 341748 Color: 0

Bin 2221: 59 of cap free
Amount of items: 2
Items: 
Size: 669314 Color: 2
Size: 330628 Color: 9

Bin 2222: 59 of cap free
Amount of items: 2
Items: 
Size: 674241 Color: 10
Size: 325701 Color: 5

Bin 2223: 59 of cap free
Amount of items: 2
Items: 
Size: 680708 Color: 16
Size: 319234 Color: 1

Bin 2224: 59 of cap free
Amount of items: 2
Items: 
Size: 708980 Color: 11
Size: 290962 Color: 6

Bin 2225: 59 of cap free
Amount of items: 3
Items: 
Size: 717878 Color: 10
Size: 146680 Color: 16
Size: 135384 Color: 19

Bin 2226: 59 of cap free
Amount of items: 2
Items: 
Size: 749990 Color: 18
Size: 249952 Color: 2

Bin 2227: 59 of cap free
Amount of items: 2
Items: 
Size: 752635 Color: 12
Size: 247307 Color: 1

Bin 2228: 59 of cap free
Amount of items: 2
Items: 
Size: 753467 Color: 16
Size: 246475 Color: 1

Bin 2229: 59 of cap free
Amount of items: 2
Items: 
Size: 786004 Color: 18
Size: 213938 Color: 12

Bin 2230: 59 of cap free
Amount of items: 3
Items: 
Size: 789174 Color: 18
Size: 105511 Color: 2
Size: 105257 Color: 10

Bin 2231: 59 of cap free
Amount of items: 2
Items: 
Size: 796702 Color: 7
Size: 203240 Color: 14

Bin 2232: 60 of cap free
Amount of items: 3
Items: 
Size: 422395 Color: 17
Size: 322845 Color: 5
Size: 254701 Color: 10

Bin 2233: 60 of cap free
Amount of items: 2
Items: 
Size: 500306 Color: 7
Size: 499635 Color: 3

Bin 2234: 60 of cap free
Amount of items: 2
Items: 
Size: 522182 Color: 6
Size: 477759 Color: 17

Bin 2235: 60 of cap free
Amount of items: 2
Items: 
Size: 556920 Color: 3
Size: 443021 Color: 18

Bin 2236: 60 of cap free
Amount of items: 2
Items: 
Size: 557228 Color: 3
Size: 442713 Color: 10

Bin 2237: 60 of cap free
Amount of items: 2
Items: 
Size: 578644 Color: 6
Size: 421297 Color: 5

Bin 2238: 60 of cap free
Amount of items: 2
Items: 
Size: 582184 Color: 5
Size: 417757 Color: 3

Bin 2239: 60 of cap free
Amount of items: 2
Items: 
Size: 589819 Color: 15
Size: 410122 Color: 13

Bin 2240: 60 of cap free
Amount of items: 2
Items: 
Size: 602620 Color: 10
Size: 397321 Color: 4

Bin 2241: 60 of cap free
Amount of items: 2
Items: 
Size: 688351 Color: 12
Size: 311590 Color: 19

Bin 2242: 60 of cap free
Amount of items: 2
Items: 
Size: 724779 Color: 19
Size: 275162 Color: 3

Bin 2243: 60 of cap free
Amount of items: 2
Items: 
Size: 728620 Color: 16
Size: 271321 Color: 18

Bin 2244: 60 of cap free
Amount of items: 2
Items: 
Size: 759823 Color: 8
Size: 240118 Color: 6

Bin 2245: 60 of cap free
Amount of items: 2
Items: 
Size: 791658 Color: 4
Size: 208283 Color: 6

Bin 2246: 60 of cap free
Amount of items: 2
Items: 
Size: 794025 Color: 9
Size: 205916 Color: 11

Bin 2247: 61 of cap free
Amount of items: 2
Items: 
Size: 512280 Color: 5
Size: 487660 Color: 12

Bin 2248: 61 of cap free
Amount of items: 2
Items: 
Size: 531797 Color: 13
Size: 468143 Color: 16

Bin 2249: 61 of cap free
Amount of items: 2
Items: 
Size: 545518 Color: 3
Size: 454422 Color: 6

Bin 2250: 61 of cap free
Amount of items: 2
Items: 
Size: 553050 Color: 19
Size: 446890 Color: 7

Bin 2251: 61 of cap free
Amount of items: 2
Items: 
Size: 605477 Color: 15
Size: 394463 Color: 10

Bin 2252: 61 of cap free
Amount of items: 3
Items: 
Size: 618501 Color: 2
Size: 191440 Color: 19
Size: 189999 Color: 1

Bin 2253: 61 of cap free
Amount of items: 2
Items: 
Size: 643702 Color: 7
Size: 356238 Color: 4

Bin 2254: 61 of cap free
Amount of items: 2
Items: 
Size: 752797 Color: 13
Size: 247143 Color: 10

Bin 2255: 61 of cap free
Amount of items: 2
Items: 
Size: 758629 Color: 14
Size: 241311 Color: 4

Bin 2256: 61 of cap free
Amount of items: 2
Items: 
Size: 759447 Color: 15
Size: 240493 Color: 4

Bin 2257: 61 of cap free
Amount of items: 2
Items: 
Size: 774993 Color: 14
Size: 224947 Color: 4

Bin 2258: 61 of cap free
Amount of items: 2
Items: 
Size: 778104 Color: 11
Size: 221836 Color: 2

Bin 2259: 62 of cap free
Amount of items: 2
Items: 
Size: 509280 Color: 6
Size: 490659 Color: 18

Bin 2260: 62 of cap free
Amount of items: 2
Items: 
Size: 520778 Color: 13
Size: 479161 Color: 9

Bin 2261: 62 of cap free
Amount of items: 2
Items: 
Size: 535434 Color: 9
Size: 464505 Color: 8

Bin 2262: 62 of cap free
Amount of items: 2
Items: 
Size: 536341 Color: 19
Size: 463598 Color: 0

Bin 2263: 62 of cap free
Amount of items: 2
Items: 
Size: 540415 Color: 18
Size: 459524 Color: 16

Bin 2264: 62 of cap free
Amount of items: 2
Items: 
Size: 548374 Color: 2
Size: 451565 Color: 18

Bin 2265: 62 of cap free
Amount of items: 2
Items: 
Size: 561789 Color: 18
Size: 438150 Color: 14

Bin 2266: 62 of cap free
Amount of items: 2
Items: 
Size: 571427 Color: 17
Size: 428512 Color: 9

Bin 2267: 62 of cap free
Amount of items: 2
Items: 
Size: 595703 Color: 6
Size: 404236 Color: 0

Bin 2268: 62 of cap free
Amount of items: 2
Items: 
Size: 607130 Color: 4
Size: 392809 Color: 0

Bin 2269: 62 of cap free
Amount of items: 2
Items: 
Size: 679659 Color: 16
Size: 320280 Color: 3

Bin 2270: 62 of cap free
Amount of items: 2
Items: 
Size: 691385 Color: 19
Size: 308554 Color: 0

Bin 2271: 62 of cap free
Amount of items: 2
Items: 
Size: 692688 Color: 8
Size: 307251 Color: 11

Bin 2272: 62 of cap free
Amount of items: 2
Items: 
Size: 703902 Color: 13
Size: 296037 Color: 15

Bin 2273: 62 of cap free
Amount of items: 2
Items: 
Size: 719846 Color: 17
Size: 280093 Color: 3

Bin 2274: 62 of cap free
Amount of items: 2
Items: 
Size: 738785 Color: 3
Size: 261154 Color: 17

Bin 2275: 62 of cap free
Amount of items: 2
Items: 
Size: 778559 Color: 17
Size: 221380 Color: 7

Bin 2276: 62 of cap free
Amount of items: 2
Items: 
Size: 793468 Color: 7
Size: 206471 Color: 6

Bin 2277: 63 of cap free
Amount of items: 2
Items: 
Size: 532363 Color: 9
Size: 467575 Color: 12

Bin 2278: 63 of cap free
Amount of items: 2
Items: 
Size: 550715 Color: 7
Size: 449223 Color: 9

Bin 2279: 63 of cap free
Amount of items: 2
Items: 
Size: 588286 Color: 17
Size: 411652 Color: 7

Bin 2280: 63 of cap free
Amount of items: 2
Items: 
Size: 592882 Color: 14
Size: 407056 Color: 15

Bin 2281: 63 of cap free
Amount of items: 2
Items: 
Size: 606879 Color: 10
Size: 393059 Color: 19

Bin 2282: 63 of cap free
Amount of items: 2
Items: 
Size: 609394 Color: 6
Size: 390544 Color: 13

Bin 2283: 63 of cap free
Amount of items: 2
Items: 
Size: 646100 Color: 16
Size: 353838 Color: 9

Bin 2284: 63 of cap free
Amount of items: 2
Items: 
Size: 671458 Color: 17
Size: 328480 Color: 9

Bin 2285: 63 of cap free
Amount of items: 2
Items: 
Size: 684625 Color: 8
Size: 315313 Color: 4

Bin 2286: 63 of cap free
Amount of items: 2
Items: 
Size: 697628 Color: 15
Size: 302310 Color: 9

Bin 2287: 63 of cap free
Amount of items: 2
Items: 
Size: 743949 Color: 18
Size: 255989 Color: 11

Bin 2288: 63 of cap free
Amount of items: 2
Items: 
Size: 759981 Color: 2
Size: 239957 Color: 11

Bin 2289: 63 of cap free
Amount of items: 3
Items: 
Size: 773227 Color: 7
Size: 116341 Color: 8
Size: 110370 Color: 12

Bin 2290: 63 of cap free
Amount of items: 2
Items: 
Size: 779229 Color: 1
Size: 220709 Color: 5

Bin 2291: 63 of cap free
Amount of items: 2
Items: 
Size: 796333 Color: 10
Size: 203605 Color: 15

Bin 2292: 64 of cap free
Amount of items: 2
Items: 
Size: 503411 Color: 8
Size: 496526 Color: 19

Bin 2293: 64 of cap free
Amount of items: 2
Items: 
Size: 551611 Color: 18
Size: 448326 Color: 13

Bin 2294: 64 of cap free
Amount of items: 2
Items: 
Size: 552397 Color: 13
Size: 447540 Color: 10

Bin 2295: 64 of cap free
Amount of items: 2
Items: 
Size: 564873 Color: 7
Size: 435064 Color: 2

Bin 2296: 64 of cap free
Amount of items: 2
Items: 
Size: 597132 Color: 18
Size: 402805 Color: 3

Bin 2297: 64 of cap free
Amount of items: 2
Items: 
Size: 683035 Color: 19
Size: 316902 Color: 15

Bin 2298: 64 of cap free
Amount of items: 2
Items: 
Size: 683158 Color: 8
Size: 316779 Color: 0

Bin 2299: 64 of cap free
Amount of items: 3
Items: 
Size: 699701 Color: 17
Size: 150305 Color: 12
Size: 149931 Color: 12

Bin 2300: 64 of cap free
Amount of items: 2
Items: 
Size: 711186 Color: 7
Size: 288751 Color: 6

Bin 2301: 64 of cap free
Amount of items: 2
Items: 
Size: 768354 Color: 2
Size: 231583 Color: 18

Bin 2302: 65 of cap free
Amount of items: 2
Items: 
Size: 512630 Color: 1
Size: 487306 Color: 10

Bin 2303: 65 of cap free
Amount of items: 2
Items: 
Size: 524739 Color: 3
Size: 475197 Color: 13

Bin 2304: 65 of cap free
Amount of items: 2
Items: 
Size: 527933 Color: 5
Size: 472003 Color: 11

Bin 2305: 65 of cap free
Amount of items: 2
Items: 
Size: 536744 Color: 3
Size: 463192 Color: 2

Bin 2306: 65 of cap free
Amount of items: 2
Items: 
Size: 569447 Color: 6
Size: 430489 Color: 5

Bin 2307: 65 of cap free
Amount of items: 2
Items: 
Size: 580282 Color: 1
Size: 419654 Color: 0

Bin 2308: 65 of cap free
Amount of items: 2
Items: 
Size: 587059 Color: 3
Size: 412877 Color: 6

Bin 2309: 65 of cap free
Amount of items: 2
Items: 
Size: 604746 Color: 8
Size: 395190 Color: 19

Bin 2310: 65 of cap free
Amount of items: 2
Items: 
Size: 606590 Color: 7
Size: 393346 Color: 5

Bin 2311: 65 of cap free
Amount of items: 2
Items: 
Size: 637100 Color: 13
Size: 362836 Color: 10

Bin 2312: 65 of cap free
Amount of items: 2
Items: 
Size: 662203 Color: 3
Size: 337733 Color: 13

Bin 2313: 65 of cap free
Amount of items: 2
Items: 
Size: 662372 Color: 19
Size: 337564 Color: 5

Bin 2314: 65 of cap free
Amount of items: 2
Items: 
Size: 670287 Color: 15
Size: 329649 Color: 1

Bin 2315: 65 of cap free
Amount of items: 2
Items: 
Size: 689609 Color: 1
Size: 310327 Color: 2

Bin 2316: 65 of cap free
Amount of items: 2
Items: 
Size: 706498 Color: 19
Size: 293438 Color: 11

Bin 2317: 65 of cap free
Amount of items: 2
Items: 
Size: 706848 Color: 9
Size: 293088 Color: 13

Bin 2318: 65 of cap free
Amount of items: 2
Items: 
Size: 740059 Color: 19
Size: 259877 Color: 4

Bin 2319: 65 of cap free
Amount of items: 2
Items: 
Size: 769763 Color: 0
Size: 230173 Color: 5

Bin 2320: 65 of cap free
Amount of items: 3
Items: 
Size: 782091 Color: 14
Size: 111446 Color: 13
Size: 106399 Color: 9

Bin 2321: 65 of cap free
Amount of items: 2
Items: 
Size: 787193 Color: 9
Size: 212743 Color: 18

Bin 2322: 65 of cap free
Amount of items: 2
Items: 
Size: 792046 Color: 3
Size: 207890 Color: 8

Bin 2323: 66 of cap free
Amount of items: 2
Items: 
Size: 505882 Color: 3
Size: 494053 Color: 18

Bin 2324: 66 of cap free
Amount of items: 2
Items: 
Size: 521862 Color: 13
Size: 478073 Color: 2

Bin 2325: 66 of cap free
Amount of items: 2
Items: 
Size: 561785 Color: 4
Size: 438150 Color: 12

Bin 2326: 66 of cap free
Amount of items: 2
Items: 
Size: 570414 Color: 18
Size: 429521 Color: 1

Bin 2327: 66 of cap free
Amount of items: 2
Items: 
Size: 591478 Color: 10
Size: 408457 Color: 2

Bin 2328: 66 of cap free
Amount of items: 2
Items: 
Size: 607785 Color: 11
Size: 392150 Color: 8

Bin 2329: 66 of cap free
Amount of items: 2
Items: 
Size: 615009 Color: 12
Size: 384926 Color: 10

Bin 2330: 66 of cap free
Amount of items: 2
Items: 
Size: 618829 Color: 15
Size: 381106 Color: 4

Bin 2331: 66 of cap free
Amount of items: 2
Items: 
Size: 647678 Color: 14
Size: 352257 Color: 19

Bin 2332: 66 of cap free
Amount of items: 2
Items: 
Size: 673742 Color: 12
Size: 326193 Color: 16

Bin 2333: 66 of cap free
Amount of items: 2
Items: 
Size: 696068 Color: 14
Size: 303867 Color: 12

Bin 2334: 66 of cap free
Amount of items: 2
Items: 
Size: 730621 Color: 2
Size: 269314 Color: 19

Bin 2335: 66 of cap free
Amount of items: 2
Items: 
Size: 744723 Color: 11
Size: 255212 Color: 19

Bin 2336: 66 of cap free
Amount of items: 2
Items: 
Size: 794161 Color: 2
Size: 205774 Color: 14

Bin 2337: 66 of cap free
Amount of items: 2
Items: 
Size: 794328 Color: 5
Size: 205607 Color: 17

Bin 2338: 67 of cap free
Amount of items: 2
Items: 
Size: 564203 Color: 14
Size: 435731 Color: 13

Bin 2339: 67 of cap free
Amount of items: 2
Items: 
Size: 564872 Color: 13
Size: 435062 Color: 12

Bin 2340: 67 of cap free
Amount of items: 2
Items: 
Size: 612563 Color: 4
Size: 387371 Color: 8

Bin 2341: 67 of cap free
Amount of items: 2
Items: 
Size: 613797 Color: 13
Size: 386137 Color: 14

Bin 2342: 67 of cap free
Amount of items: 2
Items: 
Size: 619364 Color: 16
Size: 380570 Color: 19

Bin 2343: 67 of cap free
Amount of items: 2
Items: 
Size: 653600 Color: 12
Size: 346334 Color: 17

Bin 2344: 67 of cap free
Amount of items: 2
Items: 
Size: 668404 Color: 16
Size: 331530 Color: 19

Bin 2345: 67 of cap free
Amount of items: 2
Items: 
Size: 680981 Color: 7
Size: 318953 Color: 8

Bin 2346: 67 of cap free
Amount of items: 2
Items: 
Size: 702349 Color: 4
Size: 297585 Color: 16

Bin 2347: 67 of cap free
Amount of items: 3
Items: 
Size: 721354 Color: 7
Size: 152280 Color: 8
Size: 126300 Color: 12

Bin 2348: 68 of cap free
Amount of items: 2
Items: 
Size: 525671 Color: 0
Size: 474262 Color: 14

Bin 2349: 68 of cap free
Amount of items: 2
Items: 
Size: 568506 Color: 14
Size: 431427 Color: 16

Bin 2350: 68 of cap free
Amount of items: 2
Items: 
Size: 589375 Color: 13
Size: 410558 Color: 6

Bin 2351: 68 of cap free
Amount of items: 2
Items: 
Size: 606110 Color: 2
Size: 393823 Color: 0

Bin 2352: 68 of cap free
Amount of items: 2
Items: 
Size: 611442 Color: 15
Size: 388491 Color: 9

Bin 2353: 68 of cap free
Amount of items: 2
Items: 
Size: 618328 Color: 6
Size: 381605 Color: 8

Bin 2354: 68 of cap free
Amount of items: 2
Items: 
Size: 656852 Color: 0
Size: 343081 Color: 3

Bin 2355: 68 of cap free
Amount of items: 2
Items: 
Size: 690099 Color: 0
Size: 309834 Color: 11

Bin 2356: 68 of cap free
Amount of items: 2
Items: 
Size: 705692 Color: 5
Size: 294241 Color: 4

Bin 2357: 68 of cap free
Amount of items: 2
Items: 
Size: 726236 Color: 14
Size: 273697 Color: 5

Bin 2358: 68 of cap free
Amount of items: 3
Items: 
Size: 733304 Color: 15
Size: 144771 Color: 8
Size: 121858 Color: 11

Bin 2359: 68 of cap free
Amount of items: 2
Items: 
Size: 733430 Color: 9
Size: 266503 Color: 16

Bin 2360: 68 of cap free
Amount of items: 2
Items: 
Size: 740817 Color: 14
Size: 259116 Color: 18

Bin 2361: 68 of cap free
Amount of items: 3
Items: 
Size: 795943 Color: 10
Size: 102270 Color: 16
Size: 101720 Color: 6

Bin 2362: 69 of cap free
Amount of items: 2
Items: 
Size: 502451 Color: 10
Size: 497481 Color: 13

Bin 2363: 69 of cap free
Amount of items: 2
Items: 
Size: 504431 Color: 2
Size: 495501 Color: 7

Bin 2364: 69 of cap free
Amount of items: 3
Items: 
Size: 538462 Color: 7
Size: 231958 Color: 16
Size: 229512 Color: 6

Bin 2365: 69 of cap free
Amount of items: 2
Items: 
Size: 543113 Color: 14
Size: 456819 Color: 13

Bin 2366: 69 of cap free
Amount of items: 2
Items: 
Size: 567205 Color: 9
Size: 432727 Color: 11

Bin 2367: 69 of cap free
Amount of items: 2
Items: 
Size: 616382 Color: 5
Size: 383550 Color: 2

Bin 2368: 69 of cap free
Amount of items: 2
Items: 
Size: 679059 Color: 5
Size: 320873 Color: 14

Bin 2369: 69 of cap free
Amount of items: 2
Items: 
Size: 701782 Color: 12
Size: 298150 Color: 11

Bin 2370: 69 of cap free
Amount of items: 2
Items: 
Size: 716862 Color: 2
Size: 283070 Color: 16

Bin 2371: 69 of cap free
Amount of items: 3
Items: 
Size: 741603 Color: 12
Size: 141047 Color: 13
Size: 117282 Color: 1

Bin 2372: 69 of cap free
Amount of items: 2
Items: 
Size: 766411 Color: 14
Size: 233521 Color: 6

Bin 2373: 69 of cap free
Amount of items: 3
Items: 
Size: 794947 Color: 14
Size: 102584 Color: 6
Size: 102401 Color: 17

Bin 2374: 70 of cap free
Amount of items: 2
Items: 
Size: 514820 Color: 4
Size: 485111 Color: 13

Bin 2375: 70 of cap free
Amount of items: 2
Items: 
Size: 529619 Color: 8
Size: 470312 Color: 12

Bin 2376: 70 of cap free
Amount of items: 2
Items: 
Size: 659888 Color: 14
Size: 340043 Color: 16

Bin 2377: 70 of cap free
Amount of items: 2
Items: 
Size: 664280 Color: 8
Size: 335651 Color: 15

Bin 2378: 70 of cap free
Amount of items: 2
Items: 
Size: 681839 Color: 12
Size: 318092 Color: 11

Bin 2379: 70 of cap free
Amount of items: 2
Items: 
Size: 709629 Color: 10
Size: 290302 Color: 15

Bin 2380: 70 of cap free
Amount of items: 2
Items: 
Size: 709742 Color: 14
Size: 290189 Color: 18

Bin 2381: 70 of cap free
Amount of items: 2
Items: 
Size: 716092 Color: 6
Size: 283839 Color: 12

Bin 2382: 70 of cap free
Amount of items: 3
Items: 
Size: 719399 Color: 19
Size: 158074 Color: 13
Size: 122458 Color: 1

Bin 2383: 70 of cap free
Amount of items: 2
Items: 
Size: 744903 Color: 5
Size: 255028 Color: 15

Bin 2384: 70 of cap free
Amount of items: 2
Items: 
Size: 774802 Color: 6
Size: 225129 Color: 11

Bin 2385: 71 of cap free
Amount of items: 3
Items: 
Size: 462565 Color: 11
Size: 276692 Color: 4
Size: 260673 Color: 1

Bin 2386: 71 of cap free
Amount of items: 2
Items: 
Size: 527500 Color: 7
Size: 472430 Color: 2

Bin 2387: 71 of cap free
Amount of items: 2
Items: 
Size: 561966 Color: 1
Size: 437964 Color: 12

Bin 2388: 71 of cap free
Amount of items: 2
Items: 
Size: 563506 Color: 6
Size: 436424 Color: 1

Bin 2389: 71 of cap free
Amount of items: 2
Items: 
Size: 588211 Color: 12
Size: 411719 Color: 1

Bin 2390: 71 of cap free
Amount of items: 2
Items: 
Size: 621046 Color: 6
Size: 378884 Color: 2

Bin 2391: 71 of cap free
Amount of items: 2
Items: 
Size: 678328 Color: 6
Size: 321602 Color: 13

Bin 2392: 71 of cap free
Amount of items: 2
Items: 
Size: 700468 Color: 9
Size: 299462 Color: 15

Bin 2393: 71 of cap free
Amount of items: 2
Items: 
Size: 714837 Color: 0
Size: 285093 Color: 14

Bin 2394: 71 of cap free
Amount of items: 2
Items: 
Size: 756443 Color: 11
Size: 243487 Color: 15

Bin 2395: 71 of cap free
Amount of items: 2
Items: 
Size: 758394 Color: 19
Size: 241536 Color: 7

Bin 2396: 71 of cap free
Amount of items: 2
Items: 
Size: 780958 Color: 5
Size: 218972 Color: 4

Bin 2397: 72 of cap free
Amount of items: 2
Items: 
Size: 513562 Color: 8
Size: 486367 Color: 11

Bin 2398: 72 of cap free
Amount of items: 2
Items: 
Size: 524427 Color: 1
Size: 475502 Color: 6

Bin 2399: 72 of cap free
Amount of items: 2
Items: 
Size: 530486 Color: 16
Size: 469443 Color: 13

Bin 2400: 72 of cap free
Amount of items: 2
Items: 
Size: 538306 Color: 4
Size: 461623 Color: 5

Bin 2401: 72 of cap free
Amount of items: 2
Items: 
Size: 545997 Color: 2
Size: 453932 Color: 11

Bin 2402: 72 of cap free
Amount of items: 2
Items: 
Size: 557920 Color: 3
Size: 442009 Color: 7

Bin 2403: 72 of cap free
Amount of items: 2
Items: 
Size: 607428 Color: 11
Size: 392501 Color: 1

Bin 2404: 72 of cap free
Amount of items: 2
Items: 
Size: 608099 Color: 4
Size: 391830 Color: 3

Bin 2405: 72 of cap free
Amount of items: 2
Items: 
Size: 608675 Color: 7
Size: 391254 Color: 8

Bin 2406: 72 of cap free
Amount of items: 2
Items: 
Size: 623406 Color: 11
Size: 376523 Color: 13

Bin 2407: 72 of cap free
Amount of items: 2
Items: 
Size: 630467 Color: 17
Size: 369462 Color: 13

Bin 2408: 72 of cap free
Amount of items: 2
Items: 
Size: 642381 Color: 8
Size: 357548 Color: 4

Bin 2409: 72 of cap free
Amount of items: 2
Items: 
Size: 644902 Color: 17
Size: 355027 Color: 6

Bin 2410: 72 of cap free
Amount of items: 2
Items: 
Size: 661131 Color: 11
Size: 338798 Color: 12

Bin 2411: 72 of cap free
Amount of items: 2
Items: 
Size: 688455 Color: 11
Size: 311474 Color: 2

Bin 2412: 72 of cap free
Amount of items: 2
Items: 
Size: 694350 Color: 9
Size: 305579 Color: 17

Bin 2413: 72 of cap free
Amount of items: 2
Items: 
Size: 756035 Color: 16
Size: 243894 Color: 4

Bin 2414: 72 of cap free
Amount of items: 3
Items: 
Size: 799759 Color: 0
Size: 100155 Color: 8
Size: 100015 Color: 5

Bin 2415: 73 of cap free
Amount of items: 2
Items: 
Size: 502101 Color: 4
Size: 497827 Color: 14

Bin 2416: 73 of cap free
Amount of items: 2
Items: 
Size: 505472 Color: 18
Size: 494456 Color: 10

Bin 2417: 73 of cap free
Amount of items: 2
Items: 
Size: 518703 Color: 10
Size: 481225 Color: 1

Bin 2418: 73 of cap free
Amount of items: 2
Items: 
Size: 523043 Color: 7
Size: 476885 Color: 1

Bin 2419: 73 of cap free
Amount of items: 2
Items: 
Size: 534453 Color: 13
Size: 465475 Color: 4

Bin 2420: 73 of cap free
Amount of items: 2
Items: 
Size: 647919 Color: 9
Size: 352009 Color: 3

Bin 2421: 73 of cap free
Amount of items: 2
Items: 
Size: 661023 Color: 3
Size: 338905 Color: 19

Bin 2422: 73 of cap free
Amount of items: 2
Items: 
Size: 667628 Color: 0
Size: 332300 Color: 19

Bin 2423: 73 of cap free
Amount of items: 2
Items: 
Size: 682570 Color: 7
Size: 317358 Color: 5

Bin 2424: 73 of cap free
Amount of items: 2
Items: 
Size: 689433 Color: 4
Size: 310495 Color: 19

Bin 2425: 73 of cap free
Amount of items: 2
Items: 
Size: 698662 Color: 17
Size: 301266 Color: 3

Bin 2426: 73 of cap free
Amount of items: 2
Items: 
Size: 752904 Color: 11
Size: 247024 Color: 18

Bin 2427: 73 of cap free
Amount of items: 3
Items: 
Size: 760974 Color: 6
Size: 122202 Color: 13
Size: 116752 Color: 17

Bin 2428: 73 of cap free
Amount of items: 3
Items: 
Size: 761564 Color: 13
Size: 120713 Color: 13
Size: 117651 Color: 2

Bin 2429: 73 of cap free
Amount of items: 2
Items: 
Size: 768497 Color: 19
Size: 231431 Color: 3

Bin 2430: 74 of cap free
Amount of items: 2
Items: 
Size: 540666 Color: 0
Size: 459261 Color: 8

Bin 2431: 74 of cap free
Amount of items: 2
Items: 
Size: 657384 Color: 1
Size: 342543 Color: 9

Bin 2432: 74 of cap free
Amount of items: 2
Items: 
Size: 727244 Color: 14
Size: 272683 Color: 7

Bin 2433: 74 of cap free
Amount of items: 2
Items: 
Size: 787570 Color: 17
Size: 212357 Color: 11

Bin 2434: 74 of cap free
Amount of items: 2
Items: 
Size: 788093 Color: 14
Size: 211834 Color: 0

Bin 2435: 75 of cap free
Amount of items: 2
Items: 
Size: 518142 Color: 15
Size: 481784 Color: 2

Bin 2436: 75 of cap free
Amount of items: 2
Items: 
Size: 550419 Color: 11
Size: 449507 Color: 2

Bin 2437: 75 of cap free
Amount of items: 2
Items: 
Size: 565583 Color: 7
Size: 434343 Color: 16

Bin 2438: 75 of cap free
Amount of items: 2
Items: 
Size: 587392 Color: 5
Size: 412534 Color: 8

Bin 2439: 75 of cap free
Amount of items: 2
Items: 
Size: 592014 Color: 18
Size: 407912 Color: 2

Bin 2440: 75 of cap free
Amount of items: 2
Items: 
Size: 592314 Color: 0
Size: 407612 Color: 16

Bin 2441: 75 of cap free
Amount of items: 3
Items: 
Size: 602169 Color: 9
Size: 225148 Color: 6
Size: 172609 Color: 5

Bin 2442: 75 of cap free
Amount of items: 2
Items: 
Size: 604600 Color: 10
Size: 395326 Color: 18

Bin 2443: 75 of cap free
Amount of items: 2
Items: 
Size: 622370 Color: 13
Size: 377556 Color: 7

Bin 2444: 75 of cap free
Amount of items: 2
Items: 
Size: 656247 Color: 13
Size: 343679 Color: 0

Bin 2445: 75 of cap free
Amount of items: 2
Items: 
Size: 701016 Color: 3
Size: 298910 Color: 0

Bin 2446: 75 of cap free
Amount of items: 2
Items: 
Size: 720067 Color: 10
Size: 279859 Color: 15

Bin 2447: 75 of cap free
Amount of items: 2
Items: 
Size: 745058 Color: 0
Size: 254868 Color: 5

Bin 2448: 75 of cap free
Amount of items: 2
Items: 
Size: 755694 Color: 16
Size: 244232 Color: 1

Bin 2449: 75 of cap free
Amount of items: 2
Items: 
Size: 757959 Color: 17
Size: 241967 Color: 8

Bin 2450: 75 of cap free
Amount of items: 2
Items: 
Size: 762155 Color: 3
Size: 237771 Color: 9

Bin 2451: 75 of cap free
Amount of items: 2
Items: 
Size: 770008 Color: 13
Size: 229918 Color: 11

Bin 2452: 75 of cap free
Amount of items: 3
Items: 
Size: 778618 Color: 8
Size: 113409 Color: 13
Size: 107899 Color: 4

Bin 2453: 76 of cap free
Amount of items: 2
Items: 
Size: 620276 Color: 0
Size: 379649 Color: 17

Bin 2454: 76 of cap free
Amount of items: 2
Items: 
Size: 667318 Color: 8
Size: 332607 Color: 15

Bin 2455: 76 of cap free
Amount of items: 2
Items: 
Size: 705188 Color: 19
Size: 294737 Color: 11

Bin 2456: 76 of cap free
Amount of items: 2
Items: 
Size: 709386 Color: 13
Size: 290539 Color: 18

Bin 2457: 76 of cap free
Amount of items: 2
Items: 
Size: 736064 Color: 2
Size: 263861 Color: 15

Bin 2458: 76 of cap free
Amount of items: 2
Items: 
Size: 749883 Color: 6
Size: 250042 Color: 18

Bin 2459: 76 of cap free
Amount of items: 2
Items: 
Size: 780800 Color: 12
Size: 219125 Color: 13

Bin 2460: 77 of cap free
Amount of items: 2
Items: 
Size: 535154 Color: 12
Size: 464770 Color: 2

Bin 2461: 77 of cap free
Amount of items: 2
Items: 
Size: 539555 Color: 3
Size: 460369 Color: 17

Bin 2462: 77 of cap free
Amount of items: 2
Items: 
Size: 588860 Color: 15
Size: 411064 Color: 13

Bin 2463: 77 of cap free
Amount of items: 2
Items: 
Size: 609403 Color: 18
Size: 390521 Color: 2

Bin 2464: 77 of cap free
Amount of items: 2
Items: 
Size: 609636 Color: 6
Size: 390288 Color: 13

Bin 2465: 77 of cap free
Amount of items: 2
Items: 
Size: 670401 Color: 13
Size: 329523 Color: 4

Bin 2466: 77 of cap free
Amount of items: 3
Items: 
Size: 793916 Color: 2
Size: 103101 Color: 3
Size: 102907 Color: 6

Bin 2467: 78 of cap free
Amount of items: 2
Items: 
Size: 511444 Color: 13
Size: 488479 Color: 4

Bin 2468: 78 of cap free
Amount of items: 2
Items: 
Size: 522301 Color: 16
Size: 477622 Color: 4

Bin 2469: 78 of cap free
Amount of items: 3
Items: 
Size: 537707 Color: 4
Size: 233463 Color: 10
Size: 228753 Color: 9

Bin 2470: 78 of cap free
Amount of items: 2
Items: 
Size: 541844 Color: 17
Size: 458079 Color: 16

Bin 2471: 78 of cap free
Amount of items: 2
Items: 
Size: 556429 Color: 16
Size: 443494 Color: 17

Bin 2472: 78 of cap free
Amount of items: 2
Items: 
Size: 606125 Color: 18
Size: 393798 Color: 12

Bin 2473: 78 of cap free
Amount of items: 2
Items: 
Size: 611439 Color: 1
Size: 388484 Color: 15

Bin 2474: 78 of cap free
Amount of items: 2
Items: 
Size: 625946 Color: 19
Size: 373977 Color: 13

Bin 2475: 78 of cap free
Amount of items: 2
Items: 
Size: 670569 Color: 19
Size: 329354 Color: 9

Bin 2476: 78 of cap free
Amount of items: 2
Items: 
Size: 693620 Color: 2
Size: 306303 Color: 3

Bin 2477: 78 of cap free
Amount of items: 3
Items: 
Size: 704999 Color: 18
Size: 147706 Color: 2
Size: 147218 Color: 0

Bin 2478: 78 of cap free
Amount of items: 2
Items: 
Size: 715174 Color: 0
Size: 284749 Color: 12

Bin 2479: 78 of cap free
Amount of items: 2
Items: 
Size: 750116 Color: 15
Size: 249807 Color: 5

Bin 2480: 78 of cap free
Amount of items: 2
Items: 
Size: 760729 Color: 11
Size: 239194 Color: 12

Bin 2481: 79 of cap free
Amount of items: 2
Items: 
Size: 510572 Color: 1
Size: 489350 Color: 7

Bin 2482: 79 of cap free
Amount of items: 2
Items: 
Size: 530895 Color: 6
Size: 469027 Color: 13

Bin 2483: 79 of cap free
Amount of items: 2
Items: 
Size: 570532 Color: 18
Size: 429390 Color: 16

Bin 2484: 79 of cap free
Amount of items: 2
Items: 
Size: 579813 Color: 18
Size: 420109 Color: 15

Bin 2485: 79 of cap free
Amount of items: 2
Items: 
Size: 592509 Color: 15
Size: 407413 Color: 9

Bin 2486: 79 of cap free
Amount of items: 2
Items: 
Size: 607660 Color: 13
Size: 392262 Color: 1

Bin 2487: 79 of cap free
Amount of items: 2
Items: 
Size: 612263 Color: 17
Size: 387659 Color: 19

Bin 2488: 79 of cap free
Amount of items: 2
Items: 
Size: 627699 Color: 19
Size: 372223 Color: 15

Bin 2489: 79 of cap free
Amount of items: 2
Items: 
Size: 632053 Color: 16
Size: 367869 Color: 9

Bin 2490: 79 of cap free
Amount of items: 2
Items: 
Size: 675466 Color: 13
Size: 324456 Color: 1

Bin 2491: 79 of cap free
Amount of items: 2
Items: 
Size: 677457 Color: 8
Size: 322465 Color: 15

Bin 2492: 79 of cap free
Amount of items: 2
Items: 
Size: 718343 Color: 5
Size: 281579 Color: 9

Bin 2493: 79 of cap free
Amount of items: 2
Items: 
Size: 731809 Color: 7
Size: 268113 Color: 16

Bin 2494: 79 of cap free
Amount of items: 2
Items: 
Size: 751384 Color: 17
Size: 248538 Color: 10

Bin 2495: 79 of cap free
Amount of items: 3
Items: 
Size: 764876 Color: 17
Size: 118486 Color: 13
Size: 116560 Color: 11

Bin 2496: 79 of cap free
Amount of items: 2
Items: 
Size: 767547 Color: 0
Size: 232375 Color: 12

Bin 2497: 79 of cap free
Amount of items: 2
Items: 
Size: 772326 Color: 12
Size: 227596 Color: 13

Bin 2498: 79 of cap free
Amount of items: 2
Items: 
Size: 792840 Color: 1
Size: 207082 Color: 0

Bin 2499: 79 of cap free
Amount of items: 2
Items: 
Size: 798198 Color: 14
Size: 201724 Color: 2

Bin 2500: 80 of cap free
Amount of items: 2
Items: 
Size: 516095 Color: 0
Size: 483826 Color: 6

Bin 2501: 80 of cap free
Amount of items: 2
Items: 
Size: 529789 Color: 0
Size: 470132 Color: 19

Bin 2502: 80 of cap free
Amount of items: 2
Items: 
Size: 549351 Color: 7
Size: 450570 Color: 1

Bin 2503: 80 of cap free
Amount of items: 2
Items: 
Size: 555236 Color: 16
Size: 444685 Color: 11

Bin 2504: 80 of cap free
Amount of items: 2
Items: 
Size: 555390 Color: 0
Size: 444531 Color: 11

Bin 2505: 80 of cap free
Amount of items: 2
Items: 
Size: 558507 Color: 12
Size: 441414 Color: 18

Bin 2506: 80 of cap free
Amount of items: 2
Items: 
Size: 615720 Color: 9
Size: 384201 Color: 3

Bin 2507: 80 of cap free
Amount of items: 3
Items: 
Size: 619104 Color: 17
Size: 212841 Color: 17
Size: 167976 Color: 14

Bin 2508: 80 of cap free
Amount of items: 2
Items: 
Size: 634586 Color: 3
Size: 365335 Color: 9

Bin 2509: 80 of cap free
Amount of items: 2
Items: 
Size: 644233 Color: 4
Size: 355688 Color: 6

Bin 2510: 80 of cap free
Amount of items: 2
Items: 
Size: 671026 Color: 4
Size: 328895 Color: 9

Bin 2511: 80 of cap free
Amount of items: 2
Items: 
Size: 706652 Color: 7
Size: 293269 Color: 6

Bin 2512: 80 of cap free
Amount of items: 2
Items: 
Size: 711856 Color: 18
Size: 288065 Color: 13

Bin 2513: 80 of cap free
Amount of items: 2
Items: 
Size: 724441 Color: 5
Size: 275480 Color: 3

Bin 2514: 80 of cap free
Amount of items: 2
Items: 
Size: 734046 Color: 13
Size: 265875 Color: 0

Bin 2515: 80 of cap free
Amount of items: 2
Items: 
Size: 760659 Color: 8
Size: 239262 Color: 11

Bin 2516: 81 of cap free
Amount of items: 2
Items: 
Size: 519009 Color: 8
Size: 480911 Color: 6

Bin 2517: 81 of cap free
Amount of items: 2
Items: 
Size: 531588 Color: 19
Size: 468332 Color: 8

Bin 2518: 81 of cap free
Amount of items: 2
Items: 
Size: 563182 Color: 13
Size: 436738 Color: 5

Bin 2519: 81 of cap free
Amount of items: 2
Items: 
Size: 584656 Color: 5
Size: 415264 Color: 0

Bin 2520: 81 of cap free
Amount of items: 2
Items: 
Size: 617109 Color: 12
Size: 382811 Color: 2

Bin 2521: 81 of cap free
Amount of items: 2
Items: 
Size: 718077 Color: 4
Size: 281843 Color: 6

Bin 2522: 81 of cap free
Amount of items: 2
Items: 
Size: 744478 Color: 17
Size: 255442 Color: 15

Bin 2523: 81 of cap free
Amount of items: 2
Items: 
Size: 777241 Color: 8
Size: 222679 Color: 17

Bin 2524: 81 of cap free
Amount of items: 2
Items: 
Size: 785425 Color: 10
Size: 214495 Color: 1

Bin 2525: 81 of cap free
Amount of items: 2
Items: 
Size: 786239 Color: 16
Size: 213681 Color: 10

Bin 2526: 82 of cap free
Amount of items: 2
Items: 
Size: 538087 Color: 17
Size: 461832 Color: 11

Bin 2527: 82 of cap free
Amount of items: 2
Items: 
Size: 538965 Color: 2
Size: 460954 Color: 3

Bin 2528: 82 of cap free
Amount of items: 2
Items: 
Size: 546795 Color: 18
Size: 453124 Color: 8

Bin 2529: 82 of cap free
Amount of items: 2
Items: 
Size: 569257 Color: 15
Size: 430662 Color: 1

Bin 2530: 82 of cap free
Amount of items: 2
Items: 
Size: 661573 Color: 4
Size: 338346 Color: 12

Bin 2531: 82 of cap free
Amount of items: 2
Items: 
Size: 673500 Color: 7
Size: 326419 Color: 15

Bin 2532: 82 of cap free
Amount of items: 3
Items: 
Size: 710159 Color: 5
Size: 155214 Color: 14
Size: 134546 Color: 12

Bin 2533: 82 of cap free
Amount of items: 2
Items: 
Size: 731325 Color: 4
Size: 268594 Color: 15

Bin 2534: 82 of cap free
Amount of items: 2
Items: 
Size: 739114 Color: 7
Size: 260805 Color: 14

Bin 2535: 82 of cap free
Amount of items: 2
Items: 
Size: 797608 Color: 0
Size: 202311 Color: 6

Bin 2536: 83 of cap free
Amount of items: 3
Items: 
Size: 416872 Color: 10
Size: 328251 Color: 15
Size: 254795 Color: 0

Bin 2537: 83 of cap free
Amount of items: 2
Items: 
Size: 512174 Color: 17
Size: 487744 Color: 11

Bin 2538: 83 of cap free
Amount of items: 2
Items: 
Size: 563328 Color: 0
Size: 436590 Color: 1

Bin 2539: 83 of cap free
Amount of items: 2
Items: 
Size: 616766 Color: 19
Size: 383152 Color: 17

Bin 2540: 83 of cap free
Amount of items: 2
Items: 
Size: 617264 Color: 16
Size: 382654 Color: 13

Bin 2541: 83 of cap free
Amount of items: 2
Items: 
Size: 628310 Color: 3
Size: 371608 Color: 19

Bin 2542: 83 of cap free
Amount of items: 2
Items: 
Size: 682669 Color: 10
Size: 317249 Color: 6

Bin 2543: 83 of cap free
Amount of items: 2
Items: 
Size: 695491 Color: 14
Size: 304427 Color: 16

Bin 2544: 83 of cap free
Amount of items: 2
Items: 
Size: 706280 Color: 4
Size: 293638 Color: 2

Bin 2545: 83 of cap free
Amount of items: 2
Items: 
Size: 720513 Color: 17
Size: 279405 Color: 10

Bin 2546: 83 of cap free
Amount of items: 2
Items: 
Size: 721025 Color: 19
Size: 278893 Color: 18

Bin 2547: 83 of cap free
Amount of items: 2
Items: 
Size: 772221 Color: 0
Size: 227697 Color: 18

Bin 2548: 84 of cap free
Amount of items: 2
Items: 
Size: 516249 Color: 12
Size: 483668 Color: 17

Bin 2549: 84 of cap free
Amount of items: 2
Items: 
Size: 560291 Color: 6
Size: 439626 Color: 1

Bin 2550: 84 of cap free
Amount of items: 3
Items: 
Size: 583528 Color: 16
Size: 221015 Color: 16
Size: 195374 Color: 18

Bin 2551: 84 of cap free
Amount of items: 2
Items: 
Size: 641022 Color: 8
Size: 358895 Color: 4

Bin 2552: 84 of cap free
Amount of items: 2
Items: 
Size: 659321 Color: 7
Size: 340596 Color: 10

Bin 2553: 84 of cap free
Amount of items: 2
Items: 
Size: 666999 Color: 3
Size: 332918 Color: 6

Bin 2554: 84 of cap free
Amount of items: 2
Items: 
Size: 708413 Color: 6
Size: 291504 Color: 8

Bin 2555: 84 of cap free
Amount of items: 2
Items: 
Size: 746418 Color: 18
Size: 253499 Color: 12

Bin 2556: 84 of cap free
Amount of items: 2
Items: 
Size: 756845 Color: 9
Size: 243072 Color: 3

Bin 2557: 84 of cap free
Amount of items: 2
Items: 
Size: 790209 Color: 15
Size: 209708 Color: 12

Bin 2558: 85 of cap free
Amount of items: 2
Items: 
Size: 502343 Color: 10
Size: 497573 Color: 5

Bin 2559: 85 of cap free
Amount of items: 2
Items: 
Size: 510763 Color: 12
Size: 489153 Color: 5

Bin 2560: 85 of cap free
Amount of items: 2
Items: 
Size: 537066 Color: 15
Size: 462850 Color: 6

Bin 2561: 85 of cap free
Amount of items: 2
Items: 
Size: 537491 Color: 19
Size: 462425 Color: 16

Bin 2562: 85 of cap free
Amount of items: 3
Items: 
Size: 558458 Color: 16
Size: 223191 Color: 18
Size: 218267 Color: 19

Bin 2563: 85 of cap free
Amount of items: 2
Items: 
Size: 583967 Color: 19
Size: 415949 Color: 16

Bin 2564: 85 of cap free
Amount of items: 2
Items: 
Size: 689691 Color: 3
Size: 310225 Color: 19

Bin 2565: 85 of cap free
Amount of items: 2
Items: 
Size: 730739 Color: 9
Size: 269177 Color: 4

Bin 2566: 85 of cap free
Amount of items: 2
Items: 
Size: 762338 Color: 1
Size: 237578 Color: 16

Bin 2567: 85 of cap free
Amount of items: 2
Items: 
Size: 762496 Color: 6
Size: 237420 Color: 19

Bin 2568: 85 of cap free
Amount of items: 2
Items: 
Size: 795905 Color: 0
Size: 204011 Color: 4

Bin 2569: 86 of cap free
Amount of items: 2
Items: 
Size: 544673 Color: 19
Size: 455242 Color: 2

Bin 2570: 86 of cap free
Amount of items: 2
Items: 
Size: 583746 Color: 13
Size: 416169 Color: 3

Bin 2571: 86 of cap free
Amount of items: 2
Items: 
Size: 603689 Color: 4
Size: 396226 Color: 2

Bin 2572: 86 of cap free
Amount of items: 2
Items: 
Size: 626805 Color: 1
Size: 373110 Color: 3

Bin 2573: 86 of cap free
Amount of items: 3
Items: 
Size: 643127 Color: 13
Size: 185938 Color: 2
Size: 170850 Color: 16

Bin 2574: 86 of cap free
Amount of items: 2
Items: 
Size: 676782 Color: 1
Size: 323133 Color: 15

Bin 2575: 86 of cap free
Amount of items: 2
Items: 
Size: 723320 Color: 12
Size: 276595 Color: 18

Bin 2576: 86 of cap free
Amount of items: 2
Items: 
Size: 737337 Color: 12
Size: 262578 Color: 18

Bin 2577: 86 of cap free
Amount of items: 3
Items: 
Size: 747286 Color: 17
Size: 135338 Color: 13
Size: 117291 Color: 10

Bin 2578: 86 of cap free
Amount of items: 2
Items: 
Size: 753860 Color: 9
Size: 246055 Color: 3

Bin 2579: 87 of cap free
Amount of items: 2
Items: 
Size: 522010 Color: 6
Size: 477904 Color: 16

Bin 2580: 87 of cap free
Amount of items: 2
Items: 
Size: 572195 Color: 10
Size: 427719 Color: 7

Bin 2581: 87 of cap free
Amount of items: 2
Items: 
Size: 589156 Color: 11
Size: 410758 Color: 17

Bin 2582: 87 of cap free
Amount of items: 2
Items: 
Size: 603026 Color: 16
Size: 396888 Color: 11

Bin 2583: 87 of cap free
Amount of items: 2
Items: 
Size: 657096 Color: 10
Size: 342818 Color: 7

Bin 2584: 87 of cap free
Amount of items: 2
Items: 
Size: 666580 Color: 4
Size: 333334 Color: 6

Bin 2585: 87 of cap free
Amount of items: 2
Items: 
Size: 702450 Color: 4
Size: 297464 Color: 5

Bin 2586: 87 of cap free
Amount of items: 2
Items: 
Size: 761274 Color: 4
Size: 238640 Color: 11

Bin 2587: 88 of cap free
Amount of items: 2
Items: 
Size: 501804 Color: 11
Size: 498109 Color: 13

Bin 2588: 88 of cap free
Amount of items: 2
Items: 
Size: 574091 Color: 11
Size: 425822 Color: 3

Bin 2589: 88 of cap free
Amount of items: 2
Items: 
Size: 578372 Color: 6
Size: 421541 Color: 16

Bin 2590: 88 of cap free
Amount of items: 2
Items: 
Size: 768926 Color: 4
Size: 230987 Color: 12

Bin 2591: 89 of cap free
Amount of items: 2
Items: 
Size: 611962 Color: 5
Size: 387950 Color: 9

Bin 2592: 89 of cap free
Amount of items: 2
Items: 
Size: 614856 Color: 12
Size: 385056 Color: 4

Bin 2593: 89 of cap free
Amount of items: 2
Items: 
Size: 625418 Color: 9
Size: 374494 Color: 3

Bin 2594: 89 of cap free
Amount of items: 2
Items: 
Size: 665423 Color: 13
Size: 334489 Color: 10

Bin 2595: 89 of cap free
Amount of items: 2
Items: 
Size: 672354 Color: 5
Size: 327558 Color: 13

Bin 2596: 90 of cap free
Amount of items: 2
Items: 
Size: 518586 Color: 6
Size: 481325 Color: 17

Bin 2597: 90 of cap free
Amount of items: 2
Items: 
Size: 595410 Color: 17
Size: 404501 Color: 7

Bin 2598: 90 of cap free
Amount of items: 2
Items: 
Size: 745657 Color: 17
Size: 254254 Color: 16

Bin 2599: 90 of cap free
Amount of items: 2
Items: 
Size: 756238 Color: 12
Size: 243673 Color: 0

Bin 2600: 90 of cap free
Amount of items: 2
Items: 
Size: 790302 Color: 9
Size: 209609 Color: 4

Bin 2601: 91 of cap free
Amount of items: 2
Items: 
Size: 521827 Color: 1
Size: 478083 Color: 5

Bin 2602: 91 of cap free
Amount of items: 2
Items: 
Size: 575109 Color: 6
Size: 424801 Color: 1

Bin 2603: 91 of cap free
Amount of items: 2
Items: 
Size: 580818 Color: 4
Size: 419092 Color: 8

Bin 2604: 91 of cap free
Amount of items: 2
Items: 
Size: 591741 Color: 7
Size: 408169 Color: 4

Bin 2605: 91 of cap free
Amount of items: 2
Items: 
Size: 682395 Color: 13
Size: 317515 Color: 19

Bin 2606: 91 of cap free
Amount of items: 2
Items: 
Size: 763080 Color: 16
Size: 236830 Color: 0

Bin 2607: 92 of cap free
Amount of items: 3
Items: 
Size: 346152 Color: 19
Size: 343805 Color: 16
Size: 309952 Color: 18

Bin 2608: 92 of cap free
Amount of items: 3
Items: 
Size: 351454 Color: 2
Size: 339652 Color: 4
Size: 308803 Color: 12

Bin 2609: 92 of cap free
Amount of items: 2
Items: 
Size: 511799 Color: 3
Size: 488110 Color: 7

Bin 2610: 92 of cap free
Amount of items: 2
Items: 
Size: 617085 Color: 0
Size: 382824 Color: 15

Bin 2611: 93 of cap free
Amount of items: 2
Items: 
Size: 532584 Color: 1
Size: 467324 Color: 8

Bin 2612: 93 of cap free
Amount of items: 2
Items: 
Size: 571551 Color: 13
Size: 428357 Color: 10

Bin 2613: 93 of cap free
Amount of items: 2
Items: 
Size: 584824 Color: 1
Size: 415084 Color: 12

Bin 2614: 93 of cap free
Amount of items: 2
Items: 
Size: 593462 Color: 7
Size: 406446 Color: 16

Bin 2615: 93 of cap free
Amount of items: 2
Items: 
Size: 717520 Color: 9
Size: 282388 Color: 10

Bin 2616: 93 of cap free
Amount of items: 3
Items: 
Size: 739494 Color: 12
Size: 142380 Color: 13
Size: 118034 Color: 9

Bin 2617: 93 of cap free
Amount of items: 2
Items: 
Size: 768687 Color: 3
Size: 231221 Color: 14

Bin 2618: 94 of cap free
Amount of items: 3
Items: 
Size: 350765 Color: 11
Size: 339011 Color: 13
Size: 310131 Color: 7

Bin 2619: 94 of cap free
Amount of items: 2
Items: 
Size: 500490 Color: 14
Size: 499417 Color: 7

Bin 2620: 94 of cap free
Amount of items: 2
Items: 
Size: 578039 Color: 14
Size: 421868 Color: 19

Bin 2621: 94 of cap free
Amount of items: 2
Items: 
Size: 587185 Color: 16
Size: 412722 Color: 17

Bin 2622: 94 of cap free
Amount of items: 2
Items: 
Size: 699564 Color: 12
Size: 300343 Color: 2

Bin 2623: 95 of cap free
Amount of items: 3
Items: 
Size: 409417 Color: 19
Size: 338065 Color: 1
Size: 252424 Color: 13

Bin 2624: 95 of cap free
Amount of items: 3
Items: 
Size: 422457 Color: 19
Size: 309348 Color: 1
Size: 268101 Color: 16

Bin 2625: 95 of cap free
Amount of items: 2
Items: 
Size: 550851 Color: 13
Size: 449055 Color: 15

Bin 2626: 95 of cap free
Amount of items: 2
Items: 
Size: 594989 Color: 5
Size: 404917 Color: 12

Bin 2627: 95 of cap free
Amount of items: 2
Items: 
Size: 633605 Color: 14
Size: 366301 Color: 19

Bin 2628: 95 of cap free
Amount of items: 2
Items: 
Size: 752313 Color: 8
Size: 247593 Color: 19

Bin 2629: 95 of cap free
Amount of items: 2
Items: 
Size: 760112 Color: 7
Size: 239794 Color: 0

Bin 2630: 95 of cap free
Amount of items: 2
Items: 
Size: 777408 Color: 19
Size: 222498 Color: 10

Bin 2631: 95 of cap free
Amount of items: 2
Items: 
Size: 785690 Color: 9
Size: 214216 Color: 3

Bin 2632: 95 of cap free
Amount of items: 3
Items: 
Size: 796893 Color: 2
Size: 101617 Color: 16
Size: 101396 Color: 13

Bin 2633: 96 of cap free
Amount of items: 2
Items: 
Size: 592228 Color: 7
Size: 407677 Color: 11

Bin 2634: 96 of cap free
Amount of items: 2
Items: 
Size: 634204 Color: 15
Size: 365701 Color: 16

Bin 2635: 96 of cap free
Amount of items: 2
Items: 
Size: 671269 Color: 10
Size: 328636 Color: 0

Bin 2636: 96 of cap free
Amount of items: 2
Items: 
Size: 688041 Color: 14
Size: 311864 Color: 0

Bin 2637: 96 of cap free
Amount of items: 2
Items: 
Size: 786726 Color: 19
Size: 213179 Color: 14

Bin 2638: 97 of cap free
Amount of items: 2
Items: 
Size: 504636 Color: 14
Size: 495268 Color: 11

Bin 2639: 97 of cap free
Amount of items: 2
Items: 
Size: 536483 Color: 3
Size: 463421 Color: 12

Bin 2640: 97 of cap free
Amount of items: 2
Items: 
Size: 540177 Color: 15
Size: 459727 Color: 16

Bin 2641: 97 of cap free
Amount of items: 2
Items: 
Size: 547895 Color: 3
Size: 452009 Color: 0

Bin 2642: 97 of cap free
Amount of items: 2
Items: 
Size: 548486 Color: 14
Size: 451418 Color: 1

Bin 2643: 97 of cap free
Amount of items: 2
Items: 
Size: 558224 Color: 0
Size: 441680 Color: 8

Bin 2644: 97 of cap free
Amount of items: 2
Items: 
Size: 574274 Color: 5
Size: 425630 Color: 15

Bin 2645: 97 of cap free
Amount of items: 2
Items: 
Size: 575356 Color: 2
Size: 424548 Color: 3

Bin 2646: 97 of cap free
Amount of items: 2
Items: 
Size: 590356 Color: 4
Size: 409548 Color: 17

Bin 2647: 97 of cap free
Amount of items: 2
Items: 
Size: 604222 Color: 5
Size: 395682 Color: 2

Bin 2648: 97 of cap free
Amount of items: 2
Items: 
Size: 646086 Color: 3
Size: 353818 Color: 0

Bin 2649: 97 of cap free
Amount of items: 2
Items: 
Size: 668243 Color: 17
Size: 331661 Color: 18

Bin 2650: 97 of cap free
Amount of items: 2
Items: 
Size: 722439 Color: 14
Size: 277465 Color: 11

Bin 2651: 97 of cap free
Amount of items: 2
Items: 
Size: 741268 Color: 14
Size: 258636 Color: 7

Bin 2652: 97 of cap free
Amount of items: 3
Items: 
Size: 760126 Color: 9
Size: 123044 Color: 13
Size: 116734 Color: 1

Bin 2653: 98 of cap free
Amount of items: 2
Items: 
Size: 535051 Color: 5
Size: 464852 Color: 1

Bin 2654: 98 of cap free
Amount of items: 2
Items: 
Size: 537849 Color: 10
Size: 462054 Color: 18

Bin 2655: 98 of cap free
Amount of items: 2
Items: 
Size: 579671 Color: 2
Size: 420232 Color: 6

Bin 2656: 98 of cap free
Amount of items: 2
Items: 
Size: 580103 Color: 11
Size: 419800 Color: 13

Bin 2657: 98 of cap free
Amount of items: 2
Items: 
Size: 581152 Color: 12
Size: 418751 Color: 10

Bin 2658: 98 of cap free
Amount of items: 2
Items: 
Size: 616754 Color: 1
Size: 383149 Color: 10

Bin 2659: 98 of cap free
Amount of items: 2
Items: 
Size: 666994 Color: 8
Size: 332909 Color: 18

Bin 2660: 98 of cap free
Amount of items: 3
Items: 
Size: 697028 Color: 3
Size: 151735 Color: 10
Size: 151140 Color: 15

Bin 2661: 98 of cap free
Amount of items: 2
Items: 
Size: 716605 Color: 7
Size: 283298 Color: 2

Bin 2662: 98 of cap free
Amount of items: 2
Items: 
Size: 729254 Color: 15
Size: 270649 Color: 12

Bin 2663: 98 of cap free
Amount of items: 2
Items: 
Size: 750115 Color: 5
Size: 249788 Color: 0

Bin 2664: 98 of cap free
Amount of items: 2
Items: 
Size: 750690 Color: 5
Size: 249213 Color: 10

Bin 2665: 98 of cap free
Amount of items: 2
Items: 
Size: 753598 Color: 2
Size: 246305 Color: 13

Bin 2666: 98 of cap free
Amount of items: 2
Items: 
Size: 794106 Color: 12
Size: 205797 Color: 2

Bin 2667: 98 of cap free
Amount of items: 2
Items: 
Size: 796420 Color: 12
Size: 203483 Color: 9

Bin 2668: 99 of cap free
Amount of items: 2
Items: 
Size: 529454 Color: 14
Size: 470448 Color: 19

Bin 2669: 99 of cap free
Amount of items: 2
Items: 
Size: 555917 Color: 17
Size: 443985 Color: 0

Bin 2670: 99 of cap free
Amount of items: 2
Items: 
Size: 588373 Color: 18
Size: 411529 Color: 10

Bin 2671: 99 of cap free
Amount of items: 2
Items: 
Size: 612837 Color: 16
Size: 387065 Color: 15

Bin 2672: 99 of cap free
Amount of items: 2
Items: 
Size: 703716 Color: 0
Size: 296186 Color: 6

Bin 2673: 99 of cap free
Amount of items: 3
Items: 
Size: 792710 Color: 4
Size: 103757 Color: 13
Size: 103435 Color: 10

Bin 2674: 99 of cap free
Amount of items: 2
Items: 
Size: 796267 Color: 15
Size: 203635 Color: 10

Bin 2675: 100 of cap free
Amount of items: 3
Items: 
Size: 577203 Color: 2
Size: 226001 Color: 10
Size: 196697 Color: 5

Bin 2676: 100 of cap free
Amount of items: 2
Items: 
Size: 581325 Color: 19
Size: 418576 Color: 0

Bin 2677: 100 of cap free
Amount of items: 2
Items: 
Size: 629452 Color: 15
Size: 370449 Color: 12

Bin 2678: 100 of cap free
Amount of items: 2
Items: 
Size: 642362 Color: 15
Size: 357539 Color: 6

Bin 2679: 100 of cap free
Amount of items: 2
Items: 
Size: 663940 Color: 9
Size: 335961 Color: 15

Bin 2680: 100 of cap free
Amount of items: 2
Items: 
Size: 687375 Color: 8
Size: 312526 Color: 9

Bin 2681: 100 of cap free
Amount of items: 2
Items: 
Size: 691449 Color: 19
Size: 308452 Color: 4

Bin 2682: 100 of cap free
Amount of items: 2
Items: 
Size: 692564 Color: 0
Size: 307337 Color: 13

Bin 2683: 100 of cap free
Amount of items: 2
Items: 
Size: 719816 Color: 8
Size: 280085 Color: 0

Bin 2684: 100 of cap free
Amount of items: 2
Items: 
Size: 775634 Color: 19
Size: 224267 Color: 14

Bin 2685: 101 of cap free
Amount of items: 2
Items: 
Size: 517529 Color: 4
Size: 482371 Color: 19

Bin 2686: 101 of cap free
Amount of items: 2
Items: 
Size: 566635 Color: 17
Size: 433265 Color: 8

Bin 2687: 101 of cap free
Amount of items: 2
Items: 
Size: 608182 Color: 13
Size: 391718 Color: 0

Bin 2688: 101 of cap free
Amount of items: 2
Items: 
Size: 655131 Color: 19
Size: 344769 Color: 5

Bin 2689: 101 of cap free
Amount of items: 2
Items: 
Size: 695862 Color: 6
Size: 304038 Color: 2

Bin 2690: 101 of cap free
Amount of items: 2
Items: 
Size: 707271 Color: 7
Size: 292629 Color: 4

Bin 2691: 101 of cap free
Amount of items: 2
Items: 
Size: 730280 Color: 17
Size: 269620 Color: 16

Bin 2692: 101 of cap free
Amount of items: 2
Items: 
Size: 796695 Color: 18
Size: 203205 Color: 7

Bin 2693: 102 of cap free
Amount of items: 2
Items: 
Size: 514255 Color: 10
Size: 485644 Color: 3

Bin 2694: 102 of cap free
Amount of items: 2
Items: 
Size: 569911 Color: 0
Size: 429988 Color: 15

Bin 2695: 102 of cap free
Amount of items: 2
Items: 
Size: 609358 Color: 3
Size: 390541 Color: 12

Bin 2696: 102 of cap free
Amount of items: 2
Items: 
Size: 619068 Color: 18
Size: 380831 Color: 0

Bin 2697: 102 of cap free
Amount of items: 2
Items: 
Size: 648342 Color: 16
Size: 351557 Color: 8

Bin 2698: 102 of cap free
Amount of items: 2
Items: 
Size: 689211 Color: 6
Size: 310688 Color: 18

Bin 2699: 102 of cap free
Amount of items: 2
Items: 
Size: 711182 Color: 18
Size: 288717 Color: 17

Bin 2700: 102 of cap free
Amount of items: 2
Items: 
Size: 750009 Color: 11
Size: 249890 Color: 13

Bin 2701: 103 of cap free
Amount of items: 3
Items: 
Size: 361290 Color: 9
Size: 348663 Color: 10
Size: 289945 Color: 17

Bin 2702: 103 of cap free
Amount of items: 2
Items: 
Size: 504664 Color: 11
Size: 495234 Color: 15

Bin 2703: 103 of cap free
Amount of items: 2
Items: 
Size: 526030 Color: 18
Size: 473868 Color: 14

Bin 2704: 103 of cap free
Amount of items: 2
Items: 
Size: 627786 Color: 10
Size: 372112 Color: 13

Bin 2705: 103 of cap free
Amount of items: 2
Items: 
Size: 682012 Color: 2
Size: 317886 Color: 0

Bin 2706: 103 of cap free
Amount of items: 2
Items: 
Size: 685315 Color: 15
Size: 314583 Color: 9

Bin 2707: 103 of cap free
Amount of items: 2
Items: 
Size: 727674 Color: 1
Size: 272224 Color: 19

Bin 2708: 103 of cap free
Amount of items: 2
Items: 
Size: 764427 Color: 14
Size: 235471 Color: 2

Bin 2709: 103 of cap free
Amount of items: 2
Items: 
Size: 770304 Color: 19
Size: 229594 Color: 1

Bin 2710: 103 of cap free
Amount of items: 2
Items: 
Size: 775362 Color: 6
Size: 224536 Color: 19

Bin 2711: 104 of cap free
Amount of items: 2
Items: 
Size: 508399 Color: 11
Size: 491498 Color: 17

Bin 2712: 104 of cap free
Amount of items: 3
Items: 
Size: 618446 Color: 14
Size: 191666 Color: 10
Size: 189785 Color: 12

Bin 2713: 104 of cap free
Amount of items: 2
Items: 
Size: 660617 Color: 14
Size: 339280 Color: 15

Bin 2714: 104 of cap free
Amount of items: 2
Items: 
Size: 666073 Color: 9
Size: 333824 Color: 16

Bin 2715: 104 of cap free
Amount of items: 2
Items: 
Size: 689063 Color: 12
Size: 310834 Color: 3

Bin 2716: 104 of cap free
Amount of items: 3
Items: 
Size: 691123 Color: 17
Size: 155674 Color: 17
Size: 153100 Color: 7

Bin 2717: 104 of cap free
Amount of items: 2
Items: 
Size: 748822 Color: 0
Size: 251075 Color: 17

Bin 2718: 104 of cap free
Amount of items: 2
Items: 
Size: 789954 Color: 2
Size: 209943 Color: 19

Bin 2719: 105 of cap free
Amount of items: 2
Items: 
Size: 517527 Color: 16
Size: 482369 Color: 13

Bin 2720: 105 of cap free
Amount of items: 2
Items: 
Size: 545867 Color: 13
Size: 454029 Color: 15

Bin 2721: 105 of cap free
Amount of items: 2
Items: 
Size: 549920 Color: 19
Size: 449976 Color: 1

Bin 2722: 105 of cap free
Amount of items: 2
Items: 
Size: 568979 Color: 17
Size: 430917 Color: 11

Bin 2723: 105 of cap free
Amount of items: 2
Items: 
Size: 585325 Color: 15
Size: 414571 Color: 4

Bin 2724: 105 of cap free
Amount of items: 2
Items: 
Size: 599613 Color: 11
Size: 400283 Color: 7

Bin 2725: 105 of cap free
Amount of items: 3
Items: 
Size: 642586 Color: 6
Size: 191290 Color: 2
Size: 166020 Color: 18

Bin 2726: 105 of cap free
Amount of items: 2
Items: 
Size: 651114 Color: 0
Size: 348782 Color: 3

Bin 2727: 105 of cap free
Amount of items: 2
Items: 
Size: 701104 Color: 2
Size: 298792 Color: 16

Bin 2728: 105 of cap free
Amount of items: 2
Items: 
Size: 777225 Color: 7
Size: 222671 Color: 17

Bin 2729: 106 of cap free
Amount of items: 2
Items: 
Size: 507109 Color: 1
Size: 492786 Color: 8

Bin 2730: 106 of cap free
Amount of items: 2
Items: 
Size: 590051 Color: 9
Size: 409844 Color: 15

Bin 2731: 106 of cap free
Amount of items: 2
Items: 
Size: 611848 Color: 16
Size: 388047 Color: 1

Bin 2732: 106 of cap free
Amount of items: 2
Items: 
Size: 620552 Color: 14
Size: 379343 Color: 5

Bin 2733: 106 of cap free
Amount of items: 2
Items: 
Size: 643378 Color: 14
Size: 356517 Color: 13

Bin 2734: 106 of cap free
Amount of items: 2
Items: 
Size: 699370 Color: 3
Size: 300525 Color: 14

Bin 2735: 106 of cap free
Amount of items: 3
Items: 
Size: 717056 Color: 0
Size: 161515 Color: 13
Size: 121324 Color: 5

Bin 2736: 106 of cap free
Amount of items: 2
Items: 
Size: 759540 Color: 17
Size: 240355 Color: 19

Bin 2737: 107 of cap free
Amount of items: 2
Items: 
Size: 518365 Color: 18
Size: 481529 Color: 16

Bin 2738: 107 of cap free
Amount of items: 2
Items: 
Size: 614634 Color: 14
Size: 385260 Color: 3

Bin 2739: 107 of cap free
Amount of items: 2
Items: 
Size: 634940 Color: 9
Size: 364954 Color: 15

Bin 2740: 107 of cap free
Amount of items: 2
Items: 
Size: 676892 Color: 12
Size: 323002 Color: 14

Bin 2741: 107 of cap free
Amount of items: 2
Items: 
Size: 692995 Color: 9
Size: 306899 Color: 8

Bin 2742: 107 of cap free
Amount of items: 2
Items: 
Size: 759962 Color: 14
Size: 239932 Color: 16

Bin 2743: 107 of cap free
Amount of items: 2
Items: 
Size: 780226 Color: 10
Size: 219668 Color: 7

Bin 2744: 107 of cap free
Amount of items: 2
Items: 
Size: 785075 Color: 18
Size: 214819 Color: 5

Bin 2745: 108 of cap free
Amount of items: 2
Items: 
Size: 509647 Color: 2
Size: 490246 Color: 8

Bin 2746: 108 of cap free
Amount of items: 2
Items: 
Size: 561358 Color: 15
Size: 438535 Color: 5

Bin 2747: 108 of cap free
Amount of items: 2
Items: 
Size: 587193 Color: 8
Size: 412700 Color: 11

Bin 2748: 108 of cap free
Amount of items: 2
Items: 
Size: 604419 Color: 9
Size: 395474 Color: 7

Bin 2749: 108 of cap free
Amount of items: 2
Items: 
Size: 608067 Color: 17
Size: 391826 Color: 4

Bin 2750: 108 of cap free
Amount of items: 2
Items: 
Size: 702131 Color: 5
Size: 297762 Color: 7

Bin 2751: 108 of cap free
Amount of items: 2
Items: 
Size: 704903 Color: 15
Size: 294990 Color: 7

Bin 2752: 108 of cap free
Amount of items: 2
Items: 
Size: 724230 Color: 6
Size: 275663 Color: 10

Bin 2753: 108 of cap free
Amount of items: 2
Items: 
Size: 775239 Color: 15
Size: 224654 Color: 2

Bin 2754: 108 of cap free
Amount of items: 2
Items: 
Size: 783672 Color: 12
Size: 216221 Color: 8

Bin 2755: 109 of cap free
Amount of items: 2
Items: 
Size: 606386 Color: 14
Size: 393506 Color: 15

Bin 2756: 109 of cap free
Amount of items: 3
Items: 
Size: 617505 Color: 18
Size: 217030 Color: 4
Size: 165357 Color: 8

Bin 2757: 109 of cap free
Amount of items: 2
Items: 
Size: 655582 Color: 18
Size: 344310 Color: 4

Bin 2758: 109 of cap free
Amount of items: 2
Items: 
Size: 706377 Color: 4
Size: 293515 Color: 6

Bin 2759: 109 of cap free
Amount of items: 2
Items: 
Size: 750688 Color: 7
Size: 249204 Color: 19

Bin 2760: 110 of cap free
Amount of items: 2
Items: 
Size: 583550 Color: 14
Size: 416341 Color: 6

Bin 2761: 110 of cap free
Amount of items: 2
Items: 
Size: 595799 Color: 17
Size: 404092 Color: 5

Bin 2762: 110 of cap free
Amount of items: 3
Items: 
Size: 622158 Color: 1
Size: 219624 Color: 2
Size: 158109 Color: 9

Bin 2763: 110 of cap free
Amount of items: 2
Items: 
Size: 664265 Color: 10
Size: 335626 Color: 8

Bin 2764: 110 of cap free
Amount of items: 2
Items: 
Size: 694699 Color: 15
Size: 305192 Color: 8

Bin 2765: 110 of cap free
Amount of items: 2
Items: 
Size: 708232 Color: 0
Size: 291659 Color: 9

Bin 2766: 110 of cap free
Amount of items: 2
Items: 
Size: 721971 Color: 15
Size: 277920 Color: 3

Bin 2767: 110 of cap free
Amount of items: 2
Items: 
Size: 752021 Color: 17
Size: 247870 Color: 15

Bin 2768: 111 of cap free
Amount of items: 3
Items: 
Size: 358416 Color: 16
Size: 337052 Color: 18
Size: 304422 Color: 8

Bin 2769: 111 of cap free
Amount of items: 2
Items: 
Size: 573277 Color: 16
Size: 426613 Color: 4

Bin 2770: 111 of cap free
Amount of items: 2
Items: 
Size: 581785 Color: 14
Size: 418105 Color: 12

Bin 2771: 111 of cap free
Amount of items: 2
Items: 
Size: 610560 Color: 18
Size: 389330 Color: 10

Bin 2772: 111 of cap free
Amount of items: 2
Items: 
Size: 663067 Color: 10
Size: 336823 Color: 1

Bin 2773: 111 of cap free
Amount of items: 2
Items: 
Size: 789449 Color: 3
Size: 210441 Color: 9

Bin 2774: 111 of cap free
Amount of items: 2
Items: 
Size: 793590 Color: 8
Size: 206300 Color: 2

Bin 2775: 112 of cap free
Amount of items: 2
Items: 
Size: 560318 Color: 1
Size: 439571 Color: 4

Bin 2776: 112 of cap free
Amount of items: 2
Items: 
Size: 563733 Color: 6
Size: 436156 Color: 13

Bin 2777: 112 of cap free
Amount of items: 2
Items: 
Size: 586189 Color: 9
Size: 413700 Color: 10

Bin 2778: 112 of cap free
Amount of items: 2
Items: 
Size: 683765 Color: 13
Size: 316124 Color: 14

Bin 2779: 112 of cap free
Amount of items: 2
Items: 
Size: 736246 Color: 14
Size: 263643 Color: 10

Bin 2780: 112 of cap free
Amount of items: 2
Items: 
Size: 746810 Color: 14
Size: 253079 Color: 17

Bin 2781: 112 of cap free
Amount of items: 2
Items: 
Size: 769416 Color: 2
Size: 230473 Color: 11

Bin 2782: 112 of cap free
Amount of items: 2
Items: 
Size: 786213 Color: 12
Size: 213676 Color: 19

Bin 2783: 112 of cap free
Amount of items: 2
Items: 
Size: 790959 Color: 5
Size: 208930 Color: 4

Bin 2784: 113 of cap free
Amount of items: 3
Items: 
Size: 369849 Color: 10
Size: 360147 Color: 11
Size: 269892 Color: 9

Bin 2785: 113 of cap free
Amount of items: 2
Items: 
Size: 611627 Color: 5
Size: 388261 Color: 17

Bin 2786: 113 of cap free
Amount of items: 2
Items: 
Size: 675600 Color: 17
Size: 324288 Color: 11

Bin 2787: 113 of cap free
Amount of items: 2
Items: 
Size: 711719 Color: 9
Size: 288169 Color: 18

Bin 2788: 113 of cap free
Amount of items: 2
Items: 
Size: 753184 Color: 3
Size: 246704 Color: 13

Bin 2789: 114 of cap free
Amount of items: 2
Items: 
Size: 521320 Color: 18
Size: 478567 Color: 13

Bin 2790: 114 of cap free
Amount of items: 2
Items: 
Size: 540953 Color: 17
Size: 458934 Color: 16

Bin 2791: 114 of cap free
Amount of items: 2
Items: 
Size: 680174 Color: 11
Size: 319713 Color: 14

Bin 2792: 114 of cap free
Amount of items: 2
Items: 
Size: 700664 Color: 1
Size: 299223 Color: 3

Bin 2793: 114 of cap free
Amount of items: 2
Items: 
Size: 740658 Color: 0
Size: 259229 Color: 10

Bin 2794: 114 of cap free
Amount of items: 2
Items: 
Size: 797606 Color: 14
Size: 202281 Color: 2

Bin 2795: 115 of cap free
Amount of items: 2
Items: 
Size: 529744 Color: 8
Size: 470142 Color: 0

Bin 2796: 115 of cap free
Amount of items: 2
Items: 
Size: 533164 Color: 6
Size: 466722 Color: 14

Bin 2797: 115 of cap free
Amount of items: 3
Items: 
Size: 640643 Color: 16
Size: 196264 Color: 2
Size: 162979 Color: 17

Bin 2798: 115 of cap free
Amount of items: 2
Items: 
Size: 650153 Color: 17
Size: 349733 Color: 6

Bin 2799: 115 of cap free
Amount of items: 2
Items: 
Size: 687365 Color: 0
Size: 312521 Color: 2

Bin 2800: 116 of cap free
Amount of items: 3
Items: 
Size: 604597 Color: 2
Size: 223240 Color: 6
Size: 172048 Color: 9

Bin 2801: 116 of cap free
Amount of items: 2
Items: 
Size: 613459 Color: 15
Size: 386426 Color: 0

Bin 2802: 116 of cap free
Amount of items: 2
Items: 
Size: 640604 Color: 14
Size: 359281 Color: 8

Bin 2803: 116 of cap free
Amount of items: 2
Items: 
Size: 717805 Color: 12
Size: 282080 Color: 14

Bin 2804: 116 of cap free
Amount of items: 3
Items: 
Size: 756080 Color: 13
Size: 125374 Color: 13
Size: 118431 Color: 0

Bin 2805: 117 of cap free
Amount of items: 3
Items: 
Size: 431149 Color: 8
Size: 285075 Color: 14
Size: 283660 Color: 10

Bin 2806: 117 of cap free
Amount of items: 3
Items: 
Size: 577155 Color: 13
Size: 226392 Color: 19
Size: 196337 Color: 9

Bin 2807: 117 of cap free
Amount of items: 3
Items: 
Size: 577975 Color: 0
Size: 225942 Color: 2
Size: 195967 Color: 9

Bin 2808: 117 of cap free
Amount of items: 2
Items: 
Size: 642987 Color: 9
Size: 356897 Color: 19

Bin 2809: 117 of cap free
Amount of items: 2
Items: 
Size: 773749 Color: 15
Size: 226135 Color: 1

Bin 2810: 118 of cap free
Amount of items: 2
Items: 
Size: 548904 Color: 5
Size: 450979 Color: 19

Bin 2811: 118 of cap free
Amount of items: 2
Items: 
Size: 560520 Color: 11
Size: 439363 Color: 19

Bin 2812: 118 of cap free
Amount of items: 2
Items: 
Size: 620251 Color: 14
Size: 379632 Color: 1

Bin 2813: 118 of cap free
Amount of items: 2
Items: 
Size: 768918 Color: 19
Size: 230965 Color: 16

Bin 2814: 119 of cap free
Amount of items: 2
Items: 
Size: 533826 Color: 8
Size: 466056 Color: 15

Bin 2815: 119 of cap free
Amount of items: 2
Items: 
Size: 552781 Color: 7
Size: 447101 Color: 13

Bin 2816: 119 of cap free
Amount of items: 2
Items: 
Size: 564720 Color: 15
Size: 435162 Color: 6

Bin 2817: 119 of cap free
Amount of items: 2
Items: 
Size: 578168 Color: 19
Size: 421714 Color: 4

Bin 2818: 119 of cap free
Amount of items: 2
Items: 
Size: 694434 Color: 2
Size: 305448 Color: 16

Bin 2819: 119 of cap free
Amount of items: 2
Items: 
Size: 746223 Color: 1
Size: 253659 Color: 4

Bin 2820: 119 of cap free
Amount of items: 2
Items: 
Size: 747432 Color: 5
Size: 252450 Color: 8

Bin 2821: 119 of cap free
Amount of items: 3
Items: 
Size: 776962 Color: 16
Size: 113795 Color: 13
Size: 109125 Color: 12

Bin 2822: 120 of cap free
Amount of items: 2
Items: 
Size: 512527 Color: 11
Size: 487354 Color: 1

Bin 2823: 120 of cap free
Amount of items: 2
Items: 
Size: 532952 Color: 14
Size: 466929 Color: 11

Bin 2824: 120 of cap free
Amount of items: 2
Items: 
Size: 639463 Color: 13
Size: 360418 Color: 2

Bin 2825: 120 of cap free
Amount of items: 2
Items: 
Size: 776392 Color: 9
Size: 223489 Color: 1

Bin 2826: 120 of cap free
Amount of items: 2
Items: 
Size: 790887 Color: 3
Size: 208994 Color: 5

Bin 2827: 121 of cap free
Amount of items: 2
Items: 
Size: 502755 Color: 0
Size: 497125 Color: 6

Bin 2828: 121 of cap free
Amount of items: 2
Items: 
Size: 504810 Color: 9
Size: 495070 Color: 2

Bin 2829: 121 of cap free
Amount of items: 3
Items: 
Size: 599632 Color: 12
Size: 205684 Color: 9
Size: 194564 Color: 12

Bin 2830: 121 of cap free
Amount of items: 2
Items: 
Size: 666575 Color: 14
Size: 333305 Color: 4

Bin 2831: 121 of cap free
Amount of items: 2
Items: 
Size: 699491 Color: 19
Size: 300389 Color: 12

Bin 2832: 121 of cap free
Amount of items: 2
Items: 
Size: 708600 Color: 18
Size: 291280 Color: 19

Bin 2833: 121 of cap free
Amount of items: 2
Items: 
Size: 763801 Color: 6
Size: 236079 Color: 0

Bin 2834: 122 of cap free
Amount of items: 2
Items: 
Size: 521581 Color: 5
Size: 478298 Color: 13

Bin 2835: 122 of cap free
Amount of items: 2
Items: 
Size: 527810 Color: 2
Size: 472069 Color: 3

Bin 2836: 122 of cap free
Amount of items: 2
Items: 
Size: 548609 Color: 2
Size: 451270 Color: 14

Bin 2837: 122 of cap free
Amount of items: 2
Items: 
Size: 620381 Color: 8
Size: 379498 Color: 7

Bin 2838: 122 of cap free
Amount of items: 2
Items: 
Size: 640730 Color: 14
Size: 359149 Color: 9

Bin 2839: 122 of cap free
Amount of items: 2
Items: 
Size: 677665 Color: 6
Size: 322214 Color: 16

Bin 2840: 122 of cap free
Amount of items: 3
Items: 
Size: 725923 Color: 12
Size: 152601 Color: 8
Size: 121355 Color: 9

Bin 2841: 122 of cap free
Amount of items: 2
Items: 
Size: 799993 Color: 1
Size: 199886 Color: 12

Bin 2842: 123 of cap free
Amount of items: 2
Items: 
Size: 569402 Color: 8
Size: 430476 Color: 5

Bin 2843: 123 of cap free
Amount of items: 2
Items: 
Size: 598185 Color: 13
Size: 401693 Color: 17

Bin 2844: 123 of cap free
Amount of items: 2
Items: 
Size: 638892 Color: 10
Size: 360986 Color: 12

Bin 2845: 123 of cap free
Amount of items: 2
Items: 
Size: 688449 Color: 11
Size: 311429 Color: 15

Bin 2846: 123 of cap free
Amount of items: 2
Items: 
Size: 703763 Color: 6
Size: 296115 Color: 1

Bin 2847: 123 of cap free
Amount of items: 2
Items: 
Size: 732668 Color: 1
Size: 267210 Color: 17

Bin 2848: 124 of cap free
Amount of items: 3
Items: 
Size: 361871 Color: 1
Size: 328031 Color: 19
Size: 309975 Color: 7

Bin 2849: 124 of cap free
Amount of items: 2
Items: 
Size: 505627 Color: 5
Size: 494250 Color: 0

Bin 2850: 124 of cap free
Amount of items: 2
Items: 
Size: 573070 Color: 16
Size: 426807 Color: 19

Bin 2851: 124 of cap free
Amount of items: 2
Items: 
Size: 631438 Color: 16
Size: 368439 Color: 8

Bin 2852: 124 of cap free
Amount of items: 2
Items: 
Size: 787764 Color: 11
Size: 212113 Color: 7

Bin 2853: 125 of cap free
Amount of items: 2
Items: 
Size: 517703 Color: 15
Size: 482173 Color: 11

Bin 2854: 125 of cap free
Amount of items: 2
Items: 
Size: 522044 Color: 2
Size: 477832 Color: 4

Bin 2855: 125 of cap free
Amount of items: 2
Items: 
Size: 563875 Color: 13
Size: 436001 Color: 15

Bin 2856: 125 of cap free
Amount of items: 2
Items: 
Size: 610010 Color: 2
Size: 389866 Color: 6

Bin 2857: 125 of cap free
Amount of items: 2
Items: 
Size: 653551 Color: 15
Size: 346325 Color: 9

Bin 2858: 125 of cap free
Amount of items: 2
Items: 
Size: 654728 Color: 8
Size: 345148 Color: 7

Bin 2859: 125 of cap free
Amount of items: 2
Items: 
Size: 680695 Color: 2
Size: 319181 Color: 0

Bin 2860: 125 of cap free
Amount of items: 2
Items: 
Size: 713819 Color: 6
Size: 286057 Color: 4

Bin 2861: 125 of cap free
Amount of items: 3
Items: 
Size: 722966 Color: 0
Size: 145931 Color: 13
Size: 130979 Color: 3

Bin 2862: 125 of cap free
Amount of items: 2
Items: 
Size: 726079 Color: 4
Size: 273797 Color: 15

Bin 2863: 125 of cap free
Amount of items: 2
Items: 
Size: 799277 Color: 13
Size: 200599 Color: 0

Bin 2864: 126 of cap free
Amount of items: 3
Items: 
Size: 529429 Color: 12
Size: 251985 Color: 17
Size: 218461 Color: 9

Bin 2865: 126 of cap free
Amount of items: 2
Items: 
Size: 553013 Color: 2
Size: 446862 Color: 3

Bin 2866: 126 of cap free
Amount of items: 2
Items: 
Size: 586903 Color: 9
Size: 412972 Color: 17

Bin 2867: 126 of cap free
Amount of items: 2
Items: 
Size: 598107 Color: 6
Size: 401768 Color: 3

Bin 2868: 126 of cap free
Amount of items: 2
Items: 
Size: 710296 Color: 13
Size: 289579 Color: 15

Bin 2869: 126 of cap free
Amount of items: 2
Items: 
Size: 798816 Color: 6
Size: 201059 Color: 19

Bin 2870: 127 of cap free
Amount of items: 2
Items: 
Size: 520212 Color: 7
Size: 479662 Color: 2

Bin 2871: 127 of cap free
Amount of items: 2
Items: 
Size: 547139 Color: 17
Size: 452735 Color: 6

Bin 2872: 127 of cap free
Amount of items: 2
Items: 
Size: 580626 Color: 0
Size: 419248 Color: 17

Bin 2873: 127 of cap free
Amount of items: 2
Items: 
Size: 593698 Color: 10
Size: 406176 Color: 16

Bin 2874: 127 of cap free
Amount of items: 2
Items: 
Size: 609119 Color: 12
Size: 390755 Color: 19

Bin 2875: 127 of cap free
Amount of items: 2
Items: 
Size: 689720 Color: 19
Size: 310154 Color: 4

Bin 2876: 127 of cap free
Amount of items: 2
Items: 
Size: 789686 Color: 2
Size: 210188 Color: 19

Bin 2877: 128 of cap free
Amount of items: 2
Items: 
Size: 501515 Color: 4
Size: 498358 Color: 11

Bin 2878: 128 of cap free
Amount of items: 2
Items: 
Size: 517524 Color: 5
Size: 482349 Color: 15

Bin 2879: 128 of cap free
Amount of items: 3
Items: 
Size: 531770 Color: 19
Size: 249520 Color: 9
Size: 218583 Color: 17

Bin 2880: 128 of cap free
Amount of items: 2
Items: 
Size: 536468 Color: 16
Size: 463405 Color: 10

Bin 2881: 128 of cap free
Amount of items: 2
Items: 
Size: 540819 Color: 3
Size: 459054 Color: 14

Bin 2882: 128 of cap free
Amount of items: 3
Items: 
Size: 618097 Color: 9
Size: 205473 Color: 17
Size: 176303 Color: 3

Bin 2883: 128 of cap free
Amount of items: 2
Items: 
Size: 700967 Color: 5
Size: 298906 Color: 9

Bin 2884: 128 of cap free
Amount of items: 2
Items: 
Size: 772629 Color: 9
Size: 227244 Color: 19

Bin 2885: 128 of cap free
Amount of items: 2
Items: 
Size: 776528 Color: 4
Size: 223345 Color: 3

Bin 2886: 128 of cap free
Amount of items: 3
Items: 
Size: 789315 Color: 11
Size: 105290 Color: 17
Size: 105268 Color: 2

Bin 2887: 129 of cap free
Amount of items: 2
Items: 
Size: 516919 Color: 1
Size: 482953 Color: 0

Bin 2888: 129 of cap free
Amount of items: 2
Items: 
Size: 533439 Color: 6
Size: 466433 Color: 16

Bin 2889: 129 of cap free
Amount of items: 2
Items: 
Size: 550670 Color: 5
Size: 449202 Color: 10

Bin 2890: 129 of cap free
Amount of items: 2
Items: 
Size: 555087 Color: 7
Size: 444785 Color: 12

Bin 2891: 129 of cap free
Amount of items: 2
Items: 
Size: 557494 Color: 12
Size: 442378 Color: 9

Bin 2892: 129 of cap free
Amount of items: 2
Items: 
Size: 610132 Color: 14
Size: 389740 Color: 18

Bin 2893: 129 of cap free
Amount of items: 2
Items: 
Size: 756021 Color: 19
Size: 243851 Color: 5

Bin 2894: 130 of cap free
Amount of items: 2
Items: 
Size: 520032 Color: 19
Size: 479839 Color: 4

Bin 2895: 130 of cap free
Amount of items: 2
Items: 
Size: 559922 Color: 3
Size: 439949 Color: 10

Bin 2896: 130 of cap free
Amount of items: 2
Items: 
Size: 576585 Color: 14
Size: 423286 Color: 16

Bin 2897: 130 of cap free
Amount of items: 2
Items: 
Size: 639636 Color: 16
Size: 360235 Color: 9

Bin 2898: 130 of cap free
Amount of items: 2
Items: 
Size: 642670 Color: 5
Size: 357201 Color: 10

Bin 2899: 130 of cap free
Amount of items: 2
Items: 
Size: 681817 Color: 15
Size: 318054 Color: 2

Bin 2900: 130 of cap free
Amount of items: 2
Items: 
Size: 754485 Color: 10
Size: 245386 Color: 5

Bin 2901: 131 of cap free
Amount of items: 2
Items: 
Size: 530581 Color: 3
Size: 469289 Color: 1

Bin 2902: 131 of cap free
Amount of items: 2
Items: 
Size: 553633 Color: 5
Size: 446237 Color: 13

Bin 2903: 131 of cap free
Amount of items: 2
Items: 
Size: 580378 Color: 12
Size: 419492 Color: 4

Bin 2904: 131 of cap free
Amount of items: 2
Items: 
Size: 581296 Color: 9
Size: 418574 Color: 14

Bin 2905: 131 of cap free
Amount of items: 2
Items: 
Size: 625054 Color: 13
Size: 374816 Color: 11

Bin 2906: 132 of cap free
Amount of items: 2
Items: 
Size: 550668 Color: 19
Size: 449201 Color: 9

Bin 2907: 132 of cap free
Amount of items: 2
Items: 
Size: 620248 Color: 16
Size: 379621 Color: 5

Bin 2908: 132 of cap free
Amount of items: 2
Items: 
Size: 754268 Color: 11
Size: 245601 Color: 14

Bin 2909: 133 of cap free
Amount of items: 2
Items: 
Size: 528137 Color: 8
Size: 471731 Color: 1

Bin 2910: 133 of cap free
Amount of items: 2
Items: 
Size: 528650 Color: 17
Size: 471218 Color: 2

Bin 2911: 133 of cap free
Amount of items: 2
Items: 
Size: 532164 Color: 14
Size: 467704 Color: 18

Bin 2912: 133 of cap free
Amount of items: 2
Items: 
Size: 584229 Color: 13
Size: 415639 Color: 9

Bin 2913: 133 of cap free
Amount of items: 2
Items: 
Size: 616869 Color: 13
Size: 382999 Color: 6

Bin 2914: 133 of cap free
Amount of items: 2
Items: 
Size: 727866 Color: 16
Size: 272002 Color: 2

Bin 2915: 133 of cap free
Amount of items: 2
Items: 
Size: 787562 Color: 10
Size: 212306 Color: 4

Bin 2916: 134 of cap free
Amount of items: 2
Items: 
Size: 550836 Color: 7
Size: 449031 Color: 0

Bin 2917: 134 of cap free
Amount of items: 2
Items: 
Size: 594236 Color: 17
Size: 405631 Color: 10

Bin 2918: 134 of cap free
Amount of items: 2
Items: 
Size: 761481 Color: 1
Size: 238386 Color: 5

Bin 2919: 134 of cap free
Amount of items: 2
Items: 
Size: 769277 Color: 15
Size: 230590 Color: 12

Bin 2920: 134 of cap free
Amount of items: 2
Items: 
Size: 792599 Color: 17
Size: 207268 Color: 0

Bin 2921: 135 of cap free
Amount of items: 2
Items: 
Size: 546934 Color: 12
Size: 452932 Color: 16

Bin 2922: 135 of cap free
Amount of items: 2
Items: 
Size: 582484 Color: 11
Size: 417382 Color: 7

Bin 2923: 135 of cap free
Amount of items: 2
Items: 
Size: 627561 Color: 19
Size: 372305 Color: 4

Bin 2924: 135 of cap free
Amount of items: 2
Items: 
Size: 653027 Color: 17
Size: 346839 Color: 11

Bin 2925: 135 of cap free
Amount of items: 2
Items: 
Size: 672257 Color: 14
Size: 327609 Color: 2

Bin 2926: 135 of cap free
Amount of items: 2
Items: 
Size: 758327 Color: 0
Size: 241539 Color: 19

Bin 2927: 135 of cap free
Amount of items: 2
Items: 
Size: 780217 Color: 3
Size: 219649 Color: 16

Bin 2928: 135 of cap free
Amount of items: 2
Items: 
Size: 788765 Color: 3
Size: 211101 Color: 14

Bin 2929: 136 of cap free
Amount of items: 2
Items: 
Size: 510547 Color: 7
Size: 489318 Color: 18

Bin 2930: 136 of cap free
Amount of items: 2
Items: 
Size: 523449 Color: 13
Size: 476416 Color: 6

Bin 2931: 136 of cap free
Amount of items: 2
Items: 
Size: 589109 Color: 2
Size: 410756 Color: 6

Bin 2932: 136 of cap free
Amount of items: 2
Items: 
Size: 626092 Color: 19
Size: 373773 Color: 11

Bin 2933: 136 of cap free
Amount of items: 2
Items: 
Size: 761669 Color: 15
Size: 238196 Color: 1

Bin 2934: 137 of cap free
Amount of items: 2
Items: 
Size: 501512 Color: 19
Size: 498352 Color: 0

Bin 2935: 137 of cap free
Amount of items: 2
Items: 
Size: 569397 Color: 18
Size: 430467 Color: 3

Bin 2936: 137 of cap free
Amount of items: 2
Items: 
Size: 626824 Color: 2
Size: 373040 Color: 10

Bin 2937: 137 of cap free
Amount of items: 2
Items: 
Size: 632321 Color: 15
Size: 367543 Color: 8

Bin 2938: 137 of cap free
Amount of items: 2
Items: 
Size: 723667 Color: 8
Size: 276197 Color: 10

Bin 2939: 138 of cap free
Amount of items: 2
Items: 
Size: 573061 Color: 4
Size: 426802 Color: 18

Bin 2940: 138 of cap free
Amount of items: 2
Items: 
Size: 579265 Color: 5
Size: 420598 Color: 2

Bin 2941: 138 of cap free
Amount of items: 2
Items: 
Size: 592196 Color: 0
Size: 407667 Color: 19

Bin 2942: 138 of cap free
Amount of items: 2
Items: 
Size: 600784 Color: 13
Size: 399079 Color: 5

Bin 2943: 138 of cap free
Amount of items: 2
Items: 
Size: 766350 Color: 16
Size: 233513 Color: 6

Bin 2944: 139 of cap free
Amount of items: 2
Items: 
Size: 546930 Color: 14
Size: 452932 Color: 11

Bin 2945: 139 of cap free
Amount of items: 2
Items: 
Size: 613672 Color: 3
Size: 386190 Color: 13

Bin 2946: 139 of cap free
Amount of items: 2
Items: 
Size: 645405 Color: 7
Size: 354457 Color: 6

Bin 2947: 139 of cap free
Amount of items: 2
Items: 
Size: 675462 Color: 13
Size: 324400 Color: 3

Bin 2948: 139 of cap free
Amount of items: 3
Items: 
Size: 720405 Color: 17
Size: 156936 Color: 8
Size: 122521 Color: 2

Bin 2949: 139 of cap free
Amount of items: 2
Items: 
Size: 725020 Color: 18
Size: 274842 Color: 15

Bin 2950: 140 of cap free
Amount of items: 2
Items: 
Size: 525604 Color: 1
Size: 474257 Color: 15

Bin 2951: 140 of cap free
Amount of items: 2
Items: 
Size: 647124 Color: 7
Size: 352737 Color: 13

Bin 2952: 140 of cap free
Amount of items: 2
Items: 
Size: 667842 Color: 2
Size: 332019 Color: 16

Bin 2953: 140 of cap free
Amount of items: 2
Items: 
Size: 681079 Color: 8
Size: 318782 Color: 16

Bin 2954: 140 of cap free
Amount of items: 2
Items: 
Size: 683563 Color: 6
Size: 316298 Color: 18

Bin 2955: 140 of cap free
Amount of items: 2
Items: 
Size: 713893 Color: 4
Size: 285968 Color: 19

Bin 2956: 141 of cap free
Amount of items: 3
Items: 
Size: 409332 Color: 9
Size: 341475 Color: 3
Size: 249053 Color: 18

Bin 2957: 141 of cap free
Amount of items: 2
Items: 
Size: 586401 Color: 18
Size: 413459 Color: 7

Bin 2958: 141 of cap free
Amount of items: 2
Items: 
Size: 633843 Color: 14
Size: 366017 Color: 18

Bin 2959: 141 of cap free
Amount of items: 2
Items: 
Size: 726811 Color: 9
Size: 273049 Color: 19

Bin 2960: 141 of cap free
Amount of items: 2
Items: 
Size: 730726 Color: 13
Size: 269134 Color: 5

Bin 2961: 142 of cap free
Amount of items: 2
Items: 
Size: 642352 Color: 8
Size: 357507 Color: 10

Bin 2962: 142 of cap free
Amount of items: 2
Items: 
Size: 649241 Color: 2
Size: 350618 Color: 4

Bin 2963: 143 of cap free
Amount of items: 2
Items: 
Size: 536573 Color: 2
Size: 463285 Color: 3

Bin 2964: 143 of cap free
Amount of items: 2
Items: 
Size: 769893 Color: 18
Size: 229965 Color: 0

Bin 2965: 144 of cap free
Amount of items: 3
Items: 
Size: 523114 Color: 17
Size: 256198 Color: 4
Size: 220545 Color: 1

Bin 2966: 144 of cap free
Amount of items: 2
Items: 
Size: 557670 Color: 11
Size: 442187 Color: 0

Bin 2967: 144 of cap free
Amount of items: 2
Items: 
Size: 565245 Color: 18
Size: 434612 Color: 6

Bin 2968: 144 of cap free
Amount of items: 2
Items: 
Size: 588606 Color: 4
Size: 411251 Color: 2

Bin 2969: 144 of cap free
Amount of items: 3
Items: 
Size: 727542 Color: 11
Size: 152965 Color: 13
Size: 119350 Color: 1

Bin 2970: 144 of cap free
Amount of items: 2
Items: 
Size: 753108 Color: 4
Size: 246749 Color: 1

Bin 2971: 144 of cap free
Amount of items: 2
Items: 
Size: 762665 Color: 9
Size: 237192 Color: 6

Bin 2972: 144 of cap free
Amount of items: 2
Items: 
Size: 793063 Color: 18
Size: 206794 Color: 3

Bin 2973: 145 of cap free
Amount of items: 2
Items: 
Size: 502895 Color: 9
Size: 496961 Color: 14

Bin 2974: 145 of cap free
Amount of items: 2
Items: 
Size: 569904 Color: 10
Size: 429952 Color: 2

Bin 2975: 145 of cap free
Amount of items: 2
Items: 
Size: 678277 Color: 3
Size: 321579 Color: 11

Bin 2976: 145 of cap free
Amount of items: 2
Items: 
Size: 694830 Color: 7
Size: 305026 Color: 8

Bin 2977: 145 of cap free
Amount of items: 2
Items: 
Size: 797098 Color: 5
Size: 202758 Color: 9

Bin 2978: 146 of cap free
Amount of items: 2
Items: 
Size: 517708 Color: 9
Size: 482147 Color: 17

Bin 2979: 146 of cap free
Amount of items: 2
Items: 
Size: 531538 Color: 15
Size: 468317 Color: 17

Bin 2980: 146 of cap free
Amount of items: 2
Items: 
Size: 533536 Color: 3
Size: 466319 Color: 17

Bin 2981: 146 of cap free
Amount of items: 2
Items: 
Size: 622484 Color: 3
Size: 377371 Color: 15

Bin 2982: 146 of cap free
Amount of items: 2
Items: 
Size: 694185 Color: 9
Size: 305670 Color: 13

Bin 2983: 146 of cap free
Amount of items: 2
Items: 
Size: 719753 Color: 2
Size: 280102 Color: 17

Bin 2984: 146 of cap free
Amount of items: 3
Items: 
Size: 723416 Color: 17
Size: 145086 Color: 13
Size: 131353 Color: 6

Bin 2985: 147 of cap free
Amount of items: 2
Items: 
Size: 505138 Color: 1
Size: 494716 Color: 4

Bin 2986: 147 of cap free
Amount of items: 2
Items: 
Size: 565807 Color: 19
Size: 434047 Color: 5

Bin 2987: 147 of cap free
Amount of items: 2
Items: 
Size: 567629 Color: 14
Size: 432225 Color: 0

Bin 2988: 147 of cap free
Amount of items: 2
Items: 
Size: 568052 Color: 17
Size: 431802 Color: 3

Bin 2989: 147 of cap free
Amount of items: 2
Items: 
Size: 576489 Color: 1
Size: 423365 Color: 2

Bin 2990: 147 of cap free
Amount of items: 2
Items: 
Size: 590802 Color: 7
Size: 409052 Color: 6

Bin 2991: 147 of cap free
Amount of items: 2
Items: 
Size: 645692 Color: 14
Size: 354162 Color: 17

Bin 2992: 147 of cap free
Amount of items: 2
Items: 
Size: 678474 Color: 10
Size: 321380 Color: 7

Bin 2993: 147 of cap free
Amount of items: 2
Items: 
Size: 735148 Color: 5
Size: 264706 Color: 13

Bin 2994: 147 of cap free
Amount of items: 2
Items: 
Size: 786534 Color: 15
Size: 213320 Color: 9

Bin 2995: 148 of cap free
Amount of items: 3
Items: 
Size: 359007 Color: 1
Size: 354309 Color: 2
Size: 286537 Color: 11

Bin 2996: 148 of cap free
Amount of items: 2
Items: 
Size: 554963 Color: 2
Size: 444890 Color: 8

Bin 2997: 148 of cap free
Amount of items: 2
Items: 
Size: 589104 Color: 9
Size: 410749 Color: 0

Bin 2998: 148 of cap free
Amount of items: 2
Items: 
Size: 604729 Color: 3
Size: 395124 Color: 15

Bin 2999: 149 of cap free
Amount of items: 2
Items: 
Size: 509742 Color: 1
Size: 490110 Color: 12

Bin 3000: 149 of cap free
Amount of items: 2
Items: 
Size: 559044 Color: 14
Size: 440808 Color: 15

Bin 3001: 149 of cap free
Amount of items: 2
Items: 
Size: 574030 Color: 4
Size: 425822 Color: 11

Bin 3002: 149 of cap free
Amount of items: 2
Items: 
Size: 591001 Color: 19
Size: 408851 Color: 8

Bin 3003: 149 of cap free
Amount of items: 2
Items: 
Size: 691083 Color: 12
Size: 308769 Color: 10

Bin 3004: 150 of cap free
Amount of items: 2
Items: 
Size: 506635 Color: 11
Size: 493216 Color: 15

Bin 3005: 150 of cap free
Amount of items: 2
Items: 
Size: 513533 Color: 3
Size: 486318 Color: 6

Bin 3006: 150 of cap free
Amount of items: 2
Items: 
Size: 572821 Color: 5
Size: 427030 Color: 1

Bin 3007: 150 of cap free
Amount of items: 3
Items: 
Size: 627314 Color: 2
Size: 186535 Color: 3
Size: 186002 Color: 6

Bin 3008: 150 of cap free
Amount of items: 2
Items: 
Size: 684500 Color: 13
Size: 315351 Color: 12

Bin 3009: 150 of cap free
Amount of items: 2
Items: 
Size: 756001 Color: 5
Size: 243850 Color: 10

Bin 3010: 150 of cap free
Amount of items: 2
Items: 
Size: 759375 Color: 19
Size: 240476 Color: 14

Bin 3011: 150 of cap free
Amount of items: 2
Items: 
Size: 795158 Color: 12
Size: 204693 Color: 10

Bin 3012: 151 of cap free
Amount of items: 2
Items: 
Size: 569222 Color: 5
Size: 430628 Color: 14

Bin 3013: 151 of cap free
Amount of items: 3
Items: 
Size: 581751 Color: 2
Size: 223100 Color: 16
Size: 194999 Color: 10

Bin 3014: 151 of cap free
Amount of items: 2
Items: 
Size: 585743 Color: 12
Size: 414107 Color: 17

Bin 3015: 151 of cap free
Amount of items: 2
Items: 
Size: 710163 Color: 5
Size: 289687 Color: 8

Bin 3016: 151 of cap free
Amount of items: 2
Items: 
Size: 736944 Color: 18
Size: 262906 Color: 2

Bin 3017: 151 of cap free
Amount of items: 2
Items: 
Size: 767045 Color: 4
Size: 232805 Color: 0

Bin 3018: 151 of cap free
Amount of items: 3
Items: 
Size: 775039 Color: 10
Size: 116613 Color: 13
Size: 108198 Color: 4

Bin 3019: 152 of cap free
Amount of items: 2
Items: 
Size: 617452 Color: 9
Size: 382397 Color: 15

Bin 3020: 152 of cap free
Amount of items: 2
Items: 
Size: 760072 Color: 4
Size: 239777 Color: 3

Bin 3021: 153 of cap free
Amount of items: 2
Items: 
Size: 567290 Color: 18
Size: 432558 Color: 8

Bin 3022: 153 of cap free
Amount of items: 2
Items: 
Size: 584228 Color: 13
Size: 415620 Color: 2

Bin 3023: 154 of cap free
Amount of items: 2
Items: 
Size: 633598 Color: 11
Size: 366249 Color: 5

Bin 3024: 154 of cap free
Amount of items: 2
Items: 
Size: 701711 Color: 4
Size: 298136 Color: 18

Bin 3025: 154 of cap free
Amount of items: 2
Items: 
Size: 745430 Color: 18
Size: 254417 Color: 11

Bin 3026: 155 of cap free
Amount of items: 3
Items: 
Size: 621691 Color: 5
Size: 191509 Color: 7
Size: 186646 Color: 6

Bin 3027: 155 of cap free
Amount of items: 2
Items: 
Size: 622689 Color: 6
Size: 377157 Color: 8

Bin 3028: 155 of cap free
Amount of items: 2
Items: 
Size: 682148 Color: 3
Size: 317698 Color: 1

Bin 3029: 155 of cap free
Amount of items: 2
Items: 
Size: 699904 Color: 18
Size: 299942 Color: 14

Bin 3030: 156 of cap free
Amount of items: 3
Items: 
Size: 662778 Color: 2
Size: 179676 Color: 13
Size: 157391 Color: 15

Bin 3031: 156 of cap free
Amount of items: 3
Items: 
Size: 664038 Color: 8
Size: 179089 Color: 13
Size: 156718 Color: 1

Bin 3032: 156 of cap free
Amount of items: 2
Items: 
Size: 727648 Color: 14
Size: 272197 Color: 7

Bin 3033: 157 of cap free
Amount of items: 2
Items: 
Size: 535982 Color: 5
Size: 463862 Color: 14

Bin 3034: 157 of cap free
Amount of items: 2
Items: 
Size: 573572 Color: 15
Size: 426272 Color: 13

Bin 3035: 158 of cap free
Amount of items: 2
Items: 
Size: 540948 Color: 12
Size: 458895 Color: 14

Bin 3036: 158 of cap free
Amount of items: 2
Items: 
Size: 646562 Color: 14
Size: 353281 Color: 1

Bin 3037: 158 of cap free
Amount of items: 2
Items: 
Size: 773710 Color: 4
Size: 226133 Color: 3

Bin 3038: 159 of cap free
Amount of items: 2
Items: 
Size: 523090 Color: 10
Size: 476752 Color: 12

Bin 3039: 159 of cap free
Amount of items: 2
Items: 
Size: 561771 Color: 5
Size: 438071 Color: 17

Bin 3040: 159 of cap free
Amount of items: 2
Items: 
Size: 684221 Color: 15
Size: 315621 Color: 6

Bin 3041: 159 of cap free
Amount of items: 2
Items: 
Size: 710566 Color: 16
Size: 289276 Color: 17

Bin 3042: 159 of cap free
Amount of items: 2
Items: 
Size: 779181 Color: 7
Size: 220661 Color: 6

Bin 3043: 159 of cap free
Amount of items: 2
Items: 
Size: 799708 Color: 5
Size: 200134 Color: 15

Bin 3044: 160 of cap free
Amount of items: 2
Items: 
Size: 598429 Color: 5
Size: 401412 Color: 9

Bin 3045: 160 of cap free
Amount of items: 2
Items: 
Size: 636513 Color: 3
Size: 363328 Color: 11

Bin 3046: 160 of cap free
Amount of items: 2
Items: 
Size: 652324 Color: 2
Size: 347517 Color: 10

Bin 3047: 160 of cap free
Amount of items: 2
Items: 
Size: 753581 Color: 10
Size: 246260 Color: 4

Bin 3048: 160 of cap free
Amount of items: 2
Items: 
Size: 756368 Color: 9
Size: 243473 Color: 19

Bin 3049: 161 of cap free
Amount of items: 2
Items: 
Size: 624336 Color: 18
Size: 375504 Color: 3

Bin 3050: 161 of cap free
Amount of items: 2
Items: 
Size: 670383 Color: 0
Size: 329457 Color: 15

Bin 3051: 161 of cap free
Amount of items: 2
Items: 
Size: 687046 Color: 7
Size: 312794 Color: 9

Bin 3052: 161 of cap free
Amount of items: 2
Items: 
Size: 750284 Color: 11
Size: 249556 Color: 15

Bin 3053: 161 of cap free
Amount of items: 2
Items: 
Size: 784284 Color: 11
Size: 215556 Color: 4

Bin 3054: 162 of cap free
Amount of items: 2
Items: 
Size: 545483 Color: 12
Size: 454356 Color: 10

Bin 3055: 162 of cap free
Amount of items: 2
Items: 
Size: 550195 Color: 10
Size: 449644 Color: 15

Bin 3056: 162 of cap free
Amount of items: 2
Items: 
Size: 675567 Color: 9
Size: 324272 Color: 15

Bin 3057: 162 of cap free
Amount of items: 2
Items: 
Size: 703056 Color: 4
Size: 296783 Color: 3

Bin 3058: 162 of cap free
Amount of items: 2
Items: 
Size: 706370 Color: 5
Size: 293469 Color: 19

Bin 3059: 162 of cap free
Amount of items: 3
Items: 
Size: 743757 Color: 18
Size: 137365 Color: 13
Size: 118717 Color: 9

Bin 3060: 162 of cap free
Amount of items: 2
Items: 
Size: 797568 Color: 4
Size: 202271 Color: 12

Bin 3061: 163 of cap free
Amount of items: 2
Items: 
Size: 568041 Color: 17
Size: 431797 Color: 5

Bin 3062: 163 of cap free
Amount of items: 2
Items: 
Size: 684600 Color: 11
Size: 315238 Color: 7

Bin 3063: 164 of cap free
Amount of items: 3
Items: 
Size: 521150 Color: 9
Size: 251586 Color: 11
Size: 227101 Color: 11

Bin 3064: 164 of cap free
Amount of items: 2
Items: 
Size: 623396 Color: 15
Size: 376441 Color: 7

Bin 3065: 164 of cap free
Amount of items: 2
Items: 
Size: 634170 Color: 7
Size: 365667 Color: 0

Bin 3066: 164 of cap free
Amount of items: 2
Items: 
Size: 647878 Color: 9
Size: 351959 Color: 12

Bin 3067: 164 of cap free
Amount of items: 2
Items: 
Size: 663459 Color: 18
Size: 336378 Color: 11

Bin 3068: 164 of cap free
Amount of items: 2
Items: 
Size: 673246 Color: 5
Size: 326591 Color: 7

Bin 3069: 164 of cap free
Amount of items: 2
Items: 
Size: 738766 Color: 0
Size: 261071 Color: 14

Bin 3070: 165 of cap free
Amount of items: 2
Items: 
Size: 597498 Color: 5
Size: 402338 Color: 10

Bin 3071: 165 of cap free
Amount of items: 2
Items: 
Size: 646021 Color: 16
Size: 353815 Color: 13

Bin 3072: 165 of cap free
Amount of items: 2
Items: 
Size: 765348 Color: 15
Size: 234488 Color: 6

Bin 3073: 166 of cap free
Amount of items: 2
Items: 
Size: 524401 Color: 9
Size: 475434 Color: 10

Bin 3074: 166 of cap free
Amount of items: 2
Items: 
Size: 538951 Color: 3
Size: 460884 Color: 11

Bin 3075: 166 of cap free
Amount of items: 2
Items: 
Size: 543455 Color: 11
Size: 456380 Color: 0

Bin 3076: 166 of cap free
Amount of items: 2
Items: 
Size: 569215 Color: 10
Size: 430620 Color: 17

Bin 3077: 166 of cap free
Amount of items: 2
Items: 
Size: 608660 Color: 5
Size: 391175 Color: 15

Bin 3078: 166 of cap free
Amount of items: 3
Items: 
Size: 620471 Color: 8
Size: 190073 Color: 1
Size: 189291 Color: 19

Bin 3079: 166 of cap free
Amount of items: 2
Items: 
Size: 677815 Color: 12
Size: 322020 Color: 8

Bin 3080: 166 of cap free
Amount of items: 2
Items: 
Size: 751023 Color: 0
Size: 248812 Color: 13

Bin 3081: 167 of cap free
Amount of items: 3
Items: 
Size: 516235 Color: 10
Size: 254953 Color: 13
Size: 228646 Color: 11

Bin 3082: 167 of cap free
Amount of items: 2
Items: 
Size: 517117 Color: 3
Size: 482717 Color: 16

Bin 3083: 167 of cap free
Amount of items: 2
Items: 
Size: 549892 Color: 12
Size: 449942 Color: 13

Bin 3084: 167 of cap free
Amount of items: 2
Items: 
Size: 550668 Color: 12
Size: 449166 Color: 5

Bin 3085: 167 of cap free
Amount of items: 2
Items: 
Size: 565021 Color: 9
Size: 434813 Color: 14

Bin 3086: 167 of cap free
Amount of items: 2
Items: 
Size: 604713 Color: 9
Size: 395121 Color: 7

Bin 3087: 167 of cap free
Amount of items: 2
Items: 
Size: 685276 Color: 4
Size: 314558 Color: 7

Bin 3088: 167 of cap free
Amount of items: 2
Items: 
Size: 719516 Color: 8
Size: 280318 Color: 17

Bin 3089: 167 of cap free
Amount of items: 2
Items: 
Size: 742960 Color: 19
Size: 256874 Color: 14

Bin 3090: 168 of cap free
Amount of items: 3
Items: 
Size: 350431 Color: 10
Size: 341515 Color: 16
Size: 307887 Color: 8

Bin 3091: 168 of cap free
Amount of items: 2
Items: 
Size: 552319 Color: 0
Size: 447514 Color: 19

Bin 3092: 168 of cap free
Amount of items: 2
Items: 
Size: 574822 Color: 13
Size: 425011 Color: 10

Bin 3093: 168 of cap free
Amount of items: 2
Items: 
Size: 666048 Color: 7
Size: 333785 Color: 14

Bin 3094: 169 of cap free
Amount of items: 2
Items: 
Size: 596377 Color: 12
Size: 403455 Color: 5

Bin 3095: 169 of cap free
Amount of items: 2
Items: 
Size: 602993 Color: 9
Size: 396839 Color: 19

Bin 3096: 169 of cap free
Amount of items: 2
Items: 
Size: 619067 Color: 14
Size: 380765 Color: 13

Bin 3097: 169 of cap free
Amount of items: 2
Items: 
Size: 723118 Color: 5
Size: 276714 Color: 8

Bin 3098: 169 of cap free
Amount of items: 2
Items: 
Size: 742208 Color: 9
Size: 257624 Color: 5

Bin 3099: 169 of cap free
Amount of items: 2
Items: 
Size: 743552 Color: 15
Size: 256280 Color: 16

Bin 3100: 169 of cap free
Amount of items: 3
Items: 
Size: 777292 Color: 4
Size: 114274 Color: 13
Size: 108266 Color: 11

Bin 3101: 169 of cap free
Amount of items: 2
Items: 
Size: 785296 Color: 12
Size: 214536 Color: 10

Bin 3102: 169 of cap free
Amount of items: 2
Items: 
Size: 787732 Color: 16
Size: 212100 Color: 10

Bin 3103: 170 of cap free
Amount of items: 2
Items: 
Size: 596613 Color: 6
Size: 403218 Color: 0

Bin 3104: 170 of cap free
Amount of items: 2
Items: 
Size: 601578 Color: 14
Size: 398253 Color: 13

Bin 3105: 170 of cap free
Amount of items: 2
Items: 
Size: 614455 Color: 0
Size: 385376 Color: 9

Bin 3106: 170 of cap free
Amount of items: 2
Items: 
Size: 622089 Color: 2
Size: 377742 Color: 19

Bin 3107: 170 of cap free
Amount of items: 2
Items: 
Size: 675074 Color: 8
Size: 324757 Color: 14

Bin 3108: 170 of cap free
Amount of items: 2
Items: 
Size: 716986 Color: 2
Size: 282845 Color: 10

Bin 3109: 171 of cap free
Amount of items: 2
Items: 
Size: 532557 Color: 10
Size: 467273 Color: 16

Bin 3110: 171 of cap free
Amount of items: 2
Items: 
Size: 718944 Color: 8
Size: 280886 Color: 4

Bin 3111: 171 of cap free
Amount of items: 2
Items: 
Size: 768660 Color: 14
Size: 231170 Color: 16

Bin 3112: 171 of cap free
Amount of items: 2
Items: 
Size: 781542 Color: 17
Size: 218288 Color: 14

Bin 3113: 172 of cap free
Amount of items: 2
Items: 
Size: 544092 Color: 14
Size: 455737 Color: 7

Bin 3114: 172 of cap free
Amount of items: 2
Items: 
Size: 569869 Color: 0
Size: 429960 Color: 10

Bin 3115: 172 of cap free
Amount of items: 2
Items: 
Size: 720956 Color: 6
Size: 278873 Color: 18

Bin 3116: 173 of cap free
Amount of items: 2
Items: 
Size: 594609 Color: 6
Size: 405219 Color: 16

Bin 3117: 173 of cap free
Amount of items: 2
Items: 
Size: 612147 Color: 16
Size: 387681 Color: 17

Bin 3118: 173 of cap free
Amount of items: 2
Items: 
Size: 657382 Color: 19
Size: 342446 Color: 0

Bin 3119: 173 of cap free
Amount of items: 3
Items: 
Size: 699908 Color: 13
Size: 149979 Color: 4
Size: 149941 Color: 17

Bin 3120: 174 of cap free
Amount of items: 2
Items: 
Size: 515086 Color: 0
Size: 484741 Color: 1

Bin 3121: 174 of cap free
Amount of items: 2
Items: 
Size: 574404 Color: 13
Size: 425423 Color: 10

Bin 3122: 174 of cap free
Amount of items: 2
Items: 
Size: 605869 Color: 4
Size: 393958 Color: 15

Bin 3123: 174 of cap free
Amount of items: 2
Items: 
Size: 664543 Color: 18
Size: 335284 Color: 6

Bin 3124: 174 of cap free
Amount of items: 2
Items: 
Size: 679612 Color: 19
Size: 320215 Color: 13

Bin 3125: 174 of cap free
Amount of items: 2
Items: 
Size: 690080 Color: 9
Size: 309747 Color: 13

Bin 3126: 174 of cap free
Amount of items: 2
Items: 
Size: 701702 Color: 1
Size: 298125 Color: 0

Bin 3127: 174 of cap free
Amount of items: 2
Items: 
Size: 796465 Color: 8
Size: 203362 Color: 13

Bin 3128: 175 of cap free
Amount of items: 2
Items: 
Size: 544361 Color: 0
Size: 455465 Color: 6

Bin 3129: 175 of cap free
Amount of items: 2
Items: 
Size: 672569 Color: 16
Size: 327257 Color: 15

Bin 3130: 175 of cap free
Amount of items: 2
Items: 
Size: 705613 Color: 14
Size: 294213 Color: 13

Bin 3131: 175 of cap free
Amount of items: 2
Items: 
Size: 706928 Color: 7
Size: 292898 Color: 3

Bin 3132: 175 of cap free
Amount of items: 2
Items: 
Size: 739033 Color: 2
Size: 260793 Color: 1

Bin 3133: 176 of cap free
Amount of items: 2
Items: 
Size: 523033 Color: 9
Size: 476792 Color: 11

Bin 3134: 176 of cap free
Amount of items: 2
Items: 
Size: 575710 Color: 2
Size: 424115 Color: 6

Bin 3135: 176 of cap free
Amount of items: 2
Items: 
Size: 687312 Color: 19
Size: 312513 Color: 9

Bin 3136: 176 of cap free
Amount of items: 2
Items: 
Size: 728566 Color: 5
Size: 271259 Color: 0

Bin 3137: 177 of cap free
Amount of items: 2
Items: 
Size: 768908 Color: 11
Size: 230916 Color: 5

Bin 3138: 178 of cap free
Amount of items: 3
Items: 
Size: 356856 Color: 11
Size: 337646 Color: 0
Size: 305321 Color: 18

Bin 3139: 178 of cap free
Amount of items: 3
Items: 
Size: 397296 Color: 5
Size: 337305 Color: 6
Size: 265222 Color: 1

Bin 3140: 178 of cap free
Amount of items: 2
Items: 
Size: 548896 Color: 10
Size: 450927 Color: 1

Bin 3141: 178 of cap free
Amount of items: 2
Items: 
Size: 617253 Color: 14
Size: 382570 Color: 4

Bin 3142: 178 of cap free
Amount of items: 2
Items: 
Size: 716260 Color: 17
Size: 283563 Color: 8

Bin 3143: 179 of cap free
Amount of items: 2
Items: 
Size: 506147 Color: 2
Size: 493675 Color: 11

Bin 3144: 179 of cap free
Amount of items: 2
Items: 
Size: 662504 Color: 11
Size: 337318 Color: 10

Bin 3145: 179 of cap free
Amount of items: 2
Items: 
Size: 701523 Color: 6
Size: 298299 Color: 13

Bin 3146: 179 of cap free
Amount of items: 2
Items: 
Size: 757078 Color: 15
Size: 242744 Color: 10

Bin 3147: 179 of cap free
Amount of items: 2
Items: 
Size: 771133 Color: 3
Size: 228689 Color: 13

Bin 3148: 180 of cap free
Amount of items: 2
Items: 
Size: 696685 Color: 0
Size: 303136 Color: 7

Bin 3149: 181 of cap free
Amount of items: 2
Items: 
Size: 505284 Color: 17
Size: 494536 Color: 18

Bin 3150: 181 of cap free
Amount of items: 2
Items: 
Size: 640149 Color: 18
Size: 359671 Color: 13

Bin 3151: 182 of cap free
Amount of items: 2
Items: 
Size: 526000 Color: 18
Size: 473819 Color: 7

Bin 3152: 182 of cap free
Amount of items: 3
Items: 
Size: 538423 Color: 12
Size: 231889 Color: 18
Size: 229507 Color: 12

Bin 3153: 182 of cap free
Amount of items: 2
Items: 
Size: 543016 Color: 7
Size: 456803 Color: 0

Bin 3154: 182 of cap free
Amount of items: 2
Items: 
Size: 593458 Color: 6
Size: 406361 Color: 8

Bin 3155: 182 of cap free
Amount of items: 2
Items: 
Size: 660793 Color: 3
Size: 339026 Color: 0

Bin 3156: 182 of cap free
Amount of items: 2
Items: 
Size: 673911 Color: 13
Size: 325908 Color: 3

Bin 3157: 182 of cap free
Amount of items: 2
Items: 
Size: 688758 Color: 16
Size: 311061 Color: 1

Bin 3158: 182 of cap free
Amount of items: 2
Items: 
Size: 699175 Color: 3
Size: 300644 Color: 4

Bin 3159: 183 of cap free
Amount of items: 2
Items: 
Size: 595588 Color: 11
Size: 404230 Color: 7

Bin 3160: 183 of cap free
Amount of items: 3
Items: 
Size: 600104 Color: 8
Size: 201490 Color: 17
Size: 198224 Color: 11

Bin 3161: 183 of cap free
Amount of items: 2
Items: 
Size: 720044 Color: 12
Size: 279774 Color: 7

Bin 3162: 184 of cap free
Amount of items: 2
Items: 
Size: 560712 Color: 8
Size: 439105 Color: 16

Bin 3163: 184 of cap free
Amount of items: 2
Items: 
Size: 581771 Color: 3
Size: 418046 Color: 14

Bin 3164: 184 of cap free
Amount of items: 2
Items: 
Size: 779524 Color: 9
Size: 220293 Color: 17

Bin 3165: 185 of cap free
Amount of items: 2
Items: 
Size: 526871 Color: 7
Size: 472945 Color: 10

Bin 3166: 185 of cap free
Amount of items: 2
Items: 
Size: 534364 Color: 19
Size: 465452 Color: 0

Bin 3167: 185 of cap free
Amount of items: 2
Items: 
Size: 710825 Color: 6
Size: 288991 Color: 13

Bin 3168: 185 of cap free
Amount of items: 3
Items: 
Size: 753976 Color: 13
Size: 128296 Color: 13
Size: 117544 Color: 18

Bin 3169: 185 of cap free
Amount of items: 2
Items: 
Size: 770752 Color: 1
Size: 229064 Color: 2

Bin 3170: 185 of cap free
Amount of items: 2
Items: 
Size: 789089 Color: 16
Size: 210727 Color: 10

Bin 3171: 186 of cap free
Amount of items: 3
Items: 
Size: 361932 Color: 1
Size: 353009 Color: 1
Size: 284874 Color: 3

Bin 3172: 186 of cap free
Amount of items: 3
Items: 
Size: 468716 Color: 19
Size: 266009 Color: 2
Size: 265090 Color: 2

Bin 3173: 186 of cap free
Amount of items: 2
Items: 
Size: 566364 Color: 3
Size: 433451 Color: 16

Bin 3174: 186 of cap free
Amount of items: 2
Items: 
Size: 592481 Color: 7
Size: 407334 Color: 3

Bin 3175: 186 of cap free
Amount of items: 2
Items: 
Size: 594912 Color: 17
Size: 404903 Color: 10

Bin 3176: 186 of cap free
Amount of items: 2
Items: 
Size: 657589 Color: 18
Size: 342226 Color: 6

Bin 3177: 186 of cap free
Amount of items: 2
Items: 
Size: 677430 Color: 19
Size: 322385 Color: 6

Bin 3178: 186 of cap free
Amount of items: 2
Items: 
Size: 715149 Color: 0
Size: 284666 Color: 16

Bin 3179: 186 of cap free
Amount of items: 2
Items: 
Size: 718001 Color: 0
Size: 281814 Color: 5

Bin 3180: 187 of cap free
Amount of items: 2
Items: 
Size: 597479 Color: 2
Size: 402335 Color: 5

Bin 3181: 187 of cap free
Amount of items: 2
Items: 
Size: 599993 Color: 0
Size: 399821 Color: 9

Bin 3182: 187 of cap free
Amount of items: 2
Items: 
Size: 743913 Color: 0
Size: 255901 Color: 10

Bin 3183: 187 of cap free
Amount of items: 2
Items: 
Size: 750112 Color: 17
Size: 249702 Color: 11

Bin 3184: 188 of cap free
Amount of items: 2
Items: 
Size: 500752 Color: 12
Size: 499061 Color: 14

Bin 3185: 188 of cap free
Amount of items: 2
Items: 
Size: 527250 Color: 15
Size: 472563 Color: 13

Bin 3186: 188 of cap free
Amount of items: 2
Items: 
Size: 767629 Color: 4
Size: 232184 Color: 7

Bin 3187: 189 of cap free
Amount of items: 2
Items: 
Size: 700445 Color: 10
Size: 299367 Color: 16

Bin 3188: 189 of cap free
Amount of items: 2
Items: 
Size: 733533 Color: 2
Size: 266279 Color: 12

Bin 3189: 189 of cap free
Amount of items: 2
Items: 
Size: 765546 Color: 11
Size: 234266 Color: 10

Bin 3190: 189 of cap free
Amount of items: 2
Items: 
Size: 791594 Color: 13
Size: 208218 Color: 4

Bin 3191: 190 of cap free
Amount of items: 2
Items: 
Size: 532337 Color: 3
Size: 467474 Color: 0

Bin 3192: 190 of cap free
Amount of items: 2
Items: 
Size: 547382 Color: 9
Size: 452429 Color: 8

Bin 3193: 190 of cap free
Amount of items: 2
Items: 
Size: 565452 Color: 12
Size: 434359 Color: 6

Bin 3194: 190 of cap free
Amount of items: 2
Items: 
Size: 590995 Color: 9
Size: 408816 Color: 12

Bin 3195: 190 of cap free
Amount of items: 2
Items: 
Size: 663458 Color: 3
Size: 336353 Color: 0

Bin 3196: 190 of cap free
Amount of items: 2
Items: 
Size: 711661 Color: 13
Size: 288150 Color: 9

Bin 3197: 190 of cap free
Amount of items: 2
Items: 
Size: 720671 Color: 13
Size: 279140 Color: 7

Bin 3198: 190 of cap free
Amount of items: 2
Items: 
Size: 749356 Color: 3
Size: 250455 Color: 13

Bin 3199: 190 of cap free
Amount of items: 3
Items: 
Size: 788743 Color: 16
Size: 105592 Color: 13
Size: 105476 Color: 5

Bin 3200: 191 of cap free
Amount of items: 2
Items: 
Size: 558023 Color: 5
Size: 441787 Color: 13

Bin 3201: 191 of cap free
Amount of items: 2
Items: 
Size: 682116 Color: 6
Size: 317694 Color: 19

Bin 3202: 191 of cap free
Amount of items: 2
Items: 
Size: 750665 Color: 15
Size: 249145 Color: 1

Bin 3203: 191 of cap free
Amount of items: 3
Items: 
Size: 791875 Color: 4
Size: 104020 Color: 1
Size: 103915 Color: 1

Bin 3204: 192 of cap free
Amount of items: 2
Items: 
Size: 576159 Color: 11
Size: 423650 Color: 15

Bin 3205: 192 of cap free
Amount of items: 2
Items: 
Size: 692391 Color: 2
Size: 307418 Color: 4

Bin 3206: 193 of cap free
Amount of items: 2
Items: 
Size: 501151 Color: 1
Size: 498657 Color: 2

Bin 3207: 193 of cap free
Amount of items: 2
Items: 
Size: 512649 Color: 6
Size: 487159 Color: 10

Bin 3208: 194 of cap free
Amount of items: 3
Items: 
Size: 601809 Color: 6
Size: 225666 Color: 6
Size: 172332 Color: 7

Bin 3209: 194 of cap free
Amount of items: 2
Items: 
Size: 793904 Color: 1
Size: 205903 Color: 0

Bin 3210: 195 of cap free
Amount of items: 2
Items: 
Size: 546361 Color: 3
Size: 453445 Color: 12

Bin 3211: 196 of cap free
Amount of items: 2
Items: 
Size: 527294 Color: 2
Size: 472511 Color: 3

Bin 3212: 196 of cap free
Amount of items: 2
Items: 
Size: 585253 Color: 13
Size: 414552 Color: 0

Bin 3213: 196 of cap free
Amount of items: 2
Items: 
Size: 750970 Color: 14
Size: 248835 Color: 0

Bin 3214: 196 of cap free
Amount of items: 2
Items: 
Size: 753970 Color: 1
Size: 245835 Color: 15

Bin 3215: 197 of cap free
Amount of items: 2
Items: 
Size: 617879 Color: 17
Size: 381925 Color: 15

Bin 3216: 197 of cap free
Amount of items: 2
Items: 
Size: 625353 Color: 13
Size: 374451 Color: 1

Bin 3217: 198 of cap free
Amount of items: 2
Items: 
Size: 506615 Color: 0
Size: 493188 Color: 18

Bin 3218: 198 of cap free
Amount of items: 2
Items: 
Size: 564346 Color: 18
Size: 435457 Color: 8

Bin 3219: 199 of cap free
Amount of items: 2
Items: 
Size: 582060 Color: 3
Size: 417742 Color: 8

Bin 3220: 199 of cap free
Amount of items: 2
Items: 
Size: 589798 Color: 17
Size: 410004 Color: 7

Bin 3221: 199 of cap free
Amount of items: 2
Items: 
Size: 693305 Color: 3
Size: 306497 Color: 11

Bin 3222: 199 of cap free
Amount of items: 2
Items: 
Size: 760753 Color: 7
Size: 239049 Color: 13

Bin 3223: 199 of cap free
Amount of items: 2
Items: 
Size: 762657 Color: 14
Size: 237145 Color: 15

Bin 3224: 199 of cap free
Amount of items: 2
Items: 
Size: 774518 Color: 11
Size: 225284 Color: 9

Bin 3225: 199 of cap free
Amount of items: 2
Items: 
Size: 795134 Color: 9
Size: 204668 Color: 12

Bin 3226: 200 of cap free
Amount of items: 2
Items: 
Size: 535964 Color: 19
Size: 463837 Color: 14

Bin 3227: 200 of cap free
Amount of items: 2
Items: 
Size: 623382 Color: 4
Size: 376419 Color: 10

Bin 3228: 200 of cap free
Amount of items: 2
Items: 
Size: 663935 Color: 1
Size: 335866 Color: 4

Bin 3229: 200 of cap free
Amount of items: 2
Items: 
Size: 668738 Color: 16
Size: 331063 Color: 3

Bin 3230: 201 of cap free
Amount of items: 2
Items: 
Size: 528323 Color: 9
Size: 471477 Color: 13

Bin 3231: 201 of cap free
Amount of items: 2
Items: 
Size: 541770 Color: 4
Size: 458030 Color: 18

Bin 3232: 201 of cap free
Amount of items: 2
Items: 
Size: 548884 Color: 5
Size: 450916 Color: 15

Bin 3233: 201 of cap free
Amount of items: 2
Items: 
Size: 764807 Color: 10
Size: 234993 Color: 4

Bin 3234: 201 of cap free
Amount of items: 2
Items: 
Size: 783857 Color: 0
Size: 215943 Color: 7

Bin 3235: 202 of cap free
Amount of items: 3
Items: 
Size: 409331 Color: 0
Size: 338139 Color: 4
Size: 252329 Color: 12

Bin 3236: 202 of cap free
Amount of items: 2
Items: 
Size: 541459 Color: 16
Size: 458340 Color: 12

Bin 3237: 202 of cap free
Amount of items: 2
Items: 
Size: 635322 Color: 8
Size: 364477 Color: 9

Bin 3238: 204 of cap free
Amount of items: 2
Items: 
Size: 549889 Color: 14
Size: 449908 Color: 0

Bin 3239: 204 of cap free
Amount of items: 2
Items: 
Size: 584406 Color: 10
Size: 415391 Color: 18

Bin 3240: 204 of cap free
Amount of items: 2
Items: 
Size: 591421 Color: 6
Size: 408376 Color: 0

Bin 3241: 204 of cap free
Amount of items: 2
Items: 
Size: 711128 Color: 2
Size: 288669 Color: 13

Bin 3242: 204 of cap free
Amount of items: 2
Items: 
Size: 739596 Color: 0
Size: 260201 Color: 14

Bin 3243: 204 of cap free
Amount of items: 2
Items: 
Size: 782708 Color: 8
Size: 217089 Color: 17

Bin 3244: 206 of cap free
Amount of items: 2
Items: 
Size: 584152 Color: 2
Size: 415643 Color: 13

Bin 3245: 206 of cap free
Amount of items: 2
Items: 
Size: 603341 Color: 17
Size: 396454 Color: 18

Bin 3246: 206 of cap free
Amount of items: 2
Items: 
Size: 618219 Color: 19
Size: 381576 Color: 5

Bin 3247: 206 of cap free
Amount of items: 2
Items: 
Size: 622661 Color: 16
Size: 377134 Color: 4

Bin 3248: 206 of cap free
Amount of items: 3
Items: 
Size: 697019 Color: 9
Size: 152259 Color: 19
Size: 150517 Color: 14

Bin 3249: 206 of cap free
Amount of items: 2
Items: 
Size: 708596 Color: 9
Size: 291199 Color: 13

Bin 3250: 206 of cap free
Amount of items: 2
Items: 
Size: 717989 Color: 2
Size: 281806 Color: 7

Bin 3251: 206 of cap free
Amount of items: 3
Items: 
Size: 746615 Color: 5
Size: 134160 Color: 13
Size: 119020 Color: 11

Bin 3252: 206 of cap free
Amount of items: 2
Items: 
Size: 774947 Color: 3
Size: 224848 Color: 1

Bin 3253: 207 of cap free
Amount of items: 2
Items: 
Size: 519233 Color: 19
Size: 480561 Color: 15

Bin 3254: 207 of cap free
Amount of items: 2
Items: 
Size: 602105 Color: 19
Size: 397689 Color: 10

Bin 3255: 207 of cap free
Amount of items: 2
Items: 
Size: 610471 Color: 5
Size: 389323 Color: 14

Bin 3256: 207 of cap free
Amount of items: 2
Items: 
Size: 715142 Color: 5
Size: 284652 Color: 11

Bin 3257: 208 of cap free
Amount of items: 2
Items: 
Size: 522263 Color: 14
Size: 477530 Color: 15

Bin 3258: 208 of cap free
Amount of items: 2
Items: 
Size: 597479 Color: 14
Size: 402314 Color: 3

Bin 3259: 208 of cap free
Amount of items: 2
Items: 
Size: 598878 Color: 8
Size: 400915 Color: 1

Bin 3260: 208 of cap free
Amount of items: 2
Items: 
Size: 686554 Color: 3
Size: 313239 Color: 17

Bin 3261: 208 of cap free
Amount of items: 2
Items: 
Size: 724202 Color: 6
Size: 275591 Color: 0

Bin 3262: 209 of cap free
Amount of items: 2
Items: 
Size: 542569 Color: 6
Size: 457223 Color: 16

Bin 3263: 209 of cap free
Amount of items: 2
Items: 
Size: 551511 Color: 4
Size: 448281 Color: 13

Bin 3264: 209 of cap free
Amount of items: 3
Items: 
Size: 610133 Color: 14
Size: 196086 Color: 16
Size: 193573 Color: 7

Bin 3265: 209 of cap free
Amount of items: 2
Items: 
Size: 688303 Color: 14
Size: 311489 Color: 11

Bin 3266: 210 of cap free
Amount of items: 2
Items: 
Size: 525084 Color: 13
Size: 474707 Color: 9

Bin 3267: 210 of cap free
Amount of items: 2
Items: 
Size: 736153 Color: 10
Size: 263638 Color: 2

Bin 3268: 211 of cap free
Amount of items: 2
Items: 
Size: 555814 Color: 13
Size: 443976 Color: 15

Bin 3269: 211 of cap free
Amount of items: 2
Items: 
Size: 694821 Color: 15
Size: 304969 Color: 3

Bin 3270: 212 of cap free
Amount of items: 2
Items: 
Size: 642600 Color: 13
Size: 357189 Color: 14

Bin 3271: 212 of cap free
Amount of items: 2
Items: 
Size: 651930 Color: 1
Size: 347859 Color: 12

Bin 3272: 212 of cap free
Amount of items: 2
Items: 
Size: 700220 Color: 8
Size: 299569 Color: 4

Bin 3273: 213 of cap free
Amount of items: 2
Items: 
Size: 626018 Color: 13
Size: 373770 Color: 10

Bin 3274: 213 of cap free
Amount of items: 2
Items: 
Size: 651085 Color: 11
Size: 348703 Color: 4

Bin 3275: 213 of cap free
Amount of items: 2
Items: 
Size: 659536 Color: 13
Size: 340252 Color: 12

Bin 3276: 213 of cap free
Amount of items: 2
Items: 
Size: 697492 Color: 1
Size: 302296 Color: 16

Bin 3277: 213 of cap free
Amount of items: 2
Items: 
Size: 726772 Color: 9
Size: 273016 Color: 16

Bin 3278: 213 of cap free
Amount of items: 2
Items: 
Size: 755955 Color: 5
Size: 243833 Color: 12

Bin 3279: 214 of cap free
Amount of items: 2
Items: 
Size: 511389 Color: 12
Size: 488398 Color: 5

Bin 3280: 214 of cap free
Amount of items: 2
Items: 
Size: 670550 Color: 11
Size: 329237 Color: 9

Bin 3281: 215 of cap free
Amount of items: 2
Items: 
Size: 613651 Color: 3
Size: 386135 Color: 8

Bin 3282: 215 of cap free
Amount of items: 2
Items: 
Size: 768890 Color: 16
Size: 230896 Color: 14

Bin 3283: 216 of cap free
Amount of items: 2
Items: 
Size: 598648 Color: 6
Size: 401137 Color: 12

Bin 3284: 216 of cap free
Amount of items: 2
Items: 
Size: 610252 Color: 18
Size: 389533 Color: 17

Bin 3285: 216 of cap free
Amount of items: 2
Items: 
Size: 623163 Color: 15
Size: 376622 Color: 2

Bin 3286: 217 of cap free
Amount of items: 2
Items: 
Size: 671419 Color: 9
Size: 328365 Color: 6

Bin 3287: 217 of cap free
Amount of items: 2
Items: 
Size: 705542 Color: 1
Size: 294242 Color: 8

Bin 3288: 217 of cap free
Amount of items: 2
Items: 
Size: 720916 Color: 12
Size: 278868 Color: 5

Bin 3289: 218 of cap free
Amount of items: 2
Items: 
Size: 554186 Color: 8
Size: 445597 Color: 19

Bin 3290: 218 of cap free
Amount of items: 2
Items: 
Size: 715141 Color: 9
Size: 284642 Color: 0

Bin 3291: 219 of cap free
Amount of items: 2
Items: 
Size: 503409 Color: 5
Size: 496373 Color: 11

Bin 3292: 219 of cap free
Amount of items: 2
Items: 
Size: 586640 Color: 13
Size: 413142 Color: 0

Bin 3293: 220 of cap free
Amount of items: 2
Items: 
Size: 505371 Color: 2
Size: 494410 Color: 1

Bin 3294: 220 of cap free
Amount of items: 2
Items: 
Size: 521573 Color: 5
Size: 478208 Color: 3

Bin 3295: 220 of cap free
Amount of items: 2
Items: 
Size: 684578 Color: 15
Size: 315203 Color: 18

Bin 3296: 220 of cap free
Amount of items: 2
Items: 
Size: 703116 Color: 8
Size: 296665 Color: 2

Bin 3297: 221 of cap free
Amount of items: 2
Items: 
Size: 619283 Color: 13
Size: 380497 Color: 14

Bin 3298: 221 of cap free
Amount of items: 2
Items: 
Size: 716090 Color: 18
Size: 283690 Color: 13

Bin 3299: 222 of cap free
Amount of items: 2
Items: 
Size: 571036 Color: 18
Size: 428743 Color: 1

Bin 3300: 222 of cap free
Amount of items: 2
Items: 
Size: 702984 Color: 0
Size: 296795 Color: 4

Bin 3301: 222 of cap free
Amount of items: 2
Items: 
Size: 746141 Color: 18
Size: 253638 Color: 12

Bin 3302: 222 of cap free
Amount of items: 2
Items: 
Size: 780601 Color: 6
Size: 219178 Color: 0

Bin 3303: 223 of cap free
Amount of items: 2
Items: 
Size: 665667 Color: 11
Size: 334111 Color: 1

Bin 3304: 224 of cap free
Amount of items: 2
Items: 
Size: 510470 Color: 16
Size: 489307 Color: 8

Bin 3305: 225 of cap free
Amount of items: 2
Items: 
Size: 537737 Color: 19
Size: 462039 Color: 6

Bin 3306: 225 of cap free
Amount of items: 2
Items: 
Size: 599612 Color: 8
Size: 400164 Color: 2

Bin 3307: 225 of cap free
Amount of items: 2
Items: 
Size: 603932 Color: 11
Size: 395844 Color: 9

Bin 3308: 225 of cap free
Amount of items: 2
Items: 
Size: 644224 Color: 17
Size: 355552 Color: 18

Bin 3309: 225 of cap free
Amount of items: 2
Items: 
Size: 649606 Color: 3
Size: 350170 Color: 5

Bin 3310: 225 of cap free
Amount of items: 2
Items: 
Size: 697125 Color: 6
Size: 302651 Color: 8

Bin 3311: 225 of cap free
Amount of items: 2
Items: 
Size: 705441 Color: 3
Size: 294335 Color: 14

Bin 3312: 225 of cap free
Amount of items: 2
Items: 
Size: 710802 Color: 2
Size: 288974 Color: 16

Bin 3313: 226 of cap free
Amount of items: 3
Items: 
Size: 349895 Color: 7
Size: 342026 Color: 17
Size: 307854 Color: 12

Bin 3314: 226 of cap free
Amount of items: 3
Items: 
Size: 615087 Color: 0
Size: 192345 Color: 17
Size: 192343 Color: 10

Bin 3315: 227 of cap free
Amount of items: 2
Items: 
Size: 624276 Color: 0
Size: 375498 Color: 5

Bin 3316: 227 of cap free
Amount of items: 2
Items: 
Size: 642017 Color: 13
Size: 357757 Color: 17

Bin 3317: 227 of cap free
Amount of items: 2
Items: 
Size: 645371 Color: 8
Size: 354403 Color: 16

Bin 3318: 227 of cap free
Amount of items: 3
Items: 
Size: 679031 Color: 13
Size: 166504 Color: 13
Size: 154239 Color: 12

Bin 3319: 228 of cap free
Amount of items: 2
Items: 
Size: 510883 Color: 19
Size: 488890 Color: 14

Bin 3320: 228 of cap free
Amount of items: 2
Items: 
Size: 517621 Color: 4
Size: 482152 Color: 12

Bin 3321: 228 of cap free
Amount of items: 3
Items: 
Size: 576044 Color: 1
Size: 227142 Color: 18
Size: 196587 Color: 17

Bin 3322: 228 of cap free
Amount of items: 2
Items: 
Size: 631410 Color: 0
Size: 368363 Color: 3

Bin 3323: 229 of cap free
Amount of items: 3
Items: 
Size: 518287 Color: 11
Size: 253871 Color: 12
Size: 227614 Color: 15

Bin 3324: 229 of cap free
Amount of items: 2
Items: 
Size: 573492 Color: 4
Size: 426280 Color: 12

Bin 3325: 229 of cap free
Amount of items: 2
Items: 
Size: 587760 Color: 2
Size: 412012 Color: 13

Bin 3326: 230 of cap free
Amount of items: 2
Items: 
Size: 581395 Color: 1
Size: 418376 Color: 16

Bin 3327: 230 of cap free
Amount of items: 2
Items: 
Size: 588814 Color: 5
Size: 410957 Color: 13

Bin 3328: 230 of cap free
Amount of items: 2
Items: 
Size: 647611 Color: 19
Size: 352160 Color: 17

Bin 3329: 230 of cap free
Amount of items: 3
Items: 
Size: 770110 Color: 10
Size: 115225 Color: 19
Size: 114436 Color: 0

Bin 3330: 231 of cap free
Amount of items: 2
Items: 
Size: 530845 Color: 18
Size: 468925 Color: 14

Bin 3331: 231 of cap free
Amount of items: 2
Items: 
Size: 540075 Color: 4
Size: 459695 Color: 16

Bin 3332: 231 of cap free
Amount of items: 2
Items: 
Size: 752474 Color: 9
Size: 247296 Color: 15

Bin 3333: 232 of cap free
Amount of items: 2
Items: 
Size: 529071 Color: 5
Size: 470698 Color: 1

Bin 3334: 232 of cap free
Amount of items: 2
Items: 
Size: 646550 Color: 8
Size: 353219 Color: 3

Bin 3335: 232 of cap free
Amount of items: 2
Items: 
Size: 647052 Color: 7
Size: 352717 Color: 9

Bin 3336: 232 of cap free
Amount of items: 2
Items: 
Size: 738748 Color: 6
Size: 261021 Color: 15

Bin 3337: 232 of cap free
Amount of items: 2
Items: 
Size: 742189 Color: 19
Size: 257580 Color: 17

Bin 3338: 232 of cap free
Amount of items: 2
Items: 
Size: 751929 Color: 9
Size: 247840 Color: 1

Bin 3339: 232 of cap free
Amount of items: 2
Items: 
Size: 783295 Color: 10
Size: 216474 Color: 18

Bin 3340: 233 of cap free
Amount of items: 2
Items: 
Size: 522996 Color: 3
Size: 476772 Color: 5

Bin 3341: 233 of cap free
Amount of items: 2
Items: 
Size: 535039 Color: 11
Size: 464729 Color: 5

Bin 3342: 233 of cap free
Amount of items: 2
Items: 
Size: 718261 Color: 8
Size: 281507 Color: 7

Bin 3343: 234 of cap free
Amount of items: 2
Items: 
Size: 529739 Color: 13
Size: 470028 Color: 11

Bin 3344: 234 of cap free
Amount of items: 2
Items: 
Size: 565974 Color: 1
Size: 433793 Color: 8

Bin 3345: 234 of cap free
Amount of items: 3
Items: 
Size: 667546 Color: 17
Size: 176798 Color: 13
Size: 155423 Color: 15

Bin 3346: 235 of cap free
Amount of items: 2
Items: 
Size: 527629 Color: 14
Size: 472137 Color: 18

Bin 3347: 235 of cap free
Amount of items: 2
Items: 
Size: 687610 Color: 12
Size: 312156 Color: 19

Bin 3348: 237 of cap free
Amount of items: 2
Items: 
Size: 529696 Color: 2
Size: 470068 Color: 3

Bin 3349: 237 of cap free
Amount of items: 3
Items: 
Size: 531793 Color: 2
Size: 250667 Color: 2
Size: 217304 Color: 5

Bin 3350: 237 of cap free
Amount of items: 2
Items: 
Size: 562629 Color: 15
Size: 437135 Color: 3

Bin 3351: 237 of cap free
Amount of items: 2
Items: 
Size: 684564 Color: 18
Size: 315200 Color: 16

Bin 3352: 237 of cap free
Amount of items: 2
Items: 
Size: 746774 Color: 5
Size: 252990 Color: 0

Bin 3353: 237 of cap free
Amount of items: 2
Items: 
Size: 790922 Color: 5
Size: 208842 Color: 6

Bin 3354: 238 of cap free
Amount of items: 2
Items: 
Size: 534743 Color: 5
Size: 465020 Color: 8

Bin 3355: 238 of cap free
Amount of items: 2
Items: 
Size: 540895 Color: 18
Size: 458868 Color: 7

Bin 3356: 238 of cap free
Amount of items: 2
Items: 
Size: 663433 Color: 8
Size: 336330 Color: 11

Bin 3357: 238 of cap free
Amount of items: 2
Items: 
Size: 679268 Color: 12
Size: 320495 Color: 11

Bin 3358: 239 of cap free
Amount of items: 2
Items: 
Size: 560464 Color: 13
Size: 439298 Color: 5

Bin 3359: 239 of cap free
Amount of items: 2
Items: 
Size: 611837 Color: 3
Size: 387925 Color: 14

Bin 3360: 240 of cap free
Amount of items: 2
Items: 
Size: 615406 Color: 11
Size: 384355 Color: 10

Bin 3361: 241 of cap free
Amount of items: 2
Items: 
Size: 798720 Color: 8
Size: 201040 Color: 11

Bin 3362: 242 of cap free
Amount of items: 2
Items: 
Size: 512701 Color: 12
Size: 487058 Color: 1

Bin 3363: 242 of cap free
Amount of items: 2
Items: 
Size: 542738 Color: 10
Size: 457021 Color: 2

Bin 3364: 243 of cap free
Amount of items: 2
Items: 
Size: 680147 Color: 1
Size: 319611 Color: 17

Bin 3365: 243 of cap free
Amount of items: 2
Items: 
Size: 775501 Color: 19
Size: 224257 Color: 14

Bin 3366: 245 of cap free
Amount of items: 2
Items: 
Size: 597851 Color: 17
Size: 401905 Color: 12

Bin 3367: 245 of cap free
Amount of items: 2
Items: 
Size: 766962 Color: 6
Size: 232794 Color: 7

Bin 3368: 246 of cap free
Amount of items: 2
Items: 
Size: 507946 Color: 2
Size: 491809 Color: 11

Bin 3369: 247 of cap free
Amount of items: 2
Items: 
Size: 527797 Color: 18
Size: 471957 Color: 17

Bin 3370: 247 of cap free
Amount of items: 2
Items: 
Size: 704830 Color: 6
Size: 294924 Color: 11

Bin 3371: 248 of cap free
Amount of items: 2
Items: 
Size: 752464 Color: 7
Size: 247289 Color: 4

Bin 3372: 249 of cap free
Amount of items: 2
Items: 
Size: 574748 Color: 13
Size: 425004 Color: 8

Bin 3373: 249 of cap free
Amount of items: 3
Items: 
Size: 680746 Color: 13
Size: 165916 Color: 13
Size: 153090 Color: 11

Bin 3374: 249 of cap free
Amount of items: 2
Items: 
Size: 712311 Color: 19
Size: 287441 Color: 2

Bin 3375: 249 of cap free
Amount of items: 2
Items: 
Size: 739585 Color: 15
Size: 260167 Color: 11

Bin 3376: 250 of cap free
Amount of items: 2
Items: 
Size: 554734 Color: 8
Size: 445017 Color: 5

Bin 3377: 251 of cap free
Amount of items: 2
Items: 
Size: 525566 Color: 9
Size: 474184 Color: 15

Bin 3378: 251 of cap free
Amount of items: 2
Items: 
Size: 737199 Color: 1
Size: 262551 Color: 11

Bin 3379: 252 of cap free
Amount of items: 3
Items: 
Size: 345636 Color: 19
Size: 344288 Color: 17
Size: 309825 Color: 18

Bin 3380: 252 of cap free
Amount of items: 2
Items: 
Size: 506165 Color: 3
Size: 493584 Color: 14

Bin 3381: 252 of cap free
Amount of items: 2
Items: 
Size: 534326 Color: 4
Size: 465423 Color: 14

Bin 3382: 252 of cap free
Amount of items: 2
Items: 
Size: 792992 Color: 11
Size: 206757 Color: 3

Bin 3383: 253 of cap free
Amount of items: 2
Items: 
Size: 638781 Color: 11
Size: 360967 Color: 18

Bin 3384: 253 of cap free
Amount of items: 2
Items: 
Size: 643822 Color: 13
Size: 355926 Color: 0

Bin 3385: 253 of cap free
Amount of items: 2
Items: 
Size: 717948 Color: 11
Size: 281800 Color: 6

Bin 3386: 253 of cap free
Amount of items: 2
Items: 
Size: 765265 Color: 1
Size: 234483 Color: 8

Bin 3387: 254 of cap free
Amount of items: 2
Items: 
Size: 588545 Color: 14
Size: 411202 Color: 1

Bin 3388: 254 of cap free
Amount of items: 2
Items: 
Size: 687977 Color: 17
Size: 311770 Color: 12

Bin 3389: 255 of cap free
Amount of items: 2
Items: 
Size: 651903 Color: 10
Size: 347843 Color: 11

Bin 3390: 256 of cap free
Amount of items: 2
Items: 
Size: 558982 Color: 1
Size: 440763 Color: 4

Bin 3391: 256 of cap free
Amount of items: 2
Items: 
Size: 673045 Color: 13
Size: 326700 Color: 18

Bin 3392: 256 of cap free
Amount of items: 2
Items: 
Size: 678529 Color: 7
Size: 321216 Color: 18

Bin 3393: 256 of cap free
Amount of items: 2
Items: 
Size: 773617 Color: 11
Size: 226128 Color: 19

Bin 3394: 256 of cap free
Amount of items: 2
Items: 
Size: 792989 Color: 3
Size: 206756 Color: 4

Bin 3395: 257 of cap free
Amount of items: 2
Items: 
Size: 579951 Color: 17
Size: 419793 Color: 4

Bin 3396: 257 of cap free
Amount of items: 2
Items: 
Size: 711127 Color: 10
Size: 288617 Color: 9

Bin 3397: 259 of cap free
Amount of items: 2
Items: 
Size: 510872 Color: 4
Size: 488870 Color: 12

Bin 3398: 259 of cap free
Amount of items: 2
Items: 
Size: 577625 Color: 13
Size: 422117 Color: 0

Bin 3399: 259 of cap free
Amount of items: 2
Items: 
Size: 669257 Color: 9
Size: 330485 Color: 12

Bin 3400: 259 of cap free
Amount of items: 2
Items: 
Size: 683792 Color: 17
Size: 315950 Color: 4

Bin 3401: 259 of cap free
Amount of items: 2
Items: 
Size: 702639 Color: 0
Size: 297103 Color: 16

Bin 3402: 261 of cap free
Amount of items: 2
Items: 
Size: 509892 Color: 6
Size: 489848 Color: 9

Bin 3403: 261 of cap free
Amount of items: 2
Items: 
Size: 518888 Color: 17
Size: 480852 Color: 2

Bin 3404: 261 of cap free
Amount of items: 2
Items: 
Size: 572146 Color: 8
Size: 427594 Color: 11

Bin 3405: 261 of cap free
Amount of items: 2
Items: 
Size: 719491 Color: 4
Size: 280249 Color: 12

Bin 3406: 261 of cap free
Amount of items: 2
Items: 
Size: 763710 Color: 6
Size: 236030 Color: 3

Bin 3407: 261 of cap free
Amount of items: 2
Items: 
Size: 769651 Color: 2
Size: 230089 Color: 18

Bin 3408: 262 of cap free
Amount of items: 2
Items: 
Size: 564695 Color: 2
Size: 435044 Color: 3

Bin 3409: 262 of cap free
Amount of items: 2
Items: 
Size: 751597 Color: 5
Size: 248142 Color: 4

Bin 3410: 263 of cap free
Amount of items: 2
Items: 
Size: 578167 Color: 4
Size: 421571 Color: 6

Bin 3411: 263 of cap free
Amount of items: 2
Items: 
Size: 586130 Color: 10
Size: 413608 Color: 11

Bin 3412: 263 of cap free
Amount of items: 2
Items: 
Size: 720032 Color: 17
Size: 279706 Color: 12

Bin 3413: 265 of cap free
Amount of items: 2
Items: 
Size: 537961 Color: 6
Size: 461775 Color: 19

Bin 3414: 265 of cap free
Amount of items: 2
Items: 
Size: 628717 Color: 9
Size: 371019 Color: 16

Bin 3415: 265 of cap free
Amount of items: 2
Items: 
Size: 653460 Color: 0
Size: 346276 Color: 10

Bin 3416: 265 of cap free
Amount of items: 2
Items: 
Size: 744397 Color: 3
Size: 255339 Color: 4

Bin 3417: 266 of cap free
Amount of items: 2
Items: 
Size: 621727 Color: 0
Size: 378008 Color: 10

Bin 3418: 266 of cap free
Amount of items: 2
Items: 
Size: 664473 Color: 14
Size: 335262 Color: 16

Bin 3419: 266 of cap free
Amount of items: 2
Items: 
Size: 679253 Color: 6
Size: 320482 Color: 5

Bin 3420: 267 of cap free
Amount of items: 2
Items: 
Size: 539993 Color: 2
Size: 459741 Color: 3

Bin 3421: 267 of cap free
Amount of items: 2
Items: 
Size: 764378 Color: 19
Size: 235356 Color: 16

Bin 3422: 267 of cap free
Amount of items: 2
Items: 
Size: 797087 Color: 4
Size: 202647 Color: 3

Bin 3423: 268 of cap free
Amount of items: 2
Items: 
Size: 503672 Color: 7
Size: 496061 Color: 9

Bin 3424: 269 of cap free
Amount of items: 2
Items: 
Size: 594175 Color: 10
Size: 405557 Color: 19

Bin 3425: 269 of cap free
Amount of items: 2
Items: 
Size: 781063 Color: 3
Size: 218669 Color: 8

Bin 3426: 270 of cap free
Amount of items: 2
Items: 
Size: 510862 Color: 12
Size: 488869 Color: 0

Bin 3427: 271 of cap free
Amount of items: 2
Items: 
Size: 512240 Color: 1
Size: 487490 Color: 2

Bin 3428: 271 of cap free
Amount of items: 3
Items: 
Size: 737672 Color: 10
Size: 142697 Color: 13
Size: 119361 Color: 12

Bin 3429: 271 of cap free
Amount of items: 2
Items: 
Size: 799966 Color: 9
Size: 199764 Color: 5

Bin 3430: 272 of cap free
Amount of items: 2
Items: 
Size: 532882 Color: 14
Size: 466847 Color: 16

Bin 3431: 272 of cap free
Amount of items: 2
Items: 
Size: 611160 Color: 0
Size: 388569 Color: 7

Bin 3432: 272 of cap free
Amount of items: 2
Items: 
Size: 652227 Color: 6
Size: 347502 Color: 4

Bin 3433: 273 of cap free
Amount of items: 2
Items: 
Size: 633234 Color: 10
Size: 366494 Color: 4

Bin 3434: 273 of cap free
Amount of items: 2
Items: 
Size: 692819 Color: 2
Size: 306909 Color: 9

Bin 3435: 274 of cap free
Amount of items: 2
Items: 
Size: 545086 Color: 11
Size: 454641 Color: 14

Bin 3436: 274 of cap free
Amount of items: 2
Items: 
Size: 663041 Color: 5
Size: 336686 Color: 11

Bin 3437: 275 of cap free
Amount of items: 2
Items: 
Size: 508284 Color: 16
Size: 491442 Color: 15

Bin 3438: 275 of cap free
Amount of items: 2
Items: 
Size: 692348 Color: 16
Size: 307378 Color: 5

Bin 3439: 276 of cap free
Amount of items: 2
Items: 
Size: 502029 Color: 4
Size: 497696 Color: 15

Bin 3440: 276 of cap free
Amount of items: 2
Items: 
Size: 715434 Color: 2
Size: 284291 Color: 5

Bin 3441: 276 of cap free
Amount of items: 2
Items: 
Size: 798168 Color: 16
Size: 201557 Color: 13

Bin 3442: 277 of cap free
Amount of items: 2
Items: 
Size: 589762 Color: 19
Size: 409962 Color: 6

Bin 3443: 277 of cap free
Amount of items: 2
Items: 
Size: 696610 Color: 2
Size: 303114 Color: 16

Bin 3444: 277 of cap free
Amount of items: 2
Items: 
Size: 704543 Color: 18
Size: 295181 Color: 8

Bin 3445: 277 of cap free
Amount of items: 3
Items: 
Size: 707317 Color: 12
Size: 147352 Color: 11
Size: 145055 Color: 15

Bin 3446: 277 of cap free
Amount of items: 2
Items: 
Size: 780571 Color: 5
Size: 219153 Color: 6

Bin 3447: 278 of cap free
Amount of items: 2
Items: 
Size: 519969 Color: 13
Size: 479754 Color: 12

Bin 3448: 278 of cap free
Amount of items: 2
Items: 
Size: 584761 Color: 19
Size: 414962 Color: 12

Bin 3449: 280 of cap free
Amount of items: 2
Items: 
Size: 570565 Color: 16
Size: 429156 Color: 3

Bin 3450: 280 of cap free
Amount of items: 2
Items: 
Size: 574385 Color: 2
Size: 425336 Color: 5

Bin 3451: 280 of cap free
Amount of items: 2
Items: 
Size: 765238 Color: 14
Size: 234483 Color: 12

Bin 3452: 281 of cap free
Amount of items: 2
Items: 
Size: 554729 Color: 8
Size: 444991 Color: 6

Bin 3453: 281 of cap free
Amount of items: 3
Items: 
Size: 672715 Color: 13
Size: 172810 Color: 13
Size: 154195 Color: 1

Bin 3454: 282 of cap free
Amount of items: 2
Items: 
Size: 684555 Color: 12
Size: 315164 Color: 5

Bin 3455: 282 of cap free
Amount of items: 2
Items: 
Size: 686506 Color: 1
Size: 313213 Color: 7

Bin 3456: 283 of cap free
Amount of items: 2
Items: 
Size: 503773 Color: 1
Size: 495945 Color: 2

Bin 3457: 283 of cap free
Amount of items: 2
Items: 
Size: 573971 Color: 4
Size: 425747 Color: 19

Bin 3458: 283 of cap free
Amount of items: 2
Items: 
Size: 603904 Color: 7
Size: 395814 Color: 10

Bin 3459: 285 of cap free
Amount of items: 2
Items: 
Size: 568459 Color: 11
Size: 431257 Color: 3

Bin 3460: 285 of cap free
Amount of items: 2
Items: 
Size: 643522 Color: 5
Size: 356194 Color: 14

Bin 3461: 286 of cap free
Amount of items: 2
Items: 
Size: 743899 Color: 19
Size: 255816 Color: 0

Bin 3462: 286 of cap free
Amount of items: 2
Items: 
Size: 745620 Color: 15
Size: 254095 Color: 2

Bin 3463: 287 of cap free
Amount of items: 2
Items: 
Size: 535884 Color: 5
Size: 463830 Color: 7

Bin 3464: 287 of cap free
Amount of items: 2
Items: 
Size: 673137 Color: 2
Size: 326577 Color: 17

Bin 3465: 287 of cap free
Amount of items: 2
Items: 
Size: 693859 Color: 6
Size: 305855 Color: 0

Bin 3466: 287 of cap free
Amount of items: 2
Items: 
Size: 782315 Color: 4
Size: 217399 Color: 11

Bin 3467: 288 of cap free
Amount of items: 2
Items: 
Size: 528718 Color: 3
Size: 470995 Color: 4

Bin 3468: 288 of cap free
Amount of items: 2
Items: 
Size: 712373 Color: 2
Size: 287340 Color: 5

Bin 3469: 289 of cap free
Amount of items: 2
Items: 
Size: 549878 Color: 3
Size: 449834 Color: 0

Bin 3470: 289 of cap free
Amount of items: 2
Items: 
Size: 698465 Color: 6
Size: 301247 Color: 17

Bin 3471: 290 of cap free
Amount of items: 2
Items: 
Size: 506590 Color: 6
Size: 493121 Color: 18

Bin 3472: 290 of cap free
Amount of items: 2
Items: 
Size: 522953 Color: 8
Size: 476758 Color: 10

Bin 3473: 290 of cap free
Amount of items: 2
Items: 
Size: 789582 Color: 19
Size: 210129 Color: 12

Bin 3474: 291 of cap free
Amount of items: 2
Items: 
Size: 590905 Color: 6
Size: 408805 Color: 10

Bin 3475: 291 of cap free
Amount of items: 2
Items: 
Size: 644615 Color: 15
Size: 355095 Color: 17

Bin 3476: 291 of cap free
Amount of items: 2
Items: 
Size: 751244 Color: 8
Size: 248466 Color: 0

Bin 3477: 294 of cap free
Amount of items: 2
Items: 
Size: 683431 Color: 5
Size: 316276 Color: 1

Bin 3478: 294 of cap free
Amount of items: 2
Items: 
Size: 696169 Color: 14
Size: 303538 Color: 18

Bin 3479: 295 of cap free
Amount of items: 2
Items: 
Size: 545420 Color: 19
Size: 454286 Color: 14

Bin 3480: 295 of cap free
Amount of items: 2
Items: 
Size: 788622 Color: 5
Size: 211084 Color: 16

Bin 3481: 296 of cap free
Amount of items: 3
Items: 
Size: 361676 Color: 15
Size: 352658 Color: 9
Size: 285371 Color: 1

Bin 3482: 296 of cap free
Amount of items: 2
Items: 
Size: 499866 Color: 13
Size: 499839 Color: 11

Bin 3483: 296 of cap free
Amount of items: 2
Items: 
Size: 526803 Color: 5
Size: 472902 Color: 19

Bin 3484: 296 of cap free
Amount of items: 2
Items: 
Size: 763694 Color: 7
Size: 236011 Color: 17

Bin 3485: 297 of cap free
Amount of items: 2
Items: 
Size: 686160 Color: 4
Size: 313544 Color: 0

Bin 3486: 298 of cap free
Amount of items: 2
Items: 
Size: 556547 Color: 14
Size: 443156 Color: 2

Bin 3487: 298 of cap free
Amount of items: 2
Items: 
Size: 559782 Color: 16
Size: 439921 Color: 18

Bin 3488: 298 of cap free
Amount of items: 2
Items: 
Size: 578581 Color: 4
Size: 421122 Color: 12

Bin 3489: 298 of cap free
Amount of items: 3
Items: 
Size: 641949 Color: 19
Size: 187538 Color: 2
Size: 170216 Color: 0

Bin 3490: 298 of cap free
Amount of items: 2
Items: 
Size: 741785 Color: 9
Size: 257918 Color: 15

Bin 3491: 299 of cap free
Amount of items: 2
Items: 
Size: 550175 Color: 9
Size: 449527 Color: 8

Bin 3492: 301 of cap free
Amount of items: 2
Items: 
Size: 528270 Color: 1
Size: 471430 Color: 18

Bin 3493: 301 of cap free
Amount of items: 2
Items: 
Size: 616710 Color: 9
Size: 382990 Color: 3

Bin 3494: 302 of cap free
Amount of items: 2
Items: 
Size: 519688 Color: 6
Size: 480011 Color: 18

Bin 3495: 303 of cap free
Amount of items: 2
Items: 
Size: 643521 Color: 18
Size: 356177 Color: 13

Bin 3496: 304 of cap free
Amount of items: 2
Items: 
Size: 516812 Color: 0
Size: 482885 Color: 4

Bin 3497: 304 of cap free
Amount of items: 2
Items: 
Size: 536668 Color: 9
Size: 463029 Color: 6

Bin 3498: 304 of cap free
Amount of items: 2
Items: 
Size: 570954 Color: 1
Size: 428743 Color: 2

Bin 3499: 305 of cap free
Amount of items: 2
Items: 
Size: 696167 Color: 15
Size: 303529 Color: 7

Bin 3500: 306 of cap free
Amount of items: 2
Items: 
Size: 669156 Color: 7
Size: 330539 Color: 9

Bin 3501: 307 of cap free
Amount of items: 2
Items: 
Size: 511378 Color: 8
Size: 488316 Color: 13

Bin 3502: 307 of cap free
Amount of items: 3
Items: 
Size: 696949 Color: 19
Size: 152228 Color: 15
Size: 150517 Color: 0

Bin 3503: 307 of cap free
Amount of items: 2
Items: 
Size: 701647 Color: 16
Size: 298047 Color: 2

Bin 3504: 307 of cap free
Amount of items: 2
Items: 
Size: 774714 Color: 2
Size: 224980 Color: 13

Bin 3505: 308 of cap free
Amount of items: 3
Items: 
Size: 615024 Color: 10
Size: 192335 Color: 16
Size: 192334 Color: 0

Bin 3506: 308 of cap free
Amount of items: 2
Items: 
Size: 665237 Color: 16
Size: 334456 Color: 14

Bin 3507: 308 of cap free
Amount of items: 2
Items: 
Size: 730603 Color: 15
Size: 269090 Color: 13

Bin 3508: 309 of cap free
Amount of items: 2
Items: 
Size: 784175 Color: 9
Size: 215517 Color: 2

Bin 3509: 310 of cap free
Amount of items: 2
Items: 
Size: 798713 Color: 6
Size: 200978 Color: 5

Bin 3510: 312 of cap free
Amount of items: 2
Items: 
Size: 595935 Color: 0
Size: 403754 Color: 4

Bin 3511: 313 of cap free
Amount of items: 2
Items: 
Size: 653015 Color: 14
Size: 346673 Color: 3

Bin 3512: 313 of cap free
Amount of items: 2
Items: 
Size: 746734 Color: 12
Size: 252954 Color: 5

Bin 3513: 314 of cap free
Amount of items: 2
Items: 
Size: 552724 Color: 12
Size: 446963 Color: 15

Bin 3514: 314 of cap free
Amount of items: 2
Items: 
Size: 632237 Color: 13
Size: 367450 Color: 18

Bin 3515: 315 of cap free
Amount of items: 2
Items: 
Size: 536310 Color: 13
Size: 463376 Color: 18

Bin 3516: 315 of cap free
Amount of items: 2
Items: 
Size: 797486 Color: 12
Size: 202200 Color: 11

Bin 3517: 316 of cap free
Amount of items: 2
Items: 
Size: 588486 Color: 9
Size: 411199 Color: 15

Bin 3518: 316 of cap free
Amount of items: 2
Items: 
Size: 695841 Color: 7
Size: 303844 Color: 17

Bin 3519: 316 of cap free
Amount of items: 2
Items: 
Size: 754932 Color: 9
Size: 244753 Color: 6

Bin 3520: 316 of cap free
Amount of items: 2
Items: 
Size: 763688 Color: 19
Size: 235997 Color: 5

Bin 3521: 316 of cap free
Amount of items: 2
Items: 
Size: 789069 Color: 19
Size: 210616 Color: 17

Bin 3522: 317 of cap free
Amount of items: 3
Items: 
Size: 580572 Color: 2
Size: 224123 Color: 16
Size: 194989 Color: 13

Bin 3523: 319 of cap free
Amount of items: 2
Items: 
Size: 540038 Color: 14
Size: 459644 Color: 3

Bin 3524: 319 of cap free
Amount of items: 2
Items: 
Size: 790931 Color: 13
Size: 208751 Color: 9

Bin 3525: 320 of cap free
Amount of items: 2
Items: 
Size: 779094 Color: 10
Size: 220587 Color: 16

Bin 3526: 320 of cap free
Amount of items: 2
Items: 
Size: 781852 Color: 15
Size: 217829 Color: 2

Bin 3527: 321 of cap free
Amount of items: 2
Items: 
Size: 523416 Color: 16
Size: 476264 Color: 8

Bin 3528: 321 of cap free
Amount of items: 2
Items: 
Size: 579237 Color: 11
Size: 420443 Color: 13

Bin 3529: 321 of cap free
Amount of items: 2
Items: 
Size: 587726 Color: 2
Size: 411954 Color: 4

Bin 3530: 322 of cap free
Amount of items: 2
Items: 
Size: 596888 Color: 6
Size: 402791 Color: 18

Bin 3531: 322 of cap free
Amount of items: 2
Items: 
Size: 606219 Color: 4
Size: 393460 Color: 11

Bin 3532: 322 of cap free
Amount of items: 2
Items: 
Size: 687531 Color: 6
Size: 312148 Color: 9

Bin 3533: 324 of cap free
Amount of items: 2
Items: 
Size: 508309 Color: 15
Size: 491368 Color: 18

Bin 3534: 324 of cap free
Amount of items: 2
Items: 
Size: 659649 Color: 2
Size: 340028 Color: 12

Bin 3535: 325 of cap free
Amount of items: 2
Items: 
Size: 502958 Color: 1
Size: 496718 Color: 8

Bin 3536: 325 of cap free
Amount of items: 2
Items: 
Size: 525006 Color: 7
Size: 474670 Color: 6

Bin 3537: 325 of cap free
Amount of items: 2
Items: 
Size: 698457 Color: 3
Size: 301219 Color: 7

Bin 3538: 325 of cap free
Amount of items: 2
Items: 
Size: 727195 Color: 8
Size: 272481 Color: 7

Bin 3539: 326 of cap free
Amount of items: 2
Items: 
Size: 616702 Color: 8
Size: 382973 Color: 7

Bin 3540: 326 of cap free
Amount of items: 2
Items: 
Size: 703673 Color: 10
Size: 296002 Color: 12

Bin 3541: 327 of cap free
Amount of items: 2
Items: 
Size: 519126 Color: 15
Size: 480548 Color: 4

Bin 3542: 328 of cap free
Amount of items: 2
Items: 
Size: 524734 Color: 3
Size: 474939 Color: 7

Bin 3543: 328 of cap free
Amount of items: 2
Items: 
Size: 693184 Color: 3
Size: 306489 Color: 18

Bin 3544: 328 of cap free
Amount of items: 2
Items: 
Size: 709533 Color: 13
Size: 290140 Color: 1

Bin 3545: 329 of cap free
Amount of items: 2
Items: 
Size: 510824 Color: 5
Size: 488848 Color: 3

Bin 3546: 331 of cap free
Amount of items: 2
Items: 
Size: 558935 Color: 17
Size: 440735 Color: 0

Bin 3547: 331 of cap free
Amount of items: 2
Items: 
Size: 646493 Color: 1
Size: 353177 Color: 7

Bin 3548: 333 of cap free
Amount of items: 2
Items: 
Size: 563321 Color: 12
Size: 436347 Color: 6

Bin 3549: 333 of cap free
Amount of items: 2
Items: 
Size: 666933 Color: 10
Size: 332735 Color: 14

Bin 3550: 333 of cap free
Amount of items: 2
Items: 
Size: 748703 Color: 8
Size: 250965 Color: 14

Bin 3551: 333 of cap free
Amount of items: 2
Items: 
Size: 786116 Color: 15
Size: 213552 Color: 17

Bin 3552: 335 of cap free
Amount of items: 2
Items: 
Size: 527236 Color: 5
Size: 472430 Color: 14

Bin 3553: 335 of cap free
Amount of items: 2
Items: 
Size: 765192 Color: 0
Size: 234474 Color: 7

Bin 3554: 336 of cap free
Amount of items: 2
Items: 
Size: 682978 Color: 11
Size: 316687 Color: 6

Bin 3555: 336 of cap free
Amount of items: 2
Items: 
Size: 718256 Color: 9
Size: 281409 Color: 5

Bin 3556: 337 of cap free
Amount of items: 2
Items: 
Size: 582303 Color: 3
Size: 417361 Color: 18

Bin 3557: 339 of cap free
Amount of items: 2
Items: 
Size: 536638 Color: 1
Size: 463024 Color: 18

Bin 3558: 339 of cap free
Amount of items: 2
Items: 
Size: 562220 Color: 6
Size: 437442 Color: 1

Bin 3559: 340 of cap free
Amount of items: 2
Items: 
Size: 525517 Color: 8
Size: 474144 Color: 14

Bin 3560: 340 of cap free
Amount of items: 2
Items: 
Size: 604543 Color: 9
Size: 395118 Color: 1

Bin 3561: 341 of cap free
Amount of items: 2
Items: 
Size: 504562 Color: 17
Size: 495098 Color: 12

Bin 3562: 341 of cap free
Amount of items: 2
Items: 
Size: 538805 Color: 15
Size: 460855 Color: 19

Bin 3563: 341 of cap free
Amount of items: 2
Items: 
Size: 548806 Color: 13
Size: 450854 Color: 3

Bin 3564: 341 of cap free
Amount of items: 2
Items: 
Size: 616698 Color: 8
Size: 382962 Color: 14

Bin 3565: 341 of cap free
Amount of items: 2
Items: 
Size: 674071 Color: 10
Size: 325589 Color: 18

Bin 3566: 341 of cap free
Amount of items: 2
Items: 
Size: 702600 Color: 8
Size: 297060 Color: 14

Bin 3567: 342 of cap free
Amount of items: 2
Items: 
Size: 636875 Color: 16
Size: 362784 Color: 18

Bin 3568: 343 of cap free
Amount of items: 2
Items: 
Size: 562365 Color: 3
Size: 437293 Color: 16

Bin 3569: 344 of cap free
Amount of items: 2
Items: 
Size: 668734 Color: 6
Size: 330923 Color: 12

Bin 3570: 344 of cap free
Amount of items: 2
Items: 
Size: 711125 Color: 19
Size: 288532 Color: 16

Bin 3571: 345 of cap free
Amount of items: 3
Items: 
Size: 361667 Color: 11
Size: 356816 Color: 10
Size: 281173 Color: 16

Bin 3572: 345 of cap free
Amount of items: 2
Items: 
Size: 519784 Color: 17
Size: 479872 Color: 2

Bin 3573: 345 of cap free
Amount of items: 2
Items: 
Size: 686505 Color: 4
Size: 313151 Color: 7

Bin 3574: 346 of cap free
Amount of items: 2
Items: 
Size: 746008 Color: 1
Size: 253647 Color: 13

Bin 3575: 348 of cap free
Amount of items: 2
Items: 
Size: 581251 Color: 5
Size: 418402 Color: 17

Bin 3576: 348 of cap free
Amount of items: 2
Items: 
Size: 619815 Color: 0
Size: 379838 Color: 2

Bin 3577: 348 of cap free
Amount of items: 2
Items: 
Size: 667876 Color: 16
Size: 331777 Color: 8

Bin 3578: 348 of cap free
Amount of items: 2
Items: 
Size: 737845 Color: 3
Size: 261808 Color: 6

Bin 3579: 349 of cap free
Amount of items: 2
Items: 
Size: 561153 Color: 8
Size: 438499 Color: 16

Bin 3580: 351 of cap free
Amount of items: 2
Items: 
Size: 500732 Color: 15
Size: 498918 Color: 12

Bin 3581: 351 of cap free
Amount of items: 2
Items: 
Size: 532839 Color: 15
Size: 466811 Color: 6

Bin 3582: 351 of cap free
Amount of items: 2
Items: 
Size: 591998 Color: 6
Size: 407652 Color: 11

Bin 3583: 351 of cap free
Amount of items: 2
Items: 
Size: 689056 Color: 18
Size: 310594 Color: 5

Bin 3584: 352 of cap free
Amount of items: 2
Items: 
Size: 515528 Color: 7
Size: 484121 Color: 17

Bin 3585: 352 of cap free
Amount of items: 2
Items: 
Size: 725967 Color: 18
Size: 273682 Color: 11

Bin 3586: 352 of cap free
Amount of items: 2
Items: 
Size: 774399 Color: 10
Size: 225250 Color: 0

Bin 3587: 353 of cap free
Amount of items: 2
Items: 
Size: 625339 Color: 9
Size: 374309 Color: 12

Bin 3588: 353 of cap free
Amount of items: 2
Items: 
Size: 737197 Color: 5
Size: 262451 Color: 14

Bin 3589: 355 of cap free
Amount of items: 2
Items: 
Size: 520532 Color: 0
Size: 479114 Color: 6

Bin 3590: 356 of cap free
Amount of items: 2
Items: 
Size: 613038 Color: 3
Size: 386607 Color: 4

Bin 3591: 356 of cap free
Amount of items: 2
Items: 
Size: 663029 Color: 12
Size: 336616 Color: 3

Bin 3592: 356 of cap free
Amount of items: 2
Items: 
Size: 723068 Color: 4
Size: 276577 Color: 1

Bin 3593: 357 of cap free
Amount of items: 2
Items: 
Size: 625338 Color: 10
Size: 374306 Color: 6

Bin 3594: 358 of cap free
Amount of items: 2
Items: 
Size: 600692 Color: 0
Size: 398951 Color: 2

Bin 3595: 358 of cap free
Amount of items: 2
Items: 
Size: 612441 Color: 9
Size: 387202 Color: 3

Bin 3596: 358 of cap free
Amount of items: 2
Items: 
Size: 686109 Color: 7
Size: 313534 Color: 4

Bin 3597: 359 of cap free
Amount of items: 2
Items: 
Size: 539524 Color: 16
Size: 460118 Color: 18

Bin 3598: 359 of cap free
Amount of items: 2
Items: 
Size: 574333 Color: 15
Size: 425309 Color: 1

Bin 3599: 360 of cap free
Amount of items: 2
Items: 
Size: 511375 Color: 16
Size: 488266 Color: 18

Bin 3600: 360 of cap free
Amount of items: 2
Items: 
Size: 756210 Color: 15
Size: 243431 Color: 7

Bin 3601: 361 of cap free
Amount of items: 2
Items: 
Size: 543397 Color: 16
Size: 456243 Color: 0

Bin 3602: 362 of cap free
Amount of items: 2
Items: 
Size: 593363 Color: 14
Size: 406276 Color: 4

Bin 3603: 362 of cap free
Amount of items: 2
Items: 
Size: 673066 Color: 8
Size: 326573 Color: 5

Bin 3604: 363 of cap free
Amount of items: 2
Items: 
Size: 664421 Color: 0
Size: 335217 Color: 15

Bin 3605: 363 of cap free
Amount of items: 2
Items: 
Size: 741772 Color: 17
Size: 257866 Color: 11

Bin 3606: 363 of cap free
Amount of items: 2
Items: 
Size: 795150 Color: 12
Size: 204488 Color: 5

Bin 3607: 364 of cap free
Amount of items: 2
Items: 
Size: 545053 Color: 1
Size: 454584 Color: 0

Bin 3608: 364 of cap free
Amount of items: 2
Items: 
Size: 615289 Color: 12
Size: 384348 Color: 4

Bin 3609: 367 of cap free
Amount of items: 3
Items: 
Size: 727552 Color: 16
Size: 139628 Color: 8
Size: 132454 Color: 5

Bin 3610: 368 of cap free
Amount of items: 2
Items: 
Size: 782305 Color: 15
Size: 217328 Color: 7

Bin 3611: 370 of cap free
Amount of items: 2
Items: 
Size: 682445 Color: 12
Size: 317186 Color: 16

Bin 3612: 371 of cap free
Amount of items: 2
Items: 
Size: 792205 Color: 11
Size: 207425 Color: 8

Bin 3613: 372 of cap free
Amount of items: 2
Items: 
Size: 598780 Color: 5
Size: 400849 Color: 19

Bin 3614: 373 of cap free
Amount of items: 2
Items: 
Size: 566377 Color: 1
Size: 433251 Color: 4

Bin 3615: 373 of cap free
Amount of items: 2
Items: 
Size: 665235 Color: 9
Size: 334393 Color: 6

Bin 3616: 373 of cap free
Amount of items: 2
Items: 
Size: 793393 Color: 14
Size: 206235 Color: 11

Bin 3617: 375 of cap free
Amount of items: 2
Items: 
Size: 527204 Color: 6
Size: 472422 Color: 13

Bin 3618: 375 of cap free
Amount of items: 2
Items: 
Size: 798664 Color: 9
Size: 200962 Color: 3

Bin 3619: 376 of cap free
Amount of items: 2
Items: 
Size: 570904 Color: 18
Size: 428721 Color: 6

Bin 3620: 376 of cap free
Amount of items: 2
Items: 
Size: 742889 Color: 1
Size: 256736 Color: 11

Bin 3621: 378 of cap free
Amount of items: 3
Items: 
Size: 523747 Color: 2
Size: 248420 Color: 7
Size: 227456 Color: 4

Bin 3622: 378 of cap free
Amount of items: 2
Items: 
Size: 559768 Color: 10
Size: 439855 Color: 15

Bin 3623: 378 of cap free
Amount of items: 2
Items: 
Size: 570170 Color: 8
Size: 429453 Color: 1

Bin 3624: 378 of cap free
Amount of items: 2
Items: 
Size: 582883 Color: 10
Size: 416740 Color: 16

Bin 3625: 378 of cap free
Amount of items: 2
Items: 
Size: 616682 Color: 18
Size: 382941 Color: 2

Bin 3626: 378 of cap free
Amount of items: 2
Items: 
Size: 629224 Color: 15
Size: 370399 Color: 2

Bin 3627: 380 of cap free
Amount of items: 2
Items: 
Size: 520515 Color: 19
Size: 479106 Color: 11

Bin 3628: 380 of cap free
Amount of items: 2
Items: 
Size: 627544 Color: 8
Size: 372077 Color: 12

Bin 3629: 382 of cap free
Amount of items: 2
Items: 
Size: 641018 Color: 9
Size: 358601 Color: 5

Bin 3630: 382 of cap free
Amount of items: 2
Items: 
Size: 782294 Color: 19
Size: 217325 Color: 4

Bin 3631: 383 of cap free
Amount of items: 2
Items: 
Size: 520605 Color: 12
Size: 479013 Color: 17

Bin 3632: 383 of cap free
Amount of items: 2
Items: 
Size: 756603 Color: 0
Size: 243015 Color: 10

Bin 3633: 384 of cap free
Amount of items: 2
Items: 
Size: 529476 Color: 2
Size: 470141 Color: 17

Bin 3634: 384 of cap free
Amount of items: 2
Items: 
Size: 543947 Color: 3
Size: 455670 Color: 6

Bin 3635: 384 of cap free
Amount of items: 2
Items: 
Size: 555811 Color: 19
Size: 443806 Color: 0

Bin 3636: 384 of cap free
Amount of items: 2
Items: 
Size: 596883 Color: 9
Size: 402734 Color: 4

Bin 3637: 385 of cap free
Amount of items: 2
Items: 
Size: 786529 Color: 16
Size: 213087 Color: 12

Bin 3638: 386 of cap free
Amount of items: 3
Items: 
Size: 349870 Color: 10
Size: 341920 Color: 2
Size: 307825 Color: 6

Bin 3639: 386 of cap free
Amount of items: 3
Items: 
Size: 708351 Color: 14
Size: 145838 Color: 2
Size: 145426 Color: 1

Bin 3640: 386 of cap free
Amount of items: 3
Items: 
Size: 719968 Color: 12
Size: 155409 Color: 8
Size: 124238 Color: 4

Bin 3641: 388 of cap free
Amount of items: 2
Items: 
Size: 760629 Color: 11
Size: 238984 Color: 16

Bin 3642: 389 of cap free
Amount of items: 2
Items: 
Size: 706366 Color: 18
Size: 293246 Color: 10

Bin 3643: 390 of cap free
Amount of items: 2
Items: 
Size: 700033 Color: 14
Size: 299578 Color: 8

Bin 3644: 391 of cap free
Amount of items: 2
Items: 
Size: 676657 Color: 12
Size: 322953 Color: 4

Bin 3645: 391 of cap free
Amount of items: 2
Items: 
Size: 685139 Color: 18
Size: 314471 Color: 1

Bin 3646: 392 of cap free
Amount of items: 2
Items: 
Size: 593439 Color: 4
Size: 406170 Color: 5

Bin 3647: 392 of cap free
Amount of items: 2
Items: 
Size: 726407 Color: 2
Size: 273202 Color: 13

Bin 3648: 392 of cap free
Amount of items: 2
Items: 
Size: 776271 Color: 19
Size: 223338 Color: 13

Bin 3649: 393 of cap free
Amount of items: 2
Items: 
Size: 506569 Color: 8
Size: 493039 Color: 14

Bin 3650: 393 of cap free
Amount of items: 2
Items: 
Size: 663790 Color: 1
Size: 335818 Color: 8

Bin 3651: 394 of cap free
Amount of items: 2
Items: 
Size: 503639 Color: 12
Size: 495968 Color: 3

Bin 3652: 394 of cap free
Amount of items: 2
Items: 
Size: 613555 Color: 0
Size: 386052 Color: 4

Bin 3653: 394 of cap free
Amount of items: 2
Items: 
Size: 767534 Color: 17
Size: 232073 Color: 0

Bin 3654: 395 of cap free
Amount of items: 2
Items: 
Size: 645600 Color: 5
Size: 354006 Color: 11

Bin 3655: 395 of cap free
Amount of items: 2
Items: 
Size: 772928 Color: 12
Size: 226678 Color: 2

Bin 3656: 399 of cap free
Amount of items: 2
Items: 
Size: 572070 Color: 9
Size: 427532 Color: 12

Bin 3657: 399 of cap free
Amount of items: 3
Items: 
Size: 724748 Color: 0
Size: 153306 Color: 8
Size: 121548 Color: 9

Bin 3658: 400 of cap free
Amount of items: 2
Items: 
Size: 600067 Color: 9
Size: 399534 Color: 15

Bin 3659: 403 of cap free
Amount of items: 2
Items: 
Size: 561282 Color: 16
Size: 438316 Color: 2

Bin 3660: 403 of cap free
Amount of items: 2
Items: 
Size: 757350 Color: 2
Size: 242248 Color: 12

Bin 3661: 404 of cap free
Amount of items: 3
Items: 
Size: 518132 Color: 14
Size: 253906 Color: 3
Size: 227559 Color: 15

Bin 3662: 404 of cap free
Amount of items: 2
Items: 
Size: 693852 Color: 11
Size: 305745 Color: 2

Bin 3663: 405 of cap free
Amount of items: 3
Items: 
Size: 585510 Color: 16
Size: 218661 Color: 16
Size: 195425 Color: 4

Bin 3664: 405 of cap free
Amount of items: 2
Items: 
Size: 613034 Color: 14
Size: 386562 Color: 10

Bin 3665: 405 of cap free
Amount of items: 2
Items: 
Size: 763647 Color: 7
Size: 235949 Color: 19

Bin 3666: 406 of cap free
Amount of items: 2
Items: 
Size: 520616 Color: 18
Size: 478979 Color: 17

Bin 3667: 406 of cap free
Amount of items: 2
Items: 
Size: 569149 Color: 17
Size: 430446 Color: 0

Bin 3668: 406 of cap free
Amount of items: 2
Items: 
Size: 746132 Color: 0
Size: 253463 Color: 9

Bin 3669: 407 of cap free
Amount of items: 2
Items: 
Size: 503503 Color: 10
Size: 496091 Color: 2

Bin 3670: 408 of cap free
Amount of items: 3
Items: 
Size: 502408 Color: 12
Size: 249121 Color: 10
Size: 248064 Color: 9

Bin 3671: 408 of cap free
Amount of items: 2
Items: 
Size: 675415 Color: 19
Size: 324178 Color: 7

Bin 3672: 408 of cap free
Amount of items: 2
Items: 
Size: 759363 Color: 12
Size: 240230 Color: 5

Bin 3673: 410 of cap free
Amount of items: 2
Items: 
Size: 603900 Color: 17
Size: 395691 Color: 5

Bin 3674: 410 of cap free
Amount of items: 2
Items: 
Size: 749652 Color: 10
Size: 249939 Color: 17

Bin 3675: 411 of cap free
Amount of items: 2
Items: 
Size: 598770 Color: 15
Size: 400820 Color: 17

Bin 3676: 411 of cap free
Amount of items: 2
Items: 
Size: 711122 Color: 4
Size: 288468 Color: 7

Bin 3677: 412 of cap free
Amount of items: 2
Items: 
Size: 661447 Color: 3
Size: 338142 Color: 19

Bin 3678: 413 of cap free
Amount of items: 2
Items: 
Size: 788345 Color: 0
Size: 211243 Color: 13

Bin 3679: 414 of cap free
Amount of items: 2
Items: 
Size: 546217 Color: 13
Size: 453370 Color: 19

Bin 3680: 414 of cap free
Amount of items: 2
Items: 
Size: 752313 Color: 14
Size: 247274 Color: 1

Bin 3681: 414 of cap free
Amount of items: 2
Items: 
Size: 779695 Color: 19
Size: 219892 Color: 13

Bin 3682: 415 of cap free
Amount of items: 2
Items: 
Size: 567959 Color: 1
Size: 431627 Color: 0

Bin 3683: 415 of cap free
Amount of items: 2
Items: 
Size: 724144 Color: 4
Size: 275442 Color: 8

Bin 3684: 416 of cap free
Amount of items: 2
Items: 
Size: 674058 Color: 11
Size: 325527 Color: 0

Bin 3685: 417 of cap free
Amount of items: 2
Items: 
Size: 529310 Color: 6
Size: 470274 Color: 5

Bin 3686: 419 of cap free
Amount of items: 2
Items: 
Size: 792175 Color: 18
Size: 207407 Color: 11

Bin 3687: 421 of cap free
Amount of items: 2
Items: 
Size: 515440 Color: 2
Size: 484140 Color: 8

Bin 3688: 421 of cap free
Amount of items: 2
Items: 
Size: 605788 Color: 12
Size: 393792 Color: 14

Bin 3689: 421 of cap free
Amount of items: 2
Items: 
Size: 685129 Color: 3
Size: 314451 Color: 9

Bin 3690: 421 of cap free
Amount of items: 2
Items: 
Size: 742149 Color: 4
Size: 257431 Color: 5

Bin 3691: 421 of cap free
Amount of items: 2
Items: 
Size: 758201 Color: 5
Size: 241379 Color: 11

Bin 3692: 423 of cap free
Amount of items: 2
Items: 
Size: 577115 Color: 6
Size: 422463 Color: 12

Bin 3693: 423 of cap free
Amount of items: 2
Items: 
Size: 594339 Color: 15
Size: 405239 Color: 2

Bin 3694: 424 of cap free
Amount of items: 2
Items: 
Size: 523373 Color: 15
Size: 476204 Color: 9

Bin 3695: 425 of cap free
Amount of items: 2
Items: 
Size: 502775 Color: 6
Size: 496801 Color: 17

Bin 3696: 425 of cap free
Amount of items: 2
Items: 
Size: 764746 Color: 5
Size: 234830 Color: 3

Bin 3697: 427 of cap free
Amount of items: 2
Items: 
Size: 623946 Color: 7
Size: 375628 Color: 2

Bin 3698: 428 of cap free
Amount of items: 2
Items: 
Size: 531435 Color: 11
Size: 468138 Color: 18

Bin 3699: 430 of cap free
Amount of items: 2
Items: 
Size: 548750 Color: 9
Size: 450821 Color: 7

Bin 3700: 430 of cap free
Amount of items: 2
Items: 
Size: 609090 Color: 3
Size: 390481 Color: 15

Bin 3701: 430 of cap free
Amount of items: 2
Items: 
Size: 763605 Color: 1
Size: 235966 Color: 7

Bin 3702: 431 of cap free
Amount of items: 2
Items: 
Size: 789017 Color: 6
Size: 210553 Color: 16

Bin 3703: 433 of cap free
Amount of items: 3
Items: 
Size: 719674 Color: 8
Size: 152428 Color: 13
Size: 127466 Color: 1

Bin 3704: 434 of cap free
Amount of items: 2
Items: 
Size: 525452 Color: 7
Size: 474115 Color: 9

Bin 3705: 435 of cap free
Amount of items: 2
Items: 
Size: 671216 Color: 12
Size: 328350 Color: 14

Bin 3706: 435 of cap free
Amount of items: 2
Items: 
Size: 679135 Color: 3
Size: 320431 Color: 18

Bin 3707: 435 of cap free
Amount of items: 2
Items: 
Size: 700927 Color: 17
Size: 298639 Color: 19

Bin 3708: 437 of cap free
Amount of items: 2
Items: 
Size: 793817 Color: 9
Size: 205747 Color: 3

Bin 3709: 439 of cap free
Amount of items: 2
Items: 
Size: 646422 Color: 8
Size: 353140 Color: 0

Bin 3710: 440 of cap free
Amount of items: 2
Items: 
Size: 530636 Color: 16
Size: 468925 Color: 9

Bin 3711: 441 of cap free
Amount of items: 2
Items: 
Size: 579770 Color: 1
Size: 419790 Color: 7

Bin 3712: 442 of cap free
Amount of items: 2
Items: 
Size: 574231 Color: 19
Size: 425328 Color: 15

Bin 3713: 443 of cap free
Amount of items: 2
Items: 
Size: 753001 Color: 8
Size: 246557 Color: 12

Bin 3714: 444 of cap free
Amount of items: 2
Items: 
Size: 501389 Color: 2
Size: 498168 Color: 4

Bin 3715: 444 of cap free
Amount of items: 2
Items: 
Size: 598169 Color: 11
Size: 401388 Color: 18

Bin 3716: 444 of cap free
Amount of items: 2
Items: 
Size: 672997 Color: 19
Size: 326560 Color: 15

Bin 3717: 445 of cap free
Amount of items: 2
Items: 
Size: 617759 Color: 15
Size: 381797 Color: 8

Bin 3718: 446 of cap free
Amount of items: 2
Items: 
Size: 577551 Color: 10
Size: 422004 Color: 2

Bin 3719: 446 of cap free
Amount of items: 2
Items: 
Size: 759327 Color: 16
Size: 240228 Color: 18

Bin 3720: 448 of cap free
Amount of items: 2
Items: 
Size: 654901 Color: 14
Size: 344652 Color: 11

Bin 3721: 448 of cap free
Amount of items: 2
Items: 
Size: 677637 Color: 5
Size: 321916 Color: 10

Bin 3722: 449 of cap free
Amount of items: 2
Items: 
Size: 598762 Color: 4
Size: 400790 Color: 1

Bin 3723: 451 of cap free
Amount of items: 2
Items: 
Size: 561702 Color: 18
Size: 437848 Color: 14

Bin 3724: 451 of cap free
Amount of items: 2
Items: 
Size: 783610 Color: 12
Size: 215940 Color: 15

Bin 3725: 453 of cap free
Amount of items: 2
Items: 
Size: 773584 Color: 19
Size: 225964 Color: 13

Bin 3726: 454 of cap free
Amount of items: 2
Items: 
Size: 540834 Color: 8
Size: 458713 Color: 0

Bin 3727: 460 of cap free
Amount of items: 2
Items: 
Size: 566068 Color: 3
Size: 433473 Color: 12

Bin 3728: 460 of cap free
Amount of items: 2
Items: 
Size: 578031 Color: 17
Size: 421510 Color: 18

Bin 3729: 460 of cap free
Amount of items: 2
Items: 
Size: 679141 Color: 18
Size: 320400 Color: 1

Bin 3730: 460 of cap free
Amount of items: 3
Items: 
Size: 742705 Color: 12
Size: 138696 Color: 13
Size: 118140 Color: 10

Bin 3731: 460 of cap free
Amount of items: 2
Items: 
Size: 788521 Color: 12
Size: 211020 Color: 16

Bin 3732: 461 of cap free
Amount of items: 2
Items: 
Size: 562184 Color: 18
Size: 437356 Color: 15

Bin 3733: 461 of cap free
Amount of items: 2
Items: 
Size: 646413 Color: 12
Size: 353127 Color: 18

Bin 3734: 461 of cap free
Amount of items: 2
Items: 
Size: 697285 Color: 8
Size: 302255 Color: 2

Bin 3735: 462 of cap free
Amount of items: 2
Items: 
Size: 752989 Color: 5
Size: 246550 Color: 10

Bin 3736: 464 of cap free
Amount of items: 2
Items: 
Size: 511276 Color: 18
Size: 488261 Color: 9

Bin 3737: 464 of cap free
Amount of items: 3
Items: 
Size: 519592 Color: 2
Size: 250550 Color: 12
Size: 229395 Color: 12

Bin 3738: 466 of cap free
Amount of items: 3
Items: 
Size: 528912 Color: 3
Size: 252092 Color: 4
Size: 218531 Color: 4

Bin 3739: 466 of cap free
Amount of items: 2
Items: 
Size: 539499 Color: 19
Size: 460036 Color: 18

Bin 3740: 466 of cap free
Amount of items: 2
Items: 
Size: 694697 Color: 8
Size: 304838 Color: 4

Bin 3741: 467 of cap free
Amount of items: 2
Items: 
Size: 719493 Color: 12
Size: 280041 Color: 4

Bin 3742: 467 of cap free
Amount of items: 2
Items: 
Size: 733403 Color: 2
Size: 266131 Color: 3

Bin 3743: 468 of cap free
Amount of items: 2
Items: 
Size: 582802 Color: 19
Size: 416731 Color: 16

Bin 3744: 471 of cap free
Amount of items: 2
Items: 
Size: 530038 Color: 12
Size: 469492 Color: 17

Bin 3745: 471 of cap free
Amount of items: 2
Items: 
Size: 657379 Color: 4
Size: 342151 Color: 18

Bin 3746: 473 of cap free
Amount of items: 2
Items: 
Size: 557966 Color: 19
Size: 441562 Color: 17

Bin 3747: 473 of cap free
Amount of items: 2
Items: 
Size: 635195 Color: 12
Size: 364333 Color: 16

Bin 3748: 474 of cap free
Amount of items: 2
Items: 
Size: 700906 Color: 9
Size: 298621 Color: 5

Bin 3749: 476 of cap free
Amount of items: 2
Items: 
Size: 674032 Color: 11
Size: 325493 Color: 5

Bin 3750: 477 of cap free
Amount of items: 2
Items: 
Size: 702073 Color: 14
Size: 297451 Color: 11

Bin 3751: 478 of cap free
Amount of items: 2
Items: 
Size: 625274 Color: 14
Size: 374249 Color: 16

Bin 3752: 480 of cap free
Amount of items: 2
Items: 
Size: 514426 Color: 17
Size: 485095 Color: 10

Bin 3753: 480 of cap free
Amount of items: 2
Items: 
Size: 515485 Color: 18
Size: 484036 Color: 6

Bin 3754: 481 of cap free
Amount of items: 3
Items: 
Size: 739913 Color: 14
Size: 141543 Color: 13
Size: 118064 Color: 5

Bin 3755: 483 of cap free
Amount of items: 2
Items: 
Size: 677624 Color: 17
Size: 321894 Color: 10

Bin 3756: 484 of cap free
Amount of items: 2
Items: 
Size: 522003 Color: 9
Size: 477514 Color: 18

Bin 3757: 484 of cap free
Amount of items: 2
Items: 
Size: 748615 Color: 5
Size: 250902 Color: 14

Bin 3758: 484 of cap free
Amount of items: 2
Items: 
Size: 752267 Color: 8
Size: 247250 Color: 4

Bin 3759: 484 of cap free
Amount of items: 2
Items: 
Size: 796523 Color: 11
Size: 202994 Color: 1

Bin 3760: 485 of cap free
Amount of items: 2
Items: 
Size: 602068 Color: 8
Size: 397448 Color: 18

Bin 3761: 486 of cap free
Amount of items: 2
Items: 
Size: 737181 Color: 15
Size: 262334 Color: 10

Bin 3762: 487 of cap free
Amount of items: 2
Items: 
Size: 649593 Color: 5
Size: 349921 Color: 7

Bin 3763: 488 of cap free
Amount of items: 2
Items: 
Size: 685071 Color: 0
Size: 314442 Color: 4

Bin 3764: 488 of cap free
Amount of items: 2
Items: 
Size: 795132 Color: 0
Size: 204381 Color: 18

Bin 3765: 490 of cap free
Amount of items: 2
Items: 
Size: 779511 Color: 5
Size: 220000 Color: 11

Bin 3766: 491 of cap free
Amount of items: 2
Items: 
Size: 567197 Color: 15
Size: 432313 Color: 14

Bin 3767: 491 of cap free
Amount of items: 2
Items: 
Size: 703613 Color: 10
Size: 295897 Color: 6

Bin 3768: 492 of cap free
Amount of items: 2
Items: 
Size: 507178 Color: 19
Size: 492331 Color: 2

Bin 3769: 492 of cap free
Amount of items: 2
Items: 
Size: 565719 Color: 1
Size: 433790 Color: 18

Bin 3770: 492 of cap free
Amount of items: 2
Items: 
Size: 703549 Color: 3
Size: 295960 Color: 10

Bin 3771: 492 of cap free
Amount of items: 2
Items: 
Size: 787476 Color: 7
Size: 212033 Color: 9

Bin 3772: 493 of cap free
Amount of items: 2
Items: 
Size: 558860 Color: 14
Size: 440648 Color: 1

Bin 3773: 494 of cap free
Amount of items: 2
Items: 
Size: 615314 Color: 4
Size: 384193 Color: 14

Bin 3774: 496 of cap free
Amount of items: 2
Items: 
Size: 553304 Color: 9
Size: 446201 Color: 0

Bin 3775: 496 of cap free
Amount of items: 2
Items: 
Size: 644085 Color: 10
Size: 355420 Color: 12

Bin 3776: 496 of cap free
Amount of items: 2
Items: 
Size: 710576 Color: 17
Size: 288929 Color: 6

Bin 3777: 498 of cap free
Amount of items: 2
Items: 
Size: 532837 Color: 5
Size: 466666 Color: 17

Bin 3778: 499 of cap free
Amount of items: 2
Items: 
Size: 530625 Color: 7
Size: 468877 Color: 15

Bin 3779: 502 of cap free
Amount of items: 2
Items: 
Size: 538768 Color: 3
Size: 460731 Color: 15

Bin 3780: 503 of cap free
Amount of items: 2
Items: 
Size: 536767 Color: 2
Size: 462731 Color: 3

Bin 3781: 505 of cap free
Amount of items: 2
Items: 
Size: 700892 Color: 18
Size: 298604 Color: 16

Bin 3782: 505 of cap free
Amount of items: 2
Items: 
Size: 760602 Color: 4
Size: 238894 Color: 5

Bin 3783: 509 of cap free
Amount of items: 2
Items: 
Size: 692656 Color: 0
Size: 306836 Color: 10

Bin 3784: 510 of cap free
Amount of items: 2
Items: 
Size: 792815 Color: 5
Size: 206676 Color: 11

Bin 3785: 512 of cap free
Amount of items: 2
Items: 
Size: 504430 Color: 6
Size: 495059 Color: 2

Bin 3786: 512 of cap free
Amount of items: 2
Items: 
Size: 526683 Color: 8
Size: 472806 Color: 7

Bin 3787: 512 of cap free
Amount of items: 2
Items: 
Size: 555764 Color: 15
Size: 443725 Color: 7

Bin 3788: 512 of cap free
Amount of items: 2
Items: 
Size: 777805 Color: 9
Size: 221684 Color: 5

Bin 3789: 513 of cap free
Amount of items: 2
Items: 
Size: 541708 Color: 6
Size: 457780 Color: 1

Bin 3790: 513 of cap free
Amount of items: 2
Items: 
Size: 636714 Color: 0
Size: 362774 Color: 17

Bin 3791: 514 of cap free
Amount of items: 2
Items: 
Size: 699152 Color: 12
Size: 300335 Color: 19

Bin 3792: 515 of cap free
Amount of items: 2
Items: 
Size: 607364 Color: 9
Size: 392122 Color: 8

Bin 3793: 515 of cap free
Amount of items: 2
Items: 
Size: 737728 Color: 12
Size: 261758 Color: 6

Bin 3794: 517 of cap free
Amount of items: 2
Items: 
Size: 584059 Color: 19
Size: 415425 Color: 10

Bin 3795: 520 of cap free
Amount of items: 2
Items: 
Size: 527604 Color: 7
Size: 471877 Color: 15

Bin 3796: 521 of cap free
Amount of items: 2
Items: 
Size: 671164 Color: 8
Size: 328316 Color: 7

Bin 3797: 525 of cap free
Amount of items: 2
Items: 
Size: 521283 Color: 1
Size: 478193 Color: 16

Bin 3798: 527 of cap free
Amount of items: 2
Items: 
Size: 598738 Color: 2
Size: 400736 Color: 16

Bin 3799: 527 of cap free
Amount of items: 2
Items: 
Size: 649577 Color: 14
Size: 349897 Color: 16

Bin 3800: 528 of cap free
Amount of items: 2
Items: 
Size: 628715 Color: 9
Size: 370758 Color: 4

Bin 3801: 528 of cap free
Amount of items: 2
Items: 
Size: 712305 Color: 12
Size: 287168 Color: 13

Bin 3802: 529 of cap free
Amount of items: 2
Items: 
Size: 716941 Color: 13
Size: 282531 Color: 3

Bin 3803: 530 of cap free
Amount of items: 2
Items: 
Size: 706226 Color: 1
Size: 293245 Color: 13

Bin 3804: 531 of cap free
Amount of items: 2
Items: 
Size: 783554 Color: 5
Size: 215916 Color: 14

Bin 3805: 534 of cap free
Amount of items: 2
Items: 
Size: 666774 Color: 16
Size: 332693 Color: 12

Bin 3806: 536 of cap free
Amount of items: 2
Items: 
Size: 500732 Color: 2
Size: 498733 Color: 1

Bin 3807: 536 of cap free
Amount of items: 2
Items: 
Size: 596797 Color: 6
Size: 402668 Color: 9

Bin 3808: 537 of cap free
Amount of items: 2
Items: 
Size: 664400 Color: 12
Size: 335064 Color: 8

Bin 3809: 538 of cap free
Amount of items: 2
Items: 
Size: 504407 Color: 6
Size: 495056 Color: 12

Bin 3810: 538 of cap free
Amount of items: 2
Items: 
Size: 570752 Color: 18
Size: 428711 Color: 10

Bin 3811: 539 of cap free
Amount of items: 2
Items: 
Size: 611598 Color: 13
Size: 387864 Color: 0

Bin 3812: 540 of cap free
Amount of items: 2
Items: 
Size: 587525 Color: 9
Size: 411936 Color: 13

Bin 3813: 540 of cap free
Amount of items: 2
Items: 
Size: 621721 Color: 15
Size: 377740 Color: 0

Bin 3814: 545 of cap free
Amount of items: 2
Items: 
Size: 617673 Color: 2
Size: 381783 Color: 9

Bin 3815: 548 of cap free
Amount of items: 2
Items: 
Size: 748585 Color: 16
Size: 250868 Color: 14

Bin 3816: 549 of cap free
Amount of items: 3
Items: 
Size: 523218 Color: 12
Size: 255171 Color: 17
Size: 221063 Color: 19

Bin 3817: 551 of cap free
Amount of items: 2
Items: 
Size: 548663 Color: 5
Size: 450787 Color: 12

Bin 3818: 551 of cap free
Amount of items: 2
Items: 
Size: 790662 Color: 17
Size: 208788 Color: 4

Bin 3819: 553 of cap free
Amount of items: 2
Items: 
Size: 759825 Color: 6
Size: 239623 Color: 11

Bin 3820: 555 of cap free
Amount of items: 2
Items: 
Size: 663683 Color: 5
Size: 335763 Color: 19

Bin 3821: 557 of cap free
Amount of items: 2
Items: 
Size: 548665 Color: 12
Size: 450779 Color: 14

Bin 3822: 558 of cap free
Amount of items: 2
Items: 
Size: 501981 Color: 17
Size: 497462 Color: 2

Bin 3823: 558 of cap free
Amount of items: 2
Items: 
Size: 766724 Color: 1
Size: 232719 Color: 8

Bin 3824: 560 of cap free
Amount of items: 2
Items: 
Size: 635104 Color: 5
Size: 364337 Color: 12

Bin 3825: 564 of cap free
Amount of items: 2
Items: 
Size: 682390 Color: 6
Size: 317047 Color: 14

Bin 3826: 569 of cap free
Amount of items: 2
Items: 
Size: 515400 Color: 19
Size: 484032 Color: 18

Bin 3827: 571 of cap free
Amount of items: 2
Items: 
Size: 565692 Color: 17
Size: 433738 Color: 6

Bin 3828: 571 of cap free
Amount of items: 2
Items: 
Size: 572015 Color: 8
Size: 427415 Color: 19

Bin 3829: 574 of cap free
Amount of items: 2
Items: 
Size: 765193 Color: 7
Size: 234234 Color: 4

Bin 3830: 575 of cap free
Amount of items: 2
Items: 
Size: 570088 Color: 17
Size: 429338 Color: 15

Bin 3831: 575 of cap free
Amount of items: 2
Items: 
Size: 668696 Color: 14
Size: 330730 Color: 13

Bin 3832: 576 of cap free
Amount of items: 2
Items: 
Size: 684997 Color: 3
Size: 314428 Color: 7

Bin 3833: 577 of cap free
Amount of items: 2
Items: 
Size: 705353 Color: 14
Size: 294071 Color: 3

Bin 3834: 578 of cap free
Amount of items: 2
Items: 
Size: 673960 Color: 9
Size: 325463 Color: 16

Bin 3835: 578 of cap free
Amount of items: 2
Items: 
Size: 798528 Color: 1
Size: 200895 Color: 7

Bin 3836: 581 of cap free
Amount of items: 2
Items: 
Size: 703544 Color: 3
Size: 295876 Color: 2

Bin 3837: 581 of cap free
Amount of items: 2
Items: 
Size: 721311 Color: 11
Size: 278109 Color: 2

Bin 3838: 582 of cap free
Amount of items: 2
Items: 
Size: 517504 Color: 2
Size: 481915 Color: 4

Bin 3839: 585 of cap free
Amount of items: 2
Items: 
Size: 569000 Color: 11
Size: 430416 Color: 14

Bin 3840: 585 of cap free
Amount of items: 2
Items: 
Size: 574486 Color: 6
Size: 424930 Color: 12

Bin 3841: 585 of cap free
Amount of items: 2
Items: 
Size: 763540 Color: 4
Size: 235876 Color: 9

Bin 3842: 587 of cap free
Amount of items: 2
Items: 
Size: 767402 Color: 12
Size: 232012 Color: 1

Bin 3843: 588 of cap free
Amount of items: 2
Items: 
Size: 525355 Color: 15
Size: 474058 Color: 1

Bin 3844: 588 of cap free
Amount of items: 3
Items: 
Size: 553503 Color: 2
Size: 228595 Color: 8
Size: 217315 Color: 11

Bin 3845: 591 of cap free
Amount of items: 2
Items: 
Size: 504383 Color: 8
Size: 495027 Color: 17

Bin 3846: 596 of cap free
Amount of items: 2
Items: 
Size: 549821 Color: 8
Size: 449584 Color: 9

Bin 3847: 599 of cap free
Amount of items: 3
Items: 
Size: 640147 Color: 9
Size: 197871 Color: 2
Size: 161384 Color: 16

Bin 3848: 602 of cap free
Amount of items: 2
Items: 
Size: 508250 Color: 8
Size: 491149 Color: 5

Bin 3849: 602 of cap free
Amount of items: 2
Items: 
Size: 765191 Color: 16
Size: 234208 Color: 1

Bin 3850: 603 of cap free
Amount of items: 2
Items: 
Size: 627371 Color: 1
Size: 372027 Color: 0

Bin 3851: 605 of cap free
Amount of items: 2
Items: 
Size: 522376 Color: 4
Size: 477020 Color: 12

Bin 3852: 606 of cap free
Amount of items: 2
Items: 
Size: 608974 Color: 7
Size: 390421 Color: 5

Bin 3853: 608 of cap free
Amount of items: 3
Items: 
Size: 716815 Color: 6
Size: 153164 Color: 8
Size: 129414 Color: 3

Bin 3854: 609 of cap free
Amount of items: 2
Items: 
Size: 572008 Color: 7
Size: 427384 Color: 8

Bin 3855: 609 of cap free
Amount of items: 2
Items: 
Size: 585932 Color: 2
Size: 413460 Color: 18

Bin 3856: 609 of cap free
Amount of items: 2
Items: 
Size: 752850 Color: 12
Size: 246542 Color: 18

Bin 3857: 609 of cap free
Amount of items: 2
Items: 
Size: 792765 Color: 3
Size: 206627 Color: 2

Bin 3858: 611 of cap free
Amount of items: 2
Items: 
Size: 720652 Color: 17
Size: 278738 Color: 13

Bin 3859: 611 of cap free
Amount of items: 2
Items: 
Size: 769019 Color: 15
Size: 230371 Color: 13

Bin 3860: 612 of cap free
Amount of items: 2
Items: 
Size: 500599 Color: 0
Size: 498790 Color: 18

Bin 3861: 612 of cap free
Amount of items: 2
Items: 
Size: 564952 Color: 16
Size: 434437 Color: 2

Bin 3862: 612 of cap free
Amount of items: 2
Items: 
Size: 618929 Color: 5
Size: 380460 Color: 9

Bin 3863: 614 of cap free
Amount of items: 2
Items: 
Size: 567122 Color: 4
Size: 432265 Color: 14

Bin 3864: 614 of cap free
Amount of items: 2
Items: 
Size: 714555 Color: 12
Size: 284832 Color: 8

Bin 3865: 616 of cap free
Amount of items: 2
Items: 
Size: 784900 Color: 1
Size: 214485 Color: 9

Bin 3866: 617 of cap free
Amount of items: 2
Items: 
Size: 538697 Color: 14
Size: 460687 Color: 13

Bin 3867: 622 of cap free
Amount of items: 2
Items: 
Size: 516742 Color: 9
Size: 482637 Color: 18

Bin 3868: 622 of cap free
Amount of items: 3
Items: 
Size: 717929 Color: 10
Size: 146857 Color: 13
Size: 134593 Color: 15

Bin 3869: 626 of cap free
Amount of items: 2
Items: 
Size: 546065 Color: 5
Size: 453310 Color: 10

Bin 3870: 627 of cap free
Amount of items: 2
Items: 
Size: 519565 Color: 19
Size: 479809 Color: 17

Bin 3871: 629 of cap free
Amount of items: 2
Items: 
Size: 726370 Color: 4
Size: 273002 Color: 10

Bin 3872: 631 of cap free
Amount of items: 2
Items: 
Size: 699084 Color: 7
Size: 300286 Color: 15

Bin 3873: 632 of cap free
Amount of items: 2
Items: 
Size: 678208 Color: 1
Size: 321161 Color: 2

Bin 3874: 634 of cap free
Amount of items: 2
Items: 
Size: 616615 Color: 10
Size: 382752 Color: 1

Bin 3875: 634 of cap free
Amount of items: 2
Items: 
Size: 767392 Color: 2
Size: 231975 Color: 17

Bin 3876: 641 of cap free
Amount of items: 2
Items: 
Size: 512444 Color: 5
Size: 486916 Color: 6

Bin 3877: 642 of cap free
Amount of items: 2
Items: 
Size: 523724 Color: 12
Size: 475635 Color: 17

Bin 3878: 646 of cap free
Amount of items: 3
Items: 
Size: 349544 Color: 18
Size: 341133 Color: 13
Size: 308678 Color: 1

Bin 3879: 646 of cap free
Amount of items: 2
Items: 
Size: 705340 Color: 16
Size: 294015 Color: 14

Bin 3880: 648 of cap free
Amount of items: 2
Items: 
Size: 554621 Color: 11
Size: 444732 Color: 16

Bin 3881: 649 of cap free
Amount of items: 2
Items: 
Size: 631662 Color: 0
Size: 367690 Color: 2

Bin 3882: 650 of cap free
Amount of items: 2
Items: 
Size: 765190 Color: 7
Size: 234161 Color: 0

Bin 3883: 652 of cap free
Amount of items: 3
Items: 
Size: 556455 Color: 1
Size: 221450 Color: 7
Size: 221444 Color: 8

Bin 3884: 652 of cap free
Amount of items: 2
Items: 
Size: 589632 Color: 4
Size: 409717 Color: 18

Bin 3885: 653 of cap free
Amount of items: 2
Items: 
Size: 571973 Color: 11
Size: 427375 Color: 6

Bin 3886: 653 of cap free
Amount of items: 2
Items: 
Size: 775174 Color: 11
Size: 224174 Color: 7

Bin 3887: 654 of cap free
Amount of items: 2
Items: 
Size: 511240 Color: 17
Size: 488107 Color: 0

Bin 3888: 656 of cap free
Amount of items: 2
Items: 
Size: 568970 Color: 10
Size: 430375 Color: 5

Bin 3889: 656 of cap free
Amount of items: 2
Items: 
Size: 730576 Color: 14
Size: 268769 Color: 8

Bin 3890: 657 of cap free
Amount of items: 2
Items: 
Size: 673955 Color: 0
Size: 325389 Color: 5

Bin 3891: 663 of cap free
Amount of items: 2
Items: 
Size: 580939 Color: 12
Size: 418399 Color: 4

Bin 3892: 665 of cap free
Amount of items: 3
Items: 
Size: 500637 Color: 1
Size: 250586 Color: 10
Size: 248113 Color: 18

Bin 3893: 665 of cap free
Amount of items: 2
Items: 
Size: 784875 Color: 4
Size: 214461 Color: 8

Bin 3894: 669 of cap free
Amount of items: 2
Items: 
Size: 705185 Color: 4
Size: 294147 Color: 8

Bin 3895: 671 of cap free
Amount of items: 2
Items: 
Size: 502625 Color: 6
Size: 496705 Color: 11

Bin 3896: 671 of cap free
Amount of items: 2
Items: 
Size: 687856 Color: 16
Size: 311474 Color: 11

Bin 3897: 675 of cap free
Amount of items: 2
Items: 
Size: 580939 Color: 4
Size: 418387 Color: 6

Bin 3898: 675 of cap free
Amount of items: 2
Items: 
Size: 598594 Color: 18
Size: 400732 Color: 12

Bin 3899: 675 of cap free
Amount of items: 2
Items: 
Size: 620036 Color: 12
Size: 379290 Color: 11

Bin 3900: 677 of cap free
Amount of items: 2
Items: 
Size: 555682 Color: 19
Size: 443642 Color: 12

Bin 3901: 678 of cap free
Amount of items: 2
Items: 
Size: 571953 Color: 7
Size: 427370 Color: 14

Bin 3902: 678 of cap free
Amount of items: 2
Items: 
Size: 616581 Color: 1
Size: 382742 Color: 14

Bin 3903: 681 of cap free
Amount of items: 3
Items: 
Size: 709953 Color: 17
Size: 148991 Color: 12
Size: 140376 Color: 15

Bin 3904: 686 of cap free
Amount of items: 2
Items: 
Size: 570058 Color: 7
Size: 429257 Color: 10

Bin 3905: 687 of cap free
Amount of items: 2
Items: 
Size: 505265 Color: 19
Size: 494049 Color: 0

Bin 3906: 687 of cap free
Amount of items: 2
Items: 
Size: 537726 Color: 1
Size: 461588 Color: 14

Bin 3907: 690 of cap free
Amount of items: 2
Items: 
Size: 726321 Color: 17
Size: 272990 Color: 18

Bin 3908: 691 of cap free
Amount of items: 2
Items: 
Size: 768421 Color: 17
Size: 230889 Color: 2

Bin 3909: 692 of cap free
Amount of items: 2
Items: 
Size: 584053 Color: 7
Size: 415256 Color: 15

Bin 3910: 693 of cap free
Amount of items: 2
Items: 
Size: 577933 Color: 17
Size: 421375 Color: 4

Bin 3911: 696 of cap free
Amount of items: 2
Items: 
Size: 603864 Color: 2
Size: 395441 Color: 9

Bin 3912: 699 of cap free
Amount of items: 2
Items: 
Size: 684968 Color: 19
Size: 314334 Color: 18

Bin 3913: 700 of cap free
Amount of items: 2
Items: 
Size: 564978 Color: 5
Size: 434323 Color: 12

Bin 3914: 707 of cap free
Amount of items: 2
Items: 
Size: 654701 Color: 18
Size: 344593 Color: 15

Bin 3915: 713 of cap free
Amount of items: 2
Items: 
Size: 545980 Color: 12
Size: 453308 Color: 16

Bin 3916: 713 of cap free
Amount of items: 2
Items: 
Size: 638401 Color: 13
Size: 360887 Color: 2

Bin 3917: 714 of cap free
Amount of items: 2
Items: 
Size: 609964 Color: 17
Size: 389323 Color: 10

Bin 3918: 715 of cap free
Amount of items: 2
Items: 
Size: 543941 Color: 1
Size: 455345 Color: 16

Bin 3919: 717 of cap free
Amount of items: 2
Items: 
Size: 500400 Color: 16
Size: 498884 Color: 0

Bin 3920: 717 of cap free
Amount of items: 2
Items: 
Size: 567545 Color: 16
Size: 431739 Color: 12

Bin 3921: 718 of cap free
Amount of items: 2
Items: 
Size: 504358 Color: 18
Size: 494925 Color: 9

Bin 3922: 724 of cap free
Amount of items: 2
Items: 
Size: 676337 Color: 15
Size: 322940 Color: 11

Bin 3923: 730 of cap free
Amount of items: 2
Items: 
Size: 568917 Color: 3
Size: 430354 Color: 12

Bin 3924: 731 of cap free
Amount of items: 2
Items: 
Size: 508169 Color: 1
Size: 491101 Color: 7

Bin 3925: 731 of cap free
Amount of items: 2
Items: 
Size: 673928 Color: 9
Size: 325342 Color: 3

Bin 3926: 737 of cap free
Amount of items: 3
Items: 
Size: 532791 Color: 2
Size: 247827 Color: 2
Size: 218646 Color: 18

Bin 3927: 739 of cap free
Amount of items: 3
Items: 
Size: 358279 Color: 15
Size: 337569 Color: 19
Size: 303414 Color: 7

Bin 3928: 740 of cap free
Amount of items: 2
Items: 
Size: 784844 Color: 9
Size: 214417 Color: 19

Bin 3929: 741 of cap free
Amount of items: 2
Items: 
Size: 589551 Color: 5
Size: 409709 Color: 15

Bin 3930: 742 of cap free
Amount of items: 2
Items: 
Size: 541683 Color: 13
Size: 457576 Color: 6

Bin 3931: 743 of cap free
Amount of items: 2
Items: 
Size: 519484 Color: 15
Size: 479774 Color: 0

Bin 3932: 745 of cap free
Amount of items: 2
Items: 
Size: 627240 Color: 13
Size: 372016 Color: 17

Bin 3933: 747 of cap free
Amount of items: 2
Items: 
Size: 505240 Color: 19
Size: 494014 Color: 4

Bin 3934: 751 of cap free
Amount of items: 2
Items: 
Size: 580935 Color: 1
Size: 418315 Color: 6

Bin 3935: 755 of cap free
Amount of items: 2
Items: 
Size: 508154 Color: 13
Size: 491092 Color: 18

Bin 3936: 756 of cap free
Amount of items: 2
Items: 
Size: 763445 Color: 9
Size: 235800 Color: 19

Bin 3937: 758 of cap free
Amount of items: 2
Items: 
Size: 664260 Color: 3
Size: 334983 Color: 14

Bin 3938: 759 of cap free
Amount of items: 2
Items: 
Size: 705287 Color: 7
Size: 293955 Color: 4

Bin 3939: 760 of cap free
Amount of items: 2
Items: 
Size: 593080 Color: 16
Size: 406161 Color: 2

Bin 3940: 762 of cap free
Amount of items: 3
Items: 
Size: 670743 Color: 13
Size: 174302 Color: 13
Size: 154194 Color: 9

Bin 3941: 762 of cap free
Amount of items: 2
Items: 
Size: 693795 Color: 14
Size: 305444 Color: 17

Bin 3942: 763 of cap free
Amount of items: 2
Items: 
Size: 605795 Color: 14
Size: 393443 Color: 8

Bin 3943: 766 of cap free
Amount of items: 2
Items: 
Size: 790587 Color: 18
Size: 208648 Color: 7

Bin 3944: 773 of cap free
Amount of items: 2
Items: 
Size: 508144 Color: 13
Size: 491084 Color: 12

Bin 3945: 775 of cap free
Amount of items: 2
Items: 
Size: 768433 Color: 2
Size: 230793 Color: 8

Bin 3946: 777 of cap free
Amount of items: 2
Items: 
Size: 518439 Color: 4
Size: 480785 Color: 12

Bin 3947: 781 of cap free
Amount of items: 2
Items: 
Size: 777717 Color: 7
Size: 221503 Color: 6

Bin 3948: 782 of cap free
Amount of items: 2
Items: 
Size: 664252 Color: 1
Size: 334967 Color: 0

Bin 3949: 784 of cap free
Amount of items: 2
Items: 
Size: 532554 Color: 7
Size: 466663 Color: 13

Bin 3950: 784 of cap free
Amount of items: 2
Items: 
Size: 589510 Color: 11
Size: 409707 Color: 7

Bin 3951: 784 of cap free
Amount of items: 2
Items: 
Size: 757282 Color: 4
Size: 241935 Color: 8

Bin 3952: 786 of cap free
Amount of items: 2
Items: 
Size: 697041 Color: 3
Size: 302174 Color: 17

Bin 3953: 789 of cap free
Amount of items: 2
Items: 
Size: 501789 Color: 11
Size: 497423 Color: 7

Bin 3954: 790 of cap free
Amount of items: 2
Items: 
Size: 686104 Color: 4
Size: 313107 Color: 0

Bin 3955: 795 of cap free
Amount of items: 2
Items: 
Size: 748419 Color: 8
Size: 250787 Color: 5

Bin 3956: 798 of cap free
Amount of items: 2
Items: 
Size: 678943 Color: 18
Size: 320260 Color: 19

Bin 3957: 798 of cap free
Amount of items: 2
Items: 
Size: 794921 Color: 5
Size: 204282 Color: 11

Bin 3958: 799 of cap free
Amount of items: 2
Items: 
Size: 593125 Color: 12
Size: 406077 Color: 13

Bin 3959: 806 of cap free
Amount of items: 2
Items: 
Size: 504295 Color: 17
Size: 494900 Color: 9

Bin 3960: 807 of cap free
Amount of items: 2
Items: 
Size: 521135 Color: 15
Size: 478059 Color: 12

Bin 3961: 807 of cap free
Amount of items: 2
Items: 
Size: 545025 Color: 13
Size: 454169 Color: 7

Bin 3962: 808 of cap free
Amount of items: 2
Items: 
Size: 501785 Color: 3
Size: 497408 Color: 15

Bin 3963: 813 of cap free
Amount of items: 3
Items: 
Size: 518749 Color: 6
Size: 251604 Color: 1
Size: 228835 Color: 18

Bin 3964: 815 of cap free
Amount of items: 2
Items: 
Size: 522552 Color: 15
Size: 476634 Color: 13

Bin 3965: 815 of cap free
Amount of items: 2
Items: 
Size: 709086 Color: 4
Size: 290100 Color: 7

Bin 3966: 823 of cap free
Amount of items: 2
Items: 
Size: 583928 Color: 9
Size: 415250 Color: 13

Bin 3967: 824 of cap free
Amount of items: 2
Items: 
Size: 506157 Color: 6
Size: 493020 Color: 3

Bin 3968: 826 of cap free
Amount of items: 2
Items: 
Size: 769414 Color: 6
Size: 229761 Color: 19

Bin 3969: 828 of cap free
Amount of items: 2
Items: 
Size: 741751 Color: 16
Size: 257422 Color: 5

Bin 3970: 829 of cap free
Amount of items: 2
Items: 
Size: 766563 Color: 19
Size: 232609 Color: 2

Bin 3971: 830 of cap free
Amount of items: 2
Items: 
Size: 697037 Color: 6
Size: 302134 Color: 15

Bin 3972: 831 of cap free
Amount of items: 2
Items: 
Size: 784759 Color: 4
Size: 214411 Color: 11

Bin 3973: 833 of cap free
Amount of items: 2
Items: 
Size: 760628 Color: 5
Size: 238540 Color: 15

Bin 3974: 834 of cap free
Amount of items: 2
Items: 
Size: 700637 Color: 10
Size: 298530 Color: 13

Bin 3975: 837 of cap free
Amount of items: 2
Items: 
Size: 512258 Color: 10
Size: 486906 Color: 15

Bin 3976: 841 of cap free
Amount of items: 2
Items: 
Size: 691921 Color: 17
Size: 307239 Color: 13

Bin 3977: 843 of cap free
Amount of items: 2
Items: 
Size: 678920 Color: 18
Size: 320238 Color: 15

Bin 3978: 846 of cap free
Amount of items: 2
Items: 
Size: 508087 Color: 16
Size: 491068 Color: 19

Bin 3979: 847 of cap free
Amount of items: 2
Items: 
Size: 600252 Color: 19
Size: 398902 Color: 6

Bin 3980: 850 of cap free
Amount of items: 2
Items: 
Size: 633593 Color: 18
Size: 365558 Color: 4

Bin 3981: 853 of cap free
Amount of items: 2
Items: 
Size: 558341 Color: 2
Size: 440807 Color: 3

Bin 3982: 853 of cap free
Amount of items: 2
Items: 
Size: 698968 Color: 6
Size: 300180 Color: 2

Bin 3983: 855 of cap free
Amount of items: 2
Items: 
Size: 790581 Color: 0
Size: 208565 Color: 6

Bin 3984: 856 of cap free
Amount of items: 2
Items: 
Size: 544984 Color: 10
Size: 454161 Color: 5

Bin 3985: 856 of cap free
Amount of items: 2
Items: 
Size: 670278 Color: 8
Size: 328867 Color: 2

Bin 3986: 862 of cap free
Amount of items: 2
Items: 
Size: 550953 Color: 2
Size: 448186 Color: 3

Bin 3987: 870 of cap free
Amount of items: 3
Items: 
Size: 626619 Color: 14
Size: 195645 Color: 6
Size: 176867 Color: 0

Bin 3988: 873 of cap free
Amount of items: 2
Items: 
Size: 716599 Color: 13
Size: 282529 Color: 2

Bin 3989: 876 of cap free
Amount of items: 2
Items: 
Size: 549757 Color: 6
Size: 449368 Color: 17

Bin 3990: 877 of cap free
Amount of items: 2
Items: 
Size: 543874 Color: 5
Size: 455250 Color: 13

Bin 3991: 878 of cap free
Amount of items: 2
Items: 
Size: 585695 Color: 19
Size: 413428 Color: 18

Bin 3992: 882 of cap free
Amount of items: 2
Items: 
Size: 670274 Color: 8
Size: 328845 Color: 6

Bin 3993: 882 of cap free
Amount of items: 2
Items: 
Size: 695702 Color: 19
Size: 303417 Color: 17

Bin 3994: 889 of cap free
Amount of items: 2
Items: 
Size: 648148 Color: 2
Size: 350964 Color: 8

Bin 3995: 891 of cap free
Amount of items: 2
Items: 
Size: 673820 Color: 4
Size: 325290 Color: 12

Bin 3996: 897 of cap free
Amount of items: 2
Items: 
Size: 796147 Color: 11
Size: 202957 Color: 13

Bin 3997: 898 of cap free
Amount of items: 3
Items: 
Size: 576904 Color: 10
Size: 224080 Color: 3
Size: 198119 Color: 19

Bin 3998: 899 of cap free
Amount of items: 2
Items: 
Size: 536258 Color: 10
Size: 462844 Color: 4

Bin 3999: 900 of cap free
Amount of items: 2
Items: 
Size: 706222 Color: 15
Size: 292879 Color: 5

Bin 4000: 902 of cap free
Amount of items: 2
Items: 
Size: 515339 Color: 12
Size: 483760 Color: 6

Bin 4001: 905 of cap free
Amount of items: 2
Items: 
Size: 666495 Color: 10
Size: 332601 Color: 8

Bin 4002: 911 of cap free
Amount of items: 2
Items: 
Size: 632004 Color: 19
Size: 367086 Color: 11

Bin 4003: 912 of cap free
Amount of items: 2
Items: 
Size: 627067 Color: 5
Size: 372022 Color: 13

Bin 4004: 913 of cap free
Amount of items: 2
Items: 
Size: 686096 Color: 12
Size: 312992 Color: 2

Bin 4005: 916 of cap free
Amount of items: 3
Items: 
Size: 715023 Color: 13
Size: 158302 Color: 8
Size: 125760 Color: 15

Bin 4006: 918 of cap free
Amount of items: 2
Items: 
Size: 677241 Color: 18
Size: 321842 Color: 6

Bin 4007: 918 of cap free
Amount of items: 2
Items: 
Size: 783570 Color: 14
Size: 215513 Color: 16

Bin 4008: 919 of cap free
Amount of items: 2
Items: 
Size: 716569 Color: 13
Size: 282513 Color: 10

Bin 4009: 924 of cap free
Amount of items: 2
Items: 
Size: 506478 Color: 4
Size: 492599 Color: 10

Bin 4010: 926 of cap free
Amount of items: 2
Items: 
Size: 571769 Color: 14
Size: 427306 Color: 1

Bin 4011: 926 of cap free
Amount of items: 2
Items: 
Size: 609905 Color: 7
Size: 389170 Color: 1

Bin 4012: 928 of cap free
Amount of items: 2
Items: 
Size: 736785 Color: 17
Size: 262288 Color: 18

Bin 4013: 930 of cap free
Amount of items: 3
Items: 
Size: 519034 Color: 13
Size: 250783 Color: 5
Size: 229254 Color: 1

Bin 4014: 932 of cap free
Amount of items: 2
Items: 
Size: 673810 Color: 3
Size: 325259 Color: 2

Bin 4015: 943 of cap free
Amount of items: 3
Items: 
Size: 500200 Color: 17
Size: 249491 Color: 15
Size: 249367 Color: 11

Bin 4016: 943 of cap free
Amount of items: 3
Items: 
Size: 576900 Color: 19
Size: 224067 Color: 1
Size: 198091 Color: 8

Bin 4017: 945 of cap free
Amount of items: 2
Items: 
Size: 774927 Color: 2
Size: 224129 Color: 7

Bin 4018: 948 of cap free
Amount of items: 2
Items: 
Size: 523180 Color: 2
Size: 475873 Color: 3

Bin 4019: 951 of cap free
Amount of items: 2
Items: 
Size: 554526 Color: 9
Size: 444524 Color: 13

Bin 4020: 953 of cap free
Amount of items: 2
Items: 
Size: 741933 Color: 11
Size: 257115 Color: 13

Bin 4021: 954 of cap free
Amount of items: 2
Items: 
Size: 555582 Color: 3
Size: 443465 Color: 12

Bin 4022: 956 of cap free
Amount of items: 2
Items: 
Size: 514117 Color: 6
Size: 484928 Color: 17

Bin 4023: 957 of cap free
Amount of items: 2
Items: 
Size: 677218 Color: 2
Size: 321826 Color: 8

Bin 4024: 957 of cap free
Amount of items: 2
Items: 
Size: 686067 Color: 17
Size: 312977 Color: 0

Bin 4025: 966 of cap free
Amount of items: 2
Items: 
Size: 659253 Color: 15
Size: 339782 Color: 17

Bin 4026: 966 of cap free
Amount of items: 2
Items: 
Size: 706183 Color: 5
Size: 292852 Color: 1

Bin 4027: 968 of cap free
Amount of items: 2
Items: 
Size: 645366 Color: 13
Size: 353667 Color: 18

Bin 4028: 976 of cap free
Amount of items: 2
Items: 
Size: 536190 Color: 13
Size: 462835 Color: 4

Bin 4029: 979 of cap free
Amount of items: 2
Items: 
Size: 517021 Color: 3
Size: 482001 Color: 17

Bin 4030: 980 of cap free
Amount of items: 2
Items: 
Size: 577921 Color: 3
Size: 421100 Color: 17

Bin 4031: 981 of cap free
Amount of items: 2
Items: 
Size: 790497 Color: 4
Size: 208523 Color: 9

Bin 4032: 981 of cap free
Amount of items: 2
Items: 
Size: 799486 Color: 4
Size: 199534 Color: 6

Bin 4033: 982 of cap free
Amount of items: 2
Items: 
Size: 589340 Color: 13
Size: 409679 Color: 19

Bin 4034: 990 of cap free
Amount of items: 2
Items: 
Size: 698831 Color: 1
Size: 300180 Color: 15

Bin 4035: 993 of cap free
Amount of items: 2
Items: 
Size: 561696 Color: 7
Size: 437312 Color: 10

Bin 4036: 999 of cap free
Amount of items: 2
Items: 
Size: 563174 Color: 11
Size: 435828 Color: 0

Bin 4037: 1001 of cap free
Amount of items: 2
Items: 
Size: 579231 Color: 11
Size: 419769 Color: 9

Bin 4038: 1005 of cap free
Amount of items: 2
Items: 
Size: 559701 Color: 12
Size: 439295 Color: 14

Bin 4039: 1016 of cap free
Amount of items: 2
Items: 
Size: 548458 Color: 8
Size: 450527 Color: 11

Bin 4040: 1020 of cap free
Amount of items: 2
Items: 
Size: 567654 Color: 6
Size: 431327 Color: 12

Bin 4041: 1025 of cap free
Amount of items: 2
Items: 
Size: 507919 Color: 3
Size: 491057 Color: 7

Bin 4042: 1027 of cap free
Amount of items: 2
Items: 
Size: 706133 Color: 5
Size: 292841 Color: 7

Bin 4043: 1036 of cap free
Amount of items: 2
Items: 
Size: 556571 Color: 2
Size: 442394 Color: 3

Bin 4044: 1036 of cap free
Amount of items: 2
Items: 
Size: 698812 Color: 9
Size: 300153 Color: 11

Bin 4045: 1041 of cap free
Amount of items: 2
Items: 
Size: 512062 Color: 14
Size: 486898 Color: 13

Bin 4046: 1043 of cap free
Amount of items: 2
Items: 
Size: 612980 Color: 12
Size: 385978 Color: 17

Bin 4047: 1052 of cap free
Amount of items: 2
Items: 
Size: 601547 Color: 6
Size: 397402 Color: 16

Bin 4048: 1052 of cap free
Amount of items: 2
Items: 
Size: 616470 Color: 13
Size: 382479 Color: 2

Bin 4049: 1055 of cap free
Amount of items: 2
Items: 
Size: 581793 Color: 1
Size: 417153 Color: 2

Bin 4050: 1064 of cap free
Amount of items: 3
Items: 
Size: 528482 Color: 17
Size: 251943 Color: 2
Size: 218512 Color: 9

Bin 4051: 1064 of cap free
Amount of items: 2
Items: 
Size: 741742 Color: 14
Size: 257195 Color: 3

Bin 4052: 1065 of cap free
Amount of items: 2
Items: 
Size: 790473 Color: 17
Size: 208463 Color: 7

Bin 4053: 1066 of cap free
Amount of items: 2
Items: 
Size: 554447 Color: 12
Size: 444488 Color: 14

Bin 4054: 1071 of cap free
Amount of items: 3
Items: 
Size: 516949 Color: 11
Size: 259399 Color: 12
Size: 222582 Color: 19

Bin 4055: 1071 of cap free
Amount of items: 2
Items: 
Size: 524799 Color: 2
Size: 474131 Color: 3

Bin 4056: 1076 of cap free
Amount of items: 2
Items: 
Size: 616555 Color: 3
Size: 382370 Color: 1

Bin 4057: 1077 of cap free
Amount of items: 2
Items: 
Size: 569837 Color: 11
Size: 429087 Color: 18

Bin 4058: 1079 of cap free
Amount of items: 2
Items: 
Size: 573887 Color: 2
Size: 425035 Color: 13

Bin 4059: 1087 of cap free
Amount of items: 2
Items: 
Size: 666458 Color: 18
Size: 332456 Color: 5

Bin 4060: 1088 of cap free
Amount of items: 2
Items: 
Size: 595187 Color: 10
Size: 403726 Color: 14

Bin 4061: 1088 of cap free
Amount of items: 2
Items: 
Size: 792658 Color: 13
Size: 206255 Color: 14

Bin 4062: 1089 of cap free
Amount of items: 2
Items: 
Size: 608657 Color: 9
Size: 390255 Color: 0

Bin 4063: 1095 of cap free
Amount of items: 2
Items: 
Size: 507908 Color: 17
Size: 490998 Color: 19

Bin 4064: 1097 of cap free
Amount of items: 2
Items: 
Size: 579157 Color: 19
Size: 419747 Color: 15

Bin 4065: 1103 of cap free
Amount of items: 2
Items: 
Size: 659200 Color: 9
Size: 339698 Color: 15

Bin 4066: 1103 of cap free
Amount of items: 2
Items: 
Size: 706133 Color: 18
Size: 292765 Color: 19

Bin 4067: 1107 of cap free
Amount of items: 3
Items: 
Size: 770068 Color: 8
Size: 114699 Color: 10
Size: 114127 Color: 15

Bin 4068: 1109 of cap free
Amount of items: 2
Items: 
Size: 561611 Color: 13
Size: 437281 Color: 11

Bin 4069: 1110 of cap free
Amount of items: 3
Items: 
Size: 500185 Color: 6
Size: 249365 Color: 0
Size: 249341 Color: 6

Bin 4070: 1115 of cap free
Amount of items: 2
Items: 
Size: 631726 Color: 14
Size: 367160 Color: 19

Bin 4071: 1115 of cap free
Amount of items: 2
Items: 
Size: 686013 Color: 2
Size: 312873 Color: 7

Bin 4072: 1116 of cap free
Amount of items: 2
Items: 
Size: 673739 Color: 3
Size: 325146 Color: 9

Bin 4073: 1118 of cap free
Amount of items: 2
Items: 
Size: 554428 Color: 8
Size: 444455 Color: 17

Bin 4074: 1119 of cap free
Amount of items: 2
Items: 
Size: 764732 Color: 8
Size: 234150 Color: 6

Bin 4075: 1130 of cap free
Amount of items: 2
Items: 
Size: 643865 Color: 0
Size: 355006 Color: 6

Bin 4076: 1135 of cap free
Amount of items: 2
Items: 
Size: 755865 Color: 18
Size: 243001 Color: 19

Bin 4077: 1140 of cap free
Amount of items: 2
Items: 
Size: 516715 Color: 3
Size: 482146 Color: 19

Bin 4078: 1141 of cap free
Amount of items: 2
Items: 
Size: 511993 Color: 11
Size: 486867 Color: 12

Bin 4079: 1141 of cap free
Amount of items: 2
Items: 
Size: 541385 Color: 8
Size: 457475 Color: 1

Bin 4080: 1149 of cap free
Amount of items: 2
Items: 
Size: 583940 Color: 13
Size: 414912 Color: 15

Bin 4081: 1151 of cap free
Amount of items: 2
Items: 
Size: 785847 Color: 17
Size: 213003 Color: 2

Bin 4082: 1157 of cap free
Amount of items: 2
Items: 
Size: 738835 Color: 7
Size: 260009 Color: 13

Bin 4083: 1167 of cap free
Amount of items: 2
Items: 
Size: 598146 Color: 18
Size: 400688 Color: 11

Bin 4084: 1173 of cap free
Amount of items: 2
Items: 
Size: 580544 Color: 7
Size: 418284 Color: 9

Bin 4085: 1174 of cap free
Amount of items: 2
Items: 
Size: 507859 Color: 9
Size: 490968 Color: 5

Bin 4086: 1174 of cap free
Amount of items: 2
Items: 
Size: 548313 Color: 7
Size: 450514 Color: 4

Bin 4087: 1175 of cap free
Amount of items: 2
Items: 
Size: 799467 Color: 0
Size: 199359 Color: 19

Bin 4088: 1179 of cap free
Amount of items: 2
Items: 
Size: 595136 Color: 5
Size: 403686 Color: 1

Bin 4089: 1180 of cap free
Amount of items: 2
Items: 
Size: 501441 Color: 3
Size: 497380 Color: 17

Bin 4090: 1183 of cap free
Amount of items: 3
Items: 
Size: 359648 Color: 7
Size: 352470 Color: 1
Size: 286700 Color: 11

Bin 4091: 1196 of cap free
Amount of items: 2
Items: 
Size: 759268 Color: 12
Size: 239537 Color: 9

Bin 4092: 1197 of cap free
Amount of items: 2
Items: 
Size: 512048 Color: 12
Size: 486756 Color: 16

Bin 4093: 1208 of cap free
Amount of items: 2
Items: 
Size: 592813 Color: 12
Size: 405980 Color: 6

Bin 4094: 1210 of cap free
Amount of items: 2
Items: 
Size: 603690 Color: 2
Size: 395101 Color: 16

Bin 4095: 1218 of cap free
Amount of items: 2
Items: 
Size: 599838 Color: 7
Size: 398945 Color: 13

Bin 4096: 1219 of cap free
Amount of items: 2
Items: 
Size: 775454 Color: 18
Size: 223328 Color: 13

Bin 4097: 1224 of cap free
Amount of items: 2
Items: 
Size: 685996 Color: 17
Size: 312781 Color: 7

Bin 4098: 1232 of cap free
Amount of items: 2
Items: 
Size: 563093 Color: 6
Size: 435676 Color: 5

Bin 4099: 1233 of cap free
Amount of items: 2
Items: 
Size: 559679 Color: 3
Size: 439089 Color: 4

Bin 4100: 1236 of cap free
Amount of items: 2
Items: 
Size: 710293 Color: 16
Size: 288472 Color: 4

Bin 4101: 1242 of cap free
Amount of items: 2
Items: 
Size: 796239 Color: 16
Size: 202520 Color: 1

Bin 4102: 1247 of cap free
Amount of items: 2
Items: 
Size: 799815 Color: 8
Size: 198939 Color: 13

Bin 4103: 1253 of cap free
Amount of items: 2
Items: 
Size: 631200 Color: 17
Size: 367548 Color: 2

Bin 4104: 1257 of cap free
Amount of items: 2
Items: 
Size: 673614 Color: 2
Size: 325130 Color: 15

Bin 4105: 1262 of cap free
Amount of items: 2
Items: 
Size: 592764 Color: 2
Size: 405975 Color: 15

Bin 4106: 1269 of cap free
Amount of items: 2
Items: 
Size: 580787 Color: 9
Size: 417945 Color: 4

Bin 4107: 1271 of cap free
Amount of items: 2
Items: 
Size: 527393 Color: 18
Size: 471337 Color: 4

Bin 4108: 1272 of cap free
Amount of items: 2
Items: 
Size: 541367 Color: 4
Size: 457362 Color: 6

Bin 4109: 1275 of cap free
Amount of items: 2
Items: 
Size: 759220 Color: 18
Size: 239506 Color: 12

Bin 4110: 1278 of cap free
Amount of items: 2
Items: 
Size: 631679 Color: 14
Size: 367044 Color: 6

Bin 4111: 1291 of cap free
Amount of items: 2
Items: 
Size: 513975 Color: 18
Size: 484735 Color: 3

Bin 4112: 1294 of cap free
Amount of items: 2
Items: 
Size: 783220 Color: 11
Size: 215487 Color: 5

Bin 4113: 1297 of cap free
Amount of items: 2
Items: 
Size: 588115 Color: 3
Size: 410589 Color: 13

Bin 4114: 1298 of cap free
Amount of items: 2
Items: 
Size: 667725 Color: 9
Size: 330978 Color: 6

Bin 4115: 1299 of cap free
Amount of items: 3
Items: 
Size: 513255 Color: 12
Size: 257140 Color: 18
Size: 228307 Color: 9

Bin 4116: 1300 of cap free
Amount of items: 2
Items: 
Size: 785686 Color: 19
Size: 213015 Color: 17

Bin 4117: 1301 of cap free
Amount of items: 2
Items: 
Size: 548274 Color: 17
Size: 450426 Color: 4

Bin 4118: 1302 of cap free
Amount of items: 2
Items: 
Size: 543575 Color: 0
Size: 455124 Color: 7

Bin 4119: 1304 of cap free
Amount of items: 2
Items: 
Size: 535880 Color: 11
Size: 462817 Color: 4

Bin 4120: 1308 of cap free
Amount of items: 2
Items: 
Size: 685951 Color: 18
Size: 312742 Color: 0

Bin 4121: 1316 of cap free
Amount of items: 2
Items: 
Size: 628671 Color: 13
Size: 370014 Color: 2

Bin 4122: 1345 of cap free
Amount of items: 2
Items: 
Size: 592706 Color: 6
Size: 405950 Color: 17

Bin 4123: 1347 of cap free
Amount of items: 2
Items: 
Size: 518881 Color: 7
Size: 479773 Color: 0

Bin 4124: 1351 of cap free
Amount of items: 2
Items: 
Size: 516579 Color: 0
Size: 482071 Color: 6

Bin 4125: 1363 of cap free
Amount of items: 2
Items: 
Size: 559579 Color: 18
Size: 439059 Color: 7

Bin 4126: 1366 of cap free
Amount of items: 2
Items: 
Size: 609778 Color: 2
Size: 388857 Color: 6

Bin 4127: 1369 of cap free
Amount of items: 2
Items: 
Size: 706105 Color: 19
Size: 292527 Color: 5

Bin 4128: 1371 of cap free
Amount of items: 2
Items: 
Size: 564943 Color: 8
Size: 433687 Color: 1

Bin 4129: 1374 of cap free
Amount of items: 2
Items: 
Size: 702119 Color: 11
Size: 296508 Color: 18

Bin 4130: 1384 of cap free
Amount of items: 3
Items: 
Size: 349502 Color: 8
Size: 339357 Color: 2
Size: 309758 Color: 2

Bin 4131: 1387 of cap free
Amount of items: 2
Items: 
Size: 763297 Color: 4
Size: 235317 Color: 3

Bin 4132: 1400 of cap free
Amount of items: 2
Items: 
Size: 548310 Color: 4
Size: 450291 Color: 15

Bin 4133: 1417 of cap free
Amount of items: 2
Items: 
Size: 608377 Color: 12
Size: 390207 Color: 0

Bin 4134: 1433 of cap free
Amount of items: 2
Items: 
Size: 530855 Color: 2
Size: 467713 Color: 3

Bin 4135: 1437 of cap free
Amount of items: 2
Items: 
Size: 625730 Color: 6
Size: 372834 Color: 2

Bin 4136: 1444 of cap free
Amount of items: 2
Items: 
Size: 502625 Color: 13
Size: 495932 Color: 2

Bin 4137: 1444 of cap free
Amount of items: 2
Items: 
Size: 595131 Color: 0
Size: 403426 Color: 4

Bin 4138: 1445 of cap free
Amount of items: 2
Items: 
Size: 793262 Color: 11
Size: 205294 Color: 13

Bin 4139: 1452 of cap free
Amount of items: 2
Items: 
Size: 535736 Color: 8
Size: 462813 Color: 7

Bin 4140: 1453 of cap free
Amount of items: 2
Items: 
Size: 692263 Color: 4
Size: 306285 Color: 1

Bin 4141: 1461 of cap free
Amount of items: 2
Items: 
Size: 518833 Color: 0
Size: 479707 Color: 3

Bin 4142: 1480 of cap free
Amount of items: 2
Items: 
Size: 535728 Color: 8
Size: 462793 Color: 7

Bin 4143: 1495 of cap free
Amount of items: 2
Items: 
Size: 518815 Color: 6
Size: 479691 Color: 17

Bin 4144: 1509 of cap free
Amount of items: 2
Items: 
Size: 526621 Color: 10
Size: 471871 Color: 18

Bin 4145: 1512 of cap free
Amount of items: 2
Items: 
Size: 790434 Color: 12
Size: 208055 Color: 2

Bin 4146: 1515 of cap free
Amount of items: 2
Items: 
Size: 770702 Color: 12
Size: 227784 Color: 13

Bin 4147: 1520 of cap free
Amount of items: 2
Items: 
Size: 548201 Color: 16
Size: 450280 Color: 5

Bin 4148: 1523 of cap free
Amount of items: 2
Items: 
Size: 608296 Color: 18
Size: 390182 Color: 5

Bin 4149: 1527 of cap free
Amount of items: 2
Items: 
Size: 790431 Color: 9
Size: 208043 Color: 8

Bin 4150: 1530 of cap free
Amount of items: 3
Items: 
Size: 360654 Color: 0
Size: 327831 Color: 10
Size: 309986 Color: 4

Bin 4151: 1532 of cap free
Amount of items: 2
Items: 
Size: 541300 Color: 14
Size: 457169 Color: 6

Bin 4152: 1538 of cap free
Amount of items: 2
Items: 
Size: 548192 Color: 9
Size: 450271 Color: 4

Bin 4153: 1543 of cap free
Amount of items: 2
Items: 
Size: 527713 Color: 2
Size: 470745 Color: 3

Bin 4154: 1550 of cap free
Amount of items: 2
Items: 
Size: 516574 Color: 9
Size: 481877 Color: 7

Bin 4155: 1557 of cap free
Amount of items: 2
Items: 
Size: 787702 Color: 5
Size: 210742 Color: 13

Bin 4156: 1572 of cap free
Amount of items: 2
Items: 
Size: 535700 Color: 12
Size: 462729 Color: 16

Bin 4157: 1573 of cap free
Amount of items: 3
Items: 
Size: 576473 Color: 8
Size: 224047 Color: 7
Size: 197908 Color: 8

Bin 4158: 1575 of cap free
Amount of items: 2
Items: 
Size: 569834 Color: 10
Size: 428592 Color: 7

Bin 4159: 1578 of cap free
Amount of items: 2
Items: 
Size: 535694 Color: 5
Size: 462729 Color: 15

Bin 4160: 1583 of cap free
Amount of items: 2
Items: 
Size: 508133 Color: 2
Size: 490285 Color: 1

Bin 4161: 1584 of cap free
Amount of items: 2
Items: 
Size: 516571 Color: 19
Size: 481846 Color: 5

Bin 4162: 1601 of cap free
Amount of items: 2
Items: 
Size: 710254 Color: 18
Size: 288146 Color: 15

Bin 4163: 1605 of cap free
Amount of items: 2
Items: 
Size: 561116 Color: 11
Size: 437280 Color: 7

Bin 4164: 1610 of cap free
Amount of items: 3
Items: 
Size: 512532 Color: 4
Size: 257175 Color: 11
Size: 228684 Color: 9

Bin 4165: 1610 of cap free
Amount of items: 2
Items: 
Size: 524340 Color: 14
Size: 474051 Color: 6

Bin 4166: 1612 of cap free
Amount of items: 2
Items: 
Size: 599482 Color: 13
Size: 398907 Color: 0

Bin 4167: 1613 of cap free
Amount of items: 2
Items: 
Size: 532102 Color: 3
Size: 466286 Color: 15

Bin 4168: 1617 of cap free
Amount of items: 2
Items: 
Size: 612423 Color: 10
Size: 385961 Color: 6

Bin 4169: 1623 of cap free
Amount of items: 2
Items: 
Size: 620165 Color: 1
Size: 378213 Color: 6

Bin 4170: 1631 of cap free
Amount of items: 2
Items: 
Size: 685684 Color: 10
Size: 312686 Color: 19

Bin 4171: 1640 of cap free
Amount of items: 2
Items: 
Size: 559446 Color: 0
Size: 438915 Color: 8

Bin 4172: 1641 of cap free
Amount of items: 2
Items: 
Size: 663426 Color: 8
Size: 334934 Color: 19

Bin 4173: 1652 of cap free
Amount of items: 3
Items: 
Size: 518210 Color: 2
Size: 250808 Color: 8
Size: 229331 Color: 5

Bin 4174: 1655 of cap free
Amount of items: 2
Items: 
Size: 612420 Color: 13
Size: 385926 Color: 18

Bin 4175: 1657 of cap free
Amount of items: 3
Items: 
Size: 576461 Color: 16
Size: 223977 Color: 12
Size: 197906 Color: 1

Bin 4176: 1657 of cap free
Amount of items: 2
Items: 
Size: 736600 Color: 17
Size: 261744 Color: 10

Bin 4177: 1659 of cap free
Amount of items: 2
Items: 
Size: 573988 Color: 8
Size: 424354 Color: 12

Bin 4178: 1662 of cap free
Amount of items: 2
Items: 
Size: 516533 Color: 5
Size: 481806 Color: 13

Bin 4179: 1663 of cap free
Amount of items: 2
Items: 
Size: 518679 Color: 14
Size: 479659 Color: 15

Bin 4180: 1667 of cap free
Amount of items: 2
Items: 
Size: 592442 Color: 18
Size: 405892 Color: 12

Bin 4181: 1692 of cap free
Amount of items: 2
Items: 
Size: 788402 Color: 11
Size: 209907 Color: 8

Bin 4182: 1695 of cap free
Amount of items: 2
Items: 
Size: 564671 Color: 18
Size: 433635 Color: 14

Bin 4183: 1697 of cap free
Amount of items: 2
Items: 
Size: 573618 Color: 13
Size: 424686 Color: 3

Bin 4184: 1699 of cap free
Amount of items: 2
Items: 
Size: 592454 Color: 12
Size: 405848 Color: 8

Bin 4185: 1716 of cap free
Amount of items: 2
Items: 
Size: 567023 Color: 13
Size: 431262 Color: 2

Bin 4186: 1717 of cap free
Amount of items: 3
Items: 
Size: 707778 Color: 14
Size: 145423 Color: 15
Size: 145083 Color: 3

Bin 4187: 1724 of cap free
Amount of items: 2
Items: 
Size: 548079 Color: 16
Size: 450198 Color: 6

Bin 4188: 1750 of cap free
Amount of items: 2
Items: 
Size: 796221 Color: 12
Size: 202030 Color: 1

Bin 4189: 1752 of cap free
Amount of items: 2
Items: 
Size: 511555 Color: 2
Size: 486694 Color: 8

Bin 4190: 1762 of cap free
Amount of items: 2
Items: 
Size: 508810 Color: 2
Size: 489429 Color: 4

Bin 4191: 1774 of cap free
Amount of items: 2
Items: 
Size: 535625 Color: 5
Size: 462602 Color: 19

Bin 4192: 1777 of cap free
Amount of items: 2
Items: 
Size: 710279 Color: 15
Size: 287945 Color: 19

Bin 4193: 1778 of cap free
Amount of items: 2
Items: 
Size: 524200 Color: 13
Size: 474023 Color: 8

Bin 4194: 1797 of cap free
Amount of items: 2
Items: 
Size: 514591 Color: 17
Size: 483613 Color: 2

Bin 4195: 1801 of cap free
Amount of items: 2
Items: 
Size: 565730 Color: 16
Size: 432470 Color: 2

Bin 4196: 1808 of cap free
Amount of items: 2
Items: 
Size: 791972 Color: 13
Size: 206221 Color: 12

Bin 4197: 1818 of cap free
Amount of items: 2
Items: 
Size: 569674 Color: 19
Size: 428509 Color: 9

Bin 4198: 1837 of cap free
Amount of items: 2
Items: 
Size: 599331 Color: 1
Size: 398833 Color: 14

Bin 4199: 1840 of cap free
Amount of items: 2
Items: 
Size: 504247 Color: 17
Size: 493914 Color: 12

Bin 4200: 1840 of cap free
Amount of items: 2
Items: 
Size: 573570 Color: 12
Size: 424591 Color: 18

Bin 4201: 1856 of cap free
Amount of items: 2
Items: 
Size: 790115 Color: 14
Size: 208030 Color: 3

Bin 4202: 1864 of cap free
Amount of items: 2
Items: 
Size: 518488 Color: 6
Size: 479649 Color: 5

Bin 4203: 1896 of cap free
Amount of items: 2
Items: 
Size: 531870 Color: 9
Size: 466235 Color: 11

Bin 4204: 1919 of cap free
Amount of items: 2
Items: 
Size: 672952 Color: 10
Size: 325130 Color: 8

Bin 4205: 1934 of cap free
Amount of items: 2
Items: 
Size: 616551 Color: 1
Size: 381516 Color: 14

Bin 4206: 1935 of cap free
Amount of items: 3
Items: 
Size: 499818 Color: 19
Size: 249179 Color: 15
Size: 249069 Color: 3

Bin 4207: 1941 of cap free
Amount of items: 2
Items: 
Size: 763246 Color: 9
Size: 234814 Color: 8

Bin 4208: 1951 of cap free
Amount of items: 2
Items: 
Size: 605363 Color: 13
Size: 392687 Color: 11

Bin 4209: 1953 of cap free
Amount of items: 2
Items: 
Size: 706658 Color: 18
Size: 291390 Color: 8

Bin 4210: 1962 of cap free
Amount of items: 3
Items: 
Size: 576472 Color: 2
Size: 223905 Color: 4
Size: 197662 Color: 13

Bin 4211: 1977 of cap free
Amount of items: 2
Items: 
Size: 605268 Color: 16
Size: 392756 Color: 13

Bin 4212: 1989 of cap free
Amount of items: 2
Items: 
Size: 573476 Color: 17
Size: 424536 Color: 15

Bin 4213: 2001 of cap free
Amount of items: 2
Items: 
Size: 527526 Color: 4
Size: 470474 Color: 12

Bin 4214: 2007 of cap free
Amount of items: 2
Items: 
Size: 799331 Color: 1
Size: 198663 Color: 13

Bin 4215: 2021 of cap free
Amount of items: 2
Items: 
Size: 672856 Color: 11
Size: 325124 Color: 7

Bin 4216: 2030 of cap free
Amount of items: 3
Items: 
Size: 356653 Color: 16
Size: 336534 Color: 2
Size: 304784 Color: 10

Bin 4217: 2040 of cap free
Amount of items: 2
Items: 
Size: 511200 Color: 8
Size: 486761 Color: 12

Bin 4218: 2048 of cap free
Amount of items: 2
Items: 
Size: 540779 Color: 18
Size: 457174 Color: 14

Bin 4219: 2055 of cap free
Amount of items: 2
Items: 
Size: 525558 Color: 2
Size: 472388 Color: 4

Bin 4220: 2060 of cap free
Amount of items: 2
Items: 
Size: 523924 Color: 17
Size: 474017 Color: 19

Bin 4221: 2070 of cap free
Amount of items: 2
Items: 
Size: 518293 Color: 13
Size: 479638 Color: 0

Bin 4222: 2082 of cap free
Amount of items: 2
Items: 
Size: 625262 Color: 3
Size: 372657 Color: 2

Bin 4223: 2088 of cap free
Amount of items: 2
Items: 
Size: 526610 Color: 0
Size: 471303 Color: 5

Bin 4224: 2111 of cap free
Amount of items: 2
Items: 
Size: 622458 Color: 19
Size: 375432 Color: 2

Bin 4225: 2122 of cap free
Amount of items: 2
Items: 
Size: 768311 Color: 2
Size: 229568 Color: 19

Bin 4226: 2149 of cap free
Amount of items: 2
Items: 
Size: 573407 Color: 14
Size: 424445 Color: 6

Bin 4227: 2166 of cap free
Amount of items: 3
Items: 
Size: 593346 Color: 2
Size: 205395 Color: 8
Size: 199094 Color: 9

Bin 4228: 2179 of cap free
Amount of items: 2
Items: 
Size: 554375 Color: 19
Size: 443447 Color: 14

Bin 4229: 2185 of cap free
Amount of items: 2
Items: 
Size: 701989 Color: 0
Size: 295827 Color: 13

Bin 4230: 2187 of cap free
Amount of items: 2
Items: 
Size: 698254 Color: 4
Size: 299560 Color: 8

Bin 4231: 2213 of cap free
Amount of items: 2
Items: 
Size: 698224 Color: 7
Size: 299564 Color: 4

Bin 4232: 2256 of cap free
Amount of items: 2
Items: 
Size: 616299 Color: 0
Size: 381446 Color: 8

Bin 4233: 2257 of cap free
Amount of items: 2
Items: 
Size: 798519 Color: 4
Size: 199225 Color: 1

Bin 4234: 2274 of cap free
Amount of items: 2
Items: 
Size: 513968 Color: 0
Size: 483759 Color: 5

Bin 4235: 2309 of cap free
Amount of items: 3
Items: 
Size: 360842 Color: 0
Size: 328192 Color: 6
Size: 308658 Color: 3

Bin 4236: 2331 of cap free
Amount of items: 2
Items: 
Size: 573354 Color: 5
Size: 424316 Color: 10

Bin 4237: 2344 of cap free
Amount of items: 3
Items: 
Size: 605526 Color: 2
Size: 219072 Color: 6
Size: 173059 Color: 8

Bin 4238: 2349 of cap free
Amount of items: 2
Items: 
Size: 672537 Color: 3
Size: 325115 Color: 6

Bin 4239: 2390 of cap free
Amount of items: 2
Items: 
Size: 588072 Color: 19
Size: 409539 Color: 4

Bin 4240: 2428 of cap free
Amount of items: 2
Items: 
Size: 513964 Color: 16
Size: 483609 Color: 17

Bin 4241: 2449 of cap free
Amount of items: 2
Items: 
Size: 705119 Color: 2
Size: 292433 Color: 18

Bin 4242: 2469 of cap free
Amount of items: 2
Items: 
Size: 502324 Color: 2
Size: 495208 Color: 10

Bin 4243: 2474 of cap free
Amount of items: 2
Items: 
Size: 513958 Color: 5
Size: 483569 Color: 10

Bin 4244: 2478 of cap free
Amount of items: 2
Items: 
Size: 599756 Color: 11
Size: 397767 Color: 6

Bin 4245: 2498 of cap free
Amount of items: 2
Items: 
Size: 619767 Color: 9
Size: 377736 Color: 6

Bin 4246: 2505 of cap free
Amount of items: 2
Items: 
Size: 616171 Color: 13
Size: 381325 Color: 0

Bin 4247: 2549 of cap free
Amount of items: 2
Items: 
Size: 513914 Color: 19
Size: 483538 Color: 16

Bin 4248: 2565 of cap free
Amount of items: 2
Items: 
Size: 536068 Color: 2
Size: 461368 Color: 3

Bin 4249: 2585 of cap free
Amount of items: 2
Items: 
Size: 616084 Color: 8
Size: 381332 Color: 13

Bin 4250: 2587 of cap free
Amount of items: 2
Items: 
Size: 554043 Color: 6
Size: 443371 Color: 5

Bin 4251: 2602 of cap free
Amount of items: 2
Items: 
Size: 605332 Color: 8
Size: 392067 Color: 12

Bin 4252: 2603 of cap free
Amount of items: 2
Items: 
Size: 573307 Color: 10
Size: 424091 Color: 15

Bin 4253: 2609 of cap free
Amount of items: 2
Items: 
Size: 605327 Color: 11
Size: 392065 Color: 16

Bin 4254: 2612 of cap free
Amount of items: 2
Items: 
Size: 698178 Color: 12
Size: 299211 Color: 2

Bin 4255: 2621 of cap free
Amount of items: 2
Items: 
Size: 513438 Color: 2
Size: 483942 Color: 8

Bin 4256: 2625 of cap free
Amount of items: 2
Items: 
Size: 548034 Color: 19
Size: 449342 Color: 1

Bin 4257: 2639 of cap free
Amount of items: 2
Items: 
Size: 591871 Color: 12
Size: 405491 Color: 8

Bin 4258: 2642 of cap free
Amount of items: 2
Items: 
Size: 554034 Color: 6
Size: 443325 Color: 15

Bin 4259: 2669 of cap free
Amount of items: 2
Items: 
Size: 531363 Color: 10
Size: 465969 Color: 13

Bin 4260: 2687 of cap free
Amount of items: 2
Items: 
Size: 521122 Color: 16
Size: 476192 Color: 11

Bin 4261: 2699 of cap free
Amount of items: 2
Items: 
Size: 573282 Color: 4
Size: 424020 Color: 18

Bin 4262: 2699 of cap free
Amount of items: 2
Items: 
Size: 698179 Color: 2
Size: 299123 Color: 10

Bin 4263: 2702 of cap free
Amount of items: 2
Items: 
Size: 501387 Color: 11
Size: 495912 Color: 14

Bin 4264: 2703 of cap free
Amount of items: 2
Items: 
Size: 616017 Color: 15
Size: 381281 Color: 2

Bin 4265: 2716 of cap free
Amount of items: 2
Items: 
Size: 513735 Color: 10
Size: 483550 Color: 19

Bin 4266: 2727 of cap free
Amount of items: 2
Items: 
Size: 684545 Color: 3
Size: 312729 Color: 10

Bin 4267: 2737 of cap free
Amount of items: 2
Items: 
Size: 600131 Color: 10
Size: 397133 Color: 16

Bin 4268: 2758 of cap free
Amount of items: 2
Items: 
Size: 504242 Color: 16
Size: 493001 Color: 17

Bin 4269: 2814 of cap free
Amount of items: 2
Items: 
Size: 704816 Color: 16
Size: 292371 Color: 12

Bin 4270: 2815 of cap free
Amount of items: 2
Items: 
Size: 684536 Color: 4
Size: 312650 Color: 19

Bin 4271: 2845 of cap free
Amount of items: 2
Items: 
Size: 763030 Color: 4
Size: 234126 Color: 9

Bin 4272: 2879 of cap free
Amount of items: 2
Items: 
Size: 531347 Color: 9
Size: 465775 Color: 6

Bin 4273: 2880 of cap free
Amount of items: 2
Items: 
Size: 553974 Color: 13
Size: 443147 Color: 16

Bin 4274: 2887 of cap free
Amount of items: 2
Items: 
Size: 763029 Color: 3
Size: 234085 Color: 1

Bin 4275: 2894 of cap free
Amount of items: 2
Items: 
Size: 501332 Color: 19
Size: 495775 Color: 18

Bin 4276: 2898 of cap free
Amount of items: 2
Items: 
Size: 591833 Color: 1
Size: 405270 Color: 12

Bin 4277: 2919 of cap free
Amount of items: 2
Items: 
Size: 704811 Color: 12
Size: 292271 Color: 10

Bin 4278: 2923 of cap free
Amount of items: 2
Items: 
Size: 531328 Color: 9
Size: 465750 Color: 19

Bin 4279: 2929 of cap free
Amount of items: 2
Items: 
Size: 527397 Color: 16
Size: 469675 Color: 3

Bin 4280: 2952 of cap free
Amount of items: 2
Items: 
Size: 762999 Color: 16
Size: 234050 Color: 18

Bin 4281: 2968 of cap free
Amount of items: 2
Items: 
Size: 762990 Color: 16
Size: 234043 Color: 8

Bin 4282: 2985 of cap free
Amount of items: 2
Items: 
Size: 604962 Color: 11
Size: 392054 Color: 13

Bin 4283: 2991 of cap free
Amount of items: 2
Items: 
Size: 573013 Color: 17
Size: 423997 Color: 4

Bin 4284: 3010 of cap free
Amount of items: 2
Items: 
Size: 598116 Color: 18
Size: 398875 Color: 1

Bin 4285: 3047 of cap free
Amount of items: 2
Items: 
Size: 553955 Color: 16
Size: 442999 Color: 17

Bin 4286: 3068 of cap free
Amount of items: 2
Items: 
Size: 794982 Color: 13
Size: 201951 Color: 15

Bin 4287: 3082 of cap free
Amount of items: 2
Items: 
Size: 615834 Color: 3
Size: 381085 Color: 14

Bin 4288: 3098 of cap free
Amount of items: 3
Items: 
Size: 534778 Color: 3
Size: 238339 Color: 6
Size: 223786 Color: 4

Bin 4289: 3103 of cap free
Amount of items: 2
Items: 
Size: 630063 Color: 5
Size: 366835 Color: 2

Bin 4290: 3107 of cap free
Amount of items: 2
Items: 
Size: 794905 Color: 7
Size: 201989 Color: 18

Bin 4291: 3129 of cap free
Amount of items: 2
Items: 
Size: 738481 Color: 2
Size: 258391 Color: 13

Bin 4292: 3172 of cap free
Amount of items: 2
Items: 
Size: 666381 Color: 17
Size: 330448 Color: 18

Bin 4293: 3230 of cap free
Amount of items: 2
Items: 
Size: 598049 Color: 12
Size: 398722 Color: 0

Bin 4294: 3231 of cap free
Amount of items: 2
Items: 
Size: 794841 Color: 17
Size: 201929 Color: 0

Bin 4295: 3271 of cap free
Amount of items: 2
Items: 
Size: 559525 Color: 8
Size: 437205 Color: 14

Bin 4296: 3274 of cap free
Amount of items: 2
Items: 
Size: 598015 Color: 12
Size: 398712 Color: 3

Bin 4297: 3279 of cap free
Amount of items: 2
Items: 
Size: 704478 Color: 9
Size: 292244 Color: 11

Bin 4298: 3300 of cap free
Amount of items: 2
Items: 
Size: 548029 Color: 8
Size: 448672 Color: 18

Bin 4299: 3312 of cap free
Amount of items: 2
Items: 
Size: 704464 Color: 14
Size: 292225 Color: 15

Bin 4300: 3319 of cap free
Amount of items: 2
Items: 
Size: 553824 Color: 12
Size: 442858 Color: 6

Bin 4301: 3339 of cap free
Amount of items: 2
Items: 
Size: 698094 Color: 4
Size: 298568 Color: 8

Bin 4302: 3347 of cap free
Amount of items: 2
Items: 
Size: 520489 Color: 3
Size: 476165 Color: 10

Bin 4303: 3373 of cap free
Amount of items: 2
Items: 
Size: 591416 Color: 3
Size: 405212 Color: 7

Bin 4304: 3379 of cap free
Amount of items: 2
Items: 
Size: 572732 Color: 15
Size: 423890 Color: 13

Bin 4305: 3382 of cap free
Amount of items: 2
Items: 
Size: 755399 Color: 12
Size: 241220 Color: 6

Bin 4306: 3385 of cap free
Amount of items: 2
Items: 
Size: 704441 Color: 16
Size: 292175 Color: 10

Bin 4307: 3396 of cap free
Amount of items: 2
Items: 
Size: 666312 Color: 10
Size: 330293 Color: 1

Bin 4308: 3397 of cap free
Amount of items: 2
Items: 
Size: 591402 Color: 10
Size: 405202 Color: 6

Bin 4309: 3407 of cap free
Amount of items: 2
Items: 
Size: 666302 Color: 11
Size: 330292 Color: 9

Bin 4310: 3415 of cap free
Amount of items: 2
Items: 
Size: 539478 Color: 16
Size: 457108 Color: 6

Bin 4311: 3417 of cap free
Amount of items: 2
Items: 
Size: 713756 Color: 1
Size: 282828 Color: 8

Bin 4312: 3418 of cap free
Amount of items: 2
Items: 
Size: 762603 Color: 7
Size: 233980 Color: 16

Bin 4313: 3436 of cap free
Amount of items: 2
Items: 
Size: 704404 Color: 12
Size: 292161 Color: 6

Bin 4314: 3448 of cap free
Amount of items: 2
Items: 
Size: 548020 Color: 6
Size: 448533 Color: 18

Bin 4315: 3449 of cap free
Amount of items: 2
Items: 
Size: 587165 Color: 19
Size: 409387 Color: 14

Bin 4316: 3454 of cap free
Amount of items: 2
Items: 
Size: 559430 Color: 12
Size: 437117 Color: 7

Bin 4317: 3465 of cap free
Amount of items: 2
Items: 
Size: 539450 Color: 8
Size: 457086 Color: 13

Bin 4318: 3479 of cap free
Amount of items: 2
Items: 
Size: 591369 Color: 8
Size: 405153 Color: 3

Bin 4319: 3481 of cap free
Amount of items: 2
Items: 
Size: 698000 Color: 17
Size: 298520 Color: 12

Bin 4320: 3489 of cap free
Amount of items: 2
Items: 
Size: 666276 Color: 7
Size: 330236 Color: 2

Bin 4321: 3562 of cap free
Amount of items: 2
Items: 
Size: 559405 Color: 16
Size: 437034 Color: 7

Bin 4322: 3571 of cap free
Amount of items: 2
Items: 
Size: 704450 Color: 8
Size: 291980 Color: 19

Bin 4323: 3576 of cap free
Amount of items: 2
Items: 
Size: 579079 Color: 4
Size: 417346 Color: 6

Bin 4324: 3668 of cap free
Amount of items: 3
Items: 
Size: 360539 Color: 14
Size: 348945 Color: 19
Size: 286849 Color: 18

Bin 4325: 3678 of cap free
Amount of items: 2
Items: 
Size: 563088 Color: 11
Size: 433235 Color: 12

Bin 4326: 3718 of cap free
Amount of items: 2
Items: 
Size: 547882 Color: 9
Size: 448401 Color: 4

Bin 4327: 3810 of cap free
Amount of items: 3
Items: 
Size: 518406 Color: 14
Size: 249042 Color: 8
Size: 228743 Color: 16

Bin 4328: 3818 of cap free
Amount of items: 2
Items: 
Size: 568901 Color: 4
Size: 427282 Color: 5

Bin 4329: 3867 of cap free
Amount of items: 2
Items: 
Size: 615818 Color: 18
Size: 380316 Color: 7

Bin 4330: 3875 of cap free
Amount of items: 2
Items: 
Size: 599693 Color: 19
Size: 396433 Color: 16

Bin 4331: 3899 of cap free
Amount of items: 2
Items: 
Size: 783151 Color: 14
Size: 212951 Color: 17

Bin 4332: 3904 of cap free
Amount of items: 2
Items: 
Size: 754882 Color: 7
Size: 241215 Color: 0

Bin 4333: 3932 of cap free
Amount of items: 2
Items: 
Size: 547862 Color: 10
Size: 448207 Color: 7

Bin 4334: 3944 of cap free
Amount of items: 2
Items: 
Size: 762114 Color: 5
Size: 233943 Color: 14

Bin 4335: 3949 of cap free
Amount of items: 2
Items: 
Size: 539375 Color: 6
Size: 456677 Color: 14

Bin 4336: 3955 of cap free
Amount of items: 2
Items: 
Size: 597369 Color: 1
Size: 398677 Color: 19

Bin 4337: 3979 of cap free
Amount of items: 2
Items: 
Size: 754857 Color: 11
Size: 241165 Color: 0

Bin 4338: 3992 of cap free
Amount of items: 2
Items: 
Size: 572725 Color: 6
Size: 423284 Color: 9

Bin 4339: 3999 of cap free
Amount of items: 2
Items: 
Size: 563040 Color: 4
Size: 432962 Color: 9

Bin 4340: 4020 of cap free
Amount of items: 2
Items: 
Size: 539361 Color: 13
Size: 456620 Color: 5

Bin 4341: 4023 of cap free
Amount of items: 2
Items: 
Size: 762068 Color: 16
Size: 233910 Color: 14

Bin 4342: 4044 of cap free
Amount of items: 2
Items: 
Size: 665724 Color: 1
Size: 330233 Color: 11

Bin 4343: 4066 of cap free
Amount of items: 2
Items: 
Size: 704328 Color: 15
Size: 291607 Color: 17

Bin 4344: 4139 of cap free
Amount of items: 2
Items: 
Size: 761998 Color: 12
Size: 233864 Color: 3

Bin 4345: 4144 of cap free
Amount of items: 3
Items: 
Size: 347276 Color: 13
Size: 337245 Color: 19
Size: 311336 Color: 14

Bin 4346: 4161 of cap free
Amount of items: 2
Items: 
Size: 704389 Color: 8
Size: 291451 Color: 9

Bin 4347: 4184 of cap free
Amount of items: 2
Items: 
Size: 665616 Color: 4
Size: 330201 Color: 3

Bin 4348: 4200 of cap free
Amount of items: 2
Items: 
Size: 704277 Color: 14
Size: 291524 Color: 12

Bin 4349: 4221 of cap free
Amount of items: 2
Items: 
Size: 754846 Color: 19
Size: 240934 Color: 12

Bin 4350: 4255 of cap free
Amount of items: 2
Items: 
Size: 684403 Color: 12
Size: 311343 Color: 18

Bin 4351: 4360 of cap free
Amount of items: 2
Items: 
Size: 513854 Color: 19
Size: 481787 Color: 14

Bin 4352: 4375 of cap free
Amount of items: 2
Items: 
Size: 754822 Color: 4
Size: 240804 Color: 17

Bin 4353: 4446 of cap free
Amount of items: 2
Items: 
Size: 547728 Color: 16
Size: 447827 Color: 8

Bin 4354: 4496 of cap free
Amount of items: 2
Items: 
Size: 552720 Color: 9
Size: 442785 Color: 10

Bin 4355: 4521 of cap free
Amount of items: 2
Items: 
Size: 513737 Color: 19
Size: 481743 Color: 9

Bin 4356: 4567 of cap free
Amount of items: 2
Items: 
Size: 761681 Color: 1
Size: 233753 Color: 0

Bin 4357: 4623 of cap free
Amount of items: 2
Items: 
Size: 704312 Color: 8
Size: 291066 Color: 17

Bin 4358: 4704 of cap free
Amount of items: 2
Items: 
Size: 552656 Color: 5
Size: 442641 Color: 11

Bin 4359: 4752 of cap free
Amount of items: 2
Items: 
Size: 552557 Color: 16
Size: 442692 Color: 5

Bin 4360: 4789 of cap free
Amount of items: 2
Items: 
Size: 615003 Color: 0
Size: 380209 Color: 4

Bin 4361: 4850 of cap free
Amount of items: 2
Items: 
Size: 665175 Color: 19
Size: 329976 Color: 14

Bin 4362: 4852 of cap free
Amount of items: 2
Items: 
Size: 562994 Color: 12
Size: 432155 Color: 14

Bin 4363: 4945 of cap free
Amount of items: 2
Items: 
Size: 552515 Color: 3
Size: 442541 Color: 17

Bin 4364: 5045 of cap free
Amount of items: 2
Items: 
Size: 665090 Color: 12
Size: 329866 Color: 17

Bin 4365: 5067 of cap free
Amount of items: 3
Items: 
Size: 346213 Color: 4
Size: 339003 Color: 7
Size: 309718 Color: 6

Bin 4366: 5275 of cap free
Amount of items: 2
Items: 
Size: 786950 Color: 18
Size: 207776 Color: 13

Bin 4367: 5278 of cap free
Amount of items: 2
Items: 
Size: 591325 Color: 17
Size: 403398 Color: 18

Bin 4368: 5326 of cap free
Amount of items: 2
Items: 
Size: 628173 Color: 1
Size: 366502 Color: 2

Bin 4369: 5425 of cap free
Amount of items: 2
Items: 
Size: 524623 Color: 4
Size: 469953 Color: 12

Bin 4370: 5677 of cap free
Amount of items: 2
Items: 
Size: 501325 Color: 7
Size: 492999 Color: 5

Bin 4371: 5685 of cap free
Amount of items: 2
Items: 
Size: 636307 Color: 0
Size: 358009 Color: 2

Bin 4372: 5757 of cap free
Amount of items: 3
Items: 
Size: 358383 Color: 16
Size: 349057 Color: 4
Size: 286804 Color: 1

Bin 4373: 5900 of cap free
Amount of items: 2
Items: 
Size: 786460 Color: 15
Size: 207641 Color: 13

Bin 4374: 6024 of cap free
Amount of items: 2
Items: 
Size: 501219 Color: 3
Size: 492758 Color: 17

Bin 4375: 6040 of cap free
Amount of items: 2
Items: 
Size: 711427 Color: 15
Size: 282534 Color: 8

Bin 4376: 6294 of cap free
Amount of items: 3
Items: 
Size: 358456 Color: 14
Size: 348402 Color: 3
Size: 286849 Color: 6

Bin 4377: 6297 of cap free
Amount of items: 2
Items: 
Size: 605240 Color: 2
Size: 388464 Color: 6

Bin 4378: 6417 of cap free
Amount of items: 2
Items: 
Size: 501069 Color: 18
Size: 492515 Color: 8

Bin 4379: 6866 of cap free
Amount of items: 2
Items: 
Size: 737168 Color: 14
Size: 255967 Color: 13

Bin 4380: 6929 of cap free
Amount of items: 2
Items: 
Size: 513483 Color: 8
Size: 479589 Color: 19

Bin 4381: 6976 of cap free
Amount of items: 2
Items: 
Size: 786029 Color: 6
Size: 206996 Color: 13

Bin 4382: 7065 of cap free
Amount of items: 2
Items: 
Size: 513365 Color: 19
Size: 479571 Color: 17

Bin 4383: 7378 of cap free
Amount of items: 2
Items: 
Size: 539321 Color: 11
Size: 453302 Color: 0

Bin 4384: 7418 of cap free
Amount of items: 2
Items: 
Size: 701636 Color: 16
Size: 290947 Color: 1

Bin 4385: 7549 of cap free
Amount of items: 2
Items: 
Size: 612392 Color: 0
Size: 380060 Color: 5

Bin 4386: 7564 of cap free
Amount of items: 2
Items: 
Size: 701516 Color: 8
Size: 290921 Color: 6

Bin 4387: 7973 of cap free
Amount of items: 2
Items: 
Size: 560917 Color: 16
Size: 431111 Color: 2

Bin 4388: 8091 of cap free
Amount of items: 2
Items: 
Size: 736478 Color: 0
Size: 255432 Color: 13

Bin 4389: 8261 of cap free
Amount of items: 2
Items: 
Size: 711053 Color: 14
Size: 280687 Color: 8

Bin 4390: 8518 of cap free
Amount of items: 2
Items: 
Size: 517889 Color: 1
Size: 473594 Color: 8

Bin 4391: 8579 of cap free
Amount of items: 3
Items: 
Size: 525172 Color: 12
Size: 238131 Color: 1
Size: 228119 Color: 17

Bin 4392: 9090 of cap free
Amount of items: 2
Items: 
Size: 559354 Color: 1
Size: 431557 Color: 15

Bin 4393: 9666 of cap free
Amount of items: 2
Items: 
Size: 592105 Color: 10
Size: 398230 Color: 2

Bin 4394: 10111 of cap free
Amount of items: 2
Items: 
Size: 547724 Color: 13
Size: 442166 Color: 12

Bin 4395: 10211 of cap free
Amount of items: 2
Items: 
Size: 516270 Color: 18
Size: 473520 Color: 8

Bin 4396: 10211 of cap free
Amount of items: 2
Items: 
Size: 547702 Color: 10
Size: 442088 Color: 18

Bin 4397: 10250 of cap free
Amount of items: 2
Items: 
Size: 591208 Color: 17
Size: 398543 Color: 0

Bin 4398: 10676 of cap free
Amount of items: 2
Items: 
Size: 612921 Color: 2
Size: 376404 Color: 16

Bin 4399: 10757 of cap free
Amount of items: 2
Items: 
Size: 590799 Color: 17
Size: 398445 Color: 0

Bin 4400: 10791 of cap free
Amount of items: 2
Items: 
Size: 590781 Color: 3
Size: 398429 Color: 19

Bin 4401: 11099 of cap free
Amount of items: 2
Items: 
Size: 590741 Color: 11
Size: 398161 Color: 12

Bin 4402: 11168 of cap free
Amount of items: 2
Items: 
Size: 590693 Color: 18
Size: 398140 Color: 8

Bin 4403: 11217 of cap free
Amount of items: 2
Items: 
Size: 709323 Color: 1
Size: 279461 Color: 8

Bin 4404: 11280 of cap free
Amount of items: 2
Items: 
Size: 784847 Color: 9
Size: 203874 Color: 13

Bin 4405: 12648 of cap free
Amount of items: 2
Items: 
Size: 513727 Color: 8
Size: 473626 Color: 11

Bin 4406: 12662 of cap free
Amount of items: 3
Items: 
Size: 354750 Color: 17
Size: 345840 Color: 3
Size: 286749 Color: 15

Bin 4407: 13033 of cap free
Amount of items: 2
Items: 
Size: 696901 Color: 0
Size: 290067 Color: 2

Bin 4408: 13138 of cap free
Amount of items: 2
Items: 
Size: 513285 Color: 8
Size: 473578 Color: 5

Bin 4409: 13301 of cap free
Amount of items: 2
Items: 
Size: 570173 Color: 9
Size: 416527 Color: 12

Bin 4410: 14938 of cap free
Amount of items: 3
Items: 
Size: 524842 Color: 1
Size: 231818 Color: 9
Size: 228403 Color: 4

Bin 4411: 14995 of cap free
Amount of items: 2
Items: 
Size: 615813 Color: 16
Size: 369193 Color: 6

Bin 4412: 15432 of cap free
Amount of items: 2
Items: 
Size: 511174 Color: 19
Size: 473395 Color: 0

Bin 4413: 15835 of cap free
Amount of items: 2
Items: 
Size: 569304 Color: 1
Size: 414862 Color: 12

Bin 4414: 17350 of cap free
Amount of items: 2
Items: 
Size: 698470 Color: 3
Size: 284181 Color: 13

Bin 4415: 17788 of cap free
Amount of items: 2
Items: 
Size: 615262 Color: 15
Size: 366951 Color: 6

Bin 4416: 17969 of cap free
Amount of items: 3
Items: 
Size: 350458 Color: 6
Size: 345702 Color: 9
Size: 285872 Color: 18

Bin 4417: 17993 of cap free
Amount of items: 3
Items: 
Size: 349051 Color: 4
Size: 347035 Color: 9
Size: 285922 Color: 15

Bin 4418: 18013 of cap free
Amount of items: 2
Items: 
Size: 510707 Color: 14
Size: 471281 Color: 16

Bin 4419: 18424 of cap free
Amount of items: 2
Items: 
Size: 510351 Color: 11
Size: 471226 Color: 15

Bin 4420: 18457 of cap free
Amount of items: 2
Items: 
Size: 614742 Color: 15
Size: 366802 Color: 6

Bin 4421: 19272 of cap free
Amount of items: 2
Items: 
Size: 509687 Color: 11
Size: 471042 Color: 15

Bin 4422: 19334 of cap free
Amount of items: 2
Items: 
Size: 509633 Color: 7
Size: 471034 Color: 15

Bin 4423: 19412 of cap free
Amount of items: 2
Items: 
Size: 610488 Color: 14
Size: 370101 Color: 16

Bin 4424: 19566 of cap free
Amount of items: 3
Items: 
Size: 349669 Color: 10
Size: 345562 Color: 3
Size: 285204 Color: 2

Bin 4425: 19988 of cap free
Amount of items: 2
Items: 
Size: 509603 Color: 11
Size: 470410 Color: 6

Bin 4426: 20594 of cap free
Amount of items: 2
Items: 
Size: 509595 Color: 4
Size: 469812 Color: 15

Bin 4427: 20728 of cap free
Amount of items: 2
Items: 
Size: 509508 Color: 12
Size: 469765 Color: 16

Bin 4428: 20940 of cap free
Amount of items: 2
Items: 
Size: 509475 Color: 15
Size: 469586 Color: 10

Bin 4429: 21242 of cap free
Amount of items: 2
Items: 
Size: 696988 Color: 2
Size: 281771 Color: 13

Bin 4430: 21920 of cap free
Amount of items: 2
Items: 
Size: 509238 Color: 19
Size: 468843 Color: 5

Bin 4431: 22111 of cap free
Amount of items: 2
Items: 
Size: 509162 Color: 12
Size: 468728 Color: 11

Bin 4432: 23060 of cap free
Amount of items: 2
Items: 
Size: 696104 Color: 5
Size: 280837 Color: 13

Bin 4433: 23515 of cap free
Amount of items: 3
Items: 
Size: 349387 Color: 12
Size: 349238 Color: 10
Size: 277861 Color: 0

Bin 4434: 24297 of cap free
Amount of items: 2
Items: 
Size: 695263 Color: 1
Size: 280441 Color: 13

Bin 4435: 25258 of cap free
Amount of items: 2
Items: 
Size: 694423 Color: 6
Size: 280320 Color: 13

Bin 4436: 26069 of cap free
Amount of items: 3
Items: 
Size: 348486 Color: 12
Size: 348462 Color: 5
Size: 276984 Color: 13

Bin 4437: 26858 of cap free
Amount of items: 3
Items: 
Size: 349049 Color: 1
Size: 349045 Color: 11
Size: 275049 Color: 3

Bin 4438: 27132 of cap free
Amount of items: 3
Items: 
Size: 349635 Color: 2
Size: 347161 Color: 4
Size: 276073 Color: 9

Bin 4439: 29244 of cap free
Amount of items: 3
Items: 
Size: 346551 Color: 7
Size: 346128 Color: 4
Size: 278078 Color: 5

Bin 4440: 29985 of cap free
Amount of items: 3
Items: 
Size: 348138 Color: 15
Size: 347736 Color: 6
Size: 274142 Color: 3

Bin 4441: 30500 of cap free
Amount of items: 3
Items: 
Size: 349022 Color: 5
Size: 348493 Color: 1
Size: 271986 Color: 9

Bin 4442: 30582 of cap free
Amount of items: 3
Items: 
Size: 345918 Color: 12
Size: 345440 Color: 14
Size: 278061 Color: 12

Bin 4443: 31165 of cap free
Amount of items: 2
Items: 
Size: 612030 Color: 17
Size: 356806 Color: 2

Bin 4444: 31901 of cap free
Amount of items: 3
Items: 
Size: 348409 Color: 0
Size: 342679 Color: 16
Size: 277012 Color: 17

Bin 4445: 32530 of cap free
Amount of items: 2
Items: 
Size: 611429 Color: 1
Size: 356042 Color: 2

Bin 4446: 32589 of cap free
Amount of items: 2
Items: 
Size: 570261 Color: 8
Size: 397151 Color: 3

Bin 4447: 33904 of cap free
Amount of items: 2
Items: 
Size: 596419 Color: 8
Size: 369678 Color: 16

Bin 4448: 35247 of cap free
Amount of items: 3
Items: 
Size: 345706 Color: 11
Size: 340980 Color: 13
Size: 278068 Color: 16

Bin 4449: 36059 of cap free
Amount of items: 2
Items: 
Size: 595586 Color: 1
Size: 368356 Color: 16

Bin 4450: 36590 of cap free
Amount of items: 3
Items: 
Size: 347392 Color: 10
Size: 346187 Color: 6
Size: 269832 Color: 9

Bin 4451: 36624 of cap free
Amount of items: 3
Items: 
Size: 346050 Color: 0
Size: 341938 Color: 17
Size: 275389 Color: 19

Bin 4452: 39695 of cap free
Amount of items: 3
Items: 
Size: 344430 Color: 2
Size: 344028 Color: 11
Size: 271848 Color: 3

Bin 4453: 41200 of cap free
Amount of items: 3
Items: 
Size: 343617 Color: 4
Size: 337296 Color: 7
Size: 277888 Color: 1

Bin 4454: 41569 of cap free
Amount of items: 2
Items: 
Size: 590096 Color: 10
Size: 368336 Color: 16

Bin 4455: 42492 of cap free
Amount of items: 2
Items: 
Size: 561169 Color: 0
Size: 396340 Color: 2

Bin 4456: 44170 of cap free
Amount of items: 3
Items: 
Size: 341918 Color: 0
Size: 337856 Color: 13
Size: 276057 Color: 15

Bin 4457: 47072 of cap free
Amount of items: 3
Items: 
Size: 341459 Color: 4
Size: 338562 Color: 16
Size: 272908 Color: 17

Bin 4458: 47093 of cap free
Amount of items: 3
Items: 
Size: 341429 Color: 4
Size: 340939 Color: 10
Size: 270540 Color: 19

Bin 4459: 47533 of cap free
Amount of items: 3
Items: 
Size: 343830 Color: 2
Size: 340873 Color: 8
Size: 267765 Color: 9

Bin 4460: 47589 of cap free
Amount of items: 2
Items: 
Size: 588838 Color: 5
Size: 363574 Color: 16

Bin 4461: 54744 of cap free
Amount of items: 3
Items: 
Size: 338979 Color: 4
Size: 338569 Color: 2
Size: 267709 Color: 3

Bin 4462: 56844 of cap free
Amount of items: 3
Items: 
Size: 338096 Color: 6
Size: 337879 Color: 1
Size: 267182 Color: 7

Bin 4463: 65376 of cap free
Amount of items: 2
Items: 
Size: 503281 Color: 12
Size: 431344 Color: 10

Bin 4464: 77947 of cap free
Amount of items: 2
Items: 
Size: 500974 Color: 13
Size: 421080 Color: 10

Bin 4465: 81814 of cap free
Amount of items: 2
Items: 
Size: 501073 Color: 11
Size: 417114 Color: 1

Bin 4466: 85031 of cap free
Amount of items: 2
Items: 
Size: 500458 Color: 15
Size: 414512 Color: 1

Bin 4467: 129740 of cap free
Amount of items: 3
Items: 
Size: 327551 Color: 8
Size: 276011 Color: 15
Size: 266699 Color: 9

Bin 4468: 184364 of cap free
Amount of items: 3
Items: 
Size: 274044 Color: 4
Size: 272580 Color: 13
Size: 269013 Color: 11

Bin 4469: 185362 of cap free
Amount of items: 3
Items: 
Size: 273957 Color: 18
Size: 272975 Color: 16
Size: 267707 Color: 14

Bin 4470: 187338 of cap free
Amount of items: 3
Items: 
Size: 273622 Color: 8
Size: 270060 Color: 19
Size: 268981 Color: 17

Bin 4471: 187635 of cap free
Amount of items: 3
Items: 
Size: 271616 Color: 6
Size: 271206 Color: 16
Size: 269544 Color: 0

Bin 4472: 196564 of cap free
Amount of items: 3
Items: 
Size: 268981 Color: 0
Size: 267792 Color: 5
Size: 266664 Color: 7

Bin 4473: 196632 of cap free
Amount of items: 3
Items: 
Size: 269749 Color: 13
Size: 269082 Color: 12
Size: 264538 Color: 3

Bin 4474: 197456 of cap free
Amount of items: 3
Items: 
Size: 268941 Color: 6
Size: 267770 Color: 11
Size: 265834 Color: 9

Bin 4475: 200536 of cap free
Amount of items: 3
Items: 
Size: 268064 Color: 16
Size: 267944 Color: 17
Size: 263457 Color: 19

Bin 4476: 203185 of cap free
Amount of items: 3
Items: 
Size: 267664 Color: 2
Size: 265726 Color: 13
Size: 263426 Color: 3

Bin 4477: 206649 of cap free
Amount of items: 3
Items: 
Size: 268362 Color: 4
Size: 265375 Color: 14
Size: 259615 Color: 15

Bin 4478: 208477 of cap free
Amount of items: 3
Items: 
Size: 265572 Color: 5
Size: 265371 Color: 9
Size: 260581 Color: 7

Bin 4479: 210596 of cap free
Amount of items: 3
Items: 
Size: 264423 Color: 0
Size: 264420 Color: 17
Size: 260562 Color: 18

Bin 4480: 210760 of cap free
Amount of items: 3
Items: 
Size: 264131 Color: 11
Size: 263491 Color: 17
Size: 261619 Color: 19

Bin 4481: 214891 of cap free
Amount of items: 3
Items: 
Size: 265001 Color: 14
Size: 264977 Color: 16
Size: 255132 Color: 15

Bin 4482: 217972 of cap free
Amount of items: 3
Items: 
Size: 265151 Color: 11
Size: 261254 Color: 8
Size: 255624 Color: 10

Bin 4483: 219031 of cap free
Amount of items: 3
Items: 
Size: 261403 Color: 14
Size: 260452 Color: 5
Size: 259115 Color: 7

Bin 4484: 221708 of cap free
Amount of items: 3
Items: 
Size: 261554 Color: 19
Size: 261434 Color: 17
Size: 255305 Color: 3

Bin 4485: 222286 of cap free
Amount of items: 3
Items: 
Size: 260426 Color: 11
Size: 258817 Color: 12
Size: 258472 Color: 7

Bin 4486: 225975 of cap free
Amount of items: 3
Items: 
Size: 261008 Color: 8
Size: 260712 Color: 12
Size: 252306 Color: 9

Bin 4487: 231816 of cap free
Amount of items: 3
Items: 
Size: 260633 Color: 8
Size: 254584 Color: 10
Size: 252968 Color: 13

Bin 4488: 231929 of cap free
Amount of items: 3
Items: 
Size: 260568 Color: 19
Size: 254636 Color: 18
Size: 252868 Color: 15

Bin 4489: 237483 of cap free
Amount of items: 3
Items: 
Size: 254835 Color: 5
Size: 254816 Color: 14
Size: 252867 Color: 0

Bin 4490: 240093 of cap free
Amount of items: 3
Items: 
Size: 254724 Color: 6
Size: 252811 Color: 18
Size: 252373 Color: 3

Bin 4491: 243001 of cap free
Amount of items: 3
Items: 
Size: 252710 Color: 13
Size: 252351 Color: 10
Size: 251939 Color: 9

Bin 4492: 495695 of cap free
Amount of items: 2
Items: 
Size: 252329 Color: 16
Size: 251977 Color: 15

Total size: 4482835484
Total free space: 9169008

