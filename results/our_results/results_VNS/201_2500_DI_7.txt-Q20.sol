Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 2
Size: 1018 Color: 13
Size: 200 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1476 Color: 2
Size: 956 Color: 17
Size: 40 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1700 Color: 17
Size: 644 Color: 16
Size: 128 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 5
Size: 662 Color: 2
Size: 92 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 11
Size: 726 Color: 11
Size: 20 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 12
Size: 567 Color: 16
Size: 132 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 16
Size: 665 Color: 2
Size: 32 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1838 Color: 9
Size: 434 Color: 5
Size: 200 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1879 Color: 2
Size: 495 Color: 12
Size: 98 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 5
Size: 530 Color: 12
Size: 44 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1906 Color: 8
Size: 504 Color: 13
Size: 62 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1964 Color: 17
Size: 396 Color: 3
Size: 112 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2007 Color: 8
Size: 397 Color: 13
Size: 68 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 10
Size: 267 Color: 14
Size: 144 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 1
Size: 308 Color: 5
Size: 56 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 16
Size: 252 Color: 8
Size: 102 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2127 Color: 2
Size: 289 Color: 5
Size: 56 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2162 Color: 18
Size: 230 Color: 19
Size: 80 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 3
Size: 176 Color: 15
Size: 124 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 11
Size: 176 Color: 2
Size: 116 Color: 10

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 4
Size: 836 Color: 17
Size: 82 Color: 2

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1704 Color: 3
Size: 767 Color: 8

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1859 Color: 19
Size: 548 Color: 18
Size: 64 Color: 10

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1971 Color: 2
Size: 428 Color: 5
Size: 72 Color: 10

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 2004 Color: 15
Size: 459 Color: 6
Size: 8 Color: 3

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 2026 Color: 4
Size: 389 Color: 8
Size: 56 Color: 2

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 2097 Color: 0
Size: 374 Color: 12

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 17
Size: 1022 Color: 9
Size: 204 Color: 0

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 4
Size: 576 Color: 5
Size: 342 Color: 6

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1860 Color: 13
Size: 590 Color: 9
Size: 20 Color: 4

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1951 Color: 3
Size: 511 Color: 10
Size: 8 Color: 13

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1954 Color: 16
Size: 516 Color: 18

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 2051 Color: 4
Size: 419 Color: 8

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 2086 Color: 17
Size: 384 Color: 3

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 2198 Color: 10
Size: 272 Color: 13

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 9
Size: 1028 Color: 10
Size: 204 Color: 17

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1788 Color: 3
Size: 647 Color: 14
Size: 34 Color: 18

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1766 Color: 13
Size: 702 Color: 11

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1887 Color: 11
Size: 581 Color: 10

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1979 Color: 7
Size: 489 Color: 15

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1994 Color: 9
Size: 474 Color: 14

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 2066 Color: 4
Size: 402 Color: 15

Bin 43: 6 of cap free
Amount of items: 21
Items: 
Size: 244 Color: 5
Size: 204 Color: 10
Size: 168 Color: 7
Size: 160 Color: 14
Size: 152 Color: 5
Size: 152 Color: 3
Size: 132 Color: 9
Size: 132 Color: 4
Size: 112 Color: 1
Size: 104 Color: 6
Size: 96 Color: 17
Size: 96 Color: 8
Size: 96 Color: 2
Size: 92 Color: 18
Size: 88 Color: 16
Size: 86 Color: 13
Size: 84 Color: 15
Size: 76 Color: 10
Size: 76 Color: 4
Size: 64 Color: 12
Size: 52 Color: 19

Bin 44: 6 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 12
Size: 622 Color: 9
Size: 435 Color: 16

Bin 45: 6 of cap free
Amount of items: 3
Items: 
Size: 2057 Color: 3
Size: 401 Color: 10
Size: 8 Color: 3

Bin 46: 6 of cap free
Amount of items: 3
Items: 
Size: 2084 Color: 8
Size: 366 Color: 14
Size: 16 Color: 2

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 2142 Color: 18
Size: 324 Color: 12

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 2153 Color: 1
Size: 313 Color: 13

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 2188 Color: 8
Size: 278 Color: 0

Bin 50: 9 of cap free
Amount of items: 3
Items: 
Size: 1916 Color: 5
Size: 343 Color: 6
Size: 204 Color: 2

Bin 51: 10 of cap free
Amount of items: 4
Items: 
Size: 1238 Color: 16
Size: 840 Color: 7
Size: 272 Color: 13
Size: 112 Color: 9

Bin 52: 10 of cap free
Amount of items: 2
Items: 
Size: 1462 Color: 17
Size: 1000 Color: 7

Bin 53: 11 of cap free
Amount of items: 5
Items: 
Size: 1239 Color: 1
Size: 572 Color: 12
Size: 262 Color: 1
Size: 244 Color: 8
Size: 144 Color: 10

Bin 54: 11 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 15
Size: 765 Color: 19
Size: 18 Color: 7

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1793 Color: 10
Size: 667 Color: 12

Bin 56: 19 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 16
Size: 482 Color: 2
Size: 298 Color: 7

Bin 57: 20 of cap free
Amount of items: 2
Items: 
Size: 1422 Color: 19
Size: 1030 Color: 10

Bin 58: 22 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 17
Size: 732 Color: 11
Size: 116 Color: 10

Bin 59: 23 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 18
Size: 1031 Color: 1
Size: 172 Color: 8

Bin 60: 23 of cap free
Amount of items: 2
Items: 
Size: 1562 Color: 16
Size: 887 Color: 10

Bin 61: 32 of cap free
Amount of items: 2
Items: 
Size: 1555 Color: 13
Size: 885 Color: 6

Bin 62: 34 of cap free
Amount of items: 2
Items: 
Size: 1596 Color: 3
Size: 842 Color: 9

Bin 63: 35 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 15
Size: 878 Color: 12
Size: 148 Color: 1

Bin 64: 35 of cap free
Amount of items: 2
Items: 
Size: 1675 Color: 19
Size: 762 Color: 13

Bin 65: 39 of cap free
Amount of items: 2
Items: 
Size: 1404 Color: 3
Size: 1029 Color: 15

Bin 66: 2036 of cap free
Amount of items: 8
Items: 
Size: 72 Color: 2
Size: 64 Color: 0
Size: 52 Color: 12
Size: 52 Color: 11
Size: 52 Color: 7
Size: 48 Color: 19
Size: 48 Color: 14
Size: 48 Color: 6

Total size: 160680
Total free space: 2472

