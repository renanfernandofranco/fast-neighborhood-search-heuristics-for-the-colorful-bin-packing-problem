Capicity Bin: 15600
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 12360 Color: 0
Size: 2712 Color: 0
Size: 528 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 1
Size: 1532 Color: 0
Size: 784 Color: 0

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 10860 Color: 0
Size: 4168 Color: 1
Size: 268 Color: 1
Size: 224 Color: 1
Size: 80 Color: 0

Bin 4: 0 of cap free
Amount of items: 23
Items: 
Size: 976 Color: 1
Size: 960 Color: 1
Size: 960 Color: 1
Size: 912 Color: 1
Size: 896 Color: 1
Size: 888 Color: 1
Size: 704 Color: 1
Size: 704 Color: 1
Size: 672 Color: 0
Size: 656 Color: 1
Size: 640 Color: 1
Size: 640 Color: 1
Size: 632 Color: 0
Size: 588 Color: 0
Size: 584 Color: 0
Size: 572 Color: 0
Size: 542 Color: 0
Size: 534 Color: 0
Size: 528 Color: 0
Size: 524 Color: 0
Size: 496 Color: 0
Size: 496 Color: 0
Size: 496 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13976 Color: 1
Size: 848 Color: 0
Size: 776 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11759 Color: 0
Size: 2146 Color: 1
Size: 1695 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 0
Size: 1992 Color: 1
Size: 384 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 9196 Color: 0
Size: 5756 Color: 0
Size: 344 Color: 1
Size: 304 Color: 1

Bin 9: 0 of cap free
Amount of items: 34
Items: 
Size: 640 Color: 1
Size: 636 Color: 1
Size: 604 Color: 1
Size: 596 Color: 1
Size: 558 Color: 1
Size: 532 Color: 1
Size: 532 Color: 1
Size: 520 Color: 1
Size: 520 Color: 1
Size: 512 Color: 1
Size: 506 Color: 1
Size: 496 Color: 0
Size: 488 Color: 0
Size: 480 Color: 1
Size: 472 Color: 1
Size: 472 Color: 1
Size: 470 Color: 1
Size: 468 Color: 1
Size: 456 Color: 0
Size: 456 Color: 0
Size: 448 Color: 0
Size: 432 Color: 0
Size: 424 Color: 0
Size: 412 Color: 0
Size: 404 Color: 0
Size: 360 Color: 0
Size: 358 Color: 0
Size: 356 Color: 0
Size: 342 Color: 0
Size: 340 Color: 0
Size: 338 Color: 0
Size: 332 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0

Bin 10: 0 of cap free
Amount of items: 7
Items: 
Size: 7801 Color: 1
Size: 1901 Color: 1
Size: 1350 Color: 1
Size: 1280 Color: 0
Size: 1128 Color: 0
Size: 1124 Color: 0
Size: 1016 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13204 Color: 1
Size: 2372 Color: 0
Size: 24 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13598 Color: 0
Size: 1670 Color: 0
Size: 332 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11706 Color: 1
Size: 2004 Color: 0
Size: 1890 Color: 0

Bin 14: 0 of cap free
Amount of items: 8
Items: 
Size: 7805 Color: 1
Size: 1419 Color: 1
Size: 1332 Color: 1
Size: 1322 Color: 1
Size: 998 Color: 0
Size: 984 Color: 0
Size: 876 Color: 0
Size: 864 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13448 Color: 0
Size: 1688 Color: 1
Size: 464 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 10335 Color: 0
Size: 4389 Color: 1
Size: 876 Color: 0

Bin 17: 0 of cap free
Amount of items: 6
Items: 
Size: 7816 Color: 1
Size: 3646 Color: 0
Size: 1296 Color: 1
Size: 1216 Color: 1
Size: 832 Color: 0
Size: 794 Color: 0

Bin 18: 0 of cap free
Amount of items: 6
Items: 
Size: 7813 Color: 1
Size: 1755 Color: 1
Size: 1662 Color: 0
Size: 1546 Color: 1
Size: 1434 Color: 0
Size: 1390 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 0
Size: 3652 Color: 1
Size: 616 Color: 1

Bin 20: 0 of cap free
Amount of items: 4
Items: 
Size: 6497 Color: 1
Size: 6492 Color: 0
Size: 2351 Color: 1
Size: 260 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8828 Color: 1
Size: 5083 Color: 1
Size: 1689 Color: 0

Bin 22: 0 of cap free
Amount of items: 22
Items: 
Size: 8876 Color: 1
Size: 416 Color: 1
Size: 392 Color: 1
Size: 392 Color: 1
Size: 384 Color: 1
Size: 328 Color: 1
Size: 326 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 312 Color: 0
Size: 310 Color: 0
Size: 304 Color: 1
Size: 304 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 302 Color: 1
Size: 300 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 282 Color: 0
Size: 280 Color: 0
Size: 268 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 0
Size: 1464 Color: 1
Size: 300 Color: 1

Bin 24: 0 of cap free
Amount of items: 7
Items: 
Size: 7812 Color: 1
Size: 1476 Color: 1
Size: 1466 Color: 1
Size: 1368 Color: 0
Size: 1324 Color: 0
Size: 1298 Color: 0
Size: 856 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 9684 Color: 1
Size: 5640 Color: 0
Size: 276 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 9830 Color: 0
Size: 5448 Color: 1
Size: 322 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 10218 Color: 1
Size: 4874 Color: 1
Size: 508 Color: 0

Bin 28: 0 of cap free
Amount of items: 5
Items: 
Size: 10822 Color: 0
Size: 2448 Color: 0
Size: 1608 Color: 1
Size: 400 Color: 0
Size: 322 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 1
Size: 3956 Color: 1
Size: 372 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11794 Color: 1
Size: 3378 Color: 0
Size: 428 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11960 Color: 0
Size: 3354 Color: 0
Size: 286 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12023 Color: 0
Size: 2273 Color: 1
Size: 1304 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 0
Size: 2568 Color: 0
Size: 724 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12410 Color: 0
Size: 2662 Color: 0
Size: 528 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12672 Color: 0
Size: 2620 Color: 0
Size: 308 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12756 Color: 0
Size: 2596 Color: 0
Size: 248 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 1
Size: 2504 Color: 0
Size: 352 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12926 Color: 0
Size: 2382 Color: 1
Size: 292 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13068 Color: 0
Size: 1412 Color: 0
Size: 1120 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13114 Color: 1
Size: 1414 Color: 1
Size: 1072 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13154 Color: 1
Size: 1822 Color: 0
Size: 624 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13405 Color: 0
Size: 1811 Color: 0
Size: 384 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13455 Color: 1
Size: 1553 Color: 1
Size: 592 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13444 Color: 0
Size: 1804 Color: 0
Size: 352 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 0
Size: 1604 Color: 1
Size: 424 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13575 Color: 1
Size: 1619 Color: 1
Size: 406 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13576 Color: 0
Size: 1296 Color: 1
Size: 728 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13606 Color: 0
Size: 1562 Color: 1
Size: 432 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13639 Color: 0
Size: 1507 Color: 1
Size: 454 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13654 Color: 0
Size: 1506 Color: 1
Size: 440 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13726 Color: 0
Size: 1298 Color: 0
Size: 576 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13746 Color: 0
Size: 1144 Color: 0
Size: 710 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13768 Color: 1
Size: 1144 Color: 1
Size: 688 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13851 Color: 0
Size: 1459 Color: 1
Size: 290 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 0
Size: 1296 Color: 0
Size: 380 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13934 Color: 0
Size: 1144 Color: 0
Size: 522 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 0
Size: 1296 Color: 0
Size: 284 Color: 1

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 9732 Color: 0
Size: 4889 Color: 0
Size: 978 Color: 1

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12154 Color: 0
Size: 2661 Color: 0
Size: 784 Color: 1

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12371 Color: 1
Size: 2892 Color: 1
Size: 336 Color: 0

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 12391 Color: 0
Size: 3208 Color: 1

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 12478 Color: 0
Size: 2209 Color: 1
Size: 912 Color: 0

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 12997 Color: 1
Size: 2602 Color: 0

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13159 Color: 0
Size: 2184 Color: 1
Size: 256 Color: 0

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13319 Color: 0
Size: 1280 Color: 0
Size: 1000 Color: 1

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 9594 Color: 0
Size: 5621 Color: 1
Size: 384 Color: 0

Bin 67: 1 of cap free
Amount of items: 11
Items: 
Size: 7809 Color: 1
Size: 1040 Color: 1
Size: 1008 Color: 1
Size: 978 Color: 1
Size: 976 Color: 1
Size: 740 Color: 0
Size: 720 Color: 0
Size: 712 Color: 0
Size: 692 Color: 0
Size: 668 Color: 0
Size: 256 Color: 1

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 11752 Color: 0
Size: 3623 Color: 0
Size: 224 Color: 1

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13737 Color: 0
Size: 1622 Color: 1
Size: 240 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13875 Color: 0
Size: 1460 Color: 0
Size: 264 Color: 1

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13539 Color: 1
Size: 1668 Color: 0
Size: 392 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13906 Color: 1
Size: 1565 Color: 1
Size: 128 Color: 0

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 10835 Color: 0
Size: 4402 Color: 1
Size: 362 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13821 Color: 0
Size: 1602 Color: 1
Size: 176 Color: 1

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 12977 Color: 0
Size: 2482 Color: 1
Size: 140 Color: 1

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 9727 Color: 0
Size: 5656 Color: 0
Size: 216 Color: 1

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 10268 Color: 0
Size: 5006 Color: 1
Size: 324 Color: 0

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 10926 Color: 0
Size: 4408 Color: 0
Size: 264 Color: 1

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11636 Color: 0
Size: 3402 Color: 0
Size: 560 Color: 1

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11682 Color: 0
Size: 3604 Color: 0
Size: 312 Color: 1

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 11892 Color: 0
Size: 3706 Color: 1

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12120 Color: 1
Size: 3174 Color: 0
Size: 304 Color: 1

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 12132 Color: 0
Size: 2814 Color: 0
Size: 652 Color: 1

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 12746 Color: 0
Size: 2260 Color: 1
Size: 592 Color: 1

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 13524 Color: 1
Size: 2074 Color: 0

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 13675 Color: 1
Size: 1923 Color: 0

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 11754 Color: 1
Size: 3564 Color: 0
Size: 280 Color: 1

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 8296 Color: 1
Size: 6502 Color: 0
Size: 800 Color: 0

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 8726 Color: 1
Size: 6872 Color: 0

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 0
Size: 5730 Color: 0
Size: 544 Color: 1

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 12020 Color: 1
Size: 3266 Color: 0
Size: 312 Color: 0

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 9735 Color: 0
Size: 4999 Color: 1
Size: 864 Color: 1

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 9490 Color: 0
Size: 5723 Color: 0
Size: 384 Color: 1

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 9731 Color: 0
Size: 5578 Color: 0
Size: 288 Color: 1

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 0
Size: 2748 Color: 0
Size: 1605 Color: 1

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 11443 Color: 1
Size: 3898 Color: 0
Size: 256 Color: 0

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 12779 Color: 0
Size: 2280 Color: 1
Size: 538 Color: 0

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 10088 Color: 1
Size: 4932 Color: 1
Size: 576 Color: 0

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 10756 Color: 1
Size: 4840 Color: 0

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 1
Size: 1960 Color: 0

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 7806 Color: 0
Size: 6491 Color: 1
Size: 1298 Color: 1

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 13293 Color: 0
Size: 2302 Color: 1

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 13026 Color: 0
Size: 2568 Color: 1

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13794 Color: 0
Size: 1800 Color: 1

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 11339 Color: 0
Size: 4254 Color: 1

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 11777 Color: 1
Size: 3816 Color: 0

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 12622 Color: 1
Size: 2971 Color: 0

Bin 108: 7 of cap free
Amount of items: 2
Items: 
Size: 13226 Color: 0
Size: 2367 Color: 1

Bin 109: 7 of cap free
Amount of items: 3
Items: 
Size: 9603 Color: 0
Size: 5734 Color: 0
Size: 256 Color: 1

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 13550 Color: 1
Size: 2042 Color: 0

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 13882 Color: 0
Size: 1710 Color: 1

Bin 112: 9 of cap free
Amount of items: 2
Items: 
Size: 13447 Color: 1
Size: 2144 Color: 0

Bin 113: 11 of cap free
Amount of items: 2
Items: 
Size: 9501 Color: 1
Size: 6088 Color: 0

Bin 114: 12 of cap free
Amount of items: 2
Items: 
Size: 12037 Color: 0
Size: 3551 Color: 1

Bin 115: 12 of cap free
Amount of items: 2
Items: 
Size: 12387 Color: 1
Size: 3201 Color: 0

Bin 116: 13 of cap free
Amount of items: 3
Items: 
Size: 10840 Color: 0
Size: 3971 Color: 1
Size: 776 Color: 1

Bin 117: 13 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 0
Size: 2799 Color: 1

Bin 118: 13 of cap free
Amount of items: 2
Items: 
Size: 13053 Color: 1
Size: 2534 Color: 0

Bin 119: 14 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 1
Size: 1982 Color: 0

Bin 120: 14 of cap free
Amount of items: 2
Items: 
Size: 8722 Color: 1
Size: 6864 Color: 0

Bin 121: 15 of cap free
Amount of items: 2
Items: 
Size: 12604 Color: 1
Size: 2981 Color: 0

Bin 122: 16 of cap free
Amount of items: 3
Items: 
Size: 9080 Color: 1
Size: 5824 Color: 0
Size: 680 Color: 1

Bin 123: 16 of cap free
Amount of items: 2
Items: 
Size: 13139 Color: 0
Size: 2445 Color: 1

Bin 124: 16 of cap free
Amount of items: 2
Items: 
Size: 13236 Color: 1
Size: 2348 Color: 0

Bin 125: 17 of cap free
Amount of items: 2
Items: 
Size: 12872 Color: 1
Size: 2711 Color: 0

Bin 126: 17 of cap free
Amount of items: 2
Items: 
Size: 9707 Color: 0
Size: 5876 Color: 1

Bin 127: 17 of cap free
Amount of items: 2
Items: 
Size: 12347 Color: 1
Size: 3236 Color: 0

Bin 128: 17 of cap free
Amount of items: 2
Items: 
Size: 13723 Color: 1
Size: 1860 Color: 0

Bin 129: 18 of cap free
Amount of items: 2
Items: 
Size: 13793 Color: 0
Size: 1789 Color: 1

Bin 130: 20 of cap free
Amount of items: 6
Items: 
Size: 7804 Color: 1
Size: 1814 Color: 0
Size: 1804 Color: 0
Size: 1795 Color: 0
Size: 1611 Color: 1
Size: 752 Color: 1

Bin 131: 20 of cap free
Amount of items: 2
Items: 
Size: 13848 Color: 1
Size: 1732 Color: 0

Bin 132: 21 of cap free
Amount of items: 2
Items: 
Size: 12667 Color: 0
Size: 2912 Color: 1

Bin 133: 21 of cap free
Amount of items: 2
Items: 
Size: 13688 Color: 1
Size: 1891 Color: 0

Bin 134: 22 of cap free
Amount of items: 3
Items: 
Size: 11253 Color: 0
Size: 3885 Color: 1
Size: 440 Color: 0

Bin 135: 22 of cap free
Amount of items: 2
Items: 
Size: 12956 Color: 1
Size: 2622 Color: 0

Bin 136: 24 of cap free
Amount of items: 2
Items: 
Size: 13567 Color: 1
Size: 2009 Color: 0

Bin 137: 28 of cap free
Amount of items: 2
Items: 
Size: 13684 Color: 1
Size: 1888 Color: 0

Bin 138: 31 of cap free
Amount of items: 3
Items: 
Size: 8733 Color: 0
Size: 6500 Color: 0
Size: 336 Color: 1

Bin 139: 32 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 1
Size: 1528 Color: 0

Bin 140: 32 of cap free
Amount of items: 2
Items: 
Size: 12873 Color: 0
Size: 2695 Color: 1

Bin 141: 33 of cap free
Amount of items: 2
Items: 
Size: 13036 Color: 1
Size: 2531 Color: 0

Bin 142: 34 of cap free
Amount of items: 2
Items: 
Size: 11226 Color: 1
Size: 4340 Color: 0

Bin 143: 36 of cap free
Amount of items: 2
Items: 
Size: 10328 Color: 1
Size: 5236 Color: 0

Bin 144: 37 of cap free
Amount of items: 2
Items: 
Size: 12407 Color: 0
Size: 3156 Color: 1

Bin 145: 38 of cap free
Amount of items: 2
Items: 
Size: 11518 Color: 0
Size: 4044 Color: 1

Bin 146: 38 of cap free
Amount of items: 2
Items: 
Size: 12600 Color: 1
Size: 2962 Color: 0

Bin 147: 39 of cap free
Amount of items: 2
Items: 
Size: 13842 Color: 1
Size: 1719 Color: 0

Bin 148: 41 of cap free
Amount of items: 2
Items: 
Size: 13346 Color: 1
Size: 2213 Color: 0

Bin 149: 41 of cap free
Amount of items: 2
Items: 
Size: 13436 Color: 1
Size: 2123 Color: 0

Bin 150: 42 of cap free
Amount of items: 3
Items: 
Size: 12243 Color: 0
Size: 2171 Color: 1
Size: 1144 Color: 1

Bin 151: 46 of cap free
Amount of items: 2
Items: 
Size: 11578 Color: 1
Size: 3976 Color: 0

Bin 152: 46 of cap free
Amount of items: 2
Items: 
Size: 12367 Color: 1
Size: 3187 Color: 0

Bin 153: 46 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 0
Size: 2298 Color: 1

Bin 154: 51 of cap free
Amount of items: 2
Items: 
Size: 13667 Color: 0
Size: 1882 Color: 1

Bin 155: 51 of cap free
Amount of items: 2
Items: 
Size: 13514 Color: 0
Size: 2035 Color: 1

Bin 156: 54 of cap free
Amount of items: 2
Items: 
Size: 12454 Color: 1
Size: 3092 Color: 0

Bin 157: 56 of cap free
Amount of items: 2
Items: 
Size: 12496 Color: 0
Size: 3048 Color: 1

Bin 158: 57 of cap free
Amount of items: 2
Items: 
Size: 13427 Color: 1
Size: 2116 Color: 0

Bin 159: 61 of cap free
Amount of items: 2
Items: 
Size: 10939 Color: 1
Size: 4600 Color: 0

Bin 160: 66 of cap free
Amount of items: 2
Items: 
Size: 12226 Color: 0
Size: 3308 Color: 1

Bin 161: 67 of cap free
Amount of items: 2
Items: 
Size: 12068 Color: 1
Size: 3465 Color: 0

Bin 162: 69 of cap free
Amount of items: 2
Items: 
Size: 14018 Color: 1
Size: 1513 Color: 0

Bin 163: 71 of cap free
Amount of items: 2
Items: 
Size: 12842 Color: 1
Size: 2687 Color: 0

Bin 164: 75 of cap free
Amount of items: 2
Items: 
Size: 12846 Color: 0
Size: 2679 Color: 1

Bin 165: 76 of cap free
Amount of items: 2
Items: 
Size: 12563 Color: 1
Size: 2961 Color: 0

Bin 166: 89 of cap free
Amount of items: 2
Items: 
Size: 10600 Color: 1
Size: 4911 Color: 0

Bin 167: 102 of cap free
Amount of items: 2
Items: 
Size: 7802 Color: 0
Size: 7696 Color: 1

Bin 168: 116 of cap free
Amount of items: 2
Items: 
Size: 12984 Color: 0
Size: 2500 Color: 1

Bin 169: 124 of cap free
Amount of items: 2
Items: 
Size: 11032 Color: 1
Size: 4444 Color: 0

Bin 170: 134 of cap free
Amount of items: 2
Items: 
Size: 12050 Color: 1
Size: 3416 Color: 0

Bin 171: 142 of cap free
Amount of items: 2
Items: 
Size: 11284 Color: 1
Size: 4174 Color: 0

Bin 172: 142 of cap free
Amount of items: 2
Items: 
Size: 13486 Color: 1
Size: 1972 Color: 0

Bin 173: 144 of cap free
Amount of items: 2
Items: 
Size: 11512 Color: 1
Size: 3944 Color: 0

Bin 174: 156 of cap free
Amount of items: 2
Items: 
Size: 9800 Color: 0
Size: 5644 Color: 1

Bin 175: 159 of cap free
Amount of items: 2
Items: 
Size: 11154 Color: 0
Size: 4287 Color: 1

Bin 176: 160 of cap free
Amount of items: 2
Items: 
Size: 12492 Color: 0
Size: 2948 Color: 1

Bin 177: 160 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 1
Size: 2904 Color: 0

Bin 178: 169 of cap free
Amount of items: 2
Items: 
Size: 12951 Color: 0
Size: 2480 Color: 1

Bin 179: 171 of cap free
Amount of items: 2
Items: 
Size: 13785 Color: 0
Size: 1644 Color: 1

Bin 180: 179 of cap free
Amount of items: 2
Items: 
Size: 13982 Color: 1
Size: 1439 Color: 0

Bin 181: 201 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 0
Size: 1635 Color: 1

Bin 182: 205 of cap free
Amount of items: 2
Items: 
Size: 13191 Color: 1
Size: 2204 Color: 0

Bin 183: 207 of cap free
Amount of items: 2
Items: 
Size: 10498 Color: 1
Size: 4895 Color: 0

Bin 184: 242 of cap free
Amount of items: 2
Items: 
Size: 13426 Color: 1
Size: 1932 Color: 0

Bin 185: 244 of cap free
Amount of items: 2
Items: 
Size: 8855 Color: 0
Size: 6501 Color: 1

Bin 186: 251 of cap free
Amount of items: 2
Items: 
Size: 10457 Color: 1
Size: 4892 Color: 0

Bin 187: 266 of cap free
Amount of items: 2
Items: 
Size: 12460 Color: 0
Size: 2874 Color: 1

Bin 188: 272 of cap free
Amount of items: 3
Items: 
Size: 7848 Color: 1
Size: 6498 Color: 0
Size: 982 Color: 1

Bin 189: 276 of cap free
Amount of items: 2
Items: 
Size: 9320 Color: 1
Size: 6004 Color: 0

Bin 190: 288 of cap free
Amount of items: 2
Items: 
Size: 8840 Color: 0
Size: 6472 Color: 1

Bin 191: 300 of cap free
Amount of items: 2
Items: 
Size: 14004 Color: 0
Size: 1296 Color: 1

Bin 192: 302 of cap free
Amount of items: 2
Items: 
Size: 11724 Color: 1
Size: 3574 Color: 0

Bin 193: 387 of cap free
Amount of items: 2
Items: 
Size: 10322 Color: 1
Size: 4891 Color: 0

Bin 194: 394 of cap free
Amount of items: 2
Items: 
Size: 10396 Color: 0
Size: 4810 Color: 1

Bin 195: 407 of cap free
Amount of items: 2
Items: 
Size: 8700 Color: 1
Size: 6493 Color: 0

Bin 196: 1692 of cap free
Amount of items: 1
Items: 
Size: 13908 Color: 1

Bin 197: 1701 of cap free
Amount of items: 1
Items: 
Size: 13899 Color: 1

Bin 198: 1922 of cap free
Amount of items: 1
Items: 
Size: 13678 Color: 1

Bin 199: 1941 of cap free
Amount of items: 1
Items: 
Size: 13659 Color: 0

Total size: 3088800
Total free space: 15600

