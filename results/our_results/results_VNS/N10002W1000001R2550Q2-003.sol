Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3337
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 421921 Color: 0
Size: 303294 Color: 1
Size: 274786 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 404847 Color: 1
Size: 310845 Color: 0
Size: 284309 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 388847 Color: 0
Size: 308044 Color: 0
Size: 303110 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 369884 Color: 0
Size: 316824 Color: 0
Size: 313293 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 411957 Color: 1
Size: 317038 Color: 0
Size: 271006 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 379152 Color: 1
Size: 340453 Color: 0
Size: 280396 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 384553 Color: 0
Size: 351880 Color: 0
Size: 263568 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 358764 Color: 0
Size: 330941 Color: 0
Size: 310296 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 388031 Color: 1
Size: 338720 Color: 0
Size: 273250 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 385566 Color: 0
Size: 326513 Color: 1
Size: 287922 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 373144 Color: 0
Size: 320590 Color: 1
Size: 306267 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 453271 Color: 0
Size: 286092 Color: 0
Size: 260638 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 412164 Color: 1
Size: 334354 Color: 1
Size: 253483 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 462778 Color: 0
Size: 279367 Color: 0
Size: 257856 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 488827 Color: 1
Size: 256476 Color: 1
Size: 254698 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 487662 Color: 0
Size: 257578 Color: 1
Size: 254761 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 402068 Color: 1
Size: 329787 Color: 0
Size: 268146 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 413618 Color: 0
Size: 308426 Color: 1
Size: 277957 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 469265 Color: 0
Size: 266639 Color: 1
Size: 264097 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 406549 Color: 0
Size: 327168 Color: 1
Size: 266284 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 384885 Color: 1
Size: 319450 Color: 1
Size: 295666 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 489861 Color: 1
Size: 255629 Color: 0
Size: 254511 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 442400 Color: 0
Size: 307155 Color: 1
Size: 250446 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 434902 Color: 0
Size: 299970 Color: 0
Size: 265129 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 371853 Color: 0
Size: 319779 Color: 1
Size: 308369 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 378496 Color: 0
Size: 344114 Color: 0
Size: 277391 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 456019 Color: 0
Size: 279459 Color: 1
Size: 264523 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 391771 Color: 1
Size: 329027 Color: 0
Size: 279203 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 481554 Color: 0
Size: 260295 Color: 1
Size: 258152 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 440061 Color: 0
Size: 305207 Color: 1
Size: 254733 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 391069 Color: 0
Size: 342358 Color: 1
Size: 266574 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 459250 Color: 1
Size: 275840 Color: 0
Size: 264911 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 481369 Color: 1
Size: 267629 Color: 1
Size: 251003 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 435402 Color: 0
Size: 306533 Color: 0
Size: 258066 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 383832 Color: 1
Size: 313338 Color: 1
Size: 302831 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 406238 Color: 0
Size: 297295 Color: 0
Size: 296468 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 468113 Color: 1
Size: 281115 Color: 0
Size: 250773 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 449320 Color: 0
Size: 291650 Color: 1
Size: 259031 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 453974 Color: 0
Size: 285968 Color: 1
Size: 260059 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 372436 Color: 0
Size: 326614 Color: 0
Size: 300951 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 407366 Color: 1
Size: 317162 Color: 1
Size: 275473 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 433772 Color: 0
Size: 304681 Color: 1
Size: 261548 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 393138 Color: 1
Size: 348917 Color: 1
Size: 257946 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 406149 Color: 1
Size: 301549 Color: 0
Size: 292303 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 430699 Color: 0
Size: 294147 Color: 0
Size: 275155 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 391324 Color: 1
Size: 313346 Color: 0
Size: 295331 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 399135 Color: 0
Size: 327067 Color: 1
Size: 273799 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 376768 Color: 0
Size: 323169 Color: 0
Size: 300064 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 486315 Color: 0
Size: 258470 Color: 1
Size: 255216 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 417074 Color: 0
Size: 322744 Color: 1
Size: 260183 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 393962 Color: 0
Size: 321130 Color: 0
Size: 284909 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 379187 Color: 1
Size: 339862 Color: 0
Size: 280952 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 363813 Color: 0
Size: 346622 Color: 1
Size: 289566 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 363985 Color: 0
Size: 361620 Color: 0
Size: 274396 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 387649 Color: 0
Size: 358334 Color: 1
Size: 254018 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 391141 Color: 0
Size: 312223 Color: 0
Size: 296637 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 476430 Color: 1
Size: 265505 Color: 0
Size: 258066 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 495458 Color: 1
Size: 253083 Color: 0
Size: 251460 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 373089 Color: 1
Size: 329483 Color: 1
Size: 297429 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 416857 Color: 1
Size: 303447 Color: 1
Size: 279697 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 379432 Color: 1
Size: 347324 Color: 1
Size: 273245 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 338743 Color: 1
Size: 332131 Color: 1
Size: 329127 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 454749 Color: 0
Size: 291940 Color: 0
Size: 253312 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 447977 Color: 1
Size: 292812 Color: 0
Size: 259212 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 457682 Color: 1
Size: 292123 Color: 0
Size: 250196 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 459962 Color: 1
Size: 289855 Color: 0
Size: 250184 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 492540 Color: 1
Size: 255970 Color: 0
Size: 251491 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 444977 Color: 1
Size: 298144 Color: 0
Size: 256880 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 418076 Color: 0
Size: 303639 Color: 1
Size: 278286 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 477179 Color: 0
Size: 267092 Color: 0
Size: 255730 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 476261 Color: 0
Size: 262912 Color: 0
Size: 260828 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 434913 Color: 0
Size: 300412 Color: 1
Size: 264676 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 366858 Color: 0
Size: 318372 Color: 0
Size: 314771 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 377669 Color: 0
Size: 361194 Color: 0
Size: 261138 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 380738 Color: 0
Size: 366301 Color: 0
Size: 252962 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 419863 Color: 1
Size: 309621 Color: 0
Size: 270517 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 377121 Color: 1
Size: 327908 Color: 1
Size: 294972 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 367972 Color: 1
Size: 326202 Color: 1
Size: 305827 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 479388 Color: 1
Size: 267820 Color: 0
Size: 252793 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 422711 Color: 1
Size: 323115 Color: 1
Size: 254175 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 416924 Color: 0
Size: 327400 Color: 0
Size: 255677 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 373873 Color: 0
Size: 314105 Color: 0
Size: 312023 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 446074 Color: 0
Size: 294388 Color: 0
Size: 259539 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 421593 Color: 1
Size: 293204 Color: 0
Size: 285204 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 400243 Color: 1
Size: 336588 Color: 0
Size: 263170 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 396394 Color: 1
Size: 339393 Color: 1
Size: 264214 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 363161 Color: 1
Size: 319576 Color: 1
Size: 317264 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 426029 Color: 0
Size: 309460 Color: 1
Size: 264512 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 446071 Color: 1
Size: 286001 Color: 1
Size: 267929 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 465454 Color: 1
Size: 281278 Color: 1
Size: 253269 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 497062 Color: 0
Size: 252106 Color: 1
Size: 250833 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 396765 Color: 0
Size: 337802 Color: 1
Size: 265434 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 388920 Color: 0
Size: 311098 Color: 1
Size: 299983 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 412421 Color: 0
Size: 330249 Color: 0
Size: 257331 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 496240 Color: 0
Size: 253650 Color: 1
Size: 250111 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 421156 Color: 1
Size: 323942 Color: 0
Size: 254903 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 392959 Color: 1
Size: 336086 Color: 0
Size: 270956 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 476855 Color: 1
Size: 266100 Color: 0
Size: 257046 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 435810 Color: 1
Size: 293849 Color: 0
Size: 270342 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 440748 Color: 1
Size: 285418 Color: 0
Size: 273835 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 389728 Color: 1
Size: 346336 Color: 1
Size: 263937 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 355277 Color: 0
Size: 353804 Color: 1
Size: 290920 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 406999 Color: 0
Size: 313653 Color: 0
Size: 279349 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 401837 Color: 1
Size: 347009 Color: 0
Size: 251155 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 387506 Color: 1
Size: 336183 Color: 1
Size: 276312 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 382957 Color: 1
Size: 324296 Color: 1
Size: 292748 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 405626 Color: 1
Size: 304711 Color: 1
Size: 289664 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 371153 Color: 1
Size: 345727 Color: 0
Size: 283121 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 383567 Color: 1
Size: 358675 Color: 0
Size: 257759 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 377514 Color: 1
Size: 345992 Color: 1
Size: 276495 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 380956 Color: 1
Size: 309777 Color: 1
Size: 309268 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 384731 Color: 0
Size: 353092 Color: 1
Size: 262178 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 495228 Color: 0
Size: 254669 Color: 1
Size: 250104 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 355418 Color: 0
Size: 332145 Color: 1
Size: 312438 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 364475 Color: 0
Size: 353155 Color: 0
Size: 282371 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 420387 Color: 0
Size: 290395 Color: 1
Size: 289219 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 429851 Color: 1
Size: 316225 Color: 0
Size: 253925 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 467302 Color: 1
Size: 272538 Color: 0
Size: 260161 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 463330 Color: 1
Size: 276850 Color: 0
Size: 259821 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 417221 Color: 1
Size: 297840 Color: 0
Size: 284940 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 461185 Color: 0
Size: 277250 Color: 1
Size: 261566 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 378600 Color: 1
Size: 345464 Color: 1
Size: 275937 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 387911 Color: 0
Size: 337000 Color: 1
Size: 275090 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 471903 Color: 1
Size: 274954 Color: 0
Size: 253144 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 391862 Color: 1
Size: 324889 Color: 1
Size: 283250 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 400506 Color: 0
Size: 319058 Color: 0
Size: 280437 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 418202 Color: 1
Size: 329325 Color: 0
Size: 252474 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 383947 Color: 1
Size: 308611 Color: 0
Size: 307443 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 491938 Color: 0
Size: 257800 Color: 1
Size: 250263 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 369913 Color: 0
Size: 319746 Color: 1
Size: 310342 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 488581 Color: 1
Size: 259829 Color: 0
Size: 251591 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 402181 Color: 0
Size: 299400 Color: 1
Size: 298420 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 404663 Color: 0
Size: 342117 Color: 1
Size: 253221 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 497603 Color: 1
Size: 251408 Color: 0
Size: 250990 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 358605 Color: 1
Size: 351112 Color: 1
Size: 290284 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 388392 Color: 0
Size: 332900 Color: 1
Size: 278709 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 496923 Color: 1
Size: 252109 Color: 0
Size: 250969 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 438403 Color: 1
Size: 297523 Color: 0
Size: 264075 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 339108 Color: 1
Size: 332419 Color: 0
Size: 328474 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 376963 Color: 0
Size: 324048 Color: 1
Size: 298990 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 424486 Color: 1
Size: 305865 Color: 0
Size: 269650 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 481147 Color: 0
Size: 262699 Color: 0
Size: 256155 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 482433 Color: 1
Size: 260580 Color: 0
Size: 256988 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 399589 Color: 1
Size: 327401 Color: 1
Size: 273011 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 462474 Color: 1
Size: 279207 Color: 0
Size: 258320 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 493108 Color: 1
Size: 256685 Color: 0
Size: 250208 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 489753 Color: 0
Size: 257700 Color: 1
Size: 252548 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 369947 Color: 0
Size: 324100 Color: 1
Size: 305954 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 492522 Color: 1
Size: 256865 Color: 0
Size: 250614 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 375492 Color: 0
Size: 373380 Color: 1
Size: 251129 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 411906 Color: 0
Size: 323969 Color: 0
Size: 264126 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 369291 Color: 0
Size: 352856 Color: 0
Size: 277854 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 355575 Color: 0
Size: 332058 Color: 0
Size: 312368 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 397555 Color: 0
Size: 330370 Color: 1
Size: 272076 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 407676 Color: 1
Size: 303875 Color: 1
Size: 288450 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 436019 Color: 0
Size: 301898 Color: 1
Size: 262084 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 380799 Color: 1
Size: 366566 Color: 0
Size: 252636 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 379353 Color: 0
Size: 339759 Color: 0
Size: 280889 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 388971 Color: 0
Size: 352152 Color: 1
Size: 258878 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 404215 Color: 0
Size: 301219 Color: 1
Size: 294567 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 453856 Color: 1
Size: 292327 Color: 0
Size: 253818 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 350585 Color: 0
Size: 348558 Color: 1
Size: 300858 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 456609 Color: 0
Size: 271758 Color: 1
Size: 271634 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 367750 Color: 1
Size: 353189 Color: 0
Size: 279062 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 448315 Color: 0
Size: 286465 Color: 1
Size: 265221 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 431484 Color: 0
Size: 289226 Color: 1
Size: 279291 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 440639 Color: 0
Size: 309333 Color: 0
Size: 250029 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 387194 Color: 0
Size: 314889 Color: 1
Size: 297918 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 371590 Color: 0
Size: 341375 Color: 0
Size: 287036 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 412160 Color: 0
Size: 323611 Color: 0
Size: 264230 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 379841 Color: 1
Size: 311022 Color: 1
Size: 309138 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 488291 Color: 0
Size: 261005 Color: 1
Size: 250705 Color: 1

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 390517 Color: 1
Size: 357722 Color: 0
Size: 251762 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 445935 Color: 0
Size: 282818 Color: 0
Size: 271248 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 471304 Color: 1
Size: 275659 Color: 1
Size: 253038 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 390318 Color: 1
Size: 325577 Color: 0
Size: 284106 Color: 1

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 388527 Color: 1
Size: 317714 Color: 1
Size: 293760 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 380467 Color: 0
Size: 352486 Color: 1
Size: 267048 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 407133 Color: 1
Size: 303474 Color: 0
Size: 289394 Color: 1

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 370964 Color: 1
Size: 325801 Color: 0
Size: 303236 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 376394 Color: 1
Size: 317308 Color: 0
Size: 306299 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 396060 Color: 1
Size: 319078 Color: 0
Size: 284863 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 436102 Color: 1
Size: 284083 Color: 1
Size: 279816 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 354090 Color: 0
Size: 338343 Color: 1
Size: 307568 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 477997 Color: 1
Size: 270984 Color: 0
Size: 251020 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 387538 Color: 0
Size: 336532 Color: 1
Size: 275931 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 483222 Color: 0
Size: 262054 Color: 1
Size: 254725 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 492011 Color: 1
Size: 255284 Color: 1
Size: 252706 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 394081 Color: 0
Size: 311233 Color: 1
Size: 294687 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 455723 Color: 1
Size: 273023 Color: 1
Size: 271255 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 396612 Color: 0
Size: 351115 Color: 1
Size: 252274 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 445808 Color: 0
Size: 280030 Color: 1
Size: 274163 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 407596 Color: 0
Size: 339502 Color: 1
Size: 252903 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 408569 Color: 1
Size: 303323 Color: 0
Size: 288109 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 375602 Color: 0
Size: 363309 Color: 0
Size: 261090 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 485475 Color: 1
Size: 258915 Color: 1
Size: 255611 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 383307 Color: 1
Size: 342361 Color: 0
Size: 274333 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 369358 Color: 1
Size: 323613 Color: 1
Size: 307030 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 438847 Color: 0
Size: 286946 Color: 1
Size: 274208 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 483692 Color: 1
Size: 258930 Color: 1
Size: 257379 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 441634 Color: 0
Size: 303338 Color: 0
Size: 255029 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 479705 Color: 1
Size: 270272 Color: 0
Size: 250024 Color: 1

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 356246 Color: 0
Size: 353554 Color: 1
Size: 290201 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 385405 Color: 0
Size: 327463 Color: 0
Size: 287133 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 389546 Color: 0
Size: 353317 Color: 1
Size: 257138 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 482903 Color: 0
Size: 265047 Color: 1
Size: 252051 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 393032 Color: 0
Size: 324710 Color: 1
Size: 282259 Color: 1

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 484106 Color: 1
Size: 260213 Color: 1
Size: 255682 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 465657 Color: 0
Size: 282256 Color: 1
Size: 252088 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 436707 Color: 1
Size: 287273 Color: 1
Size: 276021 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 456982 Color: 1
Size: 285484 Color: 0
Size: 257535 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 483567 Color: 1
Size: 266316 Color: 1
Size: 250118 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 368865 Color: 1
Size: 351895 Color: 0
Size: 279241 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 474759 Color: 1
Size: 271615 Color: 0
Size: 253627 Color: 1

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 435968 Color: 1
Size: 286777 Color: 0
Size: 277256 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 388658 Color: 0
Size: 343038 Color: 1
Size: 268305 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 388802 Color: 1
Size: 338644 Color: 0
Size: 272555 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 394300 Color: 1
Size: 336461 Color: 1
Size: 269240 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 353285 Color: 1
Size: 343850 Color: 0
Size: 302866 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 367497 Color: 1
Size: 326976 Color: 1
Size: 305528 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 421939 Color: 0
Size: 320788 Color: 0
Size: 257274 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 347456 Color: 1
Size: 326340 Color: 1
Size: 326205 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 400324 Color: 1
Size: 341487 Color: 0
Size: 258190 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 475937 Color: 0
Size: 265947 Color: 1
Size: 258117 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 459050 Color: 0
Size: 284436 Color: 1
Size: 256515 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 378393 Color: 1
Size: 325707 Color: 1
Size: 295901 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 466647 Color: 0
Size: 276811 Color: 1
Size: 256543 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 414087 Color: 1
Size: 322938 Color: 0
Size: 262976 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 498742 Color: 1
Size: 250957 Color: 1
Size: 250302 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 380369 Color: 1
Size: 348856 Color: 0
Size: 270776 Color: 1

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 463257 Color: 1
Size: 283657 Color: 1
Size: 253087 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 495684 Color: 0
Size: 252233 Color: 0
Size: 252084 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 383164 Color: 0
Size: 365424 Color: 0
Size: 251413 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 371273 Color: 1
Size: 324028 Color: 0
Size: 304700 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 419211 Color: 1
Size: 295015 Color: 0
Size: 285775 Color: 1

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 434870 Color: 0
Size: 314954 Color: 1
Size: 250177 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 410227 Color: 0
Size: 332453 Color: 1
Size: 257321 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 388244 Color: 0
Size: 350286 Color: 1
Size: 261471 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 353614 Color: 1
Size: 351881 Color: 0
Size: 294506 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 419125 Color: 1
Size: 325853 Color: 0
Size: 255023 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 453859 Color: 0
Size: 282931 Color: 0
Size: 263211 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 375734 Color: 1
Size: 347996 Color: 0
Size: 276271 Color: 1

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 437026 Color: 0
Size: 288124 Color: 1
Size: 274851 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 494040 Color: 0
Size: 254728 Color: 0
Size: 251233 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 437781 Color: 0
Size: 305151 Color: 0
Size: 257069 Color: 1

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 473102 Color: 1
Size: 264713 Color: 1
Size: 262186 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 421925 Color: 0
Size: 317556 Color: 0
Size: 260520 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 491219 Color: 1
Size: 257938 Color: 0
Size: 250844 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 421218 Color: 0
Size: 301836 Color: 0
Size: 276947 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 387658 Color: 0
Size: 340025 Color: 1
Size: 272318 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 359528 Color: 0
Size: 347519 Color: 1
Size: 292954 Color: 1

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 416821 Color: 1
Size: 313229 Color: 0
Size: 269951 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 376104 Color: 1
Size: 320124 Color: 1
Size: 303773 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 442575 Color: 0
Size: 296623 Color: 1
Size: 260803 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 450872 Color: 1
Size: 296008 Color: 0
Size: 253121 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 405239 Color: 0
Size: 308992 Color: 0
Size: 285770 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 477200 Color: 1
Size: 271681 Color: 0
Size: 251120 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 408901 Color: 1
Size: 340117 Color: 0
Size: 250983 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 443149 Color: 0
Size: 278915 Color: 0
Size: 277937 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 362994 Color: 1
Size: 321502 Color: 0
Size: 315505 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 366996 Color: 0
Size: 356225 Color: 0
Size: 276780 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 410702 Color: 0
Size: 298518 Color: 1
Size: 290781 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 401390 Color: 1
Size: 306121 Color: 0
Size: 292490 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 424041 Color: 0
Size: 320599 Color: 0
Size: 255361 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 450505 Color: 0
Size: 282757 Color: 1
Size: 266739 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 366561 Color: 1
Size: 351674 Color: 1
Size: 281766 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 404211 Color: 0
Size: 334045 Color: 0
Size: 261745 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 472413 Color: 0
Size: 267603 Color: 0
Size: 259985 Color: 1

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 417213 Color: 1
Size: 326865 Color: 0
Size: 255923 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 488534 Color: 1
Size: 256578 Color: 0
Size: 254889 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 374758 Color: 1
Size: 321661 Color: 0
Size: 303582 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 376653 Color: 0
Size: 356489 Color: 0
Size: 266859 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 446414 Color: 1
Size: 286790 Color: 1
Size: 266797 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 483794 Color: 1
Size: 258625 Color: 1
Size: 257582 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 479411 Color: 1
Size: 268551 Color: 0
Size: 252039 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 423960 Color: 1
Size: 321520 Color: 1
Size: 254521 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 410939 Color: 0
Size: 334384 Color: 1
Size: 254678 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 434418 Color: 0
Size: 306796 Color: 1
Size: 258787 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 456266 Color: 0
Size: 290349 Color: 1
Size: 253386 Color: 1

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 455517 Color: 1
Size: 293985 Color: 1
Size: 250499 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 339145 Color: 0
Size: 330683 Color: 1
Size: 330173 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 402132 Color: 0
Size: 309081 Color: 1
Size: 288788 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 403370 Color: 1
Size: 327915 Color: 1
Size: 268716 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 465735 Color: 1
Size: 278055 Color: 1
Size: 256211 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 373342 Color: 1
Size: 368568 Color: 1
Size: 258091 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 359224 Color: 1
Size: 332785 Color: 1
Size: 307992 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 382504 Color: 0
Size: 349116 Color: 0
Size: 268381 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 374823 Color: 1
Size: 340616 Color: 0
Size: 284562 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 392546 Color: 0
Size: 324974 Color: 1
Size: 282481 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 382108 Color: 1
Size: 349217 Color: 0
Size: 268676 Color: 1

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 387851 Color: 0
Size: 333593 Color: 1
Size: 278557 Color: 1

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 400236 Color: 1
Size: 317036 Color: 1
Size: 282729 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 422599 Color: 1
Size: 303228 Color: 0
Size: 274174 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 444399 Color: 0
Size: 297231 Color: 0
Size: 258371 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 394773 Color: 1
Size: 348896 Color: 1
Size: 256332 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 413683 Color: 0
Size: 332069 Color: 1
Size: 254249 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 432993 Color: 0
Size: 290452 Color: 1
Size: 276556 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 386340 Color: 1
Size: 326985 Color: 0
Size: 286676 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 415024 Color: 0
Size: 328199 Color: 1
Size: 256778 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 357197 Color: 0
Size: 333794 Color: 1
Size: 309010 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 337234 Color: 1
Size: 335325 Color: 0
Size: 327442 Color: 1

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 421151 Color: 0
Size: 293186 Color: 0
Size: 285664 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 336610 Color: 1
Size: 335379 Color: 0
Size: 328012 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 361399 Color: 0
Size: 340585 Color: 0
Size: 298017 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 459387 Color: 1
Size: 282768 Color: 1
Size: 257846 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 413465 Color: 0
Size: 310696 Color: 1
Size: 275840 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 436362 Color: 1
Size: 286921 Color: 1
Size: 276718 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 480885 Color: 0
Size: 266258 Color: 0
Size: 252858 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 411322 Color: 0
Size: 313184 Color: 1
Size: 275495 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 383466 Color: 1
Size: 352684 Color: 1
Size: 263851 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 412000 Color: 0
Size: 334371 Color: 0
Size: 253630 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 449886 Color: 0
Size: 285614 Color: 1
Size: 264501 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 459311 Color: 1
Size: 281492 Color: 0
Size: 259198 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 407493 Color: 1
Size: 339902 Color: 0
Size: 252606 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 381758 Color: 0
Size: 343628 Color: 0
Size: 274615 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 404530 Color: 1
Size: 319007 Color: 0
Size: 276464 Color: 1

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 363678 Color: 1
Size: 353744 Color: 0
Size: 282579 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 439567 Color: 0
Size: 292602 Color: 0
Size: 267832 Color: 1

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 377755 Color: 1
Size: 348663 Color: 1
Size: 273583 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 419868 Color: 1
Size: 315752 Color: 0
Size: 264381 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 384611 Color: 0
Size: 343862 Color: 1
Size: 271528 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 392060 Color: 0
Size: 345828 Color: 1
Size: 262113 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 384704 Color: 1
Size: 352516 Color: 0
Size: 262781 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 460734 Color: 1
Size: 284807 Color: 0
Size: 254460 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 352859 Color: 0
Size: 334722 Color: 1
Size: 312420 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 422526 Color: 0
Size: 305783 Color: 1
Size: 271692 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 379140 Color: 0
Size: 364775 Color: 0
Size: 256086 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 415793 Color: 1
Size: 310916 Color: 0
Size: 273292 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 437941 Color: 1
Size: 286192 Color: 1
Size: 275868 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 461738 Color: 1
Size: 279648 Color: 0
Size: 258615 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 403835 Color: 0
Size: 343159 Color: 1
Size: 253007 Color: 1

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 450805 Color: 0
Size: 275041 Color: 1
Size: 274155 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 425946 Color: 0
Size: 290087 Color: 1
Size: 283968 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 346877 Color: 1
Size: 346382 Color: 0
Size: 306742 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 403290 Color: 0
Size: 326062 Color: 0
Size: 270649 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 371984 Color: 1
Size: 364528 Color: 1
Size: 263489 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 489234 Color: 0
Size: 257039 Color: 0
Size: 253728 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 404515 Color: 0
Size: 303844 Color: 1
Size: 291642 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 397570 Color: 0
Size: 329875 Color: 1
Size: 272556 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 451535 Color: 1
Size: 277280 Color: 0
Size: 271186 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 439978 Color: 0
Size: 296483 Color: 1
Size: 263540 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 444684 Color: 0
Size: 295237 Color: 1
Size: 260080 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 359260 Color: 0
Size: 341329 Color: 0
Size: 299412 Color: 1

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 399047 Color: 0
Size: 325083 Color: 1
Size: 275871 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 409301 Color: 1
Size: 330966 Color: 1
Size: 259734 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 445157 Color: 0
Size: 281453 Color: 1
Size: 273391 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 400980 Color: 0
Size: 303271 Color: 1
Size: 295750 Color: 1

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 422244 Color: 1
Size: 307093 Color: 0
Size: 270664 Color: 1

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 433746 Color: 1
Size: 305983 Color: 1
Size: 260272 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 369365 Color: 1
Size: 326144 Color: 0
Size: 304492 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 426211 Color: 1
Size: 298611 Color: 1
Size: 275179 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 479270 Color: 0
Size: 262128 Color: 1
Size: 258603 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 389913 Color: 0
Size: 333807 Color: 1
Size: 276281 Color: 1

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 412142 Color: 0
Size: 320394 Color: 1
Size: 267465 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 374104 Color: 1
Size: 330996 Color: 1
Size: 294901 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 454671 Color: 0
Size: 285576 Color: 1
Size: 259754 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 371577 Color: 1
Size: 370931 Color: 0
Size: 257493 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 466866 Color: 1
Size: 281387 Color: 1
Size: 251748 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 368676 Color: 1
Size: 338651 Color: 0
Size: 292674 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 384595 Color: 0
Size: 363174 Color: 1
Size: 252232 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 492311 Color: 0
Size: 254032 Color: 0
Size: 253658 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 397780 Color: 1
Size: 337040 Color: 0
Size: 265181 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 416335 Color: 0
Size: 297198 Color: 1
Size: 286468 Color: 1

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 399270 Color: 0
Size: 310419 Color: 0
Size: 290312 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 391798 Color: 1
Size: 347081 Color: 1
Size: 261122 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 413170 Color: 0
Size: 327917 Color: 1
Size: 258914 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 491467 Color: 0
Size: 257546 Color: 0
Size: 250988 Color: 1

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 407914 Color: 0
Size: 304622 Color: 1
Size: 287465 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 440277 Color: 1
Size: 295672 Color: 1
Size: 264052 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 436576 Color: 1
Size: 288114 Color: 1
Size: 275311 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 394718 Color: 0
Size: 326910 Color: 1
Size: 278373 Color: 1

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 367445 Color: 0
Size: 337492 Color: 1
Size: 295064 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 453603 Color: 1
Size: 294358 Color: 0
Size: 252040 Color: 1

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 387099 Color: 1
Size: 348340 Color: 1
Size: 264562 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 373367 Color: 1
Size: 363113 Color: 0
Size: 263521 Color: 1

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 435218 Color: 1
Size: 289416 Color: 1
Size: 275367 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 410104 Color: 0
Size: 321192 Color: 1
Size: 268705 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 430941 Color: 1
Size: 295614 Color: 0
Size: 273446 Color: 1

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 422810 Color: 0
Size: 311546 Color: 1
Size: 265645 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 400686 Color: 1
Size: 340846 Color: 1
Size: 258469 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 346351 Color: 0
Size: 345077 Color: 1
Size: 308573 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 388678 Color: 1
Size: 348219 Color: 0
Size: 263104 Color: 1

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 408186 Color: 0
Size: 327725 Color: 0
Size: 264090 Color: 1

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 428192 Color: 1
Size: 319533 Color: 0
Size: 252276 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 355050 Color: 0
Size: 339367 Color: 0
Size: 305584 Color: 1

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 386977 Color: 1
Size: 349744 Color: 1
Size: 263280 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 379582 Color: 1
Size: 332350 Color: 0
Size: 288069 Color: 1

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 360507 Color: 1
Size: 356500 Color: 0
Size: 282994 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 435691 Color: 1
Size: 310339 Color: 0
Size: 253971 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 374559 Color: 0
Size: 355422 Color: 0
Size: 270020 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 382807 Color: 1
Size: 351288 Color: 0
Size: 265906 Color: 1

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 417492 Color: 0
Size: 324255 Color: 1
Size: 258254 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 343725 Color: 1
Size: 342752 Color: 1
Size: 313524 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 364741 Color: 0
Size: 350403 Color: 1
Size: 284857 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 375823 Color: 0
Size: 342029 Color: 1
Size: 282149 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 384441 Color: 1
Size: 360888 Color: 1
Size: 254672 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 396799 Color: 1
Size: 348600 Color: 1
Size: 254602 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 444479 Color: 1
Size: 300236 Color: 0
Size: 255286 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 391718 Color: 0
Size: 306547 Color: 0
Size: 301736 Color: 1

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 446272 Color: 1
Size: 280230 Color: 1
Size: 273499 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 486762 Color: 1
Size: 261901 Color: 1
Size: 251338 Color: 0

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 454298 Color: 1
Size: 280744 Color: 0
Size: 264959 Color: 0

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 415601 Color: 1
Size: 313052 Color: 0
Size: 271348 Color: 1

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 395997 Color: 0
Size: 338443 Color: 0
Size: 265561 Color: 1

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 385922 Color: 1
Size: 350047 Color: 1
Size: 264032 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 367641 Color: 0
Size: 345769 Color: 1
Size: 286591 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 441761 Color: 0
Size: 295556 Color: 1
Size: 262684 Color: 1

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 485453 Color: 1
Size: 257980 Color: 1
Size: 256568 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 361868 Color: 1
Size: 319089 Color: 0
Size: 319044 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 493709 Color: 1
Size: 254902 Color: 0
Size: 251390 Color: 1

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 415119 Color: 0
Size: 307281 Color: 1
Size: 277601 Color: 1

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 359347 Color: 1
Size: 341757 Color: 1
Size: 298897 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 373955 Color: 0
Size: 323747 Color: 1
Size: 302299 Color: 1

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 380212 Color: 0
Size: 337790 Color: 1
Size: 281999 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 373272 Color: 0
Size: 342098 Color: 1
Size: 284631 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 406582 Color: 0
Size: 297872 Color: 1
Size: 295547 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 438901 Color: 1
Size: 307103 Color: 1
Size: 253997 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 390917 Color: 0
Size: 348327 Color: 1
Size: 260757 Color: 0

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 412132 Color: 1
Size: 332299 Color: 0
Size: 255570 Color: 1

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 401171 Color: 1
Size: 317300 Color: 0
Size: 281530 Color: 1

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 379855 Color: 0
Size: 355934 Color: 1
Size: 264212 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 422479 Color: 0
Size: 323460 Color: 1
Size: 254062 Color: 0

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 406126 Color: 0
Size: 327650 Color: 0
Size: 266225 Color: 1

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 420124 Color: 0
Size: 321482 Color: 0
Size: 258395 Color: 1

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 393728 Color: 1
Size: 341079 Color: 0
Size: 265194 Color: 1

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 430921 Color: 0
Size: 303228 Color: 0
Size: 265852 Color: 1

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 416964 Color: 0
Size: 291935 Color: 1
Size: 291102 Color: 1

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 376218 Color: 1
Size: 369375 Color: 1
Size: 254408 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 388380 Color: 1
Size: 353943 Color: 0
Size: 257678 Color: 1

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 455091 Color: 0
Size: 292983 Color: 0
Size: 251927 Color: 1

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 356484 Color: 0
Size: 346487 Color: 1
Size: 297030 Color: 1

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 499528 Color: 1
Size: 250388 Color: 1
Size: 250085 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 384687 Color: 0
Size: 325027 Color: 1
Size: 290287 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 468996 Color: 0
Size: 275213 Color: 1
Size: 255792 Color: 1

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 442791 Color: 0
Size: 295320 Color: 1
Size: 261890 Color: 1

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 400641 Color: 1
Size: 330147 Color: 1
Size: 269213 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 425518 Color: 1
Size: 320685 Color: 1
Size: 253798 Color: 0

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 496826 Color: 1
Size: 252100 Color: 0
Size: 251075 Color: 1

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 463601 Color: 0
Size: 282852 Color: 0
Size: 253548 Color: 1

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 387480 Color: 1
Size: 319149 Color: 1
Size: 293372 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 476757 Color: 0
Size: 265289 Color: 1
Size: 257955 Color: 1

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 404339 Color: 1
Size: 339503 Color: 0
Size: 256159 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 452701 Color: 1
Size: 282500 Color: 1
Size: 264800 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 387177 Color: 1
Size: 319585 Color: 1
Size: 293239 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 395801 Color: 1
Size: 316962 Color: 1
Size: 287238 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 369792 Color: 0
Size: 334596 Color: 0
Size: 295613 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 474957 Color: 0
Size: 270404 Color: 1
Size: 254640 Color: 1

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 394740 Color: 1
Size: 305491 Color: 1
Size: 299770 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 359709 Color: 0
Size: 346127 Color: 0
Size: 294165 Color: 1

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 432074 Color: 0
Size: 298913 Color: 1
Size: 269014 Color: 0

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 377390 Color: 1
Size: 311448 Color: 1
Size: 311163 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 402031 Color: 1
Size: 334801 Color: 0
Size: 263169 Color: 1

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 490847 Color: 0
Size: 257295 Color: 0
Size: 251859 Color: 1

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 392107 Color: 0
Size: 327938 Color: 0
Size: 279956 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 417755 Color: 1
Size: 296113 Color: 1
Size: 286133 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 448583 Color: 1
Size: 297888 Color: 1
Size: 253530 Color: 0

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 371714 Color: 1
Size: 351802 Color: 0
Size: 276485 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 382083 Color: 1
Size: 318203 Color: 0
Size: 299715 Color: 1

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 410323 Color: 1
Size: 335597 Color: 0
Size: 254081 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 436667 Color: 1
Size: 296232 Color: 0
Size: 267102 Color: 0

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 447861 Color: 1
Size: 282469 Color: 0
Size: 269671 Color: 1

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 397104 Color: 0
Size: 319307 Color: 1
Size: 283590 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 451917 Color: 0
Size: 289925 Color: 1
Size: 258159 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 477415 Color: 0
Size: 267606 Color: 1
Size: 254980 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 374775 Color: 1
Size: 343339 Color: 0
Size: 281887 Color: 1

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 377450 Color: 0
Size: 335114 Color: 0
Size: 287437 Color: 1

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 401095 Color: 0
Size: 344526 Color: 0
Size: 254380 Color: 1

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 357136 Color: 0
Size: 339430 Color: 1
Size: 303435 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 429388 Color: 0
Size: 316094 Color: 1
Size: 254519 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 484949 Color: 0
Size: 259999 Color: 0
Size: 255053 Color: 1

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 428149 Color: 1
Size: 316718 Color: 0
Size: 255134 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 493431 Color: 1
Size: 256322 Color: 1
Size: 250248 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 395142 Color: 0
Size: 341328 Color: 1
Size: 263531 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 378378 Color: 0
Size: 357558 Color: 0
Size: 264065 Color: 1

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 367334 Color: 1
Size: 347512 Color: 1
Size: 285155 Color: 0

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 410661 Color: 0
Size: 301632 Color: 1
Size: 287708 Color: 1

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 397800 Color: 1
Size: 315791 Color: 1
Size: 286410 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 465855 Color: 1
Size: 275666 Color: 0
Size: 258480 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 387292 Color: 1
Size: 326125 Color: 1
Size: 286584 Color: 0

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 389545 Color: 0
Size: 350334 Color: 0
Size: 260122 Color: 1

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 464809 Color: 0
Size: 278293 Color: 0
Size: 256899 Color: 1

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 459326 Color: 1
Size: 286058 Color: 1
Size: 254617 Color: 0

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 360627 Color: 0
Size: 351259 Color: 1
Size: 288115 Color: 1

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 471255 Color: 0
Size: 270330 Color: 0
Size: 258416 Color: 1

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 386530 Color: 0
Size: 356534 Color: 1
Size: 256937 Color: 1

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 488415 Color: 1
Size: 261004 Color: 1
Size: 250582 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 371734 Color: 1
Size: 351396 Color: 0
Size: 276871 Color: 1

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 425272 Color: 1
Size: 312636 Color: 1
Size: 262093 Color: 0

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 430277 Color: 0
Size: 293678 Color: 1
Size: 276046 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 422938 Color: 0
Size: 306681 Color: 0
Size: 270382 Color: 1

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 359997 Color: 0
Size: 337016 Color: 1
Size: 302988 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 437411 Color: 0
Size: 298614 Color: 1
Size: 263976 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 395292 Color: 0
Size: 341827 Color: 1
Size: 262882 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 421447 Color: 0
Size: 301192 Color: 1
Size: 277362 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 370769 Color: 1
Size: 363733 Color: 1
Size: 265499 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 427539 Color: 0
Size: 307366 Color: 1
Size: 265096 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 372637 Color: 1
Size: 319031 Color: 0
Size: 308333 Color: 1

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 378222 Color: 1
Size: 344909 Color: 1
Size: 276870 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 403503 Color: 1
Size: 346451 Color: 1
Size: 250047 Color: 0

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 431472 Color: 0
Size: 300184 Color: 1
Size: 268345 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 386279 Color: 1
Size: 335402 Color: 0
Size: 278320 Color: 1

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 461284 Color: 0
Size: 270204 Color: 0
Size: 268513 Color: 1

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 380882 Color: 0
Size: 310066 Color: 0
Size: 309053 Color: 1

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 445873 Color: 0
Size: 278647 Color: 1
Size: 275481 Color: 1

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 365670 Color: 1
Size: 332983 Color: 1
Size: 301348 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 486858 Color: 1
Size: 259489 Color: 0
Size: 253654 Color: 1

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 497239 Color: 0
Size: 252011 Color: 1
Size: 250751 Color: 0

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 496273 Color: 1
Size: 253471 Color: 1
Size: 250257 Color: 0

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 451227 Color: 0
Size: 297234 Color: 1
Size: 251540 Color: 0

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 400565 Color: 1
Size: 340500 Color: 0
Size: 258936 Color: 1

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 431549 Color: 1
Size: 313772 Color: 0
Size: 254680 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 435686 Color: 1
Size: 287906 Color: 1
Size: 276409 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 404882 Color: 0
Size: 298617 Color: 0
Size: 296502 Color: 1

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 378376 Color: 1
Size: 349020 Color: 1
Size: 272605 Color: 0

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 406600 Color: 0
Size: 301888 Color: 0
Size: 291513 Color: 1

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 442099 Color: 1
Size: 300188 Color: 0
Size: 257714 Color: 1

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 374967 Color: 0
Size: 317301 Color: 0
Size: 307733 Color: 1

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 354983 Color: 1
Size: 349268 Color: 0
Size: 295750 Color: 1

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 495692 Color: 1
Size: 253797 Color: 0
Size: 250512 Color: 1

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 429270 Color: 0
Size: 308902 Color: 0
Size: 261829 Color: 1

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 339469 Color: 1
Size: 332377 Color: 0
Size: 328155 Color: 0

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 486380 Color: 1
Size: 257791 Color: 1
Size: 255830 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 491696 Color: 1
Size: 257459 Color: 0
Size: 250846 Color: 0

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 376125 Color: 0
Size: 346363 Color: 0
Size: 277513 Color: 1

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 393437 Color: 1
Size: 326741 Color: 0
Size: 279823 Color: 1

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 470622 Color: 1
Size: 264769 Color: 0
Size: 264610 Color: 1

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 368991 Color: 1
Size: 340401 Color: 1
Size: 290609 Color: 0

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 398806 Color: 0
Size: 340145 Color: 1
Size: 261050 Color: 1

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 409467 Color: 0
Size: 334162 Color: 0
Size: 256372 Color: 1

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 371522 Color: 0
Size: 328868 Color: 1
Size: 299611 Color: 0

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 424959 Color: 0
Size: 321547 Color: 0
Size: 253495 Color: 1

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 437853 Color: 1
Size: 305292 Color: 1
Size: 256856 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 398271 Color: 1
Size: 326796 Color: 0
Size: 274934 Color: 1

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 399748 Color: 0
Size: 340042 Color: 0
Size: 260211 Color: 1

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 480100 Color: 1
Size: 261294 Color: 0
Size: 258607 Color: 0

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 399646 Color: 0
Size: 318726 Color: 0
Size: 281629 Color: 1

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 434201 Color: 1
Size: 311135 Color: 1
Size: 254665 Color: 0

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 424040 Color: 1
Size: 295817 Color: 0
Size: 280144 Color: 0

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 370528 Color: 0
Size: 352141 Color: 1
Size: 277332 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 427885 Color: 0
Size: 318791 Color: 1
Size: 253325 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 455102 Color: 1
Size: 274649 Color: 0
Size: 270250 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 363712 Color: 0
Size: 341671 Color: 0
Size: 294618 Color: 1

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 417915 Color: 1
Size: 292579 Color: 0
Size: 289507 Color: 0

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 376591 Color: 0
Size: 373130 Color: 0
Size: 250280 Color: 1

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 377761 Color: 1
Size: 343170 Color: 0
Size: 279070 Color: 0

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 385993 Color: 1
Size: 320849 Color: 0
Size: 293159 Color: 1

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 414933 Color: 0
Size: 327469 Color: 1
Size: 257599 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 424364 Color: 0
Size: 288749 Color: 1
Size: 286888 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 452112 Color: 0
Size: 297054 Color: 1
Size: 250835 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 399964 Color: 0
Size: 310066 Color: 0
Size: 289971 Color: 1

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 396757 Color: 1
Size: 307700 Color: 0
Size: 295544 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 369230 Color: 0
Size: 351134 Color: 0
Size: 279637 Color: 1

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 431663 Color: 0
Size: 284265 Color: 0
Size: 284073 Color: 1

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 422742 Color: 1
Size: 309231 Color: 0
Size: 268028 Color: 1

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 474995 Color: 0
Size: 272366 Color: 0
Size: 252640 Color: 1

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 357420 Color: 1
Size: 323657 Color: 0
Size: 318924 Color: 1

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 413255 Color: 1
Size: 320229 Color: 0
Size: 266517 Color: 1

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 348378 Color: 1
Size: 336646 Color: 0
Size: 314977 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 436734 Color: 0
Size: 309894 Color: 0
Size: 253373 Color: 1

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 481413 Color: 0
Size: 264884 Color: 1
Size: 253704 Color: 1

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 490353 Color: 0
Size: 258942 Color: 1
Size: 250706 Color: 1

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 434825 Color: 0
Size: 284506 Color: 0
Size: 280670 Color: 1

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 398216 Color: 1
Size: 315449 Color: 0
Size: 286336 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 376556 Color: 0
Size: 360218 Color: 0
Size: 263227 Color: 1

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 450078 Color: 1
Size: 284343 Color: 1
Size: 265580 Color: 0

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 416296 Color: 1
Size: 292626 Color: 1
Size: 291079 Color: 0

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 426760 Color: 1
Size: 322587 Color: 0
Size: 250654 Color: 0

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 487948 Color: 1
Size: 261325 Color: 0
Size: 250728 Color: 1

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 398254 Color: 0
Size: 342016 Color: 0
Size: 259731 Color: 1

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 437725 Color: 1
Size: 297415 Color: 0
Size: 264861 Color: 1

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 347388 Color: 0
Size: 328202 Color: 0
Size: 324411 Color: 1

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 441084 Color: 0
Size: 295425 Color: 1
Size: 263492 Color: 1

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 361069 Color: 1
Size: 338542 Color: 0
Size: 300390 Color: 1

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 412730 Color: 1
Size: 330511 Color: 0
Size: 256760 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 376331 Color: 1
Size: 358554 Color: 0
Size: 265116 Color: 1

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 388860 Color: 0
Size: 356044 Color: 1
Size: 255097 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 458894 Color: 1
Size: 282312 Color: 0
Size: 258795 Color: 1

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 484120 Color: 1
Size: 259418 Color: 0
Size: 256463 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 382210 Color: 0
Size: 321691 Color: 0
Size: 296100 Color: 1

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 388491 Color: 0
Size: 322369 Color: 1
Size: 289141 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 397167 Color: 0
Size: 332531 Color: 0
Size: 270303 Color: 1

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 358157 Color: 0
Size: 348145 Color: 0
Size: 293699 Color: 1

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 398061 Color: 1
Size: 334488 Color: 0
Size: 267452 Color: 1

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 348249 Color: 1
Size: 329726 Color: 0
Size: 322026 Color: 1

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 384295 Color: 1
Size: 361907 Color: 1
Size: 253799 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 468099 Color: 0
Size: 274473 Color: 0
Size: 257429 Color: 1

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 444418 Color: 1
Size: 302582 Color: 0
Size: 253001 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 430837 Color: 1
Size: 298079 Color: 0
Size: 271085 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 434689 Color: 1
Size: 310589 Color: 0
Size: 254723 Color: 0

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 480709 Color: 1
Size: 269123 Color: 0
Size: 250169 Color: 1

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 392199 Color: 0
Size: 341493 Color: 1
Size: 266309 Color: 1

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 417788 Color: 1
Size: 294940 Color: 1
Size: 287273 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 409931 Color: 0
Size: 315610 Color: 0
Size: 274460 Color: 1

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 476268 Color: 0
Size: 271011 Color: 0
Size: 252722 Color: 1

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 399317 Color: 0
Size: 303332 Color: 1
Size: 297352 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 429308 Color: 0
Size: 301001 Color: 1
Size: 269692 Color: 0

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 385450 Color: 1
Size: 361262 Color: 0
Size: 253289 Color: 1

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 388637 Color: 1
Size: 324192 Color: 0
Size: 287172 Color: 0

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 384971 Color: 0
Size: 338451 Color: 1
Size: 276579 Color: 0

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 449781 Color: 0
Size: 287103 Color: 0
Size: 263117 Color: 1

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 392965 Color: 0
Size: 327255 Color: 0
Size: 279781 Color: 1

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 401665 Color: 1
Size: 336605 Color: 1
Size: 261731 Color: 0

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 389937 Color: 1
Size: 340711 Color: 1
Size: 269353 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 404170 Color: 0
Size: 345540 Color: 0
Size: 250291 Color: 1

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 381477 Color: 1
Size: 319681 Color: 1
Size: 298843 Color: 0

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 421890 Color: 1
Size: 319900 Color: 0
Size: 258211 Color: 1

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 387876 Color: 0
Size: 329845 Color: 0
Size: 282280 Color: 1

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 444482 Color: 0
Size: 278112 Color: 1
Size: 277407 Color: 0

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 448106 Color: 1
Size: 294077 Color: 0
Size: 257818 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 417472 Color: 1
Size: 307608 Color: 1
Size: 274921 Color: 0

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 409753 Color: 0
Size: 307102 Color: 0
Size: 283146 Color: 1

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 407671 Color: 1
Size: 330639 Color: 0
Size: 261691 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 422390 Color: 0
Size: 302998 Color: 0
Size: 274613 Color: 1

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 384864 Color: 1
Size: 345508 Color: 0
Size: 269629 Color: 0

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 411461 Color: 1
Size: 303644 Color: 1
Size: 284896 Color: 0

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 490984 Color: 0
Size: 256458 Color: 1
Size: 252559 Color: 1

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 437339 Color: 0
Size: 311344 Color: 0
Size: 251318 Color: 1

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 419035 Color: 1
Size: 308200 Color: 0
Size: 272766 Color: 1

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 433008 Color: 0
Size: 294136 Color: 0
Size: 272857 Color: 1

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 439488 Color: 1
Size: 286346 Color: 1
Size: 274167 Color: 0

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 446655 Color: 0
Size: 278524 Color: 1
Size: 274822 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 399411 Color: 1
Size: 313266 Color: 1
Size: 287324 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 394684 Color: 0
Size: 345447 Color: 1
Size: 259870 Color: 1

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 407556 Color: 1
Size: 305426 Color: 0
Size: 287019 Color: 1

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 432551 Color: 0
Size: 314073 Color: 1
Size: 253377 Color: 1

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 436910 Color: 1
Size: 302917 Color: 0
Size: 260174 Color: 0

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 444465 Color: 1
Size: 297858 Color: 0
Size: 257678 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 363579 Color: 1
Size: 334219 Color: 1
Size: 302203 Color: 0

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 389775 Color: 0
Size: 323515 Color: 1
Size: 286711 Color: 1

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 462020 Color: 1
Size: 282004 Color: 0
Size: 255977 Color: 0

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 458316 Color: 0
Size: 280027 Color: 1
Size: 261658 Color: 0

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 456900 Color: 1
Size: 279927 Color: 0
Size: 263174 Color: 1

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 386152 Color: 1
Size: 345545 Color: 0
Size: 268304 Color: 1

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 412366 Color: 1
Size: 324851 Color: 0
Size: 262784 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 415539 Color: 0
Size: 327452 Color: 0
Size: 257010 Color: 1

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 362284 Color: 0
Size: 341368 Color: 1
Size: 296349 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 493585 Color: 0
Size: 256411 Color: 0
Size: 250005 Color: 1

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 408744 Color: 0
Size: 330596 Color: 0
Size: 260661 Color: 1

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 384804 Color: 1
Size: 319936 Color: 0
Size: 295261 Color: 1

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 428009 Color: 1
Size: 320651 Color: 0
Size: 251341 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 439852 Color: 1
Size: 281138 Color: 0
Size: 279011 Color: 0

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 405448 Color: 1
Size: 331656 Color: 1
Size: 262897 Color: 0

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 409075 Color: 0
Size: 333852 Color: 0
Size: 257074 Color: 1

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 447050 Color: 1
Size: 287493 Color: 0
Size: 265458 Color: 1

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 458081 Color: 1
Size: 283858 Color: 1
Size: 258062 Color: 0

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 448946 Color: 0
Size: 284751 Color: 1
Size: 266304 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 413769 Color: 0
Size: 327693 Color: 1
Size: 258539 Color: 1

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 404001 Color: 1
Size: 300275 Color: 0
Size: 295725 Color: 1

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 428456 Color: 1
Size: 316602 Color: 1
Size: 254943 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 375435 Color: 0
Size: 345080 Color: 1
Size: 279486 Color: 1

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 403051 Color: 1
Size: 320325 Color: 1
Size: 276625 Color: 0

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 382749 Color: 0
Size: 320234 Color: 1
Size: 297018 Color: 0

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 478549 Color: 1
Size: 265877 Color: 0
Size: 255575 Color: 0

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 425523 Color: 0
Size: 292612 Color: 0
Size: 281866 Color: 1

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 373582 Color: 1
Size: 313297 Color: 0
Size: 313122 Color: 1

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 409767 Color: 1
Size: 315339 Color: 0
Size: 274895 Color: 0

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 391091 Color: 1
Size: 341043 Color: 1
Size: 267867 Color: 0

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 377790 Color: 1
Size: 367586 Color: 0
Size: 254625 Color: 1

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 463487 Color: 0
Size: 272234 Color: 1
Size: 264280 Color: 0

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 405682 Color: 1
Size: 344037 Color: 1
Size: 250282 Color: 0

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 382823 Color: 1
Size: 324494 Color: 0
Size: 292684 Color: 0

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 481654 Color: 1
Size: 262762 Color: 0
Size: 255585 Color: 0

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 366651 Color: 0
Size: 364603 Color: 0
Size: 268747 Color: 1

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 461659 Color: 0
Size: 287047 Color: 1
Size: 251295 Color: 0

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 366471 Color: 0
Size: 326717 Color: 0
Size: 306813 Color: 1

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 435802 Color: 1
Size: 307208 Color: 0
Size: 256991 Color: 1

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 352883 Color: 0
Size: 341025 Color: 1
Size: 306093 Color: 1

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 422614 Color: 0
Size: 294485 Color: 1
Size: 282902 Color: 1

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 365758 Color: 1
Size: 354153 Color: 1
Size: 280090 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 437239 Color: 1
Size: 287457 Color: 1
Size: 275305 Color: 0

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 404182 Color: 1
Size: 328917 Color: 0
Size: 266902 Color: 1

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 460747 Color: 0
Size: 283552 Color: 0
Size: 255702 Color: 1

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 415090 Color: 0
Size: 330466 Color: 1
Size: 254445 Color: 0

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 405033 Color: 1
Size: 342707 Color: 1
Size: 252261 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 394697 Color: 0
Size: 315952 Color: 1
Size: 289352 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 492002 Color: 0
Size: 257127 Color: 1
Size: 250872 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 389049 Color: 0
Size: 350973 Color: 1
Size: 259979 Color: 0

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 382111 Color: 0
Size: 323130 Color: 0
Size: 294760 Color: 1

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 379330 Color: 0
Size: 341508 Color: 1
Size: 279163 Color: 1

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 400496 Color: 1
Size: 304866 Color: 0
Size: 294639 Color: 1

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 383569 Color: 0
Size: 356477 Color: 1
Size: 259955 Color: 0

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 383395 Color: 1
Size: 364166 Color: 0
Size: 252440 Color: 1

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 461276 Color: 1
Size: 269595 Color: 1
Size: 269130 Color: 0

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 355396 Color: 0
Size: 343905 Color: 1
Size: 300700 Color: 0

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 369225 Color: 1
Size: 344123 Color: 0
Size: 286653 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 376987 Color: 0
Size: 315768 Color: 1
Size: 307246 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 466584 Color: 1
Size: 267550 Color: 0
Size: 265867 Color: 1

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 494167 Color: 1
Size: 254069 Color: 0
Size: 251765 Color: 0

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 487019 Color: 1
Size: 262900 Color: 0
Size: 250082 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 406988 Color: 0
Size: 299188 Color: 0
Size: 293825 Color: 1

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 490500 Color: 0
Size: 255321 Color: 1
Size: 254180 Color: 1

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 435088 Color: 1
Size: 301892 Color: 1
Size: 263021 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 460383 Color: 1
Size: 280652 Color: 0
Size: 258966 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 363503 Color: 0
Size: 339753 Color: 0
Size: 296745 Color: 1

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 483209 Color: 0
Size: 259071 Color: 0
Size: 257721 Color: 1

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 416542 Color: 1
Size: 297219 Color: 0
Size: 286240 Color: 1

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 488194 Color: 1
Size: 261668 Color: 0
Size: 250139 Color: 0

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 393597 Color: 0
Size: 325601 Color: 1
Size: 280803 Color: 1

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 341795 Color: 0
Size: 340096 Color: 0
Size: 318110 Color: 1

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 418663 Color: 1
Size: 319098 Color: 1
Size: 262240 Color: 0

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 446311 Color: 1
Size: 294290 Color: 1
Size: 259400 Color: 0

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 378690 Color: 0
Size: 342871 Color: 1
Size: 278440 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 393198 Color: 1
Size: 350794 Color: 1
Size: 256009 Color: 0

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 498505 Color: 0
Size: 250844 Color: 1
Size: 250652 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 398536 Color: 1
Size: 347402 Color: 0
Size: 254063 Color: 0

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 383899 Color: 1
Size: 337680 Color: 0
Size: 278422 Color: 0

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 477938 Color: 0
Size: 269666 Color: 0
Size: 252397 Color: 1

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 453425 Color: 0
Size: 281137 Color: 0
Size: 265439 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 478076 Color: 1
Size: 267119 Color: 0
Size: 254806 Color: 1

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 404678 Color: 0
Size: 328344 Color: 0
Size: 266979 Color: 1

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 385703 Color: 0
Size: 357944 Color: 0
Size: 256354 Color: 1

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 342134 Color: 1
Size: 340610 Color: 0
Size: 317257 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 481523 Color: 0
Size: 264449 Color: 0
Size: 254029 Color: 1

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 381316 Color: 0
Size: 311612 Color: 0
Size: 307073 Color: 1

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 433925 Color: 0
Size: 294188 Color: 1
Size: 271888 Color: 0

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 397964 Color: 1
Size: 306002 Color: 1
Size: 296035 Color: 0

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 352633 Color: 1
Size: 330762 Color: 0
Size: 316606 Color: 0

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 458873 Color: 0
Size: 284028 Color: 1
Size: 257100 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 408314 Color: 0
Size: 324461 Color: 0
Size: 267226 Color: 1

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 452239 Color: 0
Size: 277409 Color: 0
Size: 270353 Color: 1

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 469849 Color: 0
Size: 265336 Color: 1
Size: 264816 Color: 0

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 474150 Color: 0
Size: 274951 Color: 1
Size: 250900 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 388082 Color: 1
Size: 306263 Color: 0
Size: 305656 Color: 1

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 458973 Color: 1
Size: 283038 Color: 0
Size: 257990 Color: 0

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 342186 Color: 1
Size: 332225 Color: 1
Size: 325590 Color: 0

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 423229 Color: 0
Size: 306456 Color: 1
Size: 270316 Color: 1

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 418712 Color: 0
Size: 299521 Color: 1
Size: 281768 Color: 0

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 393114 Color: 1
Size: 328096 Color: 0
Size: 278791 Color: 1

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 341236 Color: 0
Size: 335072 Color: 1
Size: 323693 Color: 0

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 453423 Color: 1
Size: 273401 Color: 0
Size: 273177 Color: 1

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 457024 Color: 0
Size: 273733 Color: 0
Size: 269244 Color: 1

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 360303 Color: 1
Size: 355245 Color: 0
Size: 284453 Color: 0

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 461129 Color: 0
Size: 282445 Color: 1
Size: 256427 Color: 1

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 472617 Color: 0
Size: 266414 Color: 0
Size: 260970 Color: 1

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 461463 Color: 1
Size: 272399 Color: 0
Size: 266139 Color: 0

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 363952 Color: 1
Size: 355512 Color: 1
Size: 280537 Color: 0

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 409449 Color: 1
Size: 324087 Color: 0
Size: 266465 Color: 1

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 459107 Color: 0
Size: 279070 Color: 1
Size: 261824 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 438971 Color: 0
Size: 284942 Color: 0
Size: 276088 Color: 1

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 425315 Color: 1
Size: 313505 Color: 0
Size: 261181 Color: 0

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 450080 Color: 1
Size: 292788 Color: 0
Size: 257133 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 401299 Color: 0
Size: 304414 Color: 1
Size: 294288 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 346017 Color: 0
Size: 335937 Color: 0
Size: 318047 Color: 1

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 366069 Color: 1
Size: 325937 Color: 1
Size: 307995 Color: 0

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 417599 Color: 0
Size: 303562 Color: 1
Size: 278840 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 387017 Color: 0
Size: 326373 Color: 1
Size: 286611 Color: 0

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 341054 Color: 0
Size: 334394 Color: 0
Size: 324553 Color: 1

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 349814 Color: 1
Size: 339414 Color: 0
Size: 310773 Color: 0

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 441873 Color: 1
Size: 282322 Color: 0
Size: 275806 Color: 0

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 362900 Color: 0
Size: 341495 Color: 1
Size: 295606 Color: 0

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 391549 Color: 1
Size: 341759 Color: 1
Size: 266693 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 470757 Color: 1
Size: 274557 Color: 0
Size: 254687 Color: 1

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 339584 Color: 0
Size: 337086 Color: 0
Size: 323331 Color: 1

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 451574 Color: 1
Size: 276491 Color: 1
Size: 271936 Color: 0

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 352057 Color: 1
Size: 350900 Color: 0
Size: 297044 Color: 0

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 421418 Color: 1
Size: 325952 Color: 1
Size: 252631 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 460388 Color: 1
Size: 272942 Color: 1
Size: 266671 Color: 0

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 448705 Color: 1
Size: 287137 Color: 1
Size: 264159 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 436508 Color: 0
Size: 288300 Color: 0
Size: 275193 Color: 1

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 428956 Color: 0
Size: 288642 Color: 1
Size: 282403 Color: 1

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 367533 Color: 0
Size: 318138 Color: 1
Size: 314330 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 412873 Color: 1
Size: 327145 Color: 0
Size: 259983 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 415562 Color: 0
Size: 312558 Color: 1
Size: 271881 Color: 0

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 458501 Color: 0
Size: 275991 Color: 1
Size: 265509 Color: 1

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 427967 Color: 0
Size: 286989 Color: 1
Size: 285045 Color: 0

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 453285 Color: 0
Size: 285073 Color: 0
Size: 261643 Color: 1

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 495505 Color: 1
Size: 254188 Color: 0
Size: 250308 Color: 1

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 419910 Color: 0
Size: 308994 Color: 1
Size: 271097 Color: 1

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 446313 Color: 0
Size: 283673 Color: 0
Size: 270015 Color: 1

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 348158 Color: 1
Size: 343476 Color: 0
Size: 308367 Color: 1

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 352660 Color: 0
Size: 333652 Color: 1
Size: 313689 Color: 1

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 401246 Color: 1
Size: 341755 Color: 0
Size: 257000 Color: 0

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 362883 Color: 0
Size: 326373 Color: 0
Size: 310745 Color: 1

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 411994 Color: 1
Size: 324036 Color: 1
Size: 263971 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 434045 Color: 1
Size: 288135 Color: 0
Size: 277821 Color: 1

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 356862 Color: 0
Size: 326487 Color: 1
Size: 316652 Color: 0

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 418609 Color: 0
Size: 318956 Color: 1
Size: 262436 Color: 0

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 346778 Color: 0
Size: 331758 Color: 1
Size: 321465 Color: 1

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 360514 Color: 1
Size: 355969 Color: 1
Size: 283518 Color: 0

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 363344 Color: 0
Size: 324504 Color: 0
Size: 312153 Color: 1

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 415910 Color: 0
Size: 312848 Color: 0
Size: 271243 Color: 1

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 360502 Color: 1
Size: 360225 Color: 1
Size: 279274 Color: 0

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 370208 Color: 1
Size: 334592 Color: 0
Size: 295201 Color: 0

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 366889 Color: 0
Size: 349161 Color: 1
Size: 283951 Color: 0

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 350569 Color: 0
Size: 345884 Color: 1
Size: 303548 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 353244 Color: 0
Size: 342398 Color: 1
Size: 304359 Color: 0

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 360371 Color: 1
Size: 351407 Color: 0
Size: 288223 Color: 1

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 385079 Color: 1
Size: 319427 Color: 0
Size: 295495 Color: 1

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 348610 Color: 1
Size: 342603 Color: 1
Size: 308788 Color: 0

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 376316 Color: 0
Size: 349484 Color: 0
Size: 274201 Color: 1

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 392395 Color: 0
Size: 339393 Color: 0
Size: 268213 Color: 1

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 400398 Color: 1
Size: 346588 Color: 0
Size: 253015 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 355137 Color: 0
Size: 336264 Color: 1
Size: 308600 Color: 1

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 354158 Color: 0
Size: 335056 Color: 1
Size: 310787 Color: 1

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 352809 Color: 1
Size: 344664 Color: 0
Size: 302528 Color: 1

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 351694 Color: 1
Size: 345778 Color: 1
Size: 302529 Color: 0

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 366711 Color: 1
Size: 354794 Color: 0
Size: 278496 Color: 0

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 355134 Color: 0
Size: 326390 Color: 1
Size: 318477 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 437284 Color: 1
Size: 306271 Color: 0
Size: 256446 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 372329 Color: 0
Size: 333056 Color: 1
Size: 294616 Color: 0

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 351398 Color: 0
Size: 328877 Color: 0
Size: 319726 Color: 1

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 346372 Color: 0
Size: 334536 Color: 1
Size: 319093 Color: 1

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 368422 Color: 0
Size: 353397 Color: 1
Size: 278182 Color: 1

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 352776 Color: 0
Size: 331877 Color: 1
Size: 315348 Color: 1

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 348074 Color: 0
Size: 346518 Color: 1
Size: 305409 Color: 0

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 354076 Color: 0
Size: 349395 Color: 0
Size: 296530 Color: 1

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 404698 Color: 0
Size: 330025 Color: 0
Size: 265278 Color: 1

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 483421 Color: 0
Size: 258788 Color: 1
Size: 257792 Color: 1

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 343063 Color: 0
Size: 329504 Color: 0
Size: 327434 Color: 1

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 371207 Color: 1
Size: 327663 Color: 0
Size: 301131 Color: 1

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 367768 Color: 0
Size: 364210 Color: 1
Size: 268023 Color: 1

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 408066 Color: 1
Size: 300809 Color: 1
Size: 291126 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 475911 Color: 1
Size: 274073 Color: 0
Size: 250017 Color: 1

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 440647 Color: 1
Size: 308801 Color: 0
Size: 250553 Color: 1

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 448674 Color: 0
Size: 280169 Color: 0
Size: 271158 Color: 1

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 443565 Color: 1
Size: 287125 Color: 0
Size: 269311 Color: 0

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 425314 Color: 0
Size: 289987 Color: 0
Size: 284700 Color: 1

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 355096 Color: 0
Size: 354770 Color: 1
Size: 290135 Color: 0

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 355769 Color: 0
Size: 355480 Color: 1
Size: 288752 Color: 0

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 465205 Color: 0
Size: 277835 Color: 1
Size: 256961 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 350566 Color: 0
Size: 350461 Color: 1
Size: 298974 Color: 1

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 379014 Color: 0
Size: 344134 Color: 1
Size: 276853 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 351494 Color: 0
Size: 340337 Color: 0
Size: 308170 Color: 1

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 358463 Color: 1
Size: 346924 Color: 1
Size: 294614 Color: 0

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 413721 Color: 1
Size: 308472 Color: 1
Size: 277808 Color: 0

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 385024 Color: 1
Size: 346630 Color: 0
Size: 268347 Color: 1

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 346452 Color: 1
Size: 340946 Color: 1
Size: 312603 Color: 0

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 367038 Color: 0
Size: 350970 Color: 1
Size: 281993 Color: 1

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 344771 Color: 1
Size: 344759 Color: 0
Size: 310471 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 347426 Color: 1
Size: 337848 Color: 0
Size: 314727 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 344815 Color: 1
Size: 344387 Color: 0
Size: 310799 Color: 0

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 364061 Color: 1
Size: 328354 Color: 0
Size: 307586 Color: 1

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 378955 Color: 1
Size: 351889 Color: 1
Size: 269157 Color: 0

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 352118 Color: 1
Size: 349544 Color: 0
Size: 298339 Color: 1

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 355227 Color: 0
Size: 349682 Color: 0
Size: 295092 Color: 1

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 346569 Color: 0
Size: 342964 Color: 1
Size: 310468 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 369943 Color: 0
Size: 350286 Color: 0
Size: 279772 Color: 1

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 395969 Color: 0
Size: 353543 Color: 0
Size: 250489 Color: 1

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 422704 Color: 0
Size: 307919 Color: 0
Size: 269378 Color: 1

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 394001 Color: 0
Size: 336889 Color: 1
Size: 269111 Color: 1

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 423947 Color: 0
Size: 321931 Color: 0
Size: 254123 Color: 1

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 375144 Color: 1
Size: 343101 Color: 0
Size: 281756 Color: 1

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 427998 Color: 1
Size: 305425 Color: 1
Size: 266578 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 336877 Color: 1
Size: 336686 Color: 1
Size: 326438 Color: 0

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 347834 Color: 1
Size: 330224 Color: 0
Size: 321943 Color: 1

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 443539 Color: 1
Size: 301116 Color: 1
Size: 255346 Color: 0

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 496090 Color: 0
Size: 252036 Color: 1
Size: 251875 Color: 1

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 342686 Color: 0
Size: 329509 Color: 0
Size: 327806 Color: 1

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 355296 Color: 0
Size: 340173 Color: 0
Size: 304532 Color: 1

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 355133 Color: 1
Size: 324581 Color: 1
Size: 320287 Color: 0

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 355197 Color: 1
Size: 328755 Color: 1
Size: 316049 Color: 0

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 355519 Color: 0
Size: 330427 Color: 0
Size: 314055 Color: 1

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 356275 Color: 0
Size: 338138 Color: 0
Size: 305588 Color: 1

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 356419 Color: 0
Size: 345937 Color: 1
Size: 297645 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 356260 Color: 1
Size: 321902 Color: 1
Size: 321839 Color: 0

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 356544 Color: 1
Size: 350764 Color: 0
Size: 292693 Color: 1

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 356983 Color: 0
Size: 334651 Color: 0
Size: 308367 Color: 1

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 357002 Color: 0
Size: 345034 Color: 0
Size: 297965 Color: 1

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 357459 Color: 1
Size: 325273 Color: 0
Size: 317269 Color: 1

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 357618 Color: 0
Size: 353362 Color: 0
Size: 289021 Color: 1

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 357717 Color: 1
Size: 348863 Color: 1
Size: 293421 Color: 0

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 357631 Color: 0
Size: 331636 Color: 0
Size: 310734 Color: 1

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 357667 Color: 0
Size: 341861 Color: 0
Size: 300473 Color: 1

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 357864 Color: 1
Size: 323324 Color: 1
Size: 318813 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 357918 Color: 1
Size: 321458 Color: 1
Size: 320625 Color: 0

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 357894 Color: 0
Size: 342978 Color: 1
Size: 299129 Color: 0

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 358305 Color: 1
Size: 352354 Color: 0
Size: 289342 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 358062 Color: 0
Size: 345305 Color: 1
Size: 296634 Color: 0

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 358563 Color: 1
Size: 344899 Color: 1
Size: 296539 Color: 0

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 358259 Color: 0
Size: 348265 Color: 0
Size: 293477 Color: 1

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 358387 Color: 0
Size: 324910 Color: 0
Size: 316704 Color: 1

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 358433 Color: 0
Size: 352955 Color: 1
Size: 288613 Color: 0

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 358981 Color: 1
Size: 339210 Color: 0
Size: 301810 Color: 1

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 359004 Color: 1
Size: 337870 Color: 0
Size: 303127 Color: 1

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 359062 Color: 1
Size: 338604 Color: 1
Size: 302335 Color: 0

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 359126 Color: 0
Size: 341878 Color: 0
Size: 298997 Color: 1

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 359505 Color: 1
Size: 339008 Color: 0
Size: 301488 Color: 1

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 359512 Color: 1
Size: 350956 Color: 0
Size: 289533 Color: 1

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 359314 Color: 0
Size: 324876 Color: 1
Size: 315811 Color: 0

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 359578 Color: 1
Size: 333966 Color: 0
Size: 306457 Color: 1

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 359360 Color: 0
Size: 322742 Color: 0
Size: 317899 Color: 1

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 359935 Color: 1
Size: 331870 Color: 1
Size: 308196 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 360037 Color: 0
Size: 337684 Color: 1
Size: 302280 Color: 0

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 360526 Color: 1
Size: 333578 Color: 1
Size: 305897 Color: 0

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 360542 Color: 0
Size: 332898 Color: 1
Size: 306561 Color: 0

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 360855 Color: 0
Size: 323404 Color: 1
Size: 315742 Color: 0

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 360913 Color: 0
Size: 329361 Color: 1
Size: 309727 Color: 1

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 361240 Color: 1
Size: 345833 Color: 1
Size: 292928 Color: 0

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 361352 Color: 1
Size: 349330 Color: 0
Size: 289319 Color: 1

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 361649 Color: 1
Size: 360493 Color: 0
Size: 277859 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 361722 Color: 1
Size: 333428 Color: 0
Size: 304851 Color: 1

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 361717 Color: 0
Size: 324362 Color: 0
Size: 313922 Color: 1

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 361742 Color: 1
Size: 331410 Color: 1
Size: 306849 Color: 0

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 361782 Color: 0
Size: 330132 Color: 1
Size: 308087 Color: 0

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 361942 Color: 1
Size: 322745 Color: 0
Size: 315314 Color: 1

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 362123 Color: 0
Size: 330785 Color: 0
Size: 307093 Color: 1

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 362259 Color: 1
Size: 353747 Color: 0
Size: 283995 Color: 1

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 362245 Color: 0
Size: 355660 Color: 1
Size: 282096 Color: 0

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 362297 Color: 1
Size: 359435 Color: 0
Size: 278269 Color: 1

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 362291 Color: 0
Size: 322656 Color: 1
Size: 315054 Color: 0

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 362444 Color: 1
Size: 331020 Color: 1
Size: 306537 Color: 0

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 362627 Color: 1
Size: 351407 Color: 0
Size: 285967 Color: 0

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 362698 Color: 1
Size: 359920 Color: 0
Size: 277383 Color: 1

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 362471 Color: 0
Size: 319596 Color: 1
Size: 317934 Color: 0

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 362547 Color: 0
Size: 338111 Color: 0
Size: 299343 Color: 1

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 362836 Color: 1
Size: 344510 Color: 0
Size: 292655 Color: 1

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 363119 Color: 1
Size: 354767 Color: 0
Size: 282115 Color: 1

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 363129 Color: 1
Size: 329730 Color: 0
Size: 307142 Color: 1

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 363286 Color: 0
Size: 353831 Color: 0
Size: 282884 Color: 1

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 363208 Color: 1
Size: 328209 Color: 1
Size: 308584 Color: 0

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 363276 Color: 1
Size: 319630 Color: 1
Size: 317095 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 363388 Color: 0
Size: 338421 Color: 1
Size: 298192 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 363450 Color: 1
Size: 341303 Color: 0
Size: 295248 Color: 1

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 363483 Color: 0
Size: 320944 Color: 1
Size: 315574 Color: 0

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 363483 Color: 1
Size: 321534 Color: 0
Size: 314984 Color: 1

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 363566 Color: 0
Size: 360572 Color: 1
Size: 275863 Color: 0

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 363637 Color: 1
Size: 352116 Color: 0
Size: 284248 Color: 1

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 363744 Color: 0
Size: 326889 Color: 0
Size: 309368 Color: 1

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 363723 Color: 1
Size: 331554 Color: 1
Size: 304724 Color: 0

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 363930 Color: 0
Size: 318859 Color: 0
Size: 317212 Color: 1

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 363853 Color: 1
Size: 344601 Color: 1
Size: 291547 Color: 0

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 363933 Color: 0
Size: 351939 Color: 1
Size: 284129 Color: 0

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 363874 Color: 1
Size: 321134 Color: 1
Size: 314993 Color: 0

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 363942 Color: 0
Size: 327476 Color: 0
Size: 308583 Color: 1

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 363936 Color: 1
Size: 362408 Color: 1
Size: 273657 Color: 0

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 363947 Color: 0
Size: 336654 Color: 1
Size: 299400 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 364062 Color: 1
Size: 363687 Color: 0
Size: 272252 Color: 1

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 364104 Color: 1
Size: 341174 Color: 1
Size: 294723 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 364155 Color: 0
Size: 344743 Color: 0
Size: 291103 Color: 1

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 364286 Color: 0
Size: 322574 Color: 1
Size: 313141 Color: 0

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 364285 Color: 1
Size: 339182 Color: 1
Size: 296534 Color: 0

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 364337 Color: 1
Size: 319423 Color: 0
Size: 316241 Color: 0

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 364380 Color: 1
Size: 355127 Color: 1
Size: 280494 Color: 0

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 364408 Color: 1
Size: 360002 Color: 0
Size: 275591 Color: 0

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 364490 Color: 1
Size: 344801 Color: 0
Size: 290710 Color: 1

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 364474 Color: 0
Size: 320387 Color: 1
Size: 315140 Color: 0

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 364513 Color: 1
Size: 337686 Color: 1
Size: 297802 Color: 0

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 364528 Color: 1
Size: 351817 Color: 1
Size: 283656 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 364690 Color: 1
Size: 363015 Color: 1
Size: 272296 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 364699 Color: 1
Size: 351384 Color: 0
Size: 283918 Color: 1

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 364895 Color: 1
Size: 364320 Color: 0
Size: 270786 Color: 0

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 364921 Color: 1
Size: 334977 Color: 1
Size: 300103 Color: 0

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 364938 Color: 1
Size: 329056 Color: 1
Size: 306007 Color: 0

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 364994 Color: 1
Size: 360363 Color: 0
Size: 274644 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 365115 Color: 1
Size: 345944 Color: 0
Size: 288942 Color: 1

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 365448 Color: 0
Size: 319760 Color: 0
Size: 314793 Color: 1

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 365472 Color: 1
Size: 336768 Color: 0
Size: 297761 Color: 1

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 365724 Color: 0
Size: 357155 Color: 1
Size: 277122 Color: 0

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 365600 Color: 1
Size: 347254 Color: 1
Size: 287147 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 365801 Color: 0
Size: 363272 Color: 0
Size: 270928 Color: 1

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 365822 Color: 0
Size: 343316 Color: 0
Size: 290863 Color: 1

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 365846 Color: 0
Size: 329359 Color: 1
Size: 304796 Color: 0

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 365871 Color: 1
Size: 346743 Color: 1
Size: 287387 Color: 0

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 366032 Color: 0
Size: 354792 Color: 0
Size: 279177 Color: 1

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 366191 Color: 1
Size: 349678 Color: 1
Size: 284132 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 366279 Color: 0
Size: 350834 Color: 1
Size: 282888 Color: 0

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 366359 Color: 1
Size: 339630 Color: 0
Size: 294012 Color: 1

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 366381 Color: 1
Size: 346317 Color: 1
Size: 287303 Color: 0

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 366544 Color: 0
Size: 363032 Color: 1
Size: 270425 Color: 0

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 366411 Color: 1
Size: 334794 Color: 1
Size: 298796 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 366687 Color: 0
Size: 322783 Color: 1
Size: 310531 Color: 0

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 366654 Color: 1
Size: 360246 Color: 0
Size: 273101 Color: 1

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 366765 Color: 0
Size: 358575 Color: 1
Size: 274661 Color: 0

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 366666 Color: 1
Size: 359839 Color: 0
Size: 273496 Color: 1

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 366788 Color: 0
Size: 331596 Color: 0
Size: 301617 Color: 1

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 366710 Color: 1
Size: 322799 Color: 1
Size: 310492 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 366802 Color: 0
Size: 359573 Color: 1
Size: 273626 Color: 0

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 366818 Color: 1
Size: 341883 Color: 1
Size: 291300 Color: 0

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 366992 Color: 1
Size: 366770 Color: 0
Size: 266239 Color: 1

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 367219 Color: 1
Size: 324585 Color: 0
Size: 308197 Color: 1

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 367237 Color: 1
Size: 322144 Color: 1
Size: 310620 Color: 0

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 367263 Color: 1
Size: 352787 Color: 0
Size: 279951 Color: 1

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 366999 Color: 0
Size: 350618 Color: 0
Size: 282384 Color: 1

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 367302 Color: 1
Size: 361911 Color: 1
Size: 270788 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 367327 Color: 1
Size: 324895 Color: 1
Size: 307779 Color: 0

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 367400 Color: 0
Size: 334665 Color: 0
Size: 297936 Color: 1

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 367393 Color: 1
Size: 323197 Color: 0
Size: 309411 Color: 1

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 367395 Color: 1
Size: 355080 Color: 1
Size: 277526 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 367466 Color: 0
Size: 362230 Color: 0
Size: 270305 Color: 1

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 367522 Color: 0
Size: 347972 Color: 0
Size: 284507 Color: 1

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 367506 Color: 1
Size: 330142 Color: 1
Size: 302353 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 367584 Color: 1
Size: 319681 Color: 0
Size: 312736 Color: 1

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 367732 Color: 1
Size: 319491 Color: 0
Size: 312778 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 367643 Color: 0
Size: 320580 Color: 1
Size: 311778 Color: 0

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 367795 Color: 1
Size: 362360 Color: 0
Size: 269846 Color: 1

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 367861 Color: 0
Size: 340483 Color: 1
Size: 291657 Color: 0

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 368025 Color: 1
Size: 353411 Color: 0
Size: 278565 Color: 0

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 368239 Color: 1
Size: 329495 Color: 0
Size: 302267 Color: 1

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 368222 Color: 0
Size: 334652 Color: 0
Size: 297127 Color: 1

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 368255 Color: 1
Size: 366035 Color: 1
Size: 265711 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 368327 Color: 0
Size: 334175 Color: 1
Size: 297499 Color: 0

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 368299 Color: 1
Size: 331365 Color: 0
Size: 300337 Color: 1

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 368376 Color: 0
Size: 323282 Color: 0
Size: 308343 Color: 1

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 368448 Color: 1
Size: 335942 Color: 1
Size: 295611 Color: 0

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 368586 Color: 1
Size: 338059 Color: 1
Size: 293356 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 368698 Color: 1
Size: 324792 Color: 1
Size: 306511 Color: 0

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 368545 Color: 0
Size: 358634 Color: 0
Size: 272822 Color: 1

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 368749 Color: 1
Size: 341630 Color: 1
Size: 289622 Color: 0

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 368571 Color: 0
Size: 359828 Color: 1
Size: 271602 Color: 0

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 368779 Color: 1
Size: 329318 Color: 0
Size: 301904 Color: 1

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 368858 Color: 1
Size: 362072 Color: 0
Size: 269071 Color: 0

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 368680 Color: 0
Size: 333515 Color: 0
Size: 297806 Color: 1

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 368763 Color: 0
Size: 315746 Color: 0
Size: 315492 Color: 1

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 369106 Color: 1
Size: 365976 Color: 0
Size: 264919 Color: 1

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 369120 Color: 1
Size: 330524 Color: 0
Size: 300357 Color: 1

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 368824 Color: 0
Size: 338789 Color: 0
Size: 292388 Color: 1

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 369165 Color: 1
Size: 350173 Color: 1
Size: 280663 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 369227 Color: 1
Size: 321762 Color: 0
Size: 309012 Color: 1

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 369355 Color: 1
Size: 323237 Color: 0
Size: 307409 Color: 1

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 369272 Color: 0
Size: 361093 Color: 1
Size: 269636 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 369529 Color: 1
Size: 368335 Color: 1
Size: 262137 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 369441 Color: 0
Size: 352337 Color: 0
Size: 278223 Color: 1

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 369557 Color: 1
Size: 319335 Color: 1
Size: 311109 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 369455 Color: 0
Size: 364382 Color: 0
Size: 266164 Color: 1

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 369569 Color: 1
Size: 343127 Color: 0
Size: 287305 Color: 1

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 369456 Color: 0
Size: 315660 Color: 0
Size: 314885 Color: 1

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 369621 Color: 1
Size: 339219 Color: 0
Size: 291161 Color: 1

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 369498 Color: 0
Size: 349269 Color: 1
Size: 281234 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 369648 Color: 1
Size: 331931 Color: 1
Size: 298422 Color: 0

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 369734 Color: 0
Size: 329034 Color: 1
Size: 301233 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 369688 Color: 1
Size: 364496 Color: 0
Size: 265817 Color: 1

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 369816 Color: 1
Size: 322224 Color: 0
Size: 307961 Color: 1

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 369859 Color: 0
Size: 351065 Color: 1
Size: 279077 Color: 0

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 369849 Color: 1
Size: 342580 Color: 0
Size: 287572 Color: 1

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 370036 Color: 0
Size: 319143 Color: 1
Size: 310822 Color: 0

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 370210 Color: 0
Size: 355066 Color: 1
Size: 274725 Color: 1

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 370326 Color: 0
Size: 352502 Color: 1
Size: 277173 Color: 0

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 369939 Color: 1
Size: 348513 Color: 1
Size: 281549 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 370356 Color: 0
Size: 330106 Color: 0
Size: 299539 Color: 1

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 369940 Color: 1
Size: 353238 Color: 1
Size: 276823 Color: 0

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 370134 Color: 1
Size: 341023 Color: 1
Size: 288844 Color: 0

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 370419 Color: 0
Size: 342423 Color: 1
Size: 287159 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 370504 Color: 0
Size: 326801 Color: 1
Size: 302696 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 370249 Color: 1
Size: 359070 Color: 1
Size: 270682 Color: 0

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 370336 Color: 1
Size: 354006 Color: 1
Size: 275659 Color: 0

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 370548 Color: 0
Size: 338433 Color: 1
Size: 291020 Color: 0

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 370412 Color: 1
Size: 317713 Color: 0
Size: 311876 Color: 1

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 370707 Color: 0
Size: 364388 Color: 0
Size: 264906 Color: 1

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 370736 Color: 0
Size: 361295 Color: 0
Size: 267970 Color: 1

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 370774 Color: 0
Size: 366599 Color: 1
Size: 262628 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 370851 Color: 1
Size: 327045 Color: 0
Size: 302105 Color: 1

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 370977 Color: 1
Size: 320610 Color: 1
Size: 308414 Color: 0

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 370902 Color: 0
Size: 353326 Color: 1
Size: 275773 Color: 0

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 371014 Color: 1
Size: 364934 Color: 0
Size: 264053 Color: 1

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 370912 Color: 0
Size: 344694 Color: 1
Size: 284395 Color: 0

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 371038 Color: 1
Size: 328170 Color: 0
Size: 300793 Color: 1

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 370929 Color: 0
Size: 339143 Color: 1
Size: 289929 Color: 0

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 371077 Color: 1
Size: 324553 Color: 0
Size: 304371 Color: 1

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 371134 Color: 1
Size: 326717 Color: 0
Size: 302150 Color: 0

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 371138 Color: 1
Size: 342105 Color: 1
Size: 286758 Color: 0

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 371158 Color: 1
Size: 345057 Color: 1
Size: 283786 Color: 0

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 371070 Color: 0
Size: 343204 Color: 0
Size: 285727 Color: 1

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 371254 Color: 1
Size: 330145 Color: 1
Size: 298602 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 371075 Color: 0
Size: 362741 Color: 0
Size: 266185 Color: 1

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 371106 Color: 0
Size: 334444 Color: 0
Size: 294451 Color: 1

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 371348 Color: 1
Size: 317155 Color: 1
Size: 311498 Color: 0

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 371163 Color: 0
Size: 325834 Color: 1
Size: 303004 Color: 0

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 371546 Color: 1
Size: 330436 Color: 0
Size: 298019 Color: 0

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 371878 Color: 1
Size: 314149 Color: 1
Size: 313974 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 371909 Color: 1
Size: 355657 Color: 1
Size: 272435 Color: 0

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 371739 Color: 0
Size: 359372 Color: 1
Size: 268890 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 372188 Color: 1
Size: 344760 Color: 0
Size: 283053 Color: 0

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 371940 Color: 0
Size: 370942 Color: 0
Size: 257119 Color: 1

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 372462 Color: 1
Size: 357972 Color: 1
Size: 269567 Color: 0

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 372319 Color: 0
Size: 330720 Color: 0
Size: 296962 Color: 1

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 372681 Color: 1
Size: 349778 Color: 1
Size: 277542 Color: 0

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 372379 Color: 0
Size: 319648 Color: 1
Size: 307974 Color: 0

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 372720 Color: 1
Size: 345717 Color: 1
Size: 281564 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 372842 Color: 1
Size: 366688 Color: 1
Size: 260471 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 372658 Color: 0
Size: 329229 Color: 0
Size: 298114 Color: 1

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 372861 Color: 1
Size: 365664 Color: 0
Size: 261476 Color: 1

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 372763 Color: 0
Size: 340410 Color: 1
Size: 286828 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 372942 Color: 0
Size: 353837 Color: 1
Size: 273222 Color: 0

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 372943 Color: 1
Size: 344781 Color: 0
Size: 282277 Color: 1

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 372973 Color: 0
Size: 329296 Color: 0
Size: 297732 Color: 1

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 372955 Color: 1
Size: 318536 Color: 1
Size: 308510 Color: 0

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 373039 Color: 1
Size: 346426 Color: 0
Size: 280536 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 373073 Color: 1
Size: 349240 Color: 1
Size: 277688 Color: 0

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 373084 Color: 0
Size: 366679 Color: 1
Size: 260238 Color: 0

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 373087 Color: 0
Size: 341392 Color: 1
Size: 285522 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 373156 Color: 1
Size: 341683 Color: 1
Size: 285162 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 373209 Color: 1
Size: 351327 Color: 0
Size: 275465 Color: 1

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 373264 Color: 1
Size: 340402 Color: 1
Size: 286335 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 373280 Color: 1
Size: 344226 Color: 1
Size: 282495 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 373330 Color: 0
Size: 314447 Color: 1
Size: 312224 Color: 0

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 373284 Color: 1
Size: 347222 Color: 0
Size: 279495 Color: 1

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 373346 Color: 0
Size: 328603 Color: 0
Size: 298052 Color: 1

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 373377 Color: 0
Size: 358219 Color: 1
Size: 268405 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 373391 Color: 0
Size: 316105 Color: 1
Size: 310505 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 373539 Color: 1
Size: 335638 Color: 1
Size: 290824 Color: 0

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 373514 Color: 0
Size: 349471 Color: 0
Size: 277016 Color: 1

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 373625 Color: 0
Size: 329128 Color: 0
Size: 297248 Color: 1

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 373640 Color: 1
Size: 360510 Color: 0
Size: 265851 Color: 1

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 373955 Color: 0
Size: 328034 Color: 0
Size: 298012 Color: 1

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 373972 Color: 0
Size: 336284 Color: 1
Size: 289745 Color: 1

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 374023 Color: 0
Size: 368752 Color: 1
Size: 257226 Color: 0

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 374063 Color: 0
Size: 367045 Color: 1
Size: 258893 Color: 1

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 374075 Color: 0
Size: 330896 Color: 1
Size: 295030 Color: 0

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 374090 Color: 0
Size: 323819 Color: 1
Size: 302092 Color: 1

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 374096 Color: 0
Size: 317948 Color: 0
Size: 307957 Color: 1

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 374083 Color: 1
Size: 327353 Color: 1
Size: 298565 Color: 0

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 374122 Color: 0
Size: 325757 Color: 1
Size: 300122 Color: 0

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 374089 Color: 1
Size: 371304 Color: 0
Size: 254608 Color: 1

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 374155 Color: 0
Size: 373431 Color: 0
Size: 252415 Color: 1

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 374179 Color: 0
Size: 322074 Color: 0
Size: 303748 Color: 1

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 374371 Color: 0
Size: 314300 Color: 1
Size: 311330 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 374290 Color: 1
Size: 333869 Color: 0
Size: 291842 Color: 1

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 374435 Color: 0
Size: 364403 Color: 0
Size: 261163 Color: 1

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 374407 Color: 1
Size: 325861 Color: 1
Size: 299733 Color: 0

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 374467 Color: 0
Size: 317031 Color: 1
Size: 308503 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 374663 Color: 0
Size: 368148 Color: 0
Size: 257190 Color: 1

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 374773 Color: 0
Size: 321501 Color: 1
Size: 303727 Color: 0

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 374933 Color: 1
Size: 336438 Color: 0
Size: 288630 Color: 1

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 374977 Color: 0
Size: 371930 Color: 1
Size: 253094 Color: 0

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 374993 Color: 0
Size: 373143 Color: 0
Size: 251865 Color: 1

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 374961 Color: 1
Size: 355893 Color: 1
Size: 269147 Color: 0

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 375038 Color: 0
Size: 343677 Color: 0
Size: 281286 Color: 1

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 375135 Color: 1
Size: 362092 Color: 0
Size: 262774 Color: 1

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 375052 Color: 0
Size: 314969 Color: 1
Size: 309980 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 375208 Color: 0
Size: 338006 Color: 1
Size: 286787 Color: 1

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 375261 Color: 0
Size: 336396 Color: 0
Size: 288344 Color: 1

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 375295 Color: 0
Size: 350018 Color: 1
Size: 274688 Color: 1

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 375321 Color: 0
Size: 320424 Color: 1
Size: 304256 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 375333 Color: 0
Size: 365132 Color: 1
Size: 259536 Color: 1

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 375366 Color: 0
Size: 349154 Color: 1
Size: 275481 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 375379 Color: 0
Size: 336501 Color: 1
Size: 288121 Color: 1

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 375407 Color: 0
Size: 337390 Color: 1
Size: 287204 Color: 0

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 375442 Color: 0
Size: 319922 Color: 0
Size: 304637 Color: 1

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 375456 Color: 0
Size: 357953 Color: 1
Size: 266592 Color: 1

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 375202 Color: 1
Size: 342603 Color: 0
Size: 282196 Color: 0

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 375542 Color: 0
Size: 313457 Color: 1
Size: 311002 Color: 0

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 375343 Color: 1
Size: 363830 Color: 1
Size: 260828 Color: 0

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 375563 Color: 0
Size: 366005 Color: 1
Size: 258433 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 375399 Color: 1
Size: 334165 Color: 0
Size: 290437 Color: 1

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 375421 Color: 1
Size: 349664 Color: 1
Size: 274916 Color: 0

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 375726 Color: 0
Size: 348829 Color: 0
Size: 275446 Color: 1

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 375728 Color: 0
Size: 366467 Color: 0
Size: 257806 Color: 1

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 375650 Color: 1
Size: 358427 Color: 1
Size: 265924 Color: 0

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 375697 Color: 1
Size: 324537 Color: 0
Size: 299767 Color: 1

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 375869 Color: 0
Size: 363075 Color: 0
Size: 261057 Color: 1

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 375898 Color: 0
Size: 356244 Color: 0
Size: 267859 Color: 1

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 375900 Color: 0
Size: 370751 Color: 1
Size: 253350 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 375866 Color: 1
Size: 339767 Color: 0
Size: 284368 Color: 1

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 375937 Color: 0
Size: 344616 Color: 1
Size: 279448 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 375903 Color: 1
Size: 312688 Color: 0
Size: 311410 Color: 1

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 375983 Color: 0
Size: 370564 Color: 0
Size: 253454 Color: 1

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 376016 Color: 1
Size: 341798 Color: 0
Size: 282187 Color: 0

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 376039 Color: 0
Size: 365708 Color: 1
Size: 258254 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 376036 Color: 1
Size: 367204 Color: 0
Size: 256761 Color: 1

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 376142 Color: 0
Size: 317341 Color: 1
Size: 306518 Color: 0

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 376217 Color: 1
Size: 338054 Color: 0
Size: 285730 Color: 1

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 376153 Color: 0
Size: 312072 Color: 0
Size: 311776 Color: 1

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 376259 Color: 0
Size: 341154 Color: 1
Size: 282588 Color: 0

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 376224 Color: 1
Size: 365486 Color: 1
Size: 258291 Color: 0

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 376280 Color: 1
Size: 373357 Color: 1
Size: 250364 Color: 0

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 376330 Color: 0
Size: 359348 Color: 1
Size: 264323 Color: 0

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 376306 Color: 1
Size: 364891 Color: 0
Size: 258804 Color: 1

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 376365 Color: 0
Size: 339074 Color: 1
Size: 284562 Color: 1

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 376445 Color: 0
Size: 357810 Color: 0
Size: 265746 Color: 1

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 376554 Color: 0
Size: 348839 Color: 1
Size: 274608 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 376455 Color: 1
Size: 344238 Color: 0
Size: 279308 Color: 1

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 376466 Color: 1
Size: 332822 Color: 0
Size: 290713 Color: 1

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 376516 Color: 1
Size: 351160 Color: 0
Size: 272325 Color: 1

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 376621 Color: 0
Size: 343509 Color: 0
Size: 279871 Color: 1

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 376718 Color: 1
Size: 371833 Color: 0
Size: 251450 Color: 1

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 376716 Color: 0
Size: 318790 Color: 1
Size: 304495 Color: 0

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 376749 Color: 1
Size: 330968 Color: 1
Size: 292284 Color: 0

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 376718 Color: 0
Size: 311944 Color: 1
Size: 311339 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 376765 Color: 1
Size: 314584 Color: 1
Size: 308652 Color: 0

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 376855 Color: 1
Size: 334066 Color: 0
Size: 289080 Color: 1

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 376770 Color: 0
Size: 332853 Color: 1
Size: 290378 Color: 0

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 376893 Color: 1
Size: 331853 Color: 0
Size: 291255 Color: 1

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 376922 Color: 0
Size: 322461 Color: 1
Size: 300618 Color: 0

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 377110 Color: 1
Size: 335004 Color: 0
Size: 287887 Color: 1

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 377138 Color: 1
Size: 319786 Color: 0
Size: 303077 Color: 1

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 377092 Color: 0
Size: 343435 Color: 0
Size: 279474 Color: 1

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 377146 Color: 1
Size: 329058 Color: 1
Size: 293797 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 377117 Color: 0
Size: 326359 Color: 1
Size: 296525 Color: 0

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 377243 Color: 1
Size: 371343 Color: 0
Size: 251415 Color: 1

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 377155 Color: 0
Size: 334445 Color: 1
Size: 288401 Color: 0

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 377248 Color: 1
Size: 348938 Color: 1
Size: 273815 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 377309 Color: 1
Size: 327002 Color: 0
Size: 295690 Color: 1

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 377325 Color: 1
Size: 367656 Color: 0
Size: 255020 Color: 0

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 377450 Color: 1
Size: 369678 Color: 1
Size: 252873 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 377408 Color: 1
Size: 321246 Color: 1
Size: 301347 Color: 0

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 377503 Color: 0
Size: 330356 Color: 1
Size: 292142 Color: 0

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 377450 Color: 1
Size: 365341 Color: 0
Size: 257210 Color: 1

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 377641 Color: 0
Size: 314183 Color: 1
Size: 308177 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 377592 Color: 1
Size: 325127 Color: 0
Size: 297282 Color: 1

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 377754 Color: 1
Size: 343039 Color: 0
Size: 279208 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 377806 Color: 1
Size: 324125 Color: 0
Size: 298070 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 377815 Color: 1
Size: 348563 Color: 1
Size: 273623 Color: 0

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 377851 Color: 0
Size: 316094 Color: 0
Size: 306056 Color: 1

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 377817 Color: 1
Size: 321401 Color: 0
Size: 300783 Color: 1

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 378058 Color: 0
Size: 348583 Color: 1
Size: 273360 Color: 0

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 377856 Color: 1
Size: 364190 Color: 0
Size: 257955 Color: 1

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 378059 Color: 0
Size: 335622 Color: 0
Size: 286320 Color: 1

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 378043 Color: 1
Size: 356845 Color: 0
Size: 265113 Color: 1

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 378079 Color: 0
Size: 326867 Color: 1
Size: 295055 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 378123 Color: 0
Size: 332473 Color: 0
Size: 289405 Color: 1

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 378266 Color: 1
Size: 346071 Color: 0
Size: 275664 Color: 1

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 378291 Color: 1
Size: 330941 Color: 0
Size: 290769 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 378298 Color: 0
Size: 324501 Color: 1
Size: 297202 Color: 0

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 378430 Color: 1
Size: 336324 Color: 0
Size: 285247 Color: 1

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 378481 Color: 0
Size: 330905 Color: 1
Size: 290615 Color: 0

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 378443 Color: 1
Size: 357643 Color: 0
Size: 263915 Color: 1

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 378575 Color: 0
Size: 353042 Color: 0
Size: 268384 Color: 1

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 378645 Color: 0
Size: 330560 Color: 1
Size: 290796 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 378620 Color: 1
Size: 367159 Color: 1
Size: 254222 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 378679 Color: 0
Size: 347135 Color: 0
Size: 274187 Color: 1

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 378656 Color: 1
Size: 336456 Color: 1
Size: 284889 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 378698 Color: 1
Size: 329958 Color: 1
Size: 291345 Color: 0

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 378734 Color: 0
Size: 320493 Color: 1
Size: 300774 Color: 0

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 378809 Color: 1
Size: 337774 Color: 0
Size: 283418 Color: 1

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 378761 Color: 0
Size: 341312 Color: 1
Size: 279928 Color: 0

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 378905 Color: 1
Size: 341205 Color: 1
Size: 279891 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 378827 Color: 0
Size: 362766 Color: 1
Size: 258408 Color: 0

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 378928 Color: 0
Size: 335122 Color: 0
Size: 285951 Color: 1

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 378999 Color: 1
Size: 339587 Color: 0
Size: 281415 Color: 1

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 378934 Color: 0
Size: 311696 Color: 1
Size: 309371 Color: 0

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 379007 Color: 0
Size: 321123 Color: 1
Size: 299871 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 379094 Color: 0
Size: 334677 Color: 1
Size: 286230 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 379111 Color: 1
Size: 358995 Color: 0
Size: 261895 Color: 1

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 379022 Color: 0
Size: 358453 Color: 1
Size: 262526 Color: 0

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 379094 Color: 1
Size: 342957 Color: 1
Size: 277950 Color: 0

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 379340 Color: 1
Size: 335467 Color: 1
Size: 285194 Color: 0

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 379370 Color: 0
Size: 315410 Color: 1
Size: 305221 Color: 0

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 379372 Color: 0
Size: 310872 Color: 1
Size: 309757 Color: 0

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 379486 Color: 0
Size: 339298 Color: 1
Size: 281217 Color: 0

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 379603 Color: 1
Size: 348787 Color: 0
Size: 271611 Color: 1

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 379684 Color: 0
Size: 359541 Color: 0
Size: 260776 Color: 1

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 379702 Color: 1
Size: 335731 Color: 1
Size: 284568 Color: 0

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 379730 Color: 0
Size: 351750 Color: 1
Size: 268521 Color: 0

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 379734 Color: 1
Size: 340276 Color: 1
Size: 279991 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 379888 Color: 1
Size: 348681 Color: 1
Size: 271432 Color: 0

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 379929 Color: 1
Size: 348574 Color: 0
Size: 271498 Color: 0

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 380020 Color: 0
Size: 360610 Color: 1
Size: 259371 Color: 0

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 380007 Color: 1
Size: 354356 Color: 1
Size: 265638 Color: 0

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 380054 Color: 0
Size: 352377 Color: 1
Size: 267570 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 380038 Color: 1
Size: 335923 Color: 0
Size: 284040 Color: 1

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 380118 Color: 1
Size: 313922 Color: 0
Size: 305961 Color: 1

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 380252 Color: 0
Size: 321898 Color: 0
Size: 297851 Color: 1

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 380170 Color: 1
Size: 360964 Color: 0
Size: 258867 Color: 1

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 380389 Color: 0
Size: 329631 Color: 0
Size: 289981 Color: 1

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 380509 Color: 0
Size: 338001 Color: 0
Size: 281491 Color: 1

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 380564 Color: 1
Size: 329962 Color: 1
Size: 289475 Color: 0

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 380594 Color: 1
Size: 318871 Color: 1
Size: 300536 Color: 0

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 380647 Color: 0
Size: 360152 Color: 0
Size: 259202 Color: 1

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 380700 Color: 1
Size: 351063 Color: 1
Size: 268238 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 380752 Color: 0
Size: 331163 Color: 1
Size: 288086 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 380717 Color: 1
Size: 352454 Color: 1
Size: 266830 Color: 0

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 380785 Color: 0
Size: 322719 Color: 0
Size: 296497 Color: 1

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 380733 Color: 1
Size: 362541 Color: 0
Size: 256727 Color: 1

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 380790 Color: 0
Size: 325692 Color: 1
Size: 293519 Color: 0

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 380798 Color: 0
Size: 358808 Color: 0
Size: 260395 Color: 1

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 380880 Color: 1
Size: 316630 Color: 0
Size: 302491 Color: 1

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 381162 Color: 1
Size: 355521 Color: 1
Size: 263318 Color: 0

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 381096 Color: 0
Size: 326405 Color: 1
Size: 292500 Color: 0

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 381292 Color: 1
Size: 310789 Color: 1
Size: 307920 Color: 0

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 381328 Color: 1
Size: 361180 Color: 0
Size: 257493 Color: 1

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 381483 Color: 1
Size: 367363 Color: 0
Size: 251155 Color: 1

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 381532 Color: 1
Size: 338505 Color: 0
Size: 279964 Color: 0

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 381536 Color: 1
Size: 344898 Color: 0
Size: 273567 Color: 1

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 381355 Color: 0
Size: 355653 Color: 0
Size: 262993 Color: 1

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 381563 Color: 1
Size: 321802 Color: 1
Size: 296636 Color: 0

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 381613 Color: 1
Size: 319491 Color: 1
Size: 298897 Color: 0

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 381631 Color: 1
Size: 332325 Color: 1
Size: 286045 Color: 0

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 381425 Color: 0
Size: 310647 Color: 1
Size: 307929 Color: 0

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 381704 Color: 0
Size: 339493 Color: 1
Size: 278804 Color: 1

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 381714 Color: 1
Size: 353018 Color: 0
Size: 265269 Color: 1

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 381886 Color: 1
Size: 338302 Color: 1
Size: 279813 Color: 0

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 381805 Color: 0
Size: 365374 Color: 1
Size: 252822 Color: 0

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 382020 Color: 0
Size: 340296 Color: 1
Size: 277685 Color: 0

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 382042 Color: 0
Size: 325378 Color: 1
Size: 292581 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 382124 Color: 1
Size: 329383 Color: 0
Size: 288494 Color: 1

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 382069 Color: 0
Size: 336595 Color: 0
Size: 281337 Color: 1

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 382184 Color: 1
Size: 314914 Color: 1
Size: 302903 Color: 0

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 382257 Color: 1
Size: 325404 Color: 0
Size: 292340 Color: 1

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 382405 Color: 1
Size: 359589 Color: 0
Size: 258007 Color: 1

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 382533 Color: 0
Size: 338956 Color: 1
Size: 278512 Color: 1

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 382423 Color: 1
Size: 314098 Color: 0
Size: 303480 Color: 1

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 382565 Color: 0
Size: 320177 Color: 1
Size: 297259 Color: 0

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 382740 Color: 0
Size: 312800 Color: 1
Size: 304461 Color: 1

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 382740 Color: 0
Size: 362934 Color: 1
Size: 254327 Color: 0

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 382619 Color: 1
Size: 337980 Color: 0
Size: 279402 Color: 1

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 382738 Color: 1
Size: 362660 Color: 0
Size: 254603 Color: 1

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 382750 Color: 0
Size: 358857 Color: 1
Size: 258394 Color: 0

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 382914 Color: 1
Size: 329721 Color: 0
Size: 287366 Color: 0

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 383052 Color: 0
Size: 349148 Color: 0
Size: 267801 Color: 1

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 383085 Color: 1
Size: 322419 Color: 1
Size: 294497 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 383052 Color: 0
Size: 312825 Color: 1
Size: 304124 Color: 0

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 383101 Color: 1
Size: 353570 Color: 1
Size: 263330 Color: 0

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 383117 Color: 0
Size: 348549 Color: 0
Size: 268335 Color: 1

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 383217 Color: 1
Size: 315531 Color: 0
Size: 301253 Color: 1

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 383300 Color: 1
Size: 319263 Color: 0
Size: 297438 Color: 1

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 383271 Color: 0
Size: 325033 Color: 1
Size: 291697 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 383574 Color: 0
Size: 320522 Color: 0
Size: 295905 Color: 1

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 383517 Color: 1
Size: 354620 Color: 1
Size: 261864 Color: 0

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 383602 Color: 0
Size: 334354 Color: 1
Size: 282045 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 383641 Color: 0
Size: 310223 Color: 1
Size: 306137 Color: 0

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 383778 Color: 1
Size: 354091 Color: 0
Size: 262132 Color: 1

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 383792 Color: 1
Size: 337352 Color: 0
Size: 278857 Color: 0

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 383925 Color: 1
Size: 324100 Color: 1
Size: 291976 Color: 0

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 383954 Color: 1
Size: 358255 Color: 0
Size: 257792 Color: 0

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 383911 Color: 0
Size: 345755 Color: 0
Size: 270335 Color: 1

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 384051 Color: 1
Size: 326711 Color: 0
Size: 289239 Color: 1

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 384033 Color: 0
Size: 323034 Color: 0
Size: 292934 Color: 1

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 384202 Color: 1
Size: 319038 Color: 1
Size: 296761 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 384050 Color: 0
Size: 363539 Color: 1
Size: 252412 Color: 0

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 384233 Color: 1
Size: 320689 Color: 1
Size: 295079 Color: 0

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 384284 Color: 1
Size: 309112 Color: 0
Size: 306605 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 384236 Color: 0
Size: 348583 Color: 0
Size: 267182 Color: 1

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 384287 Color: 0
Size: 336794 Color: 0
Size: 278920 Color: 1

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 384431 Color: 1
Size: 311132 Color: 1
Size: 304438 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 384355 Color: 0
Size: 354458 Color: 0
Size: 261188 Color: 1

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 384455 Color: 1
Size: 344875 Color: 1
Size: 270671 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 384389 Color: 0
Size: 362168 Color: 1
Size: 253444 Color: 0

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 384525 Color: 1
Size: 308246 Color: 0
Size: 307230 Color: 1

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 384508 Color: 0
Size: 352080 Color: 1
Size: 263413 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 384528 Color: 1
Size: 329008 Color: 1
Size: 286465 Color: 0

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 384553 Color: 1
Size: 352792 Color: 0
Size: 262656 Color: 1

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 384607 Color: 0
Size: 310008 Color: 1
Size: 305386 Color: 1

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 384614 Color: 0
Size: 334612 Color: 1
Size: 280775 Color: 1

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 384636 Color: 0
Size: 355806 Color: 1
Size: 259559 Color: 0

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 384663 Color: 0
Size: 350056 Color: 1
Size: 265282 Color: 0

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 384691 Color: 0
Size: 310357 Color: 1
Size: 304953 Color: 0

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 384750 Color: 1
Size: 311742 Color: 1
Size: 303509 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 384803 Color: 0
Size: 344847 Color: 1
Size: 270351 Color: 1

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 384855 Color: 0
Size: 309743 Color: 1
Size: 305403 Color: 0

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 384880 Color: 0
Size: 336806 Color: 1
Size: 278315 Color: 1

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 384771 Color: 1
Size: 331063 Color: 1
Size: 284167 Color: 0

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 384926 Color: 0
Size: 361100 Color: 1
Size: 253975 Color: 0

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 384960 Color: 0
Size: 324474 Color: 1
Size: 290567 Color: 0

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 385102 Color: 0
Size: 335274 Color: 0
Size: 279625 Color: 1

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 385205 Color: 0
Size: 332595 Color: 1
Size: 282201 Color: 1

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 385231 Color: 0
Size: 348887 Color: 1
Size: 265883 Color: 0

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 385063 Color: 1
Size: 333060 Color: 1
Size: 281878 Color: 0

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 385263 Color: 0
Size: 334440 Color: 1
Size: 280298 Color: 0

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 385304 Color: 0
Size: 308997 Color: 0
Size: 305700 Color: 1

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 385145 Color: 1
Size: 315618 Color: 1
Size: 299238 Color: 0

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 385292 Color: 1
Size: 353698 Color: 0
Size: 261011 Color: 1

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 385413 Color: 0
Size: 329510 Color: 1
Size: 285078 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 385323 Color: 1
Size: 343634 Color: 1
Size: 271044 Color: 0

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 385553 Color: 0
Size: 320981 Color: 1
Size: 293467 Color: 0

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 385605 Color: 0
Size: 319665 Color: 1
Size: 294731 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 385607 Color: 1
Size: 310686 Color: 1
Size: 303708 Color: 0

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 385690 Color: 1
Size: 337837 Color: 1
Size: 276474 Color: 0

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 385745 Color: 0
Size: 321389 Color: 0
Size: 292867 Color: 1

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 385744 Color: 1
Size: 348346 Color: 1
Size: 265911 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 385755 Color: 0
Size: 360252 Color: 1
Size: 253994 Color: 0

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 385806 Color: 0
Size: 320488 Color: 0
Size: 293707 Color: 1

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 385944 Color: 1
Size: 341268 Color: 0
Size: 272789 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 385948 Color: 1
Size: 345798 Color: 1
Size: 268255 Color: 0

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 385950 Color: 1
Size: 342219 Color: 0
Size: 271832 Color: 0

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 386152 Color: 1
Size: 315613 Color: 0
Size: 298236 Color: 0

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 386059 Color: 0
Size: 355841 Color: 0
Size: 258101 Color: 1

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 386235 Color: 1
Size: 349855 Color: 0
Size: 263911 Color: 1

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 386324 Color: 0
Size: 312339 Color: 0
Size: 301338 Color: 1

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 386329 Color: 0
Size: 352093 Color: 1
Size: 261579 Color: 0

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 386416 Color: 0
Size: 343317 Color: 0
Size: 270268 Color: 1

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 386470 Color: 1
Size: 319156 Color: 1
Size: 294375 Color: 0

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 386433 Color: 0
Size: 317184 Color: 0
Size: 296384 Color: 1

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 386567 Color: 1
Size: 325377 Color: 1
Size: 288057 Color: 0

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 386438 Color: 0
Size: 317345 Color: 1
Size: 296218 Color: 0

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 386598 Color: 1
Size: 310413 Color: 1
Size: 302990 Color: 0

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 386469 Color: 0
Size: 308880 Color: 1
Size: 304652 Color: 0

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 386687 Color: 1
Size: 327480 Color: 1
Size: 285834 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 386703 Color: 1
Size: 343070 Color: 0
Size: 270228 Color: 1

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 386733 Color: 0
Size: 310388 Color: 1
Size: 302880 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 386790 Color: 0
Size: 357802 Color: 1
Size: 255409 Color: 1

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 386837 Color: 0
Size: 330235 Color: 0
Size: 282929 Color: 1

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 386880 Color: 1
Size: 340652 Color: 0
Size: 272469 Color: 1

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 386860 Color: 0
Size: 307530 Color: 1
Size: 305611 Color: 0

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 386929 Color: 0
Size: 313170 Color: 1
Size: 299902 Color: 1

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 387007 Color: 0
Size: 331630 Color: 0
Size: 281364 Color: 1

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 387008 Color: 0
Size: 349958 Color: 1
Size: 263035 Color: 0

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 387056 Color: 1
Size: 338395 Color: 0
Size: 274550 Color: 1

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 387048 Color: 0
Size: 352469 Color: 1
Size: 260484 Color: 0

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 387134 Color: 1
Size: 343711 Color: 0
Size: 269156 Color: 1

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 387109 Color: 0
Size: 327555 Color: 1
Size: 285337 Color: 0

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 387127 Color: 0
Size: 322471 Color: 1
Size: 290403 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 387201 Color: 1
Size: 308675 Color: 1
Size: 304125 Color: 0

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 387339 Color: 0
Size: 353408 Color: 1
Size: 259254 Color: 1

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 387493 Color: 0
Size: 322395 Color: 1
Size: 290113 Color: 0

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 387370 Color: 1
Size: 344032 Color: 0
Size: 268599 Color: 1

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 387810 Color: 0
Size: 332377 Color: 1
Size: 279814 Color: 1

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 387840 Color: 0
Size: 331369 Color: 0
Size: 280792 Color: 1

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 387859 Color: 0
Size: 348986 Color: 1
Size: 263156 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 387865 Color: 0
Size: 318680 Color: 1
Size: 293456 Color: 1

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 387782 Color: 1
Size: 308399 Color: 0
Size: 303820 Color: 1

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 387887 Color: 1
Size: 361904 Color: 0
Size: 250210 Color: 1

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 387926 Color: 0
Size: 319248 Color: 1
Size: 292827 Color: 0

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 387918 Color: 1
Size: 323152 Color: 0
Size: 288931 Color: 1

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 387998 Color: 0
Size: 356931 Color: 1
Size: 255072 Color: 0

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 388304 Color: 1
Size: 352387 Color: 1
Size: 259310 Color: 0

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 388369 Color: 1
Size: 316623 Color: 0
Size: 295009 Color: 0

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 388376 Color: 1
Size: 319324 Color: 0
Size: 292301 Color: 1

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 388415 Color: 1
Size: 319846 Color: 0
Size: 291740 Color: 1

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 388502 Color: 1
Size: 332677 Color: 0
Size: 278822 Color: 1

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 388471 Color: 0
Size: 325829 Color: 1
Size: 285701 Color: 0

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 388592 Color: 0
Size: 343470 Color: 1
Size: 267939 Color: 0

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 388645 Color: 1
Size: 341258 Color: 0
Size: 270098 Color: 1

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 388617 Color: 0
Size: 337238 Color: 0
Size: 274146 Color: 1

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 388752 Color: 1
Size: 340974 Color: 0
Size: 270275 Color: 1

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 388708 Color: 0
Size: 341981 Color: 1
Size: 269312 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 388774 Color: 1
Size: 334615 Color: 1
Size: 276612 Color: 0

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 388712 Color: 0
Size: 309976 Color: 1
Size: 301313 Color: 0

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 388790 Color: 1
Size: 342044 Color: 1
Size: 269167 Color: 0

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 388822 Color: 1
Size: 349086 Color: 0
Size: 262093 Color: 1

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 388862 Color: 0
Size: 341112 Color: 0
Size: 270027 Color: 1

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 388868 Color: 1
Size: 346963 Color: 0
Size: 264170 Color: 1

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 388955 Color: 0
Size: 308779 Color: 1
Size: 302267 Color: 1

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 389248 Color: 1
Size: 349276 Color: 0
Size: 261477 Color: 1

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 389160 Color: 0
Size: 340877 Color: 0
Size: 269964 Color: 1

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 389361 Color: 1
Size: 310730 Color: 1
Size: 299910 Color: 0

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 389319 Color: 0
Size: 338785 Color: 0
Size: 271897 Color: 1

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 389444 Color: 0
Size: 308320 Color: 1
Size: 302237 Color: 1

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 389468 Color: 0
Size: 340776 Color: 1
Size: 269757 Color: 0

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 389479 Color: 0
Size: 346435 Color: 1
Size: 264087 Color: 1

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 389514 Color: 0
Size: 324062 Color: 1
Size: 286425 Color: 0

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 389516 Color: 1
Size: 330483 Color: 1
Size: 280002 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 389561 Color: 0
Size: 345699 Color: 1
Size: 264741 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 389800 Color: 0
Size: 337095 Color: 1
Size: 273106 Color: 0

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 389855 Color: 0
Size: 357868 Color: 1
Size: 252278 Color: 1

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 389886 Color: 0
Size: 333523 Color: 0
Size: 276592 Color: 1

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 389685 Color: 1
Size: 329373 Color: 0
Size: 280943 Color: 1

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 389926 Color: 0
Size: 320446 Color: 0
Size: 289629 Color: 1

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 389934 Color: 0
Size: 323850 Color: 1
Size: 286217 Color: 0

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 389853 Color: 1
Size: 310613 Color: 1
Size: 299535 Color: 0

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 390078 Color: 0
Size: 359678 Color: 0
Size: 250245 Color: 1

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 390013 Color: 1
Size: 315637 Color: 1
Size: 294351 Color: 0

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 390185 Color: 0
Size: 352690 Color: 0
Size: 257126 Color: 1

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 390019 Color: 1
Size: 320057 Color: 0
Size: 289925 Color: 1

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 390216 Color: 0
Size: 330715 Color: 1
Size: 279070 Color: 0

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 390217 Color: 1
Size: 343920 Color: 0
Size: 265864 Color: 1

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 390266 Color: 1
Size: 345453 Color: 0
Size: 264282 Color: 1

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 390267 Color: 0
Size: 332346 Color: 1
Size: 277388 Color: 0

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 390301 Color: 1
Size: 312685 Color: 1
Size: 297015 Color: 0

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 390540 Color: 0
Size: 350200 Color: 0
Size: 259261 Color: 1

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 390621 Color: 0
Size: 339984 Color: 1
Size: 269396 Color: 1

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 390636 Color: 0
Size: 323881 Color: 1
Size: 285484 Color: 1

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 390686 Color: 0
Size: 308028 Color: 0
Size: 301287 Color: 1

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 390662 Color: 1
Size: 351510 Color: 0
Size: 257829 Color: 1

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 390781 Color: 0
Size: 337722 Color: 1
Size: 271498 Color: 0

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 390982 Color: 1
Size: 305748 Color: 0
Size: 303271 Color: 1

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 391208 Color: 1
Size: 350768 Color: 1
Size: 258025 Color: 0

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 391233 Color: 1
Size: 356461 Color: 0
Size: 252307 Color: 1

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 391251 Color: 1
Size: 339093 Color: 1
Size: 269657 Color: 0

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 391335 Color: 1
Size: 315658 Color: 1
Size: 293008 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 391303 Color: 0
Size: 352390 Color: 0
Size: 256308 Color: 1

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 391368 Color: 0
Size: 346578 Color: 0
Size: 262055 Color: 1

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 391476 Color: 1
Size: 356098 Color: 0
Size: 252427 Color: 1

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 391388 Color: 0
Size: 333002 Color: 0
Size: 275611 Color: 1

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 391549 Color: 1
Size: 311118 Color: 1
Size: 297334 Color: 0

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 391550 Color: 1
Size: 354111 Color: 0
Size: 254340 Color: 1

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 391585 Color: 1
Size: 352957 Color: 0
Size: 255459 Color: 0

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 391653 Color: 1
Size: 336129 Color: 1
Size: 272219 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 391534 Color: 0
Size: 349743 Color: 1
Size: 258724 Color: 0

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 391702 Color: 1
Size: 358179 Color: 0
Size: 250120 Color: 1

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 391751 Color: 1
Size: 335584 Color: 0
Size: 272666 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 391771 Color: 1
Size: 307260 Color: 1
Size: 300970 Color: 0

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 391781 Color: 0
Size: 330279 Color: 0
Size: 277941 Color: 1

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 391865 Color: 1
Size: 336606 Color: 0
Size: 271530 Color: 1

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 391830 Color: 0
Size: 308827 Color: 1
Size: 299344 Color: 0

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 391880 Color: 1
Size: 331273 Color: 0
Size: 276848 Color: 1

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 391911 Color: 0
Size: 317718 Color: 1
Size: 290372 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 392003 Color: 0
Size: 320640 Color: 1
Size: 287358 Color: 1

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 392062 Color: 0
Size: 315923 Color: 1
Size: 292016 Color: 0

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 392071 Color: 1
Size: 334405 Color: 0
Size: 273525 Color: 1

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 392250 Color: 0
Size: 348086 Color: 1
Size: 259665 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 392259 Color: 1
Size: 308789 Color: 1
Size: 298953 Color: 0

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 392268 Color: 0
Size: 352377 Color: 0
Size: 255356 Color: 1

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 392288 Color: 1
Size: 313900 Color: 1
Size: 293813 Color: 0

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 392313 Color: 0
Size: 340589 Color: 0
Size: 267099 Color: 1

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 392412 Color: 1
Size: 351552 Color: 0
Size: 256037 Color: 1

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 392593 Color: 0
Size: 339373 Color: 0
Size: 268035 Color: 1

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 392429 Color: 1
Size: 312059 Color: 0
Size: 295513 Color: 1

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 392695 Color: 0
Size: 342712 Color: 1
Size: 264594 Color: 1

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 392859 Color: 0
Size: 336709 Color: 0
Size: 270433 Color: 1

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 392937 Color: 0
Size: 329614 Color: 1
Size: 277450 Color: 1

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 393158 Color: 0
Size: 320662 Color: 1
Size: 286181 Color: 0

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 393022 Color: 1
Size: 311900 Color: 1
Size: 295079 Color: 0

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 393183 Color: 0
Size: 355475 Color: 0
Size: 251343 Color: 1

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 393209 Color: 0
Size: 337371 Color: 0
Size: 269421 Color: 1

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 393092 Color: 1
Size: 311974 Color: 1
Size: 294935 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 393259 Color: 0
Size: 352182 Color: 1
Size: 254560 Color: 0

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 393375 Color: 0
Size: 313566 Color: 0
Size: 293060 Color: 1

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 393341 Color: 1
Size: 336516 Color: 0
Size: 270144 Color: 1

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 393381 Color: 0
Size: 326070 Color: 0
Size: 280550 Color: 1

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 393449 Color: 0
Size: 342327 Color: 0
Size: 264225 Color: 1

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 393440 Color: 1
Size: 343328 Color: 0
Size: 263233 Color: 1

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 393557 Color: 0
Size: 342976 Color: 0
Size: 263468 Color: 1

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 393721 Color: 1
Size: 324151 Color: 0
Size: 282129 Color: 0

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 393648 Color: 0
Size: 306965 Color: 0
Size: 299388 Color: 1

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 393792 Color: 1
Size: 305131 Color: 0
Size: 301078 Color: 1

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 393854 Color: 0
Size: 305326 Color: 0
Size: 300821 Color: 1

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 393928 Color: 0
Size: 337057 Color: 1
Size: 269016 Color: 1

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 394008 Color: 0
Size: 332331 Color: 0
Size: 273662 Color: 1

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 393994 Color: 1
Size: 303607 Color: 0
Size: 302400 Color: 1

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 394029 Color: 0
Size: 344094 Color: 0
Size: 261878 Color: 1

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 394010 Color: 1
Size: 327755 Color: 0
Size: 278236 Color: 1

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 394075 Color: 0
Size: 355578 Color: 0
Size: 250348 Color: 1

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 394093 Color: 1
Size: 328249 Color: 0
Size: 277659 Color: 1

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 394131 Color: 1
Size: 314208 Color: 0
Size: 291662 Color: 1

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 394240 Color: 1
Size: 343081 Color: 0
Size: 262680 Color: 0

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 394206 Color: 0
Size: 329909 Color: 0
Size: 275886 Color: 1

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 394297 Color: 0
Size: 322394 Color: 1
Size: 283310 Color: 0

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 394475 Color: 1
Size: 308961 Color: 0
Size: 296565 Color: 1

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 394613 Color: 0
Size: 307037 Color: 1
Size: 298351 Color: 1

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 394683 Color: 0
Size: 324164 Color: 1
Size: 281154 Color: 0

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 394759 Color: 0
Size: 324304 Color: 0
Size: 280938 Color: 1

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 394775 Color: 0
Size: 327910 Color: 1
Size: 277316 Color: 0

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 394792 Color: 0
Size: 342137 Color: 0
Size: 263072 Color: 1

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 394841 Color: 1
Size: 324011 Color: 1
Size: 281149 Color: 0

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 394794 Color: 0
Size: 312965 Color: 0
Size: 292242 Color: 1

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 394956 Color: 1
Size: 315240 Color: 0
Size: 289805 Color: 1

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 394860 Color: 0
Size: 331094 Color: 1
Size: 274047 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 395026 Color: 1
Size: 354116 Color: 0
Size: 250859 Color: 1

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 394944 Color: 0
Size: 304412 Color: 1
Size: 300645 Color: 0

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 395136 Color: 1
Size: 315758 Color: 1
Size: 289107 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 395334 Color: 1
Size: 354515 Color: 0
Size: 250152 Color: 1

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 395352 Color: 1
Size: 321043 Color: 1
Size: 283606 Color: 0

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 395381 Color: 0
Size: 305333 Color: 0
Size: 299287 Color: 1

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 395533 Color: 1
Size: 318333 Color: 0
Size: 286135 Color: 1

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 395548 Color: 1
Size: 338555 Color: 0
Size: 265898 Color: 0

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 395592 Color: 1
Size: 308786 Color: 0
Size: 295623 Color: 1

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 395754 Color: 1
Size: 311793 Color: 0
Size: 292454 Color: 0

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 395782 Color: 1
Size: 335368 Color: 0
Size: 268851 Color: 1

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 395773 Color: 0
Size: 305289 Color: 1
Size: 298939 Color: 0

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 395839 Color: 0
Size: 320945 Color: 1
Size: 283217 Color: 0

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 395822 Color: 1
Size: 335840 Color: 1
Size: 268339 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 395840 Color: 1
Size: 333997 Color: 1
Size: 270164 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 395926 Color: 1
Size: 342260 Color: 1
Size: 261815 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 396079 Color: 1
Size: 351360 Color: 0
Size: 252562 Color: 1

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 396171 Color: 0
Size: 338245 Color: 1
Size: 265585 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 396086 Color: 1
Size: 309903 Color: 0
Size: 294012 Color: 1

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 396190 Color: 0
Size: 348839 Color: 0
Size: 254972 Color: 1

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 396323 Color: 1
Size: 312899 Color: 0
Size: 290779 Color: 1

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 396400 Color: 1
Size: 316424 Color: 1
Size: 287177 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 396370 Color: 0
Size: 326227 Color: 0
Size: 277404 Color: 1

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 396426 Color: 0
Size: 326942 Color: 1
Size: 276633 Color: 0

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 396460 Color: 1
Size: 308296 Color: 0
Size: 295245 Color: 1

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 396571 Color: 0
Size: 307952 Color: 1
Size: 295478 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 396577 Color: 1
Size: 322703 Color: 1
Size: 280721 Color: 0

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 396597 Color: 0
Size: 302824 Color: 1
Size: 300580 Color: 0

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 396591 Color: 1
Size: 326200 Color: 1
Size: 277210 Color: 0

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 396721 Color: 1
Size: 320131 Color: 0
Size: 283149 Color: 1

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 396792 Color: 1
Size: 336214 Color: 1
Size: 266995 Color: 0

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 397034 Color: 0
Size: 347025 Color: 1
Size: 255942 Color: 0

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 397004 Color: 1
Size: 309543 Color: 1
Size: 293454 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 397135 Color: 0
Size: 341435 Color: 1
Size: 261431 Color: 1

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 397164 Color: 0
Size: 304985 Color: 0
Size: 297852 Color: 1

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 397182 Color: 0
Size: 307607 Color: 0
Size: 295212 Color: 1

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 397202 Color: 0
Size: 314840 Color: 1
Size: 287959 Color: 1

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 397335 Color: 0
Size: 326589 Color: 0
Size: 276077 Color: 1

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 397293 Color: 1
Size: 337669 Color: 1
Size: 265039 Color: 0

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 397524 Color: 0
Size: 326647 Color: 1
Size: 275830 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 397549 Color: 0
Size: 328148 Color: 1
Size: 274304 Color: 1

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 397842 Color: 0
Size: 338846 Color: 0
Size: 263313 Color: 1

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 397846 Color: 0
Size: 305393 Color: 1
Size: 296762 Color: 0

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 397955 Color: 0
Size: 348028 Color: 0
Size: 254018 Color: 1

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 398157 Color: 1
Size: 311603 Color: 1
Size: 290241 Color: 0

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 398007 Color: 0
Size: 326846 Color: 0
Size: 275148 Color: 1

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 398212 Color: 0
Size: 306963 Color: 1
Size: 294826 Color: 1

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 398218 Color: 0
Size: 305671 Color: 0
Size: 296112 Color: 1

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 398243 Color: 0
Size: 330365 Color: 0
Size: 271393 Color: 1

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 398216 Color: 1
Size: 343716 Color: 1
Size: 258069 Color: 0

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 398739 Color: 1
Size: 314518 Color: 0
Size: 286744 Color: 1

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 398744 Color: 0
Size: 308376 Color: 1
Size: 292881 Color: 0

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 398757 Color: 0
Size: 307931 Color: 1
Size: 293313 Color: 1

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 398774 Color: 0
Size: 336922 Color: 0
Size: 264305 Color: 1

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 398816 Color: 1
Size: 302365 Color: 1
Size: 298820 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 398967 Color: 0
Size: 338574 Color: 1
Size: 262460 Color: 0

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 398847 Color: 1
Size: 304584 Color: 1
Size: 296570 Color: 0

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 398945 Color: 1
Size: 318378 Color: 0
Size: 282678 Color: 1

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 399166 Color: 0
Size: 314145 Color: 1
Size: 286690 Color: 1

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 399127 Color: 1
Size: 308221 Color: 0
Size: 292653 Color: 1

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 399322 Color: 0
Size: 326070 Color: 1
Size: 274609 Color: 1

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 399385 Color: 0
Size: 340878 Color: 1
Size: 259738 Color: 0

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 399393 Color: 0
Size: 302206 Color: 0
Size: 298402 Color: 1

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 399423 Color: 1
Size: 312969 Color: 0
Size: 287609 Color: 1

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 399612 Color: 0
Size: 330401 Color: 1
Size: 269988 Color: 0

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 399649 Color: 0
Size: 342966 Color: 0
Size: 257386 Color: 1

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 399596 Color: 1
Size: 348665 Color: 1
Size: 251740 Color: 0

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 399699 Color: 0
Size: 309450 Color: 0
Size: 290852 Color: 1

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 399639 Color: 1
Size: 331531 Color: 0
Size: 268831 Color: 1

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 399801 Color: 1
Size: 349772 Color: 0
Size: 250428 Color: 1

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 399820 Color: 0
Size: 347317 Color: 1
Size: 252864 Color: 0

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 399951 Color: 0
Size: 335102 Color: 1
Size: 264948 Color: 1

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 400231 Color: 1
Size: 300548 Color: 0
Size: 299222 Color: 0

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 400269 Color: 1
Size: 304199 Color: 0
Size: 295533 Color: 1

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 400219 Color: 0
Size: 327466 Color: 1
Size: 272316 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 400352 Color: 1
Size: 311819 Color: 0
Size: 287830 Color: 0

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 400358 Color: 1
Size: 300031 Color: 0
Size: 299612 Color: 1

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 400399 Color: 1
Size: 344928 Color: 0
Size: 254674 Color: 1

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 400458 Color: 0
Size: 300438 Color: 1
Size: 299105 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 400571 Color: 1
Size: 332142 Color: 1
Size: 267288 Color: 0

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 400609 Color: 0
Size: 316114 Color: 0
Size: 283278 Color: 1

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 400625 Color: 0
Size: 312772 Color: 0
Size: 286604 Color: 1

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 400647 Color: 0
Size: 300195 Color: 1
Size: 299159 Color: 0

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 400885 Color: 1
Size: 316260 Color: 1
Size: 282856 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 400863 Color: 0
Size: 337548 Color: 1
Size: 261590 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 401266 Color: 1
Size: 330388 Color: 0
Size: 268347 Color: 1

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 401124 Color: 0
Size: 322194 Color: 0
Size: 276683 Color: 1

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 401266 Color: 1
Size: 316026 Color: 0
Size: 282709 Color: 1

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 401298 Color: 1
Size: 341392 Color: 0
Size: 257311 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 401331 Color: 1
Size: 338412 Color: 1
Size: 260258 Color: 0

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 401342 Color: 1
Size: 305756 Color: 0
Size: 292903 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 401348 Color: 1
Size: 336652 Color: 0
Size: 262001 Color: 1

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 401406 Color: 0
Size: 326993 Color: 1
Size: 271602 Color: 0

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 401454 Color: 0
Size: 309939 Color: 1
Size: 288608 Color: 1

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 401622 Color: 0
Size: 323096 Color: 1
Size: 275283 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 401625 Color: 0
Size: 326257 Color: 0
Size: 272119 Color: 1

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 401704 Color: 0
Size: 315619 Color: 1
Size: 282678 Color: 1

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 401723 Color: 0
Size: 341740 Color: 1
Size: 256538 Color: 0

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 401775 Color: 0
Size: 335609 Color: 1
Size: 262617 Color: 1

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 401826 Color: 0
Size: 309976 Color: 1
Size: 288199 Color: 0

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 401837 Color: 0
Size: 312299 Color: 1
Size: 285865 Color: 1

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 401844 Color: 0
Size: 331362 Color: 1
Size: 266795 Color: 0

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 401852 Color: 0
Size: 313165 Color: 1
Size: 284984 Color: 1

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 401697 Color: 1
Size: 309973 Color: 1
Size: 288331 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 401908 Color: 0
Size: 305636 Color: 1
Size: 292457 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 401830 Color: 1
Size: 315540 Color: 0
Size: 282631 Color: 1

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 402001 Color: 0
Size: 334440 Color: 0
Size: 263560 Color: 1

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 402092 Color: 0
Size: 316379 Color: 0
Size: 281530 Color: 1

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 402104 Color: 0
Size: 347278 Color: 0
Size: 250619 Color: 1

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 402039 Color: 1
Size: 337827 Color: 1
Size: 260135 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 402049 Color: 1
Size: 334577 Color: 1
Size: 263375 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 402190 Color: 0
Size: 347134 Color: 0
Size: 250677 Color: 1

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 402072 Color: 1
Size: 309543 Color: 1
Size: 288386 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 402231 Color: 0
Size: 308820 Color: 1
Size: 288950 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 402246 Color: 1
Size: 320428 Color: 1
Size: 277327 Color: 0

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 402362 Color: 1
Size: 317357 Color: 0
Size: 280282 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 402413 Color: 1
Size: 309471 Color: 1
Size: 288117 Color: 0

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 402416 Color: 0
Size: 300244 Color: 1
Size: 297341 Color: 0

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 402534 Color: 1
Size: 304399 Color: 1
Size: 293068 Color: 0

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 402518 Color: 0
Size: 335119 Color: 1
Size: 262364 Color: 0

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 402688 Color: 1
Size: 332025 Color: 0
Size: 265288 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 402742 Color: 1
Size: 337283 Color: 0
Size: 259976 Color: 1

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 402793 Color: 1
Size: 323483 Color: 0
Size: 273725 Color: 0

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 402812 Color: 1
Size: 329297 Color: 0
Size: 267892 Color: 1

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 402983 Color: 0
Size: 333437 Color: 1
Size: 263581 Color: 0

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 402996 Color: 1
Size: 342217 Color: 0
Size: 254788 Color: 1

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 402986 Color: 0
Size: 327507 Color: 0
Size: 269508 Color: 1

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 403097 Color: 1
Size: 335687 Color: 0
Size: 261217 Color: 1

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 403202 Color: 0
Size: 311933 Color: 1
Size: 284866 Color: 0

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 403332 Color: 1
Size: 334198 Color: 1
Size: 262471 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 403428 Color: 1
Size: 306384 Color: 1
Size: 290189 Color: 0

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 403366 Color: 0
Size: 324857 Color: 1
Size: 271778 Color: 0

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 403502 Color: 1
Size: 345915 Color: 1
Size: 250584 Color: 0

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 403440 Color: 0
Size: 327333 Color: 0
Size: 269228 Color: 1

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 403593 Color: 1
Size: 304049 Color: 0
Size: 292359 Color: 0

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 403792 Color: 0
Size: 327722 Color: 1
Size: 268487 Color: 1

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 403816 Color: 0
Size: 331239 Color: 0
Size: 264946 Color: 1

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 403818 Color: 0
Size: 345545 Color: 1
Size: 250638 Color: 1

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 404041 Color: 1
Size: 322152 Color: 0
Size: 273808 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 404105 Color: 1
Size: 322173 Color: 1
Size: 273723 Color: 0

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 404010 Color: 0
Size: 324153 Color: 0
Size: 271838 Color: 1

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 404168 Color: 0
Size: 335741 Color: 1
Size: 260092 Color: 1

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 404212 Color: 1
Size: 337436 Color: 1
Size: 258353 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 404292 Color: 1
Size: 298009 Color: 0
Size: 297700 Color: 1

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 404220 Color: 0
Size: 343972 Color: 0
Size: 251809 Color: 1

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 404225 Color: 0
Size: 324273 Color: 1
Size: 271503 Color: 0

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 404409 Color: 1
Size: 337861 Color: 1
Size: 257731 Color: 0

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 404315 Color: 0
Size: 307726 Color: 0
Size: 287960 Color: 1

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 404415 Color: 1
Size: 336781 Color: 1
Size: 258805 Color: 0

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 404512 Color: 0
Size: 339033 Color: 1
Size: 256456 Color: 0

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 404639 Color: 1
Size: 337386 Color: 0
Size: 257976 Color: 1

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 404596 Color: 0
Size: 307349 Color: 1
Size: 288056 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 404800 Color: 0
Size: 310920 Color: 1
Size: 284281 Color: 0

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 404827 Color: 0
Size: 329300 Color: 1
Size: 265874 Color: 1

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 404903 Color: 0
Size: 313846 Color: 1
Size: 281252 Color: 1

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 404916 Color: 0
Size: 303600 Color: 1
Size: 291485 Color: 0

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 404942 Color: 0
Size: 321156 Color: 0
Size: 273903 Color: 1

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 404889 Color: 1
Size: 339703 Color: 0
Size: 255409 Color: 1

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 405017 Color: 1
Size: 340232 Color: 0
Size: 254752 Color: 1

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 405095 Color: 0
Size: 320087 Color: 1
Size: 274819 Color: 0

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 405127 Color: 0
Size: 315661 Color: 0
Size: 279213 Color: 1

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 405190 Color: 0
Size: 338043 Color: 1
Size: 256768 Color: 1

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 405207 Color: 0
Size: 342475 Color: 1
Size: 252319 Color: 0

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 405131 Color: 1
Size: 307444 Color: 1
Size: 287426 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 405275 Color: 1
Size: 332600 Color: 1
Size: 262126 Color: 0

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 405361 Color: 1
Size: 326958 Color: 1
Size: 267682 Color: 0

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 405393 Color: 0
Size: 317280 Color: 1
Size: 277328 Color: 0

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 405631 Color: 0
Size: 327414 Color: 0
Size: 266956 Color: 1

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 405554 Color: 1
Size: 340037 Color: 0
Size: 254410 Color: 1

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 405632 Color: 0
Size: 318800 Color: 0
Size: 275569 Color: 1

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 405751 Color: 0
Size: 322953 Color: 1
Size: 271297 Color: 1

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 405694 Color: 1
Size: 299622 Color: 0
Size: 294685 Color: 1

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 405754 Color: 0
Size: 331842 Color: 1
Size: 262405 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 405877 Color: 1
Size: 334252 Color: 0
Size: 259872 Color: 1

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 406012 Color: 0
Size: 324380 Color: 1
Size: 269609 Color: 1

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 406063 Color: 0
Size: 299693 Color: 1
Size: 294245 Color: 0

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 406113 Color: 0
Size: 298642 Color: 1
Size: 295246 Color: 1

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 406074 Color: 1
Size: 327259 Color: 0
Size: 266668 Color: 1

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 406256 Color: 0
Size: 343506 Color: 1
Size: 250239 Color: 1

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 406275 Color: 0
Size: 318815 Color: 1
Size: 274911 Color: 0

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 406208 Color: 1
Size: 319818 Color: 0
Size: 273975 Color: 1

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 406287 Color: 0
Size: 332729 Color: 0
Size: 260985 Color: 1

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 406499 Color: 0
Size: 328786 Color: 1
Size: 264716 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 406515 Color: 0
Size: 318522 Color: 1
Size: 274964 Color: 1

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 406538 Color: 1
Size: 317560 Color: 0
Size: 275903 Color: 1

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 406713 Color: 1
Size: 298781 Color: 1
Size: 294507 Color: 0

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 406829 Color: 1
Size: 335170 Color: 0
Size: 258002 Color: 1

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 406954 Color: 0
Size: 307943 Color: 1
Size: 285104 Color: 1

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 406949 Color: 1
Size: 330577 Color: 0
Size: 262475 Color: 1

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 407037 Color: 0
Size: 327132 Color: 0
Size: 265832 Color: 1

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 407149 Color: 1
Size: 317500 Color: 1
Size: 275352 Color: 0

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 407119 Color: 0
Size: 327878 Color: 0
Size: 265004 Color: 1

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 407375 Color: 1
Size: 310051 Color: 0
Size: 282575 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 407409 Color: 1
Size: 342380 Color: 1
Size: 250212 Color: 0

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 407303 Color: 0
Size: 309326 Color: 0
Size: 283372 Color: 1

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 407473 Color: 1
Size: 332843 Color: 0
Size: 259685 Color: 1

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 407429 Color: 0
Size: 300974 Color: 1
Size: 291598 Color: 0

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 407580 Color: 1
Size: 341900 Color: 1
Size: 250521 Color: 0

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 407589 Color: 0
Size: 340013 Color: 0
Size: 252399 Color: 1

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 407665 Color: 0
Size: 331114 Color: 0
Size: 261222 Color: 1

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 407812 Color: 0
Size: 317298 Color: 0
Size: 274891 Color: 1

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 407963 Color: 1
Size: 301886 Color: 0
Size: 290152 Color: 0

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 408024 Color: 1
Size: 339233 Color: 1
Size: 252744 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 408203 Color: 0
Size: 339809 Color: 1
Size: 251989 Color: 0

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 408271 Color: 0
Size: 306615 Color: 1
Size: 285115 Color: 1

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 408174 Color: 1
Size: 321937 Color: 1
Size: 269890 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 408485 Color: 0
Size: 340335 Color: 0
Size: 251181 Color: 1

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 408552 Color: 1
Size: 325895 Color: 0
Size: 265554 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 408548 Color: 0
Size: 338897 Color: 1
Size: 252556 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 408576 Color: 1
Size: 333848 Color: 0
Size: 257577 Color: 1

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 408699 Color: 0
Size: 322370 Color: 0
Size: 268932 Color: 1

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 408706 Color: 0
Size: 310475 Color: 1
Size: 280820 Color: 1

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 408801 Color: 1
Size: 310705 Color: 0
Size: 280495 Color: 1

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 408841 Color: 1
Size: 322256 Color: 0
Size: 268904 Color: 0

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 408875 Color: 1
Size: 324913 Color: 0
Size: 266213 Color: 1

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 409031 Color: 0
Size: 331513 Color: 1
Size: 259457 Color: 1

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 409017 Color: 1
Size: 314274 Color: 1
Size: 276710 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 409163 Color: 1
Size: 296317 Color: 1
Size: 294521 Color: 0

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 409163 Color: 0
Size: 320847 Color: 0
Size: 269991 Color: 1

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 409242 Color: 1
Size: 328986 Color: 0
Size: 261773 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 409364 Color: 1
Size: 310113 Color: 0
Size: 280524 Color: 0

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 409533 Color: 1
Size: 317911 Color: 0
Size: 272557 Color: 1

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 409529 Color: 0
Size: 321029 Color: 0
Size: 269443 Color: 1

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 409537 Color: 1
Size: 319207 Color: 0
Size: 271257 Color: 1

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 409590 Color: 0
Size: 313325 Color: 1
Size: 277086 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 409789 Color: 1
Size: 317962 Color: 0
Size: 272250 Color: 1

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 409694 Color: 0
Size: 299563 Color: 1
Size: 290744 Color: 0

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 409834 Color: 1
Size: 339656 Color: 1
Size: 250511 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 409850 Color: 1
Size: 298258 Color: 0
Size: 291893 Color: 1

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 409831 Color: 0
Size: 337491 Color: 0
Size: 252679 Color: 1

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 409878 Color: 1
Size: 309106 Color: 1
Size: 281017 Color: 0

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 409963 Color: 1
Size: 321710 Color: 0
Size: 268328 Color: 0

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 409985 Color: 1
Size: 332478 Color: 0
Size: 257538 Color: 1

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 409987 Color: 1
Size: 324818 Color: 0
Size: 265196 Color: 0

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 409991 Color: 1
Size: 314801 Color: 1
Size: 275209 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 410073 Color: 0
Size: 297314 Color: 1
Size: 292614 Color: 1

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 410121 Color: 1
Size: 326055 Color: 1
Size: 263825 Color: 0

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 410173 Color: 0
Size: 323183 Color: 0
Size: 266645 Color: 1

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 410192 Color: 0
Size: 303677 Color: 1
Size: 286132 Color: 1

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 410285 Color: 0
Size: 321266 Color: 1
Size: 268450 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 410498 Color: 1
Size: 305220 Color: 0
Size: 284283 Color: 0

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 410499 Color: 1
Size: 316935 Color: 0
Size: 272567 Color: 1

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 410519 Color: 1
Size: 331061 Color: 1
Size: 258421 Color: 0

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 410623 Color: 1
Size: 327916 Color: 0
Size: 261462 Color: 1

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 410558 Color: 0
Size: 311229 Color: 1
Size: 278214 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 410650 Color: 1
Size: 321762 Color: 1
Size: 267589 Color: 0

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 410723 Color: 0
Size: 314766 Color: 1
Size: 274512 Color: 1

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 410844 Color: 0
Size: 336646 Color: 1
Size: 252511 Color: 0

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 410901 Color: 0
Size: 303335 Color: 1
Size: 285765 Color: 1

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 410740 Color: 1
Size: 324050 Color: 1
Size: 265211 Color: 0

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 410835 Color: 1
Size: 326055 Color: 1
Size: 263111 Color: 0

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 410974 Color: 1
Size: 307146 Color: 1
Size: 281881 Color: 0

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 411090 Color: 0
Size: 311862 Color: 0
Size: 277049 Color: 1

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 411151 Color: 0
Size: 301113 Color: 1
Size: 287737 Color: 1

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 411187 Color: 0
Size: 336695 Color: 1
Size: 252119 Color: 0

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 411041 Color: 1
Size: 313150 Color: 1
Size: 275810 Color: 0

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 411189 Color: 0
Size: 318373 Color: 1
Size: 270439 Color: 0

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 411139 Color: 1
Size: 301585 Color: 1
Size: 287277 Color: 0

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 411209 Color: 0
Size: 327347 Color: 1
Size: 261445 Color: 0

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 411160 Color: 1
Size: 329244 Color: 0
Size: 259597 Color: 1

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 411345 Color: 1
Size: 307808 Color: 1
Size: 280848 Color: 0

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 411343 Color: 0
Size: 302074 Color: 0
Size: 286584 Color: 1

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 411382 Color: 1
Size: 326511 Color: 0
Size: 262108 Color: 1

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 411452 Color: 1
Size: 325035 Color: 0
Size: 263514 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 411459 Color: 1
Size: 336316 Color: 1
Size: 252226 Color: 0

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 411610 Color: 0
Size: 337009 Color: 1
Size: 251382 Color: 0

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 411809 Color: 0
Size: 326104 Color: 1
Size: 262088 Color: 1

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 411754 Color: 1
Size: 296996 Color: 1
Size: 291251 Color: 0

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 411884 Color: 0
Size: 315450 Color: 1
Size: 272667 Color: 0

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 411985 Color: 1
Size: 329699 Color: 0
Size: 258317 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 412091 Color: 1
Size: 333402 Color: 0
Size: 254508 Color: 1

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 412023 Color: 0
Size: 312830 Color: 0
Size: 275148 Color: 1

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 412148 Color: 1
Size: 332350 Color: 1
Size: 255503 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 412259 Color: 1
Size: 296710 Color: 0
Size: 291032 Color: 0

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 412378 Color: 1
Size: 308297 Color: 0
Size: 279326 Color: 1

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 412425 Color: 1
Size: 326635 Color: 1
Size: 260941 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 412480 Color: 0
Size: 303114 Color: 1
Size: 284407 Color: 0

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 412477 Color: 1
Size: 300119 Color: 0
Size: 287405 Color: 1

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 412665 Color: 0
Size: 297475 Color: 1
Size: 289861 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 412718 Color: 0
Size: 330532 Color: 0
Size: 256751 Color: 1

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 412960 Color: 0
Size: 294123 Color: 1
Size: 292918 Color: 0

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 412967 Color: 0
Size: 302014 Color: 1
Size: 285020 Color: 0

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 413194 Color: 1
Size: 328759 Color: 1
Size: 258048 Color: 0

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 413149 Color: 0
Size: 328654 Color: 1
Size: 258198 Color: 0

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 413364 Color: 1
Size: 308647 Color: 1
Size: 277990 Color: 0

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 413488 Color: 1
Size: 313650 Color: 0
Size: 272863 Color: 1

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 413395 Color: 0
Size: 294263 Color: 1
Size: 292343 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 413525 Color: 1
Size: 324001 Color: 1
Size: 262475 Color: 0

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 413449 Color: 0
Size: 328558 Color: 0
Size: 257994 Color: 1

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 413531 Color: 1
Size: 323124 Color: 1
Size: 263346 Color: 0

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 413580 Color: 1
Size: 324313 Color: 0
Size: 262108 Color: 1

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 413540 Color: 0
Size: 336018 Color: 0
Size: 250443 Color: 1

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 413664 Color: 0
Size: 308845 Color: 1
Size: 277492 Color: 0

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 413706 Color: 0
Size: 301904 Color: 1
Size: 284391 Color: 0

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 413616 Color: 1
Size: 298582 Color: 1
Size: 287803 Color: 0

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 413822 Color: 0
Size: 294065 Color: 1
Size: 292114 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 413845 Color: 0
Size: 335489 Color: 0
Size: 250667 Color: 1

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 413918 Color: 1
Size: 308494 Color: 0
Size: 277589 Color: 1

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 414018 Color: 0
Size: 298477 Color: 0
Size: 287506 Color: 1

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 413926 Color: 1
Size: 310012 Color: 1
Size: 276063 Color: 0

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 414091 Color: 1
Size: 313318 Color: 1
Size: 272592 Color: 0

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 414193 Color: 0
Size: 314206 Color: 1
Size: 271602 Color: 0

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 414201 Color: 1
Size: 301381 Color: 1
Size: 284419 Color: 0

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 414281 Color: 1
Size: 334282 Color: 0
Size: 251438 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 414297 Color: 1
Size: 310332 Color: 0
Size: 275372 Color: 1

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 414312 Color: 0
Size: 302476 Color: 1
Size: 283213 Color: 0

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 414448 Color: 0
Size: 299230 Color: 1
Size: 286323 Color: 1

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 414456 Color: 0
Size: 325596 Color: 1
Size: 259949 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 414467 Color: 1
Size: 293679 Color: 0
Size: 291855 Color: 1

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 414462 Color: 0
Size: 318496 Color: 1
Size: 267043 Color: 0

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 414507 Color: 1
Size: 293266 Color: 0
Size: 292228 Color: 1

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 414510 Color: 1
Size: 299153 Color: 0
Size: 286338 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 414651 Color: 0
Size: 323189 Color: 1
Size: 262161 Color: 1

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 414660 Color: 0
Size: 319416 Color: 1
Size: 265925 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 414622 Color: 1
Size: 332841 Color: 1
Size: 252538 Color: 0

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 414716 Color: 0
Size: 303348 Color: 1
Size: 281937 Color: 0

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 414651 Color: 1
Size: 320969 Color: 0
Size: 264381 Color: 1

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 414759 Color: 0
Size: 315102 Color: 1
Size: 270140 Color: 0

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 414940 Color: 0
Size: 317117 Color: 1
Size: 267944 Color: 1

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 414991 Color: 0
Size: 328645 Color: 1
Size: 256365 Color: 0

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 414979 Color: 1
Size: 331190 Color: 0
Size: 253832 Color: 1

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 415038 Color: 0
Size: 329602 Color: 1
Size: 255361 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 414998 Color: 1
Size: 333625 Color: 1
Size: 251378 Color: 0

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 415168 Color: 0
Size: 305748 Color: 0
Size: 279085 Color: 1

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 415096 Color: 1
Size: 300177 Color: 1
Size: 284728 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 415307 Color: 1
Size: 321085 Color: 0
Size: 263609 Color: 0

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 415439 Color: 1
Size: 333274 Color: 0
Size: 251288 Color: 1

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 392038 Color: 1
Size: 357948 Color: 1
Size: 250015 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 415545 Color: 0
Size: 293587 Color: 1
Size: 290869 Color: 1

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 415575 Color: 0
Size: 309788 Color: 1
Size: 274638 Color: 1

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 415604 Color: 0
Size: 311324 Color: 1
Size: 273073 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 415604 Color: 0
Size: 309527 Color: 1
Size: 274870 Color: 1

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 415615 Color: 0
Size: 306637 Color: 0
Size: 277749 Color: 1

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 415743 Color: 1
Size: 331674 Color: 0
Size: 252584 Color: 1

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 415832 Color: 1
Size: 309049 Color: 0
Size: 275120 Color: 1

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 415857 Color: 0
Size: 319348 Color: 0
Size: 264796 Color: 1

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 415837 Color: 1
Size: 331081 Color: 1
Size: 253083 Color: 0

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 416063 Color: 1
Size: 323630 Color: 0
Size: 260308 Color: 0

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 416139 Color: 0
Size: 331358 Color: 1
Size: 252504 Color: 0

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 416218 Color: 0
Size: 313901 Color: 1
Size: 269882 Color: 1

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 416313 Color: 0
Size: 301403 Color: 1
Size: 282285 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 416354 Color: 0
Size: 313846 Color: 1
Size: 269801 Color: 0

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 416372 Color: 1
Size: 314463 Color: 1
Size: 269166 Color: 0

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 416409 Color: 0
Size: 320315 Color: 1
Size: 263277 Color: 0

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 416510 Color: 1
Size: 294743 Color: 0
Size: 288748 Color: 1

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 416488 Color: 0
Size: 295206 Color: 1
Size: 288307 Color: 0

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 416632 Color: 0
Size: 326173 Color: 1
Size: 257196 Color: 0

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 416735 Color: 1
Size: 303028 Color: 1
Size: 280238 Color: 0

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 416854 Color: 0
Size: 328465 Color: 1
Size: 254682 Color: 1

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 416965 Color: 0
Size: 303842 Color: 1
Size: 279194 Color: 0

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 416949 Color: 1
Size: 328075 Color: 1
Size: 254977 Color: 0

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 416968 Color: 1
Size: 305986 Color: 0
Size: 277047 Color: 1

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 409742 Color: 1
Size: 314760 Color: 0
Size: 275499 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 416981 Color: 1
Size: 332612 Color: 1
Size: 250408 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 417148 Color: 1
Size: 299789 Color: 0
Size: 283064 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 417185 Color: 0
Size: 318512 Color: 0
Size: 264304 Color: 1

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 417225 Color: 0
Size: 331567 Color: 1
Size: 251209 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 417453 Color: 0
Size: 294376 Color: 0
Size: 288172 Color: 1

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 417401 Color: 1
Size: 320896 Color: 1
Size: 261704 Color: 0

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 417483 Color: 0
Size: 314650 Color: 0
Size: 267868 Color: 1

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 417444 Color: 1
Size: 329797 Color: 0
Size: 252760 Color: 1

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 417623 Color: 1
Size: 305318 Color: 1
Size: 277060 Color: 0

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 417619 Color: 0
Size: 315346 Color: 0
Size: 267036 Color: 1

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 417658 Color: 1
Size: 327111 Color: 0
Size: 255232 Color: 1

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 417792 Color: 0
Size: 294076 Color: 0
Size: 288133 Color: 1

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 417834 Color: 0
Size: 322925 Color: 1
Size: 259242 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 418035 Color: 0
Size: 322865 Color: 1
Size: 259101 Color: 1

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 418057 Color: 0
Size: 330357 Color: 1
Size: 251587 Color: 0

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 418066 Color: 1
Size: 313059 Color: 1
Size: 268876 Color: 0

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 418155 Color: 1
Size: 328429 Color: 0
Size: 253417 Color: 1

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 418190 Color: 0
Size: 330326 Color: 1
Size: 251485 Color: 0

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 418168 Color: 1
Size: 311933 Color: 1
Size: 269900 Color: 0

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 418240 Color: 0
Size: 310988 Color: 1
Size: 270773 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 418376 Color: 0
Size: 310978 Color: 0
Size: 270647 Color: 1

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 418401 Color: 0
Size: 317340 Color: 0
Size: 264260 Color: 1

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 418346 Color: 1
Size: 307057 Color: 1
Size: 274598 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 418440 Color: 0
Size: 298641 Color: 0
Size: 282920 Color: 1

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 418573 Color: 0
Size: 325646 Color: 0
Size: 255782 Color: 1

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 418573 Color: 1
Size: 294529 Color: 0
Size: 286899 Color: 1

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 418579 Color: 1
Size: 306536 Color: 1
Size: 274886 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 418596 Color: 1
Size: 325385 Color: 1
Size: 256020 Color: 0

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 418675 Color: 1
Size: 315248 Color: 1
Size: 266078 Color: 0

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 418687 Color: 1
Size: 298149 Color: 1
Size: 283165 Color: 0

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 418724 Color: 0
Size: 322262 Color: 1
Size: 259015 Color: 0

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 418691 Color: 1
Size: 298488 Color: 1
Size: 282822 Color: 0

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 418893 Color: 1
Size: 303496 Color: 0
Size: 277612 Color: 0

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 418926 Color: 1
Size: 298073 Color: 0
Size: 283002 Color: 1

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 418949 Color: 1
Size: 309834 Color: 0
Size: 271218 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 418920 Color: 0
Size: 298257 Color: 0
Size: 282824 Color: 1

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 419079 Color: 1
Size: 292382 Color: 1
Size: 288540 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 419129 Color: 0
Size: 314038 Color: 0
Size: 266834 Color: 1

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 419232 Color: 1
Size: 330078 Color: 1
Size: 250691 Color: 0

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 419138 Color: 0
Size: 322512 Color: 0
Size: 258351 Color: 1

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 419253 Color: 0
Size: 305469 Color: 1
Size: 275279 Color: 1

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 419284 Color: 0
Size: 321517 Color: 0
Size: 259200 Color: 1

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 419373 Color: 1
Size: 325325 Color: 1
Size: 255303 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 419405 Color: 0
Size: 319074 Color: 0
Size: 261522 Color: 1

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 419558 Color: 1
Size: 302271 Color: 1
Size: 278172 Color: 0

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 419510 Color: 0
Size: 295424 Color: 0
Size: 285067 Color: 1

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 419560 Color: 1
Size: 329797 Color: 1
Size: 250644 Color: 0

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 419625 Color: 0
Size: 296050 Color: 0
Size: 284326 Color: 1

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 419741 Color: 1
Size: 302398 Color: 0
Size: 277862 Color: 1

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 419764 Color: 0
Size: 294112 Color: 0
Size: 286125 Color: 1

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 419918 Color: 0
Size: 324430 Color: 1
Size: 255653 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 419803 Color: 1
Size: 298191 Color: 1
Size: 282007 Color: 0

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 419951 Color: 0
Size: 310646 Color: 0
Size: 269404 Color: 1

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 419978 Color: 0
Size: 328788 Color: 0
Size: 251235 Color: 1

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 420018 Color: 0
Size: 310002 Color: 1
Size: 269981 Color: 0

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 420073 Color: 1
Size: 315447 Color: 0
Size: 264481 Color: 0

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 420150 Color: 1
Size: 305765 Color: 1
Size: 274086 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 420298 Color: 0
Size: 321819 Color: 0
Size: 257884 Color: 1

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 420247 Color: 1
Size: 315536 Color: 0
Size: 264218 Color: 1

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 420430 Color: 1
Size: 323025 Color: 0
Size: 256546 Color: 1

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 420520 Color: 0
Size: 309686 Color: 0
Size: 269795 Color: 1

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 420488 Color: 1
Size: 313179 Color: 0
Size: 266334 Color: 1

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 420566 Color: 0
Size: 318189 Color: 0
Size: 261246 Color: 1

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 420660 Color: 0
Size: 297899 Color: 1
Size: 281442 Color: 1

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 420666 Color: 0
Size: 318074 Color: 1
Size: 261261 Color: 0

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 420600 Color: 1
Size: 304062 Color: 1
Size: 275339 Color: 0

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 420999 Color: 0
Size: 316304 Color: 0
Size: 262698 Color: 1

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 421010 Color: 1
Size: 324308 Color: 0
Size: 254683 Color: 1

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 421014 Color: 0
Size: 309274 Color: 1
Size: 269713 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 421032 Color: 1
Size: 315885 Color: 1
Size: 263084 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 421027 Color: 0
Size: 302115 Color: 1
Size: 276859 Color: 0

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 421047 Color: 1
Size: 289878 Color: 0
Size: 289076 Color: 1

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 421263 Color: 0
Size: 300110 Color: 1
Size: 278628 Color: 1

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 421302 Color: 0
Size: 306935 Color: 1
Size: 271764 Color: 0

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 421307 Color: 0
Size: 326696 Color: 0
Size: 251998 Color: 1

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 421311 Color: 0
Size: 296703 Color: 1
Size: 281987 Color: 0

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 421433 Color: 1
Size: 303473 Color: 0
Size: 275095 Color: 0

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 421482 Color: 1
Size: 310350 Color: 0
Size: 268169 Color: 1

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 421532 Color: 1
Size: 309853 Color: 1
Size: 268616 Color: 0

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 421506 Color: 0
Size: 304703 Color: 0
Size: 273792 Color: 1

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 421730 Color: 1
Size: 317294 Color: 1
Size: 260977 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 421771 Color: 0
Size: 313626 Color: 1
Size: 264604 Color: 0

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 421817 Color: 0
Size: 307454 Color: 1
Size: 270730 Color: 1

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 421923 Color: 1
Size: 306088 Color: 1
Size: 271990 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 421977 Color: 0
Size: 325188 Color: 1
Size: 252836 Color: 1

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 422029 Color: 0
Size: 289030 Color: 1
Size: 288942 Color: 0

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 422219 Color: 1
Size: 316910 Color: 0
Size: 260872 Color: 1

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 422233 Color: 0
Size: 324298 Color: 0
Size: 253470 Color: 1

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 422386 Color: 0
Size: 297786 Color: 1
Size: 279829 Color: 0

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 422385 Color: 1
Size: 317267 Color: 1
Size: 260349 Color: 0

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 422427 Color: 1
Size: 313481 Color: 0
Size: 264093 Color: 1

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 422473 Color: 1
Size: 321210 Color: 1
Size: 256318 Color: 0

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 422511 Color: 0
Size: 313453 Color: 1
Size: 264037 Color: 0

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 422545 Color: 1
Size: 317747 Color: 0
Size: 259709 Color: 1

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 422667 Color: 0
Size: 293990 Color: 1
Size: 283344 Color: 1

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 422549 Color: 1
Size: 320303 Color: 1
Size: 257149 Color: 0

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 422726 Color: 0
Size: 312881 Color: 1
Size: 264394 Color: 0

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 422631 Color: 1
Size: 308399 Color: 0
Size: 268971 Color: 1

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 422844 Color: 0
Size: 294057 Color: 0
Size: 283100 Color: 1

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 422879 Color: 1
Size: 307627 Color: 0
Size: 269495 Color: 1

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 423012 Color: 0
Size: 312626 Color: 0
Size: 264363 Color: 1

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 422893 Color: 1
Size: 321587 Color: 1
Size: 255521 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 423067 Color: 1
Size: 293343 Color: 0
Size: 283591 Color: 0

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 423069 Color: 1
Size: 318517 Color: 0
Size: 258415 Color: 1

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 423201 Color: 0
Size: 305835 Color: 0
Size: 270965 Color: 1

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 423295 Color: 0
Size: 302460 Color: 1
Size: 274246 Color: 0

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 423203 Color: 1
Size: 300492 Color: 1
Size: 276306 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 423447 Color: 0
Size: 291373 Color: 0
Size: 285181 Color: 1

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 423344 Color: 1
Size: 325014 Color: 1
Size: 251643 Color: 0

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 423769 Color: 1
Size: 289400 Color: 1
Size: 286832 Color: 0

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 423921 Color: 0
Size: 317333 Color: 1
Size: 258747 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 423908 Color: 1
Size: 314627 Color: 1
Size: 261466 Color: 0

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 423955 Color: 1
Size: 298845 Color: 0
Size: 277201 Color: 1

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 423983 Color: 0
Size: 311745 Color: 0
Size: 264273 Color: 1

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 424055 Color: 1
Size: 325674 Color: 1
Size: 250272 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 424021 Color: 0
Size: 314509 Color: 1
Size: 261471 Color: 0

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 424059 Color: 1
Size: 322097 Color: 0
Size: 253845 Color: 1

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 369876 Color: 1
Size: 336289 Color: 0
Size: 293836 Color: 1

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 424069 Color: 1
Size: 298463 Color: 1
Size: 277469 Color: 0

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 424090 Color: 0
Size: 292280 Color: 1
Size: 283631 Color: 0

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 424223 Color: 0
Size: 322517 Color: 1
Size: 253261 Color: 1

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 424242 Color: 0
Size: 316004 Color: 0
Size: 259755 Color: 1

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 424387 Color: 1
Size: 298476 Color: 1
Size: 277138 Color: 0

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 424436 Color: 1
Size: 300231 Color: 0
Size: 275334 Color: 1

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 424566 Color: 1
Size: 315476 Color: 1
Size: 259959 Color: 0

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 424659 Color: 0
Size: 292553 Color: 0
Size: 282789 Color: 1

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 424720 Color: 0
Size: 321071 Color: 1
Size: 254210 Color: 1

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 424801 Color: 0
Size: 289534 Color: 1
Size: 285666 Color: 0

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 424795 Color: 1
Size: 304114 Color: 0
Size: 271092 Color: 1

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 424855 Color: 0
Size: 317930 Color: 1
Size: 257216 Color: 0

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 424901 Color: 1
Size: 316043 Color: 1
Size: 259057 Color: 0

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 424883 Color: 0
Size: 311302 Color: 1
Size: 263816 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 424959 Color: 1
Size: 323717 Color: 1
Size: 251325 Color: 0

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 424998 Color: 1
Size: 297555 Color: 0
Size: 277448 Color: 1

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 425051 Color: 0
Size: 300059 Color: 1
Size: 274891 Color: 0

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 425101 Color: 0
Size: 293521 Color: 1
Size: 281379 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 425140 Color: 0
Size: 301226 Color: 1
Size: 273635 Color: 1

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 425167 Color: 0
Size: 318221 Color: 0
Size: 256613 Color: 1

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 425315 Color: 1
Size: 319386 Color: 1
Size: 255300 Color: 0

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 425259 Color: 0
Size: 303376 Color: 0
Size: 271366 Color: 1

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 425415 Color: 1
Size: 300809 Color: 1
Size: 273777 Color: 0

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 425624 Color: 1
Size: 303251 Color: 0
Size: 271126 Color: 1

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 425636 Color: 1
Size: 319200 Color: 0
Size: 255165 Color: 1

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 425558 Color: 0
Size: 298512 Color: 0
Size: 275931 Color: 1

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 425749 Color: 1
Size: 296856 Color: 1
Size: 277396 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 425757 Color: 1
Size: 314604 Color: 1
Size: 259640 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 425782 Color: 1
Size: 305263 Color: 0
Size: 268956 Color: 1

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 425798 Color: 1
Size: 322132 Color: 1
Size: 252071 Color: 0

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 425805 Color: 0
Size: 296760 Color: 0
Size: 277436 Color: 1

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 425842 Color: 0
Size: 306970 Color: 1
Size: 267189 Color: 1

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 425945 Color: 0
Size: 303630 Color: 1
Size: 270426 Color: 1

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 426031 Color: 0
Size: 309583 Color: 1
Size: 264387 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 425963 Color: 1
Size: 290643 Color: 0
Size: 283395 Color: 1

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 426068 Color: 0
Size: 302614 Color: 1
Size: 271319 Color: 0

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 425983 Color: 1
Size: 319885 Color: 0
Size: 254133 Color: 1

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 426251 Color: 0
Size: 305251 Color: 0
Size: 268499 Color: 1

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 426492 Color: 0
Size: 318140 Color: 0
Size: 255369 Color: 1

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 426509 Color: 1
Size: 301727 Color: 0
Size: 271765 Color: 1

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 426531 Color: 1
Size: 288204 Color: 0
Size: 285266 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 426571 Color: 1
Size: 289679 Color: 1
Size: 283751 Color: 0

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 426591 Color: 0
Size: 289033 Color: 0
Size: 284377 Color: 1

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 426637 Color: 1
Size: 314058 Color: 1
Size: 259306 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 426700 Color: 0
Size: 316922 Color: 1
Size: 256379 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 426655 Color: 1
Size: 289145 Color: 1
Size: 284201 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 426981 Color: 0
Size: 288743 Color: 1
Size: 284277 Color: 0

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 427172 Color: 0
Size: 306842 Color: 1
Size: 265987 Color: 1

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 427577 Color: 0
Size: 308686 Color: 1
Size: 263738 Color: 0

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 427602 Color: 1
Size: 290696 Color: 1
Size: 281703 Color: 0

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 427637 Color: 0
Size: 300421 Color: 1
Size: 271943 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 427719 Color: 1
Size: 310296 Color: 1
Size: 261986 Color: 0

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 427694 Color: 0
Size: 312441 Color: 1
Size: 259866 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 427789 Color: 1
Size: 289499 Color: 1
Size: 282713 Color: 0

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 427820 Color: 0
Size: 319230 Color: 1
Size: 252951 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 427805 Color: 1
Size: 315892 Color: 1
Size: 256304 Color: 0

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 427815 Color: 1
Size: 292252 Color: 1
Size: 279934 Color: 0

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 427907 Color: 1
Size: 293852 Color: 0
Size: 278242 Color: 1

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 427932 Color: 0
Size: 289070 Color: 1
Size: 282999 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 428005 Color: 1
Size: 318881 Color: 1
Size: 253115 Color: 0

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 428156 Color: 1
Size: 306752 Color: 0
Size: 265093 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 428257 Color: 1
Size: 301348 Color: 1
Size: 270396 Color: 0

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 428282 Color: 0
Size: 291419 Color: 0
Size: 280300 Color: 1

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 428384 Color: 0
Size: 291401 Color: 1
Size: 280216 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 428508 Color: 1
Size: 297549 Color: 0
Size: 273944 Color: 1

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 428470 Color: 0
Size: 287269 Color: 0
Size: 284262 Color: 1

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 428533 Color: 1
Size: 296178 Color: 1
Size: 275290 Color: 0

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 428570 Color: 1
Size: 310501 Color: 0
Size: 260930 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 428613 Color: 1
Size: 309031 Color: 0
Size: 262357 Color: 1

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 428685 Color: 1
Size: 304138 Color: 0
Size: 267178 Color: 1

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 428754 Color: 1
Size: 312345 Color: 0
Size: 258902 Color: 0

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 428830 Color: 1
Size: 296469 Color: 0
Size: 274702 Color: 1

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 429049 Color: 1
Size: 304573 Color: 1
Size: 266379 Color: 0

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 429115 Color: 1
Size: 305533 Color: 1
Size: 265353 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 429119 Color: 0
Size: 287897 Color: 1
Size: 282985 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 429165 Color: 1
Size: 285970 Color: 1
Size: 284866 Color: 0

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 429214 Color: 0
Size: 320346 Color: 0
Size: 250441 Color: 1

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 429239 Color: 0
Size: 302727 Color: 1
Size: 268035 Color: 1

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 429248 Color: 1
Size: 311941 Color: 0
Size: 258812 Color: 1

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 429263 Color: 0
Size: 301474 Color: 1
Size: 269264 Color: 0

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 354575 Color: 1
Size: 328688 Color: 1
Size: 316738 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 429320 Color: 0
Size: 311652 Color: 1
Size: 259029 Color: 1

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 429328 Color: 1
Size: 319568 Color: 1
Size: 251105 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 429393 Color: 0
Size: 306326 Color: 1
Size: 264282 Color: 1

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 429374 Color: 1
Size: 304253 Color: 0
Size: 266374 Color: 1

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 429404 Color: 0
Size: 317088 Color: 1
Size: 253509 Color: 0

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 429448 Color: 1
Size: 305869 Color: 1
Size: 264684 Color: 0

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 429717 Color: 1
Size: 314365 Color: 1
Size: 255919 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 429815 Color: 1
Size: 315772 Color: 0
Size: 254414 Color: 0

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 429826 Color: 1
Size: 302588 Color: 0
Size: 267587 Color: 1

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 429897 Color: 1
Size: 293542 Color: 0
Size: 276562 Color: 1

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 429893 Color: 0
Size: 298151 Color: 0
Size: 271957 Color: 1

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 429914 Color: 1
Size: 294331 Color: 0
Size: 275756 Color: 1

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 430070 Color: 1
Size: 313880 Color: 0
Size: 256051 Color: 1

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 430117 Color: 0
Size: 287677 Color: 0
Size: 282207 Color: 1

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 430184 Color: 0
Size: 303585 Color: 1
Size: 266232 Color: 1

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 430223 Color: 0
Size: 303537 Color: 0
Size: 266241 Color: 1

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 430153 Color: 1
Size: 300631 Color: 0
Size: 269217 Color: 1

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 430276 Color: 1
Size: 299577 Color: 0
Size: 270148 Color: 1

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 430460 Color: 0
Size: 298935 Color: 0
Size: 270606 Color: 1

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 430498 Color: 1
Size: 291128 Color: 0
Size: 278375 Color: 1

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 430549 Color: 1
Size: 300986 Color: 0
Size: 268466 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 430732 Color: 1
Size: 295695 Color: 1
Size: 273574 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 430782 Color: 1
Size: 318557 Color: 1
Size: 250662 Color: 0

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 430870 Color: 1
Size: 293030 Color: 0
Size: 276101 Color: 1

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 430872 Color: 0
Size: 289302 Color: 1
Size: 279827 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 430984 Color: 1
Size: 288535 Color: 0
Size: 280482 Color: 1

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 431034 Color: 1
Size: 284858 Color: 0
Size: 284109 Color: 0

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 431090 Color: 1
Size: 302307 Color: 0
Size: 266604 Color: 1

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 431344 Color: 0
Size: 286044 Color: 1
Size: 282613 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 431375 Color: 1
Size: 287007 Color: 0
Size: 281619 Color: 1

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 431616 Color: 0
Size: 289255 Color: 0
Size: 279130 Color: 1

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 431752 Color: 1
Size: 313868 Color: 1
Size: 254381 Color: 0

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 431918 Color: 1
Size: 312370 Color: 0
Size: 255713 Color: 1

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 431850 Color: 0
Size: 299042 Color: 0
Size: 269109 Color: 1

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 431955 Color: 1
Size: 287398 Color: 1
Size: 280648 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 431934 Color: 0
Size: 309752 Color: 0
Size: 258315 Color: 1

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 431966 Color: 1
Size: 284145 Color: 1
Size: 283890 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 431963 Color: 0
Size: 309455 Color: 1
Size: 258583 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 432018 Color: 1
Size: 289869 Color: 0
Size: 278114 Color: 1

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 432078 Color: 1
Size: 295283 Color: 0
Size: 272640 Color: 1

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 432196 Color: 1
Size: 284134 Color: 0
Size: 283671 Color: 0

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 432304 Color: 1
Size: 308676 Color: 1
Size: 259021 Color: 0

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 432274 Color: 0
Size: 313602 Color: 0
Size: 254125 Color: 1

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 432421 Color: 0
Size: 300708 Color: 1
Size: 266872 Color: 1

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 432428 Color: 0
Size: 310395 Color: 0
Size: 257178 Color: 1

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 432412 Color: 1
Size: 296528 Color: 1
Size: 271061 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 432486 Color: 0
Size: 308087 Color: 1
Size: 259428 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 432554 Color: 1
Size: 308057 Color: 0
Size: 259390 Color: 1

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 432658 Color: 0
Size: 314595 Color: 1
Size: 252748 Color: 0

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 432741 Color: 1
Size: 290553 Color: 0
Size: 276707 Color: 1

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 432668 Color: 0
Size: 308289 Color: 1
Size: 259044 Color: 0

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 432799 Color: 1
Size: 303470 Color: 0
Size: 263732 Color: 1

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 432807 Color: 1
Size: 291914 Color: 0
Size: 275280 Color: 0

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 432833 Color: 1
Size: 293197 Color: 1
Size: 273971 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 432804 Color: 0
Size: 300174 Color: 1
Size: 267023 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 432897 Color: 1
Size: 289724 Color: 1
Size: 277380 Color: 0

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 432948 Color: 0
Size: 287665 Color: 1
Size: 279388 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 433004 Color: 1
Size: 292826 Color: 1
Size: 274171 Color: 0

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 433006 Color: 1
Size: 296663 Color: 1
Size: 270332 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 433021 Color: 0
Size: 296251 Color: 1
Size: 270729 Color: 1

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 433186 Color: 0
Size: 312849 Color: 1
Size: 253966 Color: 1

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 433259 Color: 0
Size: 295507 Color: 1
Size: 271235 Color: 1

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 433173 Color: 1
Size: 305966 Color: 0
Size: 260862 Color: 1

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 433395 Color: 1
Size: 312949 Color: 0
Size: 253657 Color: 0

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 433529 Color: 0
Size: 298806 Color: 1
Size: 267666 Color: 1

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 433614 Color: 1
Size: 309628 Color: 0
Size: 256759 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 433655 Color: 1
Size: 283730 Color: 1
Size: 282616 Color: 0

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 433703 Color: 1
Size: 283742 Color: 0
Size: 282556 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 433759 Color: 1
Size: 304512 Color: 0
Size: 261730 Color: 0

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 433775 Color: 1
Size: 291896 Color: 1
Size: 274330 Color: 0

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 433905 Color: 1
Size: 291022 Color: 1
Size: 275074 Color: 0

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 433950 Color: 0
Size: 285753 Color: 0
Size: 280298 Color: 1

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 434075 Color: 1
Size: 292758 Color: 0
Size: 273168 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 434198 Color: 0
Size: 285305 Color: 1
Size: 280498 Color: 0

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 434217 Color: 1
Size: 287122 Color: 1
Size: 278662 Color: 0

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 434318 Color: 0
Size: 296064 Color: 0
Size: 269619 Color: 1

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 434406 Color: 0
Size: 286538 Color: 1
Size: 279057 Color: 1

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 434590 Color: 0
Size: 312364 Color: 1
Size: 253047 Color: 1

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 434718 Color: 1
Size: 288290 Color: 1
Size: 276993 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 434693 Color: 0
Size: 288376 Color: 0
Size: 276932 Color: 1

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 434805 Color: 1
Size: 300761 Color: 0
Size: 264435 Color: 1

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 434829 Color: 1
Size: 304702 Color: 0
Size: 260470 Color: 1

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 434877 Color: 1
Size: 310700 Color: 0
Size: 254424 Color: 1

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 434882 Color: 1
Size: 313312 Color: 0
Size: 251807 Color: 1

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 434921 Color: 0
Size: 307174 Color: 1
Size: 257906 Color: 0

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 434982 Color: 1
Size: 302774 Color: 1
Size: 262245 Color: 0

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 434974 Color: 0
Size: 297509 Color: 1
Size: 267518 Color: 0

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 435060 Color: 1
Size: 284784 Color: 1
Size: 280157 Color: 0

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 435074 Color: 1
Size: 300971 Color: 0
Size: 263956 Color: 0

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 435183 Color: 1
Size: 306089 Color: 0
Size: 258729 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 435286 Color: 0
Size: 312347 Color: 0
Size: 252368 Color: 1

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 435366 Color: 0
Size: 289927 Color: 1
Size: 274708 Color: 1

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 435433 Color: 1
Size: 309115 Color: 1
Size: 255453 Color: 0

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 435418 Color: 0
Size: 308288 Color: 1
Size: 256295 Color: 0

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 435484 Color: 1
Size: 285470 Color: 0
Size: 279047 Color: 1

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 435598 Color: 1
Size: 295394 Color: 0
Size: 269009 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 435843 Color: 1
Size: 290657 Color: 1
Size: 273501 Color: 0

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 435833 Color: 0
Size: 312235 Color: 1
Size: 251933 Color: 0

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 435994 Color: 1
Size: 313921 Color: 0
Size: 250086 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 436023 Color: 0
Size: 284577 Color: 0
Size: 279401 Color: 1

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 436013 Color: 1
Size: 288948 Color: 1
Size: 275040 Color: 0

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 436093 Color: 0
Size: 297429 Color: 0
Size: 266479 Color: 1

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 436213 Color: 1
Size: 291281 Color: 0
Size: 272507 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 436345 Color: 1
Size: 299041 Color: 0
Size: 264615 Color: 0

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 436422 Color: 0
Size: 310166 Color: 0
Size: 253413 Color: 1

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 436457 Color: 0
Size: 298703 Color: 1
Size: 264841 Color: 1

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 436502 Color: 0
Size: 309911 Color: 0
Size: 253588 Color: 1

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 436644 Color: 1
Size: 286098 Color: 1
Size: 277259 Color: 0

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 436685 Color: 1
Size: 298940 Color: 1
Size: 264376 Color: 0

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 436834 Color: 0
Size: 282272 Color: 1
Size: 280895 Color: 0

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 436836 Color: 1
Size: 301070 Color: 1
Size: 262095 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 436857 Color: 0
Size: 311601 Color: 1
Size: 251543 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 436893 Color: 1
Size: 301045 Color: 1
Size: 262063 Color: 0

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 437106 Color: 0
Size: 310183 Color: 1
Size: 252712 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 437075 Color: 1
Size: 296144 Color: 1
Size: 266782 Color: 0

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 437180 Color: 0
Size: 307823 Color: 1
Size: 254998 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 437596 Color: 0
Size: 295238 Color: 1
Size: 267167 Color: 1

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 437641 Color: 1
Size: 287758 Color: 0
Size: 274602 Color: 0

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 437703 Color: 1
Size: 294659 Color: 0
Size: 267639 Color: 1

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 437672 Color: 0
Size: 290298 Color: 1
Size: 272031 Color: 0

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 437854 Color: 1
Size: 303407 Color: 0
Size: 258740 Color: 0

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 437878 Color: 1
Size: 307918 Color: 0
Size: 254205 Color: 1

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 437912 Color: 0
Size: 306135 Color: 1
Size: 255954 Color: 0

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 437975 Color: 0
Size: 299491 Color: 1
Size: 262535 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 437993 Color: 1
Size: 291422 Color: 0
Size: 270586 Color: 1

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 437984 Color: 0
Size: 296834 Color: 0
Size: 265183 Color: 1

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 438035 Color: 1
Size: 297942 Color: 1
Size: 264024 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 438242 Color: 0
Size: 281524 Color: 1
Size: 280235 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 438269 Color: 0
Size: 293064 Color: 1
Size: 268668 Color: 1

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 438406 Color: 1
Size: 284701 Color: 1
Size: 276894 Color: 0

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 438377 Color: 0
Size: 296929 Color: 1
Size: 264695 Color: 0

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 438491 Color: 1
Size: 284164 Color: 1
Size: 277346 Color: 0

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 438606 Color: 0
Size: 300960 Color: 1
Size: 260435 Color: 0

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 438626 Color: 0
Size: 289078 Color: 0
Size: 272297 Color: 1

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 438690 Color: 1
Size: 294757 Color: 0
Size: 266554 Color: 1

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 438778 Color: 0
Size: 283083 Color: 1
Size: 278140 Color: 0

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 438782 Color: 1
Size: 310972 Color: 0
Size: 250247 Color: 1

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 438857 Color: 0
Size: 311087 Color: 1
Size: 250057 Color: 0

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 438992 Color: 0
Size: 310027 Color: 1
Size: 250982 Color: 1

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 439122 Color: 0
Size: 281958 Color: 1
Size: 278921 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 439102 Color: 1
Size: 289050 Color: 0
Size: 271849 Color: 1

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 439224 Color: 0
Size: 307906 Color: 0
Size: 252871 Color: 1

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 439368 Color: 1
Size: 293399 Color: 1
Size: 267234 Color: 0

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 439393 Color: 1
Size: 294568 Color: 0
Size: 266040 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 439409 Color: 1
Size: 282030 Color: 0
Size: 278562 Color: 1

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 439342 Color: 0
Size: 293323 Color: 0
Size: 267336 Color: 1

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 439435 Color: 0
Size: 303333 Color: 0
Size: 257233 Color: 1

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 439545 Color: 1
Size: 308512 Color: 1
Size: 251944 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 439547 Color: 1
Size: 301348 Color: 0
Size: 259106 Color: 0

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 439576 Color: 1
Size: 294597 Color: 0
Size: 265828 Color: 1

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 439604 Color: 1
Size: 301987 Color: 0
Size: 258410 Color: 0

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 439649 Color: 1
Size: 302910 Color: 0
Size: 257442 Color: 1

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 439697 Color: 1
Size: 307997 Color: 0
Size: 252307 Color: 1

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 439723 Color: 1
Size: 308719 Color: 0
Size: 251559 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 439782 Color: 1
Size: 298175 Color: 1
Size: 262044 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 440047 Color: 1
Size: 308899 Color: 1
Size: 251055 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 440135 Color: 1
Size: 294295 Color: 0
Size: 265571 Color: 0

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 440184 Color: 1
Size: 296795 Color: 0
Size: 263022 Color: 1

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 440286 Color: 0
Size: 305849 Color: 1
Size: 253866 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 379836 Color: 0
Size: 351362 Color: 1
Size: 268803 Color: 1

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 440287 Color: 0
Size: 298826 Color: 1
Size: 260888 Color: 0

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 440408 Color: 1
Size: 283703 Color: 1
Size: 275890 Color: 0

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 440410 Color: 1
Size: 300564 Color: 0
Size: 259027 Color: 0

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 440495 Color: 1
Size: 283785 Color: 1
Size: 275721 Color: 0

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 440556 Color: 0
Size: 296795 Color: 0
Size: 262650 Color: 1

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 440840 Color: 0
Size: 293346 Color: 1
Size: 265815 Color: 0

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 440888 Color: 1
Size: 295220 Color: 1
Size: 263893 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 441045 Color: 0
Size: 304667 Color: 1
Size: 254289 Color: 0

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 441211 Color: 0
Size: 293182 Color: 1
Size: 265608 Color: 0

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 441249 Color: 0
Size: 305990 Color: 1
Size: 252762 Color: 1

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 441291 Color: 0
Size: 300440 Color: 1
Size: 258270 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 441268 Color: 1
Size: 291374 Color: 0
Size: 267359 Color: 1

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 441509 Color: 0
Size: 295553 Color: 1
Size: 262939 Color: 0

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 441486 Color: 1
Size: 290335 Color: 0
Size: 268180 Color: 1

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 441649 Color: 0
Size: 307994 Color: 1
Size: 250358 Color: 1

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 441789 Color: 0
Size: 295411 Color: 1
Size: 262801 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 441808 Color: 0
Size: 307040 Color: 1
Size: 251153 Color: 1

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 441925 Color: 1
Size: 293980 Color: 1
Size: 264096 Color: 0

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 442040 Color: 1
Size: 306960 Color: 0
Size: 251001 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 442118 Color: 0
Size: 284183 Color: 0
Size: 273700 Color: 1

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 442198 Color: 1
Size: 290902 Color: 1
Size: 266901 Color: 0

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 442196 Color: 0
Size: 285455 Color: 0
Size: 272350 Color: 1

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 442289 Color: 1
Size: 300079 Color: 0
Size: 257633 Color: 1

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 442247 Color: 0
Size: 290198 Color: 0
Size: 267556 Color: 1

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 442407 Color: 1
Size: 291609 Color: 0
Size: 265985 Color: 1

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 442503 Color: 1
Size: 292453 Color: 0
Size: 265045 Color: 1

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 442523 Color: 0
Size: 288477 Color: 0
Size: 269001 Color: 1

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 442555 Color: 1
Size: 284496 Color: 1
Size: 272950 Color: 0

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 442894 Color: 0
Size: 304709 Color: 0
Size: 252398 Color: 1

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 442946 Color: 1
Size: 306386 Color: 0
Size: 250669 Color: 1

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 443027 Color: 0
Size: 292793 Color: 0
Size: 264181 Color: 1

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 443007 Color: 1
Size: 281310 Color: 1
Size: 275684 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 443206 Color: 1
Size: 284675 Color: 0
Size: 272120 Color: 1

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 443239 Color: 0
Size: 306717 Color: 0
Size: 250045 Color: 1

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 443247 Color: 1
Size: 294882 Color: 1
Size: 261872 Color: 0

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 443284 Color: 0
Size: 306316 Color: 1
Size: 250401 Color: 0

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 443369 Color: 0
Size: 301792 Color: 1
Size: 254840 Color: 1

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 443435 Color: 0
Size: 278731 Color: 1
Size: 277835 Color: 0

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 443465 Color: 0
Size: 296602 Color: 0
Size: 259934 Color: 1

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 437592 Color: 1
Size: 285846 Color: 0
Size: 276563 Color: 1

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 443602 Color: 1
Size: 288124 Color: 1
Size: 268275 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 443623 Color: 1
Size: 281164 Color: 0
Size: 275214 Color: 0

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 443675 Color: 1
Size: 282645 Color: 0
Size: 273681 Color: 1

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 443710 Color: 0
Size: 298822 Color: 1
Size: 257469 Color: 0

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 443848 Color: 1
Size: 283875 Color: 0
Size: 272278 Color: 1

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 443834 Color: 0
Size: 279274 Color: 1
Size: 276893 Color: 0

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 443849 Color: 1
Size: 289959 Color: 1
Size: 266193 Color: 0

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 443930 Color: 0
Size: 291093 Color: 0
Size: 264978 Color: 1

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 443979 Color: 0
Size: 304223 Color: 1
Size: 251799 Color: 1

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 443992 Color: 0
Size: 295569 Color: 0
Size: 260440 Color: 1

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 444042 Color: 1
Size: 301132 Color: 1
Size: 254827 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 444184 Color: 1
Size: 298364 Color: 0
Size: 257453 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 444381 Color: 1
Size: 294603 Color: 1
Size: 261017 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 444458 Color: 1
Size: 291804 Color: 0
Size: 263739 Color: 1

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 444460 Color: 1
Size: 278850 Color: 1
Size: 276691 Color: 0

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 444478 Color: 1
Size: 301064 Color: 0
Size: 254459 Color: 0

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 444521 Color: 1
Size: 290495 Color: 1
Size: 264985 Color: 0

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 444625 Color: 0
Size: 281122 Color: 0
Size: 274254 Color: 1

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 444560 Color: 1
Size: 287464 Color: 1
Size: 267977 Color: 0

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 444612 Color: 1
Size: 278836 Color: 0
Size: 276553 Color: 1

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 444793 Color: 0
Size: 284884 Color: 0
Size: 270324 Color: 1

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 444804 Color: 1
Size: 299299 Color: 1
Size: 255898 Color: 0

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 444843 Color: 1
Size: 283361 Color: 0
Size: 271797 Color: 0

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 444860 Color: 1
Size: 303658 Color: 0
Size: 251483 Color: 1

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 444891 Color: 1
Size: 293363 Color: 0
Size: 261747 Color: 1

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 444950 Color: 0
Size: 283786 Color: 1
Size: 271265 Color: 0

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 444973 Color: 1
Size: 279751 Color: 0
Size: 275277 Color: 1

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 445008 Color: 0
Size: 297635 Color: 1
Size: 257358 Color: 0

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 445013 Color: 0
Size: 298616 Color: 0
Size: 256372 Color: 1

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 445255 Color: 0
Size: 304742 Color: 1
Size: 250004 Color: 0

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 445286 Color: 0
Size: 284960 Color: 1
Size: 269755 Color: 1

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 445290 Color: 0
Size: 302333 Color: 0
Size: 252378 Color: 1

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 445255 Color: 1
Size: 291194 Color: 0
Size: 263552 Color: 1

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 445449 Color: 0
Size: 304483 Color: 1
Size: 250069 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 445406 Color: 1
Size: 287952 Color: 0
Size: 266643 Color: 1

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 445472 Color: 0
Size: 302898 Color: 1
Size: 251631 Color: 0

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 445508 Color: 0
Size: 287788 Color: 1
Size: 266705 Color: 1

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 445513 Color: 0
Size: 287096 Color: 0
Size: 267392 Color: 1

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 445602 Color: 1
Size: 298930 Color: 0
Size: 255469 Color: 0

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 445619 Color: 1
Size: 301325 Color: 1
Size: 253057 Color: 0

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 445596 Color: 0
Size: 284601 Color: 0
Size: 269804 Color: 1

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 445744 Color: 1
Size: 296712 Color: 1
Size: 257545 Color: 0

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 445717 Color: 0
Size: 300001 Color: 1
Size: 254283 Color: 0

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 445837 Color: 0
Size: 280591 Color: 1
Size: 273573 Color: 0

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 445989 Color: 1
Size: 283487 Color: 0
Size: 270525 Color: 1

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 446119 Color: 0
Size: 289267 Color: 1
Size: 264615 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 446253 Color: 1
Size: 279106 Color: 1
Size: 274642 Color: 0

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 446226 Color: 0
Size: 291668 Color: 0
Size: 262107 Color: 1

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 446450 Color: 0
Size: 301329 Color: 1
Size: 252222 Color: 0

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 446614 Color: 0
Size: 279285 Color: 0
Size: 274102 Color: 1

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 446591 Color: 1
Size: 279779 Color: 1
Size: 273631 Color: 0

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 446741 Color: 1
Size: 285502 Color: 1
Size: 267758 Color: 0

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 446778 Color: 1
Size: 290184 Color: 0
Size: 263039 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 446787 Color: 1
Size: 303090 Color: 1
Size: 250124 Color: 0

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 446702 Color: 0
Size: 284096 Color: 1
Size: 269203 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 446800 Color: 1
Size: 285633 Color: 0
Size: 267568 Color: 1

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 446919 Color: 0
Size: 299608 Color: 0
Size: 253474 Color: 1

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 447027 Color: 1
Size: 283898 Color: 1
Size: 269076 Color: 0

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 447003 Color: 0
Size: 297307 Color: 1
Size: 255691 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 447202 Color: 1
Size: 283550 Color: 0
Size: 269249 Color: 1

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 447311 Color: 0
Size: 282894 Color: 0
Size: 269796 Color: 1

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 447323 Color: 0
Size: 295676 Color: 1
Size: 257002 Color: 1

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 447562 Color: 1
Size: 290187 Color: 0
Size: 262252 Color: 1

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 447465 Color: 0
Size: 294347 Color: 0
Size: 258189 Color: 1

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 447566 Color: 1
Size: 295247 Color: 1
Size: 257188 Color: 0

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 447691 Color: 1
Size: 287626 Color: 0
Size: 264684 Color: 0

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 447727 Color: 1
Size: 286456 Color: 0
Size: 265818 Color: 1

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 447740 Color: 1
Size: 296115 Color: 0
Size: 256146 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 447841 Color: 0
Size: 300213 Color: 1
Size: 251947 Color: 1

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 447953 Color: 0
Size: 301139 Color: 0
Size: 250909 Color: 1

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 447935 Color: 1
Size: 291479 Color: 1
Size: 260587 Color: 0

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 447991 Color: 1
Size: 300223 Color: 1
Size: 251787 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 448151 Color: 1
Size: 287026 Color: 0
Size: 264824 Color: 1

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 448223 Color: 0
Size: 289452 Color: 0
Size: 262326 Color: 1

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 448153 Color: 1
Size: 276033 Color: 0
Size: 275815 Color: 1

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 448219 Color: 1
Size: 288084 Color: 1
Size: 263698 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 448425 Color: 0
Size: 285058 Color: 0
Size: 266518 Color: 1

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 448428 Color: 1
Size: 293551 Color: 1
Size: 258022 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 448470 Color: 0
Size: 286839 Color: 0
Size: 264692 Color: 1

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 448555 Color: 1
Size: 287027 Color: 0
Size: 264419 Color: 1

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 448703 Color: 1
Size: 281180 Color: 0
Size: 270118 Color: 0

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 448753 Color: 0
Size: 279995 Color: 0
Size: 271253 Color: 1

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 448832 Color: 1
Size: 285887 Color: 0
Size: 265282 Color: 1

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 448996 Color: 1
Size: 297025 Color: 1
Size: 253980 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 449042 Color: 1
Size: 278441 Color: 0
Size: 272518 Color: 0

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 449444 Color: 0
Size: 293673 Color: 1
Size: 256884 Color: 0

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 449455 Color: 1
Size: 287451 Color: 1
Size: 263095 Color: 0

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 449612 Color: 0
Size: 292460 Color: 1
Size: 257929 Color: 0

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 449621 Color: 1
Size: 281889 Color: 1
Size: 268491 Color: 0

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 449738 Color: 1
Size: 293669 Color: 0
Size: 256594 Color: 1

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 450008 Color: 1
Size: 286812 Color: 1
Size: 263181 Color: 0

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 449959 Color: 0
Size: 279477 Color: 1
Size: 270565 Color: 0

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 450314 Color: 1
Size: 299574 Color: 1
Size: 250113 Color: 0

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 450377 Color: 0
Size: 299134 Color: 0
Size: 250490 Color: 1

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 450337 Color: 1
Size: 276661 Color: 1
Size: 273003 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 450419 Color: 0
Size: 278985 Color: 0
Size: 270597 Color: 1

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 450493 Color: 0
Size: 296157 Color: 0
Size: 253351 Color: 1

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 450501 Color: 0
Size: 274889 Color: 1
Size: 274611 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 450504 Color: 1
Size: 293648 Color: 0
Size: 255849 Color: 1

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 450613 Color: 1
Size: 294403 Color: 0
Size: 254985 Color: 1

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 450643 Color: 1
Size: 285111 Color: 0
Size: 264247 Color: 0

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 450661 Color: 1
Size: 290424 Color: 0
Size: 258916 Color: 1

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 450608 Color: 0
Size: 292146 Color: 0
Size: 257247 Color: 1

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 450672 Color: 1
Size: 276264 Color: 0
Size: 273065 Color: 1

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 450664 Color: 0
Size: 278941 Color: 0
Size: 270396 Color: 1

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 450725 Color: 0
Size: 296352 Color: 1
Size: 252924 Color: 1

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 450763 Color: 0
Size: 280585 Color: 0
Size: 268653 Color: 1

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 450749 Color: 1
Size: 292228 Color: 1
Size: 257024 Color: 0

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 451122 Color: 1
Size: 296267 Color: 1
Size: 252612 Color: 0

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 451115 Color: 0
Size: 274458 Color: 1
Size: 274428 Color: 0

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 451164 Color: 1
Size: 285548 Color: 1
Size: 263289 Color: 0

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 451358 Color: 0
Size: 277286 Color: 1
Size: 271357 Color: 1

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 451558 Color: 0
Size: 296348 Color: 0
Size: 252095 Color: 1

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 451679 Color: 1
Size: 276426 Color: 0
Size: 271896 Color: 1

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 451752 Color: 0
Size: 274966 Color: 0
Size: 273283 Color: 1

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 451798 Color: 1
Size: 291921 Color: 1
Size: 256282 Color: 0

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 451793 Color: 0
Size: 287631 Color: 1
Size: 260577 Color: 0

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 451983 Color: 1
Size: 281662 Color: 0
Size: 266356 Color: 1

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 451986 Color: 1
Size: 294557 Color: 1
Size: 253458 Color: 0

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 451989 Color: 1
Size: 285337 Color: 0
Size: 262675 Color: 0

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 451998 Color: 1
Size: 292577 Color: 0
Size: 255426 Color: 1

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 452044 Color: 1
Size: 286862 Color: 0
Size: 261095 Color: 0

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 452086 Color: 1
Size: 290012 Color: 1
Size: 257903 Color: 0

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 452099 Color: 1
Size: 293299 Color: 1
Size: 254603 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 452149 Color: 0
Size: 290631 Color: 1
Size: 257221 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 452232 Color: 0
Size: 292817 Color: 1
Size: 254952 Color: 1

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 452243 Color: 0
Size: 292320 Color: 1
Size: 255438 Color: 1

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 452335 Color: 0
Size: 288504 Color: 1
Size: 259162 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 452357 Color: 0
Size: 290092 Color: 1
Size: 257552 Color: 1

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 452419 Color: 0
Size: 283119 Color: 1
Size: 264463 Color: 0

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 452460 Color: 1
Size: 279189 Color: 0
Size: 268352 Color: 1

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 452555 Color: 0
Size: 280965 Color: 1
Size: 266481 Color: 0

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 452478 Color: 1
Size: 283449 Color: 1
Size: 264074 Color: 0

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 452709 Color: 0
Size: 291200 Color: 1
Size: 256092 Color: 0

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 452765 Color: 0
Size: 283411 Color: 0
Size: 263825 Color: 1

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 452849 Color: 1
Size: 286687 Color: 1
Size: 260465 Color: 0

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 452950 Color: 0
Size: 274765 Color: 1
Size: 272286 Color: 1

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 453053 Color: 0
Size: 293880 Color: 0
Size: 253068 Color: 1

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 453090 Color: 1
Size: 274254 Color: 1
Size: 272657 Color: 0

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 453180 Color: 0
Size: 286703 Color: 1
Size: 260118 Color: 0

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 453215 Color: 0
Size: 284439 Color: 1
Size: 262347 Color: 1

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 453314 Color: 1
Size: 293528 Color: 1
Size: 253159 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 453401 Color: 1
Size: 290493 Color: 0
Size: 256107 Color: 0

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 453804 Color: 1
Size: 286678 Color: 0
Size: 259519 Color: 0

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 453868 Color: 1
Size: 280909 Color: 1
Size: 265224 Color: 0

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 453901 Color: 0
Size: 285520 Color: 1
Size: 260580 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 454168 Color: 0
Size: 284184 Color: 1
Size: 261649 Color: 0

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 454262 Color: 1
Size: 283079 Color: 0
Size: 262660 Color: 0

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 454341 Color: 0
Size: 291570 Color: 0
Size: 254090 Color: 1

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 454399 Color: 1
Size: 277885 Color: 0
Size: 267717 Color: 1

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 454467 Color: 0
Size: 289292 Color: 1
Size: 256242 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 454544 Color: 1
Size: 278734 Color: 1
Size: 266723 Color: 0

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 454593 Color: 0
Size: 277159 Color: 1
Size: 268249 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 454593 Color: 0
Size: 290957 Color: 1
Size: 254451 Color: 1

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 454673 Color: 1
Size: 289375 Color: 0
Size: 255953 Color: 1

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 454789 Color: 1
Size: 288956 Color: 0
Size: 256256 Color: 1

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 454999 Color: 0
Size: 273539 Color: 0
Size: 271463 Color: 1

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 455044 Color: 1
Size: 294019 Color: 1
Size: 250938 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 455113 Color: 1
Size: 292734 Color: 1
Size: 252154 Color: 0

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 455236 Color: 1
Size: 278204 Color: 0
Size: 266561 Color: 1

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 455226 Color: 0
Size: 286890 Color: 0
Size: 257885 Color: 1

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 455330 Color: 1
Size: 274187 Color: 0
Size: 270484 Color: 1

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 455322 Color: 0
Size: 284817 Color: 0
Size: 259862 Color: 1

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 455358 Color: 1
Size: 272371 Color: 1
Size: 272272 Color: 0

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 455463 Color: 1
Size: 280896 Color: 0
Size: 263642 Color: 1

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 455424 Color: 0
Size: 285377 Color: 1
Size: 259200 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 455471 Color: 1
Size: 292442 Color: 0
Size: 252088 Color: 1

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 455449 Color: 0
Size: 277883 Color: 1
Size: 266669 Color: 1

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 455481 Color: 1
Size: 291330 Color: 0
Size: 253190 Color: 1

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 455452 Color: 0
Size: 292136 Color: 1
Size: 252413 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 455590 Color: 1
Size: 284344 Color: 0
Size: 260067 Color: 0

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 455790 Color: 1
Size: 282143 Color: 0
Size: 262068 Color: 0

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 455934 Color: 0
Size: 289073 Color: 1
Size: 254994 Color: 1

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 456141 Color: 0
Size: 278201 Color: 0
Size: 265659 Color: 1

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 456242 Color: 0
Size: 288115 Color: 1
Size: 255644 Color: 1

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 456261 Color: 0
Size: 287433 Color: 1
Size: 256307 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 456489 Color: 0
Size: 280609 Color: 0
Size: 262903 Color: 1

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 456589 Color: 1
Size: 276743 Color: 0
Size: 266669 Color: 0

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 456642 Color: 1
Size: 281008 Color: 0
Size: 262351 Color: 1

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 456695 Color: 0
Size: 280507 Color: 1
Size: 262799 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 456670 Color: 1
Size: 277647 Color: 0
Size: 265684 Color: 1

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 456694 Color: 1
Size: 283183 Color: 0
Size: 260124 Color: 1

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 456747 Color: 0
Size: 279751 Color: 0
Size: 263503 Color: 1

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 456770 Color: 1
Size: 283540 Color: 1
Size: 259691 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 456802 Color: 1
Size: 282682 Color: 0
Size: 260517 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 456842 Color: 1
Size: 292548 Color: 1
Size: 250611 Color: 0

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 457228 Color: 0
Size: 288926 Color: 0
Size: 253847 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 457198 Color: 1
Size: 271608 Color: 1
Size: 271195 Color: 0

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 457273 Color: 0
Size: 291288 Color: 1
Size: 251440 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 457320 Color: 0
Size: 289657 Color: 1
Size: 253024 Color: 1

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 457417 Color: 0
Size: 288459 Color: 0
Size: 254125 Color: 1

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 457431 Color: 0
Size: 285845 Color: 1
Size: 256725 Color: 1

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 457451 Color: 0
Size: 291130 Color: 1
Size: 251420 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 457534 Color: 1
Size: 292172 Color: 1
Size: 250295 Color: 0

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 457702 Color: 1
Size: 289103 Color: 0
Size: 253196 Color: 1

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 457739 Color: 1
Size: 271960 Color: 0
Size: 270302 Color: 0

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 457897 Color: 0
Size: 286617 Color: 1
Size: 255487 Color: 1

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 457935 Color: 0
Size: 291811 Color: 1
Size: 250255 Color: 0

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 458004 Color: 0
Size: 279786 Color: 1
Size: 262211 Color: 1

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 458039 Color: 0
Size: 276035 Color: 1
Size: 265927 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 458122 Color: 1
Size: 284251 Color: 0
Size: 257628 Color: 0

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 458330 Color: 1
Size: 289193 Color: 1
Size: 252478 Color: 0

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 458442 Color: 1
Size: 275970 Color: 1
Size: 265589 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 458449 Color: 1
Size: 271085 Color: 0
Size: 270467 Color: 0

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 458657 Color: 1
Size: 274178 Color: 0
Size: 267166 Color: 0

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 458671 Color: 1
Size: 286157 Color: 0
Size: 255173 Color: 1

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 458749 Color: 0
Size: 284010 Color: 0
Size: 257242 Color: 1

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 458770 Color: 0
Size: 287076 Color: 1
Size: 254155 Color: 1

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 459058 Color: 1
Size: 279985 Color: 0
Size: 260958 Color: 1

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 459087 Color: 1
Size: 274845 Color: 1
Size: 266069 Color: 0

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 459319 Color: 0
Size: 273104 Color: 0
Size: 267578 Color: 1

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 459416 Color: 0
Size: 274442 Color: 1
Size: 266143 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 459533 Color: 0
Size: 288289 Color: 1
Size: 252179 Color: 0

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 459604 Color: 1
Size: 286372 Color: 1
Size: 254025 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 459552 Color: 0
Size: 272532 Color: 1
Size: 267917 Color: 0

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 459626 Color: 0
Size: 277433 Color: 1
Size: 262942 Color: 1

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 459834 Color: 0
Size: 286480 Color: 1
Size: 253687 Color: 0

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 459844 Color: 1
Size: 284226 Color: 0
Size: 255931 Color: 1

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 459985 Color: 1
Size: 289857 Color: 1
Size: 250159 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 459940 Color: 0
Size: 281561 Color: 1
Size: 258500 Color: 0

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 460001 Color: 1
Size: 280335 Color: 0
Size: 259665 Color: 1

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 460546 Color: 1
Size: 280340 Color: 0
Size: 259115 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 460670 Color: 1
Size: 283598 Color: 1
Size: 255733 Color: 0

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 460880 Color: 1
Size: 283778 Color: 0
Size: 255343 Color: 1

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 460933 Color: 0
Size: 284487 Color: 1
Size: 254581 Color: 0

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 460932 Color: 1
Size: 284195 Color: 0
Size: 254874 Color: 1

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 460938 Color: 0
Size: 281884 Color: 0
Size: 257179 Color: 1

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 461194 Color: 1
Size: 286152 Color: 0
Size: 252655 Color: 1

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 461246 Color: 1
Size: 273489 Color: 0
Size: 265266 Color: 0

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 461282 Color: 1
Size: 286910 Color: 0
Size: 251809 Color: 1

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 461309 Color: 1
Size: 279486 Color: 1
Size: 259206 Color: 0

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 461329 Color: 1
Size: 288002 Color: 1
Size: 250670 Color: 0

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 461358 Color: 0
Size: 279117 Color: 1
Size: 259526 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 461432 Color: 0
Size: 286618 Color: 1
Size: 251951 Color: 1

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 461528 Color: 1
Size: 275498 Color: 1
Size: 262975 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 461591 Color: 1
Size: 277374 Color: 1
Size: 261036 Color: 0

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 461704 Color: 1
Size: 287480 Color: 1
Size: 250817 Color: 0

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 461729 Color: 0
Size: 286139 Color: 1
Size: 252133 Color: 0

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 462148 Color: 0
Size: 279048 Color: 1
Size: 258805 Color: 1

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 462258 Color: 1
Size: 279236 Color: 0
Size: 258507 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 462386 Color: 0
Size: 277818 Color: 1
Size: 259797 Color: 1

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 462570 Color: 0
Size: 281885 Color: 1
Size: 255546 Color: 1

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 462640 Color: 1
Size: 268969 Color: 0
Size: 268392 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 462778 Color: 0
Size: 282058 Color: 1
Size: 255165 Color: 1

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 462826 Color: 1
Size: 282275 Color: 0
Size: 254900 Color: 1

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 462926 Color: 1
Size: 284806 Color: 0
Size: 252269 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 462968 Color: 1
Size: 269386 Color: 1
Size: 267647 Color: 0

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 462936 Color: 0
Size: 273089 Color: 1
Size: 263976 Color: 0

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 463040 Color: 1
Size: 280798 Color: 1
Size: 256163 Color: 0

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 463017 Color: 0
Size: 279952 Color: 0
Size: 257032 Color: 1

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 463193 Color: 1
Size: 282761 Color: 1
Size: 254047 Color: 0

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 463206 Color: 1
Size: 268714 Color: 0
Size: 268081 Color: 0

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 463249 Color: 1
Size: 269456 Color: 1
Size: 267296 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 463228 Color: 0
Size: 280372 Color: 0
Size: 256401 Color: 1

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 463348 Color: 1
Size: 278644 Color: 1
Size: 258009 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 463404 Color: 1
Size: 284453 Color: 0
Size: 252144 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 463525 Color: 1
Size: 278315 Color: 0
Size: 258161 Color: 1

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 463643 Color: 1
Size: 275212 Color: 0
Size: 261146 Color: 1

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 463736 Color: 1
Size: 285160 Color: 1
Size: 251105 Color: 0

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 463717 Color: 0
Size: 274881 Color: 1
Size: 261403 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 463869 Color: 1
Size: 283226 Color: 0
Size: 252906 Color: 1

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 463944 Color: 1
Size: 272490 Color: 0
Size: 263567 Color: 0

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 464062 Color: 1
Size: 279506 Color: 0
Size: 256433 Color: 1

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 464066 Color: 0
Size: 285629 Color: 1
Size: 250306 Color: 0

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 464238 Color: 0
Size: 279132 Color: 1
Size: 256631 Color: 1

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 464392 Color: 1
Size: 280937 Color: 0
Size: 254672 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 464407 Color: 1
Size: 283895 Color: 1
Size: 251699 Color: 0

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 464469 Color: 0
Size: 275754 Color: 0
Size: 259778 Color: 1

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 464482 Color: 0
Size: 270238 Color: 1
Size: 265281 Color: 1

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 464506 Color: 0
Size: 282531 Color: 1
Size: 252964 Color: 0

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 464577 Color: 1
Size: 272843 Color: 1
Size: 262581 Color: 0

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 464617 Color: 0
Size: 276282 Color: 1
Size: 259102 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 464669 Color: 1
Size: 271084 Color: 0
Size: 264248 Color: 1

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 464670 Color: 0
Size: 282790 Color: 1
Size: 252541 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 464742 Color: 1
Size: 284165 Color: 0
Size: 251094 Color: 1

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 464734 Color: 0
Size: 272247 Color: 0
Size: 263020 Color: 1

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 464750 Color: 0
Size: 278158 Color: 1
Size: 257093 Color: 1

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 464837 Color: 1
Size: 283131 Color: 1
Size: 252033 Color: 0

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 465062 Color: 0
Size: 272393 Color: 1
Size: 262546 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 465086 Color: 0
Size: 278109 Color: 1
Size: 256806 Color: 0

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 465086 Color: 0
Size: 271363 Color: 1
Size: 263552 Color: 1

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 465094 Color: 0
Size: 282250 Color: 1
Size: 252657 Color: 0

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 465043 Color: 1
Size: 284062 Color: 1
Size: 250896 Color: 0

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 465226 Color: 0
Size: 279895 Color: 1
Size: 254880 Color: 1

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 465296 Color: 0
Size: 284695 Color: 1
Size: 250010 Color: 1

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 465521 Color: 0
Size: 283733 Color: 0
Size: 250747 Color: 1

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 465682 Color: 0
Size: 270610 Color: 1
Size: 263709 Color: 0

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 465866 Color: 1
Size: 274272 Color: 1
Size: 259863 Color: 0

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 465998 Color: 0
Size: 268670 Color: 0
Size: 265333 Color: 1

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 466027 Color: 0
Size: 282447 Color: 1
Size: 251527 Color: 1

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 466117 Color: 0
Size: 278880 Color: 0
Size: 255004 Color: 1

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 466232 Color: 1
Size: 272991 Color: 1
Size: 260778 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 466482 Color: 0
Size: 273766 Color: 1
Size: 259753 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 466496 Color: 1
Size: 280974 Color: 0
Size: 252531 Color: 1

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 466621 Color: 0
Size: 282038 Color: 0
Size: 251342 Color: 1

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 466639 Color: 0
Size: 281378 Color: 0
Size: 251984 Color: 1

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 466589 Color: 1
Size: 282575 Color: 1
Size: 250837 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 466621 Color: 1
Size: 274646 Color: 0
Size: 258734 Color: 1

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 466712 Color: 0
Size: 276712 Color: 1
Size: 256577 Color: 0

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 466730 Color: 0
Size: 281550 Color: 1
Size: 251721 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 466789 Color: 1
Size: 269686 Color: 0
Size: 263526 Color: 1

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 466801 Color: 0
Size: 273610 Color: 0
Size: 259590 Color: 1

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 467034 Color: 0
Size: 276611 Color: 1
Size: 256356 Color: 0

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 467165 Color: 0
Size: 266696 Color: 0
Size: 266140 Color: 1

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 467390 Color: 0
Size: 278735 Color: 0
Size: 253876 Color: 1

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 467400 Color: 0
Size: 280891 Color: 1
Size: 251710 Color: 1

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 467508 Color: 0
Size: 268860 Color: 0
Size: 263633 Color: 1

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 467665 Color: 0
Size: 281302 Color: 1
Size: 251034 Color: 1

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 467681 Color: 0
Size: 273650 Color: 0
Size: 258670 Color: 1

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 467710 Color: 0
Size: 269168 Color: 1
Size: 263123 Color: 1

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 467867 Color: 0
Size: 266792 Color: 1
Size: 265342 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 467967 Color: 0
Size: 281762 Color: 1
Size: 250272 Color: 0

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 467940 Color: 1
Size: 281223 Color: 1
Size: 250838 Color: 0

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 468098 Color: 1
Size: 274682 Color: 0
Size: 257221 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 468150 Color: 1
Size: 273849 Color: 0
Size: 258002 Color: 1

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 468355 Color: 0
Size: 278210 Color: 1
Size: 253436 Color: 1

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 468387 Color: 0
Size: 267347 Color: 0
Size: 264267 Color: 1

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 468454 Color: 1
Size: 271669 Color: 1
Size: 259878 Color: 0

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 468430 Color: 0
Size: 266296 Color: 0
Size: 265275 Color: 1

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 468494 Color: 1
Size: 269747 Color: 1
Size: 261760 Color: 0

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 468472 Color: 0
Size: 272910 Color: 0
Size: 258619 Color: 1

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 468579 Color: 0
Size: 270962 Color: 1
Size: 260460 Color: 1

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 468717 Color: 1
Size: 278951 Color: 0
Size: 252333 Color: 0

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 468935 Color: 1
Size: 273318 Color: 0
Size: 257748 Color: 1

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 468873 Color: 0
Size: 280968 Color: 0
Size: 250160 Color: 1

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 468944 Color: 1
Size: 278819 Color: 1
Size: 252238 Color: 0

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 468984 Color: 0
Size: 274320 Color: 0
Size: 256697 Color: 1

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 468997 Color: 0
Size: 266740 Color: 1
Size: 264264 Color: 0

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 469033 Color: 1
Size: 271604 Color: 0
Size: 259364 Color: 1

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 469005 Color: 0
Size: 271525 Color: 1
Size: 259471 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 469228 Color: 1
Size: 280602 Color: 0
Size: 250171 Color: 0

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 469235 Color: 1
Size: 266719 Color: 0
Size: 264047 Color: 1

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 469239 Color: 0
Size: 268985 Color: 0
Size: 261777 Color: 1

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 469351 Color: 0
Size: 269630 Color: 1
Size: 261020 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 469400 Color: 0
Size: 273968 Color: 1
Size: 256633 Color: 1

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 469328 Color: 1
Size: 273235 Color: 1
Size: 257438 Color: 0

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 469541 Color: 0
Size: 271353 Color: 0
Size: 259107 Color: 1

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 469624 Color: 0
Size: 274467 Color: 1
Size: 255910 Color: 1

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 469707 Color: 0
Size: 276300 Color: 1
Size: 253994 Color: 0

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 469663 Color: 1
Size: 272147 Color: 0
Size: 258191 Color: 1

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 469803 Color: 0
Size: 273853 Color: 1
Size: 256345 Color: 0

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 470015 Color: 0
Size: 265579 Color: 0
Size: 264407 Color: 1

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 469942 Color: 1
Size: 265508 Color: 0
Size: 264551 Color: 1

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 470079 Color: 0
Size: 266110 Color: 0
Size: 263812 Color: 1

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 470184 Color: 1
Size: 276277 Color: 0
Size: 253540 Color: 1

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 470169 Color: 0
Size: 269207 Color: 0
Size: 260625 Color: 1

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 470250 Color: 0
Size: 268948 Color: 1
Size: 260803 Color: 0

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 470288 Color: 1
Size: 274236 Color: 0
Size: 255477 Color: 1

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 470283 Color: 0
Size: 276723 Color: 1
Size: 252995 Color: 0

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 470394 Color: 0
Size: 265867 Color: 1
Size: 263740 Color: 1

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 470408 Color: 0
Size: 269282 Color: 1
Size: 260311 Color: 0

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 470413 Color: 0
Size: 273467 Color: 1
Size: 256121 Color: 1

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 470443 Color: 0
Size: 279001 Color: 1
Size: 250557 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 470514 Color: 0
Size: 276259 Color: 1
Size: 253228 Color: 1

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 470611 Color: 1
Size: 265228 Color: 0
Size: 264162 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 470593 Color: 0
Size: 276003 Color: 1
Size: 253405 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 470758 Color: 0
Size: 270444 Color: 1
Size: 258799 Color: 0

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 470846 Color: 0
Size: 269803 Color: 0
Size: 259352 Color: 1

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 470824 Color: 1
Size: 275448 Color: 0
Size: 253729 Color: 1

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 470864 Color: 0
Size: 270823 Color: 0
Size: 258314 Color: 1

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 470893 Color: 0
Size: 272599 Color: 1
Size: 256509 Color: 1

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 471002 Color: 0
Size: 266380 Color: 1
Size: 262619 Color: 0

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 471039 Color: 1
Size: 265745 Color: 1
Size: 263217 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 471018 Color: 0
Size: 272085 Color: 1
Size: 256898 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 471129 Color: 0
Size: 271045 Color: 1
Size: 257827 Color: 1

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 471175 Color: 0
Size: 273481 Color: 1
Size: 255345 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 471246 Color: 1
Size: 267226 Color: 0
Size: 261529 Color: 1

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 471295 Color: 1
Size: 266141 Color: 0
Size: 262565 Color: 0

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 471362 Color: 0
Size: 264452 Color: 1
Size: 264187 Color: 1

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 471381 Color: 0
Size: 277579 Color: 1
Size: 251041 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 471544 Color: 1
Size: 270143 Color: 0
Size: 258314 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 471595 Color: 0
Size: 264342 Color: 1
Size: 264064 Color: 1

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 471725 Color: 1
Size: 275817 Color: 0
Size: 252459 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 471795 Color: 1
Size: 274101 Color: 1
Size: 254105 Color: 0

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 471913 Color: 1
Size: 269670 Color: 1
Size: 258418 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 471993 Color: 1
Size: 277139 Color: 1
Size: 250869 Color: 0

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 472031 Color: 0
Size: 267637 Color: 1
Size: 260333 Color: 0

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 472047 Color: 1
Size: 265839 Color: 0
Size: 262115 Color: 1

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 472143 Color: 1
Size: 273718 Color: 0
Size: 254140 Color: 0

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 472287 Color: 1
Size: 275791 Color: 1
Size: 251923 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 472321 Color: 0
Size: 269936 Color: 0
Size: 257744 Color: 1

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 472488 Color: 1
Size: 271989 Color: 1
Size: 255524 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 472599 Color: 0
Size: 275676 Color: 0
Size: 251726 Color: 1

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 472634 Color: 0
Size: 276210 Color: 0
Size: 251157 Color: 1

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 472671 Color: 1
Size: 271376 Color: 0
Size: 255954 Color: 0

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 472818 Color: 0
Size: 271229 Color: 1
Size: 255954 Color: 1

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 472873 Color: 1
Size: 269017 Color: 1
Size: 258111 Color: 0

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 473050 Color: 1
Size: 266054 Color: 0
Size: 260897 Color: 0

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 473173 Color: 1
Size: 265315 Color: 0
Size: 261513 Color: 0

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 473176 Color: 1
Size: 273763 Color: 0
Size: 253062 Color: 1

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 473273 Color: 0
Size: 271932 Color: 1
Size: 254796 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 473382 Color: 1
Size: 272476 Color: 0
Size: 254143 Color: 1

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 473589 Color: 0
Size: 268106 Color: 0
Size: 258306 Color: 1

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 473696 Color: 1
Size: 270866 Color: 1
Size: 255439 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 473764 Color: 0
Size: 273945 Color: 0
Size: 252292 Color: 1

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 473910 Color: 0
Size: 276085 Color: 1
Size: 250006 Color: 1

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 473905 Color: 1
Size: 272668 Color: 1
Size: 253428 Color: 0

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 473963 Color: 0
Size: 269363 Color: 1
Size: 256675 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 473984 Color: 1
Size: 272164 Color: 1
Size: 253853 Color: 0

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 473992 Color: 1
Size: 270491 Color: 0
Size: 255518 Color: 0

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 474011 Color: 1
Size: 273571 Color: 1
Size: 252419 Color: 0

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 474103 Color: 1
Size: 275274 Color: 1
Size: 250624 Color: 0

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 474160 Color: 1
Size: 265132 Color: 0
Size: 260709 Color: 0

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 474217 Color: 1
Size: 263537 Color: 0
Size: 262247 Color: 1

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 474372 Color: 1
Size: 264982 Color: 1
Size: 260647 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 474513 Color: 0
Size: 264755 Color: 1
Size: 260733 Color: 0

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 474516 Color: 0
Size: 274163 Color: 1
Size: 251322 Color: 1

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 474582 Color: 1
Size: 264719 Color: 0
Size: 260700 Color: 0

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 474720 Color: 1
Size: 272205 Color: 1
Size: 253076 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 474736 Color: 0
Size: 271499 Color: 0
Size: 253766 Color: 1

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 474849 Color: 1
Size: 271510 Color: 0
Size: 253642 Color: 1

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 474884 Color: 0
Size: 264266 Color: 1
Size: 260851 Color: 0

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 474962 Color: 0
Size: 273341 Color: 1
Size: 251698 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 474934 Color: 1
Size: 269537 Color: 1
Size: 255530 Color: 0

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 475120 Color: 0
Size: 262554 Color: 0
Size: 262327 Color: 1

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 475152 Color: 1
Size: 267131 Color: 1
Size: 257718 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 475256 Color: 0
Size: 269248 Color: 1
Size: 255497 Color: 0

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 475264 Color: 0
Size: 272886 Color: 1
Size: 251851 Color: 1

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 475320 Color: 0
Size: 266566 Color: 0
Size: 258115 Color: 1

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 475292 Color: 1
Size: 269869 Color: 1
Size: 254840 Color: 0

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 475464 Color: 0
Size: 265093 Color: 1
Size: 259444 Color: 1

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 475468 Color: 0
Size: 270123 Color: 1
Size: 254410 Color: 1

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 475538 Color: 0
Size: 267262 Color: 1
Size: 257201 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 475557 Color: 1
Size: 274098 Color: 0
Size: 250346 Color: 1

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 475578 Color: 0
Size: 263955 Color: 0
Size: 260468 Color: 1

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 475598 Color: 1
Size: 272294 Color: 0
Size: 252109 Color: 1

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 475599 Color: 0
Size: 274005 Color: 1
Size: 250397 Color: 0

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 475716 Color: 1
Size: 264904 Color: 0
Size: 259381 Color: 1

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 475800 Color: 1
Size: 273204 Color: 0
Size: 250997 Color: 0

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 475940 Color: 1
Size: 264306 Color: 0
Size: 259755 Color: 0

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 475996 Color: 1
Size: 269174 Color: 0
Size: 254831 Color: 1

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 476056 Color: 1
Size: 269583 Color: 1
Size: 254362 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 476104 Color: 0
Size: 267374 Color: 1
Size: 256523 Color: 0

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 455414 Color: 0
Size: 277082 Color: 0
Size: 267505 Color: 1

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 476408 Color: 1
Size: 265112 Color: 1
Size: 258481 Color: 0

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 476505 Color: 1
Size: 264448 Color: 0
Size: 259048 Color: 1

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 476476 Color: 0
Size: 263529 Color: 0
Size: 259996 Color: 1

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 476609 Color: 0
Size: 267705 Color: 1
Size: 255687 Color: 1

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 476622 Color: 0
Size: 269303 Color: 0
Size: 254076 Color: 1

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 476594 Color: 1
Size: 273321 Color: 1
Size: 250086 Color: 0

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 476794 Color: 1
Size: 267980 Color: 0
Size: 255227 Color: 1

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 477024 Color: 0
Size: 272788 Color: 0
Size: 250189 Color: 1

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 477095 Color: 1
Size: 270636 Color: 0
Size: 252270 Color: 1

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 477183 Color: 1
Size: 269347 Color: 0
Size: 253471 Color: 0

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 477201 Color: 1
Size: 262161 Color: 1
Size: 260639 Color: 0

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 477292 Color: 1
Size: 268850 Color: 0
Size: 253859 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 477352 Color: 1
Size: 267918 Color: 0
Size: 254731 Color: 1

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 477371 Color: 0
Size: 268152 Color: 1
Size: 254478 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 477509 Color: 1
Size: 261488 Color: 1
Size: 261004 Color: 0

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 477538 Color: 1
Size: 264178 Color: 0
Size: 258285 Color: 1

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 477560 Color: 1
Size: 262814 Color: 0
Size: 259627 Color: 0

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 477662 Color: 1
Size: 262106 Color: 1
Size: 260233 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 477731 Color: 0
Size: 266586 Color: 1
Size: 255684 Color: 0

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 477766 Color: 1
Size: 264229 Color: 0
Size: 258006 Color: 1

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 478019 Color: 0
Size: 271297 Color: 0
Size: 250685 Color: 1

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 478101 Color: 0
Size: 264739 Color: 1
Size: 257161 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 478185 Color: 0
Size: 261994 Color: 1
Size: 259822 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 478162 Color: 1
Size: 262015 Color: 1
Size: 259824 Color: 0

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 478350 Color: 0
Size: 266862 Color: 1
Size: 254789 Color: 0

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 478387 Color: 0
Size: 264941 Color: 1
Size: 256673 Color: 1

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 478424 Color: 1
Size: 262598 Color: 0
Size: 258979 Color: 1

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 478468 Color: 1
Size: 269871 Color: 1
Size: 251662 Color: 0

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 478470 Color: 0
Size: 269244 Color: 1
Size: 252287 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 478533 Color: 1
Size: 271383 Color: 0
Size: 250085 Color: 1

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 478732 Color: 0
Size: 266675 Color: 1
Size: 254594 Color: 1

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 478839 Color: 1
Size: 268086 Color: 0
Size: 253076 Color: 1

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 478851 Color: 1
Size: 268612 Color: 0
Size: 252538 Color: 0

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 478902 Color: 1
Size: 262462 Color: 0
Size: 258637 Color: 1

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 478916 Color: 1
Size: 271083 Color: 0
Size: 250002 Color: 0

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 478944 Color: 1
Size: 262813 Color: 1
Size: 258244 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 479062 Color: 1
Size: 260743 Color: 0
Size: 260196 Color: 1

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 479237 Color: 0
Size: 267158 Color: 1
Size: 253606 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 479283 Color: 1
Size: 265661 Color: 1
Size: 255057 Color: 0

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 374918 Color: 1
Size: 358240 Color: 0
Size: 266843 Color: 1

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 479582 Color: 1
Size: 264036 Color: 0
Size: 256383 Color: 0

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 479684 Color: 1
Size: 261389 Color: 0
Size: 258928 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 479968 Color: 0
Size: 265996 Color: 1
Size: 254037 Color: 0

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 479993 Color: 0
Size: 269935 Color: 1
Size: 250073 Color: 1

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 480193 Color: 0
Size: 265572 Color: 1
Size: 254236 Color: 1

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 480310 Color: 0
Size: 261321 Color: 1
Size: 258370 Color: 0

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 480350 Color: 1
Size: 265686 Color: 1
Size: 253965 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 480396 Color: 0
Size: 269066 Color: 0
Size: 250539 Color: 1

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 480451 Color: 1
Size: 263476 Color: 1
Size: 256074 Color: 0

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 480407 Color: 0
Size: 261090 Color: 0
Size: 258504 Color: 1

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 480828 Color: 0
Size: 264737 Color: 1
Size: 254436 Color: 0

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 480808 Color: 1
Size: 269140 Color: 1
Size: 250053 Color: 0

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 480876 Color: 1
Size: 267192 Color: 1
Size: 251933 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 481005 Color: 1
Size: 261848 Color: 0
Size: 257148 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 481020 Color: 1
Size: 261715 Color: 0
Size: 257266 Color: 1

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 481078 Color: 1
Size: 259827 Color: 0
Size: 259096 Color: 1

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 481078 Color: 0
Size: 267911 Color: 1
Size: 251012 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 481081 Color: 0
Size: 259587 Color: 1
Size: 259333 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 481120 Color: 0
Size: 259592 Color: 1
Size: 259289 Color: 1

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 481167 Color: 1
Size: 265368 Color: 1
Size: 253466 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 481172 Color: 0
Size: 261257 Color: 0
Size: 257572 Color: 1

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 481408 Color: 0
Size: 260841 Color: 1
Size: 257752 Color: 0

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 481787 Color: 0
Size: 267821 Color: 1
Size: 250393 Color: 1

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 481853 Color: 0
Size: 259951 Color: 0
Size: 258197 Color: 1

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 482036 Color: 1
Size: 260517 Color: 1
Size: 257448 Color: 0

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 481976 Color: 0
Size: 259802 Color: 0
Size: 258223 Color: 1

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 482152 Color: 1
Size: 261065 Color: 1
Size: 256784 Color: 0

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 482336 Color: 0
Size: 263682 Color: 1
Size: 253983 Color: 1

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 482492 Color: 1
Size: 262304 Color: 1
Size: 255205 Color: 0

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 482406 Color: 0
Size: 264018 Color: 0
Size: 253577 Color: 1

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 482514 Color: 1
Size: 262918 Color: 1
Size: 254569 Color: 0

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 482565 Color: 1
Size: 260660 Color: 0
Size: 256776 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 482652 Color: 1
Size: 261414 Color: 1
Size: 255935 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 482679 Color: 1
Size: 265852 Color: 0
Size: 251470 Color: 1

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 482921 Color: 0
Size: 260103 Color: 1
Size: 256977 Color: 1

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 482922 Color: 0
Size: 263434 Color: 1
Size: 253645 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 483073 Color: 0
Size: 259978 Color: 1
Size: 256950 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 483111 Color: 1
Size: 266306 Color: 0
Size: 250584 Color: 1

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 483563 Color: 1
Size: 264110 Color: 0
Size: 252328 Color: 1

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 483590 Color: 0
Size: 261469 Color: 1
Size: 254942 Color: 0

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 483597 Color: 0
Size: 265672 Color: 0
Size: 250732 Color: 1

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 483575 Color: 1
Size: 260894 Color: 0
Size: 255532 Color: 1

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 483615 Color: 0
Size: 261175 Color: 0
Size: 255211 Color: 1

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 483632 Color: 1
Size: 263828 Color: 0
Size: 252541 Color: 1

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 483651 Color: 0
Size: 263056 Color: 1
Size: 253294 Color: 0

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 483918 Color: 1
Size: 265270 Color: 0
Size: 250813 Color: 0

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 483956 Color: 1
Size: 264247 Color: 0
Size: 251798 Color: 1

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 483968 Color: 0
Size: 258192 Color: 1
Size: 257841 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 484122 Color: 1
Size: 265247 Color: 0
Size: 250632 Color: 1

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 484127 Color: 1
Size: 258353 Color: 0
Size: 257521 Color: 0

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 484204 Color: 1
Size: 262828 Color: 0
Size: 252969 Color: 1

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 484355 Color: 1
Size: 261122 Color: 1
Size: 254524 Color: 0

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 484479 Color: 0
Size: 264587 Color: 1
Size: 250935 Color: 0

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 484605 Color: 1
Size: 262929 Color: 0
Size: 252467 Color: 1

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 484605 Color: 1
Size: 265337 Color: 0
Size: 250059 Color: 0

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 484627 Color: 1
Size: 263663 Color: 1
Size: 251711 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 484774 Color: 1
Size: 258537 Color: 1
Size: 256690 Color: 0

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 484801 Color: 1
Size: 264602 Color: 0
Size: 250598 Color: 0

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 484858 Color: 1
Size: 263333 Color: 0
Size: 251810 Color: 1

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 484959 Color: 1
Size: 262230 Color: 0
Size: 252812 Color: 0

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 484961 Color: 1
Size: 257703 Color: 0
Size: 257337 Color: 1

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 484996 Color: 1
Size: 260604 Color: 1
Size: 254401 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 485144 Color: 1
Size: 264327 Color: 0
Size: 250530 Color: 0

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 485270 Color: 1
Size: 263468 Color: 0
Size: 251263 Color: 1

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 485232 Color: 0
Size: 257712 Color: 1
Size: 257057 Color: 0

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 485362 Color: 1
Size: 260815 Color: 0
Size: 253824 Color: 1

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 485317 Color: 0
Size: 260592 Color: 0
Size: 254092 Color: 1

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 485474 Color: 1
Size: 264107 Color: 0
Size: 250420 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 485640 Color: 0
Size: 263436 Color: 0
Size: 250925 Color: 1

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 485680 Color: 1
Size: 264205 Color: 1
Size: 250116 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 485729 Color: 1
Size: 257466 Color: 0
Size: 256806 Color: 1

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 485740 Color: 1
Size: 262278 Color: 1
Size: 251983 Color: 0

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 485713 Color: 0
Size: 260058 Color: 0
Size: 254230 Color: 1

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 485828 Color: 1
Size: 262420 Color: 0
Size: 251753 Color: 1

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 485776 Color: 0
Size: 261152 Color: 1
Size: 253073 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 485878 Color: 1
Size: 263268 Color: 0
Size: 250855 Color: 1

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 486020 Color: 0
Size: 257766 Color: 1
Size: 256215 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 486006 Color: 1
Size: 257645 Color: 1
Size: 256350 Color: 0

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 486035 Color: 0
Size: 258259 Color: 1
Size: 255707 Color: 0

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 486106 Color: 0
Size: 263120 Color: 1
Size: 250775 Color: 1

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 486409 Color: 0
Size: 262052 Color: 1
Size: 251540 Color: 0

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 486507 Color: 1
Size: 258728 Color: 0
Size: 254766 Color: 1

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 486541 Color: 0
Size: 258020 Color: 0
Size: 255440 Color: 1

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 486679 Color: 0
Size: 257389 Color: 1
Size: 255933 Color: 0

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 486817 Color: 1
Size: 261190 Color: 0
Size: 251994 Color: 0

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 487047 Color: 0
Size: 260080 Color: 1
Size: 252874 Color: 1

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 487170 Color: 1
Size: 259579 Color: 0
Size: 253252 Color: 0

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 487306 Color: 0
Size: 260808 Color: 1
Size: 251887 Color: 1

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 487279 Color: 1
Size: 258756 Color: 0
Size: 253966 Color: 0

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 487361 Color: 0
Size: 257254 Color: 0
Size: 255386 Color: 1

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 487363 Color: 1
Size: 256738 Color: 0
Size: 255900 Color: 1

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 487653 Color: 0
Size: 262116 Color: 0
Size: 250232 Color: 1

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 487726 Color: 0
Size: 260486 Color: 1
Size: 251789 Color: 1

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 487833 Color: 1
Size: 260916 Color: 0
Size: 251252 Color: 0

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 487853 Color: 1
Size: 259573 Color: 0
Size: 252575 Color: 1

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 487866 Color: 1
Size: 260086 Color: 0
Size: 252049 Color: 0

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 487978 Color: 0
Size: 261771 Color: 0
Size: 250252 Color: 1

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 487975 Color: 1
Size: 257391 Color: 1
Size: 254635 Color: 0

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 488074 Color: 0
Size: 259542 Color: 1
Size: 252385 Color: 1

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 488077 Color: 0
Size: 258846 Color: 0
Size: 253078 Color: 1

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 488047 Color: 1
Size: 256364 Color: 1
Size: 255590 Color: 0

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 488131 Color: 0
Size: 257277 Color: 1
Size: 254593 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 488189 Color: 1
Size: 260899 Color: 0
Size: 250913 Color: 1

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 488410 Color: 0
Size: 258803 Color: 1
Size: 252788 Color: 0

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 488446 Color: 1
Size: 258476 Color: 0
Size: 253079 Color: 0

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 488490 Color: 1
Size: 258384 Color: 0
Size: 253127 Color: 1

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 488687 Color: 1
Size: 257904 Color: 0
Size: 253410 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 488868 Color: 1
Size: 260969 Color: 0
Size: 250164 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 488873 Color: 1
Size: 261093 Color: 0
Size: 250035 Color: 1

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 488907 Color: 1
Size: 257833 Color: 0
Size: 253261 Color: 0

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 489092 Color: 1
Size: 259705 Color: 0
Size: 251204 Color: 1

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 489631 Color: 1
Size: 258441 Color: 0
Size: 251929 Color: 0

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 489601 Color: 0
Size: 259588 Color: 1
Size: 250812 Color: 0

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 489663 Color: 1
Size: 256006 Color: 0
Size: 254332 Color: 1

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 489725 Color: 0
Size: 255395 Color: 1
Size: 254881 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 489890 Color: 0
Size: 258033 Color: 1
Size: 252078 Color: 0

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 489892 Color: 0
Size: 258888 Color: 0
Size: 251221 Color: 1

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 489997 Color: 1
Size: 258599 Color: 0
Size: 251405 Color: 1

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 490133 Color: 1
Size: 259738 Color: 0
Size: 250130 Color: 0

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 490140 Color: 1
Size: 256110 Color: 1
Size: 253751 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 490095 Color: 0
Size: 259073 Color: 1
Size: 250833 Color: 0

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 490181 Color: 1
Size: 255918 Color: 1
Size: 253902 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 490124 Color: 0
Size: 258060 Color: 1
Size: 251817 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 490289 Color: 1
Size: 255171 Color: 0
Size: 254541 Color: 0

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 490518 Color: 1
Size: 255002 Color: 1
Size: 254481 Color: 0

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 490604 Color: 0
Size: 257932 Color: 0
Size: 251465 Color: 1

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 490637 Color: 0
Size: 255921 Color: 1
Size: 253443 Color: 1

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 490675 Color: 0
Size: 257296 Color: 1
Size: 252030 Color: 0

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 490828 Color: 0
Size: 258181 Color: 1
Size: 250992 Color: 1

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 490869 Color: 1
Size: 256772 Color: 0
Size: 252360 Color: 1

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 490896 Color: 0
Size: 255237 Color: 1
Size: 253868 Color: 0

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 491155 Color: 1
Size: 255993 Color: 0
Size: 252853 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 491307 Color: 0
Size: 258275 Color: 0
Size: 250419 Color: 1

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 491485 Color: 1
Size: 254781 Color: 1
Size: 253735 Color: 0

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 491500 Color: 1
Size: 256605 Color: 0
Size: 251896 Color: 1

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 492484 Color: 1
Size: 256967 Color: 0
Size: 250550 Color: 1

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 492897 Color: 1
Size: 257078 Color: 0
Size: 250026 Color: 1

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 493377 Color: 1
Size: 256402 Color: 0
Size: 250222 Color: 0

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 493436 Color: 1
Size: 256167 Color: 0
Size: 250398 Color: 0

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 493561 Color: 1
Size: 256292 Color: 1
Size: 250148 Color: 0

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 370467 Color: 1
Size: 327071 Color: 1
Size: 302463 Color: 0

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 493872 Color: 0
Size: 256022 Color: 1
Size: 250107 Color: 0

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 493789 Color: 1
Size: 255181 Color: 1
Size: 251031 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 494462 Color: 0
Size: 253888 Color: 1
Size: 251651 Color: 1

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 494519 Color: 0
Size: 253970 Color: 1
Size: 251512 Color: 0

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 494461 Color: 1
Size: 253060 Color: 0
Size: 252480 Color: 1

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 495141 Color: 0
Size: 254056 Color: 0
Size: 250804 Color: 1

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 495115 Color: 1
Size: 254837 Color: 0
Size: 250049 Color: 1

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 495308 Color: 1
Size: 254180 Color: 0
Size: 250513 Color: 0

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 495326 Color: 1
Size: 254599 Color: 0
Size: 250076 Color: 0

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 495417 Color: 0
Size: 253444 Color: 1
Size: 251140 Color: 0

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 495537 Color: 0
Size: 253328 Color: 0
Size: 251136 Color: 1

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 495566 Color: 1
Size: 254328 Color: 1
Size: 250107 Color: 0

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 495629 Color: 0
Size: 252468 Color: 1
Size: 251904 Color: 0

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 495920 Color: 1
Size: 252453 Color: 1
Size: 251628 Color: 0

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 496154 Color: 1
Size: 253660 Color: 0
Size: 250187 Color: 0

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 496587 Color: 1
Size: 252268 Color: 1
Size: 251146 Color: 0

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 496656 Color: 0
Size: 253326 Color: 1
Size: 250019 Color: 0

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 496642 Color: 1
Size: 252637 Color: 0
Size: 250722 Color: 1

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 496680 Color: 0
Size: 251801 Color: 1
Size: 251520 Color: 0

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 496665 Color: 1
Size: 251959 Color: 1
Size: 251377 Color: 0

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 496739 Color: 0
Size: 252604 Color: 0
Size: 250658 Color: 1

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 496963 Color: 0
Size: 251765 Color: 1
Size: 251273 Color: 1

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 497121 Color: 1
Size: 251529 Color: 0
Size: 251351 Color: 1

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 497273 Color: 1
Size: 252665 Color: 0
Size: 250063 Color: 0

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 497292 Color: 1
Size: 252387 Color: 1
Size: 250322 Color: 0

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 497501 Color: 1
Size: 251765 Color: 0
Size: 250735 Color: 0

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 497980 Color: 1
Size: 251252 Color: 0
Size: 250769 Color: 0

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 497982 Color: 1
Size: 251932 Color: 1
Size: 250087 Color: 0

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 498109 Color: 1
Size: 251140 Color: 1
Size: 250752 Color: 0

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 498229 Color: 1
Size: 251762 Color: 0
Size: 250010 Color: 1

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 498341 Color: 1
Size: 251344 Color: 0
Size: 250316 Color: 0

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 498385 Color: 1
Size: 250824 Color: 1
Size: 250792 Color: 0

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 498483 Color: 1
Size: 251475 Color: 0
Size: 250043 Color: 0

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 498520 Color: 1
Size: 251262 Color: 0
Size: 250219 Color: 1

Bin 2919: 1 of cap free
Amount of items: 3
Items: 
Size: 369034 Color: 0
Size: 358757 Color: 1
Size: 272209 Color: 0

Bin 2920: 1 of cap free
Amount of items: 3
Items: 
Size: 484660 Color: 0
Size: 259675 Color: 1
Size: 255665 Color: 1

Bin 2921: 1 of cap free
Amount of items: 3
Items: 
Size: 394381 Color: 1
Size: 336297 Color: 0
Size: 269322 Color: 1

Bin 2922: 1 of cap free
Amount of items: 3
Items: 
Size: 355168 Color: 0
Size: 339467 Color: 1
Size: 305365 Color: 0

Bin 2923: 1 of cap free
Amount of items: 3
Items: 
Size: 363216 Color: 0
Size: 331278 Color: 0
Size: 305506 Color: 1

Bin 2924: 1 of cap free
Amount of items: 3
Items: 
Size: 361598 Color: 1
Size: 351157 Color: 0
Size: 287245 Color: 1

Bin 2925: 1 of cap free
Amount of items: 3
Items: 
Size: 436605 Color: 0
Size: 295256 Color: 0
Size: 268139 Color: 1

Bin 2926: 1 of cap free
Amount of items: 3
Items: 
Size: 358178 Color: 1
Size: 327218 Color: 0
Size: 314604 Color: 0

Bin 2927: 1 of cap free
Amount of items: 3
Items: 
Size: 356968 Color: 0
Size: 340580 Color: 1
Size: 302452 Color: 1

Bin 2928: 1 of cap free
Amount of items: 3
Items: 
Size: 375461 Color: 0
Size: 334737 Color: 1
Size: 289802 Color: 1

Bin 2929: 1 of cap free
Amount of items: 3
Items: 
Size: 380703 Color: 1
Size: 354684 Color: 1
Size: 264613 Color: 0

Bin 2930: 1 of cap free
Amount of items: 3
Items: 
Size: 416257 Color: 0
Size: 299781 Color: 0
Size: 283962 Color: 1

Bin 2931: 1 of cap free
Amount of items: 3
Items: 
Size: 361403 Color: 1
Size: 354882 Color: 1
Size: 283715 Color: 0

Bin 2932: 1 of cap free
Amount of items: 3
Items: 
Size: 461539 Color: 1
Size: 272821 Color: 1
Size: 265640 Color: 0

Bin 2933: 1 of cap free
Amount of items: 3
Items: 
Size: 361027 Color: 1
Size: 330924 Color: 0
Size: 308049 Color: 1

Bin 2934: 1 of cap free
Amount of items: 3
Items: 
Size: 359255 Color: 0
Size: 333837 Color: 1
Size: 306908 Color: 0

Bin 2935: 1 of cap free
Amount of items: 3
Items: 
Size: 379047 Color: 1
Size: 338298 Color: 1
Size: 282655 Color: 0

Bin 2936: 1 of cap free
Amount of items: 3
Items: 
Size: 396414 Color: 1
Size: 349475 Color: 0
Size: 254111 Color: 1

Bin 2937: 1 of cap free
Amount of items: 3
Items: 
Size: 352113 Color: 1
Size: 345596 Color: 0
Size: 302291 Color: 0

Bin 2938: 1 of cap free
Amount of items: 3
Items: 
Size: 474370 Color: 1
Size: 263262 Color: 0
Size: 262368 Color: 1

Bin 2939: 1 of cap free
Amount of items: 3
Items: 
Size: 400133 Color: 1
Size: 329387 Color: 0
Size: 270480 Color: 0

Bin 2940: 1 of cap free
Amount of items: 3
Items: 
Size: 362146 Color: 0
Size: 320823 Color: 0
Size: 317031 Color: 1

Bin 2941: 1 of cap free
Amount of items: 3
Items: 
Size: 362255 Color: 1
Size: 345695 Color: 0
Size: 292050 Color: 1

Bin 2942: 1 of cap free
Amount of items: 3
Items: 
Size: 436286 Color: 1
Size: 308279 Color: 0
Size: 255435 Color: 1

Bin 2943: 1 of cap free
Amount of items: 3
Items: 
Size: 406368 Color: 0
Size: 337415 Color: 1
Size: 256217 Color: 1

Bin 2944: 1 of cap free
Amount of items: 3
Items: 
Size: 356833 Color: 1
Size: 333351 Color: 1
Size: 309816 Color: 0

Bin 2945: 1 of cap free
Amount of items: 3
Items: 
Size: 461277 Color: 1
Size: 272621 Color: 0
Size: 266102 Color: 0

Bin 2946: 1 of cap free
Amount of items: 3
Items: 
Size: 353164 Color: 0
Size: 348684 Color: 1
Size: 298152 Color: 0

Bin 2947: 1 of cap free
Amount of items: 3
Items: 
Size: 425483 Color: 0
Size: 321159 Color: 0
Size: 253358 Color: 1

Bin 2948: 1 of cap free
Amount of items: 3
Items: 
Size: 361362 Color: 1
Size: 336678 Color: 0
Size: 301960 Color: 1

Bin 2949: 1 of cap free
Amount of items: 3
Items: 
Size: 359484 Color: 1
Size: 338338 Color: 0
Size: 302178 Color: 1

Bin 2950: 1 of cap free
Amount of items: 3
Items: 
Size: 353805 Color: 1
Size: 340493 Color: 0
Size: 305702 Color: 0

Bin 2951: 1 of cap free
Amount of items: 3
Items: 
Size: 365023 Color: 1
Size: 327543 Color: 1
Size: 307434 Color: 0

Bin 2952: 1 of cap free
Amount of items: 3
Items: 
Size: 348713 Color: 1
Size: 330944 Color: 0
Size: 320343 Color: 1

Bin 2953: 1 of cap free
Amount of items: 3
Items: 
Size: 407629 Color: 0
Size: 333952 Color: 0
Size: 258419 Color: 1

Bin 2954: 1 of cap free
Amount of items: 3
Items: 
Size: 343388 Color: 0
Size: 332505 Color: 0
Size: 324107 Color: 1

Bin 2955: 1 of cap free
Amount of items: 3
Items: 
Size: 362605 Color: 1
Size: 333273 Color: 1
Size: 304122 Color: 0

Bin 2956: 1 of cap free
Amount of items: 3
Items: 
Size: 335892 Color: 0
Size: 332180 Color: 1
Size: 331928 Color: 1

Bin 2957: 1 of cap free
Amount of items: 3
Items: 
Size: 351648 Color: 1
Size: 333888 Color: 1
Size: 314464 Color: 0

Bin 2958: 1 of cap free
Amount of items: 3
Items: 
Size: 391372 Color: 1
Size: 317289 Color: 1
Size: 291339 Color: 0

Bin 2959: 1 of cap free
Amount of items: 3
Items: 
Size: 337436 Color: 0
Size: 332375 Color: 1
Size: 330189 Color: 0

Bin 2960: 1 of cap free
Amount of items: 3
Items: 
Size: 354273 Color: 1
Size: 324647 Color: 1
Size: 321080 Color: 0

Bin 2961: 1 of cap free
Amount of items: 3
Items: 
Size: 354407 Color: 1
Size: 350103 Color: 0
Size: 295490 Color: 1

Bin 2962: 1 of cap free
Amount of items: 3
Items: 
Size: 354510 Color: 1
Size: 334206 Color: 0
Size: 311284 Color: 1

Bin 2963: 1 of cap free
Amount of items: 3
Items: 
Size: 355170 Color: 0
Size: 345869 Color: 0
Size: 298961 Color: 1

Bin 2964: 1 of cap free
Amount of items: 3
Items: 
Size: 355865 Color: 0
Size: 350116 Color: 0
Size: 294019 Color: 1

Bin 2965: 1 of cap free
Amount of items: 3
Items: 
Size: 355962 Color: 0
Size: 322304 Color: 1
Size: 321734 Color: 0

Bin 2966: 1 of cap free
Amount of items: 3
Items: 
Size: 356562 Color: 0
Size: 332388 Color: 0
Size: 311050 Color: 1

Bin 2967: 1 of cap free
Amount of items: 3
Items: 
Size: 357778 Color: 1
Size: 336874 Color: 1
Size: 305348 Color: 0

Bin 2968: 1 of cap free
Amount of items: 3
Items: 
Size: 357870 Color: 1
Size: 324443 Color: 0
Size: 317687 Color: 0

Bin 2969: 1 of cap free
Amount of items: 3
Items: 
Size: 358503 Color: 1
Size: 350560 Color: 0
Size: 290937 Color: 1

Bin 2970: 1 of cap free
Amount of items: 3
Items: 
Size: 358247 Color: 0
Size: 348797 Color: 0
Size: 292956 Color: 1

Bin 2971: 1 of cap free
Amount of items: 3
Items: 
Size: 358414 Color: 0
Size: 322532 Color: 0
Size: 319054 Color: 1

Bin 2972: 1 of cap free
Amount of items: 3
Items: 
Size: 358605 Color: 0
Size: 342793 Color: 0
Size: 298602 Color: 1

Bin 2973: 1 of cap free
Amount of items: 3
Items: 
Size: 358609 Color: 0
Size: 352063 Color: 0
Size: 289328 Color: 1

Bin 2974: 1 of cap free
Amount of items: 3
Items: 
Size: 358774 Color: 0
Size: 326894 Color: 0
Size: 314332 Color: 1

Bin 2975: 1 of cap free
Amount of items: 3
Items: 
Size: 359264 Color: 0
Size: 321985 Color: 0
Size: 318751 Color: 1

Bin 2976: 1 of cap free
Amount of items: 3
Items: 
Size: 361044 Color: 0
Size: 322061 Color: 0
Size: 316895 Color: 1

Bin 2977: 1 of cap free
Amount of items: 3
Items: 
Size: 361942 Color: 0
Size: 360699 Color: 1
Size: 277359 Color: 0

Bin 2978: 1 of cap free
Amount of items: 3
Items: 
Size: 362305 Color: 0
Size: 322406 Color: 0
Size: 315289 Color: 1

Bin 2979: 1 of cap free
Amount of items: 3
Items: 
Size: 364541 Color: 0
Size: 361553 Color: 0
Size: 273906 Color: 1

Bin 2980: 1 of cap free
Amount of items: 3
Items: 
Size: 364671 Color: 1
Size: 326197 Color: 1
Size: 309132 Color: 0

Bin 2981: 1 of cap free
Amount of items: 3
Items: 
Size: 365696 Color: 0
Size: 339101 Color: 1
Size: 295203 Color: 0

Bin 2982: 1 of cap free
Amount of items: 3
Items: 
Size: 365527 Color: 1
Size: 352367 Color: 1
Size: 282106 Color: 0

Bin 2983: 1 of cap free
Amount of items: 3
Items: 
Size: 366067 Color: 0
Size: 332948 Color: 1
Size: 300985 Color: 0

Bin 2984: 1 of cap free
Amount of items: 3
Items: 
Size: 366166 Color: 0
Size: 328701 Color: 1
Size: 305133 Color: 0

Bin 2985: 1 of cap free
Amount of items: 3
Items: 
Size: 366275 Color: 1
Size: 366131 Color: 1
Size: 267594 Color: 0

Bin 2986: 1 of cap free
Amount of items: 3
Items: 
Size: 367214 Color: 0
Size: 324943 Color: 0
Size: 307843 Color: 1

Bin 2987: 1 of cap free
Amount of items: 3
Items: 
Size: 373018 Color: 0
Size: 327909 Color: 1
Size: 299073 Color: 1

Bin 2988: 1 of cap free
Amount of items: 3
Items: 
Size: 376627 Color: 0
Size: 348034 Color: 1
Size: 275339 Color: 1

Bin 2989: 1 of cap free
Amount of items: 3
Items: 
Size: 354615 Color: 0
Size: 351025 Color: 1
Size: 294360 Color: 0

Bin 2990: 1 of cap free
Amount of items: 3
Items: 
Size: 384303 Color: 0
Size: 353283 Color: 1
Size: 262414 Color: 1

Bin 2991: 1 of cap free
Amount of items: 3
Items: 
Size: 349994 Color: 1
Size: 327358 Color: 1
Size: 322648 Color: 0

Bin 2992: 1 of cap free
Amount of items: 3
Items: 
Size: 354693 Color: 0
Size: 353321 Color: 1
Size: 291986 Color: 1

Bin 2993: 1 of cap free
Amount of items: 3
Items: 
Size: 351488 Color: 1
Size: 347085 Color: 1
Size: 301427 Color: 0

Bin 2994: 1 of cap free
Amount of items: 3
Items: 
Size: 358345 Color: 1
Size: 323240 Color: 1
Size: 318415 Color: 0

Bin 2995: 1 of cap free
Amount of items: 3
Items: 
Size: 340815 Color: 0
Size: 332705 Color: 0
Size: 326480 Color: 1

Bin 2996: 1 of cap free
Amount of items: 3
Items: 
Size: 499727 Color: 0
Size: 250215 Color: 1
Size: 250058 Color: 1

Bin 2997: 1 of cap free
Amount of items: 3
Items: 
Size: 400408 Color: 1
Size: 345879 Color: 0
Size: 253713 Color: 0

Bin 2998: 1 of cap free
Amount of items: 3
Items: 
Size: 334482 Color: 0
Size: 332848 Color: 1
Size: 332670 Color: 0

Bin 2999: 1 of cap free
Amount of items: 3
Items: 
Size: 359499 Color: 0
Size: 353368 Color: 0
Size: 287133 Color: 1

Bin 3000: 1 of cap free
Amount of items: 3
Items: 
Size: 366831 Color: 0
Size: 354395 Color: 1
Size: 278774 Color: 0

Bin 3001: 1 of cap free
Amount of items: 3
Items: 
Size: 366994 Color: 0
Size: 341587 Color: 1
Size: 291419 Color: 0

Bin 3002: 1 of cap free
Amount of items: 3
Items: 
Size: 390942 Color: 1
Size: 337620 Color: 1
Size: 271438 Color: 0

Bin 3003: 1 of cap free
Amount of items: 3
Items: 
Size: 351591 Color: 0
Size: 351565 Color: 1
Size: 296844 Color: 0

Bin 3004: 1 of cap free
Amount of items: 3
Items: 
Size: 347251 Color: 1
Size: 344339 Color: 1
Size: 308410 Color: 0

Bin 3005: 1 of cap free
Amount of items: 3
Items: 
Size: 349884 Color: 0
Size: 345282 Color: 0
Size: 304834 Color: 1

Bin 3006: 1 of cap free
Amount of items: 3
Items: 
Size: 362681 Color: 1
Size: 327652 Color: 1
Size: 309667 Color: 0

Bin 3007: 1 of cap free
Amount of items: 3
Items: 
Size: 362719 Color: 1
Size: 347833 Color: 1
Size: 289448 Color: 0

Bin 3008: 1 of cap free
Amount of items: 3
Items: 
Size: 363025 Color: 0
Size: 326486 Color: 1
Size: 310489 Color: 0

Bin 3009: 1 of cap free
Amount of items: 3
Items: 
Size: 373546 Color: 1
Size: 360513 Color: 0
Size: 265941 Color: 0

Bin 3010: 1 of cap free
Amount of items: 3
Items: 
Size: 393271 Color: 0
Size: 354300 Color: 1
Size: 252429 Color: 1

Bin 3011: 1 of cap free
Amount of items: 3
Items: 
Size: 353259 Color: 1
Size: 337677 Color: 0
Size: 309064 Color: 1

Bin 3012: 2 of cap free
Amount of items: 3
Items: 
Size: 385566 Color: 0
Size: 348998 Color: 1
Size: 265435 Color: 1

Bin 3013: 2 of cap free
Amount of items: 3
Items: 
Size: 404868 Color: 0
Size: 329389 Color: 1
Size: 265742 Color: 0

Bin 3014: 2 of cap free
Amount of items: 3
Items: 
Size: 363095 Color: 1
Size: 357458 Color: 0
Size: 279446 Color: 0

Bin 3015: 2 of cap free
Amount of items: 3
Items: 
Size: 390244 Color: 0
Size: 341239 Color: 1
Size: 268516 Color: 0

Bin 3016: 2 of cap free
Amount of items: 3
Items: 
Size: 479099 Color: 0
Size: 260575 Color: 0
Size: 260325 Color: 1

Bin 3017: 2 of cap free
Amount of items: 3
Items: 
Size: 357980 Color: 1
Size: 341232 Color: 1
Size: 300787 Color: 0

Bin 3018: 2 of cap free
Amount of items: 3
Items: 
Size: 391204 Color: 0
Size: 334600 Color: 1
Size: 274195 Color: 1

Bin 3019: 2 of cap free
Amount of items: 3
Items: 
Size: 415626 Color: 0
Size: 296613 Color: 1
Size: 287760 Color: 0

Bin 3020: 2 of cap free
Amount of items: 3
Items: 
Size: 353690 Color: 1
Size: 333701 Color: 0
Size: 312608 Color: 0

Bin 3021: 2 of cap free
Amount of items: 3
Items: 
Size: 390481 Color: 0
Size: 330689 Color: 1
Size: 278829 Color: 1

Bin 3022: 2 of cap free
Amount of items: 3
Items: 
Size: 363711 Color: 1
Size: 358673 Color: 0
Size: 277615 Color: 1

Bin 3023: 2 of cap free
Amount of items: 3
Items: 
Size: 385253 Color: 0
Size: 359594 Color: 1
Size: 255152 Color: 1

Bin 3024: 2 of cap free
Amount of items: 3
Items: 
Size: 434339 Color: 0
Size: 313909 Color: 0
Size: 251751 Color: 1

Bin 3025: 2 of cap free
Amount of items: 3
Items: 
Size: 380275 Color: 1
Size: 340735 Color: 0
Size: 278989 Color: 0

Bin 3026: 2 of cap free
Amount of items: 3
Items: 
Size: 365038 Color: 1
Size: 360444 Color: 0
Size: 274517 Color: 0

Bin 3027: 2 of cap free
Amount of items: 3
Items: 
Size: 498531 Color: 1
Size: 250858 Color: 1
Size: 250610 Color: 0

Bin 3028: 2 of cap free
Amount of items: 3
Items: 
Size: 484289 Color: 0
Size: 263899 Color: 0
Size: 251811 Color: 1

Bin 3029: 2 of cap free
Amount of items: 3
Items: 
Size: 417206 Color: 1
Size: 326232 Color: 1
Size: 256561 Color: 0

Bin 3030: 2 of cap free
Amount of items: 3
Items: 
Size: 411549 Color: 0
Size: 333816 Color: 1
Size: 254634 Color: 1

Bin 3031: 2 of cap free
Amount of items: 3
Items: 
Size: 394529 Color: 0
Size: 352570 Color: 1
Size: 252900 Color: 0

Bin 3032: 2 of cap free
Amount of items: 3
Items: 
Size: 438594 Color: 1
Size: 287572 Color: 0
Size: 273833 Color: 0

Bin 3033: 2 of cap free
Amount of items: 3
Items: 
Size: 374149 Color: 1
Size: 349942 Color: 0
Size: 275908 Color: 1

Bin 3034: 2 of cap free
Amount of items: 3
Items: 
Size: 410447 Color: 0
Size: 321357 Color: 1
Size: 268195 Color: 1

Bin 3035: 2 of cap free
Amount of items: 3
Items: 
Size: 358784 Color: 1
Size: 355753 Color: 0
Size: 285462 Color: 1

Bin 3036: 2 of cap free
Amount of items: 3
Items: 
Size: 364712 Color: 0
Size: 347544 Color: 1
Size: 287743 Color: 1

Bin 3037: 2 of cap free
Amount of items: 3
Items: 
Size: 408462 Color: 1
Size: 338198 Color: 0
Size: 253339 Color: 0

Bin 3038: 2 of cap free
Amount of items: 3
Items: 
Size: 340332 Color: 1
Size: 332626 Color: 1
Size: 327041 Color: 0

Bin 3039: 2 of cap free
Amount of items: 3
Items: 
Size: 354792 Color: 0
Size: 354237 Color: 0
Size: 290970 Color: 1

Bin 3040: 2 of cap free
Amount of items: 3
Items: 
Size: 354574 Color: 0
Size: 354155 Color: 1
Size: 291270 Color: 0

Bin 3041: 2 of cap free
Amount of items: 3
Items: 
Size: 374951 Color: 1
Size: 353544 Color: 0
Size: 271504 Color: 1

Bin 3042: 2 of cap free
Amount of items: 3
Items: 
Size: 355088 Color: 1
Size: 345887 Color: 1
Size: 299024 Color: 0

Bin 3043: 2 of cap free
Amount of items: 3
Items: 
Size: 356781 Color: 0
Size: 336761 Color: 1
Size: 306457 Color: 1

Bin 3044: 2 of cap free
Amount of items: 3
Items: 
Size: 356965 Color: 1
Size: 321976 Color: 0
Size: 321058 Color: 1

Bin 3045: 2 of cap free
Amount of items: 3
Items: 
Size: 432561 Color: 0
Size: 290883 Color: 0
Size: 276555 Color: 1

Bin 3046: 2 of cap free
Amount of items: 3
Items: 
Size: 354491 Color: 1
Size: 330680 Color: 0
Size: 314828 Color: 0

Bin 3047: 2 of cap free
Amount of items: 3
Items: 
Size: 354043 Color: 1
Size: 323565 Color: 1
Size: 322391 Color: 0

Bin 3048: 2 of cap free
Amount of items: 3
Items: 
Size: 349825 Color: 1
Size: 344931 Color: 1
Size: 305243 Color: 0

Bin 3049: 2 of cap free
Amount of items: 3
Items: 
Size: 373217 Color: 1
Size: 349582 Color: 0
Size: 277200 Color: 0

Bin 3050: 2 of cap free
Amount of items: 3
Items: 
Size: 343579 Color: 0
Size: 342826 Color: 1
Size: 313594 Color: 1

Bin 3051: 2 of cap free
Amount of items: 3
Items: 
Size: 344389 Color: 0
Size: 343458 Color: 0
Size: 312152 Color: 1

Bin 3052: 2 of cap free
Amount of items: 3
Items: 
Size: 349390 Color: 0
Size: 342684 Color: 0
Size: 307925 Color: 1

Bin 3053: 2 of cap free
Amount of items: 3
Items: 
Size: 340467 Color: 1
Size: 340096 Color: 1
Size: 319436 Color: 0

Bin 3054: 2 of cap free
Amount of items: 3
Items: 
Size: 497948 Color: 0
Size: 251550 Color: 0
Size: 250501 Color: 1

Bin 3055: 2 of cap free
Amount of items: 3
Items: 
Size: 444945 Color: 0
Size: 280436 Color: 1
Size: 274618 Color: 1

Bin 3056: 3 of cap free
Amount of items: 3
Items: 
Size: 340671 Color: 1
Size: 339141 Color: 0
Size: 320186 Color: 0

Bin 3057: 3 of cap free
Amount of items: 3
Items: 
Size: 448743 Color: 1
Size: 286337 Color: 0
Size: 264918 Color: 1

Bin 3058: 3 of cap free
Amount of items: 3
Items: 
Size: 349832 Color: 0
Size: 345814 Color: 1
Size: 304352 Color: 0

Bin 3059: 3 of cap free
Amount of items: 3
Items: 
Size: 353475 Color: 1
Size: 335337 Color: 0
Size: 311186 Color: 1

Bin 3060: 3 of cap free
Amount of items: 3
Items: 
Size: 365126 Color: 0
Size: 341646 Color: 1
Size: 293226 Color: 0

Bin 3061: 3 of cap free
Amount of items: 3
Items: 
Size: 499675 Color: 1
Size: 250191 Color: 0
Size: 250132 Color: 0

Bin 3062: 3 of cap free
Amount of items: 3
Items: 
Size: 478429 Color: 1
Size: 261854 Color: 0
Size: 259715 Color: 1

Bin 3063: 3 of cap free
Amount of items: 3
Items: 
Size: 363169 Color: 0
Size: 363089 Color: 1
Size: 273740 Color: 1

Bin 3064: 3 of cap free
Amount of items: 3
Items: 
Size: 498866 Color: 1
Size: 250769 Color: 1
Size: 250363 Color: 0

Bin 3065: 3 of cap free
Amount of items: 3
Items: 
Size: 410409 Color: 0
Size: 337108 Color: 0
Size: 252481 Color: 1

Bin 3066: 3 of cap free
Amount of items: 3
Items: 
Size: 429252 Color: 0
Size: 305294 Color: 0
Size: 265452 Color: 1

Bin 3067: 3 of cap free
Amount of items: 3
Items: 
Size: 393223 Color: 0
Size: 319445 Color: 1
Size: 287330 Color: 1

Bin 3068: 3 of cap free
Amount of items: 3
Items: 
Size: 474731 Color: 0
Size: 269911 Color: 1
Size: 255356 Color: 1

Bin 3069: 3 of cap free
Amount of items: 3
Items: 
Size: 338479 Color: 0
Size: 333075 Color: 0
Size: 328444 Color: 1

Bin 3070: 3 of cap free
Amount of items: 3
Items: 
Size: 346856 Color: 1
Size: 335209 Color: 0
Size: 317933 Color: 1

Bin 3071: 3 of cap free
Amount of items: 3
Items: 
Size: 352606 Color: 1
Size: 336827 Color: 1
Size: 310565 Color: 0

Bin 3072: 3 of cap free
Amount of items: 3
Items: 
Size: 404975 Color: 1
Size: 339124 Color: 1
Size: 255899 Color: 0

Bin 3073: 3 of cap free
Amount of items: 3
Items: 
Size: 342441 Color: 0
Size: 341261 Color: 1
Size: 316296 Color: 0

Bin 3074: 3 of cap free
Amount of items: 3
Items: 
Size: 362367 Color: 0
Size: 350727 Color: 1
Size: 286904 Color: 1

Bin 3075: 3 of cap free
Amount of items: 3
Items: 
Size: 428781 Color: 0
Size: 296542 Color: 0
Size: 274675 Color: 1

Bin 3076: 3 of cap free
Amount of items: 3
Items: 
Size: 356648 Color: 0
Size: 329579 Color: 1
Size: 313771 Color: 0

Bin 3077: 3 of cap free
Amount of items: 3
Items: 
Size: 354411 Color: 0
Size: 326731 Color: 0
Size: 318856 Color: 1

Bin 3078: 3 of cap free
Amount of items: 3
Items: 
Size: 361853 Color: 1
Size: 353107 Color: 0
Size: 285038 Color: 0

Bin 3079: 3 of cap free
Amount of items: 3
Items: 
Size: 356180 Color: 0
Size: 355051 Color: 1
Size: 288767 Color: 1

Bin 3080: 3 of cap free
Amount of items: 3
Items: 
Size: 389033 Color: 0
Size: 346736 Color: 0
Size: 264229 Color: 1

Bin 3081: 3 of cap free
Amount of items: 3
Items: 
Size: 419982 Color: 1
Size: 316802 Color: 1
Size: 263214 Color: 0

Bin 3082: 3 of cap free
Amount of items: 3
Items: 
Size: 348791 Color: 1
Size: 347358 Color: 1
Size: 303849 Color: 0

Bin 3083: 3 of cap free
Amount of items: 3
Items: 
Size: 415678 Color: 0
Size: 310409 Color: 1
Size: 273911 Color: 0

Bin 3084: 3 of cap free
Amount of items: 3
Items: 
Size: 353726 Color: 0
Size: 346245 Color: 1
Size: 300027 Color: 0

Bin 3085: 3 of cap free
Amount of items: 3
Items: 
Size: 348994 Color: 1
Size: 330086 Color: 1
Size: 320918 Color: 0

Bin 3086: 3 of cap free
Amount of items: 3
Items: 
Size: 403200 Color: 0
Size: 327950 Color: 1
Size: 268848 Color: 1

Bin 3087: 3 of cap free
Amount of items: 3
Items: 
Size: 439898 Color: 1
Size: 290778 Color: 1
Size: 269322 Color: 0

Bin 3088: 3 of cap free
Amount of items: 3
Items: 
Size: 379446 Color: 0
Size: 339016 Color: 1
Size: 281536 Color: 0

Bin 3089: 3 of cap free
Amount of items: 3
Items: 
Size: 341542 Color: 1
Size: 340731 Color: 0
Size: 317725 Color: 1

Bin 3090: 4 of cap free
Amount of items: 3
Items: 
Size: 356593 Color: 0
Size: 322671 Color: 1
Size: 320733 Color: 0

Bin 3091: 4 of cap free
Amount of items: 3
Items: 
Size: 380618 Color: 0
Size: 349066 Color: 1
Size: 270313 Color: 1

Bin 3092: 4 of cap free
Amount of items: 3
Items: 
Size: 418318 Color: 1
Size: 329934 Color: 0
Size: 251745 Color: 0

Bin 3093: 4 of cap free
Amount of items: 3
Items: 
Size: 393138 Color: 0
Size: 342243 Color: 1
Size: 264616 Color: 0

Bin 3094: 4 of cap free
Amount of items: 3
Items: 
Size: 420127 Color: 1
Size: 318128 Color: 1
Size: 261742 Color: 0

Bin 3095: 4 of cap free
Amount of items: 3
Items: 
Size: 497463 Color: 1
Size: 252453 Color: 0
Size: 250081 Color: 1

Bin 3096: 4 of cap free
Amount of items: 3
Items: 
Size: 414929 Color: 1
Size: 292756 Color: 1
Size: 292312 Color: 0

Bin 3097: 4 of cap free
Amount of items: 3
Items: 
Size: 499045 Color: 0
Size: 250754 Color: 0
Size: 250198 Color: 1

Bin 3098: 4 of cap free
Amount of items: 3
Items: 
Size: 379347 Color: 1
Size: 343445 Color: 0
Size: 277205 Color: 0

Bin 3099: 4 of cap free
Amount of items: 3
Items: 
Size: 350941 Color: 1
Size: 346964 Color: 1
Size: 302092 Color: 0

Bin 3100: 4 of cap free
Amount of items: 3
Items: 
Size: 380001 Color: 1
Size: 352069 Color: 0
Size: 267927 Color: 1

Bin 3101: 4 of cap free
Amount of items: 3
Items: 
Size: 365170 Color: 1
Size: 332502 Color: 1
Size: 302325 Color: 0

Bin 3102: 4 of cap free
Amount of items: 3
Items: 
Size: 355633 Color: 1
Size: 346283 Color: 1
Size: 298081 Color: 0

Bin 3103: 4 of cap free
Amount of items: 3
Items: 
Size: 471493 Color: 1
Size: 264802 Color: 1
Size: 263702 Color: 0

Bin 3104: 4 of cap free
Amount of items: 3
Items: 
Size: 445522 Color: 0
Size: 281989 Color: 1
Size: 272486 Color: 1

Bin 3105: 5 of cap free
Amount of items: 3
Items: 
Size: 487633 Color: 1
Size: 256825 Color: 0
Size: 255538 Color: 1

Bin 3106: 5 of cap free
Amount of items: 3
Items: 
Size: 348516 Color: 1
Size: 338261 Color: 0
Size: 313219 Color: 1

Bin 3107: 5 of cap free
Amount of items: 3
Items: 
Size: 470711 Color: 0
Size: 266579 Color: 1
Size: 262706 Color: 0

Bin 3108: 5 of cap free
Amount of items: 3
Items: 
Size: 489161 Color: 1
Size: 258579 Color: 0
Size: 252256 Color: 1

Bin 3109: 5 of cap free
Amount of items: 3
Items: 
Size: 472574 Color: 1
Size: 275448 Color: 0
Size: 251974 Color: 0

Bin 3110: 5 of cap free
Amount of items: 3
Items: 
Size: 364885 Color: 1
Size: 357186 Color: 1
Size: 277925 Color: 0

Bin 3111: 5 of cap free
Amount of items: 3
Items: 
Size: 356248 Color: 0
Size: 327680 Color: 1
Size: 316068 Color: 0

Bin 3112: 5 of cap free
Amount of items: 3
Items: 
Size: 413383 Color: 1
Size: 327836 Color: 1
Size: 258777 Color: 0

Bin 3113: 5 of cap free
Amount of items: 3
Items: 
Size: 349889 Color: 1
Size: 327936 Color: 1
Size: 322171 Color: 0

Bin 3114: 5 of cap free
Amount of items: 3
Items: 
Size: 336589 Color: 0
Size: 332040 Color: 1
Size: 331367 Color: 0

Bin 3115: 5 of cap free
Amount of items: 3
Items: 
Size: 347993 Color: 0
Size: 334021 Color: 1
Size: 317982 Color: 1

Bin 3116: 5 of cap free
Amount of items: 3
Items: 
Size: 378630 Color: 1
Size: 355937 Color: 1
Size: 265429 Color: 0

Bin 3117: 5 of cap free
Amount of items: 3
Items: 
Size: 352057 Color: 1
Size: 335378 Color: 1
Size: 312561 Color: 0

Bin 3118: 5 of cap free
Amount of items: 3
Items: 
Size: 372937 Color: 1
Size: 351737 Color: 1
Size: 275322 Color: 0

Bin 3119: 5 of cap free
Amount of items: 3
Items: 
Size: 337864 Color: 0
Size: 332163 Color: 0
Size: 329969 Color: 1

Bin 3120: 5 of cap free
Amount of items: 3
Items: 
Size: 421573 Color: 0
Size: 312384 Color: 0
Size: 266039 Color: 1

Bin 3121: 6 of cap free
Amount of items: 3
Items: 
Size: 373899 Color: 0
Size: 331986 Color: 1
Size: 294110 Color: 0

Bin 3122: 6 of cap free
Amount of items: 3
Items: 
Size: 346442 Color: 1
Size: 327093 Color: 1
Size: 326460 Color: 0

Bin 3123: 6 of cap free
Amount of items: 3
Items: 
Size: 341646 Color: 1
Size: 334878 Color: 1
Size: 323471 Color: 0

Bin 3124: 6 of cap free
Amount of items: 3
Items: 
Size: 498806 Color: 1
Size: 250972 Color: 0
Size: 250217 Color: 1

Bin 3125: 6 of cap free
Amount of items: 3
Items: 
Size: 406166 Color: 0
Size: 341955 Color: 1
Size: 251874 Color: 0

Bin 3126: 6 of cap free
Amount of items: 3
Items: 
Size: 417049 Color: 0
Size: 303646 Color: 1
Size: 279300 Color: 1

Bin 3127: 6 of cap free
Amount of items: 3
Items: 
Size: 429332 Color: 0
Size: 285766 Color: 1
Size: 284897 Color: 1

Bin 3128: 6 of cap free
Amount of items: 3
Items: 
Size: 381727 Color: 0
Size: 342037 Color: 1
Size: 276231 Color: 0

Bin 3129: 6 of cap free
Amount of items: 3
Items: 
Size: 342901 Color: 0
Size: 331433 Color: 0
Size: 325661 Color: 1

Bin 3130: 6 of cap free
Amount of items: 3
Items: 
Size: 353046 Color: 0
Size: 332748 Color: 0
Size: 314201 Color: 1

Bin 3131: 6 of cap free
Amount of items: 3
Items: 
Size: 346170 Color: 0
Size: 339065 Color: 0
Size: 314760 Color: 1

Bin 3132: 6 of cap free
Amount of items: 3
Items: 
Size: 405983 Color: 0
Size: 329144 Color: 1
Size: 264868 Color: 0

Bin 3133: 6 of cap free
Amount of items: 3
Items: 
Size: 395239 Color: 0
Size: 326991 Color: 1
Size: 277765 Color: 1

Bin 3134: 6 of cap free
Amount of items: 3
Items: 
Size: 350439 Color: 0
Size: 350256 Color: 1
Size: 299300 Color: 0

Bin 3135: 6 of cap free
Amount of items: 3
Items: 
Size: 357388 Color: 1
Size: 327260 Color: 1
Size: 315347 Color: 0

Bin 3136: 6 of cap free
Amount of items: 3
Items: 
Size: 388619 Color: 0
Size: 355726 Color: 1
Size: 255650 Color: 1

Bin 3137: 7 of cap free
Amount of items: 3
Items: 
Size: 336103 Color: 0
Size: 335038 Color: 1
Size: 328853 Color: 0

Bin 3138: 7 of cap free
Amount of items: 3
Items: 
Size: 470236 Color: 1
Size: 271005 Color: 0
Size: 258753 Color: 1

Bin 3139: 7 of cap free
Amount of items: 3
Items: 
Size: 333842 Color: 1
Size: 333633 Color: 0
Size: 332519 Color: 1

Bin 3140: 7 of cap free
Amount of items: 3
Items: 
Size: 359030 Color: 1
Size: 334041 Color: 0
Size: 306923 Color: 1

Bin 3141: 7 of cap free
Amount of items: 3
Items: 
Size: 354097 Color: 0
Size: 344014 Color: 1
Size: 301883 Color: 0

Bin 3142: 7 of cap free
Amount of items: 3
Items: 
Size: 434902 Color: 1
Size: 287252 Color: 0
Size: 277840 Color: 1

Bin 3143: 8 of cap free
Amount of items: 3
Items: 
Size: 443535 Color: 1
Size: 290542 Color: 0
Size: 265916 Color: 0

Bin 3144: 8 of cap free
Amount of items: 3
Items: 
Size: 490279 Color: 1
Size: 255610 Color: 0
Size: 254104 Color: 1

Bin 3145: 8 of cap free
Amount of items: 3
Items: 
Size: 479245 Color: 0
Size: 262105 Color: 1
Size: 258643 Color: 1

Bin 3146: 8 of cap free
Amount of items: 3
Items: 
Size: 342243 Color: 0
Size: 340048 Color: 1
Size: 317702 Color: 0

Bin 3147: 8 of cap free
Amount of items: 3
Items: 
Size: 393032 Color: 1
Size: 328938 Color: 1
Size: 278023 Color: 0

Bin 3148: 8 of cap free
Amount of items: 3
Items: 
Size: 494609 Color: 0
Size: 254061 Color: 1
Size: 251323 Color: 1

Bin 3149: 8 of cap free
Amount of items: 3
Items: 
Size: 358852 Color: 1
Size: 329698 Color: 1
Size: 311443 Color: 0

Bin 3150: 8 of cap free
Amount of items: 3
Items: 
Size: 348561 Color: 1
Size: 347858 Color: 0
Size: 303574 Color: 1

Bin 3151: 8 of cap free
Amount of items: 3
Items: 
Size: 354037 Color: 1
Size: 328926 Color: 0
Size: 317030 Color: 1

Bin 3152: 8 of cap free
Amount of items: 3
Items: 
Size: 352175 Color: 0
Size: 343500 Color: 1
Size: 304318 Color: 1

Bin 3153: 8 of cap free
Amount of items: 3
Items: 
Size: 349923 Color: 1
Size: 348154 Color: 1
Size: 301916 Color: 0

Bin 3154: 8 of cap free
Amount of items: 3
Items: 
Size: 350058 Color: 1
Size: 340975 Color: 0
Size: 308960 Color: 1

Bin 3155: 9 of cap free
Amount of items: 3
Items: 
Size: 425083 Color: 1
Size: 302401 Color: 0
Size: 272508 Color: 1

Bin 3156: 9 of cap free
Amount of items: 3
Items: 
Size: 478411 Color: 0
Size: 261422 Color: 1
Size: 260159 Color: 0

Bin 3157: 9 of cap free
Amount of items: 3
Items: 
Size: 422657 Color: 0
Size: 296787 Color: 0
Size: 280548 Color: 1

Bin 3158: 9 of cap free
Amount of items: 3
Items: 
Size: 478769 Color: 0
Size: 261581 Color: 1
Size: 259642 Color: 1

Bin 3159: 9 of cap free
Amount of items: 3
Items: 
Size: 410249 Color: 1
Size: 319321 Color: 0
Size: 270422 Color: 0

Bin 3160: 9 of cap free
Amount of items: 3
Items: 
Size: 350567 Color: 0
Size: 336313 Color: 0
Size: 313112 Color: 1

Bin 3161: 9 of cap free
Amount of items: 3
Items: 
Size: 360187 Color: 0
Size: 356155 Color: 1
Size: 283650 Color: 0

Bin 3162: 9 of cap free
Amount of items: 3
Items: 
Size: 375779 Color: 1
Size: 352554 Color: 0
Size: 271659 Color: 0

Bin 3163: 9 of cap free
Amount of items: 3
Items: 
Size: 489208 Color: 0
Size: 256153 Color: 1
Size: 254631 Color: 1

Bin 3164: 10 of cap free
Amount of items: 3
Items: 
Size: 374398 Color: 1
Size: 328560 Color: 0
Size: 297033 Color: 1

Bin 3165: 10 of cap free
Amount of items: 3
Items: 
Size: 419030 Color: 1
Size: 314300 Color: 0
Size: 266661 Color: 1

Bin 3166: 10 of cap free
Amount of items: 3
Items: 
Size: 469088 Color: 0
Size: 266101 Color: 0
Size: 264802 Color: 1

Bin 3167: 10 of cap free
Amount of items: 3
Items: 
Size: 493014 Color: 1
Size: 255341 Color: 0
Size: 251636 Color: 0

Bin 3168: 10 of cap free
Amount of items: 3
Items: 
Size: 446947 Color: 0
Size: 291648 Color: 0
Size: 261396 Color: 1

Bin 3169: 10 of cap free
Amount of items: 3
Items: 
Size: 452859 Color: 0
Size: 277491 Color: 0
Size: 269641 Color: 1

Bin 3170: 10 of cap free
Amount of items: 3
Items: 
Size: 398595 Color: 1
Size: 312065 Color: 0
Size: 289331 Color: 0

Bin 3171: 10 of cap free
Amount of items: 3
Items: 
Size: 349182 Color: 1
Size: 325926 Color: 0
Size: 324883 Color: 0

Bin 3172: 10 of cap free
Amount of items: 3
Items: 
Size: 350530 Color: 0
Size: 347224 Color: 0
Size: 302237 Color: 1

Bin 3173: 10 of cap free
Amount of items: 3
Items: 
Size: 343703 Color: 1
Size: 337828 Color: 1
Size: 318460 Color: 0

Bin 3174: 11 of cap free
Amount of items: 3
Items: 
Size: 437598 Color: 0
Size: 290015 Color: 1
Size: 272377 Color: 1

Bin 3175: 11 of cap free
Amount of items: 3
Items: 
Size: 341798 Color: 1
Size: 331538 Color: 0
Size: 326654 Color: 0

Bin 3176: 11 of cap free
Amount of items: 3
Items: 
Size: 471878 Color: 0
Size: 264362 Color: 0
Size: 263750 Color: 1

Bin 3177: 11 of cap free
Amount of items: 3
Items: 
Size: 399583 Color: 1
Size: 326292 Color: 0
Size: 274115 Color: 0

Bin 3178: 11 of cap free
Amount of items: 3
Items: 
Size: 355062 Color: 1
Size: 342595 Color: 1
Size: 302333 Color: 0

Bin 3179: 12 of cap free
Amount of items: 3
Items: 
Size: 372192 Color: 1
Size: 340488 Color: 0
Size: 287309 Color: 0

Bin 3180: 12 of cap free
Amount of items: 3
Items: 
Size: 392528 Color: 0
Size: 310890 Color: 1
Size: 296571 Color: 1

Bin 3181: 12 of cap free
Amount of items: 3
Items: 
Size: 375606 Color: 1
Size: 327812 Color: 0
Size: 296571 Color: 0

Bin 3182: 12 of cap free
Amount of items: 3
Items: 
Size: 413075 Color: 1
Size: 333791 Color: 0
Size: 253123 Color: 1

Bin 3183: 12 of cap free
Amount of items: 3
Items: 
Size: 370416 Color: 0
Size: 343049 Color: 1
Size: 286524 Color: 0

Bin 3184: 12 of cap free
Amount of items: 3
Items: 
Size: 351946 Color: 1
Size: 327774 Color: 0
Size: 320269 Color: 1

Bin 3185: 12 of cap free
Amount of items: 3
Items: 
Size: 337897 Color: 1
Size: 332680 Color: 0
Size: 329412 Color: 1

Bin 3186: 12 of cap free
Amount of items: 3
Items: 
Size: 497779 Color: 0
Size: 252210 Color: 0
Size: 250000 Color: 1

Bin 3187: 13 of cap free
Amount of items: 3
Items: 
Size: 368809 Color: 0
Size: 346375 Color: 0
Size: 284804 Color: 1

Bin 3188: 13 of cap free
Amount of items: 3
Items: 
Size: 450424 Color: 0
Size: 282847 Color: 1
Size: 266717 Color: 1

Bin 3189: 13 of cap free
Amount of items: 3
Items: 
Size: 405696 Color: 0
Size: 330824 Color: 1
Size: 263468 Color: 0

Bin 3190: 14 of cap free
Amount of items: 3
Items: 
Size: 360467 Color: 1
Size: 341591 Color: 0
Size: 297929 Color: 1

Bin 3191: 14 of cap free
Amount of items: 3
Items: 
Size: 456831 Color: 0
Size: 276016 Color: 1
Size: 267140 Color: 0

Bin 3192: 14 of cap free
Amount of items: 3
Items: 
Size: 482873 Color: 1
Size: 260501 Color: 1
Size: 256613 Color: 0

Bin 3193: 14 of cap free
Amount of items: 3
Items: 
Size: 356500 Color: 1
Size: 334227 Color: 0
Size: 309260 Color: 1

Bin 3194: 14 of cap free
Amount of items: 3
Items: 
Size: 337038 Color: 0
Size: 332261 Color: 1
Size: 330688 Color: 1

Bin 3195: 14 of cap free
Amount of items: 3
Items: 
Size: 418724 Color: 0
Size: 321569 Color: 1
Size: 259694 Color: 1

Bin 3196: 15 of cap free
Amount of items: 3
Items: 
Size: 482181 Color: 0
Size: 259338 Color: 0
Size: 258467 Color: 1

Bin 3197: 15 of cap free
Amount of items: 3
Items: 
Size: 498632 Color: 0
Size: 251072 Color: 1
Size: 250282 Color: 0

Bin 3198: 15 of cap free
Amount of items: 3
Items: 
Size: 357469 Color: 1
Size: 350993 Color: 0
Size: 291524 Color: 0

Bin 3199: 15 of cap free
Amount of items: 3
Items: 
Size: 428051 Color: 0
Size: 289010 Color: 1
Size: 282925 Color: 0

Bin 3200: 16 of cap free
Amount of items: 3
Items: 
Size: 383354 Color: 1
Size: 341171 Color: 0
Size: 275460 Color: 1

Bin 3201: 17 of cap free
Amount of items: 2
Items: 
Size: 499999 Color: 1
Size: 499985 Color: 0

Bin 3202: 17 of cap free
Amount of items: 3
Items: 
Size: 353428 Color: 0
Size: 340321 Color: 1
Size: 306235 Color: 1

Bin 3203: 17 of cap free
Amount of items: 3
Items: 
Size: 337645 Color: 1
Size: 334841 Color: 1
Size: 327498 Color: 0

Bin 3204: 17 of cap free
Amount of items: 3
Items: 
Size: 467209 Color: 1
Size: 267101 Color: 1
Size: 265674 Color: 0

Bin 3205: 17 of cap free
Amount of items: 3
Items: 
Size: 365010 Color: 0
Size: 354110 Color: 1
Size: 280864 Color: 1

Bin 3206: 17 of cap free
Amount of items: 3
Items: 
Size: 427888 Color: 0
Size: 294174 Color: 1
Size: 277922 Color: 1

Bin 3207: 18 of cap free
Amount of items: 3
Items: 
Size: 459457 Color: 0
Size: 271561 Color: 0
Size: 268965 Color: 1

Bin 3208: 18 of cap free
Amount of items: 3
Items: 
Size: 425696 Color: 0
Size: 294086 Color: 1
Size: 280201 Color: 1

Bin 3209: 18 of cap free
Amount of items: 3
Items: 
Size: 347646 Color: 0
Size: 332828 Color: 1
Size: 319509 Color: 0

Bin 3210: 18 of cap free
Amount of items: 3
Items: 
Size: 357897 Color: 0
Size: 353940 Color: 1
Size: 288146 Color: 0

Bin 3211: 19 of cap free
Amount of items: 3
Items: 
Size: 431843 Color: 1
Size: 314622 Color: 0
Size: 253517 Color: 0

Bin 3212: 19 of cap free
Amount of items: 3
Items: 
Size: 494121 Color: 0
Size: 254303 Color: 1
Size: 251558 Color: 0

Bin 3213: 19 of cap free
Amount of items: 3
Items: 
Size: 346337 Color: 1
Size: 342861 Color: 1
Size: 310784 Color: 0

Bin 3214: 19 of cap free
Amount of items: 3
Items: 
Size: 354267 Color: 0
Size: 347199 Color: 0
Size: 298516 Color: 1

Bin 3215: 19 of cap free
Amount of items: 3
Items: 
Size: 429754 Color: 0
Size: 295406 Color: 1
Size: 274822 Color: 0

Bin 3216: 20 of cap free
Amount of items: 3
Items: 
Size: 403308 Color: 0
Size: 334032 Color: 0
Size: 262641 Color: 1

Bin 3217: 20 of cap free
Amount of items: 3
Items: 
Size: 476891 Color: 1
Size: 261637 Color: 1
Size: 261453 Color: 0

Bin 3218: 20 of cap free
Amount of items: 3
Items: 
Size: 350713 Color: 1
Size: 338067 Color: 0
Size: 311201 Color: 1

Bin 3219: 20 of cap free
Amount of items: 3
Items: 
Size: 352942 Color: 0
Size: 341605 Color: 1
Size: 305434 Color: 1

Bin 3220: 21 of cap free
Amount of items: 3
Items: 
Size: 360004 Color: 1
Size: 343478 Color: 0
Size: 296498 Color: 1

Bin 3221: 21 of cap free
Amount of items: 3
Items: 
Size: 343547 Color: 0
Size: 328262 Color: 1
Size: 328171 Color: 1

Bin 3222: 22 of cap free
Amount of items: 3
Items: 
Size: 412771 Color: 1
Size: 301072 Color: 0
Size: 286136 Color: 1

Bin 3223: 22 of cap free
Amount of items: 3
Items: 
Size: 488057 Color: 0
Size: 256112 Color: 0
Size: 255810 Color: 1

Bin 3224: 22 of cap free
Amount of items: 3
Items: 
Size: 361068 Color: 0
Size: 351473 Color: 0
Size: 287438 Color: 1

Bin 3225: 22 of cap free
Amount of items: 3
Items: 
Size: 497771 Color: 0
Size: 251894 Color: 0
Size: 250314 Color: 1

Bin 3226: 22 of cap free
Amount of items: 3
Items: 
Size: 410643 Color: 0
Size: 318625 Color: 1
Size: 270711 Color: 1

Bin 3227: 23 of cap free
Amount of items: 3
Items: 
Size: 375276 Color: 1
Size: 361442 Color: 0
Size: 263260 Color: 1

Bin 3228: 24 of cap free
Amount of items: 3
Items: 
Size: 374465 Color: 0
Size: 356822 Color: 0
Size: 268690 Color: 1

Bin 3229: 25 of cap free
Amount of items: 3
Items: 
Size: 475042 Color: 1
Size: 263394 Color: 0
Size: 261540 Color: 1

Bin 3230: 25 of cap free
Amount of items: 3
Items: 
Size: 355788 Color: 1
Size: 341132 Color: 1
Size: 303056 Color: 0

Bin 3231: 26 of cap free
Amount of items: 3
Items: 
Size: 461337 Color: 0
Size: 272394 Color: 1
Size: 266244 Color: 1

Bin 3232: 26 of cap free
Amount of items: 3
Items: 
Size: 483891 Color: 1
Size: 258984 Color: 1
Size: 257100 Color: 0

Bin 3233: 26 of cap free
Amount of items: 3
Items: 
Size: 428583 Color: 0
Size: 295193 Color: 1
Size: 276199 Color: 1

Bin 3234: 26 of cap free
Amount of items: 3
Items: 
Size: 415600 Color: 1
Size: 307646 Color: 0
Size: 276729 Color: 0

Bin 3235: 27 of cap free
Amount of items: 3
Items: 
Size: 499110 Color: 1
Size: 250794 Color: 0
Size: 250070 Color: 1

Bin 3236: 27 of cap free
Amount of items: 3
Items: 
Size: 402605 Color: 1
Size: 301547 Color: 0
Size: 295822 Color: 0

Bin 3237: 27 of cap free
Amount of items: 3
Items: 
Size: 486566 Color: 1
Size: 257434 Color: 0
Size: 255974 Color: 0

Bin 3238: 28 of cap free
Amount of items: 3
Items: 
Size: 390156 Color: 0
Size: 340930 Color: 0
Size: 268887 Color: 1

Bin 3239: 28 of cap free
Amount of items: 3
Items: 
Size: 356321 Color: 0
Size: 337910 Color: 1
Size: 305742 Color: 0

Bin 3240: 28 of cap free
Amount of items: 3
Items: 
Size: 361330 Color: 1
Size: 348147 Color: 1
Size: 290496 Color: 0

Bin 3241: 28 of cap free
Amount of items: 3
Items: 
Size: 441707 Color: 0
Size: 305501 Color: 1
Size: 252765 Color: 0

Bin 3242: 29 of cap free
Amount of items: 3
Items: 
Size: 354922 Color: 0
Size: 329015 Color: 0
Size: 316035 Color: 1

Bin 3243: 29 of cap free
Amount of items: 3
Items: 
Size: 381069 Color: 1
Size: 326578 Color: 1
Size: 292325 Color: 0

Bin 3244: 30 of cap free
Amount of items: 3
Items: 
Size: 407665 Color: 1
Size: 305963 Color: 1
Size: 286343 Color: 0

Bin 3245: 30 of cap free
Amount of items: 3
Items: 
Size: 433955 Color: 1
Size: 297220 Color: 1
Size: 268796 Color: 0

Bin 3246: 31 of cap free
Amount of items: 3
Items: 
Size: 448977 Color: 1
Size: 282616 Color: 0
Size: 268377 Color: 0

Bin 3247: 31 of cap free
Amount of items: 3
Items: 
Size: 384339 Color: 1
Size: 333085 Color: 0
Size: 282546 Color: 1

Bin 3248: 32 of cap free
Amount of items: 3
Items: 
Size: 377619 Color: 0
Size: 356648 Color: 0
Size: 265702 Color: 1

Bin 3249: 33 of cap free
Amount of items: 3
Items: 
Size: 366934 Color: 0
Size: 344655 Color: 0
Size: 288379 Color: 1

Bin 3250: 33 of cap free
Amount of items: 3
Items: 
Size: 412888 Color: 1
Size: 321075 Color: 0
Size: 266005 Color: 0

Bin 3251: 33 of cap free
Amount of items: 3
Items: 
Size: 379343 Color: 0
Size: 349990 Color: 0
Size: 270635 Color: 1

Bin 3252: 36 of cap free
Amount of items: 3
Items: 
Size: 466719 Color: 0
Size: 267432 Color: 1
Size: 265814 Color: 0

Bin 3253: 36 of cap free
Amount of items: 3
Items: 
Size: 356820 Color: 0
Size: 327774 Color: 1
Size: 315371 Color: 0

Bin 3254: 36 of cap free
Amount of items: 3
Items: 
Size: 349731 Color: 1
Size: 332439 Color: 1
Size: 317795 Color: 0

Bin 3255: 37 of cap free
Amount of items: 3
Items: 
Size: 497859 Color: 0
Size: 251991 Color: 1
Size: 250114 Color: 1

Bin 3256: 37 of cap free
Amount of items: 3
Items: 
Size: 382902 Color: 1
Size: 342495 Color: 1
Size: 274567 Color: 0

Bin 3257: 37 of cap free
Amount of items: 3
Items: 
Size: 498854 Color: 1
Size: 250654 Color: 1
Size: 250456 Color: 0

Bin 3258: 38 of cap free
Amount of items: 3
Items: 
Size: 371031 Color: 0
Size: 335481 Color: 1
Size: 293451 Color: 1

Bin 3259: 40 of cap free
Amount of items: 3
Items: 
Size: 468298 Color: 1
Size: 267016 Color: 1
Size: 264647 Color: 0

Bin 3260: 43 of cap free
Amount of items: 3
Items: 
Size: 450462 Color: 1
Size: 278007 Color: 0
Size: 271489 Color: 0

Bin 3261: 44 of cap free
Amount of items: 3
Items: 
Size: 406754 Color: 0
Size: 325719 Color: 0
Size: 267484 Color: 1

Bin 3262: 44 of cap free
Amount of items: 3
Items: 
Size: 358618 Color: 1
Size: 324780 Color: 0
Size: 316559 Color: 0

Bin 3263: 45 of cap free
Amount of items: 3
Items: 
Size: 465433 Color: 1
Size: 279405 Color: 0
Size: 255118 Color: 1

Bin 3264: 46 of cap free
Amount of items: 3
Items: 
Size: 349988 Color: 1
Size: 335531 Color: 0
Size: 314436 Color: 1

Bin 3265: 47 of cap free
Amount of items: 3
Items: 
Size: 438805 Color: 0
Size: 283619 Color: 1
Size: 277530 Color: 1

Bin 3266: 52 of cap free
Amount of items: 3
Items: 
Size: 479621 Color: 1
Size: 260673 Color: 1
Size: 259655 Color: 0

Bin 3267: 56 of cap free
Amount of items: 3
Items: 
Size: 348036 Color: 0
Size: 326762 Color: 0
Size: 325147 Color: 1

Bin 3268: 56 of cap free
Amount of items: 3
Items: 
Size: 371980 Color: 1
Size: 329207 Color: 0
Size: 298758 Color: 0

Bin 3269: 57 of cap free
Amount of items: 3
Items: 
Size: 338964 Color: 1
Size: 330786 Color: 0
Size: 330194 Color: 1

Bin 3270: 57 of cap free
Amount of items: 3
Items: 
Size: 419597 Color: 1
Size: 308243 Color: 1
Size: 272104 Color: 0

Bin 3271: 58 of cap free
Amount of items: 3
Items: 
Size: 418952 Color: 0
Size: 327053 Color: 1
Size: 253938 Color: 1

Bin 3272: 59 of cap free
Amount of items: 3
Items: 
Size: 412340 Color: 1
Size: 308726 Color: 0
Size: 278876 Color: 1

Bin 3273: 60 of cap free
Amount of items: 3
Items: 
Size: 355265 Color: 0
Size: 342081 Color: 1
Size: 302595 Color: 0

Bin 3274: 61 of cap free
Amount of items: 3
Items: 
Size: 494950 Color: 1
Size: 252642 Color: 1
Size: 252348 Color: 0

Bin 3275: 62 of cap free
Amount of items: 3
Items: 
Size: 418700 Color: 0
Size: 293893 Color: 1
Size: 287346 Color: 1

Bin 3276: 63 of cap free
Amount of items: 3
Items: 
Size: 411012 Color: 0
Size: 322044 Color: 1
Size: 266882 Color: 1

Bin 3277: 64 of cap free
Amount of items: 3
Items: 
Size: 385749 Color: 1
Size: 319816 Color: 0
Size: 294372 Color: 1

Bin 3278: 67 of cap free
Amount of items: 3
Items: 
Size: 353983 Color: 0
Size: 351119 Color: 0
Size: 294832 Color: 1

Bin 3279: 67 of cap free
Amount of items: 3
Items: 
Size: 434343 Color: 0
Size: 294701 Color: 1
Size: 270890 Color: 1

Bin 3280: 70 of cap free
Amount of items: 3
Items: 
Size: 497699 Color: 0
Size: 252044 Color: 1
Size: 250188 Color: 1

Bin 3281: 70 of cap free
Amount of items: 3
Items: 
Size: 371401 Color: 1
Size: 358264 Color: 0
Size: 270266 Color: 0

Bin 3282: 79 of cap free
Amount of items: 3
Items: 
Size: 451064 Color: 0
Size: 280623 Color: 0
Size: 268235 Color: 1

Bin 3283: 83 of cap free
Amount of items: 3
Items: 
Size: 495607 Color: 1
Size: 254212 Color: 0
Size: 250099 Color: 0

Bin 3284: 92 of cap free
Amount of items: 3
Items: 
Size: 454175 Color: 0
Size: 278857 Color: 0
Size: 266877 Color: 1

Bin 3285: 94 of cap free
Amount of items: 3
Items: 
Size: 421275 Color: 1
Size: 320660 Color: 0
Size: 257972 Color: 0

Bin 3286: 101 of cap free
Amount of items: 3
Items: 
Size: 336640 Color: 1
Size: 335254 Color: 0
Size: 328006 Color: 0

Bin 3287: 109 of cap free
Amount of items: 3
Items: 
Size: 451873 Color: 1
Size: 291448 Color: 1
Size: 256571 Color: 0

Bin 3288: 111 of cap free
Amount of items: 3
Items: 
Size: 465246 Color: 0
Size: 272252 Color: 0
Size: 262392 Color: 1

Bin 3289: 112 of cap free
Amount of items: 3
Items: 
Size: 411905 Color: 0
Size: 303119 Color: 0
Size: 284865 Color: 1

Bin 3290: 120 of cap free
Amount of items: 3
Items: 
Size: 361882 Color: 1
Size: 354305 Color: 1
Size: 283694 Color: 0

Bin 3291: 127 of cap free
Amount of items: 3
Items: 
Size: 453395 Color: 1
Size: 276444 Color: 1
Size: 270035 Color: 0

Bin 3292: 137 of cap free
Amount of items: 3
Items: 
Size: 426767 Color: 1
Size: 286591 Color: 0
Size: 286506 Color: 0

Bin 3293: 145 of cap free
Amount of items: 3
Items: 
Size: 347753 Color: 0
Size: 331482 Color: 1
Size: 320621 Color: 0

Bin 3294: 149 of cap free
Amount of items: 3
Items: 
Size: 423545 Color: 0
Size: 312711 Color: 0
Size: 263596 Color: 1

Bin 3295: 149 of cap free
Amount of items: 3
Items: 
Size: 349209 Color: 0
Size: 347242 Color: 1
Size: 303401 Color: 1

Bin 3296: 155 of cap free
Amount of items: 3
Items: 
Size: 344958 Color: 1
Size: 330688 Color: 1
Size: 324200 Color: 0

Bin 3297: 157 of cap free
Amount of items: 3
Items: 
Size: 447417 Color: 0
Size: 289834 Color: 0
Size: 262593 Color: 1

Bin 3298: 181 of cap free
Amount of items: 3
Items: 
Size: 391195 Color: 1
Size: 321332 Color: 0
Size: 287293 Color: 0

Bin 3299: 198 of cap free
Amount of items: 3
Items: 
Size: 348516 Color: 1
Size: 332560 Color: 0
Size: 318727 Color: 1

Bin 3300: 199 of cap free
Amount of items: 3
Items: 
Size: 388224 Color: 0
Size: 346015 Color: 0
Size: 265563 Color: 1

Bin 3301: 205 of cap free
Amount of items: 3
Items: 
Size: 467852 Color: 1
Size: 268320 Color: 1
Size: 263624 Color: 0

Bin 3302: 205 of cap free
Amount of items: 3
Items: 
Size: 411857 Color: 0
Size: 312095 Color: 0
Size: 275844 Color: 1

Bin 3303: 213 of cap free
Amount of items: 3
Items: 
Size: 369060 Color: 0
Size: 360504 Color: 1
Size: 270224 Color: 1

Bin 3304: 245 of cap free
Amount of items: 3
Items: 
Size: 430063 Color: 0
Size: 290484 Color: 1
Size: 279209 Color: 1

Bin 3305: 245 of cap free
Amount of items: 3
Items: 
Size: 339243 Color: 1
Size: 332781 Color: 0
Size: 327732 Color: 0

Bin 3306: 265 of cap free
Amount of items: 3
Items: 
Size: 392640 Color: 0
Size: 304528 Color: 1
Size: 302568 Color: 1

Bin 3307: 267 of cap free
Amount of items: 3
Items: 
Size: 456712 Color: 0
Size: 271789 Color: 0
Size: 271233 Color: 1

Bin 3308: 279 of cap free
Amount of items: 3
Items: 
Size: 343607 Color: 0
Size: 340444 Color: 1
Size: 315671 Color: 0

Bin 3309: 425 of cap free
Amount of items: 3
Items: 
Size: 388737 Color: 1
Size: 333560 Color: 1
Size: 277279 Color: 0

Bin 3310: 538 of cap free
Amount of items: 3
Items: 
Size: 414725 Color: 1
Size: 309996 Color: 1
Size: 274742 Color: 0

Bin 3311: 648 of cap free
Amount of items: 2
Items: 
Size: 499980 Color: 1
Size: 499373 Color: 0

Bin 3312: 693 of cap free
Amount of items: 3
Items: 
Size: 421260 Color: 1
Size: 309878 Color: 0
Size: 268170 Color: 1

Bin 3313: 714 of cap free
Amount of items: 2
Items: 
Size: 499939 Color: 1
Size: 499348 Color: 0

Bin 3314: 718 of cap free
Amount of items: 3
Items: 
Size: 362311 Color: 1
Size: 320949 Color: 1
Size: 316023 Color: 0

Bin 3315: 1118 of cap free
Amount of items: 3
Items: 
Size: 350398 Color: 1
Size: 335193 Color: 0
Size: 313292 Color: 1

Bin 3316: 1249 of cap free
Amount of items: 3
Items: 
Size: 398592 Color: 0
Size: 336313 Color: 0
Size: 263847 Color: 1

Bin 3317: 1458 of cap free
Amount of items: 3
Items: 
Size: 334789 Color: 1
Size: 331984 Color: 1
Size: 331770 Color: 0

Bin 3318: 1550 of cap free
Amount of items: 3
Items: 
Size: 354651 Color: 0
Size: 345912 Color: 1
Size: 297888 Color: 1

Bin 3319: 1710 of cap free
Amount of items: 2
Items: 
Size: 499829 Color: 1
Size: 498462 Color: 0

Bin 3320: 1865 of cap free
Amount of items: 2
Items: 
Size: 499722 Color: 1
Size: 498414 Color: 0

Bin 3321: 1939 of cap free
Amount of items: 2
Items: 
Size: 499686 Color: 1
Size: 498376 Color: 0

Bin 3322: 2141 of cap free
Amount of items: 2
Items: 
Size: 499659 Color: 1
Size: 498201 Color: 0

Bin 3323: 2176 of cap free
Amount of items: 3
Items: 
Size: 367345 Color: 0
Size: 359941 Color: 1
Size: 270539 Color: 1

Bin 3324: 7322 of cap free
Amount of items: 3
Items: 
Size: 405305 Color: 0
Size: 319828 Color: 1
Size: 267546 Color: 1

Bin 3325: 11756 of cap free
Amount of items: 3
Items: 
Size: 427178 Color: 0
Size: 281421 Color: 0
Size: 279646 Color: 1

Bin 3326: 184254 of cap free
Amount of items: 3
Items: 
Size: 275779 Color: 1
Size: 273523 Color: 0
Size: 266445 Color: 1

Bin 3327: 215959 of cap free
Amount of items: 3
Items: 
Size: 261955 Color: 0
Size: 261212 Color: 0
Size: 260875 Color: 1

Bin 3328: 219186 of cap free
Amount of items: 3
Items: 
Size: 260655 Color: 1
Size: 260614 Color: 1
Size: 259546 Color: 0

Bin 3329: 220358 of cap free
Amount of items: 3
Items: 
Size: 260308 Color: 1
Size: 259973 Color: 1
Size: 259362 Color: 0

Bin 3330: 222170 of cap free
Amount of items: 3
Items: 
Size: 259414 Color: 1
Size: 259282 Color: 1
Size: 259135 Color: 0

Bin 3331: 223673 of cap free
Amount of items: 3
Items: 
Size: 258973 Color: 1
Size: 258748 Color: 0
Size: 258607 Color: 1

Bin 3332: 226864 of cap free
Amount of items: 3
Items: 
Size: 257847 Color: 0
Size: 257717 Color: 1
Size: 257573 Color: 0

Bin 3333: 228344 of cap free
Amount of items: 3
Items: 
Size: 257413 Color: 0
Size: 257282 Color: 0
Size: 256962 Color: 1

Bin 3334: 230657 of cap free
Amount of items: 3
Items: 
Size: 256789 Color: 1
Size: 256582 Color: 1
Size: 255973 Color: 0

Bin 3335: 235882 of cap free
Amount of items: 3
Items: 
Size: 255925 Color: 0
Size: 254368 Color: 0
Size: 253826 Color: 1

Bin 3336: 244906 of cap free
Amount of items: 3
Items: 
Size: 253292 Color: 0
Size: 251352 Color: 0
Size: 250451 Color: 1

Bin 3337: 501369 of cap free
Amount of items: 1
Items: 
Size: 498632 Color: 1

Total size: 3334003334
Total free space: 3000003

