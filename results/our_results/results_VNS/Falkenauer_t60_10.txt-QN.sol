Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 55
Size: 278 Color: 19
Size: 274 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 47
Size: 318 Color: 30
Size: 265 Color: 11

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 50
Size: 292 Color: 25
Size: 284 Color: 22

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 59
Size: 256 Color: 5
Size: 253 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 45
Size: 323 Color: 33
Size: 277 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 48
Size: 301 Color: 28
Size: 280 Color: 20

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 52
Size: 310 Color: 29
Size: 251 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 53
Size: 294 Color: 26
Size: 266 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 46
Size: 347 Color: 36
Size: 250 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 44
Size: 351 Color: 37
Size: 251 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 56
Size: 270 Color: 15
Size: 266 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 58
Size: 265 Color: 10
Size: 257 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 54
Size: 291 Color: 24
Size: 268 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 40
Size: 355 Color: 38
Size: 285 Color: 23

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 43
Size: 332 Color: 34
Size: 280 Color: 21

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 41
Size: 335 Color: 35
Size: 299 Color: 27

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 57
Size: 271 Color: 16
Size: 257 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 51
Size: 322 Color: 32
Size: 250 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 49
Size: 320 Color: 31
Size: 257 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 42
Size: 357 Color: 39
Size: 260 Color: 9

Total size: 20000
Total free space: 0

