Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1354 Color: 5
Size: 1014 Color: 2
Size: 96 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 10
Size: 541 Color: 9
Size: 108 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 3
Size: 578 Color: 10
Size: 68 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 6
Size: 286 Color: 7
Size: 184 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 14
Size: 413 Color: 16
Size: 46 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2045 Color: 10
Size: 331 Color: 11
Size: 88 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 16
Size: 262 Color: 14
Size: 152 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2087 Color: 9
Size: 273 Color: 8
Size: 104 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 11
Size: 306 Color: 9
Size: 60 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 3
Size: 226 Color: 0
Size: 116 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2143 Color: 9
Size: 261 Color: 3
Size: 60 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2135 Color: 18
Size: 301 Color: 1
Size: 28 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 6
Size: 267 Color: 0
Size: 44 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 0
Size: 176 Color: 11
Size: 122 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2193 Color: 14
Size: 221 Color: 12
Size: 50 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 6
Size: 214 Color: 12
Size: 56 Color: 2

Bin 17: 1 of cap free
Amount of items: 20
Items: 
Size: 275 Color: 14
Size: 176 Color: 2
Size: 172 Color: 18
Size: 168 Color: 10
Size: 160 Color: 13
Size: 148 Color: 17
Size: 148 Color: 17
Size: 140 Color: 3
Size: 128 Color: 15
Size: 124 Color: 16
Size: 116 Color: 14
Size: 104 Color: 17
Size: 96 Color: 14
Size: 92 Color: 9
Size: 82 Color: 18
Size: 82 Color: 15
Size: 76 Color: 7
Size: 68 Color: 6
Size: 66 Color: 15
Size: 42 Color: 4

Bin 18: 1 of cap free
Amount of items: 5
Items: 
Size: 1235 Color: 10
Size: 1062 Color: 19
Size: 56 Color: 4
Size: 56 Color: 4
Size: 54 Color: 6

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 9
Size: 810 Color: 0
Size: 112 Color: 12

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 8
Size: 662 Color: 16
Size: 44 Color: 16

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 14
Size: 621 Color: 3
Size: 68 Color: 6

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 10
Size: 362 Color: 5
Size: 140 Color: 3

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1969 Color: 0
Size: 442 Color: 18
Size: 52 Color: 11

Bin 24: 2 of cap free
Amount of items: 5
Items: 
Size: 1233 Color: 6
Size: 418 Color: 19
Size: 338 Color: 10
Size: 269 Color: 9
Size: 204 Color: 0

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1250 Color: 1
Size: 1080 Color: 13
Size: 132 Color: 0

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1403 Color: 8
Size: 1027 Color: 3
Size: 32 Color: 1

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 2
Size: 871 Color: 10
Size: 48 Color: 12

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 9
Size: 669 Color: 13
Size: 72 Color: 15

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 9
Size: 368 Color: 14
Size: 76 Color: 17

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 2201 Color: 7
Size: 209 Color: 3
Size: 52 Color: 14

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1607 Color: 16
Size: 762 Color: 7
Size: 92 Color: 8

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 13
Size: 715 Color: 18
Size: 72 Color: 8

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 7
Size: 374 Color: 3
Size: 200 Color: 2

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 2024 Color: 19
Size: 421 Color: 12
Size: 16 Color: 3

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 2067 Color: 13
Size: 394 Color: 19

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 2121 Color: 7
Size: 340 Color: 12

Bin 37: 4 of cap free
Amount of items: 3
Items: 
Size: 2055 Color: 18
Size: 383 Color: 4
Size: 22 Color: 19

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 2210 Color: 11
Size: 250 Color: 16

Bin 39: 5 of cap free
Amount of items: 2
Items: 
Size: 1837 Color: 11
Size: 622 Color: 18

Bin 40: 5 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 13
Size: 481 Color: 9
Size: 84 Color: 10

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 2034 Color: 5
Size: 425 Color: 12

Bin 42: 6 of cap free
Amount of items: 4
Items: 
Size: 1234 Color: 5
Size: 870 Color: 17
Size: 304 Color: 9
Size: 50 Color: 14

Bin 43: 7 of cap free
Amount of items: 2
Items: 
Size: 1866 Color: 0
Size: 591 Color: 8

Bin 44: 7 of cap free
Amount of items: 2
Items: 
Size: 1934 Color: 13
Size: 523 Color: 14

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 1955 Color: 16
Size: 502 Color: 14

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 2106 Color: 13
Size: 351 Color: 2

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 2183 Color: 18
Size: 274 Color: 19

Bin 48: 8 of cap free
Amount of items: 2
Items: 
Size: 2169 Color: 13
Size: 287 Color: 5

Bin 49: 9 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 7
Size: 467 Color: 9
Size: 22 Color: 18

Bin 50: 10 of cap free
Amount of items: 3
Items: 
Size: 2215 Color: 15
Size: 235 Color: 13
Size: 4 Color: 16

Bin 51: 11 of cap free
Amount of items: 2
Items: 
Size: 2138 Color: 6
Size: 315 Color: 2

Bin 52: 12 of cap free
Amount of items: 2
Items: 
Size: 2150 Color: 1
Size: 302 Color: 5

Bin 53: 13 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 4
Size: 887 Color: 12
Size: 142 Color: 6

Bin 54: 14 of cap free
Amount of items: 2
Items: 
Size: 1972 Color: 6
Size: 478 Color: 19

Bin 55: 15 of cap free
Amount of items: 3
Items: 
Size: 1220 Color: 11
Size: 1025 Color: 18
Size: 204 Color: 16

Bin 56: 15 of cap free
Amount of items: 2
Items: 
Size: 2062 Color: 5
Size: 387 Color: 4

Bin 57: 15 of cap free
Amount of items: 2
Items: 
Size: 2103 Color: 17
Size: 346 Color: 6

Bin 58: 17 of cap free
Amount of items: 2
Items: 
Size: 1905 Color: 17
Size: 542 Color: 6

Bin 59: 22 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 19
Size: 840 Color: 14

Bin 60: 23 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 13
Size: 912 Color: 12
Size: 128 Color: 14

Bin 61: 24 of cap free
Amount of items: 2
Items: 
Size: 1338 Color: 10
Size: 1102 Color: 12

Bin 62: 24 of cap free
Amount of items: 2
Items: 
Size: 1718 Color: 3
Size: 722 Color: 17

Bin 63: 25 of cap free
Amount of items: 2
Items: 
Size: 1554 Color: 5
Size: 885 Color: 8

Bin 64: 28 of cap free
Amount of items: 2
Items: 
Size: 1494 Color: 15
Size: 942 Color: 1

Bin 65: 30 of cap free
Amount of items: 2
Items: 
Size: 1663 Color: 9
Size: 771 Color: 17

Bin 66: 2046 of cap free
Amount of items: 8
Items: 
Size: 80 Color: 2
Size: 64 Color: 0
Size: 62 Color: 9
Size: 52 Color: 6
Size: 48 Color: 16
Size: 40 Color: 12
Size: 40 Color: 7
Size: 32 Color: 4

Total size: 160160
Total free space: 2464

