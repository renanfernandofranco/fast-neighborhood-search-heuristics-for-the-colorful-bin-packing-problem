Capicity Bin: 2356
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 2083 Color: 1
Size: 229 Color: 1
Size: 44 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1878 Color: 1
Size: 474 Color: 1
Size: 4 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 1
Size: 226 Color: 1
Size: 12 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 1
Size: 283 Color: 1
Size: 56 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 1
Size: 297 Color: 1
Size: 58 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 1
Size: 214 Color: 1
Size: 100 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2075 Color: 1
Size: 235 Color: 1
Size: 46 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 1
Size: 977 Color: 1
Size: 62 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 1
Size: 202 Color: 1
Size: 52 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 1
Size: 454 Color: 1
Size: 88 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 1947 Color: 1
Size: 333 Color: 1
Size: 44 Color: 0
Size: 32 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2080 Color: 1
Size: 164 Color: 0
Size: 112 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2047 Color: 1
Size: 271 Color: 1
Size: 38 Color: 0

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 1873 Color: 1
Size: 259 Color: 1
Size: 144 Color: 0
Size: 80 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1827 Color: 1
Size: 341 Color: 1
Size: 188 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 1
Size: 981 Color: 1
Size: 194 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2010 Color: 1
Size: 290 Color: 1
Size: 56 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 1
Size: 459 Color: 1
Size: 90 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 1
Size: 613 Color: 1
Size: 42 Color: 0

Bin 20: 0 of cap free
Amount of items: 5
Items: 
Size: 1742 Color: 1
Size: 255 Color: 1
Size: 241 Color: 1
Size: 78 Color: 0
Size: 40 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 1
Size: 514 Color: 1
Size: 60 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1880 Color: 1
Size: 318 Color: 1
Size: 158 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 1
Size: 360 Color: 1
Size: 58 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1518 Color: 1
Size: 448 Color: 0
Size: 390 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 1
Size: 1053 Color: 1
Size: 66 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1182 Color: 1
Size: 982 Color: 1
Size: 192 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 1
Size: 554 Color: 1
Size: 108 Color: 0

Bin 28: 0 of cap free
Amount of items: 5
Items: 
Size: 1567 Color: 1
Size: 429 Color: 1
Size: 242 Color: 1
Size: 68 Color: 0
Size: 50 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 1
Size: 499 Color: 1
Size: 46 Color: 0

Bin 30: 0 of cap free
Amount of items: 5
Items: 
Size: 1179 Color: 1
Size: 702 Color: 1
Size: 231 Color: 1
Size: 136 Color: 0
Size: 108 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 1
Size: 495 Color: 1
Size: 98 Color: 0

Bin 32: 0 of cap free
Amount of items: 5
Items: 
Size: 930 Color: 1
Size: 667 Color: 1
Size: 547 Color: 1
Size: 122 Color: 0
Size: 90 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 1
Size: 638 Color: 1
Size: 124 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 1
Size: 858 Color: 1
Size: 168 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 1
Size: 565 Color: 1
Size: 112 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1281 Color: 1
Size: 897 Color: 1
Size: 178 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 666 Color: 1
Size: 32 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1189 Color: 1
Size: 981 Color: 1
Size: 186 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 1
Size: 679 Color: 1
Size: 134 Color: 0

Bin 40: 0 of cap free
Amount of items: 4
Items: 
Size: 1413 Color: 1
Size: 649 Color: 1
Size: 156 Color: 0
Size: 138 Color: 0

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 1
Size: 817 Color: 1
Size: 68 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 1
Size: 482 Color: 1
Size: 30 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 2031 Color: 1
Size: 236 Color: 1
Size: 88 Color: 0

Bin 44: 1 of cap free
Amount of items: 5
Items: 
Size: 1032 Color: 1
Size: 826 Color: 1
Size: 403 Color: 1
Size: 54 Color: 0
Size: 40 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 1
Size: 196 Color: 1
Size: 162 Color: 0

Bin 46: 1 of cap free
Amount of items: 4
Items: 
Size: 1185 Color: 1
Size: 742 Color: 1
Size: 292 Color: 0
Size: 136 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1899 Color: 1
Size: 262 Color: 1
Size: 194 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 1
Size: 215 Color: 1
Size: 36 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 1
Size: 350 Color: 1
Size: 114 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 1
Size: 691 Color: 1
Size: 84 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 1377 Color: 1
Size: 933 Color: 1
Size: 44 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 2119 Color: 1
Size: 199 Color: 1
Size: 36 Color: 0

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 1
Size: 475 Color: 1
Size: 76 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 1957 Color: 1
Size: 320 Color: 1
Size: 76 Color: 0

Bin 55: 5 of cap free
Amount of items: 5
Items: 
Size: 1397 Color: 1
Size: 381 Color: 1
Size: 301 Color: 1
Size: 144 Color: 0
Size: 128 Color: 0

Bin 56: 7 of cap free
Amount of items: 3
Items: 
Size: 1366 Color: 1
Size: 951 Color: 1
Size: 32 Color: 0

Bin 57: 12 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 1
Size: 801 Color: 1
Size: 16 Color: 0

Bin 58: 36 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 1
Size: 455 Color: 1
Size: 184 Color: 0

Bin 59: 146 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 1
Size: 876 Color: 1
Size: 92 Color: 0

Bin 60: 257 of cap free
Amount of items: 1
Items: 
Size: 2099 Color: 1

Bin 61: 270 of cap free
Amount of items: 1
Items: 
Size: 2086 Color: 1

Bin 62: 277 of cap free
Amount of items: 1
Items: 
Size: 2079 Color: 1

Bin 63: 285 of cap free
Amount of items: 1
Items: 
Size: 2071 Color: 1

Bin 64: 286 of cap free
Amount of items: 1
Items: 
Size: 2070 Color: 1

Bin 65: 375 of cap free
Amount of items: 3
Items: 
Size: 1164 Color: 1
Size: 787 Color: 1
Size: 30 Color: 0

Bin 66: 378 of cap free
Amount of items: 1
Items: 
Size: 1978 Color: 1

Total size: 153140
Total free space: 2356

