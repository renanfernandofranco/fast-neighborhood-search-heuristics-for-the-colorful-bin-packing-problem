Capicity Bin: 8016
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 20
Items: 
Size: 664 Color: 1
Size: 560 Color: 0
Size: 496 Color: 1
Size: 472 Color: 1
Size: 472 Color: 0
Size: 466 Color: 1
Size: 436 Color: 1
Size: 400 Color: 1
Size: 384 Color: 1
Size: 376 Color: 0
Size: 368 Color: 0
Size: 360 Color: 1
Size: 354 Color: 0
Size: 352 Color: 0
Size: 348 Color: 0
Size: 342 Color: 1
Size: 340 Color: 0
Size: 340 Color: 0
Size: 268 Color: 1
Size: 218 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 4013 Color: 0
Size: 2867 Color: 0
Size: 724 Color: 1
Size: 276 Color: 0
Size: 136 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4556 Color: 0
Size: 3260 Color: 1
Size: 200 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4906 Color: 1
Size: 2594 Color: 1
Size: 516 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5092 Color: 0
Size: 2468 Color: 0
Size: 456 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5215 Color: 1
Size: 2451 Color: 0
Size: 350 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 0
Size: 2356 Color: 1
Size: 136 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5855 Color: 1
Size: 1801 Color: 1
Size: 360 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 0
Size: 1500 Color: 1
Size: 664 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 0
Size: 1186 Color: 1
Size: 488 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 0
Size: 1124 Color: 1
Size: 496 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 0
Size: 1522 Color: 1
Size: 96 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 1
Size: 800 Color: 0
Size: 692 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6546 Color: 1
Size: 1282 Color: 1
Size: 188 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 1
Size: 876 Color: 0
Size: 584 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 0
Size: 1093 Color: 0
Size: 218 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6935 Color: 0
Size: 753 Color: 0
Size: 328 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 1
Size: 710 Color: 1
Size: 408 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 0
Size: 750 Color: 0
Size: 304 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 0
Size: 690 Color: 1
Size: 354 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7010 Color: 0
Size: 574 Color: 1
Size: 432 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 0
Size: 508 Color: 1
Size: 490 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7047 Color: 0
Size: 801 Color: 0
Size: 168 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 0
Size: 490 Color: 1
Size: 464 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 0
Size: 836 Color: 0
Size: 104 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7056 Color: 1
Size: 788 Color: 0
Size: 172 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7089 Color: 0
Size: 773 Color: 0
Size: 154 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7141 Color: 0
Size: 731 Color: 0
Size: 144 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 1
Size: 584 Color: 1
Size: 244 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 4108 Color: 1
Size: 3331 Color: 0
Size: 576 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 4498 Color: 0
Size: 3333 Color: 0
Size: 184 Color: 1

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 5211 Color: 1
Size: 2804 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 5196 Color: 0
Size: 2419 Color: 1
Size: 400 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 5478 Color: 0
Size: 2329 Color: 1
Size: 208 Color: 0

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 5897 Color: 0
Size: 2118 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5905 Color: 0
Size: 1854 Color: 0
Size: 256 Color: 1

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 6254 Color: 0
Size: 1761 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6405 Color: 1
Size: 1454 Color: 1
Size: 156 Color: 0

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6676 Color: 1
Size: 1339 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6803 Color: 1
Size: 640 Color: 1
Size: 572 Color: 0

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6919 Color: 1
Size: 648 Color: 0
Size: 448 Color: 1

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 7081 Color: 1
Size: 934 Color: 0

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 7166 Color: 1
Size: 849 Color: 0

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 4502 Color: 1
Size: 3512 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 4962 Color: 0
Size: 2374 Color: 1
Size: 678 Color: 0

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 5567 Color: 1
Size: 2335 Color: 0
Size: 112 Color: 1

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 5892 Color: 1
Size: 1702 Color: 1
Size: 420 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 5961 Color: 0
Size: 1773 Color: 1
Size: 280 Color: 1

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 6012 Color: 1
Size: 2002 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 0
Size: 1758 Color: 1
Size: 148 Color: 0

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6447 Color: 0
Size: 1567 Color: 1

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6594 Color: 1
Size: 1420 Color: 0

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6658 Color: 0
Size: 1356 Color: 1

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 7113 Color: 0
Size: 901 Color: 1

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 5071 Color: 0
Size: 2460 Color: 1
Size: 482 Color: 1

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 5398 Color: 1
Size: 2455 Color: 0
Size: 160 Color: 0

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 5889 Color: 0
Size: 1658 Color: 1
Size: 466 Color: 0

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 5910 Color: 1
Size: 1294 Color: 1
Size: 809 Color: 0

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 7098 Color: 0
Size: 915 Color: 1

Bin 60: 4 of cap free
Amount of items: 5
Items: 
Size: 4010 Color: 0
Size: 1491 Color: 1
Size: 1189 Color: 1
Size: 914 Color: 0
Size: 408 Color: 1

Bin 61: 4 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 0
Size: 2172 Color: 1
Size: 140 Color: 0

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 7093 Color: 0
Size: 919 Color: 1

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 0
Size: 766 Color: 1
Size: 56 Color: 0

Bin 64: 5 of cap free
Amount of items: 2
Items: 
Size: 6415 Color: 1
Size: 1596 Color: 0

Bin 65: 5 of cap free
Amount of items: 4
Items: 
Size: 6709 Color: 1
Size: 1212 Color: 0
Size: 58 Color: 1
Size: 32 Color: 0

Bin 66: 6 of cap free
Amount of items: 7
Items: 
Size: 4009 Color: 0
Size: 781 Color: 0
Size: 666 Color: 1
Size: 666 Color: 1
Size: 664 Color: 1
Size: 664 Color: 0
Size: 560 Color: 0

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 6999 Color: 0
Size: 1011 Color: 1

Bin 68: 7 of cap free
Amount of items: 4
Items: 
Size: 4021 Color: 1
Size: 2858 Color: 0
Size: 666 Color: 1
Size: 464 Color: 0

Bin 69: 7 of cap free
Amount of items: 3
Items: 
Size: 4472 Color: 1
Size: 3337 Color: 0
Size: 200 Color: 0

Bin 70: 7 of cap free
Amount of items: 3
Items: 
Size: 4612 Color: 0
Size: 2546 Color: 1
Size: 851 Color: 0

Bin 71: 7 of cap free
Amount of items: 3
Items: 
Size: 5029 Color: 0
Size: 2404 Color: 1
Size: 576 Color: 0

Bin 72: 9 of cap free
Amount of items: 2
Items: 
Size: 6235 Color: 1
Size: 1772 Color: 0

Bin 73: 10 of cap free
Amount of items: 2
Items: 
Size: 5965 Color: 1
Size: 2041 Color: 0

Bin 74: 10 of cap free
Amount of items: 2
Items: 
Size: 6478 Color: 0
Size: 1528 Color: 1

Bin 75: 10 of cap free
Amount of items: 2
Items: 
Size: 6915 Color: 0
Size: 1091 Color: 1

Bin 76: 11 of cap free
Amount of items: 2
Items: 
Size: 5075 Color: 0
Size: 2930 Color: 1

Bin 77: 12 of cap free
Amount of items: 3
Items: 
Size: 5614 Color: 1
Size: 2182 Color: 0
Size: 208 Color: 1

Bin 78: 12 of cap free
Amount of items: 2
Items: 
Size: 7206 Color: 0
Size: 798 Color: 1

Bin 79: 13 of cap free
Amount of items: 2
Items: 
Size: 5119 Color: 0
Size: 2884 Color: 1

Bin 80: 13 of cap free
Amount of items: 2
Items: 
Size: 6839 Color: 0
Size: 1164 Color: 1

Bin 81: 14 of cap free
Amount of items: 2
Items: 
Size: 5068 Color: 1
Size: 2934 Color: 0

Bin 82: 15 of cap free
Amount of items: 2
Items: 
Size: 7020 Color: 0
Size: 981 Color: 1

Bin 83: 16 of cap free
Amount of items: 2
Items: 
Size: 5123 Color: 1
Size: 2877 Color: 0

Bin 84: 16 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 1
Size: 1140 Color: 0
Size: 48 Color: 0

Bin 85: 16 of cap free
Amount of items: 2
Items: 
Size: 7057 Color: 0
Size: 943 Color: 1

Bin 86: 16 of cap free
Amount of items: 2
Items: 
Size: 7118 Color: 1
Size: 882 Color: 0

Bin 87: 17 of cap free
Amount of items: 2
Items: 
Size: 5023 Color: 1
Size: 2976 Color: 0

Bin 88: 17 of cap free
Amount of items: 2
Items: 
Size: 6995 Color: 1
Size: 1004 Color: 0

Bin 89: 19 of cap free
Amount of items: 2
Items: 
Size: 6220 Color: 0
Size: 1777 Color: 1

Bin 90: 20 of cap free
Amount of items: 2
Items: 
Size: 6231 Color: 1
Size: 1765 Color: 0

Bin 91: 20 of cap free
Amount of items: 2
Items: 
Size: 6805 Color: 0
Size: 1191 Color: 1

Bin 92: 21 of cap free
Amount of items: 2
Items: 
Size: 5170 Color: 1
Size: 2825 Color: 0

Bin 93: 21 of cap free
Amount of items: 3
Items: 
Size: 5412 Color: 1
Size: 2491 Color: 0
Size: 92 Color: 1

Bin 94: 22 of cap free
Amount of items: 2
Items: 
Size: 4652 Color: 1
Size: 3342 Color: 0

Bin 95: 22 of cap free
Amount of items: 3
Items: 
Size: 5794 Color: 1
Size: 1335 Color: 0
Size: 865 Color: 0

Bin 96: 24 of cap free
Amount of items: 2
Items: 
Size: 6316 Color: 1
Size: 1676 Color: 0

Bin 97: 24 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 1
Size: 1485 Color: 0
Size: 96 Color: 0

Bin 98: 24 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 1
Size: 1220 Color: 0

Bin 99: 24 of cap free
Amount of items: 2
Items: 
Size: 6766 Color: 0
Size: 1226 Color: 1

Bin 100: 25 of cap free
Amount of items: 2
Items: 
Size: 6138 Color: 0
Size: 1853 Color: 1

Bin 101: 26 of cap free
Amount of items: 2
Items: 
Size: 7156 Color: 0
Size: 834 Color: 1

Bin 102: 27 of cap free
Amount of items: 2
Items: 
Size: 6185 Color: 1
Size: 1804 Color: 0

Bin 103: 28 of cap free
Amount of items: 2
Items: 
Size: 5115 Color: 1
Size: 2873 Color: 0

Bin 104: 28 of cap free
Amount of items: 2
Items: 
Size: 6854 Color: 0
Size: 1134 Color: 1

Bin 105: 31 of cap free
Amount of items: 2
Items: 
Size: 4017 Color: 0
Size: 3968 Color: 1

Bin 106: 31 of cap free
Amount of items: 2
Items: 
Size: 5974 Color: 0
Size: 2011 Color: 1

Bin 107: 31 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 0
Size: 1711 Color: 1

Bin 108: 31 of cap free
Amount of items: 2
Items: 
Size: 6587 Color: 0
Size: 1398 Color: 1

Bin 109: 32 of cap free
Amount of items: 2
Items: 
Size: 5140 Color: 0
Size: 2844 Color: 1

Bin 110: 34 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 1
Size: 1356 Color: 0

Bin 111: 35 of cap free
Amount of items: 2
Items: 
Size: 6885 Color: 1
Size: 1096 Color: 0

Bin 112: 38 of cap free
Amount of items: 5
Items: 
Size: 4012 Color: 1
Size: 1498 Color: 1
Size: 970 Color: 0
Size: 924 Color: 0
Size: 574 Color: 0

Bin 113: 38 of cap free
Amount of items: 2
Items: 
Size: 6762 Color: 1
Size: 1216 Color: 0

Bin 114: 41 of cap free
Amount of items: 3
Items: 
Size: 5937 Color: 1
Size: 1046 Color: 0
Size: 992 Color: 0

Bin 115: 42 of cap free
Amount of items: 2
Items: 
Size: 6408 Color: 0
Size: 1566 Color: 1

Bin 116: 43 of cap free
Amount of items: 2
Items: 
Size: 6446 Color: 1
Size: 1527 Color: 0

Bin 117: 46 of cap free
Amount of items: 2
Items: 
Size: 6620 Color: 1
Size: 1350 Color: 0

Bin 118: 47 of cap free
Amount of items: 2
Items: 
Size: 5885 Color: 1
Size: 2084 Color: 0

Bin 119: 48 of cap free
Amount of items: 2
Items: 
Size: 6310 Color: 1
Size: 1658 Color: 0

Bin 120: 51 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 0
Size: 1057 Color: 1

Bin 121: 54 of cap free
Amount of items: 2
Items: 
Size: 6030 Color: 1
Size: 1932 Color: 0

Bin 122: 55 of cap free
Amount of items: 2
Items: 
Size: 6652 Color: 0
Size: 1309 Color: 1

Bin 123: 63 of cap free
Amount of items: 4
Items: 
Size: 4014 Color: 1
Size: 2415 Color: 1
Size: 1042 Color: 0
Size: 482 Color: 0

Bin 124: 70 of cap free
Amount of items: 2
Items: 
Size: 4978 Color: 0
Size: 2968 Color: 1

Bin 125: 72 of cap free
Amount of items: 2
Items: 
Size: 5605 Color: 1
Size: 2339 Color: 0

Bin 126: 76 of cap free
Amount of items: 2
Items: 
Size: 6227 Color: 1
Size: 1713 Color: 0

Bin 127: 82 of cap free
Amount of items: 2
Items: 
Size: 6591 Color: 1
Size: 1343 Color: 0

Bin 128: 98 of cap free
Amount of items: 2
Items: 
Size: 4577 Color: 1
Size: 3341 Color: 0

Bin 129: 107 of cap free
Amount of items: 2
Items: 
Size: 4569 Color: 1
Size: 3340 Color: 0

Bin 130: 108 of cap free
Amount of items: 2
Items: 
Size: 5223 Color: 1
Size: 2685 Color: 0

Bin 131: 113 of cap free
Amount of items: 2
Items: 
Size: 4565 Color: 0
Size: 3338 Color: 1

Bin 132: 128 of cap free
Amount of items: 33
Items: 
Size: 328 Color: 0
Size: 314 Color: 0
Size: 312 Color: 1
Size: 312 Color: 0
Size: 298 Color: 0
Size: 296 Color: 0
Size: 296 Color: 0
Size: 288 Color: 0
Size: 268 Color: 1
Size: 266 Color: 1
Size: 266 Color: 0
Size: 266 Color: 0
Size: 264 Color: 1
Size: 260 Color: 0
Size: 240 Color: 1
Size: 238 Color: 1
Size: 236 Color: 0
Size: 236 Color: 0
Size: 232 Color: 0
Size: 224 Color: 1
Size: 224 Color: 0
Size: 216 Color: 0
Size: 216 Color: 0
Size: 208 Color: 1
Size: 208 Color: 0
Size: 192 Color: 1
Size: 184 Color: 1
Size: 180 Color: 1
Size: 168 Color: 1
Size: 168 Color: 1
Size: 164 Color: 1
Size: 160 Color: 1
Size: 160 Color: 1

Bin 133: 5652 of cap free
Amount of items: 15
Items: 
Size: 196 Color: 0
Size: 182 Color: 0
Size: 182 Color: 0
Size: 180 Color: 0
Size: 170 Color: 0
Size: 158 Color: 1
Size: 156 Color: 0
Size: 154 Color: 1
Size: 152 Color: 1
Size: 152 Color: 1
Size: 150 Color: 1
Size: 136 Color: 1
Size: 136 Color: 0
Size: 132 Color: 1
Size: 128 Color: 0

Total size: 1058112
Total free space: 8016

