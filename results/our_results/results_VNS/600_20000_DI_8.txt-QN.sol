Capicity Bin: 15744
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 9888 Color: 438
Size: 5856 Color: 386

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10064 Color: 441
Size: 5400 Color: 376
Size: 280 Color: 23

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 443
Size: 5368 Color: 375
Size: 272 Color: 20

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10440 Color: 450
Size: 5036 Color: 369
Size: 268 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11604 Color: 475
Size: 3852 Color: 341
Size: 288 Color: 25

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11791 Color: 482
Size: 3241 Color: 321
Size: 712 Color: 139

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 486
Size: 3124 Color: 319
Size: 564 Color: 116

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12058 Color: 487
Size: 3074 Color: 317
Size: 612 Color: 128

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12187 Color: 491
Size: 2737 Color: 302
Size: 820 Color: 153

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 498
Size: 3068 Color: 316
Size: 280 Color: 22

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 501
Size: 2384 Color: 279
Size: 920 Color: 162

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12444 Color: 502
Size: 2668 Color: 299
Size: 632 Color: 130

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12578 Color: 506
Size: 2212 Color: 269
Size: 954 Color: 170

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12624 Color: 507
Size: 2188 Color: 266
Size: 932 Color: 165

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12682 Color: 509
Size: 2586 Color: 294
Size: 476 Color: 92

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12691 Color: 510
Size: 2333 Color: 276
Size: 720 Color: 142

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12696 Color: 511
Size: 2540 Color: 289
Size: 508 Color: 103

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12698 Color: 512
Size: 1678 Color: 228
Size: 1368 Color: 205

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12760 Color: 514
Size: 2176 Color: 265
Size: 808 Color: 152

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 516
Size: 1744 Color: 235
Size: 1220 Color: 186

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12797 Color: 517
Size: 2457 Color: 287
Size: 490 Color: 98

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12812 Color: 518
Size: 1604 Color: 221
Size: 1328 Color: 203

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12874 Color: 521
Size: 1442 Color: 211
Size: 1428 Color: 210

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12937 Color: 524
Size: 2341 Color: 277
Size: 466 Color: 90

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13013 Color: 528
Size: 2605 Color: 295
Size: 126 Color: 5

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 529
Size: 2392 Color: 280
Size: 336 Color: 45

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13022 Color: 530
Size: 2342 Color: 278
Size: 380 Color: 60

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13086 Color: 532
Size: 2260 Color: 271
Size: 398 Color: 66

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13242 Color: 539
Size: 1734 Color: 233
Size: 768 Color: 150

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 540
Size: 1736 Color: 234
Size: 752 Color: 148

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13262 Color: 541
Size: 1548 Color: 219
Size: 934 Color: 167

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13304 Color: 543
Size: 2036 Color: 257
Size: 404 Color: 70

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13318 Color: 545
Size: 1484 Color: 214
Size: 942 Color: 168

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13326 Color: 547
Size: 1784 Color: 239
Size: 634 Color: 131

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13376 Color: 551
Size: 1928 Color: 248
Size: 440 Color: 80

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13380 Color: 552
Size: 1748 Color: 236
Size: 616 Color: 129

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13506 Color: 556
Size: 1358 Color: 204
Size: 880 Color: 161

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13508 Color: 557
Size: 1308 Color: 198
Size: 928 Color: 164

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13538 Color: 559
Size: 2190 Color: 267
Size: 16 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13602 Color: 563
Size: 1710 Color: 231
Size: 432 Color: 76

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13652 Color: 565
Size: 1524 Color: 216
Size: 568 Color: 117

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 566
Size: 1648 Color: 226
Size: 440 Color: 79

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13680 Color: 569
Size: 1296 Color: 192
Size: 768 Color: 149

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 575
Size: 1424 Color: 209
Size: 544 Color: 110

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13784 Color: 576
Size: 1248 Color: 187
Size: 712 Color: 140

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13844 Color: 580
Size: 1628 Color: 223
Size: 272 Color: 17

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 581
Size: 1466 Color: 213
Size: 424 Color: 75

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13908 Color: 583
Size: 1540 Color: 218
Size: 296 Color: 27

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13914 Color: 584
Size: 1372 Color: 206
Size: 458 Color: 86

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 585
Size: 1280 Color: 190
Size: 546 Color: 112

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13972 Color: 586
Size: 1324 Color: 202
Size: 448 Color: 84

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 587
Size: 1310 Color: 201
Size: 448 Color: 82

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 589
Size: 1448 Color: 212
Size: 272 Color: 19

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14036 Color: 590
Size: 1296 Color: 193
Size: 412 Color: 71

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 593
Size: 1308 Color: 196
Size: 364 Color: 56

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14098 Color: 594
Size: 1254 Color: 188
Size: 392 Color: 64

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 596
Size: 1082 Color: 178
Size: 544 Color: 111

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14156 Color: 598
Size: 1072 Color: 177
Size: 516 Color: 106

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14160 Color: 599
Size: 1136 Color: 183
Size: 448 Color: 83

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 10145 Color: 445
Size: 2838 Color: 308
Size: 2760 Color: 304

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 11445 Color: 472
Size: 3582 Color: 332
Size: 716 Color: 141

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 11696 Color: 479
Size: 4047 Color: 345

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11782 Color: 481
Size: 3637 Color: 335
Size: 324 Color: 43

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12518 Color: 504
Size: 2885 Color: 310
Size: 340 Color: 48

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12873 Color: 520
Size: 2270 Color: 272
Size: 600 Color: 123

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 13198 Color: 537
Size: 2545 Color: 291

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 13349 Color: 550
Size: 2394 Color: 282

Bin 68: 2 of cap free
Amount of items: 25
Items: 
Size: 864 Color: 156
Size: 830 Color: 154
Size: 808 Color: 151
Size: 744 Color: 147
Size: 738 Color: 146
Size: 736 Color: 145
Size: 736 Color: 144
Size: 726 Color: 143
Size: 688 Color: 138
Size: 688 Color: 137
Size: 688 Color: 136
Size: 672 Color: 135
Size: 668 Color: 134
Size: 608 Color: 126
Size: 576 Color: 118
Size: 512 Color: 105
Size: 512 Color: 104
Size: 508 Color: 102
Size: 504 Color: 101
Size: 504 Color: 100
Size: 496 Color: 99
Size: 488 Color: 97
Size: 488 Color: 96
Size: 480 Color: 95
Size: 480 Color: 94

Bin 69: 2 of cap free
Amount of items: 5
Items: 
Size: 7976 Color: 418
Size: 6562 Color: 398
Size: 524 Color: 107
Size: 344 Color: 49
Size: 336 Color: 47

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 8880 Color: 423
Size: 6542 Color: 392
Size: 320 Color: 39

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 9712 Color: 435
Size: 5726 Color: 383
Size: 304 Color: 30

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 10818 Color: 458
Size: 3856 Color: 342
Size: 1068 Color: 175

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 11718 Color: 480
Size: 2896 Color: 311
Size: 1128 Color: 182

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 12342 Color: 496
Size: 3400 Color: 327

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 12412 Color: 499
Size: 3330 Color: 324

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 12844 Color: 519
Size: 2898 Color: 312

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13200 Color: 538
Size: 2542 Color: 290

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13328 Color: 548
Size: 2414 Color: 283

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 568
Size: 2070 Color: 261

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14014 Color: 588
Size: 1728 Color: 232

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14050 Color: 591
Size: 1692 Color: 230

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14064 Color: 592
Size: 1678 Color: 227

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 597
Size: 1622 Color: 222

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 14164 Color: 600
Size: 1578 Color: 220

Bin 85: 3 of cap free
Amount of items: 13
Items: 
Size: 7873 Color: 404
Size: 928 Color: 163
Size: 880 Color: 160
Size: 880 Color: 159
Size: 872 Color: 158
Size: 864 Color: 157
Size: 648 Color: 132
Size: 478 Color: 93
Size: 474 Color: 91
Size: 464 Color: 89
Size: 464 Color: 88
Size: 464 Color: 87
Size: 452 Color: 85

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 11248 Color: 467
Size: 4493 Color: 357

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 12422 Color: 500
Size: 3319 Color: 323

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 12985 Color: 527
Size: 2756 Color: 303

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 13321 Color: 546
Size: 2420 Color: 284

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 13348 Color: 549
Size: 2393 Color: 281

Bin 91: 4 of cap free
Amount of items: 11
Items: 
Size: 7874 Color: 405
Size: 1056 Color: 174
Size: 1008 Color: 173
Size: 1000 Color: 172
Size: 992 Color: 171
Size: 950 Color: 169
Size: 932 Color: 166
Size: 608 Color: 127
Size: 448 Color: 81
Size: 436 Color: 78
Size: 436 Color: 77

Bin 92: 4 of cap free
Amount of items: 5
Items: 
Size: 7888 Color: 412
Size: 3006 Color: 314
Size: 2444 Color: 286
Size: 2018 Color: 254
Size: 384 Color: 62

Bin 93: 4 of cap free
Amount of items: 5
Items: 
Size: 7898 Color: 415
Size: 6554 Color: 395
Size: 576 Color: 122
Size: 356 Color: 55
Size: 356 Color: 54

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 8852 Color: 421
Size: 6568 Color: 400
Size: 320 Color: 41

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 8874 Color: 422
Size: 6546 Color: 393
Size: 320 Color: 40

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 8916 Color: 424
Size: 6504 Color: 390
Size: 320 Color: 38

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 535
Size: 2616 Color: 297

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 13308 Color: 544
Size: 2432 Color: 285

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 13412 Color: 553
Size: 2328 Color: 275

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 13522 Color: 558
Size: 2218 Color: 270

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 13588 Color: 562
Size: 2152 Color: 264

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 13696 Color: 571
Size: 2044 Color: 259

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 574
Size: 2004 Color: 253

Bin 104: 4 of cap free
Amount of items: 2
Items: 
Size: 13798 Color: 578
Size: 1942 Color: 249

Bin 105: 4 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 582
Size: 1848 Color: 243

Bin 106: 4 of cap free
Amount of items: 2
Items: 
Size: 14100 Color: 595
Size: 1640 Color: 225

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 9251 Color: 428
Size: 6488 Color: 389

Bin 108: 5 of cap free
Amount of items: 3
Items: 
Size: 10880 Color: 460
Size: 4667 Color: 361
Size: 192 Color: 9

Bin 109: 5 of cap free
Amount of items: 2
Items: 
Size: 13263 Color: 542
Size: 2476 Color: 288

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 10420 Color: 449
Size: 5046 Color: 370
Size: 272 Color: 18

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 10472 Color: 451
Size: 5266 Color: 371

Bin 112: 6 of cap free
Amount of items: 3
Items: 
Size: 10777 Color: 457
Size: 4721 Color: 364
Size: 240 Color: 11

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 13458 Color: 555
Size: 2280 Color: 273

Bin 114: 6 of cap free
Amount of items: 2
Items: 
Size: 13586 Color: 561
Size: 2152 Color: 263

Bin 115: 6 of cap free
Amount of items: 2
Items: 
Size: 13716 Color: 572
Size: 2022 Color: 255

Bin 116: 7 of cap free
Amount of items: 5
Items: 
Size: 7944 Color: 417
Size: 6561 Color: 397
Size: 536 Color: 109
Size: 352 Color: 51
Size: 344 Color: 50

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 10129 Color: 444
Size: 3760 Color: 339
Size: 1848 Color: 244

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 10889 Color: 461
Size: 4681 Color: 362
Size: 166 Color: 8

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 13608 Color: 564
Size: 2128 Color: 262

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 13788 Color: 577
Size: 1948 Color: 250

Bin 121: 9 of cap free
Amount of items: 3
Items: 
Size: 11275 Color: 468
Size: 4396 Color: 352
Size: 64 Color: 2

Bin 122: 9 of cap free
Amount of items: 2
Items: 
Size: 12283 Color: 494
Size: 3452 Color: 329

Bin 123: 9 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 560
Size: 2191 Color: 268

Bin 124: 9 of cap free
Amount of items: 2
Items: 
Size: 13666 Color: 567
Size: 2069 Color: 260

Bin 125: 10 of cap free
Amount of items: 5
Items: 
Size: 7890 Color: 413
Size: 6536 Color: 391
Size: 552 Color: 113
Size: 384 Color: 61
Size: 372 Color: 59

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 10150 Color: 446
Size: 5584 Color: 379

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 10754 Color: 455
Size: 4980 Color: 368

Bin 128: 10 of cap free
Amount of items: 3
Items: 
Size: 11048 Color: 462
Size: 4554 Color: 358
Size: 132 Color: 7

Bin 129: 10 of cap free
Amount of items: 2
Items: 
Size: 11310 Color: 469
Size: 4424 Color: 354

Bin 130: 10 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 477
Size: 4062 Color: 346

Bin 131: 10 of cap free
Amount of items: 2
Items: 
Size: 12376 Color: 497
Size: 3358 Color: 325

Bin 132: 10 of cap free
Amount of items: 2
Items: 
Size: 12938 Color: 525
Size: 2796 Color: 306

Bin 133: 10 of cap free
Amount of items: 2
Items: 
Size: 13092 Color: 533
Size: 2642 Color: 298

Bin 134: 10 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 570
Size: 2040 Color: 258

Bin 135: 10 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 579
Size: 1906 Color: 247

Bin 136: 11 of cap free
Amount of items: 5
Items: 
Size: 7892 Color: 414
Size: 6553 Color: 394
Size: 552 Color: 114
Size: 368 Color: 58
Size: 368 Color: 57

Bin 137: 11 of cap free
Amount of items: 5
Items: 
Size: 7912 Color: 416
Size: 6557 Color: 396
Size: 560 Color: 115
Size: 352 Color: 53
Size: 352 Color: 52

Bin 138: 11 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 454
Size: 4773 Color: 367
Size: 256 Color: 13

Bin 139: 11 of cap free
Amount of items: 2
Items: 
Size: 13432 Color: 554
Size: 2301 Color: 274

Bin 140: 12 of cap free
Amount of items: 3
Items: 
Size: 9772 Color: 437
Size: 5656 Color: 381
Size: 304 Color: 28

Bin 141: 12 of cap free
Amount of items: 3
Items: 
Size: 10033 Color: 440
Size: 5411 Color: 377
Size: 288 Color: 24

Bin 142: 12 of cap free
Amount of items: 2
Items: 
Size: 11692 Color: 478
Size: 4040 Color: 344

Bin 143: 12 of cap free
Amount of items: 2
Items: 
Size: 12764 Color: 515
Size: 2968 Color: 313

Bin 144: 12 of cap free
Amount of items: 2
Items: 
Size: 12888 Color: 522
Size: 2844 Color: 309

Bin 145: 12 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 526
Size: 2780 Color: 305

Bin 146: 13 of cap free
Amount of items: 2
Items: 
Size: 9155 Color: 427
Size: 6576 Color: 401

Bin 147: 13 of cap free
Amount of items: 3
Items: 
Size: 9304 Color: 430
Size: 6107 Color: 387
Size: 320 Color: 36

Bin 148: 13 of cap free
Amount of items: 2
Items: 
Size: 12700 Color: 513
Size: 3031 Color: 315

Bin 149: 13 of cap free
Amount of items: 2
Items: 
Size: 13734 Color: 573
Size: 1997 Color: 252

Bin 150: 14 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 536
Size: 2554 Color: 293

Bin 151: 15 of cap free
Amount of items: 3
Items: 
Size: 10761 Color: 456
Size: 4712 Color: 363
Size: 256 Color: 12

Bin 152: 15 of cap free
Amount of items: 2
Items: 
Size: 12004 Color: 485
Size: 3725 Color: 338

Bin 153: 16 of cap free
Amount of items: 3
Items: 
Size: 10874 Color: 459
Size: 4662 Color: 360
Size: 192 Color: 10

Bin 154: 18 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 466
Size: 4408 Color: 353
Size: 90 Color: 3

Bin 155: 18 of cap free
Amount of items: 2
Items: 
Size: 13036 Color: 531
Size: 2690 Color: 301

Bin 156: 19 of cap free
Amount of items: 2
Items: 
Size: 12461 Color: 503
Size: 3264 Color: 322

Bin 157: 19 of cap free
Amount of items: 2
Items: 
Size: 13117 Color: 534
Size: 2608 Color: 296

Bin 158: 20 of cap free
Amount of items: 2
Items: 
Size: 11416 Color: 471
Size: 4308 Color: 350

Bin 159: 21 of cap free
Amount of items: 2
Items: 
Size: 12332 Color: 495
Size: 3391 Color: 326

Bin 160: 22 of cap free
Amount of items: 2
Items: 
Size: 11112 Color: 463
Size: 4610 Color: 359

Bin 161: 22 of cap free
Amount of items: 3
Items: 
Size: 11450 Color: 473
Size: 4208 Color: 349
Size: 64 Color: 1

Bin 162: 22 of cap free
Amount of items: 2
Items: 
Size: 12642 Color: 508
Size: 3080 Color: 318

Bin 163: 23 of cap free
Amount of items: 2
Items: 
Size: 11381 Color: 470
Size: 4340 Color: 351

Bin 164: 23 of cap free
Amount of items: 2
Items: 
Size: 12138 Color: 490
Size: 3583 Color: 333

Bin 165: 24 of cap free
Amount of items: 3
Items: 
Size: 9766 Color: 436
Size: 5650 Color: 380
Size: 304 Color: 29

Bin 166: 24 of cap free
Amount of items: 2
Items: 
Size: 10408 Color: 448
Size: 5312 Color: 372

Bin 167: 24 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 493
Size: 3448 Color: 328

Bin 168: 24 of cap free
Amount of items: 2
Items: 
Size: 12912 Color: 523
Size: 2808 Color: 307

Bin 169: 25 of cap free
Amount of items: 2
Items: 
Size: 11855 Color: 483
Size: 3864 Color: 343

Bin 170: 26 of cap free
Amount of items: 3
Items: 
Size: 9690 Color: 433
Size: 3476 Color: 330
Size: 2552 Color: 292

Bin 171: 27 of cap free
Amount of items: 9
Items: 
Size: 7877 Color: 407
Size: 1308 Color: 197
Size: 1304 Color: 195
Size: 1304 Color: 194
Size: 1296 Color: 191
Size: 1280 Color: 189
Size: 536 Color: 108
Size: 412 Color: 72
Size: 400 Color: 69

Bin 172: 27 of cap free
Amount of items: 2
Items: 
Size: 12109 Color: 489
Size: 3608 Color: 334

Bin 173: 28 of cap free
Amount of items: 2
Items: 
Size: 11952 Color: 484
Size: 3764 Color: 340

Bin 174: 28 of cap free
Amount of items: 2
Items: 
Size: 12068 Color: 488
Size: 3648 Color: 336

Bin 175: 29 of cap free
Amount of items: 7
Items: 
Size: 7881 Color: 409
Size: 1760 Color: 237
Size: 1688 Color: 229
Size: 1636 Color: 224
Size: 1526 Color: 217
Size: 832 Color: 155
Size: 392 Color: 65

Bin 176: 30 of cap free
Amount of items: 2
Items: 
Size: 11608 Color: 476
Size: 4106 Color: 347

Bin 177: 32 of cap free
Amount of items: 3
Items: 
Size: 9708 Color: 434
Size: 5692 Color: 382
Size: 312 Color: 31

Bin 178: 32 of cap free
Amount of items: 2
Items: 
Size: 12200 Color: 492
Size: 3512 Color: 331

Bin 179: 35 of cap free
Amount of items: 2
Items: 
Size: 12525 Color: 505
Size: 3184 Color: 320

Bin 180: 36 of cap free
Amount of items: 4
Items: 
Size: 9336 Color: 432
Size: 5748 Color: 385
Size: 312 Color: 33
Size: 312 Color: 32

Bin 181: 37 of cap free
Amount of items: 4
Items: 
Size: 9331 Color: 431
Size: 5744 Color: 384
Size: 320 Color: 35
Size: 312 Color: 34

Bin 182: 39 of cap free
Amount of items: 3
Items: 
Size: 10081 Color: 442
Size: 5352 Color: 374
Size: 272 Color: 21

Bin 183: 39 of cap free
Amount of items: 2
Items: 
Size: 10214 Color: 447
Size: 5491 Color: 378

Bin 184: 47 of cap free
Amount of items: 2
Items: 
Size: 11544 Color: 474
Size: 4153 Color: 348

Bin 185: 52 of cap free
Amount of items: 3
Items: 
Size: 11124 Color: 465
Size: 4456 Color: 356
Size: 112 Color: 4

Bin 186: 56 of cap free
Amount of items: 3
Items: 
Size: 11116 Color: 464
Size: 4444 Color: 355
Size: 128 Color: 6

Bin 187: 70 of cap free
Amount of items: 3
Items: 
Size: 8217 Color: 419
Size: 7121 Color: 403
Size: 336 Color: 46

Bin 188: 90 of cap free
Amount of items: 3
Items: 
Size: 10017 Color: 439
Size: 5345 Color: 373
Size: 292 Color: 26

Bin 189: 91 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 429
Size: 3698 Color: 337
Size: 2683 Color: 300

Bin 190: 111 of cap free
Amount of items: 4
Items: 
Size: 8417 Color: 420
Size: 6564 Color: 399
Size: 332 Color: 44
Size: 320 Color: 42

Bin 191: 121 of cap free
Amount of items: 5
Items: 
Size: 7885 Color: 411
Size: 2032 Color: 256
Size: 1972 Color: 251
Size: 1868 Color: 246
Size: 1866 Color: 245

Bin 192: 148 of cap free
Amount of items: 8
Items: 
Size: 7880 Color: 408
Size: 1522 Color: 215
Size: 1400 Color: 208
Size: 1374 Color: 207
Size: 1310 Color: 200
Size: 1310 Color: 199
Size: 400 Color: 68
Size: 400 Color: 67

Bin 193: 174 of cap free
Amount of items: 2
Items: 
Size: 8966 Color: 425
Size: 6604 Color: 402

Bin 194: 183 of cap free
Amount of items: 3
Items: 
Size: 8968 Color: 426
Size: 6273 Color: 388
Size: 320 Color: 37

Bin 195: 187 of cap free
Amount of items: 3
Items: 
Size: 10540 Color: 453
Size: 4761 Color: 366
Size: 256 Color: 14

Bin 196: 260 of cap free
Amount of items: 3
Items: 
Size: 10476 Color: 452
Size: 4752 Color: 365
Size: 256 Color: 15

Bin 197: 288 of cap free
Amount of items: 6
Items: 
Size: 7882 Color: 410
Size: 1842 Color: 242
Size: 1802 Color: 241
Size: 1786 Color: 240
Size: 1760 Color: 238
Size: 384 Color: 63

Bin 198: 338 of cap free
Amount of items: 9
Items: 
Size: 7876 Color: 406
Size: 1144 Color: 185
Size: 1144 Color: 184
Size: 1120 Color: 181
Size: 1120 Color: 180
Size: 1098 Color: 179
Size: 1072 Color: 176
Size: 416 Color: 74
Size: 416 Color: 73

Bin 199: 12148 of cap free
Amount of items: 6
Items: 
Size: 656 Color: 133
Size: 608 Color: 125
Size: 604 Color: 124
Size: 576 Color: 121
Size: 576 Color: 120
Size: 576 Color: 119

Total size: 3117312
Total free space: 15744

