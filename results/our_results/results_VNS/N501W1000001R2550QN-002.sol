Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 471112 Color: 472
Size: 271698 Color: 126
Size: 257191 Color: 52

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 482948 Color: 485
Size: 265957 Color: 107
Size: 251096 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 476131 Color: 476
Size: 263550 Color: 95
Size: 260320 Color: 76

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 356797 Color: 339
Size: 356659 Color: 338
Size: 286545 Color: 170

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 456309 Color: 457
Size: 260506 Color: 77
Size: 283186 Color: 157

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 489573 Color: 493
Size: 258484 Color: 62
Size: 251944 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 375726 Color: 359
Size: 344705 Color: 320
Size: 279570 Color: 142

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 411619 Color: 415
Size: 334309 Color: 302
Size: 254073 Color: 32

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 385810 Color: 372
Size: 332547 Color: 298
Size: 281644 Color: 154

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 386028 Color: 373
Size: 324765 Color: 274
Size: 289208 Color: 182

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 466813 Color: 469
Size: 281295 Color: 150
Size: 251893 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 480264 Color: 483
Size: 269042 Color: 119
Size: 250695 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 390605 Color: 379
Size: 307726 Color: 231
Size: 301670 Color: 218

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 462807 Color: 463
Size: 284427 Color: 164
Size: 252767 Color: 23

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 465807 Color: 468
Size: 272634 Color: 129
Size: 261560 Color: 81

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 429762 Color: 436
Size: 320191 Color: 264
Size: 250048 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 409540 Color: 412
Size: 332347 Color: 297
Size: 258114 Color: 57

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 397455 Color: 392
Size: 331296 Color: 295
Size: 271250 Color: 124

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 420222 Color: 427
Size: 307869 Color: 232
Size: 271910 Color: 127

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 399547 Color: 394
Size: 342095 Color: 316
Size: 258359 Color: 61

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 397356 Color: 391
Size: 314942 Color: 246
Size: 287703 Color: 177

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 426613 Color: 433
Size: 321885 Color: 267
Size: 251503 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 370267 Color: 353
Size: 333680 Color: 301
Size: 296054 Color: 204

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 405789 Color: 403
Size: 325664 Color: 276
Size: 268548 Color: 115

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 409170 Color: 411
Size: 336733 Color: 305
Size: 254098 Color: 33

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 377062 Color: 360
Size: 338147 Color: 308
Size: 284792 Color: 165

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 491179 Color: 495
Size: 255578 Color: 39
Size: 253244 Color: 27

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 396115 Color: 388
Size: 317848 Color: 258
Size: 286038 Color: 169

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 387201 Color: 377
Size: 350459 Color: 329
Size: 262341 Color: 87

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 361032 Color: 342
Size: 329134 Color: 287
Size: 309835 Color: 238

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 453063 Color: 453
Size: 296227 Color: 206
Size: 250711 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 422412 Color: 429
Size: 315983 Color: 253
Size: 261606 Color: 82

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 429909 Color: 437
Size: 288450 Color: 180
Size: 281642 Color: 153

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 424810 Color: 431
Size: 300155 Color: 214
Size: 275036 Color: 135

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 412550 Color: 416
Size: 293780 Color: 203
Size: 293671 Color: 201

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 374510 Color: 358
Size: 374322 Color: 357
Size: 251169 Color: 12

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 419167 Color: 426
Size: 315708 Color: 251
Size: 265126 Color: 104

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 404131 Color: 402
Size: 308404 Color: 234
Size: 287466 Color: 174

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 361140 Color: 343
Size: 353230 Color: 335
Size: 285631 Color: 166

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 416553 Color: 423
Size: 316133 Color: 254
Size: 267315 Color: 112

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 478877 Color: 478
Size: 261903 Color: 86
Size: 259221 Color: 68

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 472060 Color: 474
Size: 270019 Color: 121
Size: 257922 Color: 55

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 430952 Color: 439
Size: 313017 Color: 242
Size: 256032 Color: 43

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 465729 Color: 467
Size: 283941 Color: 160
Size: 250331 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 445291 Color: 448
Size: 299769 Color: 212
Size: 254941 Color: 35

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 350704 Color: 330
Size: 330357 Color: 292
Size: 318940 Color: 263

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 400859 Color: 396
Size: 333585 Color: 300
Size: 265557 Color: 105

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 386884 Color: 375
Size: 334716 Color: 303
Size: 278401 Color: 139

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 396670 Color: 390
Size: 328977 Color: 285
Size: 274354 Color: 133

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 491997 Color: 497
Size: 255035 Color: 36
Size: 252969 Color: 25

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 489730 Color: 494
Size: 257088 Color: 49
Size: 253183 Color: 26

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 352852 Color: 334
Size: 332177 Color: 296
Size: 314972 Color: 247

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 493092 Color: 498
Size: 255753 Color: 41
Size: 251156 Color: 11

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 392462 Color: 380
Size: 326198 Color: 278
Size: 281341 Color: 151

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 445551 Color: 449
Size: 287541 Color: 175
Size: 266909 Color: 108

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 412943 Color: 417
Size: 299317 Color: 210
Size: 287741 Color: 178

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 430185 Color: 438
Size: 293472 Color: 200
Size: 276344 Color: 136

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 384364 Color: 371
Size: 326497 Color: 279
Size: 289140 Color: 181

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 458261 Color: 460
Size: 290013 Color: 186
Size: 251727 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 363657 Color: 346
Size: 345249 Color: 322
Size: 291095 Color: 191

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 407286 Color: 406
Size: 341805 Color: 315
Size: 250910 Color: 8

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 499259 Color: 500
Size: 250693 Color: 4
Size: 250049 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 393424 Color: 383
Size: 350367 Color: 328
Size: 256210 Color: 46

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 390295 Color: 378
Size: 349641 Color: 327
Size: 260065 Color: 71

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 479040 Color: 480
Size: 263591 Color: 96
Size: 257370 Color: 53

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 383207 Color: 367
Size: 329826 Color: 288
Size: 286968 Color: 171

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 467906 Color: 470
Size: 268812 Color: 118
Size: 263283 Color: 93

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 468329 Color: 471
Size: 266940 Color: 109
Size: 264732 Color: 101

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 429312 Color: 435
Size: 287194 Color: 173
Size: 283495 Color: 158

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 384122 Color: 369
Size: 354877 Color: 337
Size: 261002 Color: 79

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 464298 Color: 465
Size: 282869 Color: 156
Size: 252834 Color: 24

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 362355 Color: 344
Size: 320544 Color: 265
Size: 317102 Color: 257

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 494307 Color: 499
Size: 254781 Color: 34
Size: 250913 Color: 9

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 407869 Color: 409
Size: 299688 Color: 211
Size: 292444 Color: 193

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 426493 Color: 432
Size: 315577 Color: 249
Size: 257931 Color: 56

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 476087 Color: 475
Size: 264727 Color: 100
Size: 259187 Color: 67

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 382550 Color: 366
Size: 360358 Color: 341
Size: 257093 Color: 50

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 379360 Color: 362
Size: 328134 Color: 284
Size: 292507 Color: 194

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 393742 Color: 385
Size: 306236 Color: 226
Size: 300023 Color: 213

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 471592 Color: 473
Size: 269555 Color: 120
Size: 258854 Color: 64

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 481329 Color: 484
Size: 264979 Color: 103
Size: 253693 Color: 30

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 371856 Color: 354
Size: 351460 Color: 331
Size: 276685 Color: 138

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 401236 Color: 397
Size: 327192 Color: 283
Size: 271573 Color: 125

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 365359 Color: 350
Size: 318865 Color: 262
Size: 315777 Color: 252

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 458507 Color: 461
Size: 278932 Color: 140
Size: 262562 Color: 88

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 400357 Color: 395
Size: 318417 Color: 259
Size: 281227 Color: 148

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 450267 Color: 452
Size: 292711 Color: 195
Size: 257023 Color: 48

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 422520 Color: 430
Size: 291488 Color: 192
Size: 285993 Color: 168

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 364250 Color: 348
Size: 327163 Color: 282
Size: 308588 Color: 235

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 344783 Color: 321
Size: 341717 Color: 314
Size: 313501 Color: 243

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 413788 Color: 421
Size: 296696 Color: 207
Size: 289517 Color: 185

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 397927 Color: 393
Size: 338839 Color: 309
Size: 263235 Color: 92

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 413414 Color: 418
Size: 297370 Color: 208
Size: 289217 Color: 183

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 381924 Color: 365
Size: 311951 Color: 240
Size: 306126 Color: 225

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 363452 Color: 345
Size: 323858 Color: 271
Size: 312691 Color: 241

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 420883 Color: 428
Size: 314545 Color: 245
Size: 264573 Color: 98

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 342418 Color: 317
Size: 333427 Color: 299
Size: 324156 Color: 272

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 444858 Color: 447
Size: 301083 Color: 217
Size: 254060 Color: 31

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 453771 Color: 454
Size: 274074 Color: 132
Size: 272156 Color: 128

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 488388 Color: 491
Size: 258156 Color: 59
Size: 253457 Color: 29

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 380532 Color: 364
Size: 329965 Color: 290
Size: 289504 Color: 184

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 448010 Color: 450
Size: 287039 Color: 172
Size: 264952 Color: 102

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 407341 Color: 408
Size: 330783 Color: 293
Size: 261877 Color: 85

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 406233 Color: 404
Size: 300522 Color: 215
Size: 293246 Color: 199

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 448659 Color: 451
Size: 293677 Color: 202
Size: 257665 Color: 54

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 410672 Color: 414
Size: 330995 Color: 294
Size: 258334 Color: 60

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 406508 Color: 405
Size: 341418 Color: 313
Size: 252075 Color: 20

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 378153 Color: 361
Size: 313799 Color: 244
Size: 308049 Color: 233

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 367373 Color: 352
Size: 339588 Color: 311
Size: 293040 Color: 198

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 393254 Color: 382
Size: 343636 Color: 318
Size: 263111 Color: 91

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 387132 Color: 376
Size: 356997 Color: 340
Size: 255872 Color: 42

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 456037 Color: 456
Size: 282127 Color: 155
Size: 261837 Color: 84

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 379755 Color: 363
Size: 352829 Color: 332
Size: 267417 Color: 113

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 401797 Color: 398
Size: 316823 Color: 255
Size: 281381 Color: 152

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 395368 Color: 387
Size: 317027 Color: 256
Size: 287606 Color: 176

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 402920 Color: 399
Size: 311271 Color: 239
Size: 285810 Color: 167

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 479565 Color: 481
Size: 260226 Color: 75
Size: 260210 Color: 74

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 408098 Color: 410
Size: 340417 Color: 312
Size: 251486 Color: 13

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 476236 Color: 477
Size: 267278 Color: 111
Size: 256487 Color: 47

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 403006 Color: 400
Size: 304238 Color: 223
Size: 292757 Color: 196

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 440838 Color: 446
Size: 303810 Color: 222
Size: 255353 Color: 38

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 343846 Color: 319
Size: 337496 Color: 306
Size: 318659 Color: 260

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 413609 Color: 420
Size: 302285 Color: 220
Size: 284107 Color: 161

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 392811 Color: 381
Size: 322953 Color: 269
Size: 284237 Color: 163

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 418429 Color: 424
Size: 301751 Color: 219
Size: 279821 Color: 143

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 483353 Color: 486
Size: 264723 Color: 99
Size: 251925 Color: 18

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 463006 Color: 464
Size: 281291 Color: 149
Size: 255704 Color: 40

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 438865 Color: 443
Size: 290534 Color: 188
Size: 270602 Color: 123

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 384181 Color: 370
Size: 325697 Color: 277
Size: 290123 Color: 187

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 478878 Color: 479
Size: 267763 Color: 114
Size: 253360 Color: 28

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 491913 Color: 496
Size: 256199 Color: 45
Size: 251889 Color: 16

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 349425 Color: 326
Size: 335287 Color: 304
Size: 315289 Color: 248

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 432079 Color: 441
Size: 304834 Color: 224
Size: 263088 Color: 90

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 487467 Color: 490
Size: 259767 Color: 70
Size: 252767 Color: 22

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 407290 Color: 407
Size: 318774 Color: 261
Size: 273937 Color: 131

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 372256 Color: 356
Size: 339418 Color: 310
Size: 288327 Color: 179

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 418656 Color: 425
Size: 321244 Color: 266
Size: 260101 Color: 72

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 464856 Color: 466
Size: 274991 Color: 134
Size: 260154 Color: 73

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 383899 Color: 368
Size: 308977 Color: 236
Size: 307125 Color: 229

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 440375 Color: 445
Size: 290958 Color: 190
Size: 268668 Color: 117

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 403511 Color: 401
Size: 337985 Color: 307
Size: 258505 Color: 63

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 396258 Color: 389
Size: 327066 Color: 281
Size: 276677 Color: 137

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 346341 Color: 324
Size: 328997 Color: 286
Size: 324663 Color: 273

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 479951 Color: 482
Size: 260549 Color: 78
Size: 259501 Color: 69

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 435146 Color: 442
Size: 296222 Color: 205
Size: 268633 Color: 116

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 413512 Color: 419
Size: 322771 Color: 268
Size: 263718 Color: 97

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 440008 Color: 444
Size: 301028 Color: 216
Size: 258965 Color: 66

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 363696 Color: 347
Size: 345385 Color: 323
Size: 290920 Color: 189

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 456873 Color: 458
Size: 293011 Color: 197
Size: 250117 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 431431 Color: 440
Size: 307042 Color: 228
Size: 261528 Color: 80

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 372214 Color: 355
Size: 330165 Color: 291
Size: 297622 Color: 209

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 428985 Color: 434
Size: 307539 Color: 230
Size: 263477 Color: 94

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 347119 Color: 325
Size: 329888 Color: 289
Size: 322994 Color: 270

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 394182 Color: 386
Size: 325049 Color: 275
Size: 280770 Color: 146

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 366478 Color: 351
Size: 352850 Color: 333
Size: 280673 Color: 145

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 459676 Color: 462
Size: 273219 Color: 130
Size: 267106 Color: 110

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 364528 Color: 349
Size: 354296 Color: 336
Size: 281177 Color: 147

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 454395 Color: 455
Size: 279900 Color: 144
Size: 265706 Color: 106

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 486405 Color: 489
Size: 262734 Color: 89
Size: 250862 Color: 7

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 488665 Color: 492
Size: 256089 Color: 44
Size: 255247 Color: 37

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 386666 Color: 374
Size: 309762 Color: 237
Size: 303573 Color: 221

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 485874 Color: 488
Size: 261678 Color: 83
Size: 252449 Color: 21

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 414014 Color: 422
Size: 315694 Color: 250
Size: 270293 Color: 122

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 393645 Color: 384
Size: 326858 Color: 280
Size: 279498 Color: 141

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 410087 Color: 413
Size: 306339 Color: 227
Size: 283575 Color: 159

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 483925 Color: 487
Size: 258886 Color: 65
Size: 257190 Color: 51

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 457691 Color: 459
Size: 284162 Color: 162
Size: 258148 Color: 58

Total size: 167000167
Total free space: 0

