Capicity Bin: 9504
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 13
Items: 
Size: 1200 Color: 3
Size: 1098 Color: 1
Size: 914 Color: 1
Size: 828 Color: 3
Size: 790 Color: 3
Size: 788 Color: 3
Size: 788 Color: 2
Size: 704 Color: 0
Size: 688 Color: 2
Size: 640 Color: 0
Size: 528 Color: 2
Size: 280 Color: 1
Size: 258 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 4
Size: 3954 Color: 2
Size: 788 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 2
Size: 2905 Color: 4
Size: 1821 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5346 Color: 2
Size: 3946 Color: 0
Size: 212 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 4
Size: 3414 Color: 1
Size: 216 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 0
Size: 3444 Color: 1
Size: 184 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 2
Size: 3028 Color: 1
Size: 536 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6537 Color: 3
Size: 2455 Color: 4
Size: 512 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 2
Size: 2456 Color: 3
Size: 228 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 1
Size: 2198 Color: 4
Size: 372 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 1
Size: 1844 Color: 4
Size: 368 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 0
Size: 1948 Color: 1
Size: 266 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7414 Color: 0
Size: 1148 Color: 1
Size: 942 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7573 Color: 1
Size: 1611 Color: 0
Size: 320 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7572 Color: 2
Size: 1612 Color: 1
Size: 320 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7608 Color: 3
Size: 1608 Color: 1
Size: 288 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7590 Color: 1
Size: 1846 Color: 0
Size: 68 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7707 Color: 4
Size: 1499 Color: 1
Size: 298 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7766 Color: 2
Size: 1142 Color: 1
Size: 596 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7780 Color: 2
Size: 1296 Color: 1
Size: 428 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7800 Color: 0
Size: 1444 Color: 3
Size: 260 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7864 Color: 0
Size: 1204 Color: 0
Size: 436 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7878 Color: 4
Size: 1310 Color: 1
Size: 316 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7920 Color: 3
Size: 1328 Color: 1
Size: 256 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7928 Color: 4
Size: 1320 Color: 1
Size: 256 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7934 Color: 2
Size: 1208 Color: 4
Size: 362 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8010 Color: 1
Size: 900 Color: 4
Size: 594 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8004 Color: 4
Size: 1172 Color: 3
Size: 328 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 8042 Color: 1
Size: 1222 Color: 3
Size: 240 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8068 Color: 0
Size: 972 Color: 2
Size: 464 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 8078 Color: 0
Size: 1042 Color: 1
Size: 384 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 8100 Color: 4
Size: 1166 Color: 1
Size: 238 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 8110 Color: 1
Size: 802 Color: 2
Size: 592 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 1
Size: 856 Color: 3
Size: 432 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 8244 Color: 1
Size: 1080 Color: 3
Size: 180 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 8222 Color: 3
Size: 1188 Color: 2
Size: 94 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 8340 Color: 1
Size: 788 Color: 4
Size: 376 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 8344 Color: 1
Size: 600 Color: 0
Size: 560 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 8308 Color: 3
Size: 704 Color: 1
Size: 492 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 8346 Color: 1
Size: 922 Color: 0
Size: 236 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 8378 Color: 4
Size: 886 Color: 2
Size: 240 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 3
Size: 810 Color: 4
Size: 284 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 8442 Color: 0
Size: 790 Color: 2
Size: 272 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 8470 Color: 2
Size: 922 Color: 1
Size: 112 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 1
Size: 692 Color: 4
Size: 298 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 8516 Color: 3
Size: 684 Color: 1
Size: 304 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 8542 Color: 4
Size: 790 Color: 3
Size: 172 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5378 Color: 4
Size: 3961 Color: 1
Size: 164 Color: 3

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 2
Size: 2979 Color: 0
Size: 250 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 2
Size: 2600 Color: 2
Size: 518 Color: 0

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 6763 Color: 4
Size: 2740 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 7067 Color: 0
Size: 1246 Color: 1
Size: 1190 Color: 0

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 2
Size: 2072 Color: 1
Size: 304 Color: 2

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 7333 Color: 1
Size: 2002 Color: 0
Size: 168 Color: 0

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 7719 Color: 1
Size: 1720 Color: 0
Size: 64 Color: 4

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 4765 Color: 4
Size: 3953 Color: 1
Size: 784 Color: 3

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 3
Size: 3962 Color: 2
Size: 176 Color: 0

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5931 Color: 0
Size: 3411 Color: 1
Size: 160 Color: 0

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 4
Size: 3002 Color: 2

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 6664 Color: 0
Size: 2606 Color: 1
Size: 232 Color: 4

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 7192 Color: 4
Size: 2142 Color: 3
Size: 168 Color: 4

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 7256 Color: 1
Size: 1742 Color: 3
Size: 504 Color: 4

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 7548 Color: 2
Size: 1522 Color: 3
Size: 432 Color: 1

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 7853 Color: 4
Size: 1325 Color: 0
Size: 324 Color: 1

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 8534 Color: 0
Size: 968 Color: 3

Bin 66: 3 of cap free
Amount of items: 7
Items: 
Size: 4753 Color: 4
Size: 1297 Color: 2
Size: 1231 Color: 3
Size: 852 Color: 2
Size: 784 Color: 0
Size: 336 Color: 0
Size: 248 Color: 4

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 4761 Color: 1
Size: 3956 Color: 3
Size: 784 Color: 4

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 0
Size: 2969 Color: 1
Size: 176 Color: 0

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 6373 Color: 1
Size: 2972 Color: 3
Size: 156 Color: 3

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 7531 Color: 3
Size: 1574 Color: 1
Size: 396 Color: 2

Bin 71: 3 of cap free
Amount of items: 2
Items: 
Size: 8132 Color: 3
Size: 1369 Color: 2

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 4
Size: 2136 Color: 2
Size: 456 Color: 1

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 6952 Color: 3
Size: 2548 Color: 2

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 8248 Color: 3
Size: 1252 Color: 0

Bin 75: 5 of cap free
Amount of items: 4
Items: 
Size: 4756 Color: 1
Size: 2418 Color: 2
Size: 1981 Color: 1
Size: 344 Color: 0

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 6518 Color: 1
Size: 2601 Color: 2
Size: 380 Color: 2

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 6751 Color: 2
Size: 2220 Color: 4
Size: 528 Color: 1

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 5688 Color: 2
Size: 3442 Color: 4
Size: 368 Color: 2

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6870 Color: 1
Size: 2628 Color: 3

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 8428 Color: 4
Size: 1070 Color: 0

Bin 81: 8 of cap free
Amount of items: 3
Items: 
Size: 6844 Color: 2
Size: 2508 Color: 1
Size: 144 Color: 3

Bin 82: 8 of cap free
Amount of items: 3
Items: 
Size: 8368 Color: 3
Size: 1048 Color: 0
Size: 80 Color: 0

Bin 83: 9 of cap free
Amount of items: 4
Items: 
Size: 4757 Color: 3
Size: 3942 Color: 4
Size: 604 Color: 1
Size: 192 Color: 2

Bin 84: 12 of cap free
Amount of items: 3
Items: 
Size: 5943 Color: 1
Size: 3421 Color: 2
Size: 128 Color: 4

Bin 85: 12 of cap free
Amount of items: 2
Items: 
Size: 6300 Color: 3
Size: 3192 Color: 0

Bin 86: 12 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 0
Size: 2694 Color: 3
Size: 406 Color: 1

Bin 87: 12 of cap free
Amount of items: 2
Items: 
Size: 6888 Color: 0
Size: 2604 Color: 4

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 7756 Color: 3
Size: 1736 Color: 0

Bin 89: 12 of cap free
Amount of items: 2
Items: 
Size: 8134 Color: 2
Size: 1358 Color: 3

Bin 90: 12 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 4
Size: 976 Color: 0
Size: 32 Color: 2

Bin 91: 14 of cap free
Amount of items: 5
Items: 
Size: 4754 Color: 1
Size: 1598 Color: 1
Size: 1422 Color: 4
Size: 1028 Color: 3
Size: 688 Color: 0

Bin 92: 14 of cap free
Amount of items: 2
Items: 
Size: 7228 Color: 2
Size: 2262 Color: 0

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 7933 Color: 0
Size: 1557 Color: 2

Bin 94: 15 of cap free
Amount of items: 3
Items: 
Size: 5372 Color: 1
Size: 3957 Color: 0
Size: 160 Color: 4

Bin 95: 15 of cap free
Amount of items: 3
Items: 
Size: 5413 Color: 4
Size: 3452 Color: 1
Size: 624 Color: 0

Bin 96: 15 of cap free
Amount of items: 2
Items: 
Size: 7678 Color: 0
Size: 1811 Color: 3

Bin 97: 15 of cap free
Amount of items: 3
Items: 
Size: 7901 Color: 3
Size: 1460 Color: 2
Size: 128 Color: 2

Bin 98: 18 of cap free
Amount of items: 2
Items: 
Size: 7997 Color: 0
Size: 1489 Color: 3

Bin 99: 19 of cap free
Amount of items: 2
Items: 
Size: 7321 Color: 4
Size: 2164 Color: 3

Bin 100: 19 of cap free
Amount of items: 2
Items: 
Size: 7863 Color: 4
Size: 1622 Color: 0

Bin 101: 19 of cap free
Amount of items: 2
Items: 
Size: 8029 Color: 2
Size: 1456 Color: 0

Bin 102: 20 of cap free
Amount of items: 2
Items: 
Size: 7584 Color: 4
Size: 1900 Color: 2

Bin 103: 20 of cap free
Amount of items: 2
Items: 
Size: 8084 Color: 0
Size: 1400 Color: 4

Bin 104: 22 of cap free
Amount of items: 2
Items: 
Size: 6072 Color: 2
Size: 3410 Color: 4

Bin 105: 22 of cap free
Amount of items: 3
Items: 
Size: 6420 Color: 3
Size: 1725 Color: 3
Size: 1337 Color: 1

Bin 106: 22 of cap free
Amount of items: 2
Items: 
Size: 8254 Color: 3
Size: 1228 Color: 0

Bin 107: 24 of cap free
Amount of items: 3
Items: 
Size: 6909 Color: 3
Size: 2475 Color: 2
Size: 96 Color: 2

Bin 108: 24 of cap free
Amount of items: 2
Items: 
Size: 7672 Color: 3
Size: 1808 Color: 4

Bin 109: 24 of cap free
Amount of items: 2
Items: 
Size: 7844 Color: 2
Size: 1636 Color: 4

Bin 110: 25 of cap free
Amount of items: 2
Items: 
Size: 7448 Color: 4
Size: 2031 Color: 3

Bin 111: 26 of cap free
Amount of items: 2
Items: 
Size: 6452 Color: 3
Size: 3026 Color: 4

Bin 112: 26 of cap free
Amount of items: 2
Items: 
Size: 6606 Color: 3
Size: 2872 Color: 0

Bin 113: 26 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 0
Size: 2376 Color: 2

Bin 114: 27 of cap free
Amount of items: 2
Items: 
Size: 7949 Color: 2
Size: 1528 Color: 4

Bin 115: 30 of cap free
Amount of items: 2
Items: 
Size: 8488 Color: 0
Size: 986 Color: 3

Bin 116: 31 of cap free
Amount of items: 3
Items: 
Size: 5401 Color: 0
Size: 3576 Color: 1
Size: 496 Color: 3

Bin 117: 34 of cap free
Amount of items: 2
Items: 
Size: 6794 Color: 2
Size: 2676 Color: 1

Bin 118: 44 of cap free
Amount of items: 2
Items: 
Size: 8072 Color: 4
Size: 1388 Color: 0

Bin 119: 46 of cap free
Amount of items: 2
Items: 
Size: 4770 Color: 3
Size: 4688 Color: 2

Bin 120: 47 of cap free
Amount of items: 2
Items: 
Size: 7172 Color: 2
Size: 2285 Color: 1

Bin 121: 54 of cap free
Amount of items: 2
Items: 
Size: 5410 Color: 0
Size: 4040 Color: 3

Bin 122: 57 of cap free
Amount of items: 2
Items: 
Size: 8190 Color: 2
Size: 1257 Color: 3

Bin 123: 61 of cap free
Amount of items: 2
Items: 
Size: 7798 Color: 3
Size: 1645 Color: 2

Bin 124: 62 of cap free
Amount of items: 26
Items: 
Size: 682 Color: 3
Size: 680 Color: 3
Size: 592 Color: 0
Size: 520 Color: 1
Size: 480 Color: 4
Size: 448 Color: 1
Size: 448 Color: 1
Size: 440 Color: 2
Size: 416 Color: 2
Size: 400 Color: 1
Size: 368 Color: 2
Size: 360 Color: 4
Size: 348 Color: 3
Size: 320 Color: 2
Size: 296 Color: 0
Size: 272 Color: 0
Size: 268 Color: 2
Size: 248 Color: 0
Size: 246 Color: 3
Size: 244 Color: 0
Size: 240 Color: 2
Size: 232 Color: 2
Size: 232 Color: 0
Size: 228 Color: 2
Size: 226 Color: 3
Size: 208 Color: 4

Bin 125: 62 of cap free
Amount of items: 2
Items: 
Size: 5938 Color: 3
Size: 3504 Color: 2

Bin 126: 66 of cap free
Amount of items: 2
Items: 
Size: 7558 Color: 0
Size: 1880 Color: 4

Bin 127: 72 of cap free
Amount of items: 3
Items: 
Size: 4764 Color: 4
Size: 3964 Color: 1
Size: 704 Color: 3

Bin 128: 83 of cap free
Amount of items: 2
Items: 
Size: 7258 Color: 4
Size: 2163 Color: 0

Bin 129: 88 of cap free
Amount of items: 2
Items: 
Size: 5224 Color: 0
Size: 4192 Color: 2

Bin 130: 89 of cap free
Amount of items: 2
Items: 
Size: 7541 Color: 2
Size: 1874 Color: 3

Bin 131: 113 of cap free
Amount of items: 3
Items: 
Size: 4760 Color: 3
Size: 3951 Color: 0
Size: 680 Color: 1

Bin 132: 132 of cap free
Amount of items: 2
Items: 
Size: 5906 Color: 0
Size: 3466 Color: 4

Bin 133: 7648 of cap free
Amount of items: 10
Items: 
Size: 224 Color: 2
Size: 224 Color: 1
Size: 208 Color: 1
Size: 208 Color: 1
Size: 192 Color: 4
Size: 160 Color: 4
Size: 160 Color: 4
Size: 160 Color: 3
Size: 160 Color: 0
Size: 160 Color: 0

Total size: 1254528
Total free space: 9504

