Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9828 Color: 2
Size: 8188 Color: 2
Size: 1632 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10416 Color: 3
Size: 7920 Color: 0
Size: 1312 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11128 Color: 1
Size: 7696 Color: 1
Size: 824 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11168 Color: 4
Size: 8160 Color: 1
Size: 320 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12256 Color: 3
Size: 6176 Color: 2
Size: 1216 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12478 Color: 3
Size: 6640 Color: 2
Size: 530 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13140 Color: 2
Size: 4298 Color: 4
Size: 2210 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13152 Color: 0
Size: 5424 Color: 4
Size: 1072 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 2
Size: 6020 Color: 3
Size: 468 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13398 Color: 2
Size: 5610 Color: 3
Size: 640 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13856 Color: 3
Size: 4280 Color: 0
Size: 1512 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 3
Size: 5368 Color: 1
Size: 288 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 1
Size: 4368 Color: 3
Size: 1248 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14374 Color: 3
Size: 4830 Color: 2
Size: 444 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14379 Color: 1
Size: 4391 Color: 4
Size: 878 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 2
Size: 4382 Color: 0
Size: 876 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14443 Color: 0
Size: 4339 Color: 3
Size: 866 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 14480 Color: 0
Size: 4624 Color: 3
Size: 544 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 0
Size: 4068 Color: 3
Size: 1046 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14564 Color: 4
Size: 4398 Color: 2
Size: 686 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14584 Color: 0
Size: 3832 Color: 3
Size: 1232 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14692 Color: 2
Size: 3548 Color: 0
Size: 1408 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14942 Color: 3
Size: 4262 Color: 4
Size: 444 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 15016 Color: 3
Size: 3436 Color: 2
Size: 1196 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 15192 Color: 3
Size: 4016 Color: 1
Size: 440 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 15344 Color: 0
Size: 3096 Color: 3
Size: 1208 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 15384 Color: 3
Size: 3864 Color: 4
Size: 400 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 15402 Color: 4
Size: 3542 Color: 4
Size: 704 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 15436 Color: 4
Size: 3436 Color: 1
Size: 776 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 15468 Color: 1
Size: 3364 Color: 3
Size: 816 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 15564 Color: 2
Size: 3768 Color: 3
Size: 316 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 15696 Color: 3
Size: 3184 Color: 2
Size: 768 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 15856 Color: 3
Size: 3408 Color: 1
Size: 384 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 15939 Color: 3
Size: 3091 Color: 2
Size: 618 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 15840 Color: 4
Size: 3552 Color: 1
Size: 256 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 15992 Color: 3
Size: 3232 Color: 4
Size: 424 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16112 Color: 3
Size: 2928 Color: 2
Size: 608 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16196 Color: 3
Size: 2256 Color: 4
Size: 1196 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16228 Color: 3
Size: 2844 Color: 4
Size: 576 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16304 Color: 3
Size: 1712 Color: 0
Size: 1632 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16314 Color: 3
Size: 2942 Color: 1
Size: 392 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 16474 Color: 3
Size: 2350 Color: 0
Size: 824 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 16456 Color: 2
Size: 1760 Color: 2
Size: 1432 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 16520 Color: 2
Size: 2656 Color: 2
Size: 472 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 16800 Color: 0
Size: 2400 Color: 3
Size: 448 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 16828 Color: 0
Size: 1956 Color: 0
Size: 864 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 16888 Color: 3
Size: 2176 Color: 4
Size: 584 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 16936 Color: 1
Size: 1960 Color: 3
Size: 752 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 16976 Color: 2
Size: 1744 Color: 4
Size: 928 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 16982 Color: 3
Size: 1594 Color: 1
Size: 1072 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17009 Color: 4
Size: 2393 Color: 3
Size: 246 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17104 Color: 3
Size: 2208 Color: 1
Size: 336 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 17046 Color: 0
Size: 1824 Color: 4
Size: 778 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 17056 Color: 0
Size: 1568 Color: 2
Size: 1024 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 17126 Color: 2
Size: 2102 Color: 2
Size: 420 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 17240 Color: 4
Size: 1704 Color: 0
Size: 704 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 17300 Color: 0
Size: 1964 Color: 4
Size: 384 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 17304 Color: 2
Size: 2312 Color: 1
Size: 32 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 17308 Color: 0
Size: 1568 Color: 4
Size: 772 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 17467 Color: 4
Size: 1937 Color: 3
Size: 244 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 17504 Color: 4
Size: 1632 Color: 1
Size: 512 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 17544 Color: 1
Size: 1682 Color: 3
Size: 422 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 17608 Color: 3
Size: 1160 Color: 4
Size: 880 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 17584 Color: 4
Size: 1600 Color: 2
Size: 464 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 17632 Color: 2
Size: 1248 Color: 1
Size: 768 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 17666 Color: 1
Size: 1006 Color: 3
Size: 976 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11387 Color: 3
Size: 6882 Color: 2
Size: 1378 Color: 3

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12364 Color: 2
Size: 6885 Color: 4
Size: 398 Color: 3

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12462 Color: 2
Size: 5987 Color: 1
Size: 1198 Color: 4

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 13347 Color: 3
Size: 6300 Color: 4

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13635 Color: 3
Size: 5372 Color: 4
Size: 640 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13687 Color: 4
Size: 5416 Color: 1
Size: 544 Color: 3

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 15009 Color: 3
Size: 3948 Color: 1
Size: 690 Color: 2

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 15039 Color: 4
Size: 4244 Color: 0
Size: 364 Color: 4

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 15500 Color: 1
Size: 3451 Color: 1
Size: 696 Color: 3

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 15523 Color: 0
Size: 3516 Color: 2
Size: 608 Color: 3

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 16463 Color: 3
Size: 2852 Color: 2
Size: 332 Color: 2

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 16591 Color: 3
Size: 2128 Color: 1
Size: 928 Color: 2

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 16855 Color: 4
Size: 2096 Color: 0
Size: 696 Color: 3

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 17253 Color: 3
Size: 1778 Color: 2
Size: 616 Color: 4

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 17285 Color: 3
Size: 1592 Color: 2
Size: 770 Color: 0

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 17413 Color: 4
Size: 1196 Color: 3
Size: 1038 Color: 2

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 17451 Color: 1
Size: 1424 Color: 0
Size: 772 Color: 3

Bin 84: 2 of cap free
Amount of items: 5
Items: 
Size: 9827 Color: 2
Size: 4164 Color: 0
Size: 3425 Color: 2
Size: 1654 Color: 2
Size: 576 Color: 3

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12208 Color: 2
Size: 7072 Color: 0
Size: 366 Color: 4

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 12218 Color: 2
Size: 6576 Color: 3
Size: 852 Color: 0

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 13363 Color: 3
Size: 5323 Color: 3
Size: 960 Color: 4

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 0
Size: 5472 Color: 2
Size: 320 Color: 0

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 0
Size: 5448 Color: 3
Size: 328 Color: 4

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 14352 Color: 0
Size: 3072 Color: 2
Size: 2222 Color: 3

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 14406 Color: 0
Size: 4032 Color: 3
Size: 1208 Color: 2

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 15006 Color: 2
Size: 3872 Color: 3
Size: 768 Color: 1

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 15022 Color: 0
Size: 4048 Color: 1
Size: 576 Color: 3

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 15128 Color: 3
Size: 4320 Color: 0
Size: 198 Color: 1

Bin 95: 2 of cap free
Amount of items: 2
Items: 
Size: 15396 Color: 4
Size: 4250 Color: 0

Bin 96: 2 of cap free
Amount of items: 2
Items: 
Size: 15414 Color: 2
Size: 4232 Color: 1

Bin 97: 2 of cap free
Amount of items: 2
Items: 
Size: 17454 Color: 2
Size: 2192 Color: 0

Bin 98: 2 of cap free
Amount of items: 3
Items: 
Size: 17616 Color: 2
Size: 1978 Color: 1
Size: 52 Color: 1

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 13611 Color: 1
Size: 4818 Color: 2
Size: 1216 Color: 3

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 15236 Color: 0
Size: 3841 Color: 3
Size: 568 Color: 0

Bin 101: 3 of cap free
Amount of items: 2
Items: 
Size: 15320 Color: 1
Size: 4325 Color: 0

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 16122 Color: 3
Size: 2131 Color: 1
Size: 1392 Color: 2

Bin 103: 3 of cap free
Amount of items: 3
Items: 
Size: 16627 Color: 2
Size: 2646 Color: 3
Size: 372 Color: 0

Bin 104: 3 of cap free
Amount of items: 3
Items: 
Size: 17518 Color: 1
Size: 2111 Color: 0
Size: 16 Color: 1

Bin 105: 3 of cap free
Amount of items: 3
Items: 
Size: 17568 Color: 0
Size: 1997 Color: 2
Size: 80 Color: 4

Bin 106: 4 of cap free
Amount of items: 7
Items: 
Size: 9848 Color: 0
Size: 1863 Color: 4
Size: 1831 Color: 1
Size: 1830 Color: 3
Size: 1696 Color: 1
Size: 1376 Color: 1
Size: 1200 Color: 0

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 3
Size: 7872 Color: 2
Size: 680 Color: 4

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 13844 Color: 1
Size: 5116 Color: 3
Size: 684 Color: 1

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 14500 Color: 0
Size: 4688 Color: 3
Size: 456 Color: 0

Bin 110: 4 of cap free
Amount of items: 2
Items: 
Size: 14800 Color: 1
Size: 4844 Color: 2

Bin 111: 4 of cap free
Amount of items: 2
Items: 
Size: 16160 Color: 0
Size: 3484 Color: 1

Bin 112: 5 of cap free
Amount of items: 2
Items: 
Size: 16907 Color: 1
Size: 2736 Color: 2

Bin 113: 5 of cap free
Amount of items: 2
Items: 
Size: 17672 Color: 3
Size: 1971 Color: 1

Bin 114: 6 of cap free
Amount of items: 3
Items: 
Size: 11028 Color: 4
Size: 7570 Color: 2
Size: 1044 Color: 4

Bin 115: 6 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 1
Size: 6986 Color: 2
Size: 640 Color: 1

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 12394 Color: 1
Size: 6256 Color: 2
Size: 992 Color: 0

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 13204 Color: 1
Size: 5990 Color: 3
Size: 448 Color: 2

Bin 118: 6 of cap free
Amount of items: 3
Items: 
Size: 16480 Color: 1
Size: 3098 Color: 2
Size: 64 Color: 3

Bin 119: 6 of cap free
Amount of items: 2
Items: 
Size: 17634 Color: 0
Size: 2008 Color: 1

Bin 120: 7 of cap free
Amount of items: 3
Items: 
Size: 12449 Color: 1
Size: 6136 Color: 3
Size: 1056 Color: 2

Bin 121: 7 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 4
Size: 6001 Color: 2
Size: 408 Color: 0

Bin 122: 7 of cap free
Amount of items: 3
Items: 
Size: 15539 Color: 0
Size: 3600 Color: 3
Size: 502 Color: 1

Bin 123: 7 of cap free
Amount of items: 4
Items: 
Size: 17093 Color: 2
Size: 2356 Color: 4
Size: 128 Color: 3
Size: 64 Color: 3

Bin 124: 8 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 3
Size: 8168 Color: 3
Size: 1312 Color: 2

Bin 125: 8 of cap free
Amount of items: 3
Items: 
Size: 12080 Color: 0
Size: 7112 Color: 1
Size: 448 Color: 0

Bin 126: 8 of cap free
Amount of items: 3
Items: 
Size: 12092 Color: 1
Size: 7132 Color: 3
Size: 416 Color: 3

Bin 127: 8 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 2
Size: 4728 Color: 0
Size: 2616 Color: 3

Bin 128: 8 of cap free
Amount of items: 2
Items: 
Size: 14864 Color: 0
Size: 4776 Color: 1

Bin 129: 8 of cap free
Amount of items: 2
Items: 
Size: 15532 Color: 0
Size: 4108 Color: 4

Bin 130: 9 of cap free
Amount of items: 3
Items: 
Size: 9904 Color: 2
Size: 9271 Color: 0
Size: 464 Color: 1

Bin 131: 9 of cap free
Amount of items: 2
Items: 
Size: 15507 Color: 2
Size: 4132 Color: 4

Bin 132: 9 of cap free
Amount of items: 3
Items: 
Size: 15612 Color: 1
Size: 3867 Color: 0
Size: 160 Color: 2

Bin 133: 10 of cap free
Amount of items: 3
Items: 
Size: 11282 Color: 1
Size: 6720 Color: 2
Size: 1636 Color: 3

Bin 134: 10 of cap free
Amount of items: 5
Items: 
Size: 11760 Color: 2
Size: 4278 Color: 3
Size: 2912 Color: 1
Size: 352 Color: 1
Size: 336 Color: 0

Bin 135: 10 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 2
Size: 6974 Color: 1
Size: 496 Color: 0

Bin 136: 10 of cap free
Amount of items: 2
Items: 
Size: 15456 Color: 1
Size: 4182 Color: 0

Bin 137: 11 of cap free
Amount of items: 3
Items: 
Size: 13411 Color: 0
Size: 5810 Color: 2
Size: 416 Color: 0

Bin 138: 12 of cap free
Amount of items: 3
Items: 
Size: 12144 Color: 2
Size: 6844 Color: 4
Size: 648 Color: 4

Bin 139: 12 of cap free
Amount of items: 3
Items: 
Size: 16812 Color: 0
Size: 2664 Color: 2
Size: 160 Color: 1

Bin 140: 13 of cap free
Amount of items: 3
Items: 
Size: 10566 Color: 4
Size: 6184 Color: 2
Size: 2885 Color: 3

Bin 141: 14 of cap free
Amount of items: 3
Items: 
Size: 11010 Color: 4
Size: 8176 Color: 3
Size: 448 Color: 1

Bin 142: 14 of cap free
Amount of items: 2
Items: 
Size: 17115 Color: 2
Size: 2519 Color: 4

Bin 143: 14 of cap free
Amount of items: 3
Items: 
Size: 17168 Color: 4
Size: 2364 Color: 0
Size: 102 Color: 3

Bin 144: 15 of cap free
Amount of items: 5
Items: 
Size: 9834 Color: 4
Size: 4296 Color: 2
Size: 3855 Color: 3
Size: 1088 Color: 0
Size: 560 Color: 4

Bin 145: 16 of cap free
Amount of items: 3
Items: 
Size: 13788 Color: 2
Size: 5428 Color: 4
Size: 416 Color: 4

Bin 146: 16 of cap free
Amount of items: 2
Items: 
Size: 14496 Color: 1
Size: 5136 Color: 4

Bin 147: 17 of cap free
Amount of items: 3
Items: 
Size: 13395 Color: 3
Size: 5360 Color: 3
Size: 876 Color: 0

Bin 148: 17 of cap free
Amount of items: 2
Items: 
Size: 15023 Color: 1
Size: 4608 Color: 4

Bin 149: 18 of cap free
Amount of items: 2
Items: 
Size: 12428 Color: 4
Size: 7202 Color: 0

Bin 150: 18 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 0
Size: 6248 Color: 1

Bin 151: 18 of cap free
Amount of items: 3
Items: 
Size: 14518 Color: 1
Size: 4884 Color: 2
Size: 228 Color: 0

Bin 152: 19 of cap free
Amount of items: 2
Items: 
Size: 14660 Color: 4
Size: 4969 Color: 1

Bin 153: 20 of cap free
Amount of items: 3
Items: 
Size: 15064 Color: 2
Size: 4436 Color: 4
Size: 128 Color: 3

Bin 154: 21 of cap free
Amount of items: 3
Items: 
Size: 15923 Color: 1
Size: 3608 Color: 0
Size: 96 Color: 3

Bin 155: 22 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 2
Size: 4130 Color: 1
Size: 3468 Color: 4

Bin 156: 22 of cap free
Amount of items: 2
Items: 
Size: 16187 Color: 1
Size: 3439 Color: 0

Bin 157: 22 of cap free
Amount of items: 2
Items: 
Size: 16368 Color: 1
Size: 3258 Color: 4

Bin 158: 27 of cap free
Amount of items: 2
Items: 
Size: 16966 Color: 2
Size: 2655 Color: 4

Bin 159: 29 of cap free
Amount of items: 3
Items: 
Size: 9888 Color: 1
Size: 7963 Color: 0
Size: 1768 Color: 1

Bin 160: 30 of cap free
Amount of items: 2
Items: 
Size: 17442 Color: 0
Size: 2176 Color: 4

Bin 161: 31 of cap free
Amount of items: 2
Items: 
Size: 16998 Color: 0
Size: 2619 Color: 2

Bin 162: 32 of cap free
Amount of items: 2
Items: 
Size: 17040 Color: 0
Size: 2576 Color: 1

Bin 163: 33 of cap free
Amount of items: 2
Items: 
Size: 14416 Color: 2
Size: 5199 Color: 1

Bin 164: 34 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 4
Size: 8182 Color: 1
Size: 320 Color: 1

Bin 165: 35 of cap free
Amount of items: 3
Items: 
Size: 10093 Color: 0
Size: 7984 Color: 4
Size: 1536 Color: 1

Bin 166: 36 of cap free
Amount of items: 8
Items: 
Size: 9832 Color: 0
Size: 1656 Color: 3
Size: 1632 Color: 2
Size: 1632 Color: 0
Size: 1436 Color: 2
Size: 1408 Color: 1
Size: 1248 Color: 0
Size: 768 Color: 2

Bin 167: 36 of cap free
Amount of items: 2
Items: 
Size: 15742 Color: 2
Size: 3870 Color: 1

Bin 168: 36 of cap free
Amount of items: 2
Items: 
Size: 16830 Color: 0
Size: 2782 Color: 2

Bin 169: 40 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 0
Size: 6384 Color: 3

Bin 170: 40 of cap free
Amount of items: 2
Items: 
Size: 16560 Color: 2
Size: 3048 Color: 0

Bin 171: 43 of cap free
Amount of items: 2
Items: 
Size: 14459 Color: 2
Size: 5146 Color: 1

Bin 172: 45 of cap free
Amount of items: 6
Items: 
Size: 9826 Color: 3
Size: 2286 Color: 4
Size: 2285 Color: 0
Size: 2264 Color: 3
Size: 2238 Color: 3
Size: 704 Color: 4

Bin 173: 55 of cap free
Amount of items: 2
Items: 
Size: 12465 Color: 3
Size: 7128 Color: 1

Bin 174: 62 of cap free
Amount of items: 3
Items: 
Size: 10096 Color: 3
Size: 8626 Color: 2
Size: 864 Color: 0

Bin 175: 68 of cap free
Amount of items: 2
Items: 
Size: 13504 Color: 1
Size: 6076 Color: 0

Bin 176: 71 of cap free
Amount of items: 2
Items: 
Size: 12678 Color: 1
Size: 6899 Color: 4

Bin 177: 77 of cap free
Amount of items: 2
Items: 
Size: 14332 Color: 0
Size: 5239 Color: 1

Bin 178: 81 of cap free
Amount of items: 2
Items: 
Size: 15944 Color: 2
Size: 3623 Color: 0

Bin 179: 87 of cap free
Amount of items: 2
Items: 
Size: 14550 Color: 1
Size: 5011 Color: 0

Bin 180: 90 of cap free
Amount of items: 2
Items: 
Size: 12288 Color: 4
Size: 7270 Color: 1

Bin 181: 90 of cap free
Amount of items: 2
Items: 
Size: 15998 Color: 0
Size: 3560 Color: 1

Bin 182: 92 of cap free
Amount of items: 2
Items: 
Size: 11371 Color: 0
Size: 8185 Color: 1

Bin 183: 92 of cap free
Amount of items: 2
Items: 
Size: 14724 Color: 2
Size: 4832 Color: 0

Bin 184: 92 of cap free
Amount of items: 2
Items: 
Size: 16244 Color: 0
Size: 3312 Color: 2

Bin 185: 97 of cap free
Amount of items: 2
Items: 
Size: 14520 Color: 3
Size: 5031 Color: 1

Bin 186: 114 of cap free
Amount of items: 2
Items: 
Size: 13488 Color: 0
Size: 6046 Color: 4

Bin 187: 123 of cap free
Amount of items: 10
Items: 
Size: 9825 Color: 0
Size: 1396 Color: 2
Size: 1256 Color: 2
Size: 1232 Color: 0
Size: 1216 Color: 4
Size: 1080 Color: 1
Size: 1056 Color: 3
Size: 1056 Color: 3
Size: 1024 Color: 2
Size: 384 Color: 0

Bin 188: 135 of cap free
Amount of items: 3
Items: 
Size: 11696 Color: 3
Size: 5222 Color: 3
Size: 2595 Color: 0

Bin 189: 160 of cap free
Amount of items: 2
Items: 
Size: 13168 Color: 4
Size: 6320 Color: 1

Bin 190: 199 of cap free
Amount of items: 3
Items: 
Size: 9840 Color: 4
Size: 7977 Color: 1
Size: 1632 Color: 0

Bin 191: 202 of cap free
Amount of items: 2
Items: 
Size: 11266 Color: 1
Size: 8180 Color: 4

Bin 192: 208 of cap free
Amount of items: 2
Items: 
Size: 15008 Color: 0
Size: 4432 Color: 1

Bin 193: 220 of cap free
Amount of items: 3
Items: 
Size: 9836 Color: 4
Size: 8184 Color: 2
Size: 1408 Color: 0

Bin 194: 228 of cap free
Amount of items: 2
Items: 
Size: 12232 Color: 1
Size: 7188 Color: 3

Bin 195: 245 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 2
Size: 5475 Color: 4

Bin 196: 275 of cap free
Amount of items: 2
Items: 
Size: 10077 Color: 4
Size: 9296 Color: 3

Bin 197: 284 of cap free
Amount of items: 25
Items: 
Size: 1002 Color: 3
Size: 978 Color: 4
Size: 964 Color: 3
Size: 960 Color: 1
Size: 960 Color: 0
Size: 944 Color: 1
Size: 864 Color: 1
Size: 852 Color: 2
Size: 848 Color: 0
Size: 848 Color: 0
Size: 840 Color: 0
Size: 832 Color: 4
Size: 832 Color: 4
Size: 800 Color: 3
Size: 768 Color: 0
Size: 752 Color: 0
Size: 720 Color: 3
Size: 704 Color: 4
Size: 704 Color: 2
Size: 672 Color: 2
Size: 608 Color: 2
Size: 552 Color: 3
Size: 528 Color: 4
Size: 512 Color: 1
Size: 320 Color: 1

Bin 198: 312 of cap free
Amount of items: 2
Items: 
Size: 13112 Color: 4
Size: 6224 Color: 3

Bin 199: 14730 of cap free
Amount of items: 11
Items: 
Size: 552 Color: 2
Size: 552 Color: 2
Size: 528 Color: 2
Size: 512 Color: 4
Size: 512 Color: 4
Size: 438 Color: 3
Size: 416 Color: 3
Size: 384 Color: 4
Size: 384 Color: 3
Size: 320 Color: 1
Size: 320 Color: 1

Total size: 3890304
Total free space: 19648

