Capicity Bin: 7472
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 3740 Color: 0
Size: 674 Color: 1
Size: 560 Color: 1
Size: 546 Color: 1
Size: 544 Color: 0
Size: 542 Color: 0
Size: 532 Color: 0
Size: 190 Color: 0
Size: 144 Color: 1

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 3738 Color: 1
Size: 724 Color: 1
Size: 708 Color: 1
Size: 616 Color: 0
Size: 608 Color: 0
Size: 550 Color: 1
Size: 528 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4243 Color: 1
Size: 2691 Color: 1
Size: 538 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4219 Color: 0
Size: 2685 Color: 0
Size: 568 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 1
Size: 2652 Color: 0
Size: 528 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4420 Color: 0
Size: 2628 Color: 1
Size: 424 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4576 Color: 1
Size: 2740 Color: 1
Size: 156 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5074 Color: 0
Size: 2260 Color: 1
Size: 138 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5241 Color: 0
Size: 2061 Color: 1
Size: 170 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5265 Color: 1
Size: 2043 Color: 0
Size: 164 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5281 Color: 1
Size: 1783 Color: 0
Size: 408 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5993 Color: 0
Size: 1233 Color: 1
Size: 246 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 1
Size: 1172 Color: 1
Size: 168 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6150 Color: 1
Size: 850 Color: 0
Size: 472 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 1
Size: 964 Color: 0
Size: 240 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6273 Color: 1
Size: 951 Color: 0
Size: 248 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 0
Size: 971 Color: 0
Size: 218 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 0
Size: 790 Color: 1
Size: 366 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6390 Color: 0
Size: 622 Color: 1
Size: 460 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 0
Size: 765 Color: 1
Size: 220 Color: 0

Bin 21: 1 of cap free
Amount of items: 8
Items: 
Size: 3741 Color: 1
Size: 804 Color: 1
Size: 778 Color: 1
Size: 620 Color: 0
Size: 620 Color: 0
Size: 620 Color: 0
Size: 152 Color: 0
Size: 136 Color: 1

Bin 22: 1 of cap free
Amount of items: 5
Items: 
Size: 3754 Color: 0
Size: 2361 Color: 1
Size: 836 Color: 0
Size: 328 Color: 0
Size: 192 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 1
Size: 3111 Color: 0
Size: 172 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 6068 Color: 1
Size: 931 Color: 0
Size: 472 Color: 0

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 6331 Color: 0
Size: 1140 Color: 1

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 6347 Color: 1
Size: 1124 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 6363 Color: 0
Size: 708 Color: 0
Size: 400 Color: 1

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 6650 Color: 1
Size: 821 Color: 0

Bin 29: 2 of cap free
Amount of items: 30
Items: 
Size: 356 Color: 1
Size: 344 Color: 1
Size: 328 Color: 1
Size: 318 Color: 1
Size: 304 Color: 1
Size: 304 Color: 1
Size: 290 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 272 Color: 0
Size: 272 Color: 0
Size: 266 Color: 0
Size: 264 Color: 1
Size: 256 Color: 0
Size: 244 Color: 0
Size: 242 Color: 1
Size: 240 Color: 0
Size: 232 Color: 0
Size: 224 Color: 0
Size: 220 Color: 0
Size: 220 Color: 0
Size: 216 Color: 1
Size: 216 Color: 1
Size: 212 Color: 1
Size: 202 Color: 0
Size: 198 Color: 1
Size: 198 Color: 1
Size: 192 Color: 1
Size: 136 Color: 1
Size: 128 Color: 0

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 0
Size: 2666 Color: 1
Size: 272 Color: 1

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 4599 Color: 0
Size: 2719 Color: 1
Size: 152 Color: 0

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 5727 Color: 1
Size: 1599 Color: 1
Size: 144 Color: 0

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 6018 Color: 0
Size: 1324 Color: 1
Size: 128 Color: 1

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 6542 Color: 1
Size: 928 Color: 0

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 4171 Color: 1
Size: 2654 Color: 1
Size: 644 Color: 0

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 1
Size: 1841 Color: 0
Size: 160 Color: 1

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 5826 Color: 0
Size: 1643 Color: 1

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 6503 Color: 1
Size: 966 Color: 0

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 6593 Color: 0
Size: 876 Color: 1

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 6598 Color: 0
Size: 871 Color: 1

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 4211 Color: 1
Size: 3113 Color: 0
Size: 144 Color: 0

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 4908 Color: 1
Size: 2560 Color: 0

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 1
Size: 2002 Color: 0
Size: 246 Color: 1

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 6428 Color: 0
Size: 1040 Color: 1

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 6509 Color: 0
Size: 959 Color: 1

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 4639 Color: 1
Size: 2828 Color: 0

Bin 47: 5 of cap free
Amount of items: 3
Items: 
Size: 5703 Color: 1
Size: 1676 Color: 1
Size: 88 Color: 0

Bin 48: 5 of cap free
Amount of items: 4
Items: 
Size: 6274 Color: 1
Size: 1065 Color: 0
Size: 88 Color: 1
Size: 40 Color: 0

Bin 49: 5 of cap free
Amount of items: 4
Items: 
Size: 6290 Color: 1
Size: 1073 Color: 0
Size: 80 Color: 1
Size: 24 Color: 0

Bin 50: 5 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 0
Size: 855 Color: 1

Bin 51: 6 of cap free
Amount of items: 4
Items: 
Size: 4923 Color: 0
Size: 2375 Color: 1
Size: 120 Color: 1
Size: 48 Color: 0

Bin 52: 6 of cap free
Amount of items: 2
Items: 
Size: 6025 Color: 1
Size: 1441 Color: 0

Bin 53: 6 of cap free
Amount of items: 2
Items: 
Size: 6259 Color: 0
Size: 1207 Color: 1

Bin 54: 7 of cap free
Amount of items: 10
Items: 
Size: 3737 Color: 0
Size: 542 Color: 1
Size: 538 Color: 1
Size: 536 Color: 1
Size: 504 Color: 1
Size: 456 Color: 0
Size: 448 Color: 0
Size: 416 Color: 0
Size: 152 Color: 0
Size: 136 Color: 1

Bin 55: 7 of cap free
Amount of items: 2
Items: 
Size: 5442 Color: 1
Size: 2023 Color: 0

Bin 56: 7 of cap free
Amount of items: 3
Items: 
Size: 5612 Color: 0
Size: 1175 Color: 1
Size: 678 Color: 0

Bin 57: 7 of cap free
Amount of items: 2
Items: 
Size: 6454 Color: 0
Size: 1011 Color: 1

Bin 58: 7 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 0
Size: 939 Color: 1

Bin 59: 7 of cap free
Amount of items: 2
Items: 
Size: 6662 Color: 1
Size: 803 Color: 0

Bin 60: 8 of cap free
Amount of items: 3
Items: 
Size: 5630 Color: 0
Size: 1762 Color: 1
Size: 72 Color: 0

Bin 61: 8 of cap free
Amount of items: 2
Items: 
Size: 6009 Color: 1
Size: 1455 Color: 0

Bin 62: 8 of cap free
Amount of items: 2
Items: 
Size: 6463 Color: 1
Size: 1001 Color: 0

Bin 63: 9 of cap free
Amount of items: 3
Items: 
Size: 4620 Color: 1
Size: 2711 Color: 0
Size: 132 Color: 0

Bin 64: 9 of cap free
Amount of items: 2
Items: 
Size: 4764 Color: 1
Size: 2699 Color: 0

Bin 65: 9 of cap free
Amount of items: 2
Items: 
Size: 6708 Color: 0
Size: 755 Color: 1

Bin 66: 10 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 0
Size: 3084 Color: 1
Size: 294 Color: 0

Bin 67: 10 of cap free
Amount of items: 2
Items: 
Size: 6476 Color: 0
Size: 986 Color: 1

Bin 68: 11 of cap free
Amount of items: 2
Items: 
Size: 4710 Color: 1
Size: 2751 Color: 0

Bin 69: 12 of cap free
Amount of items: 2
Items: 
Size: 3780 Color: 0
Size: 3680 Color: 1

Bin 70: 12 of cap free
Amount of items: 2
Items: 
Size: 5985 Color: 0
Size: 1475 Color: 1

Bin 71: 13 of cap free
Amount of items: 2
Items: 
Size: 4195 Color: 1
Size: 3264 Color: 0

Bin 72: 13 of cap free
Amount of items: 2
Items: 
Size: 5575 Color: 1
Size: 1884 Color: 0

Bin 73: 14 of cap free
Amount of items: 3
Items: 
Size: 5172 Color: 1
Size: 2014 Color: 1
Size: 272 Color: 0

Bin 74: 14 of cap free
Amount of items: 2
Items: 
Size: 5333 Color: 0
Size: 2125 Color: 1

Bin 75: 14 of cap free
Amount of items: 2
Items: 
Size: 6355 Color: 0
Size: 1103 Color: 1

Bin 76: 15 of cap free
Amount of items: 2
Items: 
Size: 4726 Color: 1
Size: 2731 Color: 0

Bin 77: 15 of cap free
Amount of items: 2
Items: 
Size: 6555 Color: 1
Size: 902 Color: 0

Bin 78: 16 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 1
Size: 2302 Color: 0
Size: 64 Color: 0

Bin 79: 16 of cap free
Amount of items: 3
Items: 
Size: 5508 Color: 1
Size: 1844 Color: 0
Size: 104 Color: 0

Bin 80: 16 of cap free
Amount of items: 2
Items: 
Size: 6586 Color: 1
Size: 870 Color: 0

Bin 81: 17 of cap free
Amount of items: 2
Items: 
Size: 5378 Color: 1
Size: 2077 Color: 0

Bin 82: 17 of cap free
Amount of items: 2
Items: 
Size: 6323 Color: 0
Size: 1132 Color: 1

Bin 83: 18 of cap free
Amount of items: 2
Items: 
Size: 4290 Color: 0
Size: 3164 Color: 1

Bin 84: 18 of cap free
Amount of items: 3
Items: 
Size: 5716 Color: 1
Size: 1362 Color: 0
Size: 376 Color: 0

Bin 85: 19 of cap free
Amount of items: 2
Items: 
Size: 5838 Color: 1
Size: 1615 Color: 0

Bin 86: 19 of cap free
Amount of items: 2
Items: 
Size: 6592 Color: 0
Size: 861 Color: 1

Bin 87: 20 of cap free
Amount of items: 2
Items: 
Size: 5028 Color: 1
Size: 2424 Color: 0

Bin 88: 21 of cap free
Amount of items: 2
Items: 
Size: 6077 Color: 0
Size: 1374 Color: 1

Bin 89: 23 of cap free
Amount of items: 2
Items: 
Size: 5515 Color: 0
Size: 1934 Color: 1

Bin 90: 23 of cap free
Amount of items: 2
Items: 
Size: 6447 Color: 0
Size: 1002 Color: 1

Bin 91: 24 of cap free
Amount of items: 2
Items: 
Size: 5867 Color: 1
Size: 1581 Color: 0

Bin 92: 24 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 0
Size: 1468 Color: 1

Bin 93: 24 of cap free
Amount of items: 2
Items: 
Size: 6718 Color: 0
Size: 730 Color: 1

Bin 94: 27 of cap free
Amount of items: 2
Items: 
Size: 6219 Color: 1
Size: 1226 Color: 0

Bin 95: 30 of cap free
Amount of items: 2
Items: 
Size: 5892 Color: 0
Size: 1550 Color: 1

Bin 96: 31 of cap free
Amount of items: 3
Items: 
Size: 5614 Color: 1
Size: 1339 Color: 1
Size: 488 Color: 0

Bin 97: 31 of cap free
Amount of items: 2
Items: 
Size: 6116 Color: 1
Size: 1325 Color: 0

Bin 98: 31 of cap free
Amount of items: 2
Items: 
Size: 6516 Color: 0
Size: 925 Color: 1

Bin 99: 33 of cap free
Amount of items: 2
Items: 
Size: 6195 Color: 0
Size: 1244 Color: 1

Bin 100: 35 of cap free
Amount of items: 2
Items: 
Size: 5743 Color: 1
Size: 1694 Color: 0

Bin 101: 35 of cap free
Amount of items: 2
Items: 
Size: 6446 Color: 1
Size: 991 Color: 0

Bin 102: 35 of cap free
Amount of items: 2
Items: 
Size: 6628 Color: 0
Size: 809 Color: 1

Bin 103: 37 of cap free
Amount of items: 2
Items: 
Size: 5804 Color: 0
Size: 1631 Color: 1

Bin 104: 38 of cap free
Amount of items: 2
Items: 
Size: 6692 Color: 1
Size: 742 Color: 0

Bin 105: 39 of cap free
Amount of items: 2
Items: 
Size: 4981 Color: 0
Size: 2452 Color: 1

Bin 106: 41 of cap free
Amount of items: 2
Items: 
Size: 5555 Color: 0
Size: 1876 Color: 1

Bin 107: 47 of cap free
Amount of items: 2
Items: 
Size: 5501 Color: 0
Size: 1924 Color: 1

Bin 108: 47 of cap free
Amount of items: 2
Items: 
Size: 6439 Color: 0
Size: 986 Color: 1

Bin 109: 50 of cap free
Amount of items: 2
Items: 
Size: 6567 Color: 0
Size: 855 Color: 1

Bin 110: 50 of cap free
Amount of items: 2
Items: 
Size: 6686 Color: 1
Size: 736 Color: 0

Bin 111: 51 of cap free
Amount of items: 2
Items: 
Size: 5883 Color: 0
Size: 1538 Color: 1

Bin 112: 56 of cap free
Amount of items: 2
Items: 
Size: 5021 Color: 0
Size: 2395 Color: 1

Bin 113: 56 of cap free
Amount of items: 2
Items: 
Size: 6314 Color: 1
Size: 1102 Color: 0

Bin 114: 65 of cap free
Amount of items: 2
Items: 
Size: 6308 Color: 1
Size: 1099 Color: 0

Bin 115: 66 of cap free
Amount of items: 2
Items: 
Size: 5362 Color: 1
Size: 2044 Color: 0

Bin 116: 70 of cap free
Amount of items: 2
Items: 
Size: 6430 Color: 1
Size: 972 Color: 0

Bin 117: 72 of cap free
Amount of items: 2
Items: 
Size: 5260 Color: 1
Size: 2140 Color: 0

Bin 118: 73 of cap free
Amount of items: 2
Items: 
Size: 4623 Color: 0
Size: 2776 Color: 1

Bin 119: 74 of cap free
Amount of items: 2
Items: 
Size: 6002 Color: 1
Size: 1396 Color: 0

Bin 120: 75 of cap free
Amount of items: 2
Items: 
Size: 6307 Color: 1
Size: 1090 Color: 0

Bin 121: 76 of cap free
Amount of items: 2
Items: 
Size: 5535 Color: 1
Size: 1861 Color: 0

Bin 122: 78 of cap free
Amount of items: 19
Items: 
Size: 478 Color: 1
Size: 474 Color: 1
Size: 472 Color: 1
Size: 472 Color: 1
Size: 424 Color: 1
Size: 424 Color: 1
Size: 414 Color: 1
Size: 410 Color: 0
Size: 396 Color: 1
Size: 370 Color: 1
Size: 368 Color: 0
Size: 368 Color: 0
Size: 368 Color: 0
Size: 348 Color: 0
Size: 336 Color: 0
Size: 326 Color: 0
Size: 322 Color: 0
Size: 316 Color: 0
Size: 308 Color: 0

Bin 123: 82 of cap free
Amount of items: 2
Items: 
Size: 6149 Color: 0
Size: 1241 Color: 1

Bin 124: 84 of cap free
Amount of items: 2
Items: 
Size: 4274 Color: 1
Size: 3114 Color: 0

Bin 125: 85 of cap free
Amount of items: 2
Items: 
Size: 6166 Color: 1
Size: 1221 Color: 0

Bin 126: 91 of cap free
Amount of items: 2
Items: 
Size: 5001 Color: 1
Size: 2380 Color: 0

Bin 127: 92 of cap free
Amount of items: 4
Items: 
Size: 3746 Color: 1
Size: 2290 Color: 1
Size: 686 Color: 0
Size: 658 Color: 0

Bin 128: 96 of cap free
Amount of items: 2
Items: 
Size: 5820 Color: 1
Size: 1556 Color: 0

Bin 129: 103 of cap free
Amount of items: 2
Items: 
Size: 6155 Color: 1
Size: 1214 Color: 0

Bin 130: 115 of cap free
Amount of items: 2
Items: 
Size: 4251 Color: 0
Size: 3106 Color: 1

Bin 131: 116 of cap free
Amount of items: 2
Items: 
Size: 5976 Color: 1
Size: 1380 Color: 0

Bin 132: 135 of cap free
Amount of items: 2
Items: 
Size: 4235 Color: 0
Size: 3102 Color: 1

Bin 133: 4362 of cap free
Amount of items: 18
Items: 
Size: 196 Color: 0
Size: 196 Color: 0
Size: 194 Color: 0
Size: 192 Color: 1
Size: 190 Color: 1
Size: 186 Color: 1
Size: 186 Color: 0
Size: 184 Color: 1
Size: 180 Color: 1
Size: 180 Color: 0
Size: 172 Color: 1
Size: 168 Color: 1
Size: 168 Color: 0
Size: 160 Color: 1
Size: 160 Color: 0
Size: 160 Color: 0
Size: 150 Color: 1
Size: 88 Color: 0

Total size: 986304
Total free space: 7472

