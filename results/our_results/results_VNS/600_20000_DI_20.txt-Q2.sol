Capicity Bin: 19232
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 10392 Color: 1
Size: 7956 Color: 0
Size: 884 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10542 Color: 0
Size: 8014 Color: 0
Size: 676 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10809 Color: 1
Size: 7021 Color: 1
Size: 1402 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10972 Color: 1
Size: 6884 Color: 1
Size: 1376 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11440 Color: 1
Size: 7368 Color: 0
Size: 424 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11528 Color: 0
Size: 6424 Color: 1
Size: 1280 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11760 Color: 0
Size: 7128 Color: 1
Size: 344 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12120 Color: 0
Size: 6344 Color: 1
Size: 768 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 1
Size: 6124 Color: 1
Size: 524 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12868 Color: 0
Size: 5924 Color: 0
Size: 440 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 0
Size: 5744 Color: 0
Size: 488 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14274 Color: 1
Size: 3888 Color: 1
Size: 1070 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 0
Size: 3912 Color: 1
Size: 656 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14742 Color: 0
Size: 3146 Color: 1
Size: 1344 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14798 Color: 0
Size: 3906 Color: 1
Size: 528 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15192 Color: 1
Size: 2920 Color: 0
Size: 1120 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15272 Color: 0
Size: 2612 Color: 0
Size: 1348 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15300 Color: 1
Size: 3228 Color: 1
Size: 704 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15336 Color: 0
Size: 2090 Color: 1
Size: 1806 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15364 Color: 0
Size: 3604 Color: 1
Size: 264 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15456 Color: 1
Size: 2720 Color: 0
Size: 1056 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15676 Color: 1
Size: 2924 Color: 0
Size: 632 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 15665 Color: 0
Size: 2973 Color: 0
Size: 594 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 15700 Color: 0
Size: 1972 Color: 0
Size: 1560 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 15962 Color: 1
Size: 1990 Color: 0
Size: 1280 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 15824 Color: 0
Size: 2912 Color: 0
Size: 496 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16100 Color: 1
Size: 2504 Color: 0
Size: 628 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16294 Color: 0
Size: 1736 Color: 1
Size: 1202 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16388 Color: 0
Size: 1692 Color: 1
Size: 1152 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16432 Color: 0
Size: 1936 Color: 0
Size: 864 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16556 Color: 0
Size: 1972 Color: 0
Size: 704 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16702 Color: 1
Size: 2082 Color: 1
Size: 448 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16734 Color: 1
Size: 1602 Color: 0
Size: 896 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16744 Color: 0
Size: 2088 Color: 1
Size: 400 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 1
Size: 1368 Color: 1
Size: 1088 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16812 Color: 0
Size: 1848 Color: 0
Size: 572 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16780 Color: 1
Size: 1620 Color: 0
Size: 832 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16902 Color: 1
Size: 1402 Color: 0
Size: 928 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16988 Color: 1
Size: 1600 Color: 1
Size: 644 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 17000 Color: 0
Size: 2096 Color: 1
Size: 136 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 17066 Color: 0
Size: 1646 Color: 0
Size: 520 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17072 Color: 0
Size: 1584 Color: 1
Size: 576 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17160 Color: 0
Size: 1448 Color: 1
Size: 624 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17220 Color: 0
Size: 1248 Color: 1
Size: 764 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17188 Color: 1
Size: 1708 Color: 0
Size: 336 Color: 1

Bin 46: 1 of cap free
Amount of items: 5
Items: 
Size: 9644 Color: 1
Size: 6671 Color: 1
Size: 1608 Color: 0
Size: 720 Color: 0
Size: 588 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 13469 Color: 1
Size: 5122 Color: 0
Size: 640 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 14011 Color: 0
Size: 3628 Color: 1
Size: 1592 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 14284 Color: 0
Size: 4351 Color: 0
Size: 596 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 15437 Color: 1
Size: 3234 Color: 1
Size: 560 Color: 0

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 15657 Color: 1
Size: 2614 Color: 0
Size: 960 Color: 1

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 16068 Color: 1
Size: 3163 Color: 0

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 16304 Color: 1
Size: 2927 Color: 0

Bin 54: 2 of cap free
Amount of items: 9
Items: 
Size: 9620 Color: 1
Size: 1568 Color: 1
Size: 1404 Color: 0
Size: 1398 Color: 0
Size: 1224 Color: 1
Size: 1216 Color: 0
Size: 1184 Color: 1
Size: 1104 Color: 0
Size: 512 Color: 0

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 10462 Color: 1
Size: 8472 Color: 0
Size: 296 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 10807 Color: 1
Size: 7599 Color: 0
Size: 824 Color: 0

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 14638 Color: 1
Size: 4592 Color: 0

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 0
Size: 4134 Color: 0
Size: 368 Color: 1

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 15312 Color: 1
Size: 3598 Color: 0
Size: 320 Color: 1

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 15698 Color: 1
Size: 2964 Color: 0
Size: 568 Color: 1

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 15732 Color: 1
Size: 3282 Color: 0
Size: 216 Color: 1

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 17292 Color: 1
Size: 1938 Color: 0

Bin 63: 3 of cap free
Amount of items: 11
Items: 
Size: 9617 Color: 1
Size: 1184 Color: 1
Size: 1160 Color: 1
Size: 1088 Color: 1
Size: 1068 Color: 0
Size: 1064 Color: 0
Size: 1064 Color: 0
Size: 1024 Color: 1
Size: 960 Color: 0
Size: 544 Color: 1
Size: 456 Color: 0

Bin 64: 3 of cap free
Amount of items: 5
Items: 
Size: 9648 Color: 0
Size: 7021 Color: 1
Size: 1804 Color: 0
Size: 420 Color: 1
Size: 336 Color: 0

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 10817 Color: 1
Size: 8044 Color: 0
Size: 368 Color: 0

Bin 66: 3 of cap free
Amount of items: 5
Items: 
Size: 12592 Color: 1
Size: 3965 Color: 0
Size: 1808 Color: 0
Size: 544 Color: 1
Size: 320 Color: 0

Bin 67: 3 of cap free
Amount of items: 4
Items: 
Size: 12804 Color: 1
Size: 3971 Color: 0
Size: 1942 Color: 0
Size: 512 Color: 1

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 14412 Color: 0
Size: 4816 Color: 1

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 1
Size: 4076 Color: 0
Size: 576 Color: 0

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 16964 Color: 0
Size: 2264 Color: 1

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 17032 Color: 0
Size: 2196 Color: 1

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 17214 Color: 0
Size: 2014 Color: 1

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 10865 Color: 1
Size: 8362 Color: 0

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 1
Size: 6993 Color: 0
Size: 594 Color: 0

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 12015 Color: 1
Size: 6868 Color: 0
Size: 344 Color: 0

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 13463 Color: 1
Size: 5364 Color: 1
Size: 400 Color: 0

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 14019 Color: 1
Size: 5208 Color: 0

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 1
Size: 4536 Color: 0
Size: 480 Color: 0

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 15294 Color: 1
Size: 2600 Color: 0
Size: 1332 Color: 0

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 16120 Color: 0
Size: 2946 Color: 1
Size: 160 Color: 0

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 16482 Color: 1
Size: 2744 Color: 0

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 16502 Color: 0
Size: 2724 Color: 1

Bin 83: 6 of cap free
Amount of items: 3
Items: 
Size: 16664 Color: 0
Size: 2450 Color: 1
Size: 112 Color: 1

Bin 84: 6 of cap free
Amount of items: 2
Items: 
Size: 16818 Color: 1
Size: 2408 Color: 0

Bin 85: 6 of cap free
Amount of items: 4
Items: 
Size: 17232 Color: 0
Size: 1850 Color: 1
Size: 104 Color: 1
Size: 40 Color: 0

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 13458 Color: 0
Size: 5351 Color: 1
Size: 416 Color: 0

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 1
Size: 7640 Color: 0
Size: 1704 Color: 0

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 1
Size: 7512 Color: 0
Size: 1552 Color: 0

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 15482 Color: 1
Size: 3742 Color: 0

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 16098 Color: 1
Size: 3126 Color: 0

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 16270 Color: 0
Size: 2954 Color: 1

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 16276 Color: 1
Size: 2948 Color: 0

Bin 93: 10 of cap free
Amount of items: 3
Items: 
Size: 13934 Color: 0
Size: 4464 Color: 0
Size: 824 Color: 1

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 16166 Color: 0
Size: 3056 Color: 1

Bin 95: 10 of cap free
Amount of items: 2
Items: 
Size: 17258 Color: 0
Size: 1964 Color: 1

Bin 96: 11 of cap free
Amount of items: 2
Items: 
Size: 11229 Color: 1
Size: 7992 Color: 0

Bin 97: 11 of cap free
Amount of items: 4
Items: 
Size: 12805 Color: 1
Size: 3976 Color: 0
Size: 1988 Color: 0
Size: 452 Color: 1

Bin 98: 12 of cap free
Amount of items: 3
Items: 
Size: 13086 Color: 0
Size: 3252 Color: 1
Size: 2882 Color: 1

Bin 99: 12 of cap free
Amount of items: 2
Items: 
Size: 16752 Color: 1
Size: 2468 Color: 0

Bin 100: 12 of cap free
Amount of items: 2
Items: 
Size: 16868 Color: 0
Size: 2352 Color: 1

Bin 101: 13 of cap free
Amount of items: 3
Items: 
Size: 10072 Color: 1
Size: 8011 Color: 0
Size: 1136 Color: 1

Bin 102: 14 of cap free
Amount of items: 3
Items: 
Size: 12813 Color: 1
Size: 6005 Color: 0
Size: 400 Color: 1

Bin 103: 14 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 1
Size: 6126 Color: 0
Size: 256 Color: 1

Bin 104: 14 of cap free
Amount of items: 3
Items: 
Size: 17050 Color: 1
Size: 2008 Color: 0
Size: 160 Color: 1

Bin 105: 15 of cap free
Amount of items: 2
Items: 
Size: 14857 Color: 1
Size: 4360 Color: 0

Bin 106: 15 of cap free
Amount of items: 2
Items: 
Size: 15173 Color: 0
Size: 4044 Color: 1

Bin 107: 16 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 1
Size: 5332 Color: 0
Size: 288 Color: 0

Bin 108: 16 of cap free
Amount of items: 2
Items: 
Size: 15912 Color: 1
Size: 3304 Color: 0

Bin 109: 19 of cap free
Amount of items: 2
Items: 
Size: 16232 Color: 0
Size: 2981 Color: 1

Bin 110: 20 of cap free
Amount of items: 3
Items: 
Size: 10996 Color: 0
Size: 7800 Color: 1
Size: 416 Color: 0

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 13904 Color: 0
Size: 5308 Color: 1

Bin 112: 20 of cap free
Amount of items: 2
Items: 
Size: 16436 Color: 0
Size: 2776 Color: 1

Bin 113: 20 of cap free
Amount of items: 2
Items: 
Size: 17102 Color: 1
Size: 2110 Color: 0

Bin 114: 20 of cap free
Amount of items: 2
Items: 
Size: 17192 Color: 0
Size: 2020 Color: 1

Bin 115: 22 of cap free
Amount of items: 4
Items: 
Size: 9618 Color: 0
Size: 6736 Color: 1
Size: 1656 Color: 0
Size: 1200 Color: 1

Bin 116: 22 of cap free
Amount of items: 2
Items: 
Size: 12698 Color: 1
Size: 6512 Color: 0

Bin 117: 22 of cap free
Amount of items: 2
Items: 
Size: 15442 Color: 0
Size: 3768 Color: 1

Bin 118: 26 of cap free
Amount of items: 3
Items: 
Size: 13456 Color: 0
Size: 2888 Color: 1
Size: 2862 Color: 0

Bin 119: 26 of cap free
Amount of items: 2
Items: 
Size: 15600 Color: 1
Size: 3606 Color: 0

Bin 120: 26 of cap free
Amount of items: 3
Items: 
Size: 17234 Color: 0
Size: 1876 Color: 1
Size: 96 Color: 0

Bin 121: 27 of cap free
Amount of items: 2
Items: 
Size: 16036 Color: 0
Size: 3169 Color: 1

Bin 122: 28 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 1
Size: 8012 Color: 0
Size: 960 Color: 1

Bin 123: 28 of cap free
Amount of items: 2
Items: 
Size: 16144 Color: 1
Size: 3060 Color: 0

Bin 124: 29 of cap free
Amount of items: 2
Items: 
Size: 14849 Color: 1
Size: 4354 Color: 0

Bin 125: 30 of cap free
Amount of items: 34
Items: 
Size: 730 Color: 0
Size: 728 Color: 0
Size: 720 Color: 0
Size: 676 Color: 0
Size: 672 Color: 0
Size: 656 Color: 1
Size: 656 Color: 1
Size: 648 Color: 0
Size: 648 Color: 0
Size: 640 Color: 1
Size: 640 Color: 0
Size: 632 Color: 0
Size: 628 Color: 0
Size: 592 Color: 1
Size: 592 Color: 1
Size: 584 Color: 1
Size: 584 Color: 1
Size: 576 Color: 1
Size: 544 Color: 1
Size: 544 Color: 0
Size: 544 Color: 0
Size: 528 Color: 0
Size: 520 Color: 0
Size: 492 Color: 1
Size: 488 Color: 0
Size: 472 Color: 1
Size: 464 Color: 1
Size: 464 Color: 1
Size: 464 Color: 0
Size: 448 Color: 1
Size: 448 Color: 0
Size: 400 Color: 1
Size: 392 Color: 1
Size: 388 Color: 1

Bin 126: 31 of cap free
Amount of items: 8
Items: 
Size: 9621 Color: 1
Size: 1632 Color: 1
Size: 1600 Color: 1
Size: 1584 Color: 1
Size: 1488 Color: 0
Size: 1472 Color: 0
Size: 1408 Color: 0
Size: 396 Color: 0

Bin 127: 32 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 1
Size: 5192 Color: 0

Bin 128: 36 of cap free
Amount of items: 2
Items: 
Size: 9692 Color: 0
Size: 9504 Color: 1

Bin 129: 36 of cap free
Amount of items: 2
Items: 
Size: 13378 Color: 1
Size: 5818 Color: 0

Bin 130: 39 of cap free
Amount of items: 4
Items: 
Size: 9628 Color: 0
Size: 7013 Color: 1
Size: 1682 Color: 0
Size: 870 Color: 1

Bin 131: 39 of cap free
Amount of items: 2
Items: 
Size: 10841 Color: 1
Size: 8352 Color: 0

Bin 132: 39 of cap free
Amount of items: 2
Items: 
Size: 14475 Color: 1
Size: 4718 Color: 0

Bin 133: 40 of cap free
Amount of items: 2
Items: 
Size: 15362 Color: 1
Size: 3830 Color: 0

Bin 134: 40 of cap free
Amount of items: 2
Items: 
Size: 16328 Color: 1
Size: 2864 Color: 0

Bin 135: 40 of cap free
Amount of items: 2
Items: 
Size: 17256 Color: 0
Size: 1936 Color: 1

Bin 136: 41 of cap free
Amount of items: 2
Items: 
Size: 15802 Color: 0
Size: 3389 Color: 1

Bin 137: 41 of cap free
Amount of items: 3
Items: 
Size: 16046 Color: 1
Size: 2985 Color: 0
Size: 160 Color: 1

Bin 138: 42 of cap free
Amount of items: 2
Items: 
Size: 13744 Color: 1
Size: 5446 Color: 0

Bin 139: 42 of cap free
Amount of items: 2
Items: 
Size: 16846 Color: 1
Size: 2344 Color: 0

Bin 140: 42 of cap free
Amount of items: 2
Items: 
Size: 16912 Color: 1
Size: 2278 Color: 0

Bin 141: 45 of cap free
Amount of items: 2
Items: 
Size: 15167 Color: 1
Size: 4020 Color: 0

Bin 142: 50 of cap free
Amount of items: 2
Items: 
Size: 12254 Color: 1
Size: 6928 Color: 0

Bin 143: 51 of cap free
Amount of items: 2
Items: 
Size: 15651 Color: 0
Size: 3530 Color: 1

Bin 144: 52 of cap free
Amount of items: 2
Items: 
Size: 13628 Color: 0
Size: 5552 Color: 1

Bin 145: 52 of cap free
Amount of items: 2
Items: 
Size: 16696 Color: 0
Size: 2484 Color: 1

Bin 146: 52 of cap free
Amount of items: 2
Items: 
Size: 16944 Color: 0
Size: 2236 Color: 1

Bin 147: 52 of cap free
Amount of items: 2
Items: 
Size: 17124 Color: 1
Size: 2056 Color: 0

Bin 148: 54 of cap free
Amount of items: 2
Items: 
Size: 14472 Color: 1
Size: 4706 Color: 0

Bin 149: 54 of cap free
Amount of items: 2
Items: 
Size: 16520 Color: 1
Size: 2658 Color: 0

Bin 150: 54 of cap free
Amount of items: 2
Items: 
Size: 16884 Color: 0
Size: 2294 Color: 1

Bin 151: 55 of cap free
Amount of items: 2
Items: 
Size: 14832 Color: 0
Size: 4345 Color: 1

Bin 152: 60 of cap free
Amount of items: 2
Items: 
Size: 16840 Color: 1
Size: 2332 Color: 0

Bin 153: 62 of cap free
Amount of items: 2
Items: 
Size: 14998 Color: 1
Size: 4172 Color: 0

Bin 154: 62 of cap free
Amount of items: 2
Items: 
Size: 15354 Color: 1
Size: 3816 Color: 0

Bin 155: 63 of cap free
Amount of items: 2
Items: 
Size: 14469 Color: 1
Size: 4700 Color: 0

Bin 156: 64 of cap free
Amount of items: 2
Items: 
Size: 11152 Color: 0
Size: 8016 Color: 1

Bin 157: 64 of cap free
Amount of items: 2
Items: 
Size: 17304 Color: 1
Size: 1864 Color: 0

Bin 158: 65 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 0
Size: 6015 Color: 1
Size: 2448 Color: 1

Bin 159: 70 of cap free
Amount of items: 2
Items: 
Size: 14348 Color: 1
Size: 4814 Color: 0

Bin 160: 72 of cap free
Amount of items: 2
Items: 
Size: 12124 Color: 1
Size: 7036 Color: 0

Bin 161: 72 of cap free
Amount of items: 2
Items: 
Size: 16910 Color: 1
Size: 2250 Color: 0

Bin 162: 75 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 1
Size: 5357 Color: 0

Bin 163: 76 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 0
Size: 3648 Color: 1
Size: 1394 Color: 1

Bin 164: 78 of cap free
Amount of items: 2
Items: 
Size: 15458 Color: 1
Size: 3696 Color: 0

Bin 165: 78 of cap free
Amount of items: 2
Items: 
Size: 16684 Color: 0
Size: 2470 Color: 1

Bin 166: 79 of cap free
Amount of items: 4
Items: 
Size: 9624 Color: 0
Size: 6981 Color: 1
Size: 1680 Color: 0
Size: 868 Color: 1

Bin 167: 80 of cap free
Amount of items: 2
Items: 
Size: 15784 Color: 0
Size: 3368 Color: 1

Bin 168: 80 of cap free
Amount of items: 2
Items: 
Size: 17028 Color: 0
Size: 2124 Color: 1

Bin 169: 81 of cap free
Amount of items: 2
Items: 
Size: 11138 Color: 0
Size: 8013 Color: 1

Bin 170: 82 of cap free
Amount of items: 2
Items: 
Size: 14884 Color: 1
Size: 4266 Color: 0

Bin 171: 82 of cap free
Amount of items: 2
Items: 
Size: 16424 Color: 1
Size: 2726 Color: 0

Bin 172: 84 of cap free
Amount of items: 2
Items: 
Size: 14568 Color: 1
Size: 4580 Color: 0

Bin 173: 85 of cap free
Amount of items: 2
Items: 
Size: 12027 Color: 1
Size: 7120 Color: 0

Bin 174: 92 of cap free
Amount of items: 2
Items: 
Size: 14170 Color: 0
Size: 4970 Color: 1

Bin 175: 98 of cap free
Amount of items: 2
Items: 
Size: 17014 Color: 0
Size: 2120 Color: 1

Bin 176: 108 of cap free
Amount of items: 2
Items: 
Size: 11882 Color: 0
Size: 7242 Color: 1

Bin 177: 108 of cap free
Amount of items: 2
Items: 
Size: 11892 Color: 1
Size: 7232 Color: 0

Bin 178: 110 of cap free
Amount of items: 2
Items: 
Size: 11554 Color: 0
Size: 7568 Color: 1

Bin 179: 110 of cap free
Amount of items: 2
Items: 
Size: 17076 Color: 1
Size: 2046 Color: 0

Bin 180: 113 of cap free
Amount of items: 2
Items: 
Size: 15736 Color: 1
Size: 3383 Color: 0

Bin 181: 116 of cap free
Amount of items: 23
Items: 
Size: 1024 Color: 1
Size: 1024 Color: 1
Size: 960 Color: 0
Size: 936 Color: 0
Size: 896 Color: 0
Size: 868 Color: 0
Size: 864 Color: 1
Size: 852 Color: 1
Size: 836 Color: 0
Size: 832 Color: 0
Size: 808 Color: 0
Size: 808 Color: 0
Size: 800 Color: 1
Size: 792 Color: 0
Size: 792 Color: 0
Size: 784 Color: 1
Size: 768 Color: 1
Size: 768 Color: 1
Size: 752 Color: 1
Size: 752 Color: 0
Size: 748 Color: 1
Size: 736 Color: 0
Size: 716 Color: 1

Bin 182: 118 of cap free
Amount of items: 2
Items: 
Size: 12368 Color: 1
Size: 6746 Color: 0

Bin 183: 122 of cap free
Amount of items: 2
Items: 
Size: 16534 Color: 0
Size: 2576 Color: 1

Bin 184: 128 of cap free
Amount of items: 2
Items: 
Size: 14918 Color: 0
Size: 4186 Color: 1

Bin 185: 129 of cap free
Amount of items: 2
Items: 
Size: 10857 Color: 0
Size: 8246 Color: 1

Bin 186: 148 of cap free
Amount of items: 2
Items: 
Size: 15431 Color: 0
Size: 3653 Color: 1

Bin 187: 158 of cap free
Amount of items: 6
Items: 
Size: 9622 Color: 1
Size: 3162 Color: 1
Size: 1666 Color: 1
Size: 1600 Color: 0
Size: 1520 Color: 0
Size: 1504 Color: 0

Bin 188: 168 of cap free
Amount of items: 2
Items: 
Size: 14908 Color: 0
Size: 4156 Color: 1

Bin 189: 170 of cap free
Amount of items: 2
Items: 
Size: 15778 Color: 0
Size: 3284 Color: 1

Bin 190: 171 of cap free
Amount of items: 2
Items: 
Size: 14252 Color: 1
Size: 4809 Color: 0

Bin 191: 176 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 0
Size: 4676 Color: 1

Bin 192: 186 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 0
Size: 7022 Color: 1
Size: 2368 Color: 1

Bin 193: 192 of cap free
Amount of items: 2
Items: 
Size: 13496 Color: 0
Size: 5544 Color: 1

Bin 194: 201 of cap free
Amount of items: 2
Items: 
Size: 14228 Color: 1
Size: 4803 Color: 0

Bin 195: 202 of cap free
Amount of items: 3
Items: 
Size: 14010 Color: 0
Size: 3256 Color: 1
Size: 1764 Color: 1

Bin 196: 224 of cap free
Amount of items: 2
Items: 
Size: 11012 Color: 0
Size: 7996 Color: 1

Bin 197: 231 of cap free
Amount of items: 2
Items: 
Size: 15721 Color: 1
Size: 3280 Color: 0

Bin 198: 253 of cap free
Amount of items: 2
Items: 
Size: 15332 Color: 1
Size: 3647 Color: 0

Bin 199: 11836 of cap free
Amount of items: 21
Items: 
Size: 384 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 368 Color: 1
Size: 368 Color: 0
Size: 360 Color: 1
Size: 360 Color: 0
Size: 352 Color: 1
Size: 352 Color: 1
Size: 352 Color: 1
Size: 352 Color: 0
Size: 336 Color: 1
Size: 336 Color: 0
Size: 332 Color: 0
Size: 328 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0

Total size: 3807936
Total free space: 19232

