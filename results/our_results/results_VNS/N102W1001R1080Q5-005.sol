Capicity Bin: 1001
Lower Bound: 43

Bins used: 43
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 673 Color: 4
Size: 180 Color: 3
Size: 148 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 740 Color: 3
Size: 158 Color: 0
Size: 103 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 421 Color: 2
Size: 129 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 285 Color: 3
Size: 283 Color: 4

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 3
Size: 376 Color: 1

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 288 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 407 Color: 2
Size: 175 Color: 3

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 2
Size: 239 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 4
Size: 187 Color: 4
Size: 142 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 638 Color: 0
Size: 200 Color: 2
Size: 163 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 398 Color: 3
Size: 195 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 662 Color: 0
Size: 208 Color: 1
Size: 131 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 4
Size: 316 Color: 0
Size: 263 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 4
Size: 308 Color: 4
Size: 282 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 770 Color: 4
Size: 120 Color: 3
Size: 111 Color: 4

Bin 16: 1 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 4
Size: 392 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 4
Size: 390 Color: 4
Size: 175 Color: 1

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 3
Size: 292 Color: 2
Size: 241 Color: 3

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 4
Size: 336 Color: 0

Bin 20: 2 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 281 Color: 3

Bin 21: 2 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 0
Size: 395 Color: 4

Bin 22: 2 of cap free
Amount of items: 3
Items: 
Size: 736 Color: 0
Size: 153 Color: 4
Size: 110 Color: 4

Bin 23: 2 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 294 Color: 1

Bin 24: 3 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 1
Size: 483 Color: 4

Bin 25: 6 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 3
Size: 344 Color: 4

Bin 26: 6 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 2
Size: 273 Color: 3

Bin 27: 6 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 1
Size: 217 Color: 3

Bin 28: 6 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 2
Size: 306 Color: 0

Bin 29: 7 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 1
Size: 364 Color: 0

Bin 30: 7 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 2
Size: 247 Color: 1

Bin 31: 10 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 2
Size: 478 Color: 4

Bin 32: 12 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 3
Size: 245 Color: 1

Bin 33: 12 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 4
Size: 244 Color: 3

Bin 34: 13 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 118 Color: 4
Size: 401 Color: 2

Bin 35: 17 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 0
Size: 429 Color: 2

Bin 36: 22 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 2
Size: 218 Color: 0

Bin 37: 25 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 3
Size: 236 Color: 2

Bin 38: 31 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 2
Size: 428 Color: 3

Bin 39: 34 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 1
Size: 195 Color: 4

Bin 40: 37 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 428 Color: 2

Bin 41: 52 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 4
Size: 289 Color: 0

Bin 42: 64 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 2
Size: 399 Color: 3

Bin 43: 81 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 383 Color: 0

Total size: 42580
Total free space: 463

