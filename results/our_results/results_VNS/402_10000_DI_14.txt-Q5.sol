Capicity Bin: 8344
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 0
Size: 3462 Color: 0
Size: 694 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4261 Color: 2
Size: 2962 Color: 4
Size: 1121 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4668 Color: 2
Size: 3468 Color: 3
Size: 208 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4850 Color: 0
Size: 2906 Color: 3
Size: 588 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5124 Color: 2
Size: 3004 Color: 4
Size: 216 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5068 Color: 1
Size: 3068 Color: 1
Size: 208 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6124 Color: 4
Size: 1612 Color: 4
Size: 608 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6154 Color: 0
Size: 1946 Color: 1
Size: 244 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6404 Color: 3
Size: 1584 Color: 3
Size: 356 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6458 Color: 2
Size: 1574 Color: 3
Size: 312 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6441 Color: 0
Size: 1587 Color: 1
Size: 316 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6585 Color: 4
Size: 1371 Color: 3
Size: 388 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 1
Size: 1402 Color: 4
Size: 280 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6707 Color: 3
Size: 1241 Color: 0
Size: 396 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 1
Size: 1420 Color: 3
Size: 152 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 3
Size: 1301 Color: 4
Size: 260 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6826 Color: 1
Size: 1262 Color: 0
Size: 256 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 2
Size: 1032 Color: 4
Size: 458 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 0
Size: 764 Color: 2
Size: 688 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6921 Color: 3
Size: 1187 Color: 2
Size: 236 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 0
Size: 1087 Color: 3
Size: 272 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6996 Color: 1
Size: 1076 Color: 2
Size: 272 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7025 Color: 2
Size: 867 Color: 0
Size: 452 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 0
Size: 1128 Color: 3
Size: 172 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7085 Color: 0
Size: 1029 Color: 4
Size: 230 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7111 Color: 1
Size: 1001 Color: 2
Size: 232 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 2
Size: 982 Color: 3
Size: 212 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 3
Size: 592 Color: 4
Size: 580 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 2
Size: 986 Color: 3
Size: 184 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 1
Size: 802 Color: 3
Size: 292 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7263 Color: 1
Size: 901 Color: 3
Size: 180 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7285 Color: 2
Size: 883 Color: 0
Size: 176 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7314 Color: 4
Size: 762 Color: 3
Size: 268 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7382 Color: 3
Size: 688 Color: 2
Size: 274 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7375 Color: 0
Size: 809 Color: 3
Size: 160 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7434 Color: 1
Size: 664 Color: 2
Size: 246 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7466 Color: 4
Size: 520 Color: 2
Size: 358 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7494 Color: 0
Size: 466 Color: 2
Size: 384 Color: 3

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 0
Size: 3673 Color: 3
Size: 330 Color: 1

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 4785 Color: 2
Size: 2964 Color: 0
Size: 594 Color: 3

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 4890 Color: 1
Size: 3453 Color: 4

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5569 Color: 1
Size: 2582 Color: 0
Size: 192 Color: 3

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5630 Color: 3
Size: 2313 Color: 0
Size: 400 Color: 4

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5905 Color: 4
Size: 1724 Color: 4
Size: 714 Color: 1

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 3
Size: 1987 Color: 3
Size: 360 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 0
Size: 2001 Color: 3
Size: 252 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6178 Color: 4
Size: 2021 Color: 1
Size: 144 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6401 Color: 1
Size: 1806 Color: 0
Size: 136 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6699 Color: 2
Size: 1182 Color: 3
Size: 462 Color: 4

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 2
Size: 1208 Color: 2
Size: 318 Color: 3

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6857 Color: 3
Size: 1062 Color: 2
Size: 424 Color: 1

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 0
Size: 1133 Color: 2
Size: 260 Color: 3

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 7074 Color: 4
Size: 1101 Color: 3
Size: 168 Color: 1

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 7235 Color: 1
Size: 592 Color: 3
Size: 516 Color: 2

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 7332 Color: 2
Size: 947 Color: 4
Size: 64 Color: 1

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 7423 Color: 1
Size: 680 Color: 4
Size: 240 Color: 3

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 4858 Color: 2
Size: 3340 Color: 3
Size: 144 Color: 0

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5056 Color: 0
Size: 3006 Color: 3
Size: 280 Color: 2

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5881 Color: 4
Size: 2301 Color: 0
Size: 160 Color: 4

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 4
Size: 2262 Color: 0
Size: 148 Color: 3

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5994 Color: 0
Size: 1826 Color: 2
Size: 522 Color: 3

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 4
Size: 2053 Color: 1
Size: 128 Color: 3

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6425 Color: 0
Size: 1821 Color: 1
Size: 96 Color: 3

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 4
Size: 1601 Color: 1
Size: 180 Color: 1

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 6644 Color: 1
Size: 1594 Color: 4
Size: 104 Color: 3

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 7428 Color: 0
Size: 914 Color: 2

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 5226 Color: 0
Size: 2033 Color: 0
Size: 1082 Color: 2

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 0
Size: 2603 Color: 4
Size: 84 Color: 4

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5961 Color: 3
Size: 1882 Color: 0
Size: 498 Color: 1

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 6185 Color: 2
Size: 2156 Color: 3

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 6201 Color: 3
Size: 2012 Color: 3
Size: 128 Color: 0

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 7305 Color: 1
Size: 1036 Color: 4

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 5205 Color: 4
Size: 2967 Color: 2
Size: 168 Color: 0

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 5609 Color: 2
Size: 2333 Color: 4
Size: 398 Color: 0

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 4
Size: 1668 Color: 4
Size: 388 Color: 0

Bin 76: 4 of cap free
Amount of items: 3
Items: 
Size: 6643 Color: 3
Size: 1545 Color: 2
Size: 152 Color: 4

Bin 77: 4 of cap free
Amount of items: 3
Items: 
Size: 6830 Color: 4
Size: 1390 Color: 1
Size: 120 Color: 0

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 6873 Color: 0
Size: 1467 Color: 4

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 5945 Color: 4
Size: 2242 Color: 3
Size: 152 Color: 2

Bin 80: 5 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 4
Size: 1051 Color: 1
Size: 862 Color: 0

Bin 81: 5 of cap free
Amount of items: 2
Items: 
Size: 7177 Color: 2
Size: 1162 Color: 4

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 7468 Color: 0
Size: 844 Color: 4
Size: 26 Color: 3

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 4201 Color: 0
Size: 3664 Color: 3
Size: 472 Color: 1

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 5921 Color: 1
Size: 1962 Color: 1
Size: 454 Color: 0

Bin 85: 8 of cap free
Amount of items: 14
Items: 
Size: 1316 Color: 1
Size: 804 Color: 2
Size: 732 Color: 2
Size: 692 Color: 3
Size: 690 Color: 2
Size: 664 Color: 0
Size: 598 Color: 4
Size: 544 Color: 2
Size: 526 Color: 0
Size: 512 Color: 4
Size: 364 Color: 3
Size: 336 Color: 3
Size: 320 Color: 3
Size: 238 Color: 4

Bin 86: 8 of cap free
Amount of items: 4
Items: 
Size: 4180 Color: 1
Size: 2114 Color: 0
Size: 1212 Color: 2
Size: 830 Color: 3

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 1
Size: 1787 Color: 4
Size: 164 Color: 2

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 6716 Color: 2
Size: 1620 Color: 3

Bin 89: 8 of cap free
Amount of items: 3
Items: 
Size: 7143 Color: 2
Size: 1141 Color: 1
Size: 52 Color: 1

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 7108 Color: 1
Size: 1227 Color: 0

Bin 91: 10 of cap free
Amount of items: 3
Items: 
Size: 4769 Color: 3
Size: 2981 Color: 4
Size: 584 Color: 2

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 5970 Color: 3
Size: 2364 Color: 2

Bin 93: 12 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 3
Size: 2744 Color: 0
Size: 240 Color: 3

Bin 94: 12 of cap free
Amount of items: 2
Items: 
Size: 6926 Color: 1
Size: 1406 Color: 4

Bin 95: 12 of cap free
Amount of items: 2
Items: 
Size: 7274 Color: 2
Size: 1058 Color: 0

Bin 96: 13 of cap free
Amount of items: 3
Items: 
Size: 5545 Color: 4
Size: 2602 Color: 3
Size: 184 Color: 0

Bin 97: 13 of cap free
Amount of items: 3
Items: 
Size: 7350 Color: 3
Size: 925 Color: 4
Size: 56 Color: 2

Bin 98: 18 of cap free
Amount of items: 2
Items: 
Size: 6412 Color: 2
Size: 1914 Color: 1

Bin 99: 18 of cap free
Amount of items: 2
Items: 
Size: 6961 Color: 0
Size: 1365 Color: 2

Bin 100: 19 of cap free
Amount of items: 2
Items: 
Size: 6361 Color: 4
Size: 1964 Color: 1

Bin 101: 19 of cap free
Amount of items: 2
Items: 
Size: 7041 Color: 4
Size: 1284 Color: 0

Bin 102: 20 of cap free
Amount of items: 3
Items: 
Size: 4174 Color: 0
Size: 3458 Color: 0
Size: 692 Color: 4

Bin 103: 21 of cap free
Amount of items: 2
Items: 
Size: 7170 Color: 4
Size: 1153 Color: 1

Bin 104: 21 of cap free
Amount of items: 2
Items: 
Size: 7345 Color: 0
Size: 978 Color: 1

Bin 105: 22 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 0
Size: 1272 Color: 2

Bin 106: 23 of cap free
Amount of items: 3
Items: 
Size: 5181 Color: 4
Size: 2914 Color: 3
Size: 226 Color: 2

Bin 107: 24 of cap free
Amount of items: 2
Items: 
Size: 7236 Color: 4
Size: 1084 Color: 0

Bin 108: 25 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 1
Size: 1621 Color: 2
Size: 40 Color: 2

Bin 109: 26 of cap free
Amount of items: 2
Items: 
Size: 4182 Color: 1
Size: 4136 Color: 2

Bin 110: 27 of cap free
Amount of items: 5
Items: 
Size: 4177 Color: 1
Size: 1364 Color: 1
Size: 1273 Color: 0
Size: 769 Color: 1
Size: 734 Color: 2

Bin 111: 27 of cap free
Amount of items: 2
Items: 
Size: 5221 Color: 0
Size: 3096 Color: 4

Bin 112: 27 of cap free
Amount of items: 3
Items: 
Size: 5508 Color: 1
Size: 2617 Color: 4
Size: 192 Color: 2

Bin 113: 27 of cap free
Amount of items: 2
Items: 
Size: 6050 Color: 3
Size: 2267 Color: 2

Bin 114: 27 of cap free
Amount of items: 2
Items: 
Size: 6664 Color: 3
Size: 1653 Color: 2

Bin 115: 28 of cap free
Amount of items: 4
Items: 
Size: 4173 Color: 3
Size: 1801 Color: 1
Size: 1650 Color: 4
Size: 692 Color: 2

Bin 116: 29 of cap free
Amount of items: 2
Items: 
Size: 6896 Color: 2
Size: 1419 Color: 1

Bin 117: 31 of cap free
Amount of items: 3
Items: 
Size: 4774 Color: 3
Size: 3403 Color: 4
Size: 136 Color: 0

Bin 118: 32 of cap free
Amount of items: 2
Items: 
Size: 7388 Color: 4
Size: 924 Color: 1

Bin 119: 37 of cap free
Amount of items: 2
Items: 
Size: 7001 Color: 1
Size: 1306 Color: 4

Bin 120: 42 of cap free
Amount of items: 2
Items: 
Size: 6450 Color: 4
Size: 1852 Color: 2

Bin 121: 50 of cap free
Amount of items: 23
Items: 
Size: 512 Color: 4
Size: 496 Color: 4
Size: 472 Color: 2
Size: 452 Color: 1
Size: 448 Color: 2
Size: 448 Color: 0
Size: 410 Color: 1
Size: 406 Color: 2
Size: 402 Color: 4
Size: 400 Color: 4
Size: 380 Color: 0
Size: 372 Color: 2
Size: 368 Color: 2
Size: 362 Color: 0
Size: 326 Color: 0
Size: 322 Color: 1
Size: 300 Color: 2
Size: 280 Color: 3
Size: 264 Color: 1
Size: 236 Color: 1
Size: 222 Color: 3
Size: 216 Color: 3
Size: 200 Color: 3

Bin 122: 53 of cap free
Amount of items: 2
Items: 
Size: 6010 Color: 4
Size: 2281 Color: 3

Bin 123: 62 of cap free
Amount of items: 3
Items: 
Size: 4745 Color: 3
Size: 3001 Color: 4
Size: 536 Color: 0

Bin 124: 75 of cap free
Amount of items: 2
Items: 
Size: 4796 Color: 1
Size: 3473 Color: 2

Bin 125: 75 of cap free
Amount of items: 2
Items: 
Size: 5585 Color: 4
Size: 2684 Color: 1

Bin 126: 75 of cap free
Amount of items: 2
Items: 
Size: 6636 Color: 2
Size: 1633 Color: 1

Bin 127: 80 of cap free
Amount of items: 2
Items: 
Size: 4794 Color: 2
Size: 3470 Color: 4

Bin 128: 80 of cap free
Amount of items: 2
Items: 
Size: 5764 Color: 3
Size: 2500 Color: 4

Bin 129: 82 of cap free
Amount of items: 2
Items: 
Size: 5625 Color: 1
Size: 2637 Color: 3

Bin 130: 96 of cap free
Amount of items: 3
Items: 
Size: 4190 Color: 2
Size: 3478 Color: 0
Size: 580 Color: 1

Bin 131: 116 of cap free
Amount of items: 2
Items: 
Size: 5250 Color: 3
Size: 2978 Color: 2

Bin 132: 126 of cap free
Amount of items: 2
Items: 
Size: 4198 Color: 2
Size: 4020 Color: 4

Bin 133: 6538 of cap free
Amount of items: 8
Items: 
Size: 320 Color: 0
Size: 282 Color: 0
Size: 254 Color: 4
Size: 218 Color: 1
Size: 204 Color: 1
Size: 200 Color: 3
Size: 184 Color: 3
Size: 144 Color: 3

Total size: 1101408
Total free space: 8344

