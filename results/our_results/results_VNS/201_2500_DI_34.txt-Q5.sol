Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 0
Size: 1022 Color: 1
Size: 204 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 4
Size: 1029 Color: 1
Size: 204 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1572 Color: 0
Size: 756 Color: 0
Size: 144 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1676 Color: 0
Size: 700 Color: 2
Size: 96 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 0
Size: 447 Color: 2
Size: 204 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 4
Size: 556 Color: 0
Size: 40 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1911 Color: 1
Size: 409 Color: 4
Size: 152 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 1
Size: 418 Color: 0
Size: 122 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 0
Size: 462 Color: 2
Size: 92 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 1
Size: 452 Color: 4
Size: 40 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1986 Color: 1
Size: 378 Color: 0
Size: 108 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 4
Size: 382 Color: 2
Size: 72 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2098 Color: 0
Size: 230 Color: 1
Size: 144 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 2
Size: 372 Color: 3
Size: 72 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 2
Size: 282 Color: 1
Size: 132 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 2
Size: 236 Color: 3
Size: 96 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 1
Size: 294 Color: 2
Size: 32 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 1
Size: 162 Color: 4
Size: 124 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 3
Size: 234 Color: 0
Size: 44 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 3
Size: 220 Color: 4
Size: 54 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 3
Size: 172 Color: 2
Size: 96 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 2
Size: 200 Color: 2
Size: 60 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 1
Size: 1018 Color: 4
Size: 44 Color: 3

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 1
Size: 543 Color: 0
Size: 370 Color: 2

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1623 Color: 4
Size: 576 Color: 0
Size: 272 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 1
Size: 443 Color: 2
Size: 314 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 3
Size: 529 Color: 0
Size: 136 Color: 2

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 1
Size: 494 Color: 0
Size: 80 Color: 3

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 1
Size: 466 Color: 3
Size: 64 Color: 4

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1971 Color: 1
Size: 272 Color: 0
Size: 228 Color: 2

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 2
Size: 634 Color: 4
Size: 104 Color: 0

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 2
Size: 412 Color: 4
Size: 36 Color: 4

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 1
Size: 340 Color: 4
Size: 8 Color: 0

Bin 34: 3 of cap free
Amount of items: 6
Items: 
Size: 1244 Color: 0
Size: 817 Color: 2
Size: 124 Color: 1
Size: 120 Color: 3
Size: 120 Color: 2
Size: 44 Color: 0

Bin 35: 3 of cap free
Amount of items: 5
Items: 
Size: 1237 Color: 2
Size: 840 Color: 4
Size: 192 Color: 4
Size: 120 Color: 3
Size: 80 Color: 0

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1493 Color: 2
Size: 802 Color: 4
Size: 174 Color: 1

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 3
Size: 606 Color: 4
Size: 112 Color: 2

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 1
Size: 698 Color: 3
Size: 64 Color: 2

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 1
Size: 384 Color: 2
Size: 16 Color: 0

Bin 40: 4 of cap free
Amount of items: 4
Items: 
Size: 2210 Color: 1
Size: 242 Color: 4
Size: 8 Color: 3
Size: 8 Color: 2

Bin 41: 6 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 0
Size: 1000 Color: 1
Size: 48 Color: 3

Bin 42: 6 of cap free
Amount of items: 2
Items: 
Size: 1846 Color: 4
Size: 620 Color: 1

Bin 43: 6 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 1
Size: 242 Color: 3
Size: 28 Color: 4

Bin 44: 7 of cap free
Amount of items: 2
Items: 
Size: 1731 Color: 2
Size: 734 Color: 4

Bin 45: 9 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 0
Size: 667 Color: 3
Size: 92 Color: 4

Bin 46: 11 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 2
Size: 767 Color: 4
Size: 56 Color: 3

Bin 47: 11 of cap free
Amount of items: 2
Items: 
Size: 1983 Color: 2
Size: 478 Color: 1

Bin 48: 14 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 1
Size: 860 Color: 3
Size: 140 Color: 0

Bin 49: 14 of cap free
Amount of items: 2
Items: 
Size: 1839 Color: 3
Size: 619 Color: 1

Bin 50: 15 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 2
Size: 500 Color: 4
Size: 284 Color: 0

Bin 51: 16 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 0
Size: 308 Color: 1
Size: 40 Color: 4

Bin 52: 17 of cap free
Amount of items: 2
Items: 
Size: 1746 Color: 1
Size: 709 Color: 4

Bin 53: 18 of cap free
Amount of items: 2
Items: 
Size: 1812 Color: 0
Size: 642 Color: 1

Bin 54: 19 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 3
Size: 1031 Color: 1
Size: 168 Color: 0

Bin 55: 21 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 3
Size: 1030 Color: 2

Bin 56: 24 of cap free
Amount of items: 2
Items: 
Size: 1926 Color: 1
Size: 522 Color: 4

Bin 57: 29 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 1
Size: 469 Color: 3

Bin 58: 32 of cap free
Amount of items: 11
Items: 
Size: 601 Color: 1
Size: 504 Color: 0
Size: 499 Color: 4
Size: 168 Color: 2
Size: 108 Color: 3
Size: 104 Color: 3
Size: 104 Color: 3
Size: 96 Color: 3
Size: 88 Color: 4
Size: 88 Color: 4
Size: 80 Color: 2

Bin 59: 32 of cap free
Amount of items: 2
Items: 
Size: 1594 Color: 3
Size: 846 Color: 4

Bin 60: 32 of cap free
Amount of items: 2
Items: 
Size: 1882 Color: 3
Size: 558 Color: 4

Bin 61: 35 of cap free
Amount of items: 2
Items: 
Size: 1444 Color: 4
Size: 993 Color: 2

Bin 62: 37 of cap free
Amount of items: 2
Items: 
Size: 1553 Color: 3
Size: 882 Color: 4

Bin 63: 40 of cap free
Amount of items: 2
Items: 
Size: 1292 Color: 2
Size: 1140 Color: 4

Bin 64: 42 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 3
Size: 988 Color: 1
Size: 204 Color: 0

Bin 65: 43 of cap free
Amount of items: 2
Items: 
Size: 1552 Color: 4
Size: 877 Color: 0

Bin 66: 1898 of cap free
Amount of items: 8
Items: 
Size: 88 Color: 4
Size: 88 Color: 3
Size: 80 Color: 1
Size: 76 Color: 2
Size: 72 Color: 2
Size: 70 Color: 1
Size: 56 Color: 0
Size: 44 Color: 0

Total size: 160680
Total free space: 2472

