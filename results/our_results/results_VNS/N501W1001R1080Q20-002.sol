Capicity Bin: 1001
Lower Bound: 232

Bins used: 233
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 569 Color: 14
Size: 308 Color: 17
Size: 124 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 594 Color: 9
Size: 272 Color: 19
Size: 135 Color: 6

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 19
Size: 460 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 564 Color: 11
Size: 317 Color: 16
Size: 120 Color: 4

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 12
Size: 438 Color: 7

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 9
Size: 380 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 9
Size: 172 Color: 1
Size: 161 Color: 10

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 18
Size: 333 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 358 Color: 16
Size: 272 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 563 Color: 12
Size: 311 Color: 8
Size: 127 Color: 0

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 4
Size: 218 Color: 1

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 19
Size: 385 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 673 Color: 9
Size: 165 Color: 15
Size: 163 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 12
Size: 323 Color: 3
Size: 179 Color: 8

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 6
Size: 405 Color: 9

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 466 Color: 2

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 13
Size: 405 Color: 10

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 12
Size: 229 Color: 5

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 8
Size: 497 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 19
Size: 310 Color: 17
Size: 310 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 3
Size: 316 Color: 18
Size: 288 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 574 Color: 8
Size: 282 Color: 0
Size: 145 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 8
Size: 228 Color: 4
Size: 185 Color: 2

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 6
Size: 357 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 7
Size: 344 Color: 13
Size: 294 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 9
Size: 366 Color: 17
Size: 100 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 503 Color: 4
Size: 391 Color: 5
Size: 107 Color: 8

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 507 Color: 12
Size: 370 Color: 8
Size: 124 Color: 15

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 11
Size: 370 Color: 9
Size: 134 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 13
Size: 373 Color: 15
Size: 141 Color: 18

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 15
Size: 363 Color: 9
Size: 273 Color: 16

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 357 Color: 5
Size: 247 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 508 Color: 0
Size: 316 Color: 17
Size: 177 Color: 18

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 9
Size: 486 Color: 17

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 15
Size: 486 Color: 13

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 11
Size: 481 Color: 0

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 0
Size: 481 Color: 1

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 16
Size: 474 Color: 1

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 6
Size: 468 Color: 19

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 14
Size: 467 Color: 18

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 5
Size: 462 Color: 15

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 5
Size: 454 Color: 15

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 5
Size: 450 Color: 14

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 1
Size: 454 Color: 2

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 3
Size: 447 Color: 11

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 12
Size: 447 Color: 11

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 15
Size: 309 Color: 0
Size: 124 Color: 16

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 561 Color: 3
Size: 316 Color: 10
Size: 124 Color: 16

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 13
Size: 432 Color: 19

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 5
Size: 426 Color: 9

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 18
Size: 414 Color: 2

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 14
Size: 413 Color: 16

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 19
Size: 410 Color: 6

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 5
Size: 408 Color: 9

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 17
Size: 401 Color: 7

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 14
Size: 398 Color: 12

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 8
Size: 395 Color: 12

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 13
Size: 395 Color: 8

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 10
Size: 271 Color: 7
Size: 120 Color: 17

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 16
Size: 390 Color: 5

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 11
Size: 389 Color: 1

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 15
Size: 382 Color: 8

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 5
Size: 368 Color: 0

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 3
Size: 367 Color: 11

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 636 Color: 10
Size: 187 Color: 5
Size: 178 Color: 16

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 6
Size: 365 Color: 13

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 6
Size: 364 Color: 3

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 1

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 17
Size: 355 Color: 14

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 16
Size: 355 Color: 2

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 14
Size: 353 Color: 1

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 12
Size: 353 Color: 9

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 9
Size: 351 Color: 11

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 13
Size: 350 Color: 1

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 10
Size: 350 Color: 18

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 11
Size: 350 Color: 14

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 7
Size: 346 Color: 19

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 15
Size: 343 Color: 11

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 8
Size: 340 Color: 19

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 19
Size: 174 Color: 12
Size: 156 Color: 1

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 11
Size: 329 Color: 7

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 11
Size: 168 Color: 10
Size: 156 Color: 5

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 3
Size: 170 Color: 12
Size: 153 Color: 19

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 6
Size: 322 Color: 9

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 15
Size: 313 Color: 9

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 15
Size: 307 Color: 13

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 15
Size: 178 Color: 3
Size: 128 Color: 12

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 4
Size: 298 Color: 1

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 2
Size: 296 Color: 7

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 18
Size: 294 Color: 6

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 8
Size: 291 Color: 14

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 10
Size: 285 Color: 11

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 19
Size: 283 Color: 2

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 8
Size: 279 Color: 14

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 18
Size: 274 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 18
Size: 136 Color: 8
Size: 132 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 12
Size: 155 Color: 11
Size: 113 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 12
Size: 146 Color: 11
Size: 121 Color: 19

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 16
Size: 135 Color: 19
Size: 132 Color: 8

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 11
Size: 143 Color: 7
Size: 123 Color: 3

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 4
Size: 265 Color: 9

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 14
Size: 263 Color: 17

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 9
Size: 275 Color: 2

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 11
Size: 261 Color: 4

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 14
Size: 254 Color: 19

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 756 Color: 2
Size: 136 Color: 9
Size: 109 Color: 1

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 0
Size: 244 Color: 16

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 759 Color: 19
Size: 127 Color: 3
Size: 115 Color: 3

Bin 109: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 5
Size: 242 Color: 4

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 767 Color: 6
Size: 119 Color: 7
Size: 115 Color: 12

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 10
Size: 231 Color: 11

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 5
Size: 230 Color: 4

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 4
Size: 228 Color: 9

Bin 114: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 13
Size: 225 Color: 16

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 784 Color: 13
Size: 115 Color: 11
Size: 102 Color: 17

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 9
Size: 216 Color: 17

Bin 117: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 6
Size: 213 Color: 18

Bin 118: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 7
Size: 212 Color: 16

Bin 119: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 16
Size: 206 Color: 13

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 4

Bin 121: 1 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 1
Size: 482 Color: 17

Bin 122: 1 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 13
Size: 485 Color: 16

Bin 123: 1 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 7
Size: 463 Color: 13

Bin 124: 1 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 6
Size: 447 Color: 19

Bin 125: 1 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 2
Size: 430 Color: 10

Bin 126: 1 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 4
Size: 426 Color: 15

Bin 127: 1 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 10
Size: 416 Color: 9

Bin 128: 1 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 8
Size: 406 Color: 16

Bin 129: 1 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 1
Size: 399 Color: 12

Bin 130: 1 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 4
Size: 383 Color: 3

Bin 131: 1 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 2
Size: 366 Color: 0

Bin 132: 1 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 11
Size: 358 Color: 0

Bin 133: 1 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 4
Size: 332 Color: 15

Bin 134: 1 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 10
Size: 294 Color: 5

Bin 135: 1 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 18
Size: 269 Color: 4

Bin 136: 1 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 15
Size: 249 Color: 7

Bin 137: 1 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 10
Size: 240 Color: 19

Bin 138: 1 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 13
Size: 240 Color: 6

Bin 139: 1 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 8
Size: 237 Color: 6

Bin 140: 1 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 12
Size: 231 Color: 1

Bin 141: 1 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 18
Size: 219 Color: 2

Bin 142: 1 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 3
Size: 210 Color: 18

Bin 143: 1 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 4
Size: 210 Color: 14

Bin 144: 1 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 430 Color: 17

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 2
Size: 182 Color: 17
Size: 159 Color: 7

Bin 146: 1 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 3
Size: 285 Color: 6

Bin 147: 1 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 9
Size: 290 Color: 1

Bin 148: 1 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 3
Size: 493 Color: 16

Bin 149: 1 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 14
Size: 233 Color: 13

Bin 150: 1 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 17
Size: 378 Color: 13

Bin 151: 1 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 7
Size: 458 Color: 11

Bin 152: 2 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 14
Size: 380 Color: 9

Bin 153: 2 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 4
Size: 489 Color: 2

Bin 154: 2 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 19
Size: 423 Color: 14

Bin 155: 2 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 3
Size: 415 Color: 15

Bin 156: 2 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 7
Size: 377 Color: 19

Bin 157: 2 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 1
Size: 331 Color: 15

Bin 158: 2 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 3
Size: 326 Color: 9

Bin 159: 2 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 16
Size: 313 Color: 11

Bin 160: 2 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 9
Size: 294 Color: 15

Bin 161: 2 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 14
Size: 282 Color: 17

Bin 162: 2 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 11
Size: 260 Color: 2

Bin 163: 2 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 10
Size: 213 Color: 19

Bin 164: 2 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 2
Size: 470 Color: 13

Bin 165: 3 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 4
Size: 219 Color: 11

Bin 166: 3 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 18
Size: 462 Color: 10

Bin 167: 3 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 16
Size: 445 Color: 6

Bin 168: 3 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 12
Size: 415 Color: 5

Bin 169: 3 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 7
Size: 405 Color: 17

Bin 170: 3 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 7
Size: 349 Color: 5

Bin 171: 3 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 5
Size: 197 Color: 19

Bin 172: 3 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 3
Size: 434 Color: 14

Bin 173: 3 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 19
Size: 462 Color: 17

Bin 174: 3 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 17
Size: 331 Color: 12

Bin 175: 4 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 9
Size: 331 Color: 4

Bin 176: 4 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 13
Size: 434 Color: 1

Bin 177: 4 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 13
Size: 484 Color: 3

Bin 178: 4 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 6
Size: 450 Color: 1

Bin 179: 4 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 15
Size: 421 Color: 6

Bin 180: 4 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 8
Size: 421 Color: 1

Bin 181: 4 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 16
Size: 348 Color: 10

Bin 182: 4 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 15
Size: 303 Color: 11

Bin 183: 4 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 18
Size: 357 Color: 13

Bin 184: 4 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 226 Color: 8

Bin 185: 5 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 19
Size: 348 Color: 12

Bin 186: 5 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 18
Size: 259 Color: 5

Bin 187: 5 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 18
Size: 219 Color: 8

Bin 188: 5 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 0
Size: 210 Color: 10

Bin 189: 5 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 3
Size: 338 Color: 6

Bin 190: 6 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 13
Size: 338 Color: 11

Bin 191: 6 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 8
Size: 196 Color: 10

Bin 192: 6 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 8
Size: 461 Color: 18

Bin 193: 7 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 1
Size: 280 Color: 10

Bin 194: 7 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 2
Size: 489 Color: 3

Bin 195: 7 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 10
Size: 293 Color: 19

Bin 196: 7 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 9
Size: 442 Color: 7

Bin 197: 7 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 4
Size: 442 Color: 18

Bin 198: 7 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 19
Size: 420 Color: 7

Bin 199: 7 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 7
Size: 411 Color: 14

Bin 200: 8 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 16
Size: 460 Color: 8

Bin 201: 8 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 6
Size: 247 Color: 12

Bin 202: 8 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 14
Size: 196 Color: 1

Bin 203: 9 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 7
Size: 319 Color: 15

Bin 204: 9 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 1
Size: 193 Color: 19

Bin 205: 9 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 9
Size: 400 Color: 1

Bin 206: 10 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 17
Size: 458 Color: 2

Bin 207: 10 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 269 Color: 14

Bin 208: 10 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 10
Size: 246 Color: 15

Bin 209: 10 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 9
Size: 305 Color: 15

Bin 210: 10 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 15
Size: 245 Color: 11

Bin 211: 12 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 377 Color: 7

Bin 212: 12 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 18
Size: 317 Color: 4

Bin 213: 12 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 12
Size: 192 Color: 18

Bin 214: 14 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 7
Size: 316 Color: 0

Bin 215: 14 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 18
Size: 316 Color: 7

Bin 216: 14 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 1
Size: 217 Color: 14

Bin 217: 15 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 18
Size: 243 Color: 6

Bin 218: 17 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 14
Size: 395 Color: 18

Bin 219: 17 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 3
Size: 458 Color: 15

Bin 220: 18 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 13
Size: 190 Color: 4

Bin 221: 18 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 4
Size: 458 Color: 3

Bin 222: 21 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 19
Size: 369 Color: 12

Bin 223: 22 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 1
Size: 457 Color: 13

Bin 224: 26 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 455 Color: 19

Bin 225: 26 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 9
Size: 190 Color: 15

Bin 226: 34 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 356 Color: 16

Bin 227: 40 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 393 Color: 10

Bin 228: 47 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 393 Color: 6

Bin 229: 92 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 5
Size: 391 Color: 2

Bin 230: 235 of cap free
Amount of items: 1
Items: 
Size: 766 Color: 7

Bin 231: 235 of cap free
Amount of items: 1
Items: 
Size: 766 Color: 15

Bin 232: 258 of cap free
Amount of items: 1
Items: 
Size: 743 Color: 8

Bin 233: 292 of cap free
Amount of items: 1
Items: 
Size: 709 Color: 11

Total size: 231422
Total free space: 1811

