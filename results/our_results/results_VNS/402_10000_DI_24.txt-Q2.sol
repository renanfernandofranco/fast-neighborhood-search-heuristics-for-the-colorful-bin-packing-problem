Capicity Bin: 8192
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 5069 Color: 0
Size: 2951 Color: 1
Size: 140 Color: 1
Size: 32 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 0
Size: 1353 Color: 1
Size: 937 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7086 Color: 0
Size: 922 Color: 1
Size: 184 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4607 Color: 0
Size: 3341 Color: 0
Size: 244 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6033 Color: 0
Size: 1187 Color: 1
Size: 972 Color: 1

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 6462 Color: 1
Size: 964 Color: 0
Size: 414 Color: 0
Size: 352 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 0
Size: 790 Color: 1
Size: 78 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7070 Color: 1
Size: 682 Color: 0
Size: 440 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6602 Color: 1
Size: 1078 Color: 0
Size: 512 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7346 Color: 0
Size: 680 Color: 0
Size: 166 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4649 Color: 1
Size: 2953 Color: 0
Size: 590 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 0
Size: 1804 Color: 1
Size: 144 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 3414 Color: 1
Size: 3413 Color: 1
Size: 1365 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7302 Color: 1
Size: 576 Color: 1
Size: 314 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5172 Color: 0
Size: 2908 Color: 1
Size: 112 Color: 0

Bin 16: 0 of cap free
Amount of items: 4
Items: 
Size: 5676 Color: 1
Size: 2212 Color: 0
Size: 164 Color: 1
Size: 140 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6819 Color: 0
Size: 1229 Color: 1
Size: 144 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6453 Color: 0
Size: 1451 Color: 1
Size: 288 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 1
Size: 764 Color: 1
Size: 248 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 0
Size: 894 Color: 1
Size: 254 Color: 0

Bin 21: 0 of cap free
Amount of items: 4
Items: 
Size: 4544 Color: 1
Size: 2780 Color: 0
Size: 462 Color: 0
Size: 406 Color: 1

Bin 22: 0 of cap free
Amount of items: 7
Items: 
Size: 4098 Color: 0
Size: 892 Color: 0
Size: 764 Color: 1
Size: 742 Color: 1
Size: 680 Color: 1
Size: 520 Color: 0
Size: 496 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5958 Color: 0
Size: 1141 Color: 0
Size: 1093 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 1
Size: 836 Color: 0
Size: 216 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6438 Color: 0
Size: 1462 Color: 1
Size: 292 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6949 Color: 0
Size: 1037 Color: 0
Size: 206 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6305 Color: 0
Size: 1797 Color: 1
Size: 90 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6769 Color: 0
Size: 1255 Color: 1
Size: 168 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 1
Size: 1020 Color: 0
Size: 200 Color: 0

Bin 30: 0 of cap free
Amount of items: 26
Items: 
Size: 818 Color: 0
Size: 432 Color: 0
Size: 404 Color: 1
Size: 380 Color: 1
Size: 372 Color: 1
Size: 372 Color: 1
Size: 368 Color: 1
Size: 366 Color: 1
Size: 358 Color: 0
Size: 340 Color: 0
Size: 336 Color: 0
Size: 328 Color: 1
Size: 322 Color: 1
Size: 320 Color: 1
Size: 314 Color: 0
Size: 270 Color: 0
Size: 264 Color: 1
Size: 264 Color: 1
Size: 258 Color: 0
Size: 232 Color: 0
Size: 224 Color: 0
Size: 200 Color: 0
Size: 192 Color: 1
Size: 186 Color: 0
Size: 176 Color: 0
Size: 96 Color: 1

Bin 31: 0 of cap free
Amount of items: 11
Items: 
Size: 6356 Color: 1
Size: 262 Color: 1
Size: 240 Color: 1
Size: 236 Color: 1
Size: 176 Color: 0
Size: 162 Color: 0
Size: 160 Color: 0
Size: 156 Color: 0
Size: 152 Color: 1
Size: 148 Color: 0
Size: 144 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6036 Color: 1
Size: 1988 Color: 0
Size: 168 Color: 0

Bin 33: 0 of cap free
Amount of items: 13
Items: 
Size: 1983 Color: 1
Size: 1295 Color: 1
Size: 680 Color: 1
Size: 552 Color: 1
Size: 520 Color: 1
Size: 508 Color: 1
Size: 456 Color: 0
Size: 436 Color: 1
Size: 406 Color: 0
Size: 392 Color: 0
Size: 384 Color: 0
Size: 352 Color: 0
Size: 228 Color: 0

Bin 34: 0 of cap free
Amount of items: 8
Items: 
Size: 4100 Color: 0
Size: 884 Color: 1
Size: 872 Color: 1
Size: 706 Color: 0
Size: 512 Color: 1
Size: 464 Color: 1
Size: 436 Color: 0
Size: 218 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4676 Color: 0
Size: 2932 Color: 1
Size: 584 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4674 Color: 1
Size: 3308 Color: 1
Size: 210 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5110 Color: 0
Size: 2934 Color: 1
Size: 148 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 1
Size: 2460 Color: 1
Size: 488 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5415 Color: 1
Size: 1763 Color: 0
Size: 1014 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 0
Size: 2524 Color: 1
Size: 264 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5918 Color: 0
Size: 2162 Color: 0
Size: 112 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 1
Size: 1916 Color: 0
Size: 376 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 1
Size: 1850 Color: 0
Size: 194 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6214 Color: 0
Size: 1572 Color: 0
Size: 406 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6242 Color: 0
Size: 1626 Color: 0
Size: 324 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6250 Color: 0
Size: 1628 Color: 0
Size: 314 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6313 Color: 0
Size: 1567 Color: 0
Size: 312 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 0
Size: 1452 Color: 1
Size: 288 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6617 Color: 1
Size: 1313 Color: 0
Size: 262 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6609 Color: 0
Size: 1321 Color: 0
Size: 262 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 0
Size: 1324 Color: 0
Size: 216 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 0
Size: 1202 Color: 1
Size: 264 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 0
Size: 1180 Color: 1
Size: 232 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6886 Color: 0
Size: 718 Color: 1
Size: 588 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 7037 Color: 0
Size: 1019 Color: 0
Size: 136 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 1
Size: 640 Color: 1
Size: 574 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 7214 Color: 0
Size: 758 Color: 0
Size: 220 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7246 Color: 1
Size: 730 Color: 0
Size: 216 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 0
Size: 656 Color: 0
Size: 284 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 1
Size: 716 Color: 1
Size: 200 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 7280 Color: 1
Size: 760 Color: 0
Size: 152 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 7284 Color: 0
Size: 724 Color: 0
Size: 184 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7340 Color: 1
Size: 588 Color: 0
Size: 264 Color: 0

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 5745 Color: 0
Size: 2202 Color: 0
Size: 244 Color: 1

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 0
Size: 2027 Color: 0
Size: 190 Color: 1

Bin 66: 1 of cap free
Amount of items: 4
Items: 
Size: 6077 Color: 0
Size: 1862 Color: 0
Size: 208 Color: 1
Size: 44 Color: 1

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 0
Size: 1568 Color: 1
Size: 136 Color: 0

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 6637 Color: 1
Size: 1090 Color: 0
Size: 464 Color: 0

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 6693 Color: 0
Size: 1498 Color: 1

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 0
Size: 854 Color: 1
Size: 680 Color: 1

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 5749 Color: 0
Size: 2322 Color: 1
Size: 120 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 6037 Color: 0
Size: 1650 Color: 0
Size: 504 Color: 1

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 6921 Color: 1
Size: 1110 Color: 0
Size: 160 Color: 1

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 1
Size: 2037 Color: 0
Size: 600 Color: 0

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 0
Size: 1833 Color: 0
Size: 1708 Color: 1

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 6823 Color: 1
Size: 1232 Color: 0
Size: 136 Color: 0

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 6905 Color: 1
Size: 1222 Color: 0
Size: 64 Color: 1

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 6873 Color: 1
Size: 1317 Color: 0

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 1
Size: 584 Color: 0
Size: 320 Color: 1

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 0
Size: 1024 Color: 1
Size: 120 Color: 1

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 1
Size: 1061 Color: 0
Size: 828 Color: 1

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 4609 Color: 1
Size: 3412 Color: 1
Size: 168 Color: 0

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 0
Size: 1281 Color: 1

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 6985 Color: 0
Size: 1204 Color: 1

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 5705 Color: 1
Size: 2252 Color: 0
Size: 232 Color: 0

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 6862 Color: 0
Size: 1326 Color: 1

Bin 87: 5 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 1
Size: 2954 Color: 1
Size: 166 Color: 0

Bin 88: 5 of cap free
Amount of items: 3
Items: 
Size: 4102 Color: 0
Size: 3009 Color: 1
Size: 1076 Color: 0

Bin 89: 6 of cap free
Amount of items: 2
Items: 
Size: 5154 Color: 0
Size: 3032 Color: 1

Bin 90: 6 of cap free
Amount of items: 2
Items: 
Size: 6613 Color: 1
Size: 1573 Color: 0

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 5578 Color: 1
Size: 2605 Color: 0

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 6257 Color: 0
Size: 1926 Color: 1

Bin 93: 9 of cap free
Amount of items: 3
Items: 
Size: 5950 Color: 0
Size: 2073 Color: 0
Size: 160 Color: 1

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 6569 Color: 1
Size: 1613 Color: 0

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 6884 Color: 0
Size: 1297 Color: 1

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 6842 Color: 1
Size: 1338 Color: 0

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 7160 Color: 0
Size: 1018 Color: 1

Bin 98: 14 of cap free
Amount of items: 2
Items: 
Size: 5130 Color: 1
Size: 3048 Color: 0

Bin 99: 14 of cap free
Amount of items: 2
Items: 
Size: 7196 Color: 0
Size: 982 Color: 1

Bin 100: 15 of cap free
Amount of items: 2
Items: 
Size: 6596 Color: 0
Size: 1581 Color: 1

Bin 101: 15 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 1
Size: 1421 Color: 0

Bin 102: 17 of cap free
Amount of items: 3
Items: 
Size: 4101 Color: 1
Size: 2041 Color: 1
Size: 2033 Color: 0

Bin 103: 17 of cap free
Amount of items: 2
Items: 
Size: 5993 Color: 0
Size: 2182 Color: 1

Bin 104: 17 of cap free
Amount of items: 2
Items: 
Size: 7049 Color: 0
Size: 1126 Color: 1

Bin 105: 19 of cap free
Amount of items: 2
Items: 
Size: 5570 Color: 0
Size: 2603 Color: 1

Bin 106: 25 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 0
Size: 1041 Color: 1
Size: 376 Color: 0

Bin 107: 25 of cap free
Amount of items: 2
Items: 
Size: 6297 Color: 0
Size: 1870 Color: 1

Bin 108: 25 of cap free
Amount of items: 2
Items: 
Size: 6590 Color: 0
Size: 1577 Color: 1

Bin 109: 26 of cap free
Amount of items: 2
Items: 
Size: 6544 Color: 0
Size: 1622 Color: 1

Bin 110: 29 of cap free
Amount of items: 2
Items: 
Size: 5882 Color: 1
Size: 2281 Color: 0

Bin 111: 31 of cap free
Amount of items: 2
Items: 
Size: 7069 Color: 0
Size: 1092 Color: 1

Bin 112: 39 of cap free
Amount of items: 2
Items: 
Size: 6661 Color: 1
Size: 1492 Color: 0

Bin 113: 47 of cap free
Amount of items: 2
Items: 
Size: 4097 Color: 1
Size: 4048 Color: 0

Bin 114: 49 of cap free
Amount of items: 2
Items: 
Size: 7170 Color: 1
Size: 973 Color: 0

Bin 115: 56 of cap free
Amount of items: 2
Items: 
Size: 5812 Color: 1
Size: 2324 Color: 0

Bin 116: 60 of cap free
Amount of items: 2
Items: 
Size: 6881 Color: 1
Size: 1251 Color: 0

Bin 117: 73 of cap free
Amount of items: 2
Items: 
Size: 4708 Color: 0
Size: 3411 Color: 1

Bin 118: 82 of cap free
Amount of items: 2
Items: 
Size: 5540 Color: 0
Size: 2570 Color: 1

Bin 119: 90 of cap free
Amount of items: 3
Items: 
Size: 4860 Color: 1
Size: 1910 Color: 0
Size: 1332 Color: 1

Bin 120: 90 of cap free
Amount of items: 2
Items: 
Size: 6974 Color: 1
Size: 1128 Color: 0

Bin 121: 108 of cap free
Amount of items: 2
Items: 
Size: 5530 Color: 1
Size: 2554 Color: 0

Bin 122: 117 of cap free
Amount of items: 2
Items: 
Size: 7122 Color: 1
Size: 953 Color: 0

Bin 123: 124 of cap free
Amount of items: 2
Items: 
Size: 5753 Color: 0
Size: 2315 Color: 1

Bin 124: 129 of cap free
Amount of items: 2
Items: 
Size: 4653 Color: 1
Size: 3410 Color: 0

Bin 125: 162 of cap free
Amount of items: 2
Items: 
Size: 6641 Color: 1
Size: 1389 Color: 0

Bin 126: 203 of cap free
Amount of items: 2
Items: 
Size: 5455 Color: 1
Size: 2534 Color: 0

Bin 127: 245 of cap free
Amount of items: 2
Items: 
Size: 5761 Color: 1
Size: 2186 Color: 0

Bin 128: 372 of cap free
Amount of items: 2
Items: 
Size: 4228 Color: 1
Size: 3592 Color: 0

Bin 129: 858 of cap free
Amount of items: 1
Items: 
Size: 7334 Color: 0

Bin 130: 874 of cap free
Amount of items: 1
Items: 
Size: 7318 Color: 1

Bin 131: 1164 of cap free
Amount of items: 1
Items: 
Size: 7028 Color: 0

Bin 132: 1167 of cap free
Amount of items: 1
Items: 
Size: 7025 Color: 0

Bin 133: 1660 of cap free
Amount of items: 1
Items: 
Size: 6532 Color: 1

Total size: 1081344
Total free space: 8192

