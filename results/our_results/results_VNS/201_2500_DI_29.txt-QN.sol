Capicity Bin: 2064
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 166
Size: 476 Color: 113
Size: 42 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1611 Color: 170
Size: 389 Color: 103
Size: 64 Color: 33

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 171
Size: 414 Color: 105
Size: 36 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 175
Size: 331 Color: 97
Size: 78 Color: 41

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 177
Size: 274 Color: 89
Size: 100 Color: 50

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 178
Size: 362 Color: 99
Size: 8 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1703 Color: 179
Size: 301 Color: 92
Size: 60 Color: 31

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 180
Size: 293 Color: 91
Size: 58 Color: 28

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 181
Size: 257 Color: 85
Size: 90 Color: 46

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 184
Size: 271 Color: 88
Size: 52 Color: 24

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 188
Size: 168 Color: 65
Size: 120 Color: 55

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 189
Size: 211 Color: 75
Size: 76 Color: 39

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 190
Size: 244 Color: 84
Size: 40 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 192
Size: 226 Color: 78
Size: 44 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 193
Size: 195 Color: 71
Size: 72 Color: 36

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 194
Size: 219 Color: 76
Size: 34 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 200
Size: 148 Color: 62
Size: 74 Color: 38

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 201
Size: 182 Color: 69
Size: 36 Color: 8

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 144
Size: 851 Color: 133
Size: 46 Color: 20

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 163
Size: 534 Color: 117
Size: 16 Color: 3

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 168
Size: 461 Color: 111
Size: 32 Color: 4

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1669 Color: 176
Size: 394 Color: 104

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 182
Size: 341 Color: 98
Size: 4 Color: 0

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1773 Color: 187
Size: 290 Color: 90

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 191
Size: 186 Color: 70
Size: 96 Color: 49

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1822 Color: 196
Size: 241 Color: 82

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1826 Color: 197
Size: 237 Color: 81

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1831 Color: 198
Size: 232 Color: 80

Bin 29: 2 of cap free
Amount of items: 5
Items: 
Size: 1041 Color: 141
Size: 853 Color: 134
Size: 60 Color: 29
Size: 56 Color: 27
Size: 52 Color: 26

Bin 30: 2 of cap free
Amount of items: 5
Items: 
Size: 1045 Color: 142
Size: 857 Color: 135
Size: 60 Color: 30
Size: 52 Color: 25
Size: 48 Color: 23

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1459 Color: 160
Size: 603 Color: 119

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1537 Color: 165
Size: 457 Color: 110
Size: 68 Color: 34

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1615 Color: 172
Size: 441 Color: 109
Size: 6 Color: 1

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1634 Color: 174
Size: 428 Color: 106

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1738 Color: 183
Size: 324 Color: 96

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1818 Color: 195
Size: 244 Color: 83

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1839 Color: 199
Size: 223 Color: 77

Bin 38: 3 of cap free
Amount of items: 4
Items: 
Size: 1106 Color: 143
Size: 861 Color: 136
Size: 48 Color: 22
Size: 46 Color: 21

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 152
Size: 691 Color: 125
Size: 44 Color: 15

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1627 Color: 173
Size: 434 Color: 108

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 1751 Color: 185
Size: 310 Color: 94

Bin 42: 3 of cap free
Amount of items: 2
Items: 
Size: 1758 Color: 186
Size: 303 Color: 93

Bin 43: 5 of cap free
Amount of items: 3
Items: 
Size: 1341 Color: 153
Size: 678 Color: 124
Size: 40 Color: 12

Bin 44: 5 of cap free
Amount of items: 3
Items: 
Size: 1419 Color: 156
Size: 379 Color: 102
Size: 261 Color: 87

Bin 45: 7 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 145
Size: 832 Color: 132
Size: 44 Color: 19

Bin 46: 7 of cap free
Amount of items: 3
Items: 
Size: 1296 Color: 150
Size: 717 Color: 127
Size: 44 Color: 16

Bin 47: 8 of cap free
Amount of items: 5
Items: 
Size: 1037 Color: 140
Size: 431 Color: 107
Size: 378 Color: 101
Size: 146 Color: 61
Size: 64 Color: 32

Bin 48: 8 of cap free
Amount of items: 2
Items: 
Size: 1517 Color: 164
Size: 539 Color: 118

Bin 49: 8 of cap free
Amount of items: 2
Items: 
Size: 1594 Color: 169
Size: 462 Color: 112

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 1193 Color: 146
Size: 862 Color: 137

Bin 51: 10 of cap free
Amount of items: 3
Items: 
Size: 1217 Color: 147
Size: 793 Color: 130
Size: 44 Color: 18

Bin 52: 10 of cap free
Amount of items: 2
Items: 
Size: 1549 Color: 167
Size: 505 Color: 115

Bin 53: 11 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 155
Size: 606 Color: 120
Size: 40 Color: 11

Bin 54: 12 of cap free
Amount of items: 2
Items: 
Size: 1315 Color: 151
Size: 737 Color: 128

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1427 Color: 159
Size: 625 Color: 123

Bin 56: 13 of cap free
Amount of items: 2
Items: 
Size: 1426 Color: 158
Size: 625 Color: 122

Bin 57: 15 of cap free
Amount of items: 5
Items: 
Size: 1034 Color: 139
Size: 375 Color: 100
Size: 314 Color: 95
Size: 258 Color: 86
Size: 68 Color: 35

Bin 58: 16 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 149
Size: 750 Color: 129
Size: 44 Color: 17

Bin 59: 18 of cap free
Amount of items: 2
Items: 
Size: 1350 Color: 154
Size: 696 Color: 126

Bin 60: 20 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 162
Size: 498 Color: 114
Size: 36 Color: 7

Bin 61: 22 of cap free
Amount of items: 2
Items: 
Size: 1424 Color: 157
Size: 618 Color: 121

Bin 62: 25 of cap free
Amount of items: 2
Items: 
Size: 1237 Color: 148
Size: 802 Color: 131

Bin 63: 25 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 161
Size: 531 Color: 116
Size: 38 Color: 9

Bin 64: 43 of cap free
Amount of items: 7
Items: 
Size: 1033 Color: 138
Size: 228 Color: 79
Size: 206 Color: 74
Size: 202 Color: 73
Size: 202 Color: 72
Size: 76 Color: 40
Size: 74 Color: 37

Bin 65: 88 of cap free
Amount of items: 16
Items: 
Size: 170 Color: 68
Size: 170 Color: 67
Size: 170 Color: 66
Size: 168 Color: 64
Size: 156 Color: 63
Size: 136 Color: 60
Size: 132 Color: 59
Size: 130 Color: 58
Size: 124 Color: 57
Size: 104 Color: 51
Size: 92 Color: 48
Size: 90 Color: 47
Size: 86 Color: 45
Size: 84 Color: 44
Size: 84 Color: 43
Size: 80 Color: 42

Bin 66: 1624 of cap free
Amount of items: 4
Items: 
Size: 120 Color: 56
Size: 108 Color: 54
Size: 106 Color: 53
Size: 106 Color: 52

Total size: 134160
Total free space: 2064

