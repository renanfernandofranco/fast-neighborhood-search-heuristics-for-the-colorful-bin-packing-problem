Capicity Bin: 9808
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4906 Color: 15
Size: 2050 Color: 3
Size: 1644 Color: 6
Size: 816 Color: 10
Size: 392 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 4
Size: 3784 Color: 14
Size: 1108 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 11
Size: 4408 Color: 19
Size: 480 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 16
Size: 3522 Color: 13
Size: 700 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5618 Color: 18
Size: 3494 Color: 0
Size: 696 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5896 Color: 19
Size: 3540 Color: 7
Size: 372 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 11
Size: 2932 Color: 8
Size: 584 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7350 Color: 15
Size: 2190 Color: 1
Size: 268 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7562 Color: 10
Size: 2078 Color: 19
Size: 168 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7594 Color: 9
Size: 1846 Color: 11
Size: 368 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7623 Color: 4
Size: 1925 Color: 12
Size: 260 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7701 Color: 8
Size: 1279 Color: 5
Size: 828 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7806 Color: 9
Size: 1194 Color: 11
Size: 808 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7960 Color: 18
Size: 1288 Color: 13
Size: 560 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7990 Color: 11
Size: 1242 Color: 10
Size: 576 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8005 Color: 2
Size: 1195 Color: 1
Size: 608 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 8022 Color: 4
Size: 1490 Color: 2
Size: 296 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 8122 Color: 13
Size: 1404 Color: 3
Size: 282 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 8155 Color: 13
Size: 1379 Color: 18
Size: 274 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 1
Size: 1188 Color: 6
Size: 340 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8287 Color: 14
Size: 1269 Color: 17
Size: 252 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8310 Color: 8
Size: 1350 Color: 12
Size: 148 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 8375 Color: 4
Size: 1175 Color: 3
Size: 258 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8392 Color: 19
Size: 1192 Color: 5
Size: 224 Color: 17

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 8434 Color: 4
Size: 1186 Color: 11
Size: 188 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8436 Color: 16
Size: 800 Color: 16
Size: 572 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8490 Color: 5
Size: 968 Color: 8
Size: 350 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 19
Size: 1036 Color: 12
Size: 288 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 8536 Color: 19
Size: 640 Color: 18
Size: 632 Color: 5

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8574 Color: 17
Size: 1074 Color: 19
Size: 160 Color: 18

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 8578 Color: 0
Size: 1102 Color: 15
Size: 128 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 8626 Color: 10
Size: 856 Color: 9
Size: 326 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 8742 Color: 5
Size: 874 Color: 16
Size: 192 Color: 13

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 8644 Color: 14
Size: 700 Color: 19
Size: 464 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 8650 Color: 10
Size: 890 Color: 11
Size: 268 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 8664 Color: 16
Size: 908 Color: 2
Size: 236 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 5
Size: 816 Color: 13
Size: 232 Color: 19

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 8762 Color: 13
Size: 846 Color: 19
Size: 200 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 19
Size: 752 Color: 7
Size: 264 Color: 5

Bin 40: 1 of cap free
Amount of items: 5
Items: 
Size: 4905 Color: 6
Size: 4084 Color: 18
Size: 440 Color: 8
Size: 206 Color: 10
Size: 172 Color: 19

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6103 Color: 15
Size: 3546 Color: 9
Size: 158 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 3
Size: 2643 Color: 4
Size: 152 Color: 13

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 7192 Color: 8
Size: 2371 Color: 13
Size: 244 Color: 17

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 7151 Color: 5
Size: 2350 Color: 6
Size: 306 Color: 9

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 10
Size: 2491 Color: 16
Size: 160 Color: 14

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 7830 Color: 14
Size: 1757 Color: 19
Size: 220 Color: 5

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 7836 Color: 3
Size: 1039 Color: 8
Size: 932 Color: 5

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 8163 Color: 1
Size: 1316 Color: 4
Size: 328 Color: 8

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 8227 Color: 12
Size: 1326 Color: 5
Size: 254 Color: 10

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 8407 Color: 13
Size: 1400 Color: 1

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 8533 Color: 14
Size: 1064 Color: 11
Size: 210 Color: 6

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 8563 Color: 6
Size: 1244 Color: 11

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 13
Size: 3712 Color: 12
Size: 864 Color: 4

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 5594 Color: 7
Size: 4068 Color: 17
Size: 144 Color: 11

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5748 Color: 14
Size: 3514 Color: 18
Size: 544 Color: 6

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 6101 Color: 18
Size: 3519 Color: 8
Size: 186 Color: 16

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 9
Size: 3272 Color: 13
Size: 388 Color: 17

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 5
Size: 2892 Color: 3
Size: 328 Color: 19

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 11
Size: 2908 Color: 19
Size: 300 Color: 1

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 3
Size: 2686 Color: 14
Size: 280 Color: 8

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 7290 Color: 13
Size: 2516 Color: 19

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 7318 Color: 13
Size: 2488 Color: 6

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 7785 Color: 2
Size: 2021 Color: 6

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 7965 Color: 4
Size: 1169 Color: 0
Size: 672 Color: 12

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 8136 Color: 5
Size: 1670 Color: 10

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 8214 Color: 13
Size: 1504 Color: 5
Size: 88 Color: 8

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 8794 Color: 7
Size: 980 Color: 12
Size: 32 Color: 12

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 8820 Color: 0
Size: 986 Color: 9

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 6
Size: 1720 Color: 1
Size: 1063 Color: 5

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 7461 Color: 0
Size: 2184 Color: 3
Size: 160 Color: 8

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 7741 Color: 7
Size: 1856 Color: 5
Size: 208 Color: 13

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 8399 Color: 3
Size: 1406 Color: 10

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 5272 Color: 10
Size: 4044 Color: 7
Size: 488 Color: 15

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 3
Size: 3818 Color: 8
Size: 432 Color: 13

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 6158 Color: 13
Size: 3054 Color: 14
Size: 592 Color: 12

Bin 76: 4 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 7
Size: 2596 Color: 3
Size: 412 Color: 18

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 5716 Color: 14
Size: 4087 Color: 10

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 6963 Color: 2
Size: 2840 Color: 9

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 7124 Color: 7
Size: 2678 Color: 1

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 8124 Color: 12
Size: 1678 Color: 17

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 8483 Color: 15
Size: 1319 Color: 1

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 5557 Color: 7
Size: 4084 Color: 4
Size: 160 Color: 5

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 15
Size: 2650 Color: 12
Size: 336 Color: 16

Bin 84: 8 of cap free
Amount of items: 3
Items: 
Size: 4910 Color: 12
Size: 4410 Color: 16
Size: 480 Color: 3

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 7996 Color: 6
Size: 1804 Color: 12

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 8150 Color: 9
Size: 1650 Color: 6

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 8442 Color: 10
Size: 1358 Color: 14

Bin 88: 9 of cap free
Amount of items: 2
Items: 
Size: 5559 Color: 0
Size: 4240 Color: 13

Bin 89: 9 of cap free
Amount of items: 3
Items: 
Size: 6630 Color: 5
Size: 2869 Color: 16
Size: 300 Color: 14

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 7477 Color: 7
Size: 2322 Color: 17

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 8255 Color: 15
Size: 1544 Color: 9

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 8748 Color: 2
Size: 1051 Color: 1

Bin 93: 10 of cap free
Amount of items: 3
Items: 
Size: 7087 Color: 15
Size: 2495 Color: 4
Size: 216 Color: 16

Bin 94: 11 of cap free
Amount of items: 3
Items: 
Size: 8182 Color: 5
Size: 1503 Color: 12
Size: 112 Color: 12

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 8378 Color: 5
Size: 1419 Color: 19

Bin 96: 11 of cap free
Amount of items: 2
Items: 
Size: 8547 Color: 14
Size: 1250 Color: 18

Bin 97: 11 of cap free
Amount of items: 2
Items: 
Size: 8692 Color: 2
Size: 1105 Color: 10

Bin 98: 12 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 13
Size: 4584 Color: 15
Size: 280 Color: 8

Bin 99: 12 of cap free
Amount of items: 3
Items: 
Size: 4936 Color: 12
Size: 4052 Color: 18
Size: 808 Color: 3

Bin 100: 12 of cap free
Amount of items: 2
Items: 
Size: 6408 Color: 13
Size: 3388 Color: 5

Bin 101: 12 of cap free
Amount of items: 2
Items: 
Size: 7887 Color: 14
Size: 1909 Color: 15

Bin 102: 12 of cap free
Amount of items: 2
Items: 
Size: 8195 Color: 3
Size: 1601 Color: 7

Bin 103: 14 of cap free
Amount of items: 4
Items: 
Size: 4907 Color: 2
Size: 3543 Color: 3
Size: 1184 Color: 9
Size: 160 Color: 2

Bin 104: 14 of cap free
Amount of items: 2
Items: 
Size: 8107 Color: 8
Size: 1687 Color: 12

Bin 105: 14 of cap free
Amount of items: 3
Items: 
Size: 8190 Color: 19
Size: 1516 Color: 4
Size: 88 Color: 16

Bin 106: 15 of cap free
Amount of items: 3
Items: 
Size: 7283 Color: 5
Size: 1702 Color: 7
Size: 808 Color: 8

Bin 107: 15 of cap free
Amount of items: 2
Items: 
Size: 8275 Color: 16
Size: 1518 Color: 11

Bin 108: 16 of cap free
Amount of items: 3
Items: 
Size: 7428 Color: 7
Size: 2212 Color: 17
Size: 152 Color: 13

Bin 109: 16 of cap free
Amount of items: 2
Items: 
Size: 8410 Color: 0
Size: 1382 Color: 17

Bin 110: 18 of cap free
Amount of items: 2
Items: 
Size: 6748 Color: 8
Size: 3042 Color: 7

Bin 111: 18 of cap free
Amount of items: 3
Items: 
Size: 7752 Color: 1
Size: 1330 Color: 0
Size: 708 Color: 8

Bin 112: 18 of cap free
Amount of items: 2
Items: 
Size: 8572 Color: 18
Size: 1218 Color: 17

Bin 113: 22 of cap free
Amount of items: 2
Items: 
Size: 7798 Color: 0
Size: 1988 Color: 13

Bin 114: 24 of cap free
Amount of items: 2
Items: 
Size: 6340 Color: 5
Size: 3444 Color: 13

Bin 115: 27 of cap free
Amount of items: 3
Items: 
Size: 7766 Color: 2
Size: 1943 Color: 11
Size: 72 Color: 17

Bin 116: 29 of cap free
Amount of items: 5
Items: 
Size: 4904 Color: 15
Size: 1928 Color: 4
Size: 1371 Color: 7
Size: 816 Color: 10
Size: 760 Color: 14

Bin 117: 29 of cap free
Amount of items: 2
Items: 
Size: 6367 Color: 1
Size: 3412 Color: 5

Bin 118: 30 of cap free
Amount of items: 2
Items: 
Size: 7904 Color: 5
Size: 1874 Color: 0

Bin 119: 33 of cap free
Amount of items: 3
Items: 
Size: 7496 Color: 3
Size: 2215 Color: 11
Size: 64 Color: 10

Bin 120: 35 of cap free
Amount of items: 2
Items: 
Size: 8236 Color: 3
Size: 1537 Color: 10

Bin 121: 38 of cap free
Amount of items: 2
Items: 
Size: 5684 Color: 1
Size: 4086 Color: 2

Bin 122: 44 of cap free
Amount of items: 2
Items: 
Size: 4964 Color: 18
Size: 4800 Color: 4

Bin 123: 44 of cap free
Amount of items: 2
Items: 
Size: 8324 Color: 15
Size: 1440 Color: 18

Bin 124: 50 of cap free
Amount of items: 14
Items: 
Size: 1102 Color: 0
Size: 966 Color: 5
Size: 888 Color: 3
Size: 816 Color: 6
Size: 708 Color: 6
Size: 708 Color: 1
Size: 680 Color: 14
Size: 680 Color: 11
Size: 616 Color: 5
Size: 608 Color: 0
Size: 536 Color: 12
Size: 532 Color: 17
Size: 498 Color: 18
Size: 420 Color: 8

Bin 125: 51 of cap free
Amount of items: 2
Items: 
Size: 7652 Color: 11
Size: 2105 Color: 7

Bin 126: 59 of cap free
Amount of items: 2
Items: 
Size: 8322 Color: 7
Size: 1427 Color: 16

Bin 127: 64 of cap free
Amount of items: 3
Items: 
Size: 4908 Color: 0
Size: 3541 Color: 15
Size: 1295 Color: 9

Bin 128: 68 of cap free
Amount of items: 2
Items: 
Size: 4948 Color: 14
Size: 4792 Color: 4

Bin 129: 80 of cap free
Amount of items: 2
Items: 
Size: 6637 Color: 7
Size: 3091 Color: 1

Bin 130: 87 of cap free
Amount of items: 2
Items: 
Size: 5636 Color: 16
Size: 4085 Color: 9

Bin 131: 110 of cap free
Amount of items: 2
Items: 
Size: 6990 Color: 12
Size: 2708 Color: 4

Bin 132: 142 of cap free
Amount of items: 29
Items: 
Size: 528 Color: 15
Size: 528 Color: 13
Size: 496 Color: 2
Size: 474 Color: 14
Size: 468 Color: 5
Size: 442 Color: 12
Size: 408 Color: 17
Size: 384 Color: 17
Size: 384 Color: 12
Size: 352 Color: 10
Size: 336 Color: 1
Size: 332 Color: 18
Size: 332 Color: 16
Size: 328 Color: 14
Size: 320 Color: 0
Size: 304 Color: 7
Size: 296 Color: 3
Size: 276 Color: 8
Size: 274 Color: 4
Size: 272 Color: 18
Size: 262 Color: 8
Size: 256 Color: 3
Size: 248 Color: 9
Size: 240 Color: 10
Size: 240 Color: 6
Size: 238 Color: 9
Size: 234 Color: 6
Size: 230 Color: 14
Size: 184 Color: 1

Bin 133: 8278 of cap free
Amount of items: 8
Items: 
Size: 216 Color: 9
Size: 212 Color: 19
Size: 196 Color: 13
Size: 186 Color: 16
Size: 184 Color: 18
Size: 184 Color: 1
Size: 176 Color: 17
Size: 176 Color: 8

Total size: 1294656
Total free space: 9808

