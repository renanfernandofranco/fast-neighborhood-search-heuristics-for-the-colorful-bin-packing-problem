Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 325 Color: 1
Size: 301 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 0
Size: 250 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 306 Color: 1
Size: 304 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 356 Color: 1
Size: 254 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 293 Color: 1
Size: 267 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 332 Color: 1
Size: 255 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 324 Color: 0
Size: 322 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 299 Color: 1
Size: 284 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 0
Size: 362 Color: 0
Size: 251 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 347 Color: 1
Size: 263 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 325 Color: 0
Size: 263 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 329 Color: 1
Size: 312 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 309 Color: 0
Size: 259 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 0
Size: 250 Color: 1
Size: 250 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 333 Color: 0
Size: 256 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 0
Size: 250 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 317 Color: 1
Size: 263 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 309 Color: 1
Size: 289 Color: 0
Size: 403 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 288 Color: 0
Size: 274 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 343 Color: 1
Size: 297 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 0
Size: 283 Color: 1
Size: 364 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 330 Color: 1
Size: 258 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 270 Color: 1
Size: 251 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 340 Color: 0
Size: 284 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 325 Color: 1
Size: 263 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 341 Color: 0
Size: 288 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 343 Color: 1
Size: 286 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 328 Color: 1
Size: 283 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 293 Color: 1
Size: 266 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 0
Size: 273 Color: 1
Size: 273 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 293 Color: 0
Size: 280 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 317 Color: 0
Size: 259 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 0
Size: 340 Color: 0
Size: 320 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 294 Color: 1
Size: 257 Color: 0

Total size: 34034
Total free space: 0

