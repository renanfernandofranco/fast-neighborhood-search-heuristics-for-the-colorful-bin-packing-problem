Capicity Bin: 1888
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 946 Color: 0
Size: 786 Color: 2
Size: 156 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1090 Color: 2
Size: 738 Color: 0
Size: 60 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1190 Color: 1
Size: 666 Color: 3
Size: 32 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1293 Color: 4
Size: 491 Color: 1
Size: 104 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 1
Size: 522 Color: 4
Size: 40 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 4
Size: 462 Color: 4
Size: 92 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 4
Size: 398 Color: 2
Size: 76 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 3
Size: 265 Color: 2
Size: 198 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 1
Size: 358 Color: 4
Size: 68 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1495 Color: 4
Size: 261 Color: 1
Size: 132 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 1
Size: 327 Color: 4
Size: 48 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 4
Size: 292 Color: 2
Size: 62 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1537 Color: 4
Size: 283 Color: 0
Size: 68 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 0
Size: 269 Color: 1
Size: 48 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 3
Size: 253 Color: 1
Size: 50 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 3
Size: 206 Color: 3
Size: 80 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 0
Size: 222 Color: 1
Size: 44 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1642 Color: 2
Size: 190 Color: 3
Size: 56 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 3
Size: 208 Color: 4
Size: 26 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 2
Size: 191 Color: 0
Size: 36 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 1
Size: 199 Color: 0
Size: 38 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 0
Size: 178 Color: 1
Size: 40 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 0
Size: 134 Color: 1
Size: 68 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1126 Color: 1
Size: 681 Color: 2
Size: 80 Color: 3

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 1
Size: 573 Color: 1
Size: 52 Color: 3

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 4
Size: 522 Color: 1
Size: 64 Color: 4

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 1
Size: 392 Color: 2
Size: 96 Color: 0

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1394 Color: 4
Size: 493 Color: 0

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1451 Color: 4
Size: 436 Color: 2

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 1500 Color: 3
Size: 387 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 0
Size: 350 Color: 2
Size: 20 Color: 3

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 1
Size: 322 Color: 2
Size: 16 Color: 4

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 1
Size: 254 Color: 1
Size: 56 Color: 4

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 1
Size: 182 Color: 3
Size: 112 Color: 0

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 1613 Color: 0
Size: 274 Color: 3

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 2
Size: 208 Color: 0
Size: 50 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 0
Size: 217 Color: 2
Size: 8 Color: 0

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 2
Size: 209 Color: 3

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 1187 Color: 1
Size: 667 Color: 2
Size: 32 Color: 3

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 0
Size: 567 Color: 1
Size: 32 Color: 2

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 1473 Color: 3
Size: 257 Color: 4
Size: 156 Color: 1

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 1562 Color: 4
Size: 324 Color: 0

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 0
Size: 231 Color: 1
Size: 8 Color: 4

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 1006 Color: 0
Size: 787 Color: 2
Size: 92 Color: 1

Bin 45: 3 of cap free
Amount of items: 4
Items: 
Size: 1077 Color: 3
Size: 435 Color: 3
Size: 329 Color: 1
Size: 44 Color: 4

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 1089 Color: 2
Size: 760 Color: 3
Size: 36 Color: 3

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 1184 Color: 0
Size: 669 Color: 1
Size: 32 Color: 3

Bin 48: 3 of cap free
Amount of items: 2
Items: 
Size: 1304 Color: 4
Size: 581 Color: 2

Bin 49: 4 of cap free
Amount of items: 4
Items: 
Size: 1073 Color: 3
Size: 414 Color: 1
Size: 347 Color: 3
Size: 50 Color: 2

Bin 50: 4 of cap free
Amount of items: 2
Items: 
Size: 1586 Color: 3
Size: 298 Color: 2

Bin 51: 5 of cap free
Amount of items: 3
Items: 
Size: 1193 Color: 1
Size: 638 Color: 0
Size: 52 Color: 0

Bin 52: 5 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 0
Size: 640 Color: 4
Size: 42 Color: 3

Bin 53: 6 of cap free
Amount of items: 2
Items: 
Size: 1209 Color: 2
Size: 673 Color: 0

Bin 54: 8 of cap free
Amount of items: 18
Items: 
Size: 170 Color: 3
Size: 144 Color: 2
Size: 134 Color: 4
Size: 134 Color: 2
Size: 132 Color: 3
Size: 124 Color: 1
Size: 114 Color: 4
Size: 114 Color: 0
Size: 114 Color: 0
Size: 104 Color: 3
Size: 100 Color: 2
Size: 98 Color: 3
Size: 98 Color: 1
Size: 86 Color: 1
Size: 76 Color: 1
Size: 58 Color: 4
Size: 44 Color: 4
Size: 36 Color: 0

Bin 55: 9 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 0
Size: 409 Color: 4

Bin 56: 13 of cap free
Amount of items: 5
Items: 
Size: 945 Color: 0
Size: 497 Color: 0
Size: 213 Color: 1
Size: 156 Color: 4
Size: 64 Color: 4

Bin 57: 13 of cap free
Amount of items: 2
Items: 
Size: 1633 Color: 3
Size: 242 Color: 4

Bin 58: 14 of cap free
Amount of items: 2
Items: 
Size: 1197 Color: 2
Size: 677 Color: 3

Bin 59: 14 of cap free
Amount of items: 2
Items: 
Size: 1297 Color: 0
Size: 577 Color: 2

Bin 60: 14 of cap free
Amount of items: 2
Items: 
Size: 1581 Color: 0
Size: 293 Color: 2

Bin 61: 17 of cap free
Amount of items: 2
Items: 
Size: 1506 Color: 4
Size: 365 Color: 0

Bin 62: 20 of cap free
Amount of items: 2
Items: 
Size: 1367 Color: 4
Size: 501 Color: 0

Bin 63: 22 of cap free
Amount of items: 2
Items: 
Size: 1081 Color: 4
Size: 785 Color: 0

Bin 64: 26 of cap free
Amount of items: 4
Items: 
Size: 947 Color: 2
Size: 530 Color: 1
Size: 313 Color: 2
Size: 72 Color: 4

Bin 65: 28 of cap free
Amount of items: 2
Items: 
Size: 1254 Color: 3
Size: 606 Color: 0

Bin 66: 1626 of cap free
Amount of items: 5
Items: 
Size: 72 Color: 2
Size: 68 Color: 0
Size: 44 Color: 1
Size: 42 Color: 3
Size: 36 Color: 4

Total size: 122720
Total free space: 1888

