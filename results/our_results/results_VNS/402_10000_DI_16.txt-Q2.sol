Capicity Bin: 8352
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4637 Color: 0
Size: 3431 Color: 0
Size: 284 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 0
Size: 3348 Color: 0
Size: 208 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 0
Size: 2691 Color: 1
Size: 538 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 0
Size: 3004 Color: 1
Size: 192 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 1
Size: 2660 Color: 1
Size: 216 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5916 Color: 1
Size: 2036 Color: 0
Size: 400 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6252 Color: 0
Size: 1916 Color: 0
Size: 184 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 1
Size: 1466 Color: 1
Size: 474 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 1
Size: 1094 Color: 1
Size: 742 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 0
Size: 1426 Color: 1
Size: 332 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 1
Size: 1057 Color: 0
Size: 412 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 0
Size: 718 Color: 1
Size: 688 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6964 Color: 0
Size: 1204 Color: 0
Size: 184 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 1
Size: 692 Color: 1
Size: 640 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7052 Color: 0
Size: 724 Color: 1
Size: 576 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7085 Color: 1
Size: 747 Color: 1
Size: 520 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7215 Color: 0
Size: 825 Color: 1
Size: 312 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 0
Size: 1012 Color: 1
Size: 64 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 0
Size: 692 Color: 0
Size: 336 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7353 Color: 1
Size: 771 Color: 0
Size: 228 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7334 Color: 0
Size: 778 Color: 0
Size: 240 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7363 Color: 1
Size: 709 Color: 0
Size: 280 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7338 Color: 0
Size: 804 Color: 1
Size: 210 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7364 Color: 1
Size: 902 Color: 0
Size: 86 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7395 Color: 1
Size: 701 Color: 0
Size: 256 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7452 Color: 1
Size: 590 Color: 1
Size: 310 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7458 Color: 0
Size: 694 Color: 1
Size: 200 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7484 Color: 0
Size: 580 Color: 0
Size: 288 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 0
Size: 2373 Color: 1
Size: 248 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6060 Color: 0
Size: 2083 Color: 0
Size: 208 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6315 Color: 1
Size: 1882 Color: 0
Size: 154 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 0
Size: 1815 Color: 0
Size: 194 Color: 1

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 0
Size: 1053 Color: 0
Size: 694 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 6743 Color: 0
Size: 1460 Color: 1
Size: 148 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6789 Color: 1
Size: 1442 Color: 1
Size: 120 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 0
Size: 1153 Color: 1
Size: 136 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 7457 Color: 1
Size: 788 Color: 0
Size: 106 Color: 0

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 4177 Color: 1
Size: 4061 Color: 1
Size: 112 Color: 0

Bin 39: 2 of cap free
Amount of items: 5
Items: 
Size: 4180 Color: 1
Size: 2142 Color: 0
Size: 1288 Color: 0
Size: 472 Color: 0
Size: 268 Color: 1

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 4868 Color: 1
Size: 3482 Color: 0

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 5212 Color: 1
Size: 2906 Color: 1
Size: 232 Color: 0

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 5391 Color: 1
Size: 2959 Color: 0

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 1
Size: 2164 Color: 0
Size: 104 Color: 1

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 6164 Color: 1
Size: 2186 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 6175 Color: 1
Size: 1943 Color: 0
Size: 232 Color: 1

Bin 46: 2 of cap free
Amount of items: 4
Items: 
Size: 6330 Color: 0
Size: 1002 Color: 1
Size: 814 Color: 1
Size: 204 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 6650 Color: 0
Size: 954 Color: 1
Size: 746 Color: 0

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 7500 Color: 1
Size: 850 Color: 0

Bin 49: 3 of cap free
Amount of items: 5
Items: 
Size: 4186 Color: 0
Size: 3279 Color: 0
Size: 536 Color: 1
Size: 188 Color: 1
Size: 160 Color: 0

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 0
Size: 949 Color: 1
Size: 576 Color: 0

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 7238 Color: 1
Size: 1111 Color: 0

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 7494 Color: 1
Size: 799 Color: 0
Size: 56 Color: 1

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 7503 Color: 1
Size: 846 Color: 0

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 0
Size: 1686 Color: 0
Size: 202 Color: 1

Bin 55: 4 of cap free
Amount of items: 2
Items: 
Size: 7270 Color: 1
Size: 1078 Color: 0

Bin 56: 5 of cap free
Amount of items: 2
Items: 
Size: 4866 Color: 1
Size: 3481 Color: 0

Bin 57: 5 of cap free
Amount of items: 3
Items: 
Size: 5505 Color: 1
Size: 2690 Color: 0
Size: 152 Color: 0

Bin 58: 6 of cap free
Amount of items: 3
Items: 
Size: 5762 Color: 0
Size: 2404 Color: 1
Size: 180 Color: 1

Bin 59: 6 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 0
Size: 1612 Color: 0
Size: 160 Color: 1

Bin 60: 6 of cap free
Amount of items: 2
Items: 
Size: 7204 Color: 1
Size: 1142 Color: 0

Bin 61: 7 of cap free
Amount of items: 3
Items: 
Size: 7510 Color: 0
Size: 833 Color: 1
Size: 2 Color: 1

Bin 62: 8 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 0
Size: 3656 Color: 1
Size: 348 Color: 1

Bin 63: 8 of cap free
Amount of items: 2
Items: 
Size: 4882 Color: 1
Size: 3462 Color: 0

Bin 64: 8 of cap free
Amount of items: 2
Items: 
Size: 7308 Color: 0
Size: 1036 Color: 1

Bin 65: 9 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 1
Size: 1453 Color: 0
Size: 88 Color: 1

Bin 66: 9 of cap free
Amount of items: 2
Items: 
Size: 7427 Color: 0
Size: 916 Color: 1

Bin 67: 10 of cap free
Amount of items: 18
Items: 
Size: 730 Color: 0
Size: 716 Color: 0
Size: 664 Color: 0
Size: 520 Color: 0
Size: 504 Color: 0
Size: 440 Color: 1
Size: 440 Color: 0
Size: 436 Color: 1
Size: 436 Color: 1
Size: 432 Color: 1
Size: 428 Color: 1
Size: 424 Color: 1
Size: 400 Color: 1
Size: 388 Color: 1
Size: 376 Color: 1
Size: 376 Color: 0
Size: 360 Color: 0
Size: 272 Color: 0

Bin 68: 10 of cap free
Amount of items: 3
Items: 
Size: 5318 Color: 1
Size: 2848 Color: 0
Size: 176 Color: 0

Bin 69: 10 of cap free
Amount of items: 2
Items: 
Size: 7286 Color: 0
Size: 1056 Color: 1

Bin 70: 10 of cap free
Amount of items: 2
Items: 
Size: 7378 Color: 1
Size: 964 Color: 0

Bin 71: 11 of cap free
Amount of items: 2
Items: 
Size: 6969 Color: 0
Size: 1372 Color: 1

Bin 72: 13 of cap free
Amount of items: 2
Items: 
Size: 7466 Color: 1
Size: 873 Color: 0

Bin 73: 13 of cap free
Amount of items: 2
Items: 
Size: 7511 Color: 1
Size: 828 Color: 0

Bin 74: 14 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 1
Size: 1422 Color: 0

Bin 75: 14 of cap free
Amount of items: 2
Items: 
Size: 7396 Color: 1
Size: 942 Color: 0

Bin 76: 14 of cap free
Amount of items: 2
Items: 
Size: 7478 Color: 0
Size: 860 Color: 1

Bin 77: 16 of cap free
Amount of items: 2
Items: 
Size: 7033 Color: 0
Size: 1303 Color: 1

Bin 78: 16 of cap free
Amount of items: 2
Items: 
Size: 7078 Color: 0
Size: 1258 Color: 1

Bin 79: 17 of cap free
Amount of items: 2
Items: 
Size: 4803 Color: 0
Size: 3532 Color: 1

Bin 80: 17 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 0
Size: 1699 Color: 0
Size: 296 Color: 1

Bin 81: 17 of cap free
Amount of items: 2
Items: 
Size: 7110 Color: 0
Size: 1225 Color: 1

Bin 82: 17 of cap free
Amount of items: 2
Items: 
Size: 7305 Color: 0
Size: 1030 Color: 1

Bin 83: 18 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 0
Size: 2894 Color: 1
Size: 692 Color: 1

Bin 84: 18 of cap free
Amount of items: 2
Items: 
Size: 6852 Color: 1
Size: 1482 Color: 0

Bin 85: 19 of cap free
Amount of items: 3
Items: 
Size: 5879 Color: 1
Size: 2162 Color: 0
Size: 292 Color: 0

Bin 86: 19 of cap free
Amount of items: 2
Items: 
Size: 7412 Color: 1
Size: 921 Color: 0

Bin 87: 20 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 0
Size: 1555 Color: 1
Size: 72 Color: 1

Bin 88: 20 of cap free
Amount of items: 2
Items: 
Size: 7442 Color: 0
Size: 890 Color: 1

Bin 89: 21 of cap free
Amount of items: 2
Items: 
Size: 6575 Color: 0
Size: 1756 Color: 1

Bin 90: 21 of cap free
Amount of items: 2
Items: 
Size: 7247 Color: 1
Size: 1084 Color: 0

Bin 91: 22 of cap free
Amount of items: 3
Items: 
Size: 4284 Color: 0
Size: 3358 Color: 0
Size: 688 Color: 1

Bin 92: 22 of cap free
Amount of items: 2
Items: 
Size: 6680 Color: 1
Size: 1650 Color: 0

Bin 93: 22 of cap free
Amount of items: 2
Items: 
Size: 6966 Color: 1
Size: 1364 Color: 0

Bin 94: 23 of cap free
Amount of items: 2
Items: 
Size: 5709 Color: 0
Size: 2620 Color: 1

Bin 95: 23 of cap free
Amount of items: 2
Items: 
Size: 7213 Color: 0
Size: 1116 Color: 1

Bin 96: 24 of cap free
Amount of items: 2
Items: 
Size: 6267 Color: 0
Size: 2061 Color: 1

Bin 97: 24 of cap free
Amount of items: 2
Items: 
Size: 6708 Color: 1
Size: 1620 Color: 0

Bin 98: 25 of cap free
Amount of items: 2
Items: 
Size: 6846 Color: 1
Size: 1481 Color: 0

Bin 99: 25 of cap free
Amount of items: 2
Items: 
Size: 7075 Color: 0
Size: 1252 Color: 1

Bin 100: 27 of cap free
Amount of items: 2
Items: 
Size: 4181 Color: 0
Size: 4144 Color: 1

Bin 101: 28 of cap free
Amount of items: 2
Items: 
Size: 5330 Color: 1
Size: 2994 Color: 0

Bin 102: 28 of cap free
Amount of items: 2
Items: 
Size: 7150 Color: 0
Size: 1174 Color: 1

Bin 103: 29 of cap free
Amount of items: 2
Items: 
Size: 6982 Color: 0
Size: 1341 Color: 1

Bin 104: 29 of cap free
Amount of items: 2
Items: 
Size: 7118 Color: 1
Size: 1205 Color: 0

Bin 105: 32 of cap free
Amount of items: 2
Items: 
Size: 6642 Color: 0
Size: 1678 Color: 1

Bin 106: 34 of cap free
Amount of items: 2
Items: 
Size: 4778 Color: 1
Size: 3540 Color: 0

Bin 107: 34 of cap free
Amount of items: 2
Items: 
Size: 7156 Color: 1
Size: 1162 Color: 0

Bin 108: 37 of cap free
Amount of items: 2
Items: 
Size: 6487 Color: 0
Size: 1828 Color: 1

Bin 109: 40 of cap free
Amount of items: 7
Items: 
Size: 4178 Color: 0
Size: 1136 Color: 0
Size: 778 Color: 0
Size: 756 Color: 0
Size: 528 Color: 1
Size: 496 Color: 1
Size: 440 Color: 1

Bin 110: 40 of cap free
Amount of items: 3
Items: 
Size: 4202 Color: 1
Size: 3348 Color: 0
Size: 762 Color: 1

Bin 111: 40 of cap free
Amount of items: 2
Items: 
Size: 5460 Color: 0
Size: 2852 Color: 1

Bin 112: 40 of cap free
Amount of items: 2
Items: 
Size: 6094 Color: 1
Size: 2218 Color: 0

Bin 113: 42 of cap free
Amount of items: 3
Items: 
Size: 4185 Color: 1
Size: 3104 Color: 0
Size: 1021 Color: 0

Bin 114: 45 of cap free
Amount of items: 2
Items: 
Size: 5694 Color: 1
Size: 2613 Color: 0

Bin 115: 48 of cap free
Amount of items: 2
Items: 
Size: 6724 Color: 0
Size: 1580 Color: 1

Bin 116: 48 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 0
Size: 1164 Color: 1

Bin 117: 54 of cap free
Amount of items: 2
Items: 
Size: 6374 Color: 0
Size: 1924 Color: 1

Bin 118: 54 of cap free
Amount of items: 2
Items: 
Size: 7422 Color: 0
Size: 876 Color: 1

Bin 119: 60 of cap free
Amount of items: 2
Items: 
Size: 6998 Color: 1
Size: 1294 Color: 0

Bin 120: 64 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 0
Size: 1662 Color: 1

Bin 121: 65 of cap free
Amount of items: 2
Items: 
Size: 7129 Color: 0
Size: 1158 Color: 1

Bin 122: 66 of cap free
Amount of items: 2
Items: 
Size: 5756 Color: 1
Size: 2530 Color: 0

Bin 123: 68 of cap free
Amount of items: 2
Items: 
Size: 7222 Color: 1
Size: 1062 Color: 0

Bin 124: 72 of cap free
Amount of items: 2
Items: 
Size: 5786 Color: 0
Size: 2494 Color: 1

Bin 125: 82 of cap free
Amount of items: 2
Items: 
Size: 5362 Color: 0
Size: 2908 Color: 1

Bin 126: 88 of cap free
Amount of items: 3
Items: 
Size: 4194 Color: 1
Size: 3474 Color: 0
Size: 596 Color: 1

Bin 127: 88 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 0
Size: 3476 Color: 0
Size: 600 Color: 1

Bin 128: 100 of cap free
Amount of items: 2
Items: 
Size: 6358 Color: 0
Size: 1894 Color: 1

Bin 129: 116 of cap free
Amount of items: 33
Items: 
Size: 376 Color: 1
Size: 362 Color: 1
Size: 344 Color: 1
Size: 344 Color: 0
Size: 338 Color: 0
Size: 332 Color: 1
Size: 332 Color: 0
Size: 328 Color: 1
Size: 320 Color: 1
Size: 296 Color: 1
Size: 284 Color: 0
Size: 264 Color: 1
Size: 260 Color: 0
Size: 248 Color: 0
Size: 244 Color: 0
Size: 232 Color: 0
Size: 230 Color: 0
Size: 228 Color: 0
Size: 224 Color: 0
Size: 224 Color: 0
Size: 216 Color: 1
Size: 216 Color: 0
Size: 212 Color: 1
Size: 212 Color: 1
Size: 200 Color: 0
Size: 188 Color: 1
Size: 174 Color: 1
Size: 174 Color: 1
Size: 168 Color: 1
Size: 168 Color: 1
Size: 168 Color: 1
Size: 166 Color: 0
Size: 164 Color: 0

Bin 130: 124 of cap free
Amount of items: 2
Items: 
Size: 4762 Color: 0
Size: 3466 Color: 1

Bin 131: 128 of cap free
Amount of items: 2
Items: 
Size: 4747 Color: 1
Size: 3477 Color: 0

Bin 132: 128 of cap free
Amount of items: 2
Items: 
Size: 6021 Color: 0
Size: 2203 Color: 1

Bin 133: 5658 of cap free
Amount of items: 18
Items: 
Size: 168 Color: 1
Size: 160 Color: 0
Size: 160 Color: 0
Size: 160 Color: 0
Size: 158 Color: 1
Size: 152 Color: 1
Size: 152 Color: 1
Size: 148 Color: 1
Size: 148 Color: 1
Size: 148 Color: 0
Size: 144 Color: 1
Size: 144 Color: 1
Size: 144 Color: 1
Size: 144 Color: 0
Size: 144 Color: 0
Size: 140 Color: 0
Size: 140 Color: 0
Size: 140 Color: 0

Total size: 1102464
Total free space: 8352

