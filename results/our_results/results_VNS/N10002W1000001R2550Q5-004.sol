Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3337
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 432406 Color: 1
Size: 298579 Color: 2
Size: 269016 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 374609 Color: 1
Size: 369355 Color: 3
Size: 256037 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 495364 Color: 3
Size: 253656 Color: 3
Size: 250981 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 434243 Color: 3
Size: 295653 Color: 4
Size: 270105 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 427579 Color: 2
Size: 296431 Color: 3
Size: 275991 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 381347 Color: 3
Size: 317678 Color: 0
Size: 300976 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 405932 Color: 4
Size: 332009 Color: 2
Size: 262060 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 433763 Color: 0
Size: 300888 Color: 2
Size: 265350 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 467325 Color: 0
Size: 267051 Color: 1
Size: 265625 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 459488 Color: 4
Size: 271353 Color: 0
Size: 269160 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 462808 Color: 4
Size: 278403 Color: 4
Size: 258790 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 371697 Color: 0
Size: 317959 Color: 4
Size: 310345 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 409188 Color: 1
Size: 328336 Color: 0
Size: 262477 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 410709 Color: 1
Size: 332541 Color: 4
Size: 256751 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 391509 Color: 3
Size: 315582 Color: 2
Size: 292910 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 448627 Color: 1
Size: 281411 Color: 2
Size: 269963 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 361761 Color: 0
Size: 342088 Color: 2
Size: 296152 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 473179 Color: 4
Size: 264222 Color: 4
Size: 262600 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 419739 Color: 2
Size: 295949 Color: 2
Size: 284313 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 361837 Color: 3
Size: 354252 Color: 4
Size: 283912 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 402836 Color: 0
Size: 338880 Color: 1
Size: 258285 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 443834 Color: 3
Size: 284182 Color: 4
Size: 271985 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 386392 Color: 0
Size: 354019 Color: 2
Size: 259590 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 375876 Color: 3
Size: 322287 Color: 3
Size: 301838 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 385004 Color: 3
Size: 358579 Color: 0
Size: 256418 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 388476 Color: 3
Size: 348575 Color: 0
Size: 262950 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 355742 Color: 3
Size: 330121 Color: 3
Size: 314138 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 376299 Color: 0
Size: 359323 Color: 0
Size: 264379 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 392324 Color: 3
Size: 326166 Color: 4
Size: 281511 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 401752 Color: 1
Size: 318738 Color: 3
Size: 279511 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 447860 Color: 1
Size: 279284 Color: 3
Size: 272857 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 346337 Color: 3
Size: 335406 Color: 4
Size: 318258 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 354368 Color: 4
Size: 351063 Color: 1
Size: 294570 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 365798 Color: 1
Size: 320452 Color: 4
Size: 313751 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 497726 Color: 0
Size: 251629 Color: 4
Size: 250646 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 389949 Color: 3
Size: 312942 Color: 4
Size: 297110 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 353282 Color: 2
Size: 350249 Color: 2
Size: 296470 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 379745 Color: 4
Size: 347995 Color: 0
Size: 272261 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 470574 Color: 2
Size: 272530 Color: 1
Size: 256897 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 396226 Color: 2
Size: 343800 Color: 1
Size: 259975 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 452212 Color: 2
Size: 282997 Color: 4
Size: 264792 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 478037 Color: 4
Size: 271867 Color: 0
Size: 250097 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 430325 Color: 3
Size: 309949 Color: 0
Size: 259727 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 424953 Color: 1
Size: 315365 Color: 4
Size: 259683 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 428648 Color: 0
Size: 288217 Color: 3
Size: 283136 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 407532 Color: 4
Size: 339534 Color: 2
Size: 252935 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 358695 Color: 0
Size: 334077 Color: 2
Size: 307229 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 420717 Color: 3
Size: 302973 Color: 1
Size: 276311 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 432988 Color: 3
Size: 316986 Color: 0
Size: 250027 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 362824 Color: 1
Size: 358311 Color: 4
Size: 278866 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 352345 Color: 0
Size: 340763 Color: 0
Size: 306893 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 477065 Color: 2
Size: 265376 Color: 3
Size: 257560 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 475442 Color: 4
Size: 270182 Color: 1
Size: 254377 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 343494 Color: 0
Size: 328708 Color: 3
Size: 327799 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 388359 Color: 4
Size: 307436 Color: 3
Size: 304206 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 403463 Color: 4
Size: 298481 Color: 2
Size: 298057 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 416412 Color: 0
Size: 309771 Color: 4
Size: 273818 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 376517 Color: 0
Size: 369367 Color: 1
Size: 254117 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 354332 Color: 0
Size: 353174 Color: 1
Size: 292495 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 387323 Color: 0
Size: 342018 Color: 0
Size: 270660 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 463854 Color: 0
Size: 279125 Color: 1
Size: 257022 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 394181 Color: 3
Size: 340866 Color: 1
Size: 264954 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 482095 Color: 2
Size: 262291 Color: 3
Size: 255615 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 382030 Color: 2
Size: 350306 Color: 2
Size: 267665 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 411739 Color: 4
Size: 295157 Color: 4
Size: 293105 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 405317 Color: 3
Size: 299383 Color: 3
Size: 295301 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 377321 Color: 4
Size: 352971 Color: 4
Size: 269709 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 445829 Color: 4
Size: 300662 Color: 1
Size: 253510 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 376710 Color: 4
Size: 366043 Color: 4
Size: 257248 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 493991 Color: 2
Size: 255969 Color: 1
Size: 250041 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 493358 Color: 4
Size: 254975 Color: 4
Size: 251668 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 379987 Color: 3
Size: 345349 Color: 3
Size: 274665 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 348868 Color: 1
Size: 346440 Color: 2
Size: 304693 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 479962 Color: 4
Size: 266018 Color: 2
Size: 254021 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 459199 Color: 2
Size: 280263 Color: 3
Size: 260539 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 435373 Color: 2
Size: 291620 Color: 4
Size: 273008 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 433489 Color: 3
Size: 313193 Color: 2
Size: 253319 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 470781 Color: 3
Size: 267628 Color: 2
Size: 261592 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 461303 Color: 3
Size: 276421 Color: 3
Size: 262277 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 420123 Color: 1
Size: 324311 Color: 4
Size: 255567 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 397507 Color: 0
Size: 325743 Color: 0
Size: 276751 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 404242 Color: 1
Size: 300365 Color: 4
Size: 295394 Color: 3

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 405381 Color: 1
Size: 323159 Color: 3
Size: 271461 Color: 2

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 379187 Color: 4
Size: 314448 Color: 2
Size: 306366 Color: 3

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 460556 Color: 1
Size: 284463 Color: 4
Size: 254982 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 389345 Color: 0
Size: 351003 Color: 2
Size: 259653 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 353604 Color: 4
Size: 346961 Color: 0
Size: 299436 Color: 3

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 496130 Color: 3
Size: 252600 Color: 1
Size: 251271 Color: 4

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 438937 Color: 2
Size: 290157 Color: 4
Size: 270907 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 439299 Color: 0
Size: 294951 Color: 2
Size: 265751 Color: 2

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 423206 Color: 0
Size: 291232 Color: 1
Size: 285563 Color: 2

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 411106 Color: 0
Size: 311915 Color: 1
Size: 276980 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 385942 Color: 0
Size: 355663 Color: 3
Size: 258396 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 482321 Color: 3
Size: 264991 Color: 1
Size: 252689 Color: 2

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 443529 Color: 0
Size: 291102 Color: 4
Size: 265370 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 366850 Color: 2
Size: 318201 Color: 3
Size: 314950 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 394377 Color: 3
Size: 351894 Color: 4
Size: 253730 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 416815 Color: 0
Size: 298036 Color: 4
Size: 285150 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 367753 Color: 0
Size: 319182 Color: 2
Size: 313066 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 375333 Color: 1
Size: 364822 Color: 2
Size: 259846 Color: 4

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 458804 Color: 2
Size: 275222 Color: 0
Size: 265975 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 402344 Color: 3
Size: 330262 Color: 4
Size: 267395 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 391012 Color: 1
Size: 315197 Color: 3
Size: 293792 Color: 4

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 417430 Color: 1
Size: 291587 Color: 2
Size: 290984 Color: 4

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 447251 Color: 1
Size: 283485 Color: 0
Size: 269265 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 476347 Color: 4
Size: 269755 Color: 3
Size: 253899 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 456617 Color: 2
Size: 276979 Color: 1
Size: 266405 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 412367 Color: 1
Size: 329675 Color: 2
Size: 257959 Color: 2

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 406595 Color: 3
Size: 327623 Color: 1
Size: 265783 Color: 3

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 432533 Color: 3
Size: 286187 Color: 4
Size: 281281 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 381156 Color: 3
Size: 347189 Color: 4
Size: 271656 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 419806 Color: 2
Size: 308557 Color: 0
Size: 271638 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 372342 Color: 2
Size: 328866 Color: 4
Size: 298793 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 441757 Color: 3
Size: 293431 Color: 4
Size: 264813 Color: 2

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 424489 Color: 1
Size: 306550 Color: 0
Size: 268962 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 372198 Color: 1
Size: 354383 Color: 3
Size: 273420 Color: 2

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 373552 Color: 3
Size: 358918 Color: 2
Size: 267531 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 487337 Color: 0
Size: 261442 Color: 1
Size: 251222 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 436205 Color: 1
Size: 312863 Color: 4
Size: 250933 Color: 3

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 382536 Color: 0
Size: 315163 Color: 4
Size: 302302 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 457891 Color: 4
Size: 290993 Color: 0
Size: 251117 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 437749 Color: 2
Size: 291341 Color: 3
Size: 270911 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 412839 Color: 0
Size: 310208 Color: 1
Size: 276954 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 383474 Color: 1
Size: 329158 Color: 2
Size: 287369 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 397131 Color: 1
Size: 327068 Color: 0
Size: 275802 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 453357 Color: 3
Size: 285791 Color: 4
Size: 260853 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 364950 Color: 0
Size: 351905 Color: 1
Size: 283146 Color: 3

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 410344 Color: 1
Size: 329668 Color: 3
Size: 259989 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 497943 Color: 0
Size: 251915 Color: 3
Size: 250143 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 394306 Color: 4
Size: 325613 Color: 3
Size: 280082 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 436587 Color: 0
Size: 310016 Color: 1
Size: 253398 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 406762 Color: 3
Size: 334633 Color: 0
Size: 258606 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 365914 Color: 3
Size: 336436 Color: 1
Size: 297651 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 382997 Color: 3
Size: 309283 Color: 2
Size: 307721 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 370729 Color: 2
Size: 367193 Color: 0
Size: 262079 Color: 3

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 420745 Color: 3
Size: 303327 Color: 4
Size: 275929 Color: 3

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 449141 Color: 3
Size: 282689 Color: 0
Size: 268171 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 435801 Color: 2
Size: 312572 Color: 1
Size: 251628 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 390554 Color: 0
Size: 307310 Color: 0
Size: 302137 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 417448 Color: 3
Size: 319871 Color: 4
Size: 262682 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 458615 Color: 4
Size: 286174 Color: 4
Size: 255212 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 394683 Color: 4
Size: 305018 Color: 0
Size: 300300 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 496282 Color: 4
Size: 252183 Color: 0
Size: 251536 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 404306 Color: 2
Size: 325045 Color: 1
Size: 270650 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 382847 Color: 0
Size: 331116 Color: 1
Size: 286038 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 366053 Color: 4
Size: 349351 Color: 3
Size: 284597 Color: 4

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 454857 Color: 1
Size: 275894 Color: 2
Size: 269250 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 394155 Color: 1
Size: 341582 Color: 0
Size: 264264 Color: 3

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 483798 Color: 1
Size: 259363 Color: 4
Size: 256840 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 418736 Color: 0
Size: 314578 Color: 3
Size: 266687 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 382885 Color: 0
Size: 363899 Color: 3
Size: 253217 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 382051 Color: 1
Size: 356834 Color: 3
Size: 261116 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 395987 Color: 3
Size: 305068 Color: 2
Size: 298946 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 449699 Color: 2
Size: 281170 Color: 0
Size: 269132 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 414709 Color: 2
Size: 306330 Color: 4
Size: 278962 Color: 3

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 475840 Color: 3
Size: 272136 Color: 0
Size: 252025 Color: 3

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 443965 Color: 2
Size: 280974 Color: 3
Size: 275062 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 375355 Color: 1
Size: 348412 Color: 4
Size: 276234 Color: 2

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 431300 Color: 3
Size: 302008 Color: 2
Size: 266693 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 366287 Color: 2
Size: 332114 Color: 3
Size: 301600 Color: 2

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 489239 Color: 4
Size: 256762 Color: 3
Size: 254000 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 427845 Color: 3
Size: 304550 Color: 2
Size: 267606 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 393153 Color: 0
Size: 327386 Color: 3
Size: 279462 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 431544 Color: 4
Size: 298243 Color: 0
Size: 270214 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 369244 Color: 3
Size: 322043 Color: 1
Size: 308714 Color: 4

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 388394 Color: 2
Size: 309058 Color: 4
Size: 302549 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 424397 Color: 2
Size: 303832 Color: 0
Size: 271772 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 497966 Color: 1
Size: 251181 Color: 2
Size: 250854 Color: 3

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 444546 Color: 1
Size: 277815 Color: 3
Size: 277640 Color: 2

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 394439 Color: 0
Size: 335455 Color: 2
Size: 270107 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 476775 Color: 3
Size: 261881 Color: 0
Size: 261345 Color: 4

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 403072 Color: 2
Size: 307712 Color: 1
Size: 289217 Color: 3

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 429791 Color: 3
Size: 317814 Color: 4
Size: 252396 Color: 4

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 387227 Color: 0
Size: 355659 Color: 1
Size: 257115 Color: 2

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 367155 Color: 0
Size: 345828 Color: 4
Size: 287018 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 338399 Color: 4
Size: 338246 Color: 1
Size: 323356 Color: 4

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 423699 Color: 2
Size: 322900 Color: 4
Size: 253402 Color: 4

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 448665 Color: 0
Size: 300691 Color: 2
Size: 250645 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 373176 Color: 4
Size: 319049 Color: 2
Size: 307776 Color: 1

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 380965 Color: 4
Size: 331565 Color: 1
Size: 287471 Color: 3

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 365374 Color: 3
Size: 358601 Color: 1
Size: 276026 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 479695 Color: 2
Size: 269242 Color: 1
Size: 251064 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 378719 Color: 4
Size: 315212 Color: 1
Size: 306070 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 374872 Color: 0
Size: 332966 Color: 3
Size: 292163 Color: 4

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 475046 Color: 3
Size: 274504 Color: 2
Size: 250451 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 409058 Color: 1
Size: 314378 Color: 3
Size: 276565 Color: 3

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 372102 Color: 1
Size: 359037 Color: 3
Size: 268862 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 374030 Color: 1
Size: 333902 Color: 2
Size: 292069 Color: 2

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 439994 Color: 2
Size: 280489 Color: 2
Size: 279518 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 496185 Color: 1
Size: 253376 Color: 3
Size: 250440 Color: 3

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 373049 Color: 0
Size: 366937 Color: 4
Size: 260015 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 363072 Color: 3
Size: 362253 Color: 4
Size: 274676 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 470808 Color: 1
Size: 276192 Color: 4
Size: 253001 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 460119 Color: 1
Size: 275626 Color: 2
Size: 264256 Color: 3

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 363594 Color: 1
Size: 318461 Color: 3
Size: 317946 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 394236 Color: 2
Size: 349245 Color: 1
Size: 256520 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 411695 Color: 3
Size: 326721 Color: 4
Size: 261585 Color: 4

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 367991 Color: 0
Size: 341119 Color: 4
Size: 290891 Color: 2

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 443268 Color: 4
Size: 297196 Color: 3
Size: 259537 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 442252 Color: 2
Size: 294484 Color: 1
Size: 263265 Color: 4

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 497123 Color: 3
Size: 251753 Color: 1
Size: 251125 Color: 2

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 484135 Color: 2
Size: 265238 Color: 1
Size: 250628 Color: 2

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 376546 Color: 2
Size: 361013 Color: 4
Size: 262442 Color: 4

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 396928 Color: 0
Size: 333866 Color: 4
Size: 269207 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 375604 Color: 0
Size: 326722 Color: 1
Size: 297675 Color: 3

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 458714 Color: 0
Size: 276270 Color: 2
Size: 265017 Color: 4

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 392436 Color: 2
Size: 348537 Color: 1
Size: 259028 Color: 3

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 421178 Color: 3
Size: 295973 Color: 0
Size: 282850 Color: 1

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 484695 Color: 0
Size: 263000 Color: 3
Size: 252306 Color: 4

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 449748 Color: 3
Size: 283236 Color: 2
Size: 267017 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 481958 Color: 4
Size: 263351 Color: 3
Size: 254692 Color: 3

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 496106 Color: 3
Size: 252422 Color: 4
Size: 251473 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 461322 Color: 3
Size: 287536 Color: 0
Size: 251143 Color: 3

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 427054 Color: 1
Size: 316699 Color: 4
Size: 256248 Color: 4

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 372517 Color: 0
Size: 350021 Color: 2
Size: 277463 Color: 4

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 399228 Color: 2
Size: 304339 Color: 1
Size: 296434 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 383958 Color: 4
Size: 346598 Color: 3
Size: 269445 Color: 4

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 432805 Color: 4
Size: 305664 Color: 3
Size: 261532 Color: 2

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 426804 Color: 2
Size: 317571 Color: 0
Size: 255626 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 399724 Color: 1
Size: 322807 Color: 3
Size: 277470 Color: 3

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 490716 Color: 0
Size: 257701 Color: 4
Size: 251584 Color: 4

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 383962 Color: 1
Size: 319245 Color: 3
Size: 296794 Color: 3

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 427761 Color: 3
Size: 311909 Color: 1
Size: 260331 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 429126 Color: 4
Size: 288987 Color: 2
Size: 281888 Color: 1

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 452415 Color: 2
Size: 277235 Color: 0
Size: 270351 Color: 3

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 408840 Color: 2
Size: 320128 Color: 4
Size: 271033 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 490360 Color: 2
Size: 259496 Color: 1
Size: 250145 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 472339 Color: 2
Size: 276960 Color: 2
Size: 250702 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 402930 Color: 1
Size: 323184 Color: 2
Size: 273887 Color: 4

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 436400 Color: 2
Size: 296529 Color: 1
Size: 267072 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 471462 Color: 4
Size: 267573 Color: 0
Size: 260966 Color: 3

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 358140 Color: 2
Size: 326250 Color: 1
Size: 315611 Color: 4

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 478512 Color: 0
Size: 262811 Color: 1
Size: 258678 Color: 4

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 484078 Color: 4
Size: 263629 Color: 1
Size: 252294 Color: 2

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 378785 Color: 4
Size: 367949 Color: 0
Size: 253267 Color: 3

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 374509 Color: 1
Size: 345217 Color: 4
Size: 280275 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 405863 Color: 3
Size: 325307 Color: 1
Size: 268831 Color: 4

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 443511 Color: 4
Size: 280498 Color: 4
Size: 275992 Color: 2

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 431734 Color: 4
Size: 298361 Color: 0
Size: 269906 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 472671 Color: 3
Size: 277144 Color: 4
Size: 250186 Color: 3

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 373020 Color: 4
Size: 355101 Color: 1
Size: 271880 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 478164 Color: 1
Size: 270569 Color: 3
Size: 251268 Color: 2

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 448418 Color: 2
Size: 297657 Color: 1
Size: 253926 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 372386 Color: 1
Size: 357899 Color: 0
Size: 269716 Color: 2

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 487921 Color: 0
Size: 260043 Color: 4
Size: 252037 Color: 4

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 441435 Color: 1
Size: 298601 Color: 3
Size: 259965 Color: 4

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 382660 Color: 1
Size: 319809 Color: 0
Size: 297532 Color: 2

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 404770 Color: 4
Size: 340479 Color: 1
Size: 254752 Color: 3

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 353646 Color: 0
Size: 346566 Color: 0
Size: 299789 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 435561 Color: 4
Size: 294011 Color: 3
Size: 270429 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 411755 Color: 1
Size: 295320 Color: 4
Size: 292926 Color: 4

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 388291 Color: 3
Size: 308485 Color: 3
Size: 303225 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 417789 Color: 4
Size: 292757 Color: 4
Size: 289455 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 393369 Color: 4
Size: 336527 Color: 2
Size: 270105 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 365906 Color: 1
Size: 358710 Color: 0
Size: 275385 Color: 4

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 396643 Color: 1
Size: 309231 Color: 0
Size: 294127 Color: 2

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 387039 Color: 0
Size: 359723 Color: 1
Size: 253239 Color: 2

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 443188 Color: 1
Size: 283575 Color: 3
Size: 273238 Color: 3

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 450644 Color: 4
Size: 282886 Color: 1
Size: 266471 Color: 2

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 358033 Color: 3
Size: 336644 Color: 1
Size: 305324 Color: 2

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 381372 Color: 0
Size: 343724 Color: 1
Size: 274905 Color: 4

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 486635 Color: 1
Size: 257589 Color: 3
Size: 255777 Color: 4

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 386086 Color: 1
Size: 315772 Color: 2
Size: 298143 Color: 4

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 416769 Color: 0
Size: 325567 Color: 4
Size: 257665 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 439941 Color: 1
Size: 302708 Color: 2
Size: 257352 Color: 4

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 484052 Color: 4
Size: 260873 Color: 1
Size: 255076 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 456774 Color: 3
Size: 279709 Color: 0
Size: 263518 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 385164 Color: 2
Size: 317408 Color: 0
Size: 297429 Color: 3

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 357935 Color: 4
Size: 328049 Color: 2
Size: 314017 Color: 4

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 416432 Color: 3
Size: 292017 Color: 4
Size: 291552 Color: 4

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 400201 Color: 2
Size: 348934 Color: 3
Size: 250866 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 454772 Color: 2
Size: 290860 Color: 3
Size: 254369 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 413254 Color: 1
Size: 293942 Color: 2
Size: 292805 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 452674 Color: 1
Size: 290209 Color: 2
Size: 257118 Color: 3

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 472471 Color: 3
Size: 263801 Color: 1
Size: 263729 Color: 4

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 450182 Color: 3
Size: 291625 Color: 2
Size: 258194 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 379253 Color: 3
Size: 329162 Color: 2
Size: 291586 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 376385 Color: 1
Size: 340797 Color: 2
Size: 282819 Color: 2

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 406521 Color: 0
Size: 320343 Color: 3
Size: 273137 Color: 4

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 459859 Color: 0
Size: 286877 Color: 1
Size: 253265 Color: 3

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 423428 Color: 0
Size: 313461 Color: 1
Size: 263112 Color: 4

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 479122 Color: 3
Size: 269426 Color: 4
Size: 251453 Color: 3

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 349200 Color: 0
Size: 330800 Color: 3
Size: 320001 Color: 3

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 369538 Color: 4
Size: 324747 Color: 1
Size: 305716 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 335772 Color: 4
Size: 335201 Color: 4
Size: 329028 Color: 3

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 374462 Color: 2
Size: 322674 Color: 1
Size: 302865 Color: 3

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 393735 Color: 3
Size: 312502 Color: 0
Size: 293764 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 375548 Color: 0
Size: 314442 Color: 4
Size: 310011 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 372616 Color: 4
Size: 334728 Color: 2
Size: 292657 Color: 3

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 415217 Color: 2
Size: 313093 Color: 4
Size: 271691 Color: 4

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 403181 Color: 0
Size: 333778 Color: 0
Size: 263042 Color: 1

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 429548 Color: 2
Size: 291663 Color: 1
Size: 278790 Color: 2

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 379756 Color: 4
Size: 330395 Color: 2
Size: 289850 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 419139 Color: 0
Size: 294837 Color: 3
Size: 286025 Color: 4

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 473005 Color: 1
Size: 273675 Color: 4
Size: 253321 Color: 3

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 482879 Color: 1
Size: 258631 Color: 2
Size: 258491 Color: 2

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 385456 Color: 2
Size: 363515 Color: 1
Size: 251030 Color: 3

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 494860 Color: 4
Size: 255017 Color: 3
Size: 250124 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 476833 Color: 2
Size: 272893 Color: 0
Size: 250275 Color: 3

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 393764 Color: 1
Size: 342513 Color: 0
Size: 263724 Color: 2

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 440713 Color: 1
Size: 305201 Color: 4
Size: 254087 Color: 3

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 382977 Color: 0
Size: 354557 Color: 1
Size: 262467 Color: 3

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 422445 Color: 3
Size: 312506 Color: 4
Size: 265050 Color: 2

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 427962 Color: 3
Size: 302608 Color: 0
Size: 269431 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 422327 Color: 2
Size: 294859 Color: 0
Size: 282815 Color: 3

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 460630 Color: 0
Size: 280939 Color: 4
Size: 258432 Color: 4

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 430332 Color: 0
Size: 302209 Color: 4
Size: 267460 Color: 3

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 492369 Color: 0
Size: 253883 Color: 1
Size: 253749 Color: 2

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 410982 Color: 3
Size: 319253 Color: 2
Size: 269766 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 438052 Color: 4
Size: 296772 Color: 2
Size: 265177 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 471228 Color: 3
Size: 274011 Color: 2
Size: 254762 Color: 4

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 334933 Color: 2
Size: 334058 Color: 4
Size: 331010 Color: 1

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 486445 Color: 4
Size: 262055 Color: 3
Size: 251501 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 370042 Color: 2
Size: 341046 Color: 1
Size: 288913 Color: 4

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 406224 Color: 4
Size: 322356 Color: 1
Size: 271421 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 356354 Color: 0
Size: 337083 Color: 0
Size: 306564 Color: 1

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 370968 Color: 1
Size: 355407 Color: 2
Size: 273626 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 377233 Color: 0
Size: 360674 Color: 3
Size: 262094 Color: 4

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 484434 Color: 2
Size: 263730 Color: 3
Size: 251837 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 492111 Color: 0
Size: 255785 Color: 3
Size: 252105 Color: 2

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 463163 Color: 3
Size: 275295 Color: 0
Size: 261543 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 466135 Color: 2
Size: 277108 Color: 4
Size: 256758 Color: 4

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 449119 Color: 0
Size: 287040 Color: 1
Size: 263842 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 383389 Color: 0
Size: 364323 Color: 4
Size: 252289 Color: 3

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 447047 Color: 2
Size: 294670 Color: 0
Size: 258284 Color: 4

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 422035 Color: 1
Size: 322339 Color: 2
Size: 255627 Color: 3

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 454008 Color: 3
Size: 273423 Color: 1
Size: 272570 Color: 2

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 402037 Color: 1
Size: 315188 Color: 2
Size: 282776 Color: 4

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 483630 Color: 0
Size: 260945 Color: 4
Size: 255426 Color: 4

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 380927 Color: 1
Size: 310782 Color: 2
Size: 308292 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 465405 Color: 2
Size: 281638 Color: 4
Size: 252958 Color: 2

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 436504 Color: 0
Size: 284548 Color: 1
Size: 278949 Color: 2

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 435628 Color: 0
Size: 312139 Color: 2
Size: 252234 Color: 3

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 363622 Color: 1
Size: 322506 Color: 2
Size: 313873 Color: 2

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 490306 Color: 1
Size: 255563 Color: 4
Size: 254132 Color: 3

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 406738 Color: 1
Size: 297458 Color: 3
Size: 295805 Color: 3

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 382378 Color: 3
Size: 356387 Color: 4
Size: 261236 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 473167 Color: 3
Size: 268052 Color: 0
Size: 258782 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 415508 Color: 4
Size: 292930 Color: 3
Size: 291563 Color: 3

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 425966 Color: 3
Size: 316915 Color: 1
Size: 257120 Color: 4

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 420341 Color: 3
Size: 314100 Color: 4
Size: 265560 Color: 1

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 402257 Color: 1
Size: 307222 Color: 3
Size: 290522 Color: 4

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 375995 Color: 0
Size: 372491 Color: 1
Size: 251515 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 441714 Color: 4
Size: 279425 Color: 0
Size: 278862 Color: 1

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 344847 Color: 0
Size: 340261 Color: 2
Size: 314893 Color: 1

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 365594 Color: 1
Size: 324572 Color: 3
Size: 309835 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 440918 Color: 3
Size: 292902 Color: 4
Size: 266181 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 407167 Color: 1
Size: 330745 Color: 4
Size: 262089 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 390814 Color: 0
Size: 316215 Color: 1
Size: 292972 Color: 4

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 473627 Color: 1
Size: 274919 Color: 3
Size: 251455 Color: 2

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 484976 Color: 0
Size: 258161 Color: 3
Size: 256864 Color: 2

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 373960 Color: 0
Size: 313947 Color: 0
Size: 312094 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 454879 Color: 0
Size: 281381 Color: 2
Size: 263741 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 412969 Color: 3
Size: 322217 Color: 4
Size: 264815 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 372089 Color: 2
Size: 316850 Color: 4
Size: 311062 Color: 2

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 404748 Color: 1
Size: 304026 Color: 3
Size: 291227 Color: 3

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 449826 Color: 3
Size: 291871 Color: 0
Size: 258304 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 457670 Color: 0
Size: 274202 Color: 2
Size: 268129 Color: 2

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 481522 Color: 0
Size: 266231 Color: 1
Size: 252248 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 442803 Color: 4
Size: 280084 Color: 2
Size: 277114 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 484691 Color: 3
Size: 263243 Color: 2
Size: 252067 Color: 1

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 468168 Color: 4
Size: 274016 Color: 3
Size: 257817 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 456375 Color: 1
Size: 290915 Color: 4
Size: 252711 Color: 3

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 470495 Color: 1
Size: 277410 Color: 0
Size: 252096 Color: 4

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 400986 Color: 2
Size: 323390 Color: 1
Size: 275625 Color: 4

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 434576 Color: 0
Size: 302620 Color: 4
Size: 262805 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 442565 Color: 2
Size: 296050 Color: 0
Size: 261386 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 440847 Color: 0
Size: 288873 Color: 1
Size: 270281 Color: 4

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 491994 Color: 0
Size: 254731 Color: 0
Size: 253276 Color: 3

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 351917 Color: 1
Size: 344632 Color: 3
Size: 303452 Color: 1

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 478724 Color: 1
Size: 262738 Color: 2
Size: 258539 Color: 4

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 495545 Color: 2
Size: 252535 Color: 4
Size: 251921 Color: 1

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 450354 Color: 3
Size: 290668 Color: 0
Size: 258979 Color: 2

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 387541 Color: 2
Size: 335400 Color: 1
Size: 277060 Color: 4

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 392589 Color: 3
Size: 353763 Color: 1
Size: 253649 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 491882 Color: 4
Size: 256465 Color: 2
Size: 251654 Color: 1

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 410932 Color: 3
Size: 301257 Color: 4
Size: 287812 Color: 1

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 407778 Color: 2
Size: 324694 Color: 1
Size: 267529 Color: 4

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 406209 Color: 4
Size: 329907 Color: 2
Size: 263885 Color: 2

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 365100 Color: 1
Size: 335217 Color: 4
Size: 299684 Color: 3

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 479881 Color: 3
Size: 265479 Color: 2
Size: 254641 Color: 1

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 449462 Color: 2
Size: 295450 Color: 4
Size: 255089 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 454354 Color: 0
Size: 281289 Color: 1
Size: 264358 Color: 2

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 447790 Color: 3
Size: 287023 Color: 3
Size: 265188 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 376375 Color: 1
Size: 344160 Color: 4
Size: 279466 Color: 4

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 463324 Color: 0
Size: 270345 Color: 1
Size: 266332 Color: 2

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 392496 Color: 4
Size: 327173 Color: 0
Size: 280332 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 459583 Color: 4
Size: 285563 Color: 0
Size: 254855 Color: 1

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 475291 Color: 2
Size: 271222 Color: 3
Size: 253488 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 429775 Color: 1
Size: 319498 Color: 3
Size: 250728 Color: 2

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 397414 Color: 0
Size: 329798 Color: 2
Size: 272789 Color: 4

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 450185 Color: 0
Size: 289523 Color: 4
Size: 260293 Color: 4

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 485289 Color: 2
Size: 263981 Color: 3
Size: 250731 Color: 4

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 400820 Color: 4
Size: 301606 Color: 1
Size: 297575 Color: 4

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 401902 Color: 3
Size: 334528 Color: 4
Size: 263571 Color: 1

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 436593 Color: 2
Size: 284593 Color: 3
Size: 278815 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 404729 Color: 2
Size: 308297 Color: 3
Size: 286975 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 442057 Color: 4
Size: 279735 Color: 3
Size: 278209 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 498212 Color: 2
Size: 251132 Color: 0
Size: 250657 Color: 4

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 487354 Color: 0
Size: 259833 Color: 2
Size: 252814 Color: 3

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 402807 Color: 2
Size: 315483 Color: 0
Size: 281711 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 494953 Color: 4
Size: 252610 Color: 0
Size: 252438 Color: 4

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 484768 Color: 4
Size: 259082 Color: 2
Size: 256151 Color: 1

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 388523 Color: 2
Size: 311070 Color: 3
Size: 300408 Color: 3

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 449990 Color: 3
Size: 282024 Color: 1
Size: 267987 Color: 4

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 419440 Color: 2
Size: 307030 Color: 1
Size: 273531 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 370698 Color: 4
Size: 346811 Color: 0
Size: 282492 Color: 3

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 437550 Color: 2
Size: 304367 Color: 3
Size: 258084 Color: 4

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 448291 Color: 4
Size: 277346 Color: 2
Size: 274364 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 401795 Color: 1
Size: 300923 Color: 0
Size: 297283 Color: 2

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 437547 Color: 2
Size: 286245 Color: 1
Size: 276209 Color: 0

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 402872 Color: 0
Size: 327012 Color: 4
Size: 270117 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 340034 Color: 4
Size: 339318 Color: 4
Size: 320649 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 481087 Color: 1
Size: 260636 Color: 2
Size: 258278 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 474464 Color: 3
Size: 267419 Color: 2
Size: 258118 Color: 1

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 484908 Color: 0
Size: 263032 Color: 2
Size: 252061 Color: 1

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 375411 Color: 1
Size: 324516 Color: 3
Size: 300074 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 430856 Color: 0
Size: 305122 Color: 3
Size: 264023 Color: 0

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 444237 Color: 3
Size: 278028 Color: 1
Size: 277736 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 370298 Color: 3
Size: 342586 Color: 1
Size: 287117 Color: 2

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 369122 Color: 2
Size: 317412 Color: 1
Size: 313467 Color: 3

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 470116 Color: 0
Size: 272117 Color: 1
Size: 257768 Color: 4

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 446945 Color: 2
Size: 282195 Color: 3
Size: 270861 Color: 1

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 471798 Color: 2
Size: 270574 Color: 3
Size: 257629 Color: 1

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 375992 Color: 0
Size: 360819 Color: 3
Size: 263190 Color: 0

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 461861 Color: 3
Size: 287127 Color: 4
Size: 251013 Color: 2

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 378138 Color: 0
Size: 329011 Color: 2
Size: 292852 Color: 1

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 454809 Color: 1
Size: 290455 Color: 3
Size: 254737 Color: 2

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 425435 Color: 1
Size: 322966 Color: 0
Size: 251600 Color: 3

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 467668 Color: 2
Size: 280327 Color: 2
Size: 252006 Color: 1

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 457310 Color: 1
Size: 279187 Color: 2
Size: 263504 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 368892 Color: 2
Size: 343374 Color: 1
Size: 287735 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 495380 Color: 2
Size: 254594 Color: 3
Size: 250027 Color: 2

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 477096 Color: 0
Size: 267172 Color: 1
Size: 255733 Color: 3

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 433937 Color: 3
Size: 284779 Color: 1
Size: 281285 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 386772 Color: 4
Size: 359275 Color: 3
Size: 253954 Color: 2

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 444642 Color: 1
Size: 297013 Color: 4
Size: 258346 Color: 3

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 382201 Color: 3
Size: 363277 Color: 1
Size: 254523 Color: 0

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 357952 Color: 2
Size: 322233 Color: 2
Size: 319816 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 418485 Color: 1
Size: 306156 Color: 0
Size: 275360 Color: 3

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 419457 Color: 2
Size: 299573 Color: 2
Size: 280971 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 387622 Color: 0
Size: 307645 Color: 2
Size: 304734 Color: 3

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 432298 Color: 0
Size: 295327 Color: 2
Size: 272376 Color: 1

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 444515 Color: 0
Size: 298078 Color: 3
Size: 257408 Color: 2

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 443478 Color: 4
Size: 301569 Color: 1
Size: 254954 Color: 3

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 371600 Color: 4
Size: 368123 Color: 4
Size: 260278 Color: 2

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 391285 Color: 0
Size: 337343 Color: 0
Size: 271373 Color: 3

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 350968 Color: 0
Size: 339565 Color: 2
Size: 309468 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 370240 Color: 3
Size: 335270 Color: 2
Size: 294491 Color: 3

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 396517 Color: 0
Size: 337801 Color: 2
Size: 265683 Color: 2

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 475562 Color: 2
Size: 269991 Color: 3
Size: 254448 Color: 1

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 452581 Color: 1
Size: 292431 Color: 4
Size: 254989 Color: 0

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 420954 Color: 4
Size: 294096 Color: 2
Size: 284951 Color: 3

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 389489 Color: 4
Size: 353263 Color: 0
Size: 257249 Color: 2

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 397668 Color: 4
Size: 324702 Color: 3
Size: 277631 Color: 2

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 373901 Color: 4
Size: 329861 Color: 0
Size: 296239 Color: 1

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 431579 Color: 2
Size: 291278 Color: 4
Size: 277144 Color: 3

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 447200 Color: 4
Size: 291172 Color: 2
Size: 261629 Color: 4

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 361481 Color: 2
Size: 357728 Color: 0
Size: 280792 Color: 3

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 427116 Color: 2
Size: 303762 Color: 4
Size: 269123 Color: 1

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 465896 Color: 0
Size: 274848 Color: 2
Size: 259257 Color: 4

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 390228 Color: 1
Size: 323437 Color: 4
Size: 286336 Color: 4

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 380583 Color: 4
Size: 357611 Color: 0
Size: 261807 Color: 3

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 447910 Color: 2
Size: 300040 Color: 1
Size: 252051 Color: 3

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 360584 Color: 1
Size: 321326 Color: 4
Size: 318091 Color: 4

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 386691 Color: 1
Size: 360466 Color: 2
Size: 252844 Color: 4

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 381050 Color: 0
Size: 343798 Color: 3
Size: 275153 Color: 1

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 374190 Color: 3
Size: 361414 Color: 0
Size: 264397 Color: 4

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 419548 Color: 2
Size: 325772 Color: 4
Size: 254681 Color: 2

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 361533 Color: 2
Size: 330822 Color: 1
Size: 307646 Color: 2

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 427972 Color: 4
Size: 314758 Color: 1
Size: 257271 Color: 4

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 384978 Color: 3
Size: 324073 Color: 4
Size: 290950 Color: 1

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 432588 Color: 3
Size: 292607 Color: 1
Size: 274806 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 474618 Color: 4
Size: 271250 Color: 2
Size: 254133 Color: 1

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 415335 Color: 1
Size: 321958 Color: 3
Size: 262708 Color: 0

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 412350 Color: 0
Size: 327898 Color: 1
Size: 259753 Color: 4

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 485366 Color: 1
Size: 259923 Color: 3
Size: 254712 Color: 4

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 392033 Color: 4
Size: 320523 Color: 2
Size: 287445 Color: 3

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 450872 Color: 1
Size: 292763 Color: 3
Size: 256366 Color: 4

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 411760 Color: 3
Size: 326941 Color: 1
Size: 261300 Color: 4

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 377100 Color: 2
Size: 347818 Color: 1
Size: 275083 Color: 3

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 399567 Color: 1
Size: 341488 Color: 0
Size: 258946 Color: 3

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 445371 Color: 4
Size: 291535 Color: 2
Size: 263095 Color: 3

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 403637 Color: 1
Size: 329663 Color: 2
Size: 266701 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 396724 Color: 4
Size: 303577 Color: 3
Size: 299700 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 403762 Color: 3
Size: 331781 Color: 0
Size: 264458 Color: 4

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 414848 Color: 0
Size: 324111 Color: 0
Size: 261042 Color: 2

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 405525 Color: 4
Size: 337737 Color: 3
Size: 256739 Color: 2

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 409402 Color: 0
Size: 337077 Color: 3
Size: 253522 Color: 3

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 357369 Color: 0
Size: 328515 Color: 4
Size: 314117 Color: 0

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 472573 Color: 2
Size: 275943 Color: 0
Size: 251485 Color: 1

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 436427 Color: 1
Size: 291882 Color: 3
Size: 271692 Color: 2

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 435786 Color: 2
Size: 300631 Color: 4
Size: 263584 Color: 4

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 358536 Color: 1
Size: 330553 Color: 2
Size: 310912 Color: 3

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 370062 Color: 2
Size: 364442 Color: 3
Size: 265497 Color: 1

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 363005 Color: 4
Size: 346540 Color: 1
Size: 290456 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 459502 Color: 1
Size: 284232 Color: 4
Size: 256267 Color: 4

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 442752 Color: 0
Size: 289397 Color: 4
Size: 267852 Color: 1

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 477151 Color: 0
Size: 266232 Color: 4
Size: 256618 Color: 1

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 361717 Color: 4
Size: 356909 Color: 3
Size: 281375 Color: 3

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 389104 Color: 2
Size: 326490 Color: 4
Size: 284407 Color: 2

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 394276 Color: 1
Size: 315378 Color: 2
Size: 290347 Color: 2

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 415288 Color: 1
Size: 300597 Color: 2
Size: 284116 Color: 2

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 468620 Color: 2
Size: 272135 Color: 0
Size: 259246 Color: 1

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 451224 Color: 3
Size: 296725 Color: 0
Size: 252052 Color: 4

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 428963 Color: 4
Size: 314946 Color: 0
Size: 256092 Color: 2

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 475592 Color: 1
Size: 267760 Color: 2
Size: 256649 Color: 3

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 463716 Color: 2
Size: 281021 Color: 0
Size: 255264 Color: 1

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 355959 Color: 2
Size: 326606 Color: 2
Size: 317436 Color: 0

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 381300 Color: 4
Size: 359498 Color: 2
Size: 259203 Color: 1

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 360896 Color: 3
Size: 359085 Color: 4
Size: 280020 Color: 3

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 416678 Color: 0
Size: 306504 Color: 0
Size: 276819 Color: 3

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 380500 Color: 4
Size: 367719 Color: 2
Size: 251782 Color: 1

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 384880 Color: 2
Size: 326569 Color: 3
Size: 288552 Color: 3

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 456598 Color: 4
Size: 278018 Color: 2
Size: 265385 Color: 2

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 468796 Color: 0
Size: 267356 Color: 2
Size: 263849 Color: 1

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 361936 Color: 4
Size: 325861 Color: 4
Size: 312204 Color: 2

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 339745 Color: 0
Size: 332589 Color: 1
Size: 327667 Color: 4

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 340475 Color: 3
Size: 336755 Color: 3
Size: 322771 Color: 4

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 356893 Color: 0
Size: 348493 Color: 4
Size: 294615 Color: 4

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 351738 Color: 1
Size: 338865 Color: 1
Size: 309398 Color: 0

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 356826 Color: 2
Size: 345224 Color: 4
Size: 297951 Color: 1

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 355047 Color: 3
Size: 351062 Color: 1
Size: 293892 Color: 4

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 371383 Color: 3
Size: 344231 Color: 2
Size: 284387 Color: 3

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 356625 Color: 3
Size: 322608 Color: 2
Size: 320768 Color: 3

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 356648 Color: 2
Size: 355357 Color: 0
Size: 287996 Color: 3

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 361007 Color: 0
Size: 359542 Color: 3
Size: 279452 Color: 2

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 338687 Color: 2
Size: 332483 Color: 1
Size: 328831 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 364419 Color: 4
Size: 349942 Color: 0
Size: 285640 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 350361 Color: 4
Size: 327065 Color: 0
Size: 322575 Color: 2

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 343531 Color: 4
Size: 330038 Color: 1
Size: 326432 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 354948 Color: 0
Size: 339627 Color: 3
Size: 305426 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 367235 Color: 2
Size: 344686 Color: 4
Size: 288080 Color: 1

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 353707 Color: 3
Size: 338876 Color: 0
Size: 307418 Color: 2

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 349686 Color: 2
Size: 341263 Color: 1
Size: 309052 Color: 0

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 356386 Color: 1
Size: 348024 Color: 1
Size: 295591 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 357618 Color: 4
Size: 355626 Color: 0
Size: 286757 Color: 3

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 347521 Color: 2
Size: 332102 Color: 3
Size: 320378 Color: 3

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 352883 Color: 4
Size: 336706 Color: 1
Size: 310412 Color: 2

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 384766 Color: 0
Size: 318500 Color: 3
Size: 296735 Color: 2

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 339849 Color: 2
Size: 331558 Color: 2
Size: 328594 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 350203 Color: 1
Size: 330548 Color: 3
Size: 319250 Color: 1

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 356711 Color: 4
Size: 352348 Color: 1
Size: 290942 Color: 0

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 355532 Color: 2
Size: 328864 Color: 3
Size: 315605 Color: 1

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 358279 Color: 2
Size: 324497 Color: 0
Size: 317225 Color: 0

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 351703 Color: 1
Size: 344132 Color: 4
Size: 304166 Color: 4

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 348732 Color: 2
Size: 332034 Color: 0
Size: 319235 Color: 2

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 350324 Color: 3
Size: 326848 Color: 4
Size: 322829 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 356373 Color: 0
Size: 348084 Color: 3
Size: 295544 Color: 1

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 355681 Color: 4
Size: 350618 Color: 3
Size: 293702 Color: 1

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 348734 Color: 1
Size: 333496 Color: 1
Size: 317771 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 353450 Color: 4
Size: 352134 Color: 2
Size: 294417 Color: 1

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 353870 Color: 1
Size: 325182 Color: 3
Size: 320949 Color: 1

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 350052 Color: 4
Size: 333996 Color: 0
Size: 315953 Color: 4

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 358781 Color: 3
Size: 341819 Color: 3
Size: 299401 Color: 2

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 355240 Color: 0
Size: 340502 Color: 1
Size: 304259 Color: 3

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 352006 Color: 0
Size: 332848 Color: 4
Size: 315147 Color: 1

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 349053 Color: 4
Size: 338635 Color: 4
Size: 312313 Color: 3

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 347611 Color: 1
Size: 331144 Color: 4
Size: 321246 Color: 3

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 337493 Color: 2
Size: 332978 Color: 4
Size: 329530 Color: 1

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 352842 Color: 0
Size: 340581 Color: 4
Size: 306578 Color: 3

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 347613 Color: 2
Size: 339388 Color: 3
Size: 313000 Color: 2

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 351845 Color: 1
Size: 340206 Color: 0
Size: 307950 Color: 3

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 352178 Color: 3
Size: 336420 Color: 2
Size: 311403 Color: 3

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 350849 Color: 2
Size: 333747 Color: 0
Size: 315405 Color: 2

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 348091 Color: 3
Size: 336551 Color: 3
Size: 315359 Color: 2

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 355036 Color: 0
Size: 325701 Color: 4
Size: 319264 Color: 3

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 356970 Color: 1
Size: 336359 Color: 3
Size: 306672 Color: 0

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 345532 Color: 1
Size: 333673 Color: 1
Size: 320796 Color: 2

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 362334 Color: 1
Size: 346353 Color: 2
Size: 291314 Color: 4

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 378895 Color: 0
Size: 335203 Color: 1
Size: 285903 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 346695 Color: 2
Size: 334277 Color: 2
Size: 319029 Color: 1

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 358836 Color: 3
Size: 327350 Color: 4
Size: 313815 Color: 1

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 358575 Color: 4
Size: 346967 Color: 0
Size: 294459 Color: 2

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 359312 Color: 0
Size: 324406 Color: 0
Size: 316283 Color: 3

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 343038 Color: 0
Size: 333912 Color: 2
Size: 323051 Color: 1

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 353083 Color: 0
Size: 347289 Color: 3
Size: 299629 Color: 4

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 346963 Color: 4
Size: 335157 Color: 1
Size: 317881 Color: 4

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 366588 Color: 2
Size: 359052 Color: 4
Size: 274361 Color: 0

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 355364 Color: 1
Size: 323371 Color: 3
Size: 321266 Color: 3

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 355268 Color: 4
Size: 328102 Color: 1
Size: 316631 Color: 2

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 334335 Color: 0
Size: 333407 Color: 3
Size: 332259 Color: 3

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 355904 Color: 0
Size: 345057 Color: 3
Size: 299040 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 346390 Color: 1
Size: 335567 Color: 4
Size: 318044 Color: 0

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 352606 Color: 1
Size: 325410 Color: 4
Size: 321985 Color: 1

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 345249 Color: 1
Size: 343031 Color: 0
Size: 311721 Color: 1

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 345221 Color: 2
Size: 329954 Color: 0
Size: 324826 Color: 3

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 341126 Color: 4
Size: 329532 Color: 2
Size: 329343 Color: 4

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 360575 Color: 1
Size: 324615 Color: 4
Size: 314811 Color: 2

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 344719 Color: 0
Size: 327958 Color: 1
Size: 327324 Color: 2

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 351914 Color: 3
Size: 331579 Color: 4
Size: 316508 Color: 2

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 346845 Color: 4
Size: 344773 Color: 1
Size: 308383 Color: 1

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 350497 Color: 1
Size: 336782 Color: 3
Size: 312722 Color: 4

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 352308 Color: 1
Size: 334112 Color: 2
Size: 313581 Color: 0

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 342477 Color: 3
Size: 329215 Color: 4
Size: 328309 Color: 3

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 340702 Color: 0
Size: 330139 Color: 0
Size: 329160 Color: 3

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 351323 Color: 3
Size: 328279 Color: 1
Size: 320399 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 363566 Color: 4
Size: 353282 Color: 3
Size: 283153 Color: 2

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 340149 Color: 2
Size: 335478 Color: 0
Size: 324374 Color: 3

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 473847 Color: 3
Size: 268470 Color: 1
Size: 257684 Color: 2

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 338213 Color: 4
Size: 336079 Color: 1
Size: 325709 Color: 4

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 365049 Color: 4
Size: 356331 Color: 3
Size: 278621 Color: 0

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 357545 Color: 4
Size: 346726 Color: 2
Size: 295730 Color: 1

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 362352 Color: 3
Size: 357939 Color: 2
Size: 279710 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 352872 Color: 0
Size: 352232 Color: 2
Size: 294897 Color: 4

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 363882 Color: 4
Size: 353670 Color: 3
Size: 282449 Color: 3

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 410136 Color: 4
Size: 328887 Color: 1
Size: 260978 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 353144 Color: 0
Size: 326550 Color: 0
Size: 320307 Color: 3

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 357947 Color: 3
Size: 355463 Color: 2
Size: 286591 Color: 3

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 350582 Color: 4
Size: 339223 Color: 3
Size: 310196 Color: 2

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 349791 Color: 0
Size: 346664 Color: 0
Size: 303546 Color: 2

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 349280 Color: 0
Size: 327780 Color: 2
Size: 322941 Color: 1

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 391050 Color: 3
Size: 336946 Color: 1
Size: 272005 Color: 4

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 346153 Color: 1
Size: 345242 Color: 2
Size: 308606 Color: 2

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 354418 Color: 1
Size: 346620 Color: 3
Size: 298963 Color: 3

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 342856 Color: 2
Size: 339857 Color: 1
Size: 317288 Color: 3

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 349734 Color: 2
Size: 337426 Color: 2
Size: 312841 Color: 3

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 359888 Color: 2
Size: 349437 Color: 4
Size: 290676 Color: 4

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 343117 Color: 1
Size: 342234 Color: 2
Size: 314650 Color: 2

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 337417 Color: 1
Size: 336217 Color: 0
Size: 326367 Color: 1

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 342263 Color: 4
Size: 337684 Color: 4
Size: 320054 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 341131 Color: 3
Size: 336740 Color: 2
Size: 322130 Color: 2

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 338502 Color: 3
Size: 336157 Color: 0
Size: 325342 Color: 3

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 351410 Color: 3
Size: 337316 Color: 3
Size: 311275 Color: 0

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 356631 Color: 0
Size: 336390 Color: 3
Size: 306980 Color: 1

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 380448 Color: 0
Size: 355571 Color: 3
Size: 263982 Color: 1

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 346455 Color: 4
Size: 337486 Color: 2
Size: 316060 Color: 3

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 356747 Color: 2
Size: 334831 Color: 1
Size: 308423 Color: 3

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 340441 Color: 0
Size: 335743 Color: 0
Size: 323817 Color: 4

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 345036 Color: 1
Size: 332898 Color: 0
Size: 322067 Color: 1

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 356553 Color: 2
Size: 354685 Color: 4
Size: 288763 Color: 3

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 357016 Color: 3
Size: 323106 Color: 1
Size: 319879 Color: 2

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 357317 Color: 2
Size: 322677 Color: 0
Size: 320007 Color: 4

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 357345 Color: 1
Size: 356619 Color: 2
Size: 286037 Color: 2

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 357925 Color: 2
Size: 353629 Color: 0
Size: 288447 Color: 1

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 357468 Color: 1
Size: 329319 Color: 3
Size: 313214 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 358080 Color: 3
Size: 333453 Color: 2
Size: 308468 Color: 1

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 358145 Color: 0
Size: 343626 Color: 0
Size: 298230 Color: 1

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 358343 Color: 3
Size: 341217 Color: 0
Size: 300441 Color: 2

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 358307 Color: 0
Size: 324964 Color: 1
Size: 316730 Color: 4

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 358959 Color: 4
Size: 321683 Color: 3
Size: 319359 Color: 2

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 358534 Color: 2
Size: 354155 Color: 3
Size: 287312 Color: 4

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 359088 Color: 3
Size: 352132 Color: 4
Size: 288781 Color: 3

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 359255 Color: 3
Size: 325149 Color: 1
Size: 315597 Color: 0

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 359380 Color: 0
Size: 353731 Color: 4
Size: 286890 Color: 4

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 359299 Color: 1
Size: 350736 Color: 4
Size: 289966 Color: 2

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 359347 Color: 2
Size: 344780 Color: 1
Size: 295874 Color: 4

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 359380 Color: 1
Size: 356687 Color: 0
Size: 283934 Color: 1

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 359538 Color: 1
Size: 342467 Color: 4
Size: 297996 Color: 2

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 359388 Color: 0
Size: 334417 Color: 2
Size: 306196 Color: 4

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 359552 Color: 1
Size: 330265 Color: 3
Size: 310184 Color: 4

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 359845 Color: 1
Size: 347944 Color: 3
Size: 292212 Color: 3

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 359992 Color: 0
Size: 336508 Color: 2
Size: 303501 Color: 1

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 359935 Color: 1
Size: 325640 Color: 0
Size: 314426 Color: 4

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 360188 Color: 0
Size: 327760 Color: 2
Size: 312053 Color: 3

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 360147 Color: 2
Size: 327570 Color: 4
Size: 312284 Color: 1

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 360181 Color: 1
Size: 326716 Color: 0
Size: 313104 Color: 2

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 360485 Color: 1
Size: 321160 Color: 3
Size: 318356 Color: 4

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 360595 Color: 2
Size: 349386 Color: 2
Size: 290020 Color: 1

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 360901 Color: 2
Size: 319878 Color: 3
Size: 319222 Color: 1

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 360910 Color: 1
Size: 357677 Color: 3
Size: 281414 Color: 0

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 360961 Color: 4
Size: 319803 Color: 2
Size: 319237 Color: 4

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 360923 Color: 1
Size: 346458 Color: 0
Size: 292620 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 361033 Color: 4
Size: 353779 Color: 4
Size: 285189 Color: 2

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 361371 Color: 2
Size: 348714 Color: 1
Size: 289916 Color: 4

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 361461 Color: 3
Size: 327206 Color: 2
Size: 311334 Color: 0

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 361482 Color: 0
Size: 353783 Color: 4
Size: 284736 Color: 0

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 361727 Color: 0
Size: 330214 Color: 0
Size: 308060 Color: 3

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 361672 Color: 1
Size: 321473 Color: 1
Size: 316856 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 361899 Color: 3
Size: 320837 Color: 2
Size: 317265 Color: 0

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 361922 Color: 4
Size: 326436 Color: 2
Size: 311643 Color: 4

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 362115 Color: 0
Size: 326721 Color: 3
Size: 311165 Color: 2

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 362339 Color: 0
Size: 353080 Color: 2
Size: 284582 Color: 1

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 362381 Color: 0
Size: 355363 Color: 3
Size: 282257 Color: 2

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 362384 Color: 0
Size: 356560 Color: 4
Size: 281057 Color: 2

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 362569 Color: 0
Size: 355869 Color: 1
Size: 281563 Color: 3

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 362530 Color: 4
Size: 326318 Color: 2
Size: 311153 Color: 3

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 362651 Color: 3
Size: 325334 Color: 0
Size: 312016 Color: 1

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 362746 Color: 0
Size: 333895 Color: 2
Size: 303360 Color: 3

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 362737 Color: 3
Size: 352078 Color: 2
Size: 285186 Color: 3

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 362750 Color: 1
Size: 319744 Color: 3
Size: 317507 Color: 2

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 362778 Color: 4
Size: 326030 Color: 1
Size: 311193 Color: 2

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 362848 Color: 0
Size: 321998 Color: 2
Size: 315155 Color: 0

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 363044 Color: 3
Size: 328127 Color: 2
Size: 308830 Color: 3

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 362645 Color: 2
Size: 339304 Color: 1
Size: 298052 Color: 1

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 363152 Color: 3
Size: 357009 Color: 4
Size: 279840 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 362848 Color: 2
Size: 348572 Color: 2
Size: 288581 Color: 1

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 363047 Color: 2
Size: 336715 Color: 1
Size: 300239 Color: 0

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 363213 Color: 1
Size: 330959 Color: 4
Size: 305829 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 363228 Color: 1
Size: 337839 Color: 1
Size: 298934 Color: 3

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 363315 Color: 3
Size: 322661 Color: 4
Size: 314025 Color: 4

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 363421 Color: 4
Size: 340971 Color: 2
Size: 295609 Color: 4

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 363508 Color: 1
Size: 331208 Color: 0
Size: 305285 Color: 4

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 363576 Color: 3
Size: 329055 Color: 0
Size: 307370 Color: 1

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 363625 Color: 2
Size: 324198 Color: 4
Size: 312178 Color: 0

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 363883 Color: 0
Size: 360768 Color: 1
Size: 275350 Color: 2

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 363661 Color: 2
Size: 350705 Color: 3
Size: 285635 Color: 4

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 363687 Color: 1
Size: 351776 Color: 4
Size: 284538 Color: 0

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 363722 Color: 1
Size: 349817 Color: 1
Size: 286462 Color: 2

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 363723 Color: 3
Size: 320914 Color: 0
Size: 315364 Color: 2

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 363915 Color: 4
Size: 349570 Color: 0
Size: 286516 Color: 3

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 364216 Color: 0
Size: 329561 Color: 1
Size: 306224 Color: 3

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 364154 Color: 4
Size: 324314 Color: 1
Size: 311533 Color: 2

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 364198 Color: 4
Size: 337981 Color: 3
Size: 297822 Color: 2

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 364279 Color: 3
Size: 326897 Color: 4
Size: 308825 Color: 2

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 364317 Color: 3
Size: 342311 Color: 1
Size: 293373 Color: 1

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 364329 Color: 2
Size: 351952 Color: 0
Size: 283720 Color: 3

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 364397 Color: 4
Size: 326786 Color: 0
Size: 308818 Color: 3

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 364484 Color: 1
Size: 319287 Color: 3
Size: 316230 Color: 2

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 364470 Color: 2
Size: 330618 Color: 1
Size: 304913 Color: 1

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 364485 Color: 4
Size: 360633 Color: 1
Size: 274883 Color: 4

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 364509 Color: 1
Size: 352160 Color: 2
Size: 283332 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 364527 Color: 0
Size: 361871 Color: 4
Size: 273603 Color: 1

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 364685 Color: 0
Size: 348631 Color: 2
Size: 286685 Color: 4

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 364687 Color: 3
Size: 339789 Color: 1
Size: 295525 Color: 2

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 364724 Color: 1
Size: 320081 Color: 4
Size: 315196 Color: 0

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 364741 Color: 4
Size: 319458 Color: 2
Size: 315802 Color: 3

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 365039 Color: 0
Size: 331909 Color: 2
Size: 303053 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 364799 Color: 1
Size: 333067 Color: 0
Size: 302135 Color: 3

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 365134 Color: 0
Size: 335238 Color: 3
Size: 299629 Color: 3

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 364874 Color: 2
Size: 342122 Color: 2
Size: 293005 Color: 4

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 364920 Color: 3
Size: 332004 Color: 0
Size: 303077 Color: 1

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 365162 Color: 0
Size: 318381 Color: 3
Size: 316458 Color: 3

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 364977 Color: 3
Size: 336142 Color: 2
Size: 298882 Color: 4

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 365195 Color: 0
Size: 340594 Color: 3
Size: 294212 Color: 3

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 365046 Color: 4
Size: 327435 Color: 1
Size: 307520 Color: 2

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 365314 Color: 0
Size: 345309 Color: 1
Size: 289378 Color: 4

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 365256 Color: 4
Size: 341020 Color: 1
Size: 293725 Color: 1

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 365273 Color: 4
Size: 352001 Color: 3
Size: 282727 Color: 0

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 365362 Color: 0
Size: 335697 Color: 1
Size: 298942 Color: 4

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 365592 Color: 3
Size: 345798 Color: 0
Size: 288611 Color: 3

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 365798 Color: 2
Size: 322002 Color: 1
Size: 312201 Color: 3

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 365798 Color: 3
Size: 347663 Color: 2
Size: 286540 Color: 0

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 365894 Color: 1
Size: 333052 Color: 1
Size: 301055 Color: 0

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 365899 Color: 3
Size: 362792 Color: 4
Size: 271310 Color: 4

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 365840 Color: 0
Size: 342548 Color: 1
Size: 291613 Color: 3

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 366042 Color: 0
Size: 353728 Color: 2
Size: 280231 Color: 3

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 366103 Color: 2
Size: 349729 Color: 4
Size: 284169 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 366245 Color: 0
Size: 349191 Color: 2
Size: 284565 Color: 3

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 366189 Color: 1
Size: 360236 Color: 2
Size: 273576 Color: 2

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 366420 Color: 0
Size: 325826 Color: 3
Size: 307755 Color: 1

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 366396 Color: 1
Size: 333182 Color: 3
Size: 300423 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 366412 Color: 2
Size: 319656 Color: 3
Size: 313933 Color: 1

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 366493 Color: 1
Size: 322468 Color: 0
Size: 311040 Color: 1

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 366512 Color: 1
Size: 339764 Color: 1
Size: 293725 Color: 3

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 366705 Color: 2
Size: 357335 Color: 3
Size: 275961 Color: 4

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 366732 Color: 3
Size: 348382 Color: 2
Size: 284887 Color: 1

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 367122 Color: 2
Size: 334982 Color: 3
Size: 297897 Color: 2

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 367265 Color: 1
Size: 332309 Color: 2
Size: 300427 Color: 3

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 367134 Color: 4
Size: 331551 Color: 2
Size: 301316 Color: 0

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 367276 Color: 1
Size: 353339 Color: 3
Size: 279386 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 367257 Color: 3
Size: 348175 Color: 1
Size: 284569 Color: 1

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 367295 Color: 1
Size: 333695 Color: 4
Size: 299011 Color: 3

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 367298 Color: 3
Size: 363978 Color: 4
Size: 268725 Color: 1

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 367479 Color: 3
Size: 341078 Color: 0
Size: 291444 Color: 4

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 367453 Color: 1
Size: 351079 Color: 0
Size: 281469 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 367871 Color: 0
Size: 340314 Color: 1
Size: 291816 Color: 2

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 367664 Color: 3
Size: 340077 Color: 0
Size: 292260 Color: 3

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 367982 Color: 0
Size: 344653 Color: 4
Size: 287366 Color: 1

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 367689 Color: 4
Size: 364585 Color: 4
Size: 267727 Color: 3

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 367825 Color: 1
Size: 326671 Color: 4
Size: 305505 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 367918 Color: 3
Size: 319074 Color: 4
Size: 313009 Color: 4

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 368099 Color: 0
Size: 338109 Color: 4
Size: 293793 Color: 1

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 368009 Color: 4
Size: 342715 Color: 1
Size: 289277 Color: 2

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 368103 Color: 1
Size: 340120 Color: 2
Size: 291778 Color: 2

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 368183 Color: 4
Size: 328623 Color: 3
Size: 303195 Color: 3

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 368324 Color: 0
Size: 337636 Color: 2
Size: 294041 Color: 2

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 368224 Color: 2
Size: 326528 Color: 1
Size: 305249 Color: 2

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 368445 Color: 4
Size: 360757 Color: 4
Size: 270799 Color: 2

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 368450 Color: 3
Size: 332720 Color: 0
Size: 298831 Color: 1

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 368590 Color: 0
Size: 319415 Color: 4
Size: 311996 Color: 4

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 368461 Color: 2
Size: 362988 Color: 1
Size: 268552 Color: 3

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 368610 Color: 2
Size: 350997 Color: 4
Size: 280394 Color: 0

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 368637 Color: 0
Size: 334605 Color: 2
Size: 296759 Color: 4

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 368727 Color: 2
Size: 335478 Color: 3
Size: 295796 Color: 1

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 368895 Color: 0
Size: 328440 Color: 4
Size: 302666 Color: 2

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 368748 Color: 1
Size: 341169 Color: 2
Size: 290084 Color: 3

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 368895 Color: 0
Size: 339423 Color: 4
Size: 291683 Color: 1

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 368924 Color: 3
Size: 361214 Color: 2
Size: 269863 Color: 4

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 368949 Color: 3
Size: 351777 Color: 0
Size: 279275 Color: 1

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 368997 Color: 4
Size: 325518 Color: 4
Size: 305486 Color: 1

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 369223 Color: 1
Size: 359493 Color: 3
Size: 271285 Color: 2

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 369063 Color: 2
Size: 345722 Color: 0
Size: 285216 Color: 4

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 369232 Color: 1
Size: 362492 Color: 3
Size: 268277 Color: 4

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 369151 Color: 4
Size: 325516 Color: 0
Size: 305334 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 369163 Color: 0
Size: 327286 Color: 2
Size: 303552 Color: 1

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 369298 Color: 4
Size: 348243 Color: 0
Size: 282460 Color: 1

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 369304 Color: 0
Size: 328177 Color: 2
Size: 302520 Color: 4

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 369401 Color: 0
Size: 326817 Color: 1
Size: 303783 Color: 0

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 369233 Color: 1
Size: 350012 Color: 2
Size: 280756 Color: 2

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 369391 Color: 4
Size: 332523 Color: 2
Size: 298087 Color: 4

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 369428 Color: 3
Size: 325111 Color: 3
Size: 305462 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 369567 Color: 3
Size: 345539 Color: 1
Size: 284895 Color: 4

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 369567 Color: 2
Size: 338721 Color: 3
Size: 291713 Color: 1

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 369568 Color: 4
Size: 365046 Color: 0
Size: 265387 Color: 2

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 369686 Color: 0
Size: 321576 Color: 3
Size: 308739 Color: 1

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 369445 Color: 1
Size: 328660 Color: 2
Size: 301896 Color: 3

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 369629 Color: 4
Size: 353992 Color: 0
Size: 276380 Color: 3

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 369809 Color: 0
Size: 315322 Color: 2
Size: 314870 Color: 2

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 369643 Color: 1
Size: 355629 Color: 4
Size: 274729 Color: 3

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 369831 Color: 0
Size: 338498 Color: 3
Size: 291672 Color: 4

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 369799 Color: 4
Size: 346316 Color: 1
Size: 283886 Color: 4

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 369896 Color: 3
Size: 347957 Color: 0
Size: 282148 Color: 1

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 369948 Color: 3
Size: 341474 Color: 0
Size: 288579 Color: 2

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 369723 Color: 1
Size: 355602 Color: 3
Size: 274676 Color: 1

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 370011 Color: 0
Size: 345139 Color: 3
Size: 284851 Color: 3

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 370108 Color: 4
Size: 343852 Color: 3
Size: 286041 Color: 3

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 370113 Color: 2
Size: 317846 Color: 0
Size: 312042 Color: 1

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 370140 Color: 4
Size: 336768 Color: 2
Size: 293093 Color: 1

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 370140 Color: 4
Size: 326197 Color: 3
Size: 303664 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 370156 Color: 0
Size: 365385 Color: 4
Size: 264460 Color: 1

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 370221 Color: 4
Size: 316296 Color: 1
Size: 313484 Color: 2

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 370251 Color: 2
Size: 366290 Color: 0
Size: 263460 Color: 1

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 370255 Color: 4
Size: 364326 Color: 3
Size: 265420 Color: 4

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 370256 Color: 0
Size: 347223 Color: 1
Size: 282522 Color: 4

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 370257 Color: 2
Size: 357245 Color: 0
Size: 272499 Color: 1

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 370298 Color: 2
Size: 320429 Color: 0
Size: 309274 Color: 3

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 370060 Color: 1
Size: 336586 Color: 0
Size: 293355 Color: 2

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 370464 Color: 2
Size: 353054 Color: 1
Size: 276483 Color: 4

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 370469 Color: 4
Size: 322477 Color: 4
Size: 307055 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 370540 Color: 0
Size: 369853 Color: 4
Size: 259608 Color: 3

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 370606 Color: 2
Size: 344602 Color: 4
Size: 284793 Color: 4

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 370688 Color: 3
Size: 330065 Color: 0
Size: 299248 Color: 1

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 370690 Color: 4
Size: 317092 Color: 1
Size: 312219 Color: 3

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 370244 Color: 1
Size: 315555 Color: 0
Size: 314202 Color: 1

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 370572 Color: 1
Size: 324124 Color: 3
Size: 305305 Color: 2

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 370714 Color: 2
Size: 343941 Color: 0
Size: 285346 Color: 3

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 370740 Color: 0
Size: 322891 Color: 3
Size: 306370 Color: 1

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 370618 Color: 1
Size: 340533 Color: 4
Size: 288850 Color: 2

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 370768 Color: 3
Size: 320771 Color: 4
Size: 308462 Color: 4

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 370775 Color: 4
Size: 344107 Color: 1
Size: 285119 Color: 4

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 370819 Color: 4
Size: 359547 Color: 1
Size: 269635 Color: 2

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 370844 Color: 2
Size: 320666 Color: 3
Size: 308491 Color: 2

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 370849 Color: 0
Size: 337513 Color: 1
Size: 291639 Color: 2

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 370936 Color: 4
Size: 333464 Color: 1
Size: 295601 Color: 0

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 371110 Color: 3
Size: 363328 Color: 3
Size: 265563 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 371148 Color: 1
Size: 338721 Color: 0
Size: 290132 Color: 2

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 371172 Color: 2
Size: 343625 Color: 0
Size: 285204 Color: 2

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 371396 Color: 0
Size: 337391 Color: 1
Size: 291214 Color: 4

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 371422 Color: 2
Size: 334086 Color: 2
Size: 294493 Color: 1

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 371333 Color: 1
Size: 346480 Color: 0
Size: 282188 Color: 1

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 371429 Color: 3
Size: 359025 Color: 4
Size: 269547 Color: 0

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 371450 Color: 4
Size: 353648 Color: 3
Size: 274903 Color: 2

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 371539 Color: 4
Size: 321486 Color: 1
Size: 306976 Color: 0

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 371513 Color: 0
Size: 365592 Color: 4
Size: 262896 Color: 4

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 371657 Color: 2
Size: 348345 Color: 0
Size: 279999 Color: 1

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 371684 Color: 4
Size: 328090 Color: 3
Size: 300227 Color: 2

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 371723 Color: 4
Size: 331400 Color: 0
Size: 296878 Color: 2

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 371828 Color: 2
Size: 336629 Color: 2
Size: 291544 Color: 3

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 371833 Color: 3
Size: 352543 Color: 0
Size: 275625 Color: 3

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 371931 Color: 0
Size: 357990 Color: 1
Size: 270080 Color: 4

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 371863 Color: 2
Size: 318115 Color: 1
Size: 310023 Color: 1

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 371969 Color: 1
Size: 364452 Color: 0
Size: 263580 Color: 4

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 372041 Color: 1
Size: 359997 Color: 4
Size: 267963 Color: 4

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 372066 Color: 4
Size: 316511 Color: 3
Size: 311424 Color: 0

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 372189 Color: 0
Size: 361616 Color: 1
Size: 266196 Color: 3

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 372112 Color: 2
Size: 324341 Color: 1
Size: 303548 Color: 2

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 372336 Color: 0
Size: 349466 Color: 1
Size: 278199 Color: 3

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 372523 Color: 3
Size: 336934 Color: 4
Size: 290544 Color: 1

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 372546 Color: 0
Size: 329405 Color: 1
Size: 298050 Color: 3

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 372616 Color: 3
Size: 333846 Color: 2
Size: 293539 Color: 0

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 372636 Color: 3
Size: 334566 Color: 2
Size: 292799 Color: 0

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 372747 Color: 0
Size: 331758 Color: 3
Size: 295496 Color: 2

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 372680 Color: 3
Size: 352437 Color: 4
Size: 274884 Color: 1

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 372765 Color: 3
Size: 330488 Color: 0
Size: 296748 Color: 1

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 372783 Color: 1
Size: 337427 Color: 2
Size: 289791 Color: 3

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 372784 Color: 2
Size: 342668 Color: 0
Size: 284549 Color: 1

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 372757 Color: 0
Size: 320898 Color: 4
Size: 306346 Color: 1

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 372801 Color: 3
Size: 343239 Color: 2
Size: 283961 Color: 3

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 372802 Color: 3
Size: 348181 Color: 1
Size: 279018 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 372810 Color: 3
Size: 326361 Color: 2
Size: 300830 Color: 4

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 372869 Color: 2
Size: 353848 Color: 0
Size: 273284 Color: 1

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 372871 Color: 3
Size: 357349 Color: 3
Size: 269781 Color: 0

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 372891 Color: 4
Size: 348195 Color: 1
Size: 278915 Color: 2

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 372931 Color: 3
Size: 324729 Color: 4
Size: 302341 Color: 4

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 373030 Color: 4
Size: 351013 Color: 2
Size: 275958 Color: 0

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 373097 Color: 2
Size: 364339 Color: 4
Size: 262565 Color: 4

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 373133 Color: 4
Size: 317988 Color: 0
Size: 308880 Color: 1

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 373172 Color: 3
Size: 345461 Color: 1
Size: 281368 Color: 4

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 373195 Color: 2
Size: 354876 Color: 1
Size: 271930 Color: 3

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 373232 Color: 2
Size: 360817 Color: 3
Size: 265952 Color: 0

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 373249 Color: 4
Size: 355226 Color: 1
Size: 271526 Color: 3

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 373263 Color: 4
Size: 328463 Color: 4
Size: 298275 Color: 0

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 373280 Color: 3
Size: 332807 Color: 1
Size: 293914 Color: 2

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 373373 Color: 2
Size: 325143 Color: 1
Size: 301485 Color: 2

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 373419 Color: 0
Size: 344385 Color: 0
Size: 282197 Color: 4

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 373472 Color: 3
Size: 341125 Color: 1
Size: 285404 Color: 3

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 373538 Color: 4
Size: 324322 Color: 4
Size: 302141 Color: 1

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 373720 Color: 1
Size: 316671 Color: 4
Size: 309610 Color: 0

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 373572 Color: 2
Size: 357142 Color: 4
Size: 269287 Color: 3

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 373623 Color: 4
Size: 338173 Color: 1
Size: 288205 Color: 2

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 373737 Color: 1
Size: 372244 Color: 3
Size: 254020 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 373757 Color: 2
Size: 317969 Color: 4
Size: 308275 Color: 4

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 373791 Color: 3
Size: 334840 Color: 0
Size: 291370 Color: 1

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 373830 Color: 4
Size: 348626 Color: 3
Size: 277545 Color: 3

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 373907 Color: 1
Size: 345469 Color: 4
Size: 280625 Color: 2

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 373908 Color: 3
Size: 372258 Color: 4
Size: 253835 Color: 2

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 373917 Color: 0
Size: 313896 Color: 3
Size: 312188 Color: 1

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 373912 Color: 1
Size: 335433 Color: 3
Size: 290656 Color: 2

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 373927 Color: 2
Size: 315218 Color: 3
Size: 310856 Color: 4

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 374077 Color: 3
Size: 330450 Color: 2
Size: 295474 Color: 2

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 374078 Color: 2
Size: 327154 Color: 0
Size: 298769 Color: 1

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 374117 Color: 2
Size: 326899 Color: 4
Size: 298985 Color: 1

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 374137 Color: 4
Size: 323198 Color: 2
Size: 302666 Color: 4

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 374152 Color: 3
Size: 336425 Color: 1
Size: 289424 Color: 2

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 374166 Color: 4
Size: 364965 Color: 2
Size: 260870 Color: 3

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 374207 Color: 2
Size: 324093 Color: 1
Size: 301701 Color: 2

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 374303 Color: 1
Size: 347583 Color: 3
Size: 278115 Color: 3

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 374214 Color: 0
Size: 317099 Color: 3
Size: 308688 Color: 2

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 374223 Color: 0
Size: 354874 Color: 2
Size: 270904 Color: 1

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 374242 Color: 3
Size: 327535 Color: 2
Size: 298224 Color: 4

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 374330 Color: 3
Size: 318334 Color: 1
Size: 307337 Color: 2

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 374442 Color: 1
Size: 313811 Color: 3
Size: 311748 Color: 3

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 374346 Color: 3
Size: 328362 Color: 0
Size: 297293 Color: 4

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 374368 Color: 3
Size: 371886 Color: 3
Size: 253747 Color: 1

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 374411 Color: 3
Size: 348411 Color: 0
Size: 277179 Color: 2

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 374500 Color: 4
Size: 350922 Color: 3
Size: 274579 Color: 2

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 374552 Color: 3
Size: 321832 Color: 4
Size: 303617 Color: 1

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 374633 Color: 2
Size: 330919 Color: 2
Size: 294449 Color: 4

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 374696 Color: 3
Size: 316309 Color: 2
Size: 308996 Color: 1

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 374812 Color: 1
Size: 331908 Color: 4
Size: 293281 Color: 2

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 374715 Color: 3
Size: 333107 Color: 4
Size: 292179 Color: 2

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 374897 Color: 1
Size: 339534 Color: 2
Size: 285570 Color: 2

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 374721 Color: 4
Size: 314408 Color: 2
Size: 310872 Color: 3

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 374818 Color: 4
Size: 343157 Color: 1
Size: 282026 Color: 2

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 374953 Color: 1
Size: 353434 Color: 2
Size: 271614 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 374914 Color: 2
Size: 329156 Color: 1
Size: 295931 Color: 4

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 374999 Color: 1
Size: 331645 Color: 4
Size: 293357 Color: 3

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 374955 Color: 2
Size: 338228 Color: 0
Size: 286818 Color: 3

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 375016 Color: 0
Size: 370926 Color: 4
Size: 254059 Color: 1

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 375087 Color: 0
Size: 368979 Color: 3
Size: 255935 Color: 1

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 375089 Color: 2
Size: 316122 Color: 3
Size: 308790 Color: 4

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 375159 Color: 2
Size: 369380 Color: 1
Size: 255462 Color: 0

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 375279 Color: 4
Size: 331499 Color: 3
Size: 293223 Color: 1

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 375320 Color: 3
Size: 356868 Color: 0
Size: 267813 Color: 4

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 375343 Color: 0
Size: 332578 Color: 4
Size: 292080 Color: 1

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 375346 Color: 2
Size: 356560 Color: 3
Size: 268095 Color: 3

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 375346 Color: 0
Size: 312680 Color: 4
Size: 311975 Color: 1

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 375403 Color: 3
Size: 313695 Color: 4
Size: 310903 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 375413 Color: 4
Size: 368952 Color: 0
Size: 255636 Color: 1

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 375430 Color: 4
Size: 371282 Color: 0
Size: 253289 Color: 1

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 375449 Color: 0
Size: 362984 Color: 3
Size: 261568 Color: 3

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 375451 Color: 4
Size: 315393 Color: 2
Size: 309157 Color: 1

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 375517 Color: 4
Size: 329104 Color: 3
Size: 295380 Color: 2

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 375567 Color: 4
Size: 356592 Color: 0
Size: 267842 Color: 3

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 375567 Color: 2
Size: 314253 Color: 3
Size: 310181 Color: 1

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 375581 Color: 3
Size: 314306 Color: 4
Size: 310114 Color: 1

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 375593 Color: 0
Size: 355517 Color: 4
Size: 268891 Color: 3

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 375630 Color: 4
Size: 315664 Color: 3
Size: 308707 Color: 2

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 375645 Color: 0
Size: 340069 Color: 2
Size: 284287 Color: 1

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 375690 Color: 0
Size: 354442 Color: 3
Size: 269869 Color: 1

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 375703 Color: 3
Size: 350537 Color: 0
Size: 273761 Color: 0

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 375783 Color: 0
Size: 319205 Color: 3
Size: 305013 Color: 1

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 375830 Color: 4
Size: 360898 Color: 3
Size: 263273 Color: 1

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 375864 Color: 2
Size: 358708 Color: 3
Size: 265429 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 375921 Color: 2
Size: 367988 Color: 4
Size: 256092 Color: 4

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 376024 Color: 4
Size: 324994 Color: 0
Size: 298983 Color: 2

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 376045 Color: 0
Size: 373915 Color: 0
Size: 250041 Color: 1

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 376055 Color: 1
Size: 356299 Color: 3
Size: 267647 Color: 0

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 376134 Color: 3
Size: 322462 Color: 4
Size: 301405 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 376177 Color: 0
Size: 372498 Color: 4
Size: 251326 Color: 2

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 376206 Color: 3
Size: 326161 Color: 4
Size: 297634 Color: 4

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 376301 Color: 0
Size: 366546 Color: 1
Size: 257154 Color: 4

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 376315 Color: 3
Size: 343718 Color: 0
Size: 279968 Color: 4

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 376327 Color: 2
Size: 323747 Color: 1
Size: 299927 Color: 0

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 376329 Color: 2
Size: 334018 Color: 2
Size: 289654 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 376355 Color: 2
Size: 345415 Color: 1
Size: 278231 Color: 4

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 376458 Color: 3
Size: 324315 Color: 4
Size: 299228 Color: 4

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 376461 Color: 4
Size: 357925 Color: 1
Size: 265615 Color: 3

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 376478 Color: 0
Size: 371354 Color: 4
Size: 252169 Color: 4

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 376592 Color: 4
Size: 365287 Color: 1
Size: 258122 Color: 0

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 376693 Color: 1
Size: 344588 Color: 0
Size: 278720 Color: 2

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 376597 Color: 3
Size: 354489 Color: 3
Size: 268915 Color: 4

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 376687 Color: 2
Size: 328736 Color: 1
Size: 294578 Color: 4

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 376759 Color: 1
Size: 372923 Color: 3
Size: 250319 Color: 0

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 376717 Color: 0
Size: 369015 Color: 1
Size: 254269 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 376750 Color: 4
Size: 357675 Color: 4
Size: 265576 Color: 0

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 376750 Color: 0
Size: 321703 Color: 4
Size: 301548 Color: 1

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 376797 Color: 3
Size: 324249 Color: 4
Size: 298955 Color: 1

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 376819 Color: 2
Size: 364292 Color: 4
Size: 258890 Color: 4

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 376873 Color: 3
Size: 354337 Color: 1
Size: 268791 Color: 4

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 377101 Color: 0
Size: 329870 Color: 2
Size: 293030 Color: 0

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 377277 Color: 1
Size: 350287 Color: 3
Size: 272437 Color: 4

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 377268 Color: 4
Size: 360867 Color: 1
Size: 261866 Color: 4

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 377368 Color: 1
Size: 367172 Color: 2
Size: 255461 Color: 3

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 377276 Color: 0
Size: 352608 Color: 2
Size: 270117 Color: 3

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 377317 Color: 3
Size: 322145 Color: 0
Size: 300539 Color: 1

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 377359 Color: 0
Size: 318684 Color: 2
Size: 303958 Color: 1

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 377441 Color: 1
Size: 353623 Color: 4
Size: 268937 Color: 2

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 377364 Color: 3
Size: 348094 Color: 4
Size: 274543 Color: 2

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 377433 Color: 2
Size: 332719 Color: 1
Size: 289849 Color: 4

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 377487 Color: 1
Size: 369815 Color: 0
Size: 252699 Color: 4

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 377446 Color: 3
Size: 322582 Color: 4
Size: 299973 Color: 3

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 377648 Color: 2
Size: 336772 Color: 3
Size: 285581 Color: 1

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 377677 Color: 2
Size: 342028 Color: 4
Size: 280296 Color: 4

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 377766 Color: 0
Size: 319239 Color: 4
Size: 302996 Color: 2

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 377750 Color: 1
Size: 335584 Color: 4
Size: 286667 Color: 0

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 377769 Color: 2
Size: 318664 Color: 3
Size: 303568 Color: 0

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 377806 Color: 3
Size: 317785 Color: 1
Size: 304410 Color: 4

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 377877 Color: 4
Size: 313266 Color: 2
Size: 308858 Color: 1

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 378130 Color: 0
Size: 312998 Color: 4
Size: 308873 Color: 0

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 378177 Color: 0
Size: 318773 Color: 2
Size: 303051 Color: 4

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 378186 Color: 0
Size: 324381 Color: 2
Size: 297434 Color: 1

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 378221 Color: 1
Size: 334957 Color: 2
Size: 286823 Color: 4

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 378275 Color: 4
Size: 335179 Color: 3
Size: 286547 Color: 0

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 378329 Color: 0
Size: 316870 Color: 2
Size: 304802 Color: 1

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 378336 Color: 1
Size: 325111 Color: 4
Size: 296554 Color: 2

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 378337 Color: 0
Size: 312936 Color: 4
Size: 308728 Color: 4

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 378422 Color: 1
Size: 319767 Color: 4
Size: 301812 Color: 4

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 378357 Color: 0
Size: 349992 Color: 3
Size: 271652 Color: 3

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 378383 Color: 4
Size: 310822 Color: 2
Size: 310796 Color: 1

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 378502 Color: 1
Size: 348587 Color: 2
Size: 272912 Color: 3

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 378563 Color: 2
Size: 314217 Color: 0
Size: 307221 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 378595 Color: 2
Size: 343021 Color: 3
Size: 278385 Color: 1

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 378606 Color: 3
Size: 328188 Color: 2
Size: 293207 Color: 4

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 378623 Color: 2
Size: 354774 Color: 4
Size: 266604 Color: 1

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 378661 Color: 1
Size: 351897 Color: 0
Size: 269443 Color: 2

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 378664 Color: 3
Size: 351104 Color: 4
Size: 270233 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 378729 Color: 1
Size: 360082 Color: 3
Size: 261190 Color: 3

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 378762 Color: 0
Size: 355454 Color: 3
Size: 265785 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 378773 Color: 2
Size: 343783 Color: 4
Size: 277445 Color: 1

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 378796 Color: 3
Size: 355182 Color: 3
Size: 266023 Color: 1

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 378843 Color: 1
Size: 365957 Color: 3
Size: 255201 Color: 3

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 378815 Color: 4
Size: 328321 Color: 0
Size: 292865 Color: 2

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 378828 Color: 4
Size: 312374 Color: 1
Size: 308799 Color: 2

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 378912 Color: 1
Size: 354671 Color: 0
Size: 266418 Color: 3

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 378844 Color: 0
Size: 320756 Color: 0
Size: 300401 Color: 3

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 378915 Color: 0
Size: 353089 Color: 4
Size: 267997 Color: 2

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 379140 Color: 1
Size: 352657 Color: 0
Size: 268204 Color: 2

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 379221 Color: 1
Size: 369818 Color: 3
Size: 250962 Color: 4

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 378948 Color: 2
Size: 317220 Color: 3
Size: 303833 Color: 0

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 379140 Color: 0
Size: 357688 Color: 1
Size: 263173 Color: 2

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 379285 Color: 4
Size: 364036 Color: 1
Size: 256680 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 379321 Color: 2
Size: 332072 Color: 4
Size: 288608 Color: 0

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 379329 Color: 0
Size: 331391 Color: 3
Size: 289281 Color: 1

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 379341 Color: 2
Size: 341751 Color: 1
Size: 278909 Color: 3

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 379344 Color: 4
Size: 365357 Color: 2
Size: 255300 Color: 3

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 379456 Color: 1
Size: 351040 Color: 2
Size: 269505 Color: 3

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 379448 Color: 4
Size: 355236 Color: 2
Size: 265317 Color: 2

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 379497 Color: 3
Size: 360448 Color: 2
Size: 260056 Color: 1

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 379593 Color: 3
Size: 339631 Color: 1
Size: 280777 Color: 3

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 379594 Color: 3
Size: 350934 Color: 4
Size: 269473 Color: 2

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 379608 Color: 3
Size: 358700 Color: 4
Size: 261693 Color: 1

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 379536 Color: 1
Size: 316185 Color: 0
Size: 304280 Color: 4

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 379692 Color: 4
Size: 331171 Color: 4
Size: 289138 Color: 0

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 379711 Color: 2
Size: 345167 Color: 1
Size: 275123 Color: 0

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 379801 Color: 4
Size: 336250 Color: 2
Size: 283950 Color: 1

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 379874 Color: 3
Size: 357841 Color: 3
Size: 262286 Color: 0

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 379903 Color: 4
Size: 312524 Color: 1
Size: 307574 Color: 4

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 379867 Color: 1
Size: 312680 Color: 3
Size: 307454 Color: 0

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 379931 Color: 3
Size: 370064 Color: 4
Size: 250006 Color: 3

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 380002 Color: 1
Size: 357466 Color: 2
Size: 262533 Color: 3

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 380004 Color: 3
Size: 339542 Color: 2
Size: 280455 Color: 1

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 380123 Color: 1
Size: 365225 Color: 0
Size: 254653 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 380050 Color: 4
Size: 331254 Color: 2
Size: 288697 Color: 4

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 380052 Color: 0
Size: 312469 Color: 2
Size: 307480 Color: 1

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 380103 Color: 3
Size: 318959 Color: 2
Size: 300939 Color: 4

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 380109 Color: 3
Size: 346497 Color: 0
Size: 273395 Color: 1

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 380135 Color: 2
Size: 334353 Color: 0
Size: 285513 Color: 1

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 380218 Color: 2
Size: 360819 Color: 4
Size: 258964 Color: 2

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 380233 Color: 4
Size: 349356 Color: 2
Size: 270412 Color: 1

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 380381 Color: 0
Size: 322320 Color: 2
Size: 297300 Color: 1

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 380383 Color: 3
Size: 338369 Color: 0
Size: 281249 Color: 3

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 380393 Color: 0
Size: 344265 Color: 3
Size: 275343 Color: 1

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 380422 Color: 3
Size: 333114 Color: 2
Size: 286465 Color: 4

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 380479 Color: 4
Size: 317037 Color: 1
Size: 302485 Color: 2

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 380496 Color: 3
Size: 340555 Color: 2
Size: 278950 Color: 3

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 380506 Color: 2
Size: 367625 Color: 0
Size: 251870 Color: 1

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 380518 Color: 3
Size: 330809 Color: 4
Size: 288674 Color: 3

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 380583 Color: 1
Size: 322409 Color: 0
Size: 297009 Color: 2

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 380587 Color: 0
Size: 354551 Color: 1
Size: 264863 Color: 2

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 380620 Color: 1
Size: 343148 Color: 4
Size: 276233 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 380616 Color: 0
Size: 339744 Color: 3
Size: 279641 Color: 4

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 380624 Color: 4
Size: 347694 Color: 0
Size: 271683 Color: 1

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 380638 Color: 2
Size: 332857 Color: 4
Size: 286506 Color: 3

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 380638 Color: 2
Size: 321687 Color: 0
Size: 297676 Color: 1

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 380675 Color: 1
Size: 335562 Color: 0
Size: 283764 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 380684 Color: 0
Size: 344992 Color: 2
Size: 274325 Color: 3

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 380754 Color: 0
Size: 337984 Color: 0
Size: 281263 Color: 1

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 380855 Color: 1
Size: 354399 Color: 3
Size: 264747 Color: 3

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 380779 Color: 4
Size: 318114 Color: 3
Size: 301108 Color: 3

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 380821 Color: 3
Size: 336764 Color: 2
Size: 282416 Color: 1

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 380883 Color: 0
Size: 330762 Color: 3
Size: 288356 Color: 3

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 380924 Color: 3
Size: 343337 Color: 0
Size: 275740 Color: 1

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 380941 Color: 3
Size: 360907 Color: 4
Size: 258153 Color: 4

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 381056 Color: 1
Size: 345032 Color: 3
Size: 273913 Color: 2

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 380975 Color: 3
Size: 318832 Color: 2
Size: 300194 Color: 0

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 381121 Color: 1
Size: 345805 Color: 3
Size: 273075 Color: 3

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 380981 Color: 3
Size: 367016 Color: 0
Size: 252004 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 381208 Color: 1
Size: 357824 Color: 3
Size: 260969 Color: 0

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 381065 Color: 4
Size: 358647 Color: 2
Size: 260289 Color: 3

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 381131 Color: 2
Size: 344285 Color: 1
Size: 274585 Color: 0

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 381164 Color: 0
Size: 318279 Color: 3
Size: 300558 Color: 1

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 381199 Color: 0
Size: 342216 Color: 1
Size: 276586 Color: 2

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 381217 Color: 4
Size: 341932 Color: 0
Size: 276852 Color: 2

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 381346 Color: 1
Size: 348107 Color: 3
Size: 270548 Color: 3

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 381327 Color: 0
Size: 314429 Color: 4
Size: 304245 Color: 4

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 381353 Color: 3
Size: 342125 Color: 2
Size: 276523 Color: 2

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 381630 Color: 1
Size: 363380 Color: 2
Size: 254991 Color: 0

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 381497 Color: 3
Size: 336481 Color: 2
Size: 282023 Color: 0

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 381500 Color: 2
Size: 337473 Color: 1
Size: 281028 Color: 4

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 381685 Color: 1
Size: 362616 Color: 2
Size: 255700 Color: 3

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 381636 Color: 0
Size: 333092 Color: 4
Size: 285273 Color: 2

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 381736 Color: 3
Size: 352065 Color: 4
Size: 266200 Color: 0

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 381744 Color: 3
Size: 352106 Color: 3
Size: 266151 Color: 4

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 381770 Color: 3
Size: 360818 Color: 1
Size: 257413 Color: 4

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 381783 Color: 2
Size: 317856 Color: 1
Size: 300362 Color: 3

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 381810 Color: 4
Size: 347723 Color: 3
Size: 270468 Color: 2

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 381820 Color: 2
Size: 346602 Color: 3
Size: 271579 Color: 1

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 381824 Color: 0
Size: 366677 Color: 4
Size: 251500 Color: 1

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 381962 Color: 0
Size: 313726 Color: 4
Size: 304313 Color: 0

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 382055 Color: 2
Size: 311788 Color: 1
Size: 306158 Color: 4

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 382056 Color: 1
Size: 326848 Color: 0
Size: 291097 Color: 2

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 382164 Color: 0
Size: 330789 Color: 3
Size: 287048 Color: 3

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 382232 Color: 0
Size: 334000 Color: 0
Size: 283769 Color: 2

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 382305 Color: 2
Size: 339976 Color: 1
Size: 277720 Color: 4

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 382339 Color: 0
Size: 309109 Color: 3
Size: 308553 Color: 1

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 382352 Color: 2
Size: 349460 Color: 4
Size: 268189 Color: 3

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 382365 Color: 0
Size: 328620 Color: 1
Size: 289016 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 382301 Color: 1
Size: 326217 Color: 3
Size: 291483 Color: 3

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 382377 Color: 0
Size: 366314 Color: 4
Size: 251310 Color: 4

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 382503 Color: 4
Size: 324606 Color: 0
Size: 292892 Color: 3

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 382510 Color: 0
Size: 363740 Color: 0
Size: 253751 Color: 1

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 382602 Color: 2
Size: 325508 Color: 3
Size: 291891 Color: 0

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 382638 Color: 3
Size: 317856 Color: 1
Size: 299507 Color: 2

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 382646 Color: 0
Size: 343141 Color: 3
Size: 274214 Color: 2

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 382819 Color: 1
Size: 325275 Color: 3
Size: 291907 Color: 4

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 382839 Color: 4
Size: 344376 Color: 2
Size: 272786 Color: 0

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 382939 Color: 1
Size: 342353 Color: 2
Size: 274709 Color: 2

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 382895 Color: 0
Size: 360275 Color: 3
Size: 256831 Color: 1

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 382897 Color: 0
Size: 323504 Color: 2
Size: 293600 Color: 3

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 382957 Color: 3
Size: 355630 Color: 0
Size: 261414 Color: 1

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 382991 Color: 0
Size: 335984 Color: 2
Size: 281026 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 382993 Color: 0
Size: 360231 Color: 3
Size: 256777 Color: 1

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 382946 Color: 1
Size: 324193 Color: 2
Size: 292862 Color: 4

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 382998 Color: 3
Size: 314431 Color: 4
Size: 302572 Color: 1

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 383017 Color: 0
Size: 349253 Color: 0
Size: 267731 Color: 4

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 383067 Color: 3
Size: 334609 Color: 1
Size: 282325 Color: 0

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 383070 Color: 0
Size: 366625 Color: 0
Size: 250306 Color: 1

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 383090 Color: 0
Size: 312893 Color: 4
Size: 304018 Color: 3

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 383139 Color: 3
Size: 308759 Color: 3
Size: 308103 Color: 1

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 383358 Color: 1
Size: 319318 Color: 4
Size: 297325 Color: 4

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 383194 Color: 0
Size: 331367 Color: 3
Size: 285440 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 383375 Color: 1
Size: 351050 Color: 0
Size: 265576 Color: 2

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 383239 Color: 0
Size: 317616 Color: 4
Size: 299146 Color: 2

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 383482 Color: 1
Size: 357078 Color: 4
Size: 259441 Color: 3

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 383411 Color: 4
Size: 330480 Color: 1
Size: 286110 Color: 2

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 383529 Color: 1
Size: 319203 Color: 3
Size: 297269 Color: 2

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 383425 Color: 4
Size: 351155 Color: 3
Size: 265421 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 383535 Color: 1
Size: 340934 Color: 4
Size: 275532 Color: 4

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 383425 Color: 4
Size: 319497 Color: 3
Size: 297079 Color: 0

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 383529 Color: 4
Size: 359217 Color: 1
Size: 257255 Color: 3

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 383620 Color: 1
Size: 358691 Color: 0
Size: 257690 Color: 2

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 383558 Color: 4
Size: 314320 Color: 0
Size: 302123 Color: 3

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 383601 Color: 0
Size: 333857 Color: 3
Size: 282543 Color: 1

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 383640 Color: 3
Size: 343538 Color: 0
Size: 272823 Color: 4

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 383690 Color: 0
Size: 331052 Color: 1
Size: 285259 Color: 0

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 383731 Color: 2
Size: 364489 Color: 4
Size: 251781 Color: 1

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 383732 Color: 3
Size: 336485 Color: 0
Size: 279784 Color: 3

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 383811 Color: 2
Size: 316275 Color: 0
Size: 299915 Color: 1

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 383914 Color: 1
Size: 341926 Color: 4
Size: 274161 Color: 0

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 383850 Color: 3
Size: 311116 Color: 4
Size: 305035 Color: 0

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 383960 Color: 1
Size: 322549 Color: 0
Size: 293492 Color: 3

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 383972 Color: 2
Size: 360966 Color: 4
Size: 255063 Color: 1

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 384066 Color: 2
Size: 361075 Color: 0
Size: 254860 Color: 4

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 384094 Color: 0
Size: 344731 Color: 1
Size: 271176 Color: 3

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 384113 Color: 4
Size: 355315 Color: 0
Size: 260573 Color: 1

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 384130 Color: 4
Size: 333129 Color: 2
Size: 282742 Color: 3

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 384185 Color: 2
Size: 328666 Color: 4
Size: 287150 Color: 1

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 384254 Color: 3
Size: 334990 Color: 0
Size: 280757 Color: 3

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 384277 Color: 4
Size: 308619 Color: 2
Size: 307105 Color: 1

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 384464 Color: 1
Size: 324391 Color: 0
Size: 291146 Color: 4

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 384343 Color: 4
Size: 315532 Color: 3
Size: 300126 Color: 3

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 384381 Color: 2
Size: 316451 Color: 1
Size: 299169 Color: 2

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 384381 Color: 0
Size: 325688 Color: 4
Size: 289932 Color: 3

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 384381 Color: 4
Size: 347313 Color: 3
Size: 268307 Color: 1

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 384527 Color: 3
Size: 312768 Color: 4
Size: 302706 Color: 1

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 384534 Color: 3
Size: 359051 Color: 0
Size: 256416 Color: 4

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 384552 Color: 3
Size: 360059 Color: 0
Size: 255390 Color: 1

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 384595 Color: 1
Size: 362036 Color: 0
Size: 253370 Color: 3

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 384553 Color: 0
Size: 327070 Color: 3
Size: 288378 Color: 4

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 384687 Color: 1
Size: 309414 Color: 0
Size: 305900 Color: 3

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 384571 Color: 0
Size: 322110 Color: 2
Size: 293320 Color: 4

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 384682 Color: 3
Size: 343524 Color: 1
Size: 271795 Color: 4

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 384710 Color: 0
Size: 354024 Color: 1
Size: 261267 Color: 3

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 384845 Color: 2
Size: 313558 Color: 4
Size: 301598 Color: 1

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 384849 Color: 1
Size: 317972 Color: 2
Size: 297180 Color: 3

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 384875 Color: 4
Size: 354978 Color: 4
Size: 260148 Color: 3

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 385010 Color: 1
Size: 364071 Color: 2
Size: 250920 Color: 2

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 384908 Color: 0
Size: 352818 Color: 1
Size: 262275 Color: 4

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 385051 Color: 1
Size: 319992 Color: 0
Size: 294958 Color: 3

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 384924 Color: 2
Size: 344936 Color: 0
Size: 270141 Color: 4

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 385072 Color: 1
Size: 361447 Color: 0
Size: 253482 Color: 3

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 385063 Color: 2
Size: 349369 Color: 4
Size: 265569 Color: 1

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 385074 Color: 3
Size: 313071 Color: 0
Size: 301856 Color: 0

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 385124 Color: 3
Size: 338832 Color: 1
Size: 276045 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 385126 Color: 4
Size: 349043 Color: 3
Size: 265832 Color: 3

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 385214 Color: 1
Size: 343921 Color: 2
Size: 270866 Color: 0

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 385139 Color: 3
Size: 313961 Color: 2
Size: 300901 Color: 2

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 385162 Color: 4
Size: 347429 Color: 0
Size: 267410 Color: 1

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 385220 Color: 1
Size: 307592 Color: 0
Size: 307189 Color: 2

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 385189 Color: 0
Size: 340140 Color: 4
Size: 274672 Color: 1

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 385407 Color: 1
Size: 337355 Color: 3
Size: 277239 Color: 3

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 385215 Color: 0
Size: 347888 Color: 4
Size: 266898 Color: 2

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 385501 Color: 1
Size: 332474 Color: 3
Size: 282026 Color: 4

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 385235 Color: 3
Size: 308429 Color: 2
Size: 306337 Color: 3

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 385516 Color: 1
Size: 358357 Color: 2
Size: 256128 Color: 3

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 385484 Color: 2
Size: 319516 Color: 0
Size: 295001 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 385534 Color: 3
Size: 326099 Color: 0
Size: 288368 Color: 1

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 385558 Color: 1
Size: 352620 Color: 4
Size: 261823 Color: 2

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 385664 Color: 0
Size: 319825 Color: 2
Size: 294512 Color: 4

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 385696 Color: 0
Size: 342345 Color: 2
Size: 271960 Color: 1

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 385733 Color: 0
Size: 345821 Color: 4
Size: 268447 Color: 2

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 385875 Color: 2
Size: 326388 Color: 1
Size: 287738 Color: 4

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 385887 Color: 0
Size: 359500 Color: 3
Size: 254614 Color: 1

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 386084 Color: 1
Size: 311204 Color: 2
Size: 302713 Color: 3

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 386007 Color: 4
Size: 327982 Color: 3
Size: 286012 Color: 4

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 386046 Color: 4
Size: 353996 Color: 1
Size: 259959 Color: 3

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 386163 Color: 2
Size: 352956 Color: 1
Size: 260882 Color: 4

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 386177 Color: 3
Size: 315659 Color: 4
Size: 298165 Color: 2

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 386212 Color: 2
Size: 337636 Color: 3
Size: 276153 Color: 1

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 386226 Color: 3
Size: 310516 Color: 3
Size: 303259 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 386270 Color: 0
Size: 358623 Color: 0
Size: 255108 Color: 1

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 386287 Color: 0
Size: 362311 Color: 2
Size: 251403 Color: 1

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 386403 Color: 2
Size: 315902 Color: 0
Size: 297696 Color: 1

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 386331 Color: 1
Size: 314961 Color: 3
Size: 298709 Color: 2

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 386410 Color: 3
Size: 354659 Color: 3
Size: 258932 Color: 2

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 386459 Color: 3
Size: 340575 Color: 3
Size: 272967 Color: 1

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 386515 Color: 0
Size: 313175 Color: 4
Size: 300311 Color: 4

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 386567 Color: 3
Size: 358747 Color: 0
Size: 254687 Color: 1

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 386570 Color: 2
Size: 353987 Color: 1
Size: 259444 Color: 2

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 386598 Color: 4
Size: 314134 Color: 0
Size: 299269 Color: 2

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 386717 Color: 4
Size: 350005 Color: 2
Size: 263279 Color: 1

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 386819 Color: 1
Size: 352110 Color: 0
Size: 261072 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 386779 Color: 2
Size: 344468 Color: 2
Size: 268754 Color: 4

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 386946 Color: 1
Size: 349159 Color: 0
Size: 263896 Color: 0

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 387071 Color: 1
Size: 317043 Color: 3
Size: 295887 Color: 3

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 386806 Color: 2
Size: 324480 Color: 0
Size: 288715 Color: 3

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 387091 Color: 2
Size: 313440 Color: 0
Size: 299470 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 387154 Color: 3
Size: 311169 Color: 0
Size: 301678 Color: 1

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 387192 Color: 1
Size: 350467 Color: 0
Size: 262342 Color: 2

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 387216 Color: 2
Size: 351026 Color: 4
Size: 261759 Color: 4

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 387273 Color: 2
Size: 315026 Color: 3
Size: 297702 Color: 1

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 387277 Color: 4
Size: 327672 Color: 4
Size: 285052 Color: 0

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 387370 Color: 2
Size: 327627 Color: 4
Size: 285004 Color: 0

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 387384 Color: 3
Size: 320114 Color: 3
Size: 292503 Color: 1

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 387457 Color: 1
Size: 306609 Color: 2
Size: 305935 Color: 4

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 387464 Color: 2
Size: 316300 Color: 4
Size: 296237 Color: 1

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 387515 Color: 1
Size: 325290 Color: 3
Size: 287196 Color: 2

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 387472 Color: 4
Size: 333760 Color: 0
Size: 278769 Color: 3

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 387502 Color: 0
Size: 312589 Color: 2
Size: 299910 Color: 1

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 387511 Color: 0
Size: 336386 Color: 4
Size: 276104 Color: 3

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 387582 Color: 3
Size: 325272 Color: 1
Size: 287147 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 387801 Color: 1
Size: 309073 Color: 4
Size: 303127 Color: 2

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 387705 Color: 3
Size: 327550 Color: 4
Size: 284746 Color: 4

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 387834 Color: 1
Size: 355404 Color: 0
Size: 256763 Color: 0

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 387821 Color: 2
Size: 347161 Color: 3
Size: 265019 Color: 2

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 387828 Color: 2
Size: 324656 Color: 4
Size: 287517 Color: 1

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 387851 Color: 1
Size: 344234 Color: 4
Size: 267916 Color: 4

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 387837 Color: 0
Size: 345565 Color: 4
Size: 266599 Color: 4

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 387846 Color: 2
Size: 323171 Color: 1
Size: 288984 Color: 3

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 387906 Color: 2
Size: 337932 Color: 3
Size: 274163 Color: 4

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 387961 Color: 4
Size: 306132 Color: 3
Size: 305908 Color: 1

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 388068 Color: 0
Size: 311468 Color: 1
Size: 300465 Color: 2

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 388075 Color: 4
Size: 356331 Color: 0
Size: 255595 Color: 3

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 388168 Color: 2
Size: 341286 Color: 2
Size: 270547 Color: 1

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 388236 Color: 1
Size: 349211 Color: 4
Size: 262554 Color: 4

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 388218 Color: 4
Size: 342293 Color: 0
Size: 269490 Color: 0

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 388261 Color: 0
Size: 308648 Color: 1
Size: 303092 Color: 4

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 388269 Color: 0
Size: 347452 Color: 3
Size: 264280 Color: 3

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 388299 Color: 2
Size: 306528 Color: 1
Size: 305174 Color: 2

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 388354 Color: 2
Size: 347519 Color: 3
Size: 264128 Color: 2

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 388356 Color: 0
Size: 336948 Color: 1
Size: 274697 Color: 2

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 388330 Color: 1
Size: 351197 Color: 0
Size: 260474 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 388401 Color: 4
Size: 316378 Color: 3
Size: 295222 Color: 2

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 388446 Color: 4
Size: 359560 Color: 1
Size: 251995 Color: 0

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 388463 Color: 4
Size: 309706 Color: 0
Size: 301832 Color: 1

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 388587 Color: 1
Size: 318701 Color: 4
Size: 292713 Color: 2

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 388550 Color: 2
Size: 356837 Color: 3
Size: 254614 Color: 1

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 388626 Color: 1
Size: 321078 Color: 0
Size: 290297 Color: 4

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 388649 Color: 0
Size: 325206 Color: 2
Size: 286146 Color: 4

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 388694 Color: 2
Size: 344432 Color: 4
Size: 266875 Color: 1

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 388773 Color: 2
Size: 349501 Color: 1
Size: 261727 Color: 3

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 388799 Color: 2
Size: 350894 Color: 2
Size: 260308 Color: 4

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 388827 Color: 2
Size: 309112 Color: 1
Size: 302062 Color: 4

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 388881 Color: 2
Size: 352546 Color: 4
Size: 258574 Color: 4

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 388895 Color: 0
Size: 355754 Color: 1
Size: 255352 Color: 4

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 388955 Color: 1
Size: 312591 Color: 0
Size: 298455 Color: 0

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 388991 Color: 2
Size: 310073 Color: 4
Size: 300937 Color: 3

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 389029 Color: 0
Size: 324905 Color: 1
Size: 286067 Color: 2

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 389077 Color: 1
Size: 333037 Color: 3
Size: 277887 Color: 4

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 389126 Color: 4
Size: 325024 Color: 3
Size: 285851 Color: 1

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 389142 Color: 2
Size: 314011 Color: 0
Size: 296848 Color: 0

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 389289 Color: 3
Size: 320833 Color: 0
Size: 289879 Color: 1

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 389325 Color: 1
Size: 307940 Color: 0
Size: 302736 Color: 3

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 389488 Color: 0
Size: 334026 Color: 4
Size: 276487 Color: 1

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 389508 Color: 3
Size: 336866 Color: 1
Size: 273627 Color: 2

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 389537 Color: 4
Size: 331886 Color: 1
Size: 278578 Color: 2

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 389541 Color: 4
Size: 340675 Color: 0
Size: 269785 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 389564 Color: 0
Size: 311216 Color: 1
Size: 299221 Color: 3

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 389558 Color: 1
Size: 350159 Color: 4
Size: 260284 Color: 2

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 389566 Color: 4
Size: 324339 Color: 2
Size: 286096 Color: 0

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 389817 Color: 3
Size: 330662 Color: 1
Size: 279522 Color: 0

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 389843 Color: 0
Size: 312203 Color: 2
Size: 297955 Color: 2

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 389939 Color: 0
Size: 319989 Color: 0
Size: 290073 Color: 1

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 389949 Color: 1
Size: 342634 Color: 2
Size: 267418 Color: 2

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 390056 Color: 4
Size: 353331 Color: 1
Size: 256614 Color: 3

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 390075 Color: 2
Size: 336257 Color: 1
Size: 273669 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 390102 Color: 2
Size: 332119 Color: 0
Size: 277780 Color: 2

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 390190 Color: 1
Size: 328508 Color: 0
Size: 281303 Color: 2

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 390184 Color: 3
Size: 349625 Color: 2
Size: 260192 Color: 2

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 390236 Color: 2
Size: 319758 Color: 4
Size: 290007 Color: 1

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 390277 Color: 2
Size: 340881 Color: 0
Size: 268843 Color: 4

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 390318 Color: 0
Size: 318568 Color: 2
Size: 291115 Color: 1

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 390328 Color: 0
Size: 340468 Color: 1
Size: 269205 Color: 4

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 390341 Color: 3
Size: 357261 Color: 0
Size: 252399 Color: 2

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 390365 Color: 4
Size: 349863 Color: 3
Size: 259773 Color: 1

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 390415 Color: 3
Size: 307973 Color: 4
Size: 301613 Color: 4

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 390475 Color: 3
Size: 349672 Color: 1
Size: 259854 Color: 2

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 390594 Color: 0
Size: 345235 Color: 2
Size: 264172 Color: 4

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 390620 Color: 4
Size: 342566 Color: 1
Size: 266815 Color: 3

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 390651 Color: 2
Size: 305110 Color: 0
Size: 304240 Color: 0

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 390660 Color: 4
Size: 314593 Color: 2
Size: 294748 Color: 1

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 390777 Color: 0
Size: 343439 Color: 1
Size: 265785 Color: 3

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 390780 Color: 4
Size: 340736 Color: 0
Size: 268485 Color: 2

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 390808 Color: 0
Size: 326119 Color: 1
Size: 283074 Color: 2

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 390823 Color: 4
Size: 346791 Color: 2
Size: 262387 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 390911 Color: 1
Size: 358992 Color: 3
Size: 250098 Color: 4

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 390830 Color: 4
Size: 342305 Color: 2
Size: 266866 Color: 3

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 390839 Color: 4
Size: 322535 Color: 1
Size: 286627 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 390921 Color: 3
Size: 332194 Color: 2
Size: 276886 Color: 4

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 390926 Color: 2
Size: 341788 Color: 2
Size: 267287 Color: 1

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 391038 Color: 1
Size: 306874 Color: 3
Size: 302089 Color: 2

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 391021 Color: 0
Size: 306346 Color: 2
Size: 302634 Color: 3

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 391136 Color: 2
Size: 324426 Color: 2
Size: 284439 Color: 4

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 391259 Color: 4
Size: 312832 Color: 0
Size: 295910 Color: 1

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 391277 Color: 0
Size: 326146 Color: 4
Size: 282578 Color: 1

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 391287 Color: 4
Size: 319662 Color: 3
Size: 289052 Color: 1

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 391329 Color: 0
Size: 341766 Color: 3
Size: 266906 Color: 2

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 391351 Color: 2
Size: 324173 Color: 4
Size: 284477 Color: 1

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 391392 Color: 4
Size: 318872 Color: 1
Size: 289737 Color: 2

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 391445 Color: 2
Size: 355360 Color: 4
Size: 253196 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 391540 Color: 4
Size: 325456 Color: 3
Size: 283005 Color: 1

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 391641 Color: 3
Size: 346098 Color: 0
Size: 262262 Color: 2

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 391720 Color: 1
Size: 342185 Color: 3
Size: 266096 Color: 4

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 391659 Color: 0
Size: 336178 Color: 3
Size: 272164 Color: 3

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 391684 Color: 2
Size: 354317 Color: 1
Size: 254000 Color: 4

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 391742 Color: 1
Size: 339319 Color: 3
Size: 268940 Color: 3

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 391693 Color: 2
Size: 346116 Color: 3
Size: 262192 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 391768 Color: 4
Size: 311057 Color: 4
Size: 297176 Color: 1

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 391820 Color: 4
Size: 353506 Color: 2
Size: 254675 Color: 1

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 391914 Color: 0
Size: 354000 Color: 3
Size: 254087 Color: 2

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 391960 Color: 4
Size: 314199 Color: 3
Size: 293842 Color: 1

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 391972 Color: 3
Size: 326445 Color: 4
Size: 281584 Color: 4

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 391977 Color: 3
Size: 339576 Color: 3
Size: 268448 Color: 1

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 391983 Color: 3
Size: 356179 Color: 3
Size: 251839 Color: 1

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 392003 Color: 3
Size: 339417 Color: 4
Size: 268581 Color: 0

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 392017 Color: 0
Size: 319123 Color: 1
Size: 288861 Color: 0

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 391964 Color: 1
Size: 337277 Color: 0
Size: 270760 Color: 2

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 392042 Color: 3
Size: 340710 Color: 1
Size: 267249 Color: 4

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 392049 Color: 4
Size: 334573 Color: 2
Size: 273379 Color: 2

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 392119 Color: 0
Size: 337154 Color: 1
Size: 270728 Color: 4

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 392122 Color: 2
Size: 348160 Color: 1
Size: 259719 Color: 3

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 392130 Color: 3
Size: 312628 Color: 4
Size: 295243 Color: 2

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 392154 Color: 0
Size: 332593 Color: 1
Size: 275254 Color: 2

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 392155 Color: 3
Size: 320442 Color: 4
Size: 287404 Color: 4

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 392237 Color: 0
Size: 330446 Color: 3
Size: 277318 Color: 1

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 392275 Color: 3
Size: 341552 Color: 1
Size: 266174 Color: 2

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 392457 Color: 3
Size: 312963 Color: 0
Size: 294581 Color: 1

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 392475 Color: 2
Size: 355108 Color: 4
Size: 252418 Color: 2

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 392557 Color: 1
Size: 338273 Color: 3
Size: 269171 Color: 3

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 392581 Color: 1
Size: 341220 Color: 0
Size: 266200 Color: 1

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 392598 Color: 2
Size: 342088 Color: 0
Size: 265315 Color: 3

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 392683 Color: 3
Size: 337711 Color: 0
Size: 269607 Color: 1

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 392692 Color: 3
Size: 313604 Color: 1
Size: 293705 Color: 0

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 392773 Color: 3
Size: 354925 Color: 2
Size: 252303 Color: 4

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 392958 Color: 1
Size: 314792 Color: 3
Size: 292251 Color: 2

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 392879 Color: 4
Size: 306421 Color: 0
Size: 300701 Color: 4

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 393031 Color: 3
Size: 329544 Color: 4
Size: 277426 Color: 1

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 393125 Color: 1
Size: 341929 Color: 2
Size: 264947 Color: 3

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 393075 Color: 0
Size: 307940 Color: 2
Size: 298986 Color: 3

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 393098 Color: 4
Size: 355577 Color: 2
Size: 251326 Color: 1

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 393164 Color: 3
Size: 319879 Color: 3
Size: 286958 Color: 1

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 393202 Color: 3
Size: 350136 Color: 1
Size: 256663 Color: 2

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 393236 Color: 0
Size: 354991 Color: 0
Size: 251774 Color: 2

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 393366 Color: 4
Size: 303770 Color: 1
Size: 302865 Color: 2

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 393399 Color: 3
Size: 342611 Color: 4
Size: 263991 Color: 4

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 393467 Color: 0
Size: 350219 Color: 1
Size: 256315 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 393525 Color: 0
Size: 353376 Color: 2
Size: 253100 Color: 2

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 393611 Color: 2
Size: 310952 Color: 0
Size: 295438 Color: 1

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 393671 Color: 4
Size: 349582 Color: 1
Size: 256748 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 393677 Color: 2
Size: 312309 Color: 0
Size: 294015 Color: 4

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 393687 Color: 0
Size: 347554 Color: 1
Size: 258760 Color: 4

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 393707 Color: 3
Size: 331959 Color: 4
Size: 274335 Color: 4

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 393758 Color: 3
Size: 346192 Color: 2
Size: 260051 Color: 0

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 393792 Color: 0
Size: 356112 Color: 1
Size: 250097 Color: 4

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 393774 Color: 1
Size: 310662 Color: 1
Size: 295565 Color: 4

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 393891 Color: 4
Size: 347109 Color: 2
Size: 259001 Color: 0

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 393905 Color: 4
Size: 331291 Color: 1
Size: 274805 Color: 0

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 393931 Color: 3
Size: 339623 Color: 1
Size: 266447 Color: 4

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 393994 Color: 2
Size: 316771 Color: 4
Size: 289236 Color: 4

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 394028 Color: 3
Size: 336281 Color: 2
Size: 269692 Color: 4

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 394064 Color: 3
Size: 350225 Color: 1
Size: 255712 Color: 2

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 394211 Color: 1
Size: 317669 Color: 0
Size: 288121 Color: 0

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 394103 Color: 3
Size: 305626 Color: 0
Size: 300272 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 394156 Color: 3
Size: 331383 Color: 4
Size: 274462 Color: 1

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 394298 Color: 2
Size: 328090 Color: 1
Size: 277613 Color: 4

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 394365 Color: 2
Size: 336245 Color: 0
Size: 269391 Color: 1

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 394380 Color: 0
Size: 312010 Color: 0
Size: 293611 Color: 1

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 394490 Color: 3
Size: 330467 Color: 2
Size: 275044 Color: 1

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 394495 Color: 4
Size: 303337 Color: 1
Size: 302169 Color: 3

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 394566 Color: 3
Size: 332154 Color: 0
Size: 273281 Color: 2

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 394719 Color: 1
Size: 343497 Color: 2
Size: 261785 Color: 2

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 394696 Color: 2
Size: 346397 Color: 2
Size: 258908 Color: 0

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 394856 Color: 1
Size: 338247 Color: 2
Size: 266898 Color: 3

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 394729 Color: 3
Size: 336920 Color: 4
Size: 268352 Color: 0

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 394865 Color: 3
Size: 310662 Color: 0
Size: 294474 Color: 2

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 394933 Color: 3
Size: 316010 Color: 0
Size: 289058 Color: 1

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 394934 Color: 3
Size: 349166 Color: 4
Size: 255901 Color: 0

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 395142 Color: 1
Size: 317840 Color: 3
Size: 287019 Color: 2

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 394942 Color: 0
Size: 305696 Color: 0
Size: 299363 Color: 3

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 395104 Color: 3
Size: 341023 Color: 4
Size: 263874 Color: 1

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 395171 Color: 4
Size: 344244 Color: 3
Size: 260586 Color: 1

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 395199 Color: 0
Size: 343734 Color: 2
Size: 261068 Color: 3

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 395203 Color: 4
Size: 310899 Color: 0
Size: 293899 Color: 1

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 395193 Color: 1
Size: 319731 Color: 4
Size: 285077 Color: 0

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 395228 Color: 3
Size: 303781 Color: 0
Size: 300992 Color: 4

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 395279 Color: 2
Size: 346755 Color: 0
Size: 257967 Color: 1

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 395312 Color: 2
Size: 312859 Color: 4
Size: 291830 Color: 2

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 395371 Color: 4
Size: 352839 Color: 2
Size: 251791 Color: 1

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 395459 Color: 1
Size: 331139 Color: 3
Size: 273403 Color: 2

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 395408 Color: 3
Size: 340556 Color: 2
Size: 264037 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 395450 Color: 4
Size: 313973 Color: 1
Size: 290578 Color: 4

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 395539 Color: 1
Size: 352518 Color: 2
Size: 251944 Color: 0

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 395528 Color: 4
Size: 350838 Color: 3
Size: 253635 Color: 0

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 395622 Color: 1
Size: 326218 Color: 3
Size: 278161 Color: 3

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 395563 Color: 0
Size: 320751 Color: 0
Size: 283687 Color: 2

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 395707 Color: 4
Size: 324250 Color: 1
Size: 280044 Color: 2

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 395710 Color: 1
Size: 317712 Color: 3
Size: 286579 Color: 4

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 395766 Color: 2
Size: 340923 Color: 0
Size: 263312 Color: 3

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 395781 Color: 4
Size: 316547 Color: 4
Size: 287673 Color: 1

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 395869 Color: 1
Size: 304308 Color: 2
Size: 299824 Color: 2

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 395816 Color: 2
Size: 345319 Color: 3
Size: 258866 Color: 4

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 395893 Color: 0
Size: 320815 Color: 3
Size: 283293 Color: 1

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 395911 Color: 3
Size: 306578 Color: 4
Size: 297512 Color: 3

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 396039 Color: 1
Size: 331745 Color: 3
Size: 272217 Color: 4

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 395999 Color: 0
Size: 307479 Color: 2
Size: 296523 Color: 4

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 396013 Color: 2
Size: 344748 Color: 0
Size: 259240 Color: 1

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 396048 Color: 0
Size: 346956 Color: 0
Size: 256997 Color: 3

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 396076 Color: 3
Size: 338843 Color: 1
Size: 265082 Color: 2

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 396122 Color: 2
Size: 304396 Color: 1
Size: 299483 Color: 0

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 396213 Color: 4
Size: 320841 Color: 0
Size: 282947 Color: 2

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 396276 Color: 1
Size: 347485 Color: 0
Size: 256240 Color: 4

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 396268 Color: 0
Size: 332777 Color: 3
Size: 270956 Color: 3

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 396369 Color: 3
Size: 307376 Color: 1
Size: 296256 Color: 2

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 396425 Color: 4
Size: 347185 Color: 3
Size: 256391 Color: 4

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 396429 Color: 4
Size: 342331 Color: 1
Size: 261241 Color: 4

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 396434 Color: 0
Size: 340849 Color: 4
Size: 262718 Color: 1

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 396439 Color: 4
Size: 350651 Color: 0
Size: 252911 Color: 0

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 396457 Color: 4
Size: 309630 Color: 1
Size: 293914 Color: 4

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 396663 Color: 1
Size: 353178 Color: 2
Size: 250160 Color: 1

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 396535 Color: 4
Size: 301812 Color: 4
Size: 301654 Color: 2

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 396659 Color: 4
Size: 333143 Color: 3
Size: 270199 Color: 1

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 396817 Color: 1
Size: 307570 Color: 3
Size: 295614 Color: 3

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 396849 Color: 1
Size: 313716 Color: 2
Size: 289436 Color: 2

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 396727 Color: 2
Size: 303586 Color: 3
Size: 299688 Color: 4

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 396741 Color: 0
Size: 344798 Color: 4
Size: 258462 Color: 1

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 396884 Color: 1
Size: 347084 Color: 4
Size: 256033 Color: 3

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 396747 Color: 0
Size: 343207 Color: 2
Size: 260047 Color: 4

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 396790 Color: 4
Size: 337644 Color: 3
Size: 265567 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 396903 Color: 1
Size: 340798 Color: 4
Size: 262300 Color: 3

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 396850 Color: 0
Size: 307637 Color: 3
Size: 295514 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 396945 Color: 1
Size: 347872 Color: 0
Size: 255184 Color: 2

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 396908 Color: 4
Size: 331792 Color: 3
Size: 271301 Color: 3

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 396985 Color: 4
Size: 345240 Color: 1
Size: 257776 Color: 2

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 397030 Color: 4
Size: 313749 Color: 3
Size: 289222 Color: 2

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 397046 Color: 4
Size: 335135 Color: 1
Size: 267820 Color: 4

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 397066 Color: 4
Size: 303801 Color: 3
Size: 299134 Color: 1

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 397082 Color: 4
Size: 328576 Color: 4
Size: 274343 Color: 3

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 397135 Color: 3
Size: 316947 Color: 0
Size: 285919 Color: 3

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 397164 Color: 4
Size: 312072 Color: 4
Size: 290765 Color: 1

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 397157 Color: 1
Size: 309918 Color: 4
Size: 292926 Color: 4

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 397176 Color: 0
Size: 327386 Color: 4
Size: 275439 Color: 2

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 397181 Color: 2
Size: 306271 Color: 1
Size: 296549 Color: 0

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 397291 Color: 1
Size: 316878 Color: 0
Size: 285832 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 397246 Color: 3
Size: 347440 Color: 4
Size: 255315 Color: 4

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 397352 Color: 1
Size: 304572 Color: 2
Size: 298077 Color: 4

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 397252 Color: 0
Size: 308322 Color: 2
Size: 294427 Color: 3

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 397255 Color: 2
Size: 340999 Color: 1
Size: 261747 Color: 0

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 397379 Color: 1
Size: 338826 Color: 0
Size: 263796 Color: 0

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 397268 Color: 2
Size: 349667 Color: 2
Size: 253066 Color: 3

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 397449 Color: 1
Size: 352427 Color: 3
Size: 250125 Color: 3

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 397393 Color: 4
Size: 311326 Color: 3
Size: 291282 Color: 3

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 397410 Color: 2
Size: 336672 Color: 4
Size: 265919 Color: 1

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 397451 Color: 1
Size: 330443 Color: 4
Size: 272107 Color: 3

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 397455 Color: 2
Size: 307643 Color: 3
Size: 294903 Color: 1

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 397462 Color: 1
Size: 322585 Color: 4
Size: 279954 Color: 0

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 397470 Color: 2
Size: 318842 Color: 0
Size: 283689 Color: 2

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 397547 Color: 1
Size: 305386 Color: 4
Size: 297068 Color: 4

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 397639 Color: 3
Size: 336015 Color: 4
Size: 266347 Color: 1

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 397647 Color: 0
Size: 329316 Color: 1
Size: 273038 Color: 2

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 397720 Color: 2
Size: 324233 Color: 1
Size: 278048 Color: 3

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 397744 Color: 4
Size: 317543 Color: 1
Size: 284714 Color: 2

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 397750 Color: 3
Size: 322382 Color: 0
Size: 279869 Color: 4

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 397750 Color: 2
Size: 339633 Color: 0
Size: 262618 Color: 1

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 397755 Color: 0
Size: 324078 Color: 0
Size: 278168 Color: 3

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 397825 Color: 0
Size: 321648 Color: 3
Size: 280528 Color: 1

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 397828 Color: 3
Size: 341459 Color: 1
Size: 260714 Color: 3

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 397832 Color: 2
Size: 338029 Color: 0
Size: 264140 Color: 0

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 398017 Color: 1
Size: 313322 Color: 4
Size: 288662 Color: 3

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 397907 Color: 4
Size: 322562 Color: 4
Size: 279532 Color: 2

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 398012 Color: 2
Size: 321829 Color: 1
Size: 280160 Color: 4

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 398026 Color: 2
Size: 320250 Color: 1
Size: 281725 Color: 3

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 398148 Color: 3
Size: 343665 Color: 0
Size: 258188 Color: 3

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 398155 Color: 2
Size: 334459 Color: 1
Size: 267387 Color: 4

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 398125 Color: 1
Size: 347668 Color: 4
Size: 254208 Color: 0

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 398186 Color: 0
Size: 321617 Color: 2
Size: 280198 Color: 2

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 398265 Color: 3
Size: 324613 Color: 1
Size: 277123 Color: 0

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 398526 Color: 3
Size: 338949 Color: 0
Size: 262526 Color: 3

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 398548 Color: 0
Size: 333940 Color: 2
Size: 267513 Color: 1

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 398556 Color: 1
Size: 332942 Color: 4
Size: 268503 Color: 4

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 398553 Color: 2
Size: 322548 Color: 0
Size: 278900 Color: 2

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 398620 Color: 3
Size: 326803 Color: 1
Size: 274578 Color: 2

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 398680 Color: 1
Size: 312839 Color: 0
Size: 288482 Color: 2

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 398644 Color: 4
Size: 337129 Color: 2
Size: 264228 Color: 3

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 398765 Color: 1
Size: 347413 Color: 3
Size: 253823 Color: 3

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 398690 Color: 3
Size: 307478 Color: 0
Size: 293833 Color: 3

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 398750 Color: 4
Size: 346348 Color: 0
Size: 254903 Color: 1

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 398797 Color: 0
Size: 304562 Color: 0
Size: 296642 Color: 1

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 398839 Color: 3
Size: 335288 Color: 2
Size: 265874 Color: 0

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 398887 Color: 4
Size: 304142 Color: 1
Size: 296972 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 398915 Color: 0
Size: 311590 Color: 3
Size: 289496 Color: 1

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 399108 Color: 2
Size: 300527 Color: 2
Size: 300366 Color: 4

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 399155 Color: 2
Size: 319806 Color: 4
Size: 281040 Color: 1

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 399162 Color: 0
Size: 308304 Color: 3
Size: 292535 Color: 2

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 399289 Color: 2
Size: 326373 Color: 4
Size: 274339 Color: 1

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 399308 Color: 2
Size: 336859 Color: 0
Size: 263834 Color: 2

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 399450 Color: 1
Size: 300663 Color: 4
Size: 299888 Color: 4

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 399329 Color: 3
Size: 327789 Color: 0
Size: 272883 Color: 3

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 399447 Color: 0
Size: 325137 Color: 4
Size: 275417 Color: 1

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 399466 Color: 4
Size: 300512 Color: 3
Size: 300023 Color: 1

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 399472 Color: 2
Size: 333551 Color: 2
Size: 266978 Color: 3

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 399484 Color: 3
Size: 333700 Color: 0
Size: 266817 Color: 1

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 399475 Color: 1
Size: 333775 Color: 3
Size: 266751 Color: 3

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 399494 Color: 4
Size: 333308 Color: 2
Size: 267199 Color: 2

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 399522 Color: 4
Size: 330831 Color: 2
Size: 269648 Color: 3

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 399569 Color: 0
Size: 305414 Color: 4
Size: 295018 Color: 1

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 399593 Color: 1
Size: 318771 Color: 4
Size: 281637 Color: 4

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 399615 Color: 2
Size: 342872 Color: 0
Size: 257514 Color: 4

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 399644 Color: 3
Size: 339528 Color: 1
Size: 260829 Color: 3

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 399636 Color: 1
Size: 323894 Color: 3
Size: 276471 Color: 2

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 399697 Color: 3
Size: 300861 Color: 2
Size: 299443 Color: 4

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 399721 Color: 0
Size: 302496 Color: 2
Size: 297784 Color: 4

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 399772 Color: 0
Size: 334911 Color: 0
Size: 265318 Color: 1

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 399878 Color: 1
Size: 330001 Color: 3
Size: 270122 Color: 4

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 399893 Color: 2
Size: 324662 Color: 1
Size: 275446 Color: 4

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 399919 Color: 0
Size: 348042 Color: 2
Size: 252040 Color: 4

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 399969 Color: 4
Size: 323469 Color: 0
Size: 276563 Color: 1

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 400065 Color: 4
Size: 332400 Color: 4
Size: 267536 Color: 2

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 400121 Color: 2
Size: 330828 Color: 0
Size: 269052 Color: 3

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 400148 Color: 4
Size: 334951 Color: 1
Size: 264902 Color: 4

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 400234 Color: 1
Size: 316409 Color: 2
Size: 283358 Color: 4

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 400158 Color: 3
Size: 320956 Color: 0
Size: 278887 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 400368 Color: 1
Size: 311388 Color: 0
Size: 288245 Color: 4

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 400301 Color: 0
Size: 301327 Color: 3
Size: 298373 Color: 1

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 400397 Color: 1
Size: 331241 Color: 3
Size: 268363 Color: 2

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 400312 Color: 2
Size: 321911 Color: 2
Size: 277778 Color: 3

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 400423 Color: 4
Size: 343168 Color: 1
Size: 256410 Color: 3

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 400515 Color: 0
Size: 316394 Color: 1
Size: 283092 Color: 4

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 400611 Color: 0
Size: 334450 Color: 0
Size: 264940 Color: 4

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 400744 Color: 1
Size: 306133 Color: 3
Size: 293124 Color: 3

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 400701 Color: 3
Size: 314631 Color: 4
Size: 284669 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 400810 Color: 4
Size: 340395 Color: 1
Size: 258796 Color: 3

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 400841 Color: 0
Size: 333727 Color: 3
Size: 265433 Color: 4

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 400848 Color: 3
Size: 300915 Color: 1
Size: 298238 Color: 4

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 400936 Color: 1
Size: 329951 Color: 2
Size: 269114 Color: 0

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 400952 Color: 4
Size: 336551 Color: 0
Size: 262498 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 401020 Color: 0
Size: 310878 Color: 4
Size: 288103 Color: 2

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 401033 Color: 4
Size: 335028 Color: 2
Size: 263940 Color: 1

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 401044 Color: 3
Size: 332557 Color: 1
Size: 266400 Color: 0

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 401059 Color: 2
Size: 308879 Color: 0
Size: 290063 Color: 4

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 401136 Color: 1
Size: 334467 Color: 4
Size: 264398 Color: 4

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 401096 Color: 4
Size: 306047 Color: 3
Size: 292858 Color: 0

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 401188 Color: 2
Size: 316444 Color: 1
Size: 282369 Color: 2

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 401201 Color: 0
Size: 314071 Color: 1
Size: 284729 Color: 3

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 401226 Color: 2
Size: 313142 Color: 4
Size: 285633 Color: 0

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 401302 Color: 4
Size: 321906 Color: 1
Size: 276793 Color: 2

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 401311 Color: 3
Size: 319424 Color: 0
Size: 279266 Color: 1

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 401323 Color: 4
Size: 304123 Color: 2
Size: 294555 Color: 3

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 401323 Color: 0
Size: 312404 Color: 2
Size: 286274 Color: 1

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 401345 Color: 2
Size: 344647 Color: 4
Size: 254009 Color: 2

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 401412 Color: 0
Size: 338868 Color: 0
Size: 259721 Color: 1

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 401460 Color: 0
Size: 315879 Color: 2
Size: 282662 Color: 1

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 401489 Color: 4
Size: 312808 Color: 3
Size: 285704 Color: 4

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 401551 Color: 2
Size: 329185 Color: 0
Size: 269265 Color: 4

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 401575 Color: 2
Size: 307453 Color: 0
Size: 290973 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 401696 Color: 4
Size: 317473 Color: 3
Size: 280832 Color: 0

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 401711 Color: 0
Size: 330284 Color: 3
Size: 268006 Color: 1

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 401926 Color: 1
Size: 341545 Color: 4
Size: 256530 Color: 3

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 401893 Color: 3
Size: 319172 Color: 4
Size: 278936 Color: 0

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 401964 Color: 2
Size: 342646 Color: 2
Size: 255391 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 401970 Color: 3
Size: 331960 Color: 1
Size: 266071 Color: 2

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 401986 Color: 3
Size: 316904 Color: 2
Size: 281111 Color: 0

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 402079 Color: 3
Size: 328931 Color: 1
Size: 268991 Color: 3

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 402157 Color: 1
Size: 315656 Color: 2
Size: 282188 Color: 2

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 402118 Color: 0
Size: 344193 Color: 2
Size: 253690 Color: 3

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 402133 Color: 3
Size: 313202 Color: 1
Size: 284666 Color: 4

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 402175 Color: 4
Size: 308018 Color: 2
Size: 289808 Color: 2

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 402220 Color: 4
Size: 337707 Color: 1
Size: 260074 Color: 0

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 402236 Color: 1
Size: 342067 Color: 0
Size: 255698 Color: 0

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 402233 Color: 4
Size: 312528 Color: 0
Size: 285240 Color: 2

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 402238 Color: 0
Size: 325314 Color: 1
Size: 272449 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 402292 Color: 4
Size: 332791 Color: 0
Size: 264918 Color: 3

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 402345 Color: 1
Size: 327813 Color: 0
Size: 269843 Color: 2

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 402386 Color: 0
Size: 324632 Color: 1
Size: 272983 Color: 0

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 402593 Color: 1
Size: 316197 Color: 2
Size: 281211 Color: 0

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 402569 Color: 0
Size: 331865 Color: 3
Size: 265567 Color: 1

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 402632 Color: 4
Size: 342111 Color: 1
Size: 255258 Color: 4

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 402706 Color: 1
Size: 346685 Color: 0
Size: 250610 Color: 2

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 402758 Color: 2
Size: 329301 Color: 3
Size: 267942 Color: 3

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 402826 Color: 2
Size: 305035 Color: 4
Size: 292140 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 402864 Color: 4
Size: 326506 Color: 1
Size: 270631 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 402985 Color: 3
Size: 340908 Color: 4
Size: 256108 Color: 4

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 403097 Color: 2
Size: 312166 Color: 1
Size: 284738 Color: 3

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 403114 Color: 4
Size: 305038 Color: 3
Size: 291849 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 403144 Color: 0
Size: 339247 Color: 2
Size: 257610 Color: 1

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 403187 Color: 2
Size: 344242 Color: 0
Size: 252572 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 403261 Color: 4
Size: 313820 Color: 0
Size: 282920 Color: 1

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 403293 Color: 2
Size: 319996 Color: 2
Size: 276712 Color: 3

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 403358 Color: 3
Size: 312688 Color: 0
Size: 283955 Color: 1

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 403422 Color: 2
Size: 346208 Color: 1
Size: 250371 Color: 2

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 403438 Color: 4
Size: 340828 Color: 2
Size: 255735 Color: 2

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 403600 Color: 1
Size: 330973 Color: 3
Size: 265428 Color: 4

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 403590 Color: 2
Size: 307796 Color: 0
Size: 288615 Color: 4

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 403604 Color: 2
Size: 317205 Color: 0
Size: 279192 Color: 1

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 403630 Color: 4
Size: 313730 Color: 0
Size: 282641 Color: 3

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 403648 Color: 0
Size: 344970 Color: 1
Size: 251383 Color: 2

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 403677 Color: 3
Size: 315176 Color: 2
Size: 281148 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 403756 Color: 1
Size: 346242 Color: 2
Size: 250003 Color: 4

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 403737 Color: 3
Size: 304024 Color: 0
Size: 292240 Color: 4

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 403761 Color: 2
Size: 327939 Color: 1
Size: 268301 Color: 4

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 403834 Color: 1
Size: 343736 Color: 0
Size: 252431 Color: 4

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 403820 Color: 0
Size: 313820 Color: 2
Size: 282361 Color: 1

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 403956 Color: 1
Size: 321940 Color: 4
Size: 274105 Color: 4

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 403871 Color: 4
Size: 323011 Color: 4
Size: 273119 Color: 2

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 403962 Color: 0
Size: 343319 Color: 0
Size: 252720 Color: 1

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 403980 Color: 0
Size: 326811 Color: 3
Size: 269210 Color: 3

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 404013 Color: 2
Size: 324835 Color: 1
Size: 271153 Color: 2

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 404128 Color: 4
Size: 333376 Color: 3
Size: 262497 Color: 4

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 404329 Color: 1
Size: 341531 Color: 0
Size: 254141 Color: 3

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 404337 Color: 0
Size: 322343 Color: 3
Size: 273321 Color: 2

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 404369 Color: 4
Size: 310240 Color: 2
Size: 285392 Color: 1

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 404388 Color: 4
Size: 310342 Color: 3
Size: 285271 Color: 0

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 404697 Color: 1
Size: 332947 Color: 3
Size: 262357 Color: 3

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 404723 Color: 3
Size: 303051 Color: 0
Size: 292227 Color: 1

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 404828 Color: 0
Size: 302352 Color: 3
Size: 292821 Color: 3

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 404945 Color: 2
Size: 317565 Color: 2
Size: 277491 Color: 1

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 404950 Color: 2
Size: 338550 Color: 4
Size: 256501 Color: 4

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 404992 Color: 2
Size: 341742 Color: 1
Size: 253267 Color: 4

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 404963 Color: 1
Size: 339880 Color: 0
Size: 255158 Color: 4

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 405029 Color: 2
Size: 321761 Color: 0
Size: 273211 Color: 4

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 405086 Color: 0
Size: 344525 Color: 1
Size: 250390 Color: 0

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 405104 Color: 3
Size: 300812 Color: 3
Size: 294085 Color: 0

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 405106 Color: 0
Size: 329279 Color: 1
Size: 265616 Color: 4

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 405213 Color: 1
Size: 341791 Color: 3
Size: 252997 Color: 4

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 405158 Color: 0
Size: 317161 Color: 2
Size: 277682 Color: 2

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 405233 Color: 3
Size: 301548 Color: 0
Size: 293220 Color: 1

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 405215 Color: 1
Size: 326190 Color: 4
Size: 268596 Color: 4

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 405249 Color: 4
Size: 333869 Color: 2
Size: 260883 Color: 3

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 405295 Color: 3
Size: 330082 Color: 2
Size: 264624 Color: 1

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 405346 Color: 3
Size: 336079 Color: 4
Size: 258576 Color: 1

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 405355 Color: 4
Size: 342843 Color: 2
Size: 251803 Color: 0

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 405452 Color: 2
Size: 321974 Color: 1
Size: 272575 Color: 4

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 405469 Color: 1
Size: 300329 Color: 2
Size: 294203 Color: 3

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 405580 Color: 3
Size: 338370 Color: 4
Size: 256051 Color: 1

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 405585 Color: 4
Size: 314959 Color: 0
Size: 279457 Color: 3

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 405593 Color: 3
Size: 322090 Color: 2
Size: 272318 Color: 1

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 405617 Color: 0
Size: 336742 Color: 4
Size: 257642 Color: 1

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 405666 Color: 3
Size: 342032 Color: 0
Size: 252303 Color: 0

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 405796 Color: 2
Size: 321603 Color: 1
Size: 272602 Color: 3

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 405828 Color: 2
Size: 316890 Color: 4
Size: 277283 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 405926 Color: 3
Size: 299697 Color: 1
Size: 294378 Color: 3

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 405932 Color: 2
Size: 334131 Color: 2
Size: 259938 Color: 4

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 405951 Color: 3
Size: 312797 Color: 0
Size: 281253 Color: 1

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 405986 Color: 4
Size: 298083 Color: 3
Size: 295932 Color: 3

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 406126 Color: 1
Size: 302341 Color: 0
Size: 291534 Color: 3

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 406002 Color: 3
Size: 342563 Color: 0
Size: 251436 Color: 4

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 406171 Color: 1
Size: 319341 Color: 0
Size: 274489 Color: 3

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 406190 Color: 3
Size: 328146 Color: 1
Size: 265665 Color: 3

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 406248 Color: 3
Size: 309869 Color: 1
Size: 283884 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 406362 Color: 3
Size: 327644 Color: 0
Size: 265995 Color: 0

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 406482 Color: 4
Size: 333219 Color: 1
Size: 260300 Color: 2

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 406543 Color: 0
Size: 299698 Color: 1
Size: 293760 Color: 3

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 406564 Color: 3
Size: 300592 Color: 1
Size: 292845 Color: 3

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 406587 Color: 0
Size: 315349 Color: 0
Size: 278065 Color: 2

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 406675 Color: 1
Size: 314934 Color: 0
Size: 278392 Color: 0

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 406691 Color: 4
Size: 302168 Color: 2
Size: 291142 Color: 4

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 406776 Color: 3
Size: 303088 Color: 0
Size: 290137 Color: 1

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 406839 Color: 1
Size: 297677 Color: 3
Size: 295485 Color: 3

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 406840 Color: 4
Size: 307770 Color: 4
Size: 285391 Color: 2

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 406868 Color: 4
Size: 296882 Color: 1
Size: 296251 Color: 3

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 406912 Color: 4
Size: 333527 Color: 0
Size: 259562 Color: 0

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 406937 Color: 0
Size: 307770 Color: 1
Size: 285294 Color: 2

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 406984 Color: 4
Size: 317730 Color: 2
Size: 275287 Color: 1

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 407022 Color: 2
Size: 339979 Color: 3
Size: 253000 Color: 4

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 407108 Color: 1
Size: 303186 Color: 2
Size: 289707 Color: 0

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 407207 Color: 4
Size: 340687 Color: 2
Size: 252107 Color: 3

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 407299 Color: 1
Size: 314443 Color: 2
Size: 278259 Color: 4

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 407218 Color: 4
Size: 305800 Color: 3
Size: 286983 Color: 2

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 407322 Color: 0
Size: 329648 Color: 0
Size: 263031 Color: 1

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 407446 Color: 1
Size: 334764 Color: 4
Size: 257791 Color: 4

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 407350 Color: 2
Size: 337530 Color: 2
Size: 255121 Color: 0

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 407396 Color: 0
Size: 333230 Color: 4
Size: 259375 Color: 1

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 407490 Color: 1
Size: 316648 Color: 0
Size: 275863 Color: 3

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 407417 Color: 4
Size: 333795 Color: 3
Size: 258789 Color: 2

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 407581 Color: 4
Size: 306738 Color: 3
Size: 285682 Color: 0

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 407595 Color: 3
Size: 329421 Color: 1
Size: 262985 Color: 2

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 407637 Color: 1
Size: 302229 Color: 2
Size: 290135 Color: 3

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 407659 Color: 2
Size: 304859 Color: 0
Size: 287483 Color: 2

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 407862 Color: 1
Size: 334664 Color: 4
Size: 257475 Color: 2

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 407775 Color: 0
Size: 339090 Color: 3
Size: 253136 Color: 3

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 407880 Color: 4
Size: 339318 Color: 1
Size: 252803 Color: 4

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 407894 Color: 0
Size: 314042 Color: 4
Size: 278065 Color: 3

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 407995 Color: 0
Size: 323492 Color: 4
Size: 268514 Color: 1

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 407983 Color: 1
Size: 334960 Color: 2
Size: 257058 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 407996 Color: 0
Size: 332400 Color: 1
Size: 259605 Color: 2

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 408091 Color: 3
Size: 307796 Color: 4
Size: 284114 Color: 1

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 408170 Color: 2
Size: 333650 Color: 0
Size: 258181 Color: 3

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 408211 Color: 2
Size: 339409 Color: 1
Size: 252381 Color: 4

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 408219 Color: 3
Size: 336316 Color: 4
Size: 255466 Color: 1

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 408282 Color: 4
Size: 322282 Color: 2
Size: 269437 Color: 0

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 408387 Color: 1
Size: 315633 Color: 2
Size: 275981 Color: 4

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 408300 Color: 2
Size: 312262 Color: 0
Size: 279439 Color: 0

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 408514 Color: 1
Size: 331144 Color: 0
Size: 260343 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 408527 Color: 1
Size: 295765 Color: 2
Size: 295709 Color: 3

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 408302 Color: 3
Size: 324701 Color: 2
Size: 266998 Color: 4

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 408573 Color: 1
Size: 323692 Color: 2
Size: 267736 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 408586 Color: 1
Size: 341012 Color: 3
Size: 250403 Color: 2

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 408546 Color: 2
Size: 332601 Color: 4
Size: 258854 Color: 0

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 408693 Color: 4
Size: 332977 Color: 1
Size: 258331 Color: 0

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 408782 Color: 4
Size: 327246 Color: 3
Size: 263973 Color: 1

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 409031 Color: 1
Size: 324329 Color: 0
Size: 266641 Color: 4

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 408938 Color: 2
Size: 319587 Color: 2
Size: 271476 Color: 4

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 408980 Color: 4
Size: 334293 Color: 4
Size: 256728 Color: 1

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 409085 Color: 2
Size: 311869 Color: 4
Size: 279047 Color: 4

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 409097 Color: 0
Size: 322566 Color: 4
Size: 268338 Color: 1

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 409106 Color: 0
Size: 322264 Color: 4
Size: 268631 Color: 0

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 409121 Color: 3
Size: 328363 Color: 4
Size: 262517 Color: 1

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 409144 Color: 2
Size: 302135 Color: 4
Size: 288722 Color: 1

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 409208 Color: 3
Size: 303919 Color: 4
Size: 286874 Color: 0

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 409238 Color: 2
Size: 337744 Color: 1
Size: 253019 Color: 4

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 409350 Color: 2
Size: 312856 Color: 0
Size: 277795 Color: 3

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 409363 Color: 2
Size: 305169 Color: 0
Size: 285469 Color: 1

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 409417 Color: 3
Size: 300714 Color: 1
Size: 289870 Color: 2

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 409551 Color: 4
Size: 299241 Color: 3
Size: 291209 Color: 1

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 409634 Color: 4
Size: 329086 Color: 3
Size: 261281 Color: 0

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 409716 Color: 3
Size: 307469 Color: 1
Size: 282816 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 409658 Color: 1
Size: 331360 Color: 2
Size: 258983 Color: 0

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 409823 Color: 0
Size: 337276 Color: 3
Size: 252902 Color: 2

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 409893 Color: 2
Size: 326375 Color: 3
Size: 263733 Color: 1

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 409917 Color: 3
Size: 311642 Color: 2
Size: 278442 Color: 2

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 409948 Color: 0
Size: 297716 Color: 4
Size: 292337 Color: 1

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 409951 Color: 1
Size: 295237 Color: 4
Size: 294813 Color: 4

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 409993 Color: 2
Size: 320562 Color: 0
Size: 269446 Color: 2

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 410051 Color: 1
Size: 316764 Color: 4
Size: 273186 Color: 0

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 410028 Color: 3
Size: 331133 Color: 3
Size: 258840 Color: 4

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 410059 Color: 4
Size: 304937 Color: 3
Size: 285005 Color: 1

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 410067 Color: 4
Size: 315241 Color: 2
Size: 274693 Color: 1

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 410073 Color: 3
Size: 337648 Color: 2
Size: 252280 Color: 2

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 410091 Color: 0
Size: 325376 Color: 1
Size: 264534 Color: 4

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 410137 Color: 0
Size: 300677 Color: 2
Size: 289187 Color: 2

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 410180 Color: 4
Size: 309813 Color: 1
Size: 280008 Color: 3

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 410198 Color: 4
Size: 338203 Color: 0
Size: 251600 Color: 3

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 410373 Color: 1
Size: 320596 Color: 4
Size: 269032 Color: 2

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 410288 Color: 4
Size: 323652 Color: 0
Size: 266061 Color: 0

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 410467 Color: 4
Size: 317105 Color: 1
Size: 272429 Color: 3

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 410732 Color: 4
Size: 326702 Color: 3
Size: 262567 Color: 3

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 410797 Color: 2
Size: 328555 Color: 1
Size: 260649 Color: 3

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 410924 Color: 2
Size: 339045 Color: 4
Size: 250032 Color: 2

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 410953 Color: 0
Size: 337552 Color: 2
Size: 251496 Color: 1

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 410960 Color: 2
Size: 303997 Color: 3
Size: 285044 Color: 4

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 410964 Color: 4
Size: 307371 Color: 3
Size: 281666 Color: 1

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 410988 Color: 4
Size: 320190 Color: 4
Size: 268823 Color: 2

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 411043 Color: 4
Size: 324831 Color: 1
Size: 264127 Color: 3

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 411049 Color: 4
Size: 311006 Color: 3
Size: 277946 Color: 3

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 411098 Color: 1
Size: 297488 Color: 2
Size: 291415 Color: 3

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 411155 Color: 4
Size: 332067 Color: 2
Size: 256779 Color: 4

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 411336 Color: 1
Size: 316755 Color: 3
Size: 271910 Color: 4

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 411291 Color: 4
Size: 322341 Color: 3
Size: 266369 Color: 0

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 411402 Color: 0
Size: 307875 Color: 0
Size: 280724 Color: 1

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 411552 Color: 4
Size: 318806 Color: 3
Size: 269643 Color: 1

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 411736 Color: 4
Size: 315759 Color: 1
Size: 272506 Color: 4

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 411789 Color: 0
Size: 315047 Color: 3
Size: 273165 Color: 3

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 411847 Color: 0
Size: 337422 Color: 2
Size: 250732 Color: 1

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 411861 Color: 1
Size: 297522 Color: 3
Size: 290618 Color: 2

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 411857 Color: 4
Size: 310510 Color: 0
Size: 277634 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 411969 Color: 1
Size: 336538 Color: 0
Size: 251494 Color: 3

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 411870 Color: 0
Size: 320599 Color: 3
Size: 267532 Color: 4

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 412090 Color: 1
Size: 314320 Color: 2
Size: 273591 Color: 2

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 412126 Color: 1
Size: 313287 Color: 3
Size: 274588 Color: 3

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 411901 Color: 0
Size: 331278 Color: 4
Size: 256822 Color: 4

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 412133 Color: 1
Size: 335212 Color: 0
Size: 252656 Color: 0

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 412005 Color: 3
Size: 325688 Color: 4
Size: 262308 Color: 0

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 412353 Color: 1
Size: 331202 Color: 0
Size: 256446 Color: 4

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 412272 Color: 2
Size: 326463 Color: 4
Size: 261266 Color: 3

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 412331 Color: 2
Size: 325836 Color: 1
Size: 261834 Color: 3

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 412342 Color: 4
Size: 317188 Color: 0
Size: 270471 Color: 0

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 412374 Color: 1
Size: 324054 Color: 4
Size: 263573 Color: 3

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 412455 Color: 3
Size: 334955 Color: 0
Size: 252591 Color: 0

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 412533 Color: 2
Size: 328380 Color: 1
Size: 259088 Color: 0

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 412535 Color: 2
Size: 329545 Color: 4
Size: 257921 Color: 0

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 412545 Color: 3
Size: 297438 Color: 3
Size: 290018 Color: 1

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 412641 Color: 3
Size: 324132 Color: 2
Size: 263228 Color: 1

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 412706 Color: 0
Size: 321768 Color: 2
Size: 265527 Color: 2

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 412984 Color: 4
Size: 329444 Color: 4
Size: 257573 Color: 0

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 413078 Color: 1
Size: 309596 Color: 2
Size: 277327 Color: 2

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 413164 Color: 0
Size: 296210 Color: 3
Size: 290627 Color: 2

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 413167 Color: 0
Size: 294075 Color: 3
Size: 292759 Color: 1

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 413209 Color: 2
Size: 319157 Color: 4
Size: 267635 Color: 4

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 413219 Color: 3
Size: 318228 Color: 1
Size: 268554 Color: 3

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 413274 Color: 3
Size: 300524 Color: 2
Size: 286203 Color: 4

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 413288 Color: 2
Size: 295062 Color: 1
Size: 291651 Color: 2

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 413291 Color: 0
Size: 317358 Color: 2
Size: 269352 Color: 1

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 413618 Color: 3
Size: 334355 Color: 2
Size: 252028 Color: 4

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 413713 Color: 3
Size: 300610 Color: 1
Size: 285678 Color: 2

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 413811 Color: 1
Size: 326054 Color: 4
Size: 260136 Color: 2

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 413755 Color: 2
Size: 316357 Color: 0
Size: 269889 Color: 4

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 413822 Color: 4
Size: 322606 Color: 0
Size: 263573 Color: 0

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 413852 Color: 4
Size: 331676 Color: 3
Size: 254473 Color: 2

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 413990 Color: 3
Size: 313895 Color: 1
Size: 272116 Color: 0

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 413990 Color: 1
Size: 297934 Color: 3
Size: 288077 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 414172 Color: 0
Size: 296786 Color: 3
Size: 289043 Color: 0

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 414234 Color: 2
Size: 295868 Color: 1
Size: 289899 Color: 2

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 414279 Color: 0
Size: 329869 Color: 1
Size: 255853 Color: 3

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 414293 Color: 0
Size: 317241 Color: 4
Size: 268467 Color: 4

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 414387 Color: 1
Size: 318164 Color: 2
Size: 267450 Color: 3

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 414350 Color: 0
Size: 329524 Color: 2
Size: 256127 Color: 2

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 414365 Color: 3
Size: 318748 Color: 1
Size: 266888 Color: 2

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 414403 Color: 1
Size: 334770 Color: 0
Size: 250828 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 414404 Color: 4
Size: 304631 Color: 4
Size: 280966 Color: 3

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 414433 Color: 4
Size: 323613 Color: 2
Size: 261955 Color: 1

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 414497 Color: 1
Size: 330898 Color: 0
Size: 254606 Color: 4

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 414434 Color: 2
Size: 307103 Color: 0
Size: 278464 Color: 0

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 414591 Color: 1
Size: 325843 Color: 3
Size: 259567 Color: 0

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 414581 Color: 3
Size: 298715 Color: 0
Size: 286705 Color: 2

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 414619 Color: 4
Size: 305356 Color: 1
Size: 280026 Color: 2

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 414624 Color: 2
Size: 314861 Color: 4
Size: 270516 Color: 1

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 414645 Color: 0
Size: 329178 Color: 2
Size: 256178 Color: 2

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 414690 Color: 2
Size: 310102 Color: 1
Size: 275209 Color: 2

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 414826 Color: 4
Size: 299488 Color: 3
Size: 285687 Color: 1

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 414935 Color: 1
Size: 325529 Color: 3
Size: 259537 Color: 0

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 414885 Color: 3
Size: 307042 Color: 1
Size: 278074 Color: 2

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 414987 Color: 3
Size: 298265 Color: 3
Size: 286749 Color: 4

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 415188 Color: 1
Size: 323620 Color: 3
Size: 261193 Color: 3

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 415329 Color: 3
Size: 305401 Color: 0
Size: 279271 Color: 1

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 415371 Color: 2
Size: 293958 Color: 4
Size: 290672 Color: 3

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 415478 Color: 3
Size: 312278 Color: 1
Size: 272245 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 415497 Color: 4
Size: 296196 Color: 1
Size: 288308 Color: 4

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 415530 Color: 0
Size: 330309 Color: 0
Size: 254162 Color: 1

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 415563 Color: 4
Size: 313431 Color: 0
Size: 271007 Color: 2

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 415564 Color: 4
Size: 328812 Color: 1
Size: 255625 Color: 3

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 415717 Color: 3
Size: 297154 Color: 2
Size: 287130 Color: 4

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 415587 Color: 0
Size: 308156 Color: 3
Size: 276258 Color: 2

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 415717 Color: 1
Size: 314529 Color: 1
Size: 269755 Color: 3

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 415756 Color: 4
Size: 329941 Color: 4
Size: 254304 Color: 0

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 415792 Color: 0
Size: 297041 Color: 3
Size: 287168 Color: 1

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 415860 Color: 4
Size: 300074 Color: 0
Size: 284067 Color: 1

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 415868 Color: 2
Size: 328077 Color: 4
Size: 256056 Color: 3

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 415875 Color: 2
Size: 302719 Color: 1
Size: 281407 Color: 4

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 415977 Color: 4
Size: 325836 Color: 2
Size: 258188 Color: 2

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 416167 Color: 1
Size: 328079 Color: 4
Size: 255755 Color: 3

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 416130 Color: 3
Size: 318128 Color: 4
Size: 265743 Color: 1

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 416165 Color: 3
Size: 330739 Color: 2
Size: 253097 Color: 2

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 416329 Color: 4
Size: 333268 Color: 1
Size: 250404 Color: 3

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 416364 Color: 3
Size: 314989 Color: 4
Size: 268648 Color: 1

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 416530 Color: 1
Size: 312435 Color: 3
Size: 271036 Color: 4

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 416427 Color: 0
Size: 329496 Color: 2
Size: 254078 Color: 4

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 416564 Color: 1
Size: 295342 Color: 3
Size: 288095 Color: 2

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 416489 Color: 4
Size: 332423 Color: 1
Size: 251089 Color: 3

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 416578 Color: 1
Size: 305391 Color: 4
Size: 278032 Color: 4

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 416493 Color: 3
Size: 308877 Color: 2
Size: 274631 Color: 2

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 416590 Color: 2
Size: 302480 Color: 4
Size: 280931 Color: 1

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 416707 Color: 1
Size: 315022 Color: 4
Size: 268272 Color: 4

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 416754 Color: 1
Size: 325418 Color: 2
Size: 257829 Color: 4

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 416752 Color: 4
Size: 298597 Color: 2
Size: 284652 Color: 2

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 416800 Color: 0
Size: 328022 Color: 2
Size: 255179 Color: 3

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 416800 Color: 1
Size: 326099 Color: 4
Size: 257102 Color: 3

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 416868 Color: 3
Size: 313987 Color: 0
Size: 269146 Color: 4

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 416925 Color: 2
Size: 330587 Color: 1
Size: 252489 Color: 3

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 416954 Color: 4
Size: 304493 Color: 1
Size: 278554 Color: 2

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 416981 Color: 3
Size: 329463 Color: 4
Size: 253557 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 416993 Color: 0
Size: 315004 Color: 1
Size: 268004 Color: 4

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 417164 Color: 1
Size: 320455 Color: 2
Size: 262382 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 417051 Color: 0
Size: 300661 Color: 3
Size: 282289 Color: 0

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 417337 Color: 1
Size: 299838 Color: 0
Size: 282826 Color: 3

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 417157 Color: 0
Size: 293647 Color: 2
Size: 289197 Color: 4

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 417255 Color: 2
Size: 296420 Color: 1
Size: 286326 Color: 4

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 417356 Color: 1
Size: 314910 Color: 0
Size: 267735 Color: 0

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 417290 Color: 4
Size: 326213 Color: 3
Size: 256498 Color: 2

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 417317 Color: 0
Size: 302240 Color: 4
Size: 280444 Color: 1

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 417317 Color: 0
Size: 295158 Color: 2
Size: 287526 Color: 4

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 417599 Color: 1
Size: 294443 Color: 4
Size: 287959 Color: 3

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 417531 Color: 4
Size: 294607 Color: 3
Size: 287863 Color: 1

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 417627 Color: 1
Size: 315633 Color: 0
Size: 266741 Color: 4

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 417653 Color: 3
Size: 331630 Color: 0
Size: 250718 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 417739 Color: 1
Size: 315531 Color: 3
Size: 266731 Color: 3

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 417754 Color: 2
Size: 300455 Color: 0
Size: 281792 Color: 3

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 417872 Color: 1
Size: 312118 Color: 0
Size: 270011 Color: 3

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 417881 Color: 0
Size: 318547 Color: 3
Size: 263573 Color: 1

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 418155 Color: 2
Size: 325969 Color: 1
Size: 255877 Color: 3

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 418188 Color: 3
Size: 292316 Color: 2
Size: 289497 Color: 1

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 418200 Color: 3
Size: 308690 Color: 0
Size: 273111 Color: 2

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 418344 Color: 0
Size: 328544 Color: 1
Size: 253113 Color: 3

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 418344 Color: 4
Size: 305511 Color: 2
Size: 276146 Color: 4

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 418351 Color: 0
Size: 310045 Color: 1
Size: 271605 Color: 4

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 418356 Color: 3
Size: 314789 Color: 2
Size: 266856 Color: 1

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 418398 Color: 2
Size: 328672 Color: 3
Size: 252931 Color: 2

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 418435 Color: 4
Size: 331413 Color: 0
Size: 250153 Color: 1

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 418495 Color: 3
Size: 302185 Color: 4
Size: 279321 Color: 4

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 418528 Color: 1
Size: 327845 Color: 2
Size: 253628 Color: 2

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 418570 Color: 0
Size: 316463 Color: 0
Size: 264968 Color: 4

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 418717 Color: 2
Size: 331079 Color: 1
Size: 250205 Color: 4

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 418744 Color: 4
Size: 301208 Color: 3
Size: 280049 Color: 3

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 418769 Color: 2
Size: 304266 Color: 0
Size: 276966 Color: 1

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 418750 Color: 1
Size: 299854 Color: 0
Size: 281397 Color: 4

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 418791 Color: 3
Size: 310616 Color: 4
Size: 270594 Color: 2

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 418904 Color: 1
Size: 325956 Color: 0
Size: 255141 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 419011 Color: 3
Size: 307394 Color: 4
Size: 273596 Color: 0

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 419188 Color: 1
Size: 293421 Color: 3
Size: 287392 Color: 0

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 419244 Color: 1
Size: 312814 Color: 3
Size: 267943 Color: 3

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 419200 Color: 2
Size: 307521 Color: 1
Size: 273280 Color: 3

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 419243 Color: 3
Size: 325498 Color: 2
Size: 255260 Color: 2

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 419422 Color: 0
Size: 322830 Color: 1
Size: 257749 Color: 2

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 419506 Color: 3
Size: 327758 Color: 4
Size: 252737 Color: 1

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 419519 Color: 1
Size: 330364 Color: 2
Size: 250118 Color: 0

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 419541 Color: 0
Size: 310707 Color: 4
Size: 269753 Color: 2

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 419669 Color: 1
Size: 299605 Color: 2
Size: 280727 Color: 3

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 419711 Color: 1
Size: 304074 Color: 4
Size: 276216 Color: 1

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 419744 Color: 1
Size: 319715 Color: 0
Size: 260542 Color: 4

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 419709 Color: 4
Size: 313567 Color: 0
Size: 266725 Color: 3

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 419881 Color: 3
Size: 328270 Color: 0
Size: 251850 Color: 2

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 419897 Color: 2
Size: 312239 Color: 1
Size: 267865 Color: 3

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 419908 Color: 4
Size: 311305 Color: 3
Size: 268788 Color: 2

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 420062 Color: 3
Size: 316455 Color: 0
Size: 263484 Color: 1

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 420121 Color: 0
Size: 306719 Color: 4
Size: 273161 Color: 4

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 420297 Color: 1
Size: 321997 Color: 0
Size: 257707 Color: 3

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 420275 Color: 0
Size: 308917 Color: 2
Size: 270809 Color: 4

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 420355 Color: 1
Size: 289898 Color: 4
Size: 289748 Color: 4

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 420404 Color: 2
Size: 295450 Color: 0
Size: 284147 Color: 4

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 420406 Color: 2
Size: 327629 Color: 3
Size: 251966 Color: 1

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 420439 Color: 1
Size: 310149 Color: 0
Size: 269413 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 420529 Color: 4
Size: 293064 Color: 3
Size: 286408 Color: 2

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 420638 Color: 1
Size: 298926 Color: 0
Size: 280437 Color: 3

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 420701 Color: 0
Size: 304377 Color: 4
Size: 274923 Color: 2

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 420706 Color: 0
Size: 328610 Color: 0
Size: 250685 Color: 1

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 420756 Color: 3
Size: 310147 Color: 2
Size: 269098 Color: 1

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 420835 Color: 1
Size: 297100 Color: 2
Size: 282066 Color: 2

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 420764 Color: 2
Size: 324159 Color: 0
Size: 255078 Color: 0

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 420860 Color: 1
Size: 306299 Color: 0
Size: 272842 Color: 2

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 420843 Color: 2
Size: 319497 Color: 2
Size: 259661 Color: 3

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 420854 Color: 0
Size: 315762 Color: 0
Size: 263385 Color: 1

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 420861 Color: 1
Size: 318282 Color: 2
Size: 260858 Color: 4

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 420863 Color: 0
Size: 297874 Color: 3
Size: 281264 Color: 4

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 420877 Color: 2
Size: 308587 Color: 1
Size: 270537 Color: 0

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 420938 Color: 1
Size: 324511 Color: 0
Size: 254552 Color: 4

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 420917 Color: 2
Size: 313781 Color: 3
Size: 265303 Color: 3

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 420925 Color: 4
Size: 296358 Color: 1
Size: 282718 Color: 0

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 420963 Color: 4
Size: 328306 Color: 3
Size: 250732 Color: 1

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 421009 Color: 2
Size: 307234 Color: 1
Size: 271758 Color: 3

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 421015 Color: 2
Size: 293811 Color: 4
Size: 285175 Color: 0

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 421176 Color: 1
Size: 301994 Color: 0
Size: 276831 Color: 3

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 421238 Color: 3
Size: 320729 Color: 4
Size: 258034 Color: 1

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 421244 Color: 4
Size: 292900 Color: 2
Size: 285857 Color: 2

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 421356 Color: 1
Size: 290201 Color: 4
Size: 288444 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 421499 Color: 1
Size: 312364 Color: 0
Size: 266138 Color: 4

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 421464 Color: 4
Size: 326854 Color: 0
Size: 251683 Color: 0

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 421520 Color: 0
Size: 326574 Color: 3
Size: 251907 Color: 1

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 421525 Color: 4
Size: 325779 Color: 0
Size: 252697 Color: 2

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 421581 Color: 4
Size: 310220 Color: 2
Size: 268200 Color: 1

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 421620 Color: 4
Size: 300748 Color: 4
Size: 277633 Color: 2

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 421641 Color: 0
Size: 324041 Color: 4
Size: 254319 Color: 1

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 421594 Color: 1
Size: 294852 Color: 2
Size: 283555 Color: 3

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 421691 Color: 4
Size: 320509 Color: 2
Size: 257801 Color: 0

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 421749 Color: 3
Size: 319212 Color: 1
Size: 259040 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 421754 Color: 4
Size: 295745 Color: 0
Size: 282502 Color: 3

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 421832 Color: 2
Size: 315235 Color: 1
Size: 262934 Color: 4

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 421930 Color: 1
Size: 319119 Color: 0
Size: 258952 Color: 2

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 421978 Color: 2
Size: 301500 Color: 0
Size: 276523 Color: 4

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 422014 Color: 2
Size: 297939 Color: 2
Size: 280048 Color: 4

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 422032 Color: 2
Size: 325982 Color: 3
Size: 251987 Color: 1

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 422082 Color: 1
Size: 306884 Color: 4
Size: 271035 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 422126 Color: 0
Size: 313678 Color: 2
Size: 264197 Color: 3

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 422286 Color: 4
Size: 327642 Color: 1
Size: 250073 Color: 2

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 422277 Color: 1
Size: 308906 Color: 4
Size: 268818 Color: 3

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 422288 Color: 0
Size: 324875 Color: 2
Size: 252838 Color: 2

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 422332 Color: 4
Size: 290018 Color: 2
Size: 287651 Color: 3

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 422332 Color: 1
Size: 296200 Color: 1
Size: 281469 Color: 0

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 422365 Color: 1
Size: 323596 Color: 4
Size: 254040 Color: 3

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 422639 Color: 0
Size: 309052 Color: 3
Size: 268310 Color: 1

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 422699 Color: 0
Size: 308481 Color: 4
Size: 268821 Color: 1

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 422713 Color: 0
Size: 306375 Color: 4
Size: 270913 Color: 3

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 422728 Color: 3
Size: 323738 Color: 3
Size: 253535 Color: 1

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 422764 Color: 2
Size: 313762 Color: 3
Size: 263475 Color: 4

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 422808 Color: 4
Size: 296247 Color: 1
Size: 280946 Color: 3

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 422812 Color: 3
Size: 322698 Color: 2
Size: 254491 Color: 1

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 422864 Color: 2
Size: 309047 Color: 4
Size: 268090 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 422884 Color: 0
Size: 313794 Color: 1
Size: 263323 Color: 4

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 422913 Color: 2
Size: 312252 Color: 1
Size: 264836 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 422929 Color: 0
Size: 310103 Color: 4
Size: 266969 Color: 3

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 422943 Color: 0
Size: 306125 Color: 1
Size: 270933 Color: 3

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 423060 Color: 3
Size: 304884 Color: 2
Size: 272057 Color: 0

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 423086 Color: 2
Size: 302963 Color: 1
Size: 273952 Color: 2

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 423191 Color: 4
Size: 309084 Color: 4
Size: 267726 Color: 1

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 423203 Color: 3
Size: 318375 Color: 2
Size: 258423 Color: 3

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 423216 Color: 0
Size: 297908 Color: 2
Size: 278877 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 423331 Color: 4
Size: 312779 Color: 0
Size: 263891 Color: 1

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 423349 Color: 0
Size: 323816 Color: 2
Size: 252836 Color: 1

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 423399 Color: 4
Size: 305764 Color: 2
Size: 270838 Color: 3

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 423420 Color: 4
Size: 319479 Color: 1
Size: 257102 Color: 4

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 423484 Color: 0
Size: 296949 Color: 2
Size: 279568 Color: 3

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 423542 Color: 1
Size: 324487 Color: 3
Size: 251972 Color: 0

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 423525 Color: 0
Size: 323216 Color: 4
Size: 253260 Color: 3

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 423558 Color: 0
Size: 315670 Color: 1
Size: 260773 Color: 3

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 423560 Color: 4
Size: 313568 Color: 3
Size: 262873 Color: 1

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 423776 Color: 3
Size: 302426 Color: 1
Size: 273799 Color: 0

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 423879 Color: 1
Size: 312447 Color: 0
Size: 263675 Color: 3

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 423830 Color: 0
Size: 322753 Color: 3
Size: 253418 Color: 3

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 423936 Color: 4
Size: 324331 Color: 2
Size: 251734 Color: 1

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 423952 Color: 3
Size: 315499 Color: 0
Size: 260550 Color: 4

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 424093 Color: 2
Size: 297519 Color: 0
Size: 278389 Color: 1

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 424286 Color: 1
Size: 289305 Color: 2
Size: 286410 Color: 2

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 424259 Color: 0
Size: 296402 Color: 4
Size: 279340 Color: 4

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 424328 Color: 3
Size: 312528 Color: 1
Size: 263145 Color: 4

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 424350 Color: 2
Size: 295233 Color: 3
Size: 280418 Color: 3

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 424386 Color: 0
Size: 310645 Color: 3
Size: 264970 Color: 1

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 424445 Color: 1
Size: 292894 Color: 3
Size: 282662 Color: 2

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 424418 Color: 4
Size: 294576 Color: 1
Size: 281007 Color: 4

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 424436 Color: 3
Size: 307887 Color: 4
Size: 267678 Color: 3

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 424615 Color: 1
Size: 321757 Color: 0
Size: 253629 Color: 2

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 424474 Color: 0
Size: 292868 Color: 2
Size: 282659 Color: 4

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 424796 Color: 0
Size: 324888 Color: 3
Size: 250317 Color: 1

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 424809 Color: 0
Size: 314659 Color: 1
Size: 260533 Color: 3

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 424969 Color: 4
Size: 301864 Color: 2
Size: 273168 Color: 3

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 424980 Color: 4
Size: 310319 Color: 1
Size: 264702 Color: 3

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 424981 Color: 3
Size: 295572 Color: 0
Size: 279448 Color: 0

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 425067 Color: 1
Size: 322192 Color: 0
Size: 252742 Color: 0

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 425110 Color: 2
Size: 315533 Color: 2
Size: 259358 Color: 3

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 425115 Color: 2
Size: 299761 Color: 0
Size: 275125 Color: 1

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 425168 Color: 3
Size: 294067 Color: 4
Size: 280766 Color: 1

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 425268 Color: 4
Size: 314817 Color: 2
Size: 259916 Color: 0

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 425303 Color: 4
Size: 297867 Color: 0
Size: 276831 Color: 1

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 425350 Color: 1
Size: 321005 Color: 2
Size: 253646 Color: 3

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 425317 Color: 0
Size: 324599 Color: 0
Size: 250085 Color: 2

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 425373 Color: 4
Size: 306932 Color: 1
Size: 267696 Color: 3

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 425380 Color: 4
Size: 320604 Color: 0
Size: 254017 Color: 0

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 425407 Color: 2
Size: 293312 Color: 1
Size: 281282 Color: 4

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 425428 Color: 2
Size: 312236 Color: 3
Size: 262337 Color: 2

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 425540 Color: 0
Size: 307863 Color: 1
Size: 266598 Color: 4

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 425739 Color: 0
Size: 320397 Color: 0
Size: 253865 Color: 3

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 425796 Color: 4
Size: 288644 Color: 0
Size: 285561 Color: 1

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 425874 Color: 2
Size: 312075 Color: 3
Size: 262052 Color: 1

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 425883 Color: 3
Size: 317676 Color: 2
Size: 256442 Color: 2

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 425973 Color: 2
Size: 320906 Color: 1
Size: 253122 Color: 3

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 426059 Color: 0
Size: 290365 Color: 2
Size: 283577 Color: 2

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 426069 Color: 0
Size: 292106 Color: 2
Size: 281826 Color: 1

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 426103 Color: 0
Size: 323357 Color: 0
Size: 250541 Color: 4

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 426141 Color: 0
Size: 304331 Color: 0
Size: 269529 Color: 1

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 426246 Color: 1
Size: 305126 Color: 3
Size: 268629 Color: 4

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 426305 Color: 2
Size: 301374 Color: 4
Size: 272322 Color: 3

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 426328 Color: 0
Size: 302996 Color: 1
Size: 270677 Color: 1

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 426352 Color: 0
Size: 315467 Color: 4
Size: 258182 Color: 2

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 426522 Color: 1
Size: 306558 Color: 0
Size: 266921 Color: 3

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 426362 Color: 3
Size: 317046 Color: 4
Size: 256593 Color: 2

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 426560 Color: 3
Size: 287356 Color: 1
Size: 286085 Color: 4

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 426578 Color: 2
Size: 301220 Color: 1
Size: 272203 Color: 2

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 426732 Color: 3
Size: 320805 Color: 2
Size: 252464 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 426845 Color: 1
Size: 310126 Color: 0
Size: 263030 Color: 4

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 426797 Color: 2
Size: 304190 Color: 4
Size: 269014 Color: 3

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 426878 Color: 3
Size: 315311 Color: 1
Size: 257812 Color: 0

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 426881 Color: 2
Size: 289887 Color: 0
Size: 283233 Color: 4

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 426885 Color: 0
Size: 288795 Color: 2
Size: 284321 Color: 1

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 426923 Color: 1
Size: 302863 Color: 0
Size: 270215 Color: 4

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 426913 Color: 4
Size: 287171 Color: 2
Size: 285917 Color: 4

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 426950 Color: 1
Size: 289846 Color: 2
Size: 283205 Color: 0

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 427008 Color: 4
Size: 312256 Color: 3
Size: 260737 Color: 2

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 427039 Color: 0
Size: 299427 Color: 2
Size: 273535 Color: 1

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 427075 Color: 4
Size: 316260 Color: 4
Size: 256666 Color: 3

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 427109 Color: 2
Size: 294873 Color: 2
Size: 278019 Color: 1

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 427174 Color: 0
Size: 289138 Color: 2
Size: 283689 Color: 3

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 427269 Color: 3
Size: 321070 Color: 1
Size: 251662 Color: 2

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 427270 Color: 3
Size: 287155 Color: 2
Size: 285576 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 427320 Color: 0
Size: 290367 Color: 3
Size: 282314 Color: 1

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 427421 Color: 3
Size: 302730 Color: 4
Size: 269850 Color: 1

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 427429 Color: 3
Size: 319199 Color: 4
Size: 253373 Color: 4

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 427535 Color: 3
Size: 303496 Color: 4
Size: 268970 Color: 1

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 427552 Color: 2
Size: 291819 Color: 1
Size: 280630 Color: 0

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 427559 Color: 0
Size: 316735 Color: 2
Size: 255707 Color: 3

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 427621 Color: 4
Size: 316472 Color: 3
Size: 255908 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 427646 Color: 0
Size: 316391 Color: 2
Size: 255964 Color: 1

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 427657 Color: 3
Size: 295462 Color: 2
Size: 276882 Color: 1

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 427691 Color: 2
Size: 307331 Color: 3
Size: 264979 Color: 0

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 427795 Color: 2
Size: 297967 Color: 3
Size: 274239 Color: 2

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 427889 Color: 1
Size: 320228 Color: 3
Size: 251884 Color: 2

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 427954 Color: 2
Size: 307112 Color: 0
Size: 264935 Color: 3

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 428047 Color: 4
Size: 287762 Color: 2
Size: 284192 Color: 0

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 428266 Color: 2
Size: 311157 Color: 4
Size: 260578 Color: 1

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 428399 Color: 4
Size: 314957 Color: 4
Size: 256645 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 428455 Color: 3
Size: 313597 Color: 1
Size: 257949 Color: 2

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 428485 Color: 3
Size: 288952 Color: 3
Size: 282564 Color: 1

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 428622 Color: 3
Size: 289690 Color: 2
Size: 281689 Color: 4

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 428652 Color: 2
Size: 309966 Color: 3
Size: 261383 Color: 1

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 428681 Color: 3
Size: 310054 Color: 4
Size: 261266 Color: 3

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 428828 Color: 1
Size: 301046 Color: 3
Size: 270127 Color: 3

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 428732 Color: 3
Size: 308603 Color: 4
Size: 262666 Color: 2

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 428799 Color: 2
Size: 304212 Color: 4
Size: 266990 Color: 1

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 428881 Color: 2
Size: 288133 Color: 1
Size: 282987 Color: 3

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 428908 Color: 0
Size: 304346 Color: 3
Size: 266747 Color: 2

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 428921 Color: 3
Size: 299271 Color: 0
Size: 271809 Color: 1

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 428995 Color: 2
Size: 309722 Color: 1
Size: 261284 Color: 2

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 429139 Color: 4
Size: 288480 Color: 0
Size: 282382 Color: 3

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 429281 Color: 1
Size: 311939 Color: 4
Size: 258781 Color: 3

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 429239 Color: 0
Size: 299090 Color: 2
Size: 271672 Color: 3

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 429282 Color: 1
Size: 320387 Color: 0
Size: 250332 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 429343 Color: 4
Size: 318338 Color: 4
Size: 252320 Color: 2

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 429365 Color: 2
Size: 319058 Color: 1
Size: 251578 Color: 2

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 429579 Color: 1
Size: 296152 Color: 4
Size: 274270 Color: 2

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 429431 Color: 2
Size: 300486 Color: 0
Size: 270084 Color: 4

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 429671 Color: 1
Size: 286930 Color: 0
Size: 283400 Color: 3

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 429568 Color: 0
Size: 310818 Color: 2
Size: 259615 Color: 4

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 429743 Color: 1
Size: 300526 Color: 0
Size: 269732 Color: 3

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 429607 Color: 2
Size: 313413 Color: 0
Size: 256981 Color: 0

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 429719 Color: 0
Size: 293112 Color: 1
Size: 277170 Color: 3

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 429838 Color: 3
Size: 309675 Color: 1
Size: 260488 Color: 3

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 429915 Color: 1
Size: 317513 Color: 4
Size: 252573 Color: 0

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 429988 Color: 2
Size: 292473 Color: 0
Size: 277540 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 430005 Color: 4
Size: 287365 Color: 4
Size: 282631 Color: 1

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 430034 Color: 0
Size: 318364 Color: 2
Size: 251603 Color: 2

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 430036 Color: 2
Size: 295436 Color: 4
Size: 274529 Color: 1

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 430069 Color: 0
Size: 295048 Color: 2
Size: 274884 Color: 1

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 430205 Color: 4
Size: 300153 Color: 0
Size: 269643 Color: 2

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 430221 Color: 0
Size: 308287 Color: 1
Size: 261493 Color: 4

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 430326 Color: 0
Size: 303142 Color: 1
Size: 266533 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 430428 Color: 1
Size: 297117 Color: 0
Size: 272456 Color: 4

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 430399 Color: 2
Size: 313265 Color: 1
Size: 256337 Color: 4

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 430487 Color: 1
Size: 308738 Color: 0
Size: 260776 Color: 0

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 430406 Color: 0
Size: 318083 Color: 3
Size: 251512 Color: 4

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 430470 Color: 0
Size: 296411 Color: 1
Size: 273120 Color: 4

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 430499 Color: 4
Size: 288989 Color: 0
Size: 280513 Color: 3

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 430512 Color: 4
Size: 306158 Color: 2
Size: 263331 Color: 1

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 430536 Color: 4
Size: 287130 Color: 2
Size: 282335 Color: 1

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 430650 Color: 4
Size: 290326 Color: 2
Size: 279025 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 430689 Color: 2
Size: 315972 Color: 2
Size: 253340 Color: 1

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 430734 Color: 1
Size: 305742 Color: 4
Size: 263525 Color: 0

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 430745 Color: 4
Size: 306687 Color: 3
Size: 262569 Color: 3

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 430751 Color: 3
Size: 313207 Color: 2
Size: 256043 Color: 1

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 430773 Color: 2
Size: 287411 Color: 0
Size: 281817 Color: 4

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 430953 Color: 1
Size: 316875 Color: 4
Size: 252173 Color: 3

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 431011 Color: 1
Size: 314169 Color: 4
Size: 254821 Color: 2

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 431038 Color: 1
Size: 317664 Color: 2
Size: 251299 Color: 4

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 430870 Color: 0
Size: 315960 Color: 0
Size: 253171 Color: 2

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 431076 Color: 1
Size: 306887 Color: 0
Size: 262038 Color: 3

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 431080 Color: 1
Size: 314201 Color: 3
Size: 254720 Color: 2

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 431012 Color: 4
Size: 301533 Color: 3
Size: 267456 Color: 0

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 431070 Color: 4
Size: 308275 Color: 1
Size: 260656 Color: 3

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 431124 Color: 1
Size: 312421 Color: 3
Size: 256456 Color: 4

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 431090 Color: 4
Size: 284655 Color: 2
Size: 284256 Color: 3

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 431285 Color: 1
Size: 307790 Color: 3
Size: 260926 Color: 2

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 431204 Color: 4
Size: 318358 Color: 2
Size: 250439 Color: 3

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 431210 Color: 2
Size: 307552 Color: 1
Size: 261239 Color: 3

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 431314 Color: 1
Size: 315372 Color: 0
Size: 253315 Color: 4

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 431421 Color: 1
Size: 308239 Color: 3
Size: 260341 Color: 3

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 431468 Color: 3
Size: 287547 Color: 1
Size: 280986 Color: 4

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 431643 Color: 1
Size: 314634 Color: 2
Size: 253724 Color: 4

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 431635 Color: 4
Size: 308752 Color: 3
Size: 259614 Color: 1

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 431729 Color: 0
Size: 302788 Color: 4
Size: 265484 Color: 1

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 431871 Color: 2
Size: 285559 Color: 1
Size: 282571 Color: 3

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 431900 Color: 2
Size: 288633 Color: 1
Size: 279468 Color: 0

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 431908 Color: 0
Size: 298394 Color: 2
Size: 269699 Color: 3

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 432077 Color: 1
Size: 291071 Color: 2
Size: 276853 Color: 0

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 431993 Color: 0
Size: 302611 Color: 2
Size: 265397 Color: 4

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 432028 Color: 0
Size: 291689 Color: 1
Size: 276284 Color: 3

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 432138 Color: 4
Size: 299116 Color: 1
Size: 268747 Color: 0

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 432210 Color: 2
Size: 316852 Color: 0
Size: 250939 Color: 4

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 432269 Color: 1
Size: 292633 Color: 3
Size: 275099 Color: 2

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 432284 Color: 2
Size: 297064 Color: 3
Size: 270653 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 432419 Color: 4
Size: 286502 Color: 0
Size: 281080 Color: 3

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 432437 Color: 0
Size: 290944 Color: 1
Size: 276620 Color: 3

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 432497 Color: 1
Size: 284994 Color: 2
Size: 282510 Color: 0

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 432440 Color: 2
Size: 295196 Color: 0
Size: 272365 Color: 2

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 432522 Color: 1
Size: 300225 Color: 0
Size: 267254 Color: 0

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 432582 Color: 0
Size: 308248 Color: 2
Size: 259171 Color: 2

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 432666 Color: 2
Size: 288964 Color: 4
Size: 278371 Color: 3

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 432757 Color: 2
Size: 284222 Color: 1
Size: 283022 Color: 4

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 432762 Color: 3
Size: 285195 Color: 1
Size: 282044 Color: 0

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 432840 Color: 2
Size: 304846 Color: 4
Size: 262315 Color: 1

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 432889 Color: 2
Size: 311499 Color: 0
Size: 255613 Color: 4

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 432914 Color: 2
Size: 296152 Color: 1
Size: 270935 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 432939 Color: 1
Size: 292958 Color: 0
Size: 274104 Color: 4

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 432953 Color: 4
Size: 303402 Color: 0
Size: 263646 Color: 2

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 432960 Color: 4
Size: 297195 Color: 3
Size: 269846 Color: 1

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 433104 Color: 3
Size: 283619 Color: 3
Size: 283278 Color: 1

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 433264 Color: 1
Size: 296997 Color: 2
Size: 269740 Color: 3

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 433148 Color: 4
Size: 299699 Color: 0
Size: 267154 Color: 4

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 433276 Color: 3
Size: 294655 Color: 0
Size: 272070 Color: 1

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 433310 Color: 4
Size: 308184 Color: 1
Size: 258507 Color: 0

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 433338 Color: 0
Size: 315770 Color: 0
Size: 250893 Color: 2

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 433379 Color: 3
Size: 300433 Color: 0
Size: 266189 Color: 1

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 433418 Color: 2
Size: 303526 Color: 3
Size: 263057 Color: 4

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 433451 Color: 3
Size: 297112 Color: 4
Size: 269438 Color: 1

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 433514 Color: 1
Size: 304197 Color: 2
Size: 262290 Color: 3

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 433550 Color: 3
Size: 314183 Color: 2
Size: 252268 Color: 1

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 433909 Color: 1
Size: 315750 Color: 0
Size: 250342 Color: 3

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 433948 Color: 3
Size: 307190 Color: 4
Size: 258863 Color: 3

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 433991 Color: 4
Size: 297587 Color: 2
Size: 268423 Color: 1

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 434005 Color: 0
Size: 297734 Color: 1
Size: 268262 Color: 3

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 434024 Color: 4
Size: 295425 Color: 2
Size: 270552 Color: 0

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 434052 Color: 4
Size: 297851 Color: 1
Size: 268098 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 434052 Color: 1
Size: 284867 Color: 0
Size: 281082 Color: 3

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 434150 Color: 4
Size: 292304 Color: 4
Size: 273547 Color: 0

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 434173 Color: 0
Size: 285988 Color: 1
Size: 279840 Color: 4

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 434182 Color: 3
Size: 284592 Color: 2
Size: 281227 Color: 4

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 434206 Color: 0
Size: 295011 Color: 1
Size: 270784 Color: 3

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 434227 Color: 2
Size: 294584 Color: 0
Size: 271190 Color: 1

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 434297 Color: 2
Size: 314605 Color: 1
Size: 251099 Color: 4

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 434304 Color: 3
Size: 285742 Color: 2
Size: 279955 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 434365 Color: 4
Size: 313531 Color: 3
Size: 252105 Color: 1

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 434474 Color: 1
Size: 307495 Color: 0
Size: 258032 Color: 4

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 434544 Color: 4
Size: 300519 Color: 3
Size: 264938 Color: 3

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 434645 Color: 1
Size: 285149 Color: 0
Size: 280207 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 434926 Color: 2
Size: 290443 Color: 0
Size: 274632 Color: 4

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 435013 Color: 1
Size: 310624 Color: 4
Size: 254364 Color: 3

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 434969 Color: 4
Size: 300584 Color: 3
Size: 264448 Color: 0

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 434992 Color: 3
Size: 294388 Color: 4
Size: 270621 Color: 1

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 435057 Color: 2
Size: 304964 Color: 1
Size: 259980 Color: 0

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 435092 Color: 3
Size: 298147 Color: 0
Size: 266762 Color: 2

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 435212 Color: 1
Size: 300440 Color: 4
Size: 264349 Color: 3

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 435508 Color: 1
Size: 294850 Color: 2
Size: 269643 Color: 2

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 435392 Color: 3
Size: 302939 Color: 2
Size: 261670 Color: 2

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 435490 Color: 0
Size: 298746 Color: 1
Size: 265765 Color: 4

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 435571 Color: 4
Size: 283861 Color: 0
Size: 280569 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 435626 Color: 4
Size: 289446 Color: 1
Size: 274929 Color: 3

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 435709 Color: 3
Size: 284675 Color: 4
Size: 279617 Color: 1

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 435757 Color: 4
Size: 286601 Color: 1
Size: 277643 Color: 0

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 435823 Color: 2
Size: 305553 Color: 0
Size: 258625 Color: 3

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 435979 Color: 1
Size: 288651 Color: 3
Size: 275371 Color: 4

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 435866 Color: 0
Size: 297490 Color: 3
Size: 266645 Color: 2

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 436072 Color: 1
Size: 300283 Color: 2
Size: 263646 Color: 0

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 436102 Color: 1
Size: 288526 Color: 0
Size: 275373 Color: 4

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 435909 Color: 2
Size: 296921 Color: 4
Size: 267171 Color: 0

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 435999 Color: 2
Size: 311261 Color: 3
Size: 252741 Color: 4

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 436235 Color: 1
Size: 304229 Color: 2
Size: 259537 Color: 0

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 436334 Color: 1
Size: 308954 Color: 2
Size: 254713 Color: 2

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 436054 Color: 2
Size: 298838 Color: 0
Size: 265109 Color: 4

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 436367 Color: 1
Size: 291391 Color: 4
Size: 272243 Color: 4

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 436403 Color: 0
Size: 289942 Color: 4
Size: 273656 Color: 4

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 436510 Color: 4
Size: 294454 Color: 1
Size: 269037 Color: 0

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 436581 Color: 2
Size: 299855 Color: 0
Size: 263565 Color: 4

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 436663 Color: 0
Size: 312179 Color: 1
Size: 251159 Color: 2

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 436699 Color: 0
Size: 288537 Color: 1
Size: 274765 Color: 2

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 436718 Color: 2
Size: 299943 Color: 3
Size: 263340 Color: 3

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 436773 Color: 3
Size: 305715 Color: 4
Size: 257513 Color: 1

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 436834 Color: 2
Size: 309200 Color: 4
Size: 253967 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 436842 Color: 2
Size: 286541 Color: 4
Size: 276618 Color: 1

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 436847 Color: 2
Size: 294989 Color: 1
Size: 268165 Color: 3

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 436911 Color: 3
Size: 282426 Color: 2
Size: 280664 Color: 3

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 436992 Color: 2
Size: 307087 Color: 1
Size: 255922 Color: 0

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 437002 Color: 1
Size: 299429 Color: 2
Size: 263570 Color: 2

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 437024 Color: 2
Size: 298305 Color: 0
Size: 264672 Color: 3

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 437257 Color: 4
Size: 290011 Color: 1
Size: 272733 Color: 0

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 437283 Color: 0
Size: 311392 Color: 2
Size: 251326 Color: 1

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 437434 Color: 3
Size: 285611 Color: 4
Size: 276956 Color: 1

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 437506 Color: 0
Size: 311924 Color: 1
Size: 250571 Color: 4

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 437511 Color: 3
Size: 301973 Color: 0
Size: 260517 Color: 3

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 437546 Color: 2
Size: 285368 Color: 3
Size: 277087 Color: 1

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 437600 Color: 1
Size: 285455 Color: 4
Size: 276946 Color: 4

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 437569 Color: 0
Size: 300922 Color: 2
Size: 261510 Color: 0

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 437719 Color: 1
Size: 292721 Color: 2
Size: 269561 Color: 3

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 437810 Color: 1
Size: 299685 Color: 4
Size: 262506 Color: 3

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 437750 Color: 4
Size: 309214 Color: 3
Size: 253037 Color: 2

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 437963 Color: 1
Size: 298655 Color: 2
Size: 263383 Color: 3

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 437992 Color: 1
Size: 309264 Color: 0
Size: 252745 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 438141 Color: 0
Size: 292603 Color: 3
Size: 269257 Color: 4

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 438170 Color: 0
Size: 299210 Color: 4
Size: 262621 Color: 1

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 438207 Color: 3
Size: 307173 Color: 1
Size: 254621 Color: 0

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 438222 Color: 2
Size: 290195 Color: 4
Size: 271584 Color: 4

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 438282 Color: 1
Size: 304334 Color: 4
Size: 257385 Color: 2

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 438247 Color: 4
Size: 308429 Color: 4
Size: 253325 Color: 3

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 438260 Color: 3
Size: 290146 Color: 1
Size: 271595 Color: 4

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 438307 Color: 1
Size: 288520 Color: 2
Size: 273174 Color: 3

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 438344 Color: 3
Size: 298546 Color: 2
Size: 263111 Color: 0

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 438364 Color: 4
Size: 310198 Color: 1
Size: 251439 Color: 2

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 438452 Color: 4
Size: 293941 Color: 4
Size: 267608 Color: 1

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 438535 Color: 3
Size: 300666 Color: 0
Size: 260800 Color: 3

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 438611 Color: 3
Size: 294344 Color: 2
Size: 267046 Color: 1

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 438698 Color: 2
Size: 303639 Color: 3
Size: 257664 Color: 4

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 438728 Color: 4
Size: 291664 Color: 1
Size: 269609 Color: 2

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 438755 Color: 0
Size: 299715 Color: 1
Size: 261531 Color: 0

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 438779 Color: 2
Size: 298254 Color: 3
Size: 262968 Color: 4

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 438814 Color: 4
Size: 310626 Color: 1
Size: 250561 Color: 2

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 438837 Color: 1
Size: 296663 Color: 4
Size: 264501 Color: 3

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 438911 Color: 4
Size: 294794 Color: 0
Size: 266296 Color: 3

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 438930 Color: 2
Size: 288334 Color: 1
Size: 272737 Color: 4

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 438932 Color: 4
Size: 310911 Color: 0
Size: 250158 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 438982 Color: 4
Size: 285542 Color: 3
Size: 275477 Color: 1

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 438982 Color: 0
Size: 289120 Color: 4
Size: 271899 Color: 0

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 439144 Color: 1
Size: 292258 Color: 3
Size: 268599 Color: 2

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 439018 Color: 2
Size: 301486 Color: 0
Size: 259497 Color: 3

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 439059 Color: 3
Size: 304942 Color: 1
Size: 256000 Color: 0

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 439230 Color: 3
Size: 304283 Color: 0
Size: 256488 Color: 1

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 439301 Color: 3
Size: 308115 Color: 1
Size: 252585 Color: 3

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 439303 Color: 1
Size: 300210 Color: 0
Size: 260488 Color: 3

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 439344 Color: 3
Size: 285377 Color: 2
Size: 275280 Color: 0

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 439429 Color: 3
Size: 305766 Color: 1
Size: 254806 Color: 3

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 439490 Color: 3
Size: 280335 Color: 4
Size: 280176 Color: 4

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 439542 Color: 3
Size: 295082 Color: 4
Size: 265377 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 439662 Color: 4
Size: 282340 Color: 3
Size: 277999 Color: 3

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 439720 Color: 0
Size: 298207 Color: 1
Size: 262074 Color: 4

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 439739 Color: 0
Size: 301505 Color: 0
Size: 258757 Color: 1

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 439803 Color: 0
Size: 280760 Color: 4
Size: 279438 Color: 2

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 439837 Color: 3
Size: 289776 Color: 1
Size: 270388 Color: 2

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 439842 Color: 4
Size: 294101 Color: 4
Size: 266058 Color: 3

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 440029 Color: 1
Size: 295749 Color: 2
Size: 264223 Color: 4

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 439995 Color: 4
Size: 289576 Color: 3
Size: 270430 Color: 0

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 440210 Color: 1
Size: 282703 Color: 2
Size: 277088 Color: 3

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 440244 Color: 1
Size: 308448 Color: 0
Size: 251309 Color: 4

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 440434 Color: 4
Size: 283831 Color: 1
Size: 275736 Color: 0

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 440438 Color: 0
Size: 287355 Color: 4
Size: 272208 Color: 4

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 440480 Color: 3
Size: 308380 Color: 0
Size: 251141 Color: 1

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 440581 Color: 3
Size: 305116 Color: 0
Size: 254304 Color: 1

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 440657 Color: 0
Size: 287985 Color: 2
Size: 271359 Color: 4

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 440675 Color: 3
Size: 292314 Color: 2
Size: 267012 Color: 4

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 440681 Color: 3
Size: 295070 Color: 2
Size: 264250 Color: 1

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 440737 Color: 1
Size: 305461 Color: 3
Size: 253803 Color: 2

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 440716 Color: 2
Size: 280472 Color: 2
Size: 278813 Color: 4

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 440871 Color: 1
Size: 280140 Color: 0
Size: 278990 Color: 0

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 440922 Color: 0
Size: 284353 Color: 3
Size: 274726 Color: 1

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 440951 Color: 3
Size: 299922 Color: 2
Size: 259128 Color: 4

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 440972 Color: 0
Size: 305886 Color: 1
Size: 253143 Color: 4

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 440977 Color: 4
Size: 297856 Color: 2
Size: 261168 Color: 1

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 441011 Color: 4
Size: 297361 Color: 0
Size: 261629 Color: 3

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 441014 Color: 0
Size: 305273 Color: 1
Size: 253714 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 441112 Color: 1
Size: 298208 Color: 2
Size: 260681 Color: 3

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 441020 Color: 2
Size: 300036 Color: 4
Size: 258945 Color: 4

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 441120 Color: 1
Size: 307802 Color: 4
Size: 251079 Color: 2

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 441086 Color: 4
Size: 306373 Color: 0
Size: 252542 Color: 2

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 441160 Color: 0
Size: 292240 Color: 2
Size: 266601 Color: 1

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 441189 Color: 1
Size: 283330 Color: 2
Size: 275482 Color: 2

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 441206 Color: 2
Size: 285883 Color: 4
Size: 272912 Color: 4

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 441413 Color: 1
Size: 295990 Color: 4
Size: 262598 Color: 0

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 441319 Color: 4
Size: 303116 Color: 3
Size: 255566 Color: 0

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 441340 Color: 0
Size: 303797 Color: 2
Size: 254864 Color: 1

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 441527 Color: 4
Size: 300849 Color: 2
Size: 257625 Color: 2

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 441594 Color: 0
Size: 301492 Color: 1
Size: 256915 Color: 4

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 441686 Color: 0
Size: 289893 Color: 2
Size: 268422 Color: 1

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 441688 Color: 4
Size: 291684 Color: 3
Size: 266629 Color: 4

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 441848 Color: 4
Size: 286901 Color: 1
Size: 271252 Color: 2

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 441878 Color: 2
Size: 294677 Color: 0
Size: 263446 Color: 1

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 441878 Color: 2
Size: 307887 Color: 3
Size: 250236 Color: 4

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 441944 Color: 0
Size: 306392 Color: 1
Size: 251665 Color: 3

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 441961 Color: 2
Size: 288676 Color: 4
Size: 269364 Color: 1

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 442002 Color: 2
Size: 292661 Color: 4
Size: 265338 Color: 0

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 442028 Color: 1
Size: 304183 Color: 0
Size: 253790 Color: 3

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 442009 Color: 4
Size: 304303 Color: 3
Size: 253689 Color: 3

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 442156 Color: 1
Size: 294066 Color: 4
Size: 263779 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 442085 Color: 3
Size: 303841 Color: 4
Size: 254075 Color: 4

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 442209 Color: 2
Size: 300367 Color: 0
Size: 257425 Color: 1

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 442220 Color: 2
Size: 292574 Color: 3
Size: 265207 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 442312 Color: 4
Size: 286072 Color: 2
Size: 271617 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 442341 Color: 2
Size: 298666 Color: 2
Size: 258994 Color: 4

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 442472 Color: 0
Size: 306773 Color: 2
Size: 250756 Color: 1

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 442516 Color: 0
Size: 299107 Color: 3
Size: 258378 Color: 1

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 442703 Color: 1
Size: 290395 Color: 4
Size: 266903 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 442639 Color: 2
Size: 305651 Color: 0
Size: 251711 Color: 0

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 442702 Color: 3
Size: 281707 Color: 2
Size: 275592 Color: 1

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 442748 Color: 1
Size: 293612 Color: 4
Size: 263641 Color: 2

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 442717 Color: 4
Size: 304534 Color: 0
Size: 252750 Color: 3

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 442906 Color: 0
Size: 290218 Color: 0
Size: 266877 Color: 4

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 442909 Color: 2
Size: 284039 Color: 4
Size: 273053 Color: 1

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 442912 Color: 4
Size: 300825 Color: 3
Size: 256264 Color: 4

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 442955 Color: 0
Size: 284569 Color: 4
Size: 272477 Color: 4

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 443119 Color: 4
Size: 291447 Color: 3
Size: 265435 Color: 1

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 443144 Color: 0
Size: 303062 Color: 2
Size: 253795 Color: 4

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 443191 Color: 3
Size: 288421 Color: 1
Size: 268389 Color: 0

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 443191 Color: 2
Size: 283658 Color: 3
Size: 273152 Color: 4

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 443218 Color: 4
Size: 292042 Color: 1
Size: 264741 Color: 4

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 443445 Color: 0
Size: 306246 Color: 0
Size: 250310 Color: 1

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 443605 Color: 1
Size: 281957 Color: 4
Size: 274439 Color: 2

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 443645 Color: 0
Size: 294741 Color: 4
Size: 261615 Color: 1

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 443671 Color: 2
Size: 306170 Color: 0
Size: 250160 Color: 2

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 443727 Color: 4
Size: 290099 Color: 3
Size: 266175 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 443775 Color: 4
Size: 305192 Color: 1
Size: 251034 Color: 0

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 443882 Color: 1
Size: 299072 Color: 0
Size: 257047 Color: 3

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 443866 Color: 3
Size: 280040 Color: 1
Size: 276095 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 443921 Color: 3
Size: 296236 Color: 4
Size: 259844 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 444000 Color: 1
Size: 288095 Color: 3
Size: 267906 Color: 3

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 444155 Color: 0
Size: 291509 Color: 4
Size: 264337 Color: 3

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 444312 Color: 1
Size: 278326 Color: 2
Size: 277363 Color: 3

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 444566 Color: 0
Size: 281584 Color: 0
Size: 273851 Color: 3

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 444569 Color: 0
Size: 293038 Color: 2
Size: 262394 Color: 1

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 444575 Color: 0
Size: 297094 Color: 3
Size: 258332 Color: 1

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 444581 Color: 0
Size: 302272 Color: 3
Size: 253148 Color: 2

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 444658 Color: 0
Size: 300749 Color: 2
Size: 254594 Color: 1

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 444684 Color: 3
Size: 303324 Color: 1
Size: 251993 Color: 4

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 444718 Color: 4
Size: 301075 Color: 1
Size: 254208 Color: 2

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 444866 Color: 4
Size: 302893 Color: 0
Size: 252242 Color: 2

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 444949 Color: 2
Size: 284771 Color: 3
Size: 270281 Color: 1

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 444964 Color: 0
Size: 300205 Color: 1
Size: 254832 Color: 3

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 445118 Color: 2
Size: 281385 Color: 0
Size: 273498 Color: 3

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 445150 Color: 4
Size: 297026 Color: 1
Size: 257825 Color: 2

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 445274 Color: 4
Size: 299170 Color: 4
Size: 255557 Color: 2

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 445284 Color: 3
Size: 303048 Color: 1
Size: 251669 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 445421 Color: 1
Size: 277573 Color: 4
Size: 277007 Color: 3

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 445395 Color: 3
Size: 281807 Color: 1
Size: 272799 Color: 2

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 445467 Color: 2
Size: 289823 Color: 3
Size: 264711 Color: 1

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 445471 Color: 0
Size: 294921 Color: 3
Size: 259609 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 445621 Color: 1
Size: 277954 Color: 2
Size: 276426 Color: 4

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 445484 Color: 2
Size: 283742 Color: 2
Size: 270775 Color: 4

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 445722 Color: 1
Size: 296925 Color: 3
Size: 257354 Color: 4

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 445523 Color: 2
Size: 277967 Color: 0
Size: 276511 Color: 3

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 445780 Color: 1
Size: 299674 Color: 4
Size: 254547 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 445751 Color: 3
Size: 277460 Color: 0
Size: 276790 Color: 1

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 445806 Color: 1
Size: 293247 Color: 0
Size: 260948 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 445753 Color: 4
Size: 287833 Color: 2
Size: 266415 Color: 2

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 445812 Color: 0
Size: 282765 Color: 1
Size: 271424 Color: 3

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 445819 Color: 4
Size: 296666 Color: 3
Size: 257516 Color: 4

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 445978 Color: 2
Size: 281974 Color: 2
Size: 272049 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 446012 Color: 3
Size: 301074 Color: 0
Size: 252915 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 446030 Color: 0
Size: 285319 Color: 2
Size: 268652 Color: 2

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 446120 Color: 3
Size: 299516 Color: 1
Size: 254365 Color: 2

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 446168 Color: 3
Size: 292782 Color: 1
Size: 261051 Color: 3

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 446178 Color: 2
Size: 302672 Color: 3
Size: 251151 Color: 3

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 446317 Color: 1
Size: 294849 Color: 2
Size: 258835 Color: 2

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 446497 Color: 3
Size: 300310 Color: 1
Size: 253194 Color: 0

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 446527 Color: 3
Size: 290289 Color: 4
Size: 263185 Color: 4

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 446680 Color: 1
Size: 283011 Color: 3
Size: 270310 Color: 2

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 446689 Color: 4
Size: 276685 Color: 3
Size: 276627 Color: 4

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 446718 Color: 4
Size: 293840 Color: 1
Size: 259443 Color: 0

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 446736 Color: 0
Size: 298134 Color: 1
Size: 255131 Color: 3

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 446762 Color: 2
Size: 282075 Color: 3
Size: 271164 Color: 3

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 446784 Color: 0
Size: 301148 Color: 1
Size: 252069 Color: 4

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 446807 Color: 2
Size: 284355 Color: 3
Size: 268839 Color: 2

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 446875 Color: 4
Size: 276766 Color: 0
Size: 276360 Color: 1

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 446876 Color: 4
Size: 281512 Color: 0
Size: 271613 Color: 3

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 447033 Color: 4
Size: 282558 Color: 1
Size: 270410 Color: 4

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 447242 Color: 0
Size: 276738 Color: 1
Size: 276021 Color: 3

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 447303 Color: 1
Size: 289509 Color: 2
Size: 263189 Color: 0

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 447324 Color: 3
Size: 281235 Color: 2
Size: 271442 Color: 4

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 447483 Color: 0
Size: 276968 Color: 1
Size: 275550 Color: 4

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 447601 Color: 2
Size: 298581 Color: 1
Size: 253819 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 447698 Color: 2
Size: 298970 Color: 4
Size: 253333 Color: 4

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 447821 Color: 1
Size: 290420 Color: 4
Size: 261760 Color: 3

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 447762 Color: 4
Size: 280780 Color: 0
Size: 271459 Color: 2

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 447765 Color: 2
Size: 289389 Color: 3
Size: 262847 Color: 1

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 447864 Color: 4
Size: 284946 Color: 1
Size: 267191 Color: 2

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 447913 Color: 3
Size: 285938 Color: 0
Size: 266150 Color: 4

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 447954 Color: 0
Size: 286122 Color: 2
Size: 265925 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 448113 Color: 0
Size: 300903 Color: 3
Size: 250985 Color: 3

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 448391 Color: 4
Size: 284728 Color: 3
Size: 266882 Color: 1

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 448403 Color: 4
Size: 294486 Color: 3
Size: 257112 Color: 2

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 448421 Color: 3
Size: 287508 Color: 2
Size: 264072 Color: 1

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 448446 Color: 0
Size: 281183 Color: 3
Size: 270372 Color: 2

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 448554 Color: 1
Size: 292785 Color: 2
Size: 258662 Color: 2

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 448520 Color: 3
Size: 294502 Color: 3
Size: 256979 Color: 4

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 448541 Color: 2
Size: 290763 Color: 1
Size: 260697 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 448604 Color: 1
Size: 293112 Color: 3
Size: 258285 Color: 4

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 448550 Color: 2
Size: 297172 Color: 2
Size: 254279 Color: 3

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 448608 Color: 4
Size: 288241 Color: 2
Size: 263152 Color: 2

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 448645 Color: 4
Size: 294667 Color: 0
Size: 256689 Color: 1

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 448649 Color: 2
Size: 300958 Color: 3
Size: 250394 Color: 1

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 448653 Color: 3
Size: 275834 Color: 2
Size: 275514 Color: 0

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 448714 Color: 0
Size: 291397 Color: 1
Size: 259890 Color: 2

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 448818 Color: 2
Size: 290227 Color: 4
Size: 260956 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 448918 Color: 1
Size: 300094 Color: 3
Size: 250989 Color: 4

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 448978 Color: 4
Size: 297106 Color: 4
Size: 253917 Color: 0

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 449155 Color: 4
Size: 275873 Color: 3
Size: 274973 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 449159 Color: 2
Size: 278722 Color: 3
Size: 272120 Color: 1

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 449174 Color: 3
Size: 284352 Color: 0
Size: 266475 Color: 1

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 449187 Color: 3
Size: 280780 Color: 4
Size: 270034 Color: 0

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 449214 Color: 4
Size: 292234 Color: 3
Size: 258553 Color: 1

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 449276 Color: 2
Size: 292832 Color: 4
Size: 257893 Color: 0

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 449284 Color: 4
Size: 285786 Color: 3
Size: 264931 Color: 1

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 449382 Color: 3
Size: 277972 Color: 2
Size: 272647 Color: 1

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 449506 Color: 1
Size: 287625 Color: 3
Size: 262870 Color: 3

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 449616 Color: 3
Size: 289770 Color: 0
Size: 260615 Color: 2

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 449784 Color: 0
Size: 285375 Color: 3
Size: 264842 Color: 0

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 449803 Color: 4
Size: 291778 Color: 0
Size: 258420 Color: 4

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 449815 Color: 0
Size: 282861 Color: 2
Size: 267325 Color: 2

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 449815 Color: 1
Size: 291015 Color: 4
Size: 259171 Color: 1

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 449928 Color: 1
Size: 278792 Color: 3
Size: 271281 Color: 2

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 449923 Color: 4
Size: 299047 Color: 3
Size: 251031 Color: 1

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 450079 Color: 4
Size: 291119 Color: 2
Size: 258803 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 450085 Color: 0
Size: 287039 Color: 1
Size: 262877 Color: 2

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 450095 Color: 0
Size: 289391 Color: 3
Size: 260515 Color: 1

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 450172 Color: 2
Size: 294776 Color: 3
Size: 255053 Color: 3

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 450218 Color: 0
Size: 279464 Color: 0
Size: 270319 Color: 1

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 450334 Color: 1
Size: 282440 Color: 0
Size: 267227 Color: 2

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 450357 Color: 0
Size: 298982 Color: 1
Size: 250662 Color: 4

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 450606 Color: 4
Size: 296375 Color: 2
Size: 253020 Color: 3

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 450635 Color: 4
Size: 277849 Color: 2
Size: 271517 Color: 1

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 450754 Color: 3
Size: 293054 Color: 0
Size: 256193 Color: 4

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 450774 Color: 0
Size: 297886 Color: 2
Size: 251341 Color: 1

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 450798 Color: 2
Size: 284270 Color: 1
Size: 264933 Color: 3

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 450815 Color: 2
Size: 282143 Color: 0
Size: 267043 Color: 0

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 450862 Color: 3
Size: 278282 Color: 4
Size: 270857 Color: 2

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 450865 Color: 3
Size: 295019 Color: 0
Size: 254117 Color: 1

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 450909 Color: 3
Size: 281914 Color: 0
Size: 267178 Color: 1

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 450912 Color: 2
Size: 290481 Color: 0
Size: 258608 Color: 3

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 450925 Color: 3
Size: 294982 Color: 3
Size: 254094 Color: 1

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 451013 Color: 0
Size: 274672 Color: 0
Size: 274316 Color: 1

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 451024 Color: 3
Size: 276168 Color: 2
Size: 272809 Color: 0

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 451065 Color: 1
Size: 297872 Color: 3
Size: 251064 Color: 0

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 451048 Color: 3
Size: 280949 Color: 3
Size: 268004 Color: 4

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 451070 Color: 2
Size: 288116 Color: 2
Size: 260815 Color: 1

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 451219 Color: 3
Size: 279301 Color: 1
Size: 269481 Color: 2

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 451264 Color: 0
Size: 296352 Color: 1
Size: 252385 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 451393 Color: 1
Size: 296832 Color: 4
Size: 251776 Color: 4

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 451373 Color: 2
Size: 280855 Color: 4
Size: 267773 Color: 3

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 451393 Color: 3
Size: 279011 Color: 0
Size: 269597 Color: 1

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 451432 Color: 0
Size: 296061 Color: 2
Size: 252508 Color: 2

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 451482 Color: 2
Size: 281321 Color: 4
Size: 267198 Color: 1

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 451558 Color: 1
Size: 275178 Color: 2
Size: 273265 Color: 4

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 451504 Color: 3
Size: 274678 Color: 2
Size: 273819 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 451546 Color: 2
Size: 295893 Color: 2
Size: 252562 Color: 1

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 451722 Color: 0
Size: 276765 Color: 2
Size: 271514 Color: 2

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 451801 Color: 4
Size: 277995 Color: 1
Size: 270205 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 451901 Color: 1
Size: 292697 Color: 4
Size: 255403 Color: 4

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 451850 Color: 4
Size: 290232 Color: 0
Size: 257919 Color: 2

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 451935 Color: 1
Size: 288968 Color: 4
Size: 259098 Color: 4

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 452012 Color: 1
Size: 290312 Color: 0
Size: 257677 Color: 4

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 452054 Color: 3
Size: 278312 Color: 0
Size: 269635 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 452122 Color: 1
Size: 291006 Color: 3
Size: 256873 Color: 0

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 452066 Color: 0
Size: 279730 Color: 2
Size: 268205 Color: 3

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 452169 Color: 2
Size: 287578 Color: 1
Size: 260254 Color: 3

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 452264 Color: 4
Size: 287654 Color: 2
Size: 260083 Color: 3

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 452294 Color: 3
Size: 294453 Color: 1
Size: 253254 Color: 2

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 452338 Color: 0
Size: 285882 Color: 3
Size: 261781 Color: 1

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 452341 Color: 4
Size: 289355 Color: 3
Size: 258305 Color: 0

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 452365 Color: 4
Size: 294368 Color: 3
Size: 253268 Color: 1

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 452580 Color: 4
Size: 289767 Color: 2
Size: 257654 Color: 4

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 452841 Color: 1
Size: 280697 Color: 3
Size: 266463 Color: 2

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 452684 Color: 0
Size: 282254 Color: 2
Size: 265063 Color: 3

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 452849 Color: 1
Size: 282519 Color: 3
Size: 264633 Color: 2

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 452900 Color: 1
Size: 286531 Color: 4
Size: 260570 Color: 4

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 452818 Color: 2
Size: 295464 Color: 0
Size: 251719 Color: 0

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 452905 Color: 1
Size: 281707 Color: 2
Size: 265389 Color: 0

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 452924 Color: 1
Size: 280275 Color: 4
Size: 266802 Color: 2

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 452930 Color: 0
Size: 291857 Color: 0
Size: 255214 Color: 3

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 453081 Color: 4
Size: 290167 Color: 1
Size: 256753 Color: 2

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 453150 Color: 0
Size: 276252 Color: 4
Size: 270599 Color: 2

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 453323 Color: 2
Size: 291469 Color: 3
Size: 255209 Color: 1

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 453377 Color: 1
Size: 275025 Color: 3
Size: 271599 Color: 4

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 453529 Color: 3
Size: 288227 Color: 1
Size: 258245 Color: 4

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 453908 Color: 1
Size: 287577 Color: 3
Size: 258516 Color: 2

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 453867 Color: 2
Size: 294697 Color: 2
Size: 251437 Color: 0

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 453952 Color: 4
Size: 274704 Color: 2
Size: 271345 Color: 1

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 453979 Color: 0
Size: 282511 Color: 4
Size: 263511 Color: 3

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 453987 Color: 2
Size: 295769 Color: 2
Size: 250245 Color: 1

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 454038 Color: 3
Size: 285929 Color: 4
Size: 260034 Color: 0

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 454078 Color: 2
Size: 293195 Color: 1
Size: 252728 Color: 2

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 454161 Color: 3
Size: 287880 Color: 2
Size: 257960 Color: 1

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 454162 Color: 0
Size: 290259 Color: 3
Size: 255580 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 454346 Color: 1
Size: 279276 Color: 2
Size: 266379 Color: 4

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 454300 Color: 0
Size: 277724 Color: 0
Size: 267977 Color: 2

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 454427 Color: 4
Size: 288178 Color: 2
Size: 257396 Color: 1

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 454449 Color: 4
Size: 282872 Color: 0
Size: 262680 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 454687 Color: 1
Size: 278307 Color: 4
Size: 267007 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 454596 Color: 3
Size: 283474 Color: 0
Size: 261931 Color: 3

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 454738 Color: 3
Size: 290699 Color: 1
Size: 254564 Color: 2

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 454742 Color: 0
Size: 277495 Color: 3
Size: 267764 Color: 3

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 454781 Color: 2
Size: 280268 Color: 0
Size: 264952 Color: 2

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 454852 Color: 4
Size: 284269 Color: 1
Size: 260880 Color: 3

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 454904 Color: 0
Size: 287844 Color: 1
Size: 257253 Color: 2

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 454969 Color: 1
Size: 291540 Color: 2
Size: 253492 Color: 3

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 454906 Color: 4
Size: 280245 Color: 2
Size: 264850 Color: 3

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 454956 Color: 4
Size: 279516 Color: 4
Size: 265529 Color: 1

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 455012 Color: 2
Size: 294061 Color: 1
Size: 250928 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 455039 Color: 3
Size: 280910 Color: 3
Size: 264052 Color: 0

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 455199 Color: 1
Size: 292560 Color: 3
Size: 252242 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 455061 Color: 2
Size: 292477 Color: 3
Size: 252463 Color: 3

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 455127 Color: 2
Size: 274788 Color: 0
Size: 270086 Color: 1

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 455309 Color: 1
Size: 292823 Color: 4
Size: 251869 Color: 2

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 455258 Color: 3
Size: 280845 Color: 2
Size: 263898 Color: 4

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 455298 Color: 4
Size: 280573 Color: 0
Size: 264130 Color: 1

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 455333 Color: 4
Size: 276082 Color: 4
Size: 268586 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 455506 Color: 1
Size: 275195 Color: 4
Size: 269300 Color: 0

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 455520 Color: 2
Size: 278354 Color: 3
Size: 266127 Color: 1

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 455563 Color: 0
Size: 282140 Color: 4
Size: 262298 Color: 2

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 455640 Color: 3
Size: 293862 Color: 2
Size: 250499 Color: 1

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 455725 Color: 0
Size: 277717 Color: 2
Size: 266559 Color: 0

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 455869 Color: 1
Size: 290253 Color: 4
Size: 253879 Color: 3

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 455903 Color: 1
Size: 289291 Color: 4
Size: 254807 Color: 3

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 455748 Color: 4
Size: 293467 Color: 3
Size: 250786 Color: 4

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 455853 Color: 2
Size: 284181 Color: 4
Size: 259967 Color: 1

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 455948 Color: 1
Size: 279734 Color: 4
Size: 264319 Color: 0

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 455934 Color: 3
Size: 282363 Color: 2
Size: 261704 Color: 2

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 456147 Color: 4
Size: 292866 Color: 1
Size: 250988 Color: 3

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 456185 Color: 0
Size: 277924 Color: 0
Size: 265892 Color: 4

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 456361 Color: 1
Size: 286635 Color: 0
Size: 257005 Color: 2

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 456359 Color: 4
Size: 285321 Color: 2
Size: 258321 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 456547 Color: 2
Size: 289351 Color: 1
Size: 254103 Color: 3

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 456572 Color: 3
Size: 275577 Color: 2
Size: 267852 Color: 1

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 456614 Color: 2
Size: 271963 Color: 3
Size: 271424 Color: 1

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 456637 Color: 2
Size: 276875 Color: 0
Size: 266489 Color: 4

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 456878 Color: 0
Size: 276912 Color: 3
Size: 266211 Color: 2

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 456978 Color: 4
Size: 287351 Color: 1
Size: 255672 Color: 2

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 457079 Color: 4
Size: 286929 Color: 0
Size: 255993 Color: 2

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 457357 Color: 1
Size: 281479 Color: 2
Size: 261165 Color: 4

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 457208 Color: 4
Size: 291237 Color: 2
Size: 251556 Color: 3

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 457313 Color: 3
Size: 283832 Color: 0
Size: 258856 Color: 1

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 457396 Color: 1
Size: 284501 Color: 2
Size: 258104 Color: 0

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 457628 Color: 2
Size: 282336 Color: 4
Size: 260037 Color: 2

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 457637 Color: 4
Size: 285056 Color: 0
Size: 257308 Color: 1

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 457639 Color: 1
Size: 287112 Color: 3
Size: 255250 Color: 2

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 457693 Color: 2
Size: 289132 Color: 1
Size: 253176 Color: 2

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 457969 Color: 3
Size: 291202 Color: 2
Size: 250830 Color: 1

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 458024 Color: 1
Size: 284867 Color: 2
Size: 257110 Color: 0

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 458137 Color: 2
Size: 287787 Color: 3
Size: 254077 Color: 0

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 458138 Color: 2
Size: 291801 Color: 4
Size: 250062 Color: 1

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 458258 Color: 2
Size: 290602 Color: 0
Size: 251141 Color: 1

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 458291 Color: 3
Size: 291318 Color: 2
Size: 250392 Color: 4

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 458351 Color: 1
Size: 272786 Color: 2
Size: 268864 Color: 3

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 458327 Color: 0
Size: 283154 Color: 3
Size: 258520 Color: 2

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 458415 Color: 1
Size: 290022 Color: 4
Size: 251564 Color: 0

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 458576 Color: 1
Size: 284829 Color: 0
Size: 256596 Color: 2

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 458548 Color: 0
Size: 282523 Color: 4
Size: 258930 Color: 3

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 458548 Color: 0
Size: 284932 Color: 2
Size: 256521 Color: 1

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 458690 Color: 4
Size: 291212 Color: 4
Size: 250099 Color: 1

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 458737 Color: 1
Size: 284954 Color: 4
Size: 256310 Color: 2

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 458775 Color: 3
Size: 271673 Color: 1
Size: 269553 Color: 2

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 458808 Color: 0
Size: 274553 Color: 3
Size: 266640 Color: 3

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 458857 Color: 1
Size: 280864 Color: 4
Size: 260280 Color: 2

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 458808 Color: 0
Size: 280763 Color: 0
Size: 260430 Color: 3

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 458853 Color: 0
Size: 286704 Color: 0
Size: 254444 Color: 1

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 458882 Color: 2
Size: 282475 Color: 1
Size: 258644 Color: 3

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 458917 Color: 0
Size: 285698 Color: 2
Size: 255386 Color: 4

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 459128 Color: 4
Size: 279412 Color: 3
Size: 261461 Color: 1

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 459140 Color: 1
Size: 275273 Color: 3
Size: 265588 Color: 4

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 459182 Color: 0
Size: 287067 Color: 2
Size: 253752 Color: 2

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 459229 Color: 1
Size: 274299 Color: 0
Size: 266473 Color: 3

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 459281 Color: 1
Size: 270771 Color: 2
Size: 269949 Color: 4

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 459296 Color: 4
Size: 276835 Color: 1
Size: 263870 Color: 3

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 459308 Color: 3
Size: 288842 Color: 2
Size: 251851 Color: 2

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 459319 Color: 4
Size: 272825 Color: 3
Size: 267857 Color: 1

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 459496 Color: 2
Size: 277578 Color: 1
Size: 262927 Color: 3

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 459517 Color: 4
Size: 288119 Color: 0
Size: 252365 Color: 4

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 459712 Color: 3
Size: 277518 Color: 4
Size: 262771 Color: 4

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 459884 Color: 3
Size: 286980 Color: 0
Size: 253137 Color: 4

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 459931 Color: 4
Size: 288118 Color: 1
Size: 251952 Color: 3

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 459991 Color: 3
Size: 288009 Color: 4
Size: 252001 Color: 1

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 460039 Color: 3
Size: 276544 Color: 4
Size: 263418 Color: 3

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 460199 Color: 1
Size: 278792 Color: 3
Size: 261010 Color: 2

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 460097 Color: 2
Size: 277838 Color: 2
Size: 262066 Color: 3

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 460209 Color: 3
Size: 277472 Color: 1
Size: 262320 Color: 3

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 460301 Color: 2
Size: 280416 Color: 2
Size: 259284 Color: 3

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 460320 Color: 2
Size: 277288 Color: 1
Size: 262393 Color: 3

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 460407 Color: 2
Size: 283170 Color: 0
Size: 256424 Color: 1

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 460470 Color: 2
Size: 283260 Color: 3
Size: 256271 Color: 2

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 460571 Color: 1
Size: 281211 Color: 3
Size: 258219 Color: 3

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 460847 Color: 1
Size: 279267 Color: 2
Size: 259887 Color: 4

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 460990 Color: 4
Size: 275547 Color: 2
Size: 263464 Color: 3

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 461002 Color: 2
Size: 284138 Color: 3
Size: 254861 Color: 1

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 461050 Color: 0
Size: 272108 Color: 4
Size: 266843 Color: 3

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 461242 Color: 1
Size: 274095 Color: 3
Size: 264664 Color: 3

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 461426 Color: 4
Size: 286734 Color: 0
Size: 251841 Color: 1

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 461483 Color: 4
Size: 276829 Color: 1
Size: 261689 Color: 2

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 461581 Color: 3
Size: 279774 Color: 0
Size: 258646 Color: 0

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 461647 Color: 2
Size: 288176 Color: 2
Size: 250178 Color: 1

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 461647 Color: 4
Size: 272670 Color: 1
Size: 265684 Color: 0

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 461672 Color: 2
Size: 285954 Color: 2
Size: 252375 Color: 3

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 461733 Color: 1
Size: 270494 Color: 2
Size: 267774 Color: 2

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 461755 Color: 0
Size: 275299 Color: 2
Size: 262947 Color: 3

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 461799 Color: 4
Size: 282910 Color: 3
Size: 255292 Color: 1

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 461858 Color: 4
Size: 273532 Color: 1
Size: 264611 Color: 3

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 461923 Color: 3
Size: 287539 Color: 2
Size: 250539 Color: 1

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 461972 Color: 0
Size: 283807 Color: 1
Size: 254222 Color: 3

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 462051 Color: 2
Size: 280319 Color: 4
Size: 257631 Color: 0

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 462146 Color: 1
Size: 281025 Color: 2
Size: 256830 Color: 0

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 462062 Color: 3
Size: 275483 Color: 0
Size: 262456 Color: 2

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 462532 Color: 0
Size: 279013 Color: 4
Size: 258456 Color: 0

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 462716 Color: 2
Size: 275724 Color: 2
Size: 261561 Color: 4

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 462799 Color: 1
Size: 281071 Color: 0
Size: 256131 Color: 2

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 462817 Color: 1
Size: 270157 Color: 4
Size: 267027 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 462804 Color: 0
Size: 279430 Color: 2
Size: 257767 Color: 2

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 462907 Color: 3
Size: 281369 Color: 0
Size: 255725 Color: 4

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 462980 Color: 4
Size: 283999 Color: 4
Size: 253022 Color: 1

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 463186 Color: 1
Size: 268741 Color: 3
Size: 268074 Color: 4

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 463183 Color: 0
Size: 278760 Color: 1
Size: 258058 Color: 2

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 463251 Color: 4
Size: 277973 Color: 3
Size: 258777 Color: 0

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 463253 Color: 2
Size: 282518 Color: 3
Size: 254230 Color: 1

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 463434 Color: 3
Size: 273904 Color: 2
Size: 262663 Color: 4

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 463464 Color: 0
Size: 278601 Color: 1
Size: 257936 Color: 2

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 463454 Color: 1
Size: 282703 Color: 2
Size: 253844 Color: 4

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 463545 Color: 0
Size: 277657 Color: 1
Size: 258799 Color: 2

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 463639 Color: 4
Size: 282490 Color: 3
Size: 253872 Color: 2

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 463770 Color: 2
Size: 274720 Color: 4
Size: 261511 Color: 1

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 463851 Color: 2
Size: 277891 Color: 0
Size: 258259 Color: 4

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 463859 Color: 2
Size: 283316 Color: 3
Size: 252826 Color: 1

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 463915 Color: 3
Size: 273615 Color: 4
Size: 262471 Color: 3

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 463919 Color: 4
Size: 284905 Color: 0
Size: 251177 Color: 1

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 463997 Color: 4
Size: 271037 Color: 2
Size: 264967 Color: 3

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 464077 Color: 3
Size: 277756 Color: 1
Size: 258168 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 464324 Color: 0
Size: 282845 Color: 0
Size: 252832 Color: 4

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 464363 Color: 3
Size: 280419 Color: 1
Size: 255219 Color: 2

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 464423 Color: 2
Size: 283390 Color: 2
Size: 252188 Color: 4

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 464517 Color: 1
Size: 272839 Color: 3
Size: 262645 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 464545 Color: 1
Size: 273891 Color: 3
Size: 261565 Color: 2

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 464520 Color: 0
Size: 278521 Color: 4
Size: 256960 Color: 4

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 464580 Color: 3
Size: 275450 Color: 2
Size: 259971 Color: 1

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 464613 Color: 1
Size: 280159 Color: 2
Size: 255229 Color: 3

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 464614 Color: 2
Size: 274646 Color: 3
Size: 260741 Color: 2

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 464767 Color: 2
Size: 282089 Color: 3
Size: 253145 Color: 4

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 464870 Color: 0
Size: 268778 Color: 0
Size: 266353 Color: 1

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 464957 Color: 0
Size: 277262 Color: 1
Size: 257782 Color: 0

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 465158 Color: 0
Size: 269259 Color: 2
Size: 265584 Color: 2

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 465236 Color: 1
Size: 282265 Color: 4
Size: 252500 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 465162 Color: 0
Size: 271711 Color: 3
Size: 263128 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 465320 Color: 1
Size: 278922 Color: 4
Size: 255759 Color: 2

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 465352 Color: 1
Size: 271150 Color: 2
Size: 263499 Color: 4

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 465309 Color: 0
Size: 283366 Color: 3
Size: 251326 Color: 2

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 465438 Color: 1
Size: 284073 Color: 3
Size: 250490 Color: 3

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 465470 Color: 1
Size: 279962 Color: 2
Size: 254569 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 465447 Color: 3
Size: 271701 Color: 3
Size: 262853 Color: 1

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 465471 Color: 1
Size: 284365 Color: 3
Size: 250165 Color: 2

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 465485 Color: 4
Size: 277198 Color: 2
Size: 257318 Color: 2

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 465578 Color: 3
Size: 280130 Color: 1
Size: 254293 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 465701 Color: 2
Size: 282359 Color: 4
Size: 251941 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 465773 Color: 1
Size: 282005 Color: 3
Size: 252223 Color: 4

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 465852 Color: 1
Size: 269486 Color: 2
Size: 264663 Color: 0

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 465866 Color: 0
Size: 277021 Color: 2
Size: 257114 Color: 2

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 465888 Color: 0
Size: 273848 Color: 4
Size: 260265 Color: 1

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 465900 Color: 2
Size: 268494 Color: 1
Size: 265607 Color: 3

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 465906 Color: 0
Size: 281001 Color: 3
Size: 253094 Color: 1

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 465932 Color: 0
Size: 273573 Color: 4
Size: 260496 Color: 4

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 465972 Color: 4
Size: 274209 Color: 3
Size: 259820 Color: 1

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 465989 Color: 1
Size: 277098 Color: 4
Size: 256914 Color: 0

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 465981 Color: 0
Size: 280803 Color: 3
Size: 253217 Color: 0

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 466091 Color: 0
Size: 281553 Color: 2
Size: 252357 Color: 1

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 466231 Color: 4
Size: 283176 Color: 1
Size: 250594 Color: 3

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 466313 Color: 4
Size: 282994 Color: 3
Size: 250694 Color: 1

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 466316 Color: 0
Size: 279245 Color: 4
Size: 254440 Color: 0

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 466509 Color: 1
Size: 272172 Color: 4
Size: 261320 Color: 4

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 466650 Color: 1
Size: 274858 Color: 4
Size: 258493 Color: 0

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 466651 Color: 1
Size: 281709 Color: 0
Size: 251641 Color: 4

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 466511 Color: 0
Size: 270426 Color: 3
Size: 263064 Color: 3

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 466684 Color: 3
Size: 276099 Color: 1
Size: 257218 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 466730 Color: 0
Size: 278382 Color: 3
Size: 254889 Color: 1

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 466804 Color: 3
Size: 276499 Color: 2
Size: 256698 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 466991 Color: 1
Size: 267634 Color: 4
Size: 265376 Color: 3

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 466879 Color: 2
Size: 270219 Color: 3
Size: 262903 Color: 3

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 467052 Color: 4
Size: 276612 Color: 2
Size: 256337 Color: 1

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 467097 Color: 2
Size: 282617 Color: 1
Size: 250287 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 467184 Color: 3
Size: 280979 Color: 4
Size: 251838 Color: 3

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 467361 Color: 2
Size: 282328 Color: 1
Size: 250312 Color: 2

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 467409 Color: 4
Size: 277548 Color: 3
Size: 255044 Color: 3

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 467515 Color: 1
Size: 267109 Color: 2
Size: 265377 Color: 1

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 467430 Color: 3
Size: 268484 Color: 0
Size: 264087 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 467608 Color: 2
Size: 272047 Color: 1
Size: 260346 Color: 4

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 467625 Color: 1
Size: 281581 Color: 2
Size: 250795 Color: 3

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 467636 Color: 3
Size: 279940 Color: 0
Size: 252425 Color: 4

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 467686 Color: 0
Size: 279459 Color: 1
Size: 252856 Color: 2

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 467768 Color: 2
Size: 276267 Color: 3
Size: 255966 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 467852 Color: 4
Size: 275790 Color: 3
Size: 256359 Color: 3

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 468104 Color: 1
Size: 271936 Color: 3
Size: 259961 Color: 2

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 468115 Color: 3
Size: 278740 Color: 1
Size: 253146 Color: 0

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 468165 Color: 2
Size: 270209 Color: 3
Size: 261627 Color: 1

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 468278 Color: 3
Size: 266010 Color: 1
Size: 265713 Color: 3

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 468272 Color: 1
Size: 268348 Color: 0
Size: 263381 Color: 3

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 468451 Color: 0
Size: 270505 Color: 4
Size: 261045 Color: 2

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 468527 Color: 1
Size: 270989 Color: 4
Size: 260485 Color: 4

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 468457 Color: 0
Size: 279911 Color: 2
Size: 251633 Color: 3

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 468650 Color: 2
Size: 280184 Color: 1
Size: 251167 Color: 0

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 468751 Color: 3
Size: 276683 Color: 3
Size: 254567 Color: 2

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 468872 Color: 2
Size: 277623 Color: 2
Size: 253506 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 468946 Color: 3
Size: 266116 Color: 2
Size: 264939 Color: 1

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 469044 Color: 1
Size: 271845 Color: 4
Size: 259112 Color: 3

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 469011 Color: 4
Size: 273456 Color: 3
Size: 257534 Color: 4

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 469219 Color: 0
Size: 269328 Color: 4
Size: 261454 Color: 1

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 469222 Color: 3
Size: 280374 Color: 4
Size: 250405 Color: 1

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 469224 Color: 4
Size: 269297 Color: 3
Size: 261480 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 469321 Color: 1
Size: 269642 Color: 2
Size: 261038 Color: 4

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 469235 Color: 4
Size: 270604 Color: 0
Size: 260162 Color: 0

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 469562 Color: 1
Size: 271820 Color: 3
Size: 258619 Color: 0

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 469591 Color: 4
Size: 274460 Color: 1
Size: 255950 Color: 2

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 469613 Color: 4
Size: 274428 Color: 2
Size: 255960 Color: 3

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 469833 Color: 4
Size: 271320 Color: 3
Size: 258848 Color: 1

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 469805 Color: 1
Size: 277446 Color: 0
Size: 252750 Color: 4

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 469912 Color: 4
Size: 273143 Color: 3
Size: 256946 Color: 2

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 469921 Color: 4
Size: 270241 Color: 2
Size: 259839 Color: 1

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 469967 Color: 4
Size: 271412 Color: 0
Size: 258622 Color: 4

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 470010 Color: 2
Size: 278139 Color: 1
Size: 251852 Color: 4

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 470020 Color: 1
Size: 265567 Color: 2
Size: 264414 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 470074 Color: 4
Size: 266344 Color: 0
Size: 263583 Color: 3

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 470161 Color: 4
Size: 271662 Color: 3
Size: 258178 Color: 4

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 470204 Color: 2
Size: 269187 Color: 4
Size: 260610 Color: 1

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 470413 Color: 1
Size: 273006 Color: 3
Size: 256582 Color: 3

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 470293 Color: 3
Size: 265079 Color: 2
Size: 264629 Color: 3

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 470447 Color: 0
Size: 275702 Color: 4
Size: 253852 Color: 1

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 470537 Color: 2
Size: 266223 Color: 0
Size: 263241 Color: 3

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 470613 Color: 3
Size: 269722 Color: 2
Size: 259666 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 470618 Color: 3
Size: 276944 Color: 2
Size: 252439 Color: 1

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 471076 Color: 3
Size: 272489 Color: 4
Size: 256436 Color: 1

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 471082 Color: 0
Size: 278730 Color: 4
Size: 250189 Color: 4

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 471103 Color: 0
Size: 273465 Color: 3
Size: 255433 Color: 1

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 471249 Color: 4
Size: 271894 Color: 3
Size: 256858 Color: 1

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 471410 Color: 3
Size: 272594 Color: 1
Size: 255997 Color: 2

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 471657 Color: 2
Size: 266876 Color: 0
Size: 261468 Color: 1

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 471672 Color: 2
Size: 269715 Color: 4
Size: 258614 Color: 3

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 471826 Color: 2
Size: 271617 Color: 1
Size: 256558 Color: 0

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 472089 Color: 0
Size: 268234 Color: 3
Size: 259678 Color: 1

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 472221 Color: 0
Size: 271825 Color: 2
Size: 255955 Color: 1

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 472241 Color: 4
Size: 265605 Color: 3
Size: 262155 Color: 4

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 472391 Color: 0
Size: 270979 Color: 2
Size: 256631 Color: 2

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 472706 Color: 4
Size: 269301 Color: 1
Size: 257994 Color: 4

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 472723 Color: 0
Size: 264926 Color: 3
Size: 262352 Color: 1

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 472741 Color: 0
Size: 272392 Color: 3
Size: 254868 Color: 4

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 472750 Color: 2
Size: 274957 Color: 0
Size: 252294 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 472986 Color: 3
Size: 267365 Color: 3
Size: 259650 Color: 1

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 473065 Color: 3
Size: 275844 Color: 2
Size: 251092 Color: 1

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 473111 Color: 4
Size: 275132 Color: 0
Size: 251758 Color: 0

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 473228 Color: 3
Size: 272348 Color: 0
Size: 254425 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 473251 Color: 4
Size: 273280 Color: 4
Size: 253470 Color: 1

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 473285 Color: 3
Size: 270821 Color: 2
Size: 255895 Color: 0

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 473342 Color: 2
Size: 272222 Color: 2
Size: 254437 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 473350 Color: 1
Size: 271162 Color: 2
Size: 255489 Color: 4

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 473379 Color: 3
Size: 265286 Color: 0
Size: 261336 Color: 0

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 473428 Color: 1
Size: 273935 Color: 4
Size: 252638 Color: 4

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 473414 Color: 3
Size: 268273 Color: 2
Size: 258314 Color: 4

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 473650 Color: 1
Size: 271367 Color: 3
Size: 254984 Color: 4

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 473684 Color: 2
Size: 272327 Color: 4
Size: 253990 Color: 0

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 473901 Color: 2
Size: 264361 Color: 1
Size: 261739 Color: 0

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 473942 Color: 0
Size: 263775 Color: 0
Size: 262284 Color: 3

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 474005 Color: 2
Size: 265126 Color: 1
Size: 260870 Color: 3

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 474165 Color: 0
Size: 271323 Color: 2
Size: 254513 Color: 1

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 474210 Color: 2
Size: 273105 Color: 1
Size: 252686 Color: 4

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 474211 Color: 3
Size: 268328 Color: 4
Size: 257462 Color: 2

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 474320 Color: 2
Size: 271880 Color: 4
Size: 253801 Color: 1

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 474380 Color: 1
Size: 270153 Color: 0
Size: 255468 Color: 2

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 474417 Color: 0
Size: 274234 Color: 2
Size: 251350 Color: 2

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 474518 Color: 2
Size: 268990 Color: 4
Size: 256493 Color: 2

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 474521 Color: 4
Size: 270831 Color: 1
Size: 254649 Color: 2

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 474550 Color: 2
Size: 270790 Color: 1
Size: 254661 Color: 0

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 474577 Color: 0
Size: 272380 Color: 3
Size: 253044 Color: 3

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 474671 Color: 2
Size: 275264 Color: 1
Size: 250066 Color: 0

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 474672 Color: 0
Size: 262892 Color: 4
Size: 262437 Color: 4

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 474793 Color: 1
Size: 266723 Color: 2
Size: 258485 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 474733 Color: 0
Size: 275015 Color: 2
Size: 250253 Color: 4

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 474747 Color: 2
Size: 264199 Color: 3
Size: 261055 Color: 1

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 474966 Color: 1
Size: 263771 Color: 4
Size: 261264 Color: 3

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 475143 Color: 1
Size: 263270 Color: 3
Size: 261588 Color: 3

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 475201 Color: 2
Size: 269603 Color: 3
Size: 255197 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 475207 Color: 3
Size: 273733 Color: 2
Size: 251061 Color: 1

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 475269 Color: 3
Size: 271900 Color: 1
Size: 252832 Color: 2

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 475354 Color: 3
Size: 269817 Color: 1
Size: 254830 Color: 0

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 475362 Color: 1
Size: 266185 Color: 3
Size: 258454 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 475398 Color: 4
Size: 270800 Color: 2
Size: 253803 Color: 3

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 475460 Color: 3
Size: 269949 Color: 2
Size: 254592 Color: 2

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 475622 Color: 2
Size: 265966 Color: 2
Size: 258413 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 475656 Color: 0
Size: 268754 Color: 3
Size: 255591 Color: 1

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 475679 Color: 1
Size: 265418 Color: 3
Size: 258904 Color: 3

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 475660 Color: 2
Size: 272447 Color: 4
Size: 251894 Color: 3

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 475705 Color: 0
Size: 267111 Color: 0
Size: 257185 Color: 1

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 475725 Color: 4
Size: 268985 Color: 0
Size: 255291 Color: 0

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 475740 Color: 3
Size: 267583 Color: 2
Size: 256678 Color: 1

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 475770 Color: 0
Size: 269742 Color: 4
Size: 254489 Color: 4

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 475938 Color: 1
Size: 262940 Color: 0
Size: 261123 Color: 3

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 476020 Color: 1
Size: 265011 Color: 2
Size: 258970 Color: 3

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 475989 Color: 4
Size: 271611 Color: 1
Size: 252401 Color: 4

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 476101 Color: 1
Size: 272872 Color: 4
Size: 251028 Color: 2

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 476045 Color: 3
Size: 269376 Color: 0
Size: 254580 Color: 0

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 476136 Color: 2
Size: 271141 Color: 1
Size: 252724 Color: 3

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 476209 Color: 1
Size: 273417 Color: 0
Size: 250375 Color: 2

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 476283 Color: 2
Size: 262654 Color: 4
Size: 261064 Color: 3

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 476819 Color: 3
Size: 267370 Color: 3
Size: 255812 Color: 1

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 476828 Color: 0
Size: 265595 Color: 4
Size: 257578 Color: 1

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 477036 Color: 2
Size: 269863 Color: 0
Size: 253102 Color: 1

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 477015 Color: 1
Size: 264752 Color: 2
Size: 258234 Color: 3

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 477147 Color: 0
Size: 262054 Color: 0
Size: 260800 Color: 4

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 477379 Color: 1
Size: 269947 Color: 0
Size: 252675 Color: 3

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 477390 Color: 0
Size: 272286 Color: 3
Size: 250325 Color: 4

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 477430 Color: 3
Size: 266051 Color: 0
Size: 256520 Color: 4

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 477647 Color: 4
Size: 270215 Color: 0
Size: 252139 Color: 1

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 477680 Color: 2
Size: 266120 Color: 3
Size: 256201 Color: 2

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 477753 Color: 1
Size: 270337 Color: 0
Size: 251911 Color: 4

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 477777 Color: 4
Size: 270151 Color: 3
Size: 252073 Color: 2

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 478016 Color: 1
Size: 270762 Color: 3
Size: 251223 Color: 3

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 478200 Color: 1
Size: 268734 Color: 3
Size: 253067 Color: 2

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 478187 Color: 0
Size: 261122 Color: 2
Size: 260692 Color: 2

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 478190 Color: 0
Size: 266811 Color: 2
Size: 255000 Color: 1

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 478236 Color: 3
Size: 268588 Color: 1
Size: 253177 Color: 3

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 478353 Color: 4
Size: 263465 Color: 2
Size: 258183 Color: 3

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 478527 Color: 3
Size: 269507 Color: 1
Size: 251967 Color: 3

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 478573 Color: 0
Size: 265083 Color: 3
Size: 256345 Color: 4

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 478683 Color: 1
Size: 264304 Color: 2
Size: 257014 Color: 4

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 478578 Color: 2
Size: 260837 Color: 4
Size: 260586 Color: 0

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 478900 Color: 4
Size: 270362 Color: 4
Size: 250739 Color: 0

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 479096 Color: 2
Size: 270803 Color: 4
Size: 250102 Color: 1

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 479234 Color: 1
Size: 263558 Color: 0
Size: 257209 Color: 3

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 479271 Color: 1
Size: 266620 Color: 2
Size: 254110 Color: 3

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 479138 Color: 0
Size: 261983 Color: 4
Size: 258880 Color: 4

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 479475 Color: 4
Size: 264690 Color: 2
Size: 255836 Color: 1

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 479529 Color: 3
Size: 268220 Color: 1
Size: 252252 Color: 4

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 479531 Color: 3
Size: 266322 Color: 4
Size: 254148 Color: 4

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 479662 Color: 1
Size: 267533 Color: 4
Size: 252806 Color: 3

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 479535 Color: 4
Size: 261507 Color: 2
Size: 258959 Color: 4

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 479689 Color: 1
Size: 264719 Color: 4
Size: 255593 Color: 4

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 479716 Color: 1
Size: 268454 Color: 0
Size: 251831 Color: 4

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 479593 Color: 3
Size: 267095 Color: 2
Size: 253313 Color: 0

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 480016 Color: 2
Size: 262194 Color: 1
Size: 257791 Color: 2

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 480039 Color: 4
Size: 266431 Color: 3
Size: 253531 Color: 3

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 480086 Color: 4
Size: 266510 Color: 0
Size: 253405 Color: 1

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 480119 Color: 2
Size: 264966 Color: 1
Size: 254916 Color: 3

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 480150 Color: 2
Size: 269819 Color: 3
Size: 250032 Color: 4

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 480347 Color: 4
Size: 262490 Color: 1
Size: 257164 Color: 3

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 480387 Color: 1
Size: 268913 Color: 0
Size: 250701 Color: 3

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 480433 Color: 4
Size: 260878 Color: 2
Size: 258690 Color: 4

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 480488 Color: 2
Size: 265072 Color: 1
Size: 254441 Color: 3

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 480939 Color: 4
Size: 267869 Color: 2
Size: 251193 Color: 4

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 481015 Color: 1
Size: 260473 Color: 3
Size: 258513 Color: 2

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 481008 Color: 3
Size: 262616 Color: 4
Size: 256377 Color: 3

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 481223 Color: 1
Size: 268682 Color: 3
Size: 250096 Color: 4

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 481246 Color: 1
Size: 261216 Color: 3
Size: 257539 Color: 2

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 481310 Color: 4
Size: 267856 Color: 2
Size: 250835 Color: 4

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 481320 Color: 0
Size: 259616 Color: 1
Size: 259065 Color: 3

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 481397 Color: 4
Size: 268572 Color: 3
Size: 250032 Color: 3

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 481515 Color: 1
Size: 268207 Color: 3
Size: 250279 Color: 2

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 481562 Color: 2
Size: 261428 Color: 3
Size: 257011 Color: 3

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 481593 Color: 0
Size: 265830 Color: 1
Size: 252578 Color: 2

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 481723 Color: 3
Size: 259320 Color: 4
Size: 258958 Color: 3

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 481854 Color: 4
Size: 263926 Color: 1
Size: 254221 Color: 3

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 481873 Color: 0
Size: 267364 Color: 1
Size: 250764 Color: 2

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 481959 Color: 0
Size: 262225 Color: 0
Size: 255817 Color: 1

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 482092 Color: 4
Size: 267277 Color: 1
Size: 250632 Color: 0

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 482197 Color: 0
Size: 267443 Color: 1
Size: 250361 Color: 4

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 482301 Color: 3
Size: 266212 Color: 3
Size: 251488 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 482332 Color: 4
Size: 262590 Color: 4
Size: 255079 Color: 0

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 482454 Color: 2
Size: 260863 Color: 0
Size: 256684 Color: 3

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 482835 Color: 1
Size: 260174 Color: 2
Size: 256992 Color: 0

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 482876 Color: 1
Size: 265003 Color: 0
Size: 252122 Color: 4

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 482881 Color: 0
Size: 258831 Color: 4
Size: 258289 Color: 1

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 482898 Color: 0
Size: 260525 Color: 3
Size: 256578 Color: 2

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 483006 Color: 4
Size: 264728 Color: 1
Size: 252267 Color: 3

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 483258 Color: 1
Size: 258740 Color: 4
Size: 258003 Color: 4

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 483179 Color: 2
Size: 265250 Color: 2
Size: 251572 Color: 0

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 483316 Color: 2
Size: 258420 Color: 1
Size: 258265 Color: 1

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 483369 Color: 0
Size: 263607 Color: 4
Size: 253025 Color: 3

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 483613 Color: 2
Size: 259758 Color: 1
Size: 256630 Color: 3

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 483667 Color: 1
Size: 259714 Color: 2
Size: 256620 Color: 2

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 483639 Color: 2
Size: 265230 Color: 1
Size: 251132 Color: 2

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 483819 Color: 3
Size: 259963 Color: 4
Size: 256219 Color: 2

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 483892 Color: 1
Size: 258787 Color: 3
Size: 257322 Color: 0

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 483831 Color: 3
Size: 258849 Color: 0
Size: 257321 Color: 2

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 483922 Color: 4
Size: 259370 Color: 0
Size: 256709 Color: 1

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 483958 Color: 3
Size: 262212 Color: 2
Size: 253831 Color: 1

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 483960 Color: 4
Size: 260700 Color: 0
Size: 255341 Color: 0

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 483978 Color: 3
Size: 258832 Color: 3
Size: 257191 Color: 1

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 483981 Color: 4
Size: 260173 Color: 3
Size: 255847 Color: 1

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 484044 Color: 3
Size: 260317 Color: 3
Size: 255640 Color: 4

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 484170 Color: 4
Size: 264665 Color: 1
Size: 251166 Color: 0

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 484177 Color: 0
Size: 259389 Color: 4
Size: 256435 Color: 4

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 484268 Color: 1
Size: 262537 Color: 4
Size: 253196 Color: 0

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 484325 Color: 3
Size: 258163 Color: 2
Size: 257513 Color: 2

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 484427 Color: 1
Size: 258525 Color: 0
Size: 257049 Color: 0

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 484424 Color: 4
Size: 265076 Color: 2
Size: 250501 Color: 0

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 484458 Color: 2
Size: 259533 Color: 1
Size: 256010 Color: 3

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 484751 Color: 1
Size: 259072 Color: 2
Size: 256178 Color: 3

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 484704 Color: 2
Size: 264844 Color: 4
Size: 250453 Color: 4

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 484759 Color: 4
Size: 265158 Color: 3
Size: 250084 Color: 1

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 484904 Color: 3
Size: 259312 Color: 4
Size: 255785 Color: 0

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 484937 Color: 1
Size: 262109 Color: 3
Size: 252955 Color: 4

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 484993 Color: 2
Size: 262344 Color: 0
Size: 252664 Color: 4

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 485056 Color: 0
Size: 261116 Color: 3
Size: 253829 Color: 4

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 485095 Color: 3
Size: 263852 Color: 4
Size: 251054 Color: 1

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 485243 Color: 2
Size: 264257 Color: 1
Size: 250501 Color: 1

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 485261 Color: 2
Size: 258634 Color: 2
Size: 256106 Color: 4

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 485318 Color: 1
Size: 260495 Color: 4
Size: 254188 Color: 3

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 485364 Color: 0
Size: 258737 Color: 1
Size: 255900 Color: 2

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 485388 Color: 2
Size: 260107 Color: 3
Size: 254506 Color: 3

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 485487 Color: 2
Size: 263948 Color: 1
Size: 250566 Color: 0

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 485574 Color: 2
Size: 261963 Color: 3
Size: 252464 Color: 1

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 485586 Color: 0
Size: 263783 Color: 3
Size: 250632 Color: 4

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 485677 Color: 3
Size: 261567 Color: 4
Size: 252757 Color: 1

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 485692 Color: 3
Size: 263560 Color: 0
Size: 250749 Color: 4

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 485807 Color: 3
Size: 261592 Color: 1
Size: 252602 Color: 4

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 486191 Color: 4
Size: 260078 Color: 1
Size: 253732 Color: 2

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 486229 Color: 4
Size: 258542 Color: 2
Size: 255230 Color: 0

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 486406 Color: 0
Size: 263272 Color: 3
Size: 250323 Color: 3

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 486546 Color: 1
Size: 260893 Color: 2
Size: 252562 Color: 4

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 486536 Color: 3
Size: 257302 Color: 4
Size: 256163 Color: 0

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 486650 Color: 4
Size: 257322 Color: 1
Size: 256029 Color: 2

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 486780 Color: 3
Size: 261858 Color: 2
Size: 251363 Color: 1

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 486892 Color: 0
Size: 262134 Color: 2
Size: 250975 Color: 3

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 486939 Color: 2
Size: 256607 Color: 1
Size: 256455 Color: 4

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 487040 Color: 1
Size: 259384 Color: 2
Size: 253577 Color: 3

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 487012 Color: 3
Size: 257808 Color: 0
Size: 255181 Color: 0

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 487060 Color: 1
Size: 256716 Color: 4
Size: 256225 Color: 0

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 487060 Color: 0
Size: 262803 Color: 4
Size: 250138 Color: 2

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 487128 Color: 3
Size: 258614 Color: 1
Size: 254259 Color: 4

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 487241 Color: 1
Size: 260865 Color: 4
Size: 251895 Color: 3

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 487162 Color: 3
Size: 262794 Color: 3
Size: 250045 Color: 2

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 487303 Color: 4
Size: 256562 Color: 0
Size: 256136 Color: 1

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 487322 Color: 3
Size: 259987 Color: 2
Size: 252692 Color: 2

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 487336 Color: 3
Size: 260896 Color: 4
Size: 251769 Color: 0

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 487384 Color: 2
Size: 261445 Color: 1
Size: 251172 Color: 0

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 487392 Color: 2
Size: 258261 Color: 1
Size: 254348 Color: 4

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 487461 Color: 0
Size: 257326 Color: 0
Size: 255214 Color: 3

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 487536 Color: 4
Size: 257416 Color: 1
Size: 255049 Color: 2

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 487589 Color: 2
Size: 260999 Color: 3
Size: 251413 Color: 4

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 487778 Color: 4
Size: 258336 Color: 3
Size: 253887 Color: 1

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 487877 Color: 3
Size: 256233 Color: 1
Size: 255891 Color: 4

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 487946 Color: 2
Size: 256070 Color: 1
Size: 255985 Color: 2

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 487948 Color: 3
Size: 256108 Color: 2
Size: 255945 Color: 1

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 488195 Color: 4
Size: 261383 Color: 0
Size: 250423 Color: 3

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 488294 Color: 3
Size: 257560 Color: 0
Size: 254147 Color: 4

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 488455 Color: 1
Size: 260300 Color: 4
Size: 251246 Color: 4

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 488460 Color: 0
Size: 260082 Color: 2
Size: 251459 Color: 1

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 488484 Color: 3
Size: 257345 Color: 0
Size: 254172 Color: 1

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 488485 Color: 4
Size: 261097 Color: 2
Size: 250419 Color: 2

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 488656 Color: 1
Size: 260195 Color: 2
Size: 251150 Color: 4

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 488782 Color: 1
Size: 258164 Color: 3
Size: 253055 Color: 4

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 488939 Color: 4
Size: 259174 Color: 3
Size: 251888 Color: 0

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 489039 Color: 0
Size: 258052 Color: 4
Size: 252910 Color: 1

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 489057 Color: 2
Size: 255551 Color: 3
Size: 255393 Color: 0

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 489137 Color: 1
Size: 257182 Color: 0
Size: 253682 Color: 0

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 489141 Color: 1
Size: 260820 Color: 4
Size: 250040 Color: 2

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 489111 Color: 3
Size: 259964 Color: 4
Size: 250926 Color: 0

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 489211 Color: 3
Size: 257800 Color: 1
Size: 252990 Color: 4

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 489300 Color: 0
Size: 259453 Color: 3
Size: 251248 Color: 1

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 489301 Color: 2
Size: 257637 Color: 4
Size: 253063 Color: 1

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 489578 Color: 4
Size: 258606 Color: 3
Size: 251817 Color: 3

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 489639 Color: 3
Size: 260072 Color: 2
Size: 250290 Color: 1

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 489839 Color: 4
Size: 259373 Color: 4
Size: 250789 Color: 3

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 490017 Color: 1
Size: 255152 Color: 0
Size: 254832 Color: 2

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 490094 Color: 1
Size: 259121 Color: 2
Size: 250786 Color: 3

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 490071 Color: 4
Size: 257973 Color: 4
Size: 251957 Color: 2

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 490190 Color: 3
Size: 256406 Color: 2
Size: 253405 Color: 1

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 490192 Color: 3
Size: 256655 Color: 0
Size: 253154 Color: 2

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 490385 Color: 4
Size: 258540 Color: 0
Size: 251076 Color: 1

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 490530 Color: 4
Size: 258176 Color: 3
Size: 251295 Color: 2

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 490670 Color: 0
Size: 256613 Color: 1
Size: 252718 Color: 2

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 490749 Color: 3
Size: 257805 Color: 0
Size: 251447 Color: 4

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 491171 Color: 1
Size: 256411 Color: 0
Size: 252419 Color: 0

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 491380 Color: 1
Size: 254437 Color: 1
Size: 254184 Color: 4

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 491548 Color: 0
Size: 256356 Color: 1
Size: 252097 Color: 3

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 491629 Color: 1
Size: 254909 Color: 3
Size: 253463 Color: 4

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 491560 Color: 4
Size: 254581 Color: 3
Size: 253860 Color: 0

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 491713 Color: 1
Size: 257703 Color: 3
Size: 250585 Color: 3

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 491645 Color: 2
Size: 258009 Color: 0
Size: 250347 Color: 0

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 491900 Color: 3
Size: 256901 Color: 1
Size: 251200 Color: 3

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 492000 Color: 3
Size: 256690 Color: 2
Size: 251311 Color: 1

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 492133 Color: 4
Size: 256111 Color: 0
Size: 251757 Color: 1

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 492177 Color: 1
Size: 256867 Color: 0
Size: 250957 Color: 2

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 492141 Color: 3
Size: 255014 Color: 4
Size: 252846 Color: 3

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 492158 Color: 3
Size: 255151 Color: 1
Size: 252692 Color: 1

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 492268 Color: 3
Size: 254786 Color: 1
Size: 252947 Color: 0

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 492349 Color: 0
Size: 256653 Color: 3
Size: 250999 Color: 2

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 492428 Color: 2
Size: 256023 Color: 4
Size: 251550 Color: 0

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 492548 Color: 1
Size: 257340 Color: 2
Size: 250113 Color: 3

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 492587 Color: 1
Size: 257070 Color: 3
Size: 250344 Color: 4

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 492528 Color: 3
Size: 254851 Color: 4
Size: 252622 Color: 2

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 492592 Color: 2
Size: 255752 Color: 1
Size: 251657 Color: 0

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 492707 Color: 0
Size: 255108 Color: 1
Size: 252186 Color: 2

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 492728 Color: 2
Size: 255563 Color: 0
Size: 251710 Color: 3

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 493177 Color: 1
Size: 255599 Color: 2
Size: 251225 Color: 4

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 493063 Color: 3
Size: 254048 Color: 4
Size: 252890 Color: 3

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 493121 Color: 0
Size: 255949 Color: 3
Size: 250931 Color: 1

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 493232 Color: 2
Size: 254277 Color: 3
Size: 252492 Color: 1

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 493245 Color: 0
Size: 254129 Color: 4
Size: 252627 Color: 3

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 493509 Color: 1
Size: 255328 Color: 0
Size: 251164 Color: 3

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 493535 Color: 3
Size: 254363 Color: 1
Size: 252103 Color: 2

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 493595 Color: 3
Size: 254049 Color: 4
Size: 252357 Color: 1

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 493604 Color: 0
Size: 255229 Color: 4
Size: 251168 Color: 3

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 493620 Color: 0
Size: 255284 Color: 1
Size: 251097 Color: 4

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 493683 Color: 0
Size: 253575 Color: 1
Size: 252743 Color: 0

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 493744 Color: 2
Size: 255037 Color: 0
Size: 251220 Color: 0

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 493855 Color: 1
Size: 254379 Color: 0
Size: 251767 Color: 3

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 493914 Color: 4
Size: 254237 Color: 3
Size: 251850 Color: 2

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 494008 Color: 1
Size: 253143 Color: 3
Size: 252850 Color: 4

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 494003 Color: 0
Size: 255140 Color: 4
Size: 250858 Color: 2

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 494118 Color: 4
Size: 254903 Color: 1
Size: 250980 Color: 3

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 494135 Color: 0
Size: 254981 Color: 1
Size: 250885 Color: 2

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 494327 Color: 4
Size: 255195 Color: 4
Size: 250479 Color: 2

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 494414 Color: 1
Size: 254077 Color: 4
Size: 251510 Color: 0

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 494328 Color: 2
Size: 254916 Color: 3
Size: 250757 Color: 3

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 494387 Color: 4
Size: 253454 Color: 2
Size: 252160 Color: 1

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 494478 Color: 1
Size: 254876 Color: 3
Size: 250647 Color: 3

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 494405 Color: 4
Size: 253360 Color: 0
Size: 252236 Color: 3

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 494525 Color: 1
Size: 253895 Color: 3
Size: 251581 Color: 0

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 494866 Color: 4
Size: 253692 Color: 0
Size: 251443 Color: 2

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 494929 Color: 3
Size: 254810 Color: 1
Size: 250262 Color: 0

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 494958 Color: 2
Size: 254880 Color: 1
Size: 250163 Color: 0

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 494939 Color: 1
Size: 254350 Color: 4
Size: 250712 Color: 2

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 495053 Color: 3
Size: 254817 Color: 3
Size: 250131 Color: 2

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 495305 Color: 1
Size: 254561 Color: 3
Size: 250135 Color: 2

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 495296 Color: 4
Size: 253641 Color: 0
Size: 251064 Color: 3

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 495315 Color: 4
Size: 254615 Color: 0
Size: 250071 Color: 1

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 495348 Color: 3
Size: 253019 Color: 4
Size: 251634 Color: 1

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 495481 Color: 0
Size: 252564 Color: 3
Size: 251956 Color: 1

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 495538 Color: 3
Size: 253780 Color: 4
Size: 250683 Color: 2

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 495606 Color: 2
Size: 253472 Color: 3
Size: 250923 Color: 3

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 495632 Color: 4
Size: 253280 Color: 1
Size: 251089 Color: 3

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 495676 Color: 0
Size: 253096 Color: 1
Size: 251229 Color: 4

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 495723 Color: 4
Size: 252805 Color: 3
Size: 251473 Color: 0

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 495740 Color: 2
Size: 253458 Color: 3
Size: 250803 Color: 1

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 495740 Color: 1
Size: 253579 Color: 4
Size: 250682 Color: 0

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 495966 Color: 2
Size: 253093 Color: 2
Size: 250942 Color: 4

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 495967 Color: 2
Size: 254027 Color: 4
Size: 250007 Color: 1

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 495977 Color: 2
Size: 253205 Color: 3
Size: 250819 Color: 4

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 496002 Color: 4
Size: 252638 Color: 0
Size: 251361 Color: 1

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 496015 Color: 0
Size: 253505 Color: 1
Size: 250481 Color: 2

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 496102 Color: 0
Size: 252915 Color: 1
Size: 250984 Color: 2

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 496450 Color: 2
Size: 252411 Color: 4
Size: 251140 Color: 2

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 496523 Color: 4
Size: 253278 Color: 4
Size: 250200 Color: 1

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 496580 Color: 4
Size: 252317 Color: 1
Size: 251104 Color: 0

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 496673 Color: 0
Size: 253303 Color: 4
Size: 250025 Color: 0

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 496859 Color: 0
Size: 252295 Color: 1
Size: 250847 Color: 4

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 496993 Color: 0
Size: 252491 Color: 4
Size: 250517 Color: 1

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 496992 Color: 1
Size: 252664 Color: 1
Size: 250345 Color: 4

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 497018 Color: 0
Size: 252266 Color: 4
Size: 250717 Color: 4

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 497046 Color: 0
Size: 252394 Color: 1
Size: 250561 Color: 4

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 497015 Color: 1
Size: 251673 Color: 0
Size: 251313 Color: 0

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 497093 Color: 0
Size: 251827 Color: 3
Size: 251081 Color: 3

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 497095 Color: 2
Size: 251721 Color: 3
Size: 251185 Color: 1

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 497109 Color: 3
Size: 251950 Color: 4
Size: 250942 Color: 0

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 497184 Color: 2
Size: 251520 Color: 0
Size: 251297 Color: 1

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 497245 Color: 0
Size: 251816 Color: 0
Size: 250940 Color: 2

Bin 3126: 0 of cap free
Amount of items: 3
Items: 
Size: 497276 Color: 4
Size: 252601 Color: 1
Size: 250124 Color: 4

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 497281 Color: 4
Size: 252031 Color: 1
Size: 250689 Color: 0

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 497345 Color: 0
Size: 251440 Color: 4
Size: 251216 Color: 4

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 497360 Color: 2
Size: 252318 Color: 0
Size: 250323 Color: 1

Bin 3130: 0 of cap free
Amount of items: 3
Items: 
Size: 497595 Color: 1
Size: 251837 Color: 0
Size: 250569 Color: 2

Bin 3131: 0 of cap free
Amount of items: 3
Items: 
Size: 497546 Color: 0
Size: 252276 Color: 1
Size: 250179 Color: 4

Bin 3132: 0 of cap free
Amount of items: 3
Items: 
Size: 497822 Color: 3
Size: 252060 Color: 0
Size: 250119 Color: 2

Bin 3133: 0 of cap free
Amount of items: 3
Items: 
Size: 498071 Color: 4
Size: 251124 Color: 2
Size: 250806 Color: 1

Bin 3134: 0 of cap free
Amount of items: 3
Items: 
Size: 498090 Color: 3
Size: 251660 Color: 0
Size: 250251 Color: 1

Bin 3135: 0 of cap free
Amount of items: 3
Items: 
Size: 498185 Color: 1
Size: 251124 Color: 3
Size: 250692 Color: 2

Bin 3136: 0 of cap free
Amount of items: 3
Items: 
Size: 498486 Color: 0
Size: 251029 Color: 1
Size: 250486 Color: 2

Bin 3137: 0 of cap free
Amount of items: 3
Items: 
Size: 499099 Color: 3
Size: 250707 Color: 1
Size: 250195 Color: 2

Bin 3138: 0 of cap free
Amount of items: 3
Items: 
Size: 499201 Color: 0
Size: 250589 Color: 4
Size: 250211 Color: 1

Bin 3139: 0 of cap free
Amount of items: 3
Items: 
Size: 499394 Color: 2
Size: 250424 Color: 0
Size: 250183 Color: 1

Bin 3140: 0 of cap free
Amount of items: 3
Items: 
Size: 499548 Color: 1
Size: 250308 Color: 0
Size: 250145 Color: 4

Bin 3141: 0 of cap free
Amount of items: 3
Items: 
Size: 499592 Color: 1
Size: 250316 Color: 3
Size: 250093 Color: 0

Bin 3142: 0 of cap free
Amount of items: 3
Items: 
Size: 499840 Color: 1
Size: 250149 Color: 3
Size: 250012 Color: 4

Bin 3143: 0 of cap free
Amount of items: 3
Items: 
Size: 499819 Color: 2
Size: 250140 Color: 2
Size: 250042 Color: 1

Bin 3144: 1 of cap free
Amount of items: 3
Items: 
Size: 374246 Color: 1
Size: 357189 Color: 0
Size: 268565 Color: 4

Bin 3145: 1 of cap free
Amount of items: 3
Items: 
Size: 358355 Color: 1
Size: 350028 Color: 2
Size: 291617 Color: 0

Bin 3146: 1 of cap free
Amount of items: 3
Items: 
Size: 350582 Color: 1
Size: 337202 Color: 4
Size: 312216 Color: 0

Bin 3147: 1 of cap free
Amount of items: 3
Items: 
Size: 367499 Color: 0
Size: 361710 Color: 1
Size: 270791 Color: 3

Bin 3148: 1 of cap free
Amount of items: 3
Items: 
Size: 375073 Color: 4
Size: 327554 Color: 2
Size: 297373 Color: 1

Bin 3149: 1 of cap free
Amount of items: 3
Items: 
Size: 343412 Color: 2
Size: 329995 Color: 2
Size: 326593 Color: 4

Bin 3150: 1 of cap free
Amount of items: 3
Items: 
Size: 355871 Color: 0
Size: 343556 Color: 1
Size: 300573 Color: 3

Bin 3151: 1 of cap free
Amount of items: 3
Items: 
Size: 356739 Color: 1
Size: 352406 Color: 4
Size: 290855 Color: 2

Bin 3152: 1 of cap free
Amount of items: 3
Items: 
Size: 358020 Color: 3
Size: 328860 Color: 3
Size: 313120 Color: 4

Bin 3153: 1 of cap free
Amount of items: 3
Items: 
Size: 358146 Color: 3
Size: 348685 Color: 3
Size: 293169 Color: 4

Bin 3154: 1 of cap free
Amount of items: 3
Items: 
Size: 358095 Color: 2
Size: 328463 Color: 0
Size: 313442 Color: 2

Bin 3155: 1 of cap free
Amount of items: 3
Items: 
Size: 358553 Color: 0
Size: 343737 Color: 4
Size: 297710 Color: 2

Bin 3156: 1 of cap free
Amount of items: 3
Items: 
Size: 359727 Color: 2
Size: 332431 Color: 4
Size: 307842 Color: 0

Bin 3157: 1 of cap free
Amount of items: 3
Items: 
Size: 359845 Color: 2
Size: 355251 Color: 1
Size: 284904 Color: 2

Bin 3158: 1 of cap free
Amount of items: 3
Items: 
Size: 360608 Color: 3
Size: 352172 Color: 4
Size: 287220 Color: 1

Bin 3159: 1 of cap free
Amount of items: 3
Items: 
Size: 361092 Color: 2
Size: 322094 Color: 1
Size: 316814 Color: 3

Bin 3160: 1 of cap free
Amount of items: 3
Items: 
Size: 361705 Color: 0
Size: 319160 Color: 0
Size: 319135 Color: 2

Bin 3161: 1 of cap free
Amount of items: 3
Items: 
Size: 361688 Color: 2
Size: 322365 Color: 3
Size: 315947 Color: 4

Bin 3162: 1 of cap free
Amount of items: 3
Items: 
Size: 350909 Color: 0
Size: 335128 Color: 0
Size: 313963 Color: 3

Bin 3163: 1 of cap free
Amount of items: 3
Items: 
Size: 363352 Color: 2
Size: 347008 Color: 0
Size: 289640 Color: 2

Bin 3164: 1 of cap free
Amount of items: 3
Items: 
Size: 364039 Color: 1
Size: 358816 Color: 2
Size: 277145 Color: 0

Bin 3165: 1 of cap free
Amount of items: 3
Items: 
Size: 364185 Color: 2
Size: 323995 Color: 0
Size: 311820 Color: 4

Bin 3166: 1 of cap free
Amount of items: 3
Items: 
Size: 364433 Color: 4
Size: 360291 Color: 3
Size: 275276 Color: 0

Bin 3167: 1 of cap free
Amount of items: 3
Items: 
Size: 365015 Color: 0
Size: 340453 Color: 3
Size: 294532 Color: 2

Bin 3168: 1 of cap free
Amount of items: 3
Items: 
Size: 365430 Color: 4
Size: 364994 Color: 1
Size: 269576 Color: 0

Bin 3169: 1 of cap free
Amount of items: 3
Items: 
Size: 367296 Color: 3
Size: 319280 Color: 0
Size: 313424 Color: 3

Bin 3170: 1 of cap free
Amount of items: 3
Items: 
Size: 368181 Color: 3
Size: 365392 Color: 0
Size: 266427 Color: 1

Bin 3171: 1 of cap free
Amount of items: 3
Items: 
Size: 368265 Color: 1
Size: 335845 Color: 0
Size: 295890 Color: 1

Bin 3172: 1 of cap free
Amount of items: 3
Items: 
Size: 369038 Color: 3
Size: 333080 Color: 1
Size: 297882 Color: 4

Bin 3173: 1 of cap free
Amount of items: 3
Items: 
Size: 369380 Color: 0
Size: 352425 Color: 3
Size: 278195 Color: 1

Bin 3174: 1 of cap free
Amount of items: 3
Items: 
Size: 369988 Color: 2
Size: 364778 Color: 4
Size: 265234 Color: 2

Bin 3175: 1 of cap free
Amount of items: 3
Items: 
Size: 372347 Color: 3
Size: 316637 Color: 1
Size: 311016 Color: 1

Bin 3176: 1 of cap free
Amount of items: 3
Items: 
Size: 360464 Color: 4
Size: 357248 Color: 2
Size: 282288 Color: 3

Bin 3177: 1 of cap free
Amount of items: 3
Items: 
Size: 365197 Color: 2
Size: 357717 Color: 3
Size: 277086 Color: 3

Bin 3178: 1 of cap free
Amount of items: 3
Items: 
Size: 357528 Color: 4
Size: 349341 Color: 2
Size: 293131 Color: 0

Bin 3179: 1 of cap free
Amount of items: 3
Items: 
Size: 349511 Color: 2
Size: 340588 Color: 4
Size: 309901 Color: 1

Bin 3180: 1 of cap free
Amount of items: 3
Items: 
Size: 342343 Color: 2
Size: 330974 Color: 3
Size: 326683 Color: 4

Bin 3181: 1 of cap free
Amount of items: 3
Items: 
Size: 347875 Color: 2
Size: 329919 Color: 3
Size: 322206 Color: 4

Bin 3182: 1 of cap free
Amount of items: 3
Items: 
Size: 340866 Color: 0
Size: 332239 Color: 1
Size: 326895 Color: 0

Bin 3183: 1 of cap free
Amount of items: 3
Items: 
Size: 359095 Color: 4
Size: 340889 Color: 3
Size: 300016 Color: 0

Bin 3184: 1 of cap free
Amount of items: 3
Items: 
Size: 351784 Color: 0
Size: 332690 Color: 1
Size: 315526 Color: 2

Bin 3185: 1 of cap free
Amount of items: 3
Items: 
Size: 358525 Color: 1
Size: 352683 Color: 1
Size: 288792 Color: 0

Bin 3186: 1 of cap free
Amount of items: 3
Items: 
Size: 343713 Color: 3
Size: 342616 Color: 0
Size: 313671 Color: 1

Bin 3187: 1 of cap free
Amount of items: 3
Items: 
Size: 387741 Color: 4
Size: 351005 Color: 3
Size: 261254 Color: 4

Bin 3188: 1 of cap free
Amount of items: 3
Items: 
Size: 369166 Color: 3
Size: 359217 Color: 1
Size: 271617 Color: 4

Bin 3189: 1 of cap free
Amount of items: 3
Items: 
Size: 345862 Color: 0
Size: 345516 Color: 4
Size: 308622 Color: 4

Bin 3190: 1 of cap free
Amount of items: 3
Items: 
Size: 349886 Color: 0
Size: 342621 Color: 0
Size: 307493 Color: 1

Bin 3191: 1 of cap free
Amount of items: 3
Items: 
Size: 412365 Color: 1
Size: 334048 Color: 1
Size: 253587 Color: 4

Bin 3192: 1 of cap free
Amount of items: 3
Items: 
Size: 337465 Color: 4
Size: 336160 Color: 0
Size: 326375 Color: 0

Bin 3193: 1 of cap free
Amount of items: 3
Items: 
Size: 421576 Color: 1
Size: 304165 Color: 3
Size: 274259 Color: 3

Bin 3194: 1 of cap free
Amount of items: 3
Items: 
Size: 429220 Color: 0
Size: 307152 Color: 2
Size: 263628 Color: 3

Bin 3195: 2 of cap free
Amount of items: 3
Items: 
Size: 387398 Color: 0
Size: 349336 Color: 3
Size: 263265 Color: 4

Bin 3196: 2 of cap free
Amount of items: 3
Items: 
Size: 352953 Color: 0
Size: 332712 Color: 0
Size: 314334 Color: 3

Bin 3197: 2 of cap free
Amount of items: 3
Items: 
Size: 357342 Color: 0
Size: 321611 Color: 1
Size: 321046 Color: 1

Bin 3198: 2 of cap free
Amount of items: 3
Items: 
Size: 363119 Color: 4
Size: 344754 Color: 2
Size: 292126 Color: 1

Bin 3199: 2 of cap free
Amount of items: 3
Items: 
Size: 344695 Color: 4
Size: 328163 Color: 2
Size: 327141 Color: 2

Bin 3200: 2 of cap free
Amount of items: 3
Items: 
Size: 361774 Color: 0
Size: 357501 Color: 3
Size: 280724 Color: 1

Bin 3201: 2 of cap free
Amount of items: 3
Items: 
Size: 375245 Color: 4
Size: 356858 Color: 0
Size: 267896 Color: 0

Bin 3202: 2 of cap free
Amount of items: 3
Items: 
Size: 338691 Color: 1
Size: 334243 Color: 0
Size: 327065 Color: 2

Bin 3203: 2 of cap free
Amount of items: 3
Items: 
Size: 385098 Color: 0
Size: 338685 Color: 3
Size: 276216 Color: 3

Bin 3204: 3 of cap free
Amount of items: 3
Items: 
Size: 360665 Color: 2
Size: 357046 Color: 4
Size: 282287 Color: 2

Bin 3205: 3 of cap free
Amount of items: 3
Items: 
Size: 357460 Color: 1
Size: 349625 Color: 1
Size: 292913 Color: 4

Bin 3206: 3 of cap free
Amount of items: 3
Items: 
Size: 426430 Color: 1
Size: 288723 Color: 2
Size: 284845 Color: 4

Bin 3207: 3 of cap free
Amount of items: 3
Items: 
Size: 361133 Color: 4
Size: 330319 Color: 0
Size: 308546 Color: 1

Bin 3208: 3 of cap free
Amount of items: 3
Items: 
Size: 351361 Color: 3
Size: 325262 Color: 2
Size: 323375 Color: 0

Bin 3209: 3 of cap free
Amount of items: 3
Items: 
Size: 356437 Color: 4
Size: 331213 Color: 3
Size: 312348 Color: 0

Bin 3210: 3 of cap free
Amount of items: 3
Items: 
Size: 362249 Color: 3
Size: 324432 Color: 0
Size: 313317 Color: 1

Bin 3211: 3 of cap free
Amount of items: 3
Items: 
Size: 357800 Color: 2
Size: 357137 Color: 0
Size: 285061 Color: 0

Bin 3212: 3 of cap free
Amount of items: 3
Items: 
Size: 498123 Color: 3
Size: 251055 Color: 3
Size: 250820 Color: 2

Bin 3213: 3 of cap free
Amount of items: 3
Items: 
Size: 355151 Color: 4
Size: 352836 Color: 1
Size: 292011 Color: 0

Bin 3214: 3 of cap free
Amount of items: 3
Items: 
Size: 351089 Color: 2
Size: 332243 Color: 2
Size: 316666 Color: 1

Bin 3215: 3 of cap free
Amount of items: 3
Items: 
Size: 366796 Color: 3
Size: 357668 Color: 3
Size: 275534 Color: 4

Bin 3216: 3 of cap free
Amount of items: 3
Items: 
Size: 346865 Color: 4
Size: 344887 Color: 1
Size: 308246 Color: 1

Bin 3217: 4 of cap free
Amount of items: 3
Items: 
Size: 368222 Color: 2
Size: 319309 Color: 4
Size: 312466 Color: 0

Bin 3218: 4 of cap free
Amount of items: 3
Items: 
Size: 359302 Color: 1
Size: 323751 Color: 3
Size: 316944 Color: 3

Bin 3219: 4 of cap free
Amount of items: 3
Items: 
Size: 354392 Color: 3
Size: 354379 Color: 2
Size: 291226 Color: 3

Bin 3220: 4 of cap free
Amount of items: 3
Items: 
Size: 346052 Color: 2
Size: 333091 Color: 2
Size: 320854 Color: 1

Bin 3221: 4 of cap free
Amount of items: 3
Items: 
Size: 346359 Color: 0
Size: 331701 Color: 2
Size: 321937 Color: 1

Bin 3222: 4 of cap free
Amount of items: 3
Items: 
Size: 337296 Color: 0
Size: 331433 Color: 1
Size: 331268 Color: 4

Bin 3223: 5 of cap free
Amount of items: 3
Items: 
Size: 356533 Color: 1
Size: 342191 Color: 1
Size: 301272 Color: 4

Bin 3224: 5 of cap free
Amount of items: 3
Items: 
Size: 348841 Color: 0
Size: 336218 Color: 1
Size: 314937 Color: 2

Bin 3225: 5 of cap free
Amount of items: 3
Items: 
Size: 345389 Color: 2
Size: 344917 Color: 3
Size: 309690 Color: 2

Bin 3226: 5 of cap free
Amount of items: 3
Items: 
Size: 361955 Color: 1
Size: 358131 Color: 0
Size: 279910 Color: 1

Bin 3227: 5 of cap free
Amount of items: 3
Items: 
Size: 385967 Color: 4
Size: 354002 Color: 4
Size: 260027 Color: 2

Bin 3228: 6 of cap free
Amount of items: 3
Items: 
Size: 354544 Color: 2
Size: 343902 Color: 3
Size: 301549 Color: 0

Bin 3229: 6 of cap free
Amount of items: 3
Items: 
Size: 342178 Color: 0
Size: 332648 Color: 2
Size: 325169 Color: 1

Bin 3230: 6 of cap free
Amount of items: 3
Items: 
Size: 352052 Color: 3
Size: 344099 Color: 1
Size: 303844 Color: 1

Bin 3231: 6 of cap free
Amount of items: 3
Items: 
Size: 358116 Color: 2
Size: 355402 Color: 0
Size: 286477 Color: 3

Bin 3232: 7 of cap free
Amount of items: 3
Items: 
Size: 346323 Color: 3
Size: 346003 Color: 4
Size: 307668 Color: 2

Bin 3233: 7 of cap free
Amount of items: 3
Items: 
Size: 499211 Color: 4
Size: 250669 Color: 3
Size: 250114 Color: 2

Bin 3234: 7 of cap free
Amount of items: 3
Items: 
Size: 344126 Color: 4
Size: 340812 Color: 4
Size: 315056 Color: 1

Bin 3235: 7 of cap free
Amount of items: 3
Items: 
Size: 360699 Color: 2
Size: 359477 Color: 2
Size: 279818 Color: 1

Bin 3236: 8 of cap free
Amount of items: 3
Items: 
Size: 358429 Color: 4
Size: 355544 Color: 0
Size: 286020 Color: 0

Bin 3237: 9 of cap free
Amount of items: 3
Items: 
Size: 496894 Color: 3
Size: 251842 Color: 0
Size: 251256 Color: 2

Bin 3238: 9 of cap free
Amount of items: 3
Items: 
Size: 358652 Color: 1
Size: 356550 Color: 2
Size: 284790 Color: 4

Bin 3239: 9 of cap free
Amount of items: 3
Items: 
Size: 372183 Color: 3
Size: 355473 Color: 1
Size: 272336 Color: 4

Bin 3240: 9 of cap free
Amount of items: 3
Items: 
Size: 463501 Color: 0
Size: 270068 Color: 2
Size: 266423 Color: 1

Bin 3241: 10 of cap free
Amount of items: 3
Items: 
Size: 361251 Color: 2
Size: 359185 Color: 3
Size: 279555 Color: 3

Bin 3242: 10 of cap free
Amount of items: 3
Items: 
Size: 399858 Color: 2
Size: 340002 Color: 2
Size: 260131 Color: 0

Bin 3243: 11 of cap free
Amount of items: 3
Items: 
Size: 342107 Color: 4
Size: 334923 Color: 3
Size: 322960 Color: 1

Bin 3244: 11 of cap free
Amount of items: 3
Items: 
Size: 375028 Color: 2
Size: 355363 Color: 2
Size: 269599 Color: 4

Bin 3245: 11 of cap free
Amount of items: 3
Items: 
Size: 358369 Color: 4
Size: 349284 Color: 1
Size: 292337 Color: 2

Bin 3246: 12 of cap free
Amount of items: 3
Items: 
Size: 445973 Color: 1
Size: 280494 Color: 4
Size: 273522 Color: 4

Bin 3247: 12 of cap free
Amount of items: 3
Items: 
Size: 366263 Color: 3
Size: 364333 Color: 0
Size: 269393 Color: 3

Bin 3248: 12 of cap free
Amount of items: 3
Items: 
Size: 385936 Color: 0
Size: 352273 Color: 4
Size: 261780 Color: 2

Bin 3249: 13 of cap free
Amount of items: 3
Items: 
Size: 348178 Color: 1
Size: 337444 Color: 0
Size: 314366 Color: 0

Bin 3250: 13 of cap free
Amount of items: 3
Items: 
Size: 353460 Color: 1
Size: 340602 Color: 0
Size: 305926 Color: 1

Bin 3251: 13 of cap free
Amount of items: 3
Items: 
Size: 359077 Color: 1
Size: 345817 Color: 0
Size: 295094 Color: 0

Bin 3252: 14 of cap free
Amount of items: 3
Items: 
Size: 349397 Color: 3
Size: 343832 Color: 3
Size: 306758 Color: 4

Bin 3253: 15 of cap free
Amount of items: 3
Items: 
Size: 350263 Color: 0
Size: 347197 Color: 1
Size: 302526 Color: 1

Bin 3254: 15 of cap free
Amount of items: 3
Items: 
Size: 435122 Color: 4
Size: 313542 Color: 2
Size: 251322 Color: 0

Bin 3255: 15 of cap free
Amount of items: 3
Items: 
Size: 356618 Color: 4
Size: 327794 Color: 4
Size: 315574 Color: 2

Bin 3256: 18 of cap free
Amount of items: 3
Items: 
Size: 470830 Color: 1
Size: 268152 Color: 4
Size: 261001 Color: 3

Bin 3257: 18 of cap free
Amount of items: 3
Items: 
Size: 347430 Color: 0
Size: 345488 Color: 1
Size: 307065 Color: 2

Bin 3258: 19 of cap free
Amount of items: 3
Items: 
Size: 373511 Color: 3
Size: 325342 Color: 1
Size: 301129 Color: 0

Bin 3259: 19 of cap free
Amount of items: 3
Items: 
Size: 352290 Color: 4
Size: 345334 Color: 4
Size: 302358 Color: 0

Bin 3260: 22 of cap free
Amount of items: 3
Items: 
Size: 351643 Color: 1
Size: 325508 Color: 2
Size: 322828 Color: 1

Bin 3261: 23 of cap free
Amount of items: 3
Items: 
Size: 336026 Color: 4
Size: 335932 Color: 1
Size: 328020 Color: 2

Bin 3262: 23 of cap free
Amount of items: 3
Items: 
Size: 369353 Color: 2
Size: 354566 Color: 3
Size: 276059 Color: 1

Bin 3263: 24 of cap free
Amount of items: 3
Items: 
Size: 361547 Color: 2
Size: 334086 Color: 2
Size: 304344 Color: 1

Bin 3264: 26 of cap free
Amount of items: 3
Items: 
Size: 357489 Color: 4
Size: 338198 Color: 2
Size: 304288 Color: 4

Bin 3265: 27 of cap free
Amount of items: 3
Items: 
Size: 462431 Color: 1
Size: 271920 Color: 2
Size: 265623 Color: 0

Bin 3266: 27 of cap free
Amount of items: 3
Items: 
Size: 353100 Color: 0
Size: 324385 Color: 3
Size: 322489 Color: 2

Bin 3267: 28 of cap free
Amount of items: 3
Items: 
Size: 373097 Color: 3
Size: 350097 Color: 2
Size: 276779 Color: 4

Bin 3268: 32 of cap free
Amount of items: 3
Items: 
Size: 344750 Color: 3
Size: 332125 Color: 3
Size: 323094 Color: 1

Bin 3269: 35 of cap free
Amount of items: 3
Items: 
Size: 360367 Color: 3
Size: 354391 Color: 3
Size: 285208 Color: 4

Bin 3270: 36 of cap free
Amount of items: 3
Items: 
Size: 362732 Color: 1
Size: 360300 Color: 2
Size: 276933 Color: 3

Bin 3271: 36 of cap free
Amount of items: 3
Items: 
Size: 344202 Color: 3
Size: 330293 Color: 4
Size: 325470 Color: 2

Bin 3272: 38 of cap free
Amount of items: 3
Items: 
Size: 367511 Color: 0
Size: 360761 Color: 4
Size: 271691 Color: 3

Bin 3273: 42 of cap free
Amount of items: 3
Items: 
Size: 439538 Color: 0
Size: 307790 Color: 2
Size: 252631 Color: 2

Bin 3274: 46 of cap free
Amount of items: 3
Items: 
Size: 416384 Color: 3
Size: 330350 Color: 3
Size: 253221 Color: 0

Bin 3275: 47 of cap free
Amount of items: 3
Items: 
Size: 371303 Color: 2
Size: 356420 Color: 2
Size: 272231 Color: 0

Bin 3276: 53 of cap free
Amount of items: 3
Items: 
Size: 471648 Color: 3
Size: 274673 Color: 4
Size: 253627 Color: 4

Bin 3277: 54 of cap free
Amount of items: 3
Items: 
Size: 363990 Color: 3
Size: 363297 Color: 3
Size: 272660 Color: 1

Bin 3278: 54 of cap free
Amount of items: 3
Items: 
Size: 372099 Color: 3
Size: 358033 Color: 2
Size: 269815 Color: 0

Bin 3279: 55 of cap free
Amount of items: 3
Items: 
Size: 421046 Color: 2
Size: 327849 Color: 3
Size: 251051 Color: 1

Bin 3280: 56 of cap free
Amount of items: 3
Items: 
Size: 344957 Color: 4
Size: 328985 Color: 0
Size: 326003 Color: 4

Bin 3281: 56 of cap free
Amount of items: 3
Items: 
Size: 352922 Color: 0
Size: 348251 Color: 1
Size: 298772 Color: 3

Bin 3282: 57 of cap free
Amount of items: 3
Items: 
Size: 369467 Color: 0
Size: 358866 Color: 4
Size: 271611 Color: 4

Bin 3283: 59 of cap free
Amount of items: 3
Items: 
Size: 466348 Color: 4
Size: 279114 Color: 4
Size: 254480 Color: 2

Bin 3284: 63 of cap free
Amount of items: 3
Items: 
Size: 357402 Color: 0
Size: 351440 Color: 4
Size: 291096 Color: 3

Bin 3285: 68 of cap free
Amount of items: 3
Items: 
Size: 464136 Color: 1
Size: 268261 Color: 4
Size: 267536 Color: 1

Bin 3286: 70 of cap free
Amount of items: 3
Items: 
Size: 459820 Color: 1
Size: 278429 Color: 2
Size: 261682 Color: 0

Bin 3287: 80 of cap free
Amount of items: 3
Items: 
Size: 366511 Color: 2
Size: 357231 Color: 0
Size: 276179 Color: 1

Bin 3288: 86 of cap free
Amount of items: 3
Items: 
Size: 478724 Color: 4
Size: 269144 Color: 2
Size: 252047 Color: 0

Bin 3289: 88 of cap free
Amount of items: 3
Items: 
Size: 498324 Color: 3
Size: 251351 Color: 2
Size: 250238 Color: 4

Bin 3290: 88 of cap free
Amount of items: 3
Items: 
Size: 474109 Color: 3
Size: 266505 Color: 3
Size: 259299 Color: 2

Bin 3291: 90 of cap free
Amount of items: 3
Items: 
Size: 464685 Color: 3
Size: 281828 Color: 1
Size: 253398 Color: 0

Bin 3292: 92 of cap free
Amount of items: 3
Items: 
Size: 343910 Color: 3
Size: 343415 Color: 2
Size: 312584 Color: 1

Bin 3293: 94 of cap free
Amount of items: 3
Items: 
Size: 350486 Color: 1
Size: 326491 Color: 1
Size: 322930 Color: 2

Bin 3294: 94 of cap free
Amount of items: 3
Items: 
Size: 365682 Color: 0
Size: 356396 Color: 0
Size: 277829 Color: 4

Bin 3295: 95 of cap free
Amount of items: 3
Items: 
Size: 353604 Color: 2
Size: 339520 Color: 4
Size: 306782 Color: 4

Bin 3296: 103 of cap free
Amount of items: 3
Items: 
Size: 352560 Color: 1
Size: 341478 Color: 0
Size: 305860 Color: 3

Bin 3297: 117 of cap free
Amount of items: 3
Items: 
Size: 370371 Color: 4
Size: 357208 Color: 1
Size: 272305 Color: 2

Bin 3298: 118 of cap free
Amount of items: 3
Items: 
Size: 334353 Color: 4
Size: 334340 Color: 2
Size: 331190 Color: 4

Bin 3299: 119 of cap free
Amount of items: 3
Items: 
Size: 351712 Color: 2
Size: 351711 Color: 0
Size: 296459 Color: 4

Bin 3300: 157 of cap free
Amount of items: 3
Items: 
Size: 347387 Color: 3
Size: 346919 Color: 0
Size: 305538 Color: 3

Bin 3301: 181 of cap free
Amount of items: 3
Items: 
Size: 345577 Color: 4
Size: 345435 Color: 0
Size: 308808 Color: 0

Bin 3302: 224 of cap free
Amount of items: 3
Items: 
Size: 343008 Color: 3
Size: 341632 Color: 3
Size: 315137 Color: 0

Bin 3303: 241 of cap free
Amount of items: 3
Items: 
Size: 366794 Color: 3
Size: 357895 Color: 1
Size: 275071 Color: 4

Bin 3304: 244 of cap free
Amount of items: 3
Items: 
Size: 349524 Color: 2
Size: 329102 Color: 3
Size: 321131 Color: 3

Bin 3305: 252 of cap free
Amount of items: 3
Items: 
Size: 370337 Color: 0
Size: 357624 Color: 1
Size: 271788 Color: 2

Bin 3306: 280 of cap free
Amount of items: 3
Items: 
Size: 344948 Color: 4
Size: 332838 Color: 3
Size: 321935 Color: 2

Bin 3307: 304 of cap free
Amount of items: 3
Items: 
Size: 350426 Color: 0
Size: 346334 Color: 0
Size: 302937 Color: 2

Bin 3308: 363 of cap free
Amount of items: 3
Items: 
Size: 366752 Color: 3
Size: 363354 Color: 3
Size: 269532 Color: 0

Bin 3309: 418 of cap free
Amount of items: 3
Items: 
Size: 362710 Color: 3
Size: 356229 Color: 4
Size: 280644 Color: 2

Bin 3310: 496 of cap free
Amount of items: 2
Items: 
Size: 499775 Color: 3
Size: 499730 Color: 0

Bin 3311: 508 of cap free
Amount of items: 3
Items: 
Size: 446825 Color: 1
Size: 287859 Color: 0
Size: 264809 Color: 0

Bin 3312: 674 of cap free
Amount of items: 3
Items: 
Size: 360665 Color: 1
Size: 339916 Color: 4
Size: 298746 Color: 3

Bin 3313: 905 of cap free
Amount of items: 2
Items: 
Size: 499624 Color: 2
Size: 499472 Color: 3

Bin 3314: 1298 of cap free
Amount of items: 2
Items: 
Size: 499471 Color: 2
Size: 499232 Color: 4

Bin 3315: 1379 of cap free
Amount of items: 2
Items: 
Size: 499398 Color: 2
Size: 499224 Color: 0

Bin 3316: 1444 of cap free
Amount of items: 2
Items: 
Size: 499386 Color: 2
Size: 499171 Color: 3

Bin 3317: 1790 of cap free
Amount of items: 3
Items: 
Size: 335896 Color: 0
Size: 335598 Color: 4
Size: 326717 Color: 0

Bin 3318: 1923 of cap free
Amount of items: 2
Items: 
Size: 499148 Color: 4
Size: 498930 Color: 0

Bin 3319: 3063 of cap free
Amount of items: 2
Items: 
Size: 498769 Color: 4
Size: 498169 Color: 0

Bin 3320: 4196 of cap free
Amount of items: 3
Items: 
Size: 364226 Color: 4
Size: 354378 Color: 2
Size: 277201 Color: 2

Bin 3321: 7625 of cap free
Amount of items: 3
Items: 
Size: 354144 Color: 0
Size: 353948 Color: 2
Size: 284284 Color: 0

Bin 3322: 28567 of cap free
Amount of items: 3
Items: 
Size: 353527 Color: 2
Size: 353171 Color: 3
Size: 264736 Color: 4

Bin 3323: 30650 of cap free
Amount of items: 3
Items: 
Size: 353065 Color: 0
Size: 352546 Color: 3
Size: 263740 Color: 0

Bin 3324: 33042 of cap free
Amount of items: 3
Items: 
Size: 352199 Color: 2
Size: 351958 Color: 3
Size: 262802 Color: 4

Bin 3325: 35330 of cap free
Amount of items: 3
Items: 
Size: 351695 Color: 3
Size: 351197 Color: 1
Size: 261779 Color: 2

Bin 3326: 42837 of cap free
Amount of items: 3
Items: 
Size: 347888 Color: 3
Size: 347883 Color: 2
Size: 261393 Color: 4

Bin 3327: 50457 of cap free
Amount of items: 3
Items: 
Size: 344646 Color: 3
Size: 344112 Color: 2
Size: 260786 Color: 2

Bin 3328: 145372 of cap free
Amount of items: 3
Items: 
Size: 334744 Color: 4
Size: 260125 Color: 2
Size: 259760 Color: 3

Bin 3329: 223670 of cap free
Amount of items: 3
Items: 
Size: 259027 Color: 1
Size: 258905 Color: 2
Size: 258399 Color: 4

Bin 3330: 225892 of cap free
Amount of items: 3
Items: 
Size: 258364 Color: 2
Size: 258051 Color: 1
Size: 257694 Color: 4

Bin 3331: 228215 of cap free
Amount of items: 3
Items: 
Size: 257518 Color: 1
Size: 257234 Color: 1
Size: 257034 Color: 4

Bin 3332: 230114 of cap free
Amount of items: 3
Items: 
Size: 256960 Color: 0
Size: 256892 Color: 1
Size: 256035 Color: 1

Bin 3333: 233055 of cap free
Amount of items: 3
Items: 
Size: 255798 Color: 2
Size: 255696 Color: 1
Size: 255452 Color: 3

Bin 3334: 235028 of cap free
Amount of items: 3
Items: 
Size: 255224 Color: 2
Size: 254895 Color: 4
Size: 254854 Color: 4

Bin 3335: 237405 of cap free
Amount of items: 3
Items: 
Size: 254426 Color: 4
Size: 254177 Color: 4
Size: 253993 Color: 3

Bin 3336: 241496 of cap free
Amount of items: 3
Items: 
Size: 253387 Color: 1
Size: 252874 Color: 4
Size: 252244 Color: 0

Bin 3337: 747973 of cap free
Amount of items: 1
Items: 
Size: 252028 Color: 1

Total size: 3334003334
Total free space: 3000003

