Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 293 Color: 1
Size: 278 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 259 Color: 1
Size: 253 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 361 Color: 1
Size: 253 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 251 Color: 0
Size: 254 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 262 Color: 1
Size: 258 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 261 Color: 1
Size: 254 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 299 Color: 1
Size: 280 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 281 Color: 1
Size: 267 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 256 Color: 1
Size: 251 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 286 Color: 1
Size: 255 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 258 Color: 1
Size: 250 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 288 Color: 1
Size: 278 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 327 Color: 1
Size: 317 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 323 Color: 1
Size: 296 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 340 Color: 1
Size: 291 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 287 Color: 1
Size: 257 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 317 Color: 1
Size: 264 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 296 Color: 1
Size: 260 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 296 Color: 1
Size: 256 Color: 0
Size: 448 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 350 Color: 1
Size: 297 Color: 0

Total size: 20000
Total free space: 0

