Capicity Bin: 7896
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 1
Size: 624 Color: 1
Size: 194 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 1
Size: 750 Color: 1
Size: 206 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 1
Size: 1179 Color: 1
Size: 504 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5180 Color: 1
Size: 2124 Color: 1
Size: 592 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4446 Color: 1
Size: 2818 Color: 1
Size: 632 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 3188 Color: 1
Size: 2936 Color: 1
Size: 1488 Color: 0
Size: 284 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6386 Color: 1
Size: 1356 Color: 1
Size: 154 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4918 Color: 1
Size: 2406 Color: 1
Size: 572 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6685 Color: 1
Size: 907 Color: 1
Size: 304 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6538 Color: 1
Size: 862 Color: 1
Size: 496 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5594 Color: 1
Size: 1922 Color: 1
Size: 380 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4449 Color: 1
Size: 3289 Color: 1
Size: 158 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5330 Color: 1
Size: 2142 Color: 1
Size: 424 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6033 Color: 1
Size: 1527 Color: 1
Size: 336 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7068 Color: 1
Size: 692 Color: 1
Size: 136 Color: 0

Bin 16: 0 of cap free
Amount of items: 5
Items: 
Size: 3904 Color: 1
Size: 1634 Color: 1
Size: 1314 Color: 1
Size: 564 Color: 0
Size: 480 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6674 Color: 1
Size: 986 Color: 1
Size: 236 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 1
Size: 1283 Color: 1
Size: 148 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5658 Color: 1
Size: 1866 Color: 1
Size: 372 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4872 Color: 1
Size: 2576 Color: 1
Size: 448 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 1
Size: 1716 Color: 1
Size: 376 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6397 Color: 1
Size: 1251 Color: 1
Size: 248 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6889 Color: 1
Size: 807 Color: 1
Size: 200 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6124 Color: 1
Size: 1468 Color: 1
Size: 304 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 3956 Color: 1
Size: 3284 Color: 1
Size: 656 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 1
Size: 666 Color: 1
Size: 132 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 1
Size: 1186 Color: 1
Size: 314 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 1
Size: 898 Color: 1
Size: 274 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 1
Size: 764 Color: 1
Size: 152 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 1
Size: 961 Color: 1
Size: 174 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6621 Color: 1
Size: 1011 Color: 1
Size: 264 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 1
Size: 1252 Color: 1
Size: 88 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 1
Size: 1180 Color: 1
Size: 232 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4927 Color: 1
Size: 2475 Color: 1
Size: 494 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 1
Size: 2116 Color: 1
Size: 352 Color: 0

Bin 36: 0 of cap free
Amount of items: 5
Items: 
Size: 4518 Color: 1
Size: 1573 Color: 1
Size: 1541 Color: 1
Size: 152 Color: 0
Size: 112 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6202 Color: 1
Size: 1134 Color: 1
Size: 560 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5382 Color: 1
Size: 2098 Color: 1
Size: 416 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6551 Color: 1
Size: 1133 Color: 1
Size: 212 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 1
Size: 1224 Color: 1
Size: 350 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6302 Color: 1
Size: 1330 Color: 1
Size: 264 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5010 Color: 1
Size: 2580 Color: 1
Size: 306 Color: 0

Bin 43: 0 of cap free
Amount of items: 5
Items: 
Size: 4076 Color: 1
Size: 1923 Color: 1
Size: 873 Color: 1
Size: 608 Color: 0
Size: 416 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6868 Color: 1
Size: 860 Color: 1
Size: 168 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 1
Size: 2628 Color: 1
Size: 520 Color: 0

Bin 46: 0 of cap free
Amount of items: 4
Items: 
Size: 5874 Color: 1
Size: 1630 Color: 1
Size: 224 Color: 0
Size: 168 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6649 Color: 1
Size: 799 Color: 1
Size: 448 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 7058 Color: 1
Size: 674 Color: 1
Size: 164 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 1
Size: 682 Color: 1
Size: 280 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 4510 Color: 1
Size: 3044 Color: 1
Size: 342 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 1
Size: 2268 Color: 1
Size: 656 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 5573 Color: 1
Size: 2181 Color: 1
Size: 142 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 3951 Color: 1
Size: 2873 Color: 1
Size: 1072 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6499 Color: 1
Size: 1101 Color: 1
Size: 296 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 1
Size: 891 Color: 1
Size: 196 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 7006 Color: 1
Size: 742 Color: 1
Size: 148 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4636 Color: 1
Size: 2724 Color: 1
Size: 536 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 7030 Color: 1
Size: 722 Color: 1
Size: 144 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5297 Color: 1
Size: 2167 Color: 1
Size: 432 Color: 0

Bin 60: 0 of cap free
Amount of items: 5
Items: 
Size: 3950 Color: 1
Size: 1862 Color: 1
Size: 1686 Color: 1
Size: 208 Color: 0
Size: 190 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 1
Size: 1124 Color: 1
Size: 160 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4425 Color: 1
Size: 2893 Color: 1
Size: 578 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 1
Size: 1324 Color: 1
Size: 296 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 1
Size: 1076 Color: 1
Size: 136 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 1
Size: 852 Color: 1
Size: 184 Color: 0

Bin 66: 0 of cap free
Amount of items: 4
Items: 
Size: 6065 Color: 1
Size: 1375 Color: 1
Size: 324 Color: 0
Size: 132 Color: 0

Bin 67: 0 of cap free
Amount of items: 4
Items: 
Size: 5364 Color: 1
Size: 2060 Color: 1
Size: 256 Color: 0
Size: 216 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 1
Size: 1365 Color: 1
Size: 272 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4869 Color: 1
Size: 2523 Color: 1
Size: 504 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 1
Size: 774 Color: 1
Size: 220 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5356 Color: 1
Size: 2028 Color: 1
Size: 512 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 3954 Color: 1
Size: 3286 Color: 1
Size: 656 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6609 Color: 1
Size: 947 Color: 1
Size: 340 Color: 0

Bin 74: 0 of cap free
Amount of items: 5
Items: 
Size: 5549 Color: 1
Size: 973 Color: 1
Size: 802 Color: 1
Size: 368 Color: 0
Size: 204 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 1
Size: 1403 Color: 1
Size: 484 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 3949 Color: 1
Size: 3291 Color: 1
Size: 656 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 1
Size: 1073 Color: 1
Size: 166 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6828 Color: 1
Size: 892 Color: 1
Size: 176 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6608 Color: 1
Size: 1080 Color: 1
Size: 208 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 4481 Color: 1
Size: 2847 Color: 1
Size: 568 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6351 Color: 1
Size: 1289 Color: 1
Size: 256 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 1
Size: 1122 Color: 1
Size: 108 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6049 Color: 1
Size: 1191 Color: 1
Size: 656 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5942 Color: 1
Size: 1516 Color: 1
Size: 438 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6714 Color: 1
Size: 976 Color: 1
Size: 206 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6474 Color: 1
Size: 1262 Color: 1
Size: 160 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4939 Color: 1
Size: 2465 Color: 1
Size: 492 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 1
Size: 2878 Color: 1
Size: 214 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6044 Color: 1
Size: 1780 Color: 1
Size: 72 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6483 Color: 1
Size: 1165 Color: 1
Size: 248 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6102 Color: 1
Size: 1484 Color: 1
Size: 310 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 1
Size: 3472 Color: 1
Size: 180 Color: 0

Bin 93: 1 of cap free
Amount of items: 3
Items: 
Size: 5966 Color: 1
Size: 1753 Color: 1
Size: 176 Color: 0

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 6729 Color: 1
Size: 830 Color: 1
Size: 336 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 6178 Color: 1
Size: 1553 Color: 1
Size: 164 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 1
Size: 2193 Color: 1
Size: 234 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 6467 Color: 1
Size: 1012 Color: 1
Size: 416 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 6866 Color: 1
Size: 841 Color: 1
Size: 188 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 6849 Color: 1
Size: 868 Color: 1
Size: 178 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 5251 Color: 1
Size: 2444 Color: 1
Size: 200 Color: 0

Bin 101: 1 of cap free
Amount of items: 4
Items: 
Size: 6084 Color: 1
Size: 1041 Color: 1
Size: 570 Color: 0
Size: 200 Color: 0

Bin 102: 1 of cap free
Amount of items: 4
Items: 
Size: 6312 Color: 1
Size: 861 Color: 1
Size: 434 Color: 0
Size: 288 Color: 0

Bin 103: 1 of cap free
Amount of items: 3
Items: 
Size: 6778 Color: 1
Size: 949 Color: 1
Size: 168 Color: 0

Bin 104: 1 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 1
Size: 1033 Color: 1
Size: 40 Color: 0

Bin 105: 1 of cap free
Amount of items: 5
Items: 
Size: 2861 Color: 1
Size: 2822 Color: 1
Size: 1548 Color: 1
Size: 440 Color: 0
Size: 224 Color: 0

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 5281 Color: 1
Size: 2205 Color: 1
Size: 408 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 1
Size: 1434 Color: 1
Size: 104 Color: 0

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 5666 Color: 1
Size: 1748 Color: 1
Size: 480 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 6939 Color: 1
Size: 827 Color: 1
Size: 128 Color: 0

Bin 110: 3 of cap free
Amount of items: 3
Items: 
Size: 6247 Color: 1
Size: 1414 Color: 1
Size: 232 Color: 0

Bin 111: 3 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 1
Size: 1191 Color: 1
Size: 386 Color: 0

Bin 112: 3 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 1
Size: 1022 Color: 1
Size: 296 Color: 0

Bin 113: 3 of cap free
Amount of items: 3
Items: 
Size: 4465 Color: 1
Size: 2988 Color: 1
Size: 440 Color: 0

Bin 114: 3 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 1
Size: 1937 Color: 1
Size: 192 Color: 0

Bin 115: 4 of cap free
Amount of items: 3
Items: 
Size: 5589 Color: 1
Size: 1957 Color: 1
Size: 346 Color: 0

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 5793 Color: 1
Size: 1707 Color: 1
Size: 390 Color: 0

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 5817 Color: 1
Size: 1498 Color: 1
Size: 574 Color: 0

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 6602 Color: 1
Size: 1026 Color: 1
Size: 260 Color: 0

Bin 119: 9 of cap free
Amount of items: 3
Items: 
Size: 6827 Color: 1
Size: 980 Color: 1
Size: 80 Color: 0

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 1
Size: 3290 Color: 1
Size: 280 Color: 0

Bin 121: 12 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 1
Size: 934 Color: 1
Size: 400 Color: 0

Bin 122: 35 of cap free
Amount of items: 3
Items: 
Size: 5265 Color: 1
Size: 2426 Color: 1
Size: 170 Color: 0

Bin 123: 44 of cap free
Amount of items: 3
Items: 
Size: 4986 Color: 1
Size: 2482 Color: 1
Size: 384 Color: 0

Bin 124: 66 of cap free
Amount of items: 3
Items: 
Size: 5849 Color: 1
Size: 1733 Color: 1
Size: 248 Color: 0

Bin 125: 93 of cap free
Amount of items: 3
Items: 
Size: 5844 Color: 1
Size: 1721 Color: 1
Size: 238 Color: 0

Bin 126: 637 of cap free
Amount of items: 3
Items: 
Size: 5833 Color: 1
Size: 1082 Color: 1
Size: 344 Color: 0

Bin 127: 806 of cap free
Amount of items: 1
Items: 
Size: 7090 Color: 1

Bin 128: 926 of cap free
Amount of items: 1
Items: 
Size: 6970 Color: 1

Bin 129: 967 of cap free
Amount of items: 1
Items: 
Size: 6929 Color: 1

Bin 130: 991 of cap free
Amount of items: 1
Items: 
Size: 6905 Color: 1

Bin 131: 1031 of cap free
Amount of items: 1
Items: 
Size: 6865 Color: 1

Bin 132: 1057 of cap free
Amount of items: 1
Items: 
Size: 6839 Color: 1

Bin 133: 1151 of cap free
Amount of items: 1
Items: 
Size: 6745 Color: 1

Total size: 1042272
Total free space: 7896

