Capicity Bin: 15760
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 7884 Color: 12
Size: 1507 Color: 17
Size: 1501 Color: 10
Size: 1484 Color: 8
Size: 1444 Color: 17
Size: 1412 Color: 14
Size: 528 Color: 14

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 7886 Color: 4
Size: 1962 Color: 13
Size: 1631 Color: 17
Size: 1631 Color: 0
Size: 1624 Color: 9
Size: 746 Color: 15
Size: 280 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 18
Size: 6566 Color: 18
Size: 674 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 1
Size: 6532 Color: 18
Size: 436 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8871 Color: 6
Size: 6565 Color: 4
Size: 324 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 3
Size: 5464 Color: 4
Size: 428 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10580 Color: 10
Size: 4916 Color: 12
Size: 264 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10596 Color: 1
Size: 4728 Color: 1
Size: 436 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10910 Color: 6
Size: 4582 Color: 18
Size: 268 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11260 Color: 19
Size: 4028 Color: 14
Size: 472 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11276 Color: 0
Size: 4152 Color: 8
Size: 332 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11672 Color: 1
Size: 2968 Color: 16
Size: 1120 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11898 Color: 1
Size: 2662 Color: 6
Size: 1200 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11982 Color: 18
Size: 3324 Color: 15
Size: 454 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12076 Color: 5
Size: 3260 Color: 2
Size: 424 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12156 Color: 6
Size: 3020 Color: 19
Size: 584 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 2
Size: 3416 Color: 3
Size: 128 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12284 Color: 13
Size: 3180 Color: 8
Size: 296 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12453 Color: 12
Size: 3009 Color: 5
Size: 298 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12570 Color: 17
Size: 2470 Color: 17
Size: 720 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12700 Color: 8
Size: 2460 Color: 2
Size: 600 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12755 Color: 18
Size: 1563 Color: 3
Size: 1442 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12824 Color: 2
Size: 2648 Color: 13
Size: 288 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 15
Size: 2456 Color: 12
Size: 432 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12881 Color: 9
Size: 2401 Color: 19
Size: 478 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13042 Color: 1
Size: 1414 Color: 5
Size: 1304 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 4
Size: 2116 Color: 17
Size: 688 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 19
Size: 2297 Color: 7
Size: 458 Color: 15

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13013 Color: 13
Size: 2291 Color: 0
Size: 456 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 1
Size: 1964 Color: 0
Size: 700 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13090 Color: 13
Size: 2408 Color: 17
Size: 262 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13218 Color: 5
Size: 2266 Color: 13
Size: 276 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13228 Color: 6
Size: 2162 Color: 19
Size: 370 Color: 13

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13242 Color: 7
Size: 2176 Color: 17
Size: 342 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 4
Size: 2122 Color: 12
Size: 274 Color: 11

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13283 Color: 17
Size: 2065 Color: 11
Size: 412 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 0
Size: 2156 Color: 11
Size: 316 Color: 17

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13403 Color: 11
Size: 1549 Color: 8
Size: 808 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13410 Color: 17
Size: 1386 Color: 15
Size: 964 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13442 Color: 4
Size: 1522 Color: 7
Size: 796 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 17
Size: 2072 Color: 3
Size: 172 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13521 Color: 9
Size: 1699 Color: 1
Size: 540 Color: 11

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13576 Color: 10
Size: 1528 Color: 2
Size: 656 Color: 5

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13594 Color: 8
Size: 1448 Color: 14
Size: 718 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13662 Color: 15
Size: 1750 Color: 11
Size: 348 Color: 15

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 10
Size: 1176 Color: 17
Size: 924 Color: 14

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13678 Color: 9
Size: 1322 Color: 2
Size: 760 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13754 Color: 10
Size: 1844 Color: 5
Size: 162 Color: 19

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13778 Color: 0
Size: 1494 Color: 3
Size: 488 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13788 Color: 4
Size: 1688 Color: 15
Size: 284 Color: 5

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13811 Color: 9
Size: 1625 Color: 2
Size: 324 Color: 13

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 2
Size: 1028 Color: 10
Size: 916 Color: 13

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 18
Size: 1482 Color: 4
Size: 424 Color: 19

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13885 Color: 7
Size: 1455 Color: 15
Size: 420 Color: 13

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 14
Size: 1560 Color: 12
Size: 304 Color: 7

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13948 Color: 5
Size: 948 Color: 7
Size: 864 Color: 7

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13953 Color: 10
Size: 1375 Color: 1
Size: 432 Color: 15

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13961 Color: 2
Size: 1351 Color: 4
Size: 448 Color: 16

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14023 Color: 10
Size: 1383 Color: 6
Size: 354 Color: 19

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 16
Size: 1248 Color: 1
Size: 488 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14073 Color: 12
Size: 1633 Color: 13
Size: 54 Color: 6

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14090 Color: 10
Size: 1120 Color: 17
Size: 550 Color: 19

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 4
Size: 984 Color: 1
Size: 636 Color: 15

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14141 Color: 9
Size: 1331 Color: 19
Size: 288 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14146 Color: 5
Size: 1326 Color: 1
Size: 288 Color: 9

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14148 Color: 2
Size: 1140 Color: 8
Size: 472 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 8697 Color: 0
Size: 6562 Color: 14
Size: 500 Color: 2

Bin 68: 1 of cap free
Amount of items: 4
Items: 
Size: 8980 Color: 15
Size: 4775 Color: 2
Size: 1516 Color: 5
Size: 488 Color: 14

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 10202 Color: 5
Size: 4837 Color: 2
Size: 720 Color: 1

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 10870 Color: 11
Size: 4585 Color: 5
Size: 304 Color: 8

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 19
Size: 3993 Color: 4
Size: 570 Color: 8

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 11470 Color: 15
Size: 3981 Color: 13
Size: 308 Color: 16

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 11573 Color: 8
Size: 3896 Color: 10
Size: 290 Color: 11

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 1
Size: 2857 Color: 3
Size: 338 Color: 4

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 12909 Color: 15
Size: 1546 Color: 10
Size: 1304 Color: 0

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13213 Color: 9
Size: 2226 Color: 11
Size: 320 Color: 12

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13371 Color: 3
Size: 2084 Color: 4
Size: 304 Color: 18

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 8
Size: 2377 Color: 16

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 13591 Color: 0
Size: 1312 Color: 18
Size: 856 Color: 17

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 4
Size: 1768 Color: 4
Size: 268 Color: 15

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 13903 Color: 18
Size: 976 Color: 16
Size: 880 Color: 4

Bin 82: 1 of cap free
Amount of items: 2
Items: 
Size: 14111 Color: 17
Size: 1648 Color: 16

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 7882 Color: 3
Size: 6564 Color: 2
Size: 1312 Color: 19

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 8942 Color: 16
Size: 6504 Color: 15
Size: 312 Color: 5

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 9826 Color: 12
Size: 5624 Color: 19
Size: 308 Color: 17

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 10628 Color: 1
Size: 4842 Color: 7
Size: 288 Color: 4

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 19
Size: 3578 Color: 2
Size: 400 Color: 17

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 12151 Color: 8
Size: 3191 Color: 16
Size: 416 Color: 14

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 16
Size: 3150 Color: 1
Size: 424 Color: 9

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 12
Size: 3222 Color: 15

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 12790 Color: 7
Size: 2696 Color: 1
Size: 272 Color: 18

Bin 92: 2 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 17
Size: 1852 Color: 18

Bin 93: 2 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 12
Size: 1690 Color: 2

Bin 94: 2 of cap free
Amount of items: 2
Items: 
Size: 14178 Color: 17
Size: 1580 Color: 14

Bin 95: 3 of cap free
Amount of items: 9
Items: 
Size: 7883 Color: 11
Size: 1356 Color: 3
Size: 1348 Color: 9
Size: 1346 Color: 11
Size: 1312 Color: 13
Size: 1304 Color: 9
Size: 640 Color: 1
Size: 304 Color: 6
Size: 264 Color: 4

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 9624 Color: 8
Size: 5741 Color: 4
Size: 392 Color: 7

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 10661 Color: 1
Size: 5096 Color: 10

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 11281 Color: 16
Size: 3876 Color: 16
Size: 600 Color: 1

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 12060 Color: 4
Size: 3377 Color: 7
Size: 320 Color: 18

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 12333 Color: 5
Size: 2296 Color: 18
Size: 1128 Color: 14

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 12585 Color: 7
Size: 3028 Color: 18
Size: 144 Color: 8

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 9208 Color: 11
Size: 6548 Color: 0

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 10146 Color: 16
Size: 4754 Color: 17
Size: 856 Color: 13

Bin 104: 4 of cap free
Amount of items: 2
Items: 
Size: 11432 Color: 17
Size: 4324 Color: 10

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 3
Size: 3024 Color: 2
Size: 184 Color: 4

Bin 106: 4 of cap free
Amount of items: 3
Items: 
Size: 13539 Color: 9
Size: 1449 Color: 4
Size: 768 Color: 7

Bin 107: 4 of cap free
Amount of items: 2
Items: 
Size: 14170 Color: 15
Size: 1586 Color: 12

Bin 108: 5 of cap free
Amount of items: 9
Items: 
Size: 7881 Color: 2
Size: 1296 Color: 13
Size: 1296 Color: 11
Size: 1296 Color: 3
Size: 1152 Color: 19
Size: 1148 Color: 18
Size: 1034 Color: 5
Size: 388 Color: 1
Size: 264 Color: 18

Bin 109: 5 of cap free
Amount of items: 2
Items: 
Size: 9016 Color: 6
Size: 6739 Color: 13

Bin 110: 5 of cap free
Amount of items: 2
Items: 
Size: 14101 Color: 12
Size: 1654 Color: 8

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 11709 Color: 16
Size: 3733 Color: 7
Size: 312 Color: 12

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 12140 Color: 2
Size: 3614 Color: 17

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 4
Size: 2956 Color: 5

Bin 114: 6 of cap free
Amount of items: 2
Items: 
Size: 13858 Color: 19
Size: 1896 Color: 11

Bin 115: 7 of cap free
Amount of items: 2
Items: 
Size: 13944 Color: 17
Size: 1809 Color: 16

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 14015 Color: 8
Size: 1738 Color: 12

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 10266 Color: 19
Size: 5486 Color: 7

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 13412 Color: 10
Size: 2340 Color: 7

Bin 119: 9 of cap free
Amount of items: 3
Items: 
Size: 10259 Color: 9
Size: 5104 Color: 8
Size: 388 Color: 4

Bin 120: 9 of cap free
Amount of items: 3
Items: 
Size: 11890 Color: 0
Size: 3733 Color: 15
Size: 128 Color: 0

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 12220 Color: 19
Size: 3530 Color: 3

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 12584 Color: 18
Size: 3022 Color: 1
Size: 144 Color: 16

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 13627 Color: 16
Size: 2123 Color: 13

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 14034 Color: 11
Size: 1716 Color: 18

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 11441 Color: 5
Size: 4308 Color: 14

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 13970 Color: 16
Size: 1779 Color: 18

Bin 127: 12 of cap free
Amount of items: 5
Items: 
Size: 7892 Color: 12
Size: 2984 Color: 8
Size: 2458 Color: 12
Size: 2118 Color: 5
Size: 296 Color: 1

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 7940 Color: 6
Size: 7808 Color: 9

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 10104 Color: 14
Size: 5644 Color: 8

Bin 130: 14 of cap free
Amount of items: 4
Items: 
Size: 7896 Color: 12
Size: 4028 Color: 3
Size: 3294 Color: 18
Size: 528 Color: 11

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 13879 Color: 11
Size: 1867 Color: 12

Bin 132: 15 of cap free
Amount of items: 2
Items: 
Size: 13381 Color: 3
Size: 2364 Color: 9

Bin 133: 16 of cap free
Amount of items: 3
Items: 
Size: 7908 Color: 12
Size: 6524 Color: 9
Size: 1312 Color: 2

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 10058 Color: 8
Size: 5686 Color: 17

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 13938 Color: 2
Size: 1806 Color: 16

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 5
Size: 1756 Color: 8

Bin 137: 17 of cap free
Amount of items: 3
Items: 
Size: 10693 Color: 0
Size: 4924 Color: 10
Size: 126 Color: 14

Bin 138: 17 of cap free
Amount of items: 3
Items: 
Size: 13138 Color: 13
Size: 1965 Color: 0
Size: 640 Color: 4

Bin 139: 18 of cap free
Amount of items: 2
Items: 
Size: 12134 Color: 19
Size: 3608 Color: 4

Bin 140: 18 of cap free
Amount of items: 2
Items: 
Size: 12812 Color: 4
Size: 2930 Color: 15

Bin 141: 18 of cap free
Amount of items: 2
Items: 
Size: 14098 Color: 1
Size: 1644 Color: 3

Bin 142: 19 of cap free
Amount of items: 3
Items: 
Size: 9581 Color: 15
Size: 5816 Color: 13
Size: 344 Color: 5

Bin 143: 19 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 1
Size: 2085 Color: 5

Bin 144: 19 of cap free
Amount of items: 2
Items: 
Size: 14165 Color: 13
Size: 1576 Color: 1

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 12
Size: 2004 Color: 11

Bin 146: 20 of cap free
Amount of items: 2
Items: 
Size: 14066 Color: 15
Size: 1674 Color: 12

Bin 147: 21 of cap free
Amount of items: 2
Items: 
Size: 14028 Color: 12
Size: 1711 Color: 10

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 10792 Color: 16
Size: 4946 Color: 6

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 16
Size: 2598 Color: 13

Bin 150: 22 of cap free
Amount of items: 2
Items: 
Size: 13180 Color: 7
Size: 2558 Color: 3

Bin 151: 23 of cap free
Amount of items: 2
Items: 
Size: 11933 Color: 10
Size: 3804 Color: 9

Bin 152: 23 of cap free
Amount of items: 2
Items: 
Size: 13803 Color: 19
Size: 1934 Color: 3

Bin 153: 24 of cap free
Amount of items: 3
Items: 
Size: 7960 Color: 9
Size: 6928 Color: 5
Size: 848 Color: 11

Bin 154: 24 of cap free
Amount of items: 3
Items: 
Size: 9400 Color: 2
Size: 5808 Color: 15
Size: 528 Color: 15

Bin 155: 24 of cap free
Amount of items: 2
Items: 
Size: 13400 Color: 5
Size: 2336 Color: 16

Bin 156: 24 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 15
Size: 2188 Color: 17

Bin 157: 25 of cap free
Amount of items: 2
Items: 
Size: 13503 Color: 14
Size: 2232 Color: 5

Bin 158: 25 of cap free
Amount of items: 2
Items: 
Size: 13982 Color: 15
Size: 1753 Color: 5

Bin 159: 28 of cap free
Amount of items: 3
Items: 
Size: 10983 Color: 7
Size: 2647 Color: 15
Size: 2102 Color: 1

Bin 160: 29 of cap free
Amount of items: 2
Items: 
Size: 13880 Color: 12
Size: 1851 Color: 3

Bin 161: 30 of cap free
Amount of items: 2
Items: 
Size: 11096 Color: 9
Size: 4634 Color: 15

Bin 162: 30 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 2
Size: 2714 Color: 10

Bin 163: 32 of cap free
Amount of items: 2
Items: 
Size: 13542 Color: 11
Size: 2186 Color: 16

Bin 164: 32 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 3
Size: 1920 Color: 0

Bin 165: 34 of cap free
Amount of items: 2
Items: 
Size: 13058 Color: 5
Size: 2668 Color: 4

Bin 166: 34 of cap free
Amount of items: 2
Items: 
Size: 13170 Color: 8
Size: 2556 Color: 2

Bin 167: 35 of cap free
Amount of items: 3
Items: 
Size: 9549 Color: 18
Size: 5816 Color: 15
Size: 360 Color: 6

Bin 168: 35 of cap free
Amount of items: 2
Items: 
Size: 13540 Color: 6
Size: 2185 Color: 15

Bin 169: 36 of cap free
Amount of items: 3
Items: 
Size: 13139 Color: 17
Size: 2505 Color: 15
Size: 80 Color: 13

Bin 170: 37 of cap free
Amount of items: 2
Items: 
Size: 13496 Color: 5
Size: 2227 Color: 9

Bin 171: 40 of cap free
Amount of items: 3
Items: 
Size: 9852 Color: 10
Size: 5652 Color: 14
Size: 216 Color: 6

Bin 172: 41 of cap free
Amount of items: 2
Items: 
Size: 11641 Color: 10
Size: 4078 Color: 0

Bin 173: 42 of cap free
Amount of items: 2
Items: 
Size: 13868 Color: 10
Size: 1850 Color: 0

Bin 174: 43 of cap free
Amount of items: 2
Items: 
Size: 13805 Color: 3
Size: 1912 Color: 5

Bin 175: 46 of cap free
Amount of items: 2
Items: 
Size: 9818 Color: 10
Size: 5896 Color: 17

Bin 176: 46 of cap free
Amount of items: 2
Items: 
Size: 12506 Color: 9
Size: 3208 Color: 18

Bin 177: 46 of cap free
Amount of items: 2
Items: 
Size: 12814 Color: 15
Size: 2900 Color: 8

Bin 178: 50 of cap free
Amount of items: 2
Items: 
Size: 11426 Color: 3
Size: 4284 Color: 17

Bin 179: 52 of cap free
Amount of items: 2
Items: 
Size: 12624 Color: 2
Size: 3084 Color: 0

Bin 180: 56 of cap free
Amount of items: 36
Items: 
Size: 616 Color: 17
Size: 604 Color: 17
Size: 592 Color: 4
Size: 584 Color: 18
Size: 576 Color: 15
Size: 576 Color: 8
Size: 528 Color: 13
Size: 528 Color: 6
Size: 504 Color: 7
Size: 492 Color: 16
Size: 480 Color: 5
Size: 480 Color: 5
Size: 464 Color: 3
Size: 464 Color: 0
Size: 452 Color: 1
Size: 444 Color: 6
Size: 424 Color: 7
Size: 416 Color: 4
Size: 396 Color: 19
Size: 392 Color: 9
Size: 384 Color: 19
Size: 384 Color: 8
Size: 384 Color: 2
Size: 372 Color: 18
Size: 372 Color: 10
Size: 368 Color: 19
Size: 368 Color: 14
Size: 368 Color: 11
Size: 368 Color: 4
Size: 360 Color: 10
Size: 344 Color: 5
Size: 340 Color: 1
Size: 336 Color: 10
Size: 336 Color: 6
Size: 328 Color: 16
Size: 280 Color: 3

Bin 181: 56 of cap free
Amount of items: 2
Items: 
Size: 9664 Color: 18
Size: 6040 Color: 14

Bin 182: 57 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 7
Size: 5887 Color: 2
Size: 160 Color: 13

Bin 183: 69 of cap free
Amount of items: 2
Items: 
Size: 13708 Color: 7
Size: 1983 Color: 2

Bin 184: 70 of cap free
Amount of items: 2
Items: 
Size: 12246 Color: 8
Size: 3444 Color: 15

Bin 185: 77 of cap free
Amount of items: 2
Items: 
Size: 13707 Color: 8
Size: 1976 Color: 1

Bin 186: 79 of cap free
Amount of items: 2
Items: 
Size: 12924 Color: 9
Size: 2757 Color: 7

Bin 187: 81 of cap free
Amount of items: 2
Items: 
Size: 12078 Color: 18
Size: 3601 Color: 16

Bin 188: 90 of cap free
Amount of items: 2
Items: 
Size: 11628 Color: 0
Size: 4042 Color: 7

Bin 189: 108 of cap free
Amount of items: 2
Items: 
Size: 11244 Color: 14
Size: 4408 Color: 2

Bin 190: 108 of cap free
Amount of items: 2
Items: 
Size: 11912 Color: 5
Size: 3740 Color: 16

Bin 191: 111 of cap free
Amount of items: 2
Items: 
Size: 10472 Color: 5
Size: 5177 Color: 19

Bin 192: 117 of cap free
Amount of items: 2
Items: 
Size: 11420 Color: 4
Size: 4223 Color: 9

Bin 193: 124 of cap free
Amount of items: 2
Items: 
Size: 9954 Color: 11
Size: 5682 Color: 7

Bin 194: 136 of cap free
Amount of items: 3
Items: 
Size: 7924 Color: 4
Size: 6564 Color: 19
Size: 1136 Color: 1

Bin 195: 137 of cap free
Amount of items: 2
Items: 
Size: 10285 Color: 6
Size: 5338 Color: 7

Bin 196: 194 of cap free
Amount of items: 19
Items: 
Size: 1136 Color: 2
Size: 1088 Color: 15
Size: 1008 Color: 15
Size: 992 Color: 5
Size: 988 Color: 8
Size: 976 Color: 7
Size: 928 Color: 13
Size: 912 Color: 0
Size: 844 Color: 4
Size: 816 Color: 0
Size: 812 Color: 13
Size: 768 Color: 6
Size: 744 Color: 16
Size: 712 Color: 14
Size: 672 Color: 16
Size: 628 Color: 7
Size: 604 Color: 3
Size: 474 Color: 19
Size: 464 Color: 1

Bin 197: 219 of cap free
Amount of items: 3
Items: 
Size: 8996 Color: 8
Size: 5151 Color: 14
Size: 1394 Color: 4

Bin 198: 254 of cap free
Amount of items: 2
Items: 
Size: 8938 Color: 11
Size: 6568 Color: 2

Bin 199: 11978 of cap free
Amount of items: 13
Items: 
Size: 336 Color: 1
Size: 328 Color: 13
Size: 326 Color: 19
Size: 300 Color: 18
Size: 300 Color: 12
Size: 296 Color: 9
Size: 288 Color: 5
Size: 276 Color: 7
Size: 276 Color: 4
Size: 272 Color: 17
Size: 264 Color: 3
Size: 260 Color: 16
Size: 260 Color: 11

Total size: 3120480
Total free space: 15760

