Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 0
Size: 300 Color: 0
Size: 291 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 296 Color: 0
Size: 262 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 274 Color: 0
Size: 255 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 299 Color: 1
Size: 288 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 329 Color: 0
Size: 279 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 288 Color: 1
Size: 258 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 296 Color: 0
Size: 273 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 323 Color: 1
Size: 251 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 0
Size: 271 Color: 1
Size: 266 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 274 Color: 1
Size: 254 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 0
Size: 261 Color: 0
Size: 252 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 263 Color: 1
Size: 252 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 0
Size: 281 Color: 1
Size: 383 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 256 Color: 0
Size: 253 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 291 Color: 0
Size: 261 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 0
Size: 267 Color: 1
Size: 252 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 299 Color: 0
Size: 250 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 292 Color: 1
Size: 257 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 347 Color: 0
Size: 264 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 328 Color: 0
Size: 312 Color: 0

Total size: 20000
Total free space: 0

