Capicity Bin: 8000
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 4004 Color: 1
Size: 724 Color: 1
Size: 702 Color: 1
Size: 632 Color: 0
Size: 576 Color: 0
Size: 416 Color: 1
Size: 394 Color: 0
Size: 390 Color: 0
Size: 162 Color: 0

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 4002 Color: 0
Size: 1154 Color: 0
Size: 820 Color: 1
Size: 774 Color: 1
Size: 674 Color: 0
Size: 296 Color: 0
Size: 280 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4956 Color: 0
Size: 2900 Color: 0
Size: 144 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5892 Color: 1
Size: 1762 Color: 1
Size: 346 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 0
Size: 1102 Color: 1
Size: 592 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6403 Color: 1
Size: 1149 Color: 1
Size: 448 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 1
Size: 1101 Color: 1
Size: 332 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 0
Size: 1146 Color: 0
Size: 304 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6568 Color: 0
Size: 1240 Color: 1
Size: 192 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 1
Size: 1294 Color: 0
Size: 112 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 0
Size: 1156 Color: 0
Size: 224 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6674 Color: 0
Size: 1106 Color: 1
Size: 220 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 1
Size: 738 Color: 0
Size: 404 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6814 Color: 0
Size: 990 Color: 0
Size: 196 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 1
Size: 828 Color: 1
Size: 152 Color: 0

Bin 16: 1 of cap free
Amount of items: 10
Items: 
Size: 4001 Color: 1
Size: 640 Color: 1
Size: 632 Color: 1
Size: 548 Color: 1
Size: 536 Color: 1
Size: 376 Color: 0
Size: 368 Color: 0
Size: 360 Color: 0
Size: 352 Color: 0
Size: 186 Color: 0

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 4430 Color: 1
Size: 3331 Color: 1
Size: 238 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 4497 Color: 1
Size: 3332 Color: 1
Size: 170 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 0
Size: 3212 Color: 1
Size: 246 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 4945 Color: 0
Size: 2716 Color: 0
Size: 338 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 1
Size: 1981 Color: 1
Size: 372 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 5844 Color: 1
Size: 1863 Color: 0
Size: 292 Color: 0

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 6042 Color: 0
Size: 1957 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 6066 Color: 1
Size: 1665 Color: 0
Size: 268 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 0
Size: 1414 Color: 1
Size: 200 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 0
Size: 1155 Color: 0
Size: 452 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 6579 Color: 1
Size: 1284 Color: 0
Size: 136 Color: 1

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 6652 Color: 0
Size: 1347 Color: 1

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 6821 Color: 1
Size: 1178 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6839 Color: 1
Size: 924 Color: 1
Size: 236 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6915 Color: 0
Size: 818 Color: 1
Size: 266 Color: 0

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 6971 Color: 0
Size: 1028 Color: 1

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 0
Size: 969 Color: 1

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 1
Size: 859 Color: 0

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 4524 Color: 1
Size: 3274 Color: 0
Size: 200 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 5270 Color: 0
Size: 2540 Color: 0
Size: 188 Color: 1

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 5629 Color: 0
Size: 2193 Color: 1
Size: 176 Color: 0

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 5834 Color: 1
Size: 2164 Color: 0

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 6395 Color: 1
Size: 1603 Color: 0

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 1
Size: 1436 Color: 0

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 6746 Color: 1
Size: 1252 Color: 0

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 0
Size: 716 Color: 0
Size: 480 Color: 1

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 7006 Color: 1
Size: 992 Color: 0

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 1
Size: 810 Color: 0
Size: 176 Color: 0

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 6978 Color: 0
Size: 1020 Color: 1

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 0
Size: 3709 Color: 1
Size: 140 Color: 0

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 4268 Color: 1
Size: 3161 Color: 1
Size: 568 Color: 0

Bin 48: 3 of cap free
Amount of items: 2
Items: 
Size: 6193 Color: 1
Size: 1804 Color: 0

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 6258 Color: 0
Size: 1231 Color: 1
Size: 508 Color: 1

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 6269 Color: 0
Size: 1512 Color: 0
Size: 216 Color: 1

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 6523 Color: 0
Size: 1474 Color: 1

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 1
Size: 1195 Color: 0
Size: 230 Color: 0

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 6900 Color: 1
Size: 1053 Color: 0
Size: 44 Color: 1

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 6153 Color: 1
Size: 1699 Color: 0
Size: 144 Color: 0

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 1
Size: 1298 Color: 0
Size: 88 Color: 1

Bin 56: 4 of cap free
Amount of items: 2
Items: 
Size: 7194 Color: 0
Size: 802 Color: 1

Bin 57: 5 of cap free
Amount of items: 3
Items: 
Size: 4902 Color: 1
Size: 2741 Color: 0
Size: 352 Color: 1

Bin 58: 5 of cap free
Amount of items: 3
Items: 
Size: 5399 Color: 0
Size: 2084 Color: 1
Size: 512 Color: 0

Bin 59: 6 of cap free
Amount of items: 21
Items: 
Size: 536 Color: 1
Size: 516 Color: 1
Size: 512 Color: 1
Size: 504 Color: 1
Size: 500 Color: 1
Size: 496 Color: 1
Size: 484 Color: 1
Size: 448 Color: 1
Size: 440 Color: 1
Size: 432 Color: 1
Size: 344 Color: 0
Size: 320 Color: 0
Size: 306 Color: 0
Size: 304 Color: 0
Size: 296 Color: 0
Size: 290 Color: 0
Size: 280 Color: 0
Size: 266 Color: 0
Size: 264 Color: 0
Size: 260 Color: 0
Size: 196 Color: 0

Bin 60: 6 of cap free
Amount of items: 2
Items: 
Size: 4660 Color: 1
Size: 3334 Color: 0

Bin 61: 6 of cap free
Amount of items: 3
Items: 
Size: 5014 Color: 1
Size: 2686 Color: 0
Size: 294 Color: 1

Bin 62: 6 of cap free
Amount of items: 2
Items: 
Size: 6380 Color: 1
Size: 1614 Color: 0

Bin 63: 6 of cap free
Amount of items: 2
Items: 
Size: 7100 Color: 0
Size: 894 Color: 1

Bin 64: 7 of cap free
Amount of items: 7
Items: 
Size: 4005 Color: 1
Size: 756 Color: 1
Size: 664 Color: 1
Size: 664 Color: 0
Size: 664 Color: 0
Size: 664 Color: 0
Size: 576 Color: 0

Bin 65: 7 of cap free
Amount of items: 2
Items: 
Size: 6831 Color: 1
Size: 1162 Color: 0

Bin 66: 8 of cap free
Amount of items: 2
Items: 
Size: 5482 Color: 1
Size: 2510 Color: 0

Bin 67: 8 of cap free
Amount of items: 4
Items: 
Size: 5620 Color: 1
Size: 2140 Color: 0
Size: 120 Color: 1
Size: 112 Color: 0

Bin 68: 8 of cap free
Amount of items: 2
Items: 
Size: 5954 Color: 0
Size: 2038 Color: 1

Bin 69: 8 of cap free
Amount of items: 2
Items: 
Size: 6468 Color: 1
Size: 1524 Color: 0

Bin 70: 8 of cap free
Amount of items: 2
Items: 
Size: 6682 Color: 0
Size: 1310 Color: 1

Bin 71: 8 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 1
Size: 884 Color: 0
Size: 86 Color: 0

Bin 72: 9 of cap free
Amount of items: 2
Items: 
Size: 5108 Color: 0
Size: 2883 Color: 1

Bin 73: 9 of cap free
Amount of items: 3
Items: 
Size: 6225 Color: 0
Size: 1634 Color: 0
Size: 132 Color: 1

Bin 74: 10 of cap free
Amount of items: 2
Items: 
Size: 5886 Color: 1
Size: 2104 Color: 0

Bin 75: 10 of cap free
Amount of items: 2
Items: 
Size: 6284 Color: 1
Size: 1706 Color: 0

Bin 76: 10 of cap free
Amount of items: 2
Items: 
Size: 6547 Color: 1
Size: 1443 Color: 0

Bin 77: 11 of cap free
Amount of items: 3
Items: 
Size: 5581 Color: 0
Size: 1454 Color: 0
Size: 954 Color: 1

Bin 78: 12 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 1
Size: 1362 Color: 0

Bin 79: 12 of cap free
Amount of items: 2
Items: 
Size: 6737 Color: 1
Size: 1251 Color: 0

Bin 80: 12 of cap free
Amount of items: 2
Items: 
Size: 6780 Color: 0
Size: 1208 Color: 1

Bin 81: 12 of cap free
Amount of items: 2
Items: 
Size: 7158 Color: 1
Size: 830 Color: 0

Bin 82: 13 of cap free
Amount of items: 2
Items: 
Size: 5558 Color: 1
Size: 2429 Color: 0

Bin 83: 13 of cap free
Amount of items: 2
Items: 
Size: 6446 Color: 0
Size: 1541 Color: 1

Bin 84: 13 of cap free
Amount of items: 2
Items: 
Size: 6941 Color: 0
Size: 1046 Color: 1

Bin 85: 13 of cap free
Amount of items: 2
Items: 
Size: 7148 Color: 0
Size: 839 Color: 1

Bin 86: 14 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 1
Size: 2976 Color: 0
Size: 232 Color: 1

Bin 87: 14 of cap free
Amount of items: 3
Items: 
Size: 5625 Color: 1
Size: 2169 Color: 0
Size: 192 Color: 1

Bin 88: 14 of cap free
Amount of items: 2
Items: 
Size: 6995 Color: 1
Size: 991 Color: 0

Bin 89: 16 of cap free
Amount of items: 2
Items: 
Size: 6679 Color: 0
Size: 1305 Color: 1

Bin 90: 16 of cap free
Amount of items: 2
Items: 
Size: 6923 Color: 1
Size: 1061 Color: 0

Bin 91: 17 of cap free
Amount of items: 2
Items: 
Size: 5436 Color: 1
Size: 2547 Color: 0

Bin 92: 18 of cap free
Amount of items: 3
Items: 
Size: 5908 Color: 1
Size: 1962 Color: 0
Size: 112 Color: 0

Bin 93: 18 of cap free
Amount of items: 2
Items: 
Size: 6234 Color: 1
Size: 1748 Color: 0

Bin 94: 19 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 1
Size: 1481 Color: 0

Bin 95: 20 of cap free
Amount of items: 2
Items: 
Size: 6003 Color: 1
Size: 1977 Color: 0

Bin 96: 21 of cap free
Amount of items: 4
Items: 
Size: 4884 Color: 0
Size: 1124 Color: 1
Size: 1088 Color: 1
Size: 883 Color: 0

Bin 97: 21 of cap free
Amount of items: 2
Items: 
Size: 6623 Color: 1
Size: 1356 Color: 0

Bin 98: 21 of cap free
Amount of items: 2
Items: 
Size: 6948 Color: 1
Size: 1031 Color: 0

Bin 99: 21 of cap free
Amount of items: 2
Items: 
Size: 7074 Color: 1
Size: 905 Color: 0

Bin 100: 22 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 1
Size: 2102 Color: 1
Size: 472 Color: 0

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 6172 Color: 0
Size: 1806 Color: 1

Bin 102: 22 of cap free
Amount of items: 2
Items: 
Size: 6214 Color: 0
Size: 1764 Color: 1

Bin 103: 22 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 1
Size: 1007 Color: 0
Size: 88 Color: 1

Bin 104: 24 of cap free
Amount of items: 2
Items: 
Size: 4748 Color: 0
Size: 3228 Color: 1

Bin 105: 24 of cap free
Amount of items: 2
Items: 
Size: 6765 Color: 1
Size: 1211 Color: 0

Bin 106: 25 of cap free
Amount of items: 2
Items: 
Size: 5963 Color: 0
Size: 2012 Color: 1

Bin 107: 26 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 0
Size: 1202 Color: 1

Bin 108: 27 of cap free
Amount of items: 2
Items: 
Size: 7042 Color: 1
Size: 931 Color: 0

Bin 109: 28 of cap free
Amount of items: 2
Items: 
Size: 4990 Color: 1
Size: 2982 Color: 0

Bin 110: 28 of cap free
Amount of items: 2
Items: 
Size: 7118 Color: 1
Size: 854 Color: 0

Bin 111: 29 of cap free
Amount of items: 2
Items: 
Size: 4711 Color: 0
Size: 3260 Color: 1

Bin 112: 30 of cap free
Amount of items: 2
Items: 
Size: 5480 Color: 1
Size: 2490 Color: 0

Bin 113: 30 of cap free
Amount of items: 2
Items: 
Size: 6430 Color: 1
Size: 1540 Color: 0

Bin 114: 33 of cap free
Amount of items: 2
Items: 
Size: 5765 Color: 1
Size: 2202 Color: 0

Bin 115: 35 of cap free
Amount of items: 2
Items: 
Size: 6634 Color: 0
Size: 1331 Color: 1

Bin 116: 39 of cap free
Amount of items: 3
Items: 
Size: 6930 Color: 0
Size: 983 Color: 1
Size: 48 Color: 1

Bin 117: 42 of cap free
Amount of items: 2
Items: 
Size: 4006 Color: 1
Size: 3952 Color: 0

Bin 118: 46 of cap free
Amount of items: 2
Items: 
Size: 6615 Color: 1
Size: 1339 Color: 0

Bin 119: 47 of cap free
Amount of items: 3
Items: 
Size: 4207 Color: 1
Size: 3330 Color: 1
Size: 416 Color: 0

Bin 120: 55 of cap free
Amount of items: 2
Items: 
Size: 5087 Color: 0
Size: 2858 Color: 1

Bin 121: 60 of cap free
Amount of items: 2
Items: 
Size: 5358 Color: 1
Size: 2582 Color: 0

Bin 122: 62 of cap free
Amount of items: 2
Items: 
Size: 4426 Color: 0
Size: 3512 Color: 1

Bin 123: 65 of cap free
Amount of items: 2
Items: 
Size: 5657 Color: 1
Size: 2278 Color: 0

Bin 124: 68 of cap free
Amount of items: 2
Items: 
Size: 5915 Color: 1
Size: 2017 Color: 0

Bin 125: 75 of cap free
Amount of items: 2
Items: 
Size: 6435 Color: 0
Size: 1490 Color: 1

Bin 126: 77 of cap free
Amount of items: 2
Items: 
Size: 6727 Color: 1
Size: 1196 Color: 0

Bin 127: 80 of cap free
Amount of items: 2
Items: 
Size: 5508 Color: 0
Size: 2412 Color: 1

Bin 128: 91 of cap free
Amount of items: 2
Items: 
Size: 5305 Color: 0
Size: 2604 Color: 1

Bin 129: 100 of cap free
Amount of items: 2
Items: 
Size: 5653 Color: 1
Size: 2247 Color: 0

Bin 130: 105 of cap free
Amount of items: 2
Items: 
Size: 6156 Color: 1
Size: 1739 Color: 0

Bin 131: 115 of cap free
Amount of items: 2
Items: 
Size: 4574 Color: 1
Size: 3311 Color: 0

Bin 132: 132 of cap free
Amount of items: 28
Items: 
Size: 432 Color: 1
Size: 424 Color: 1
Size: 408 Color: 1
Size: 402 Color: 1
Size: 394 Color: 1
Size: 392 Color: 1
Size: 344 Color: 1
Size: 340 Color: 1
Size: 324 Color: 1
Size: 288 Color: 1
Size: 288 Color: 1
Size: 260 Color: 0
Size: 256 Color: 1
Size: 248 Color: 1
Size: 248 Color: 0
Size: 242 Color: 0
Size: 228 Color: 1
Size: 228 Color: 0
Size: 228 Color: 0
Size: 224 Color: 0
Size: 224 Color: 0
Size: 220 Color: 0
Size: 212 Color: 0
Size: 210 Color: 0
Size: 208 Color: 0
Size: 204 Color: 0
Size: 204 Color: 0
Size: 188 Color: 0

Bin 133: 5726 of cap free
Amount of items: 14
Items: 
Size: 180 Color: 1
Size: 176 Color: 1
Size: 170 Color: 1
Size: 168 Color: 1
Size: 168 Color: 1
Size: 166 Color: 0
Size: 164 Color: 1
Size: 160 Color: 1
Size: 160 Color: 0
Size: 160 Color: 0
Size: 160 Color: 0
Size: 156 Color: 0
Size: 150 Color: 0
Size: 136 Color: 0

Total size: 1056000
Total free space: 8000

