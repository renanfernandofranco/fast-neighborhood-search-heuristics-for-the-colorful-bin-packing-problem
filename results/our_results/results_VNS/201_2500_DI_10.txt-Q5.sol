Capicity Bin: 2012
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1010 Color: 0
Size: 838 Color: 3
Size: 84 Color: 4
Size: 80 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 0
Size: 747 Color: 1
Size: 66 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1247 Color: 1
Size: 639 Color: 3
Size: 126 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1349 Color: 1
Size: 553 Color: 3
Size: 110 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 4
Size: 362 Color: 1
Size: 148 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 4
Size: 381 Color: 1
Size: 58 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 1
Size: 293 Color: 3
Size: 116 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 1
Size: 282 Color: 0
Size: 112 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 0
Size: 331 Color: 4
Size: 38 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 4
Size: 269 Color: 1
Size: 90 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 4
Size: 261 Color: 1
Size: 82 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 1
Size: 182 Color: 0
Size: 92 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 3
Size: 237 Color: 1
Size: 46 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 1
Size: 202 Color: 3
Size: 56 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 0
Size: 213 Color: 1
Size: 50 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 2
Size: 191 Color: 1
Size: 64 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 1
Size: 193 Color: 4
Size: 42 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 2
Size: 164 Color: 4
Size: 84 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 1
Size: 189 Color: 4
Size: 42 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 0
Size: 210 Color: 1
Size: 32 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 1
Size: 174 Color: 4
Size: 32 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 3
Size: 186 Color: 1
Size: 36 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 4
Size: 150 Color: 1
Size: 64 Color: 3

Bin 24: 1 of cap free
Amount of items: 4
Items: 
Size: 1011 Color: 2
Size: 751 Color: 4
Size: 181 Color: 1
Size: 68 Color: 2

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 1
Size: 671 Color: 4
Size: 74 Color: 2

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 3
Size: 478 Color: 0
Size: 76 Color: 4

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1499 Color: 1
Size: 464 Color: 3
Size: 48 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 2
Size: 426 Color: 1
Size: 30 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 3
Size: 302 Color: 4
Size: 92 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1691 Color: 0
Size: 312 Color: 4
Size: 8 Color: 3

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 4
Size: 262 Color: 1
Size: 48 Color: 3

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 3
Size: 221 Color: 1
Size: 72 Color: 4

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 2
Size: 208 Color: 4
Size: 8 Color: 3

Bin 34: 2 of cap free
Amount of items: 5
Items: 
Size: 1007 Color: 4
Size: 506 Color: 1
Size: 221 Color: 3
Size: 220 Color: 3
Size: 56 Color: 3

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1151 Color: 2
Size: 791 Color: 1
Size: 68 Color: 4

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1194 Color: 4
Size: 682 Color: 3
Size: 134 Color: 1

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1326 Color: 2
Size: 684 Color: 4

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 1392 Color: 4
Size: 574 Color: 3
Size: 44 Color: 1

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 1431 Color: 1
Size: 527 Color: 3
Size: 52 Color: 4

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 1
Size: 429 Color: 0
Size: 60 Color: 3

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 1723 Color: 0
Size: 287 Color: 2

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 1807 Color: 1
Size: 203 Color: 2

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 1311 Color: 4
Size: 622 Color: 2
Size: 76 Color: 1

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 1
Size: 463 Color: 3
Size: 104 Color: 4

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 1747 Color: 2
Size: 230 Color: 0
Size: 32 Color: 0

Bin 46: 3 of cap free
Amount of items: 2
Items: 
Size: 1785 Color: 4
Size: 224 Color: 0

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 1381 Color: 1
Size: 420 Color: 0
Size: 207 Color: 3

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 1
Size: 330 Color: 4

Bin 49: 4 of cap free
Amount of items: 2
Items: 
Size: 1762 Color: 4
Size: 246 Color: 3

Bin 50: 5 of cap free
Amount of items: 2
Items: 
Size: 1522 Color: 3
Size: 485 Color: 2

Bin 51: 5 of cap free
Amount of items: 2
Items: 
Size: 1698 Color: 1
Size: 309 Color: 4

Bin 52: 6 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 4
Size: 329 Color: 2
Size: 16 Color: 1

Bin 53: 6 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 3
Size: 241 Color: 4

Bin 54: 7 of cap free
Amount of items: 3
Items: 
Size: 1111 Color: 1
Size: 752 Color: 0
Size: 142 Color: 0

Bin 55: 13 of cap free
Amount of items: 3
Items: 
Size: 1260 Color: 4
Size: 643 Color: 0
Size: 96 Color: 1

Bin 56: 14 of cap free
Amount of items: 2
Items: 
Size: 1159 Color: 3
Size: 839 Color: 4

Bin 57: 17 of cap free
Amount of items: 2
Items: 
Size: 1654 Color: 0
Size: 341 Color: 4

Bin 58: 18 of cap free
Amount of items: 2
Items: 
Size: 1207 Color: 3
Size: 787 Color: 0

Bin 59: 19 of cap free
Amount of items: 2
Items: 
Size: 1582 Color: 0
Size: 411 Color: 4

Bin 60: 21 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 3
Size: 585 Color: 4

Bin 61: 22 of cap free
Amount of items: 2
Items: 
Size: 1279 Color: 2
Size: 711 Color: 0

Bin 62: 24 of cap free
Amount of items: 3
Items: 
Size: 1110 Color: 3
Size: 754 Color: 4
Size: 124 Color: 1

Bin 63: 25 of cap free
Amount of items: 15
Items: 
Size: 367 Color: 1
Size: 366 Color: 1
Size: 218 Color: 3
Size: 166 Color: 3
Size: 158 Color: 0
Size: 136 Color: 4
Size: 100 Color: 2
Size: 80 Color: 3
Size: 72 Color: 4
Size: 72 Color: 1
Size: 56 Color: 0
Size: 52 Color: 4
Size: 52 Color: 3
Size: 52 Color: 0
Size: 40 Color: 1

Bin 64: 28 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 0
Size: 410 Color: 3

Bin 65: 30 of cap free
Amount of items: 2
Items: 
Size: 1063 Color: 2
Size: 919 Color: 3

Bin 66: 1700 of cap free
Amount of items: 8
Items: 
Size: 44 Color: 0
Size: 40 Color: 4
Size: 40 Color: 4
Size: 40 Color: 1
Size: 40 Color: 1
Size: 36 Color: 2
Size: 36 Color: 2
Size: 36 Color: 0

Total size: 130780
Total free space: 2012

