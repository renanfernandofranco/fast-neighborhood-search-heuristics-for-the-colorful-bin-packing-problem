Capicity Bin: 8000
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 4748 Color: 3
Size: 2164 Color: 2
Size: 924 Color: 4
Size: 164 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5108 Color: 3
Size: 2604 Color: 3
Size: 288 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5653 Color: 3
Size: 1863 Color: 0
Size: 484 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5892 Color: 1
Size: 1764 Color: 3
Size: 344 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6066 Color: 2
Size: 1748 Color: 4
Size: 186 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6214 Color: 3
Size: 1414 Color: 3
Size: 372 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 1
Size: 1149 Color: 2
Size: 416 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6430 Color: 0
Size: 1202 Color: 1
Size: 368 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 2
Size: 1154 Color: 1
Size: 346 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 3
Size: 818 Color: 2
Size: 632 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 2
Size: 774 Color: 1
Size: 664 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 0
Size: 1088 Color: 3
Size: 340 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 3
Size: 1146 Color: 0
Size: 260 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6615 Color: 2
Size: 991 Color: 4
Size: 394 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 3
Size: 1124 Color: 1
Size: 266 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 4
Size: 1211 Color: 3
Size: 166 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6634 Color: 0
Size: 1102 Color: 3
Size: 264 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 2
Size: 1156 Color: 3
Size: 192 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6679 Color: 1
Size: 1101 Color: 3
Size: 220 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6727 Color: 4
Size: 1061 Color: 3
Size: 212 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6737 Color: 0
Size: 1031 Color: 3
Size: 232 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 3
Size: 716 Color: 1
Size: 504 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 2
Size: 820 Color: 0
Size: 408 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6821 Color: 3
Size: 883 Color: 4
Size: 296 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6814 Color: 1
Size: 674 Color: 2
Size: 512 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6930 Color: 3
Size: 894 Color: 1
Size: 176 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7006 Color: 2
Size: 854 Color: 3
Size: 140 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 0
Size: 500 Color: 4
Size: 480 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 3
Size: 828 Color: 0
Size: 150 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7100 Color: 3
Size: 724 Color: 0
Size: 176 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7074 Color: 1
Size: 536 Color: 4
Size: 390 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7118 Color: 1
Size: 640 Color: 2
Size: 242 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 2
Size: 548 Color: 3
Size: 304 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 0
Size: 576 Color: 3
Size: 230 Color: 4

Bin 35: 1 of cap free
Amount of items: 5
Items: 
Size: 4004 Color: 1
Size: 1524 Color: 3
Size: 1481 Color: 3
Size: 830 Color: 4
Size: 160 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 4497 Color: 1
Size: 3274 Color: 1
Size: 228 Color: 4

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5087 Color: 3
Size: 2716 Color: 1
Size: 196 Color: 3

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5558 Color: 2
Size: 2017 Color: 2
Size: 424 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5625 Color: 2
Size: 2084 Color: 1
Size: 290 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 0
Size: 2193 Color: 1
Size: 160 Color: 4

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5963 Color: 1
Size: 1634 Color: 4
Size: 402 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5954 Color: 4
Size: 1739 Color: 1
Size: 306 Color: 2

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6042 Color: 3
Size: 1957 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 3
Size: 1699 Color: 1
Size: 144 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 3
Size: 1665 Color: 0
Size: 162 Color: 1

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6193 Color: 2
Size: 1806 Color: 3

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6385 Color: 0
Size: 1614 Color: 3

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6395 Color: 3
Size: 1284 Color: 0
Size: 320 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 2
Size: 1028 Color: 4
Size: 88 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6923 Color: 4
Size: 568 Color: 2
Size: 508 Color: 3

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6971 Color: 2
Size: 884 Color: 3
Size: 144 Color: 0

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 0
Size: 969 Color: 1

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 4
Size: 859 Color: 0

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 0
Size: 2012 Color: 4
Size: 1208 Color: 1

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 5508 Color: 0
Size: 2490 Color: 4

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 6003 Color: 4
Size: 1331 Color: 2
Size: 664 Color: 1

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 0
Size: 1490 Color: 3
Size: 224 Color: 1

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 3
Size: 1240 Color: 1
Size: 452 Color: 2

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 0
Size: 1436 Color: 1
Size: 170 Color: 2

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 6468 Color: 4
Size: 1310 Color: 3
Size: 220 Color: 4

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 6746 Color: 4
Size: 1252 Color: 1

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 6802 Color: 1
Size: 1196 Color: 0

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 0
Size: 1020 Color: 3
Size: 120 Color: 2

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 4574 Color: 3
Size: 3311 Color: 0
Size: 112 Color: 1

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 5305 Color: 3
Size: 2540 Color: 1
Size: 152 Color: 3

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 5358 Color: 0
Size: 2429 Color: 1
Size: 210 Color: 3

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 5908 Color: 0
Size: 1977 Color: 1
Size: 112 Color: 2

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 3
Size: 1251 Color: 2
Size: 512 Color: 1

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 6523 Color: 4
Size: 1474 Color: 2

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 0
Size: 1294 Color: 2
Size: 136 Color: 3

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 6900 Color: 0
Size: 905 Color: 3
Size: 192 Color: 2

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 7158 Color: 4
Size: 839 Color: 1

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 4426 Color: 4
Size: 3332 Color: 3
Size: 238 Color: 1

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 5270 Color: 1
Size: 2510 Color: 0
Size: 216 Color: 2

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 5436 Color: 1
Size: 1454 Color: 4
Size: 1106 Color: 2

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 6765 Color: 0
Size: 1231 Color: 2

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 7042 Color: 1
Size: 954 Color: 4

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 5482 Color: 0
Size: 2247 Color: 1
Size: 266 Color: 3

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 4660 Color: 2
Size: 3334 Color: 4

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 5765 Color: 3
Size: 1981 Color: 1
Size: 248 Color: 0

Bin 81: 6 of cap free
Amount of items: 3
Items: 
Size: 6258 Color: 3
Size: 1512 Color: 4
Size: 224 Color: 1

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6839 Color: 3
Size: 1155 Color: 2

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 6941 Color: 2
Size: 1053 Color: 0

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 5480 Color: 1
Size: 2169 Color: 3
Size: 344 Color: 2

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 5581 Color: 4
Size: 2412 Color: 0

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 6568 Color: 2
Size: 1339 Color: 4
Size: 86 Color: 1

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 6831 Color: 0
Size: 1162 Color: 2

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 3
Size: 3212 Color: 0
Size: 632 Color: 3

Bin 89: 8 of cap free
Amount of items: 3
Items: 
Size: 4524 Color: 1
Size: 3260 Color: 4
Size: 208 Color: 3

Bin 90: 8 of cap free
Amount of items: 3
Items: 
Size: 5620 Color: 4
Size: 2104 Color: 1
Size: 268 Color: 2

Bin 91: 8 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 2
Size: 1962 Color: 0
Size: 196 Color: 1

Bin 92: 9 of cap free
Amount of items: 3
Items: 
Size: 5657 Color: 0
Size: 2202 Color: 3
Size: 132 Color: 2

Bin 93: 9 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 1
Size: 931 Color: 0
Size: 48 Color: 1

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 6547 Color: 4
Size: 1443 Color: 1

Bin 95: 12 of cap free
Amount of items: 2
Items: 
Size: 5886 Color: 0
Size: 2102 Color: 4

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 2
Size: 1362 Color: 0

Bin 97: 13 of cap free
Amount of items: 2
Items: 
Size: 6225 Color: 3
Size: 1762 Color: 2

Bin 98: 13 of cap free
Amount of items: 2
Items: 
Size: 6446 Color: 2
Size: 1541 Color: 0

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 6682 Color: 4
Size: 1305 Color: 1

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 6995 Color: 0
Size: 992 Color: 1

Bin 101: 14 of cap free
Amount of items: 2
Items: 
Size: 5404 Color: 2
Size: 2582 Color: 3

Bin 102: 14 of cap free
Amount of items: 3
Items: 
Size: 6915 Color: 4
Size: 983 Color: 1
Size: 88 Color: 1

Bin 103: 15 of cap free
Amount of items: 2
Items: 
Size: 6978 Color: 3
Size: 1007 Color: 1

Bin 104: 16 of cap free
Amount of items: 3
Items: 
Size: 4884 Color: 3
Size: 2900 Color: 1
Size: 200 Color: 4

Bin 105: 16 of cap free
Amount of items: 3
Items: 
Size: 4990 Color: 0
Size: 2858 Color: 3
Size: 136 Color: 1

Bin 106: 16 of cap free
Amount of items: 2
Items: 
Size: 5844 Color: 3
Size: 2140 Color: 2

Bin 107: 17 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 0
Size: 3330 Color: 4
Size: 112 Color: 1

Bin 108: 17 of cap free
Amount of items: 2
Items: 
Size: 6380 Color: 3
Size: 1603 Color: 4

Bin 109: 18 of cap free
Amount of items: 26
Items: 
Size: 448 Color: 2
Size: 448 Color: 2
Size: 440 Color: 1
Size: 432 Color: 4
Size: 394 Color: 1
Size: 392 Color: 3
Size: 360 Color: 4
Size: 352 Color: 4
Size: 352 Color: 0
Size: 338 Color: 4
Size: 332 Color: 2
Size: 324 Color: 0
Size: 304 Color: 3
Size: 296 Color: 0
Size: 294 Color: 0
Size: 288 Color: 0
Size: 248 Color: 2
Size: 246 Color: 3
Size: 236 Color: 1
Size: 228 Color: 3
Size: 228 Color: 2
Size: 224 Color: 4
Size: 204 Color: 3
Size: 204 Color: 0
Size: 200 Color: 2
Size: 170 Color: 4

Bin 110: 18 of cap free
Amount of items: 3
Items: 
Size: 6948 Color: 4
Size: 990 Color: 0
Size: 44 Color: 3

Bin 111: 23 of cap free
Amount of items: 3
Items: 
Size: 4956 Color: 3
Size: 2741 Color: 4
Size: 280 Color: 4

Bin 112: 24 of cap free
Amount of items: 2
Items: 
Size: 6620 Color: 4
Size: 1356 Color: 1

Bin 113: 25 of cap free
Amount of items: 3
Items: 
Size: 4006 Color: 0
Size: 3709 Color: 4
Size: 260 Color: 2

Bin 114: 25 of cap free
Amount of items: 3
Items: 
Size: 4268 Color: 2
Size: 3331 Color: 0
Size: 376 Color: 4

Bin 115: 25 of cap free
Amount of items: 2
Items: 
Size: 6269 Color: 2
Size: 1706 Color: 4

Bin 116: 26 of cap free
Amount of items: 14
Items: 
Size: 1046 Color: 3
Size: 738 Color: 4
Size: 702 Color: 1
Size: 664 Color: 4
Size: 664 Color: 0
Size: 592 Color: 2
Size: 576 Color: 1
Size: 516 Color: 2
Size: 496 Color: 1
Size: 472 Color: 4
Size: 432 Color: 0
Size: 416 Color: 0
Size: 404 Color: 0
Size: 256 Color: 4

Bin 117: 27 of cap free
Amount of items: 3
Items: 
Size: 4002 Color: 4
Size: 3161 Color: 1
Size: 810 Color: 1

Bin 118: 28 of cap free
Amount of items: 2
Items: 
Size: 6674 Color: 0
Size: 1298 Color: 1

Bin 119: 29 of cap free
Amount of items: 3
Items: 
Size: 4207 Color: 2
Size: 3228 Color: 1
Size: 536 Color: 4

Bin 120: 33 of cap free
Amount of items: 3
Items: 
Size: 4711 Color: 0
Size: 2976 Color: 3
Size: 280 Color: 1

Bin 121: 43 of cap free
Amount of items: 2
Items: 
Size: 4005 Color: 1
Size: 3952 Color: 2

Bin 122: 43 of cap free
Amount of items: 2
Items: 
Size: 6153 Color: 3
Size: 1804 Color: 0

Bin 123: 47 of cap free
Amount of items: 2
Items: 
Size: 5915 Color: 3
Size: 2038 Color: 2

Bin 124: 54 of cap free
Amount of items: 2
Items: 
Size: 5399 Color: 2
Size: 2547 Color: 3

Bin 125: 57 of cap free
Amount of items: 2
Items: 
Size: 6403 Color: 0
Size: 1540 Color: 2

Bin 126: 58 of cap free
Amount of items: 2
Items: 
Size: 4430 Color: 0
Size: 3512 Color: 1

Bin 127: 68 of cap free
Amount of items: 5
Items: 
Size: 4001 Color: 1
Size: 1195 Color: 0
Size: 1178 Color: 3
Size: 802 Color: 2
Size: 756 Color: 4

Bin 128: 74 of cap free
Amount of items: 2
Items: 
Size: 6579 Color: 2
Size: 1347 Color: 0

Bin 129: 77 of cap free
Amount of items: 3
Items: 
Size: 4945 Color: 0
Size: 2686 Color: 3
Size: 292 Color: 1

Bin 130: 93 of cap free
Amount of items: 2
Items: 
Size: 5629 Color: 3
Size: 2278 Color: 0

Bin 131: 103 of cap free
Amount of items: 2
Items: 
Size: 5014 Color: 3
Size: 2883 Color: 2

Bin 132: 116 of cap free
Amount of items: 2
Items: 
Size: 4902 Color: 1
Size: 2982 Color: 3

Bin 133: 6456 of cap free
Amount of items: 9
Items: 
Size: 188 Color: 3
Size: 188 Color: 0
Size: 180 Color: 1
Size: 176 Color: 1
Size: 168 Color: 4
Size: 168 Color: 4
Size: 160 Color: 3
Size: 160 Color: 2
Size: 156 Color: 0

Total size: 1056000
Total free space: 8000

