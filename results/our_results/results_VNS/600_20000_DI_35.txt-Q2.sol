Capicity Bin: 14688
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 7354 Color: 1
Size: 1314 Color: 1
Size: 1222 Color: 1
Size: 1220 Color: 1
Size: 842 Color: 0
Size: 824 Color: 0
Size: 784 Color: 0
Size: 784 Color: 0
Size: 344 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8712 Color: 1
Size: 5672 Color: 0
Size: 304 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8936 Color: 0
Size: 5252 Color: 0
Size: 500 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10611 Color: 1
Size: 3375 Color: 0
Size: 702 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10639 Color: 0
Size: 3371 Color: 0
Size: 678 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10824 Color: 1
Size: 3336 Color: 0
Size: 528 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11582 Color: 1
Size: 2616 Color: 0
Size: 490 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11615 Color: 1
Size: 2561 Color: 0
Size: 512 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 0
Size: 1852 Color: 1
Size: 684 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12124 Color: 1
Size: 1292 Color: 0
Size: 1272 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12329 Color: 0
Size: 1431 Color: 0
Size: 928 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12854 Color: 1
Size: 1322 Color: 1
Size: 512 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 1
Size: 1428 Color: 0
Size: 304 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 1
Size: 1110 Color: 0
Size: 616 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 0
Size: 896 Color: 1
Size: 828 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12968 Color: 1
Size: 1420 Color: 0
Size: 300 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13021 Color: 1
Size: 1351 Color: 1
Size: 316 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13074 Color: 1
Size: 1246 Color: 0
Size: 368 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13109 Color: 0
Size: 1291 Color: 1
Size: 288 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13087 Color: 1
Size: 1329 Color: 0
Size: 272 Color: 1

Bin 21: 1 of cap free
Amount of items: 13
Items: 
Size: 7345 Color: 1
Size: 848 Color: 1
Size: 832 Color: 1
Size: 768 Color: 1
Size: 766 Color: 1
Size: 756 Color: 1
Size: 576 Color: 1
Size: 544 Color: 0
Size: 532 Color: 0
Size: 512 Color: 0
Size: 480 Color: 0
Size: 468 Color: 0
Size: 260 Color: 0

Bin 22: 1 of cap free
Amount of items: 4
Items: 
Size: 7729 Color: 1
Size: 6448 Color: 0
Size: 414 Color: 0
Size: 96 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 8017 Color: 1
Size: 6114 Color: 0
Size: 556 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 8420 Color: 1
Size: 5829 Color: 0
Size: 438 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 0
Size: 4959 Color: 0
Size: 456 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 10951 Color: 1
Size: 3144 Color: 0
Size: 592 Color: 0

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 11800 Color: 0
Size: 2887 Color: 1

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 12025 Color: 0
Size: 2662 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 11998 Color: 1
Size: 1793 Color: 1
Size: 896 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 0
Size: 1335 Color: 1
Size: 1244 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 12363 Color: 0
Size: 2116 Color: 1
Size: 208 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 12379 Color: 1
Size: 1686 Color: 1
Size: 622 Color: 0

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 12409 Color: 0
Size: 2278 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 12529 Color: 0
Size: 1346 Color: 1
Size: 812 Color: 1

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 12614 Color: 0
Size: 2073 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 12779 Color: 1
Size: 1540 Color: 1
Size: 368 Color: 0

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 12852 Color: 1
Size: 1835 Color: 0

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 12923 Color: 1
Size: 1764 Color: 0

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 12951 Color: 1
Size: 1736 Color: 0

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 8285 Color: 1
Size: 6117 Color: 0
Size: 284 Color: 0

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 9076 Color: 1
Size: 5258 Color: 0
Size: 352 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 0
Size: 3646 Color: 0
Size: 1048 Color: 1

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 9906 Color: 1
Size: 4500 Color: 0
Size: 280 Color: 1

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 9996 Color: 0
Size: 3794 Color: 0
Size: 896 Color: 1

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 10044 Color: 1
Size: 4642 Color: 0

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 11572 Color: 1
Size: 2082 Color: 1
Size: 1032 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 11542 Color: 0
Size: 1960 Color: 1
Size: 1184 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 12153 Color: 0
Size: 1357 Color: 1
Size: 1176 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 1
Size: 1402 Color: 0
Size: 904 Color: 0

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 12840 Color: 0
Size: 1846 Color: 1

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 12973 Color: 1
Size: 1713 Color: 0

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 13095 Color: 0
Size: 1591 Color: 1

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 13096 Color: 1
Size: 1590 Color: 0

Bin 54: 3 of cap free
Amount of items: 2
Items: 
Size: 9210 Color: 0
Size: 5475 Color: 1

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 10165 Color: 0
Size: 4520 Color: 1

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 11753 Color: 0
Size: 2932 Color: 1

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 12113 Color: 1
Size: 2332 Color: 1
Size: 240 Color: 0

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 12494 Color: 0
Size: 2191 Color: 1

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 12835 Color: 1
Size: 1332 Color: 0
Size: 518 Color: 0

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 0
Size: 1545 Color: 1

Bin 61: 4 of cap free
Amount of items: 10
Items: 
Size: 7348 Color: 0
Size: 1130 Color: 1
Size: 1120 Color: 1
Size: 990 Color: 1
Size: 960 Color: 1
Size: 752 Color: 0
Size: 724 Color: 0
Size: 704 Color: 0
Size: 640 Color: 0
Size: 316 Color: 1

Bin 62: 4 of cap free
Amount of items: 7
Items: 
Size: 7352 Color: 0
Size: 1704 Color: 1
Size: 1442 Color: 1
Size: 1302 Color: 0
Size: 1260 Color: 1
Size: 1048 Color: 0
Size: 576 Color: 0

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 8490 Color: 1
Size: 5886 Color: 1
Size: 308 Color: 0

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 9244 Color: 0
Size: 5440 Color: 1

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 9956 Color: 1
Size: 4728 Color: 0

Bin 66: 4 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 0
Size: 3784 Color: 0
Size: 668 Color: 1

Bin 67: 4 of cap free
Amount of items: 4
Items: 
Size: 10523 Color: 1
Size: 3833 Color: 0
Size: 208 Color: 0
Size: 120 Color: 1

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 11480 Color: 0
Size: 2964 Color: 1
Size: 240 Color: 0

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 12537 Color: 1
Size: 2147 Color: 0

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 8739 Color: 0
Size: 5944 Color: 1

Bin 71: 5 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 1
Size: 2067 Color: 0

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 12685 Color: 1
Size: 1998 Color: 0

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 12782 Color: 0
Size: 1901 Color: 1

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 13139 Color: 1
Size: 1544 Color: 0

Bin 75: 6 of cap free
Amount of items: 28
Items: 
Size: 736 Color: 1
Size: 728 Color: 1
Size: 694 Color: 1
Size: 690 Color: 1
Size: 680 Color: 1
Size: 674 Color: 1
Size: 674 Color: 1
Size: 656 Color: 1
Size: 656 Color: 1
Size: 648 Color: 1
Size: 624 Color: 1
Size: 584 Color: 1
Size: 576 Color: 1
Size: 550 Color: 1
Size: 454 Color: 0
Size: 416 Color: 0
Size: 416 Color: 0
Size: 412 Color: 0
Size: 412 Color: 0
Size: 408 Color: 0
Size: 400 Color: 0
Size: 386 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 376 Color: 0
Size: 376 Color: 0
Size: 368 Color: 0
Size: 320 Color: 0

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 9292 Color: 0
Size: 5166 Color: 1
Size: 224 Color: 0

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 9734 Color: 1
Size: 4684 Color: 0
Size: 264 Color: 1

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 10782 Color: 1
Size: 3900 Color: 0

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 11494 Color: 1
Size: 3118 Color: 0
Size: 70 Color: 1

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 0
Size: 1336 Color: 0
Size: 1242 Color: 1

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 12294 Color: 0
Size: 2388 Color: 1

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 9896 Color: 1
Size: 3399 Color: 0
Size: 1386 Color: 1

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 11050 Color: 0
Size: 3631 Color: 1

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 11580 Color: 1
Size: 2759 Color: 0
Size: 342 Color: 0

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 11745 Color: 0
Size: 2936 Color: 1

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 12742 Color: 0
Size: 1939 Color: 1

Bin 87: 7 of cap free
Amount of items: 3
Items: 
Size: 13026 Color: 1
Size: 1431 Color: 0
Size: 224 Color: 0

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 9032 Color: 1
Size: 4984 Color: 0
Size: 664 Color: 1

Bin 89: 8 of cap free
Amount of items: 3
Items: 
Size: 11132 Color: 0
Size: 3324 Color: 0
Size: 224 Color: 1

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 11225 Color: 1
Size: 3455 Color: 0

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 13069 Color: 1
Size: 1611 Color: 0

Bin 92: 9 of cap free
Amount of items: 3
Items: 
Size: 8935 Color: 0
Size: 5420 Color: 0
Size: 324 Color: 1

Bin 93: 9 of cap free
Amount of items: 2
Items: 
Size: 9118 Color: 0
Size: 5561 Color: 1

Bin 94: 9 of cap free
Amount of items: 2
Items: 
Size: 10671 Color: 0
Size: 4008 Color: 1

Bin 95: 9 of cap free
Amount of items: 3
Items: 
Size: 11172 Color: 0
Size: 2595 Color: 1
Size: 912 Color: 1

Bin 96: 9 of cap free
Amount of items: 2
Items: 
Size: 12755 Color: 0
Size: 1924 Color: 1

Bin 97: 9 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 1
Size: 1503 Color: 0

Bin 98: 10 of cap free
Amount of items: 11
Items: 
Size: 7346 Color: 0
Size: 958 Color: 1
Size: 944 Color: 1
Size: 932 Color: 1
Size: 928 Color: 1
Size: 888 Color: 1
Size: 592 Color: 0
Size: 582 Color: 0
Size: 576 Color: 0
Size: 568 Color: 0
Size: 364 Color: 1

Bin 99: 10 of cap free
Amount of items: 2
Items: 
Size: 10138 Color: 0
Size: 4540 Color: 1

Bin 100: 10 of cap free
Amount of items: 2
Items: 
Size: 10467 Color: 0
Size: 4211 Color: 1

Bin 101: 11 of cap free
Amount of items: 2
Items: 
Size: 10012 Color: 1
Size: 4665 Color: 0

Bin 102: 11 of cap free
Amount of items: 2
Items: 
Size: 12073 Color: 0
Size: 2604 Color: 1

Bin 103: 12 of cap free
Amount of items: 2
Items: 
Size: 11774 Color: 1
Size: 2902 Color: 0

Bin 104: 13 of cap free
Amount of items: 2
Items: 
Size: 10708 Color: 1
Size: 3967 Color: 0

Bin 105: 13 of cap free
Amount of items: 2
Items: 
Size: 11560 Color: 0
Size: 3115 Color: 1

Bin 106: 13 of cap free
Amount of items: 2
Items: 
Size: 12053 Color: 0
Size: 2622 Color: 1

Bin 107: 13 of cap free
Amount of items: 2
Items: 
Size: 12666 Color: 1
Size: 2009 Color: 0

Bin 108: 13 of cap free
Amount of items: 2
Items: 
Size: 12708 Color: 1
Size: 1967 Color: 0

Bin 109: 14 of cap free
Amount of items: 2
Items: 
Size: 12156 Color: 1
Size: 2518 Color: 0

Bin 110: 15 of cap free
Amount of items: 2
Items: 
Size: 10543 Color: 1
Size: 4130 Color: 0

Bin 111: 15 of cap free
Amount of items: 2
Items: 
Size: 12885 Color: 1
Size: 1788 Color: 0

Bin 112: 16 of cap free
Amount of items: 2
Items: 
Size: 12430 Color: 1
Size: 2242 Color: 0

Bin 113: 16 of cap free
Amount of items: 2
Items: 
Size: 12552 Color: 0
Size: 2120 Color: 1

Bin 114: 17 of cap free
Amount of items: 2
Items: 
Size: 12218 Color: 0
Size: 2453 Color: 1

Bin 115: 17 of cap free
Amount of items: 2
Items: 
Size: 12474 Color: 1
Size: 2197 Color: 0

Bin 116: 17 of cap free
Amount of items: 2
Items: 
Size: 12609 Color: 0
Size: 2062 Color: 1

Bin 117: 17 of cap free
Amount of items: 2
Items: 
Size: 12980 Color: 1
Size: 1691 Color: 0

Bin 118: 18 of cap free
Amount of items: 2
Items: 
Size: 11258 Color: 1
Size: 3412 Color: 0

Bin 119: 19 of cap free
Amount of items: 2
Items: 
Size: 13198 Color: 0
Size: 1471 Color: 1

Bin 120: 20 of cap free
Amount of items: 3
Items: 
Size: 7560 Color: 1
Size: 6116 Color: 0
Size: 992 Color: 1

Bin 121: 20 of cap free
Amount of items: 2
Items: 
Size: 11943 Color: 0
Size: 2725 Color: 1

Bin 122: 20 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 0
Size: 1892 Color: 1

Bin 123: 20 of cap free
Amount of items: 2
Items: 
Size: 12838 Color: 0
Size: 1830 Color: 1

Bin 124: 20 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 1
Size: 1456 Color: 0

Bin 125: 21 of cap free
Amount of items: 3
Items: 
Size: 10314 Color: 0
Size: 4149 Color: 1
Size: 204 Color: 1

Bin 126: 22 of cap free
Amount of items: 3
Items: 
Size: 9711 Color: 0
Size: 4795 Color: 0
Size: 160 Color: 1

Bin 127: 22 of cap free
Amount of items: 2
Items: 
Size: 10152 Color: 1
Size: 4514 Color: 0

Bin 128: 22 of cap free
Amount of items: 2
Items: 
Size: 13068 Color: 0
Size: 1598 Color: 1

Bin 129: 23 of cap free
Amount of items: 2
Items: 
Size: 12376 Color: 0
Size: 2289 Color: 1

Bin 130: 24 of cap free
Amount of items: 2
Items: 
Size: 13012 Color: 0
Size: 1652 Color: 1

Bin 131: 26 of cap free
Amount of items: 2
Items: 
Size: 10596 Color: 1
Size: 4066 Color: 0

Bin 132: 26 of cap free
Amount of items: 2
Items: 
Size: 13130 Color: 1
Size: 1532 Color: 0

Bin 133: 32 of cap free
Amount of items: 2
Items: 
Size: 11185 Color: 1
Size: 3471 Color: 0

Bin 134: 32 of cap free
Amount of items: 2
Items: 
Size: 11575 Color: 0
Size: 3081 Color: 1

Bin 135: 32 of cap free
Amount of items: 2
Items: 
Size: 12209 Color: 1
Size: 2447 Color: 0

Bin 136: 32 of cap free
Amount of items: 2
Items: 
Size: 12872 Color: 1
Size: 1784 Color: 0

Bin 137: 33 of cap free
Amount of items: 8
Items: 
Size: 7356 Color: 1
Size: 1405 Color: 1
Size: 1404 Color: 1
Size: 1362 Color: 1
Size: 928 Color: 0
Size: 928 Color: 0
Size: 856 Color: 0
Size: 416 Color: 0

Bin 138: 33 of cap free
Amount of items: 2
Items: 
Size: 10089 Color: 0
Size: 4566 Color: 1

Bin 139: 34 of cap free
Amount of items: 2
Items: 
Size: 13010 Color: 0
Size: 1644 Color: 1

Bin 140: 36 of cap free
Amount of items: 2
Items: 
Size: 12724 Color: 1
Size: 1928 Color: 0

Bin 141: 39 of cap free
Amount of items: 2
Items: 
Size: 12468 Color: 1
Size: 2181 Color: 0

Bin 142: 40 of cap free
Amount of items: 2
Items: 
Size: 11670 Color: 0
Size: 2978 Color: 1

Bin 143: 41 of cap free
Amount of items: 2
Items: 
Size: 12976 Color: 1
Size: 1671 Color: 0

Bin 144: 42 of cap free
Amount of items: 2
Items: 
Size: 7896 Color: 0
Size: 6750 Color: 1

Bin 145: 44 of cap free
Amount of items: 39
Items: 
Size: 524 Color: 1
Size: 520 Color: 1
Size: 512 Color: 1
Size: 488 Color: 1
Size: 464 Color: 1
Size: 464 Color: 1
Size: 448 Color: 1
Size: 448 Color: 1
Size: 442 Color: 1
Size: 434 Color: 1
Size: 428 Color: 1
Size: 424 Color: 1
Size: 424 Color: 1
Size: 416 Color: 1
Size: 414 Color: 1
Size: 396 Color: 1
Size: 392 Color: 1
Size: 384 Color: 1
Size: 378 Color: 1
Size: 376 Color: 1
Size: 358 Color: 0
Size: 358 Color: 0
Size: 336 Color: 0
Size: 336 Color: 0
Size: 336 Color: 0
Size: 332 Color: 0
Size: 328 Color: 0
Size: 318 Color: 0
Size: 308 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 294 Color: 0
Size: 288 Color: 0
Size: 284 Color: 0
Size: 280 Color: 0
Size: 280 Color: 0
Size: 276 Color: 0
Size: 276 Color: 0
Size: 272 Color: 0

Bin 146: 44 of cap free
Amount of items: 2
Items: 
Size: 10696 Color: 0
Size: 3948 Color: 1

Bin 147: 44 of cap free
Amount of items: 2
Items: 
Size: 11878 Color: 1
Size: 2766 Color: 0

Bin 148: 44 of cap free
Amount of items: 2
Items: 
Size: 12644 Color: 0
Size: 2000 Color: 1

Bin 149: 44 of cap free
Amount of items: 2
Items: 
Size: 13114 Color: 1
Size: 1530 Color: 0

Bin 150: 45 of cap free
Amount of items: 2
Items: 
Size: 11419 Color: 0
Size: 3224 Color: 1

Bin 151: 45 of cap free
Amount of items: 2
Items: 
Size: 13194 Color: 1
Size: 1449 Color: 0

Bin 152: 46 of cap free
Amount of items: 2
Items: 
Size: 7362 Color: 1
Size: 7280 Color: 0

Bin 153: 47 of cap free
Amount of items: 2
Items: 
Size: 12420 Color: 1
Size: 2221 Color: 0

Bin 154: 48 of cap free
Amount of items: 2
Items: 
Size: 10920 Color: 1
Size: 3720 Color: 0

Bin 155: 50 of cap free
Amount of items: 2
Items: 
Size: 12774 Color: 0
Size: 1864 Color: 1

Bin 156: 51 of cap free
Amount of items: 2
Items: 
Size: 11118 Color: 0
Size: 3519 Color: 1

Bin 157: 51 of cap free
Amount of items: 2
Items: 
Size: 11379 Color: 1
Size: 3258 Color: 0

Bin 158: 54 of cap free
Amount of items: 2
Items: 
Size: 12553 Color: 1
Size: 2081 Color: 0

Bin 159: 54 of cap free
Amount of items: 2
Items: 
Size: 13092 Color: 0
Size: 1542 Color: 1

Bin 160: 57 of cap free
Amount of items: 2
Items: 
Size: 12289 Color: 0
Size: 2342 Color: 1

Bin 161: 60 of cap free
Amount of items: 2
Items: 
Size: 12130 Color: 1
Size: 2498 Color: 0

Bin 162: 63 of cap free
Amount of items: 2
Items: 
Size: 13003 Color: 0
Size: 1622 Color: 1

Bin 163: 64 of cap free
Amount of items: 2
Items: 
Size: 12456 Color: 0
Size: 2168 Color: 1

Bin 164: 64 of cap free
Amount of items: 2
Items: 
Size: 13180 Color: 0
Size: 1444 Color: 1

Bin 165: 68 of cap free
Amount of items: 2
Items: 
Size: 10333 Color: 0
Size: 4287 Color: 1

Bin 166: 70 of cap free
Amount of items: 2
Items: 
Size: 9810 Color: 0
Size: 4808 Color: 1

Bin 167: 72 of cap free
Amount of items: 2
Items: 
Size: 12201 Color: 1
Size: 2415 Color: 0

Bin 168: 78 of cap free
Amount of items: 2
Items: 
Size: 11188 Color: 0
Size: 3422 Color: 1

Bin 169: 82 of cap free
Amount of items: 2
Items: 
Size: 11744 Color: 0
Size: 2862 Color: 1

Bin 170: 87 of cap free
Amount of items: 2
Items: 
Size: 12193 Color: 0
Size: 2408 Color: 1

Bin 171: 102 of cap free
Amount of items: 2
Items: 
Size: 12661 Color: 1
Size: 1925 Color: 0

Bin 172: 104 of cap free
Amount of items: 3
Items: 
Size: 10991 Color: 0
Size: 1801 Color: 1
Size: 1792 Color: 1

Bin 173: 107 of cap free
Amount of items: 2
Items: 
Size: 11901 Color: 0
Size: 2680 Color: 1

Bin 174: 112 of cap free
Amount of items: 2
Items: 
Size: 10660 Color: 0
Size: 3916 Color: 1

Bin 175: 114 of cap free
Amount of items: 2
Items: 
Size: 12844 Color: 1
Size: 1730 Color: 0

Bin 176: 118 of cap free
Amount of items: 2
Items: 
Size: 11646 Color: 1
Size: 2924 Color: 0

Bin 177: 119 of cap free
Amount of items: 2
Items: 
Size: 9545 Color: 1
Size: 5024 Color: 0

Bin 178: 127 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 1
Size: 2921 Color: 0

Bin 179: 128 of cap free
Amount of items: 2
Items: 
Size: 12404 Color: 1
Size: 2156 Color: 0

Bin 180: 128 of cap free
Amount of items: 2
Items: 
Size: 12652 Color: 1
Size: 1908 Color: 0

Bin 181: 134 of cap free
Amount of items: 2
Items: 
Size: 13106 Color: 1
Size: 1448 Color: 0

Bin 182: 151 of cap free
Amount of items: 8
Items: 
Size: 7349 Color: 0
Size: 1220 Color: 1
Size: 1216 Color: 1
Size: 1216 Color: 1
Size: 1216 Color: 1
Size: 776 Color: 0
Size: 776 Color: 0
Size: 768 Color: 0

Bin 183: 156 of cap free
Amount of items: 2
Items: 
Size: 12392 Color: 1
Size: 2140 Color: 0

Bin 184: 161 of cap free
Amount of items: 3
Items: 
Size: 9635 Color: 1
Size: 3364 Color: 0
Size: 1528 Color: 1

Bin 185: 163 of cap free
Amount of items: 2
Items: 
Size: 11176 Color: 0
Size: 3349 Color: 1

Bin 186: 169 of cap free
Amount of items: 2
Items: 
Size: 9091 Color: 0
Size: 5428 Color: 1

Bin 187: 169 of cap free
Amount of items: 2
Items: 
Size: 10643 Color: 0
Size: 3876 Color: 1

Bin 188: 171 of cap free
Amount of items: 2
Items: 
Size: 12194 Color: 1
Size: 2323 Color: 0

Bin 189: 173 of cap free
Amount of items: 2
Items: 
Size: 7626 Color: 0
Size: 6889 Color: 1

Bin 190: 173 of cap free
Amount of items: 2
Items: 
Size: 12633 Color: 1
Size: 1882 Color: 0

Bin 191: 176 of cap free
Amount of items: 2
Items: 
Size: 8388 Color: 1
Size: 6124 Color: 0

Bin 192: 178 of cap free
Amount of items: 2
Items: 
Size: 10582 Color: 1
Size: 3928 Color: 0

Bin 193: 183 of cap free
Amount of items: 2
Items: 
Size: 13061 Color: 1
Size: 1444 Color: 0

Bin 194: 184 of cap free
Amount of items: 2
Items: 
Size: 8382 Color: 1
Size: 6122 Color: 0

Bin 195: 187 of cap free
Amount of items: 2
Items: 
Size: 13058 Color: 1
Size: 1443 Color: 0

Bin 196: 188 of cap free
Amount of items: 2
Items: 
Size: 11900 Color: 0
Size: 2600 Color: 1

Bin 197: 196 of cap free
Amount of items: 2
Items: 
Size: 8372 Color: 0
Size: 6120 Color: 1

Bin 198: 200 of cap free
Amount of items: 2
Items: 
Size: 11892 Color: 0
Size: 2596 Color: 1

Bin 199: 7620 of cap free
Amount of items: 27
Items: 
Size: 336 Color: 1
Size: 322 Color: 1
Size: 300 Color: 1
Size: 288 Color: 1
Size: 280 Color: 1
Size: 276 Color: 1
Size: 272 Color: 0
Size: 270 Color: 0
Size: 268 Color: 0
Size: 268 Color: 0
Size: 268 Color: 0
Size: 268 Color: 0
Size: 266 Color: 0
Size: 264 Color: 1
Size: 260 Color: 0
Size: 258 Color: 0
Size: 256 Color: 1
Size: 256 Color: 1
Size: 256 Color: 1
Size: 256 Color: 0
Size: 256 Color: 0
Size: 248 Color: 1
Size: 248 Color: 0
Size: 248 Color: 0
Size: 232 Color: 0
Size: 200 Color: 1
Size: 148 Color: 1

Total size: 2908224
Total free space: 14688

