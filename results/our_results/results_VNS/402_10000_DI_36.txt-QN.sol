Capicity Bin: 8256
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 297
Size: 2916 Color: 250
Size: 184 Color: 29

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5922 Color: 320
Size: 2182 Color: 229
Size: 152 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5960 Color: 322
Size: 2072 Color: 226
Size: 224 Color: 49

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5987 Color: 323
Size: 2013 Color: 224
Size: 256 Color: 64

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6095 Color: 327
Size: 1106 Color: 171
Size: 1055 Color: 164

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6249 Color: 332
Size: 1673 Color: 207
Size: 334 Color: 87

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 340
Size: 1100 Color: 170
Size: 732 Color: 138

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6479 Color: 344
Size: 1481 Color: 196
Size: 296 Color: 76

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 345
Size: 1032 Color: 161
Size: 740 Color: 139

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 355
Size: 1002 Color: 159
Size: 544 Color: 119

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6740 Color: 357
Size: 908 Color: 150
Size: 608 Color: 126

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 358
Size: 1268 Color: 182
Size: 244 Color: 59

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6804 Color: 361
Size: 1176 Color: 176
Size: 276 Color: 72

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6814 Color: 362
Size: 1174 Color: 175
Size: 268 Color: 68

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6837 Color: 363
Size: 1183 Color: 177
Size: 236 Color: 52

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6850 Color: 364
Size: 1094 Color: 168
Size: 312 Color: 81

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6925 Color: 368
Size: 1111 Color: 172
Size: 220 Color: 47

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 370
Size: 828 Color: 144
Size: 488 Color: 111

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 376
Size: 1096 Color: 169
Size: 156 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 377
Size: 1022 Color: 160
Size: 220 Color: 48

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7094 Color: 380
Size: 1062 Color: 165
Size: 100 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7104 Color: 381
Size: 904 Color: 149
Size: 248 Color: 60

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7120 Color: 383
Size: 922 Color: 152
Size: 214 Color: 42

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7166 Color: 385
Size: 742 Color: 140
Size: 348 Color: 88

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 386
Size: 836 Color: 147
Size: 248 Color: 61

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7242 Color: 390
Size: 766 Color: 141
Size: 248 Color: 62

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7258 Color: 391
Size: 834 Color: 145
Size: 164 Color: 21

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7298 Color: 393
Size: 834 Color: 146
Size: 124 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7300 Color: 394
Size: 652 Color: 128
Size: 304 Color: 80

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7320 Color: 396
Size: 672 Color: 129
Size: 264 Color: 66

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7370 Color: 398
Size: 584 Color: 122
Size: 302 Color: 77

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7372 Color: 399
Size: 680 Color: 131
Size: 204 Color: 37

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7416 Color: 401
Size: 568 Color: 121
Size: 272 Color: 71

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7430 Color: 402
Size: 556 Color: 120
Size: 270 Color: 69

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 5995 Color: 325
Size: 1284 Color: 184
Size: 976 Color: 157

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6126 Color: 329
Size: 1801 Color: 212
Size: 328 Color: 86

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6253 Color: 333
Size: 1862 Color: 214
Size: 140 Color: 12

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6617 Color: 349
Size: 1202 Color: 178
Size: 436 Color: 104

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 352
Size: 1484 Color: 197
Size: 116 Color: 7

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 378
Size: 1225 Color: 180

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 7176 Color: 387
Size: 1079 Color: 167

Bin 42: 2 of cap free
Amount of items: 9
Items: 
Size: 4130 Color: 273
Size: 684 Color: 132
Size: 680 Color: 130
Size: 596 Color: 125
Size: 586 Color: 124
Size: 586 Color: 123
Size: 512 Color: 118
Size: 240 Color: 55
Size: 240 Color: 54

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 5923 Color: 321
Size: 1947 Color: 220
Size: 384 Color: 96

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 6116 Color: 328
Size: 2138 Color: 227

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 6952 Color: 372
Size: 1302 Color: 187

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 6982 Color: 374
Size: 1272 Color: 183

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 7182 Color: 388
Size: 1072 Color: 166

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 7210 Color: 389
Size: 1044 Color: 163

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 7380 Color: 400
Size: 874 Color: 148

Bin 50: 3 of cap free
Amount of items: 2
Items: 
Size: 6671 Color: 353
Size: 1582 Color: 202

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 6963 Color: 373
Size: 1290 Color: 185

Bin 52: 3 of cap free
Amount of items: 2
Items: 
Size: 7108 Color: 382
Size: 1145 Color: 174

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 5601 Color: 309
Size: 2507 Color: 240
Size: 144 Color: 16

Bin 54: 4 of cap free
Amount of items: 2
Items: 
Size: 5988 Color: 324
Size: 2264 Color: 233

Bin 55: 4 of cap free
Amount of items: 2
Items: 
Size: 6598 Color: 348
Size: 1654 Color: 206

Bin 56: 4 of cap free
Amount of items: 2
Items: 
Size: 6724 Color: 356
Size: 1528 Color: 200

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 6856 Color: 365
Size: 1396 Color: 194

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 7304 Color: 395
Size: 948 Color: 154

Bin 59: 5 of cap free
Amount of items: 3
Items: 
Size: 5629 Color: 311
Size: 2478 Color: 238
Size: 144 Color: 14

Bin 60: 5 of cap free
Amount of items: 2
Items: 
Size: 6463 Color: 342
Size: 1788 Color: 211

Bin 61: 5 of cap free
Amount of items: 3
Items: 
Size: 6627 Color: 350
Size: 1608 Color: 203
Size: 16 Color: 0

Bin 62: 5 of cap free
Amount of items: 2
Items: 
Size: 6787 Color: 360
Size: 1464 Color: 195

Bin 63: 5 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 369
Size: 1321 Color: 188

Bin 64: 6 of cap free
Amount of items: 2
Items: 
Size: 6358 Color: 338
Size: 1892 Color: 217

Bin 65: 6 of cap free
Amount of items: 2
Items: 
Size: 6630 Color: 351
Size: 1620 Color: 204

Bin 66: 6 of cap free
Amount of items: 2
Items: 
Size: 6760 Color: 359
Size: 1490 Color: 198

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 6883 Color: 366
Size: 1367 Color: 192

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 7260 Color: 392
Size: 990 Color: 158

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 7338 Color: 397
Size: 912 Color: 151

Bin 70: 7 of cap free
Amount of items: 3
Items: 
Size: 5544 Color: 308
Size: 2561 Color: 243
Size: 144 Color: 17

Bin 71: 7 of cap free
Amount of items: 3
Items: 
Size: 5617 Color: 310
Size: 2488 Color: 239
Size: 144 Color: 15

Bin 72: 7 of cap free
Amount of items: 3
Items: 
Size: 6200 Color: 330
Size: 1359 Color: 191
Size: 690 Color: 136

Bin 73: 7 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 334
Size: 1975 Color: 223

Bin 74: 7 of cap free
Amount of items: 2
Items: 
Size: 6435 Color: 341
Size: 1814 Color: 213

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 6694 Color: 354
Size: 1555 Color: 201

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 6914 Color: 367
Size: 1335 Color: 189

Bin 77: 8 of cap free
Amount of items: 3
Items: 
Size: 5288 Color: 304
Size: 2784 Color: 246
Size: 176 Color: 25

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 6344 Color: 337
Size: 1904 Color: 218

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 6470 Color: 343
Size: 1778 Color: 210

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 6285 Color: 335
Size: 1962 Color: 222

Bin 81: 9 of cap free
Amount of items: 2
Items: 
Size: 6991 Color: 375
Size: 1256 Color: 181

Bin 82: 10 of cap free
Amount of items: 3
Items: 
Size: 4914 Color: 293
Size: 3140 Color: 257
Size: 192 Color: 32

Bin 83: 10 of cap free
Amount of items: 2
Items: 
Size: 5218 Color: 300
Size: 3028 Color: 255

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 5784 Color: 316
Size: 2462 Color: 237

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 6226 Color: 331
Size: 2020 Color: 225

Bin 86: 10 of cap free
Amount of items: 2
Items: 
Size: 7124 Color: 384
Size: 1122 Color: 173

Bin 87: 12 of cap free
Amount of items: 2
Items: 
Size: 6316 Color: 336
Size: 1928 Color: 219

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 7032 Color: 379
Size: 1212 Color: 179

Bin 89: 14 of cap free
Amount of items: 2
Items: 
Size: 6946 Color: 371
Size: 1296 Color: 186

Bin 90: 16 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 314
Size: 2450 Color: 236
Size: 136 Color: 11

Bin 91: 17 of cap free
Amount of items: 2
Items: 
Size: 6026 Color: 326
Size: 2213 Color: 232

Bin 92: 18 of cap free
Amount of items: 2
Items: 
Size: 6374 Color: 339
Size: 1864 Color: 215

Bin 93: 21 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 298
Size: 2868 Color: 248
Size: 184 Color: 28

Bin 94: 21 of cap free
Amount of items: 2
Items: 
Size: 5241 Color: 301
Size: 2994 Color: 254

Bin 95: 22 of cap free
Amount of items: 3
Items: 
Size: 5318 Color: 306
Size: 2744 Color: 245
Size: 172 Color: 22

Bin 96: 23 of cap free
Amount of items: 4
Items: 
Size: 5906 Color: 318
Size: 2191 Color: 230
Size: 72 Color: 4
Size: 64 Color: 3

Bin 97: 25 of cap free
Amount of items: 2
Items: 
Size: 6588 Color: 347
Size: 1643 Color: 205

Bin 98: 26 of cap free
Amount of items: 3
Items: 
Size: 5184 Color: 299
Size: 2870 Color: 249
Size: 176 Color: 27

Bin 99: 26 of cap free
Amount of items: 2
Items: 
Size: 6504 Color: 346
Size: 1726 Color: 209

Bin 100: 29 of cap free
Amount of items: 3
Items: 
Size: 5249 Color: 302
Size: 2802 Color: 247
Size: 176 Color: 26

Bin 101: 30 of cap free
Amount of items: 2
Items: 
Size: 4898 Color: 292
Size: 3328 Color: 260

Bin 102: 30 of cap free
Amount of items: 2
Items: 
Size: 5638 Color: 312
Size: 2588 Color: 244

Bin 103: 30 of cap free
Amount of items: 4
Items: 
Size: 5836 Color: 317
Size: 2170 Color: 228
Size: 128 Color: 9
Size: 92 Color: 5

Bin 104: 31 of cap free
Amount of items: 2
Items: 
Size: 5286 Color: 303
Size: 2939 Color: 253

Bin 105: 38 of cap free
Amount of items: 3
Items: 
Size: 4747 Color: 290
Size: 3271 Color: 259
Size: 200 Color: 34

Bin 106: 42 of cap free
Amount of items: 4
Items: 
Size: 5917 Color: 319
Size: 2201 Color: 231
Size: 64 Color: 2
Size: 32 Color: 1

Bin 107: 46 of cap free
Amount of items: 3
Items: 
Size: 4584 Color: 284
Size: 3426 Color: 263
Size: 200 Color: 35

Bin 108: 47 of cap free
Amount of items: 5
Items: 
Size: 4138 Color: 277
Size: 1712 Color: 208
Size: 1519 Color: 199
Size: 624 Color: 127
Size: 216 Color: 45

Bin 109: 50 of cap free
Amount of items: 3
Items: 
Size: 4820 Color: 291
Size: 3190 Color: 258
Size: 196 Color: 33

Bin 110: 52 of cap free
Amount of items: 2
Items: 
Size: 5140 Color: 296
Size: 3064 Color: 256

Bin 111: 60 of cap free
Amount of items: 3
Items: 
Size: 5696 Color: 315
Size: 2372 Color: 235
Size: 128 Color: 10

Bin 112: 62 of cap free
Amount of items: 4
Items: 
Size: 4140 Color: 278
Size: 1951 Color: 221
Size: 1887 Color: 216
Size: 216 Color: 44

Bin 113: 68 of cap free
Amount of items: 2
Items: 
Size: 4739 Color: 289
Size: 3449 Color: 268

Bin 114: 70 of cap free
Amount of items: 3
Items: 
Size: 4172 Color: 281
Size: 3804 Color: 271
Size: 210 Color: 40

Bin 115: 77 of cap free
Amount of items: 2
Items: 
Size: 4731 Color: 288
Size: 3448 Color: 267

Bin 116: 89 of cap free
Amount of items: 4
Items: 
Size: 5302 Color: 305
Size: 2513 Color: 241
Size: 176 Color: 24
Size: 176 Color: 23

Bin 117: 96 of cap free
Amount of items: 8
Items: 
Size: 4132 Color: 274
Size: 792 Color: 142
Size: 712 Color: 137
Size: 686 Color: 135
Size: 684 Color: 134
Size: 684 Color: 133
Size: 238 Color: 53
Size: 232 Color: 51

Bin 118: 103 of cap free
Amount of items: 4
Items: 
Size: 4333 Color: 282
Size: 3404 Color: 261
Size: 208 Color: 39
Size: 208 Color: 38

Bin 119: 104 of cap free
Amount of items: 3
Items: 
Size: 5644 Color: 313
Size: 2364 Color: 234
Size: 144 Color: 13

Bin 120: 105 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 295
Size: 2931 Color: 252
Size: 192 Color: 30

Bin 121: 115 of cap free
Amount of items: 3
Items: 
Size: 4154 Color: 280
Size: 3775 Color: 270
Size: 212 Color: 41

Bin 122: 122 of cap free
Amount of items: 5
Items: 
Size: 4136 Color: 276
Size: 1382 Color: 193
Size: 1358 Color: 190
Size: 1038 Color: 162
Size: 220 Color: 46

Bin 123: 132 of cap free
Amount of items: 2
Items: 
Size: 4682 Color: 287
Size: 3442 Color: 266

Bin 124: 134 of cap free
Amount of items: 22
Items: 
Size: 480 Color: 110
Size: 472 Color: 109
Size: 472 Color: 108
Size: 448 Color: 107
Size: 442 Color: 106
Size: 438 Color: 105
Size: 436 Color: 103
Size: 432 Color: 102
Size: 400 Color: 101
Size: 400 Color: 100
Size: 400 Color: 99
Size: 388 Color: 98
Size: 320 Color: 83
Size: 316 Color: 82
Size: 304 Color: 79
Size: 304 Color: 78
Size: 296 Color: 75
Size: 288 Color: 74
Size: 288 Color: 73
Size: 272 Color: 70
Size: 266 Color: 67
Size: 260 Color: 65

Bin 125: 138 of cap free
Amount of items: 3
Items: 
Size: 4492 Color: 283
Size: 3422 Color: 262
Size: 204 Color: 36

Bin 126: 142 of cap free
Amount of items: 3
Items: 
Size: 5420 Color: 307
Size: 2534 Color: 242
Size: 160 Color: 20

Bin 127: 148 of cap free
Amount of items: 2
Items: 
Size: 4667 Color: 286
Size: 3441 Color: 265

Bin 128: 149 of cap free
Amount of items: 11
Items: 
Size: 4129 Color: 272
Size: 512 Color: 117
Size: 504 Color: 116
Size: 502 Color: 115
Size: 500 Color: 114
Size: 492 Color: 113
Size: 492 Color: 112
Size: 256 Color: 63
Size: 240 Color: 58
Size: 240 Color: 57
Size: 240 Color: 56

Bin 129: 156 of cap free
Amount of items: 2
Items: 
Size: 4666 Color: 285
Size: 3434 Color: 264

Bin 130: 170 of cap free
Amount of items: 3
Items: 
Size: 4146 Color: 279
Size: 3724 Color: 269
Size: 216 Color: 43

Bin 131: 171 of cap free
Amount of items: 3
Items: 
Size: 4968 Color: 294
Size: 2925 Color: 251
Size: 192 Color: 31

Bin 132: 225 of cap free
Amount of items: 6
Items: 
Size: 4133 Color: 275
Size: 970 Color: 156
Size: 964 Color: 155
Size: 928 Color: 153
Size: 808 Color: 143
Size: 228 Color: 50

Bin 133: 4684 of cap free
Amount of items: 10
Items: 
Size: 388 Color: 97
Size: 376 Color: 95
Size: 368 Color: 94
Size: 368 Color: 93
Size: 360 Color: 92
Size: 358 Color: 91
Size: 352 Color: 90
Size: 352 Color: 89
Size: 328 Color: 85
Size: 322 Color: 84

Total size: 1089792
Total free space: 8256

