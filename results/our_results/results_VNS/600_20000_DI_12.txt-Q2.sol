Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 14
Items: 
Size: 9826 Color: 1
Size: 928 Color: 1
Size: 928 Color: 1
Size: 924 Color: 1
Size: 918 Color: 1
Size: 912 Color: 1
Size: 716 Color: 0
Size: 708 Color: 0
Size: 704 Color: 0
Size: 704 Color: 0
Size: 704 Color: 0
Size: 704 Color: 0
Size: 628 Color: 1
Size: 344 Color: 0

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 9848 Color: 0
Size: 2974 Color: 1
Size: 1730 Color: 1
Size: 1600 Color: 0
Size: 1600 Color: 0
Size: 1064 Color: 0
Size: 832 Color: 1

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 10306 Color: 1
Size: 6786 Color: 0
Size: 1852 Color: 0
Size: 384 Color: 1
Size: 320 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10354 Color: 0
Size: 6666 Color: 0
Size: 2628 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 0
Size: 6626 Color: 0
Size: 2588 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11506 Color: 1
Size: 6614 Color: 1
Size: 1528 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12259 Color: 1
Size: 6159 Color: 0
Size: 1230 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12448 Color: 0
Size: 6720 Color: 0
Size: 480 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 0
Size: 5284 Color: 0
Size: 376 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14064 Color: 0
Size: 5168 Color: 0
Size: 416 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 1
Size: 4064 Color: 0
Size: 1532 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 1
Size: 4668 Color: 0
Size: 368 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 0
Size: 4104 Color: 0
Size: 954 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 1
Size: 3996 Color: 0
Size: 1036 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 0
Size: 4544 Color: 1
Size: 376 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 14866 Color: 1
Size: 4142 Color: 0
Size: 640 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14876 Color: 1
Size: 3084 Color: 1
Size: 1688 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15048 Color: 0
Size: 3784 Color: 1
Size: 816 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15112 Color: 1
Size: 4136 Color: 0
Size: 400 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15240 Color: 1
Size: 4096 Color: 0
Size: 312 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15336 Color: 1
Size: 3792 Color: 0
Size: 520 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15346 Color: 0
Size: 2400 Color: 1
Size: 1902 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16072 Color: 1
Size: 1892 Color: 0
Size: 1684 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16168 Color: 1
Size: 2148 Color: 0
Size: 1332 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16240 Color: 1
Size: 3072 Color: 1
Size: 336 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16484 Color: 1
Size: 1632 Color: 0
Size: 1532 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16920 Color: 1
Size: 2136 Color: 0
Size: 592 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 17056 Color: 0
Size: 1632 Color: 0
Size: 960 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 17136 Color: 0
Size: 1824 Color: 0
Size: 688 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 17190 Color: 1
Size: 2042 Color: 0
Size: 416 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 17340 Color: 0
Size: 1156 Color: 1
Size: 1152 Color: 0

Bin 32: 0 of cap free
Amount of items: 4
Items: 
Size: 17446 Color: 0
Size: 1838 Color: 1
Size: 268 Color: 0
Size: 96 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 17544 Color: 1
Size: 1768 Color: 0
Size: 336 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 17574 Color: 1
Size: 1636 Color: 0
Size: 438 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 17636 Color: 0
Size: 1924 Color: 1
Size: 88 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 17632 Color: 1
Size: 1632 Color: 1
Size: 384 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 17640 Color: 0
Size: 1928 Color: 1
Size: 80 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 13250 Color: 1
Size: 5849 Color: 0
Size: 548 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 13865 Color: 0
Size: 5334 Color: 0
Size: 448 Color: 1

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 0
Size: 3957 Color: 1
Size: 1798 Color: 0

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 13917 Color: 0
Size: 5442 Color: 1
Size: 288 Color: 1

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 14133 Color: 0
Size: 3878 Color: 1
Size: 1636 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 14112 Color: 1
Size: 4597 Color: 0
Size: 938 Color: 1

Bin 44: 2 of cap free
Amount of items: 8
Items: 
Size: 9836 Color: 1
Size: 1650 Color: 1
Size: 1636 Color: 1
Size: 1632 Color: 1
Size: 1600 Color: 0
Size: 1556 Color: 0
Size: 1408 Color: 0
Size: 328 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 10402 Color: 1
Size: 8160 Color: 1
Size: 1084 Color: 0

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 11100 Color: 0
Size: 7706 Color: 0
Size: 840 Color: 1

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 11554 Color: 0
Size: 7132 Color: 0
Size: 960 Color: 1

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 11634 Color: 0
Size: 7660 Color: 0
Size: 352 Color: 1

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 1
Size: 6746 Color: 1
Size: 612 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 12986 Color: 0
Size: 6252 Color: 1
Size: 408 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 13028 Color: 0
Size: 5978 Color: 1
Size: 640 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 13122 Color: 1
Size: 5848 Color: 0
Size: 676 Color: 1

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 13427 Color: 0
Size: 5483 Color: 1
Size: 736 Color: 0

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 13814 Color: 1
Size: 5016 Color: 0
Size: 816 Color: 1

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 0
Size: 4570 Color: 0
Size: 500 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 1
Size: 3536 Color: 0
Size: 1426 Color: 0

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 15442 Color: 1
Size: 4204 Color: 0

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 15682 Color: 1
Size: 3964 Color: 0

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 16430 Color: 1
Size: 1856 Color: 0
Size: 1360 Color: 1

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 16500 Color: 1
Size: 3146 Color: 0

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 17504 Color: 0
Size: 2142 Color: 1

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 11081 Color: 1
Size: 8180 Color: 0
Size: 384 Color: 1

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 13069 Color: 1
Size: 6000 Color: 1
Size: 576 Color: 0

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 1
Size: 5185 Color: 0
Size: 820 Color: 1

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 15005 Color: 0
Size: 4640 Color: 1

Bin 66: 4 of cap free
Amount of items: 9
Items: 
Size: 9832 Color: 1
Size: 1424 Color: 1
Size: 1376 Color: 1
Size: 1376 Color: 1
Size: 1340 Color: 0
Size: 1324 Color: 0
Size: 1324 Color: 0
Size: 1248 Color: 0
Size: 400 Color: 1

Bin 67: 4 of cap free
Amount of items: 5
Items: 
Size: 9840 Color: 1
Size: 3484 Color: 0
Size: 3076 Color: 1
Size: 1704 Color: 1
Size: 1540 Color: 0

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 11602 Color: 1
Size: 7722 Color: 0
Size: 320 Color: 1

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 0
Size: 6760 Color: 1
Size: 536 Color: 0

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 12546 Color: 1
Size: 6682 Color: 0
Size: 416 Color: 1

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 1
Size: 4140 Color: 0
Size: 2412 Color: 0

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 15730 Color: 1
Size: 3914 Color: 0

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 15800 Color: 0
Size: 3524 Color: 0
Size: 320 Color: 1

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 15964 Color: 1
Size: 3680 Color: 0

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 17076 Color: 1
Size: 2568 Color: 0

Bin 76: 4 of cap free
Amount of items: 4
Items: 
Size: 17494 Color: 0
Size: 1846 Color: 1
Size: 256 Color: 0
Size: 48 Color: 1

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 17536 Color: 0
Size: 2108 Color: 1

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 11649 Color: 0
Size: 7666 Color: 1
Size: 328 Color: 1

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 0
Size: 6667 Color: 1
Size: 384 Color: 0

Bin 80: 5 of cap free
Amount of items: 3
Items: 
Size: 14313 Color: 0
Size: 4114 Color: 0
Size: 1216 Color: 1

Bin 81: 6 of cap free
Amount of items: 3
Items: 
Size: 11544 Color: 0
Size: 6706 Color: 0
Size: 1392 Color: 1

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 13058 Color: 1
Size: 5768 Color: 1
Size: 816 Color: 0

Bin 83: 6 of cap free
Amount of items: 3
Items: 
Size: 13568 Color: 1
Size: 5554 Color: 0
Size: 520 Color: 0

Bin 84: 6 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 1
Size: 5922 Color: 0

Bin 85: 6 of cap free
Amount of items: 3
Items: 
Size: 15264 Color: 1
Size: 4154 Color: 0
Size: 224 Color: 1

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 16730 Color: 1
Size: 2912 Color: 0

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 17370 Color: 0
Size: 2272 Color: 1

Bin 88: 7 of cap free
Amount of items: 3
Items: 
Size: 11041 Color: 0
Size: 8040 Color: 1
Size: 560 Color: 0

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 12648 Color: 0
Size: 6992 Color: 1

Bin 90: 8 of cap free
Amount of items: 3
Items: 
Size: 14760 Color: 0
Size: 4656 Color: 1
Size: 224 Color: 1

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 15018 Color: 0
Size: 4622 Color: 1

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 15440 Color: 0
Size: 4200 Color: 1

Bin 93: 8 of cap free
Amount of items: 3
Items: 
Size: 15874 Color: 0
Size: 3506 Color: 1
Size: 260 Color: 1

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 17028 Color: 1
Size: 2612 Color: 0

Bin 95: 8 of cap free
Amount of items: 3
Items: 
Size: 17264 Color: 0
Size: 2248 Color: 1
Size: 128 Color: 1

Bin 96: 8 of cap free
Amount of items: 3
Items: 
Size: 17608 Color: 0
Size: 1904 Color: 1
Size: 128 Color: 0

Bin 97: 10 of cap free
Amount of items: 28
Items: 
Size: 888 Color: 1
Size: 888 Color: 1
Size: 828 Color: 1
Size: 824 Color: 1
Size: 792 Color: 1
Size: 790 Color: 1
Size: 784 Color: 1
Size: 772 Color: 1
Size: 768 Color: 1
Size: 752 Color: 1
Size: 720 Color: 1
Size: 704 Color: 0
Size: 704 Color: 0
Size: 704 Color: 0
Size: 700 Color: 0
Size: 672 Color: 0
Size: 652 Color: 0
Size: 640 Color: 1
Size: 640 Color: 1
Size: 632 Color: 0
Size: 616 Color: 0
Size: 616 Color: 0
Size: 608 Color: 0
Size: 608 Color: 0
Size: 608 Color: 0
Size: 592 Color: 0
Size: 592 Color: 0
Size: 544 Color: 1

Bin 98: 10 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 0
Size: 7654 Color: 1
Size: 1072 Color: 0

Bin 99: 10 of cap free
Amount of items: 3
Items: 
Size: 13862 Color: 0
Size: 5388 Color: 1
Size: 388 Color: 1

Bin 100: 10 of cap free
Amount of items: 2
Items: 
Size: 14166 Color: 0
Size: 5472 Color: 1

Bin 101: 10 of cap free
Amount of items: 2
Items: 
Size: 14816 Color: 1
Size: 4822 Color: 0

Bin 102: 10 of cap free
Amount of items: 2
Items: 
Size: 16956 Color: 0
Size: 2682 Color: 1

Bin 103: 11 of cap free
Amount of items: 2
Items: 
Size: 14940 Color: 0
Size: 4697 Color: 1

Bin 104: 12 of cap free
Amount of items: 4
Items: 
Size: 10258 Color: 1
Size: 6642 Color: 0
Size: 2240 Color: 1
Size: 496 Color: 0

Bin 105: 12 of cap free
Amount of items: 2
Items: 
Size: 11448 Color: 1
Size: 8188 Color: 0

Bin 106: 12 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 1
Size: 6084 Color: 0

Bin 107: 12 of cap free
Amount of items: 2
Items: 
Size: 16516 Color: 0
Size: 3120 Color: 1

Bin 108: 12 of cap free
Amount of items: 2
Items: 
Size: 17096 Color: 0
Size: 2540 Color: 1

Bin 109: 12 of cap free
Amount of items: 3
Items: 
Size: 17380 Color: 1
Size: 2192 Color: 0
Size: 64 Color: 1

Bin 110: 12 of cap free
Amount of items: 2
Items: 
Size: 17392 Color: 1
Size: 2244 Color: 0

Bin 111: 12 of cap free
Amount of items: 2
Items: 
Size: 17428 Color: 0
Size: 2208 Color: 1

Bin 112: 14 of cap free
Amount of items: 3
Items: 
Size: 10466 Color: 1
Size: 8144 Color: 0
Size: 1024 Color: 1

Bin 113: 14 of cap free
Amount of items: 2
Items: 
Size: 15394 Color: 1
Size: 4240 Color: 0

Bin 114: 14 of cap free
Amount of items: 2
Items: 
Size: 16082 Color: 0
Size: 3552 Color: 1

Bin 115: 14 of cap free
Amount of items: 2
Items: 
Size: 16368 Color: 1
Size: 3266 Color: 0

Bin 116: 14 of cap free
Amount of items: 2
Items: 
Size: 17336 Color: 1
Size: 2298 Color: 0

Bin 117: 14 of cap free
Amount of items: 2
Items: 
Size: 17584 Color: 0
Size: 2050 Color: 1

Bin 118: 16 of cap free
Amount of items: 2
Items: 
Size: 13728 Color: 0
Size: 5904 Color: 1

Bin 119: 16 of cap free
Amount of items: 2
Items: 
Size: 17352 Color: 1
Size: 2280 Color: 0

Bin 120: 18 of cap free
Amount of items: 8
Items: 
Size: 9834 Color: 1
Size: 1564 Color: 1
Size: 1548 Color: 1
Size: 1540 Color: 1
Size: 1356 Color: 0
Size: 1348 Color: 0
Size: 1344 Color: 0
Size: 1096 Color: 0

Bin 121: 18 of cap free
Amount of items: 2
Items: 
Size: 16894 Color: 0
Size: 2736 Color: 1

Bin 122: 18 of cap free
Amount of items: 2
Items: 
Size: 17534 Color: 1
Size: 2096 Color: 0

Bin 123: 18 of cap free
Amount of items: 2
Items: 
Size: 17558 Color: 0
Size: 2072 Color: 1

Bin 124: 19 of cap free
Amount of items: 13
Items: 
Size: 9827 Color: 1
Size: 1152 Color: 1
Size: 1108 Color: 1
Size: 952 Color: 1
Size: 936 Color: 1
Size: 832 Color: 0
Size: 832 Color: 0
Size: 804 Color: 0
Size: 802 Color: 0
Size: 776 Color: 0
Size: 768 Color: 0
Size: 520 Color: 0
Size: 320 Color: 1

Bin 125: 20 of cap free
Amount of items: 11
Items: 
Size: 9828 Color: 1
Size: 1184 Color: 1
Size: 1180 Color: 1
Size: 1168 Color: 1
Size: 1152 Color: 1
Size: 976 Color: 0
Size: 972 Color: 0
Size: 936 Color: 0
Size: 912 Color: 0
Size: 896 Color: 0
Size: 424 Color: 1

Bin 126: 20 of cap free
Amount of items: 2
Items: 
Size: 13756 Color: 1
Size: 5872 Color: 0

Bin 127: 20 of cap free
Amount of items: 3
Items: 
Size: 14013 Color: 0
Size: 3871 Color: 1
Size: 1744 Color: 1

Bin 128: 20 of cap free
Amount of items: 2
Items: 
Size: 15820 Color: 1
Size: 3808 Color: 0

Bin 129: 21 of cap free
Amount of items: 2
Items: 
Size: 12631 Color: 1
Size: 6996 Color: 0

Bin 130: 22 of cap free
Amount of items: 2
Items: 
Size: 14102 Color: 0
Size: 5524 Color: 1

Bin 131: 22 of cap free
Amount of items: 2
Items: 
Size: 14666 Color: 0
Size: 4960 Color: 1

Bin 132: 22 of cap free
Amount of items: 3
Items: 
Size: 17306 Color: 1
Size: 2176 Color: 0
Size: 144 Color: 0

Bin 133: 23 of cap free
Amount of items: 2
Items: 
Size: 14901 Color: 0
Size: 4724 Color: 1

Bin 134: 24 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 0
Size: 8176 Color: 1
Size: 356 Color: 1

Bin 135: 24 of cap free
Amount of items: 2
Items: 
Size: 17670 Color: 1
Size: 1954 Color: 0

Bin 136: 25 of cap free
Amount of items: 2
Items: 
Size: 14751 Color: 1
Size: 4872 Color: 0

Bin 137: 26 of cap free
Amount of items: 2
Items: 
Size: 16864 Color: 1
Size: 2758 Color: 0

Bin 138: 28 of cap free
Amount of items: 2
Items: 
Size: 15696 Color: 1
Size: 3924 Color: 0

Bin 139: 28 of cap free
Amount of items: 2
Items: 
Size: 16756 Color: 0
Size: 2864 Color: 1

Bin 140: 30 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 0
Size: 5802 Color: 1

Bin 141: 32 of cap free
Amount of items: 2
Items: 
Size: 14664 Color: 0
Size: 4952 Color: 1

Bin 142: 32 of cap free
Amount of items: 2
Items: 
Size: 14892 Color: 0
Size: 4724 Color: 1

Bin 143: 34 of cap free
Amount of items: 2
Items: 
Size: 14810 Color: 1
Size: 4804 Color: 0

Bin 144: 36 of cap free
Amount of items: 2
Items: 
Size: 17124 Color: 0
Size: 2488 Color: 1

Bin 145: 38 of cap free
Amount of items: 2
Items: 
Size: 15392 Color: 0
Size: 4218 Color: 1

Bin 146: 40 of cap free
Amount of items: 4
Items: 
Size: 10008 Color: 1
Size: 6624 Color: 1
Size: 1696 Color: 0
Size: 1280 Color: 0

Bin 147: 40 of cap free
Amount of items: 3
Items: 
Size: 10450 Color: 0
Size: 8902 Color: 1
Size: 256 Color: 1

Bin 148: 42 of cap free
Amount of items: 2
Items: 
Size: 14829 Color: 0
Size: 4777 Color: 1

Bin 149: 43 of cap free
Amount of items: 2
Items: 
Size: 12464 Color: 1
Size: 7141 Color: 0

Bin 150: 44 of cap free
Amount of items: 2
Items: 
Size: 15916 Color: 1
Size: 3688 Color: 0

Bin 151: 47 of cap free
Amount of items: 2
Items: 
Size: 15520 Color: 0
Size: 4081 Color: 1

Bin 152: 48 of cap free
Amount of items: 2
Items: 
Size: 16288 Color: 0
Size: 3312 Color: 1

Bin 153: 52 of cap free
Amount of items: 2
Items: 
Size: 16548 Color: 1
Size: 3048 Color: 0

Bin 154: 53 of cap free
Amount of items: 2
Items: 
Size: 11408 Color: 1
Size: 8187 Color: 0

Bin 155: 60 of cap free
Amount of items: 2
Items: 
Size: 15420 Color: 1
Size: 4168 Color: 0

Bin 156: 62 of cap free
Amount of items: 2
Items: 
Size: 12690 Color: 0
Size: 6896 Color: 1

Bin 157: 64 of cap free
Amount of items: 2
Items: 
Size: 16680 Color: 1
Size: 2904 Color: 0

Bin 158: 65 of cap free
Amount of items: 2
Items: 
Size: 15136 Color: 1
Size: 4447 Color: 0

Bin 159: 72 of cap free
Amount of items: 2
Items: 
Size: 14714 Color: 1
Size: 4862 Color: 0

Bin 160: 76 of cap free
Amount of items: 2
Items: 
Size: 16928 Color: 0
Size: 2644 Color: 1

Bin 161: 78 of cap free
Amount of items: 3
Items: 
Size: 16930 Color: 1
Size: 2448 Color: 0
Size: 192 Color: 0

Bin 162: 80 of cap free
Amount of items: 2
Items: 
Size: 12728 Color: 1
Size: 6840 Color: 0

Bin 163: 80 of cap free
Amount of items: 2
Items: 
Size: 16584 Color: 0
Size: 2984 Color: 1

Bin 164: 80 of cap free
Amount of items: 2
Items: 
Size: 16720 Color: 0
Size: 2848 Color: 1

Bin 165: 86 of cap free
Amount of items: 2
Items: 
Size: 12594 Color: 0
Size: 6968 Color: 1

Bin 166: 88 of cap free
Amount of items: 2
Items: 
Size: 9888 Color: 1
Size: 9672 Color: 0

Bin 167: 92 of cap free
Amount of items: 2
Items: 
Size: 15948 Color: 0
Size: 3608 Color: 1

Bin 168: 94 of cap free
Amount of items: 2
Items: 
Size: 16008 Color: 1
Size: 3546 Color: 0

Bin 169: 96 of cap free
Amount of items: 3
Items: 
Size: 17082 Color: 0
Size: 2406 Color: 1
Size: 64 Color: 1

Bin 170: 104 of cap free
Amount of items: 2
Items: 
Size: 15120 Color: 1
Size: 4424 Color: 0

Bin 171: 110 of cap free
Amount of items: 2
Items: 
Size: 16342 Color: 0
Size: 3196 Color: 1

Bin 172: 116 of cap free
Amount of items: 3
Items: 
Size: 16224 Color: 0
Size: 3116 Color: 1
Size: 192 Color: 0

Bin 173: 118 of cap free
Amount of items: 2
Items: 
Size: 11744 Color: 0
Size: 7786 Color: 1

Bin 174: 118 of cap free
Amount of items: 2
Items: 
Size: 13152 Color: 1
Size: 6378 Color: 0

Bin 175: 122 of cap free
Amount of items: 2
Items: 
Size: 17008 Color: 1
Size: 2518 Color: 0

Bin 176: 124 of cap free
Amount of items: 2
Items: 
Size: 11698 Color: 1
Size: 7826 Color: 0

Bin 177: 128 of cap free
Amount of items: 2
Items: 
Size: 16496 Color: 1
Size: 3024 Color: 0

Bin 178: 129 of cap free
Amount of items: 3
Items: 
Size: 10386 Color: 1
Size: 8169 Color: 0
Size: 964 Color: 1

Bin 179: 133 of cap free
Amount of items: 2
Items: 
Size: 14020 Color: 0
Size: 5495 Color: 1

Bin 180: 136 of cap free
Amount of items: 2
Items: 
Size: 9904 Color: 0
Size: 9608 Color: 1

Bin 181: 138 of cap free
Amount of items: 2
Items: 
Size: 15476 Color: 0
Size: 4034 Color: 1

Bin 182: 142 of cap free
Amount of items: 2
Items: 
Size: 15920 Color: 0
Size: 3586 Color: 1

Bin 183: 142 of cap free
Amount of items: 2
Items: 
Size: 17000 Color: 1
Size: 2506 Color: 0

Bin 184: 144 of cap free
Amount of items: 2
Items: 
Size: 13456 Color: 0
Size: 6048 Color: 1

Bin 185: 157 of cap free
Amount of items: 2
Items: 
Size: 15474 Color: 0
Size: 4017 Color: 1

Bin 186: 158 of cap free
Amount of items: 2
Items: 
Size: 11304 Color: 0
Size: 8186 Color: 1

Bin 187: 158 of cap free
Amount of items: 2
Items: 
Size: 11682 Color: 1
Size: 7808 Color: 0

Bin 188: 160 of cap free
Amount of items: 2
Items: 
Size: 13188 Color: 0
Size: 6300 Color: 1

Bin 189: 172 of cap free
Amount of items: 2
Items: 
Size: 12148 Color: 1
Size: 7328 Color: 0

Bin 190: 172 of cap free
Amount of items: 2
Items: 
Size: 16268 Color: 1
Size: 3208 Color: 0

Bin 191: 176 of cap free
Amount of items: 2
Items: 
Size: 14780 Color: 0
Size: 4692 Color: 1

Bin 192: 183 of cap free
Amount of items: 2
Items: 
Size: 11280 Color: 0
Size: 8185 Color: 1

Bin 193: 186 of cap free
Amount of items: 2
Items: 
Size: 16642 Color: 1
Size: 2820 Color: 0

Bin 194: 194 of cap free
Amount of items: 4
Items: 
Size: 9880 Color: 0
Size: 3862 Color: 0
Size: 3488 Color: 1
Size: 2224 Color: 1

Bin 195: 204 of cap free
Amount of items: 2
Items: 
Size: 11260 Color: 1
Size: 8184 Color: 0

Bin 196: 231 of cap free
Amount of items: 9
Items: 
Size: 9825 Color: 0
Size: 1344 Color: 1
Size: 1332 Color: 1
Size: 1332 Color: 1
Size: 1320 Color: 1
Size: 1152 Color: 0
Size: 1096 Color: 0
Size: 1024 Color: 0
Size: 992 Color: 0

Bin 197: 252 of cap free
Amount of items: 2
Items: 
Size: 11650 Color: 1
Size: 7746 Color: 0

Bin 198: 252 of cap free
Amount of items: 2
Items: 
Size: 11714 Color: 0
Size: 7682 Color: 1

Bin 199: 12356 of cap free
Amount of items: 16
Items: 
Size: 544 Color: 0
Size: 512 Color: 0
Size: 512 Color: 0
Size: 512 Color: 0
Size: 480 Color: 0
Size: 480 Color: 0
Size: 464 Color: 1
Size: 464 Color: 1
Size: 456 Color: 1
Size: 448 Color: 1
Size: 448 Color: 1
Size: 424 Color: 1
Size: 416 Color: 0
Size: 384 Color: 1
Size: 384 Color: 1
Size: 364 Color: 0

Total size: 3890304
Total free space: 19648

