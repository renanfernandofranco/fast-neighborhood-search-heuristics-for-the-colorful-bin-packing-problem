Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 0
Size: 259 Color: 0
Size: 250 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 328 Color: 1
Size: 283 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 275 Color: 0
Size: 260 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 330 Color: 1
Size: 268 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 366 Color: 0
Size: 254 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 0
Size: 253 Color: 1
Size: 251 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 328 Color: 0
Size: 289 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 289 Color: 1
Size: 282 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 256 Color: 0
Size: 251 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 280 Color: 0
Size: 276 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 303 Color: 1
Size: 298 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 336 Color: 1
Size: 250 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 316 Color: 0
Size: 295 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 262 Color: 1
Size: 253 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 351 Color: 1
Size: 277 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 316 Color: 1
Size: 303 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 325 Color: 0
Size: 254 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 365 Color: 0
Size: 268 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 252 Color: 1
Size: 250 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 347 Color: 0
Size: 296 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 322 Color: 0
Size: 257 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 335 Color: 0
Size: 259 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 292 Color: 1
Size: 260 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 268 Color: 0
Size: 324 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 317 Color: 1
Size: 251 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 265 Color: 1
Size: 252 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 0
Size: 311 Color: 1
Size: 265 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 338 Color: 1
Size: 396 Color: 1
Size: 266 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 303 Color: 1
Size: 294 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 0
Size: 353 Color: 0
Size: 279 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 313 Color: 0
Size: 260 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 259 Color: 1
Size: 250 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 340 Color: 1
Size: 295 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 0
Size: 311 Color: 0
Size: 296 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 363 Color: 1
Size: 262 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 0
Size: 308 Color: 0
Size: 259 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 353 Color: 1
Size: 255 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 0
Size: 329 Color: 1
Size: 308 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 331 Color: 1
Size: 271 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 363 Color: 0
Size: 265 Color: 1

Total size: 40000
Total free space: 0

