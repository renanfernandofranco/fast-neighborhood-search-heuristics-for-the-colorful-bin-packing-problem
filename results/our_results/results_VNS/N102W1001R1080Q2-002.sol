Capicity Bin: 1001
Lower Bound: 49

Bins used: 55
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 0
Size: 442 Color: 1

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 1
Size: 467 Color: 0

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 0
Size: 305 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 754 Color: 0
Size: 131 Color: 1
Size: 116 Color: 0

Bin 5: 1 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 1
Size: 381 Color: 0

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 1
Size: 262 Color: 0

Bin 7: 2 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 0
Size: 457 Color: 1

Bin 8: 2 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 0
Size: 497 Color: 1

Bin 9: 2 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 254 Color: 1

Bin 10: 3 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 290 Color: 1

Bin 11: 3 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 1
Size: 136 Color: 0
Size: 130 Color: 1

Bin 12: 3 of cap free
Amount of items: 3
Items: 
Size: 756 Color: 1
Size: 134 Color: 1
Size: 108 Color: 0

Bin 13: 4 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 0
Size: 442 Color: 1

Bin 14: 6 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 216 Color: 0

Bin 15: 7 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 0
Size: 322 Color: 1

Bin 16: 7 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 205 Color: 0

Bin 17: 8 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 0
Size: 441 Color: 1

Bin 18: 8 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 1
Size: 455 Color: 0

Bin 19: 8 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 0
Size: 364 Color: 1

Bin 20: 8 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 1
Size: 149 Color: 1
Size: 141 Color: 0

Bin 21: 9 of cap free
Amount of items: 3
Items: 
Size: 770 Color: 1
Size: 120 Color: 0
Size: 102 Color: 1

Bin 22: 10 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 1
Size: 250 Color: 0

Bin 23: 12 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 1
Size: 409 Color: 0

Bin 24: 15 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 0
Size: 347 Color: 1

Bin 25: 15 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 0
Size: 320 Color: 1

Bin 26: 15 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 1
Size: 287 Color: 0

Bin 27: 16 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 1
Size: 435 Color: 0

Bin 28: 17 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 234 Color: 1

Bin 29: 20 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 1
Size: 331 Color: 0

Bin 30: 23 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 471 Color: 0

Bin 31: 25 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 1
Size: 366 Color: 0

Bin 32: 29 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 1
Size: 328 Color: 0

Bin 33: 33 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 398 Color: 1

Bin 34: 33 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 0
Size: 195 Color: 1

Bin 35: 34 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 0
Size: 284 Color: 1

Bin 36: 47 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 312 Color: 0

Bin 37: 47 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 0
Size: 184 Color: 1

Bin 38: 56 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 189 Color: 0

Bin 39: 61 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 1
Size: 185 Color: 0

Bin 40: 90 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 1
Size: 174 Color: 0

Bin 41: 106 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 0
Size: 169 Color: 1

Bin 42: 125 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 1
Size: 159 Color: 0

Bin 43: 309 of cap free
Amount of items: 1
Items: 
Size: 692 Color: 1

Bin 44: 337 of cap free
Amount of items: 1
Items: 
Size: 664 Color: 0

Bin 45: 365 of cap free
Amount of items: 1
Items: 
Size: 636 Color: 1

Bin 46: 403 of cap free
Amount of items: 1
Items: 
Size: 598 Color: 1

Bin 47: 422 of cap free
Amount of items: 1
Items: 
Size: 579 Color: 1

Bin 48: 432 of cap free
Amount of items: 1
Items: 
Size: 569 Color: 0

Bin 49: 438 of cap free
Amount of items: 1
Items: 
Size: 563 Color: 0

Bin 50: 438 of cap free
Amount of items: 1
Items: 
Size: 563 Color: 0

Bin 51: 468 of cap free
Amount of items: 1
Items: 
Size: 533 Color: 1

Bin 52: 516 of cap free
Amount of items: 1
Items: 
Size: 485 Color: 1

Bin 53: 518 of cap free
Amount of items: 1
Items: 
Size: 483 Color: 1

Bin 54: 521 of cap free
Amount of items: 1
Items: 
Size: 480 Color: 1

Bin 55: 548 of cap free
Amount of items: 1
Items: 
Size: 453 Color: 1

Total size: 48429
Total free space: 6626

