Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 259 Color: 1
Size: 252 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 0
Size: 250 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 290 Color: 0
Size: 278 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 347 Color: 0
Size: 281 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 264 Color: 1
Size: 252 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 265 Color: 0
Size: 252 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 302 Color: 1
Size: 282 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 358 Color: 0
Size: 281 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 335 Color: 0
Size: 285 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 0
Size: 360 Color: 1
Size: 252 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 352 Color: 0
Size: 268 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 276 Color: 0
Size: 262 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 286 Color: 0
Size: 281 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 296 Color: 1
Size: 282 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 327 Color: 0
Size: 301 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 340 Color: 0
Size: 305 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 328 Color: 1
Size: 283 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 334 Color: 0
Size: 270 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 276 Color: 0
Size: 261 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 0
Size: 269 Color: 1
Size: 262 Color: 1

Total size: 20000
Total free space: 0

