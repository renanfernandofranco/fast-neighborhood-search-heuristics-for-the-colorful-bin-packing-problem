Capicity Bin: 7888
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4510 Color: 289
Size: 2822 Color: 251
Size: 208 Color: 47
Size: 176 Color: 33
Size: 172 Color: 32

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5923 Color: 326
Size: 1761 Color: 221
Size: 204 Color: 45

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 331
Size: 1646 Color: 217
Size: 160 Color: 25

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6130 Color: 334
Size: 1102 Color: 175
Size: 656 Color: 135

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6164 Color: 335
Size: 1068 Color: 173
Size: 656 Color: 136

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6170 Color: 336
Size: 1348 Color: 197
Size: 370 Color: 94

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6250 Color: 340
Size: 884 Color: 158
Size: 754 Color: 146

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 343
Size: 1204 Color: 186
Size: 392 Color: 100

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 352
Size: 974 Color: 164
Size: 488 Color: 115

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 358
Size: 724 Color: 144
Size: 616 Color: 128

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 359
Size: 1106 Color: 176
Size: 228 Color: 56

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 362
Size: 1245 Color: 192
Size: 76 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6570 Color: 363
Size: 1054 Color: 171
Size: 264 Color: 67

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6607 Color: 365
Size: 1069 Color: 174
Size: 212 Color: 48

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 368
Size: 989 Color: 166
Size: 244 Color: 63

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 369
Size: 852 Color: 153
Size: 376 Color: 96

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 380
Size: 988 Color: 165
Size: 104 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6836 Color: 381
Size: 724 Color: 145
Size: 328 Color: 87

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6846 Color: 383
Size: 810 Color: 152
Size: 232 Color: 57

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6918 Color: 387
Size: 700 Color: 140
Size: 270 Color: 70

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 390
Size: 706 Color: 142
Size: 216 Color: 50

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 392
Size: 580 Color: 127
Size: 328 Color: 86

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 393
Size: 770 Color: 148
Size: 132 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 394
Size: 452 Color: 110
Size: 416 Color: 104

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7042 Color: 397
Size: 702 Color: 141
Size: 144 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7050 Color: 398
Size: 512 Color: 118
Size: 326 Color: 85

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7052 Color: 399
Size: 572 Color: 126
Size: 264 Color: 68

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7074 Color: 400
Size: 662 Color: 137
Size: 152 Color: 23

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 402
Size: 564 Color: 124
Size: 226 Color: 55

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 5865 Color: 323
Size: 2022 Color: 229

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 324
Size: 1423 Color: 203
Size: 564 Color: 123

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 5948 Color: 327
Size: 1859 Color: 224
Size: 80 Color: 3

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 5963 Color: 328
Size: 1924 Color: 227

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 6187 Color: 338
Size: 1700 Color: 220

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 6267 Color: 342
Size: 1620 Color: 215

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6299 Color: 344
Size: 1366 Color: 200
Size: 222 Color: 53

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 6555 Color: 360
Size: 1332 Color: 196

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 361
Size: 1325 Color: 195

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 364
Size: 1134 Color: 180
Size: 178 Color: 35

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 373
Size: 1064 Color: 172
Size: 120 Color: 6

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6722 Color: 375
Size: 1165 Color: 183

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6735 Color: 376
Size: 956 Color: 162
Size: 196 Color: 41

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6748 Color: 377
Size: 1139 Color: 181

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6838 Color: 382
Size: 1049 Color: 170

Bin 45: 2 of cap free
Amount of items: 17
Items: 
Size: 640 Color: 131
Size: 624 Color: 130
Size: 624 Color: 129
Size: 564 Color: 125
Size: 552 Color: 122
Size: 552 Color: 121
Size: 522 Color: 119
Size: 512 Color: 117
Size: 496 Color: 116
Size: 476 Color: 113
Size: 472 Color: 112
Size: 468 Color: 111
Size: 342 Color: 90
Size: 272 Color: 71
Size: 270 Color: 69
Size: 252 Color: 66
Size: 248 Color: 65

Bin 46: 2 of cap free
Amount of items: 5
Items: 
Size: 4502 Color: 288
Size: 2804 Color: 250
Size: 220 Color: 52
Size: 184 Color: 36
Size: 176 Color: 34

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 5914 Color: 325
Size: 1972 Color: 228

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 6390 Color: 349
Size: 1496 Color: 209

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 6452 Color: 354
Size: 1434 Color: 204

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6868 Color: 386
Size: 1018 Color: 167

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 7028 Color: 396
Size: 858 Color: 154

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 401
Size: 796 Color: 150

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 6862 Color: 385
Size: 1023 Color: 168

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 298
Size: 2788 Color: 249
Size: 148 Color: 20

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 6085 Color: 332
Size: 1639 Color: 216
Size: 160 Color: 26

Bin 56: 4 of cap free
Amount of items: 2
Items: 
Size: 6968 Color: 391
Size: 916 Color: 159

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 7022 Color: 395
Size: 862 Color: 155

Bin 58: 5 of cap free
Amount of items: 7
Items: 
Size: 3948 Color: 275
Size: 1111 Color: 177
Size: 938 Color: 160
Size: 878 Color: 157
Size: 532 Color: 120
Size: 240 Color: 60
Size: 236 Color: 59

Bin 59: 5 of cap free
Amount of items: 2
Items: 
Size: 6439 Color: 353
Size: 1444 Color: 205

Bin 60: 5 of cap free
Amount of items: 2
Items: 
Size: 6661 Color: 370
Size: 1222 Color: 189

Bin 61: 5 of cap free
Amount of items: 2
Items: 
Size: 6674 Color: 372
Size: 1209 Color: 188

Bin 62: 5 of cap free
Amount of items: 2
Items: 
Size: 6708 Color: 374
Size: 1175 Color: 184

Bin 63: 6 of cap free
Amount of items: 7
Items: 
Size: 3946 Color: 274
Size: 870 Color: 156
Size: 806 Color: 151
Size: 776 Color: 149
Size: 764 Color: 147
Size: 480 Color: 114
Size: 240 Color: 61

Bin 64: 6 of cap free
Amount of items: 2
Items: 
Size: 6523 Color: 357
Size: 1359 Color: 199

Bin 65: 6 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 378
Size: 1124 Color: 179

Bin 66: 6 of cap free
Amount of items: 2
Items: 
Size: 6854 Color: 384
Size: 1028 Color: 169

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 6922 Color: 388
Size: 960 Color: 163

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 6940 Color: 389
Size: 942 Color: 161

Bin 69: 7 of cap free
Amount of items: 3
Items: 
Size: 5774 Color: 320
Size: 2059 Color: 231
Size: 48 Color: 0

Bin 70: 8 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 305
Size: 1884 Color: 226
Size: 656 Color: 134

Bin 71: 8 of cap free
Amount of items: 3
Items: 
Size: 5628 Color: 316
Size: 2124 Color: 237
Size: 128 Color: 8

Bin 72: 8 of cap free
Amount of items: 2
Items: 
Size: 5852 Color: 322
Size: 2028 Color: 230

Bin 73: 8 of cap free
Amount of items: 2
Items: 
Size: 6506 Color: 356
Size: 1374 Color: 201

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 6766 Color: 379
Size: 1114 Color: 178

Bin 75: 9 of cap free
Amount of items: 2
Items: 
Size: 6373 Color: 348
Size: 1506 Color: 211

Bin 76: 10 of cap free
Amount of items: 2
Items: 
Size: 6116 Color: 333
Size: 1762 Color: 222

Bin 77: 10 of cap free
Amount of items: 2
Items: 
Size: 6670 Color: 371
Size: 1208 Color: 187

Bin 78: 11 of cap free
Amount of items: 4
Items: 
Size: 5028 Color: 300
Size: 2561 Color: 246
Size: 144 Color: 19
Size: 144 Color: 18

Bin 79: 11 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 312
Size: 2265 Color: 240
Size: 136 Color: 12

Bin 80: 11 of cap free
Amount of items: 2
Items: 
Size: 6340 Color: 346
Size: 1537 Color: 212

Bin 81: 12 of cap free
Amount of items: 3
Items: 
Size: 5337 Color: 304
Size: 1351 Color: 198
Size: 1188 Color: 185

Bin 82: 12 of cap free
Amount of items: 2
Items: 
Size: 6014 Color: 329
Size: 1862 Color: 225

Bin 83: 12 of cap free
Amount of items: 2
Items: 
Size: 6410 Color: 351
Size: 1466 Color: 207

Bin 84: 12 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 367
Size: 1250 Color: 193

Bin 85: 13 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 366
Size: 1263 Color: 194

Bin 86: 14 of cap free
Amount of items: 3
Items: 
Size: 4815 Color: 296
Size: 2907 Color: 255
Size: 152 Color: 22

Bin 87: 14 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 307
Size: 2386 Color: 242
Size: 140 Color: 14

Bin 88: 14 of cap free
Amount of items: 2
Items: 
Size: 5775 Color: 321
Size: 2099 Color: 234

Bin 89: 15 of cap free
Amount of items: 3
Items: 
Size: 4401 Color: 284
Size: 3284 Color: 265
Size: 188 Color: 38

Bin 90: 15 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 302
Size: 2562 Color: 247
Size: 140 Color: 15

Bin 91: 15 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 308
Size: 1235 Color: 191
Size: 1234 Color: 190

Bin 92: 16 of cap free
Amount of items: 2
Items: 
Size: 6407 Color: 350
Size: 1465 Color: 206

Bin 93: 17 of cap free
Amount of items: 2
Items: 
Size: 5419 Color: 309
Size: 2452 Color: 244

Bin 94: 17 of cap free
Amount of items: 2
Items: 
Size: 5524 Color: 313
Size: 2347 Color: 241

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 5346 Color: 306
Size: 2524 Color: 245

Bin 96: 18 of cap free
Amount of items: 2
Items: 
Size: 6210 Color: 339
Size: 1660 Color: 218

Bin 97: 18 of cap free
Amount of items: 2
Items: 
Size: 6468 Color: 355
Size: 1402 Color: 202

Bin 98: 20 of cap free
Amount of items: 2
Items: 
Size: 6181 Color: 337
Size: 1687 Color: 219

Bin 99: 21 of cap free
Amount of items: 3
Items: 
Size: 5611 Color: 315
Size: 2124 Color: 236
Size: 132 Color: 10

Bin 100: 22 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 318
Size: 2127 Color: 238
Size: 80 Color: 4

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 6304 Color: 345
Size: 1562 Color: 213

Bin 102: 23 of cap free
Amount of items: 2
Items: 
Size: 6362 Color: 347
Size: 1503 Color: 210

Bin 103: 24 of cap free
Amount of items: 2
Items: 
Size: 5026 Color: 299
Size: 2838 Color: 253

Bin 104: 24 of cap free
Amount of items: 2
Items: 
Size: 6045 Color: 330
Size: 1819 Color: 223

Bin 105: 24 of cap free
Amount of items: 2
Items: 
Size: 6259 Color: 341
Size: 1605 Color: 214

Bin 106: 27 of cap free
Amount of items: 2
Items: 
Size: 3949 Color: 276
Size: 3912 Color: 272

Bin 107: 27 of cap free
Amount of items: 3
Items: 
Size: 5727 Color: 319
Size: 2076 Color: 233
Size: 58 Color: 1

Bin 108: 28 of cap free
Amount of items: 3
Items: 
Size: 4438 Color: 285
Size: 3238 Color: 261
Size: 184 Color: 37

Bin 109: 33 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 317
Size: 2073 Color: 232
Size: 128 Color: 7

Bin 110: 37 of cap free
Amount of items: 3
Items: 
Size: 4755 Color: 294
Size: 2928 Color: 256
Size: 168 Color: 27

Bin 111: 41 of cap free
Amount of items: 2
Items: 
Size: 5459 Color: 310
Size: 2388 Color: 243

Bin 112: 42 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 297
Size: 2830 Color: 252
Size: 148 Color: 21

Bin 113: 44 of cap free
Amount of items: 3
Items: 
Size: 4548 Color: 291
Size: 3124 Color: 260
Size: 172 Color: 30

Bin 114: 44 of cap free
Amount of items: 3
Items: 
Size: 4814 Color: 295
Size: 2878 Color: 254
Size: 152 Color: 24

Bin 115: 44 of cap free
Amount of items: 3
Items: 
Size: 5466 Color: 311
Size: 2242 Color: 239
Size: 136 Color: 13

Bin 116: 48 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 303
Size: 1484 Color: 208
Size: 1154 Color: 182

Bin 117: 50 of cap free
Amount of items: 3
Items: 
Size: 5580 Color: 314
Size: 2122 Color: 235
Size: 136 Color: 11

Bin 118: 56 of cap free
Amount of items: 3
Items: 
Size: 4695 Color: 293
Size: 2969 Color: 257
Size: 168 Color: 28

Bin 119: 60 of cap free
Amount of items: 3
Items: 
Size: 5073 Color: 301
Size: 2611 Color: 248
Size: 144 Color: 17

Bin 120: 62 of cap free
Amount of items: 3
Items: 
Size: 3974 Color: 280
Size: 3652 Color: 271
Size: 200 Color: 44

Bin 121: 63 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 290
Size: 3121 Color: 259
Size: 172 Color: 31

Bin 122: 69 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 279
Size: 3645 Color: 270
Size: 208 Color: 46

Bin 123: 72 of cap free
Amount of items: 3
Items: 
Size: 4611 Color: 292
Size: 3037 Color: 258
Size: 168 Color: 29

Bin 124: 83 of cap free
Amount of items: 8
Items: 
Size: 3945 Color: 273
Size: 722 Color: 143
Size: 682 Color: 139
Size: 666 Color: 138
Size: 652 Color: 133
Size: 652 Color: 132
Size: 246 Color: 64
Size: 240 Color: 62

Bin 125: 84 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 283
Size: 3464 Color: 269
Size: 192 Color: 39

Bin 126: 104 of cap free
Amount of items: 2
Items: 
Size: 4494 Color: 287
Size: 3290 Color: 267

Bin 127: 116 of cap free
Amount of items: 2
Items: 
Size: 4486 Color: 286
Size: 3286 Color: 266

Bin 128: 138 of cap free
Amount of items: 22
Items: 
Size: 448 Color: 109
Size: 444 Color: 108
Size: 424 Color: 107
Size: 424 Color: 106
Size: 420 Color: 105
Size: 410 Color: 103
Size: 408 Color: 102
Size: 400 Color: 101
Size: 384 Color: 99
Size: 384 Color: 98
Size: 384 Color: 97
Size: 312 Color: 82
Size: 306 Color: 81
Size: 300 Color: 80
Size: 300 Color: 79
Size: 298 Color: 78
Size: 292 Color: 77
Size: 288 Color: 76
Size: 284 Color: 75
Size: 284 Color: 74
Size: 280 Color: 73
Size: 276 Color: 72

Bin 129: 145 of cap free
Amount of items: 4
Items: 
Size: 4060 Color: 281
Size: 3283 Color: 264
Size: 200 Color: 43
Size: 200 Color: 42

Bin 130: 147 of cap free
Amount of items: 3
Items: 
Size: 4143 Color: 282
Size: 3406 Color: 268
Size: 192 Color: 40

Bin 131: 212 of cap free
Amount of items: 4
Items: 
Size: 3950 Color: 277
Size: 3270 Color: 262
Size: 232 Color: 58
Size: 224 Color: 54

Bin 132: 216 of cap free
Amount of items: 4
Items: 
Size: 3958 Color: 278
Size: 3278 Color: 263
Size: 220 Color: 51
Size: 216 Color: 49

Bin 133: 5144 of cap free
Amount of items: 8
Items: 
Size: 372 Color: 95
Size: 356 Color: 93
Size: 352 Color: 92
Size: 352 Color: 91
Size: 336 Color: 89
Size: 336 Color: 88
Size: 320 Color: 84
Size: 320 Color: 83

Total size: 1041216
Total free space: 7888

