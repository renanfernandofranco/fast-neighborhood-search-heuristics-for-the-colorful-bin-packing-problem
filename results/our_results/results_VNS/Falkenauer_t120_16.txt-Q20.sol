Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 2
Size: 321 Color: 3
Size: 251 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 2
Size: 329 Color: 17
Size: 278 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7
Size: 367 Color: 18
Size: 264 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 8
Size: 330 Color: 3
Size: 273 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 14
Size: 337 Color: 11
Size: 259 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 2
Size: 281 Color: 18
Size: 252 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 317 Color: 19
Size: 279 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 16
Size: 355 Color: 0
Size: 284 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 260 Color: 16
Size: 250 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 5
Size: 279 Color: 1
Size: 294 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 15
Size: 365 Color: 17
Size: 267 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 7
Size: 293 Color: 0
Size: 285 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 17
Size: 369 Color: 3
Size: 261 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8
Size: 324 Color: 16
Size: 262 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 19
Size: 310 Color: 9
Size: 256 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 359 Color: 15
Size: 264 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 13
Size: 274 Color: 15
Size: 254 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 7
Size: 285 Color: 17
Size: 272 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 14
Size: 323 Color: 15
Size: 290 Color: 5

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 6
Size: 299 Color: 1
Size: 282 Color: 10

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 10
Size: 298 Color: 3
Size: 265 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 259 Color: 3
Size: 257 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 8
Size: 273 Color: 11
Size: 252 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 19
Size: 319 Color: 16
Size: 250 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 298 Color: 1
Size: 270 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 12
Size: 352 Color: 12
Size: 265 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 19
Size: 303 Color: 5
Size: 255 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 7
Size: 293 Color: 8
Size: 260 Color: 15

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 5
Size: 257 Color: 4
Size: 251 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 3
Size: 262 Color: 8
Size: 253 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 287 Color: 4
Size: 267 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 14
Size: 364 Color: 19
Size: 255 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 11
Size: 358 Color: 4
Size: 265 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 10
Size: 316 Color: 13
Size: 272 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 10
Size: 294 Color: 19
Size: 264 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 5
Size: 285 Color: 9
Size: 254 Color: 11

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 329 Color: 5
Size: 290 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 9
Size: 349 Color: 1
Size: 275 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 3
Size: 337 Color: 5
Size: 260 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 16
Size: 284 Color: 10
Size: 262 Color: 4

Total size: 40000
Total free space: 0

