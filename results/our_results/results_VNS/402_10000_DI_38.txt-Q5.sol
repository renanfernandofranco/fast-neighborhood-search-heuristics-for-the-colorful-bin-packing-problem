Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 13
Items: 
Size: 1001 Color: 0
Size: 1000 Color: 2
Size: 820 Color: 2
Size: 788 Color: 2
Size: 711 Color: 0
Size: 708 Color: 3
Size: 672 Color: 3
Size: 544 Color: 1
Size: 512 Color: 2
Size: 416 Color: 0
Size: 380 Color: 1
Size: 360 Color: 4
Size: 224 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4452 Color: 0
Size: 3356 Color: 3
Size: 328 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4654 Color: 4
Size: 2902 Color: 1
Size: 580 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5324 Color: 0
Size: 2348 Color: 1
Size: 464 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5447 Color: 4
Size: 2501 Color: 1
Size: 188 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5481 Color: 2
Size: 2375 Color: 1
Size: 280 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 1
Size: 1844 Color: 2
Size: 488 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6089 Color: 3
Size: 1547 Color: 2
Size: 500 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 1
Size: 1842 Color: 4
Size: 148 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 2
Size: 1740 Color: 4
Size: 152 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6367 Color: 4
Size: 1387 Color: 0
Size: 382 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 1
Size: 1618 Color: 4
Size: 170 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 0
Size: 1450 Color: 4
Size: 288 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 1
Size: 1492 Color: 4
Size: 144 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 4
Size: 1395 Color: 2
Size: 112 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 4
Size: 1228 Color: 2
Size: 240 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6673 Color: 4
Size: 1221 Color: 3
Size: 242 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6669 Color: 2
Size: 1325 Color: 4
Size: 142 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6841 Color: 2
Size: 1081 Color: 2
Size: 214 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6877 Color: 4
Size: 819 Color: 2
Size: 440 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6924 Color: 1
Size: 1012 Color: 3
Size: 200 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 2
Size: 580 Color: 1
Size: 576 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7030 Color: 1
Size: 946 Color: 2
Size: 160 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7041 Color: 1
Size: 755 Color: 3
Size: 340 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 2
Size: 862 Color: 4
Size: 176 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7106 Color: 2
Size: 672 Color: 4
Size: 358 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 0
Size: 696 Color: 2
Size: 350 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7107 Color: 3
Size: 793 Color: 2
Size: 236 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7155 Color: 2
Size: 771 Color: 0
Size: 210 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7166 Color: 2
Size: 676 Color: 1
Size: 294 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7196 Color: 3
Size: 766 Color: 4
Size: 174 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7197 Color: 1
Size: 805 Color: 2
Size: 134 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7231 Color: 2
Size: 745 Color: 4
Size: 160 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7243 Color: 0
Size: 721 Color: 3
Size: 172 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 1
Size: 676 Color: 4
Size: 208 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 0
Size: 546 Color: 3
Size: 336 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7271 Color: 3
Size: 681 Color: 0
Size: 184 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7294 Color: 3
Size: 676 Color: 1
Size: 166 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7310 Color: 0
Size: 484 Color: 3
Size: 342 Color: 2

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 4597 Color: 4
Size: 3378 Color: 0
Size: 160 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5674 Color: 3
Size: 2241 Color: 1
Size: 220 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6100 Color: 0
Size: 1707 Color: 1
Size: 328 Color: 2

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6190 Color: 3
Size: 1753 Color: 1
Size: 192 Color: 2

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6281 Color: 4
Size: 1622 Color: 4
Size: 232 Color: 3

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6473 Color: 0
Size: 1662 Color: 1

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 2
Size: 1314 Color: 2
Size: 314 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6547 Color: 2
Size: 1364 Color: 4
Size: 224 Color: 3

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6600 Color: 4
Size: 871 Color: 3
Size: 664 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6733 Color: 2
Size: 866 Color: 4
Size: 536 Color: 3

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 0
Size: 1168 Color: 4
Size: 150 Color: 2

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6845 Color: 3
Size: 702 Color: 4
Size: 588 Color: 2

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6914 Color: 4
Size: 1077 Color: 2
Size: 144 Color: 3

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 7139 Color: 0
Size: 836 Color: 2
Size: 160 Color: 4

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 7283 Color: 2
Size: 690 Color: 0
Size: 162 Color: 4

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 7321 Color: 0
Size: 544 Color: 3
Size: 270 Color: 4

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 5044 Color: 3
Size: 2898 Color: 2
Size: 192 Color: 0

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 2
Size: 2314 Color: 1
Size: 218 Color: 4

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5637 Color: 1
Size: 1638 Color: 3
Size: 859 Color: 0

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5926 Color: 4
Size: 1948 Color: 1
Size: 260 Color: 4

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 6073 Color: 2
Size: 1581 Color: 2
Size: 480 Color: 1

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 2
Size: 1572 Color: 1

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 7010 Color: 3
Size: 942 Color: 2
Size: 182 Color: 0

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 5137 Color: 3
Size: 2268 Color: 0
Size: 728 Color: 2

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 6692 Color: 3
Size: 1441 Color: 1

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 6791 Color: 2
Size: 1252 Color: 0
Size: 90 Color: 3

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 7131 Color: 4
Size: 1002 Color: 3

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 7211 Color: 1
Size: 922 Color: 4

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 7278 Color: 4
Size: 783 Color: 1
Size: 72 Color: 3

Bin 69: 3 of cap free
Amount of items: 4
Items: 
Size: 7280 Color: 4
Size: 785 Color: 0
Size: 40 Color: 1
Size: 28 Color: 3

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 6544 Color: 4
Size: 1148 Color: 3
Size: 440 Color: 2

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 6934 Color: 0
Size: 1198 Color: 4

Bin 72: 5 of cap free
Amount of items: 4
Items: 
Size: 4076 Color: 2
Size: 2685 Color: 1
Size: 1082 Color: 0
Size: 288 Color: 3

Bin 73: 5 of cap free
Amount of items: 3
Items: 
Size: 6702 Color: 1
Size: 1121 Color: 4
Size: 308 Color: 1

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 6953 Color: 4
Size: 1178 Color: 3

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 7218 Color: 4
Size: 913 Color: 3

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 7292 Color: 2
Size: 839 Color: 4

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 5847 Color: 4
Size: 2083 Color: 1
Size: 200 Color: 3

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 5894 Color: 4
Size: 2236 Color: 3

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 6407 Color: 1
Size: 892 Color: 1
Size: 831 Color: 4

Bin 80: 7 of cap free
Amount of items: 3
Items: 
Size: 5226 Color: 2
Size: 2165 Color: 4
Size: 738 Color: 0

Bin 81: 7 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 1
Size: 1061 Color: 3

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 7182 Color: 4
Size: 947 Color: 3

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 4368 Color: 2
Size: 3528 Color: 4
Size: 232 Color: 4

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 5177 Color: 2
Size: 2951 Color: 4

Bin 85: 9 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 2
Size: 1401 Color: 0

Bin 86: 9 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 4
Size: 987 Color: 0

Bin 87: 10 of cap free
Amount of items: 5
Items: 
Size: 4069 Color: 3
Size: 2054 Color: 2
Size: 1101 Color: 4
Size: 718 Color: 1
Size: 184 Color: 2

Bin 88: 10 of cap free
Amount of items: 4
Items: 
Size: 4078 Color: 4
Size: 2790 Color: 3
Size: 1102 Color: 0
Size: 156 Color: 4

Bin 89: 10 of cap free
Amount of items: 2
Items: 
Size: 6838 Color: 2
Size: 1288 Color: 3

Bin 90: 12 of cap free
Amount of items: 3
Items: 
Size: 4692 Color: 1
Size: 3064 Color: 4
Size: 368 Color: 0

Bin 91: 12 of cap free
Amount of items: 3
Items: 
Size: 4915 Color: 0
Size: 3033 Color: 4
Size: 176 Color: 1

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 6892 Color: 0
Size: 1232 Color: 1

Bin 93: 13 of cap free
Amount of items: 4
Items: 
Size: 4071 Color: 4
Size: 3382 Color: 2
Size: 498 Color: 0
Size: 172 Color: 3

Bin 94: 13 of cap free
Amount of items: 2
Items: 
Size: 5839 Color: 4
Size: 2284 Color: 0

Bin 95: 13 of cap free
Amount of items: 2
Items: 
Size: 6764 Color: 4
Size: 1359 Color: 1

Bin 96: 13 of cap free
Amount of items: 2
Items: 
Size: 7249 Color: 3
Size: 874 Color: 1

Bin 97: 14 of cap free
Amount of items: 29
Items: 
Size: 492 Color: 3
Size: 456 Color: 3
Size: 424 Color: 3
Size: 408 Color: 3
Size: 384 Color: 2
Size: 372 Color: 3
Size: 336 Color: 0
Size: 324 Color: 3
Size: 324 Color: 1
Size: 320 Color: 1
Size: 296 Color: 2
Size: 288 Color: 1
Size: 276 Color: 4
Size: 272 Color: 2
Size: 264 Color: 3
Size: 256 Color: 4
Size: 250 Color: 0
Size: 248 Color: 4
Size: 248 Color: 0
Size: 216 Color: 4
Size: 216 Color: 1
Size: 216 Color: 1
Size: 214 Color: 4
Size: 200 Color: 1
Size: 198 Color: 4
Size: 168 Color: 0
Size: 166 Color: 0
Size: 154 Color: 0
Size: 136 Color: 4

Bin 98: 14 of cap free
Amount of items: 2
Items: 
Size: 5708 Color: 2
Size: 2414 Color: 4

Bin 99: 14 of cap free
Amount of items: 2
Items: 
Size: 6578 Color: 2
Size: 1544 Color: 0

Bin 100: 14 of cap free
Amount of items: 2
Items: 
Size: 6865 Color: 1
Size: 1257 Color: 0

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 5396 Color: 4
Size: 2724 Color: 1

Bin 102: 16 of cap free
Amount of items: 2
Items: 
Size: 6645 Color: 2
Size: 1475 Color: 1

Bin 103: 16 of cap free
Amount of items: 2
Items: 
Size: 6818 Color: 1
Size: 1302 Color: 0

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 7156 Color: 4
Size: 964 Color: 3

Bin 105: 21 of cap free
Amount of items: 2
Items: 
Size: 6394 Color: 3
Size: 1721 Color: 0

Bin 106: 23 of cap free
Amount of items: 2
Items: 
Size: 6198 Color: 3
Size: 1915 Color: 2

Bin 107: 23 of cap free
Amount of items: 2
Items: 
Size: 7091 Color: 0
Size: 1022 Color: 1

Bin 108: 23 of cap free
Amount of items: 2
Items: 
Size: 7315 Color: 3
Size: 798 Color: 0

Bin 109: 24 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 2
Size: 1454 Color: 0
Size: 102 Color: 1

Bin 110: 25 of cap free
Amount of items: 2
Items: 
Size: 6241 Color: 4
Size: 1870 Color: 3

Bin 111: 27 of cap free
Amount of items: 3
Items: 
Size: 4557 Color: 0
Size: 2876 Color: 3
Size: 676 Color: 3

Bin 112: 27 of cap free
Amount of items: 2
Items: 
Size: 7001 Color: 4
Size: 1108 Color: 3

Bin 113: 28 of cap free
Amount of items: 2
Items: 
Size: 6937 Color: 4
Size: 1171 Color: 3

Bin 114: 28 of cap free
Amount of items: 2
Items: 
Size: 7057 Color: 3
Size: 1051 Color: 0

Bin 115: 36 of cap free
Amount of items: 3
Items: 
Size: 5130 Color: 3
Size: 2522 Color: 1
Size: 448 Color: 0

Bin 116: 38 of cap free
Amount of items: 3
Items: 
Size: 7171 Color: 1
Size: 903 Color: 0
Size: 24 Color: 4

Bin 117: 39 of cap free
Amount of items: 3
Items: 
Size: 6033 Color: 0
Size: 1324 Color: 4
Size: 740 Color: 4

Bin 118: 44 of cap free
Amount of items: 2
Items: 
Size: 4868 Color: 3
Size: 3224 Color: 2

Bin 119: 48 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 1
Size: 1700 Color: 2
Size: 72 Color: 0

Bin 120: 51 of cap free
Amount of items: 3
Items: 
Size: 4086 Color: 1
Size: 3391 Color: 2
Size: 608 Color: 1

Bin 121: 53 of cap free
Amount of items: 2
Items: 
Size: 6174 Color: 3
Size: 1909 Color: 0

Bin 122: 56 of cap free
Amount of items: 2
Items: 
Size: 5613 Color: 4
Size: 2467 Color: 2

Bin 123: 58 of cap free
Amount of items: 2
Items: 
Size: 4070 Color: 2
Size: 4008 Color: 0

Bin 124: 63 of cap free
Amount of items: 3
Items: 
Size: 4116 Color: 1
Size: 3389 Color: 0
Size: 568 Color: 3

Bin 125: 63 of cap free
Amount of items: 2
Items: 
Size: 4997 Color: 4
Size: 3076 Color: 2

Bin 126: 82 of cap free
Amount of items: 2
Items: 
Size: 5932 Color: 0
Size: 2122 Color: 1

Bin 127: 83 of cap free
Amount of items: 2
Items: 
Size: 5242 Color: 3
Size: 2811 Color: 2

Bin 128: 84 of cap free
Amount of items: 2
Items: 
Size: 4662 Color: 2
Size: 3390 Color: 1

Bin 129: 96 of cap free
Amount of items: 2
Items: 
Size: 5460 Color: 1
Size: 2580 Color: 2

Bin 130: 104 of cap free
Amount of items: 2
Items: 
Size: 5114 Color: 3
Size: 2918 Color: 4

Bin 131: 110 of cap free
Amount of items: 2
Items: 
Size: 4638 Color: 1
Size: 3388 Color: 3

Bin 132: 120 of cap free
Amount of items: 2
Items: 
Size: 5590 Color: 3
Size: 2426 Color: 2

Bin 133: 6264 of cap free
Amount of items: 12
Items: 
Size: 256 Color: 3
Size: 196 Color: 4
Size: 156 Color: 4
Size: 152 Color: 4
Size: 152 Color: 0
Size: 144 Color: 0
Size: 140 Color: 1
Size: 140 Color: 0
Size: 136 Color: 3
Size: 136 Color: 3
Size: 136 Color: 1
Size: 128 Color: 1

Total size: 1073952
Total free space: 8136

