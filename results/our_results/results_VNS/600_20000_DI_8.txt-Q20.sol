Capicity Bin: 15744
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 7880 Color: 13
Size: 1308 Color: 14
Size: 1308 Color: 7
Size: 1136 Color: 7
Size: 1120 Color: 7
Size: 928 Color: 2
Size: 808 Color: 3
Size: 688 Color: 16
Size: 568 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 7912 Color: 3
Size: 6536 Color: 11
Size: 1296 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9712 Color: 14
Size: 5744 Color: 8
Size: 288 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10064 Color: 18
Size: 5368 Color: 1
Size: 312 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 12
Size: 4456 Color: 2
Size: 880 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10440 Color: 10
Size: 4424 Color: 8
Size: 880 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 9
Size: 4752 Color: 19
Size: 288 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10874 Color: 0
Size: 4396 Color: 5
Size: 474 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11124 Color: 3
Size: 4208 Color: 5
Size: 412 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11416 Color: 0
Size: 3852 Color: 7
Size: 476 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11450 Color: 8
Size: 3582 Color: 11
Size: 712 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11608 Color: 3
Size: 3856 Color: 11
Size: 280 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11718 Color: 4
Size: 3358 Color: 1
Size: 668 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11791 Color: 8
Size: 3241 Color: 18
Size: 712 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12068 Color: 19
Size: 2756 Color: 10
Size: 920 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 14
Size: 3068 Color: 4
Size: 344 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 3
Size: 2760 Color: 4
Size: 544 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12461 Color: 7
Size: 2885 Color: 2
Size: 398 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12624 Color: 11
Size: 2188 Color: 4
Size: 932 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12682 Color: 6
Size: 1942 Color: 2
Size: 1120 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12700 Color: 5
Size: 2036 Color: 9
Size: 1008 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12691 Color: 13
Size: 2545 Color: 8
Size: 508 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 1
Size: 2668 Color: 6
Size: 296 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12812 Color: 1
Size: 2260 Color: 7
Size: 672 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 6
Size: 2384 Color: 14
Size: 516 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12938 Color: 1
Size: 2342 Color: 11
Size: 464 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13036 Color: 17
Size: 2542 Color: 9
Size: 166 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13086 Color: 12
Size: 2218 Color: 17
Size: 440 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 19
Size: 2004 Color: 2
Size: 648 Color: 9

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 7
Size: 2420 Color: 13
Size: 126 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13242 Color: 19
Size: 1254 Color: 8
Size: 1248 Color: 19

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13308 Color: 19
Size: 1628 Color: 12
Size: 808 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13376 Color: 10
Size: 2032 Color: 7
Size: 336 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13380 Color: 17
Size: 1308 Color: 2
Size: 1056 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13506 Color: 17
Size: 1310 Color: 8
Size: 928 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13508 Color: 14
Size: 1484 Color: 19
Size: 752 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13522 Color: 2
Size: 1744 Color: 10
Size: 478 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13602 Color: 9
Size: 1678 Color: 7
Size: 464 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 17
Size: 1400 Color: 9
Size: 736 Color: 10

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13652 Color: 11
Size: 1728 Color: 2
Size: 364 Color: 12

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13694 Color: 14
Size: 1442 Color: 16
Size: 608 Color: 16

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13696 Color: 9
Size: 1098 Color: 16
Size: 950 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 2
Size: 1424 Color: 8
Size: 544 Color: 15

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13784 Color: 10
Size: 1304 Color: 6
Size: 656 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13788 Color: 6
Size: 1448 Color: 0
Size: 508 Color: 12

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13908 Color: 9
Size: 1220 Color: 13
Size: 616 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 4
Size: 1304 Color: 8
Size: 416 Color: 16

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14036 Color: 16
Size: 1372 Color: 5
Size: 336 Color: 19

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 0
Size: 1358 Color: 2
Size: 336 Color: 12

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14100 Color: 17
Size: 1324 Color: 18
Size: 320 Color: 5

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 12
Size: 992 Color: 6
Size: 634 Color: 14

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 5
Size: 1072 Color: 14
Size: 552 Color: 2

Bin 53: 1 of cap free
Amount of items: 7
Items: 
Size: 7873 Color: 10
Size: 1928 Color: 4
Size: 1784 Color: 15
Size: 1692 Color: 7
Size: 1526 Color: 14
Size: 608 Color: 14
Size: 332 Color: 18

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 8874 Color: 12
Size: 6557 Color: 2
Size: 312 Color: 19

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 9766 Color: 14
Size: 3391 Color: 1
Size: 2586 Color: 12

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 10017 Color: 16
Size: 5726 Color: 17

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 10214 Color: 1
Size: 4761 Color: 16
Size: 768 Color: 0

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 10754 Color: 10
Size: 4721 Color: 15
Size: 268 Color: 8

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 10777 Color: 6
Size: 4554 Color: 16
Size: 412 Color: 13

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11445 Color: 4
Size: 3698 Color: 8
Size: 600 Color: 1

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 11696 Color: 19
Size: 4047 Color: 18

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 11855 Color: 3
Size: 2128 Color: 4
Size: 1760 Color: 1

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 12109 Color: 4
Size: 2552 Color: 5
Size: 1082 Color: 18

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12642 Color: 19
Size: 2333 Color: 16
Size: 768 Color: 16

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12873 Color: 12
Size: 2780 Color: 2
Size: 90 Color: 1

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13321 Color: 11
Size: 1734 Color: 15
Size: 688 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13349 Color: 18
Size: 1848 Color: 1
Size: 546 Color: 11

Bin 68: 2 of cap free
Amount of items: 35
Items: 
Size: 576 Color: 10
Size: 576 Color: 1
Size: 576 Color: 1
Size: 564 Color: 4
Size: 552 Color: 13
Size: 536 Color: 11
Size: 536 Color: 5
Size: 512 Color: 19
Size: 512 Color: 4
Size: 496 Color: 15
Size: 488 Color: 14
Size: 480 Color: 18
Size: 480 Color: 10
Size: 464 Color: 18
Size: 458 Color: 9
Size: 452 Color: 19
Size: 448 Color: 19
Size: 448 Color: 17
Size: 448 Color: 3
Size: 448 Color: 1
Size: 436 Color: 9
Size: 432 Color: 15
Size: 404 Color: 9
Size: 400 Color: 10
Size: 400 Color: 9
Size: 392 Color: 18
Size: 392 Color: 6
Size: 384 Color: 0
Size: 384 Color: 0
Size: 380 Color: 8
Size: 368 Color: 17
Size: 352 Color: 16
Size: 352 Color: 3
Size: 312 Color: 18
Size: 304 Color: 8

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 8852 Color: 19
Size: 6546 Color: 12
Size: 344 Color: 7

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 8880 Color: 16
Size: 6542 Color: 17
Size: 320 Color: 19

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 9690 Color: 13
Size: 5748 Color: 4
Size: 304 Color: 8

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 9772 Color: 8
Size: 5650 Color: 16
Size: 320 Color: 17

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 10150 Color: 7
Size: 5352 Color: 5
Size: 240 Color: 1

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 10476 Color: 14
Size: 4662 Color: 13
Size: 604 Color: 13

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 10540 Color: 8
Size: 4712 Color: 15
Size: 490 Color: 17

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 12412 Color: 7
Size: 3330 Color: 9

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 12444 Color: 16
Size: 2018 Color: 18
Size: 1280 Color: 13

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 16
Size: 2270 Color: 0
Size: 272 Color: 13

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 13318 Color: 1
Size: 1296 Color: 8
Size: 1128 Color: 17

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 13328 Color: 9
Size: 2414 Color: 16

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 13348 Color: 15
Size: 2394 Color: 11

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 19
Size: 2070 Color: 6

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 13798 Color: 5
Size: 1604 Color: 2
Size: 340 Color: 9

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 13914 Color: 10
Size: 1428 Color: 15
Size: 400 Color: 2

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 14064 Color: 9
Size: 1678 Color: 7

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 14164 Color: 4
Size: 1578 Color: 12

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 7892 Color: 9
Size: 6553 Color: 17
Size: 1296 Color: 0

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 10129 Color: 3
Size: 5036 Color: 12
Size: 576 Color: 12

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 10761 Color: 7
Size: 4980 Color: 12

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 10818 Color: 15
Size: 4667 Color: 0
Size: 256 Color: 8

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 11248 Color: 13
Size: 4493 Color: 5

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 11381 Color: 17
Size: 4040 Color: 17
Size: 320 Color: 19

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 11692 Color: 7
Size: 3583 Color: 3
Size: 466 Color: 2

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 12422 Color: 10
Size: 3319 Color: 18

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 11116 Color: 12
Size: 3760 Color: 11
Size: 864 Color: 3

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 12342 Color: 7
Size: 3074 Color: 13
Size: 324 Color: 4

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 12760 Color: 3
Size: 2540 Color: 2
Size: 440 Color: 8

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 12
Size: 2176 Color: 16
Size: 612 Color: 8

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 17
Size: 2616 Color: 14

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 13412 Color: 16
Size: 2328 Color: 7

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 13588 Color: 13
Size: 2152 Color: 5

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 4
Size: 1848 Color: 9

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 9251 Color: 6
Size: 6488 Color: 5

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 13263 Color: 16
Size: 2476 Color: 14

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 10472 Color: 9
Size: 5266 Color: 3

Bin 106: 6 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 3
Size: 3006 Color: 2
Size: 356 Color: 8

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13458 Color: 0
Size: 2280 Color: 9

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 13586 Color: 15
Size: 2152 Color: 3

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 13716 Color: 19
Size: 2022 Color: 11

Bin 110: 7 of cap free
Amount of items: 4
Items: 
Size: 7890 Color: 1
Size: 4308 Color: 2
Size: 2605 Color: 15
Size: 934 Color: 12

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 9888 Color: 10
Size: 5345 Color: 6
Size: 504 Color: 3

Bin 112: 7 of cap free
Amount of items: 2
Items: 
Size: 10081 Color: 2
Size: 5656 Color: 7

Bin 113: 7 of cap free
Amount of items: 3
Items: 
Size: 10145 Color: 2
Size: 5400 Color: 19
Size: 192 Color: 1

Bin 114: 7 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 5
Size: 4153 Color: 2
Size: 356 Color: 1

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 2
Size: 5584 Color: 2
Size: 880 Color: 13

Bin 116: 8 of cap free
Amount of items: 3
Items: 
Size: 10033 Color: 15
Size: 5411 Color: 7
Size: 292 Color: 14

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 2
Size: 2432 Color: 19

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 12283 Color: 7
Size: 3452 Color: 13

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 13
Size: 2191 Color: 15

Bin 120: 9 of cap free
Amount of items: 2
Items: 
Size: 13666 Color: 11
Size: 2069 Color: 8

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 1
Size: 1906 Color: 11

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 13986 Color: 5
Size: 1748 Color: 13

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 14098 Color: 10
Size: 1636 Color: 17

Bin 124: 11 of cap free
Amount of items: 3
Items: 
Size: 8916 Color: 7
Size: 6561 Color: 10
Size: 256 Color: 18

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 12937 Color: 19
Size: 2796 Color: 17

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 13432 Color: 19
Size: 2301 Color: 14

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 3
Size: 1997 Color: 15

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 18
Size: 5312 Color: 10

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 12764 Color: 4
Size: 2968 Color: 8

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 13972 Color: 1
Size: 1760 Color: 10

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 9155 Color: 17
Size: 6576 Color: 11

Bin 132: 13 of cap free
Amount of items: 3
Items: 
Size: 12985 Color: 3
Size: 1466 Color: 10
Size: 1280 Color: 2

Bin 133: 14 of cap free
Amount of items: 6
Items: 
Size: 7882 Color: 12
Size: 1688 Color: 15
Size: 1640 Color: 10
Size: 1622 Color: 10
Size: 1524 Color: 7
Size: 1374 Color: 4

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 11
Size: 2554 Color: 8

Bin 135: 15 of cap free
Amount of items: 2
Items: 
Size: 11048 Color: 2
Size: 4681 Color: 11

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 12004 Color: 8
Size: 3725 Color: 14

Bin 137: 15 of cap free
Amount of items: 2
Items: 
Size: 12698 Color: 11
Size: 3031 Color: 5

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 13538 Color: 3
Size: 2190 Color: 17

Bin 139: 17 of cap free
Amount of items: 3
Items: 
Size: 7944 Color: 15
Size: 5046 Color: 2
Size: 2737 Color: 6

Bin 140: 18 of cap free
Amount of items: 3
Items: 
Size: 8966 Color: 12
Size: 6568 Color: 6
Size: 192 Color: 17

Bin 141: 18 of cap free
Amount of items: 3
Items: 
Size: 12518 Color: 14
Size: 3080 Color: 7
Size: 128 Color: 17

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 12888 Color: 16
Size: 2838 Color: 6

Bin 143: 19 of cap free
Amount of items: 3
Items: 
Size: 8217 Color: 15
Size: 6554 Color: 0
Size: 954 Color: 14

Bin 144: 19 of cap free
Amount of items: 2
Items: 
Size: 13117 Color: 12
Size: 2608 Color: 9

Bin 145: 20 of cap free
Amount of items: 3
Items: 
Size: 10880 Color: 3
Size: 4340 Color: 2
Size: 504 Color: 12

Bin 146: 20 of cap free
Amount of items: 3
Items: 
Size: 11952 Color: 16
Size: 3400 Color: 7
Size: 372 Color: 12

Bin 147: 20 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 11
Size: 3264 Color: 12
Size: 64 Color: 10

Bin 148: 20 of cap free
Amount of items: 3
Items: 
Size: 12696 Color: 12
Size: 2896 Color: 1
Size: 132 Color: 3

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 13680 Color: 15
Size: 2044 Color: 1

Bin 150: 20 of cap free
Amount of items: 2
Items: 
Size: 14014 Color: 14
Size: 1710 Color: 10

Bin 151: 20 of cap free
Amount of items: 3
Items: 
Size: 14160 Color: 13
Size: 1548 Color: 3
Size: 16 Color: 17

Bin 152: 22 of cap free
Amount of items: 3
Items: 
Size: 9304 Color: 2
Size: 5692 Color: 13
Size: 726 Color: 18

Bin 153: 22 of cap free
Amount of items: 2
Items: 
Size: 11112 Color: 10
Size: 4610 Color: 0

Bin 154: 22 of cap free
Amount of items: 2
Items: 
Size: 13854 Color: 10
Size: 1868 Color: 4

Bin 155: 24 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 14
Size: 3448 Color: 9

Bin 156: 24 of cap free
Amount of items: 2
Items: 
Size: 12912 Color: 4
Size: 2808 Color: 19

Bin 157: 24 of cap free
Amount of items: 2
Items: 
Size: 13918 Color: 9
Size: 1802 Color: 12

Bin 158: 24 of cap free
Amount of items: 2
Items: 
Size: 14072 Color: 8
Size: 1648 Color: 1

Bin 159: 25 of cap free
Amount of items: 2
Items: 
Size: 11275 Color: 14
Size: 4444 Color: 5

Bin 160: 25 of cap free
Amount of items: 3
Items: 
Size: 13013 Color: 6
Size: 2642 Color: 10
Size: 64 Color: 4

Bin 161: 25 of cap free
Amount of items: 2
Items: 
Size: 13326 Color: 12
Size: 2393 Color: 1

Bin 162: 26 of cap free
Amount of items: 14
Items: 
Size: 7876 Color: 12
Size: 744 Color: 4
Size: 738 Color: 11
Size: 736 Color: 1
Size: 720 Color: 6
Size: 716 Color: 11
Size: 688 Color: 16
Size: 632 Color: 2
Size: 608 Color: 13
Size: 576 Color: 10
Size: 524 Color: 3
Size: 424 Color: 0
Size: 416 Color: 8
Size: 320 Color: 13

Bin 163: 26 of cap free
Amount of items: 2
Items: 
Size: 11310 Color: 8
Size: 4408 Color: 13

Bin 164: 26 of cap free
Amount of items: 3
Items: 
Size: 11544 Color: 2
Size: 4062 Color: 16
Size: 112 Color: 12

Bin 165: 26 of cap free
Amount of items: 2
Items: 
Size: 12874 Color: 10
Size: 2844 Color: 16

Bin 166: 28 of cap free
Amount of items: 3
Items: 
Size: 11672 Color: 13
Size: 3764 Color: 4
Size: 280 Color: 12

Bin 167: 28 of cap free
Amount of items: 3
Items: 
Size: 12138 Color: 13
Size: 1842 Color: 18
Size: 1736 Color: 2

Bin 168: 31 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 14
Size: 2457 Color: 9

Bin 169: 32 of cap free
Amount of items: 2
Items: 
Size: 12200 Color: 18
Size: 3512 Color: 12

Bin 170: 32 of cap free
Amount of items: 2
Items: 
Size: 13022 Color: 15
Size: 2690 Color: 12

Bin 171: 34 of cap free
Amount of items: 2
Items: 
Size: 11604 Color: 0
Size: 4106 Color: 6

Bin 172: 34 of cap free
Amount of items: 2
Items: 
Size: 13844 Color: 15
Size: 1866 Color: 4

Bin 173: 35 of cap free
Amount of items: 2
Items: 
Size: 12525 Color: 3
Size: 3184 Color: 19

Bin 174: 38 of cap free
Amount of items: 2
Items: 
Size: 12058 Color: 8
Size: 3648 Color: 4

Bin 175: 38 of cap free
Amount of items: 2
Items: 
Size: 13262 Color: 9
Size: 2444 Color: 0

Bin 176: 38 of cap free
Amount of items: 2
Items: 
Size: 13734 Color: 12
Size: 1972 Color: 10

Bin 177: 42 of cap free
Amount of items: 2
Items: 
Size: 12578 Color: 0
Size: 3124 Color: 18

Bin 178: 45 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 11
Size: 2683 Color: 13

Bin 179: 48 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 15
Size: 2040 Color: 3

Bin 180: 48 of cap free
Amount of items: 2
Items: 
Size: 14156 Color: 16
Size: 1540 Color: 17

Bin 181: 49 of cap free
Amount of items: 11
Items: 
Size: 7877 Color: 0
Size: 1072 Color: 5
Size: 942 Color: 13
Size: 932 Color: 2
Size: 872 Color: 11
Size: 864 Color: 14
Size: 832 Color: 17
Size: 820 Color: 15
Size: 560 Color: 3
Size: 488 Color: 18
Size: 436 Color: 8

Bin 182: 49 of cap free
Amount of items: 2
Items: 
Size: 12797 Color: 0
Size: 2898 Color: 10

Bin 183: 50 of cap free
Amount of items: 3
Items: 
Size: 9331 Color: 4
Size: 6107 Color: 5
Size: 256 Color: 10

Bin 184: 51 of cap free
Amount of items: 2
Items: 
Size: 12056 Color: 2
Size: 3637 Color: 10

Bin 185: 70 of cap free
Amount of items: 4
Items: 
Size: 7888 Color: 12
Size: 3608 Color: 3
Size: 2392 Color: 12
Size: 1786 Color: 2

Bin 186: 81 of cap free
Amount of items: 2
Items: 
Size: 12187 Color: 8
Size: 3476 Color: 13

Bin 187: 82 of cap free
Amount of items: 2
Items: 
Size: 10889 Color: 9
Size: 4773 Color: 17

Bin 188: 86 of cap free
Amount of items: 6
Items: 
Size: 7885 Color: 5
Size: 2341 Color: 17
Size: 2212 Color: 16
Size: 1948 Color: 13
Size: 1000 Color: 0
Size: 272 Color: 4

Bin 189: 98 of cap free
Amount of items: 2
Items: 
Size: 11782 Color: 9
Size: 3864 Color: 17

Bin 190: 120 of cap free
Amount of items: 3
Items: 
Size: 7976 Color: 9
Size: 6504 Color: 18
Size: 1144 Color: 16

Bin 191: 135 of cap free
Amount of items: 2
Items: 
Size: 9336 Color: 15
Size: 6273 Color: 11

Bin 192: 138 of cap free
Amount of items: 3
Items: 
Size: 7898 Color: 6
Size: 6564 Color: 7
Size: 1144 Color: 4

Bin 193: 149 of cap free
Amount of items: 2
Items: 
Size: 10104 Color: 13
Size: 5491 Color: 3

Bin 194: 172 of cap free
Amount of items: 2
Items: 
Size: 8968 Color: 16
Size: 6604 Color: 14

Bin 195: 180 of cap free
Amount of items: 2
Items: 
Size: 9708 Color: 2
Size: 5856 Color: 8

Bin 196: 195 of cap free
Amount of items: 7
Items: 
Size: 7881 Color: 11
Size: 1522 Color: 5
Size: 1368 Color: 8
Size: 1328 Color: 1
Size: 1310 Color: 19
Size: 1310 Color: 14
Size: 830 Color: 0

Bin 197: 206 of cap free
Amount of items: 2
Items: 
Size: 8417 Color: 10
Size: 7121 Color: 19

Bin 198: 240 of cap free
Amount of items: 3
Items: 
Size: 7874 Color: 10
Size: 6562 Color: 12
Size: 1068 Color: 18

Bin 199: 11992 of cap free
Amount of items: 12
Items: 
Size: 384 Color: 13
Size: 368 Color: 4
Size: 352 Color: 15
Size: 320 Color: 17
Size: 320 Color: 14
Size: 320 Color: 6
Size: 312 Color: 16
Size: 304 Color: 3
Size: 272 Color: 12
Size: 272 Color: 11
Size: 272 Color: 0
Size: 256 Color: 8

Total size: 3117312
Total free space: 15744

