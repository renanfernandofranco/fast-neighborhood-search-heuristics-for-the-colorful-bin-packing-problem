Capicity Bin: 1001
Lower Bound: 216

Bins used: 217
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 4
Size: 290 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 2
Size: 366 Color: 4
Size: 110 Color: 3

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 2
Size: 420 Color: 1

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 2
Size: 220 Color: 0

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 0
Size: 401 Color: 4

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 3
Size: 380 Color: 0

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 0
Size: 261 Color: 3

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 3
Size: 294 Color: 4

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 406 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 521 Color: 4
Size: 340 Color: 2
Size: 140 Color: 3

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 2
Size: 227 Color: 1

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 1
Size: 262 Color: 0

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 4
Size: 316 Color: 3

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 2
Size: 325 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 781 Color: 4
Size: 117 Color: 0
Size: 103 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 546 Color: 1
Size: 284 Color: 4
Size: 171 Color: 2

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 4
Size: 240 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 528 Color: 3
Size: 335 Color: 4
Size: 138 Color: 3

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 1
Size: 473 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 546 Color: 2
Size: 321 Color: 4
Size: 134 Color: 4

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 3
Size: 326 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 2
Size: 337 Color: 3
Size: 315 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 546 Color: 3
Size: 351 Color: 2
Size: 104 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 789 Color: 0
Size: 109 Color: 2
Size: 103 Color: 0

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 4
Size: 470 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 530 Color: 0
Size: 348 Color: 3
Size: 123 Color: 4

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 4
Size: 243 Color: 3

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 4
Size: 368 Color: 0

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 1
Size: 283 Color: 0

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 4

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 0
Size: 255 Color: 4

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 3
Size: 465 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 2
Size: 159 Color: 4
Size: 133 Color: 0

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 0
Size: 318 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 517 Color: 1
Size: 343 Color: 2
Size: 141 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 3
Size: 363 Color: 3
Size: 253 Color: 4

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 445 Color: 2

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 1
Size: 325 Color: 3

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 0
Size: 351 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 384 Color: 4
Size: 161 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 2
Size: 253 Color: 4
Size: 125 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 4
Size: 134 Color: 0
Size: 102 Color: 0

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 395 Color: 3

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 4
Size: 474 Color: 2

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 250 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 3
Size: 327 Color: 1
Size: 149 Color: 3

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 2
Size: 270 Color: 1

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 2
Size: 319 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 737 Color: 4
Size: 144 Color: 3
Size: 120 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 4
Size: 311 Color: 3
Size: 189 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 3
Size: 158 Color: 1
Size: 129 Color: 3

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 4
Size: 276 Color: 1

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 1
Size: 485 Color: 2

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 4
Size: 269 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 4
Size: 218 Color: 0
Size: 198 Color: 3

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 2
Size: 333 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 580 Color: 0
Size: 262 Color: 0
Size: 159 Color: 3

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 0
Size: 265 Color: 2

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 3

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 0
Size: 426 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 1
Size: 261 Color: 2
Size: 190 Color: 2

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 1
Size: 385 Color: 3

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 317 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 0
Size: 197 Color: 3
Size: 129 Color: 0

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 246 Color: 1

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 3
Size: 500 Color: 2

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 3
Size: 443 Color: 2

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 309 Color: 2

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 2
Size: 317 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 2
Size: 266 Color: 1
Size: 185 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 793 Color: 3
Size: 108 Color: 4
Size: 100 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 523 Color: 1
Size: 262 Color: 1
Size: 216 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 0
Size: 128 Color: 4
Size: 116 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 548 Color: 3
Size: 266 Color: 2
Size: 187 Color: 3

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 2
Size: 480 Color: 3

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 3
Size: 270 Color: 4

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 4
Size: 318 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 691 Color: 2
Size: 176 Color: 1
Size: 134 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 342 Color: 1
Size: 268 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 404 Color: 3
Size: 166 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 0
Size: 146 Color: 1
Size: 131 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 4
Size: 181 Color: 0
Size: 133 Color: 1

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 2
Size: 460 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 2
Size: 342 Color: 4
Size: 190 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 587 Color: 3
Size: 288 Color: 0
Size: 126 Color: 4

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 339 Color: 2
Size: 301 Color: 1

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 0
Size: 282 Color: 4

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 0
Size: 388 Color: 1

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 1
Size: 243 Color: 0

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 3
Size: 360 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 705 Color: 1
Size: 173 Color: 3
Size: 123 Color: 4

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 275 Color: 4

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 4
Size: 402 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 4
Size: 338 Color: 2
Size: 136 Color: 1

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 0
Size: 410 Color: 2

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 4
Size: 191 Color: 3
Size: 113 Color: 4

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 4
Size: 372 Color: 3
Size: 240 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 598 Color: 4
Size: 256 Color: 2
Size: 147 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 365 Color: 3
Size: 244 Color: 0

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 3
Size: 436 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 2
Size: 181 Color: 4
Size: 123 Color: 2

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 2
Size: 470 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 2
Size: 422 Color: 4
Size: 151 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 3
Size: 177 Color: 0
Size: 137 Color: 2

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 351 Color: 2
Size: 251 Color: 4

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 394 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 3
Size: 196 Color: 3
Size: 181 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 0
Size: 362 Color: 2
Size: 172 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 370 Color: 3
Size: 144 Color: 3

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 4
Size: 374 Color: 1
Size: 129 Color: 3

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 2
Size: 318 Color: 3

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 315 Color: 1
Size: 193 Color: 3

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 3
Size: 221 Color: 4

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 2
Size: 205 Color: 2
Size: 117 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 581 Color: 0
Size: 230 Color: 4
Size: 190 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 427 Color: 1
Size: 127 Color: 2

Bin 117: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 2
Size: 361 Color: 3

Bin 118: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 473 Color: 4

Bin 119: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 3
Size: 408 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 4
Size: 138 Color: 2
Size: 120 Color: 2

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 0
Size: 178 Color: 4
Size: 104 Color: 1

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 0
Size: 474 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 0
Size: 362 Color: 1
Size: 170 Color: 0

Bin 124: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 2
Size: 471 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 3
Size: 338 Color: 2
Size: 166 Color: 4

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 0
Size: 386 Color: 3
Size: 216 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 4
Size: 143 Color: 4
Size: 140 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 576 Color: 0
Size: 307 Color: 2
Size: 118 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 643 Color: 3
Size: 247 Color: 3
Size: 111 Color: 2

Bin 130: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 0
Size: 428 Color: 3

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 2
Size: 391 Color: 1
Size: 149 Color: 3

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 0
Size: 366 Color: 0
Size: 269 Color: 4

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 268 Color: 2
Size: 237 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 582 Color: 3
Size: 241 Color: 3
Size: 178 Color: 1

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 3
Size: 437 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 4
Size: 353 Color: 4
Size: 259 Color: 0

Bin 137: 1 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 3
Size: 340 Color: 0

Bin 138: 1 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 4
Size: 383 Color: 3

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 312 Color: 1
Size: 259 Color: 1

Bin 140: 1 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 1
Size: 400 Color: 0

Bin 141: 1 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 394 Color: 2

Bin 142: 1 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 1
Size: 456 Color: 2

Bin 143: 1 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 209 Color: 2

Bin 144: 1 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 405 Color: 2

Bin 145: 1 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 3
Size: 281 Color: 0

Bin 146: 1 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 1
Size: 223 Color: 2

Bin 147: 1 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 405 Color: 0

Bin 148: 1 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 0
Size: 436 Color: 3

Bin 149: 1 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 2
Size: 375 Color: 0

Bin 150: 1 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 203 Color: 0

Bin 151: 1 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 3
Size: 413 Color: 1

Bin 152: 1 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 3
Size: 444 Color: 0

Bin 153: 1 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 0
Size: 488 Color: 3

Bin 154: 1 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 4
Size: 214 Color: 1

Bin 155: 1 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 3
Size: 221 Color: 0

Bin 156: 1 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 0
Size: 289 Color: 2

Bin 157: 1 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 4
Size: 334 Color: 1

Bin 158: 1 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 4
Size: 203 Color: 3

Bin 159: 1 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 1
Size: 463 Color: 4

Bin 160: 1 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 4
Size: 380 Color: 2

Bin 161: 1 of cap free
Amount of items: 3
Items: 
Size: 521 Color: 3
Size: 309 Color: 0
Size: 170 Color: 0

Bin 162: 1 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 2
Size: 238 Color: 3

Bin 163: 1 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 0
Size: 381 Color: 4

Bin 164: 1 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 4
Size: 406 Color: 1

Bin 165: 1 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 423 Color: 4

Bin 166: 1 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 331 Color: 1

Bin 167: 1 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 2
Size: 396 Color: 0

Bin 168: 1 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 0
Size: 227 Color: 2

Bin 169: 1 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 1
Size: 354 Color: 3

Bin 170: 2 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 202 Color: 0

Bin 171: 2 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 1
Size: 497 Color: 2

Bin 172: 2 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 1
Size: 452 Color: 0

Bin 173: 2 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 0
Size: 224 Color: 1

Bin 174: 2 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 3
Size: 245 Color: 4

Bin 175: 2 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 0
Size: 205 Color: 4

Bin 176: 2 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 4
Size: 410 Color: 2

Bin 177: 2 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 0
Size: 306 Color: 1

Bin 178: 2 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 2
Size: 306 Color: 0

Bin 179: 2 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 2
Size: 432 Color: 3

Bin 180: 2 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 0
Size: 198 Color: 4

Bin 181: 2 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 4
Size: 344 Color: 1

Bin 182: 2 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 2
Size: 443 Color: 1

Bin 183: 2 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 4
Size: 449 Color: 3

Bin 184: 2 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 2
Size: 254 Color: 0

Bin 185: 2 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 1
Size: 293 Color: 3

Bin 186: 3 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 2
Size: 284 Color: 3

Bin 187: 3 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 491 Color: 1

Bin 188: 3 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 2
Size: 439 Color: 0

Bin 189: 3 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 387 Color: 3

Bin 190: 3 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 3
Size: 370 Color: 1

Bin 191: 3 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 1
Size: 331 Color: 0

Bin 192: 4 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 3
Size: 231 Color: 1

Bin 193: 4 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 1
Size: 482 Color: 0

Bin 194: 4 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 1
Size: 413 Color: 3

Bin 195: 4 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 3
Size: 273 Color: 4

Bin 196: 4 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 2
Size: 408 Color: 0

Bin 197: 5 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 1
Size: 442 Color: 2

Bin 198: 5 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 4
Size: 230 Color: 3

Bin 199: 5 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 4
Size: 426 Color: 0

Bin 200: 6 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 330 Color: 2

Bin 201: 7 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 2
Size: 201 Color: 3

Bin 202: 9 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 0
Size: 458 Color: 3

Bin 203: 10 of cap free
Amount of items: 2
Items: 
Size: 496 Color: 1
Size: 495 Color: 4

Bin 204: 11 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 4
Size: 240 Color: 0

Bin 205: 11 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 197 Color: 1

Bin 206: 12 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 4
Size: 216 Color: 1

Bin 207: 12 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 2
Size: 459 Color: 0

Bin 208: 17 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 2
Size: 330 Color: 1

Bin 209: 20 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 2
Size: 193 Color: 4

Bin 210: 20 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 3
Size: 193 Color: 1

Bin 211: 21 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 3
Size: 457 Color: 1

Bin 212: 32 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 1
Size: 422 Color: 3

Bin 213: 42 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 3
Size: 237 Color: 4

Bin 214: 42 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 4
Size: 379 Color: 2

Bin 215: 228 of cap free
Amount of items: 1
Items: 
Size: 773 Color: 1

Bin 216: 309 of cap free
Amount of items: 1
Items: 
Size: 692 Color: 0

Bin 217: 311 of cap free
Amount of items: 1
Items: 
Size: 690 Color: 3

Total size: 215979
Total free space: 1238

