Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1660 Color: 157
Size: 568 Color: 106
Size: 228 Color: 71

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 165
Size: 609 Color: 109
Size: 120 Color: 45

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 166
Size: 692 Color: 116
Size: 32 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 167
Size: 522 Color: 101
Size: 200 Color: 64

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 169
Size: 532 Color: 103
Size: 104 Color: 41

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 171
Size: 458 Color: 93
Size: 172 Color: 60

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1906 Color: 177
Size: 462 Color: 94
Size: 88 Color: 36

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 185
Size: 438 Color: 92
Size: 4 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 186
Size: 412 Color: 90
Size: 8 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 187
Size: 254 Color: 74
Size: 144 Color: 52

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2060 Color: 188
Size: 272 Color: 77
Size: 124 Color: 49

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 189
Size: 330 Color: 83
Size: 64 Color: 24

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2074 Color: 190
Size: 356 Color: 85
Size: 26 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 191
Size: 248 Color: 73
Size: 94 Color: 37

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 192
Size: 332 Color: 84
Size: 8 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 194
Size: 262 Color: 75
Size: 48 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 195
Size: 174 Color: 62
Size: 124 Color: 48

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 197
Size: 168 Color: 59
Size: 100 Color: 39

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 199
Size: 210 Color: 69
Size: 48 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 200
Size: 172 Color: 61
Size: 80 Color: 28

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2206 Color: 201
Size: 146 Color: 55
Size: 104 Color: 42

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 161
Size: 738 Color: 119
Size: 16 Color: 3

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 174
Size: 406 Color: 89
Size: 200 Color: 65

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1891 Color: 176
Size: 564 Color: 105

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1907 Color: 178
Size: 436 Color: 91
Size: 112 Color: 43

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1947 Color: 181
Size: 508 Color: 99

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1970 Color: 183
Size: 485 Color: 97

Bin 28: 2 of cap free
Amount of items: 5
Items: 
Size: 1236 Color: 141
Size: 1018 Color: 133
Size: 88 Color: 33
Size: 56 Color: 21
Size: 56 Color: 20

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 148
Size: 988 Color: 132
Size: 40 Color: 11

Bin 30: 2 of cap free
Amount of items: 4
Items: 
Size: 1574 Color: 155
Size: 812 Color: 124
Size: 36 Color: 10
Size: 32 Color: 8

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1723 Color: 164
Size: 731 Color: 118

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1823 Color: 170
Size: 631 Color: 112

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1852 Color: 175
Size: 602 Color: 108

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 2132 Color: 193
Size: 322 Color: 82

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 2174 Color: 196
Size: 280 Color: 79

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 2192 Color: 198
Size: 262 Color: 76

Bin 37: 3 of cap free
Amount of items: 7
Items: 
Size: 1229 Color: 138
Size: 370 Color: 86
Size: 284 Color: 80
Size: 238 Color: 72
Size: 204 Color: 66
Size: 64 Color: 25
Size: 64 Color: 23

Bin 38: 3 of cap free
Amount of items: 5
Items: 
Size: 1230 Color: 139
Size: 507 Color: 98
Size: 370 Color: 87
Size: 286 Color: 81
Size: 60 Color: 22

Bin 39: 3 of cap free
Amount of items: 5
Items: 
Size: 1276 Color: 143
Size: 1021 Color: 135
Size: 64 Color: 26
Size: 48 Color: 15
Size: 44 Color: 14

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1484 Color: 149
Size: 585 Color: 107
Size: 384 Color: 88

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 160
Size: 740 Color: 120
Size: 16 Color: 4

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 180
Size: 512 Color: 100

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1710 Color: 162
Size: 741 Color: 121

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1910 Color: 179
Size: 541 Color: 104

Bin 45: 6 of cap free
Amount of items: 5
Items: 
Size: 1238 Color: 142
Size: 1020 Color: 134
Size: 96 Color: 38
Size: 48 Color: 18
Size: 48 Color: 17

Bin 46: 6 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 150
Size: 638 Color: 113
Size: 276 Color: 78

Bin 47: 7 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 156
Size: 840 Color: 125
Size: 28 Color: 7

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1719 Color: 163
Size: 730 Color: 117

Bin 49: 8 of cap free
Amount of items: 2
Items: 
Size: 1780 Color: 168
Size: 668 Color: 115

Bin 50: 8 of cap free
Amount of items: 2
Items: 
Size: 1833 Color: 173
Size: 615 Color: 111

Bin 51: 9 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 158
Size: 749 Color: 122
Size: 16 Color: 5

Bin 52: 9 of cap free
Amount of items: 2
Items: 
Size: 1964 Color: 182
Size: 483 Color: 96

Bin 53: 10 of cap free
Amount of items: 2
Items: 
Size: 1572 Color: 154
Size: 874 Color: 129

Bin 54: 11 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 184
Size: 471 Color: 95

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1694 Color: 159
Size: 750 Color: 123

Bin 56: 14 of cap free
Amount of items: 2
Items: 
Size: 1569 Color: 153
Size: 873 Color: 128

Bin 57: 15 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 147
Size: 980 Color: 131
Size: 40 Color: 12

Bin 58: 15 of cap free
Amount of items: 2
Items: 
Size: 1830 Color: 172
Size: 611 Color: 110

Bin 59: 24 of cap free
Amount of items: 2
Items: 
Size: 1409 Color: 145
Size: 1023 Color: 137

Bin 60: 26 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 146
Size: 976 Color: 130
Size: 44 Color: 13

Bin 61: 34 of cap free
Amount of items: 2
Items: 
Size: 1559 Color: 152
Size: 863 Color: 127

Bin 62: 36 of cap free
Amount of items: 2
Items: 
Size: 1558 Color: 151
Size: 862 Color: 126

Bin 63: 38 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 144
Size: 1022 Color: 136

Bin 64: 53 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 140
Size: 646 Color: 114
Size: 526 Color: 102

Bin 65: 90 of cap free
Amount of items: 18
Items: 
Size: 220 Color: 70
Size: 204 Color: 68
Size: 204 Color: 67
Size: 192 Color: 63
Size: 160 Color: 58
Size: 148 Color: 57
Size: 148 Color: 56
Size: 144 Color: 54
Size: 144 Color: 53
Size: 128 Color: 51
Size: 104 Color: 40
Size: 88 Color: 35
Size: 88 Color: 34
Size: 82 Color: 32
Size: 80 Color: 31
Size: 80 Color: 30
Size: 80 Color: 29
Size: 72 Color: 27

Bin 66: 1964 of cap free
Amount of items: 4
Items: 
Size: 128 Color: 50
Size: 122 Color: 47
Size: 122 Color: 46
Size: 120 Color: 44

Total size: 159640
Total free space: 2456

