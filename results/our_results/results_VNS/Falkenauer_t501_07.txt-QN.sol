Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 435
Size: 287 Color: 191
Size: 276 Color: 158

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 358
Size: 371 Color: 352
Size: 255 Color: 48

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 460
Size: 276 Color: 157
Size: 259 Color: 71

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 454
Size: 280 Color: 172
Size: 263 Color: 96

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 456
Size: 280 Color: 168
Size: 260 Color: 81

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 464
Size: 281 Color: 174
Size: 253 Color: 35

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 494
Size: 257 Color: 59
Size: 250 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 444
Size: 288 Color: 193
Size: 263 Color: 98

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 493
Size: 256 Color: 55
Size: 251 Color: 26

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 500
Size: 251 Color: 25
Size: 250 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 491
Size: 257 Color: 66
Size: 251 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 496
Size: 256 Color: 50
Size: 250 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 471
Size: 279 Color: 165
Size: 251 Color: 23

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 490
Size: 256 Color: 56
Size: 253 Color: 40

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 427
Size: 312 Color: 261
Size: 257 Color: 60

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 416
Size: 326 Color: 280
Size: 253 Color: 32

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 415
Size: 307 Color: 246
Size: 272 Color: 140

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 445
Size: 284 Color: 181
Size: 266 Color: 111

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 348
Size: 340 Color: 296
Size: 291 Color: 210

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 448
Size: 297 Color: 222
Size: 251 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 472
Size: 268 Color: 118
Size: 262 Color: 88

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 401
Size: 318 Color: 270
Size: 274 Color: 151

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 473
Size: 273 Color: 143
Size: 257 Color: 61

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 479
Size: 263 Color: 93
Size: 260 Color: 80

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 462
Size: 279 Color: 166
Size: 256 Color: 53

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 400
Size: 274 Color: 150
Size: 319 Color: 272

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 441
Size: 285 Color: 184
Size: 269 Color: 123

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 410
Size: 328 Color: 284
Size: 256 Color: 57

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 478
Size: 268 Color: 121
Size: 255 Color: 45

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 419
Size: 307 Color: 247
Size: 266 Color: 110

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 458
Size: 282 Color: 177
Size: 256 Color: 58

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 337
Size: 364 Color: 336
Size: 272 Color: 142

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 420
Size: 301 Color: 234
Size: 271 Color: 133

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 338
Size: 363 Color: 335
Size: 273 Color: 144

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 396
Size: 318 Color: 271
Size: 278 Color: 162

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 367
Size: 320 Color: 273
Size: 300 Color: 228

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 392
Size: 331 Color: 286
Size: 269 Color: 124

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 393
Size: 308 Color: 249
Size: 290 Color: 206

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 485
Size: 260 Color: 78
Size: 256 Color: 54

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 469
Size: 272 Color: 139
Size: 259 Color: 75

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 317
Size: 337 Color: 295
Size: 312 Color: 262

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 437
Size: 288 Color: 194
Size: 271 Color: 132

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 499
Size: 251 Color: 27
Size: 250 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 453
Size: 289 Color: 197
Size: 255 Color: 49

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 363
Size: 361 Color: 334
Size: 263 Color: 90

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 350
Size: 357 Color: 325
Size: 273 Color: 147

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 449
Size: 286 Color: 188
Size: 262 Color: 85

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 447
Size: 291 Color: 207
Size: 258 Color: 68

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 353
Size: 359 Color: 329
Size: 270 Color: 127

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 470
Size: 266 Color: 108
Size: 264 Color: 100

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 351
Size: 343 Color: 301
Size: 287 Color: 190

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 412
Size: 293 Color: 211
Size: 290 Color: 204

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 375
Size: 335 Color: 293
Size: 277 Color: 160

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 452
Size: 273 Color: 146
Size: 271 Color: 136

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 482
Size: 266 Color: 106
Size: 255 Color: 46

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 455
Size: 280 Color: 171
Size: 262 Color: 86

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 324
Size: 352 Color: 318
Size: 293 Color: 212

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 340
Size: 344 Color: 307
Size: 290 Color: 200

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 481
Size: 271 Color: 134
Size: 250 Color: 10

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 418
Size: 314 Color: 263
Size: 263 Color: 91

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 406
Size: 322 Color: 274
Size: 266 Color: 107

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 344
Size: 343 Color: 302
Size: 289 Color: 196

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 333
Size: 361 Color: 332
Size: 278 Color: 163

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 492
Size: 257 Color: 63
Size: 251 Color: 16

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 386
Size: 333 Color: 289
Size: 272 Color: 138

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 381
Size: 306 Color: 245
Size: 302 Color: 235

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 391
Size: 325 Color: 279
Size: 276 Color: 156

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 366
Size: 354 Color: 320
Size: 268 Color: 122

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 372
Size: 336 Color: 294
Size: 280 Color: 167

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 369
Size: 355 Color: 322
Size: 263 Color: 95

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 424
Size: 289 Color: 199
Size: 281 Color: 173

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 374
Size: 309 Color: 252
Size: 303 Color: 237

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 360
Size: 360 Color: 331
Size: 266 Color: 105

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 442
Size: 301 Color: 232
Size: 251 Color: 28

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 432
Size: 316 Color: 264
Size: 250 Color: 13

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 359
Size: 372 Color: 355
Size: 254 Color: 42

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 389
Size: 332 Color: 287
Size: 271 Color: 131

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 404
Size: 324 Color: 275
Size: 267 Color: 115

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 421
Size: 289 Color: 195
Size: 283 Color: 178

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 488
Size: 257 Color: 62
Size: 256 Color: 51

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 497
Size: 254 Color: 43
Size: 251 Color: 21

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 486
Size: 263 Color: 97
Size: 253 Color: 33

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 394
Size: 318 Color: 269
Size: 279 Color: 164

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 362
Size: 375 Color: 361
Size: 250 Color: 5

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 377
Size: 305 Color: 241
Size: 304 Color: 239

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 474
Size: 277 Color: 159
Size: 252 Color: 29

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 378
Size: 309 Color: 253
Size: 300 Color: 231

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 310
Size: 345 Color: 309
Size: 310 Color: 256

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 408
Size: 328 Color: 281
Size: 257 Color: 65

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 428
Size: 311 Color: 257
Size: 258 Color: 67

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 467
Size: 270 Color: 128
Size: 263 Color: 92

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 425
Size: 311 Color: 258
Size: 259 Color: 74

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 413
Size: 328 Color: 282
Size: 253 Color: 34

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 328
Size: 357 Color: 326
Size: 284 Color: 183

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 384
Size: 308 Color: 248
Size: 298 Color: 223

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 463
Size: 284 Color: 182
Size: 251 Color: 24

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 382
Size: 335 Color: 292
Size: 273 Color: 145

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 426
Size: 316 Color: 265
Size: 253 Color: 41

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 312
Size: 347 Color: 311
Size: 304 Color: 238

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 398
Size: 328 Color: 283
Size: 267 Color: 114

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 357
Size: 354 Color: 321
Size: 274 Color: 149

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 465
Size: 268 Color: 119
Size: 266 Color: 112

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 439
Size: 286 Color: 186
Size: 269 Color: 125

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 468
Size: 273 Color: 148
Size: 260 Color: 76

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 405
Size: 295 Color: 218
Size: 295 Color: 215

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 434
Size: 312 Color: 260
Size: 251 Color: 20

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 315
Size: 350 Color: 313
Size: 300 Color: 226

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 402
Size: 324 Color: 276
Size: 268 Color: 120

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 365
Size: 340 Color: 297
Size: 284 Color: 180

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 339
Size: 344 Color: 306
Size: 290 Color: 203

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 383
Size: 306 Color: 242
Size: 301 Color: 233

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 364
Size: 325 Color: 278
Size: 299 Color: 225

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 451
Size: 290 Color: 205
Size: 255 Color: 44

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 407
Size: 306 Color: 243
Size: 280 Color: 170

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 480
Size: 269 Color: 126
Size: 252 Color: 31

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 438
Size: 282 Color: 176
Size: 276 Color: 154

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 371
Size: 367 Color: 343
Size: 250 Color: 14

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 484
Size: 265 Color: 103
Size: 252 Color: 30

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 379
Size: 309 Color: 251
Size: 300 Color: 229

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 440
Size: 287 Color: 189
Size: 267 Color: 113

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 388
Size: 309 Color: 255
Size: 295 Color: 214

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 430
Size: 317 Color: 268
Size: 250 Color: 6

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 459
Size: 286 Color: 185
Size: 251 Color: 18

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 489
Size: 261 Color: 83
Size: 250 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 429
Size: 317 Color: 266
Size: 250 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 308
Size: 344 Color: 305
Size: 311 Color: 259

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 399
Size: 317 Color: 267
Size: 276 Color: 155

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 390
Size: 332 Color: 288
Size: 271 Color: 135

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 397
Size: 335 Color: 290
Size: 261 Color: 82

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 411
Size: 309 Color: 254
Size: 274 Color: 152

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 414
Size: 330 Color: 285
Size: 250 Color: 11

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 354
Size: 343 Color: 303
Size: 286 Color: 187

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 368
Size: 369 Color: 346
Size: 250 Color: 9

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 356
Size: 360 Color: 330
Size: 268 Color: 117

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 487
Size: 264 Color: 99
Size: 250 Color: 12

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 457
Size: 274 Color: 153
Size: 265 Color: 102

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 466
Size: 272 Color: 141
Size: 262 Color: 84

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 417
Size: 308 Color: 250
Size: 271 Color: 130

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 376
Size: 357 Color: 327
Size: 253 Color: 38

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 347
Size: 368 Color: 345
Size: 263 Color: 94

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 495
Size: 255 Color: 47
Size: 251 Color: 15

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 436
Size: 303 Color: 236
Size: 259 Color: 73

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 433
Size: 305 Color: 240
Size: 260 Color: 79

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 342
Size: 343 Color: 304
Size: 290 Color: 201

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 446
Size: 290 Color: 202
Size: 260 Color: 77

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 403
Size: 296 Color: 221
Size: 296 Color: 219

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 341
Size: 340 Color: 299
Size: 294 Color: 213

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 385
Size: 341 Color: 300
Size: 265 Color: 104

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 477
Size: 264 Color: 101
Size: 259 Color: 72

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 316
Size: 350 Color: 314
Size: 299 Color: 224

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 380
Size: 325 Color: 277
Size: 283 Color: 179

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 423
Size: 300 Color: 230
Size: 271 Color: 129

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 461
Size: 277 Color: 161
Size: 258 Color: 70

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 483
Size: 262 Color: 87
Size: 258 Color: 69

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 431
Size: 300 Color: 227
Size: 267 Color: 116

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 422
Size: 289 Color: 198
Size: 282 Color: 175

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 443
Size: 295 Color: 217
Size: 256 Color: 52

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 450
Size: 295 Color: 216
Size: 251 Color: 22

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 475
Size: 272 Color: 137
Size: 253 Color: 39

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 395
Size: 306 Color: 244
Size: 291 Color: 208

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 387
Size: 352 Color: 319
Size: 253 Color: 37

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 370
Size: 355 Color: 323
Size: 263 Color: 89

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 409
Size: 296 Color: 220
Size: 288 Color: 192

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 349
Size: 340 Color: 298
Size: 291 Color: 209

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 373
Size: 335 Color: 291
Size: 280 Color: 169

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 476
Size: 266 Color: 109
Size: 257 Color: 64

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 498
Size: 253 Color: 36
Size: 250 Color: 7

Total size: 167000
Total free space: 0

