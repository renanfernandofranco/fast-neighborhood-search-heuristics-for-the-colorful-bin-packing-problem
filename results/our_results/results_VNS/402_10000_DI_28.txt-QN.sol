Capicity Bin: 8160
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 302
Size: 2804 Color: 248
Size: 136 Color: 12

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 317
Size: 1580 Color: 211
Size: 680 Color: 136

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6105 Color: 323
Size: 1361 Color: 199
Size: 694 Color: 138

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6246 Color: 329
Size: 1426 Color: 202
Size: 488 Color: 111

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6450 Color: 337
Size: 1414 Color: 200
Size: 296 Color: 76

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 345
Size: 1436 Color: 203
Size: 120 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 350
Size: 1036 Color: 178
Size: 376 Color: 92

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 351
Size: 858 Color: 160
Size: 552 Color: 119

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6753 Color: 352
Size: 1339 Color: 198
Size: 68 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6769 Color: 354
Size: 715 Color: 141
Size: 676 Color: 133

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6841 Color: 355
Size: 839 Color: 158
Size: 480 Color: 108

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 359
Size: 1082 Color: 182
Size: 196 Color: 48

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6890 Color: 360
Size: 822 Color: 156
Size: 448 Color: 107

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6937 Color: 363
Size: 921 Color: 169
Size: 302 Color: 78

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 368
Size: 764 Color: 149
Size: 352 Color: 89

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7081 Color: 371
Size: 695 Color: 139
Size: 384 Color: 96

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7101 Color: 374
Size: 883 Color: 165
Size: 176 Color: 38

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7130 Color: 376
Size: 822 Color: 155
Size: 208 Color: 52

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 378
Size: 668 Color: 129
Size: 342 Color: 88

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7168 Color: 380
Size: 892 Color: 166
Size: 100 Color: 6

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 384
Size: 700 Color: 140
Size: 270 Color: 71

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7205 Color: 386
Size: 797 Color: 152
Size: 158 Color: 28

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7245 Color: 388
Size: 763 Color: 148
Size: 152 Color: 23

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7246 Color: 389
Size: 684 Color: 137
Size: 230 Color: 58

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7248 Color: 390
Size: 722 Color: 142
Size: 190 Color: 45

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7249 Color: 391
Size: 761 Color: 146
Size: 150 Color: 21

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 392
Size: 576 Color: 122
Size: 334 Color: 86

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7282 Color: 394
Size: 678 Color: 135
Size: 200 Color: 49

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7283 Color: 395
Size: 731 Color: 143
Size: 146 Color: 20

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7322 Color: 398
Size: 606 Color: 125
Size: 232 Color: 62

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 399
Size: 676 Color: 134
Size: 160 Color: 30

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7330 Color: 401
Size: 664 Color: 128
Size: 166 Color: 33

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7340 Color: 402
Size: 668 Color: 131
Size: 152 Color: 25

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 5245 Color: 303
Size: 2778 Color: 246
Size: 136 Color: 11

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 6145 Color: 325
Size: 2014 Color: 229

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 327
Size: 1420 Color: 201
Size: 554 Color: 120

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 6466 Color: 339
Size: 1693 Color: 218

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 6561 Color: 343
Size: 1598 Color: 213

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6581 Color: 344
Size: 1578 Color: 210

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 347
Size: 1517 Color: 207
Size: 24 Color: 0

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 353
Size: 879 Color: 163
Size: 518 Color: 115

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 7183 Color: 383
Size: 976 Color: 173

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 7327 Color: 400
Size: 832 Color: 157

Bin 44: 2 of cap free
Amount of items: 5
Items: 
Size: 4130 Color: 281
Size: 3362 Color: 260
Size: 284 Color: 75
Size: 192 Color: 46
Size: 190 Color: 44

Bin 45: 2 of cap free
Amount of items: 5
Items: 
Size: 4138 Color: 282
Size: 3386 Color: 261
Size: 266 Color: 70
Size: 184 Color: 43
Size: 184 Color: 42

Bin 46: 2 of cap free
Amount of items: 5
Items: 
Size: 4146 Color: 283
Size: 3394 Color: 262
Size: 256 Color: 68
Size: 184 Color: 40
Size: 178 Color: 39

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 290
Size: 3346 Color: 258
Size: 160 Color: 29

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 4804 Color: 293
Size: 3354 Color: 259

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 5596 Color: 309
Size: 2426 Color: 238
Size: 136 Color: 8

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 5779 Color: 314
Size: 1497 Color: 206
Size: 882 Color: 164

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6018 Color: 321
Size: 2140 Color: 232

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6606 Color: 346
Size: 1552 Color: 209

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 373
Size: 1066 Color: 181

Bin 54: 3 of cap free
Amount of items: 2
Items: 
Size: 6444 Color: 336
Size: 1713 Color: 219

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 6620 Color: 348
Size: 1537 Color: 208

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 6924 Color: 362
Size: 1233 Color: 191

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 7013 Color: 367
Size: 1144 Color: 186

Bin 58: 4 of cap free
Amount of items: 3
Items: 
Size: 5088 Color: 299
Size: 2924 Color: 252
Size: 144 Color: 15

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 310
Size: 2452 Color: 240
Size: 98 Color: 5

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 6990 Color: 366
Size: 1166 Color: 188

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 7055 Color: 370
Size: 1101 Color: 185

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 7294 Color: 396
Size: 862 Color: 162

Bin 63: 5 of cap free
Amount of items: 5
Items: 
Size: 4822 Color: 294
Size: 2777 Color: 245
Size: 248 Color: 64
Size: 156 Color: 26
Size: 152 Color: 24

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 322
Size: 1937 Color: 227
Size: 184 Color: 41

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 6381 Color: 335
Size: 1774 Color: 220

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 7303 Color: 397
Size: 852 Color: 159

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 6169 Color: 326
Size: 1985 Color: 228

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 6270 Color: 332
Size: 1884 Color: 225

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 6854 Color: 357
Size: 1300 Color: 196

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 7174 Color: 382
Size: 980 Color: 175

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 7222 Color: 387
Size: 932 Color: 171

Bin 72: 7 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 375
Size: 1051 Color: 179

Bin 73: 7 of cap free
Amount of items: 2
Items: 
Size: 7132 Color: 377
Size: 1021 Color: 177

Bin 74: 7 of cap free
Amount of items: 2
Items: 
Size: 7155 Color: 379
Size: 998 Color: 176

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 7196 Color: 385
Size: 957 Color: 172

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 7252 Color: 393
Size: 901 Color: 167

Bin 77: 8 of cap free
Amount of items: 3
Items: 
Size: 4180 Color: 285
Size: 3796 Color: 268
Size: 176 Color: 36

Bin 78: 8 of cap free
Amount of items: 3
Items: 
Size: 5121 Color: 301
Size: 2893 Color: 250
Size: 138 Color: 13

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 5746 Color: 313
Size: 2406 Color: 237

Bin 80: 8 of cap free
Amount of items: 2
Items: 
Size: 6505 Color: 340
Size: 1647 Color: 215

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 358
Size: 1286 Color: 194

Bin 82: 9 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 319
Size: 2209 Color: 234
Size: 40 Color: 1

Bin 83: 9 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 331
Size: 1883 Color: 224

Bin 84: 9 of cap free
Amount of items: 2
Items: 
Size: 6365 Color: 334
Size: 1786 Color: 221

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 369
Size: 1100 Color: 184

Bin 86: 10 of cap free
Amount of items: 2
Items: 
Size: 7172 Color: 381
Size: 978 Color: 174

Bin 87: 11 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 312
Size: 1483 Color: 205
Size: 926 Color: 170

Bin 88: 11 of cap free
Amount of items: 2
Items: 
Size: 6129 Color: 324
Size: 2020 Color: 230

Bin 89: 11 of cap free
Amount of items: 2
Items: 
Size: 6988 Color: 365
Size: 1161 Color: 187

Bin 90: 12 of cap free
Amount of items: 5
Items: 
Size: 4114 Color: 280
Size: 3342 Color: 257
Size: 298 Color: 77
Size: 202 Color: 50
Size: 192 Color: 47

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 7086 Color: 372
Size: 1062 Color: 180

Bin 92: 13 of cap free
Amount of items: 5
Items: 
Size: 4085 Color: 276
Size: 1661 Color: 216
Size: 1333 Color: 197
Size: 860 Color: 161
Size: 208 Color: 51

Bin 93: 13 of cap free
Amount of items: 2
Items: 
Size: 5861 Color: 316
Size: 2286 Color: 236

Bin 94: 14 of cap free
Amount of items: 7
Items: 
Size: 4082 Color: 274
Size: 918 Color: 168
Size: 810 Color: 154
Size: 804 Color: 153
Size: 782 Color: 151
Size: 534 Color: 117
Size: 216 Color: 55

Bin 95: 14 of cap free
Amount of items: 2
Items: 
Size: 6681 Color: 349
Size: 1465 Color: 204

Bin 96: 14 of cap free
Amount of items: 2
Items: 
Size: 6966 Color: 364
Size: 1180 Color: 190

Bin 97: 15 of cap free
Amount of items: 3
Items: 
Size: 5626 Color: 311
Size: 2431 Color: 239
Size: 88 Color: 4

Bin 98: 15 of cap free
Amount of items: 2
Items: 
Size: 6228 Color: 328
Size: 1917 Color: 226

Bin 99: 16 of cap free
Amount of items: 2
Items: 
Size: 5250 Color: 304
Size: 2894 Color: 251

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 6262 Color: 330
Size: 1882 Color: 223

Bin 101: 18 of cap free
Amount of items: 2
Items: 
Size: 6012 Color: 320
Size: 2130 Color: 231

Bin 102: 18 of cap free
Amount of items: 2
Items: 
Size: 6560 Color: 342
Size: 1582 Color: 212

Bin 103: 18 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 356
Size: 1298 Color: 195

Bin 104: 19 of cap free
Amount of items: 2
Items: 
Size: 5581 Color: 308
Size: 2560 Color: 242

Bin 105: 19 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 338
Size: 1681 Color: 217

Bin 106: 19 of cap free
Amount of items: 2
Items: 
Size: 6529 Color: 341
Size: 1612 Color: 214

Bin 107: 19 of cap free
Amount of items: 2
Items: 
Size: 6901 Color: 361
Size: 1240 Color: 192

Bin 108: 21 of cap free
Amount of items: 3
Items: 
Size: 4154 Color: 284
Size: 3809 Color: 269
Size: 176 Color: 37

Bin 109: 22 of cap free
Amount of items: 2
Items: 
Size: 4106 Color: 279
Size: 4032 Color: 272

Bin 110: 23 of cap free
Amount of items: 2
Items: 
Size: 6341 Color: 333
Size: 1796 Color: 222

Bin 111: 31 of cap free
Amount of items: 3
Items: 
Size: 4945 Color: 297
Size: 3040 Color: 254
Size: 144 Color: 18

Bin 112: 39 of cap free
Amount of items: 2
Items: 
Size: 5837 Color: 315
Size: 2284 Color: 235

Bin 113: 48 of cap free
Amount of items: 2
Items: 
Size: 5511 Color: 307
Size: 2601 Color: 243

Bin 114: 49 of cap free
Amount of items: 4
Items: 
Size: 5041 Color: 298
Size: 2782 Color: 247
Size: 144 Color: 17
Size: 144 Color: 16

Bin 115: 52 of cap free
Amount of items: 17
Items: 
Size: 668 Color: 130
Size: 638 Color: 127
Size: 608 Color: 126
Size: 584 Color: 124
Size: 578 Color: 123
Size: 556 Color: 121
Size: 552 Color: 118
Size: 528 Color: 116
Size: 512 Color: 114
Size: 506 Color: 113
Size: 496 Color: 112
Size: 484 Color: 110
Size: 484 Color: 109
Size: 232 Color: 61
Size: 232 Color: 60
Size: 232 Color: 59
Size: 218 Color: 57

Bin 116: 63 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 306
Size: 2533 Color: 241
Size: 136 Color: 9

Bin 117: 66 of cap free
Amount of items: 3
Items: 
Size: 5901 Color: 318
Size: 2151 Color: 233
Size: 42 Color: 2

Bin 118: 67 of cap free
Amount of items: 2
Items: 
Size: 4689 Color: 291
Size: 3404 Color: 265

Bin 119: 69 of cap free
Amount of items: 3
Items: 
Size: 5274 Color: 305
Size: 2681 Color: 244
Size: 136 Color: 10

Bin 120: 77 of cap free
Amount of items: 3
Items: 
Size: 4521 Color: 289
Size: 3402 Color: 264
Size: 160 Color: 31

Bin 121: 90 of cap free
Amount of items: 3
Items: 
Size: 5100 Color: 300
Size: 2828 Color: 249
Size: 142 Color: 14

Bin 122: 92 of cap free
Amount of items: 3
Items: 
Size: 4332 Color: 287
Size: 3568 Color: 267
Size: 168 Color: 34

Bin 123: 100 of cap free
Amount of items: 6
Items: 
Size: 4084 Color: 275
Size: 1284 Color: 193
Size: 1178 Color: 189
Size: 1090 Color: 183
Size: 212 Color: 54
Size: 212 Color: 53

Bin 124: 110 of cap free
Amount of items: 3
Items: 
Size: 4830 Color: 296
Size: 3076 Color: 255
Size: 144 Color: 19

Bin 125: 112 of cap free
Amount of items: 23
Items: 
Size: 448 Color: 106
Size: 440 Color: 105
Size: 428 Color: 104
Size: 424 Color: 103
Size: 424 Color: 102
Size: 424 Color: 101
Size: 400 Color: 100
Size: 400 Color: 99
Size: 396 Color: 98
Size: 386 Color: 97
Size: 382 Color: 95
Size: 376 Color: 94
Size: 376 Color: 93
Size: 312 Color: 80
Size: 312 Color: 79
Size: 280 Color: 74
Size: 280 Color: 73
Size: 280 Color: 72
Size: 266 Color: 69
Size: 256 Color: 67
Size: 256 Color: 66
Size: 256 Color: 65
Size: 246 Color: 63

Bin 126: 113 of cap free
Amount of items: 3
Items: 
Size: 4690 Color: 292
Size: 3201 Color: 256
Size: 156 Color: 27

Bin 127: 119 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 288
Size: 3401 Color: 263
Size: 164 Color: 32

Bin 128: 123 of cap free
Amount of items: 3
Items: 
Size: 4321 Color: 286
Size: 3548 Color: 266
Size: 168 Color: 35

Bin 129: 136 of cap free
Amount of items: 2
Items: 
Size: 4098 Color: 278
Size: 3926 Color: 271

Bin 130: 146 of cap free
Amount of items: 3
Items: 
Size: 4829 Color: 295
Size: 3033 Color: 253
Size: 152 Color: 22

Bin 131: 152 of cap free
Amount of items: 2
Items: 
Size: 4090 Color: 277
Size: 3918 Color: 270

Bin 132: 185 of cap free
Amount of items: 7
Items: 
Size: 4081 Color: 273
Size: 768 Color: 150
Size: 762 Color: 147
Size: 738 Color: 145
Size: 734 Color: 144
Size: 676 Color: 132
Size: 216 Color: 56

Bin 133: 5504 of cap free
Amount of items: 8
Items: 
Size: 356 Color: 91
Size: 352 Color: 90
Size: 338 Color: 87
Size: 330 Color: 85
Size: 328 Color: 84
Size: 320 Color: 83
Size: 316 Color: 82
Size: 316 Color: 81

Total size: 1077120
Total free space: 8160

