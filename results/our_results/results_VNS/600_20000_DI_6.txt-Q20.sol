Capicity Bin: 13392
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 23
Items: 
Size: 752 Color: 17
Size: 736 Color: 5
Size: 712 Color: 16
Size: 712 Color: 7
Size: 708 Color: 16
Size: 704 Color: 13
Size: 672 Color: 18
Size: 644 Color: 6
Size: 640 Color: 9
Size: 640 Color: 0
Size: 616 Color: 15
Size: 616 Color: 13
Size: 608 Color: 16
Size: 592 Color: 10
Size: 576 Color: 14
Size: 560 Color: 0
Size: 546 Color: 9
Size: 544 Color: 19
Size: 536 Color: 5
Size: 464 Color: 11
Size: 368 Color: 1
Size: 230 Color: 17
Size: 216 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 7530 Color: 13
Size: 5578 Color: 7
Size: 284 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 13
Size: 4108 Color: 13
Size: 524 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 3
Size: 4132 Color: 16
Size: 244 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9519 Color: 18
Size: 3551 Color: 17
Size: 322 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9554 Color: 4
Size: 3402 Color: 11
Size: 436 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9578 Color: 9
Size: 3564 Color: 8
Size: 250 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10124 Color: 16
Size: 2840 Color: 13
Size: 428 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10132 Color: 9
Size: 3092 Color: 2
Size: 168 Color: 16

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 12
Size: 2886 Color: 17
Size: 200 Color: 12

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10367 Color: 15
Size: 2667 Color: 5
Size: 358 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10424 Color: 7
Size: 2124 Color: 17
Size: 844 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 10470 Color: 12
Size: 2642 Color: 5
Size: 280 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 10524 Color: 8
Size: 2340 Color: 1
Size: 528 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 10664 Color: 1
Size: 2438 Color: 10
Size: 290 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 10770 Color: 15
Size: 1874 Color: 14
Size: 748 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 10778 Color: 1
Size: 2182 Color: 18
Size: 432 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 10876 Color: 19
Size: 1716 Color: 4
Size: 800 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 10933 Color: 17
Size: 2297 Color: 7
Size: 162 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 16
Size: 1234 Color: 4
Size: 1124 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11146 Color: 19
Size: 1124 Color: 15
Size: 1122 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11165 Color: 15
Size: 1857 Color: 14
Size: 370 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11177 Color: 11
Size: 1319 Color: 16
Size: 896 Color: 14

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11237 Color: 12
Size: 1763 Color: 0
Size: 392 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11306 Color: 4
Size: 1806 Color: 3
Size: 280 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11318 Color: 11
Size: 1112 Color: 18
Size: 962 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 17
Size: 1588 Color: 4
Size: 472 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11382 Color: 3
Size: 1198 Color: 13
Size: 812 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11391 Color: 8
Size: 1617 Color: 9
Size: 384 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11429 Color: 18
Size: 1583 Color: 4
Size: 380 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11457 Color: 1
Size: 1597 Color: 9
Size: 338 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11517 Color: 3
Size: 1563 Color: 13
Size: 312 Color: 10

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11524 Color: 4
Size: 1288 Color: 17
Size: 580 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11544 Color: 18
Size: 1364 Color: 11
Size: 484 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11624 Color: 9
Size: 1352 Color: 16
Size: 416 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11625 Color: 0
Size: 1473 Color: 17
Size: 294 Color: 5

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11628 Color: 0
Size: 1204 Color: 3
Size: 560 Color: 10

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11694 Color: 10
Size: 854 Color: 1
Size: 844 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11707 Color: 0
Size: 1253 Color: 10
Size: 432 Color: 7

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11749 Color: 10
Size: 1371 Color: 11
Size: 272 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 11745 Color: 16
Size: 1405 Color: 4
Size: 242 Color: 8

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 11796 Color: 8
Size: 952 Color: 11
Size: 644 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 11802 Color: 1
Size: 1158 Color: 17
Size: 432 Color: 16

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 11808 Color: 17
Size: 952 Color: 9
Size: 632 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 11841 Color: 12
Size: 1247 Color: 15
Size: 304 Color: 18

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 11845 Color: 10
Size: 1215 Color: 4
Size: 332 Color: 6

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 11884 Color: 5
Size: 1164 Color: 2
Size: 344 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 11897 Color: 14
Size: 1183 Color: 4
Size: 312 Color: 11

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 11914 Color: 14
Size: 1362 Color: 9
Size: 116 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 11958 Color: 12
Size: 1142 Color: 0
Size: 292 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 11959 Color: 6
Size: 1351 Color: 18
Size: 82 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12006 Color: 17
Size: 1114 Color: 14
Size: 272 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12022 Color: 5
Size: 1114 Color: 8
Size: 256 Color: 17

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12038 Color: 5
Size: 1130 Color: 14
Size: 224 Color: 9

Bin 55: 1 of cap free
Amount of items: 10
Items: 
Size: 6697 Color: 4
Size: 928 Color: 8
Size: 860 Color: 15
Size: 832 Color: 3
Size: 824 Color: 14
Size: 816 Color: 1
Size: 758 Color: 5
Size: 720 Color: 13
Size: 504 Color: 11
Size: 452 Color: 10

Bin 56: 1 of cap free
Amount of items: 4
Items: 
Size: 6732 Color: 1
Size: 5571 Color: 13
Size: 864 Color: 7
Size: 224 Color: 6

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 9463 Color: 8
Size: 3556 Color: 8
Size: 372 Color: 6

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 10550 Color: 7
Size: 1669 Color: 13
Size: 1172 Color: 18

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 10637 Color: 0
Size: 1986 Color: 17
Size: 768 Color: 14

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11103 Color: 4
Size: 2048 Color: 7
Size: 240 Color: 9

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 11340 Color: 6
Size: 2051 Color: 15

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 11425 Color: 12
Size: 1966 Color: 9

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11448 Color: 13
Size: 1847 Color: 5
Size: 96 Color: 16

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11473 Color: 9
Size: 1282 Color: 15
Size: 636 Color: 14

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 11594 Color: 2
Size: 1797 Color: 0

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11721 Color: 1
Size: 1446 Color: 2
Size: 224 Color: 9

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11889 Color: 9
Size: 1176 Color: 15
Size: 326 Color: 16

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 11973 Color: 0
Size: 730 Color: 8
Size: 688 Color: 9

Bin 69: 2 of cap free
Amount of items: 7
Items: 
Size: 6700 Color: 13
Size: 1293 Color: 6
Size: 1260 Color: 8
Size: 1224 Color: 7
Size: 1157 Color: 18
Size: 1112 Color: 18
Size: 644 Color: 8

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 6712 Color: 11
Size: 5564 Color: 5
Size: 1114 Color: 10

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 7534 Color: 2
Size: 5856 Color: 0

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 9426 Color: 19
Size: 2271 Color: 19
Size: 1693 Color: 17

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 9506 Color: 14
Size: 3596 Color: 9
Size: 288 Color: 2

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 9809 Color: 11
Size: 3229 Color: 7
Size: 352 Color: 18

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 9930 Color: 7
Size: 3064 Color: 3
Size: 396 Color: 8

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 10164 Color: 13
Size: 3226 Color: 6

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 5
Size: 2100 Color: 12
Size: 360 Color: 10

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 15
Size: 1972 Color: 12
Size: 326 Color: 16

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 11124 Color: 18
Size: 2266 Color: 8

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11633 Color: 9
Size: 1467 Color: 14
Size: 290 Color: 16

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 11817 Color: 4
Size: 1573 Color: 1

Bin 82: 3 of cap free
Amount of items: 5
Items: 
Size: 7976 Color: 9
Size: 3081 Color: 1
Size: 1438 Color: 8
Size: 654 Color: 3
Size: 240 Color: 12

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 10669 Color: 6
Size: 2424 Color: 9
Size: 296 Color: 7

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 10813 Color: 15
Size: 2232 Color: 7
Size: 344 Color: 12

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 11948 Color: 4
Size: 1441 Color: 14

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 12005 Color: 18
Size: 1384 Color: 12

Bin 87: 4 of cap free
Amount of items: 7
Items: 
Size: 6701 Color: 14
Size: 1324 Color: 2
Size: 1313 Color: 10
Size: 1302 Color: 12
Size: 1112 Color: 1
Size: 976 Color: 3
Size: 660 Color: 17

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 7614 Color: 5
Size: 5512 Color: 14
Size: 262 Color: 11

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 9464 Color: 15
Size: 3084 Color: 11
Size: 840 Color: 15

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 10113 Color: 11
Size: 2987 Color: 4
Size: 288 Color: 17

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 10226 Color: 12
Size: 2818 Color: 10
Size: 344 Color: 7

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 10536 Color: 0
Size: 2724 Color: 7
Size: 128 Color: 14

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 3
Size: 1622 Color: 9
Size: 304 Color: 13

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 11492 Color: 10
Size: 1896 Color: 0

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 11740 Color: 6
Size: 1648 Color: 10

Bin 96: 5 of cap free
Amount of items: 4
Items: 
Size: 6936 Color: 5
Size: 5577 Color: 0
Size: 472 Color: 12
Size: 402 Color: 3

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 11657 Color: 3
Size: 1730 Color: 18

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 11758 Color: 18
Size: 1373 Color: 19
Size: 256 Color: 9

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 5
Size: 1459 Color: 4

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 11935 Color: 17
Size: 1452 Color: 19

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 11956 Color: 12
Size: 1431 Color: 6

Bin 102: 6 of cap free
Amount of items: 5
Items: 
Size: 6705 Color: 4
Size: 2392 Color: 8
Size: 2125 Color: 11
Size: 1204 Color: 10
Size: 960 Color: 12

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 8306 Color: 2
Size: 4728 Color: 14
Size: 352 Color: 7

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 8330 Color: 15
Size: 4520 Color: 2
Size: 536 Color: 10

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 11016 Color: 11
Size: 2370 Color: 3

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 11477 Color: 19
Size: 1909 Color: 6

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 11570 Color: 16
Size: 1816 Color: 1

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 11864 Color: 1
Size: 1522 Color: 0

Bin 109: 7 of cap free
Amount of items: 3
Items: 
Size: 8890 Color: 16
Size: 4275 Color: 4
Size: 220 Color: 2

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 9116 Color: 13
Size: 4061 Color: 2
Size: 208 Color: 1

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 9911 Color: 16
Size: 3242 Color: 18
Size: 232 Color: 9

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 16
Size: 3275 Color: 14
Size: 118 Color: 2

Bin 113: 7 of cap free
Amount of items: 2
Items: 
Size: 10097 Color: 5
Size: 3288 Color: 19

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 11643 Color: 3
Size: 1742 Color: 17

Bin 115: 7 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 6
Size: 1601 Color: 0

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 11992 Color: 2
Size: 1393 Color: 11

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 12004 Color: 7
Size: 1381 Color: 0

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 10824 Color: 0
Size: 2560 Color: 17

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 12052 Color: 13
Size: 1332 Color: 4

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 7513 Color: 11
Size: 5581 Color: 8
Size: 288 Color: 4

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 7617 Color: 2
Size: 5573 Color: 10
Size: 192 Color: 2

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 9652 Color: 9
Size: 3306 Color: 2
Size: 424 Color: 18

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 10180 Color: 16
Size: 3202 Color: 15

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 11769 Color: 12
Size: 1613 Color: 18

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 9720 Color: 10
Size: 3661 Color: 17

Bin 126: 12 of cap free
Amount of items: 4
Items: 
Size: 8204 Color: 0
Size: 4504 Color: 15
Size: 512 Color: 7
Size: 160 Color: 19

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 10556 Color: 12
Size: 2824 Color: 10

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 10688 Color: 12
Size: 2692 Color: 10

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 11015 Color: 4
Size: 2364 Color: 0

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 11226 Color: 6
Size: 2152 Color: 3

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 11737 Color: 3
Size: 1641 Color: 6

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 12050 Color: 3
Size: 1328 Color: 8

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 11453 Color: 16
Size: 1924 Color: 4

Bin 134: 15 of cap free
Amount of items: 2
Items: 
Size: 11493 Color: 0
Size: 1884 Color: 7

Bin 135: 15 of cap free
Amount of items: 2
Items: 
Size: 11833 Color: 13
Size: 1544 Color: 8

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 7992 Color: 6
Size: 5384 Color: 19

Bin 137: 16 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 14
Size: 4780 Color: 16
Size: 274 Color: 5

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 10014 Color: 14
Size: 3362 Color: 13

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 11361 Color: 6
Size: 2015 Color: 15

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 11652 Color: 19
Size: 1724 Color: 8

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 11812 Color: 2
Size: 1564 Color: 17

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 8552 Color: 1
Size: 4823 Color: 6

Bin 143: 17 of cap free
Amount of items: 2
Items: 
Size: 9133 Color: 8
Size: 4242 Color: 16

Bin 144: 17 of cap free
Amount of items: 2
Items: 
Size: 10193 Color: 16
Size: 3182 Color: 7

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 11224 Color: 9
Size: 2151 Color: 2

Bin 146: 17 of cap free
Amount of items: 2
Items: 
Size: 11873 Color: 4
Size: 1502 Color: 15

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 11670 Color: 4
Size: 1704 Color: 13

Bin 148: 18 of cap free
Amount of items: 2
Items: 
Size: 11764 Color: 12
Size: 1610 Color: 10

Bin 149: 19 of cap free
Amount of items: 2
Items: 
Size: 10852 Color: 17
Size: 2521 Color: 15

Bin 150: 19 of cap free
Amount of items: 2
Items: 
Size: 11736 Color: 11
Size: 1637 Color: 1

Bin 151: 20 of cap free
Amount of items: 3
Items: 
Size: 9084 Color: 5
Size: 4040 Color: 9
Size: 248 Color: 10

Bin 152: 20 of cap free
Amount of items: 2
Items: 
Size: 9716 Color: 3
Size: 3656 Color: 4

Bin 153: 21 of cap free
Amount of items: 2
Items: 
Size: 10975 Color: 16
Size: 2396 Color: 3

Bin 154: 22 of cap free
Amount of items: 2
Items: 
Size: 9684 Color: 1
Size: 3686 Color: 3

Bin 155: 22 of cap free
Amount of items: 2
Items: 
Size: 12044 Color: 7
Size: 1326 Color: 2

Bin 156: 24 of cap free
Amount of items: 2
Items: 
Size: 11988 Color: 10
Size: 1380 Color: 6

Bin 157: 25 of cap free
Amount of items: 3
Items: 
Size: 8841 Color: 12
Size: 4222 Color: 13
Size: 304 Color: 5

Bin 158: 30 of cap free
Amount of items: 8
Items: 
Size: 6698 Color: 16
Size: 1112 Color: 15
Size: 1112 Color: 4
Size: 1104 Color: 14
Size: 1104 Color: 12
Size: 1072 Color: 14
Size: 896 Color: 1
Size: 264 Color: 3

Bin 159: 33 of cap free
Amount of items: 2
Items: 
Size: 7669 Color: 14
Size: 5690 Color: 18

Bin 160: 33 of cap free
Amount of items: 2
Items: 
Size: 11277 Color: 15
Size: 2082 Color: 14

Bin 161: 34 of cap free
Amount of items: 2
Items: 
Size: 9132 Color: 19
Size: 4226 Color: 3

Bin 162: 34 of cap free
Amount of items: 2
Items: 
Size: 10457 Color: 15
Size: 2901 Color: 12

Bin 163: 34 of cap free
Amount of items: 2
Items: 
Size: 10674 Color: 9
Size: 2684 Color: 15

Bin 164: 37 of cap free
Amount of items: 2
Items: 
Size: 11010 Color: 6
Size: 2345 Color: 11

Bin 165: 38 of cap free
Amount of items: 2
Items: 
Size: 8468 Color: 15
Size: 4886 Color: 0

Bin 166: 42 of cap free
Amount of items: 2
Items: 
Size: 9358 Color: 6
Size: 3992 Color: 9

Bin 167: 42 of cap free
Amount of items: 2
Items: 
Size: 11726 Color: 4
Size: 1624 Color: 12

Bin 168: 45 of cap free
Amount of items: 3
Items: 
Size: 7660 Color: 5
Size: 5431 Color: 0
Size: 256 Color: 8

Bin 169: 46 of cap free
Amount of items: 3
Items: 
Size: 8279 Color: 10
Size: 4771 Color: 11
Size: 296 Color: 8

Bin 170: 48 of cap free
Amount of items: 2
Items: 
Size: 11352 Color: 0
Size: 1992 Color: 13

Bin 171: 50 of cap free
Amount of items: 2
Items: 
Size: 11140 Color: 10
Size: 2202 Color: 16

Bin 172: 52 of cap free
Amount of items: 2
Items: 
Size: 8519 Color: 18
Size: 4821 Color: 9

Bin 173: 52 of cap free
Amount of items: 2
Items: 
Size: 10216 Color: 3
Size: 3124 Color: 18

Bin 174: 54 of cap free
Amount of items: 2
Items: 
Size: 10754 Color: 9
Size: 2584 Color: 17

Bin 175: 55 of cap free
Amount of items: 3
Items: 
Size: 6709 Color: 16
Size: 5556 Color: 9
Size: 1072 Color: 8

Bin 176: 56 of cap free
Amount of items: 5
Items: 
Size: 6702 Color: 2
Size: 1981 Color: 10
Size: 1824 Color: 18
Size: 1476 Color: 6
Size: 1353 Color: 7

Bin 177: 56 of cap free
Amount of items: 2
Items: 
Size: 11854 Color: 7
Size: 1482 Color: 10

Bin 178: 57 of cap free
Amount of items: 2
Items: 
Size: 7736 Color: 4
Size: 5599 Color: 9

Bin 179: 60 of cap free
Amount of items: 2
Items: 
Size: 11450 Color: 10
Size: 1882 Color: 19

Bin 180: 67 of cap free
Amount of items: 2
Items: 
Size: 9001 Color: 14
Size: 4324 Color: 6

Bin 181: 68 of cap free
Amount of items: 2
Items: 
Size: 6716 Color: 12
Size: 6608 Color: 4

Bin 182: 69 of cap free
Amount of items: 2
Items: 
Size: 9530 Color: 7
Size: 3793 Color: 6

Bin 183: 71 of cap free
Amount of items: 2
Items: 
Size: 10588 Color: 17
Size: 2733 Color: 14

Bin 184: 78 of cap free
Amount of items: 2
Items: 
Size: 11128 Color: 14
Size: 2186 Color: 5

Bin 185: 82 of cap free
Amount of items: 2
Items: 
Size: 11830 Color: 8
Size: 1480 Color: 19

Bin 186: 88 of cap free
Amount of items: 3
Items: 
Size: 7128 Color: 5
Size: 5580 Color: 2
Size: 596 Color: 10

Bin 187: 89 of cap free
Amount of items: 2
Items: 
Size: 10579 Color: 3
Size: 2724 Color: 12

Bin 188: 90 of cap free
Amount of items: 2
Items: 
Size: 10728 Color: 5
Size: 2574 Color: 11

Bin 189: 102 of cap free
Amount of items: 2
Items: 
Size: 10843 Color: 2
Size: 2447 Color: 0

Bin 190: 110 of cap free
Amount of items: 2
Items: 
Size: 8970 Color: 14
Size: 4312 Color: 9

Bin 191: 116 of cap free
Amount of items: 2
Items: 
Size: 9522 Color: 19
Size: 3754 Color: 14

Bin 192: 118 of cap free
Amount of items: 2
Items: 
Size: 7692 Color: 2
Size: 5582 Color: 7

Bin 193: 118 of cap free
Amount of items: 2
Items: 
Size: 8968 Color: 10
Size: 4306 Color: 3

Bin 194: 132 of cap free
Amount of items: 36
Items: 
Size: 532 Color: 5
Size: 488 Color: 8
Size: 472 Color: 15
Size: 468 Color: 4
Size: 464 Color: 5
Size: 460 Color: 4
Size: 458 Color: 12
Size: 452 Color: 3
Size: 436 Color: 18
Size: 416 Color: 10
Size: 416 Color: 6
Size: 408 Color: 8
Size: 396 Color: 3
Size: 384 Color: 16
Size: 380 Color: 11
Size: 376 Color: 10
Size: 368 Color: 16
Size: 368 Color: 15
Size: 352 Color: 8
Size: 336 Color: 11
Size: 336 Color: 9
Size: 336 Color: 7
Size: 322 Color: 13
Size: 320 Color: 17
Size: 320 Color: 14
Size: 320 Color: 11
Size: 320 Color: 1
Size: 318 Color: 14
Size: 318 Color: 7
Size: 316 Color: 18
Size: 278 Color: 0
Size: 272 Color: 12
Size: 270 Color: 6
Size: 264 Color: 2
Size: 264 Color: 1
Size: 256 Color: 4

Bin 195: 138 of cap free
Amount of items: 2
Items: 
Size: 8436 Color: 17
Size: 4818 Color: 4

Bin 196: 153 of cap free
Amount of items: 2
Items: 
Size: 8263 Color: 5
Size: 4976 Color: 11

Bin 197: 154 of cap free
Amount of items: 2
Items: 
Size: 8226 Color: 17
Size: 5012 Color: 16

Bin 198: 207 of cap free
Amount of items: 2
Items: 
Size: 7609 Color: 2
Size: 5576 Color: 12

Bin 199: 9506 of cap free
Amount of items: 16
Items: 
Size: 300 Color: 19
Size: 274 Color: 0
Size: 272 Color: 9
Size: 260 Color: 12
Size: 258 Color: 12
Size: 248 Color: 4
Size: 240 Color: 15
Size: 236 Color: 13
Size: 236 Color: 7
Size: 232 Color: 6
Size: 228 Color: 11
Size: 228 Color: 7
Size: 224 Color: 13
Size: 220 Color: 3
Size: 216 Color: 1
Size: 214 Color: 2

Total size: 2651616
Total free space: 13392

