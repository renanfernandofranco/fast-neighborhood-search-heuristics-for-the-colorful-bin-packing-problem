Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 93
Size: 336 Color: 66
Size: 270 Color: 27

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 85
Size: 344 Color: 71
Size: 288 Color: 45

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 98
Size: 338 Color: 68
Size: 254 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 90
Size: 324 Color: 61
Size: 289 Color: 47

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 104
Size: 296 Color: 52
Size: 276 Color: 33

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 92
Size: 347 Color: 74
Size: 262 Color: 23

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 118
Size: 252 Color: 11
Size: 250 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 103
Size: 304 Color: 56
Size: 269 Color: 25

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 119
Size: 251 Color: 6
Size: 250 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 109
Size: 288 Color: 46
Size: 250 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 113
Size: 262 Color: 21
Size: 256 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 96
Size: 327 Color: 63
Size: 276 Color: 34

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 97
Size: 323 Color: 60
Size: 277 Color: 36

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 108
Size: 285 Color: 44
Size: 281 Color: 39

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 101
Size: 304 Color: 55
Size: 282 Color: 41

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 67
Size: 395 Color: 95
Size: 269 Color: 26

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 81
Size: 357 Color: 79
Size: 281 Color: 40

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 100
Size: 297 Color: 53
Size: 293 Color: 50

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 83
Size: 361 Color: 80
Size: 275 Color: 32

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 87
Size: 345 Color: 72
Size: 280 Color: 38

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 105
Size: 314 Color: 58
Size: 256 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 86
Size: 346 Color: 73
Size: 284 Color: 43

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 88
Size: 326 Color: 62
Size: 292 Color: 48

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 110
Size: 263 Color: 24
Size: 259 Color: 19

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 112
Size: 270 Color: 28
Size: 250 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 116
Size: 253 Color: 14
Size: 252 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 107
Size: 305 Color: 57
Size: 261 Color: 20

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 91
Size: 329 Color: 64
Size: 284 Color: 42

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 76
Size: 352 Color: 75
Size: 295 Color: 51

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 94
Size: 356 Color: 78
Size: 250 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 115
Size: 259 Color: 18
Size: 251 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 77
Size: 344 Color: 70
Size: 300 Color: 54

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 102
Size: 330 Color: 65
Size: 251 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 84
Size: 362 Color: 82
Size: 272 Color: 30

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 106
Size: 292 Color: 49
Size: 276 Color: 35

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 111
Size: 271 Color: 29
Size: 251 Color: 7

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 117
Size: 252 Color: 13
Size: 250 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 89
Size: 340 Color: 69
Size: 274 Color: 31

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 99
Size: 314 Color: 59
Size: 278 Color: 37

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 114
Size: 262 Color: 22
Size: 252 Color: 12

Total size: 40000
Total free space: 0

