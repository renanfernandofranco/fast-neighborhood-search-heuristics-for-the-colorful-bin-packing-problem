Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 13617 Color: 1
Size: 2227 Color: 1
Size: 444 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 14266 Color: 1
Size: 1622 Color: 1
Size: 400 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11816 Color: 1
Size: 3528 Color: 1
Size: 944 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 13712 Color: 1
Size: 2160 Color: 1
Size: 416 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11416 Color: 1
Size: 4520 Color: 1
Size: 352 Color: 0

Bin 6: 0 of cap free
Amount of items: 5
Items: 
Size: 5979 Color: 1
Size: 5851 Color: 1
Size: 3448 Color: 1
Size: 548 Color: 0
Size: 462 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 1
Size: 1922 Color: 1
Size: 422 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 1
Size: 2488 Color: 1
Size: 624 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13548 Color: 1
Size: 2232 Color: 1
Size: 508 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14059 Color: 1
Size: 1461 Color: 1
Size: 768 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14271 Color: 1
Size: 1547 Color: 1
Size: 470 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12797 Color: 1
Size: 2147 Color: 1
Size: 1344 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 1
Size: 6080 Color: 1
Size: 360 Color: 0

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 13355 Color: 1
Size: 2293 Color: 1
Size: 360 Color: 0
Size: 280 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 10938 Color: 1
Size: 4462 Color: 1
Size: 888 Color: 0

Bin 16: 0 of cap free
Amount of items: 5
Items: 
Size: 11527 Color: 1
Size: 2042 Color: 1
Size: 1791 Color: 1
Size: 464 Color: 0
Size: 464 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13814 Color: 1
Size: 1818 Color: 1
Size: 656 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 14598 Color: 1
Size: 1374 Color: 1
Size: 316 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12596 Color: 1
Size: 2894 Color: 1
Size: 798 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 1
Size: 1964 Color: 1
Size: 160 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8148 Color: 1
Size: 6828 Color: 1
Size: 1312 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13982 Color: 1
Size: 1738 Color: 1
Size: 568 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 9267 Color: 1
Size: 5021 Color: 1
Size: 2000 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13644 Color: 1
Size: 2284 Color: 1
Size: 360 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14456 Color: 1
Size: 1328 Color: 1
Size: 504 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13609 Color: 1
Size: 2321 Color: 1
Size: 358 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12557 Color: 1
Size: 3111 Color: 1
Size: 620 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13857 Color: 1
Size: 1911 Color: 1
Size: 520 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 1
Size: 1400 Color: 1
Size: 272 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11031 Color: 1
Size: 4413 Color: 1
Size: 844 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14559 Color: 1
Size: 1441 Color: 1
Size: 288 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 10188 Color: 1
Size: 5788 Color: 1
Size: 312 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 1
Size: 1394 Color: 1
Size: 370 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 10225 Color: 1
Size: 5053 Color: 1
Size: 1010 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13761 Color: 1
Size: 2247 Color: 1
Size: 280 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13008 Color: 1
Size: 2876 Color: 1
Size: 404 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13838 Color: 1
Size: 2084 Color: 1
Size: 366 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13682 Color: 1
Size: 2140 Color: 1
Size: 466 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 10297 Color: 1
Size: 4993 Color: 1
Size: 998 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12728 Color: 1
Size: 3176 Color: 1
Size: 384 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 1
Size: 1706 Color: 1
Size: 192 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 10953 Color: 1
Size: 4447 Color: 1
Size: 888 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14462 Color: 1
Size: 1522 Color: 1
Size: 304 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 11939 Color: 1
Size: 3969 Color: 1
Size: 380 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14131 Color: 1
Size: 1681 Color: 1
Size: 476 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 11562 Color: 1
Size: 3942 Color: 1
Size: 784 Color: 0

Bin 47: 0 of cap free
Amount of items: 5
Items: 
Size: 5782 Color: 1
Size: 5018 Color: 1
Size: 3142 Color: 1
Size: 1194 Color: 0
Size: 1152 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 11144 Color: 1
Size: 4600 Color: 1
Size: 544 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14340 Color: 1
Size: 1532 Color: 1
Size: 416 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14457 Color: 1
Size: 1527 Color: 1
Size: 304 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 1
Size: 6264 Color: 1
Size: 368 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 9731 Color: 1
Size: 5465 Color: 1
Size: 1092 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 9171 Color: 1
Size: 5931 Color: 1
Size: 1186 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13304 Color: 1
Size: 2408 Color: 1
Size: 576 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13310 Color: 1
Size: 2686 Color: 1
Size: 292 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 9276 Color: 1
Size: 6610 Color: 1
Size: 402 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12269 Color: 1
Size: 3351 Color: 1
Size: 668 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 1
Size: 3084 Color: 1
Size: 1352 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13970 Color: 1
Size: 1822 Color: 1
Size: 496 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13349 Color: 1
Size: 2451 Color: 1
Size: 488 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 1848 Color: 1
Size: 272 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13929 Color: 1
Size: 1967 Color: 1
Size: 392 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 8172 Color: 1
Size: 5044 Color: 1
Size: 3072 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 1
Size: 2086 Color: 1
Size: 900 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 1
Size: 2968 Color: 1
Size: 1152 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 10776 Color: 1
Size: 4792 Color: 1
Size: 720 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 1
Size: 1692 Color: 1
Size: 336 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 12090 Color: 1
Size: 3562 Color: 1
Size: 636 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 1
Size: 3528 Color: 1
Size: 128 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 14031 Color: 1
Size: 1899 Color: 1
Size: 358 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 14139 Color: 1
Size: 1565 Color: 1
Size: 584 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13151 Color: 1
Size: 2615 Color: 1
Size: 522 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 1
Size: 2532 Color: 1
Size: 672 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 14171 Color: 1
Size: 1765 Color: 1
Size: 352 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13788 Color: 1
Size: 2204 Color: 1
Size: 296 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 14355 Color: 1
Size: 1611 Color: 1
Size: 322 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 9432 Color: 1
Size: 5720 Color: 1
Size: 1136 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 11832 Color: 1
Size: 4028 Color: 1
Size: 428 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 1
Size: 1372 Color: 1
Size: 272 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 8147 Color: 1
Size: 6785 Color: 1
Size: 1356 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 14206 Color: 1
Size: 1502 Color: 1
Size: 580 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 14239 Color: 1
Size: 1585 Color: 1
Size: 464 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 12180 Color: 1
Size: 3636 Color: 1
Size: 472 Color: 0

Bin 84: 0 of cap free
Amount of items: 5
Items: 
Size: 12392 Color: 1
Size: 1860 Color: 1
Size: 1628 Color: 1
Size: 384 Color: 0
Size: 24 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 1
Size: 5084 Color: 1
Size: 274 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 12818 Color: 1
Size: 3194 Color: 1
Size: 276 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 14650 Color: 1
Size: 1366 Color: 1
Size: 272 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 1
Size: 6642 Color: 1
Size: 472 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 9113 Color: 1
Size: 6039 Color: 1
Size: 1136 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13252 Color: 1
Size: 2412 Color: 1
Size: 624 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 13997 Color: 1
Size: 1859 Color: 1
Size: 432 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 10145 Color: 1
Size: 5121 Color: 1
Size: 1022 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 1
Size: 2600 Color: 1
Size: 640 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 10236 Color: 1
Size: 5188 Color: 1
Size: 864 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 8146 Color: 1
Size: 6786 Color: 1
Size: 1356 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 8188 Color: 1
Size: 6780 Color: 1
Size: 1320 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 8145 Color: 1
Size: 6787 Color: 1
Size: 1356 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 9315 Color: 1
Size: 5811 Color: 1
Size: 1162 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 14038 Color: 1
Size: 1886 Color: 1
Size: 364 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 14234 Color: 1
Size: 1714 Color: 1
Size: 340 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 13537 Color: 1
Size: 2025 Color: 1
Size: 726 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 1
Size: 2746 Color: 1
Size: 304 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 8154 Color: 1
Size: 7322 Color: 1
Size: 812 Color: 0

Bin 104: 0 of cap free
Amount of items: 5
Items: 
Size: 9115 Color: 1
Size: 3345 Color: 1
Size: 2542 Color: 1
Size: 736 Color: 0
Size: 550 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 10866 Color: 1
Size: 4420 Color: 1
Size: 1002 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 1
Size: 6792 Color: 1
Size: 1248 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 11495 Color: 1
Size: 3625 Color: 1
Size: 1168 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 13066 Color: 1
Size: 2342 Color: 1
Size: 880 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 12983 Color: 1
Size: 2609 Color: 1
Size: 696 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 8048 Color: 1
Size: 7136 Color: 1
Size: 1104 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 12789 Color: 1
Size: 2917 Color: 1
Size: 582 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 1
Size: 3460 Color: 1
Size: 280 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 12458 Color: 1
Size: 2938 Color: 1
Size: 892 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 9123 Color: 1
Size: 6287 Color: 1
Size: 878 Color: 0

Bin 115: 0 of cap free
Amount of items: 5
Items: 
Size: 9348 Color: 1
Size: 3995 Color: 1
Size: 2113 Color: 1
Size: 528 Color: 0
Size: 304 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 1
Size: 1878 Color: 1
Size: 384 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 9723 Color: 1
Size: 5795 Color: 1
Size: 770 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 1
Size: 3506 Color: 1
Size: 700 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14502 Color: 1
Size: 1410 Color: 1
Size: 376 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 1
Size: 1528 Color: 1
Size: 688 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 1
Size: 4072 Color: 1
Size: 1344 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 11498 Color: 1
Size: 3994 Color: 1
Size: 796 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14492 Color: 1
Size: 992 Color: 1
Size: 804 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 1
Size: 1512 Color: 1
Size: 288 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 1
Size: 4296 Color: 1
Size: 896 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 8362 Color: 1
Size: 6756 Color: 1
Size: 1170 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 10552 Color: 1
Size: 5368 Color: 1
Size: 368 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14264 Color: 1
Size: 1688 Color: 1
Size: 336 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 1
Size: 2068 Color: 1
Size: 192 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 11966 Color: 1
Size: 3700 Color: 1
Size: 622 Color: 0

Bin 131: 0 of cap free
Amount of items: 5
Items: 
Size: 10265 Color: 1
Size: 3256 Color: 1
Size: 1819 Color: 1
Size: 708 Color: 0
Size: 240 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 1
Size: 1960 Color: 1
Size: 912 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 8872 Color: 1
Size: 6184 Color: 1
Size: 1232 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 11460 Color: 1
Size: 4466 Color: 1
Size: 362 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 10178 Color: 1
Size: 5094 Color: 1
Size: 1016 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 12072 Color: 1
Size: 3736 Color: 1
Size: 480 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 14396 Color: 1
Size: 1812 Color: 1
Size: 80 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 12018 Color: 1
Size: 3602 Color: 1
Size: 668 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 1
Size: 1500 Color: 1
Size: 336 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 13772 Color: 1
Size: 2208 Color: 1
Size: 308 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 9235 Color: 1
Size: 5879 Color: 1
Size: 1174 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 1
Size: 1570 Color: 1
Size: 608 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 9462 Color: 1
Size: 5690 Color: 1
Size: 1136 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 13841 Color: 1
Size: 2041 Color: 1
Size: 406 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 12292 Color: 1
Size: 3332 Color: 1
Size: 664 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 12766 Color: 1
Size: 2722 Color: 1
Size: 800 Color: 0

Bin 147: 1 of cap free
Amount of items: 4
Items: 
Size: 14091 Color: 1
Size: 1364 Color: 1
Size: 488 Color: 0
Size: 344 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 14116 Color: 1
Size: 1799 Color: 1
Size: 372 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 13489 Color: 1
Size: 2482 Color: 1
Size: 316 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 10988 Color: 1
Size: 4947 Color: 1
Size: 352 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 14433 Color: 1
Size: 1476 Color: 1
Size: 378 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 1
Size: 1891 Color: 1
Size: 456 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 14387 Color: 1
Size: 1580 Color: 1
Size: 320 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 12522 Color: 1
Size: 2749 Color: 1
Size: 1016 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 13873 Color: 1
Size: 2174 Color: 1
Size: 240 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 13396 Color: 1
Size: 2445 Color: 1
Size: 446 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 13713 Color: 1
Size: 2062 Color: 1
Size: 512 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 13418 Color: 1
Size: 2485 Color: 1
Size: 384 Color: 0

Bin 159: 1 of cap free
Amount of items: 3
Items: 
Size: 12991 Color: 1
Size: 2448 Color: 1
Size: 848 Color: 0

Bin 160: 1 of cap free
Amount of items: 3
Items: 
Size: 11453 Color: 1
Size: 4522 Color: 1
Size: 312 Color: 0

Bin 161: 1 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 1
Size: 2027 Color: 1
Size: 412 Color: 0

Bin 162: 1 of cap free
Amount of items: 3
Items: 
Size: 9318 Color: 1
Size: 6177 Color: 1
Size: 792 Color: 0

Bin 163: 1 of cap free
Amount of items: 3
Items: 
Size: 14107 Color: 1
Size: 1772 Color: 1
Size: 408 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 14618 Color: 1
Size: 944 Color: 0
Size: 724 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 1
Size: 3502 Color: 1
Size: 296 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 1
Size: 1768 Color: 1
Size: 458 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 13022 Color: 1
Size: 2528 Color: 1
Size: 736 Color: 0

Bin 168: 3 of cap free
Amount of items: 5
Items: 
Size: 10948 Color: 1
Size: 2712 Color: 1
Size: 1849 Color: 1
Size: 440 Color: 0
Size: 336 Color: 0

Bin 169: 3 of cap free
Amount of items: 3
Items: 
Size: 9331 Color: 1
Size: 6606 Color: 1
Size: 348 Color: 0

Bin 170: 3 of cap free
Amount of items: 3
Items: 
Size: 8776 Color: 1
Size: 6917 Color: 1
Size: 592 Color: 0

Bin 171: 4 of cap free
Amount of items: 3
Items: 
Size: 13630 Color: 1
Size: 1582 Color: 1
Size: 1072 Color: 0

Bin 172: 5 of cap free
Amount of items: 3
Items: 
Size: 12275 Color: 1
Size: 3720 Color: 1
Size: 288 Color: 0

Bin 173: 5 of cap free
Amount of items: 3
Items: 
Size: 13457 Color: 1
Size: 2394 Color: 1
Size: 432 Color: 0

Bin 174: 5 of cap free
Amount of items: 3
Items: 
Size: 13376 Color: 1
Size: 2755 Color: 1
Size: 152 Color: 0

Bin 175: 5 of cap free
Amount of items: 3
Items: 
Size: 13529 Color: 1
Size: 2218 Color: 1
Size: 536 Color: 0

Bin 176: 5 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 1
Size: 2911 Color: 1
Size: 528 Color: 0

Bin 177: 6 of cap free
Amount of items: 3
Items: 
Size: 11931 Color: 1
Size: 4031 Color: 1
Size: 320 Color: 0

Bin 178: 6 of cap free
Amount of items: 3
Items: 
Size: 13505 Color: 1
Size: 2361 Color: 1
Size: 416 Color: 0

Bin 179: 12 of cap free
Amount of items: 3
Items: 
Size: 12549 Color: 1
Size: 3631 Color: 1
Size: 96 Color: 0

Bin 180: 12 of cap free
Amount of items: 3
Items: 
Size: 8156 Color: 1
Size: 7384 Color: 1
Size: 736 Color: 0

Bin 181: 13 of cap free
Amount of items: 3
Items: 
Size: 13159 Color: 1
Size: 2676 Color: 1
Size: 440 Color: 0

Bin 182: 13 of cap free
Amount of items: 3
Items: 
Size: 14011 Color: 1
Size: 2040 Color: 1
Size: 224 Color: 0

Bin 183: 16 of cap free
Amount of items: 3
Items: 
Size: 13786 Color: 1
Size: 1686 Color: 1
Size: 800 Color: 0

Bin 184: 18 of cap free
Amount of items: 5
Items: 
Size: 9211 Color: 1
Size: 3117 Color: 1
Size: 1934 Color: 1
Size: 1008 Color: 0
Size: 1000 Color: 0

Bin 185: 19 of cap free
Amount of items: 3
Items: 
Size: 13624 Color: 1
Size: 2333 Color: 1
Size: 312 Color: 0

Bin 186: 20 of cap free
Amount of items: 3
Items: 
Size: 8152 Color: 1
Size: 6764 Color: 1
Size: 1352 Color: 0

Bin 187: 26 of cap free
Amount of items: 3
Items: 
Size: 13753 Color: 1
Size: 2013 Color: 1
Size: 496 Color: 0

Bin 188: 31 of cap free
Amount of items: 3
Items: 
Size: 10353 Color: 1
Size: 5528 Color: 1
Size: 376 Color: 0

Bin 189: 102 of cap free
Amount of items: 3
Items: 
Size: 9354 Color: 1
Size: 5844 Color: 1
Size: 988 Color: 0

Bin 190: 301 of cap free
Amount of items: 3
Items: 
Size: 13482 Color: 1
Size: 2233 Color: 1
Size: 272 Color: 0

Bin 191: 910 of cap free
Amount of items: 3
Items: 
Size: 10270 Color: 1
Size: 4328 Color: 1
Size: 780 Color: 0

Bin 192: 1636 of cap free
Amount of items: 1
Items: 
Size: 14652 Color: 1

Bin 193: 1646 of cap free
Amount of items: 1
Items: 
Size: 14642 Color: 1

Bin 194: 1753 of cap free
Amount of items: 1
Items: 
Size: 14535 Color: 1

Bin 195: 1798 of cap free
Amount of items: 1
Items: 
Size: 14490 Color: 1

Bin 196: 1877 of cap free
Amount of items: 1
Items: 
Size: 14411 Color: 1

Bin 197: 1882 of cap free
Amount of items: 1
Items: 
Size: 14406 Color: 1

Bin 198: 1942 of cap free
Amount of items: 1
Items: 
Size: 14346 Color: 1

Bin 199: 2186 of cap free
Amount of items: 1
Items: 
Size: 14102 Color: 1

Total size: 3225024
Total free space: 16288

