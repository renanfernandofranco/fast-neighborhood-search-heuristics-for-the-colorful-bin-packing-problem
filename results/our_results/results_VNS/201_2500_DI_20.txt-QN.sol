Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 145
Size: 1020 Color: 133
Size: 40 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 159
Size: 459 Color: 95
Size: 338 Color: 80

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 172
Size: 409 Color: 88
Size: 204 Color: 66

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 176
Size: 430 Color: 91
Size: 132 Color: 52

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1901 Color: 177
Size: 463 Color: 96
Size: 92 Color: 37

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 185
Size: 384 Color: 86
Size: 90 Color: 35

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 186
Size: 374 Color: 82
Size: 84 Color: 30

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2052 Color: 190
Size: 340 Color: 81
Size: 64 Color: 22

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 191
Size: 306 Color: 78
Size: 96 Color: 38

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 193
Size: 254 Color: 73
Size: 112 Color: 46

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2092 Color: 194
Size: 308 Color: 79
Size: 56 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 195
Size: 200 Color: 65
Size: 150 Color: 58

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 196
Size: 236 Color: 71
Size: 96 Color: 39

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 199
Size: 220 Color: 70
Size: 56 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 200
Size: 176 Color: 63
Size: 84 Color: 31

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 201
Size: 150 Color: 57
Size: 96 Color: 40

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 1536 Color: 151
Size: 919 Color: 129

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1708 Color: 164
Size: 747 Color: 118

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 166
Size: 690 Color: 115

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1851 Color: 173
Size: 484 Color: 97
Size: 120 Color: 49

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1884 Color: 175
Size: 571 Color: 106

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1907 Color: 178
Size: 548 Color: 105

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 187
Size: 406 Color: 87
Size: 48 Color: 13

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 2004 Color: 188
Size: 451 Color: 94

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 2074 Color: 192
Size: 381 Color: 85

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1391 Color: 144
Size: 1023 Color: 136
Size: 40 Color: 11

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 150
Size: 661 Color: 113
Size: 272 Color: 75

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 167
Size: 665 Color: 114
Size: 16 Color: 3

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 168
Size: 576 Color: 107
Size: 80 Color: 29

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2170 Color: 198
Size: 284 Color: 76

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1444 Color: 149
Size: 969 Color: 130
Size: 40 Color: 9

Bin 32: 3 of cap free
Amount of items: 4
Items: 
Size: 1665 Color: 160
Size: 724 Color: 116
Size: 32 Color: 6
Size: 32 Color: 5

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1722 Color: 165
Size: 731 Color: 117

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1835 Color: 171
Size: 618 Color: 110

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1915 Color: 179
Size: 538 Color: 104

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1923 Color: 180
Size: 530 Color: 103

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1934 Color: 181
Size: 519 Color: 102

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1942 Color: 182
Size: 511 Color: 101

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1948 Color: 183
Size: 505 Color: 100

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1967 Color: 184
Size: 486 Color: 98

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 2158 Color: 197
Size: 294 Color: 77

Bin 42: 5 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 143
Size: 1022 Color: 135
Size: 40 Color: 12

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1804 Color: 169
Size: 647 Color: 112

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1874 Color: 174
Size: 577 Color: 108

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 2006 Color: 189
Size: 445 Color: 93

Bin 46: 6 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 162
Size: 753 Color: 120
Size: 16 Color: 4

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 170
Size: 628 Color: 111
Size: 8 Color: 1

Bin 48: 7 of cap free
Amount of items: 5
Items: 
Size: 1238 Color: 142
Size: 1021 Color: 134
Size: 90 Color: 36
Size: 52 Color: 15
Size: 48 Color: 14

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 1688 Color: 163
Size: 761 Color: 121

Bin 50: 10 of cap free
Amount of items: 5
Items: 
Size: 1236 Color: 141
Size: 1018 Color: 132
Size: 88 Color: 34
Size: 52 Color: 17
Size: 52 Color: 16

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1553 Color: 153
Size: 891 Color: 128

Bin 52: 12 of cap free
Amount of items: 2
Items: 
Size: 1630 Color: 158
Size: 814 Color: 122

Bin 53: 17 of cap free
Amount of items: 5
Items: 
Size: 1231 Color: 140
Size: 504 Color: 99
Size: 438 Color: 92
Size: 206 Color: 69
Size: 60 Color: 20

Bin 54: 17 of cap free
Amount of items: 4
Items: 
Size: 1673 Color: 161
Size: 750 Color: 119
Size: 8 Color: 2
Size: 8 Color: 0

Bin 55: 18 of cap free
Amount of items: 2
Items: 
Size: 1410 Color: 146
Size: 1028 Color: 137

Bin 56: 19 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 155
Size: 844 Color: 124
Size: 32 Color: 7

Bin 57: 20 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 147
Size: 614 Color: 109
Size: 380 Color: 84

Bin 58: 20 of cap free
Amount of items: 2
Items: 
Size: 1590 Color: 157
Size: 846 Color: 126

Bin 59: 23 of cap free
Amount of items: 2
Items: 
Size: 1588 Color: 156
Size: 845 Color: 125

Bin 60: 26 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 154
Size: 840 Color: 123
Size: 32 Color: 8

Bin 61: 29 of cap free
Amount of items: 2
Items: 
Size: 1443 Color: 148
Size: 984 Color: 131

Bin 62: 37 of cap free
Amount of items: 2
Items: 
Size: 1545 Color: 152
Size: 874 Color: 127

Bin 63: 52 of cap free
Amount of items: 19
Items: 
Size: 200 Color: 64
Size: 172 Color: 62
Size: 168 Color: 61
Size: 168 Color: 60
Size: 168 Color: 59
Size: 148 Color: 56
Size: 148 Color: 55
Size: 144 Color: 54
Size: 136 Color: 53
Size: 130 Color: 51
Size: 128 Color: 50
Size: 120 Color: 48
Size: 100 Color: 41
Size: 88 Color: 33
Size: 88 Color: 32
Size: 80 Color: 28
Size: 74 Color: 27
Size: 72 Color: 26
Size: 72 Color: 25

Bin 64: 52 of cap free
Amount of items: 5
Items: 
Size: 1230 Color: 139
Size: 428 Color: 90
Size: 414 Color: 89
Size: 272 Color: 74
Size: 60 Color: 21

Bin 65: 63 of cap free
Amount of items: 7
Items: 
Size: 1229 Color: 138
Size: 378 Color: 83
Size: 250 Color: 72
Size: 204 Color: 68
Size: 204 Color: 67
Size: 64 Color: 24
Size: 64 Color: 23

Bin 66: 1930 of cap free
Amount of items: 5
Items: 
Size: 114 Color: 47
Size: 104 Color: 45
Size: 104 Color: 44
Size: 102 Color: 43
Size: 102 Color: 42

Total size: 159640
Total free space: 2456

