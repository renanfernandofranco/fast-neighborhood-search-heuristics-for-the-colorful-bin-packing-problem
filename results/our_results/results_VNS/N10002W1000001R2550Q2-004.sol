Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3337
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 403640 Color: 1
Size: 321860 Color: 0
Size: 274501 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 465575 Color: 0
Size: 270387 Color: 1
Size: 264039 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 415212 Color: 0
Size: 295689 Color: 1
Size: 289100 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 369256 Color: 1
Size: 317916 Color: 0
Size: 312829 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 459568 Color: 1
Size: 281724 Color: 1
Size: 258709 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 455561 Color: 0
Size: 278864 Color: 1
Size: 265576 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 398589 Color: 1
Size: 332823 Color: 0
Size: 268589 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 495857 Color: 0
Size: 253760 Color: 0
Size: 250384 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 429854 Color: 0
Size: 286619 Color: 0
Size: 283528 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 463084 Color: 1
Size: 273405 Color: 0
Size: 263512 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 459852 Color: 0
Size: 272386 Color: 1
Size: 267763 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 484218 Color: 0
Size: 258866 Color: 0
Size: 256917 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 386363 Color: 0
Size: 309604 Color: 1
Size: 304034 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 418239 Color: 0
Size: 306587 Color: 1
Size: 275175 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 425883 Color: 1
Size: 323849 Color: 1
Size: 250269 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 460660 Color: 0
Size: 276573 Color: 1
Size: 262768 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 452798 Color: 1
Size: 287586 Color: 0
Size: 259617 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 472905 Color: 0
Size: 266293 Color: 1
Size: 260803 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 479070 Color: 0
Size: 264066 Color: 1
Size: 256865 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 492157 Color: 0
Size: 253924 Color: 1
Size: 253920 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 470239 Color: 1
Size: 270751 Color: 0
Size: 259011 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 415932 Color: 1
Size: 330948 Color: 0
Size: 253121 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 399290 Color: 0
Size: 317178 Color: 0
Size: 283533 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 448291 Color: 0
Size: 292327 Color: 1
Size: 259383 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 405593 Color: 1
Size: 330334 Color: 1
Size: 264074 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 454180 Color: 1
Size: 286391 Color: 0
Size: 259430 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 469110 Color: 0
Size: 280792 Color: 0
Size: 250099 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 416591 Color: 0
Size: 317938 Color: 1
Size: 265472 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 441142 Color: 1
Size: 302739 Color: 0
Size: 256120 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 436580 Color: 1
Size: 290195 Color: 0
Size: 273226 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 449015 Color: 1
Size: 282870 Color: 1
Size: 268116 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 468193 Color: 0
Size: 266927 Color: 0
Size: 264881 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 381288 Color: 0
Size: 310948 Color: 1
Size: 307765 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 392241 Color: 1
Size: 355784 Color: 0
Size: 251976 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 498210 Color: 1
Size: 251446 Color: 1
Size: 250345 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 401930 Color: 0
Size: 344431 Color: 0
Size: 253640 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 388944 Color: 0
Size: 340541 Color: 1
Size: 270516 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 395226 Color: 1
Size: 349357 Color: 0
Size: 255418 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 458396 Color: 1
Size: 279012 Color: 1
Size: 262593 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 433010 Color: 0
Size: 294443 Color: 1
Size: 272548 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 396983 Color: 0
Size: 305244 Color: 1
Size: 297774 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 382257 Color: 0
Size: 361045 Color: 0
Size: 256699 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 373887 Color: 1
Size: 328852 Color: 1
Size: 297262 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 336556 Color: 0
Size: 335984 Color: 1
Size: 327461 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 468182 Color: 1
Size: 270796 Color: 1
Size: 261023 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 356920 Color: 0
Size: 346972 Color: 0
Size: 296109 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 397518 Color: 1
Size: 329272 Color: 0
Size: 273211 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 497378 Color: 0
Size: 251358 Color: 1
Size: 251265 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 495441 Color: 0
Size: 253943 Color: 0
Size: 250617 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 469224 Color: 1
Size: 272641 Color: 1
Size: 258136 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 464708 Color: 1
Size: 282691 Color: 1
Size: 252602 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 376275 Color: 1
Size: 343816 Color: 0
Size: 279910 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 431062 Color: 0
Size: 310364 Color: 1
Size: 258575 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 449937 Color: 1
Size: 284807 Color: 1
Size: 265257 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 388422 Color: 0
Size: 338721 Color: 0
Size: 272858 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 398509 Color: 0
Size: 312052 Color: 0
Size: 289440 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 420824 Color: 0
Size: 312825 Color: 1
Size: 266352 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 376408 Color: 0
Size: 348855 Color: 0
Size: 274738 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 419353 Color: 0
Size: 290600 Color: 0
Size: 290048 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 435343 Color: 1
Size: 301098 Color: 1
Size: 263560 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 383308 Color: 1
Size: 317682 Color: 0
Size: 299011 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 460870 Color: 1
Size: 288183 Color: 0
Size: 250948 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 433342 Color: 0
Size: 284608 Color: 1
Size: 282051 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 449511 Color: 1
Size: 282644 Color: 0
Size: 267846 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 386036 Color: 1
Size: 357302 Color: 0
Size: 256663 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 460830 Color: 0
Size: 279156 Color: 0
Size: 260015 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 371367 Color: 1
Size: 325698 Color: 0
Size: 302936 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 479742 Color: 1
Size: 262537 Color: 0
Size: 257722 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 373508 Color: 1
Size: 335128 Color: 1
Size: 291365 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 386300 Color: 0
Size: 312559 Color: 1
Size: 301142 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 471445 Color: 1
Size: 271685 Color: 1
Size: 256871 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 452031 Color: 1
Size: 288947 Color: 0
Size: 259023 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 494121 Color: 1
Size: 254167 Color: 0
Size: 251713 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 441735 Color: 1
Size: 307658 Color: 1
Size: 250608 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 450925 Color: 1
Size: 279378 Color: 0
Size: 269698 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 393196 Color: 1
Size: 305964 Color: 0
Size: 300841 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 379308 Color: 1
Size: 354276 Color: 1
Size: 266417 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 429171 Color: 1
Size: 305304 Color: 0
Size: 265526 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 445984 Color: 1
Size: 277979 Color: 1
Size: 276038 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 380602 Color: 1
Size: 337853 Color: 1
Size: 281546 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 398744 Color: 1
Size: 301596 Color: 0
Size: 299661 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 385196 Color: 0
Size: 331454 Color: 1
Size: 283351 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 398329 Color: 0
Size: 336926 Color: 1
Size: 264746 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 442174 Color: 1
Size: 286468 Color: 0
Size: 271359 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 401252 Color: 0
Size: 348526 Color: 1
Size: 250223 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 461563 Color: 1
Size: 275050 Color: 0
Size: 263388 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 421598 Color: 1
Size: 321165 Color: 1
Size: 257238 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 406073 Color: 1
Size: 301525 Color: 0
Size: 292403 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 437347 Color: 1
Size: 283990 Color: 1
Size: 278664 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 472104 Color: 0
Size: 267828 Color: 1
Size: 260069 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 376947 Color: 1
Size: 322711 Color: 1
Size: 300343 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 363045 Color: 1
Size: 332980 Color: 0
Size: 303976 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 355345 Color: 0
Size: 340090 Color: 1
Size: 304566 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 370257 Color: 0
Size: 369742 Color: 1
Size: 260002 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 426608 Color: 0
Size: 291327 Color: 1
Size: 282066 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 362329 Color: 0
Size: 347622 Color: 1
Size: 290050 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 432130 Color: 1
Size: 302921 Color: 1
Size: 264950 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 424238 Color: 0
Size: 296652 Color: 1
Size: 279111 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 433627 Color: 1
Size: 309640 Color: 0
Size: 256734 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 414352 Color: 0
Size: 310200 Color: 1
Size: 275449 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 485805 Color: 1
Size: 259826 Color: 0
Size: 254370 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 359793 Color: 0
Size: 325194 Color: 1
Size: 315014 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 487810 Color: 1
Size: 258871 Color: 0
Size: 253320 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 368457 Color: 1
Size: 333289 Color: 0
Size: 298255 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 379398 Color: 0
Size: 358751 Color: 0
Size: 261852 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 464851 Color: 1
Size: 270482 Color: 0
Size: 264668 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 423812 Color: 0
Size: 319225 Color: 0
Size: 256964 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 383453 Color: 1
Size: 352386 Color: 0
Size: 264162 Color: 1

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 382730 Color: 1
Size: 328778 Color: 0
Size: 288493 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 464355 Color: 0
Size: 279339 Color: 1
Size: 256307 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 393453 Color: 1
Size: 342381 Color: 0
Size: 264167 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 495685 Color: 0
Size: 253489 Color: 1
Size: 250827 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 371881 Color: 1
Size: 359687 Color: 0
Size: 268433 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 406134 Color: 1
Size: 305839 Color: 1
Size: 288028 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 487780 Color: 0
Size: 257712 Color: 1
Size: 254509 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 433988 Color: 1
Size: 302225 Color: 0
Size: 263788 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 438575 Color: 1
Size: 309012 Color: 0
Size: 252414 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 377618 Color: 0
Size: 324775 Color: 0
Size: 297608 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 425536 Color: 0
Size: 317741 Color: 0
Size: 256724 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 487705 Color: 1
Size: 256259 Color: 0
Size: 256037 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 433820 Color: 1
Size: 287582 Color: 1
Size: 278599 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 450313 Color: 0
Size: 281540 Color: 1
Size: 268148 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 454270 Color: 1
Size: 290859 Color: 0
Size: 254872 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 373525 Color: 0
Size: 358907 Color: 1
Size: 267569 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 388997 Color: 1
Size: 337652 Color: 1
Size: 273352 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 443016 Color: 0
Size: 280299 Color: 0
Size: 276686 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 432764 Color: 1
Size: 315772 Color: 1
Size: 251465 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 401720 Color: 0
Size: 326619 Color: 0
Size: 271662 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 484654 Color: 1
Size: 261462 Color: 1
Size: 253885 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 404695 Color: 1
Size: 309046 Color: 0
Size: 286260 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 351041 Color: 1
Size: 343003 Color: 0
Size: 305957 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 429403 Color: 0
Size: 297908 Color: 1
Size: 272690 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 495431 Color: 1
Size: 253095 Color: 1
Size: 251475 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 494075 Color: 1
Size: 255201 Color: 0
Size: 250725 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 372381 Color: 1
Size: 357247 Color: 1
Size: 270373 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 449592 Color: 1
Size: 285339 Color: 0
Size: 265070 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 419563 Color: 1
Size: 309896 Color: 1
Size: 270542 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 431944 Color: 1
Size: 315868 Color: 0
Size: 252189 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 448203 Color: 1
Size: 285787 Color: 0
Size: 266011 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 421016 Color: 1
Size: 303040 Color: 0
Size: 275945 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 433813 Color: 1
Size: 312539 Color: 0
Size: 253649 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 367936 Color: 1
Size: 317608 Color: 0
Size: 314457 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 362805 Color: 0
Size: 343778 Color: 1
Size: 293418 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 395380 Color: 0
Size: 352339 Color: 1
Size: 252282 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 404781 Color: 1
Size: 337556 Color: 0
Size: 257664 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 480644 Color: 0
Size: 269234 Color: 1
Size: 250123 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 423536 Color: 1
Size: 298380 Color: 0
Size: 278085 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 423356 Color: 0
Size: 325385 Color: 1
Size: 251260 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 409766 Color: 0
Size: 307963 Color: 0
Size: 282272 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 460610 Color: 0
Size: 279735 Color: 1
Size: 259656 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 386031 Color: 0
Size: 317308 Color: 1
Size: 296662 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 379380 Color: 1
Size: 341476 Color: 1
Size: 279145 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 379390 Color: 1
Size: 365101 Color: 0
Size: 255510 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 445531 Color: 1
Size: 301597 Color: 1
Size: 252873 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 417646 Color: 1
Size: 326467 Color: 0
Size: 255888 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 403573 Color: 1
Size: 339876 Color: 0
Size: 256552 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 475144 Color: 1
Size: 267442 Color: 1
Size: 257415 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 411785 Color: 0
Size: 334631 Color: 0
Size: 253585 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 457996 Color: 1
Size: 281245 Color: 1
Size: 260760 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 460739 Color: 1
Size: 273037 Color: 0
Size: 266225 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 403954 Color: 1
Size: 345853 Color: 0
Size: 250194 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 469370 Color: 1
Size: 268699 Color: 0
Size: 261932 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 379310 Color: 1
Size: 311087 Color: 1
Size: 309604 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 389952 Color: 0
Size: 317206 Color: 1
Size: 292843 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 422036 Color: 0
Size: 290689 Color: 0
Size: 287276 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 418474 Color: 1
Size: 293248 Color: 1
Size: 288279 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 480743 Color: 1
Size: 260882 Color: 0
Size: 258376 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 414741 Color: 1
Size: 310244 Color: 0
Size: 275016 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 429387 Color: 0
Size: 316949 Color: 1
Size: 253665 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 366498 Color: 1
Size: 318691 Color: 0
Size: 314812 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 491036 Color: 0
Size: 256666 Color: 1
Size: 252299 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 441377 Color: 1
Size: 290500 Color: 1
Size: 268124 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 486388 Color: 1
Size: 258722 Color: 0
Size: 254891 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 381125 Color: 0
Size: 352332 Color: 1
Size: 266544 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 391733 Color: 1
Size: 351447 Color: 0
Size: 256821 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 481723 Color: 1
Size: 259642 Color: 0
Size: 258636 Color: 1

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 371825 Color: 1
Size: 344105 Color: 0
Size: 284071 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 383824 Color: 0
Size: 356253 Color: 1
Size: 259924 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 417719 Color: 0
Size: 324274 Color: 1
Size: 258008 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 385905 Color: 0
Size: 337732 Color: 1
Size: 276364 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 396542 Color: 1
Size: 305422 Color: 1
Size: 298037 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 424160 Color: 1
Size: 322254 Color: 0
Size: 253587 Color: 1

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 493062 Color: 1
Size: 254460 Color: 0
Size: 252479 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 474250 Color: 0
Size: 269850 Color: 1
Size: 255901 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 406584 Color: 0
Size: 335800 Color: 1
Size: 257617 Color: 1

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 409223 Color: 1
Size: 310422 Color: 0
Size: 280356 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 477056 Color: 0
Size: 262970 Color: 0
Size: 259975 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 399549 Color: 0
Size: 302124 Color: 0
Size: 298328 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 452897 Color: 0
Size: 276658 Color: 1
Size: 270446 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 406106 Color: 1
Size: 316990 Color: 1
Size: 276905 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 371387 Color: 1
Size: 348601 Color: 0
Size: 280013 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 406182 Color: 0
Size: 337494 Color: 0
Size: 256325 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 436359 Color: 0
Size: 302154 Color: 1
Size: 261488 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 396835 Color: 1
Size: 318571 Color: 1
Size: 284595 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 386724 Color: 1
Size: 340457 Color: 1
Size: 272820 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 358756 Color: 0
Size: 331834 Color: 1
Size: 309411 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 372856 Color: 0
Size: 319517 Color: 1
Size: 307628 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 384489 Color: 1
Size: 315517 Color: 1
Size: 299995 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 355522 Color: 0
Size: 345229 Color: 0
Size: 299250 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 379176 Color: 1
Size: 332056 Color: 0
Size: 288769 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 402181 Color: 1
Size: 316205 Color: 0
Size: 281615 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 449764 Color: 0
Size: 277308 Color: 0
Size: 272929 Color: 1

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 405909 Color: 0
Size: 326021 Color: 0
Size: 268071 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 423604 Color: 0
Size: 308901 Color: 0
Size: 267496 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 488219 Color: 1
Size: 260400 Color: 0
Size: 251382 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 411143 Color: 0
Size: 330476 Color: 1
Size: 258382 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 448381 Color: 0
Size: 291342 Color: 0
Size: 260278 Color: 1

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 474492 Color: 1
Size: 262940 Color: 0
Size: 262569 Color: 1

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 380475 Color: 0
Size: 316132 Color: 1
Size: 303394 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 412558 Color: 0
Size: 327203 Color: 0
Size: 260240 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 466437 Color: 0
Size: 271086 Color: 0
Size: 262478 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 481948 Color: 1
Size: 263825 Color: 0
Size: 254228 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 393853 Color: 1
Size: 320968 Color: 0
Size: 285180 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 395601 Color: 0
Size: 307527 Color: 1
Size: 296873 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 437969 Color: 1
Size: 290321 Color: 0
Size: 271711 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 431100 Color: 1
Size: 300408 Color: 1
Size: 268493 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 384022 Color: 0
Size: 340785 Color: 1
Size: 275194 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 399052 Color: 1
Size: 337879 Color: 0
Size: 263070 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 387276 Color: 1
Size: 323174 Color: 1
Size: 289551 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 413257 Color: 1
Size: 315806 Color: 1
Size: 270938 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 460787 Color: 1
Size: 271475 Color: 0
Size: 267739 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 399800 Color: 1
Size: 325280 Color: 0
Size: 274921 Color: 1

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 411233 Color: 0
Size: 326683 Color: 1
Size: 262085 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 410450 Color: 1
Size: 334607 Color: 1
Size: 254944 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 385386 Color: 0
Size: 312613 Color: 1
Size: 302002 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 398333 Color: 1
Size: 304278 Color: 0
Size: 297390 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 461657 Color: 0
Size: 284310 Color: 0
Size: 254034 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 420603 Color: 1
Size: 302331 Color: 0
Size: 277067 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 385416 Color: 0
Size: 346388 Color: 1
Size: 268197 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 498450 Color: 1
Size: 251513 Color: 0
Size: 250038 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 375710 Color: 1
Size: 345721 Color: 0
Size: 278570 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 452445 Color: 1
Size: 296803 Color: 0
Size: 250753 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 340171 Color: 1
Size: 335414 Color: 0
Size: 324416 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 425960 Color: 0
Size: 308610 Color: 1
Size: 265431 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 415923 Color: 0
Size: 321657 Color: 0
Size: 262421 Color: 1

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 451234 Color: 1
Size: 283848 Color: 0
Size: 264919 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 396368 Color: 1
Size: 316701 Color: 1
Size: 286932 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 488627 Color: 0
Size: 257992 Color: 1
Size: 253382 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 373843 Color: 1
Size: 350592 Color: 0
Size: 275566 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 443267 Color: 1
Size: 288088 Color: 1
Size: 268646 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 466579 Color: 0
Size: 281372 Color: 0
Size: 252050 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 395495 Color: 0
Size: 353682 Color: 1
Size: 250824 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 368023 Color: 0
Size: 350952 Color: 1
Size: 281026 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 431446 Color: 0
Size: 316238 Color: 0
Size: 252317 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 426195 Color: 0
Size: 288407 Color: 0
Size: 285399 Color: 1

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 432983 Color: 1
Size: 301067 Color: 1
Size: 265951 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 393955 Color: 1
Size: 355979 Color: 1
Size: 250067 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 459200 Color: 0
Size: 283983 Color: 1
Size: 256818 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 419579 Color: 0
Size: 325188 Color: 1
Size: 255234 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 381254 Color: 0
Size: 348626 Color: 1
Size: 270121 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 384579 Color: 0
Size: 313618 Color: 1
Size: 301804 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 395761 Color: 0
Size: 348609 Color: 1
Size: 255631 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 395443 Color: 1
Size: 351528 Color: 0
Size: 253030 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 411152 Color: 1
Size: 299783 Color: 1
Size: 289066 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 390951 Color: 0
Size: 334268 Color: 0
Size: 274782 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 376512 Color: 1
Size: 319240 Color: 0
Size: 304249 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 458619 Color: 0
Size: 286026 Color: 1
Size: 255356 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 359651 Color: 0
Size: 344946 Color: 1
Size: 295404 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 477935 Color: 1
Size: 267641 Color: 0
Size: 254425 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 404427 Color: 0
Size: 304559 Color: 1
Size: 291015 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 440730 Color: 1
Size: 304520 Color: 0
Size: 254751 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 486311 Color: 0
Size: 259006 Color: 1
Size: 254684 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 406035 Color: 1
Size: 336223 Color: 0
Size: 257743 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 436945 Color: 1
Size: 295085 Color: 0
Size: 267971 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 450872 Color: 1
Size: 277147 Color: 0
Size: 271982 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 408949 Color: 1
Size: 337602 Color: 0
Size: 253450 Color: 1

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 375940 Color: 1
Size: 358056 Color: 0
Size: 266005 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 378173 Color: 1
Size: 361068 Color: 0
Size: 260760 Color: 1

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 433938 Color: 0
Size: 293773 Color: 1
Size: 272290 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 393727 Color: 0
Size: 336759 Color: 0
Size: 269515 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 395907 Color: 0
Size: 349396 Color: 1
Size: 254698 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 374076 Color: 1
Size: 359527 Color: 0
Size: 266398 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 366718 Color: 0
Size: 359415 Color: 0
Size: 273868 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 402760 Color: 0
Size: 315283 Color: 1
Size: 281958 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 434351 Color: 0
Size: 293507 Color: 1
Size: 272143 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 378782 Color: 1
Size: 360694 Color: 1
Size: 260525 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 389866 Color: 1
Size: 344897 Color: 0
Size: 265238 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 390355 Color: 1
Size: 328308 Color: 0
Size: 281338 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 376637 Color: 1
Size: 358240 Color: 1
Size: 265124 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 497601 Color: 0
Size: 251828 Color: 1
Size: 250572 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 416280 Color: 0
Size: 326388 Color: 1
Size: 257333 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 411080 Color: 1
Size: 323204 Color: 1
Size: 265717 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 447056 Color: 1
Size: 289713 Color: 1
Size: 263232 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 398246 Color: 1
Size: 304832 Color: 1
Size: 296923 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 382714 Color: 1
Size: 361776 Color: 0
Size: 255511 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 358971 Color: 1
Size: 342881 Color: 1
Size: 298149 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 405369 Color: 0
Size: 309986 Color: 1
Size: 284646 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 334001 Color: 0
Size: 333627 Color: 0
Size: 332373 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 457646 Color: 1
Size: 291166 Color: 0
Size: 251189 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 463601 Color: 0
Size: 280821 Color: 1
Size: 255579 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 439734 Color: 0
Size: 309276 Color: 0
Size: 250991 Color: 1

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 378940 Color: 0
Size: 333358 Color: 1
Size: 287703 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 379443 Color: 0
Size: 312131 Color: 0
Size: 308427 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 401587 Color: 0
Size: 308095 Color: 0
Size: 290319 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 410141 Color: 1
Size: 319825 Color: 1
Size: 270035 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 429765 Color: 1
Size: 314429 Color: 0
Size: 255807 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 367992 Color: 1
Size: 318289 Color: 0
Size: 313720 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 375192 Color: 1
Size: 364608 Color: 0
Size: 260201 Color: 1

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 387340 Color: 1
Size: 314162 Color: 1
Size: 298499 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 473562 Color: 1
Size: 276222 Color: 0
Size: 250217 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 410669 Color: 1
Size: 328671 Color: 1
Size: 260661 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 460153 Color: 1
Size: 286971 Color: 1
Size: 252877 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 377406 Color: 0
Size: 321835 Color: 1
Size: 300760 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 456106 Color: 0
Size: 289012 Color: 0
Size: 254883 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 452667 Color: 1
Size: 274811 Color: 1
Size: 272523 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 370528 Color: 1
Size: 340347 Color: 1
Size: 289126 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 439795 Color: 1
Size: 289337 Color: 1
Size: 270869 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 383833 Color: 1
Size: 342624 Color: 0
Size: 273544 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 373604 Color: 0
Size: 320757 Color: 0
Size: 305640 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 496590 Color: 1
Size: 252907 Color: 1
Size: 250504 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 492150 Color: 1
Size: 256717 Color: 0
Size: 251134 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 449723 Color: 1
Size: 294303 Color: 1
Size: 255975 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 392951 Color: 0
Size: 336318 Color: 0
Size: 270732 Color: 1

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 389346 Color: 1
Size: 313406 Color: 1
Size: 297249 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 438240 Color: 1
Size: 295538 Color: 0
Size: 266223 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 375611 Color: 0
Size: 316815 Color: 0
Size: 307575 Color: 1

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 384455 Color: 0
Size: 332307 Color: 1
Size: 283239 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 451885 Color: 0
Size: 280211 Color: 0
Size: 267905 Color: 1

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 389652 Color: 0
Size: 327706 Color: 1
Size: 282643 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 406948 Color: 1
Size: 334634 Color: 1
Size: 258419 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 463658 Color: 0
Size: 274140 Color: 1
Size: 262203 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 460745 Color: 0
Size: 270701 Color: 0
Size: 268555 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 361467 Color: 1
Size: 340853 Color: 1
Size: 297681 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 441075 Color: 1
Size: 286505 Color: 0
Size: 272421 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 380753 Color: 0
Size: 326507 Color: 1
Size: 292741 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 418907 Color: 1
Size: 293773 Color: 0
Size: 287321 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 466978 Color: 1
Size: 278284 Color: 1
Size: 254739 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 394511 Color: 1
Size: 311863 Color: 0
Size: 293627 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 490828 Color: 0
Size: 258016 Color: 0
Size: 251157 Color: 1

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 475394 Color: 0
Size: 267993 Color: 1
Size: 256614 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 392845 Color: 1
Size: 326261 Color: 0
Size: 280895 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 492295 Color: 0
Size: 255384 Color: 1
Size: 252322 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 411700 Color: 0
Size: 327599 Color: 1
Size: 260702 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 456670 Color: 0
Size: 279147 Color: 0
Size: 264184 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 442129 Color: 0
Size: 300324 Color: 1
Size: 257548 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 463873 Color: 1
Size: 270910 Color: 1
Size: 265218 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 479582 Color: 0
Size: 262676 Color: 1
Size: 257743 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 454079 Color: 0
Size: 279076 Color: 1
Size: 266846 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 432024 Color: 1
Size: 295475 Color: 0
Size: 272502 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 363465 Color: 1
Size: 353149 Color: 1
Size: 283387 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 380982 Color: 1
Size: 323521 Color: 1
Size: 295498 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 458779 Color: 0
Size: 278405 Color: 1
Size: 262817 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 390266 Color: 0
Size: 321483 Color: 0
Size: 288252 Color: 1

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 338105 Color: 0
Size: 333590 Color: 1
Size: 328306 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 394878 Color: 1
Size: 330016 Color: 1
Size: 275107 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 381143 Color: 0
Size: 353532 Color: 1
Size: 265326 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 439367 Color: 0
Size: 282479 Color: 1
Size: 278155 Color: 1

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 400266 Color: 0
Size: 308549 Color: 0
Size: 291186 Color: 1

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 426397 Color: 0
Size: 322549 Color: 1
Size: 251055 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 377374 Color: 0
Size: 315958 Color: 0
Size: 306669 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 400783 Color: 0
Size: 300650 Color: 1
Size: 298568 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 446177 Color: 1
Size: 276980 Color: 0
Size: 276844 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 424670 Color: 1
Size: 300994 Color: 0
Size: 274337 Color: 1

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 492402 Color: 1
Size: 255323 Color: 0
Size: 252276 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 428871 Color: 1
Size: 307494 Color: 1
Size: 263636 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 477380 Color: 0
Size: 268557 Color: 1
Size: 254064 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 496823 Color: 1
Size: 252222 Color: 1
Size: 250956 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 404245 Color: 1
Size: 308430 Color: 1
Size: 287326 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 467903 Color: 1
Size: 268544 Color: 1
Size: 263554 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 427448 Color: 0
Size: 292735 Color: 1
Size: 279818 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 450176 Color: 1
Size: 288026 Color: 1
Size: 261799 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 439592 Color: 0
Size: 297456 Color: 0
Size: 262953 Color: 1

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 476622 Color: 0
Size: 267235 Color: 0
Size: 256144 Color: 1

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 393758 Color: 0
Size: 338445 Color: 1
Size: 267798 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 372893 Color: 0
Size: 339777 Color: 0
Size: 287331 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 493494 Color: 1
Size: 256260 Color: 1
Size: 250247 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 451683 Color: 0
Size: 280361 Color: 1
Size: 267957 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 435343 Color: 0
Size: 283890 Color: 1
Size: 280768 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 396750 Color: 1
Size: 349998 Color: 1
Size: 253253 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 376398 Color: 1
Size: 340285 Color: 1
Size: 283318 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 426310 Color: 1
Size: 304063 Color: 0
Size: 269628 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 371381 Color: 0
Size: 333851 Color: 0
Size: 294769 Color: 1

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 415591 Color: 1
Size: 312957 Color: 0
Size: 271453 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 383053 Color: 0
Size: 327082 Color: 1
Size: 289866 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 400839 Color: 0
Size: 315936 Color: 1
Size: 283226 Color: 1

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 460484 Color: 1
Size: 278364 Color: 1
Size: 261153 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 462015 Color: 1
Size: 285632 Color: 1
Size: 252354 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 487134 Color: 0
Size: 259104 Color: 0
Size: 253763 Color: 1

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 381289 Color: 0
Size: 314858 Color: 1
Size: 303854 Color: 1

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 399578 Color: 1
Size: 305539 Color: 0
Size: 294884 Color: 1

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 378844 Color: 0
Size: 350131 Color: 1
Size: 271026 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 372101 Color: 0
Size: 366494 Color: 1
Size: 261406 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 383984 Color: 0
Size: 313971 Color: 1
Size: 302046 Color: 1

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 401548 Color: 1
Size: 321745 Color: 0
Size: 276708 Color: 1

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 471296 Color: 0
Size: 269226 Color: 0
Size: 259479 Color: 1

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 364582 Color: 0
Size: 343789 Color: 1
Size: 291630 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 472460 Color: 0
Size: 274093 Color: 0
Size: 253448 Color: 1

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 416538 Color: 0
Size: 313042 Color: 1
Size: 270421 Color: 1

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 399255 Color: 0
Size: 324511 Color: 0
Size: 276235 Color: 1

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 413670 Color: 1
Size: 312458 Color: 0
Size: 273873 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 377643 Color: 0
Size: 311775 Color: 0
Size: 310583 Color: 1

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 467578 Color: 1
Size: 281754 Color: 0
Size: 250669 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 498268 Color: 1
Size: 251643 Color: 0
Size: 250090 Color: 1

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 373067 Color: 1
Size: 357382 Color: 1
Size: 269552 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 389206 Color: 1
Size: 339431 Color: 1
Size: 271364 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 379247 Color: 0
Size: 341578 Color: 0
Size: 279176 Color: 1

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 395546 Color: 1
Size: 329596 Color: 0
Size: 274859 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 399384 Color: 1
Size: 333774 Color: 0
Size: 266843 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 492169 Color: 0
Size: 254909 Color: 0
Size: 252923 Color: 1

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 462622 Color: 1
Size: 274010 Color: 0
Size: 263369 Color: 1

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 380994 Color: 1
Size: 319196 Color: 0
Size: 299811 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 394133 Color: 0
Size: 314310 Color: 0
Size: 291558 Color: 1

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 387681 Color: 0
Size: 334356 Color: 1
Size: 277964 Color: 0

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 490507 Color: 1
Size: 259276 Color: 0
Size: 250218 Color: 1

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 445144 Color: 1
Size: 301086 Color: 0
Size: 253771 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 491579 Color: 1
Size: 254231 Color: 1
Size: 254191 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 451841 Color: 0
Size: 296948 Color: 0
Size: 251212 Color: 1

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 456677 Color: 1
Size: 289068 Color: 1
Size: 254256 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 391696 Color: 0
Size: 306333 Color: 0
Size: 301972 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 461408 Color: 1
Size: 286887 Color: 0
Size: 251706 Color: 1

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 389585 Color: 1
Size: 319556 Color: 0
Size: 290860 Color: 1

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 387371 Color: 0
Size: 359311 Color: 1
Size: 253319 Color: 1

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 402603 Color: 0
Size: 305647 Color: 1
Size: 291751 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 447163 Color: 1
Size: 291790 Color: 1
Size: 261048 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 439470 Color: 0
Size: 280667 Color: 0
Size: 279864 Color: 1

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 371551 Color: 1
Size: 352004 Color: 1
Size: 276446 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 496975 Color: 0
Size: 252137 Color: 1
Size: 250889 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 383684 Color: 0
Size: 326212 Color: 0
Size: 290105 Color: 1

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 434803 Color: 0
Size: 287743 Color: 0
Size: 277455 Color: 1

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 470718 Color: 0
Size: 277578 Color: 1
Size: 251705 Color: 1

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 407636 Color: 0
Size: 297828 Color: 1
Size: 294537 Color: 1

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 359155 Color: 1
Size: 328980 Color: 1
Size: 311866 Color: 0

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 386696 Color: 1
Size: 309014 Color: 1
Size: 304291 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 388251 Color: 0
Size: 311916 Color: 1
Size: 299834 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 473396 Color: 1
Size: 270414 Color: 0
Size: 256191 Color: 1

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 422232 Color: 1
Size: 310866 Color: 0
Size: 266903 Color: 1

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 403559 Color: 0
Size: 326688 Color: 0
Size: 269754 Color: 1

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 373634 Color: 0
Size: 321769 Color: 0
Size: 304598 Color: 1

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 380786 Color: 0
Size: 343272 Color: 1
Size: 275943 Color: 0

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 378811 Color: 0
Size: 361662 Color: 1
Size: 259528 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 361187 Color: 0
Size: 353081 Color: 1
Size: 285733 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 385499 Color: 1
Size: 355276 Color: 0
Size: 259226 Color: 1

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 467534 Color: 0
Size: 279990 Color: 1
Size: 252477 Color: 1

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 440273 Color: 1
Size: 285322 Color: 0
Size: 274406 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 386627 Color: 1
Size: 317190 Color: 0
Size: 296184 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 405847 Color: 1
Size: 300478 Color: 1
Size: 293676 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 479261 Color: 0
Size: 264982 Color: 0
Size: 255758 Color: 1

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 405447 Color: 1
Size: 324576 Color: 1
Size: 269978 Color: 0

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 412989 Color: 0
Size: 305760 Color: 1
Size: 281252 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 379720 Color: 1
Size: 346350 Color: 0
Size: 273931 Color: 1

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 414086 Color: 0
Size: 303676 Color: 1
Size: 282239 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 456939 Color: 1
Size: 289473 Color: 0
Size: 253589 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 495843 Color: 1
Size: 253069 Color: 1
Size: 251089 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 369024 Color: 0
Size: 356608 Color: 1
Size: 274369 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 388873 Color: 1
Size: 309417 Color: 0
Size: 301711 Color: 1

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 376482 Color: 1
Size: 370443 Color: 0
Size: 253076 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 433550 Color: 0
Size: 288181 Color: 0
Size: 278270 Color: 1

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 442858 Color: 0
Size: 282091 Color: 0
Size: 275052 Color: 1

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 427309 Color: 1
Size: 321871 Color: 0
Size: 250821 Color: 1

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 368419 Color: 0
Size: 351104 Color: 1
Size: 280478 Color: 0

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 359412 Color: 0
Size: 324826 Color: 1
Size: 315763 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 394730 Color: 0
Size: 343006 Color: 1
Size: 262265 Color: 0

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 382961 Color: 0
Size: 347306 Color: 1
Size: 269734 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 350104 Color: 1
Size: 334043 Color: 0
Size: 315854 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 436526 Color: 0
Size: 296680 Color: 1
Size: 266795 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 381057 Color: 1
Size: 362288 Color: 0
Size: 256656 Color: 1

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 426944 Color: 1
Size: 303399 Color: 1
Size: 269658 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 378456 Color: 0
Size: 327116 Color: 1
Size: 294429 Color: 1

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 373293 Color: 1
Size: 315720 Color: 1
Size: 310988 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 458294 Color: 0
Size: 271446 Color: 1
Size: 270261 Color: 1

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 485634 Color: 1
Size: 262624 Color: 1
Size: 251743 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 461227 Color: 1
Size: 285631 Color: 0
Size: 253143 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 395846 Color: 0
Size: 339312 Color: 0
Size: 264843 Color: 1

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 477518 Color: 0
Size: 268108 Color: 1
Size: 254375 Color: 1

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 498079 Color: 1
Size: 251751 Color: 1
Size: 250171 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 400706 Color: 1
Size: 326320 Color: 0
Size: 272975 Color: 1

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 447273 Color: 1
Size: 285514 Color: 0
Size: 267214 Color: 0

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 395136 Color: 1
Size: 310404 Color: 0
Size: 294461 Color: 1

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 378724 Color: 0
Size: 341226 Color: 0
Size: 280051 Color: 1

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 442158 Color: 0
Size: 298219 Color: 1
Size: 259624 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 362737 Color: 1
Size: 324681 Color: 0
Size: 312583 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 401628 Color: 0
Size: 299843 Color: 0
Size: 298530 Color: 1

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 418168 Color: 0
Size: 315979 Color: 1
Size: 265854 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 471601 Color: 1
Size: 271100 Color: 0
Size: 257300 Color: 1

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 391953 Color: 0
Size: 336879 Color: 0
Size: 271169 Color: 1

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 430019 Color: 0
Size: 318656 Color: 1
Size: 251326 Color: 0

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 436050 Color: 0
Size: 310226 Color: 1
Size: 253725 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 494861 Color: 1
Size: 254291 Color: 1
Size: 250849 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 408822 Color: 0
Size: 304758 Color: 1
Size: 286421 Color: 0

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 481344 Color: 0
Size: 260036 Color: 0
Size: 258621 Color: 1

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 414149 Color: 1
Size: 310641 Color: 0
Size: 275211 Color: 1

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 420005 Color: 1
Size: 299906 Color: 0
Size: 280090 Color: 1

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 491245 Color: 1
Size: 256356 Color: 0
Size: 252400 Color: 0

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 485451 Color: 1
Size: 258851 Color: 0
Size: 255699 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 493566 Color: 1
Size: 255566 Color: 0
Size: 250869 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 362343 Color: 0
Size: 351752 Color: 1
Size: 285906 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 396935 Color: 1
Size: 335631 Color: 0
Size: 267435 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 382494 Color: 1
Size: 330199 Color: 1
Size: 287308 Color: 0

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 473565 Color: 0
Size: 267009 Color: 1
Size: 259427 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 415034 Color: 0
Size: 334016 Color: 1
Size: 250951 Color: 1

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 381432 Color: 1
Size: 353449 Color: 1
Size: 265120 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 495755 Color: 1
Size: 252445 Color: 1
Size: 251801 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 465096 Color: 1
Size: 267957 Color: 0
Size: 266948 Color: 1

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 402462 Color: 0
Size: 343541 Color: 1
Size: 253998 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 343141 Color: 1
Size: 328591 Color: 0
Size: 328269 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 499505 Color: 1
Size: 250434 Color: 0
Size: 250062 Color: 0

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 337260 Color: 0
Size: 337229 Color: 1
Size: 325512 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 421465 Color: 0
Size: 327766 Color: 1
Size: 250770 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 374912 Color: 0
Size: 355246 Color: 1
Size: 269843 Color: 0

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 388136 Color: 1
Size: 359730 Color: 0
Size: 252135 Color: 1

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 393694 Color: 1
Size: 333131 Color: 1
Size: 273176 Color: 0

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 444261 Color: 1
Size: 281856 Color: 0
Size: 273884 Color: 1

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 450463 Color: 0
Size: 275850 Color: 1
Size: 273688 Color: 1

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 373294 Color: 1
Size: 352266 Color: 0
Size: 274441 Color: 1

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 480012 Color: 0
Size: 264522 Color: 1
Size: 255467 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 373468 Color: 1
Size: 357790 Color: 0
Size: 268743 Color: 1

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 405869 Color: 1
Size: 332033 Color: 0
Size: 262099 Color: 1

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 482486 Color: 0
Size: 260307 Color: 0
Size: 257208 Color: 1

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 402291 Color: 1
Size: 304491 Color: 1
Size: 293219 Color: 0

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 388035 Color: 1
Size: 307053 Color: 0
Size: 304913 Color: 1

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 377079 Color: 1
Size: 340056 Color: 0
Size: 282866 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 411741 Color: 1
Size: 317030 Color: 0
Size: 271230 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 383515 Color: 1
Size: 323811 Color: 0
Size: 292675 Color: 1

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 392235 Color: 1
Size: 325883 Color: 1
Size: 281883 Color: 0

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 384784 Color: 1
Size: 315647 Color: 1
Size: 299570 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 381678 Color: 0
Size: 311889 Color: 1
Size: 306434 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 379841 Color: 0
Size: 342634 Color: 0
Size: 277526 Color: 1

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 425873 Color: 1
Size: 305869 Color: 1
Size: 268259 Color: 0

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 361112 Color: 0
Size: 326939 Color: 0
Size: 311950 Color: 1

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 372203 Color: 0
Size: 370081 Color: 0
Size: 257717 Color: 1

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 387252 Color: 1
Size: 331191 Color: 0
Size: 281558 Color: 1

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 412182 Color: 0
Size: 325412 Color: 1
Size: 262407 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 486417 Color: 0
Size: 261522 Color: 0
Size: 252062 Color: 1

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 405356 Color: 0
Size: 339036 Color: 1
Size: 255609 Color: 1

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 465595 Color: 0
Size: 276778 Color: 0
Size: 257628 Color: 1

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 496102 Color: 1
Size: 253176 Color: 0
Size: 250723 Color: 0

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 396780 Color: 0
Size: 350946 Color: 1
Size: 252275 Color: 0

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 382205 Color: 1
Size: 331912 Color: 0
Size: 285884 Color: 1

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 371535 Color: 0
Size: 318159 Color: 1
Size: 310307 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 445336 Color: 0
Size: 285592 Color: 1
Size: 269073 Color: 1

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 415321 Color: 0
Size: 328100 Color: 1
Size: 256580 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 445782 Color: 0
Size: 289660 Color: 1
Size: 264559 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 487547 Color: 0
Size: 261820 Color: 1
Size: 250634 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 425453 Color: 1
Size: 309718 Color: 1
Size: 264830 Color: 0

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 472312 Color: 1
Size: 274240 Color: 1
Size: 253449 Color: 0

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 468614 Color: 0
Size: 277059 Color: 0
Size: 254328 Color: 1

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 442835 Color: 0
Size: 282988 Color: 0
Size: 274178 Color: 1

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 355880 Color: 0
Size: 325666 Color: 1
Size: 318455 Color: 0

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 368579 Color: 1
Size: 321624 Color: 0
Size: 309798 Color: 1

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 382762 Color: 1
Size: 339368 Color: 0
Size: 277871 Color: 1

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 365725 Color: 1
Size: 346041 Color: 0
Size: 288235 Color: 1

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 478780 Color: 0
Size: 267655 Color: 1
Size: 253566 Color: 0

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 495826 Color: 0
Size: 253279 Color: 0
Size: 250896 Color: 1

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 437250 Color: 1
Size: 288093 Color: 1
Size: 274658 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 360827 Color: 0
Size: 355719 Color: 1
Size: 283455 Color: 0

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 498902 Color: 0
Size: 250910 Color: 1
Size: 250189 Color: 1

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 394963 Color: 1
Size: 328546 Color: 1
Size: 276492 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 427655 Color: 0
Size: 302510 Color: 1
Size: 269836 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 432674 Color: 1
Size: 310388 Color: 0
Size: 256939 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 444546 Color: 0
Size: 303982 Color: 1
Size: 251473 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 426865 Color: 1
Size: 299322 Color: 0
Size: 273814 Color: 1

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 486567 Color: 0
Size: 260353 Color: 1
Size: 253081 Color: 1

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 466091 Color: 0
Size: 278549 Color: 0
Size: 255361 Color: 1

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 414804 Color: 0
Size: 297975 Color: 0
Size: 287222 Color: 1

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 376187 Color: 1
Size: 342797 Color: 1
Size: 281017 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 423957 Color: 1
Size: 314746 Color: 0
Size: 261298 Color: 1

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 444517 Color: 0
Size: 296564 Color: 1
Size: 258920 Color: 0

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 399881 Color: 0
Size: 312197 Color: 0
Size: 287923 Color: 1

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 425596 Color: 0
Size: 316833 Color: 0
Size: 257572 Color: 1

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 378177 Color: 0
Size: 311511 Color: 0
Size: 310313 Color: 1

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 369226 Color: 0
Size: 356292 Color: 0
Size: 274483 Color: 1

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 393707 Color: 0
Size: 344691 Color: 1
Size: 261603 Color: 1

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 485776 Color: 0
Size: 259195 Color: 1
Size: 255030 Color: 1

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 371545 Color: 1
Size: 354108 Color: 0
Size: 274348 Color: 1

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 364071 Color: 1
Size: 353588 Color: 0
Size: 282342 Color: 1

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 470361 Color: 1
Size: 277150 Color: 0
Size: 252490 Color: 1

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 473518 Color: 1
Size: 273062 Color: 1
Size: 253421 Color: 0

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 498448 Color: 1
Size: 251268 Color: 1
Size: 250285 Color: 0

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 449929 Color: 0
Size: 289673 Color: 1
Size: 260399 Color: 1

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 472136 Color: 1
Size: 276165 Color: 1
Size: 251700 Color: 0

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 345520 Color: 1
Size: 332387 Color: 0
Size: 322094 Color: 1

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 456973 Color: 1
Size: 273095 Color: 0
Size: 269933 Color: 1

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 492266 Color: 1
Size: 256271 Color: 1
Size: 251464 Color: 0

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 415423 Color: 1
Size: 331905 Color: 0
Size: 252673 Color: 1

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 351951 Color: 1
Size: 325390 Color: 0
Size: 322660 Color: 0

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 389233 Color: 1
Size: 317947 Color: 0
Size: 292821 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 445679 Color: 0
Size: 295513 Color: 1
Size: 258809 Color: 0

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 451230 Color: 0
Size: 275034 Color: 1
Size: 273737 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 374865 Color: 0
Size: 373971 Color: 1
Size: 251165 Color: 0

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 437556 Color: 1
Size: 310530 Color: 0
Size: 251915 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 477699 Color: 1
Size: 261288 Color: 0
Size: 261014 Color: 0

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 399961 Color: 0
Size: 316994 Color: 0
Size: 283046 Color: 1

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 392486 Color: 1
Size: 323462 Color: 1
Size: 284053 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 400661 Color: 0
Size: 303864 Color: 1
Size: 295476 Color: 1

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 384463 Color: 0
Size: 344417 Color: 1
Size: 271121 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 407268 Color: 1
Size: 330993 Color: 0
Size: 261740 Color: 0

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 380309 Color: 0
Size: 346126 Color: 0
Size: 273566 Color: 1

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 401149 Color: 0
Size: 300504 Color: 1
Size: 298348 Color: 1

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 374508 Color: 1
Size: 314458 Color: 1
Size: 311035 Color: 0

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 443682 Color: 1
Size: 303977 Color: 1
Size: 252342 Color: 0

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 472760 Color: 1
Size: 265619 Color: 0
Size: 261622 Color: 1

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 393098 Color: 1
Size: 350611 Color: 0
Size: 256292 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 367581 Color: 1
Size: 322033 Color: 1
Size: 310387 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 371944 Color: 0
Size: 323107 Color: 1
Size: 304950 Color: 0

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 393947 Color: 1
Size: 320738 Color: 1
Size: 285316 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 494226 Color: 1
Size: 255341 Color: 0
Size: 250434 Color: 0

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 374964 Color: 0
Size: 339753 Color: 1
Size: 285284 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 414144 Color: 0
Size: 296169 Color: 1
Size: 289688 Color: 0

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 412492 Color: 0
Size: 312358 Color: 0
Size: 275151 Color: 1

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 393724 Color: 1
Size: 333966 Color: 0
Size: 272311 Color: 1

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 399799 Color: 1
Size: 307589 Color: 1
Size: 292613 Color: 0

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 398437 Color: 1
Size: 306840 Color: 0
Size: 294724 Color: 1

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 442783 Color: 0
Size: 286164 Color: 1
Size: 271054 Color: 1

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 370141 Color: 1
Size: 327242 Color: 0
Size: 302618 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 461465 Color: 1
Size: 285121 Color: 0
Size: 253415 Color: 1

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 372110 Color: 0
Size: 321042 Color: 0
Size: 306849 Color: 1

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 377046 Color: 1
Size: 319229 Color: 0
Size: 303726 Color: 1

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 373880 Color: 1
Size: 316138 Color: 0
Size: 309983 Color: 1

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 397729 Color: 1
Size: 342649 Color: 1
Size: 259623 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 428715 Color: 1
Size: 291274 Color: 0
Size: 280012 Color: 0

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 484045 Color: 1
Size: 258064 Color: 0
Size: 257892 Color: 1

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 482767 Color: 1
Size: 266330 Color: 1
Size: 250904 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 359768 Color: 0
Size: 343823 Color: 1
Size: 296410 Color: 1

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 430514 Color: 1
Size: 303911 Color: 1
Size: 265576 Color: 0

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 461450 Color: 0
Size: 277006 Color: 1
Size: 261545 Color: 0

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 419038 Color: 1
Size: 301142 Color: 0
Size: 279821 Color: 0

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 445175 Color: 0
Size: 300062 Color: 1
Size: 254764 Color: 0

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 442301 Color: 1
Size: 307215 Color: 0
Size: 250485 Color: 1

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 451396 Color: 0
Size: 295854 Color: 1
Size: 252751 Color: 0

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 378056 Color: 1
Size: 325421 Color: 1
Size: 296524 Color: 0

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 377729 Color: 1
Size: 317457 Color: 0
Size: 304815 Color: 1

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 485664 Color: 0
Size: 264164 Color: 0
Size: 250173 Color: 1

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 397299 Color: 0
Size: 337677 Color: 1
Size: 265025 Color: 0

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 386235 Color: 0
Size: 335354 Color: 1
Size: 278412 Color: 0

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 425326 Color: 0
Size: 298111 Color: 1
Size: 276564 Color: 0

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 427113 Color: 1
Size: 321920 Color: 1
Size: 250968 Color: 0

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 478334 Color: 1
Size: 269632 Color: 0
Size: 252035 Color: 1

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 382324 Color: 0
Size: 343810 Color: 0
Size: 273867 Color: 1

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 489795 Color: 0
Size: 260179 Color: 1
Size: 250027 Color: 0

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 379905 Color: 1
Size: 311182 Color: 1
Size: 308914 Color: 0

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 485846 Color: 0
Size: 257983 Color: 0
Size: 256172 Color: 1

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 456861 Color: 1
Size: 280677 Color: 1
Size: 262463 Color: 0

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 409248 Color: 1
Size: 320731 Color: 0
Size: 270022 Color: 1

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 455162 Color: 0
Size: 273804 Color: 0
Size: 271035 Color: 1

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 399472 Color: 0
Size: 334938 Color: 1
Size: 265591 Color: 0

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 425804 Color: 1
Size: 323408 Color: 1
Size: 250789 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 494769 Color: 1
Size: 253733 Color: 0
Size: 251499 Color: 0

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 358853 Color: 1
Size: 355201 Color: 0
Size: 285947 Color: 1

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 367925 Color: 0
Size: 354849 Color: 1
Size: 277227 Color: 0

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 412436 Color: 0
Size: 334161 Color: 0
Size: 253404 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 495081 Color: 1
Size: 254804 Color: 0
Size: 250116 Color: 1

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 376474 Color: 1
Size: 363317 Color: 0
Size: 260210 Color: 1

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 443052 Color: 1
Size: 291684 Color: 0
Size: 265265 Color: 1

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 395155 Color: 0
Size: 313471 Color: 1
Size: 291375 Color: 0

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 486167 Color: 0
Size: 257576 Color: 1
Size: 256258 Color: 1

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 392046 Color: 0
Size: 334874 Color: 0
Size: 273081 Color: 1

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 394204 Color: 0
Size: 311979 Color: 1
Size: 293818 Color: 0

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 474034 Color: 0
Size: 272067 Color: 0
Size: 253900 Color: 1

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 384897 Color: 1
Size: 333525 Color: 1
Size: 281579 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 435967 Color: 1
Size: 301158 Color: 0
Size: 262876 Color: 1

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 465527 Color: 0
Size: 277068 Color: 1
Size: 257406 Color: 1

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 476910 Color: 1
Size: 266611 Color: 0
Size: 256480 Color: 1

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 417707 Color: 0
Size: 312842 Color: 0
Size: 269452 Color: 1

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 377914 Color: 1
Size: 311899 Color: 0
Size: 310188 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 402281 Color: 1
Size: 321101 Color: 0
Size: 276619 Color: 1

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 489364 Color: 0
Size: 257375 Color: 1
Size: 253262 Color: 1

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 379183 Color: 1
Size: 334668 Color: 1
Size: 286150 Color: 0

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 402729 Color: 0
Size: 335379 Color: 1
Size: 261893 Color: 1

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 466320 Color: 0
Size: 276357 Color: 1
Size: 257324 Color: 1

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 463845 Color: 0
Size: 279863 Color: 1
Size: 256293 Color: 1

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 418221 Color: 1
Size: 301595 Color: 1
Size: 280185 Color: 0

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 404358 Color: 1
Size: 315758 Color: 1
Size: 279885 Color: 0

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 485803 Color: 0
Size: 259514 Color: 1
Size: 254684 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 382901 Color: 1
Size: 313552 Color: 0
Size: 303548 Color: 1

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 418394 Color: 1
Size: 326191 Color: 0
Size: 255416 Color: 1

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 368814 Color: 0
Size: 350291 Color: 1
Size: 280896 Color: 0

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 393086 Color: 1
Size: 305013 Color: 0
Size: 301902 Color: 1

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 458670 Color: 0
Size: 290399 Color: 1
Size: 250932 Color: 1

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 491902 Color: 1
Size: 256143 Color: 1
Size: 251956 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 387635 Color: 0
Size: 314930 Color: 1
Size: 297436 Color: 0

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 384353 Color: 1
Size: 345029 Color: 0
Size: 270619 Color: 1

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 365583 Color: 0
Size: 322168 Color: 0
Size: 312250 Color: 1

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 419200 Color: 0
Size: 291491 Color: 0
Size: 289310 Color: 1

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 394119 Color: 0
Size: 354250 Color: 1
Size: 251632 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 478433 Color: 1
Size: 265897 Color: 0
Size: 255671 Color: 1

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 367299 Color: 0
Size: 353389 Color: 0
Size: 279313 Color: 1

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 401581 Color: 1
Size: 307591 Color: 0
Size: 290829 Color: 1

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 451338 Color: 1
Size: 287988 Color: 1
Size: 260675 Color: 0

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 460103 Color: 1
Size: 280337 Color: 0
Size: 259561 Color: 0

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 367300 Color: 1
Size: 360109 Color: 0
Size: 272592 Color: 0

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 471705 Color: 0
Size: 268588 Color: 1
Size: 259708 Color: 0

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 487464 Color: 0
Size: 259153 Color: 0
Size: 253384 Color: 1

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 499054 Color: 0
Size: 250525 Color: 0
Size: 250422 Color: 1

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 483339 Color: 1
Size: 258868 Color: 0
Size: 257794 Color: 1

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 359824 Color: 0
Size: 324279 Color: 1
Size: 315898 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 434153 Color: 0
Size: 301149 Color: 1
Size: 264699 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 390424 Color: 0
Size: 318862 Color: 1
Size: 290715 Color: 0

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 493057 Color: 1
Size: 255153 Color: 0
Size: 251791 Color: 0

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 398805 Color: 0
Size: 336915 Color: 0
Size: 264281 Color: 1

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 477277 Color: 0
Size: 268704 Color: 1
Size: 254020 Color: 1

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 417516 Color: 1
Size: 330506 Color: 1
Size: 251979 Color: 0

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 433781 Color: 0
Size: 284904 Color: 1
Size: 281316 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 446488 Color: 1
Size: 301284 Color: 1
Size: 252229 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 397138 Color: 1
Size: 326904 Color: 0
Size: 275959 Color: 1

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 496982 Color: 1
Size: 252500 Color: 0
Size: 250519 Color: 0

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 397187 Color: 1
Size: 333747 Color: 1
Size: 269067 Color: 0

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 493759 Color: 1
Size: 255716 Color: 0
Size: 250526 Color: 1

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 353817 Color: 0
Size: 326604 Color: 0
Size: 319580 Color: 1

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 376719 Color: 1
Size: 356890 Color: 1
Size: 266392 Color: 0

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 403184 Color: 1
Size: 315920 Color: 0
Size: 280897 Color: 1

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 368236 Color: 0
Size: 338283 Color: 1
Size: 293482 Color: 0

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 378757 Color: 0
Size: 325352 Color: 1
Size: 295892 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 495990 Color: 1
Size: 253354 Color: 0
Size: 250657 Color: 1

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 491927 Color: 0
Size: 257196 Color: 1
Size: 250878 Color: 1

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 455737 Color: 1
Size: 278520 Color: 0
Size: 265744 Color: 1

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 341046 Color: 1
Size: 338155 Color: 1
Size: 320800 Color: 0

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 469193 Color: 0
Size: 271602 Color: 1
Size: 259206 Color: 1

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 397445 Color: 1
Size: 321485 Color: 1
Size: 281071 Color: 0

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 434388 Color: 0
Size: 301774 Color: 1
Size: 263839 Color: 0

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 434900 Color: 0
Size: 304611 Color: 1
Size: 260490 Color: 0

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 476114 Color: 0
Size: 273454 Color: 0
Size: 250433 Color: 1

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 499638 Color: 0
Size: 250341 Color: 0
Size: 250022 Color: 1

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 387789 Color: 1
Size: 352972 Color: 1
Size: 259240 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 474502 Color: 0
Size: 269940 Color: 0
Size: 255559 Color: 1

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 446673 Color: 1
Size: 284228 Color: 0
Size: 269100 Color: 1

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 388017 Color: 1
Size: 341979 Color: 0
Size: 270005 Color: 0

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 475554 Color: 1
Size: 272402 Color: 0
Size: 252045 Color: 1

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 387126 Color: 0
Size: 350664 Color: 0
Size: 262211 Color: 1

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 453394 Color: 0
Size: 282760 Color: 1
Size: 263847 Color: 0

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 470795 Color: 1
Size: 276361 Color: 0
Size: 252845 Color: 0

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 343372 Color: 0
Size: 330119 Color: 0
Size: 326510 Color: 1

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 493728 Color: 1
Size: 255173 Color: 0
Size: 251100 Color: 1

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 361581 Color: 1
Size: 325615 Color: 0
Size: 312805 Color: 1

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 386510 Color: 0
Size: 340103 Color: 1
Size: 273388 Color: 0

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 444453 Color: 1
Size: 279722 Color: 1
Size: 275826 Color: 0

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 407984 Color: 1
Size: 313615 Color: 0
Size: 278402 Color: 1

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 454708 Color: 0
Size: 280500 Color: 1
Size: 264793 Color: 0

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 402933 Color: 0
Size: 318161 Color: 0
Size: 278907 Color: 1

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 384358 Color: 0
Size: 352362 Color: 1
Size: 263281 Color: 1

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 377488 Color: 1
Size: 347470 Color: 1
Size: 275043 Color: 0

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 397707 Color: 0
Size: 321168 Color: 0
Size: 281126 Color: 1

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 367007 Color: 0
Size: 319606 Color: 1
Size: 313388 Color: 1

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 376167 Color: 0
Size: 362313 Color: 1
Size: 261521 Color: 0

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 460423 Color: 0
Size: 288754 Color: 1
Size: 250824 Color: 0

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 374631 Color: 0
Size: 330184 Color: 1
Size: 295186 Color: 0

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 481928 Color: 1
Size: 260712 Color: 0
Size: 257361 Color: 0

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 363593 Color: 1
Size: 342566 Color: 1
Size: 293842 Color: 0

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 448085 Color: 1
Size: 285252 Color: 1
Size: 266664 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 372412 Color: 1
Size: 356924 Color: 0
Size: 270665 Color: 1

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 406690 Color: 0
Size: 342617 Color: 0
Size: 250694 Color: 1

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 428863 Color: 1
Size: 304094 Color: 0
Size: 267044 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 382734 Color: 1
Size: 329439 Color: 0
Size: 287828 Color: 1

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 347667 Color: 0
Size: 341557 Color: 1
Size: 310777 Color: 1

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 376709 Color: 0
Size: 362819 Color: 0
Size: 260473 Color: 1

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 489569 Color: 0
Size: 258029 Color: 1
Size: 252403 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 364661 Color: 1
Size: 318120 Color: 1
Size: 317220 Color: 0

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 368857 Color: 0
Size: 342024 Color: 1
Size: 289120 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 488906 Color: 1
Size: 258525 Color: 0
Size: 252570 Color: 0

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 422940 Color: 1
Size: 308610 Color: 1
Size: 268451 Color: 0

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 491344 Color: 0
Size: 257756 Color: 1
Size: 250901 Color: 1

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 492578 Color: 1
Size: 254506 Color: 1
Size: 252917 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 406133 Color: 0
Size: 301077 Color: 1
Size: 292791 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 392886 Color: 1
Size: 316077 Color: 0
Size: 291038 Color: 1

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 376789 Color: 0
Size: 318409 Color: 0
Size: 304803 Color: 1

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 381171 Color: 0
Size: 338420 Color: 0
Size: 280410 Color: 1

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 455869 Color: 0
Size: 293023 Color: 0
Size: 251109 Color: 1

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 434888 Color: 1
Size: 297411 Color: 0
Size: 267702 Color: 1

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 437888 Color: 1
Size: 298709 Color: 1
Size: 263404 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 424271 Color: 0
Size: 295522 Color: 1
Size: 280208 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 444147 Color: 0
Size: 290661 Color: 0
Size: 265193 Color: 1

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 429610 Color: 0
Size: 292414 Color: 0
Size: 277977 Color: 1

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 443769 Color: 1
Size: 281687 Color: 1
Size: 274545 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 464423 Color: 0
Size: 280102 Color: 1
Size: 255476 Color: 1

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 417470 Color: 1
Size: 301799 Color: 0
Size: 280732 Color: 0

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 400801 Color: 0
Size: 309776 Color: 1
Size: 289424 Color: 0

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 364852 Color: 1
Size: 348905 Color: 0
Size: 286244 Color: 1

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 423030 Color: 1
Size: 298243 Color: 0
Size: 278728 Color: 0

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 436512 Color: 1
Size: 302366 Color: 1
Size: 261123 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 391564 Color: 1
Size: 323476 Color: 0
Size: 284961 Color: 0

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 358203 Color: 1
Size: 335952 Color: 0
Size: 305846 Color: 1

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 351030 Color: 0
Size: 335636 Color: 1
Size: 313335 Color: 1

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 365646 Color: 0
Size: 360737 Color: 1
Size: 273618 Color: 1

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 345774 Color: 0
Size: 333776 Color: 0
Size: 320451 Color: 1

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 451331 Color: 0
Size: 282796 Color: 1
Size: 265874 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 429404 Color: 0
Size: 311079 Color: 1
Size: 259518 Color: 0

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 497393 Color: 1
Size: 251569 Color: 0
Size: 251039 Color: 0

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 397971 Color: 1
Size: 314783 Color: 0
Size: 287247 Color: 1

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 423760 Color: 0
Size: 308727 Color: 0
Size: 267514 Color: 1

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 484838 Color: 1
Size: 263294 Color: 0
Size: 251869 Color: 1

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 380266 Color: 1
Size: 356038 Color: 0
Size: 263697 Color: 0

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 365186 Color: 0
Size: 326869 Color: 1
Size: 307946 Color: 0

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 439697 Color: 1
Size: 309550 Color: 0
Size: 250754 Color: 0

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 437817 Color: 1
Size: 311065 Color: 1
Size: 251119 Color: 0

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 368689 Color: 0
Size: 349203 Color: 0
Size: 282109 Color: 1

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 402049 Color: 0
Size: 326996 Color: 1
Size: 270956 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 346435 Color: 0
Size: 342559 Color: 1
Size: 311007 Color: 0

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 369516 Color: 0
Size: 321415 Color: 0
Size: 309070 Color: 1

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 381678 Color: 1
Size: 331801 Color: 1
Size: 286522 Color: 0

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 435232 Color: 0
Size: 289660 Color: 0
Size: 275109 Color: 1

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 393100 Color: 1
Size: 339840 Color: 1
Size: 267061 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 449814 Color: 1
Size: 290008 Color: 1
Size: 260179 Color: 0

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 493511 Color: 1
Size: 254236 Color: 0
Size: 252254 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 439726 Color: 0
Size: 306192 Color: 0
Size: 254083 Color: 1

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 363036 Color: 1
Size: 326348 Color: 1
Size: 310617 Color: 0

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 458190 Color: 0
Size: 282382 Color: 1
Size: 259429 Color: 0

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 468832 Color: 1
Size: 268110 Color: 1
Size: 263059 Color: 0

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 410649 Color: 1
Size: 324975 Color: 0
Size: 264377 Color: 1

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 454404 Color: 1
Size: 276864 Color: 0
Size: 268733 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 433655 Color: 1
Size: 290389 Color: 0
Size: 275957 Color: 1

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 427543 Color: 1
Size: 296805 Color: 0
Size: 275653 Color: 1

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 386114 Color: 0
Size: 335580 Color: 1
Size: 278307 Color: 0

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 399173 Color: 0
Size: 338098 Color: 1
Size: 262730 Color: 0

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 458370 Color: 0
Size: 273692 Color: 1
Size: 267939 Color: 0

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 442392 Color: 1
Size: 298433 Color: 1
Size: 259176 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 335672 Color: 1
Size: 334115 Color: 0
Size: 330214 Color: 1

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 487923 Color: 1
Size: 256789 Color: 1
Size: 255289 Color: 0

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 472056 Color: 0
Size: 266652 Color: 1
Size: 261293 Color: 1

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 402076 Color: 0
Size: 343110 Color: 0
Size: 254815 Color: 1

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 443510 Color: 1
Size: 304124 Color: 0
Size: 252367 Color: 0

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 461756 Color: 0
Size: 269933 Color: 1
Size: 268312 Color: 1

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 361023 Color: 0
Size: 358837 Color: 0
Size: 280141 Color: 1

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 369682 Color: 0
Size: 358041 Color: 1
Size: 272278 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 413662 Color: 1
Size: 299835 Color: 0
Size: 286504 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 363295 Color: 1
Size: 337107 Color: 1
Size: 299599 Color: 0

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 389016 Color: 0
Size: 311805 Color: 0
Size: 299180 Color: 1

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 407224 Color: 0
Size: 301828 Color: 1
Size: 290949 Color: 0

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 367806 Color: 0
Size: 343874 Color: 0
Size: 288321 Color: 1

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 442881 Color: 0
Size: 305615 Color: 1
Size: 251505 Color: 1

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 474720 Color: 1
Size: 273623 Color: 0
Size: 251658 Color: 0

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 496420 Color: 1
Size: 252136 Color: 0
Size: 251445 Color: 1

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 397912 Color: 1
Size: 311253 Color: 0
Size: 290836 Color: 1

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 463403 Color: 1
Size: 277498 Color: 0
Size: 259100 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 478566 Color: 1
Size: 267028 Color: 0
Size: 254407 Color: 1

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 461329 Color: 0
Size: 286116 Color: 0
Size: 252556 Color: 1

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 379692 Color: 1
Size: 351175 Color: 0
Size: 269134 Color: 1

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 413526 Color: 0
Size: 334898 Color: 0
Size: 251577 Color: 1

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 454787 Color: 1
Size: 288971 Color: 1
Size: 256243 Color: 0

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 419572 Color: 0
Size: 300237 Color: 1
Size: 280192 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 388003 Color: 1
Size: 345973 Color: 1
Size: 266025 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 443643 Color: 0
Size: 288092 Color: 0
Size: 268266 Color: 1

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 385057 Color: 0
Size: 358798 Color: 0
Size: 256146 Color: 1

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 376144 Color: 1
Size: 345896 Color: 1
Size: 277961 Color: 0

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 427922 Color: 1
Size: 310617 Color: 0
Size: 261462 Color: 1

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 420319 Color: 1
Size: 313966 Color: 1
Size: 265716 Color: 0

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 383321 Color: 0
Size: 346222 Color: 1
Size: 270458 Color: 0

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 366377 Color: 0
Size: 320258 Color: 0
Size: 313366 Color: 1

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 423665 Color: 1
Size: 309833 Color: 1
Size: 266503 Color: 0

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 437346 Color: 0
Size: 310945 Color: 0
Size: 251710 Color: 1

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 494432 Color: 1
Size: 253339 Color: 0
Size: 252230 Color: 1

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 389887 Color: 0
Size: 319350 Color: 1
Size: 290764 Color: 1

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 415997 Color: 0
Size: 327788 Color: 1
Size: 256216 Color: 0

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 396114 Color: 0
Size: 308447 Color: 0
Size: 295440 Color: 1

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 376491 Color: 1
Size: 364384 Color: 0
Size: 259126 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 390076 Color: 0
Size: 357790 Color: 1
Size: 252135 Color: 1

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 395878 Color: 0
Size: 333619 Color: 0
Size: 270504 Color: 1

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 355956 Color: 1
Size: 327432 Color: 1
Size: 316613 Color: 0

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 349492 Color: 0
Size: 343647 Color: 0
Size: 306862 Color: 1

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 425328 Color: 1
Size: 324239 Color: 1
Size: 250434 Color: 0

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 421910 Color: 1
Size: 326036 Color: 1
Size: 252055 Color: 0

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 349379 Color: 0
Size: 338539 Color: 0
Size: 312083 Color: 1

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 384731 Color: 1
Size: 313544 Color: 0
Size: 301726 Color: 1

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 464208 Color: 0
Size: 273461 Color: 1
Size: 262332 Color: 1

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 369380 Color: 1
Size: 324720 Color: 0
Size: 305901 Color: 1

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 496564 Color: 0
Size: 252931 Color: 1
Size: 250506 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 438456 Color: 1
Size: 301713 Color: 0
Size: 259832 Color: 1

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 355976 Color: 1
Size: 355290 Color: 0
Size: 288735 Color: 1

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 395791 Color: 0
Size: 314250 Color: 1
Size: 289960 Color: 0

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 379124 Color: 0
Size: 358783 Color: 0
Size: 262094 Color: 1

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 411341 Color: 1
Size: 329520 Color: 1
Size: 259140 Color: 0

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 397870 Color: 1
Size: 349543 Color: 0
Size: 252588 Color: 1

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 455468 Color: 1
Size: 275979 Color: 0
Size: 268554 Color: 0

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 413927 Color: 1
Size: 293120 Color: 1
Size: 292954 Color: 0

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 429660 Color: 1
Size: 290357 Color: 1
Size: 279984 Color: 0

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 389247 Color: 1
Size: 334455 Color: 0
Size: 276299 Color: 1

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 395422 Color: 0
Size: 305346 Color: 1
Size: 299233 Color: 0

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 460803 Color: 1
Size: 271013 Color: 1
Size: 268185 Color: 0

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 473930 Color: 1
Size: 275610 Color: 0
Size: 250461 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 497212 Color: 1
Size: 251471 Color: 1
Size: 251318 Color: 0

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 436783 Color: 1
Size: 301657 Color: 1
Size: 261561 Color: 0

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 451060 Color: 0
Size: 295038 Color: 1
Size: 253903 Color: 1

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 461046 Color: 1
Size: 269649 Color: 1
Size: 269306 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 458645 Color: 0
Size: 277485 Color: 0
Size: 263871 Color: 1

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 395102 Color: 0
Size: 349121 Color: 0
Size: 255778 Color: 1

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 462579 Color: 1
Size: 279202 Color: 1
Size: 258220 Color: 0

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 405159 Color: 0
Size: 330041 Color: 0
Size: 264801 Color: 1

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 410852 Color: 0
Size: 331859 Color: 1
Size: 257290 Color: 0

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 439648 Color: 0
Size: 291264 Color: 1
Size: 269089 Color: 0

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 392556 Color: 1
Size: 348365 Color: 0
Size: 259080 Color: 1

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 358639 Color: 0
Size: 345131 Color: 1
Size: 296231 Color: 0

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 419014 Color: 0
Size: 296794 Color: 1
Size: 284193 Color: 0

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 421716 Color: 1
Size: 291893 Color: 1
Size: 286392 Color: 0

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 492611 Color: 1
Size: 255800 Color: 1
Size: 251590 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 368772 Color: 0
Size: 367724 Color: 0
Size: 263505 Color: 1

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 413156 Color: 1
Size: 295560 Color: 1
Size: 291285 Color: 0

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 382732 Color: 1
Size: 351407 Color: 0
Size: 265862 Color: 1

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 402482 Color: 1
Size: 344330 Color: 0
Size: 253189 Color: 1

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 441899 Color: 1
Size: 301416 Color: 1
Size: 256686 Color: 0

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 415231 Color: 1
Size: 304049 Color: 0
Size: 280721 Color: 1

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 334185 Color: 0
Size: 333658 Color: 0
Size: 332158 Color: 1

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 369941 Color: 0
Size: 341113 Color: 0
Size: 288947 Color: 1

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 483367 Color: 1
Size: 259136 Color: 0
Size: 257498 Color: 1

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 429299 Color: 1
Size: 320340 Color: 0
Size: 250362 Color: 1

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 392491 Color: 1
Size: 317788 Color: 0
Size: 289722 Color: 0

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 384368 Color: 0
Size: 357334 Color: 1
Size: 258299 Color: 0

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 403622 Color: 0
Size: 302935 Color: 0
Size: 293444 Color: 1

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 367561 Color: 0
Size: 318103 Color: 1
Size: 314337 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 443457 Color: 0
Size: 285693 Color: 1
Size: 270851 Color: 1

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 445447 Color: 1
Size: 278977 Color: 0
Size: 275577 Color: 0

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 449705 Color: 0
Size: 278787 Color: 1
Size: 271509 Color: 0

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 450219 Color: 0
Size: 282102 Color: 0
Size: 267680 Color: 1

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 378691 Color: 0
Size: 313974 Color: 1
Size: 307336 Color: 0

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 381144 Color: 0
Size: 332470 Color: 1
Size: 286387 Color: 0

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 411026 Color: 1
Size: 305208 Color: 1
Size: 283767 Color: 0

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 393282 Color: 1
Size: 343325 Color: 1
Size: 263394 Color: 0

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 378687 Color: 0
Size: 322628 Color: 1
Size: 298686 Color: 1

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 407849 Color: 0
Size: 338674 Color: 0
Size: 253478 Color: 1

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 378313 Color: 0
Size: 349948 Color: 0
Size: 271740 Color: 1

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 390840 Color: 1
Size: 354120 Color: 1
Size: 255041 Color: 0

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 486742 Color: 0
Size: 257392 Color: 0
Size: 255867 Color: 1

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 382633 Color: 0
Size: 340679 Color: 1
Size: 276689 Color: 0

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 473576 Color: 1
Size: 274761 Color: 1
Size: 251664 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 485621 Color: 0
Size: 262336 Color: 1
Size: 252044 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 413443 Color: 1
Size: 327710 Color: 1
Size: 258848 Color: 0

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 449536 Color: 1
Size: 285473 Color: 1
Size: 264992 Color: 0

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 369352 Color: 0
Size: 329970 Color: 1
Size: 300679 Color: 0

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 398512 Color: 1
Size: 350708 Color: 0
Size: 250781 Color: 1

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 427549 Color: 0
Size: 304490 Color: 1
Size: 267962 Color: 0

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 491504 Color: 1
Size: 256771 Color: 1
Size: 251726 Color: 0

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 464546 Color: 1
Size: 268282 Color: 0
Size: 267173 Color: 0

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 404319 Color: 0
Size: 329651 Color: 1
Size: 266031 Color: 0

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 442772 Color: 1
Size: 301497 Color: 0
Size: 255732 Color: 1

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 397594 Color: 1
Size: 326658 Color: 1
Size: 275749 Color: 0

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 370507 Color: 1
Size: 345565 Color: 0
Size: 283929 Color: 1

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 461423 Color: 1
Size: 281272 Color: 0
Size: 257306 Color: 1

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 363566 Color: 0
Size: 332590 Color: 1
Size: 303845 Color: 0

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 426212 Color: 0
Size: 295417 Color: 1
Size: 278372 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 478808 Color: 0
Size: 262991 Color: 1
Size: 258202 Color: 1

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 387791 Color: 1
Size: 314227 Color: 1
Size: 297983 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 371044 Color: 1
Size: 364679 Color: 0
Size: 264278 Color: 1

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 492252 Color: 0
Size: 257211 Color: 1
Size: 250538 Color: 0

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 399185 Color: 0
Size: 333839 Color: 1
Size: 266977 Color: 0

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 380675 Color: 0
Size: 360711 Color: 1
Size: 258615 Color: 0

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 379839 Color: 1
Size: 323500 Color: 0
Size: 296662 Color: 1

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 367277 Color: 0
Size: 346792 Color: 1
Size: 285932 Color: 0

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 425841 Color: 0
Size: 289997 Color: 0
Size: 284163 Color: 1

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 430482 Color: 0
Size: 307249 Color: 1
Size: 262270 Color: 0

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 430312 Color: 1
Size: 298914 Color: 1
Size: 270775 Color: 0

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 391991 Color: 1
Size: 348420 Color: 0
Size: 259590 Color: 1

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 395717 Color: 1
Size: 337222 Color: 1
Size: 267062 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 395158 Color: 0
Size: 329224 Color: 1
Size: 275619 Color: 0

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 443437 Color: 0
Size: 303750 Color: 0
Size: 252814 Color: 1

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 467698 Color: 1
Size: 273754 Color: 0
Size: 258549 Color: 0

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 399448 Color: 1
Size: 302873 Color: 1
Size: 297680 Color: 0

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 395604 Color: 1
Size: 316251 Color: 1
Size: 288146 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 395562 Color: 1
Size: 303947 Color: 1
Size: 300492 Color: 0

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 487855 Color: 1
Size: 256113 Color: 0
Size: 256033 Color: 0

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 430122 Color: 0
Size: 285556 Color: 0
Size: 284323 Color: 1

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 405790 Color: 1
Size: 319140 Color: 0
Size: 275071 Color: 1

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 491764 Color: 0
Size: 254437 Color: 1
Size: 253800 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 410716 Color: 0
Size: 339052 Color: 1
Size: 250233 Color: 0

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 373580 Color: 0
Size: 345013 Color: 0
Size: 281408 Color: 1

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 396995 Color: 1
Size: 317022 Color: 0
Size: 285984 Color: 1

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 479613 Color: 0
Size: 267930 Color: 1
Size: 252458 Color: 0

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 419973 Color: 0
Size: 319900 Color: 1
Size: 260128 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 486715 Color: 0
Size: 261345 Color: 1
Size: 251941 Color: 1

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 456819 Color: 0
Size: 278403 Color: 1
Size: 264779 Color: 1

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 435546 Color: 1
Size: 292310 Color: 1
Size: 272145 Color: 0

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 413568 Color: 1
Size: 318526 Color: 0
Size: 267907 Color: 1

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 455146 Color: 1
Size: 294690 Color: 1
Size: 250165 Color: 0

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 442525 Color: 0
Size: 284136 Color: 1
Size: 273340 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 413567 Color: 0
Size: 301272 Color: 1
Size: 285162 Color: 0

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 441002 Color: 1
Size: 280626 Color: 0
Size: 278373 Color: 1

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 450540 Color: 0
Size: 281739 Color: 0
Size: 267722 Color: 1

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 408894 Color: 1
Size: 340616 Color: 1
Size: 250491 Color: 0

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 442866 Color: 1
Size: 291799 Color: 0
Size: 265336 Color: 1

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 433446 Color: 1
Size: 284624 Color: 0
Size: 281931 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 398945 Color: 0
Size: 320034 Color: 1
Size: 281022 Color: 1

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 417174 Color: 0
Size: 298251 Color: 1
Size: 284576 Color: 0

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 486702 Color: 0
Size: 262762 Color: 1
Size: 250537 Color: 0

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 418776 Color: 0
Size: 323660 Color: 1
Size: 257565 Color: 1

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 485184 Color: 1
Size: 263180 Color: 0
Size: 251637 Color: 1

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 489144 Color: 1
Size: 260514 Color: 1
Size: 250343 Color: 0

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 392304 Color: 0
Size: 316628 Color: 1
Size: 291069 Color: 0

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 396305 Color: 1
Size: 350149 Color: 1
Size: 253547 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 363111 Color: 1
Size: 357972 Color: 0
Size: 278918 Color: 0

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 412659 Color: 1
Size: 329324 Color: 0
Size: 258018 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 428986 Color: 0
Size: 300683 Color: 0
Size: 270332 Color: 1

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 395868 Color: 1
Size: 349636 Color: 0
Size: 254497 Color: 1

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 419720 Color: 0
Size: 291721 Color: 0
Size: 288560 Color: 1

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 410802 Color: 1
Size: 320503 Color: 1
Size: 268696 Color: 0

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 387364 Color: 1
Size: 325120 Color: 0
Size: 287517 Color: 1

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 482231 Color: 0
Size: 259322 Color: 0
Size: 258448 Color: 1

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 471161 Color: 0
Size: 272062 Color: 1
Size: 256778 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 376824 Color: 0
Size: 366842 Color: 1
Size: 256335 Color: 0

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 441473 Color: 0
Size: 295251 Color: 1
Size: 263277 Color: 0

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 440840 Color: 1
Size: 303168 Color: 1
Size: 255993 Color: 0

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 376167 Color: 0
Size: 323448 Color: 0
Size: 300386 Color: 1

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 437964 Color: 1
Size: 304461 Color: 0
Size: 257576 Color: 1

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 402131 Color: 1
Size: 312848 Color: 1
Size: 285022 Color: 0

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 452014 Color: 1
Size: 290349 Color: 0
Size: 257638 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 437068 Color: 1
Size: 301365 Color: 0
Size: 261568 Color: 1

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 363007 Color: 1
Size: 327089 Color: 1
Size: 309905 Color: 0

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 417904 Color: 1
Size: 291757 Color: 0
Size: 290340 Color: 0

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 392401 Color: 0
Size: 352413 Color: 1
Size: 255187 Color: 0

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 373804 Color: 0
Size: 344292 Color: 0
Size: 281905 Color: 1

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 435562 Color: 0
Size: 291718 Color: 1
Size: 272721 Color: 1

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 408161 Color: 1
Size: 302678 Color: 0
Size: 289162 Color: 1

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 397766 Color: 0
Size: 336923 Color: 1
Size: 265312 Color: 1

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 450088 Color: 0
Size: 298018 Color: 0
Size: 251895 Color: 1

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 492461 Color: 0
Size: 255420 Color: 1
Size: 252120 Color: 1

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 391905 Color: 1
Size: 338541 Color: 1
Size: 269555 Color: 0

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 395664 Color: 0
Size: 352790 Color: 0
Size: 251547 Color: 1

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 369114 Color: 1
Size: 321590 Color: 0
Size: 309297 Color: 1

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 450806 Color: 1
Size: 279409 Color: 1
Size: 269786 Color: 0

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 369971 Color: 1
Size: 355782 Color: 1
Size: 274248 Color: 0

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 366531 Color: 1
Size: 361929 Color: 0
Size: 271541 Color: 1

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 485177 Color: 1
Size: 263406 Color: 0
Size: 251418 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 387413 Color: 0
Size: 337560 Color: 0
Size: 275028 Color: 1

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 418052 Color: 0
Size: 311718 Color: 0
Size: 270231 Color: 1

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 446102 Color: 0
Size: 286653 Color: 1
Size: 267246 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 494253 Color: 1
Size: 254847 Color: 1
Size: 250901 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 450263 Color: 0
Size: 287892 Color: 1
Size: 261846 Color: 1

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 447421 Color: 0
Size: 279411 Color: 0
Size: 273169 Color: 1

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 427741 Color: 1
Size: 302467 Color: 0
Size: 269793 Color: 1

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 468470 Color: 1
Size: 280254 Color: 1
Size: 251277 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 349184 Color: 1
Size: 337023 Color: 0
Size: 313794 Color: 0

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 414169 Color: 0
Size: 331066 Color: 0
Size: 254766 Color: 1

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 453939 Color: 1
Size: 281922 Color: 1
Size: 264140 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 398067 Color: 0
Size: 329921 Color: 0
Size: 272013 Color: 1

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 357065 Color: 1
Size: 324532 Color: 1
Size: 318404 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 369461 Color: 0
Size: 352201 Color: 1
Size: 278339 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 482516 Color: 0
Size: 260792 Color: 1
Size: 256693 Color: 1

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 390861 Color: 0
Size: 333769 Color: 1
Size: 275371 Color: 0

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 451473 Color: 1
Size: 278651 Color: 0
Size: 269877 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 384616 Color: 0
Size: 315831 Color: 1
Size: 299554 Color: 0

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 408086 Color: 0
Size: 335142 Color: 0
Size: 256773 Color: 1

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 362352 Color: 1
Size: 352208 Color: 0
Size: 285441 Color: 1

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 489508 Color: 1
Size: 256327 Color: 0
Size: 254166 Color: 1

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 478027 Color: 1
Size: 271323 Color: 1
Size: 250651 Color: 0

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 492505 Color: 0
Size: 256264 Color: 1
Size: 251232 Color: 1

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 377428 Color: 1
Size: 364886 Color: 0
Size: 257687 Color: 1

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 465677 Color: 1
Size: 267382 Color: 0
Size: 266942 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 366694 Color: 1
Size: 346832 Color: 1
Size: 286475 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 373144 Color: 0
Size: 325524 Color: 0
Size: 301333 Color: 1

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 411383 Color: 0
Size: 335188 Color: 0
Size: 253430 Color: 1

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 353486 Color: 0
Size: 329153 Color: 0
Size: 317362 Color: 1

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 380481 Color: 0
Size: 366690 Color: 0
Size: 252830 Color: 1

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 390341 Color: 1
Size: 358424 Color: 1
Size: 251236 Color: 0

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 375535 Color: 0
Size: 358906 Color: 1
Size: 265560 Color: 0

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 445842 Color: 0
Size: 284743 Color: 0
Size: 269416 Color: 1

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 494020 Color: 0
Size: 253996 Color: 0
Size: 251985 Color: 1

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 381449 Color: 0
Size: 340433 Color: 1
Size: 278119 Color: 1

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 389832 Color: 0
Size: 324179 Color: 0
Size: 285990 Color: 1

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 471649 Color: 1
Size: 266235 Color: 0
Size: 262117 Color: 1

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 416393 Color: 1
Size: 304832 Color: 0
Size: 278776 Color: 1

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 422362 Color: 0
Size: 304708 Color: 0
Size: 272931 Color: 1

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 416337 Color: 1
Size: 308956 Color: 0
Size: 274708 Color: 1

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 387941 Color: 1
Size: 320962 Color: 0
Size: 291098 Color: 0

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 408564 Color: 0
Size: 311424 Color: 0
Size: 280013 Color: 1

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 469236 Color: 0
Size: 275194 Color: 0
Size: 255571 Color: 1

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 442138 Color: 1
Size: 292308 Color: 0
Size: 265555 Color: 1

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 441458 Color: 0
Size: 300418 Color: 1
Size: 258125 Color: 0

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 367844 Color: 1
Size: 331402 Color: 0
Size: 300755 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 496517 Color: 0
Size: 252223 Color: 1
Size: 251261 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 411713 Color: 0
Size: 303165 Color: 0
Size: 285123 Color: 1

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 455821 Color: 1
Size: 292650 Color: 1
Size: 251530 Color: 0

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 434727 Color: 0
Size: 295023 Color: 1
Size: 270251 Color: 1

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 374859 Color: 0
Size: 337586 Color: 1
Size: 287556 Color: 0

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 440708 Color: 0
Size: 295024 Color: 1
Size: 264269 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 391264 Color: 1
Size: 342028 Color: 0
Size: 266709 Color: 1

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 391596 Color: 1
Size: 355570 Color: 0
Size: 252835 Color: 1

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 428983 Color: 1
Size: 308401 Color: 0
Size: 262617 Color: 0

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 413182 Color: 1
Size: 315466 Color: 0
Size: 271353 Color: 1

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 391775 Color: 1
Size: 332931 Color: 1
Size: 275295 Color: 0

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 425869 Color: 1
Size: 316823 Color: 0
Size: 257309 Color: 1

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 404343 Color: 1
Size: 307423 Color: 0
Size: 288235 Color: 1

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 462490 Color: 0
Size: 281799 Color: 0
Size: 255712 Color: 1

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 370683 Color: 0
Size: 349255 Color: 1
Size: 280063 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 381449 Color: 1
Size: 367805 Color: 1
Size: 250747 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 417341 Color: 1
Size: 291697 Color: 1
Size: 290963 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 429383 Color: 0
Size: 318786 Color: 0
Size: 251832 Color: 1

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 357255 Color: 1
Size: 353702 Color: 1
Size: 289044 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 439725 Color: 1
Size: 285942 Color: 0
Size: 274334 Color: 1

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 480133 Color: 0
Size: 261670 Color: 1
Size: 258198 Color: 0

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 368579 Color: 0
Size: 325489 Color: 0
Size: 305933 Color: 1

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 351257 Color: 1
Size: 349987 Color: 1
Size: 298757 Color: 0

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 408770 Color: 1
Size: 306407 Color: 0
Size: 284824 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 402217 Color: 1
Size: 303017 Color: 0
Size: 294767 Color: 1

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 402624 Color: 1
Size: 325205 Color: 1
Size: 272172 Color: 0

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 435578 Color: 0
Size: 310032 Color: 1
Size: 254391 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 452112 Color: 1
Size: 274694 Color: 0
Size: 273195 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 454581 Color: 1
Size: 282061 Color: 0
Size: 263359 Color: 1

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 397909 Color: 1
Size: 325483 Color: 0
Size: 276609 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 382130 Color: 1
Size: 334380 Color: 1
Size: 283491 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 394507 Color: 1
Size: 324002 Color: 0
Size: 281492 Color: 0

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 386658 Color: 1
Size: 327473 Color: 1
Size: 285870 Color: 0

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 350396 Color: 0
Size: 329137 Color: 1
Size: 320468 Color: 0

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 450000 Color: 0
Size: 296753 Color: 1
Size: 253248 Color: 1

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 436015 Color: 0
Size: 284688 Color: 1
Size: 279298 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 446941 Color: 0
Size: 293823 Color: 0
Size: 259237 Color: 1

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 373940 Color: 1
Size: 347739 Color: 0
Size: 278322 Color: 1

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 449093 Color: 1
Size: 285264 Color: 0
Size: 265644 Color: 0

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 456601 Color: 1
Size: 271927 Color: 0
Size: 271473 Color: 1

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 402043 Color: 1
Size: 335097 Color: 1
Size: 262861 Color: 0

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 433068 Color: 0
Size: 315218 Color: 1
Size: 251715 Color: 0

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 402585 Color: 0
Size: 306540 Color: 0
Size: 290876 Color: 1

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 391897 Color: 1
Size: 339295 Color: 0
Size: 268809 Color: 0

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 397931 Color: 0
Size: 314159 Color: 0
Size: 287911 Color: 1

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 437276 Color: 1
Size: 300384 Color: 0
Size: 262341 Color: 1

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 472065 Color: 1
Size: 269669 Color: 1
Size: 258267 Color: 0

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 410179 Color: 0
Size: 326982 Color: 1
Size: 262840 Color: 0

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 440860 Color: 0
Size: 293829 Color: 0
Size: 265312 Color: 1

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 478269 Color: 0
Size: 271284 Color: 0
Size: 250448 Color: 1

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 374129 Color: 0
Size: 342749 Color: 0
Size: 283123 Color: 1

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 345997 Color: 0
Size: 337767 Color: 1
Size: 316237 Color: 0

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 349430 Color: 1
Size: 347190 Color: 0
Size: 303381 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 350368 Color: 0
Size: 348833 Color: 0
Size: 300800 Color: 1

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 471823 Color: 1
Size: 277380 Color: 0
Size: 250798 Color: 1

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 411771 Color: 1
Size: 335651 Color: 1
Size: 252579 Color: 0

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 385844 Color: 1
Size: 317932 Color: 0
Size: 296225 Color: 1

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 372408 Color: 1
Size: 340907 Color: 1
Size: 286686 Color: 0

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 436265 Color: 1
Size: 287728 Color: 0
Size: 276008 Color: 1

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 478331 Color: 0
Size: 263320 Color: 1
Size: 258350 Color: 0

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 480426 Color: 1
Size: 269481 Color: 0
Size: 250094 Color: 1

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 367990 Color: 1
Size: 327153 Color: 0
Size: 304858 Color: 1

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 481747 Color: 0
Size: 261885 Color: 0
Size: 256369 Color: 1

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 343157 Color: 0
Size: 343137 Color: 0
Size: 313707 Color: 1

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 407100 Color: 0
Size: 333545 Color: 1
Size: 259356 Color: 1

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 403995 Color: 0
Size: 306683 Color: 1
Size: 289323 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 392770 Color: 1
Size: 355244 Color: 1
Size: 251987 Color: 0

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 464066 Color: 0
Size: 273168 Color: 1
Size: 262767 Color: 1

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 391424 Color: 1
Size: 323119 Color: 0
Size: 285458 Color: 1

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 363865 Color: 1
Size: 327055 Color: 1
Size: 309081 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 458736 Color: 1
Size: 286031 Color: 0
Size: 255234 Color: 1

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 355992 Color: 0
Size: 349995 Color: 1
Size: 294014 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 397232 Color: 1
Size: 311301 Color: 0
Size: 291468 Color: 1

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 470489 Color: 0
Size: 274534 Color: 1
Size: 254978 Color: 0

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 340420 Color: 1
Size: 339030 Color: 0
Size: 320551 Color: 1

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 400594 Color: 0
Size: 331300 Color: 1
Size: 268107 Color: 0

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 439621 Color: 1
Size: 290927 Color: 1
Size: 269453 Color: 0

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 370336 Color: 1
Size: 329579 Color: 1
Size: 300086 Color: 0

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 354571 Color: 0
Size: 334241 Color: 0
Size: 311189 Color: 1

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 448962 Color: 1
Size: 285912 Color: 0
Size: 265127 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 419829 Color: 0
Size: 308726 Color: 1
Size: 271446 Color: 0

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 427019 Color: 0
Size: 317675 Color: 0
Size: 255307 Color: 1

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 427706 Color: 1
Size: 304010 Color: 0
Size: 268285 Color: 1

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 406459 Color: 0
Size: 341747 Color: 0
Size: 251795 Color: 1

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 493671 Color: 0
Size: 254174 Color: 1
Size: 252156 Color: 1

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 407327 Color: 1
Size: 314094 Color: 0
Size: 278580 Color: 1

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 481526 Color: 0
Size: 260065 Color: 0
Size: 258410 Color: 1

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 409883 Color: 0
Size: 297032 Color: 1
Size: 293086 Color: 0

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 371753 Color: 1
Size: 348682 Color: 1
Size: 279566 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 385381 Color: 0
Size: 360131 Color: 1
Size: 254489 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 439851 Color: 1
Size: 305891 Color: 1
Size: 254259 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 404018 Color: 1
Size: 299503 Color: 0
Size: 296480 Color: 1

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 497014 Color: 1
Size: 252293 Color: 1
Size: 250694 Color: 0

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 486809 Color: 0
Size: 257105 Color: 1
Size: 256087 Color: 1

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 376605 Color: 0
Size: 313610 Color: 1
Size: 309786 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 398263 Color: 0
Size: 344512 Color: 1
Size: 257226 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 380209 Color: 0
Size: 312071 Color: 1
Size: 307721 Color: 1

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 436600 Color: 1
Size: 303665 Color: 1
Size: 259736 Color: 0

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 417213 Color: 1
Size: 309144 Color: 1
Size: 273644 Color: 0

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 363452 Color: 1
Size: 336124 Color: 0
Size: 300425 Color: 1

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 426464 Color: 0
Size: 293715 Color: 1
Size: 279822 Color: 0

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 392028 Color: 0
Size: 312826 Color: 0
Size: 295147 Color: 1

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 385916 Color: 1
Size: 329692 Color: 1
Size: 284393 Color: 0

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 378100 Color: 0
Size: 325937 Color: 1
Size: 295964 Color: 0

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 385990 Color: 0
Size: 328072 Color: 0
Size: 285939 Color: 1

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 420796 Color: 0
Size: 289614 Color: 1
Size: 289591 Color: 1

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 385015 Color: 0
Size: 309868 Color: 1
Size: 305118 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 395110 Color: 1
Size: 315722 Color: 0
Size: 289169 Color: 1

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 377828 Color: 1
Size: 321808 Color: 0
Size: 300365 Color: 0

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 460469 Color: 0
Size: 285928 Color: 1
Size: 253604 Color: 0

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 433320 Color: 0
Size: 291931 Color: 1
Size: 274750 Color: 1

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 398774 Color: 1
Size: 347906 Color: 0
Size: 253321 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 476309 Color: 0
Size: 269261 Color: 0
Size: 254431 Color: 1

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 351450 Color: 1
Size: 351053 Color: 1
Size: 297498 Color: 0

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 359409 Color: 1
Size: 344095 Color: 1
Size: 296497 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 456306 Color: 1
Size: 290733 Color: 0
Size: 252962 Color: 1

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 357578 Color: 0
Size: 353336 Color: 0
Size: 289087 Color: 1

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 346585 Color: 0
Size: 344695 Color: 1
Size: 308721 Color: 1

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 360983 Color: 1
Size: 332030 Color: 0
Size: 306988 Color: 1

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 352334 Color: 1
Size: 326553 Color: 0
Size: 321114 Color: 0

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 351593 Color: 1
Size: 328383 Color: 0
Size: 320025 Color: 1

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 388407 Color: 1
Size: 341724 Color: 0
Size: 269870 Color: 0

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 407666 Color: 1
Size: 319519 Color: 1
Size: 272816 Color: 0

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 357551 Color: 1
Size: 322558 Color: 1
Size: 319892 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 359787 Color: 0
Size: 341772 Color: 0
Size: 298442 Color: 1

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 361182 Color: 1
Size: 330683 Color: 1
Size: 308136 Color: 0

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 345517 Color: 1
Size: 335944 Color: 0
Size: 318540 Color: 0

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 346349 Color: 0
Size: 345821 Color: 1
Size: 307831 Color: 1

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 411154 Color: 0
Size: 318311 Color: 1
Size: 270536 Color: 0

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 355254 Color: 0
Size: 345287 Color: 1
Size: 299460 Color: 1

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 377338 Color: 1
Size: 350166 Color: 0
Size: 272497 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 358568 Color: 0
Size: 323102 Color: 1
Size: 318331 Color: 1

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 353490 Color: 0
Size: 332014 Color: 0
Size: 314497 Color: 1

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 367909 Color: 1
Size: 354628 Color: 1
Size: 277464 Color: 0

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 389288 Color: 1
Size: 345312 Color: 1
Size: 265401 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 431400 Color: 1
Size: 311477 Color: 1
Size: 257124 Color: 0

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 354813 Color: 0
Size: 332701 Color: 1
Size: 312487 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 379845 Color: 0
Size: 361972 Color: 1
Size: 258184 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 357599 Color: 0
Size: 339863 Color: 1
Size: 302539 Color: 1

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 351275 Color: 1
Size: 336171 Color: 1
Size: 312555 Color: 0

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 406405 Color: 0
Size: 313069 Color: 1
Size: 280527 Color: 0

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 344920 Color: 0
Size: 332531 Color: 1
Size: 322550 Color: 0

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 350050 Color: 1
Size: 336834 Color: 0
Size: 313117 Color: 1

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 346145 Color: 1
Size: 340827 Color: 1
Size: 313029 Color: 0

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 350832 Color: 1
Size: 326776 Color: 1
Size: 322393 Color: 0

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 351597 Color: 0
Size: 333581 Color: 1
Size: 314823 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 350159 Color: 0
Size: 342977 Color: 1
Size: 306865 Color: 0

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 349435 Color: 0
Size: 336855 Color: 1
Size: 313711 Color: 0

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 430414 Color: 0
Size: 293428 Color: 1
Size: 276159 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 343721 Color: 0
Size: 343525 Color: 0
Size: 312755 Color: 1

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 355294 Color: 1
Size: 335971 Color: 0
Size: 308736 Color: 0

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 350758 Color: 1
Size: 333037 Color: 0
Size: 316206 Color: 1

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 340841 Color: 1
Size: 332981 Color: 1
Size: 326179 Color: 0

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 340562 Color: 1
Size: 333739 Color: 1
Size: 325700 Color: 0

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 441577 Color: 0
Size: 295319 Color: 1
Size: 263105 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 350624 Color: 1
Size: 325201 Color: 0
Size: 324176 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 410450 Color: 1
Size: 330457 Color: 0
Size: 259094 Color: 0

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 427974 Color: 1
Size: 309104 Color: 0
Size: 262923 Color: 1

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 463581 Color: 0
Size: 284982 Color: 1
Size: 251438 Color: 1

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 492323 Color: 0
Size: 255421 Color: 1
Size: 252257 Color: 0

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 364138 Color: 1
Size: 352631 Color: 0
Size: 283232 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 362260 Color: 1
Size: 359718 Color: 1
Size: 278023 Color: 0

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 348373 Color: 1
Size: 341229 Color: 1
Size: 310399 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 355938 Color: 1
Size: 351752 Color: 0
Size: 292311 Color: 0

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 392892 Color: 0
Size: 338118 Color: 1
Size: 268991 Color: 1

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 473110 Color: 0
Size: 274376 Color: 1
Size: 252515 Color: 0

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 370464 Color: 1
Size: 354946 Color: 0
Size: 274591 Color: 1

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 345905 Color: 1
Size: 345519 Color: 1
Size: 308577 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 461341 Color: 1
Size: 273008 Color: 0
Size: 265652 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 345378 Color: 0
Size: 344744 Color: 1
Size: 309879 Color: 1

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 477749 Color: 1
Size: 269211 Color: 0
Size: 253041 Color: 1

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 421994 Color: 0
Size: 301205 Color: 0
Size: 276802 Color: 1

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 353872 Color: 0
Size: 344275 Color: 0
Size: 301854 Color: 1

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 343939 Color: 0
Size: 332281 Color: 1
Size: 323781 Color: 1

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 350784 Color: 0
Size: 339532 Color: 1
Size: 309685 Color: 0

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 397491 Color: 0
Size: 341243 Color: 0
Size: 261267 Color: 1

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 407090 Color: 0
Size: 305561 Color: 0
Size: 287350 Color: 1

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 336357 Color: 1
Size: 336157 Color: 1
Size: 327487 Color: 0

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 341505 Color: 1
Size: 336637 Color: 0
Size: 321859 Color: 0

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 344674 Color: 1
Size: 330014 Color: 0
Size: 325313 Color: 0

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 335041 Color: 1
Size: 334377 Color: 1
Size: 330583 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 340431 Color: 1
Size: 335346 Color: 0
Size: 324224 Color: 1

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 353235 Color: 0
Size: 343320 Color: 0
Size: 303446 Color: 1

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 353882 Color: 0
Size: 345080 Color: 0
Size: 301039 Color: 1

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 354081 Color: 0
Size: 333493 Color: 0
Size: 312427 Color: 1

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 354662 Color: 1
Size: 343245 Color: 0
Size: 302094 Color: 1

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 354715 Color: 0
Size: 324845 Color: 0
Size: 320441 Color: 1

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 355407 Color: 1
Size: 332976 Color: 1
Size: 311618 Color: 0

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 355851 Color: 1
Size: 351904 Color: 0
Size: 292246 Color: 1

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 355595 Color: 0
Size: 323086 Color: 0
Size: 321320 Color: 1

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 355680 Color: 0
Size: 348960 Color: 1
Size: 295361 Color: 0

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 356038 Color: 1
Size: 337686 Color: 0
Size: 306277 Color: 1

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 355976 Color: 1
Size: 339405 Color: 1
Size: 304620 Color: 0

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 356039 Color: 0
Size: 355079 Color: 0
Size: 288883 Color: 1

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 355981 Color: 1
Size: 341802 Color: 0
Size: 302218 Color: 1

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 356545 Color: 1
Size: 336894 Color: 1
Size: 306562 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 356658 Color: 1
Size: 339404 Color: 1
Size: 303939 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 356871 Color: 1
Size: 324843 Color: 0
Size: 318287 Color: 1

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 357406 Color: 0
Size: 344556 Color: 0
Size: 298039 Color: 1

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 356873 Color: 1
Size: 330794 Color: 1
Size: 312334 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 356907 Color: 1
Size: 321888 Color: 0
Size: 321206 Color: 1

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 357526 Color: 1
Size: 342611 Color: 0
Size: 299864 Color: 1

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 358338 Color: 0
Size: 339644 Color: 0
Size: 302019 Color: 1

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 358099 Color: 1
Size: 328710 Color: 0
Size: 313192 Color: 1

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 358637 Color: 0
Size: 356920 Color: 1
Size: 284444 Color: 0

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 358669 Color: 0
Size: 355724 Color: 0
Size: 285608 Color: 1

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 358780 Color: 0
Size: 324978 Color: 1
Size: 316243 Color: 0

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 358899 Color: 1
Size: 325518 Color: 0
Size: 315584 Color: 1

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 358903 Color: 1
Size: 353990 Color: 0
Size: 287108 Color: 1

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 359044 Color: 0
Size: 328285 Color: 0
Size: 312672 Color: 1

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 359004 Color: 1
Size: 324608 Color: 0
Size: 316389 Color: 1

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 359540 Color: 0
Size: 321808 Color: 1
Size: 318653 Color: 1

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 359714 Color: 1
Size: 332328 Color: 0
Size: 307959 Color: 1

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 360052 Color: 1
Size: 337802 Color: 1
Size: 302147 Color: 0

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 360216 Color: 1
Size: 354726 Color: 0
Size: 285059 Color: 1

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 360173 Color: 0
Size: 345670 Color: 1
Size: 294158 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 360588 Color: 1
Size: 328021 Color: 1
Size: 311392 Color: 0

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 360753 Color: 1
Size: 332581 Color: 0
Size: 306667 Color: 1

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 360756 Color: 1
Size: 343727 Color: 0
Size: 295518 Color: 1

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 360793 Color: 1
Size: 337159 Color: 0
Size: 302049 Color: 0

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 360900 Color: 0
Size: 333640 Color: 1
Size: 305461 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 361161 Color: 0
Size: 328161 Color: 1
Size: 310679 Color: 0

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 361524 Color: 1
Size: 324695 Color: 1
Size: 313782 Color: 0

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 361179 Color: 0
Size: 321257 Color: 1
Size: 317565 Color: 0

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 361532 Color: 1
Size: 337384 Color: 1
Size: 301085 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 361214 Color: 0
Size: 334584 Color: 0
Size: 304203 Color: 1

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 361265 Color: 0
Size: 322055 Color: 0
Size: 316681 Color: 1

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 361688 Color: 1
Size: 342757 Color: 0
Size: 295556 Color: 1

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 361866 Color: 1
Size: 325117 Color: 0
Size: 313018 Color: 1

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 362058 Color: 1
Size: 331073 Color: 0
Size: 306870 Color: 0

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 362083 Color: 1
Size: 334377 Color: 0
Size: 303541 Color: 1

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 361891 Color: 0
Size: 354804 Color: 0
Size: 283306 Color: 1

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 361961 Color: 0
Size: 328650 Color: 1
Size: 309390 Color: 0

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 362312 Color: 1
Size: 323838 Color: 1
Size: 313851 Color: 0

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 362315 Color: 0
Size: 330384 Color: 0
Size: 307302 Color: 1

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 362355 Color: 1
Size: 323679 Color: 1
Size: 313967 Color: 0

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 362387 Color: 1
Size: 325887 Color: 1
Size: 311727 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 362395 Color: 0
Size: 353283 Color: 0
Size: 284323 Color: 1

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 362681 Color: 1
Size: 327111 Color: 0
Size: 310209 Color: 1

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 362815 Color: 1
Size: 348040 Color: 0
Size: 289146 Color: 1

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 362730 Color: 0
Size: 336226 Color: 1
Size: 301045 Color: 0

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 363216 Color: 1
Size: 341804 Color: 0
Size: 294981 Color: 1

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 363382 Color: 1
Size: 336871 Color: 1
Size: 299748 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 363424 Color: 1
Size: 319568 Color: 0
Size: 317009 Color: 1

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 363195 Color: 0
Size: 324948 Color: 0
Size: 311858 Color: 1

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 363323 Color: 0
Size: 320411 Color: 1
Size: 316267 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 363371 Color: 0
Size: 335214 Color: 0
Size: 301416 Color: 1

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 363590 Color: 0
Size: 339422 Color: 0
Size: 296989 Color: 1

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 363847 Color: 0
Size: 319603 Color: 0
Size: 316551 Color: 1

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 364061 Color: 0
Size: 335519 Color: 0
Size: 300421 Color: 1

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 364007 Color: 1
Size: 335980 Color: 1
Size: 300014 Color: 0

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 364216 Color: 0
Size: 353231 Color: 1
Size: 282554 Color: 0

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 364337 Color: 0
Size: 327554 Color: 0
Size: 308110 Color: 1

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 364195 Color: 1
Size: 341976 Color: 1
Size: 293830 Color: 0

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 364474 Color: 0
Size: 335231 Color: 0
Size: 300296 Color: 1

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 364291 Color: 1
Size: 342182 Color: 0
Size: 293528 Color: 1

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 364569 Color: 0
Size: 318021 Color: 1
Size: 317411 Color: 0

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 364567 Color: 1
Size: 346953 Color: 1
Size: 288481 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 364596 Color: 1
Size: 333350 Color: 1
Size: 302055 Color: 0

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 365050 Color: 0
Size: 326762 Color: 1
Size: 308189 Color: 0

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 364635 Color: 1
Size: 324825 Color: 1
Size: 310541 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 364840 Color: 1
Size: 351981 Color: 0
Size: 283180 Color: 1

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 365223 Color: 0
Size: 339375 Color: 1
Size: 295403 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 365238 Color: 0
Size: 339665 Color: 0
Size: 295098 Color: 1

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 365239 Color: 1
Size: 345291 Color: 0
Size: 289471 Color: 1

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 365250 Color: 0
Size: 344037 Color: 1
Size: 290714 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 365409 Color: 0
Size: 343054 Color: 0
Size: 291538 Color: 1

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 365402 Color: 1
Size: 327808 Color: 0
Size: 306791 Color: 1

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 365496 Color: 0
Size: 336854 Color: 1
Size: 297651 Color: 0

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 365591 Color: 1
Size: 353960 Color: 0
Size: 280450 Color: 1

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 365978 Color: 1
Size: 355828 Color: 1
Size: 278195 Color: 0

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 366023 Color: 1
Size: 361523 Color: 0
Size: 272455 Color: 1

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 365883 Color: 0
Size: 362547 Color: 0
Size: 271571 Color: 1

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 366103 Color: 1
Size: 330186 Color: 1
Size: 303712 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 365973 Color: 0
Size: 361178 Color: 0
Size: 272850 Color: 1

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 366121 Color: 1
Size: 358119 Color: 0
Size: 275761 Color: 1

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 366106 Color: 0
Size: 358766 Color: 1
Size: 275129 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 366144 Color: 1
Size: 337762 Color: 0
Size: 296095 Color: 1

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 366225 Color: 0
Size: 346987 Color: 0
Size: 286789 Color: 1

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 366317 Color: 1
Size: 349755 Color: 0
Size: 283929 Color: 1

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 366328 Color: 1
Size: 357281 Color: 0
Size: 276392 Color: 1

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 366444 Color: 0
Size: 336556 Color: 0
Size: 297001 Color: 1

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 366404 Color: 1
Size: 363265 Color: 0
Size: 270332 Color: 1

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 366601 Color: 0
Size: 346075 Color: 0
Size: 287325 Color: 1

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 366428 Color: 1
Size: 317134 Color: 1
Size: 316439 Color: 0

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 366642 Color: 0
Size: 352094 Color: 0
Size: 281265 Color: 1

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 366784 Color: 0
Size: 339842 Color: 1
Size: 293375 Color: 0

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 366853 Color: 0
Size: 361390 Color: 0
Size: 271758 Color: 1

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 366854 Color: 0
Size: 358920 Color: 1
Size: 274227 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 367014 Color: 1
Size: 338090 Color: 0
Size: 294897 Color: 1

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 367183 Color: 1
Size: 334549 Color: 0
Size: 298269 Color: 1

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 367240 Color: 1
Size: 354237 Color: 1
Size: 278524 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 367322 Color: 0
Size: 347355 Color: 0
Size: 285324 Color: 1

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 367324 Color: 0
Size: 321018 Color: 0
Size: 311659 Color: 1

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 367310 Color: 1
Size: 335115 Color: 0
Size: 297576 Color: 1

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 367359 Color: 0
Size: 362102 Color: 0
Size: 270540 Color: 1

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 367313 Color: 1
Size: 359416 Color: 1
Size: 273272 Color: 0

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 367414 Color: 0
Size: 344903 Color: 0
Size: 287684 Color: 1

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 367396 Color: 1
Size: 352355 Color: 1
Size: 280250 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 367507 Color: 1
Size: 324795 Color: 1
Size: 307699 Color: 0

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 367575 Color: 0
Size: 339138 Color: 0
Size: 293288 Color: 1

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 367662 Color: 1
Size: 328068 Color: 1
Size: 304271 Color: 0

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 367596 Color: 0
Size: 357350 Color: 0
Size: 275055 Color: 1

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 367705 Color: 1
Size: 350612 Color: 1
Size: 281684 Color: 0

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 367837 Color: 1
Size: 359764 Color: 1
Size: 272400 Color: 0

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 367886 Color: 1
Size: 322591 Color: 1
Size: 309524 Color: 0

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 367891 Color: 1
Size: 324233 Color: 0
Size: 307877 Color: 1

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 367956 Color: 0
Size: 336178 Color: 0
Size: 295867 Color: 1

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 367959 Color: 0
Size: 337466 Color: 0
Size: 294576 Color: 1

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 368044 Color: 1
Size: 339048 Color: 1
Size: 292909 Color: 0

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 368086 Color: 1
Size: 361937 Color: 1
Size: 269978 Color: 0

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 368364 Color: 1
Size: 356505 Color: 0
Size: 275132 Color: 1

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 368208 Color: 1
Size: 325770 Color: 0
Size: 306023 Color: 1

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 368273 Color: 1
Size: 349511 Color: 1
Size: 282217 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 368353 Color: 1
Size: 367813 Color: 0
Size: 263835 Color: 1

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 368859 Color: 1
Size: 323809 Color: 1
Size: 307333 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 369047 Color: 0
Size: 333261 Color: 0
Size: 297693 Color: 1

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 368908 Color: 1
Size: 362398 Color: 1
Size: 268695 Color: 0

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 369054 Color: 0
Size: 329182 Color: 0
Size: 301765 Color: 1

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 368999 Color: 1
Size: 325525 Color: 1
Size: 305477 Color: 0

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 369107 Color: 1
Size: 363530 Color: 0
Size: 267364 Color: 0

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 369142 Color: 1
Size: 332501 Color: 0
Size: 298358 Color: 0

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 369421 Color: 1
Size: 366652 Color: 1
Size: 263928 Color: 0

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 369508 Color: 1
Size: 344449 Color: 0
Size: 286044 Color: 1

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 369759 Color: 1
Size: 354180 Color: 1
Size: 276062 Color: 0

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 369769 Color: 0
Size: 342897 Color: 1
Size: 287335 Color: 0

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 369807 Color: 0
Size: 318177 Color: 1
Size: 312017 Color: 0

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 369935 Color: 1
Size: 337299 Color: 0
Size: 292767 Color: 1

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 370090 Color: 1
Size: 347370 Color: 0
Size: 282541 Color: 0

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 370101 Color: 1
Size: 352898 Color: 0
Size: 277002 Color: 1

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 370144 Color: 1
Size: 368849 Color: 0
Size: 261008 Color: 1

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 370218 Color: 1
Size: 337970 Color: 0
Size: 291813 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 370303 Color: 0
Size: 320578 Color: 0
Size: 309120 Color: 1

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 370341 Color: 1
Size: 354074 Color: 0
Size: 275586 Color: 0

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 370396 Color: 1
Size: 327102 Color: 0
Size: 302503 Color: 1

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 370473 Color: 0
Size: 353928 Color: 1
Size: 275600 Color: 0

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 370438 Color: 1
Size: 350802 Color: 0
Size: 278761 Color: 1

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 370646 Color: 0
Size: 366549 Color: 0
Size: 262806 Color: 1

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 370846 Color: 1
Size: 333521 Color: 0
Size: 295634 Color: 0

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 370924 Color: 1
Size: 346563 Color: 1
Size: 282514 Color: 0

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 371242 Color: 0
Size: 331590 Color: 0
Size: 297169 Color: 1

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 371479 Color: 0
Size: 331848 Color: 1
Size: 296674 Color: 0

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 371486 Color: 0
Size: 352294 Color: 1
Size: 276221 Color: 0

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 371488 Color: 1
Size: 369542 Color: 1
Size: 258971 Color: 0

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 371502 Color: 0
Size: 355386 Color: 1
Size: 273113 Color: 0

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 371570 Color: 0
Size: 342024 Color: 0
Size: 286407 Color: 1

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 371652 Color: 1
Size: 340565 Color: 1
Size: 287784 Color: 0

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 371693 Color: 0
Size: 333248 Color: 1
Size: 295060 Color: 0

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 371746 Color: 1
Size: 354622 Color: 0
Size: 273633 Color: 1

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 371721 Color: 0
Size: 343982 Color: 0
Size: 284298 Color: 1

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 371860 Color: 0
Size: 332971 Color: 0
Size: 295170 Color: 1

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 372002 Color: 1
Size: 341093 Color: 0
Size: 286906 Color: 1

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 371995 Color: 0
Size: 325638 Color: 1
Size: 302368 Color: 0

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 372200 Color: 1
Size: 355912 Color: 0
Size: 271889 Color: 1

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 372251 Color: 0
Size: 313941 Color: 0
Size: 313809 Color: 1

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 372353 Color: 0
Size: 320303 Color: 1
Size: 307345 Color: 0

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 372452 Color: 1
Size: 362121 Color: 1
Size: 265428 Color: 0

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 372476 Color: 0
Size: 315842 Color: 1
Size: 311683 Color: 0

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 372593 Color: 0
Size: 336292 Color: 0
Size: 291116 Color: 1

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 372596 Color: 0
Size: 351130 Color: 1
Size: 276275 Color: 1

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 372638 Color: 0
Size: 334841 Color: 0
Size: 292522 Color: 1

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 372671 Color: 1
Size: 340915 Color: 1
Size: 286415 Color: 0

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 372706 Color: 0
Size: 328120 Color: 1
Size: 299175 Color: 0

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 372687 Color: 1
Size: 331298 Color: 1
Size: 296016 Color: 0

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 372764 Color: 0
Size: 328929 Color: 1
Size: 298308 Color: 0

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 372867 Color: 0
Size: 360327 Color: 0
Size: 266807 Color: 1

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 373093 Color: 1
Size: 370335 Color: 0
Size: 256573 Color: 1

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 373007 Color: 0
Size: 341752 Color: 1
Size: 285242 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 373115 Color: 1
Size: 318283 Color: 1
Size: 308603 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 373041 Color: 0
Size: 322053 Color: 1
Size: 304907 Color: 0

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 373124 Color: 1
Size: 314183 Color: 1
Size: 312694 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 373314 Color: 0
Size: 318039 Color: 0
Size: 308648 Color: 1

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 373369 Color: 1
Size: 365406 Color: 0
Size: 261226 Color: 1

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 373432 Color: 1
Size: 331682 Color: 0
Size: 294887 Color: 1

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 373348 Color: 0
Size: 344062 Color: 0
Size: 282591 Color: 1

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 373464 Color: 1
Size: 322216 Color: 0
Size: 304321 Color: 1

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 373431 Color: 0
Size: 313963 Color: 0
Size: 312607 Color: 1

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 373457 Color: 0
Size: 330059 Color: 0
Size: 296485 Color: 1

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 373497 Color: 1
Size: 355118 Color: 0
Size: 271386 Color: 1

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 373495 Color: 0
Size: 343392 Color: 0
Size: 283114 Color: 1

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 373708 Color: 1
Size: 344615 Color: 0
Size: 281678 Color: 1

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 373668 Color: 0
Size: 369183 Color: 0
Size: 257150 Color: 1

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 373719 Color: 0
Size: 370503 Color: 1
Size: 255779 Color: 0

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 373727 Color: 0
Size: 342316 Color: 1
Size: 283958 Color: 0

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 373926 Color: 1
Size: 337255 Color: 0
Size: 288820 Color: 1

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 373978 Color: 0
Size: 315509 Color: 0
Size: 310514 Color: 1

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 374118 Color: 1
Size: 322749 Color: 0
Size: 303134 Color: 1

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 374267 Color: 1
Size: 319138 Color: 1
Size: 306596 Color: 0

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 374201 Color: 0
Size: 351943 Color: 1
Size: 273857 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 374279 Color: 0
Size: 340242 Color: 0
Size: 285480 Color: 1

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 374533 Color: 1
Size: 316344 Color: 1
Size: 309124 Color: 0

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 374689 Color: 0
Size: 324230 Color: 0
Size: 301082 Color: 1

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 374850 Color: 1
Size: 341315 Color: 1
Size: 283836 Color: 0

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 374900 Color: 1
Size: 374491 Color: 1
Size: 250610 Color: 0

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 374768 Color: 0
Size: 327799 Color: 0
Size: 297434 Color: 1

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 374916 Color: 1
Size: 348612 Color: 1
Size: 276473 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 374920 Color: 1
Size: 315307 Color: 1
Size: 309774 Color: 0

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 375088 Color: 1
Size: 364550 Color: 0
Size: 260363 Color: 1

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 374875 Color: 0
Size: 353268 Color: 0
Size: 271858 Color: 1

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 375124 Color: 1
Size: 313720 Color: 0
Size: 311157 Color: 1

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 375243 Color: 0
Size: 361776 Color: 0
Size: 262982 Color: 1

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 375211 Color: 1
Size: 315372 Color: 0
Size: 309418 Color: 1

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 375255 Color: 0
Size: 338649 Color: 0
Size: 286097 Color: 1

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 375219 Color: 1
Size: 348313 Color: 1
Size: 276469 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 375308 Color: 0
Size: 357464 Color: 1
Size: 267229 Color: 0

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 375245 Color: 1
Size: 328428 Color: 0
Size: 296328 Color: 1

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 375411 Color: 1
Size: 343167 Color: 1
Size: 281423 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 375341 Color: 0
Size: 325576 Color: 1
Size: 299084 Color: 0

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 375447 Color: 1
Size: 353230 Color: 0
Size: 271324 Color: 1

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 375370 Color: 0
Size: 371781 Color: 1
Size: 252850 Color: 0

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 375467 Color: 1
Size: 346646 Color: 0
Size: 277888 Color: 1

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 375702 Color: 1
Size: 336379 Color: 0
Size: 287920 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 375524 Color: 0
Size: 337674 Color: 0
Size: 286803 Color: 1

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 375784 Color: 1
Size: 335040 Color: 0
Size: 289177 Color: 1

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 375806 Color: 1
Size: 364032 Color: 0
Size: 260163 Color: 1

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 375928 Color: 1
Size: 349524 Color: 1
Size: 274549 Color: 0

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 375754 Color: 0
Size: 356360 Color: 1
Size: 267887 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 376047 Color: 1
Size: 329574 Color: 1
Size: 294380 Color: 0

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 375820 Color: 0
Size: 339009 Color: 1
Size: 285172 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 376142 Color: 1
Size: 356270 Color: 0
Size: 267589 Color: 1

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 375981 Color: 0
Size: 354555 Color: 0
Size: 269465 Color: 1

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 376071 Color: 0
Size: 357581 Color: 0
Size: 266349 Color: 1

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 376216 Color: 1
Size: 312064 Color: 1
Size: 311721 Color: 0

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 376085 Color: 0
Size: 346355 Color: 1
Size: 277561 Color: 0

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 376218 Color: 1
Size: 312099 Color: 0
Size: 311684 Color: 1

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 376087 Color: 0
Size: 340006 Color: 0
Size: 283908 Color: 1

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 376166 Color: 0
Size: 365894 Color: 0
Size: 257941 Color: 1

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 376293 Color: 0
Size: 347480 Color: 1
Size: 276228 Color: 1

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 376405 Color: 1
Size: 372170 Color: 0
Size: 251426 Color: 1

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 376487 Color: 1
Size: 329079 Color: 1
Size: 294435 Color: 0

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 376491 Color: 1
Size: 362639 Color: 0
Size: 260871 Color: 1

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 376555 Color: 1
Size: 329628 Color: 0
Size: 293818 Color: 1

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 376497 Color: 0
Size: 353012 Color: 0
Size: 270492 Color: 1

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 376621 Color: 1
Size: 358334 Color: 1
Size: 265046 Color: 0

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 376614 Color: 0
Size: 337798 Color: 1
Size: 285589 Color: 0

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 376693 Color: 0
Size: 329217 Color: 1
Size: 294091 Color: 0

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 376801 Color: 1
Size: 362688 Color: 0
Size: 260512 Color: 1

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 376931 Color: 1
Size: 319697 Color: 0
Size: 303373 Color: 1

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 377093 Color: 1
Size: 360477 Color: 0
Size: 262431 Color: 1

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 377056 Color: 0
Size: 348694 Color: 0
Size: 274251 Color: 1

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 377186 Color: 0
Size: 327061 Color: 0
Size: 295754 Color: 1

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 377513 Color: 1
Size: 321701 Color: 1
Size: 300787 Color: 0

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 377611 Color: 1
Size: 354303 Color: 0
Size: 268087 Color: 1

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 377510 Color: 0
Size: 315522 Color: 0
Size: 306969 Color: 1

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 377797 Color: 1
Size: 334495 Color: 1
Size: 287709 Color: 0

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 377852 Color: 1
Size: 317021 Color: 1
Size: 305128 Color: 0

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 377671 Color: 0
Size: 313784 Color: 0
Size: 308546 Color: 1

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 377907 Color: 1
Size: 367662 Color: 0
Size: 254432 Color: 1

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 377864 Color: 0
Size: 339099 Color: 1
Size: 283038 Color: 0

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 377919 Color: 0
Size: 330044 Color: 1
Size: 292038 Color: 0

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 378064 Color: 0
Size: 364303 Color: 1
Size: 257634 Color: 0

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 378067 Color: 1
Size: 363999 Color: 0
Size: 257935 Color: 1

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 378073 Color: 0
Size: 330937 Color: 0
Size: 290991 Color: 1

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 378067 Color: 1
Size: 362109 Color: 0
Size: 259825 Color: 1

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 378183 Color: 1
Size: 325823 Color: 0
Size: 295995 Color: 0

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 378179 Color: 0
Size: 367875 Color: 1
Size: 253947 Color: 0

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 378203 Color: 1
Size: 314861 Color: 1
Size: 306937 Color: 0

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 378258 Color: 1
Size: 361566 Color: 1
Size: 260177 Color: 0

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 378359 Color: 0
Size: 327493 Color: 0
Size: 294149 Color: 1

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 378462 Color: 0
Size: 318660 Color: 0
Size: 302879 Color: 1

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 378418 Color: 1
Size: 351673 Color: 0
Size: 269910 Color: 1

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 378625 Color: 0
Size: 354646 Color: 0
Size: 266730 Color: 1

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 378571 Color: 1
Size: 334432 Color: 0
Size: 286998 Color: 1

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 378640 Color: 1
Size: 311632 Color: 1
Size: 309729 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 378862 Color: 1
Size: 351610 Color: 0
Size: 269529 Color: 1

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 378952 Color: 0
Size: 311407 Color: 0
Size: 309642 Color: 1

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 378932 Color: 1
Size: 349798 Color: 0
Size: 271271 Color: 1

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 379081 Color: 0
Size: 346783 Color: 1
Size: 274137 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 379256 Color: 1
Size: 339610 Color: 0
Size: 281135 Color: 1

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 379258 Color: 0
Size: 332708 Color: 0
Size: 288035 Color: 1

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 379328 Color: 0
Size: 340958 Color: 0
Size: 279715 Color: 1

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 379589 Color: 1
Size: 323109 Color: 0
Size: 297303 Color: 1

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 379657 Color: 1
Size: 313134 Color: 0
Size: 307210 Color: 0

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 379718 Color: 0
Size: 343985 Color: 0
Size: 276298 Color: 1

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 379850 Color: 1
Size: 322042 Color: 0
Size: 298109 Color: 1

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 379891 Color: 0
Size: 321969 Color: 0
Size: 298141 Color: 1

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 379892 Color: 0
Size: 327420 Color: 1
Size: 292689 Color: 0

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 380080 Color: 0
Size: 361774 Color: 1
Size: 258147 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 380119 Color: 0
Size: 334210 Color: 1
Size: 285672 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 380234 Color: 0
Size: 364580 Color: 1
Size: 255187 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 380328 Color: 1
Size: 351497 Color: 0
Size: 268176 Color: 1

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 380346 Color: 1
Size: 353891 Color: 1
Size: 265764 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 380407 Color: 0
Size: 323414 Color: 1
Size: 296180 Color: 0

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 380421 Color: 1
Size: 350403 Color: 1
Size: 269177 Color: 0

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 380620 Color: 1
Size: 315839 Color: 1
Size: 303542 Color: 0

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 380535 Color: 0
Size: 314955 Color: 0
Size: 304511 Color: 1

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 380651 Color: 1
Size: 314920 Color: 0
Size: 304430 Color: 1

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 380652 Color: 1
Size: 316924 Color: 0
Size: 302425 Color: 1

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 380828 Color: 1
Size: 341180 Color: 1
Size: 277993 Color: 0

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 380848 Color: 1
Size: 334765 Color: 1
Size: 284388 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 380859 Color: 1
Size: 346082 Color: 1
Size: 273060 Color: 0

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 380926 Color: 0
Size: 319732 Color: 0
Size: 299343 Color: 1

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 381085 Color: 0
Size: 368688 Color: 1
Size: 250228 Color: 1

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 381047 Color: 1
Size: 328460 Color: 0
Size: 290494 Color: 1

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 381195 Color: 0
Size: 319903 Color: 0
Size: 298903 Color: 1

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 381422 Color: 0
Size: 365196 Color: 0
Size: 253383 Color: 1

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 381453 Color: 0
Size: 313675 Color: 0
Size: 304873 Color: 1

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 381360 Color: 1
Size: 368364 Color: 0
Size: 250277 Color: 0

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 381485 Color: 0
Size: 313469 Color: 0
Size: 305047 Color: 1

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 381535 Color: 0
Size: 343663 Color: 0
Size: 274803 Color: 1

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 381539 Color: 0
Size: 320871 Color: 0
Size: 297591 Color: 1

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 446254 Color: 1
Size: 284571 Color: 0
Size: 269176 Color: 0

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 381621 Color: 1
Size: 366501 Color: 0
Size: 251879 Color: 1

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 381706 Color: 0
Size: 327312 Color: 0
Size: 290983 Color: 1

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 381622 Color: 1
Size: 367243 Color: 0
Size: 251136 Color: 1

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 381749 Color: 0
Size: 323255 Color: 0
Size: 294997 Color: 1

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 381749 Color: 0
Size: 320088 Color: 0
Size: 298164 Color: 1

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 381680 Color: 1
Size: 359389 Color: 1
Size: 258932 Color: 0

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 381707 Color: 1
Size: 326517 Color: 1
Size: 291777 Color: 0

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 381905 Color: 0
Size: 325603 Color: 1
Size: 292493 Color: 1

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 381916 Color: 0
Size: 345535 Color: 1
Size: 272550 Color: 0

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 381923 Color: 0
Size: 321030 Color: 1
Size: 297048 Color: 1

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 382088 Color: 0
Size: 311678 Color: 1
Size: 306235 Color: 0

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 382171 Color: 0
Size: 354391 Color: 0
Size: 263439 Color: 1

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 382198 Color: 1
Size: 323396 Color: 0
Size: 294407 Color: 1

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 382293 Color: 0
Size: 315953 Color: 1
Size: 301755 Color: 0

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 382319 Color: 1
Size: 311700 Color: 0
Size: 305982 Color: 1

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 382522 Color: 1
Size: 315770 Color: 0
Size: 301709 Color: 0

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 382570 Color: 1
Size: 331817 Color: 0
Size: 285614 Color: 1

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 382590 Color: 1
Size: 320842 Color: 0
Size: 296569 Color: 0

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 382637 Color: 1
Size: 338665 Color: 1
Size: 278699 Color: 0

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 382528 Color: 0
Size: 352085 Color: 1
Size: 265388 Color: 0

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 382638 Color: 1
Size: 340010 Color: 0
Size: 277353 Color: 1

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 382674 Color: 0
Size: 348519 Color: 1
Size: 268808 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 382766 Color: 0
Size: 329415 Color: 1
Size: 287820 Color: 0

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 382819 Color: 0
Size: 339729 Color: 0
Size: 277453 Color: 1

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 382807 Color: 1
Size: 311477 Color: 0
Size: 305717 Color: 1

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 383069 Color: 0
Size: 349219 Color: 1
Size: 267713 Color: 0

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 383024 Color: 1
Size: 356028 Color: 0
Size: 260949 Color: 1

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 383078 Color: 0
Size: 314203 Color: 1
Size: 302720 Color: 0

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 383237 Color: 0
Size: 332438 Color: 1
Size: 284326 Color: 1

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 383239 Color: 1
Size: 356056 Color: 0
Size: 260706 Color: 1

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 383408 Color: 0
Size: 363771 Color: 0
Size: 252822 Color: 1

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 383459 Color: 0
Size: 346616 Color: 0
Size: 269926 Color: 1

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 383471 Color: 1
Size: 341480 Color: 1
Size: 275050 Color: 0

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 383495 Color: 0
Size: 347308 Color: 0
Size: 269198 Color: 1

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 383565 Color: 1
Size: 344675 Color: 1
Size: 271761 Color: 0

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 383718 Color: 0
Size: 314159 Color: 1
Size: 302124 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 383773 Color: 0
Size: 365035 Color: 1
Size: 251193 Color: 1

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 383805 Color: 0
Size: 320576 Color: 0
Size: 295620 Color: 1

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 383625 Color: 1
Size: 351407 Color: 1
Size: 264969 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 384011 Color: 0
Size: 312568 Color: 0
Size: 303422 Color: 1

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 384134 Color: 0
Size: 314365 Color: 1
Size: 301502 Color: 1

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 384548 Color: 0
Size: 347796 Color: 0
Size: 267657 Color: 1

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 384401 Color: 1
Size: 311301 Color: 1
Size: 304299 Color: 0

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 384569 Color: 0
Size: 351634 Color: 0
Size: 263798 Color: 1

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 384533 Color: 1
Size: 334850 Color: 1
Size: 280618 Color: 0

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 384620 Color: 1
Size: 362861 Color: 1
Size: 252520 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 384687 Color: 0
Size: 347596 Color: 0
Size: 267718 Color: 1

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 384652 Color: 1
Size: 358630 Color: 0
Size: 256719 Color: 1

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 384929 Color: 0
Size: 339909 Color: 0
Size: 275163 Color: 1

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 384994 Color: 0
Size: 316469 Color: 1
Size: 298538 Color: 0

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 384901 Color: 1
Size: 328144 Color: 1
Size: 286956 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 384968 Color: 1
Size: 361260 Color: 1
Size: 253773 Color: 0

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 385163 Color: 0
Size: 342849 Color: 0
Size: 271989 Color: 1

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 385087 Color: 1
Size: 346761 Color: 0
Size: 268153 Color: 1

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 385144 Color: 1
Size: 330835 Color: 0
Size: 284022 Color: 1

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 385267 Color: 0
Size: 309726 Color: 0
Size: 305008 Color: 1

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 385219 Color: 1
Size: 347371 Color: 0
Size: 267411 Color: 1

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 385255 Color: 1
Size: 359555 Color: 0
Size: 255191 Color: 1

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 385406 Color: 0
Size: 354504 Color: 1
Size: 260091 Color: 0

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 385340 Color: 1
Size: 358428 Color: 0
Size: 256233 Color: 1

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 385412 Color: 0
Size: 345821 Color: 1
Size: 268768 Color: 0

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 385385 Color: 1
Size: 322065 Color: 1
Size: 292551 Color: 0

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 385488 Color: 0
Size: 325147 Color: 0
Size: 289366 Color: 1

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 385584 Color: 1
Size: 334872 Color: 0
Size: 279545 Color: 0

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 385615 Color: 1
Size: 325586 Color: 1
Size: 288800 Color: 0

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 385697 Color: 0
Size: 337175 Color: 0
Size: 277129 Color: 1

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 385866 Color: 1
Size: 361747 Color: 0
Size: 252388 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 386001 Color: 1
Size: 346344 Color: 0
Size: 267656 Color: 0

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 386002 Color: 1
Size: 324828 Color: 1
Size: 289171 Color: 0

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 386015 Color: 1
Size: 330256 Color: 1
Size: 283730 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 386210 Color: 1
Size: 329891 Color: 1
Size: 283900 Color: 0

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 386231 Color: 1
Size: 347445 Color: 0
Size: 266325 Color: 1

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 386290 Color: 1
Size: 317189 Color: 0
Size: 296522 Color: 1

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 386305 Color: 1
Size: 340508 Color: 1
Size: 273188 Color: 0

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 386521 Color: 0
Size: 311730 Color: 1
Size: 301750 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 386388 Color: 1
Size: 331097 Color: 1
Size: 282516 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 386570 Color: 0
Size: 343942 Color: 1
Size: 269489 Color: 0

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 386641 Color: 1
Size: 312965 Color: 0
Size: 300395 Color: 1

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 386642 Color: 0
Size: 307061 Color: 1
Size: 306298 Color: 0

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 386668 Color: 1
Size: 350965 Color: 1
Size: 262368 Color: 0

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 386752 Color: 0
Size: 346339 Color: 0
Size: 266910 Color: 1

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 386796 Color: 1
Size: 357629 Color: 0
Size: 255576 Color: 0

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 386862 Color: 1
Size: 361528 Color: 1
Size: 251611 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 386860 Color: 0
Size: 320680 Color: 0
Size: 292461 Color: 1

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 386894 Color: 1
Size: 358686 Color: 0
Size: 254421 Color: 1

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 387108 Color: 0
Size: 321779 Color: 1
Size: 291114 Color: 0

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 386918 Color: 1
Size: 330871 Color: 0
Size: 282212 Color: 1

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 386991 Color: 1
Size: 315143 Color: 0
Size: 297867 Color: 1

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 387179 Color: 0
Size: 314199 Color: 1
Size: 298623 Color: 0

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 387232 Color: 0
Size: 354587 Color: 1
Size: 258182 Color: 1

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 387571 Color: 0
Size: 309900 Color: 1
Size: 302530 Color: 0

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 387571 Color: 0
Size: 326041 Color: 1
Size: 286389 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 387380 Color: 1
Size: 318665 Color: 0
Size: 293956 Color: 1

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 387500 Color: 1
Size: 317443 Color: 1
Size: 295058 Color: 0

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 387641 Color: 1
Size: 343951 Color: 0
Size: 268409 Color: 1

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 387753 Color: 1
Size: 342775 Color: 0
Size: 269473 Color: 0

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 361015 Color: 1
Size: 334963 Color: 0
Size: 304023 Color: 1

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 387787 Color: 0
Size: 317584 Color: 0
Size: 294630 Color: 1

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 387843 Color: 1
Size: 320359 Color: 1
Size: 291799 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 387930 Color: 1
Size: 325635 Color: 0
Size: 286436 Color: 1

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 388237 Color: 0
Size: 321278 Color: 1
Size: 290486 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 388107 Color: 1
Size: 328601 Color: 1
Size: 283293 Color: 0

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 388359 Color: 0
Size: 317142 Color: 0
Size: 294500 Color: 1

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 388354 Color: 1
Size: 327558 Color: 0
Size: 284089 Color: 1

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 388512 Color: 0
Size: 361436 Color: 1
Size: 250053 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 388609 Color: 0
Size: 347433 Color: 0
Size: 263959 Color: 1

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 388608 Color: 1
Size: 335212 Color: 0
Size: 276181 Color: 1

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 388960 Color: 1
Size: 344335 Color: 0
Size: 266706 Color: 1

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 389005 Color: 0
Size: 333065 Color: 0
Size: 277931 Color: 1

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 389023 Color: 1
Size: 307433 Color: 0
Size: 303545 Color: 1

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 389077 Color: 0
Size: 351288 Color: 1
Size: 259636 Color: 0

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 389055 Color: 1
Size: 308230 Color: 1
Size: 302716 Color: 0

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 389140 Color: 1
Size: 355268 Color: 0
Size: 255593 Color: 1

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 389143 Color: 1
Size: 355356 Color: 1
Size: 255502 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 389332 Color: 1
Size: 331975 Color: 0
Size: 278694 Color: 1

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 389275 Color: 0
Size: 347644 Color: 0
Size: 263082 Color: 1

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 389353 Color: 1
Size: 327985 Color: 1
Size: 282663 Color: 0

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 389358 Color: 1
Size: 329252 Color: 0
Size: 281391 Color: 1

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 389442 Color: 0
Size: 308194 Color: 1
Size: 302365 Color: 0

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 389479 Color: 0
Size: 322518 Color: 1
Size: 288004 Color: 1

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 389512 Color: 0
Size: 305830 Color: 0
Size: 304659 Color: 1

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 389527 Color: 0
Size: 356261 Color: 1
Size: 254213 Color: 1

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 389543 Color: 0
Size: 355204 Color: 1
Size: 255254 Color: 0

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 389571 Color: 0
Size: 347984 Color: 0
Size: 262446 Color: 1

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 389764 Color: 1
Size: 333587 Color: 0
Size: 276650 Color: 1

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 389723 Color: 0
Size: 356587 Color: 1
Size: 253691 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 389783 Color: 1
Size: 339996 Color: 0
Size: 270222 Color: 1

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 389772 Color: 0
Size: 344951 Color: 1
Size: 265278 Color: 0

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 389822 Color: 0
Size: 322607 Color: 1
Size: 287572 Color: 1

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 390122 Color: 0
Size: 335474 Color: 1
Size: 274405 Color: 0

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 390120 Color: 1
Size: 318686 Color: 1
Size: 291195 Color: 0

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 390163 Color: 1
Size: 337891 Color: 0
Size: 271947 Color: 0

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 390230 Color: 0
Size: 324918 Color: 0
Size: 284853 Color: 1

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 390204 Color: 1
Size: 339087 Color: 0
Size: 270710 Color: 1

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 390295 Color: 1
Size: 354037 Color: 0
Size: 255669 Color: 1

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 390342 Color: 0
Size: 350876 Color: 1
Size: 258783 Color: 0

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 390444 Color: 0
Size: 313755 Color: 1
Size: 295802 Color: 0

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 390495 Color: 0
Size: 345440 Color: 0
Size: 264066 Color: 1

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 390364 Color: 1
Size: 312428 Color: 1
Size: 297209 Color: 0

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 390473 Color: 1
Size: 330693 Color: 1
Size: 278835 Color: 0

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 390658 Color: 0
Size: 353819 Color: 1
Size: 255524 Color: 0

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 390561 Color: 1
Size: 344060 Color: 0
Size: 265380 Color: 1

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 390713 Color: 1
Size: 311349 Color: 0
Size: 297939 Color: 0

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 390814 Color: 1
Size: 318248 Color: 0
Size: 290939 Color: 1

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 390945 Color: 1
Size: 344072 Color: 0
Size: 264984 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 390954 Color: 1
Size: 309379 Color: 1
Size: 299668 Color: 0

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 391067 Color: 1
Size: 312990 Color: 0
Size: 295944 Color: 0

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 391261 Color: 0
Size: 358095 Color: 0
Size: 250645 Color: 1

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 391131 Color: 1
Size: 344405 Color: 1
Size: 264465 Color: 0

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 391343 Color: 0
Size: 341240 Color: 0
Size: 267418 Color: 1

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 391365 Color: 0
Size: 314691 Color: 1
Size: 293945 Color: 1

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 391378 Color: 0
Size: 337956 Color: 0
Size: 270667 Color: 1

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 391409 Color: 0
Size: 315146 Color: 1
Size: 293446 Color: 0

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 391454 Color: 0
Size: 310572 Color: 0
Size: 297975 Color: 1

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 391487 Color: 1
Size: 334124 Color: 0
Size: 274390 Color: 1

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 391537 Color: 0
Size: 309222 Color: 0
Size: 299242 Color: 1

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 391520 Color: 1
Size: 308783 Color: 0
Size: 299698 Color: 1

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 391646 Color: 0
Size: 341531 Color: 0
Size: 266824 Color: 1

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 391612 Color: 1
Size: 321699 Color: 0
Size: 286690 Color: 1

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 391660 Color: 1
Size: 350645 Color: 0
Size: 257696 Color: 1

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 391709 Color: 0
Size: 308556 Color: 0
Size: 299736 Color: 1

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 391731 Color: 0
Size: 341363 Color: 1
Size: 266907 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 391745 Color: 1
Size: 326763 Color: 1
Size: 281493 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 391784 Color: 0
Size: 340077 Color: 1
Size: 268140 Color: 0

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 391872 Color: 0
Size: 340995 Color: 1
Size: 267134 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 391821 Color: 1
Size: 346234 Color: 1
Size: 261946 Color: 0

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 391908 Color: 1
Size: 349560 Color: 0
Size: 258533 Color: 1

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 392004 Color: 0
Size: 315704 Color: 0
Size: 292293 Color: 1

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 391942 Color: 1
Size: 310078 Color: 0
Size: 297981 Color: 1

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 392038 Color: 0
Size: 337761 Color: 0
Size: 270202 Color: 1

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 391972 Color: 1
Size: 305802 Color: 0
Size: 302227 Color: 1

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 392084 Color: 1
Size: 315776 Color: 0
Size: 292141 Color: 1

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 392106 Color: 0
Size: 318058 Color: 0
Size: 289837 Color: 1

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 392122 Color: 1
Size: 351903 Color: 0
Size: 255976 Color: 1

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 392361 Color: 0
Size: 351924 Color: 1
Size: 255716 Color: 0

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 392385 Color: 0
Size: 346115 Color: 1
Size: 261501 Color: 0

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 392283 Color: 1
Size: 346468 Color: 0
Size: 261250 Color: 1

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 392463 Color: 1
Size: 333803 Color: 1
Size: 273735 Color: 0

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 392486 Color: 1
Size: 319405 Color: 0
Size: 288110 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 392513 Color: 1
Size: 331407 Color: 0
Size: 276081 Color: 1

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 392523 Color: 1
Size: 336597 Color: 0
Size: 270881 Color: 0

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 392620 Color: 0
Size: 304749 Color: 1
Size: 302632 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 392595 Color: 1
Size: 317068 Color: 1
Size: 290338 Color: 0

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 392743 Color: 1
Size: 341524 Color: 0
Size: 265734 Color: 0

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 392751 Color: 0
Size: 326385 Color: 1
Size: 280865 Color: 0

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 392771 Color: 1
Size: 337614 Color: 0
Size: 269616 Color: 1

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 392878 Color: 1
Size: 341890 Color: 0
Size: 265233 Color: 1

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 392899 Color: 0
Size: 348439 Color: 0
Size: 258663 Color: 1

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 392933 Color: 1
Size: 321246 Color: 0
Size: 285822 Color: 1

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 393068 Color: 1
Size: 354687 Color: 0
Size: 252246 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 393232 Color: 1
Size: 347930 Color: 1
Size: 258839 Color: 0

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 393207 Color: 0
Size: 311210 Color: 1
Size: 295584 Color: 0

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 393273 Color: 1
Size: 329376 Color: 1
Size: 277352 Color: 0

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 393278 Color: 1
Size: 325194 Color: 0
Size: 281529 Color: 0

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 393382 Color: 0
Size: 310644 Color: 0
Size: 295975 Color: 1

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 393385 Color: 1
Size: 315800 Color: 0
Size: 290816 Color: 1

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 393617 Color: 1
Size: 321559 Color: 1
Size: 284825 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 393727 Color: 0
Size: 317634 Color: 1
Size: 288640 Color: 0

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 393888 Color: 1
Size: 325721 Color: 0
Size: 280392 Color: 1

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 393915 Color: 0
Size: 331814 Color: 0
Size: 274272 Color: 1

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 393945 Color: 1
Size: 312805 Color: 1
Size: 293251 Color: 0

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 393941 Color: 0
Size: 331045 Color: 1
Size: 275015 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 393952 Color: 0
Size: 344673 Color: 1
Size: 261376 Color: 0

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 394005 Color: 0
Size: 329552 Color: 1
Size: 276444 Color: 0

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 393970 Color: 1
Size: 335759 Color: 1
Size: 270272 Color: 0

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 394060 Color: 0
Size: 317608 Color: 1
Size: 288333 Color: 1

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 393975 Color: 1
Size: 328491 Color: 0
Size: 277535 Color: 1

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 394078 Color: 1
Size: 312736 Color: 0
Size: 293187 Color: 1

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 394099 Color: 1
Size: 320788 Color: 1
Size: 285114 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 394153 Color: 0
Size: 339894 Color: 0
Size: 265954 Color: 1

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 394115 Color: 1
Size: 338538 Color: 1
Size: 267348 Color: 0

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 394162 Color: 0
Size: 316462 Color: 1
Size: 289377 Color: 0

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 394132 Color: 1
Size: 303666 Color: 1
Size: 302203 Color: 0

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 394315 Color: 0
Size: 344897 Color: 1
Size: 260789 Color: 1

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 394348 Color: 0
Size: 311420 Color: 1
Size: 294233 Color: 0

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 394773 Color: 1
Size: 340202 Color: 1
Size: 265026 Color: 0

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 394859 Color: 0
Size: 351735 Color: 0
Size: 253407 Color: 1

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 394797 Color: 1
Size: 325291 Color: 1
Size: 279913 Color: 0

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 394889 Color: 0
Size: 313639 Color: 1
Size: 291473 Color: 0

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 395006 Color: 0
Size: 314752 Color: 1
Size: 290243 Color: 0

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 395072 Color: 0
Size: 307643 Color: 1
Size: 297286 Color: 0

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 395029 Color: 1
Size: 333022 Color: 0
Size: 271950 Color: 1

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 395246 Color: 1
Size: 348606 Color: 1
Size: 256149 Color: 0

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 395285 Color: 0
Size: 312096 Color: 0
Size: 292620 Color: 1

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 395363 Color: 0
Size: 320804 Color: 1
Size: 283834 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 395461 Color: 1
Size: 348509 Color: 1
Size: 256031 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 395465 Color: 1
Size: 348709 Color: 0
Size: 255827 Color: 1

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 395532 Color: 1
Size: 320100 Color: 0
Size: 284369 Color: 1

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 395577 Color: 0
Size: 342146 Color: 1
Size: 262278 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 395729 Color: 0
Size: 332735 Color: 0
Size: 271537 Color: 1

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 395608 Color: 1
Size: 323154 Color: 0
Size: 281239 Color: 1

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 395737 Color: 0
Size: 307341 Color: 1
Size: 296923 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 395839 Color: 0
Size: 346723 Color: 0
Size: 257439 Color: 1

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 395771 Color: 1
Size: 332981 Color: 1
Size: 271249 Color: 0

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 395840 Color: 1
Size: 338256 Color: 1
Size: 265905 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 395974 Color: 1
Size: 332756 Color: 1
Size: 271271 Color: 0

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 395933 Color: 0
Size: 321195 Color: 0
Size: 282873 Color: 1

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 396035 Color: 1
Size: 338389 Color: 0
Size: 265577 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 396133 Color: 1
Size: 329058 Color: 0
Size: 274810 Color: 0

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 396183 Color: 1
Size: 302787 Color: 0
Size: 301031 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 396208 Color: 1
Size: 328458 Color: 1
Size: 275335 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 396251 Color: 0
Size: 315458 Color: 0
Size: 288292 Color: 1

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 396380 Color: 0
Size: 329398 Color: 0
Size: 274223 Color: 1

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 396492 Color: 0
Size: 331635 Color: 1
Size: 271874 Color: 0

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 396594 Color: 0
Size: 318664 Color: 0
Size: 284743 Color: 1

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 396727 Color: 0
Size: 336432 Color: 1
Size: 266842 Color: 1

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 396764 Color: 0
Size: 342487 Color: 1
Size: 260750 Color: 0

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 396779 Color: 1
Size: 304803 Color: 1
Size: 298419 Color: 0

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 396831 Color: 0
Size: 318219 Color: 1
Size: 284951 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 397247 Color: 0
Size: 323612 Color: 0
Size: 279142 Color: 1

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 397360 Color: 0
Size: 334796 Color: 1
Size: 267845 Color: 0

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 397281 Color: 1
Size: 324384 Color: 0
Size: 278336 Color: 1

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 397378 Color: 0
Size: 344500 Color: 0
Size: 258123 Color: 1

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 397421 Color: 1
Size: 328160 Color: 1
Size: 274420 Color: 0

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 397414 Color: 0
Size: 333252 Color: 0
Size: 269335 Color: 1

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 397575 Color: 1
Size: 318533 Color: 0
Size: 283893 Color: 1

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 397649 Color: 0
Size: 308300 Color: 0
Size: 294052 Color: 1

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 397800 Color: 0
Size: 313073 Color: 0
Size: 289128 Color: 1

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 397862 Color: 0
Size: 343340 Color: 0
Size: 258799 Color: 1

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 398209 Color: 1
Size: 300897 Color: 0
Size: 300895 Color: 1

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 398173 Color: 1
Size: 351052 Color: 0
Size: 250776 Color: 1

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 398262 Color: 0
Size: 329754 Color: 0
Size: 271985 Color: 1

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 398209 Color: 0
Size: 341450 Color: 0
Size: 260342 Color: 1

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 398296 Color: 0
Size: 322505 Color: 0
Size: 279200 Color: 1

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 398422 Color: 0
Size: 308809 Color: 0
Size: 292770 Color: 1

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 398547 Color: 0
Size: 332628 Color: 1
Size: 268826 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 398794 Color: 1
Size: 330106 Color: 1
Size: 271101 Color: 0

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 398914 Color: 0
Size: 343461 Color: 0
Size: 257626 Color: 1

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 399059 Color: 0
Size: 312239 Color: 1
Size: 288703 Color: 0

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 399004 Color: 1
Size: 344526 Color: 1
Size: 256471 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 399142 Color: 0
Size: 307639 Color: 1
Size: 293220 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 399268 Color: 1
Size: 334085 Color: 1
Size: 266648 Color: 0

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 399385 Color: 0
Size: 312558 Color: 0
Size: 288058 Color: 1

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 399437 Color: 0
Size: 341357 Color: 1
Size: 259207 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 399493 Color: 0
Size: 345752 Color: 1
Size: 254756 Color: 0

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 399706 Color: 1
Size: 323733 Color: 1
Size: 276562 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 399613 Color: 0
Size: 346407 Color: 1
Size: 253981 Color: 0

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 399899 Color: 1
Size: 303807 Color: 0
Size: 296295 Color: 1

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 399930 Color: 1
Size: 311110 Color: 1
Size: 288961 Color: 0

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 399947 Color: 0
Size: 329866 Color: 0
Size: 270188 Color: 1

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 399951 Color: 1
Size: 316532 Color: 1
Size: 283518 Color: 0

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 400042 Color: 0
Size: 328455 Color: 1
Size: 271504 Color: 1

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 400054 Color: 0
Size: 310771 Color: 1
Size: 289176 Color: 0

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 400019 Color: 1
Size: 339696 Color: 1
Size: 260286 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 400114 Color: 0
Size: 303714 Color: 1
Size: 296173 Color: 0

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 400093 Color: 1
Size: 325455 Color: 0
Size: 274453 Color: 1

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 400215 Color: 0
Size: 331424 Color: 1
Size: 268362 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 400249 Color: 0
Size: 321218 Color: 1
Size: 278534 Color: 1

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 400297 Color: 1
Size: 341031 Color: 0
Size: 258673 Color: 1

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 400278 Color: 0
Size: 347060 Color: 0
Size: 252663 Color: 1

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 400312 Color: 1
Size: 305840 Color: 1
Size: 293849 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 400897 Color: 0
Size: 343403 Color: 0
Size: 255701 Color: 1

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 401220 Color: 0
Size: 303524 Color: 1
Size: 295257 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 401388 Color: 0
Size: 348103 Color: 0
Size: 250510 Color: 1

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 401352 Color: 1
Size: 305419 Color: 1
Size: 293230 Color: 0

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 401608 Color: 0
Size: 320165 Color: 0
Size: 278228 Color: 1

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 401699 Color: 1
Size: 315856 Color: 0
Size: 282446 Color: 1

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 401774 Color: 1
Size: 347756 Color: 0
Size: 250471 Color: 1

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 401997 Color: 1
Size: 312944 Color: 0
Size: 285060 Color: 0

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 402043 Color: 1
Size: 343750 Color: 0
Size: 254208 Color: 1

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 402067 Color: 0
Size: 305795 Color: 0
Size: 292139 Color: 1

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 402185 Color: 1
Size: 301845 Color: 0
Size: 295971 Color: 1

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 402136 Color: 0
Size: 340747 Color: 1
Size: 257118 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 402337 Color: 0
Size: 320768 Color: 1
Size: 276896 Color: 0

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 402340 Color: 0
Size: 337351 Color: 1
Size: 260310 Color: 0

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 402376 Color: 1
Size: 323308 Color: 1
Size: 274317 Color: 0

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 402396 Color: 1
Size: 337078 Color: 0
Size: 260527 Color: 1

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 402511 Color: 0
Size: 328867 Color: 1
Size: 268623 Color: 1

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 402545 Color: 0
Size: 334239 Color: 1
Size: 263217 Color: 0

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 402547 Color: 0
Size: 337353 Color: 0
Size: 260101 Color: 1

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 402648 Color: 1
Size: 326106 Color: 0
Size: 271247 Color: 1

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 402681 Color: 0
Size: 326982 Color: 0
Size: 270338 Color: 1

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 402769 Color: 0
Size: 323790 Color: 1
Size: 273442 Color: 1

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 402879 Color: 0
Size: 302686 Color: 1
Size: 294436 Color: 0

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 402934 Color: 1
Size: 327647 Color: 1
Size: 269420 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 402967 Color: 1
Size: 341024 Color: 1
Size: 256010 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 402933 Color: 0
Size: 305146 Color: 1
Size: 291922 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 403033 Color: 0
Size: 336854 Color: 1
Size: 260114 Color: 1

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 403043 Color: 0
Size: 320286 Color: 0
Size: 276672 Color: 1

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 403160 Color: 1
Size: 340536 Color: 0
Size: 256305 Color: 1

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 403142 Color: 0
Size: 344777 Color: 0
Size: 252082 Color: 1

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 403298 Color: 1
Size: 315831 Color: 0
Size: 280872 Color: 0

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 403358 Color: 1
Size: 306320 Color: 1
Size: 290323 Color: 0

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 403239 Color: 0
Size: 319133 Color: 0
Size: 277629 Color: 1

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 403486 Color: 1
Size: 326718 Color: 1
Size: 269797 Color: 0

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 403574 Color: 1
Size: 331345 Color: 0
Size: 265082 Color: 1

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 403604 Color: 1
Size: 342996 Color: 1
Size: 253401 Color: 0

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 403658 Color: 0
Size: 341200 Color: 0
Size: 255143 Color: 1

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 403858 Color: 1
Size: 324890 Color: 0
Size: 271253 Color: 0

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 403921 Color: 1
Size: 312812 Color: 0
Size: 283268 Color: 0

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 403953 Color: 1
Size: 338306 Color: 1
Size: 257742 Color: 0

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 404064 Color: 1
Size: 329308 Color: 0
Size: 266629 Color: 1

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 404081 Color: 1
Size: 344484 Color: 1
Size: 251436 Color: 0

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 404248 Color: 1
Size: 307254 Color: 0
Size: 288499 Color: 0

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 404272 Color: 1
Size: 343227 Color: 1
Size: 252502 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 404248 Color: 0
Size: 317096 Color: 1
Size: 278657 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 404633 Color: 1
Size: 309500 Color: 1
Size: 285868 Color: 0

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 404499 Color: 0
Size: 325700 Color: 1
Size: 269802 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 404639 Color: 1
Size: 344922 Color: 0
Size: 250440 Color: 1

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 404619 Color: 0
Size: 341827 Color: 1
Size: 253555 Color: 0

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 404755 Color: 1
Size: 316909 Color: 0
Size: 278337 Color: 1

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 404852 Color: 1
Size: 344281 Color: 0
Size: 250868 Color: 1

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 404787 Color: 0
Size: 325085 Color: 0
Size: 270129 Color: 1

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 405019 Color: 1
Size: 300006 Color: 1
Size: 294976 Color: 0

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 405004 Color: 0
Size: 325158 Color: 0
Size: 269839 Color: 1

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 405030 Color: 1
Size: 339035 Color: 1
Size: 255936 Color: 0

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 405006 Color: 0
Size: 336812 Color: 0
Size: 258183 Color: 1

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 405075 Color: 1
Size: 324487 Color: 0
Size: 270439 Color: 1

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 405139 Color: 1
Size: 331987 Color: 1
Size: 262875 Color: 0

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 405199 Color: 0
Size: 329631 Color: 1
Size: 265171 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 405163 Color: 1
Size: 303200 Color: 1
Size: 291638 Color: 0

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 405248 Color: 0
Size: 305229 Color: 0
Size: 289524 Color: 1

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 405338 Color: 0
Size: 339315 Color: 1
Size: 255348 Color: 1

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 405358 Color: 0
Size: 319122 Color: 1
Size: 275521 Color: 1

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 405377 Color: 0
Size: 332809 Color: 1
Size: 261815 Color: 1

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 405377 Color: 0
Size: 338358 Color: 1
Size: 256266 Color: 0

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 405385 Color: 0
Size: 309718 Color: 1
Size: 284898 Color: 0

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 405487 Color: 1
Size: 306052 Color: 1
Size: 288462 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 405541 Color: 1
Size: 300340 Color: 0
Size: 294120 Color: 0

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 405550 Color: 1
Size: 325133 Color: 1
Size: 269318 Color: 0

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 405582 Color: 1
Size: 312414 Color: 0
Size: 282005 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 405591 Color: 0
Size: 308977 Color: 0
Size: 285433 Color: 1

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 405640 Color: 1
Size: 306299 Color: 1
Size: 288062 Color: 0

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 405801 Color: 0
Size: 338725 Color: 1
Size: 255475 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 405849 Color: 0
Size: 330472 Color: 1
Size: 263680 Color: 0

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 405877 Color: 0
Size: 332625 Color: 1
Size: 261499 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 405927 Color: 0
Size: 331022 Color: 1
Size: 263052 Color: 0

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 405971 Color: 0
Size: 299263 Color: 1
Size: 294767 Color: 0

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 406134 Color: 1
Size: 327832 Color: 0
Size: 266035 Color: 0

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 406165 Color: 0
Size: 298473 Color: 1
Size: 295363 Color: 1

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 406173 Color: 1
Size: 315211 Color: 1
Size: 278617 Color: 0

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 406213 Color: 0
Size: 312026 Color: 0
Size: 281762 Color: 1

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 406285 Color: 0
Size: 325514 Color: 1
Size: 268202 Color: 1

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 406339 Color: 0
Size: 342288 Color: 1
Size: 251374 Color: 1

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 406498 Color: 1
Size: 331029 Color: 0
Size: 262474 Color: 1

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 406514 Color: 1
Size: 304948 Color: 1
Size: 288539 Color: 0

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 406537 Color: 0
Size: 339136 Color: 1
Size: 254328 Color: 0

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 406586 Color: 0
Size: 317398 Color: 1
Size: 276017 Color: 0

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 406697 Color: 0
Size: 340323 Color: 1
Size: 252981 Color: 1

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 407034 Color: 0
Size: 327957 Color: 1
Size: 265010 Color: 1

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 407108 Color: 0
Size: 332586 Color: 0
Size: 260307 Color: 1

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 407238 Color: 1
Size: 309321 Color: 1
Size: 283442 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 407471 Color: 1
Size: 315393 Color: 1
Size: 277137 Color: 0

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 407540 Color: 1
Size: 330420 Color: 0
Size: 262041 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 407537 Color: 0
Size: 336306 Color: 1
Size: 256158 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 407763 Color: 1
Size: 304279 Color: 1
Size: 287959 Color: 0

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 407779 Color: 1
Size: 335489 Color: 1
Size: 256733 Color: 0

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 407869 Color: 0
Size: 329179 Color: 1
Size: 262953 Color: 0

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 407995 Color: 1
Size: 304978 Color: 0
Size: 287028 Color: 1

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 408021 Color: 1
Size: 316203 Color: 0
Size: 275777 Color: 1

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 407945 Color: 0
Size: 330640 Color: 0
Size: 261416 Color: 1

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 408163 Color: 1
Size: 297135 Color: 1
Size: 294703 Color: 0

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 408191 Color: 0
Size: 332750 Color: 1
Size: 259060 Color: 0

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 408299 Color: 0
Size: 318981 Color: 1
Size: 272721 Color: 1

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 408315 Color: 0
Size: 336042 Color: 0
Size: 255644 Color: 1

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 408363 Color: 1
Size: 338500 Color: 0
Size: 253138 Color: 1

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 408498 Color: 0
Size: 334150 Color: 0
Size: 257353 Color: 1

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 408562 Color: 1
Size: 318984 Color: 0
Size: 272455 Color: 1

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 409080 Color: 1
Size: 319856 Color: 0
Size: 271065 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 409084 Color: 1
Size: 312263 Color: 0
Size: 278654 Color: 1

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 409105 Color: 1
Size: 324103 Color: 0
Size: 266793 Color: 0

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 409204 Color: 1
Size: 314314 Color: 0
Size: 276483 Color: 1

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 409162 Color: 0
Size: 325768 Color: 1
Size: 265071 Color: 0

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 409353 Color: 1
Size: 321857 Color: 0
Size: 268791 Color: 0

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 409379 Color: 1
Size: 340145 Color: 1
Size: 250477 Color: 0

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 409424 Color: 0
Size: 313129 Color: 0
Size: 277448 Color: 1

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 409432 Color: 1
Size: 296829 Color: 1
Size: 293740 Color: 0

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 409539 Color: 0
Size: 333391 Color: 0
Size: 257071 Color: 1

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 409641 Color: 1
Size: 297763 Color: 0
Size: 292597 Color: 1

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 409629 Color: 0
Size: 314503 Color: 0
Size: 275869 Color: 1

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 409819 Color: 1
Size: 331969 Color: 0
Size: 258213 Color: 1

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 409752 Color: 0
Size: 320617 Color: 0
Size: 269632 Color: 1

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 409826 Color: 1
Size: 317948 Color: 1
Size: 272227 Color: 0

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 409919 Color: 1
Size: 303909 Color: 0
Size: 286173 Color: 1

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 409944 Color: 1
Size: 330028 Color: 1
Size: 260029 Color: 0

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 409967 Color: 0
Size: 332209 Color: 0
Size: 257825 Color: 1

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 410317 Color: 1
Size: 317139 Color: 1
Size: 272545 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 410238 Color: 0
Size: 334950 Color: 1
Size: 254813 Color: 0

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 410378 Color: 1
Size: 334745 Color: 1
Size: 254878 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 410291 Color: 0
Size: 299707 Color: 0
Size: 290003 Color: 1

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 410401 Color: 0
Size: 335728 Color: 0
Size: 253872 Color: 1

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 410487 Color: 0
Size: 300295 Color: 0
Size: 289219 Color: 1

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 410597 Color: 0
Size: 317404 Color: 1
Size: 272000 Color: 0

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 410664 Color: 1
Size: 333031 Color: 1
Size: 256306 Color: 0

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 410660 Color: 0
Size: 331302 Color: 0
Size: 258039 Color: 1

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 410668 Color: 0
Size: 333113 Color: 1
Size: 256220 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 410698 Color: 0
Size: 301093 Color: 1
Size: 288210 Color: 1

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 410814 Color: 1
Size: 302478 Color: 1
Size: 286709 Color: 0

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 410884 Color: 1
Size: 316853 Color: 1
Size: 272264 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 410953 Color: 1
Size: 310045 Color: 0
Size: 279003 Color: 1

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 411102 Color: 0
Size: 335209 Color: 1
Size: 253690 Color: 0

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 411056 Color: 1
Size: 308545 Color: 1
Size: 280400 Color: 0

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 411364 Color: 0
Size: 306789 Color: 1
Size: 281848 Color: 0

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 411371 Color: 0
Size: 329475 Color: 1
Size: 259155 Color: 1

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 411378 Color: 1
Size: 317128 Color: 1
Size: 271495 Color: 0

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 411480 Color: 0
Size: 336258 Color: 1
Size: 252263 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 411529 Color: 1
Size: 324443 Color: 0
Size: 264029 Color: 1

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 411539 Color: 0
Size: 305676 Color: 0
Size: 282786 Color: 1

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 411697 Color: 0
Size: 310458 Color: 1
Size: 277846 Color: 1

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 411646 Color: 1
Size: 335196 Color: 0
Size: 253159 Color: 1

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 411734 Color: 1
Size: 329079 Color: 0
Size: 259188 Color: 1

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 411781 Color: 1
Size: 329566 Color: 0
Size: 258654 Color: 1

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 412047 Color: 1
Size: 311782 Color: 0
Size: 276172 Color: 0

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 412134 Color: 1
Size: 325051 Color: 1
Size: 262816 Color: 0

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 412223 Color: 0
Size: 321772 Color: 0
Size: 266006 Color: 1

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 412178 Color: 1
Size: 320910 Color: 1
Size: 266913 Color: 0

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 412245 Color: 0
Size: 297021 Color: 0
Size: 290735 Color: 1

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 412270 Color: 1
Size: 331851 Color: 0
Size: 255880 Color: 1

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 412405 Color: 0
Size: 316745 Color: 0
Size: 270851 Color: 1

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 412270 Color: 1
Size: 317662 Color: 0
Size: 270069 Color: 1

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 412318 Color: 1
Size: 337184 Color: 1
Size: 250499 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 412447 Color: 0
Size: 303022 Color: 1
Size: 284532 Color: 0

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 412556 Color: 1
Size: 314896 Color: 0
Size: 272549 Color: 1

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 412632 Color: 0
Size: 295163 Color: 1
Size: 292206 Color: 0

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 412810 Color: 0
Size: 314966 Color: 0
Size: 272225 Color: 1

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 412679 Color: 1
Size: 337279 Color: 1
Size: 250043 Color: 0

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 413062 Color: 0
Size: 320879 Color: 1
Size: 266060 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 413052 Color: 1
Size: 327622 Color: 0
Size: 259327 Color: 1

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 413165 Color: 0
Size: 335381 Color: 1
Size: 251455 Color: 0

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 413316 Color: 0
Size: 322446 Color: 0
Size: 264239 Color: 1

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 413385 Color: 1
Size: 310637 Color: 1
Size: 275979 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 413407 Color: 1
Size: 334843 Color: 0
Size: 251751 Color: 1

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 413407 Color: 1
Size: 321042 Color: 1
Size: 265552 Color: 0

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 413543 Color: 1
Size: 307449 Color: 1
Size: 279009 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 413551 Color: 0
Size: 305424 Color: 1
Size: 281026 Color: 0

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 413543 Color: 0
Size: 316157 Color: 1
Size: 270301 Color: 0

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 413594 Color: 1
Size: 327245 Color: 1
Size: 259162 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 413623 Color: 0
Size: 301435 Color: 1
Size: 284943 Color: 0

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 413643 Color: 1
Size: 295481 Color: 1
Size: 290877 Color: 0

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 413853 Color: 0
Size: 317268 Color: 0
Size: 268880 Color: 1

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 413886 Color: 0
Size: 305610 Color: 1
Size: 280505 Color: 0

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 413905 Color: 0
Size: 323120 Color: 1
Size: 262976 Color: 1

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 413990 Color: 0
Size: 309366 Color: 1
Size: 276645 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 414033 Color: 1
Size: 321718 Color: 0
Size: 264250 Color: 1

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 414064 Color: 1
Size: 308583 Color: 0
Size: 277354 Color: 1

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 414509 Color: 0
Size: 331388 Color: 0
Size: 254104 Color: 1

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 414446 Color: 1
Size: 294420 Color: 1
Size: 291135 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 414509 Color: 0
Size: 326381 Color: 1
Size: 259111 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 414490 Color: 1
Size: 328515 Color: 0
Size: 256996 Color: 1

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 414622 Color: 0
Size: 334179 Color: 0
Size: 251200 Color: 1

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 414933 Color: 0
Size: 331928 Color: 1
Size: 253140 Color: 1

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 414981 Color: 0
Size: 310573 Color: 1
Size: 274447 Color: 0

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 415029 Color: 0
Size: 330361 Color: 1
Size: 254611 Color: 1

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 414918 Color: 1
Size: 332697 Color: 0
Size: 252386 Color: 1

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 415085 Color: 0
Size: 334913 Color: 1
Size: 250003 Color: 1

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 415170 Color: 1
Size: 304819 Color: 1
Size: 280012 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 415218 Color: 0
Size: 309187 Color: 0
Size: 275596 Color: 1

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 415303 Color: 0
Size: 306444 Color: 1
Size: 278254 Color: 1

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 415710 Color: 0
Size: 300637 Color: 1
Size: 283654 Color: 0

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 415777 Color: 0
Size: 313276 Color: 0
Size: 270948 Color: 1

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 415698 Color: 1
Size: 304366 Color: 1
Size: 279937 Color: 0

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 415819 Color: 1
Size: 324518 Color: 1
Size: 259664 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 415995 Color: 0
Size: 298368 Color: 0
Size: 285638 Color: 1

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 416142 Color: 0
Size: 313543 Color: 1
Size: 270316 Color: 1

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 416160 Color: 0
Size: 313822 Color: 1
Size: 270019 Color: 0

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 416192 Color: 0
Size: 309920 Color: 1
Size: 273889 Color: 1

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 416226 Color: 0
Size: 329286 Color: 0
Size: 254489 Color: 1

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 416267 Color: 1
Size: 296730 Color: 1
Size: 287004 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 416531 Color: 0
Size: 295661 Color: 1
Size: 287809 Color: 1

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 416633 Color: 0
Size: 319430 Color: 0
Size: 263938 Color: 1

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 416712 Color: 0
Size: 311337 Color: 1
Size: 271952 Color: 1

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 416750 Color: 0
Size: 291693 Color: 1
Size: 291558 Color: 0

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 416642 Color: 1
Size: 304896 Color: 0
Size: 278463 Color: 1

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 416939 Color: 1
Size: 318254 Color: 0
Size: 264808 Color: 1

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 416979 Color: 1
Size: 315697 Color: 1
Size: 267325 Color: 0

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 417101 Color: 1
Size: 329468 Color: 0
Size: 253432 Color: 0

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 417326 Color: 0
Size: 306669 Color: 1
Size: 276006 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 417378 Color: 1
Size: 306068 Color: 1
Size: 276555 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 417406 Color: 0
Size: 329001 Color: 1
Size: 253594 Color: 0

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 417389 Color: 1
Size: 291552 Color: 1
Size: 291060 Color: 0

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 417651 Color: 0
Size: 313492 Color: 1
Size: 268858 Color: 0

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 417636 Color: 1
Size: 309840 Color: 1
Size: 272525 Color: 0

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 417733 Color: 1
Size: 302640 Color: 0
Size: 279628 Color: 1

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 417991 Color: 1
Size: 330850 Color: 1
Size: 251160 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 418078 Color: 1
Size: 328364 Color: 0
Size: 253559 Color: 1

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 418379 Color: 1
Size: 304387 Color: 0
Size: 277235 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 418444 Color: 0
Size: 290806 Color: 0
Size: 290751 Color: 1

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 418476 Color: 0
Size: 295430 Color: 1
Size: 286095 Color: 0

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 418554 Color: 1
Size: 298157 Color: 1
Size: 283290 Color: 0

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 418609 Color: 0
Size: 306598 Color: 0
Size: 274794 Color: 1

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 418858 Color: 0
Size: 326750 Color: 1
Size: 254393 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 419123 Color: 0
Size: 329932 Color: 1
Size: 250946 Color: 0

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 419152 Color: 1
Size: 310100 Color: 0
Size: 270749 Color: 1

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 419246 Color: 0
Size: 308341 Color: 0
Size: 272414 Color: 1

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 419243 Color: 1
Size: 293174 Color: 1
Size: 287584 Color: 0

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 419451 Color: 1
Size: 327514 Color: 0
Size: 253036 Color: 1

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 419430 Color: 0
Size: 319068 Color: 0
Size: 261503 Color: 1

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 419563 Color: 0
Size: 304008 Color: 1
Size: 276430 Color: 1

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 419588 Color: 0
Size: 304730 Color: 0
Size: 275683 Color: 1

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 419589 Color: 1
Size: 304600 Color: 1
Size: 275812 Color: 0

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 419638 Color: 1
Size: 294369 Color: 0
Size: 285994 Color: 1

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 419796 Color: 0
Size: 321667 Color: 1
Size: 258538 Color: 0

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 419854 Color: 0
Size: 290141 Color: 0
Size: 290006 Color: 1

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 419865 Color: 0
Size: 307340 Color: 1
Size: 272796 Color: 0

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 419857 Color: 1
Size: 326721 Color: 1
Size: 253423 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 419873 Color: 0
Size: 306255 Color: 0
Size: 273873 Color: 1

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 419907 Color: 1
Size: 295253 Color: 1
Size: 284841 Color: 0

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 419877 Color: 0
Size: 313339 Color: 0
Size: 266785 Color: 1

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 419987 Color: 1
Size: 296565 Color: 1
Size: 283449 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 420051 Color: 0
Size: 319684 Color: 0
Size: 260266 Color: 1

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 420126 Color: 0
Size: 324702 Color: 1
Size: 255173 Color: 1

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 420271 Color: 0
Size: 328963 Color: 0
Size: 250767 Color: 1

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 420248 Color: 1
Size: 292490 Color: 1
Size: 287263 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 420291 Color: 0
Size: 314829 Color: 0
Size: 264881 Color: 1

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 420412 Color: 0
Size: 324488 Color: 0
Size: 255101 Color: 1

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 420348 Color: 1
Size: 306152 Color: 0
Size: 273501 Color: 1

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 420660 Color: 1
Size: 301323 Color: 1
Size: 278018 Color: 0

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 420791 Color: 0
Size: 310127 Color: 1
Size: 269083 Color: 0

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 420951 Color: 0
Size: 312429 Color: 1
Size: 266621 Color: 1

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 420962 Color: 0
Size: 292445 Color: 1
Size: 286594 Color: 0

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 421100 Color: 1
Size: 310831 Color: 0
Size: 268070 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 421133 Color: 1
Size: 328277 Color: 1
Size: 250591 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 421332 Color: 0
Size: 312347 Color: 1
Size: 266322 Color: 1

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 421509 Color: 0
Size: 319642 Color: 0
Size: 258850 Color: 1

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 421536 Color: 1
Size: 309656 Color: 1
Size: 268809 Color: 0

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 421546 Color: 0
Size: 297779 Color: 1
Size: 280676 Color: 0

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 421766 Color: 1
Size: 320069 Color: 0
Size: 258166 Color: 0

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 421815 Color: 1
Size: 313136 Color: 1
Size: 265050 Color: 0

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 421885 Color: 1
Size: 297373 Color: 0
Size: 280743 Color: 0

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 421914 Color: 1
Size: 322065 Color: 0
Size: 256022 Color: 1

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 422149 Color: 1
Size: 310728 Color: 1
Size: 267124 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 422239 Color: 0
Size: 325298 Color: 1
Size: 252464 Color: 0

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 422691 Color: 1
Size: 301583 Color: 0
Size: 275727 Color: 1

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 422769 Color: 0
Size: 291717 Color: 1
Size: 285515 Color: 0

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 422913 Color: 0
Size: 293009 Color: 1
Size: 284079 Color: 1

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 422939 Color: 0
Size: 322316 Color: 1
Size: 254746 Color: 0

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 423192 Color: 1
Size: 310555 Color: 0
Size: 266254 Color: 1

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 423262 Color: 1
Size: 306825 Color: 0
Size: 269914 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 423478 Color: 1
Size: 316237 Color: 1
Size: 260286 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 423563 Color: 1
Size: 307584 Color: 0
Size: 268854 Color: 1

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 423637 Color: 0
Size: 319543 Color: 1
Size: 256821 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 423687 Color: 0
Size: 304203 Color: 1
Size: 272111 Color: 1

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 423692 Color: 0
Size: 313295 Color: 1
Size: 263014 Color: 1

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 423701 Color: 0
Size: 291444 Color: 1
Size: 284856 Color: 0

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 423673 Color: 1
Size: 289231 Color: 1
Size: 287097 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 423794 Color: 0
Size: 311752 Color: 1
Size: 264455 Color: 1

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 423802 Color: 0
Size: 301812 Color: 1
Size: 274387 Color: 0

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 423805 Color: 0
Size: 306515 Color: 1
Size: 269681 Color: 1

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 423848 Color: 0
Size: 303169 Color: 0
Size: 272984 Color: 1

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 423946 Color: 1
Size: 313764 Color: 1
Size: 262291 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 423960 Color: 1
Size: 293834 Color: 1
Size: 282207 Color: 0

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 424012 Color: 1
Size: 324036 Color: 0
Size: 251953 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 424045 Color: 1
Size: 319106 Color: 1
Size: 256850 Color: 0

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 424029 Color: 0
Size: 318824 Color: 1
Size: 257148 Color: 0

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 424106 Color: 1
Size: 323307 Color: 0
Size: 252588 Color: 1

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 424113 Color: 0
Size: 321898 Color: 0
Size: 253990 Color: 1

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 424159 Color: 1
Size: 294576 Color: 1
Size: 281266 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 424220 Color: 0
Size: 324222 Color: 0
Size: 251559 Color: 1

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 424224 Color: 1
Size: 322431 Color: 0
Size: 253346 Color: 1

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 424274 Color: 1
Size: 295953 Color: 0
Size: 279774 Color: 1

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 424294 Color: 1
Size: 321515 Color: 0
Size: 254192 Color: 1

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 424357 Color: 0
Size: 287836 Color: 0
Size: 287808 Color: 1

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 424387 Color: 0
Size: 315556 Color: 1
Size: 260058 Color: 1

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 424400 Color: 0
Size: 303586 Color: 1
Size: 272015 Color: 0

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 424648 Color: 0
Size: 289825 Color: 1
Size: 285528 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 424743 Color: 0
Size: 317483 Color: 1
Size: 257775 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 424793 Color: 0
Size: 322605 Color: 1
Size: 252603 Color: 1

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 424822 Color: 0
Size: 290824 Color: 1
Size: 284355 Color: 0

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 424863 Color: 0
Size: 320086 Color: 1
Size: 255052 Color: 1

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 424906 Color: 0
Size: 310107 Color: 0
Size: 264988 Color: 1

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 424940 Color: 1
Size: 309988 Color: 0
Size: 265073 Color: 1

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 424911 Color: 0
Size: 314823 Color: 0
Size: 260267 Color: 1

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 425070 Color: 1
Size: 308078 Color: 1
Size: 266853 Color: 0

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 425087 Color: 1
Size: 323656 Color: 0
Size: 251258 Color: 1

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 425098 Color: 0
Size: 299898 Color: 0
Size: 275005 Color: 1

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 425272 Color: 0
Size: 292961 Color: 1
Size: 281768 Color: 0

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 425302 Color: 0
Size: 310123 Color: 1
Size: 264576 Color: 1

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 425335 Color: 0
Size: 324663 Color: 0
Size: 250003 Color: 1

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 425365 Color: 1
Size: 302070 Color: 1
Size: 272566 Color: 0

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 425340 Color: 0
Size: 319780 Color: 1
Size: 254881 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 418316 Color: 0
Size: 313892 Color: 1
Size: 267793 Color: 0

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 425543 Color: 1
Size: 314163 Color: 0
Size: 260295 Color: 1

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 425647 Color: 1
Size: 318079 Color: 1
Size: 256275 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 425792 Color: 1
Size: 289044 Color: 0
Size: 285165 Color: 0

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 425851 Color: 0
Size: 307769 Color: 0
Size: 266381 Color: 1

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 426155 Color: 1
Size: 292982 Color: 1
Size: 280864 Color: 0

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 426275 Color: 1
Size: 317549 Color: 1
Size: 256177 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 426370 Color: 1
Size: 315574 Color: 0
Size: 258057 Color: 1

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 426370 Color: 1
Size: 293349 Color: 0
Size: 280282 Color: 0

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 426433 Color: 1
Size: 303410 Color: 0
Size: 270158 Color: 1

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 426439 Color: 1
Size: 315853 Color: 1
Size: 257709 Color: 0

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 426536 Color: 1
Size: 320096 Color: 1
Size: 253369 Color: 0

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 426492 Color: 0
Size: 318871 Color: 0
Size: 254638 Color: 1

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 426640 Color: 1
Size: 288766 Color: 1
Size: 284595 Color: 0

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 426712 Color: 1
Size: 292178 Color: 1
Size: 281111 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 426785 Color: 1
Size: 288836 Color: 0
Size: 284380 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 426981 Color: 0
Size: 312649 Color: 0
Size: 260371 Color: 1

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 427104 Color: 1
Size: 311435 Color: 1
Size: 261462 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 427098 Color: 0
Size: 287591 Color: 0
Size: 285312 Color: 1

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 427120 Color: 0
Size: 290308 Color: 0
Size: 282573 Color: 1

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 427182 Color: 1
Size: 288875 Color: 1
Size: 283944 Color: 0

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 427218 Color: 0
Size: 308250 Color: 0
Size: 264533 Color: 1

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 427321 Color: 0
Size: 296794 Color: 0
Size: 275886 Color: 1

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 427639 Color: 1
Size: 320872 Color: 1
Size: 251490 Color: 0

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 427630 Color: 0
Size: 294757 Color: 0
Size: 277614 Color: 1

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 427891 Color: 0
Size: 298667 Color: 1
Size: 273443 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 427923 Color: 0
Size: 298966 Color: 1
Size: 273112 Color: 0

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 427925 Color: 0
Size: 314168 Color: 1
Size: 257908 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 428011 Color: 1
Size: 299638 Color: 1
Size: 272352 Color: 0

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 427950 Color: 0
Size: 294743 Color: 0
Size: 277308 Color: 1

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 428031 Color: 1
Size: 316867 Color: 1
Size: 255103 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 428025 Color: 0
Size: 309583 Color: 1
Size: 262393 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 428115 Color: 1
Size: 289558 Color: 1
Size: 282328 Color: 0

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 428116 Color: 1
Size: 293797 Color: 0
Size: 278088 Color: 1

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 428328 Color: 0
Size: 309586 Color: 1
Size: 262087 Color: 1

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 428483 Color: 0
Size: 296532 Color: 0
Size: 274986 Color: 1

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 428524 Color: 1
Size: 302441 Color: 1
Size: 269036 Color: 0

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 428766 Color: 1
Size: 287236 Color: 0
Size: 283999 Color: 1

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 428865 Color: 0
Size: 310310 Color: 1
Size: 260826 Color: 0

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 428922 Color: 1
Size: 316039 Color: 0
Size: 255040 Color: 1

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 429075 Color: 1
Size: 308185 Color: 0
Size: 262741 Color: 1

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 429137 Color: 0
Size: 299944 Color: 0
Size: 270920 Color: 1

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 429400 Color: 0
Size: 314159 Color: 0
Size: 256442 Color: 1

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 429377 Color: 1
Size: 302760 Color: 1
Size: 267864 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 429476 Color: 1
Size: 298972 Color: 1
Size: 271553 Color: 0

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 429559 Color: 0
Size: 293164 Color: 1
Size: 277278 Color: 1

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 429768 Color: 0
Size: 319285 Color: 1
Size: 250948 Color: 0

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 429796 Color: 0
Size: 310795 Color: 1
Size: 259410 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 429827 Color: 1
Size: 313783 Color: 1
Size: 256391 Color: 0

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 429938 Color: 0
Size: 285687 Color: 1
Size: 284376 Color: 1

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 430021 Color: 1
Size: 292793 Color: 0
Size: 277187 Color: 1

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 430255 Color: 1
Size: 287962 Color: 1
Size: 281784 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 430229 Color: 0
Size: 307001 Color: 0
Size: 262771 Color: 1

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 430410 Color: 0
Size: 310786 Color: 1
Size: 258805 Color: 0

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 430494 Color: 1
Size: 297472 Color: 1
Size: 272035 Color: 0

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 430669 Color: 1
Size: 317308 Color: 1
Size: 252024 Color: 0

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 430756 Color: 0
Size: 300453 Color: 0
Size: 268792 Color: 1

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 430752 Color: 1
Size: 300271 Color: 1
Size: 268978 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 430877 Color: 0
Size: 306827 Color: 0
Size: 262297 Color: 1

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 430886 Color: 1
Size: 285083 Color: 0
Size: 284032 Color: 1

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 430882 Color: 0
Size: 287824 Color: 1
Size: 281295 Color: 0

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 431063 Color: 1
Size: 315774 Color: 1
Size: 253164 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 431201 Color: 1
Size: 293814 Color: 1
Size: 274986 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 431237 Color: 0
Size: 318681 Color: 1
Size: 250083 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 431248 Color: 1
Size: 288335 Color: 0
Size: 280418 Color: 1

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 431419 Color: 1
Size: 318558 Color: 1
Size: 250024 Color: 0

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 431454 Color: 1
Size: 315046 Color: 0
Size: 253501 Color: 0

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 431547 Color: 1
Size: 316616 Color: 0
Size: 251838 Color: 1

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 431575 Color: 1
Size: 309176 Color: 0
Size: 259250 Color: 1

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 431490 Color: 0
Size: 307765 Color: 1
Size: 260746 Color: 0

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 431597 Color: 1
Size: 300265 Color: 1
Size: 268139 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 431653 Color: 1
Size: 294488 Color: 0
Size: 273860 Color: 0

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 431788 Color: 1
Size: 307942 Color: 1
Size: 260271 Color: 0

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 431782 Color: 0
Size: 317003 Color: 0
Size: 251216 Color: 1

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 431880 Color: 1
Size: 302235 Color: 0
Size: 265886 Color: 1

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 431961 Color: 1
Size: 309311 Color: 0
Size: 258729 Color: 1

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 431908 Color: 0
Size: 297390 Color: 0
Size: 270703 Color: 1

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 431993 Color: 1
Size: 294870 Color: 1
Size: 273138 Color: 0

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 432064 Color: 1
Size: 310106 Color: 0
Size: 257831 Color: 1

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 431971 Color: 0
Size: 302252 Color: 1
Size: 265778 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 432160 Color: 0
Size: 303843 Color: 0
Size: 263998 Color: 1

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 432164 Color: 0
Size: 292827 Color: 1
Size: 275010 Color: 1

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 432405 Color: 0
Size: 294230 Color: 1
Size: 273366 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 432463 Color: 1
Size: 296518 Color: 0
Size: 271020 Color: 1

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 432436 Color: 0
Size: 302035 Color: 0
Size: 265530 Color: 1

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 432610 Color: 1
Size: 286331 Color: 0
Size: 281060 Color: 1

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 432722 Color: 0
Size: 299700 Color: 1
Size: 267579 Color: 1

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 432770 Color: 0
Size: 302918 Color: 0
Size: 264313 Color: 1

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 432910 Color: 0
Size: 283985 Color: 1
Size: 283106 Color: 0

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 432920 Color: 0
Size: 295456 Color: 0
Size: 271625 Color: 1

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 433016 Color: 0
Size: 288690 Color: 1
Size: 278295 Color: 0

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 433104 Color: 0
Size: 290686 Color: 1
Size: 276211 Color: 0

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 433171 Color: 1
Size: 304353 Color: 0
Size: 262477 Color: 1

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 433196 Color: 0
Size: 296149 Color: 1
Size: 270656 Color: 0

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 433215 Color: 0
Size: 288905 Color: 1
Size: 277881 Color: 1

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 433249 Color: 0
Size: 309704 Color: 1
Size: 257048 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 433463 Color: 1
Size: 285546 Color: 1
Size: 280992 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 433548 Color: 0
Size: 306534 Color: 0
Size: 259919 Color: 1

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 433612 Color: 0
Size: 291700 Color: 0
Size: 274689 Color: 1

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 433737 Color: 1
Size: 295666 Color: 1
Size: 270598 Color: 0

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 433832 Color: 1
Size: 297659 Color: 0
Size: 268510 Color: 0

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 433853 Color: 1
Size: 309778 Color: 0
Size: 256370 Color: 1

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 433937 Color: 1
Size: 307613 Color: 1
Size: 258451 Color: 0

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 433890 Color: 0
Size: 314292 Color: 0
Size: 251819 Color: 1

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 434187 Color: 1
Size: 304141 Color: 1
Size: 261673 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 434170 Color: 0
Size: 315185 Color: 0
Size: 250646 Color: 1

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 434274 Color: 1
Size: 297467 Color: 0
Size: 268260 Color: 1

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 434237 Color: 0
Size: 315744 Color: 1
Size: 250020 Color: 0

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 434393 Color: 0
Size: 297612 Color: 1
Size: 267996 Color: 1

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 434413 Color: 0
Size: 300892 Color: 0
Size: 264696 Color: 1

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 434470 Color: 0
Size: 293821 Color: 1
Size: 271710 Color: 1

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 434604 Color: 1
Size: 291739 Color: 0
Size: 273658 Color: 0

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 434680 Color: 1
Size: 283498 Color: 0
Size: 281823 Color: 1

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 434584 Color: 0
Size: 290723 Color: 0
Size: 274694 Color: 1

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 434754 Color: 0
Size: 289070 Color: 1
Size: 276177 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 434824 Color: 0
Size: 312367 Color: 1
Size: 252810 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 434927 Color: 1
Size: 286437 Color: 0
Size: 278637 Color: 1

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 434845 Color: 0
Size: 308833 Color: 1
Size: 256323 Color: 0

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 434949 Color: 1
Size: 307656 Color: 1
Size: 257396 Color: 0

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 434985 Color: 1
Size: 309284 Color: 0
Size: 255732 Color: 1

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 435065 Color: 1
Size: 282931 Color: 0
Size: 282005 Color: 0

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 435067 Color: 1
Size: 282793 Color: 1
Size: 282141 Color: 0

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 435118 Color: 0
Size: 308757 Color: 0
Size: 256126 Color: 1

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 435220 Color: 1
Size: 304003 Color: 0
Size: 260778 Color: 1

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 435294 Color: 0
Size: 301269 Color: 1
Size: 263438 Color: 1

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 435329 Color: 0
Size: 287873 Color: 0
Size: 276799 Color: 1

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 435472 Color: 1
Size: 291807 Color: 0
Size: 272722 Color: 1

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 435429 Color: 0
Size: 300438 Color: 1
Size: 264134 Color: 0

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 435522 Color: 1
Size: 290064 Color: 0
Size: 274415 Color: 1

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 435465 Color: 0
Size: 310606 Color: 0
Size: 253930 Color: 1

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 435537 Color: 0
Size: 286424 Color: 0
Size: 278040 Color: 1

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 435699 Color: 1
Size: 289440 Color: 0
Size: 274862 Color: 1

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 435663 Color: 0
Size: 289988 Color: 0
Size: 274350 Color: 1

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 435793 Color: 0
Size: 287040 Color: 1
Size: 277168 Color: 1

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 435710 Color: 1
Size: 312194 Color: 0
Size: 252097 Color: 1

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 435876 Color: 0
Size: 307009 Color: 0
Size: 257116 Color: 1

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 435769 Color: 1
Size: 296758 Color: 1
Size: 267474 Color: 0

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 435984 Color: 0
Size: 295248 Color: 0
Size: 268769 Color: 1

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 435970 Color: 1
Size: 308082 Color: 0
Size: 255949 Color: 1

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 436142 Color: 1
Size: 309516 Color: 0
Size: 254343 Color: 1

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 436202 Color: 1
Size: 297228 Color: 0
Size: 266571 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 436591 Color: 0
Size: 294554 Color: 1
Size: 268856 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 436630 Color: 1
Size: 311199 Color: 0
Size: 252172 Color: 0

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 436678 Color: 1
Size: 294005 Color: 1
Size: 269318 Color: 0

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 436743 Color: 1
Size: 294154 Color: 0
Size: 269104 Color: 1

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 436808 Color: 0
Size: 307294 Color: 1
Size: 255899 Color: 0

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 436883 Color: 1
Size: 286115 Color: 1
Size: 277003 Color: 0

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 437066 Color: 1
Size: 291379 Color: 0
Size: 271556 Color: 1

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 437112 Color: 0
Size: 291724 Color: 1
Size: 271165 Color: 0

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 437233 Color: 1
Size: 305680 Color: 1
Size: 257088 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 437297 Color: 0
Size: 312543 Color: 1
Size: 250161 Color: 0

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 437369 Color: 0
Size: 286294 Color: 0
Size: 276338 Color: 1

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 437308 Color: 1
Size: 282978 Color: 0
Size: 279715 Color: 1

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 437374 Color: 0
Size: 308312 Color: 0
Size: 254315 Color: 1

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 437396 Color: 0
Size: 301618 Color: 1
Size: 260987 Color: 0

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 437587 Color: 0
Size: 305576 Color: 1
Size: 256838 Color: 0

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 437589 Color: 1
Size: 287119 Color: 1
Size: 275293 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 437629 Color: 0
Size: 304602 Color: 0
Size: 257770 Color: 1

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 437652 Color: 1
Size: 289684 Color: 0
Size: 272665 Color: 1

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 437724 Color: 1
Size: 282148 Color: 0
Size: 280129 Color: 1

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 437739 Color: 0
Size: 296305 Color: 0
Size: 265957 Color: 1

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 437754 Color: 1
Size: 303200 Color: 0
Size: 259047 Color: 1

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 437762 Color: 1
Size: 303337 Color: 0
Size: 258902 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 437765 Color: 1
Size: 286372 Color: 0
Size: 275864 Color: 1

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 437758 Color: 0
Size: 307156 Color: 0
Size: 255087 Color: 1

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 437862 Color: 1
Size: 284775 Color: 0
Size: 277364 Color: 0

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 437910 Color: 1
Size: 295900 Color: 0
Size: 266191 Color: 0

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 437922 Color: 1
Size: 310622 Color: 0
Size: 251457 Color: 1

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 437953 Color: 0
Size: 288909 Color: 0
Size: 273139 Color: 1

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 438110 Color: 0
Size: 307485 Color: 1
Size: 254406 Color: 1

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 438212 Color: 1
Size: 286704 Color: 0
Size: 275085 Color: 0

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 438236 Color: 1
Size: 306122 Color: 0
Size: 255643 Color: 1

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 438365 Color: 0
Size: 310447 Color: 1
Size: 251189 Color: 1

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 438474 Color: 0
Size: 306601 Color: 0
Size: 254926 Color: 1

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 438495 Color: 0
Size: 291732 Color: 1
Size: 269774 Color: 0

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 438592 Color: 1
Size: 287604 Color: 0
Size: 273805 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 438601 Color: 1
Size: 284558 Color: 0
Size: 276842 Color: 0

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 438629 Color: 1
Size: 285346 Color: 0
Size: 276026 Color: 1

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 438709 Color: 0
Size: 308785 Color: 1
Size: 252507 Color: 0

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 438707 Color: 1
Size: 309655 Color: 1
Size: 251639 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 438889 Color: 0
Size: 310166 Color: 0
Size: 250946 Color: 1

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 438934 Color: 0
Size: 306918 Color: 1
Size: 254149 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 438974 Color: 0
Size: 294482 Color: 1
Size: 266545 Color: 1

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 439007 Color: 0
Size: 282535 Color: 0
Size: 278459 Color: 1

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 438935 Color: 1
Size: 309456 Color: 0
Size: 251610 Color: 1

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 439030 Color: 0
Size: 281888 Color: 0
Size: 279083 Color: 1

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 438943 Color: 1
Size: 295119 Color: 1
Size: 265939 Color: 0

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 439166 Color: 1
Size: 305680 Color: 0
Size: 255155 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 439445 Color: 0
Size: 294619 Color: 1
Size: 265937 Color: 0

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 439457 Color: 0
Size: 295052 Color: 1
Size: 265492 Color: 1

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 439508 Color: 0
Size: 293426 Color: 1
Size: 267067 Color: 1

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 439575 Color: 0
Size: 288562 Color: 0
Size: 271864 Color: 1

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 439509 Color: 1
Size: 299096 Color: 0
Size: 261396 Color: 1

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 439525 Color: 1
Size: 280293 Color: 1
Size: 280183 Color: 0

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 439717 Color: 0
Size: 292601 Color: 1
Size: 267683 Color: 0

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 439736 Color: 1
Size: 306615 Color: 0
Size: 253650 Color: 1

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 439741 Color: 1
Size: 299224 Color: 1
Size: 261036 Color: 0

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 439879 Color: 0
Size: 292748 Color: 1
Size: 267374 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 440004 Color: 1
Size: 287080 Color: 0
Size: 272917 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 440065 Color: 1
Size: 300080 Color: 1
Size: 259856 Color: 0

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 440209 Color: 1
Size: 282002 Color: 0
Size: 277790 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 440268 Color: 1
Size: 294009 Color: 1
Size: 265724 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 440652 Color: 0
Size: 300677 Color: 1
Size: 258672 Color: 1

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 440672 Color: 0
Size: 303277 Color: 0
Size: 256052 Color: 1

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 440699 Color: 1
Size: 297059 Color: 0
Size: 262243 Color: 1

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 440856 Color: 0
Size: 295038 Color: 0
Size: 264107 Color: 1

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 440858 Color: 0
Size: 304292 Color: 1
Size: 254851 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 441089 Color: 1
Size: 290785 Color: 0
Size: 268127 Color: 1

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 441357 Color: 1
Size: 306000 Color: 1
Size: 252644 Color: 0

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 441368 Color: 1
Size: 301175 Color: 0
Size: 257458 Color: 1

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 441627 Color: 0
Size: 297594 Color: 0
Size: 260780 Color: 1

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 441682 Color: 1
Size: 279539 Color: 1
Size: 278780 Color: 0

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 441733 Color: 0
Size: 293814 Color: 0
Size: 264454 Color: 1

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 441806 Color: 0
Size: 305135 Color: 0
Size: 253060 Color: 1

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 441822 Color: 1
Size: 285966 Color: 0
Size: 272213 Color: 1

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 441856 Color: 0
Size: 284847 Color: 1
Size: 273298 Color: 0

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 441953 Color: 0
Size: 307155 Color: 1
Size: 250893 Color: 0

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 442045 Color: 0
Size: 282718 Color: 1
Size: 275238 Color: 1

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 442054 Color: 0
Size: 293723 Color: 1
Size: 264224 Color: 0

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 442228 Color: 0
Size: 302382 Color: 0
Size: 255391 Color: 1

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 442363 Color: 1
Size: 298663 Color: 0
Size: 258975 Color: 0

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 442438 Color: 0
Size: 294607 Color: 1
Size: 262956 Color: 0

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 442455 Color: 1
Size: 306756 Color: 0
Size: 250790 Color: 1

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 442489 Color: 1
Size: 296254 Color: 1
Size: 261258 Color: 0

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 442579 Color: 0
Size: 307279 Color: 1
Size: 250143 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 442662 Color: 0
Size: 296568 Color: 1
Size: 260771 Color: 1

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 442726 Color: 0
Size: 287732 Color: 1
Size: 269543 Color: 0

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 442918 Color: 1
Size: 281364 Color: 0
Size: 275719 Color: 1

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 443174 Color: 0
Size: 304829 Color: 0
Size: 251998 Color: 1

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 443220 Color: 0
Size: 302721 Color: 1
Size: 254060 Color: 1

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 443226 Color: 0
Size: 284695 Color: 1
Size: 272080 Color: 0

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 443281 Color: 1
Size: 287186 Color: 0
Size: 269534 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 443437 Color: 0
Size: 295326 Color: 1
Size: 261238 Color: 1

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 443744 Color: 0
Size: 300244 Color: 0
Size: 256013 Color: 1

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 443851 Color: 0
Size: 287793 Color: 1
Size: 268357 Color: 0

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 443877 Color: 0
Size: 294439 Color: 1
Size: 261685 Color: 1

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 443926 Color: 0
Size: 278429 Color: 0
Size: 277646 Color: 1

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 443886 Color: 1
Size: 281677 Color: 0
Size: 274438 Color: 1

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 443966 Color: 0
Size: 304595 Color: 1
Size: 251440 Color: 0

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 443992 Color: 0
Size: 279239 Color: 1
Size: 276770 Color: 1

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 444092 Color: 0
Size: 284499 Color: 0
Size: 271410 Color: 1

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 444036 Color: 1
Size: 292944 Color: 1
Size: 263021 Color: 0

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 444159 Color: 0
Size: 297922 Color: 1
Size: 257920 Color: 1

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 444197 Color: 0
Size: 301844 Color: 1
Size: 253960 Color: 1

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 444203 Color: 0
Size: 298268 Color: 1
Size: 257530 Color: 0

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 444194 Color: 1
Size: 300785 Color: 1
Size: 255022 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 444208 Color: 0
Size: 303816 Color: 1
Size: 251977 Color: 0

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 444350 Color: 1
Size: 289127 Color: 0
Size: 266524 Color: 0

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 444527 Color: 1
Size: 290215 Color: 1
Size: 265259 Color: 0

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 444616 Color: 0
Size: 279852 Color: 1
Size: 275533 Color: 1

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 444631 Color: 0
Size: 285787 Color: 1
Size: 269583 Color: 1

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 444563 Color: 1
Size: 289902 Color: 1
Size: 265536 Color: 0

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 444719 Color: 0
Size: 285768 Color: 0
Size: 269514 Color: 1

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 444696 Color: 1
Size: 294185 Color: 0
Size: 261120 Color: 1

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 444725 Color: 0
Size: 279184 Color: 1
Size: 276092 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 444774 Color: 0
Size: 282881 Color: 1
Size: 272346 Color: 1

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 444844 Color: 1
Size: 294194 Color: 0
Size: 260963 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 444882 Color: 1
Size: 278822 Color: 0
Size: 276297 Color: 1

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 445160 Color: 1
Size: 291352 Color: 1
Size: 263489 Color: 0

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 445500 Color: 1
Size: 300662 Color: 0
Size: 253839 Color: 1

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 445508 Color: 1
Size: 292205 Color: 0
Size: 262288 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 445593 Color: 1
Size: 304353 Color: 0
Size: 250055 Color: 1

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 445666 Color: 0
Size: 294711 Color: 1
Size: 259624 Color: 0

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 445613 Color: 1
Size: 302458 Color: 1
Size: 251930 Color: 0

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 445733 Color: 0
Size: 303983 Color: 1
Size: 250285 Color: 1

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 445734 Color: 1
Size: 300373 Color: 0
Size: 253894 Color: 1

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 445837 Color: 1
Size: 288173 Color: 1
Size: 265991 Color: 0

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 445855 Color: 0
Size: 277658 Color: 1
Size: 276488 Color: 1

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 445912 Color: 1
Size: 292852 Color: 0
Size: 261237 Color: 1

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 445972 Color: 1
Size: 297812 Color: 0
Size: 256217 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 445969 Color: 0
Size: 291944 Color: 0
Size: 262088 Color: 1

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 446135 Color: 1
Size: 303037 Color: 0
Size: 250829 Color: 1

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 446348 Color: 1
Size: 287615 Color: 1
Size: 266038 Color: 0

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 446460 Color: 0
Size: 279646 Color: 1
Size: 273895 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 446542 Color: 1
Size: 286926 Color: 0
Size: 266533 Color: 0

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 446763 Color: 0
Size: 277746 Color: 1
Size: 275492 Color: 0

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 446837 Color: 0
Size: 291588 Color: 1
Size: 261576 Color: 1

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 446851 Color: 1
Size: 281759 Color: 1
Size: 271391 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 446968 Color: 1
Size: 300294 Color: 0
Size: 252739 Color: 1

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 446954 Color: 0
Size: 292194 Color: 0
Size: 260853 Color: 1

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 447144 Color: 0
Size: 289807 Color: 0
Size: 263050 Color: 1

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 447180 Color: 0
Size: 297811 Color: 0
Size: 255010 Color: 1

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 447230 Color: 1
Size: 287085 Color: 0
Size: 265686 Color: 1

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 447335 Color: 0
Size: 293544 Color: 1
Size: 259122 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 447557 Color: 0
Size: 290228 Color: 1
Size: 262216 Color: 0

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 447678 Color: 0
Size: 290383 Color: 1
Size: 261940 Color: 0

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 447752 Color: 1
Size: 287030 Color: 1
Size: 265219 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 447795 Color: 1
Size: 296801 Color: 0
Size: 255405 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 447967 Color: 1
Size: 297004 Color: 1
Size: 255030 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 448025 Color: 0
Size: 287253 Color: 1
Size: 264723 Color: 0

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 448310 Color: 1
Size: 296368 Color: 0
Size: 255323 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 448315 Color: 0
Size: 291636 Color: 0
Size: 260050 Color: 1

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 448324 Color: 1
Size: 279100 Color: 1
Size: 272577 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 448323 Color: 0
Size: 301159 Color: 0
Size: 250519 Color: 1

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 448340 Color: 1
Size: 295335 Color: 1
Size: 256326 Color: 0

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 448515 Color: 1
Size: 299779 Color: 1
Size: 251707 Color: 0

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 448792 Color: 1
Size: 283528 Color: 0
Size: 267681 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 448882 Color: 1
Size: 276520 Color: 0
Size: 274599 Color: 1

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 449155 Color: 0
Size: 291582 Color: 0
Size: 259264 Color: 1

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 449208 Color: 1
Size: 289344 Color: 0
Size: 261449 Color: 1

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 449270 Color: 0
Size: 286470 Color: 1
Size: 264261 Color: 0

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 449678 Color: 1
Size: 278503 Color: 1
Size: 271820 Color: 0

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 449940 Color: 0
Size: 297446 Color: 1
Size: 252615 Color: 0

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 449898 Color: 1
Size: 284068 Color: 1
Size: 266035 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 449976 Color: 0
Size: 291836 Color: 1
Size: 258189 Color: 0

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 449979 Color: 0
Size: 286396 Color: 0
Size: 263626 Color: 1

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 450668 Color: 1
Size: 298961 Color: 0
Size: 250372 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 450896 Color: 1
Size: 288333 Color: 0
Size: 260772 Color: 1

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 451003 Color: 1
Size: 290260 Color: 0
Size: 258738 Color: 1

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 451032 Color: 1
Size: 286425 Color: 0
Size: 262544 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 451072 Color: 1
Size: 294870 Color: 0
Size: 254059 Color: 1

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 451323 Color: 0
Size: 276592 Color: 0
Size: 272086 Color: 1

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 451464 Color: 1
Size: 296496 Color: 0
Size: 252041 Color: 1

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 451469 Color: 1
Size: 295707 Color: 1
Size: 252825 Color: 0

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 451552 Color: 1
Size: 281783 Color: 1
Size: 266666 Color: 0

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 451557 Color: 0
Size: 298125 Color: 1
Size: 250319 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 451599 Color: 1
Size: 277903 Color: 0
Size: 270499 Color: 1

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 451824 Color: 0
Size: 279173 Color: 1
Size: 269004 Color: 1

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 451861 Color: 1
Size: 285309 Color: 1
Size: 262831 Color: 0

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 451988 Color: 1
Size: 291826 Color: 1
Size: 256187 Color: 0

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 452023 Color: 1
Size: 290976 Color: 0
Size: 257002 Color: 1

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 452054 Color: 1
Size: 279300 Color: 0
Size: 268647 Color: 1

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 452106 Color: 0
Size: 279478 Color: 1
Size: 268417 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 452252 Color: 0
Size: 289374 Color: 1
Size: 258375 Color: 1

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 452379 Color: 0
Size: 289775 Color: 0
Size: 257847 Color: 1

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 452534 Color: 0
Size: 290531 Color: 0
Size: 256936 Color: 1

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 452615 Color: 1
Size: 291719 Color: 0
Size: 255667 Color: 1

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 452656 Color: 1
Size: 295729 Color: 0
Size: 251616 Color: 0

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 452689 Color: 0
Size: 293151 Color: 1
Size: 254161 Color: 0

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 452715 Color: 1
Size: 292535 Color: 0
Size: 254751 Color: 1

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 452886 Color: 1
Size: 274392 Color: 1
Size: 272723 Color: 0

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 453028 Color: 0
Size: 287452 Color: 0
Size: 259521 Color: 1

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 453074 Color: 1
Size: 291730 Color: 0
Size: 255197 Color: 1

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 453095 Color: 0
Size: 277994 Color: 0
Size: 268912 Color: 1

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 453126 Color: 0
Size: 281224 Color: 1
Size: 265651 Color: 1

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 453157 Color: 0
Size: 284642 Color: 1
Size: 262202 Color: 1

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 453246 Color: 1
Size: 274958 Color: 0
Size: 271797 Color: 1

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 453283 Color: 1
Size: 292846 Color: 1
Size: 253872 Color: 0

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 453329 Color: 1
Size: 273695 Color: 0
Size: 272977 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 453374 Color: 0
Size: 287255 Color: 1
Size: 259372 Color: 1

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 453557 Color: 0
Size: 285311 Color: 1
Size: 261133 Color: 1

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 453575 Color: 0
Size: 295543 Color: 0
Size: 250883 Color: 1

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 453611 Color: 1
Size: 273252 Color: 0
Size: 273138 Color: 1

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 453697 Color: 1
Size: 285945 Color: 0
Size: 260359 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 453731 Color: 1
Size: 294529 Color: 1
Size: 251741 Color: 0

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 453797 Color: 0
Size: 277674 Color: 1
Size: 268530 Color: 0

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 453944 Color: 1
Size: 289077 Color: 0
Size: 256980 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 454000 Color: 0
Size: 291842 Color: 1
Size: 254159 Color: 1

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 454063 Color: 0
Size: 286767 Color: 1
Size: 259171 Color: 0

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 454168 Color: 1
Size: 281604 Color: 0
Size: 264229 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 454175 Color: 1
Size: 289054 Color: 0
Size: 256772 Color: 1

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 454203 Color: 1
Size: 274296 Color: 0
Size: 271502 Color: 1

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 454205 Color: 1
Size: 283847 Color: 0
Size: 261949 Color: 0

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 454309 Color: 0
Size: 290856 Color: 1
Size: 254836 Color: 1

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 454747 Color: 1
Size: 286147 Color: 0
Size: 259107 Color: 1

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 454783 Color: 0
Size: 287714 Color: 1
Size: 257504 Color: 0

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 454816 Color: 0
Size: 283843 Color: 1
Size: 261342 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 454845 Color: 1
Size: 273590 Color: 1
Size: 271566 Color: 0

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 455098 Color: 1
Size: 272926 Color: 1
Size: 271977 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 455069 Color: 0
Size: 292100 Color: 0
Size: 252832 Color: 1

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 455261 Color: 1
Size: 289340 Color: 1
Size: 255400 Color: 0

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 455327 Color: 0
Size: 283477 Color: 1
Size: 261197 Color: 0

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 455311 Color: 1
Size: 285189 Color: 1
Size: 259501 Color: 0

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 455548 Color: 1
Size: 276752 Color: 1
Size: 267701 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 455807 Color: 1
Size: 277896 Color: 0
Size: 266298 Color: 1

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 455821 Color: 0
Size: 287480 Color: 0
Size: 256700 Color: 1

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 455979 Color: 1
Size: 275363 Color: 0
Size: 268659 Color: 0

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 456080 Color: 0
Size: 290967 Color: 0
Size: 252954 Color: 1

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 456177 Color: 1
Size: 282236 Color: 1
Size: 261588 Color: 0

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 456214 Color: 1
Size: 293196 Color: 1
Size: 250591 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 456445 Color: 1
Size: 279848 Color: 0
Size: 263708 Color: 0

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 456373 Color: 0
Size: 276462 Color: 0
Size: 267166 Color: 1

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 456597 Color: 1
Size: 283205 Color: 0
Size: 260199 Color: 1

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 456518 Color: 0
Size: 276713 Color: 1
Size: 266770 Color: 0

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 456531 Color: 0
Size: 281745 Color: 0
Size: 261725 Color: 1

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 456649 Color: 0
Size: 286654 Color: 1
Size: 256698 Color: 1

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 456660 Color: 1
Size: 276134 Color: 1
Size: 267207 Color: 0

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 456772 Color: 0
Size: 287780 Color: 1
Size: 255449 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 456843 Color: 0
Size: 286019 Color: 1
Size: 257139 Color: 0

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 457176 Color: 0
Size: 290931 Color: 1
Size: 251894 Color: 0

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 457306 Color: 0
Size: 279044 Color: 0
Size: 263651 Color: 1

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 457386 Color: 0
Size: 281991 Color: 1
Size: 260624 Color: 1

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 457539 Color: 0
Size: 287692 Color: 1
Size: 254770 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 457606 Color: 0
Size: 290409 Color: 1
Size: 251986 Color: 0

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 457661 Color: 0
Size: 274212 Color: 0
Size: 268128 Color: 1

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 457734 Color: 1
Size: 285853 Color: 0
Size: 256414 Color: 1

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 457800 Color: 0
Size: 291997 Color: 1
Size: 250204 Color: 0

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 458072 Color: 1
Size: 279834 Color: 1
Size: 262095 Color: 0

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 458057 Color: 0
Size: 283602 Color: 0
Size: 258342 Color: 1

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 458178 Color: 1
Size: 291067 Color: 1
Size: 250756 Color: 0

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 458386 Color: 1
Size: 277804 Color: 1
Size: 263811 Color: 0

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 458741 Color: 0
Size: 290419 Color: 1
Size: 250841 Color: 0

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 458905 Color: 0
Size: 286123 Color: 1
Size: 254973 Color: 0

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 458950 Color: 1
Size: 275925 Color: 1
Size: 265126 Color: 0

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 458959 Color: 0
Size: 275751 Color: 1
Size: 265291 Color: 0

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 458998 Color: 1
Size: 273426 Color: 0
Size: 267577 Color: 1

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 459266 Color: 1
Size: 282970 Color: 1
Size: 257765 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 459244 Color: 0
Size: 276335 Color: 1
Size: 264422 Color: 0

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 459500 Color: 0
Size: 280938 Color: 1
Size: 259563 Color: 1

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 459510 Color: 0
Size: 279117 Color: 1
Size: 261374 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 459615 Color: 0
Size: 275712 Color: 0
Size: 264674 Color: 1

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 459624 Color: 1
Size: 278776 Color: 1
Size: 261601 Color: 0

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 459638 Color: 0
Size: 280115 Color: 1
Size: 260248 Color: 0

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 459649 Color: 0
Size: 284750 Color: 1
Size: 255602 Color: 1

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 459702 Color: 0
Size: 288509 Color: 1
Size: 251790 Color: 0

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 459753 Color: 0
Size: 283425 Color: 1
Size: 256823 Color: 1

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 459785 Color: 0
Size: 286285 Color: 1
Size: 253931 Color: 0

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 459928 Color: 0
Size: 279268 Color: 0
Size: 260805 Color: 1

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 459977 Color: 0
Size: 278091 Color: 1
Size: 261933 Color: 1

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 460518 Color: 1
Size: 287492 Color: 0
Size: 251991 Color: 1

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 460573 Color: 1
Size: 281657 Color: 0
Size: 257771 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 460723 Color: 1
Size: 282898 Color: 0
Size: 256380 Color: 0

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 460905 Color: 1
Size: 287401 Color: 1
Size: 251695 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 460867 Color: 0
Size: 287855 Color: 0
Size: 251279 Color: 1

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 461238 Color: 1
Size: 272876 Color: 1
Size: 265887 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 461272 Color: 0
Size: 279799 Color: 0
Size: 258930 Color: 1

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 461584 Color: 1
Size: 273298 Color: 1
Size: 265119 Color: 0

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 461605 Color: 1
Size: 278311 Color: 0
Size: 260085 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 461718 Color: 1
Size: 280148 Color: 1
Size: 258135 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 461893 Color: 1
Size: 281597 Color: 0
Size: 256511 Color: 0

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 461927 Color: 1
Size: 279708 Color: 1
Size: 258366 Color: 0

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 461985 Color: 1
Size: 285390 Color: 0
Size: 252626 Color: 0

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 461915 Color: 0
Size: 270674 Color: 0
Size: 267412 Color: 1

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 462167 Color: 1
Size: 278017 Color: 1
Size: 259817 Color: 0

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 462263 Color: 1
Size: 285501 Color: 0
Size: 252237 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 462381 Color: 0
Size: 270498 Color: 1
Size: 267122 Color: 1

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 462464 Color: 0
Size: 269326 Color: 0
Size: 268211 Color: 1

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 462705 Color: 1
Size: 278440 Color: 0
Size: 258856 Color: 0

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 462982 Color: 1
Size: 274711 Color: 0
Size: 262308 Color: 1

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 463121 Color: 1
Size: 279520 Color: 0
Size: 257360 Color: 1

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 463513 Color: 1
Size: 284054 Color: 0
Size: 252434 Color: 1

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 463672 Color: 0
Size: 275068 Color: 1
Size: 261261 Color: 1

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 463834 Color: 0
Size: 285993 Color: 1
Size: 250174 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 463892 Color: 0
Size: 282897 Color: 1
Size: 253212 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 463903 Color: 0
Size: 284364 Color: 1
Size: 251734 Color: 1

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 463906 Color: 0
Size: 273828 Color: 0
Size: 262267 Color: 1

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 464028 Color: 0
Size: 277957 Color: 1
Size: 258016 Color: 1

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 464180 Color: 0
Size: 281204 Color: 1
Size: 254617 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 464243 Color: 1
Size: 282611 Color: 0
Size: 253147 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 464389 Color: 0
Size: 269354 Color: 0
Size: 266258 Color: 1

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 464888 Color: 1
Size: 275048 Color: 1
Size: 260065 Color: 0

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 464975 Color: 0
Size: 269421 Color: 0
Size: 265605 Color: 1

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 465090 Color: 0
Size: 283960 Color: 1
Size: 250951 Color: 0

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 465212 Color: 1
Size: 280415 Color: 0
Size: 254374 Color: 1

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 465372 Color: 0
Size: 277726 Color: 1
Size: 256903 Color: 0

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 465383 Color: 0
Size: 277654 Color: 1
Size: 256964 Color: 1

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 465414 Color: 1
Size: 284487 Color: 0
Size: 250100 Color: 0

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 465543 Color: 0
Size: 281424 Color: 1
Size: 253034 Color: 1

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 465567 Color: 0
Size: 281357 Color: 1
Size: 253077 Color: 0

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 465816 Color: 1
Size: 273213 Color: 0
Size: 260972 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 466111 Color: 0
Size: 280392 Color: 1
Size: 253498 Color: 1

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 466207 Color: 1
Size: 269758 Color: 0
Size: 264036 Color: 1

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 466274 Color: 1
Size: 282608 Color: 0
Size: 251119 Color: 0

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 466319 Color: 1
Size: 283294 Color: 1
Size: 250388 Color: 0

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 466328 Color: 0
Size: 278159 Color: 0
Size: 255514 Color: 1

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 466324 Color: 1
Size: 273329 Color: 0
Size: 260348 Color: 1

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 466360 Color: 1
Size: 270284 Color: 0
Size: 263357 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 466447 Color: 1
Size: 282576 Color: 0
Size: 250978 Color: 0

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 466572 Color: 0
Size: 270384 Color: 1
Size: 263045 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 466476 Color: 1
Size: 277256 Color: 1
Size: 256269 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 466504 Color: 1
Size: 280439 Color: 0
Size: 253058 Color: 1

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 466634 Color: 0
Size: 270389 Color: 0
Size: 262978 Color: 1

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 466781 Color: 0
Size: 273262 Color: 1
Size: 259958 Color: 1

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 466865 Color: 0
Size: 280197 Color: 1
Size: 252939 Color: 1

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 466866 Color: 0
Size: 282106 Color: 1
Size: 251029 Color: 0

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 466878 Color: 1
Size: 272244 Color: 0
Size: 260879 Color: 1

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 466901 Color: 0
Size: 280724 Color: 1
Size: 252376 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 467180 Color: 0
Size: 270850 Color: 0
Size: 261971 Color: 1

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 467291 Color: 1
Size: 274694 Color: 1
Size: 258016 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 467583 Color: 1
Size: 279026 Color: 1
Size: 253392 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 467673 Color: 1
Size: 278230 Color: 0
Size: 254098 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 467947 Color: 1
Size: 279150 Color: 0
Size: 252904 Color: 0

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 468035 Color: 1
Size: 269299 Color: 1
Size: 262667 Color: 0

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 468125 Color: 0
Size: 276695 Color: 0
Size: 255181 Color: 1

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 468084 Color: 1
Size: 266050 Color: 1
Size: 265867 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 468267 Color: 1
Size: 272717 Color: 0
Size: 259017 Color: 0

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 468383 Color: 0
Size: 279516 Color: 1
Size: 252102 Color: 1

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 468501 Color: 0
Size: 273822 Color: 1
Size: 257678 Color: 0

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 468625 Color: 1
Size: 266320 Color: 1
Size: 265056 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 468832 Color: 0
Size: 276859 Color: 0
Size: 254310 Color: 1

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 468923 Color: 1
Size: 272999 Color: 0
Size: 258079 Color: 1

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 468978 Color: 0
Size: 271134 Color: 1
Size: 259889 Color: 0

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 469033 Color: 0
Size: 280218 Color: 1
Size: 250750 Color: 1

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 469200 Color: 0
Size: 268534 Color: 1
Size: 262267 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 469138 Color: 1
Size: 275975 Color: 0
Size: 254888 Color: 1

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 469229 Color: 0
Size: 273790 Color: 0
Size: 256982 Color: 1

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 469278 Color: 1
Size: 270772 Color: 0
Size: 259951 Color: 1

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 469359 Color: 1
Size: 273614 Color: 0
Size: 257028 Color: 0

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 469558 Color: 1
Size: 275992 Color: 0
Size: 254451 Color: 0

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 469621 Color: 1
Size: 278714 Color: 0
Size: 251666 Color: 1

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 469845 Color: 0
Size: 266341 Color: 1
Size: 263815 Color: 1

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 469867 Color: 0
Size: 270541 Color: 0
Size: 259593 Color: 1

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 469904 Color: 1
Size: 266831 Color: 0
Size: 263266 Color: 0

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 470317 Color: 1
Size: 265109 Color: 0
Size: 264575 Color: 0

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 470375 Color: 1
Size: 274867 Color: 0
Size: 254759 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 470473 Color: 1
Size: 265474 Color: 0
Size: 264054 Color: 1

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 470505 Color: 1
Size: 275431 Color: 0
Size: 254065 Color: 1

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 470856 Color: 0
Size: 267481 Color: 0
Size: 261664 Color: 1

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 471131 Color: 1
Size: 278100 Color: 0
Size: 250770 Color: 1

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 471216 Color: 1
Size: 274755 Color: 0
Size: 254030 Color: 1

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 471390 Color: 0
Size: 271527 Color: 0
Size: 257084 Color: 1

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 471445 Color: 0
Size: 276197 Color: 1
Size: 252359 Color: 1

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 471478 Color: 0
Size: 268224 Color: 1
Size: 260299 Color: 0

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 471507 Color: 0
Size: 274344 Color: 1
Size: 254150 Color: 0

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 471543 Color: 0
Size: 267083 Color: 1
Size: 261375 Color: 0

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 471655 Color: 0
Size: 267366 Color: 1
Size: 260980 Color: 0

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 471696 Color: 0
Size: 275166 Color: 1
Size: 253139 Color: 1

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 471776 Color: 0
Size: 277316 Color: 1
Size: 250909 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 471965 Color: 1
Size: 270867 Color: 0
Size: 257169 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 472064 Color: 0
Size: 272354 Color: 0
Size: 255583 Color: 1

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 472000 Color: 1
Size: 276157 Color: 1
Size: 251844 Color: 0

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 472209 Color: 0
Size: 277561 Color: 0
Size: 250231 Color: 1

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 472307 Color: 1
Size: 268030 Color: 1
Size: 259664 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 472385 Color: 0
Size: 274811 Color: 0
Size: 252805 Color: 1

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 472542 Color: 1
Size: 274801 Color: 1
Size: 252658 Color: 0

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 472548 Color: 0
Size: 274604 Color: 0
Size: 252849 Color: 1

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 472546 Color: 1
Size: 266784 Color: 1
Size: 260671 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 472564 Color: 1
Size: 271533 Color: 0
Size: 255904 Color: 1

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 472634 Color: 0
Size: 276881 Color: 0
Size: 250486 Color: 1

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 395995 Color: 1
Size: 312437 Color: 1
Size: 291569 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 472722 Color: 0
Size: 274330 Color: 1
Size: 252949 Color: 0

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 472718 Color: 1
Size: 270183 Color: 1
Size: 257100 Color: 0

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 472759 Color: 0
Size: 269710 Color: 1
Size: 257532 Color: 0

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 473025 Color: 0
Size: 275708 Color: 1
Size: 251268 Color: 1

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 473132 Color: 1
Size: 271085 Color: 0
Size: 255784 Color: 1

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 473241 Color: 1
Size: 271632 Color: 0
Size: 255128 Color: 0

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 473337 Color: 1
Size: 270963 Color: 1
Size: 255701 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 473371 Color: 1
Size: 276180 Color: 0
Size: 250450 Color: 1

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 473564 Color: 1
Size: 264234 Color: 1
Size: 262203 Color: 0

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 473820 Color: 0
Size: 264460 Color: 1
Size: 261721 Color: 0

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 474063 Color: 1
Size: 275004 Color: 0
Size: 250934 Color: 1

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 384784 Color: 0
Size: 355822 Color: 1
Size: 259395 Color: 0

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 474216 Color: 0
Size: 271890 Color: 1
Size: 253895 Color: 1

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 474381 Color: 1
Size: 267942 Color: 0
Size: 257678 Color: 1

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 474373 Color: 0
Size: 272494 Color: 0
Size: 253134 Color: 1

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 363369 Color: 1
Size: 334147 Color: 1
Size: 302485 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 474531 Color: 0
Size: 270087 Color: 0
Size: 255383 Color: 1

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 474647 Color: 1
Size: 268752 Color: 0
Size: 256602 Color: 0

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 474858 Color: 0
Size: 271103 Color: 1
Size: 254040 Color: 1

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 475162 Color: 1
Size: 264619 Color: 0
Size: 260220 Color: 1

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 475291 Color: 0
Size: 273911 Color: 1
Size: 250799 Color: 1

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 475488 Color: 0
Size: 268914 Color: 1
Size: 255599 Color: 0

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 475649 Color: 0
Size: 265842 Color: 1
Size: 258510 Color: 0

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 475648 Color: 1
Size: 272799 Color: 0
Size: 251554 Color: 1

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 475740 Color: 1
Size: 273738 Color: 0
Size: 250523 Color: 0

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 475796 Color: 1
Size: 264167 Color: 1
Size: 260038 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 475833 Color: 1
Size: 265423 Color: 0
Size: 258745 Color: 1

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 475986 Color: 0
Size: 268901 Color: 0
Size: 255114 Color: 1

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 476106 Color: 0
Size: 267457 Color: 1
Size: 256438 Color: 1

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 476336 Color: 1
Size: 271091 Color: 1
Size: 252574 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 476527 Color: 0
Size: 263870 Color: 1
Size: 259604 Color: 1

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 476575 Color: 0
Size: 264976 Color: 1
Size: 258450 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 476598 Color: 1
Size: 268223 Color: 1
Size: 255180 Color: 0

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 476696 Color: 0
Size: 269417 Color: 1
Size: 253888 Color: 1

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 476701 Color: 0
Size: 272719 Color: 0
Size: 250581 Color: 1

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 476857 Color: 1
Size: 267685 Color: 0
Size: 255459 Color: 1

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 476931 Color: 0
Size: 270574 Color: 1
Size: 252496 Color: 0

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 476994 Color: 0
Size: 270935 Color: 1
Size: 252072 Color: 1

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 477046 Color: 1
Size: 272167 Color: 0
Size: 250788 Color: 1

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 477223 Color: 0
Size: 269340 Color: 1
Size: 253438 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 477210 Color: 1
Size: 266946 Color: 1
Size: 255845 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 477445 Color: 1
Size: 268814 Color: 0
Size: 253742 Color: 0

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 477777 Color: 0
Size: 261839 Color: 1
Size: 260385 Color: 0

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 477791 Color: 1
Size: 264938 Color: 1
Size: 257272 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 477874 Color: 1
Size: 261147 Color: 0
Size: 260980 Color: 0

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 477889 Color: 0
Size: 261513 Color: 1
Size: 260599 Color: 0

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 478508 Color: 1
Size: 263145 Color: 0
Size: 258348 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 478591 Color: 0
Size: 263639 Color: 1
Size: 257771 Color: 1

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 478609 Color: 0
Size: 266611 Color: 1
Size: 254781 Color: 0

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 479131 Color: 1
Size: 269324 Color: 1
Size: 251546 Color: 0

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 479249 Color: 0
Size: 264160 Color: 1
Size: 256592 Color: 0

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 479201 Color: 1
Size: 270001 Color: 0
Size: 250799 Color: 0

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 479524 Color: 0
Size: 265947 Color: 1
Size: 254530 Color: 1

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 479654 Color: 1
Size: 268080 Color: 0
Size: 252267 Color: 1

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 479680 Color: 1
Size: 262224 Color: 0
Size: 258097 Color: 1

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 479781 Color: 0
Size: 269221 Color: 1
Size: 250999 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 479958 Color: 0
Size: 263477 Color: 1
Size: 256566 Color: 1

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 480081 Color: 0
Size: 267034 Color: 1
Size: 252886 Color: 1

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 480332 Color: 1
Size: 263213 Color: 1
Size: 256456 Color: 0

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 480384 Color: 1
Size: 263463 Color: 0
Size: 256154 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 480535 Color: 1
Size: 261982 Color: 0
Size: 257484 Color: 0

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 480891 Color: 1
Size: 268468 Color: 0
Size: 250642 Color: 1

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 481083 Color: 0
Size: 264093 Color: 1
Size: 254825 Color: 0

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 481373 Color: 1
Size: 264895 Color: 0
Size: 253733 Color: 1

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 481613 Color: 0
Size: 264238 Color: 1
Size: 254150 Color: 1

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 481719 Color: 0
Size: 268142 Color: 0
Size: 250140 Color: 1

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 481781 Color: 1
Size: 267908 Color: 0
Size: 250312 Color: 1

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 481932 Color: 0
Size: 266692 Color: 0
Size: 251377 Color: 1

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 481998 Color: 1
Size: 267289 Color: 1
Size: 250714 Color: 0

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 482063 Color: 0
Size: 260529 Color: 0
Size: 257409 Color: 1

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 482198 Color: 1
Size: 260884 Color: 0
Size: 256919 Color: 1

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 482313 Color: 1
Size: 267177 Color: 1
Size: 250511 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 482630 Color: 1
Size: 259923 Color: 0
Size: 257448 Color: 0

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 482948 Color: 1
Size: 259474 Color: 0
Size: 257579 Color: 0

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 483104 Color: 0
Size: 265055 Color: 1
Size: 251842 Color: 1

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 483689 Color: 1
Size: 260495 Color: 0
Size: 255817 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 483781 Color: 1
Size: 263442 Color: 0
Size: 252778 Color: 1

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 483855 Color: 1
Size: 259009 Color: 0
Size: 257137 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 484217 Color: 0
Size: 260146 Color: 1
Size: 255638 Color: 0

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 484446 Color: 0
Size: 258743 Color: 1
Size: 256812 Color: 1

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 484563 Color: 0
Size: 261194 Color: 0
Size: 254244 Color: 1

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 484525 Color: 1
Size: 257830 Color: 0
Size: 257646 Color: 1

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 484698 Color: 0
Size: 262168 Color: 0
Size: 253135 Color: 1

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 484761 Color: 0
Size: 257916 Color: 1
Size: 257324 Color: 0

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 484835 Color: 0
Size: 264644 Color: 1
Size: 250522 Color: 0

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 484878 Color: 0
Size: 259374 Color: 1
Size: 255749 Color: 0

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 484944 Color: 1
Size: 259367 Color: 0
Size: 255690 Color: 1

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 485052 Color: 1
Size: 257787 Color: 0
Size: 257162 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 485113 Color: 0
Size: 259606 Color: 1
Size: 255282 Color: 1

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 485624 Color: 1
Size: 258070 Color: 0
Size: 256307 Color: 1

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 485733 Color: 1
Size: 259174 Color: 0
Size: 255094 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 485954 Color: 1
Size: 258582 Color: 1
Size: 255465 Color: 0

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 399083 Color: 0
Size: 341010 Color: 1
Size: 259908 Color: 0

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 486138 Color: 0
Size: 263433 Color: 1
Size: 250430 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 486172 Color: 1
Size: 259938 Color: 1
Size: 253891 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 486238 Color: 0
Size: 262578 Color: 1
Size: 251185 Color: 0

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 486230 Color: 1
Size: 257993 Color: 0
Size: 255778 Color: 1

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 486297 Color: 1
Size: 263127 Color: 1
Size: 250577 Color: 0

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 486313 Color: 0
Size: 260530 Color: 0
Size: 253158 Color: 1

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 486360 Color: 0
Size: 260896 Color: 1
Size: 252745 Color: 0

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 486414 Color: 1
Size: 262080 Color: 0
Size: 251507 Color: 1

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 486499 Color: 1
Size: 261762 Color: 1
Size: 251740 Color: 0

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 486524 Color: 0
Size: 258563 Color: 0
Size: 254914 Color: 1

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 486942 Color: 1
Size: 256766 Color: 0
Size: 256293 Color: 0

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 487025 Color: 1
Size: 262859 Color: 0
Size: 250117 Color: 0

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 487054 Color: 1
Size: 256910 Color: 1
Size: 256037 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 487073 Color: 0
Size: 257748 Color: 1
Size: 255180 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 487095 Color: 1
Size: 262709 Color: 0
Size: 250197 Color: 1

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 487136 Color: 1
Size: 260964 Color: 1
Size: 251901 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 487332 Color: 1
Size: 262523 Color: 0
Size: 250146 Color: 1

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 487349 Color: 1
Size: 258291 Color: 0
Size: 254361 Color: 1

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 487448 Color: 1
Size: 260133 Color: 1
Size: 252420 Color: 0

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 487616 Color: 1
Size: 259612 Color: 0
Size: 252773 Color: 1

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 487549 Color: 0
Size: 260525 Color: 1
Size: 251927 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 487706 Color: 1
Size: 257724 Color: 0
Size: 254571 Color: 0

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 487774 Color: 0
Size: 257875 Color: 1
Size: 254352 Color: 1

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 487868 Color: 1
Size: 260350 Color: 0
Size: 251783 Color: 1

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 487905 Color: 0
Size: 256672 Color: 0
Size: 255424 Color: 1

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 488066 Color: 1
Size: 259019 Color: 0
Size: 252916 Color: 1

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 488320 Color: 1
Size: 258646 Color: 0
Size: 253035 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 488356 Color: 0
Size: 260648 Color: 1
Size: 250997 Color: 1

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 488411 Color: 0
Size: 259970 Color: 0
Size: 251620 Color: 1

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 488570 Color: 1
Size: 257093 Color: 1
Size: 254338 Color: 0

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 488866 Color: 1
Size: 259555 Color: 0
Size: 251580 Color: 1

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 488855 Color: 0
Size: 255995 Color: 1
Size: 255151 Color: 0

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 489210 Color: 1
Size: 257851 Color: 0
Size: 252940 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 489608 Color: 0
Size: 259651 Color: 1
Size: 250742 Color: 1

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 489837 Color: 1
Size: 259624 Color: 0
Size: 250540 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 489856 Color: 1
Size: 255898 Color: 0
Size: 254247 Color: 1

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 489898 Color: 0
Size: 257487 Color: 1
Size: 252616 Color: 1

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 490034 Color: 1
Size: 258846 Color: 0
Size: 251121 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 490417 Color: 0
Size: 255212 Color: 1
Size: 254372 Color: 1

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 490448 Color: 0
Size: 257009 Color: 1
Size: 252544 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 490750 Color: 0
Size: 258659 Color: 0
Size: 250592 Color: 1

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 490775 Color: 1
Size: 258858 Color: 1
Size: 250368 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 490809 Color: 0
Size: 257029 Color: 1
Size: 252163 Color: 0

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 490842 Color: 0
Size: 258142 Color: 0
Size: 251017 Color: 1

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 490991 Color: 1
Size: 258066 Color: 0
Size: 250944 Color: 1

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 491001 Color: 0
Size: 255992 Color: 0
Size: 253008 Color: 1

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 491098 Color: 0
Size: 256513 Color: 1
Size: 252390 Color: 0

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 491113 Color: 0
Size: 258678 Color: 1
Size: 250210 Color: 0

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 491319 Color: 0
Size: 256330 Color: 1
Size: 252352 Color: 1

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 491333 Color: 0
Size: 255598 Color: 0
Size: 253070 Color: 1

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 491564 Color: 0
Size: 257459 Color: 1
Size: 250978 Color: 1

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 492211 Color: 0
Size: 257111 Color: 1
Size: 250679 Color: 0

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 492201 Color: 1
Size: 254763 Color: 1
Size: 253037 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 492409 Color: 1
Size: 256789 Color: 0
Size: 250803 Color: 1

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 492612 Color: 0
Size: 254634 Color: 1
Size: 252755 Color: 1

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 492614 Color: 0
Size: 256412 Color: 1
Size: 250975 Color: 0

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 492655 Color: 0
Size: 256447 Color: 0
Size: 250899 Color: 1

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 492766 Color: 1
Size: 254805 Color: 0
Size: 252430 Color: 1

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 492704 Color: 0
Size: 254810 Color: 1
Size: 252487 Color: 0

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 493146 Color: 0
Size: 253805 Color: 1
Size: 253050 Color: 1

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 493260 Color: 1
Size: 256108 Color: 0
Size: 250633 Color: 0

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 493265 Color: 1
Size: 256098 Color: 0
Size: 250638 Color: 1

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 448118 Color: 1
Size: 300788 Color: 1
Size: 251095 Color: 0

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 493448 Color: 1
Size: 254487 Color: 0
Size: 252066 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 493517 Color: 1
Size: 255274 Color: 0
Size: 251210 Color: 1

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 493590 Color: 0
Size: 253916 Color: 1
Size: 252495 Color: 1

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 493619 Color: 0
Size: 255818 Color: 1
Size: 250564 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 493719 Color: 0
Size: 255886 Color: 0
Size: 250396 Color: 1

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 493842 Color: 0
Size: 255650 Color: 0
Size: 250509 Color: 1

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 494017 Color: 1
Size: 255961 Color: 1
Size: 250023 Color: 0

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 494210 Color: 1
Size: 254676 Color: 1
Size: 251115 Color: 0

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 494355 Color: 1
Size: 255469 Color: 0
Size: 250177 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 494409 Color: 0
Size: 254402 Color: 1
Size: 251190 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 495036 Color: 1
Size: 254771 Color: 0
Size: 250194 Color: 0

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 495056 Color: 0
Size: 253981 Color: 0
Size: 250964 Color: 1

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 495300 Color: 1
Size: 253078 Color: 0
Size: 251623 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 495311 Color: 0
Size: 253001 Color: 1
Size: 251689 Color: 1

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 495436 Color: 1
Size: 253743 Color: 1
Size: 250822 Color: 0

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 495702 Color: 1
Size: 253280 Color: 0
Size: 251019 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 495895 Color: 1
Size: 253799 Color: 0
Size: 250307 Color: 1

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 423901 Color: 1
Size: 291271 Color: 0
Size: 284829 Color: 0

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 496352 Color: 1
Size: 253460 Color: 0
Size: 250189 Color: 0

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 496279 Color: 0
Size: 253052 Color: 1
Size: 250670 Color: 0

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 496560 Color: 0
Size: 252642 Color: 1
Size: 250799 Color: 1

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 398117 Color: 0
Size: 349225 Color: 1
Size: 252659 Color: 1

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 496928 Color: 1
Size: 252959 Color: 0
Size: 250114 Color: 1

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 496903 Color: 0
Size: 252789 Color: 0
Size: 250309 Color: 1

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 497106 Color: 1
Size: 251689 Color: 1
Size: 251206 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 497121 Color: 1
Size: 252707 Color: 0
Size: 250173 Color: 0

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 497418 Color: 1
Size: 251527 Color: 0
Size: 251056 Color: 1

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 497978 Color: 1
Size: 251709 Color: 0
Size: 250314 Color: 1

Bin 2870: 1 of cap free
Amount of items: 3
Items: 
Size: 416370 Color: 0
Size: 306167 Color: 1
Size: 277463 Color: 0

Bin 2871: 1 of cap free
Amount of items: 3
Items: 
Size: 395081 Color: 1
Size: 332294 Color: 0
Size: 272625 Color: 0

Bin 2872: 1 of cap free
Amount of items: 3
Items: 
Size: 389358 Color: 0
Size: 343275 Color: 1
Size: 267367 Color: 1

Bin 2873: 1 of cap free
Amount of items: 3
Items: 
Size: 497360 Color: 0
Size: 252494 Color: 1
Size: 250146 Color: 0

Bin 2874: 1 of cap free
Amount of items: 3
Items: 
Size: 363894 Color: 1
Size: 336363 Color: 0
Size: 299743 Color: 1

Bin 2875: 1 of cap free
Amount of items: 3
Items: 
Size: 403918 Color: 1
Size: 332941 Color: 1
Size: 263141 Color: 0

Bin 2876: 1 of cap free
Amount of items: 3
Items: 
Size: 474615 Color: 1
Size: 264459 Color: 0
Size: 260926 Color: 0

Bin 2877: 1 of cap free
Amount of items: 3
Items: 
Size: 356178 Color: 0
Size: 331628 Color: 1
Size: 312194 Color: 0

Bin 2878: 1 of cap free
Amount of items: 3
Items: 
Size: 339811 Color: 1
Size: 338309 Color: 1
Size: 321880 Color: 0

Bin 2879: 1 of cap free
Amount of items: 3
Items: 
Size: 487031 Color: 1
Size: 258541 Color: 0
Size: 254428 Color: 0

Bin 2880: 1 of cap free
Amount of items: 3
Items: 
Size: 346021 Color: 0
Size: 341453 Color: 1
Size: 312526 Color: 1

Bin 2881: 1 of cap free
Amount of items: 3
Items: 
Size: 473361 Color: 1
Size: 266332 Color: 0
Size: 260307 Color: 0

Bin 2882: 1 of cap free
Amount of items: 3
Items: 
Size: 419674 Color: 0
Size: 302533 Color: 0
Size: 277793 Color: 1

Bin 2883: 1 of cap free
Amount of items: 3
Items: 
Size: 368513 Color: 0
Size: 349039 Color: 1
Size: 282448 Color: 0

Bin 2884: 1 of cap free
Amount of items: 3
Items: 
Size: 373820 Color: 1
Size: 335466 Color: 0
Size: 290714 Color: 0

Bin 2885: 1 of cap free
Amount of items: 3
Items: 
Size: 473543 Color: 1
Size: 267183 Color: 0
Size: 259274 Color: 0

Bin 2886: 1 of cap free
Amount of items: 3
Items: 
Size: 425803 Color: 0
Size: 317849 Color: 1
Size: 256348 Color: 0

Bin 2887: 1 of cap free
Amount of items: 3
Items: 
Size: 413390 Color: 1
Size: 311123 Color: 0
Size: 275487 Color: 0

Bin 2888: 1 of cap free
Amount of items: 3
Items: 
Size: 351924 Color: 1
Size: 346329 Color: 0
Size: 301747 Color: 0

Bin 2889: 1 of cap free
Amount of items: 3
Items: 
Size: 363811 Color: 1
Size: 352000 Color: 1
Size: 284189 Color: 0

Bin 2890: 1 of cap free
Amount of items: 3
Items: 
Size: 380326 Color: 0
Size: 350326 Color: 1
Size: 269348 Color: 0

Bin 2891: 1 of cap free
Amount of items: 3
Items: 
Size: 389120 Color: 1
Size: 349278 Color: 1
Size: 261602 Color: 0

Bin 2892: 1 of cap free
Amount of items: 3
Items: 
Size: 350377 Color: 1
Size: 326448 Color: 0
Size: 323175 Color: 1

Bin 2893: 1 of cap free
Amount of items: 3
Items: 
Size: 357945 Color: 0
Size: 322794 Color: 1
Size: 319261 Color: 0

Bin 2894: 1 of cap free
Amount of items: 3
Items: 
Size: 455920 Color: 1
Size: 273884 Color: 1
Size: 270196 Color: 0

Bin 2895: 1 of cap free
Amount of items: 3
Items: 
Size: 398835 Color: 0
Size: 309187 Color: 1
Size: 291978 Color: 0

Bin 2896: 1 of cap free
Amount of items: 3
Items: 
Size: 378049 Color: 1
Size: 350683 Color: 1
Size: 271268 Color: 0

Bin 2897: 1 of cap free
Amount of items: 3
Items: 
Size: 359004 Color: 0
Size: 331506 Color: 1
Size: 309490 Color: 0

Bin 2898: 1 of cap free
Amount of items: 3
Items: 
Size: 406581 Color: 1
Size: 339428 Color: 0
Size: 253991 Color: 0

Bin 2899: 1 of cap free
Amount of items: 3
Items: 
Size: 386699 Color: 0
Size: 357547 Color: 1
Size: 255754 Color: 0

Bin 2900: 1 of cap free
Amount of items: 3
Items: 
Size: 475121 Color: 0
Size: 263861 Color: 1
Size: 261018 Color: 1

Bin 2901: 1 of cap free
Amount of items: 3
Items: 
Size: 466787 Color: 0
Size: 271597 Color: 0
Size: 261616 Color: 1

Bin 2902: 1 of cap free
Amount of items: 3
Items: 
Size: 447556 Color: 1
Size: 294698 Color: 0
Size: 257746 Color: 0

Bin 2903: 1 of cap free
Amount of items: 3
Items: 
Size: 359756 Color: 1
Size: 353437 Color: 1
Size: 286807 Color: 0

Bin 2904: 1 of cap free
Amount of items: 3
Items: 
Size: 469890 Color: 1
Size: 276486 Color: 0
Size: 253624 Color: 0

Bin 2905: 1 of cap free
Amount of items: 3
Items: 
Size: 390355 Color: 1
Size: 334626 Color: 1
Size: 275019 Color: 0

Bin 2906: 1 of cap free
Amount of items: 3
Items: 
Size: 361208 Color: 1
Size: 357148 Color: 0
Size: 281644 Color: 0

Bin 2907: 1 of cap free
Amount of items: 3
Items: 
Size: 357913 Color: 0
Size: 325370 Color: 1
Size: 316717 Color: 0

Bin 2908: 1 of cap free
Amount of items: 3
Items: 
Size: 343592 Color: 0
Size: 339965 Color: 0
Size: 316443 Color: 1

Bin 2909: 1 of cap free
Amount of items: 3
Items: 
Size: 498066 Color: 1
Size: 251341 Color: 0
Size: 250593 Color: 0

Bin 2910: 1 of cap free
Amount of items: 3
Items: 
Size: 339997 Color: 1
Size: 336065 Color: 0
Size: 323938 Color: 1

Bin 2911: 1 of cap free
Amount of items: 3
Items: 
Size: 345188 Color: 1
Size: 332125 Color: 1
Size: 322687 Color: 0

Bin 2912: 1 of cap free
Amount of items: 3
Items: 
Size: 358914 Color: 1
Size: 347161 Color: 1
Size: 293925 Color: 0

Bin 2913: 1 of cap free
Amount of items: 3
Items: 
Size: 387296 Color: 0
Size: 349840 Color: 1
Size: 262864 Color: 1

Bin 2914: 1 of cap free
Amount of items: 3
Items: 
Size: 391056 Color: 1
Size: 347036 Color: 0
Size: 261908 Color: 0

Bin 2915: 1 of cap free
Amount of items: 3
Items: 
Size: 361581 Color: 0
Size: 346852 Color: 1
Size: 291567 Color: 0

Bin 2916: 1 of cap free
Amount of items: 3
Items: 
Size: 353811 Color: 0
Size: 344431 Color: 1
Size: 301758 Color: 0

Bin 2917: 1 of cap free
Amount of items: 3
Items: 
Size: 362504 Color: 1
Size: 329287 Color: 0
Size: 308209 Color: 1

Bin 2918: 1 of cap free
Amount of items: 3
Items: 
Size: 419179 Color: 0
Size: 300263 Color: 1
Size: 280558 Color: 1

Bin 2919: 1 of cap free
Amount of items: 3
Items: 
Size: 334336 Color: 1
Size: 334292 Color: 0
Size: 331372 Color: 0

Bin 2920: 1 of cap free
Amount of items: 3
Items: 
Size: 415452 Color: 0
Size: 315915 Color: 0
Size: 268633 Color: 1

Bin 2921: 1 of cap free
Amount of items: 3
Items: 
Size: 460918 Color: 1
Size: 271324 Color: 0
Size: 267758 Color: 1

Bin 2922: 1 of cap free
Amount of items: 3
Items: 
Size: 373926 Color: 0
Size: 371379 Color: 0
Size: 254695 Color: 1

Bin 2923: 1 of cap free
Amount of items: 3
Items: 
Size: 358655 Color: 1
Size: 340175 Color: 0
Size: 301170 Color: 1

Bin 2924: 1 of cap free
Amount of items: 3
Items: 
Size: 364150 Color: 1
Size: 335572 Color: 0
Size: 300278 Color: 0

Bin 2925: 1 of cap free
Amount of items: 3
Items: 
Size: 416828 Color: 0
Size: 316096 Color: 0
Size: 267076 Color: 1

Bin 2926: 1 of cap free
Amount of items: 3
Items: 
Size: 360142 Color: 1
Size: 350496 Color: 0
Size: 289362 Color: 0

Bin 2927: 1 of cap free
Amount of items: 3
Items: 
Size: 368689 Color: 1
Size: 328236 Color: 1
Size: 303075 Color: 0

Bin 2928: 1 of cap free
Amount of items: 3
Items: 
Size: 363321 Color: 1
Size: 351338 Color: 0
Size: 285341 Color: 0

Bin 2929: 1 of cap free
Amount of items: 3
Items: 
Size: 458776 Color: 0
Size: 289765 Color: 1
Size: 251459 Color: 0

Bin 2930: 1 of cap free
Amount of items: 3
Items: 
Size: 357201 Color: 1
Size: 345712 Color: 0
Size: 297087 Color: 0

Bin 2931: 1 of cap free
Amount of items: 3
Items: 
Size: 335217 Color: 1
Size: 332560 Color: 0
Size: 332223 Color: 1

Bin 2932: 1 of cap free
Amount of items: 3
Items: 
Size: 351260 Color: 0
Size: 336271 Color: 1
Size: 312469 Color: 0

Bin 2933: 1 of cap free
Amount of items: 3
Items: 
Size: 353107 Color: 1
Size: 326976 Color: 0
Size: 319917 Color: 1

Bin 2934: 1 of cap free
Amount of items: 3
Items: 
Size: 367507 Color: 0
Size: 355356 Color: 0
Size: 277137 Color: 1

Bin 2935: 1 of cap free
Amount of items: 3
Items: 
Size: 356363 Color: 1
Size: 322781 Color: 1
Size: 320856 Color: 0

Bin 2936: 1 of cap free
Amount of items: 3
Items: 
Size: 357070 Color: 0
Size: 353724 Color: 0
Size: 289206 Color: 1

Bin 2937: 1 of cap free
Amount of items: 3
Items: 
Size: 357419 Color: 0
Size: 326489 Color: 1
Size: 316092 Color: 0

Bin 2938: 1 of cap free
Amount of items: 3
Items: 
Size: 357864 Color: 0
Size: 322261 Color: 1
Size: 319875 Color: 0

Bin 2939: 1 of cap free
Amount of items: 3
Items: 
Size: 357806 Color: 1
Size: 337135 Color: 0
Size: 305059 Color: 1

Bin 2940: 1 of cap free
Amount of items: 3
Items: 
Size: 358342 Color: 0
Size: 356447 Color: 0
Size: 285211 Color: 1

Bin 2941: 1 of cap free
Amount of items: 3
Items: 
Size: 358268 Color: 1
Size: 351783 Color: 1
Size: 289949 Color: 0

Bin 2942: 1 of cap free
Amount of items: 3
Items: 
Size: 360348 Color: 0
Size: 332783 Color: 0
Size: 306869 Color: 1

Bin 2943: 1 of cap free
Amount of items: 3
Items: 
Size: 360794 Color: 1
Size: 325285 Color: 1
Size: 313921 Color: 0

Bin 2944: 1 of cap free
Amount of items: 3
Items: 
Size: 360702 Color: 0
Size: 353623 Color: 0
Size: 285675 Color: 1

Bin 2945: 1 of cap free
Amount of items: 3
Items: 
Size: 361407 Color: 0
Size: 329562 Color: 1
Size: 309031 Color: 0

Bin 2946: 1 of cap free
Amount of items: 3
Items: 
Size: 363234 Color: 1
Size: 360857 Color: 0
Size: 275909 Color: 0

Bin 2947: 1 of cap free
Amount of items: 3
Items: 
Size: 364152 Color: 1
Size: 352872 Color: 0
Size: 282976 Color: 1

Bin 2948: 1 of cap free
Amount of items: 3
Items: 
Size: 364332 Color: 1
Size: 321436 Color: 1
Size: 314232 Color: 0

Bin 2949: 1 of cap free
Amount of items: 3
Items: 
Size: 390587 Color: 0
Size: 345478 Color: 1
Size: 263935 Color: 0

Bin 2950: 1 of cap free
Amount of items: 3
Items: 
Size: 365829 Color: 0
Size: 345575 Color: 0
Size: 288596 Color: 1

Bin 2951: 1 of cap free
Amount of items: 3
Items: 
Size: 367300 Color: 0
Size: 335357 Color: 0
Size: 297343 Color: 1

Bin 2952: 1 of cap free
Amount of items: 3
Items: 
Size: 364139 Color: 0
Size: 346902 Color: 0
Size: 288959 Color: 1

Bin 2953: 1 of cap free
Amount of items: 3
Items: 
Size: 351639 Color: 1
Size: 345907 Color: 0
Size: 302454 Color: 0

Bin 2954: 1 of cap free
Amount of items: 3
Items: 
Size: 353795 Color: 0
Size: 341788 Color: 0
Size: 304417 Color: 1

Bin 2955: 1 of cap free
Amount of items: 3
Items: 
Size: 367293 Color: 0
Size: 341109 Color: 1
Size: 291598 Color: 1

Bin 2956: 1 of cap free
Amount of items: 3
Items: 
Size: 427053 Color: 0
Size: 295289 Color: 1
Size: 277658 Color: 0

Bin 2957: 1 of cap free
Amount of items: 3
Items: 
Size: 342300 Color: 1
Size: 331250 Color: 0
Size: 326450 Color: 0

Bin 2958: 1 of cap free
Amount of items: 3
Items: 
Size: 434791 Color: 1
Size: 288393 Color: 0
Size: 276816 Color: 1

Bin 2959: 1 of cap free
Amount of items: 3
Items: 
Size: 351102 Color: 0
Size: 326552 Color: 1
Size: 322346 Color: 0

Bin 2960: 1 of cap free
Amount of items: 3
Items: 
Size: 350197 Color: 1
Size: 332850 Color: 1
Size: 316953 Color: 0

Bin 2961: 1 of cap free
Amount of items: 3
Items: 
Size: 339934 Color: 1
Size: 331766 Color: 0
Size: 328300 Color: 1

Bin 2962: 1 of cap free
Amount of items: 3
Items: 
Size: 497977 Color: 0
Size: 251323 Color: 1
Size: 250700 Color: 1

Bin 2963: 1 of cap free
Amount of items: 3
Items: 
Size: 390168 Color: 1
Size: 338035 Color: 1
Size: 271797 Color: 0

Bin 2964: 1 of cap free
Amount of items: 3
Items: 
Size: 366768 Color: 1
Size: 356934 Color: 0
Size: 276298 Color: 0

Bin 2965: 1 of cap free
Amount of items: 3
Items: 
Size: 345118 Color: 1
Size: 334636 Color: 1
Size: 320246 Color: 0

Bin 2966: 1 of cap free
Amount of items: 3
Items: 
Size: 352111 Color: 1
Size: 327496 Color: 1
Size: 320393 Color: 0

Bin 2967: 1 of cap free
Amount of items: 3
Items: 
Size: 443672 Color: 1
Size: 284237 Color: 0
Size: 272091 Color: 1

Bin 2968: 1 of cap free
Amount of items: 3
Items: 
Size: 397976 Color: 1
Size: 349107 Color: 0
Size: 252917 Color: 1

Bin 2969: 1 of cap free
Amount of items: 3
Items: 
Size: 340506 Color: 0
Size: 337603 Color: 0
Size: 321891 Color: 1

Bin 2970: 1 of cap free
Amount of items: 3
Items: 
Size: 368025 Color: 0
Size: 336905 Color: 1
Size: 295070 Color: 0

Bin 2971: 1 of cap free
Amount of items: 3
Items: 
Size: 496994 Color: 1
Size: 251775 Color: 0
Size: 251231 Color: 1

Bin 2972: 2 of cap free
Amount of items: 3
Items: 
Size: 388286 Color: 1
Size: 342682 Color: 0
Size: 269031 Color: 1

Bin 2973: 2 of cap free
Amount of items: 3
Items: 
Size: 393363 Color: 1
Size: 313212 Color: 0
Size: 293424 Color: 1

Bin 2974: 2 of cap free
Amount of items: 3
Items: 
Size: 350374 Color: 0
Size: 341136 Color: 0
Size: 308489 Color: 1

Bin 2975: 2 of cap free
Amount of items: 3
Items: 
Size: 371839 Color: 0
Size: 344924 Color: 0
Size: 283236 Color: 1

Bin 2976: 2 of cap free
Amount of items: 3
Items: 
Size: 436106 Color: 1
Size: 282946 Color: 0
Size: 280947 Color: 1

Bin 2977: 2 of cap free
Amount of items: 3
Items: 
Size: 473546 Color: 1
Size: 263855 Color: 0
Size: 262598 Color: 0

Bin 2978: 2 of cap free
Amount of items: 3
Items: 
Size: 377931 Color: 1
Size: 311747 Color: 0
Size: 310321 Color: 1

Bin 2979: 2 of cap free
Amount of items: 3
Items: 
Size: 353470 Color: 1
Size: 343596 Color: 0
Size: 302933 Color: 0

Bin 2980: 2 of cap free
Amount of items: 3
Items: 
Size: 491114 Color: 1
Size: 256323 Color: 1
Size: 252562 Color: 0

Bin 2981: 2 of cap free
Amount of items: 3
Items: 
Size: 496855 Color: 0
Size: 251952 Color: 1
Size: 251192 Color: 0

Bin 2982: 2 of cap free
Amount of items: 3
Items: 
Size: 453135 Color: 0
Size: 281631 Color: 0
Size: 265233 Color: 1

Bin 2983: 2 of cap free
Amount of items: 3
Items: 
Size: 428277 Color: 1
Size: 300766 Color: 1
Size: 270956 Color: 0

Bin 2984: 2 of cap free
Amount of items: 3
Items: 
Size: 485508 Color: 1
Size: 260858 Color: 0
Size: 253633 Color: 1

Bin 2985: 2 of cap free
Amount of items: 3
Items: 
Size: 374665 Color: 0
Size: 361008 Color: 1
Size: 264326 Color: 1

Bin 2986: 2 of cap free
Amount of items: 3
Items: 
Size: 362036 Color: 0
Size: 354606 Color: 0
Size: 283357 Color: 1

Bin 2987: 2 of cap free
Amount of items: 3
Items: 
Size: 358643 Color: 1
Size: 331469 Color: 1
Size: 309887 Color: 0

Bin 2988: 2 of cap free
Amount of items: 3
Items: 
Size: 359935 Color: 0
Size: 356078 Color: 1
Size: 283986 Color: 0

Bin 2989: 2 of cap free
Amount of items: 3
Items: 
Size: 412449 Color: 1
Size: 320791 Color: 0
Size: 266759 Color: 1

Bin 2990: 2 of cap free
Amount of items: 3
Items: 
Size: 396066 Color: 1
Size: 349305 Color: 0
Size: 254628 Color: 0

Bin 2991: 2 of cap free
Amount of items: 3
Items: 
Size: 358031 Color: 0
Size: 348937 Color: 0
Size: 293031 Color: 1

Bin 2992: 2 of cap free
Amount of items: 3
Items: 
Size: 365089 Color: 0
Size: 319894 Color: 0
Size: 315016 Color: 1

Bin 2993: 2 of cap free
Amount of items: 3
Items: 
Size: 402439 Color: 0
Size: 337916 Color: 1
Size: 259644 Color: 0

Bin 2994: 2 of cap free
Amount of items: 3
Items: 
Size: 356088 Color: 0
Size: 351588 Color: 1
Size: 292323 Color: 1

Bin 2995: 2 of cap free
Amount of items: 3
Items: 
Size: 375336 Color: 0
Size: 354793 Color: 1
Size: 269870 Color: 0

Bin 2996: 2 of cap free
Amount of items: 3
Items: 
Size: 486199 Color: 0
Size: 259870 Color: 1
Size: 253930 Color: 0

Bin 2997: 2 of cap free
Amount of items: 3
Items: 
Size: 355968 Color: 0
Size: 326587 Color: 0
Size: 317444 Color: 1

Bin 2998: 2 of cap free
Amount of items: 3
Items: 
Size: 438826 Color: 1
Size: 288143 Color: 1
Size: 273030 Color: 0

Bin 2999: 2 of cap free
Amount of items: 3
Items: 
Size: 345488 Color: 1
Size: 341216 Color: 0
Size: 313295 Color: 0

Bin 3000: 2 of cap free
Amount of items: 3
Items: 
Size: 346711 Color: 1
Size: 334063 Color: 1
Size: 319225 Color: 0

Bin 3001: 2 of cap free
Amount of items: 3
Items: 
Size: 470663 Color: 0
Size: 266093 Color: 1
Size: 263243 Color: 0

Bin 3002: 2 of cap free
Amount of items: 3
Items: 
Size: 387887 Color: 0
Size: 339992 Color: 1
Size: 272120 Color: 1

Bin 3003: 2 of cap free
Amount of items: 3
Items: 
Size: 359117 Color: 1
Size: 323217 Color: 0
Size: 317665 Color: 0

Bin 3004: 2 of cap free
Amount of items: 3
Items: 
Size: 345960 Color: 1
Size: 334916 Color: 0
Size: 319123 Color: 0

Bin 3005: 2 of cap free
Amount of items: 3
Items: 
Size: 360909 Color: 1
Size: 325511 Color: 0
Size: 313579 Color: 0

Bin 3006: 2 of cap free
Amount of items: 3
Items: 
Size: 481476 Color: 0
Size: 264926 Color: 1
Size: 253597 Color: 0

Bin 3007: 2 of cap free
Amount of items: 3
Items: 
Size: 352838 Color: 1
Size: 348709 Color: 1
Size: 298452 Color: 0

Bin 3008: 2 of cap free
Amount of items: 3
Items: 
Size: 357467 Color: 0
Size: 334392 Color: 1
Size: 308140 Color: 0

Bin 3009: 2 of cap free
Amount of items: 3
Items: 
Size: 358944 Color: 0
Size: 323538 Color: 0
Size: 317517 Color: 1

Bin 3010: 2 of cap free
Amount of items: 3
Items: 
Size: 361033 Color: 1
Size: 336987 Color: 0
Size: 301979 Color: 1

Bin 3011: 2 of cap free
Amount of items: 3
Items: 
Size: 363162 Color: 0
Size: 348005 Color: 0
Size: 288832 Color: 1

Bin 3012: 2 of cap free
Amount of items: 3
Items: 
Size: 384305 Color: 0
Size: 348985 Color: 0
Size: 266709 Color: 1

Bin 3013: 2 of cap free
Amount of items: 3
Items: 
Size: 445592 Color: 0
Size: 294235 Color: 0
Size: 260172 Color: 1

Bin 3014: 2 of cap free
Amount of items: 3
Items: 
Size: 358726 Color: 1
Size: 331566 Color: 1
Size: 309707 Color: 0

Bin 3015: 2 of cap free
Amount of items: 3
Items: 
Size: 459046 Color: 0
Size: 271762 Color: 1
Size: 269191 Color: 0

Bin 3016: 2 of cap free
Amount of items: 3
Items: 
Size: 388540 Color: 0
Size: 350164 Color: 1
Size: 261295 Color: 1

Bin 3017: 2 of cap free
Amount of items: 3
Items: 
Size: 399628 Color: 1
Size: 305813 Color: 0
Size: 294558 Color: 0

Bin 3018: 2 of cap free
Amount of items: 3
Items: 
Size: 457211 Color: 0
Size: 290634 Color: 1
Size: 252154 Color: 1

Bin 3019: 2 of cap free
Amount of items: 3
Items: 
Size: 473976 Color: 1
Size: 265960 Color: 1
Size: 260063 Color: 0

Bin 3020: 3 of cap free
Amount of items: 3
Items: 
Size: 407871 Color: 0
Size: 336284 Color: 1
Size: 255843 Color: 0

Bin 3021: 3 of cap free
Amount of items: 3
Items: 
Size: 377264 Color: 1
Size: 352495 Color: 0
Size: 270239 Color: 1

Bin 3022: 3 of cap free
Amount of items: 3
Items: 
Size: 373320 Color: 0
Size: 351978 Color: 0
Size: 274700 Color: 1

Bin 3023: 3 of cap free
Amount of items: 3
Items: 
Size: 352696 Color: 0
Size: 333836 Color: 0
Size: 313466 Color: 1

Bin 3024: 3 of cap free
Amount of items: 3
Items: 
Size: 351512 Color: 1
Size: 346279 Color: 0
Size: 302207 Color: 1

Bin 3025: 3 of cap free
Amount of items: 3
Items: 
Size: 438595 Color: 1
Size: 287475 Color: 0
Size: 273928 Color: 1

Bin 3026: 3 of cap free
Amount of items: 3
Items: 
Size: 451104 Color: 1
Size: 277161 Color: 0
Size: 271733 Color: 1

Bin 3027: 3 of cap free
Amount of items: 3
Items: 
Size: 474516 Color: 1
Size: 265792 Color: 0
Size: 259690 Color: 0

Bin 3028: 3 of cap free
Amount of items: 3
Items: 
Size: 498447 Color: 1
Size: 251544 Color: 1
Size: 250007 Color: 0

Bin 3029: 3 of cap free
Amount of items: 3
Items: 
Size: 344924 Color: 1
Size: 343926 Color: 0
Size: 311148 Color: 1

Bin 3030: 3 of cap free
Amount of items: 3
Items: 
Size: 493050 Color: 0
Size: 254019 Color: 1
Size: 252929 Color: 0

Bin 3031: 3 of cap free
Amount of items: 3
Items: 
Size: 484187 Color: 1
Size: 259193 Color: 0
Size: 256618 Color: 0

Bin 3032: 3 of cap free
Amount of items: 3
Items: 
Size: 497700 Color: 0
Size: 251737 Color: 1
Size: 250561 Color: 0

Bin 3033: 3 of cap free
Amount of items: 3
Items: 
Size: 368234 Color: 0
Size: 349999 Color: 1
Size: 281765 Color: 1

Bin 3034: 3 of cap free
Amount of items: 3
Items: 
Size: 421436 Color: 1
Size: 297002 Color: 0
Size: 281560 Color: 0

Bin 3035: 3 of cap free
Amount of items: 3
Items: 
Size: 377481 Color: 0
Size: 353380 Color: 1
Size: 269137 Color: 0

Bin 3036: 3 of cap free
Amount of items: 3
Items: 
Size: 459203 Color: 0
Size: 280211 Color: 0
Size: 260584 Color: 1

Bin 3037: 3 of cap free
Amount of items: 3
Items: 
Size: 352339 Color: 1
Size: 329982 Color: 1
Size: 317677 Color: 0

Bin 3038: 3 of cap free
Amount of items: 3
Items: 
Size: 416902 Color: 0
Size: 322840 Color: 1
Size: 260256 Color: 1

Bin 3039: 3 of cap free
Amount of items: 3
Items: 
Size: 433588 Color: 1
Size: 315066 Color: 0
Size: 251344 Color: 0

Bin 3040: 3 of cap free
Amount of items: 3
Items: 
Size: 496759 Color: 0
Size: 252695 Color: 1
Size: 250544 Color: 1

Bin 3041: 3 of cap free
Amount of items: 3
Items: 
Size: 362185 Color: 1
Size: 346951 Color: 0
Size: 290862 Color: 1

Bin 3042: 3 of cap free
Amount of items: 3
Items: 
Size: 402661 Color: 1
Size: 330012 Color: 0
Size: 267325 Color: 1

Bin 3043: 3 of cap free
Amount of items: 3
Items: 
Size: 340067 Color: 0
Size: 331571 Color: 0
Size: 328360 Color: 1

Bin 3044: 3 of cap free
Amount of items: 3
Items: 
Size: 350396 Color: 0
Size: 325550 Color: 1
Size: 324052 Color: 1

Bin 3045: 3 of cap free
Amount of items: 3
Items: 
Size: 342329 Color: 1
Size: 334629 Color: 0
Size: 323040 Color: 0

Bin 3046: 3 of cap free
Amount of items: 3
Items: 
Size: 335307 Color: 1
Size: 332696 Color: 0
Size: 331995 Color: 0

Bin 3047: 3 of cap free
Amount of items: 3
Items: 
Size: 343254 Color: 1
Size: 333017 Color: 1
Size: 323727 Color: 0

Bin 3048: 3 of cap free
Amount of items: 3
Items: 
Size: 420525 Color: 0
Size: 321359 Color: 1
Size: 258114 Color: 0

Bin 3049: 3 of cap free
Amount of items: 3
Items: 
Size: 356169 Color: 1
Size: 326599 Color: 1
Size: 317230 Color: 0

Bin 3050: 3 of cap free
Amount of items: 3
Items: 
Size: 381757 Color: 0
Size: 359799 Color: 0
Size: 258442 Color: 1

Bin 3051: 3 of cap free
Amount of items: 3
Items: 
Size: 367987 Color: 1
Size: 354121 Color: 0
Size: 277890 Color: 0

Bin 3052: 3 of cap free
Amount of items: 3
Items: 
Size: 387828 Color: 1
Size: 327164 Color: 0
Size: 285006 Color: 1

Bin 3053: 3 of cap free
Amount of items: 3
Items: 
Size: 338529 Color: 1
Size: 338077 Color: 1
Size: 323392 Color: 0

Bin 3054: 3 of cap free
Amount of items: 3
Items: 
Size: 415048 Color: 0
Size: 334041 Color: 0
Size: 250909 Color: 1

Bin 3055: 3 of cap free
Amount of items: 3
Items: 
Size: 420469 Color: 1
Size: 323099 Color: 1
Size: 256430 Color: 0

Bin 3056: 3 of cap free
Amount of items: 3
Items: 
Size: 346070 Color: 0
Size: 341596 Color: 1
Size: 312332 Color: 0

Bin 3057: 3 of cap free
Amount of items: 3
Items: 
Size: 470889 Color: 1
Size: 274884 Color: 0
Size: 254225 Color: 0

Bin 3058: 4 of cap free
Amount of items: 3
Items: 
Size: 483212 Color: 0
Size: 258731 Color: 1
Size: 258054 Color: 1

Bin 3059: 4 of cap free
Amount of items: 3
Items: 
Size: 357089 Color: 1
Size: 348381 Color: 0
Size: 294527 Color: 0

Bin 3060: 4 of cap free
Amount of items: 3
Items: 
Size: 475111 Color: 0
Size: 263714 Color: 1
Size: 261172 Color: 1

Bin 3061: 4 of cap free
Amount of items: 3
Items: 
Size: 484647 Color: 1
Size: 258739 Color: 0
Size: 256611 Color: 1

Bin 3062: 4 of cap free
Amount of items: 3
Items: 
Size: 372280 Color: 1
Size: 342132 Color: 0
Size: 285585 Color: 0

Bin 3063: 4 of cap free
Amount of items: 3
Items: 
Size: 467211 Color: 0
Size: 266785 Color: 1
Size: 266001 Color: 1

Bin 3064: 4 of cap free
Amount of items: 3
Items: 
Size: 357421 Color: 1
Size: 344221 Color: 0
Size: 298355 Color: 1

Bin 3065: 4 of cap free
Amount of items: 3
Items: 
Size: 486957 Color: 1
Size: 256762 Color: 0
Size: 256278 Color: 1

Bin 3066: 4 of cap free
Amount of items: 3
Items: 
Size: 486000 Color: 1
Size: 262950 Color: 0
Size: 251047 Color: 0

Bin 3067: 4 of cap free
Amount of items: 3
Items: 
Size: 447137 Color: 0
Size: 299200 Color: 0
Size: 253660 Color: 1

Bin 3068: 4 of cap free
Amount of items: 3
Items: 
Size: 352649 Color: 1
Size: 350144 Color: 0
Size: 297204 Color: 0

Bin 3069: 4 of cap free
Amount of items: 3
Items: 
Size: 479698 Color: 0
Size: 266315 Color: 0
Size: 253984 Color: 1

Bin 3070: 4 of cap free
Amount of items: 3
Items: 
Size: 403705 Color: 0
Size: 336197 Color: 1
Size: 260095 Color: 1

Bin 3071: 4 of cap free
Amount of items: 3
Items: 
Size: 367745 Color: 1
Size: 339586 Color: 0
Size: 292666 Color: 0

Bin 3072: 4 of cap free
Amount of items: 3
Items: 
Size: 352669 Color: 0
Size: 346030 Color: 1
Size: 301298 Color: 0

Bin 3073: 4 of cap free
Amount of items: 3
Items: 
Size: 364899 Color: 0
Size: 323211 Color: 0
Size: 311887 Color: 1

Bin 3074: 4 of cap free
Amount of items: 3
Items: 
Size: 355451 Color: 0
Size: 351388 Color: 1
Size: 293158 Color: 0

Bin 3075: 4 of cap free
Amount of items: 3
Items: 
Size: 351989 Color: 0
Size: 340021 Color: 0
Size: 307987 Color: 1

Bin 3076: 4 of cap free
Amount of items: 3
Items: 
Size: 356023 Color: 1
Size: 336016 Color: 1
Size: 307958 Color: 0

Bin 3077: 4 of cap free
Amount of items: 3
Items: 
Size: 347225 Color: 1
Size: 332591 Color: 0
Size: 320181 Color: 1

Bin 3078: 4 of cap free
Amount of items: 3
Items: 
Size: 347078 Color: 1
Size: 328385 Color: 0
Size: 324534 Color: 1

Bin 3079: 5 of cap free
Amount of items: 3
Items: 
Size: 467698 Color: 1
Size: 278436 Color: 0
Size: 253862 Color: 1

Bin 3080: 5 of cap free
Amount of items: 3
Items: 
Size: 355870 Color: 0
Size: 336300 Color: 1
Size: 307826 Color: 1

Bin 3081: 5 of cap free
Amount of items: 3
Items: 
Size: 457600 Color: 1
Size: 285388 Color: 0
Size: 257008 Color: 0

Bin 3082: 5 of cap free
Amount of items: 3
Items: 
Size: 402135 Color: 0
Size: 326221 Color: 0
Size: 271640 Color: 1

Bin 3083: 5 of cap free
Amount of items: 3
Items: 
Size: 348633 Color: 0
Size: 344201 Color: 1
Size: 307162 Color: 1

Bin 3084: 5 of cap free
Amount of items: 3
Items: 
Size: 465537 Color: 0
Size: 268806 Color: 1
Size: 265653 Color: 1

Bin 3085: 5 of cap free
Amount of items: 3
Items: 
Size: 359695 Color: 1
Size: 330741 Color: 0
Size: 309560 Color: 1

Bin 3086: 5 of cap free
Amount of items: 3
Items: 
Size: 335906 Color: 1
Size: 335377 Color: 0
Size: 328713 Color: 0

Bin 3087: 5 of cap free
Amount of items: 3
Items: 
Size: 337630 Color: 1
Size: 332870 Color: 1
Size: 329496 Color: 0

Bin 3088: 5 of cap free
Amount of items: 3
Items: 
Size: 342656 Color: 1
Size: 342581 Color: 0
Size: 314759 Color: 0

Bin 3089: 5 of cap free
Amount of items: 3
Items: 
Size: 380111 Color: 0
Size: 317523 Color: 1
Size: 302362 Color: 1

Bin 3090: 5 of cap free
Amount of items: 3
Items: 
Size: 449190 Color: 1
Size: 276344 Color: 0
Size: 274462 Color: 1

Bin 3091: 5 of cap free
Amount of items: 3
Items: 
Size: 340310 Color: 1
Size: 335991 Color: 0
Size: 323695 Color: 1

Bin 3092: 5 of cap free
Amount of items: 3
Items: 
Size: 418913 Color: 1
Size: 298552 Color: 1
Size: 282531 Color: 0

Bin 3093: 5 of cap free
Amount of items: 3
Items: 
Size: 441588 Color: 0
Size: 291493 Color: 1
Size: 266915 Color: 1

Bin 3094: 6 of cap free
Amount of items: 3
Items: 
Size: 449626 Color: 0
Size: 287844 Color: 1
Size: 262525 Color: 0

Bin 3095: 6 of cap free
Amount of items: 3
Items: 
Size: 378512 Color: 0
Size: 359451 Color: 1
Size: 262032 Color: 1

Bin 3096: 6 of cap free
Amount of items: 3
Items: 
Size: 395425 Color: 1
Size: 344181 Color: 0
Size: 260389 Color: 1

Bin 3097: 6 of cap free
Amount of items: 3
Items: 
Size: 412857 Color: 1
Size: 305636 Color: 1
Size: 281502 Color: 0

Bin 3098: 6 of cap free
Amount of items: 3
Items: 
Size: 368766 Color: 1
Size: 360059 Color: 0
Size: 271170 Color: 0

Bin 3099: 6 of cap free
Amount of items: 3
Items: 
Size: 413296 Color: 0
Size: 305890 Color: 0
Size: 280809 Color: 1

Bin 3100: 6 of cap free
Amount of items: 3
Items: 
Size: 454246 Color: 1
Size: 282347 Color: 0
Size: 263402 Color: 1

Bin 3101: 6 of cap free
Amount of items: 3
Items: 
Size: 427599 Color: 0
Size: 298424 Color: 1
Size: 273972 Color: 1

Bin 3102: 6 of cap free
Amount of items: 3
Items: 
Size: 355261 Color: 1
Size: 331513 Color: 0
Size: 313221 Color: 0

Bin 3103: 6 of cap free
Amount of items: 3
Items: 
Size: 335630 Color: 1
Size: 335238 Color: 1
Size: 329127 Color: 0

Bin 3104: 6 of cap free
Amount of items: 3
Items: 
Size: 363379 Color: 1
Size: 344096 Color: 1
Size: 292520 Color: 0

Bin 3105: 6 of cap free
Amount of items: 3
Items: 
Size: 380571 Color: 0
Size: 348564 Color: 1
Size: 270860 Color: 0

Bin 3106: 6 of cap free
Amount of items: 3
Items: 
Size: 347187 Color: 1
Size: 340842 Color: 0
Size: 311966 Color: 1

Bin 3107: 6 of cap free
Amount of items: 3
Items: 
Size: 346554 Color: 0
Size: 346144 Color: 0
Size: 307297 Color: 1

Bin 3108: 6 of cap free
Amount of items: 3
Items: 
Size: 374738 Color: 0
Size: 314372 Color: 0
Size: 310885 Color: 1

Bin 3109: 7 of cap free
Amount of items: 3
Items: 
Size: 495050 Color: 0
Size: 253519 Color: 0
Size: 251425 Color: 1

Bin 3110: 7 of cap free
Amount of items: 3
Items: 
Size: 495521 Color: 1
Size: 253898 Color: 1
Size: 250575 Color: 0

Bin 3111: 7 of cap free
Amount of items: 3
Items: 
Size: 447089 Color: 0
Size: 300176 Color: 1
Size: 252729 Color: 0

Bin 3112: 7 of cap free
Amount of items: 3
Items: 
Size: 449311 Color: 1
Size: 278520 Color: 1
Size: 272163 Color: 0

Bin 3113: 7 of cap free
Amount of items: 3
Items: 
Size: 381469 Color: 1
Size: 355895 Color: 1
Size: 262630 Color: 0

Bin 3114: 7 of cap free
Amount of items: 3
Items: 
Size: 410767 Color: 0
Size: 329544 Color: 0
Size: 259683 Color: 1

Bin 3115: 7 of cap free
Amount of items: 3
Items: 
Size: 421656 Color: 0
Size: 291776 Color: 1
Size: 286562 Color: 0

Bin 3116: 7 of cap free
Amount of items: 3
Items: 
Size: 388838 Color: 1
Size: 351771 Color: 1
Size: 259385 Color: 0

Bin 3117: 7 of cap free
Amount of items: 3
Items: 
Size: 366802 Color: 1
Size: 325865 Color: 0
Size: 307327 Color: 1

Bin 3118: 7 of cap free
Amount of items: 3
Items: 
Size: 454859 Color: 0
Size: 293620 Color: 1
Size: 251515 Color: 1

Bin 3119: 7 of cap free
Amount of items: 3
Items: 
Size: 473416 Color: 0
Size: 269718 Color: 0
Size: 256860 Color: 1

Bin 3120: 8 of cap free
Amount of items: 3
Items: 
Size: 488909 Color: 0
Size: 256212 Color: 1
Size: 254872 Color: 0

Bin 3121: 8 of cap free
Amount of items: 3
Items: 
Size: 351314 Color: 0
Size: 346223 Color: 1
Size: 302456 Color: 1

Bin 3122: 8 of cap free
Amount of items: 3
Items: 
Size: 350224 Color: 1
Size: 347945 Color: 0
Size: 301824 Color: 1

Bin 3123: 8 of cap free
Amount of items: 3
Items: 
Size: 349929 Color: 1
Size: 325823 Color: 1
Size: 324241 Color: 0

Bin 3124: 8 of cap free
Amount of items: 3
Items: 
Size: 391909 Color: 1
Size: 320015 Color: 1
Size: 288069 Color: 0

Bin 3125: 8 of cap free
Amount of items: 3
Items: 
Size: 373954 Color: 0
Size: 323411 Color: 0
Size: 302628 Color: 1

Bin 3126: 8 of cap free
Amount of items: 3
Items: 
Size: 437067 Color: 1
Size: 304621 Color: 0
Size: 258305 Color: 1

Bin 3127: 8 of cap free
Amount of items: 3
Items: 
Size: 491452 Color: 0
Size: 254551 Color: 1
Size: 253990 Color: 0

Bin 3128: 8 of cap free
Amount of items: 3
Items: 
Size: 363617 Color: 1
Size: 358607 Color: 0
Size: 277769 Color: 1

Bin 3129: 8 of cap free
Amount of items: 3
Items: 
Size: 440788 Color: 0
Size: 279981 Color: 0
Size: 279224 Color: 1

Bin 3130: 8 of cap free
Amount of items: 3
Items: 
Size: 342312 Color: 1
Size: 336410 Color: 0
Size: 321271 Color: 0

Bin 3131: 8 of cap free
Amount of items: 3
Items: 
Size: 358057 Color: 1
Size: 331170 Color: 0
Size: 310766 Color: 1

Bin 3132: 8 of cap free
Amount of items: 3
Items: 
Size: 361540 Color: 1
Size: 351067 Color: 0
Size: 287386 Color: 0

Bin 3133: 8 of cap free
Amount of items: 3
Items: 
Size: 472554 Color: 0
Size: 266473 Color: 1
Size: 260966 Color: 1

Bin 3134: 8 of cap free
Amount of items: 3
Items: 
Size: 386605 Color: 1
Size: 362131 Color: 0
Size: 251257 Color: 0

Bin 3135: 9 of cap free
Amount of items: 3
Items: 
Size: 466190 Color: 0
Size: 274442 Color: 1
Size: 259360 Color: 1

Bin 3136: 9 of cap free
Amount of items: 3
Items: 
Size: 498411 Color: 1
Size: 251510 Color: 0
Size: 250071 Color: 1

Bin 3137: 9 of cap free
Amount of items: 3
Items: 
Size: 392079 Color: 0
Size: 343028 Color: 1
Size: 264885 Color: 1

Bin 3138: 9 of cap free
Amount of items: 3
Items: 
Size: 452870 Color: 1
Size: 285214 Color: 0
Size: 261908 Color: 0

Bin 3139: 9 of cap free
Amount of items: 3
Items: 
Size: 465836 Color: 1
Size: 273328 Color: 0
Size: 260828 Color: 0

Bin 3140: 9 of cap free
Amount of items: 3
Items: 
Size: 338693 Color: 1
Size: 330942 Color: 0
Size: 330357 Color: 0

Bin 3141: 9 of cap free
Amount of items: 3
Items: 
Size: 369364 Color: 1
Size: 358571 Color: 1
Size: 272057 Color: 0

Bin 3142: 9 of cap free
Amount of items: 3
Items: 
Size: 348306 Color: 0
Size: 339544 Color: 1
Size: 312142 Color: 0

Bin 3143: 9 of cap free
Amount of items: 3
Items: 
Size: 353680 Color: 0
Size: 337556 Color: 1
Size: 308756 Color: 1

Bin 3144: 9 of cap free
Amount of items: 3
Items: 
Size: 492865 Color: 1
Size: 255316 Color: 0
Size: 251811 Color: 1

Bin 3145: 9 of cap free
Amount of items: 3
Items: 
Size: 371760 Color: 0
Size: 335063 Color: 0
Size: 293169 Color: 1

Bin 3146: 9 of cap free
Amount of items: 3
Items: 
Size: 474064 Color: 0
Size: 264280 Color: 1
Size: 261648 Color: 0

Bin 3147: 9 of cap free
Amount of items: 3
Items: 
Size: 456457 Color: 1
Size: 280000 Color: 0
Size: 263535 Color: 0

Bin 3148: 9 of cap free
Amount of items: 3
Items: 
Size: 350631 Color: 1
Size: 350515 Color: 1
Size: 298846 Color: 0

Bin 3149: 9 of cap free
Amount of items: 3
Items: 
Size: 345185 Color: 0
Size: 344198 Color: 1
Size: 310609 Color: 1

Bin 3150: 10 of cap free
Amount of items: 3
Items: 
Size: 403977 Color: 0
Size: 332614 Color: 1
Size: 263400 Color: 0

Bin 3151: 10 of cap free
Amount of items: 3
Items: 
Size: 381767 Color: 0
Size: 313244 Color: 0
Size: 304980 Color: 1

Bin 3152: 10 of cap free
Amount of items: 3
Items: 
Size: 380065 Color: 0
Size: 353857 Color: 1
Size: 266069 Color: 1

Bin 3153: 10 of cap free
Amount of items: 3
Items: 
Size: 365435 Color: 1
Size: 348683 Color: 0
Size: 285873 Color: 1

Bin 3154: 10 of cap free
Amount of items: 3
Items: 
Size: 354066 Color: 1
Size: 343014 Color: 1
Size: 302911 Color: 0

Bin 3155: 10 of cap free
Amount of items: 3
Items: 
Size: 425156 Color: 0
Size: 312366 Color: 0
Size: 262469 Color: 1

Bin 3156: 10 of cap free
Amount of items: 3
Items: 
Size: 427083 Color: 0
Size: 297058 Color: 1
Size: 275850 Color: 1

Bin 3157: 10 of cap free
Amount of items: 3
Items: 
Size: 454488 Color: 1
Size: 274943 Color: 1
Size: 270560 Color: 0

Bin 3158: 10 of cap free
Amount of items: 3
Items: 
Size: 361971 Color: 0
Size: 361725 Color: 1
Size: 276295 Color: 1

Bin 3159: 10 of cap free
Amount of items: 3
Items: 
Size: 352511 Color: 1
Size: 331040 Color: 0
Size: 316440 Color: 1

Bin 3160: 10 of cap free
Amount of items: 3
Items: 
Size: 436792 Color: 0
Size: 306242 Color: 0
Size: 256957 Color: 1

Bin 3161: 11 of cap free
Amount of items: 3
Items: 
Size: 424175 Color: 0
Size: 304687 Color: 1
Size: 271128 Color: 0

Bin 3162: 11 of cap free
Amount of items: 3
Items: 
Size: 462786 Color: 1
Size: 284620 Color: 0
Size: 252584 Color: 0

Bin 3163: 11 of cap free
Amount of items: 3
Items: 
Size: 385902 Color: 1
Size: 359638 Color: 0
Size: 254450 Color: 1

Bin 3164: 11 of cap free
Amount of items: 3
Items: 
Size: 338912 Color: 1
Size: 337973 Color: 0
Size: 323105 Color: 0

Bin 3165: 11 of cap free
Amount of items: 3
Items: 
Size: 489406 Color: 0
Size: 258149 Color: 0
Size: 252435 Color: 1

Bin 3166: 11 of cap free
Amount of items: 3
Items: 
Size: 391103 Color: 1
Size: 335032 Color: 0
Size: 273855 Color: 1

Bin 3167: 11 of cap free
Amount of items: 3
Items: 
Size: 466285 Color: 1
Size: 271255 Color: 1
Size: 262450 Color: 0

Bin 3168: 11 of cap free
Amount of items: 3
Items: 
Size: 424497 Color: 0
Size: 304932 Color: 0
Size: 270561 Color: 1

Bin 3169: 11 of cap free
Amount of items: 3
Items: 
Size: 413586 Color: 0
Size: 294912 Color: 1
Size: 291492 Color: 1

Bin 3170: 11 of cap free
Amount of items: 3
Items: 
Size: 347920 Color: 1
Size: 330744 Color: 1
Size: 321326 Color: 0

Bin 3171: 11 of cap free
Amount of items: 3
Items: 
Size: 340374 Color: 1
Size: 336450 Color: 0
Size: 323166 Color: 1

Bin 3172: 12 of cap free
Amount of items: 3
Items: 
Size: 368520 Color: 1
Size: 350943 Color: 1
Size: 280526 Color: 0

Bin 3173: 12 of cap free
Amount of items: 3
Items: 
Size: 494513 Color: 0
Size: 254208 Color: 1
Size: 251268 Color: 1

Bin 3174: 12 of cap free
Amount of items: 3
Items: 
Size: 347514 Color: 0
Size: 331055 Color: 1
Size: 321420 Color: 0

Bin 3175: 12 of cap free
Amount of items: 3
Items: 
Size: 425037 Color: 0
Size: 299617 Color: 0
Size: 275335 Color: 1

Bin 3176: 12 of cap free
Amount of items: 3
Items: 
Size: 346221 Color: 0
Size: 328909 Color: 1
Size: 324859 Color: 0

Bin 3177: 12 of cap free
Amount of items: 3
Items: 
Size: 461362 Color: 1
Size: 285471 Color: 0
Size: 253156 Color: 1

Bin 3178: 12 of cap free
Amount of items: 3
Items: 
Size: 348576 Color: 0
Size: 347097 Color: 1
Size: 304316 Color: 1

Bin 3179: 12 of cap free
Amount of items: 3
Items: 
Size: 354530 Color: 1
Size: 351043 Color: 0
Size: 294416 Color: 0

Bin 3180: 12 of cap free
Amount of items: 3
Items: 
Size: 488237 Color: 1
Size: 258704 Color: 0
Size: 253048 Color: 0

Bin 3181: 13 of cap free
Amount of items: 3
Items: 
Size: 457945 Color: 1
Size: 273990 Color: 0
Size: 268053 Color: 0

Bin 3182: 13 of cap free
Amount of items: 3
Items: 
Size: 356247 Color: 1
Size: 334737 Color: 0
Size: 309004 Color: 0

Bin 3183: 13 of cap free
Amount of items: 3
Items: 
Size: 347647 Color: 1
Size: 336203 Color: 0
Size: 316138 Color: 0

Bin 3184: 13 of cap free
Amount of items: 3
Items: 
Size: 364297 Color: 1
Size: 359082 Color: 0
Size: 276609 Color: 0

Bin 3185: 13 of cap free
Amount of items: 3
Items: 
Size: 413236 Color: 0
Size: 308055 Color: 1
Size: 278697 Color: 0

Bin 3186: 13 of cap free
Amount of items: 3
Items: 
Size: 351160 Color: 1
Size: 339500 Color: 1
Size: 309328 Color: 0

Bin 3187: 13 of cap free
Amount of items: 3
Items: 
Size: 358265 Color: 1
Size: 329148 Color: 1
Size: 312575 Color: 0

Bin 3188: 13 of cap free
Amount of items: 3
Items: 
Size: 442008 Color: 1
Size: 282298 Color: 0
Size: 275682 Color: 1

Bin 3189: 14 of cap free
Amount of items: 3
Items: 
Size: 358087 Color: 0
Size: 323901 Color: 1
Size: 317999 Color: 1

Bin 3190: 14 of cap free
Amount of items: 3
Items: 
Size: 493297 Color: 0
Size: 253830 Color: 1
Size: 252860 Color: 1

Bin 3191: 14 of cap free
Amount of items: 3
Items: 
Size: 498140 Color: 0
Size: 251583 Color: 1
Size: 250264 Color: 1

Bin 3192: 14 of cap free
Amount of items: 3
Items: 
Size: 397472 Color: 0
Size: 322062 Color: 1
Size: 280453 Color: 1

Bin 3193: 14 of cap free
Amount of items: 3
Items: 
Size: 383401 Color: 0
Size: 318481 Color: 1
Size: 298105 Color: 0

Bin 3194: 15 of cap free
Amount of items: 3
Items: 
Size: 455791 Color: 1
Size: 286891 Color: 0
Size: 257304 Color: 1

Bin 3195: 15 of cap free
Amount of items: 3
Items: 
Size: 435913 Color: 1
Size: 293869 Color: 0
Size: 270204 Color: 0

Bin 3196: 15 of cap free
Amount of items: 3
Items: 
Size: 488071 Color: 0
Size: 255962 Color: 0
Size: 255953 Color: 1

Bin 3197: 15 of cap free
Amount of items: 3
Items: 
Size: 368066 Color: 1
Size: 358567 Color: 0
Size: 273353 Color: 0

Bin 3198: 15 of cap free
Amount of items: 3
Items: 
Size: 347134 Color: 1
Size: 341900 Color: 0
Size: 310952 Color: 0

Bin 3199: 16 of cap free
Amount of items: 3
Items: 
Size: 472635 Color: 1
Size: 263893 Color: 0
Size: 263457 Color: 0

Bin 3200: 16 of cap free
Amount of items: 3
Items: 
Size: 352168 Color: 0
Size: 326427 Color: 1
Size: 321390 Color: 0

Bin 3201: 16 of cap free
Amount of items: 3
Items: 
Size: 456671 Color: 0
Size: 277409 Color: 1
Size: 265905 Color: 1

Bin 3202: 16 of cap free
Amount of items: 3
Items: 
Size: 353453 Color: 1
Size: 349407 Color: 0
Size: 297125 Color: 1

Bin 3203: 16 of cap free
Amount of items: 3
Items: 
Size: 355275 Color: 0
Size: 352620 Color: 0
Size: 292090 Color: 1

Bin 3204: 16 of cap free
Amount of items: 3
Items: 
Size: 339626 Color: 1
Size: 334955 Color: 0
Size: 325404 Color: 1

Bin 3205: 16 of cap free
Amount of items: 3
Items: 
Size: 385165 Color: 1
Size: 343367 Color: 1
Size: 271453 Color: 0

Bin 3206: 17 of cap free
Amount of items: 3
Items: 
Size: 491137 Color: 1
Size: 258782 Color: 0
Size: 250065 Color: 0

Bin 3207: 17 of cap free
Amount of items: 3
Items: 
Size: 446864 Color: 0
Size: 283536 Color: 1
Size: 269584 Color: 0

Bin 3208: 17 of cap free
Amount of items: 3
Items: 
Size: 436727 Color: 1
Size: 286189 Color: 0
Size: 277068 Color: 0

Bin 3209: 18 of cap free
Amount of items: 3
Items: 
Size: 493806 Color: 1
Size: 254487 Color: 0
Size: 251690 Color: 0

Bin 3210: 18 of cap free
Amount of items: 3
Items: 
Size: 370281 Color: 1
Size: 339881 Color: 1
Size: 289821 Color: 0

Bin 3211: 18 of cap free
Amount of items: 3
Items: 
Size: 425730 Color: 0
Size: 294188 Color: 0
Size: 280065 Color: 1

Bin 3212: 19 of cap free
Amount of items: 3
Items: 
Size: 462131 Color: 1
Size: 271977 Color: 1
Size: 265874 Color: 0

Bin 3213: 19 of cap free
Amount of items: 3
Items: 
Size: 447533 Color: 0
Size: 283286 Color: 1
Size: 269163 Color: 0

Bin 3214: 20 of cap free
Amount of items: 3
Items: 
Size: 469634 Color: 0
Size: 265341 Color: 0
Size: 265006 Color: 1

Bin 3215: 20 of cap free
Amount of items: 3
Items: 
Size: 456086 Color: 1
Size: 274271 Color: 0
Size: 269624 Color: 0

Bin 3216: 20 of cap free
Amount of items: 3
Items: 
Size: 349009 Color: 1
Size: 333960 Color: 0
Size: 317012 Color: 1

Bin 3217: 21 of cap free
Amount of items: 3
Items: 
Size: 413730 Color: 1
Size: 314603 Color: 0
Size: 271647 Color: 0

Bin 3218: 21 of cap free
Amount of items: 3
Items: 
Size: 348285 Color: 1
Size: 333634 Color: 0
Size: 318061 Color: 0

Bin 3219: 21 of cap free
Amount of items: 3
Items: 
Size: 359784 Color: 0
Size: 320204 Color: 0
Size: 319992 Color: 1

Bin 3220: 22 of cap free
Amount of items: 3
Items: 
Size: 386293 Color: 1
Size: 346847 Color: 1
Size: 266839 Color: 0

Bin 3221: 22 of cap free
Amount of items: 3
Items: 
Size: 340083 Color: 1
Size: 330117 Color: 0
Size: 329779 Color: 0

Bin 3222: 22 of cap free
Amount of items: 3
Items: 
Size: 404523 Color: 0
Size: 336701 Color: 1
Size: 258755 Color: 1

Bin 3223: 22 of cap free
Amount of items: 3
Items: 
Size: 368371 Color: 1
Size: 339580 Color: 0
Size: 292028 Color: 0

Bin 3224: 23 of cap free
Amount of items: 3
Items: 
Size: 476833 Color: 0
Size: 262143 Color: 1
Size: 261002 Color: 1

Bin 3225: 24 of cap free
Amount of items: 3
Items: 
Size: 419171 Color: 0
Size: 307600 Color: 1
Size: 273206 Color: 1

Bin 3226: 24 of cap free
Amount of items: 3
Items: 
Size: 410856 Color: 0
Size: 319268 Color: 0
Size: 269853 Color: 1

Bin 3227: 24 of cap free
Amount of items: 3
Items: 
Size: 365350 Color: 1
Size: 336999 Color: 0
Size: 297628 Color: 1

Bin 3228: 24 of cap free
Amount of items: 3
Items: 
Size: 468967 Color: 0
Size: 266749 Color: 1
Size: 264261 Color: 0

Bin 3229: 24 of cap free
Amount of items: 3
Items: 
Size: 456247 Color: 0
Size: 276044 Color: 1
Size: 267686 Color: 1

Bin 3230: 25 of cap free
Amount of items: 3
Items: 
Size: 364349 Color: 0
Size: 355256 Color: 1
Size: 280371 Color: 0

Bin 3231: 26 of cap free
Amount of items: 3
Items: 
Size: 351683 Color: 0
Size: 337878 Color: 0
Size: 310414 Color: 1

Bin 3232: 26 of cap free
Amount of items: 3
Items: 
Size: 498540 Color: 0
Size: 250963 Color: 0
Size: 250472 Color: 1

Bin 3233: 26 of cap free
Amount of items: 3
Items: 
Size: 353040 Color: 0
Size: 342732 Color: 1
Size: 304203 Color: 1

Bin 3234: 27 of cap free
Amount of items: 3
Items: 
Size: 375108 Color: 1
Size: 368005 Color: 0
Size: 256861 Color: 0

Bin 3235: 27 of cap free
Amount of items: 3
Items: 
Size: 444184 Color: 0
Size: 289000 Color: 0
Size: 266790 Color: 1

Bin 3236: 27 of cap free
Amount of items: 3
Items: 
Size: 388444 Color: 1
Size: 321416 Color: 0
Size: 290114 Color: 1

Bin 3237: 27 of cap free
Amount of items: 3
Items: 
Size: 370821 Color: 0
Size: 335126 Color: 1
Size: 294027 Color: 0

Bin 3238: 29 of cap free
Amount of items: 3
Items: 
Size: 450406 Color: 1
Size: 281982 Color: 0
Size: 267584 Color: 1

Bin 3239: 29 of cap free
Amount of items: 3
Items: 
Size: 375739 Color: 0
Size: 354381 Color: 1
Size: 269852 Color: 1

Bin 3240: 30 of cap free
Amount of items: 3
Items: 
Size: 350736 Color: 0
Size: 350055 Color: 0
Size: 299180 Color: 1

Bin 3241: 31 of cap free
Amount of items: 3
Items: 
Size: 350938 Color: 1
Size: 350935 Color: 0
Size: 298097 Color: 1

Bin 3242: 33 of cap free
Amount of items: 3
Items: 
Size: 486972 Color: 0
Size: 256554 Color: 1
Size: 256442 Color: 1

Bin 3243: 33 of cap free
Amount of items: 3
Items: 
Size: 471349 Color: 0
Size: 269101 Color: 1
Size: 259518 Color: 0

Bin 3244: 34 of cap free
Amount of items: 3
Items: 
Size: 421204 Color: 0
Size: 318921 Color: 0
Size: 259842 Color: 1

Bin 3245: 35 of cap free
Amount of items: 3
Items: 
Size: 363829 Color: 1
Size: 331055 Color: 1
Size: 305082 Color: 0

Bin 3246: 37 of cap free
Amount of items: 3
Items: 
Size: 410901 Color: 0
Size: 298539 Color: 1
Size: 290524 Color: 0

Bin 3247: 39 of cap free
Amount of items: 3
Items: 
Size: 349499 Color: 1
Size: 330461 Color: 0
Size: 320002 Color: 0

Bin 3248: 39 of cap free
Amount of items: 3
Items: 
Size: 419800 Color: 1
Size: 309781 Color: 0
Size: 270381 Color: 0

Bin 3249: 39 of cap free
Amount of items: 3
Items: 
Size: 408562 Color: 0
Size: 323421 Color: 0
Size: 267979 Color: 1

Bin 3250: 41 of cap free
Amount of items: 3
Items: 
Size: 475836 Color: 0
Size: 262125 Color: 1
Size: 261999 Color: 0

Bin 3251: 43 of cap free
Amount of items: 3
Items: 
Size: 493319 Color: 0
Size: 254450 Color: 1
Size: 252189 Color: 0

Bin 3252: 43 of cap free
Amount of items: 3
Items: 
Size: 483595 Color: 1
Size: 260719 Color: 0
Size: 255644 Color: 0

Bin 3253: 43 of cap free
Amount of items: 3
Items: 
Size: 354628 Color: 1
Size: 325100 Color: 1
Size: 320230 Color: 0

Bin 3254: 46 of cap free
Amount of items: 3
Items: 
Size: 416495 Color: 0
Size: 293721 Color: 1
Size: 289739 Color: 1

Bin 3255: 47 of cap free
Amount of items: 3
Items: 
Size: 443132 Color: 0
Size: 296461 Color: 0
Size: 260361 Color: 1

Bin 3256: 49 of cap free
Amount of items: 3
Items: 
Size: 348563 Color: 0
Size: 327825 Color: 1
Size: 323564 Color: 1

Bin 3257: 49 of cap free
Amount of items: 3
Items: 
Size: 485981 Color: 0
Size: 257483 Color: 1
Size: 256488 Color: 1

Bin 3258: 53 of cap free
Amount of items: 3
Items: 
Size: 334859 Color: 1
Size: 333329 Color: 0
Size: 331760 Color: 0

Bin 3259: 55 of cap free
Amount of items: 3
Items: 
Size: 357799 Color: 0
Size: 331698 Color: 1
Size: 310449 Color: 1

Bin 3260: 58 of cap free
Amount of items: 3
Items: 
Size: 412102 Color: 1
Size: 319592 Color: 1
Size: 268249 Color: 0

Bin 3261: 65 of cap free
Amount of items: 3
Items: 
Size: 382774 Color: 0
Size: 334546 Color: 1
Size: 282616 Color: 1

Bin 3262: 67 of cap free
Amount of items: 3
Items: 
Size: 350512 Color: 1
Size: 350030 Color: 0
Size: 299392 Color: 0

Bin 3263: 68 of cap free
Amount of items: 3
Items: 
Size: 379372 Color: 1
Size: 317038 Color: 0
Size: 303523 Color: 0

Bin 3264: 68 of cap free
Amount of items: 3
Items: 
Size: 450406 Color: 0
Size: 296194 Color: 1
Size: 253333 Color: 0

Bin 3265: 69 of cap free
Amount of items: 3
Items: 
Size: 497765 Color: 1
Size: 251505 Color: 0
Size: 250662 Color: 1

Bin 3266: 72 of cap free
Amount of items: 3
Items: 
Size: 452080 Color: 1
Size: 279153 Color: 0
Size: 268696 Color: 1

Bin 3267: 76 of cap free
Amount of items: 3
Items: 
Size: 497568 Color: 0
Size: 251607 Color: 0
Size: 250750 Color: 1

Bin 3268: 78 of cap free
Amount of items: 3
Items: 
Size: 386603 Color: 0
Size: 312092 Color: 1
Size: 301228 Color: 0

Bin 3269: 80 of cap free
Amount of items: 3
Items: 
Size: 392876 Color: 1
Size: 346815 Color: 1
Size: 260230 Color: 0

Bin 3270: 81 of cap free
Amount of items: 3
Items: 
Size: 364645 Color: 0
Size: 363261 Color: 0
Size: 272014 Color: 1

Bin 3271: 81 of cap free
Amount of items: 3
Items: 
Size: 378429 Color: 1
Size: 338319 Color: 0
Size: 283172 Color: 0

Bin 3272: 81 of cap free
Amount of items: 3
Items: 
Size: 448641 Color: 1
Size: 290788 Color: 0
Size: 260491 Color: 1

Bin 3273: 81 of cap free
Amount of items: 3
Items: 
Size: 397783 Color: 0
Size: 342077 Color: 0
Size: 260060 Color: 1

Bin 3274: 82 of cap free
Amount of items: 3
Items: 
Size: 472332 Color: 0
Size: 263817 Color: 1
Size: 263770 Color: 0

Bin 3275: 84 of cap free
Amount of items: 3
Items: 
Size: 409293 Color: 1
Size: 324433 Color: 1
Size: 266191 Color: 0

Bin 3276: 87 of cap free
Amount of items: 3
Items: 
Size: 373192 Color: 0
Size: 313770 Color: 0
Size: 312952 Color: 1

Bin 3277: 95 of cap free
Amount of items: 3
Items: 
Size: 434183 Color: 1
Size: 291807 Color: 1
Size: 273916 Color: 0

Bin 3278: 98 of cap free
Amount of items: 3
Items: 
Size: 375877 Color: 0
Size: 369775 Color: 1
Size: 254251 Color: 1

Bin 3279: 105 of cap free
Amount of items: 3
Items: 
Size: 356432 Color: 1
Size: 342927 Color: 0
Size: 300537 Color: 1

Bin 3280: 105 of cap free
Amount of items: 3
Items: 
Size: 473407 Color: 0
Size: 268053 Color: 0
Size: 258436 Color: 1

Bin 3281: 113 of cap free
Amount of items: 3
Items: 
Size: 373702 Color: 1
Size: 350885 Color: 1
Size: 275301 Color: 0

Bin 3282: 120 of cap free
Amount of items: 2
Items: 
Size: 499954 Color: 1
Size: 499927 Color: 0

Bin 3283: 121 of cap free
Amount of items: 3
Items: 
Size: 428094 Color: 0
Size: 289680 Color: 1
Size: 282106 Color: 1

Bin 3284: 122 of cap free
Amount of items: 3
Items: 
Size: 466397 Color: 1
Size: 273972 Color: 1
Size: 259510 Color: 0

Bin 3285: 124 of cap free
Amount of items: 3
Items: 
Size: 354931 Color: 1
Size: 337620 Color: 0
Size: 307326 Color: 0

Bin 3286: 125 of cap free
Amount of items: 3
Items: 
Size: 348536 Color: 0
Size: 330744 Color: 1
Size: 320596 Color: 0

Bin 3287: 132 of cap free
Amount of items: 3
Items: 
Size: 471396 Color: 1
Size: 266484 Color: 1
Size: 261989 Color: 0

Bin 3288: 140 of cap free
Amount of items: 3
Items: 
Size: 355741 Color: 1
Size: 351995 Color: 0
Size: 292125 Color: 0

Bin 3289: 146 of cap free
Amount of items: 3
Items: 
Size: 373913 Color: 1
Size: 347847 Color: 1
Size: 278095 Color: 0

Bin 3290: 147 of cap free
Amount of items: 3
Items: 
Size: 473879 Color: 0
Size: 264943 Color: 1
Size: 261032 Color: 0

Bin 3291: 156 of cap free
Amount of items: 3
Items: 
Size: 448604 Color: 0
Size: 298685 Color: 1
Size: 252556 Color: 0

Bin 3292: 157 of cap free
Amount of items: 3
Items: 
Size: 437726 Color: 0
Size: 285181 Color: 1
Size: 276937 Color: 1

Bin 3293: 165 of cap free
Amount of items: 3
Items: 
Size: 348263 Color: 0
Size: 332165 Color: 1
Size: 319408 Color: 0

Bin 3294: 168 of cap free
Amount of items: 3
Items: 
Size: 443126 Color: 0
Size: 296008 Color: 1
Size: 260699 Color: 1

Bin 3295: 177 of cap free
Amount of items: 3
Items: 
Size: 358056 Color: 1
Size: 337908 Color: 1
Size: 303860 Color: 0

Bin 3296: 180 of cap free
Amount of items: 3
Items: 
Size: 454437 Color: 1
Size: 284045 Color: 0
Size: 261339 Color: 0

Bin 3297: 192 of cap free
Amount of items: 3
Items: 
Size: 340389 Color: 1
Size: 335455 Color: 0
Size: 323965 Color: 0

Bin 3298: 192 of cap free
Amount of items: 3
Items: 
Size: 372559 Color: 1
Size: 352609 Color: 1
Size: 274641 Color: 0

Bin 3299: 203 of cap free
Amount of items: 3
Items: 
Size: 414093 Color: 0
Size: 311162 Color: 1
Size: 274543 Color: 1

Bin 3300: 209 of cap free
Amount of items: 3
Items: 
Size: 369720 Color: 0
Size: 356185 Color: 0
Size: 273887 Color: 1

Bin 3301: 217 of cap free
Amount of items: 3
Items: 
Size: 474650 Color: 1
Size: 264710 Color: 0
Size: 260424 Color: 1

Bin 3302: 241 of cap free
Amount of items: 3
Items: 
Size: 341421 Color: 1
Size: 331128 Color: 0
Size: 327211 Color: 1

Bin 3303: 253 of cap free
Amount of items: 3
Items: 
Size: 406293 Color: 0
Size: 322519 Color: 1
Size: 270936 Color: 0

Bin 3304: 260 of cap free
Amount of items: 3
Items: 
Size: 430976 Color: 0
Size: 285644 Color: 1
Size: 283121 Color: 1

Bin 3305: 262 of cap free
Amount of items: 3
Items: 
Size: 371266 Color: 0
Size: 337840 Color: 0
Size: 290633 Color: 1

Bin 3306: 285 of cap free
Amount of items: 3
Items: 
Size: 385393 Color: 1
Size: 313606 Color: 1
Size: 300717 Color: 0

Bin 3307: 290 of cap free
Amount of items: 3
Items: 
Size: 393294 Color: 0
Size: 328104 Color: 1
Size: 278313 Color: 1

Bin 3308: 303 of cap free
Amount of items: 3
Items: 
Size: 394350 Color: 1
Size: 324690 Color: 1
Size: 280658 Color: 0

Bin 3309: 308 of cap free
Amount of items: 3
Items: 
Size: 411471 Color: 0
Size: 310356 Color: 0
Size: 277866 Color: 1

Bin 3310: 358 of cap free
Amount of items: 3
Items: 
Size: 348833 Color: 0
Size: 342477 Color: 0
Size: 308333 Color: 1

Bin 3311: 364 of cap free
Amount of items: 3
Items: 
Size: 462064 Color: 0
Size: 272802 Color: 1
Size: 264771 Color: 1

Bin 3312: 454 of cap free
Amount of items: 2
Items: 
Size: 499833 Color: 1
Size: 499714 Color: 0

Bin 3313: 524 of cap free
Amount of items: 3
Items: 
Size: 412425 Color: 1
Size: 318853 Color: 1
Size: 268199 Color: 0

Bin 3314: 610 of cap free
Amount of items: 3
Items: 
Size: 347702 Color: 1
Size: 345037 Color: 0
Size: 306652 Color: 0

Bin 3315: 664 of cap free
Amount of items: 3
Items: 
Size: 389547 Color: 0
Size: 311158 Color: 0
Size: 298632 Color: 1

Bin 3316: 687 of cap free
Amount of items: 3
Items: 
Size: 335206 Color: 0
Size: 335123 Color: 0
Size: 328985 Color: 1

Bin 3317: 725 of cap free
Amount of items: 2
Items: 
Size: 499642 Color: 1
Size: 499634 Color: 0

Bin 3318: 999 of cap free
Amount of items: 2
Items: 
Size: 499612 Color: 1
Size: 499390 Color: 0

Bin 3319: 1148 of cap free
Amount of items: 2
Items: 
Size: 499516 Color: 1
Size: 499337 Color: 0

Bin 3320: 1325 of cap free
Amount of items: 3
Items: 
Size: 353371 Color: 0
Size: 340902 Color: 1
Size: 304403 Color: 1

Bin 3321: 1975 of cap free
Amount of items: 3
Items: 
Size: 404159 Color: 1
Size: 301051 Color: 0
Size: 292816 Color: 1

Bin 3322: 2034 of cap free
Amount of items: 2
Items: 
Size: 499338 Color: 1
Size: 498629 Color: 0

Bin 3323: 4168 of cap free
Amount of items: 3
Items: 
Size: 347598 Color: 0
Size: 332059 Color: 1
Size: 316176 Color: 1

Bin 3324: 4753 of cap free
Amount of items: 3
Items: 
Size: 450562 Color: 0
Size: 283492 Color: 1
Size: 261194 Color: 0

Bin 3325: 14624 of cap free
Amount of items: 3
Items: 
Size: 357607 Color: 1
Size: 353240 Color: 1
Size: 274530 Color: 0

Bin 3326: 122380 of cap free
Amount of items: 3
Items: 
Size: 308013 Color: 1
Size: 302787 Color: 1
Size: 266821 Color: 0

Bin 3327: 212994 of cap free
Amount of items: 3
Items: 
Size: 268788 Color: 1
Size: 259237 Color: 0
Size: 258982 Color: 1

Bin 3328: 224956 of cap free
Amount of items: 3
Items: 
Size: 258718 Color: 0
Size: 258444 Color: 0
Size: 257883 Color: 1

Bin 3329: 227426 of cap free
Amount of items: 3
Items: 
Size: 257840 Color: 0
Size: 257464 Color: 0
Size: 257271 Color: 1

Bin 3330: 228724 of cap free
Amount of items: 3
Items: 
Size: 257258 Color: 1
Size: 257046 Color: 0
Size: 256973 Color: 0

Bin 3331: 230135 of cap free
Amount of items: 3
Items: 
Size: 256897 Color: 1
Size: 256623 Color: 1
Size: 256346 Color: 0

Bin 3332: 231551 of cap free
Amount of items: 3
Items: 
Size: 256323 Color: 1
Size: 256277 Color: 0
Size: 255850 Color: 1

Bin 3333: 238217 of cap free
Amount of items: 3
Items: 
Size: 254850 Color: 1
Size: 253804 Color: 1
Size: 253130 Color: 0

Bin 3334: 242146 of cap free
Amount of items: 3
Items: 
Size: 253754 Color: 1
Size: 252056 Color: 1
Size: 252045 Color: 0

Bin 3335: 244687 of cap free
Amount of items: 3
Items: 
Size: 251950 Color: 0
Size: 251765 Color: 1
Size: 251599 Color: 1

Bin 3336: 249860 of cap free
Amount of items: 2
Items: 
Size: 499233 Color: 1
Size: 250908 Color: 0

Bin 3337: 501148 of cap free
Amount of items: 1
Items: 
Size: 498853 Color: 1

Total size: 3334003334
Total free space: 3000003

