Capicity Bin: 2036
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 10
Size: 692 Color: 8
Size: 106 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 0
Size: 537 Color: 17
Size: 144 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1366 Color: 11
Size: 562 Color: 17
Size: 108 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 16
Size: 538 Color: 11
Size: 104 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 5
Size: 531 Color: 19
Size: 104 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 4
Size: 442 Color: 10
Size: 88 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 5
Size: 490 Color: 12
Size: 32 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 12
Size: 361 Color: 15
Size: 70 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 8
Size: 367 Color: 7
Size: 60 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 18
Size: 326 Color: 0
Size: 64 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 6
Size: 321 Color: 10
Size: 64 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1664 Color: 3
Size: 316 Color: 1
Size: 56 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 8
Size: 283 Color: 6
Size: 56 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 2
Size: 298 Color: 16
Size: 40 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 7
Size: 227 Color: 6
Size: 80 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 4
Size: 262 Color: 13
Size: 48 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 1
Size: 246 Color: 7
Size: 56 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 15
Size: 254 Color: 0
Size: 32 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 18
Size: 230 Color: 4
Size: 44 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 7
Size: 190 Color: 19
Size: 44 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 7
Size: 185 Color: 18
Size: 36 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 2
Size: 187 Color: 13
Size: 36 Color: 9

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 16
Size: 820 Color: 6
Size: 50 Color: 7

Bin 24: 1 of cap free
Amount of items: 4
Items: 
Size: 1549 Color: 5
Size: 242 Color: 6
Size: 224 Color: 4
Size: 20 Color: 17

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1645 Color: 5
Size: 314 Color: 10
Size: 76 Color: 15

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1719 Color: 5
Size: 316 Color: 6

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1742 Color: 19
Size: 293 Color: 14

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1785 Color: 5
Size: 198 Color: 14
Size: 52 Color: 17

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1810 Color: 13
Size: 225 Color: 9

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 1826 Color: 9
Size: 209 Color: 0

Bin 31: 2 of cap free
Amount of items: 4
Items: 
Size: 1066 Color: 1
Size: 718 Color: 0
Size: 178 Color: 5
Size: 72 Color: 13

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1276 Color: 12
Size: 666 Color: 5
Size: 92 Color: 16

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1289 Color: 10
Size: 657 Color: 4
Size: 88 Color: 14

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1291 Color: 13
Size: 743 Color: 7

Bin 35: 2 of cap free
Amount of items: 5
Items: 
Size: 1481 Color: 11
Size: 441 Color: 18
Size: 56 Color: 7
Size: 36 Color: 19
Size: 20 Color: 6

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1566 Color: 2
Size: 468 Color: 9

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1571 Color: 0
Size: 463 Color: 7

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 1787 Color: 16
Size: 247 Color: 4

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 5
Size: 132 Color: 14
Size: 88 Color: 7

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 0
Size: 348 Color: 12

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 16
Size: 327 Color: 2
Size: 16 Color: 18

Bin 42: 4 of cap free
Amount of items: 20
Items: 
Size: 282 Color: 15
Size: 168 Color: 17
Size: 160 Color: 5
Size: 144 Color: 1
Size: 140 Color: 5
Size: 126 Color: 17
Size: 124 Color: 13
Size: 106 Color: 13
Size: 96 Color: 9
Size: 84 Color: 1
Size: 76 Color: 13
Size: 76 Color: 2
Size: 76 Color: 2
Size: 68 Color: 4
Size: 64 Color: 12
Size: 58 Color: 6
Size: 56 Color: 7
Size: 52 Color: 12
Size: 48 Color: 14
Size: 28 Color: 17

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1507 Color: 17
Size: 525 Color: 10

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 13
Size: 362 Color: 8
Size: 8 Color: 7

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1767 Color: 12
Size: 265 Color: 15

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 1408 Color: 4
Size: 623 Color: 1

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 1741 Color: 5
Size: 290 Color: 14

Bin 48: 6 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 3
Size: 224 Color: 18
Size: 124 Color: 7

Bin 49: 6 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 13
Size: 257 Color: 1
Size: 8 Color: 13

Bin 50: 7 of cap free
Amount of items: 2
Items: 
Size: 1818 Color: 6
Size: 211 Color: 19

Bin 51: 9 of cap free
Amount of items: 2
Items: 
Size: 1620 Color: 16
Size: 407 Color: 8

Bin 52: 10 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 13
Size: 424 Color: 0

Bin 53: 11 of cap free
Amount of items: 3
Items: 
Size: 1023 Color: 2
Size: 890 Color: 4
Size: 112 Color: 3

Bin 54: 12 of cap free
Amount of items: 2
Items: 
Size: 1617 Color: 8
Size: 407 Color: 1

Bin 55: 13 of cap free
Amount of items: 3
Items: 
Size: 1178 Color: 4
Size: 721 Color: 14
Size: 124 Color: 13

Bin 56: 17 of cap free
Amount of items: 2
Items: 
Size: 1450 Color: 12
Size: 569 Color: 7

Bin 57: 18 of cap free
Amount of items: 5
Items: 
Size: 1019 Color: 7
Size: 394 Color: 14
Size: 389 Color: 6
Size: 168 Color: 8
Size: 48 Color: 1

Bin 58: 18 of cap free
Amount of items: 2
Items: 
Size: 1171 Color: 19
Size: 847 Color: 15

Bin 59: 19 of cap free
Amount of items: 2
Items: 
Size: 1290 Color: 17
Size: 727 Color: 7

Bin 60: 19 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 7
Size: 438 Color: 18
Size: 190 Color: 11

Bin 61: 20 of cap free
Amount of items: 2
Items: 
Size: 1475 Color: 4
Size: 541 Color: 1

Bin 62: 21 of cap free
Amount of items: 2
Items: 
Size: 1393 Color: 10
Size: 622 Color: 11

Bin 63: 22 of cap free
Amount of items: 3
Items: 
Size: 1022 Color: 12
Size: 810 Color: 4
Size: 182 Color: 4

Bin 64: 24 of cap free
Amount of items: 2
Items: 
Size: 1167 Color: 19
Size: 845 Color: 5

Bin 65: 26 of cap free
Amount of items: 2
Items: 
Size: 1021 Color: 15
Size: 989 Color: 10

Bin 66: 1700 of cap free
Amount of items: 8
Items: 
Size: 52 Color: 0
Size: 48 Color: 8
Size: 44 Color: 7
Size: 44 Color: 6
Size: 40 Color: 19
Size: 36 Color: 16
Size: 36 Color: 5
Size: 36 Color: 3

Total size: 132340
Total free space: 2036

