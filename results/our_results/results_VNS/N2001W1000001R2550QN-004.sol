Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2001

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 396276 Color: 1597
Size: 326865 Color: 1093
Size: 276860 Color: 546

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 448473 Color: 1826
Size: 282680 Color: 623
Size: 268848 Color: 418

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 482770 Color: 1953
Size: 258739 Color: 222
Size: 258492 Color: 217

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 488200 Color: 1966
Size: 259068 Color: 231
Size: 252733 Color: 90

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 383300 Color: 1525
Size: 329752 Color: 1120
Size: 286949 Color: 686

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 413761 Color: 1684
Size: 304803 Color: 863
Size: 281437 Color: 600

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 424102 Color: 1733
Size: 307274 Color: 887
Size: 268625 Color: 411

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 374312 Color: 1466
Size: 362291 Color: 1380
Size: 263398 Color: 303

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 357083 Color: 1338
Size: 326925 Color: 1094
Size: 315993 Color: 981

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 484864 Color: 1960
Size: 260813 Color: 260
Size: 254324 Color: 125

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 447213 Color: 1820
Size: 302179 Color: 846
Size: 250609 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 419040 Color: 1704
Size: 317254 Color: 998
Size: 263707 Color: 309

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 381461 Color: 1514
Size: 326711 Color: 1092
Size: 291829 Color: 741

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 457531 Color: 1858
Size: 287173 Color: 689
Size: 255297 Color: 147

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 466868 Color: 1896
Size: 274659 Color: 512
Size: 258474 Color: 216

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 454912 Color: 1850
Size: 278936 Color: 572
Size: 266153 Color: 361

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 392133 Color: 1573
Size: 333295 Color: 1160
Size: 274573 Color: 510

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 394344 Color: 1588
Size: 327262 Color: 1101
Size: 278395 Color: 567

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 490002 Color: 1973
Size: 255928 Color: 168
Size: 254071 Color: 119

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 428596 Color: 1748
Size: 297035 Color: 794
Size: 274370 Color: 504

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 374242 Color: 1463
Size: 358282 Color: 1353
Size: 267477 Color: 392

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 485502 Color: 1962
Size: 264259 Color: 319
Size: 250240 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 382798 Color: 1523
Size: 363681 Color: 1388
Size: 253522 Color: 106

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 432642 Color: 1763
Size: 289827 Color: 721
Size: 277532 Color: 557

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 407061 Color: 1650
Size: 320550 Color: 1029
Size: 272390 Color: 475

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 437186 Color: 1786
Size: 282128 Color: 609
Size: 280687 Color: 591

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 362892 Color: 1383
Size: 357675 Color: 1342
Size: 279434 Color: 581

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 429967 Color: 1750
Size: 291046 Color: 730
Size: 278988 Color: 573

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 356896 Color: 1336
Size: 353188 Color: 1312
Size: 289917 Color: 723

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 470676 Color: 1908
Size: 270265 Color: 439
Size: 259060 Color: 230

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 478473 Color: 1935
Size: 267752 Color: 399
Size: 253776 Color: 114

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 409076 Color: 1659
Size: 329803 Color: 1122
Size: 261122 Color: 264

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 480528 Color: 1942
Size: 265887 Color: 355
Size: 253586 Color: 109

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 456088 Color: 1853
Size: 280298 Color: 587
Size: 263615 Color: 307

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 432809 Color: 1765
Size: 284990 Color: 660
Size: 282202 Color: 610

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 421372 Color: 1718
Size: 308113 Color: 899
Size: 270516 Color: 443

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 393184 Color: 1580
Size: 354221 Color: 1318
Size: 252596 Color: 85

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 403092 Color: 1626
Size: 312681 Color: 947
Size: 284228 Color: 648

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 489034 Color: 1969
Size: 256769 Color: 182
Size: 254198 Color: 122

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 468174 Color: 1901
Size: 267098 Color: 385
Size: 264729 Color: 328

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 337715 Color: 1197
Size: 335114 Color: 1177
Size: 327172 Color: 1098

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 368136 Color: 1430
Size: 348181 Color: 1278
Size: 283684 Color: 637

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 417228 Color: 1700
Size: 320777 Color: 1034
Size: 261996 Color: 277

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 439934 Color: 1796
Size: 296749 Color: 790
Size: 263318 Color: 298

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 467718 Color: 1898
Size: 269351 Color: 425
Size: 262932 Color: 294

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 400513 Color: 1616
Size: 340118 Color: 1212
Size: 259370 Color: 235

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 386270 Color: 1537
Size: 332989 Color: 1155
Size: 280742 Color: 592

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 453946 Color: 1843
Size: 282055 Color: 608
Size: 264000 Color: 316

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 406334 Color: 1645
Size: 342693 Color: 1235
Size: 250974 Color: 30

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 434252 Color: 1770
Size: 304435 Color: 860
Size: 261314 Color: 267

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 386613 Color: 1542
Size: 351036 Color: 1294
Size: 262352 Color: 284

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 371980 Color: 1450
Size: 316188 Color: 986
Size: 311833 Color: 940

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 394581 Color: 1589
Size: 333070 Color: 1156
Size: 272350 Color: 474

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 366802 Color: 1417
Size: 325291 Color: 1079
Size: 307908 Color: 897

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 457556 Color: 1859
Size: 277051 Color: 549
Size: 265394 Color: 345

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 471697 Color: 1913
Size: 266176 Color: 364
Size: 262128 Color: 281

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 424053 Color: 1731
Size: 301467 Color: 842
Size: 274481 Color: 507

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 464003 Color: 1886
Size: 278365 Color: 566
Size: 257633 Color: 200

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 455178 Color: 1851
Size: 289884 Color: 722
Size: 254939 Color: 141

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 386310 Color: 1539
Size: 357766 Color: 1344
Size: 255925 Color: 167

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 373819 Color: 1461
Size: 367279 Color: 1421
Size: 258903 Color: 228

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 454291 Color: 1847
Size: 273119 Color: 485
Size: 272591 Color: 476

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 460945 Color: 1870
Size: 271305 Color: 455
Size: 267751 Color: 398

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 474967 Color: 1927
Size: 273319 Color: 488
Size: 251715 Color: 64

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 494664 Color: 1987
Size: 253880 Color: 116
Size: 251457 Color: 51

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 368594 Color: 1432
Size: 318485 Color: 1012
Size: 312922 Color: 951

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 449919 Color: 1830
Size: 299136 Color: 817
Size: 250946 Color: 29

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 366910 Color: 1419
Size: 351258 Color: 1299
Size: 281833 Color: 606

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 462036 Color: 1877
Size: 284696 Color: 653
Size: 253269 Color: 102

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 372993 Color: 1456
Size: 348715 Color: 1281
Size: 278293 Color: 565

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 444386 Color: 1811
Size: 299858 Color: 826
Size: 255757 Color: 161

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 489287 Color: 1971
Size: 260093 Color: 249
Size: 250621 Color: 15

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 447453 Color: 1822
Size: 279550 Color: 583
Size: 272998 Color: 480

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 461815 Color: 1876
Size: 283515 Color: 633
Size: 254671 Color: 133

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 392235 Color: 1574
Size: 335622 Color: 1182
Size: 272144 Color: 471

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 368959 Color: 1437
Size: 337155 Color: 1193
Size: 293887 Color: 769

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 445984 Color: 1814
Size: 299986 Color: 828
Size: 254031 Color: 117

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 414399 Color: 1688
Size: 315538 Color: 976
Size: 270064 Color: 435

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 458862 Color: 1863
Size: 282556 Color: 621
Size: 258583 Color: 218

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 470713 Color: 1909
Size: 268998 Color: 421
Size: 260290 Color: 252

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 393982 Color: 1586
Size: 349514 Color: 1286
Size: 256505 Color: 178

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 429478 Color: 1749
Size: 317953 Color: 1004
Size: 252570 Color: 82

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 483185 Color: 1954
Size: 263644 Color: 308
Size: 253172 Color: 99

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 403725 Color: 1631
Size: 307055 Color: 883
Size: 289221 Color: 713

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 425228 Color: 1738
Size: 322540 Color: 1055
Size: 252233 Color: 70

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 479953 Color: 1939
Size: 264641 Color: 325
Size: 255407 Color: 148

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 414703 Color: 1692
Size: 308345 Color: 903
Size: 276953 Color: 548

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 425551 Color: 1741
Size: 301379 Color: 840
Size: 273071 Color: 484

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 454529 Color: 1849
Size: 294113 Color: 770
Size: 251359 Color: 47

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 372740 Color: 1454
Size: 356868 Color: 1335
Size: 270393 Color: 441

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 424704 Color: 1736
Size: 298582 Color: 808
Size: 276715 Color: 540

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 358086 Color: 1347
Size: 342579 Color: 1231
Size: 299336 Color: 822

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 381359 Color: 1513
Size: 341238 Color: 1225
Size: 277404 Color: 555

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 375237 Color: 1477
Size: 358497 Color: 1354
Size: 266267 Color: 366

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 408157 Color: 1655
Size: 339491 Color: 1208
Size: 252353 Color: 75

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 410177 Color: 1665
Size: 306882 Color: 881
Size: 282942 Color: 626

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 390954 Color: 1568
Size: 338609 Color: 1199
Size: 270438 Color: 442

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 411138 Color: 1666
Size: 336254 Color: 1189
Size: 252609 Color: 86

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 405112 Color: 1638
Size: 329772 Color: 1121
Size: 265117 Color: 339

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 386562 Color: 1541
Size: 338853 Color: 1203
Size: 274586 Color: 511

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 395521 Color: 1591
Size: 339039 Color: 1204
Size: 265441 Color: 347

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 376139 Color: 1486
Size: 371286 Color: 1448
Size: 252576 Color: 83

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 388866 Color: 1551
Size: 322783 Color: 1058
Size: 288352 Color: 703

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 433930 Color: 1769
Size: 311356 Color: 933
Size: 254715 Color: 134

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 375197 Color: 1475
Size: 359512 Color: 1360
Size: 265292 Color: 342

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 404300 Color: 1634
Size: 331991 Color: 1144
Size: 263710 Color: 310

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 402210 Color: 1623
Size: 337072 Color: 1192
Size: 260719 Color: 259

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 389906 Color: 1558
Size: 344233 Color: 1245
Size: 265862 Color: 354

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 402177 Color: 1621
Size: 317523 Color: 999
Size: 280301 Color: 588

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 397573 Color: 1601
Size: 334879 Color: 1174
Size: 267549 Color: 394

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 372220 Color: 1452
Size: 366812 Color: 1418
Size: 260969 Color: 262

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 407319 Color: 1651
Size: 335524 Color: 1181
Size: 257158 Color: 190

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 406057 Color: 1644
Size: 330392 Color: 1127
Size: 263552 Color: 306

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 399280 Color: 1611
Size: 332209 Color: 1147
Size: 268512 Color: 409

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 403965 Color: 1632
Size: 340963 Color: 1220
Size: 255073 Color: 145

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 396058 Color: 1594
Size: 313583 Color: 959
Size: 290360 Color: 725

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 385900 Color: 1533
Size: 333337 Color: 1161
Size: 280764 Color: 593

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 405420 Color: 1640
Size: 330415 Color: 1128
Size: 264166 Color: 318

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 404194 Color: 1633
Size: 330691 Color: 1132
Size: 265116 Color: 338

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 405478 Color: 1641
Size: 331673 Color: 1141
Size: 262850 Color: 293

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 354719 Color: 1321
Size: 331407 Color: 1140
Size: 313875 Color: 962

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 364693 Color: 1398
Size: 318986 Color: 1018
Size: 316322 Color: 989

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 382583 Color: 1520
Size: 360201 Color: 1366
Size: 257217 Color: 192

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 389894 Color: 1557
Size: 328861 Color: 1111
Size: 281246 Color: 598

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 385844 Color: 1532
Size: 339180 Color: 1205
Size: 274977 Color: 519

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 408817 Color: 1658
Size: 311664 Color: 938
Size: 279520 Color: 582

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 409617 Color: 1663
Size: 311234 Color: 931
Size: 279150 Color: 578

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 389297 Color: 1554
Size: 334879 Color: 1175
Size: 275825 Color: 528

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 394029 Color: 1587
Size: 341128 Color: 1223
Size: 264844 Color: 332

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 389137 Color: 1553
Size: 339683 Color: 1209
Size: 271181 Color: 454

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 390481 Color: 1564
Size: 346970 Color: 1269
Size: 262550 Color: 287

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 406741 Color: 1647
Size: 330448 Color: 1129
Size: 262812 Color: 292

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 389972 Color: 1559
Size: 325850 Color: 1087
Size: 284179 Color: 646

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 399133 Color: 1610
Size: 326364 Color: 1090
Size: 274504 Color: 509

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 404723 Color: 1637
Size: 318067 Color: 1006
Size: 277211 Color: 553

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 363409 Color: 1386
Size: 351940 Color: 1304
Size: 284652 Color: 652

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 402849 Color: 1624
Size: 347038 Color: 1270
Size: 250114 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 375155 Color: 1474
Size: 361059 Color: 1370
Size: 263787 Color: 312

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 390398 Color: 1562
Size: 327151 Color: 1097
Size: 282452 Color: 618

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 387762 Color: 1546
Size: 324690 Color: 1075
Size: 287549 Color: 696

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 379394 Color: 1503
Size: 320822 Color: 1035
Size: 299785 Color: 825

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 375475 Color: 1478
Size: 347145 Color: 1271
Size: 277381 Color: 554

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 371108 Color: 1446
Size: 346910 Color: 1268
Size: 281983 Color: 607

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 358934 Color: 1356
Size: 351878 Color: 1303
Size: 289189 Color: 712

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 388460 Color: 1549
Size: 326693 Color: 1091
Size: 284848 Color: 656

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 402159 Color: 1620
Size: 321063 Color: 1037
Size: 276779 Color: 542

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 408250 Color: 1657
Size: 317781 Color: 1002
Size: 273970 Color: 496

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 409908 Color: 1664
Size: 320240 Color: 1027
Size: 269853 Color: 432

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 385612 Color: 1531
Size: 320660 Color: 1032
Size: 293729 Color: 765

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 354627 Color: 1319
Size: 325320 Color: 1080
Size: 320054 Color: 1025

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 411317 Color: 1667
Size: 297553 Color: 798
Size: 291131 Color: 731

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 406424 Color: 1646
Size: 306538 Color: 876
Size: 287039 Color: 687

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 382460 Color: 1518
Size: 321020 Color: 1036
Size: 296521 Color: 789

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 357968 Color: 1346
Size: 352522 Color: 1305
Size: 289511 Color: 718

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 398404 Color: 1606
Size: 318185 Color: 1008
Size: 283412 Color: 629

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 379946 Color: 1504
Size: 319722 Color: 1023
Size: 300333 Color: 830

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 391325 Color: 1571
Size: 355072 Color: 1324
Size: 253604 Color: 110

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 402207 Color: 1622
Size: 327726 Color: 1104
Size: 270068 Color: 436

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 365054 Color: 1401
Size: 325325 Color: 1081
Size: 309622 Color: 917

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 377014 Color: 1492
Size: 365439 Color: 1406
Size: 257548 Color: 198

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 400441 Color: 1615
Size: 305095 Color: 866
Size: 294465 Color: 777

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 381631 Color: 1515
Size: 311186 Color: 930
Size: 307184 Color: 885

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 401434 Color: 1617
Size: 334594 Color: 1172
Size: 263973 Color: 314

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 373578 Color: 1459
Size: 334184 Color: 1170
Size: 292239 Color: 748

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 373594 Color: 1460
Size: 327203 Color: 1100
Size: 299204 Color: 820

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 371253 Color: 1447
Size: 363183 Color: 1385
Size: 265565 Color: 350

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 384460 Color: 1527
Size: 321427 Color: 1040
Size: 294114 Color: 771

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 360096 Color: 1363
Size: 324591 Color: 1072
Size: 315314 Color: 972

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 374973 Color: 1472
Size: 373473 Color: 1458
Size: 251555 Color: 58

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 393207 Color: 1581
Size: 314065 Color: 964
Size: 292729 Color: 757

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 369049 Color: 1438
Size: 362240 Color: 1379
Size: 268712 Color: 414

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 397550 Color: 1600
Size: 336039 Color: 1185
Size: 266412 Color: 371

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 369578 Color: 1440
Size: 319238 Color: 1019
Size: 311185 Color: 929

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 367569 Color: 1426
Size: 318899 Color: 1017
Size: 313533 Color: 958

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 361402 Color: 1372
Size: 329526 Color: 1118
Size: 309073 Color: 913

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 365132 Color: 1402
Size: 357307 Color: 1341
Size: 277562 Color: 558

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 393058 Color: 1578
Size: 327066 Color: 1096
Size: 279877 Color: 586

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 372557 Color: 1453
Size: 350961 Color: 1293
Size: 276483 Color: 538

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 366282 Color: 1412
Size: 331079 Color: 1137
Size: 302640 Color: 849

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 406973 Color: 1648
Size: 332388 Color: 1151
Size: 260640 Color: 258

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 378204 Color: 1495
Size: 356537 Color: 1333
Size: 265260 Color: 341

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 364449 Color: 1396
Size: 363972 Color: 1389
Size: 271580 Color: 462

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 404459 Color: 1636
Size: 344543 Color: 1250
Size: 250999 Color: 31

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 364409 Color: 1395
Size: 361491 Color: 1373
Size: 274101 Color: 499

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 395904 Color: 1593
Size: 332760 Color: 1153
Size: 271337 Color: 457

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 365287 Color: 1404
Size: 361031 Color: 1369
Size: 273683 Color: 493

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 382354 Color: 1517
Size: 347748 Color: 1275
Size: 269899 Color: 433

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 364323 Color: 1392
Size: 357704 Color: 1343
Size: 277974 Color: 563

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 376282 Color: 1487
Size: 334069 Color: 1169
Size: 289650 Color: 720

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 368617 Color: 1433
Size: 358165 Color: 1349
Size: 273219 Color: 486

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 360165 Color: 1365
Size: 354195 Color: 1317
Size: 285641 Color: 668

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 362140 Color: 1378
Size: 352941 Color: 1309
Size: 284920 Color: 659

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 367986 Color: 1428
Size: 344874 Color: 1253
Size: 287141 Color: 688

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 407036 Color: 1649
Size: 300770 Color: 832
Size: 292195 Color: 745

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 368173 Color: 1431
Size: 357103 Color: 1339
Size: 274725 Color: 513

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 398680 Color: 1607
Size: 315253 Color: 971
Size: 286068 Color: 672

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 389072 Color: 1552
Size: 355874 Color: 1327
Size: 255055 Color: 144

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 409512 Color: 1662
Size: 307807 Color: 894
Size: 282682 Color: 624

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 378422 Color: 1498
Size: 333365 Color: 1162
Size: 288214 Color: 700

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 368657 Color: 1435
Size: 345480 Color: 1256
Size: 285864 Color: 669

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 370616 Color: 1443
Size: 350678 Color: 1291
Size: 278707 Color: 569

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 357062 Color: 1337
Size: 353483 Color: 1314
Size: 289456 Color: 715

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 395571 Color: 1592
Size: 332310 Color: 1149
Size: 272120 Color: 469

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 407999 Color: 1653
Size: 303611 Color: 856
Size: 288391 Color: 704

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 391173 Color: 1569
Size: 346215 Color: 1260
Size: 262613 Color: 289

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 352862 Color: 1308
Size: 339971 Color: 1211
Size: 307168 Color: 884

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 399064 Color: 1609
Size: 340655 Color: 1216
Size: 260282 Color: 251

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 351648 Color: 1301
Size: 336094 Color: 1187
Size: 312259 Color: 944

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 409508 Color: 1661
Size: 302711 Color: 851
Size: 287782 Color: 697

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 396189 Color: 1595
Size: 351227 Color: 1298
Size: 252585 Color: 84

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 393213 Color: 1582
Size: 311645 Color: 937
Size: 295143 Color: 780

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 345022 Color: 1254
Size: 342687 Color: 1234
Size: 312292 Color: 945

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 358280 Color: 1352
Size: 356816 Color: 1334
Size: 284905 Color: 658

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 396684 Color: 1598
Size: 346159 Color: 1258
Size: 257158 Color: 189

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 411413 Color: 1668
Size: 323038 Color: 1060
Size: 265550 Color: 349

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 411721 Color: 1669
Size: 296353 Color: 788
Size: 291927 Color: 743

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 411822 Color: 1670
Size: 332361 Color: 1150
Size: 255818 Color: 164

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 412010 Color: 1672
Size: 328796 Color: 1110
Size: 259195 Color: 232

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 412041 Color: 1673
Size: 316826 Color: 993
Size: 271134 Color: 452

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 412267 Color: 1675
Size: 318768 Color: 1016
Size: 268966 Color: 419

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 412667 Color: 1677
Size: 330900 Color: 1135
Size: 256434 Color: 176

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 412685 Color: 1678
Size: 330840 Color: 1134
Size: 256476 Color: 177

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 413179 Color: 1679
Size: 336606 Color: 1190
Size: 250216 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 413471 Color: 1680
Size: 296148 Color: 786
Size: 290382 Color: 726

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 413538 Color: 1681
Size: 322739 Color: 1056
Size: 263724 Color: 311

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 413609 Color: 1682
Size: 321992 Color: 1048
Size: 264400 Color: 320

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 413645 Color: 1683
Size: 324935 Color: 1077
Size: 261421 Color: 268

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 413883 Color: 1685
Size: 335106 Color: 1176
Size: 251012 Color: 33

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 414064 Color: 1686
Size: 333850 Color: 1167
Size: 252087 Color: 68

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 414201 Color: 1687
Size: 293244 Color: 762
Size: 292556 Color: 754

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 414429 Color: 1689
Size: 310492 Color: 924
Size: 275080 Color: 520

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 414602 Color: 1690
Size: 292897 Color: 759
Size: 292502 Color: 753

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 414742 Color: 1693
Size: 316193 Color: 987
Size: 269066 Color: 422

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 415119 Color: 1694
Size: 318720 Color: 1014
Size: 266162 Color: 362

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 415207 Color: 1695
Size: 308508 Color: 907
Size: 276286 Color: 535

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 415213 Color: 1696
Size: 306268 Color: 874
Size: 278520 Color: 568

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 416229 Color: 1697
Size: 296921 Color: 793
Size: 286851 Color: 684

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 416959 Color: 1698
Size: 311386 Color: 934
Size: 271656 Color: 464

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 417009 Color: 1699
Size: 308492 Color: 906
Size: 274500 Color: 508

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 417613 Color: 1701
Size: 302041 Color: 845
Size: 280347 Color: 589

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 417775 Color: 1702
Size: 298447 Color: 806
Size: 283779 Color: 639

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 419072 Color: 1705
Size: 314243 Color: 968
Size: 266686 Color: 375

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 419154 Color: 1706
Size: 310181 Color: 920
Size: 270666 Color: 446

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 419213 Color: 1707
Size: 294269 Color: 773
Size: 286519 Color: 680

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 419251 Color: 1708
Size: 316233 Color: 988
Size: 264517 Color: 322

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 419262 Color: 1709
Size: 292863 Color: 758
Size: 287876 Color: 698

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 419488 Color: 1710
Size: 307513 Color: 890
Size: 273000 Color: 481

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 420054 Color: 1711
Size: 308081 Color: 898
Size: 271866 Color: 466

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 420318 Color: 1712
Size: 314242 Color: 967
Size: 265441 Color: 348

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 420428 Color: 1713
Size: 313422 Color: 957
Size: 266151 Color: 360

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 420488 Color: 1714
Size: 322466 Color: 1053
Size: 257047 Color: 186

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 420809 Color: 1715
Size: 321706 Color: 1042
Size: 257486 Color: 197

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 420841 Color: 1716
Size: 292389 Color: 751
Size: 286771 Color: 683

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 421016 Color: 1717
Size: 304650 Color: 862
Size: 274335 Color: 503

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 421430 Color: 1719
Size: 315341 Color: 973
Size: 263230 Color: 297

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 421635 Color: 1720
Size: 294442 Color: 776
Size: 283924 Color: 641

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 421999 Color: 1721
Size: 308757 Color: 911
Size: 269245 Color: 423

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 422197 Color: 1722
Size: 312757 Color: 949
Size: 265047 Color: 336

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 422395 Color: 1723
Size: 325348 Color: 1082
Size: 252258 Color: 72

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 422521 Color: 1724
Size: 304472 Color: 861
Size: 273008 Color: 482

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 422573 Color: 1725
Size: 325759 Color: 1086
Size: 251669 Color: 60

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 422890 Color: 1726
Size: 309457 Color: 915
Size: 267654 Color: 397

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 423001 Color: 1727
Size: 324575 Color: 1071
Size: 252425 Color: 77

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 423361 Color: 1728
Size: 309060 Color: 912
Size: 267580 Color: 395

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 423698 Color: 1729
Size: 311153 Color: 927
Size: 265150 Color: 340

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 423793 Color: 1730
Size: 299406 Color: 823
Size: 276802 Color: 543

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 424058 Color: 1732
Size: 307657 Color: 892
Size: 268286 Color: 407

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 424129 Color: 1734
Size: 310445 Color: 922
Size: 265427 Color: 346

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 424617 Color: 1735
Size: 315788 Color: 979
Size: 259596 Color: 242

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 425313 Color: 1739
Size: 323983 Color: 1066
Size: 250705 Color: 19

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 425424 Color: 1740
Size: 295759 Color: 783
Size: 278818 Color: 571

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 425715 Color: 1742
Size: 288977 Color: 708
Size: 285309 Color: 663

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 425761 Color: 1743
Size: 323352 Color: 1063
Size: 250888 Color: 27

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 426664 Color: 1745
Size: 288274 Color: 702
Size: 285063 Color: 661

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 427048 Color: 1746
Size: 287545 Color: 695
Size: 285408 Color: 664

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 427821 Color: 1747
Size: 297346 Color: 796
Size: 274834 Color: 516

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 430168 Color: 1751
Size: 311906 Color: 941
Size: 257927 Color: 210

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 430658 Color: 1752
Size: 285188 Color: 662
Size: 284155 Color: 645

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 431076 Color: 1754
Size: 298617 Color: 810
Size: 270308 Color: 440

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 431122 Color: 1755
Size: 306964 Color: 882
Size: 261915 Color: 275

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 431209 Color: 1756
Size: 307266 Color: 886
Size: 261526 Color: 270

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 431562 Color: 1757
Size: 301641 Color: 843
Size: 266798 Color: 379

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 431568 Color: 1758
Size: 286108 Color: 674
Size: 282325 Color: 612

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 431612 Color: 1759
Size: 294257 Color: 772
Size: 274132 Color: 500

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 432073 Color: 1760
Size: 286383 Color: 679
Size: 281545 Color: 602

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 432205 Color: 1762
Size: 286060 Color: 671
Size: 281736 Color: 603

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 433277 Color: 1766
Size: 284449 Color: 650
Size: 282275 Color: 611

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 433351 Color: 1767
Size: 289460 Color: 716
Size: 277190 Color: 551

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 433646 Color: 1768
Size: 298264 Color: 805
Size: 268091 Color: 404

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 434432 Color: 1771
Size: 306825 Color: 879
Size: 258744 Color: 223

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 434979 Color: 1772
Size: 293057 Color: 761
Size: 271965 Color: 467

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 435001 Color: 1773
Size: 289433 Color: 714
Size: 275567 Color: 523

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 435315 Color: 1774
Size: 312145 Color: 943
Size: 252541 Color: 81

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 435365 Color: 1775
Size: 311480 Color: 935
Size: 253156 Color: 98

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 435375 Color: 1776
Size: 288504 Color: 705
Size: 276122 Color: 533

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 435452 Color: 1777
Size: 291667 Color: 740
Size: 272882 Color: 478

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 435497 Color: 1778
Size: 291476 Color: 735
Size: 273028 Color: 483

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 435510 Color: 1779
Size: 298584 Color: 809
Size: 265907 Color: 356

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 435626 Color: 1780
Size: 298688 Color: 812
Size: 265687 Color: 352

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 435810 Color: 1781
Size: 305594 Color: 869
Size: 258597 Color: 219

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 435924 Color: 1783
Size: 301092 Color: 837
Size: 262985 Color: 295

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 436023 Color: 1784
Size: 297803 Color: 802
Size: 266175 Color: 363

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 436992 Color: 1785
Size: 299573 Color: 824
Size: 263436 Color: 304

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 437358 Color: 1787
Size: 302389 Color: 847
Size: 260254 Color: 250

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 437802 Color: 1788
Size: 302855 Color: 852
Size: 259344 Color: 234

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 438684 Color: 1790
Size: 283639 Color: 636
Size: 277678 Color: 559

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 439164 Color: 1791
Size: 286940 Color: 685
Size: 273897 Color: 495

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 439374 Color: 1792
Size: 300157 Color: 829
Size: 260470 Color: 255

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 439455 Color: 1793
Size: 305121 Color: 867
Size: 255425 Color: 149

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 439558 Color: 1794
Size: 306875 Color: 880
Size: 253568 Color: 108

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 439914 Color: 1795
Size: 301386 Color: 841
Size: 258701 Color: 220

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 440711 Color: 1797
Size: 291420 Color: 734
Size: 267870 Color: 400

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 440876 Color: 1798
Size: 300344 Color: 831
Size: 258781 Color: 225

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 440934 Color: 1799
Size: 283487 Color: 631
Size: 275580 Color: 524

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 441699 Color: 1800
Size: 294852 Color: 778
Size: 263450 Color: 305

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 442157 Color: 1801
Size: 306587 Color: 877
Size: 251257 Color: 43

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 442717 Color: 1803
Size: 292259 Color: 749
Size: 265025 Color: 335

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 442873 Color: 1804
Size: 285512 Color: 665
Size: 271616 Color: 463

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 443033 Color: 1805
Size: 299290 Color: 821
Size: 257678 Color: 201

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 443103 Color: 1806
Size: 285581 Color: 666
Size: 271317 Color: 456

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 443709 Color: 1807
Size: 287301 Color: 691
Size: 268991 Color: 420

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 443725 Color: 1808
Size: 284703 Color: 654
Size: 271573 Color: 461

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 443855 Color: 1810
Size: 282512 Color: 619
Size: 273634 Color: 492

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 445823 Color: 1813
Size: 286069 Color: 673
Size: 268109 Color: 405

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 446286 Color: 1815
Size: 277774 Color: 561
Size: 275941 Color: 531

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 446499 Color: 1817
Size: 300819 Color: 833
Size: 252683 Color: 88

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 446623 Color: 1818
Size: 276952 Color: 547
Size: 276426 Color: 537

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 447129 Color: 1819
Size: 283028 Color: 628
Size: 269844 Color: 431

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 447426 Color: 1821
Size: 295089 Color: 779
Size: 257486 Color: 196

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 447669 Color: 1823
Size: 283938 Color: 642
Size: 268394 Color: 408

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 447792 Color: 1824
Size: 299193 Color: 819
Size: 253016 Color: 95

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 448959 Color: 1827
Size: 297515 Color: 797
Size: 253527 Color: 107

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 449108 Color: 1828
Size: 286143 Color: 676
Size: 264750 Color: 329

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 449759 Color: 1829
Size: 280987 Color: 596
Size: 269255 Color: 424

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 450302 Color: 1831
Size: 282327 Color: 614
Size: 267372 Color: 389

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 450307 Color: 1832
Size: 279000 Color: 574
Size: 270694 Color: 447

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 450745 Color: 1833
Size: 292200 Color: 746
Size: 257056 Color: 187

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 450855 Color: 1834
Size: 276854 Color: 545
Size: 272292 Color: 472

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 451378 Color: 1835
Size: 274811 Color: 515
Size: 273812 Color: 494

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 451397 Color: 1836
Size: 298077 Color: 803
Size: 250527 Color: 10

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 451646 Color: 1837
Size: 295323 Color: 782
Size: 253032 Color: 96

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 451777 Color: 1838
Size: 297727 Color: 800
Size: 250497 Color: 9

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 452466 Color: 1840
Size: 287523 Color: 694
Size: 260012 Color: 247

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 453088 Color: 1842
Size: 291222 Color: 732
Size: 255691 Color: 158

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 454044 Color: 1844
Size: 284874 Color: 657
Size: 261083 Color: 263

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 454175 Color: 1845
Size: 283583 Color: 634
Size: 262243 Color: 283

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 454283 Color: 1846
Size: 274190 Color: 502
Size: 271528 Color: 459

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 454505 Color: 1848
Size: 278748 Color: 570
Size: 266748 Color: 376

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 456023 Color: 1852
Size: 291549 Color: 737
Size: 252429 Color: 78

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 456141 Color: 1854
Size: 289575 Color: 719
Size: 254285 Color: 123

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 456435 Color: 1855
Size: 282379 Color: 616
Size: 261187 Color: 265

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 456483 Color: 1856
Size: 274898 Color: 517
Size: 268620 Color: 410

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 456899 Color: 1857
Size: 275839 Color: 529
Size: 267263 Color: 387

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 458111 Color: 1860
Size: 286134 Color: 675
Size: 255756 Color: 160

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 458477 Color: 1861
Size: 276134 Color: 534
Size: 265390 Color: 344

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 458684 Color: 1862
Size: 276746 Color: 541
Size: 264571 Color: 323

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 458890 Color: 1864
Size: 282361 Color: 615
Size: 258750 Color: 224

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 459584 Color: 1866
Size: 282517 Color: 620
Size: 257900 Color: 208

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 460249 Color: 1867
Size: 277692 Color: 560
Size: 262060 Color: 279

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 460269 Color: 1868
Size: 279335 Color: 579
Size: 260397 Color: 254

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 460815 Color: 1869
Size: 277197 Color: 552
Size: 261989 Color: 276

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 460960 Color: 1871
Size: 284201 Color: 647
Size: 254840 Color: 139

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 461328 Color: 1872
Size: 272327 Color: 473
Size: 266346 Color: 370

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 461636 Color: 1873
Size: 269606 Color: 429
Size: 268759 Color: 415

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 461654 Color: 1874
Size: 283600 Color: 635
Size: 254747 Color: 136

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 461662 Color: 1875
Size: 279142 Color: 577
Size: 259197 Color: 233

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 462619 Color: 1878
Size: 277490 Color: 556
Size: 259892 Color: 245

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 462719 Color: 1879
Size: 281436 Color: 599
Size: 255846 Color: 165

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 462833 Color: 1880
Size: 271564 Color: 460
Size: 265604 Color: 351

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 462962 Color: 1881
Size: 285917 Color: 670
Size: 251122 Color: 41

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 463098 Color: 1882
Size: 273523 Color: 491
Size: 263380 Color: 301

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 463338 Color: 1883
Size: 272069 Color: 468
Size: 264594 Color: 324

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 463732 Color: 1884
Size: 276840 Color: 544
Size: 259429 Color: 237

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 463811 Color: 1885
Size: 268675 Color: 413
Size: 267515 Color: 393

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 464119 Color: 1887
Size: 269579 Color: 426
Size: 266303 Color: 369

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 464445 Color: 1888
Size: 279002 Color: 575
Size: 256554 Color: 179

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 464485 Color: 1889
Size: 284643 Color: 651
Size: 250873 Color: 25

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 464525 Color: 1890
Size: 281740 Color: 604
Size: 253736 Color: 112

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 464950 Color: 1891
Size: 270219 Color: 438
Size: 264832 Color: 331

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 465094 Color: 1892
Size: 268663 Color: 412
Size: 266244 Color: 365

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 465369 Color: 1893
Size: 275188 Color: 521
Size: 259444 Color: 238

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 466476 Color: 1894
Size: 275752 Color: 526
Size: 257773 Color: 204

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 466634 Color: 1895
Size: 280870 Color: 594
Size: 252497 Color: 80

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 466921 Color: 1897
Size: 277183 Color: 550
Size: 255897 Color: 166

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 468060 Color: 1899
Size: 266862 Color: 380
Size: 265079 Color: 337

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 468130 Color: 1900
Size: 269824 Color: 430
Size: 262047 Color: 278

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 469717 Color: 1902
Size: 268799 Color: 417
Size: 261485 Color: 269

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 470020 Color: 1903
Size: 267385 Color: 390
Size: 262596 Color: 288

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 470069 Color: 1904
Size: 275613 Color: 525
Size: 254319 Color: 124

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 470303 Color: 1905
Size: 270142 Color: 437
Size: 259556 Color: 241

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 470326 Color: 1906
Size: 264977 Color: 333
Size: 264698 Color: 327

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 470379 Color: 1907
Size: 267968 Color: 401
Size: 261654 Color: 272

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 471141 Color: 1910
Size: 275519 Color: 522
Size: 253341 Color: 105

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 471316 Color: 1911
Size: 270779 Color: 448
Size: 257906 Color: 209

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 471622 Color: 1912
Size: 270586 Color: 444
Size: 257793 Color: 205

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 472270 Color: 1914
Size: 274427 Color: 505
Size: 253304 Color: 103

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 472612 Color: 1915
Size: 275915 Color: 530
Size: 251474 Color: 53

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 473109 Color: 1917
Size: 270612 Color: 445
Size: 256280 Color: 174

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 473196 Color: 1918
Size: 267066 Color: 383
Size: 259739 Color: 244

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 473224 Color: 1919
Size: 274439 Color: 506
Size: 252338 Color: 74

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 473290 Color: 1920
Size: 269939 Color: 434
Size: 256772 Color: 183

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 473487 Color: 1921
Size: 264056 Color: 317
Size: 262458 Color: 286

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 473641 Color: 1922
Size: 274070 Color: 498
Size: 252290 Color: 73

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 474177 Color: 1923
Size: 268006 Color: 402
Size: 257818 Color: 206

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 474321 Color: 1924
Size: 274137 Color: 501
Size: 251543 Color: 55

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 474358 Color: 1925
Size: 273273 Color: 487
Size: 252370 Color: 76

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 474876 Color: 1926
Size: 274040 Color: 497
Size: 251085 Color: 38

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 476348 Color: 1928
Size: 266783 Color: 378
Size: 256870 Color: 184

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 476413 Color: 1929
Size: 266285 Color: 368
Size: 257303 Color: 194

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 476564 Color: 1930
Size: 272136 Color: 470
Size: 251301 Color: 45

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 476614 Color: 1931
Size: 267451 Color: 391
Size: 255936 Color: 169

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 476816 Color: 1932
Size: 262223 Color: 282
Size: 260962 Color: 261

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 478051 Color: 1933
Size: 265388 Color: 343
Size: 256562 Color: 180

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 478139 Color: 1934
Size: 267272 Color: 388
Size: 254590 Color: 132

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 479147 Color: 1936
Size: 267613 Color: 396
Size: 253241 Color: 101

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 479747 Color: 1937
Size: 263328 Color: 299
Size: 256926 Color: 185

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 479833 Color: 1938
Size: 261773 Color: 274
Size: 258395 Color: 213

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 480220 Color: 1940
Size: 262100 Color: 280
Size: 257681 Color: 202

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 480260 Color: 1941
Size: 267078 Color: 384
Size: 252663 Color: 87

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 480974 Color: 1943
Size: 264995 Color: 334
Size: 254032 Color: 118

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 481373 Color: 1944
Size: 268064 Color: 403
Size: 250564 Color: 12

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 481392 Color: 1945
Size: 268191 Color: 406
Size: 250418 Color: 5

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 481846 Color: 1946
Size: 259732 Color: 243
Size: 258423 Color: 215

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 481857 Color: 1947
Size: 266643 Color: 374
Size: 251501 Color: 54

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 482095 Color: 1948
Size: 267024 Color: 381
Size: 250882 Color: 26

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 482125 Color: 1949
Size: 263334 Color: 300
Size: 254542 Color: 130

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 482192 Color: 1950
Size: 266764 Color: 377
Size: 251045 Color: 36

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 482613 Color: 1951
Size: 265688 Color: 353
Size: 251700 Color: 62

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 482630 Color: 1952
Size: 262623 Color: 290
Size: 254748 Color: 137

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 483465 Color: 1955
Size: 262435 Color: 285
Size: 254101 Color: 120

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 483589 Color: 1956
Size: 262649 Color: 291
Size: 253763 Color: 113

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 483734 Color: 1957
Size: 266000 Color: 357
Size: 250267 Color: 4

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 484253 Color: 1958
Size: 259962 Color: 246
Size: 255786 Color: 162

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 484595 Color: 1959
Size: 263161 Color: 296
Size: 252245 Color: 71

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 485418 Color: 1961
Size: 263982 Color: 315
Size: 250601 Color: 13

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 487162 Color: 1964
Size: 257144 Color: 188
Size: 255695 Color: 159

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 488222 Color: 1967
Size: 257313 Color: 195
Size: 254466 Color: 127

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 488943 Color: 1968
Size: 256051 Color: 172
Size: 255007 Color: 142

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 489113 Color: 1970
Size: 259025 Color: 229
Size: 251863 Color: 66

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 489912 Color: 1972
Size: 258404 Color: 214
Size: 251685 Color: 61

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 490358 Color: 1974
Size: 258785 Color: 226
Size: 250858 Color: 24

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 491223 Color: 1975
Size: 256003 Color: 171
Size: 252775 Color: 92

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 491617 Color: 1976
Size: 255667 Color: 157
Size: 252717 Color: 89

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 491708 Color: 1977
Size: 255510 Color: 151
Size: 252783 Color: 93

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 492170 Color: 1978
Size: 256124 Color: 173
Size: 251707 Color: 63

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 492588 Color: 1979
Size: 254329 Color: 126
Size: 253084 Color: 97

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 492833 Color: 1980
Size: 256348 Color: 175
Size: 250820 Color: 22

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 493611 Color: 1981
Size: 255633 Color: 156
Size: 250757 Color: 20

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 493740 Color: 1982
Size: 255565 Color: 153
Size: 250696 Color: 17

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 493872 Color: 1983
Size: 255570 Color: 154
Size: 250559 Color: 11

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 493880 Color: 1984
Size: 254777 Color: 138
Size: 251344 Color: 46

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 494378 Color: 1985
Size: 254925 Color: 140
Size: 250698 Color: 18

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 494566 Color: 1986
Size: 252986 Color: 94
Size: 252449 Color: 79

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 494782 Color: 1988
Size: 253668 Color: 111
Size: 251551 Color: 56

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 495153 Color: 1989
Size: 253832 Color: 115
Size: 251016 Color: 34

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 495382 Color: 1990
Size: 253240 Color: 100
Size: 251379 Color: 48

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 495517 Color: 1991
Size: 253325 Color: 104
Size: 251159 Color: 42

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 495880 Color: 1992
Size: 252096 Color: 69
Size: 252025 Color: 67

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 496785 Color: 1993
Size: 252738 Color: 91
Size: 250478 Color: 8

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 496974 Color: 1994
Size: 251576 Color: 59
Size: 251451 Color: 50

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 497185 Color: 1995
Size: 251555 Color: 57
Size: 251261 Color: 44

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 497641 Color: 1996
Size: 251470 Color: 52
Size: 250890 Color: 28

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 498040 Color: 1997
Size: 251111 Color: 39
Size: 250850 Color: 23

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 498135 Color: 1998
Size: 251071 Color: 37
Size: 250795 Color: 21

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 498850 Color: 1999
Size: 250681 Color: 16
Size: 250470 Color: 7

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 499292 Color: 2000
Size: 250465 Color: 6
Size: 250244 Color: 3

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 375673 Color: 1481
Size: 341920 Color: 1229
Size: 282407 Color: 617

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 370812 Color: 1445
Size: 359589 Color: 1361
Size: 269599 Color: 427

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 407456 Color: 1652
Size: 332015 Color: 1145
Size: 260529 Color: 256

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 390145 Color: 1561
Size: 325854 Color: 1088
Size: 284001 Color: 644

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 380882 Color: 1510
Size: 338243 Color: 1198
Size: 280875 Color: 595

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 412169 Color: 1674
Size: 311511 Color: 936
Size: 276320 Color: 536

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 367564 Color: 1425
Size: 333590 Color: 1164
Size: 298846 Color: 816

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 386532 Color: 1540
Size: 321905 Color: 1046
Size: 291563 Color: 738

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 370081 Color: 1441
Size: 346192 Color: 1259
Size: 283727 Color: 638

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 396233 Color: 1596
Size: 346520 Color: 1264
Size: 257247 Color: 193

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 376744 Color: 1489
Size: 365219 Color: 1403
Size: 258037 Color: 211

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 386827 Color: 1544
Size: 331386 Color: 1139
Size: 281787 Color: 605

Bin 479: 1 of cap free
Amount of items: 3
Items: 
Size: 418479 Color: 1703
Size: 310074 Color: 919
Size: 271447 Color: 458

Bin 480: 1 of cap free
Amount of items: 3
Items: 
Size: 432157 Color: 1761
Size: 308454 Color: 905
Size: 259389 Color: 236

Bin 481: 1 of cap free
Amount of items: 3
Items: 
Size: 435909 Color: 1782
Size: 308576 Color: 908
Size: 255515 Color: 152

Bin 482: 1 of cap free
Amount of items: 3
Items: 
Size: 442652 Color: 1802
Size: 285614 Color: 667
Size: 271734 Color: 465

Bin 483: 1 of cap free
Amount of items: 3
Items: 
Size: 443812 Color: 1809
Size: 281442 Color: 601
Size: 274746 Color: 514

Bin 484: 1 of cap free
Amount of items: 3
Items: 
Size: 444674 Color: 1812
Size: 288221 Color: 701
Size: 267105 Color: 386

Bin 485: 1 of cap free
Amount of items: 3
Items: 
Size: 446449 Color: 1816
Size: 282650 Color: 622
Size: 270901 Color: 450

Bin 486: 1 of cap free
Amount of items: 3
Items: 
Size: 472792 Color: 1916
Size: 269601 Color: 428
Size: 257607 Color: 199

Bin 487: 1 of cap free
Amount of items: 3
Items: 
Size: 486437 Color: 1963
Size: 258107 Color: 212
Size: 255456 Color: 150

Bin 488: 1 of cap free
Amount of items: 3
Items: 
Size: 343105 Color: 1237
Size: 333524 Color: 1163
Size: 323371 Color: 1064

Bin 489: 1 of cap free
Amount of items: 3
Items: 
Size: 385112 Color: 1529
Size: 358167 Color: 1350
Size: 256721 Color: 181

Bin 490: 1 of cap free
Amount of items: 3
Items: 
Size: 408233 Color: 1656
Size: 320591 Color: 1031
Size: 271176 Color: 453

Bin 491: 1 of cap free
Amount of items: 3
Items: 
Size: 400381 Color: 1614
Size: 343807 Color: 1243
Size: 255812 Color: 163

Bin 492: 1 of cap free
Amount of items: 3
Items: 
Size: 394821 Color: 1590
Size: 331846 Color: 1143
Size: 273333 Color: 489

Bin 493: 1 of cap free
Amount of items: 3
Items: 
Size: 375232 Color: 1476
Size: 360098 Color: 1364
Size: 264670 Color: 326

Bin 494: 1 of cap free
Amount of items: 3
Items: 
Size: 401445 Color: 1618
Size: 301686 Color: 844
Size: 296869 Color: 791

Bin 495: 1 of cap free
Amount of items: 3
Items: 
Size: 388580 Color: 1550
Size: 356224 Color: 1330
Size: 255196 Color: 146

Bin 496: 1 of cap free
Amount of items: 3
Items: 
Size: 378598 Color: 1499
Size: 318762 Color: 1015
Size: 302640 Color: 850

Bin 497: 1 of cap free
Amount of items: 3
Items: 
Size: 364399 Color: 1394
Size: 351215 Color: 1296
Size: 284386 Color: 649

Bin 498: 1 of cap free
Amount of items: 3
Items: 
Size: 380178 Color: 1506
Size: 349038 Color: 1283
Size: 270784 Color: 449

Bin 499: 1 of cap free
Amount of items: 3
Items: 
Size: 361317 Color: 1371
Size: 337654 Color: 1196
Size: 301029 Color: 836

Bin 500: 1 of cap free
Amount of items: 3
Items: 
Size: 381801 Color: 1516
Size: 359462 Color: 1357
Size: 258737 Color: 221

Bin 501: 1 of cap free
Amount of items: 3
Items: 
Size: 378316 Color: 1497
Size: 313402 Color: 956
Size: 308282 Color: 902

Bin 502: 1 of cap free
Amount of items: 3
Items: 
Size: 381098 Color: 1511
Size: 341081 Color: 1222
Size: 277821 Color: 562

Bin 503: 1 of cap free
Amount of items: 3
Items: 
Size: 386297 Color: 1538
Size: 338802 Color: 1202
Size: 274901 Color: 518

Bin 504: 1 of cap free
Amount of items: 3
Items: 
Size: 378059 Color: 1494
Size: 330911 Color: 1136
Size: 291030 Color: 729

Bin 505: 1 of cap free
Amount of items: 3
Items: 
Size: 398077 Color: 1604
Size: 335302 Color: 1179
Size: 266621 Color: 373

Bin 506: 1 of cap free
Amount of items: 3
Items: 
Size: 363449 Color: 1387
Size: 353047 Color: 1310
Size: 283504 Color: 632

Bin 507: 1 of cap free
Amount of items: 3
Items: 
Size: 397771 Color: 1603
Size: 329404 Color: 1117
Size: 272825 Color: 477

Bin 508: 1 of cap free
Amount of items: 3
Items: 
Size: 361842 Color: 1377
Size: 324367 Color: 1068
Size: 313791 Color: 961

Bin 509: 1 of cap free
Amount of items: 3
Items: 
Size: 354967 Color: 1323
Size: 337633 Color: 1195
Size: 307400 Color: 889

Bin 510: 1 of cap free
Amount of items: 3
Items: 
Size: 385177 Color: 1530
Size: 335439 Color: 1180
Size: 279384 Color: 580

Bin 511: 1 of cap free
Amount of items: 3
Items: 
Size: 359499 Color: 1359
Size: 347843 Color: 1276
Size: 292658 Color: 756

Bin 512: 1 of cap free
Amount of items: 3
Items: 
Size: 361687 Color: 1374
Size: 321383 Color: 1039
Size: 316930 Color: 995

Bin 513: 1 of cap free
Amount of items: 3
Items: 
Size: 390043 Color: 1560
Size: 358837 Color: 1355
Size: 251120 Color: 40

Bin 514: 1 of cap free
Amount of items: 3
Items: 
Size: 351414 Color: 1300
Size: 340766 Color: 1219
Size: 307820 Color: 895

Bin 515: 1 of cap free
Amount of items: 3
Items: 
Size: 344461 Color: 1248
Size: 344376 Color: 1246
Size: 311163 Color: 928

Bin 516: 2 of cap free
Amount of items: 3
Items: 
Size: 403174 Color: 1627
Size: 345430 Color: 1255
Size: 251395 Color: 49

Bin 517: 2 of cap free
Amount of items: 3
Items: 
Size: 351215 Color: 1297
Size: 340407 Color: 1214
Size: 308377 Color: 904

Bin 518: 2 of cap free
Amount of items: 3
Items: 
Size: 405247 Color: 1639
Size: 307386 Color: 888
Size: 287366 Color: 692

Bin 519: 2 of cap free
Amount of items: 3
Items: 
Size: 393399 Color: 1584
Size: 333099 Color: 1157
Size: 273501 Color: 490

Bin 520: 2 of cap free
Amount of items: 3
Items: 
Size: 392499 Color: 1576
Size: 328404 Color: 1107
Size: 279096 Color: 576

Bin 521: 2 of cap free
Amount of items: 3
Items: 
Size: 391194 Color: 1570
Size: 305795 Color: 870
Size: 303010 Color: 853

Bin 522: 2 of cap free
Amount of items: 3
Items: 
Size: 412422 Color: 1676
Size: 295303 Color: 781
Size: 292274 Color: 750

Bin 523: 2 of cap free
Amount of items: 3
Items: 
Size: 430992 Color: 1753
Size: 286275 Color: 677
Size: 282732 Color: 625

Bin 524: 2 of cap free
Amount of items: 3
Items: 
Size: 438520 Color: 1789
Size: 297614 Color: 799
Size: 263865 Color: 313

Bin 525: 2 of cap free
Amount of items: 3
Items: 
Size: 386849 Color: 1545
Size: 324632 Color: 1073
Size: 288518 Color: 706

Bin 526: 2 of cap free
Amount of items: 3
Items: 
Size: 404444 Color: 1635
Size: 336051 Color: 1186
Size: 259504 Color: 239

Bin 527: 2 of cap free
Amount of items: 3
Items: 
Size: 359952 Color: 1362
Size: 321783 Color: 1044
Size: 318264 Color: 1009

Bin 528: 2 of cap free
Amount of items: 3
Items: 
Size: 377149 Color: 1493
Size: 362531 Color: 1381
Size: 260319 Color: 253

Bin 529: 2 of cap free
Amount of items: 3
Items: 
Size: 360344 Color: 1367
Size: 329300 Color: 1115
Size: 310355 Color: 921

Bin 530: 2 of cap free
Amount of items: 3
Items: 
Size: 374408 Color: 1468
Size: 339281 Color: 1207
Size: 286310 Color: 678

Bin 531: 2 of cap free
Amount of items: 3
Items: 
Size: 380597 Color: 1509
Size: 320679 Color: 1033
Size: 298723 Color: 813

Bin 532: 2 of cap free
Amount of items: 3
Items: 
Size: 379122 Color: 1500
Size: 327031 Color: 1095
Size: 293846 Color: 768

Bin 533: 2 of cap free
Amount of items: 3
Items: 
Size: 374342 Color: 1467
Size: 365641 Color: 1408
Size: 260016 Color: 248

Bin 534: 2 of cap free
Amount of items: 3
Items: 
Size: 374972 Color: 1471
Size: 369410 Color: 1439
Size: 255617 Color: 155

Bin 535: 2 of cap free
Amount of items: 3
Items: 
Size: 372142 Color: 1451
Size: 361815 Color: 1375
Size: 266042 Color: 358

Bin 536: 2 of cap free
Amount of items: 3
Items: 
Size: 375038 Color: 1473
Size: 314094 Color: 965
Size: 310867 Color: 926

Bin 537: 2 of cap free
Amount of items: 3
Items: 
Size: 400055 Color: 1613
Size: 316068 Color: 982
Size: 283876 Color: 640

Bin 538: 2 of cap free
Amount of items: 3
Items: 
Size: 452887 Color: 1841
Size: 292637 Color: 755
Size: 254475 Color: 128

Bin 539: 2 of cap free
Amount of items: 3
Items: 
Size: 366367 Color: 1415
Size: 319477 Color: 1020
Size: 314155 Color: 966

Bin 540: 2 of cap free
Amount of items: 3
Items: 
Size: 366756 Color: 1416
Size: 366178 Color: 1411
Size: 267065 Color: 382

Bin 541: 2 of cap free
Amount of items: 3
Items: 
Size: 371665 Color: 1449
Size: 348662 Color: 1280
Size: 279672 Color: 584

Bin 542: 3 of cap free
Amount of items: 3
Items: 
Size: 351141 Color: 1295
Size: 342911 Color: 1236
Size: 305946 Color: 872

Bin 543: 3 of cap free
Amount of items: 3
Items: 
Size: 399352 Color: 1612
Size: 324120 Color: 1067
Size: 276526 Color: 539

Bin 544: 3 of cap free
Amount of items: 3
Items: 
Size: 365423 Color: 1405
Size: 341598 Color: 1228
Size: 292977 Color: 760

Bin 545: 3 of cap free
Amount of items: 3
Items: 
Size: 343766 Color: 1242
Size: 332668 Color: 1152
Size: 323564 Color: 1065

Bin 546: 3 of cap free
Amount of items: 3
Items: 
Size: 356062 Color: 1328
Size: 327447 Color: 1102
Size: 316489 Color: 992

Bin 547: 3 of cap free
Amount of items: 3
Items: 
Size: 425069 Color: 1737
Size: 310491 Color: 923
Size: 264438 Color: 321

Bin 548: 3 of cap free
Amount of items: 3
Items: 
Size: 409484 Color: 1660
Size: 335976 Color: 1184
Size: 254538 Color: 129

Bin 549: 3 of cap free
Amount of items: 3
Items: 
Size: 353134 Color: 1311
Size: 329705 Color: 1119
Size: 317159 Color: 997

Bin 550: 3 of cap free
Amount of items: 3
Items: 
Size: 357954 Color: 1345
Size: 348223 Color: 1279
Size: 293821 Color: 767

Bin 551: 3 of cap free
Amount of items: 3
Items: 
Size: 405485 Color: 1642
Size: 333229 Color: 1158
Size: 261284 Color: 266

Bin 552: 3 of cap free
Amount of items: 3
Items: 
Size: 375782 Color: 1483
Size: 317550 Color: 1000
Size: 306666 Color: 878

Bin 553: 3 of cap free
Amount of items: 3
Items: 
Size: 356299 Color: 1331
Size: 331772 Color: 1142
Size: 311927 Color: 942

Bin 554: 3 of cap free
Amount of items: 3
Items: 
Size: 349633 Color: 1287
Size: 349159 Color: 1284
Size: 301206 Color: 839

Bin 555: 3 of cap free
Amount of items: 3
Items: 
Size: 350914 Color: 1292
Size: 341493 Color: 1227
Size: 307591 Color: 891

Bin 556: 3 of cap free
Amount of items: 3
Items: 
Size: 343387 Color: 1239
Size: 343255 Color: 1238
Size: 313356 Color: 954

Bin 557: 4 of cap free
Amount of items: 3
Items: 
Size: 432775 Color: 1764
Size: 309360 Color: 914
Size: 257862 Color: 207

Bin 558: 4 of cap free
Amount of items: 3
Items: 
Size: 389425 Color: 1555
Size: 325725 Color: 1084
Size: 284847 Color: 655

Bin 559: 4 of cap free
Amount of items: 3
Items: 
Size: 382506 Color: 1519
Size: 330096 Color: 1124
Size: 287395 Color: 693

Bin 560: 4 of cap free
Amount of items: 3
Items: 
Size: 397617 Color: 1602
Size: 320055 Color: 1026
Size: 282325 Color: 613

Bin 561: 4 of cap free
Amount of items: 3
Items: 
Size: 365970 Color: 1409
Size: 318518 Color: 1013
Size: 315509 Color: 975

Bin 562: 4 of cap free
Amount of items: 3
Items: 
Size: 352689 Color: 1307
Size: 338635 Color: 1200
Size: 308673 Color: 909

Bin 563: 4 of cap free
Amount of items: 3
Items: 
Size: 368778 Color: 1436
Size: 316104 Color: 983
Size: 315115 Color: 970

Bin 564: 4 of cap free
Amount of items: 3
Items: 
Size: 401580 Color: 1619
Size: 340689 Color: 1217
Size: 257728 Color: 203

Bin 565: 5 of cap free
Amount of items: 3
Items: 
Size: 425916 Color: 1744
Size: 319889 Color: 1024
Size: 254191 Color: 121

Bin 566: 5 of cap free
Amount of items: 3
Items: 
Size: 364694 Color: 1399
Size: 346320 Color: 1261
Size: 288982 Color: 709

Bin 567: 5 of cap free
Amount of items: 3
Items: 
Size: 387846 Color: 1547
Size: 346006 Color: 1257
Size: 266144 Color: 359

Bin 568: 5 of cap free
Amount of items: 3
Items: 
Size: 447934 Color: 1825
Size: 293259 Color: 763
Size: 258803 Color: 227

Bin 569: 5 of cap free
Amount of items: 3
Items: 
Size: 390670 Color: 1566
Size: 317672 Color: 1001
Size: 291654 Color: 739

Bin 570: 5 of cap free
Amount of items: 3
Items: 
Size: 393735 Color: 1585
Size: 330472 Color: 1130
Size: 275789 Color: 527

Bin 571: 5 of cap free
Amount of items: 3
Items: 
Size: 350549 Color: 1289
Size: 341184 Color: 1224
Size: 308263 Color: 901

Bin 572: 5 of cap free
Amount of items: 3
Items: 
Size: 353312 Color: 1313
Size: 338786 Color: 1201
Size: 307898 Color: 896

Bin 573: 5 of cap free
Amount of items: 3
Items: 
Size: 372796 Color: 1455
Size: 322174 Color: 1051
Size: 305026 Color: 865

Bin 574: 5 of cap free
Amount of items: 3
Items: 
Size: 384828 Color: 1528
Size: 364162 Color: 1391
Size: 251006 Color: 32

Bin 575: 5 of cap free
Amount of items: 3
Items: 
Size: 343504 Color: 1240
Size: 332071 Color: 1146
Size: 324421 Color: 1069

Bin 576: 5 of cap free
Amount of items: 3
Items: 
Size: 360611 Color: 1368
Size: 336175 Color: 1188
Size: 303210 Color: 854

Bin 577: 6 of cap free
Amount of items: 3
Items: 
Size: 390401 Color: 1563
Size: 320423 Color: 1028
Size: 289171 Color: 711

Bin 578: 6 of cap free
Amount of items: 3
Items: 
Size: 347540 Color: 1273
Size: 342458 Color: 1230
Size: 309997 Color: 918

Bin 579: 6 of cap free
Amount of items: 3
Items: 
Size: 392597 Color: 1577
Size: 329211 Color: 1113
Size: 278187 Color: 564

Bin 580: 6 of cap free
Amount of items: 3
Items: 
Size: 385955 Color: 1534
Size: 359468 Color: 1358
Size: 254572 Color: 131

Bin 581: 6 of cap free
Amount of items: 3
Items: 
Size: 382680 Color: 1522
Size: 316183 Color: 985
Size: 301132 Color: 838

Bin 582: 6 of cap free
Amount of items: 3
Items: 
Size: 364725 Color: 1400
Size: 331228 Color: 1138
Size: 304042 Color: 859

Bin 583: 6 of cap free
Amount of items: 3
Items: 
Size: 375763 Color: 1482
Size: 373203 Color: 1457
Size: 251029 Color: 35

Bin 584: 7 of cap free
Amount of items: 3
Items: 
Size: 351877 Color: 1302
Size: 325356 Color: 1083
Size: 322761 Color: 1057

Bin 585: 7 of cap free
Amount of items: 3
Items: 
Size: 382623 Color: 1521
Size: 344457 Color: 1247
Size: 272914 Color: 479

Bin 586: 7 of cap free
Amount of items: 3
Items: 
Size: 398178 Color: 1605
Size: 317854 Color: 1003
Size: 283962 Color: 643

Bin 587: 7 of cap free
Amount of items: 3
Items: 
Size: 368067 Color: 1429
Size: 321233 Color: 1038
Size: 310694 Color: 925

Bin 588: 7 of cap free
Amount of items: 3
Items: 
Size: 390485 Color: 1565
Size: 318113 Color: 1007
Size: 291396 Color: 733

Bin 589: 8 of cap free
Amount of items: 3
Items: 
Size: 386010 Color: 1535
Size: 324862 Color: 1076
Size: 289121 Color: 710

Bin 590: 8 of cap free
Amount of items: 3
Items: 
Size: 398918 Color: 1608
Size: 332288 Color: 1148
Size: 268787 Color: 416

Bin 591: 8 of cap free
Amount of items: 3
Items: 
Size: 352589 Color: 1306
Size: 334689 Color: 1173
Size: 312715 Color: 948

Bin 592: 8 of cap free
Amount of items: 3
Items: 
Size: 361828 Color: 1376
Size: 355180 Color: 1325
Size: 282985 Color: 627

Bin 593: 8 of cap free
Amount of items: 3
Items: 
Size: 403686 Color: 1630
Size: 315684 Color: 977
Size: 280623 Color: 590

Bin 594: 9 of cap free
Amount of items: 3
Items: 
Size: 392343 Color: 1575
Size: 313378 Color: 955
Size: 294271 Color: 774

Bin 595: 9 of cap free
Amount of items: 3
Items: 
Size: 451904 Color: 1839
Size: 292087 Color: 744
Size: 256001 Color: 170

Bin 596: 9 of cap free
Amount of items: 3
Items: 
Size: 366051 Color: 1410
Size: 335149 Color: 1178
Size: 298792 Color: 815

Bin 597: 10 of cap free
Amount of items: 3
Items: 
Size: 367624 Color: 1427
Size: 334586 Color: 1171
Size: 297781 Color: 801

Bin 598: 11 of cap free
Amount of items: 3
Items: 
Size: 393160 Color: 1579
Size: 314408 Color: 969
Size: 292422 Color: 752

Bin 599: 11 of cap free
Amount of items: 3
Items: 
Size: 358122 Color: 1348
Size: 353825 Color: 1316
Size: 288043 Color: 699

Bin 600: 11 of cap free
Amount of items: 3
Items: 
Size: 365589 Color: 1407
Size: 347220 Color: 1272
Size: 287181 Color: 690

Bin 601: 12 of cap free
Amount of items: 3
Items: 
Size: 340265 Color: 1213
Size: 333979 Color: 1168
Size: 325745 Color: 1085

Bin 602: 12 of cap free
Amount of items: 3
Items: 
Size: 367457 Color: 1423
Size: 341031 Color: 1221
Size: 291501 Color: 736

Bin 603: 13 of cap free
Amount of items: 3
Items: 
Size: 376012 Color: 1484
Size: 315852 Color: 980
Size: 308124 Color: 900

Bin 604: 13 of cap free
Amount of items: 3
Items: 
Size: 374304 Color: 1465
Size: 327181 Color: 1099
Size: 298503 Color: 807

Bin 605: 14 of cap free
Amount of items: 3
Items: 
Size: 379148 Color: 1501
Size: 330241 Color: 1126
Size: 290598 Color: 727

Bin 606: 14 of cap free
Amount of items: 3
Items: 
Size: 403651 Color: 1629
Size: 344610 Color: 1251
Size: 251726 Color: 65

Bin 607: 14 of cap free
Amount of items: 3
Items: 
Size: 354945 Color: 1322
Size: 328119 Color: 1106
Size: 316923 Color: 994

Bin 608: 15 of cap free
Amount of items: 3
Items: 
Size: 376059 Color: 1485
Size: 320584 Color: 1030
Size: 303343 Color: 855

Bin 609: 15 of cap free
Amount of items: 3
Items: 
Size: 356148 Color: 1329
Size: 322346 Color: 1052
Size: 321492 Color: 1041

Bin 610: 15 of cap free
Amount of items: 3
Items: 
Size: 402922 Color: 1625
Size: 330504 Color: 1131
Size: 266560 Color: 372

Bin 611: 15 of cap free
Amount of items: 3
Items: 
Size: 376871 Color: 1490
Size: 322134 Color: 1050
Size: 300981 Color: 835

Bin 612: 16 of cap free
Amount of items: 3
Items: 
Size: 374212 Color: 1462
Size: 313297 Color: 953
Size: 312476 Color: 946

Bin 613: 16 of cap free
Amount of items: 3
Items: 
Size: 364049 Color: 1390
Size: 346427 Color: 1263
Size: 289509 Color: 717

Bin 614: 17 of cap free
Amount of items: 3
Items: 
Size: 367415 Color: 1422
Size: 366286 Color: 1413
Size: 266283 Color: 367

Bin 615: 17 of cap free
Amount of items: 3
Items: 
Size: 374274 Color: 1464
Size: 316176 Color: 984
Size: 309534 Color: 916

Bin 616: 18 of cap free
Amount of items: 3
Items: 
Size: 347595 Color: 1274
Size: 329260 Color: 1114
Size: 323128 Color: 1061

Bin 617: 18 of cap free
Amount of items: 3
Items: 
Size: 380201 Color: 1507
Size: 322472 Color: 1054
Size: 297310 Color: 795

Bin 618: 19 of cap free
Amount of items: 3
Items: 
Size: 389807 Color: 1556
Size: 346779 Color: 1266
Size: 263396 Color: 302

Bin 619: 20 of cap free
Amount of items: 3
Items: 
Size: 367163 Color: 1420
Size: 332934 Color: 1154
Size: 299884 Color: 827

Bin 620: 21 of cap free
Amount of items: 3
Items: 
Size: 381273 Color: 1512
Size: 357136 Color: 1340
Size: 261571 Color: 271

Bin 621: 21 of cap free
Amount of items: 3
Items: 
Size: 366364 Color: 1414
Size: 321849 Color: 1045
Size: 311767 Color: 939

Bin 622: 23 of cap free
Amount of items: 3
Items: 
Size: 374752 Color: 1470
Size: 316487 Color: 991
Size: 308739 Color: 910

Bin 623: 24 of cap free
Amount of items: 3
Items: 
Size: 384058 Color: 1526
Size: 325074 Color: 1078
Size: 290845 Color: 728

Bin 624: 25 of cap free
Amount of items: 3
Items: 
Size: 459575 Color: 1865
Size: 279852 Color: 585
Size: 260549 Color: 257

Bin 625: 26 of cap free
Amount of items: 3
Items: 
Size: 368631 Color: 1434
Size: 336919 Color: 1191
Size: 294425 Color: 775

Bin 626: 26 of cap free
Amount of items: 3
Items: 
Size: 376447 Color: 1488
Size: 333280 Color: 1159
Size: 290248 Color: 724

Bin 627: 27 of cap free
Amount of items: 3
Items: 
Size: 380318 Color: 1508
Size: 343571 Color: 1241
Size: 276085 Color: 532

Bin 628: 28 of cap free
Amount of items: 3
Items: 
Size: 350579 Color: 1290
Size: 350241 Color: 1288
Size: 299153 Color: 818

Bin 629: 30 of cap free
Amount of items: 3
Items: 
Size: 370740 Color: 1444
Size: 316367 Color: 990
Size: 312864 Color: 950

Bin 630: 33 of cap free
Amount of items: 3
Items: 
Size: 414620 Color: 1691
Size: 298682 Color: 811
Size: 286666 Color: 681

Bin 631: 33 of cap free
Amount of items: 3
Items: 
Size: 376969 Color: 1491
Size: 318043 Color: 1005
Size: 304956 Color: 864

Bin 632: 34 of cap free
Amount of items: 3
Items: 
Size: 391522 Color: 1572
Size: 346727 Color: 1265
Size: 261718 Color: 273

Bin 633: 35 of cap free
Amount of items: 3
Items: 
Size: 358229 Color: 1351
Size: 325990 Color: 1089
Size: 315747 Color: 978

Bin 634: 41 of cap free
Amount of items: 3
Items: 
Size: 375488 Color: 1479
Size: 321990 Color: 1047
Size: 302482 Color: 848

Bin 635: 41 of cap free
Amount of items: 3
Items: 
Size: 364680 Color: 1397
Size: 328750 Color: 1109
Size: 306530 Color: 875

Bin 636: 41 of cap free
Amount of items: 3
Items: 
Size: 340611 Color: 1215
Size: 329978 Color: 1123
Size: 329371 Color: 1116

Bin 637: 51 of cap free
Amount of items: 3
Items: 
Size: 349483 Color: 1285
Size: 328698 Color: 1108
Size: 321769 Color: 1043

Bin 638: 52 of cap free
Amount of items: 3
Items: 
Size: 363094 Color: 1384
Size: 355761 Color: 1326
Size: 281094 Color: 597

Bin 639: 60 of cap free
Amount of items: 3
Items: 
Size: 364354 Color: 1393
Size: 339259 Color: 1206
Size: 296328 Color: 787

Bin 640: 61 of cap free
Amount of items: 3
Items: 
Size: 347970 Color: 1277
Size: 327482 Color: 1103
Size: 324488 Color: 1070

Bin 641: 75 of cap free
Amount of items: 3
Items: 
Size: 375549 Color: 1480
Size: 318461 Color: 1011
Size: 305916 Color: 871

Bin 642: 89 of cap free
Amount of items: 3
Items: 
Size: 408012 Color: 1654
Size: 298156 Color: 804
Size: 293744 Color: 766

Bin 643: 89 of cap free
Amount of items: 3
Items: 
Size: 378294 Color: 1496
Size: 313898 Color: 963
Size: 307720 Color: 893

Bin 644: 101 of cap free
Amount of items: 3
Items: 
Size: 344486 Color: 1249
Size: 344112 Color: 1244
Size: 311302 Color: 932

Bin 645: 108 of cap free
Amount of items: 3
Items: 
Size: 348749 Color: 1282
Size: 329059 Color: 1112
Size: 322085 Color: 1049

Bin 646: 111 of cap free
Amount of items: 3
Items: 
Size: 374421 Color: 1469
Size: 333626 Color: 1165
Size: 291843 Color: 742

Bin 647: 114 of cap free
Amount of items: 3
Items: 
Size: 353496 Color: 1315
Size: 342652 Color: 1233
Size: 303739 Color: 858

Bin 648: 115 of cap free
Amount of items: 3
Items: 
Size: 356321 Color: 1332
Size: 337462 Color: 1194
Size: 306103 Color: 873

Bin 649: 123 of cap free
Amount of items: 3
Items: 
Size: 405592 Color: 1643
Size: 300970 Color: 834
Size: 293316 Color: 764

Bin 650: 132 of cap free
Amount of items: 3
Items: 
Size: 346904 Color: 1267
Size: 330162 Color: 1125
Size: 322803 Color: 1059

Bin 651: 133 of cap free
Amount of items: 3
Items: 
Size: 388449 Color: 1548
Size: 327944 Color: 1105
Size: 283475 Color: 630

Bin 652: 147 of cap free
Amount of items: 3
Items: 
Size: 390897 Color: 1567
Size: 313095 Color: 952
Size: 295862 Color: 785

Bin 653: 161 of cap free
Amount of items: 3
Items: 
Size: 383273 Color: 1524
Size: 319659 Color: 1022
Size: 296908 Color: 792

Bin 654: 161 of cap free
Amount of items: 3
Items: 
Size: 344751 Color: 1252
Size: 341423 Color: 1226
Size: 313666 Color: 960

Bin 655: 163 of cap free
Amount of items: 3
Items: 
Size: 411853 Color: 1671
Size: 330790 Color: 1133
Size: 257195 Color: 191

Bin 656: 242 of cap free
Amount of items: 3
Items: 
Size: 354719 Color: 1320
Size: 339708 Color: 1210
Size: 305332 Color: 868

Bin 657: 268 of cap free
Amount of items: 3
Items: 
Size: 393368 Color: 1583
Size: 319638 Color: 1021
Size: 286727 Color: 682

Bin 658: 313 of cap free
Amount of items: 3
Items: 
Size: 386827 Color: 1543
Size: 317023 Color: 996
Size: 295838 Color: 784

Bin 659: 394 of cap free
Amount of items: 3
Items: 
Size: 386150 Color: 1536
Size: 324649 Color: 1074
Size: 288808 Color: 707

Bin 660: 405 of cap free
Amount of items: 3
Items: 
Size: 342615 Color: 1232
Size: 333816 Color: 1166
Size: 323165 Color: 1062

Bin 661: 588 of cap free
Amount of items: 3
Items: 
Size: 403650 Color: 1628
Size: 340740 Color: 1218
Size: 255023 Color: 143

Bin 662: 847 of cap free
Amount of items: 3
Items: 
Size: 380143 Color: 1505
Size: 315352 Color: 974
Size: 303659 Color: 857

Bin 663: 1509 of cap free
Amount of items: 3
Items: 
Size: 397430 Color: 1599
Size: 346331 Color: 1262
Size: 254731 Color: 135

Bin 664: 2015 of cap free
Amount of items: 3
Items: 
Size: 370121 Color: 1442
Size: 335648 Color: 1183
Size: 292217 Color: 747

Bin 665: 3468 of cap free
Amount of items: 3
Items: 
Size: 379367 Color: 1502
Size: 318440 Color: 1010
Size: 298726 Color: 814

Bin 666: 4826 of cap free
Amount of items: 3
Items: 
Size: 367509 Color: 1424
Size: 362888 Color: 1382
Size: 264778 Color: 330

Bin 667: 241372 of cap free
Amount of items: 2
Items: 
Size: 487715 Color: 1965
Size: 270914 Color: 451

Bin 668: 740481 of cap free
Amount of items: 1
Items: 
Size: 259520 Color: 240

Total size: 667000667
Total free space: 1000001

