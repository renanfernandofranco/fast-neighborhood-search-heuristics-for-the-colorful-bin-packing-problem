Capicity Bin: 19712
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 14482 Color: 467
Size: 4362 Color: 336
Size: 868 Color: 143

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 14511 Color: 468
Size: 4335 Color: 335
Size: 866 Color: 142

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 15085 Color: 480
Size: 3843 Color: 322
Size: 784 Color: 135

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 15086 Color: 481
Size: 4450 Color: 340
Size: 176 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 15529 Color: 489
Size: 3219 Color: 300
Size: 964 Color: 152

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 15542 Color: 490
Size: 3478 Color: 313
Size: 692 Color: 119

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 15550 Color: 491
Size: 3614 Color: 318
Size: 548 Color: 89

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 15568 Color: 492
Size: 3604 Color: 317
Size: 540 Color: 87

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 15606 Color: 494
Size: 3422 Color: 310
Size: 684 Color: 117

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 15640 Color: 496
Size: 2776 Color: 280
Size: 1296 Color: 180

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15816 Color: 502
Size: 3400 Color: 308
Size: 496 Color: 79

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15875 Color: 505
Size: 3165 Color: 296
Size: 672 Color: 115

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15879 Color: 506
Size: 3195 Color: 298
Size: 638 Color: 106

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15982 Color: 510
Size: 2058 Color: 238
Size: 1672 Color: 205

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 15990 Color: 512
Size: 3304 Color: 304
Size: 418 Color: 56

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 16030 Color: 514
Size: 2802 Color: 282
Size: 880 Color: 145

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 16046 Color: 515
Size: 2568 Color: 270
Size: 1098 Color: 164

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 16144 Color: 516
Size: 2704 Color: 276
Size: 864 Color: 140

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 16328 Color: 521
Size: 1960 Color: 228
Size: 1424 Color: 188

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16350 Color: 523
Size: 2706 Color: 277
Size: 656 Color: 111

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 16764 Color: 538
Size: 1508 Color: 194
Size: 1440 Color: 191

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 540
Size: 2168 Color: 244
Size: 768 Color: 129

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16784 Color: 541
Size: 2290 Color: 252
Size: 638 Color: 107

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16793 Color: 543
Size: 2299 Color: 255
Size: 620 Color: 102

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16826 Color: 544
Size: 2296 Color: 254
Size: 590 Color: 97

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16829 Color: 545
Size: 2403 Color: 258
Size: 480 Color: 70

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16948 Color: 546
Size: 1892 Color: 226
Size: 872 Color: 144

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16966 Color: 548
Size: 2308 Color: 256
Size: 438 Color: 60

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16968 Color: 549
Size: 1880 Color: 224
Size: 864 Color: 141

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 17027 Color: 551
Size: 2041 Color: 235
Size: 644 Color: 110

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 17042 Color: 552
Size: 2058 Color: 237
Size: 612 Color: 100

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 17176 Color: 558
Size: 1768 Color: 213
Size: 768 Color: 131

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 17220 Color: 560
Size: 1850 Color: 222
Size: 642 Color: 109

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 17313 Color: 569
Size: 1715 Color: 208
Size: 684 Color: 118

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 17316 Color: 570
Size: 1308 Color: 181
Size: 1088 Color: 162

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 17321 Color: 571
Size: 2065 Color: 239
Size: 326 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 17328 Color: 572
Size: 1720 Color: 210
Size: 664 Color: 114

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 17329 Color: 573
Size: 1775 Color: 215
Size: 608 Color: 98

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 17350 Color: 574
Size: 1872 Color: 223
Size: 490 Color: 77

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 17444 Color: 576
Size: 1408 Color: 183
Size: 860 Color: 139

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 17445 Color: 577
Size: 1891 Color: 225
Size: 376 Color: 41

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17464 Color: 578
Size: 2004 Color: 231
Size: 244 Color: 11

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17488 Color: 580
Size: 2000 Color: 230
Size: 224 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17494 Color: 581
Size: 1808 Color: 218
Size: 410 Color: 53

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17552 Color: 586
Size: 1640 Color: 201
Size: 520 Color: 84

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17559 Color: 587
Size: 1795 Color: 217
Size: 358 Color: 34

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17592 Color: 588
Size: 1640 Color: 202
Size: 480 Color: 69

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17598 Color: 589
Size: 1762 Color: 212
Size: 352 Color: 27

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17608 Color: 590
Size: 1256 Color: 179
Size: 848 Color: 138

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17655 Color: 594
Size: 1645 Color: 204
Size: 412 Color: 54

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17660 Color: 596
Size: 1568 Color: 195
Size: 484 Color: 73

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17682 Color: 597
Size: 1678 Color: 206
Size: 352 Color: 28

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 17720 Color: 599
Size: 1432 Color: 190
Size: 560 Color: 91

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 14890 Color: 473
Size: 4821 Color: 347

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 15031 Color: 478
Size: 4248 Color: 332
Size: 432 Color: 59

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 15037 Color: 479
Size: 2852 Color: 284
Size: 1822 Color: 219

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 15899 Color: 507
Size: 3470 Color: 312
Size: 342 Color: 25

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 15988 Color: 511
Size: 3179 Color: 297
Size: 544 Color: 88

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 16570 Color: 531
Size: 2781 Color: 281
Size: 360 Color: 35

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 17067 Color: 553
Size: 2644 Color: 275

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 11056 Color: 420
Size: 8214 Color: 399
Size: 440 Color: 61

Bin 62: 2 of cap free
Amount of items: 4
Items: 
Size: 13320 Color: 452
Size: 5718 Color: 363
Size: 336 Color: 22
Size: 336 Color: 21

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 14374 Color: 462
Size: 5336 Color: 353

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 15378 Color: 486
Size: 4332 Color: 334

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 15590 Color: 493
Size: 2288 Color: 251
Size: 1832 Color: 220

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 15688 Color: 497
Size: 4022 Color: 329

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 16292 Color: 520
Size: 3418 Color: 309

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 16342 Color: 522
Size: 3368 Color: 307

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 16600 Color: 534
Size: 3110 Color: 294

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 16963 Color: 547
Size: 2747 Color: 279

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 17109 Color: 557
Size: 2601 Color: 272

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 17277 Color: 567
Size: 2433 Color: 261

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 17304 Color: 568
Size: 2406 Color: 259

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 17505 Color: 582
Size: 2205 Color: 249

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 17616 Color: 592
Size: 2094 Color: 243

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 17702 Color: 598
Size: 2008 Color: 232

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 12245 Color: 434
Size: 7096 Color: 380
Size: 368 Color: 39

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 13072 Color: 443
Size: 6285 Color: 371
Size: 352 Color: 32

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 14546 Color: 469
Size: 4363 Color: 337
Size: 800 Color: 136

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 15851 Color: 504
Size: 3858 Color: 324

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 16377 Color: 524
Size: 3332 Color: 306

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 16765 Color: 539
Size: 2944 Color: 286

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 17249 Color: 564
Size: 2460 Color: 267

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 17656 Color: 595
Size: 2053 Color: 236

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 17739 Color: 600
Size: 1970 Color: 229

Bin 86: 4 of cap free
Amount of items: 7
Items: 
Size: 9862 Color: 409
Size: 2441 Color: 262
Size: 2188 Color: 247
Size: 2072 Color: 240
Size: 2033 Color: 234
Size: 616 Color: 101
Size: 496 Color: 78

Bin 87: 4 of cap free
Amount of items: 5
Items: 
Size: 11196 Color: 424
Size: 7080 Color: 378
Size: 608 Color: 99
Size: 416 Color: 55
Size: 408 Color: 52

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 12205 Color: 433
Size: 7127 Color: 382
Size: 376 Color: 40

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 12854 Color: 440
Size: 6494 Color: 374
Size: 360 Color: 36

Bin 90: 5 of cap free
Amount of items: 2
Items: 
Size: 16786 Color: 542
Size: 2921 Color: 285

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 17222 Color: 561
Size: 2485 Color: 268

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 17265 Color: 565
Size: 2442 Color: 263

Bin 93: 6 of cap free
Amount of items: 3
Items: 
Size: 13887 Color: 457
Size: 5499 Color: 359
Size: 320 Color: 15

Bin 94: 6 of cap free
Amount of items: 2
Items: 
Size: 16468 Color: 528
Size: 3238 Color: 302

Bin 95: 6 of cap free
Amount of items: 2
Items: 
Size: 16648 Color: 535
Size: 3058 Color: 290

Bin 96: 6 of cap free
Amount of items: 2
Items: 
Size: 17106 Color: 556
Size: 2600 Color: 271

Bin 97: 7 of cap free
Amount of items: 2
Items: 
Size: 17201 Color: 559
Size: 2504 Color: 269

Bin 98: 7 of cap free
Amount of items: 2
Items: 
Size: 17273 Color: 566
Size: 2432 Color: 260

Bin 99: 8 of cap free
Amount of items: 5
Items: 
Size: 14972 Color: 476
Size: 4510 Color: 342
Size: 104 Color: 2
Size: 64 Color: 1
Size: 54 Color: 0

Bin 100: 8 of cap free
Amount of items: 2
Items: 
Size: 15176 Color: 483
Size: 4528 Color: 343

Bin 101: 8 of cap free
Amount of items: 2
Items: 
Size: 15240 Color: 484
Size: 4464 Color: 341

Bin 102: 8 of cap free
Amount of items: 2
Items: 
Size: 16008 Color: 513
Size: 3696 Color: 319

Bin 103: 8 of cap free
Amount of items: 2
Items: 
Size: 16712 Color: 536
Size: 2992 Color: 288

Bin 104: 8 of cap free
Amount of items: 2
Items: 
Size: 17478 Color: 579
Size: 2226 Color: 250

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 17530 Color: 585
Size: 2174 Color: 246

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 17611 Color: 591
Size: 2093 Color: 242

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 17620 Color: 593
Size: 2084 Color: 241

Bin 108: 9 of cap free
Amount of items: 2
Items: 
Size: 17081 Color: 554
Size: 2622 Color: 274

Bin 109: 9 of cap free
Amount of items: 2
Items: 
Size: 17246 Color: 563
Size: 2457 Color: 266

Bin 110: 10 of cap free
Amount of items: 9
Items: 
Size: 9858 Color: 406
Size: 1632 Color: 198
Size: 1632 Color: 197
Size: 1632 Color: 196
Size: 1508 Color: 193
Size: 1472 Color: 192
Size: 928 Color: 149
Size: 520 Color: 85
Size: 520 Color: 83

Bin 111: 10 of cap free
Amount of items: 7
Items: 
Size: 9864 Color: 410
Size: 3070 Color: 291
Size: 2824 Color: 283
Size: 2448 Color: 264
Size: 520 Color: 86
Size: 488 Color: 76
Size: 488 Color: 75

Bin 112: 10 of cap free
Amount of items: 3
Items: 
Size: 13927 Color: 458
Size: 5503 Color: 360
Size: 272 Color: 14

Bin 113: 10 of cap free
Amount of items: 2
Items: 
Size: 15845 Color: 503
Size: 3857 Color: 323

Bin 114: 10 of cap free
Amount of items: 2
Items: 
Size: 16392 Color: 525
Size: 3310 Color: 305

Bin 115: 11 of cap free
Amount of items: 2
Items: 
Size: 17508 Color: 583
Size: 2193 Color: 248

Bin 116: 12 of cap free
Amount of items: 3
Items: 
Size: 14680 Color: 472
Size: 4884 Color: 350
Size: 136 Color: 5

Bin 117: 12 of cap free
Amount of items: 2
Items: 
Size: 14892 Color: 474
Size: 4808 Color: 346

Bin 118: 12 of cap free
Amount of items: 2
Items: 
Size: 15716 Color: 498
Size: 3984 Color: 328

Bin 119: 13 of cap free
Amount of items: 2
Items: 
Size: 15915 Color: 508
Size: 3784 Color: 321

Bin 120: 13 of cap free
Amount of items: 2
Items: 
Size: 16591 Color: 533
Size: 3108 Color: 293

Bin 121: 13 of cap free
Amount of items: 2
Items: 
Size: 17092 Color: 555
Size: 2607 Color: 273

Bin 122: 13 of cap free
Amount of items: 2
Items: 
Size: 17528 Color: 584
Size: 2171 Color: 245

Bin 123: 14 of cap free
Amount of items: 5
Items: 
Size: 9876 Color: 412
Size: 8200 Color: 393
Size: 656 Color: 113
Size: 486 Color: 74
Size: 480 Color: 72

Bin 124: 14 of cap free
Amount of items: 2
Items: 
Size: 12328 Color: 437
Size: 7370 Color: 388

Bin 125: 15 of cap free
Amount of items: 2
Items: 
Size: 15396 Color: 487
Size: 4301 Color: 333

Bin 126: 15 of cap free
Amount of items: 2
Items: 
Size: 16417 Color: 526
Size: 3280 Color: 303

Bin 127: 15 of cap free
Amount of items: 2
Items: 
Size: 17368 Color: 575
Size: 2329 Color: 257

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 16976 Color: 550
Size: 2720 Color: 278

Bin 129: 16 of cap free
Amount of items: 2
Items: 
Size: 17240 Color: 562
Size: 2456 Color: 265

Bin 130: 17 of cap free
Amount of items: 3
Items: 
Size: 14384 Color: 463
Size: 3020 Color: 289
Size: 2291 Color: 253

Bin 131: 17 of cap free
Amount of items: 2
Items: 
Size: 16496 Color: 529
Size: 3199 Color: 299

Bin 132: 18 of cap free
Amount of items: 2
Items: 
Size: 15614 Color: 495
Size: 4080 Color: 330

Bin 133: 18 of cap free
Amount of items: 2
Items: 
Size: 16207 Color: 519
Size: 3487 Color: 315

Bin 134: 19 of cap free
Amount of items: 2
Items: 
Size: 15792 Color: 501
Size: 3901 Color: 326

Bin 135: 20 of cap free
Amount of items: 2
Items: 
Size: 15280 Color: 485
Size: 4412 Color: 338

Bin 136: 20 of cap free
Amount of items: 2
Items: 
Size: 15752 Color: 499
Size: 3940 Color: 327

Bin 137: 20 of cap free
Amount of items: 2
Items: 
Size: 16548 Color: 530
Size: 3144 Color: 295

Bin 138: 21 of cap free
Amount of items: 3
Items: 
Size: 13115 Color: 445
Size: 6224 Color: 369
Size: 352 Color: 30

Bin 139: 21 of cap free
Amount of items: 3
Items: 
Size: 14477 Color: 466
Size: 3438 Color: 311
Size: 1776 Color: 216

Bin 140: 22 of cap free
Amount of items: 2
Items: 
Size: 14088 Color: 460
Size: 5602 Color: 362

Bin 141: 23 of cap free
Amount of items: 2
Items: 
Size: 15101 Color: 482
Size: 4588 Color: 344

Bin 142: 23 of cap free
Amount of items: 2
Items: 
Size: 15489 Color: 488
Size: 4200 Color: 331

Bin 143: 23 of cap free
Amount of items: 2
Items: 
Size: 16466 Color: 527
Size: 3223 Color: 301

Bin 144: 24 of cap free
Amount of items: 2
Items: 
Size: 16167 Color: 517
Size: 3521 Color: 316

Bin 145: 26 of cap free
Amount of items: 2
Items: 
Size: 16731 Color: 537
Size: 2955 Color: 287

Bin 146: 27 of cap free
Amount of items: 7
Items: 
Size: 9861 Color: 408
Size: 2033 Color: 233
Size: 1935 Color: 227
Size: 1844 Color: 221
Size: 1768 Color: 214
Size: 1748 Color: 211
Size: 496 Color: 80

Bin 147: 27 of cap free
Amount of items: 5
Items: 
Size: 11161 Color: 423
Size: 7076 Color: 377
Size: 584 Color: 96
Size: 432 Color: 58
Size: 432 Color: 57

Bin 148: 28 of cap free
Amount of items: 3
Items: 
Size: 13109 Color: 444
Size: 6223 Color: 368
Size: 352 Color: 31

Bin 149: 28 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 449
Size: 6168 Color: 367
Size: 336 Color: 23

Bin 150: 28 of cap free
Amount of items: 2
Items: 
Size: 14988 Color: 477
Size: 4696 Color: 345

Bin 151: 31 of cap free
Amount of items: 2
Items: 
Size: 16585 Color: 532
Size: 3096 Color: 292

Bin 152: 32 of cap free
Amount of items: 2
Items: 
Size: 13264 Color: 451
Size: 6416 Color: 373

Bin 153: 32 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 459
Size: 5384 Color: 354
Size: 256 Color: 13

Bin 154: 32 of cap free
Amount of items: 2
Items: 
Size: 14288 Color: 461
Size: 5392 Color: 355

Bin 155: 32 of cap free
Amount of items: 2
Items: 
Size: 15944 Color: 509
Size: 3736 Color: 320

Bin 156: 39 of cap free
Amount of items: 3
Items: 
Size: 13179 Color: 448
Size: 6158 Color: 366
Size: 336 Color: 24

Bin 157: 39 of cap free
Amount of items: 2
Items: 
Size: 15776 Color: 500
Size: 3897 Color: 325

Bin 158: 40 of cap free
Amount of items: 2
Items: 
Size: 16192 Color: 518
Size: 3480 Color: 314

Bin 159: 42 of cap free
Amount of items: 3
Items: 
Size: 14420 Color: 465
Size: 5026 Color: 352
Size: 224 Color: 9

Bin 160: 48 of cap free
Amount of items: 3
Items: 
Size: 14386 Color: 464
Size: 5022 Color: 351
Size: 256 Color: 12

Bin 161: 50 of cap free
Amount of items: 4
Items: 
Size: 14960 Color: 475
Size: 4442 Color: 339
Size: 132 Color: 4
Size: 128 Color: 3

Bin 162: 52 of cap free
Amount of items: 3
Items: 
Size: 13164 Color: 447
Size: 6152 Color: 365
Size: 344 Color: 26

Bin 163: 53 of cap free
Amount of items: 2
Items: 
Size: 11121 Color: 422
Size: 8538 Color: 401

Bin 164: 57 of cap free
Amount of items: 3
Items: 
Size: 12171 Color: 432
Size: 7100 Color: 381
Size: 384 Color: 42

Bin 165: 57 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 446
Size: 6140 Color: 364
Size: 352 Color: 29

Bin 166: 61 of cap free
Amount of items: 3
Items: 
Size: 13872 Color: 456
Size: 5459 Color: 358
Size: 320 Color: 16

Bin 167: 64 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 471
Size: 4880 Color: 349
Size: 152 Color: 6

Bin 168: 72 of cap free
Amount of items: 2
Items: 
Size: 12344 Color: 438
Size: 7296 Color: 387

Bin 169: 76 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 450
Size: 6380 Color: 372

Bin 170: 79 of cap free
Amount of items: 3
Items: 
Size: 13860 Color: 455
Size: 5445 Color: 357
Size: 328 Color: 18

Bin 171: 80 of cap free
Amount of items: 2
Items: 
Size: 9872 Color: 411
Size: 9760 Color: 404

Bin 172: 82 of cap free
Amount of items: 4
Items: 
Size: 12348 Color: 439
Size: 6546 Color: 375
Size: 368 Color: 38
Size: 368 Color: 37

Bin 173: 104 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 428
Size: 7984 Color: 392
Size: 396 Color: 46

Bin 174: 104 of cap free
Amount of items: 2
Items: 
Size: 12324 Color: 436
Size: 7284 Color: 386

Bin 175: 106 of cap free
Amount of items: 19
Items: 
Size: 1250 Color: 178
Size: 1248 Color: 177
Size: 1248 Color: 176
Size: 1244 Color: 175
Size: 1224 Color: 174
Size: 1216 Color: 173
Size: 1216 Color: 172
Size: 1216 Color: 171
Size: 1184 Color: 170
Size: 1140 Color: 169
Size: 1120 Color: 168
Size: 1120 Color: 167
Size: 1104 Color: 166
Size: 1100 Color: 165
Size: 632 Color: 104
Size: 624 Color: 103
Size: 576 Color: 95
Size: 576 Color: 94
Size: 568 Color: 93

Bin 176: 107 of cap free
Amount of items: 3
Items: 
Size: 12990 Color: 442
Size: 6257 Color: 370
Size: 358 Color: 33

Bin 177: 119 of cap free
Amount of items: 3
Items: 
Size: 12048 Color: 431
Size: 7161 Color: 384
Size: 384 Color: 43

Bin 178: 130 of cap free
Amount of items: 2
Items: 
Size: 12862 Color: 441
Size: 6720 Color: 376

Bin 179: 138 of cap free
Amount of items: 3
Items: 
Size: 13690 Color: 454
Size: 5552 Color: 361
Size: 332 Color: 19

Bin 180: 138 of cap free
Amount of items: 3
Items: 
Size: 14551 Color: 470
Size: 4855 Color: 348
Size: 168 Color: 7

Bin 181: 144 of cap free
Amount of items: 4
Items: 
Size: 11212 Color: 426
Size: 7550 Color: 390
Size: 406 Color: 49
Size: 400 Color: 48

Bin 182: 148 of cap free
Amount of items: 4
Items: 
Size: 11208 Color: 425
Size: 7542 Color: 389
Size: 408 Color: 51
Size: 406 Color: 50

Bin 183: 185 of cap free
Amount of items: 3
Items: 
Size: 10870 Color: 419
Size: 8213 Color: 398
Size: 444 Color: 62

Bin 184: 224 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 435
Size: 7216 Color: 385

Bin 185: 237 of cap free
Amount of items: 3
Items: 
Size: 11922 Color: 430
Size: 7161 Color: 383
Size: 392 Color: 44

Bin 186: 240 of cap free
Amount of items: 4
Items: 
Size: 9880 Color: 413
Size: 8640 Color: 402
Size: 480 Color: 71
Size: 472 Color: 68

Bin 187: 252 of cap free
Amount of items: 3
Items: 
Size: 13682 Color: 453
Size: 5444 Color: 356
Size: 334 Color: 20

Bin 188: 319 of cap free
Amount of items: 9
Items: 
Size: 9857 Color: 405
Size: 1430 Color: 189
Size: 1416 Color: 187
Size: 1416 Color: 186
Size: 1408 Color: 185
Size: 1408 Color: 184
Size: 1344 Color: 182
Size: 560 Color: 92
Size: 554 Color: 90

Bin 189: 328 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 427
Size: 7760 Color: 391
Size: 400 Color: 47

Bin 190: 348 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 414
Size: 8746 Color: 403
Size: 458 Color: 67

Bin 191: 348 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 418
Size: 8212 Color: 397
Size: 448 Color: 63

Bin 192: 351 of cap free
Amount of items: 3
Items: 
Size: 10702 Color: 417
Size: 8211 Color: 396
Size: 448 Color: 64

Bin 193: 377 of cap free
Amount of items: 2
Items: 
Size: 11119 Color: 421
Size: 8216 Color: 400

Bin 194: 378 of cap free
Amount of items: 3
Items: 
Size: 11858 Color: 429
Size: 7084 Color: 379
Size: 392 Color: 45

Bin 195: 386 of cap free
Amount of items: 3
Items: 
Size: 10662 Color: 416
Size: 8208 Color: 395
Size: 456 Color: 65

Bin 196: 398 of cap free
Amount of items: 3
Items: 
Size: 10654 Color: 415
Size: 8204 Color: 394
Size: 456 Color: 66

Bin 197: 504 of cap free
Amount of items: 8
Items: 
Size: 9860 Color: 407
Size: 1716 Color: 209
Size: 1694 Color: 207
Size: 1642 Color: 203
Size: 1640 Color: 200
Size: 1632 Color: 199
Size: 512 Color: 82
Size: 512 Color: 81

Bin 198: 608 of cap free
Amount of items: 22
Items: 
Size: 1090 Color: 163
Size: 1088 Color: 161
Size: 1088 Color: 160
Size: 1072 Color: 159
Size: 1056 Color: 158
Size: 1056 Color: 157
Size: 1004 Color: 156
Size: 1000 Color: 155
Size: 970 Color: 154
Size: 968 Color: 153
Size: 960 Color: 151
Size: 928 Color: 150
Size: 720 Color: 125
Size: 712 Color: 124
Size: 702 Color: 123
Size: 696 Color: 122
Size: 692 Color: 121
Size: 692 Color: 120
Size: 680 Color: 116
Size: 656 Color: 112
Size: 640 Color: 108
Size: 634 Color: 105

Bin 199: 10892 of cap free
Amount of items: 11
Items: 
Size: 896 Color: 148
Size: 888 Color: 147
Size: 884 Color: 146
Size: 832 Color: 137
Size: 780 Color: 134
Size: 778 Color: 133
Size: 770 Color: 132
Size: 768 Color: 130
Size: 752 Color: 128
Size: 736 Color: 127
Size: 736 Color: 126

Total size: 3902976
Total free space: 19712

