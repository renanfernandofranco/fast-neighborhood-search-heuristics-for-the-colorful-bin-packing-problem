Capicity Bin: 1972
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1427 Color: 162
Size: 455 Color: 111
Size: 90 Color: 47

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1431 Color: 163
Size: 451 Color: 110
Size: 90 Color: 49

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1451 Color: 165
Size: 485 Color: 115
Size: 36 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 166
Size: 429 Color: 108
Size: 84 Color: 45

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 172
Size: 350 Color: 101
Size: 68 Color: 38

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1631 Color: 179
Size: 181 Color: 72
Size: 160 Color: 63

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 180
Size: 283 Color: 91
Size: 56 Color: 30

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 181
Size: 285 Color: 92
Size: 34 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 183
Size: 213 Color: 78
Size: 90 Color: 48

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 184
Size: 164 Color: 68
Size: 134 Color: 60

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 185
Size: 233 Color: 84
Size: 64 Color: 33

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 188
Size: 227 Color: 83
Size: 52 Color: 28

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 192
Size: 250 Color: 88
Size: 4 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 193
Size: 171 Color: 69
Size: 76 Color: 41

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1747 Color: 195
Size: 221 Color: 82
Size: 4 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 196
Size: 211 Color: 77
Size: 8 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1755 Color: 197
Size: 175 Color: 71
Size: 42 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1766 Color: 200
Size: 160 Color: 64
Size: 46 Color: 21

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1267 Color: 152
Size: 664 Color: 123
Size: 40 Color: 11

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1305 Color: 154
Size: 666 Color: 124

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 157
Size: 589 Color: 122
Size: 32 Color: 6

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1414 Color: 160
Size: 557 Color: 120

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 177
Size: 184 Color: 73
Size: 162 Color: 67

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 182
Size: 305 Color: 96
Size: 8 Color: 2

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1676 Color: 186
Size: 295 Color: 94

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 187
Size: 286 Color: 93

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1709 Color: 190
Size: 262 Color: 89

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1730 Color: 194
Size: 241 Color: 85

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1769 Color: 201
Size: 202 Color: 75

Bin 30: 2 of cap free
Amount of items: 5
Items: 
Size: 989 Color: 139
Size: 466 Color: 114
Size: 349 Color: 100
Size: 110 Color: 55
Size: 56 Color: 29

Bin 31: 2 of cap free
Amount of items: 5
Items: 
Size: 997 Color: 142
Size: 813 Color: 132
Size: 64 Color: 32
Size: 48 Color: 25
Size: 48 Color: 24

Bin 32: 2 of cap free
Amount of items: 5
Items: 
Size: 998 Color: 143
Size: 814 Color: 133
Size: 64 Color: 34
Size: 48 Color: 23
Size: 46 Color: 22

Bin 33: 2 of cap free
Amount of items: 5
Items: 
Size: 1001 Color: 144
Size: 817 Color: 134
Size: 64 Color: 35
Size: 44 Color: 20
Size: 44 Color: 19

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 161
Size: 327 Color: 99
Size: 220 Color: 81

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1514 Color: 169
Size: 456 Color: 112

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1581 Color: 174
Size: 389 Color: 105

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 178
Size: 248 Color: 86
Size: 92 Color: 50

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 1756 Color: 198
Size: 214 Color: 79

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 1763 Color: 199
Size: 207 Color: 76

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1547 Color: 171
Size: 422 Color: 107

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 1587 Color: 176
Size: 382 Color: 104

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1309 Color: 155
Size: 355 Color: 102
Size: 304 Color: 95

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1701 Color: 189
Size: 267 Color: 90

Bin 44: 5 of cap free
Amount of items: 4
Items: 
Size: 1313 Color: 156
Size: 586 Color: 121
Size: 36 Color: 10
Size: 32 Color: 7

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1507 Color: 168
Size: 459 Color: 113

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 1717 Color: 191
Size: 249 Color: 87

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1586 Color: 175
Size: 379 Color: 103

Bin 48: 8 of cap free
Amount of items: 5
Items: 
Size: 993 Color: 141
Size: 811 Color: 131
Size: 56 Color: 31
Size: 52 Color: 27
Size: 52 Color: 26

Bin 49: 9 of cap free
Amount of items: 4
Items: 
Size: 1171 Color: 149
Size: 710 Color: 129
Size: 42 Color: 15
Size: 40 Color: 14

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 1555 Color: 173
Size: 408 Color: 106

Bin 51: 11 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 167
Size: 491 Color: 116

Bin 52: 12 of cap free
Amount of items: 4
Items: 
Size: 1167 Color: 148
Size: 707 Color: 128
Size: 44 Color: 18
Size: 42 Color: 16

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 1525 Color: 170
Size: 435 Color: 109

Bin 54: 13 of cap free
Amount of items: 2
Items: 
Size: 1163 Color: 147
Size: 796 Color: 130

Bin 55: 15 of cap free
Amount of items: 2
Items: 
Size: 1435 Color: 164
Size: 522 Color: 117

Bin 56: 18 of cap free
Amount of items: 3
Items: 
Size: 1385 Color: 159
Size: 553 Color: 119
Size: 16 Color: 4

Bin 57: 21 of cap free
Amount of items: 3
Items: 
Size: 1240 Color: 151
Size: 671 Color: 126
Size: 40 Color: 12

Bin 58: 25 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 158
Size: 551 Color: 118
Size: 32 Color: 5

Bin 59: 26 of cap free
Amount of items: 2
Items: 
Size: 1125 Color: 146
Size: 821 Color: 135

Bin 60: 27 of cap free
Amount of items: 2
Items: 
Size: 1270 Color: 153
Size: 675 Color: 127

Bin 61: 29 of cap free
Amount of items: 2
Items: 
Size: 1122 Color: 145
Size: 821 Color: 136

Bin 62: 37 of cap free
Amount of items: 3
Items: 
Size: 1226 Color: 150
Size: 669 Color: 125
Size: 40 Color: 13

Bin 63: 44 of cap free
Amount of items: 2
Items: 
Size: 990 Color: 140
Size: 938 Color: 137

Bin 64: 54 of cap free
Amount of items: 5
Items: 
Size: 987 Color: 138
Size: 322 Color: 98
Size: 321 Color: 97
Size: 220 Color: 80
Size: 68 Color: 36

Bin 65: 91 of cap free
Amount of items: 16
Items: 
Size: 189 Color: 74
Size: 174 Color: 70
Size: 162 Color: 66
Size: 162 Color: 65
Size: 140 Color: 62
Size: 140 Color: 61
Size: 134 Color: 59
Size: 132 Color: 58
Size: 116 Color: 57
Size: 86 Color: 46
Size: 80 Color: 44
Size: 80 Color: 43
Size: 76 Color: 42
Size: 72 Color: 40
Size: 70 Color: 39
Size: 68 Color: 37

Bin 66: 1442 of cap free
Amount of items: 5
Items: 
Size: 116 Color: 56
Size: 110 Color: 54
Size: 108 Color: 53
Size: 100 Color: 52
Size: 96 Color: 51

Total size: 128180
Total free space: 1972

