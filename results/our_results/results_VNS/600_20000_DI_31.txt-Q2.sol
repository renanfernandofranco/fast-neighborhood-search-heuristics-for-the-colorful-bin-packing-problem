Capicity Bin: 16608
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 8308 Color: 1
Size: 1737 Color: 1
Size: 1651 Color: 1
Size: 1582 Color: 0
Size: 1496 Color: 0
Size: 1386 Color: 1
Size: 448 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 8328 Color: 1
Size: 5854 Color: 0
Size: 2048 Color: 1
Size: 378 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9394 Color: 1
Size: 6902 Color: 0
Size: 312 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9470 Color: 0
Size: 5938 Color: 1
Size: 1200 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9791 Color: 0
Size: 5681 Color: 0
Size: 1136 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10712 Color: 0
Size: 5292 Color: 1
Size: 604 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 0
Size: 4534 Color: 0
Size: 294 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 1
Size: 4546 Color: 0
Size: 300 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11778 Color: 1
Size: 3448 Color: 0
Size: 1382 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12270 Color: 1
Size: 4004 Color: 1
Size: 334 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 1
Size: 3846 Color: 0
Size: 418 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12830 Color: 0
Size: 3466 Color: 0
Size: 312 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 0
Size: 2602 Color: 0
Size: 784 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 0
Size: 2776 Color: 1
Size: 560 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 0
Size: 2988 Color: 1
Size: 344 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13329 Color: 0
Size: 2741 Color: 1
Size: 538 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13942 Color: 1
Size: 1862 Color: 1
Size: 804 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 0
Size: 1988 Color: 1
Size: 680 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 1
Size: 1476 Color: 0
Size: 1188 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13957 Color: 1
Size: 2079 Color: 1
Size: 572 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 0
Size: 2260 Color: 1
Size: 280 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 1
Size: 1576 Color: 1
Size: 976 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14123 Color: 1
Size: 1621 Color: 1
Size: 864 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14132 Color: 1
Size: 2068 Color: 1
Size: 408 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 1
Size: 2232 Color: 1
Size: 212 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14335 Color: 1
Size: 1649 Color: 1
Size: 624 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 0
Size: 1240 Color: 0
Size: 1040 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14396 Color: 0
Size: 1932 Color: 1
Size: 280 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14556 Color: 0
Size: 1380 Color: 0
Size: 672 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 1
Size: 1382 Color: 1
Size: 704 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14596 Color: 0
Size: 1448 Color: 1
Size: 564 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14558 Color: 1
Size: 1064 Color: 1
Size: 986 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 0
Size: 1420 Color: 0
Size: 572 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 1
Size: 1642 Color: 1
Size: 328 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14712 Color: 1
Size: 1532 Color: 0
Size: 364 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 0
Size: 1558 Color: 1
Size: 322 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14714 Color: 1
Size: 1190 Color: 1
Size: 704 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14742 Color: 0
Size: 1520 Color: 1
Size: 346 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14727 Color: 1
Size: 1467 Color: 0
Size: 414 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14817 Color: 0
Size: 1493 Color: 0
Size: 298 Color: 1

Bin 41: 1 of cap free
Amount of items: 9
Items: 
Size: 8309 Color: 0
Size: 1376 Color: 1
Size: 1312 Color: 1
Size: 1184 Color: 0
Size: 1182 Color: 1
Size: 1088 Color: 1
Size: 1056 Color: 0
Size: 620 Color: 1
Size: 480 Color: 0

Bin 42: 1 of cap free
Amount of items: 5
Items: 
Size: 8312 Color: 1
Size: 4791 Color: 1
Size: 2594 Color: 0
Size: 496 Color: 1
Size: 414 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 9463 Color: 0
Size: 6504 Color: 1
Size: 640 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 11804 Color: 0
Size: 4257 Color: 0
Size: 546 Color: 1

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 11826 Color: 0
Size: 4301 Color: 0
Size: 480 Color: 1

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 13265 Color: 1
Size: 2180 Color: 0
Size: 1162 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 14099 Color: 1
Size: 1320 Color: 0
Size: 1188 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 14091 Color: 0
Size: 2244 Color: 1
Size: 272 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 14212 Color: 1
Size: 2091 Color: 0
Size: 304 Color: 0

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 14319 Color: 1
Size: 2288 Color: 0

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 14697 Color: 0
Size: 1296 Color: 1
Size: 614 Color: 0

Bin 52: 2 of cap free
Amount of items: 38
Items: 
Size: 560 Color: 1
Size: 560 Color: 1
Size: 544 Color: 1
Size: 528 Color: 1
Size: 520 Color: 1
Size: 488 Color: 1
Size: 484 Color: 0
Size: 476 Color: 1
Size: 468 Color: 0
Size: 464 Color: 1
Size: 464 Color: 1
Size: 456 Color: 0
Size: 452 Color: 1
Size: 452 Color: 0
Size: 450 Color: 1
Size: 448 Color: 1
Size: 440 Color: 1
Size: 440 Color: 1
Size: 440 Color: 1
Size: 440 Color: 1
Size: 440 Color: 0
Size: 432 Color: 0
Size: 420 Color: 1
Size: 418 Color: 1
Size: 416 Color: 0
Size: 416 Color: 0
Size: 414 Color: 0
Size: 412 Color: 0
Size: 412 Color: 0
Size: 400 Color: 0
Size: 392 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 372 Color: 0
Size: 368 Color: 0
Size: 352 Color: 0
Size: 336 Color: 1
Size: 264 Color: 0

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 9467 Color: 0
Size: 5763 Color: 0
Size: 1376 Color: 1

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 9478 Color: 1
Size: 4604 Color: 1
Size: 2524 Color: 0

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 12356 Color: 0
Size: 2194 Color: 0
Size: 2056 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 1
Size: 2648 Color: 0
Size: 720 Color: 1

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 13260 Color: 1
Size: 2210 Color: 0
Size: 1136 Color: 1

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 13345 Color: 1
Size: 2071 Color: 1
Size: 1190 Color: 0

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 13866 Color: 1
Size: 2740 Color: 0

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 14004 Color: 0
Size: 2074 Color: 1
Size: 528 Color: 0

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 13158 Color: 0
Size: 2257 Color: 1
Size: 1190 Color: 1

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 13236 Color: 0
Size: 3073 Color: 1
Size: 296 Color: 0

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 13437 Color: 1
Size: 3168 Color: 0

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 0
Size: 2681 Color: 1

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 14601 Color: 1
Size: 2004 Color: 0

Bin 66: 4 of cap free
Amount of items: 24
Items: 
Size: 860 Color: 1
Size: 850 Color: 1
Size: 840 Color: 0
Size: 816 Color: 0
Size: 804 Color: 0
Size: 800 Color: 1
Size: 800 Color: 0
Size: 768 Color: 0
Size: 754 Color: 0
Size: 744 Color: 0
Size: 720 Color: 0
Size: 704 Color: 1
Size: 692 Color: 1
Size: 688 Color: 1
Size: 688 Color: 1
Size: 688 Color: 1
Size: 624 Color: 1
Size: 624 Color: 1
Size: 592 Color: 1
Size: 576 Color: 0
Size: 552 Color: 0
Size: 546 Color: 0
Size: 534 Color: 0
Size: 340 Color: 1

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 9188 Color: 1
Size: 6906 Color: 0
Size: 510 Color: 1

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 10196 Color: 0
Size: 5928 Color: 0
Size: 480 Color: 1

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 11848 Color: 1
Size: 4580 Color: 0
Size: 176 Color: 1

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 12137 Color: 1
Size: 4211 Color: 1
Size: 256 Color: 0

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 0
Size: 2606 Color: 0
Size: 998 Color: 1

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 13588 Color: 0
Size: 3016 Color: 1

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 13901 Color: 0
Size: 2703 Color: 1

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 0
Size: 2418 Color: 1

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 10024 Color: 1
Size: 6187 Color: 0
Size: 392 Color: 1

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 10792 Color: 1
Size: 5811 Color: 0

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 13741 Color: 0
Size: 2862 Color: 1

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 0
Size: 7260 Color: 0
Size: 534 Color: 1

Bin 79: 6 of cap free
Amount of items: 4
Items: 
Size: 10156 Color: 0
Size: 5950 Color: 1
Size: 264 Color: 1
Size: 232 Color: 0

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 12450 Color: 1
Size: 4152 Color: 0

Bin 81: 6 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 0
Size: 2391 Color: 1
Size: 96 Color: 0

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 14374 Color: 1
Size: 2228 Color: 0

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 13192 Color: 0
Size: 3409 Color: 1

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 13393 Color: 1
Size: 3208 Color: 0

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 13958 Color: 0
Size: 2643 Color: 1

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 1
Size: 2211 Color: 0

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 10681 Color: 1
Size: 5919 Color: 0

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 0
Size: 2022 Color: 1
Size: 1742 Color: 1

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 13146 Color: 1
Size: 3454 Color: 0

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 14248 Color: 0
Size: 2352 Color: 1

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 14525 Color: 1
Size: 2075 Color: 0

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 14800 Color: 1
Size: 1800 Color: 0

Bin 93: 9 of cap free
Amount of items: 4
Items: 
Size: 8317 Color: 0
Size: 4856 Color: 0
Size: 1895 Color: 1
Size: 1531 Color: 1

Bin 94: 9 of cap free
Amount of items: 3
Items: 
Size: 8681 Color: 1
Size: 5942 Color: 0
Size: 1976 Color: 1

Bin 95: 9 of cap free
Amount of items: 2
Items: 
Size: 14665 Color: 1
Size: 1934 Color: 0

Bin 96: 9 of cap free
Amount of items: 2
Items: 
Size: 14773 Color: 1
Size: 1826 Color: 0

Bin 97: 9 of cap free
Amount of items: 2
Items: 
Size: 14908 Color: 1
Size: 1691 Color: 0

Bin 98: 10 of cap free
Amount of items: 5
Items: 
Size: 8316 Color: 0
Size: 3816 Color: 0
Size: 1844 Color: 1
Size: 1382 Color: 0
Size: 1240 Color: 1

Bin 99: 10 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 1
Size: 2136 Color: 0
Size: 2082 Color: 1

Bin 100: 10 of cap free
Amount of items: 2
Items: 
Size: 14102 Color: 0
Size: 2496 Color: 1

Bin 101: 12 of cap free
Amount of items: 2
Items: 
Size: 11994 Color: 1
Size: 4602 Color: 0

Bin 102: 12 of cap free
Amount of items: 2
Items: 
Size: 13710 Color: 1
Size: 2886 Color: 0

Bin 103: 12 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 0
Size: 2796 Color: 1

Bin 104: 12 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 0
Size: 2286 Color: 1

Bin 105: 12 of cap free
Amount of items: 2
Items: 
Size: 14470 Color: 0
Size: 2126 Color: 1

Bin 106: 14 of cap free
Amount of items: 2
Items: 
Size: 11384 Color: 0
Size: 5210 Color: 1

Bin 107: 15 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 0
Size: 3105 Color: 1
Size: 192 Color: 1

Bin 108: 16 of cap free
Amount of items: 3
Items: 
Size: 9140 Color: 1
Size: 6014 Color: 0
Size: 1438 Color: 1

Bin 109: 16 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 0
Size: 6188 Color: 1
Size: 192 Color: 1

Bin 110: 16 of cap free
Amount of items: 2
Items: 
Size: 12232 Color: 0
Size: 4360 Color: 1

Bin 111: 17 of cap free
Amount of items: 3
Items: 
Size: 10709 Color: 0
Size: 4318 Color: 1
Size: 1564 Color: 0

Bin 112: 17 of cap free
Amount of items: 2
Items: 
Size: 12565 Color: 0
Size: 4026 Color: 1

Bin 113: 17 of cap free
Amount of items: 2
Items: 
Size: 13894 Color: 0
Size: 2697 Color: 1

Bin 114: 18 of cap free
Amount of items: 2
Items: 
Size: 14769 Color: 1
Size: 1821 Color: 0

Bin 115: 18 of cap free
Amount of items: 2
Items: 
Size: 14946 Color: 1
Size: 1644 Color: 0

Bin 116: 19 of cap free
Amount of items: 2
Items: 
Size: 11447 Color: 0
Size: 5142 Color: 1

Bin 117: 19 of cap free
Amount of items: 2
Items: 
Size: 13562 Color: 1
Size: 3027 Color: 0

Bin 118: 20 of cap free
Amount of items: 2
Items: 
Size: 11092 Color: 1
Size: 5496 Color: 0

Bin 119: 21 of cap free
Amount of items: 2
Items: 
Size: 12860 Color: 0
Size: 3727 Color: 1

Bin 120: 21 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 1
Size: 2099 Color: 0

Bin 121: 21 of cap free
Amount of items: 2
Items: 
Size: 14888 Color: 0
Size: 1699 Color: 1

Bin 122: 22 of cap free
Amount of items: 2
Items: 
Size: 10358 Color: 0
Size: 6228 Color: 1

Bin 123: 22 of cap free
Amount of items: 2
Items: 
Size: 10442 Color: 0
Size: 6144 Color: 1

Bin 124: 24 of cap free
Amount of items: 3
Items: 
Size: 9459 Color: 0
Size: 5939 Color: 1
Size: 1186 Color: 0

Bin 125: 24 of cap free
Amount of items: 4
Items: 
Size: 11344 Color: 0
Size: 4920 Color: 1
Size: 172 Color: 0
Size: 148 Color: 1

Bin 126: 26 of cap free
Amount of items: 2
Items: 
Size: 13432 Color: 1
Size: 3150 Color: 0

Bin 127: 26 of cap free
Amount of items: 2
Items: 
Size: 14122 Color: 1
Size: 2460 Color: 0

Bin 128: 26 of cap free
Amount of items: 2
Items: 
Size: 14152 Color: 0
Size: 2430 Color: 1

Bin 129: 26 of cap free
Amount of items: 2
Items: 
Size: 14814 Color: 0
Size: 1768 Color: 1

Bin 130: 27 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 1
Size: 4941 Color: 0

Bin 131: 28 of cap free
Amount of items: 2
Items: 
Size: 13724 Color: 0
Size: 2856 Color: 1

Bin 132: 32 of cap free
Amount of items: 2
Items: 
Size: 14581 Color: 1
Size: 1995 Color: 0

Bin 133: 35 of cap free
Amount of items: 4
Items: 
Size: 14081 Color: 0
Size: 2276 Color: 1
Size: 192 Color: 0
Size: 24 Color: 1

Bin 134: 36 of cap free
Amount of items: 2
Items: 
Size: 9655 Color: 0
Size: 6917 Color: 1

Bin 135: 36 of cap free
Amount of items: 2
Items: 
Size: 10613 Color: 0
Size: 5959 Color: 1

Bin 136: 36 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 1
Size: 2878 Color: 0

Bin 137: 36 of cap free
Amount of items: 2
Items: 
Size: 14228 Color: 0
Size: 2344 Color: 1

Bin 138: 39 of cap free
Amount of items: 2
Items: 
Size: 14765 Color: 0
Size: 1804 Color: 1

Bin 139: 40 of cap free
Amount of items: 2
Items: 
Size: 14290 Color: 0
Size: 2278 Color: 1

Bin 140: 40 of cap free
Amount of items: 3
Items: 
Size: 14812 Color: 1
Size: 1684 Color: 0
Size: 72 Color: 1

Bin 141: 41 of cap free
Amount of items: 4
Items: 
Size: 14740 Color: 1
Size: 1673 Color: 0
Size: 96 Color: 0
Size: 58 Color: 1

Bin 142: 43 of cap free
Amount of items: 2
Items: 
Size: 14849 Color: 1
Size: 1716 Color: 0

Bin 143: 44 of cap free
Amount of items: 2
Items: 
Size: 10609 Color: 0
Size: 5955 Color: 1

Bin 144: 47 of cap free
Amount of items: 2
Items: 
Size: 12519 Color: 1
Size: 4042 Color: 0

Bin 145: 49 of cap free
Amount of items: 2
Items: 
Size: 14452 Color: 0
Size: 2107 Color: 1

Bin 146: 50 of cap free
Amount of items: 2
Items: 
Size: 11557 Color: 1
Size: 5001 Color: 0

Bin 147: 50 of cap free
Amount of items: 2
Items: 
Size: 14886 Color: 0
Size: 1672 Color: 1

Bin 148: 52 of cap free
Amount of items: 2
Items: 
Size: 14644 Color: 1
Size: 1912 Color: 0

Bin 149: 53 of cap free
Amount of items: 2
Items: 
Size: 13878 Color: 1
Size: 2677 Color: 0

Bin 150: 54 of cap free
Amount of items: 2
Items: 
Size: 8330 Color: 0
Size: 8224 Color: 1

Bin 151: 54 of cap free
Amount of items: 2
Items: 
Size: 14292 Color: 1
Size: 2262 Color: 0

Bin 152: 54 of cap free
Amount of items: 2
Items: 
Size: 14844 Color: 1
Size: 1710 Color: 0

Bin 153: 55 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 0
Size: 7603 Color: 0
Size: 628 Color: 1

Bin 154: 58 of cap free
Amount of items: 2
Items: 
Size: 11170 Color: 1
Size: 5380 Color: 0

Bin 155: 58 of cap free
Amount of items: 2
Items: 
Size: 14127 Color: 0
Size: 2423 Color: 1

Bin 156: 59 of cap free
Amount of items: 2
Items: 
Size: 9635 Color: 0
Size: 6914 Color: 1

Bin 157: 59 of cap free
Amount of items: 2
Items: 
Size: 10861 Color: 1
Size: 5688 Color: 0

Bin 158: 61 of cap free
Amount of items: 2
Items: 
Size: 11116 Color: 0
Size: 5431 Color: 1

Bin 159: 63 of cap free
Amount of items: 2
Items: 
Size: 13174 Color: 1
Size: 3371 Color: 0

Bin 160: 63 of cap free
Amount of items: 2
Items: 
Size: 13397 Color: 0
Size: 3148 Color: 1

Bin 161: 63 of cap free
Amount of items: 2
Items: 
Size: 13723 Color: 0
Size: 2822 Color: 1

Bin 162: 67 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 1
Size: 2733 Color: 0

Bin 163: 69 of cap free
Amount of items: 2
Items: 
Size: 12883 Color: 0
Size: 3656 Color: 1

Bin 164: 71 of cap free
Amount of items: 2
Items: 
Size: 10434 Color: 1
Size: 6103 Color: 0

Bin 165: 75 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 0
Size: 3773 Color: 1

Bin 166: 80 of cap free
Amount of items: 2
Items: 
Size: 12472 Color: 0
Size: 4056 Color: 1

Bin 167: 83 of cap free
Amount of items: 2
Items: 
Size: 12977 Color: 1
Size: 3548 Color: 0

Bin 168: 84 of cap free
Amount of items: 2
Items: 
Size: 14119 Color: 1
Size: 2405 Color: 0

Bin 169: 90 of cap free
Amount of items: 2
Items: 
Size: 14114 Color: 1
Size: 2404 Color: 0

Bin 170: 93 of cap free
Amount of items: 2
Items: 
Size: 14824 Color: 1
Size: 1691 Color: 0

Bin 171: 95 of cap free
Amount of items: 2
Items: 
Size: 14423 Color: 0
Size: 2090 Color: 1

Bin 172: 104 of cap free
Amount of items: 2
Items: 
Size: 10260 Color: 1
Size: 6244 Color: 0

Bin 173: 106 of cap free
Amount of items: 2
Items: 
Size: 11154 Color: 1
Size: 5348 Color: 0

Bin 174: 110 of cap free
Amount of items: 2
Items: 
Size: 11501 Color: 1
Size: 4997 Color: 0

Bin 175: 111 of cap free
Amount of items: 2
Items: 
Size: 13373 Color: 1
Size: 3124 Color: 0

Bin 176: 112 of cap free
Amount of items: 2
Items: 
Size: 10545 Color: 0
Size: 5951 Color: 1

Bin 177: 114 of cap free
Amount of items: 2
Items: 
Size: 12466 Color: 1
Size: 4028 Color: 0

Bin 178: 126 of cap free
Amount of items: 2
Items: 
Size: 13649 Color: 1
Size: 2833 Color: 0

Bin 179: 127 of cap free
Amount of items: 2
Items: 
Size: 12081 Color: 1
Size: 4400 Color: 0

Bin 180: 136 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 0
Size: 2812 Color: 1

Bin 181: 139 of cap free
Amount of items: 2
Items: 
Size: 13486 Color: 1
Size: 2983 Color: 0

Bin 182: 140 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 0
Size: 3144 Color: 1

Bin 183: 144 of cap free
Amount of items: 2
Items: 
Size: 12846 Color: 0
Size: 3618 Color: 1

Bin 184: 148 of cap free
Amount of items: 9
Items: 
Size: 8306 Color: 0
Size: 1072 Color: 1
Size: 1064 Color: 1
Size: 1040 Color: 0
Size: 1040 Color: 0
Size: 1024 Color: 1
Size: 998 Color: 1
Size: 960 Color: 0
Size: 956 Color: 0

Bin 185: 149 of cap free
Amount of items: 2
Items: 
Size: 13321 Color: 0
Size: 3138 Color: 1

Bin 186: 155 of cap free
Amount of items: 2
Items: 
Size: 9531 Color: 1
Size: 6922 Color: 0

Bin 187: 158 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 1
Size: 2810 Color: 0

Bin 188: 163 of cap free
Amount of items: 2
Items: 
Size: 12921 Color: 1
Size: 3524 Color: 0

Bin 189: 188 of cap free
Amount of items: 2
Items: 
Size: 9124 Color: 0
Size: 7296 Color: 1

Bin 190: 188 of cap free
Amount of items: 2
Items: 
Size: 9507 Color: 0
Size: 6913 Color: 1

Bin 191: 190 of cap free
Amount of items: 2
Items: 
Size: 11086 Color: 0
Size: 5332 Color: 1

Bin 192: 191 of cap free
Amount of items: 2
Items: 
Size: 9496 Color: 1
Size: 6921 Color: 0

Bin 193: 206 of cap free
Amount of items: 2
Items: 
Size: 9486 Color: 1
Size: 6916 Color: 0

Bin 194: 217 of cap free
Amount of items: 2
Items: 
Size: 9784 Color: 0
Size: 6607 Color: 1

Bin 195: 221 of cap free
Amount of items: 2
Items: 
Size: 9483 Color: 0
Size: 6904 Color: 1

Bin 196: 231 of cap free
Amount of items: 10
Items: 
Size: 8305 Color: 0
Size: 920 Color: 1
Size: 912 Color: 1
Size: 912 Color: 0
Size: 912 Color: 0
Size: 908 Color: 1
Size: 904 Color: 1
Size: 890 Color: 1
Size: 864 Color: 0
Size: 850 Color: 0

Bin 197: 238 of cap free
Amount of items: 2
Items: 
Size: 8314 Color: 1
Size: 8056 Color: 0

Bin 198: 245 of cap free
Amount of items: 7
Items: 
Size: 8313 Color: 0
Size: 1498 Color: 1
Size: 1380 Color: 1
Size: 1380 Color: 1
Size: 1376 Color: 0
Size: 1232 Color: 0
Size: 1184 Color: 0

Bin 199: 9112 of cap free
Amount of items: 24
Items: 
Size: 400 Color: 1
Size: 384 Color: 1
Size: 368 Color: 1
Size: 352 Color: 0
Size: 344 Color: 0
Size: 344 Color: 0
Size: 336 Color: 1
Size: 328 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 320 Color: 0
Size: 308 Color: 1
Size: 304 Color: 0
Size: 304 Color: 0
Size: 292 Color: 1
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0
Size: 288 Color: 0
Size: 284 Color: 1
Size: 276 Color: 0
Size: 264 Color: 1
Size: 256 Color: 1
Size: 240 Color: 0

Total size: 3288384
Total free space: 16608

