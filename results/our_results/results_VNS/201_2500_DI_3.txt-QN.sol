Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1500 Color: 150
Size: 916 Color: 130
Size: 40 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 160
Size: 668 Color: 113
Size: 142 Color: 54

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 162
Size: 650 Color: 112
Size: 128 Color: 49

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 167
Size: 574 Color: 107
Size: 128 Color: 50

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 175
Size: 474 Color: 97
Size: 92 Color: 37

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 180
Size: 435 Color: 91
Size: 86 Color: 31

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1993 Color: 185
Size: 387 Color: 87
Size: 76 Color: 26

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 188
Size: 354 Color: 83
Size: 68 Color: 22

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 189
Size: 356 Color: 84
Size: 64 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2074 Color: 191
Size: 322 Color: 81
Size: 60 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 193
Size: 280 Color: 77
Size: 68 Color: 23

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 194
Size: 292 Color: 79
Size: 40 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 195
Size: 226 Color: 71
Size: 84 Color: 29

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 196
Size: 242 Color: 74
Size: 48 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 197
Size: 272 Color: 76
Size: 4 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 198
Size: 174 Color: 62
Size: 96 Color: 38

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 200
Size: 172 Color: 60
Size: 88 Color: 32

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 201
Size: 150 Color: 57
Size: 104 Color: 42

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1571 Color: 155
Size: 884 Color: 129

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1599 Color: 158
Size: 840 Color: 123
Size: 16 Color: 3

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1611 Color: 159
Size: 844 Color: 125

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1713 Color: 164
Size: 742 Color: 119

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 170
Size: 596 Color: 108
Size: 52 Color: 15

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1887 Color: 174
Size: 568 Color: 106

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1924 Color: 178
Size: 531 Color: 103

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1931 Color: 179
Size: 524 Color: 101

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 190
Size: 364 Color: 85
Size: 52 Color: 16

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 2106 Color: 192
Size: 349 Color: 82

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 153
Size: 863 Color: 126
Size: 36 Color: 5

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1804 Color: 169
Size: 650 Color: 111

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 171
Size: 541 Color: 104
Size: 92 Color: 36

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 2192 Color: 199
Size: 262 Color: 75

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 148
Size: 719 Color: 117
Size: 284 Color: 78

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 156
Size: 833 Color: 122
Size: 32 Color: 4

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 165
Size: 724 Color: 118

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1748 Color: 166
Size: 705 Color: 115

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 176
Size: 548 Color: 105
Size: 8 Color: 1

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1986 Color: 184
Size: 467 Color: 96

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 2028 Color: 187
Size: 425 Color: 90

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1514 Color: 151
Size: 938 Color: 131

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1922 Color: 177
Size: 530 Color: 102

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 181
Size: 512 Color: 100

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 182
Size: 497 Color: 99
Size: 8 Color: 2

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 183
Size: 478 Color: 98

Bin 45: 5 of cap free
Amount of items: 6
Items: 
Size: 1236 Color: 141
Size: 444 Color: 94
Size: 439 Color: 93
Size: 236 Color: 73
Size: 48 Color: 13
Size: 48 Color: 11

Bin 46: 7 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 152
Size: 873 Color: 127
Size: 40 Color: 6

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 2003 Color: 186
Size: 446 Color: 95

Bin 48: 8 of cap free
Amount of items: 2
Items: 
Size: 1770 Color: 168
Size: 678 Color: 114

Bin 49: 9 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 154
Size: 877 Color: 128

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 163
Size: 751 Color: 120

Bin 51: 10 of cap free
Amount of items: 2
Items: 
Size: 1660 Color: 161
Size: 786 Color: 121

Bin 52: 12 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 147
Size: 1023 Color: 136

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 1468 Color: 149
Size: 976 Color: 132

Bin 54: 13 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 143
Size: 715 Color: 116
Size: 394 Color: 89

Bin 55: 13 of cap free
Amount of items: 2
Items: 
Size: 1822 Color: 172
Size: 621 Color: 110

Bin 56: 13 of cap free
Amount of items: 2
Items: 
Size: 1836 Color: 173
Size: 607 Color: 109

Bin 57: 19 of cap free
Amount of items: 9
Items: 
Size: 1229 Color: 138
Size: 204 Color: 68
Size: 204 Color: 67
Size: 204 Color: 66
Size: 200 Color: 65
Size: 184 Color: 64
Size: 76 Color: 25
Size: 72 Color: 24
Size: 64 Color: 21

Bin 58: 19 of cap free
Amount of items: 2
Items: 
Size: 1595 Color: 157
Size: 842 Color: 124

Bin 59: 24 of cap free
Amount of items: 20
Items: 
Size: 176 Color: 63
Size: 174 Color: 61
Size: 164 Color: 59
Size: 156 Color: 58
Size: 144 Color: 56
Size: 144 Color: 55
Size: 142 Color: 53
Size: 140 Color: 52
Size: 132 Color: 51
Size: 122 Color: 48
Size: 120 Color: 47
Size: 104 Color: 41
Size: 104 Color: 40
Size: 104 Color: 39
Size: 88 Color: 35
Size: 88 Color: 34
Size: 88 Color: 33
Size: 86 Color: 30
Size: 80 Color: 28
Size: 76 Color: 27

Bin 60: 25 of cap free
Amount of items: 2
Items: 
Size: 1409 Color: 146
Size: 1022 Color: 135

Bin 61: 28 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 142
Size: 1142 Color: 137
Size: 48 Color: 10

Bin 62: 28 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 144
Size: 1020 Color: 133
Size: 44 Color: 9

Bin 63: 30 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 145
Size: 1021 Color: 134

Bin 64: 38 of cap free
Amount of items: 5
Items: 
Size: 1231 Color: 140
Size: 436 Color: 92
Size: 389 Color: 88
Size: 314 Color: 80
Size: 48 Color: 14

Bin 65: 56 of cap free
Amount of items: 7
Items: 
Size: 1230 Color: 139
Size: 384 Color: 86
Size: 232 Color: 72
Size: 220 Color: 70
Size: 214 Color: 69
Size: 64 Color: 20
Size: 56 Color: 17

Bin 66: 2012 of cap free
Amount of items: 4
Items: 
Size: 112 Color: 46
Size: 112 Color: 45
Size: 112 Color: 44
Size: 108 Color: 43

Total size: 159640
Total free space: 2456

