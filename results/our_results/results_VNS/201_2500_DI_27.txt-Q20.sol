Capicity Bin: 1940
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1095 Color: 17
Size: 645 Color: 3
Size: 160 Color: 14
Size: 40 Color: 2

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1259 Color: 14
Size: 409 Color: 19
Size: 170 Color: 13
Size: 102 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1451 Color: 12
Size: 447 Color: 9
Size: 42 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 0
Size: 403 Color: 16
Size: 80 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 4
Size: 333 Color: 7
Size: 104 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 15
Size: 303 Color: 19
Size: 88 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 3
Size: 309 Color: 5
Size: 60 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1601 Color: 18
Size: 283 Color: 14
Size: 56 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 3
Size: 270 Color: 4
Size: 52 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 0
Size: 206 Color: 18
Size: 100 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 7
Size: 221 Color: 1
Size: 66 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 15
Size: 245 Color: 5
Size: 34 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 12
Size: 181 Color: 0
Size: 90 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 1
Size: 227 Color: 3
Size: 28 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 9
Size: 210 Color: 6
Size: 40 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 14
Size: 171 Color: 1
Size: 76 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 6
Size: 112 Color: 6
Size: 102 Color: 12

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 14
Size: 177 Color: 8
Size: 34 Color: 5

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1216 Color: 8
Size: 683 Color: 16
Size: 40 Color: 15

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1333 Color: 7
Size: 550 Color: 5
Size: 56 Color: 7

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 9
Size: 365 Color: 2
Size: 72 Color: 8

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 3
Size: 366 Color: 1
Size: 32 Color: 10

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 9
Size: 327 Color: 19
Size: 42 Color: 15

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 18
Size: 346 Color: 0
Size: 16 Color: 9

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1639 Color: 17
Size: 300 Color: 13

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1723 Color: 10
Size: 216 Color: 5

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 13
Size: 166 Color: 3
Size: 36 Color: 17

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1354 Color: 7
Size: 448 Color: 1
Size: 136 Color: 19

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1397 Color: 13
Size: 541 Color: 3

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 1207 Color: 10
Size: 646 Color: 1
Size: 84 Color: 12

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1215 Color: 18
Size: 656 Color: 0
Size: 66 Color: 10

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 1495 Color: 19
Size: 442 Color: 11

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1533 Color: 1
Size: 244 Color: 5
Size: 160 Color: 17

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 4
Size: 371 Color: 16
Size: 24 Color: 9

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1648 Color: 8
Size: 289 Color: 3

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1715 Color: 19
Size: 222 Color: 8

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 9
Size: 191 Color: 4
Size: 8 Color: 18

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 16
Size: 738 Color: 2
Size: 32 Color: 7

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1293 Color: 13
Size: 569 Color: 16
Size: 74 Color: 0

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1317 Color: 6
Size: 619 Color: 14

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1325 Color: 17
Size: 611 Color: 19

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 0
Size: 334 Color: 10

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1595 Color: 4
Size: 341 Color: 11

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 2
Size: 251 Color: 7
Size: 8 Color: 7

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 9
Size: 258 Color: 10

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1097 Color: 14
Size: 806 Color: 1
Size: 32 Color: 7

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 1422 Color: 10
Size: 513 Color: 5

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 1721 Color: 16
Size: 213 Color: 15

Bin 49: 7 of cap free
Amount of items: 3
Items: 
Size: 1098 Color: 17
Size: 783 Color: 0
Size: 52 Color: 2

Bin 50: 7 of cap free
Amount of items: 3
Items: 
Size: 1340 Color: 12
Size: 453 Color: 2
Size: 140 Color: 19

Bin 51: 7 of cap free
Amount of items: 2
Items: 
Size: 1647 Color: 11
Size: 286 Color: 6

Bin 52: 7 of cap free
Amount of items: 2
Items: 
Size: 1742 Color: 12
Size: 191 Color: 16

Bin 53: 10 of cap free
Amount of items: 2
Items: 
Size: 1121 Color: 18
Size: 809 Color: 6

Bin 54: 11 of cap free
Amount of items: 3
Items: 
Size: 1058 Color: 19
Size: 807 Color: 6
Size: 64 Color: 9

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1462 Color: 5
Size: 466 Color: 6

Bin 56: 13 of cap free
Amount of items: 2
Items: 
Size: 1301 Color: 12
Size: 626 Color: 9

Bin 57: 14 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 7
Size: 521 Color: 11

Bin 58: 17 of cap free
Amount of items: 2
Items: 
Size: 1199 Color: 2
Size: 724 Color: 8

Bin 59: 17 of cap free
Amount of items: 2
Items: 
Size: 1218 Color: 5
Size: 705 Color: 19

Bin 60: 19 of cap free
Amount of items: 3
Items: 
Size: 1282 Color: 8
Size: 587 Color: 5
Size: 52 Color: 1

Bin 61: 19 of cap free
Amount of items: 2
Items: 
Size: 1414 Color: 0
Size: 507 Color: 3

Bin 62: 25 of cap free
Amount of items: 19
Items: 
Size: 216 Color: 1
Size: 207 Color: 16
Size: 160 Color: 3
Size: 128 Color: 18
Size: 122 Color: 13
Size: 122 Color: 13
Size: 108 Color: 17
Size: 106 Color: 11
Size: 96 Color: 9
Size: 80 Color: 6
Size: 80 Color: 4
Size: 72 Color: 12
Size: 72 Color: 2
Size: 64 Color: 15
Size: 64 Color: 9
Size: 60 Color: 5
Size: 56 Color: 0
Size: 52 Color: 1
Size: 50 Color: 18

Bin 63: 27 of cap free
Amount of items: 4
Items: 
Size: 971 Color: 1
Size: 490 Color: 14
Size: 404 Color: 9
Size: 48 Color: 3

Bin 64: 31 of cap free
Amount of items: 5
Items: 
Size: 973 Color: 8
Size: 402 Color: 10
Size: 241 Color: 5
Size: 233 Color: 8
Size: 60 Color: 13

Bin 65: 32 of cap free
Amount of items: 3
Items: 
Size: 974 Color: 17
Size: 838 Color: 18
Size: 96 Color: 0

Bin 66: 1580 of cap free
Amount of items: 8
Items: 
Size: 48 Color: 11
Size: 48 Color: 3
Size: 46 Color: 15
Size: 46 Color: 10
Size: 44 Color: 16
Size: 44 Color: 12
Size: 44 Color: 5
Size: 40 Color: 0

Total size: 126100
Total free space: 1940

