Capicity Bin: 1001
Lower Bound: 49

Bins used: 51
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 9

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 3
Size: 419 Color: 8

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 16
Size: 300 Color: 4

Bin 4: 1 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 9
Size: 373 Color: 15

Bin 5: 1 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 18
Size: 308 Color: 4

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 19
Size: 295 Color: 14

Bin 7: 1 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 274 Color: 15

Bin 8: 1 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 236 Color: 17

Bin 9: 2 of cap free
Amount of items: 3
Items: 
Size: 731 Color: 9
Size: 138 Color: 2
Size: 130 Color: 8

Bin 10: 3 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 13
Size: 372 Color: 11

Bin 11: 3 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 5
Size: 320 Color: 9

Bin 12: 3 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 17
Size: 273 Color: 14

Bin 13: 3 of cap free
Amount of items: 3
Items: 
Size: 726 Color: 16
Size: 169 Color: 0
Size: 103 Color: 6

Bin 14: 3 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 14
Size: 242 Color: 13

Bin 15: 3 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 6
Size: 235 Color: 7

Bin 16: 3 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 1
Size: 218 Color: 3

Bin 17: 5 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 7
Size: 460 Color: 1

Bin 18: 5 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 13
Size: 370 Color: 19

Bin 19: 6 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 3
Size: 493 Color: 14

Bin 20: 6 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 19
Size: 365 Color: 15

Bin 21: 6 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 19
Size: 311 Color: 5

Bin 22: 6 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 5
Size: 271 Color: 14

Bin 23: 6 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 6
Size: 198 Color: 1

Bin 24: 7 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 14
Size: 480 Color: 16

Bin 25: 7 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 6
Size: 447 Color: 2

Bin 26: 8 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 10
Size: 447 Color: 7

Bin 27: 8 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 19
Size: 253 Color: 10

Bin 28: 8 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 2
Size: 216 Color: 10

Bin 29: 9 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 11
Size: 305 Color: 0

Bin 30: 11 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 429 Color: 10

Bin 31: 11 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 18
Size: 350 Color: 3

Bin 32: 11 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 18
Size: 302 Color: 1

Bin 33: 15 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 0
Size: 395 Color: 8

Bin 34: 15 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 13
Size: 210 Color: 14

Bin 35: 17 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 13
Size: 483 Color: 0

Bin 36: 18 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 10
Size: 459 Color: 17

Bin 37: 19 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 0
Size: 173 Color: 14
Size: 114 Color: 17

Bin 38: 20 of cap free
Amount of items: 3
Items: 
Size: 702 Color: 15
Size: 176 Color: 18
Size: 103 Color: 9

Bin 39: 21 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 9
Size: 361 Color: 1

Bin 40: 25 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 10
Size: 408 Color: 19

Bin 41: 28 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 5
Size: 385 Color: 16

Bin 42: 30 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 13
Size: 268 Color: 0

Bin 43: 37 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 1
Size: 437 Color: 19

Bin 44: 57 of cap free
Amount of items: 2
Items: 
Size: 488 Color: 5
Size: 456 Color: 2

Bin 45: 166 of cap free
Amount of items: 2
Items: 
Size: 488 Color: 12
Size: 347 Color: 15

Bin 46: 215 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 18
Size: 101 Color: 9

Bin 47: 298 of cap free
Amount of items: 2
Items: 
Size: 352 Color: 19
Size: 351 Color: 16

Bin 48: 324 of cap free
Amount of items: 1
Items: 
Size: 677 Color: 10

Bin 49: 328 of cap free
Amount of items: 1
Items: 
Size: 673 Color: 0

Bin 50: 332 of cap free
Amount of items: 1
Items: 
Size: 669 Color: 16

Bin 51: 341 of cap free
Amount of items: 1
Items: 
Size: 660 Color: 8

Total size: 48597
Total free space: 2454

