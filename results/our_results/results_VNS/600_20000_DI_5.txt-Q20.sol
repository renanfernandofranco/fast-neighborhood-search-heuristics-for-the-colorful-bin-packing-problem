Capicity Bin: 15712
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9036 Color: 0
Size: 5564 Color: 9
Size: 1112 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9528 Color: 19
Size: 5848 Color: 13
Size: 336 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9630 Color: 11
Size: 5058 Color: 2
Size: 1024 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9704 Color: 18
Size: 5432 Color: 0
Size: 576 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10004 Color: 14
Size: 4216 Color: 0
Size: 1492 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10360 Color: 6
Size: 5016 Color: 3
Size: 336 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10379 Color: 6
Size: 4445 Color: 19
Size: 888 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10664 Color: 9
Size: 3592 Color: 10
Size: 1456 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10724 Color: 2
Size: 4108 Color: 6
Size: 880 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10756 Color: 19
Size: 3592 Color: 13
Size: 1364 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10866 Color: 15
Size: 4424 Color: 3
Size: 422 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10935 Color: 11
Size: 3981 Color: 5
Size: 796 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 10896 Color: 16
Size: 4472 Color: 18
Size: 344 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 15
Size: 4048 Color: 4
Size: 392 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11410 Color: 11
Size: 3974 Color: 15
Size: 328 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11356 Color: 2
Size: 3636 Color: 1
Size: 720 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11380 Color: 1
Size: 3612 Color: 3
Size: 720 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 16
Size: 2970 Color: 17
Size: 890 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11940 Color: 11
Size: 3148 Color: 9
Size: 624 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11919 Color: 6
Size: 3161 Color: 13
Size: 632 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11945 Color: 2
Size: 2967 Color: 7
Size: 800 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11976 Color: 17
Size: 3368 Color: 4
Size: 368 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12134 Color: 11
Size: 2952 Color: 19
Size: 626 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12069 Color: 7
Size: 3037 Color: 2
Size: 606 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12150 Color: 4
Size: 2602 Color: 11
Size: 960 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12264 Color: 5
Size: 3128 Color: 0
Size: 320 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12340 Color: 2
Size: 3160 Color: 14
Size: 212 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12413 Color: 8
Size: 2675 Color: 2
Size: 624 Color: 14

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12424 Color: 9
Size: 2984 Color: 13
Size: 304 Color: 11

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 3
Size: 2164 Color: 3
Size: 1012 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 7
Size: 1564 Color: 15
Size: 1556 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 11
Size: 2364 Color: 9
Size: 640 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 7
Size: 2648 Color: 18
Size: 288 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12782 Color: 0
Size: 2322 Color: 11
Size: 608 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12834 Color: 17
Size: 2762 Color: 1
Size: 116 Color: 17

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 4
Size: 1628 Color: 11
Size: 1248 Color: 13

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12930 Color: 6
Size: 1726 Color: 7
Size: 1056 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 1
Size: 2022 Color: 17
Size: 728 Color: 18

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13116 Color: 11
Size: 1450 Color: 7
Size: 1146 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12969 Color: 17
Size: 2287 Color: 1
Size: 456 Color: 9

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 2
Size: 2248 Color: 14
Size: 432 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13124 Color: 11
Size: 2176 Color: 0
Size: 412 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13128 Color: 14
Size: 2168 Color: 8
Size: 416 Color: 15

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13202 Color: 11
Size: 1794 Color: 15
Size: 716 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13158 Color: 13
Size: 2094 Color: 1
Size: 460 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 11
Size: 2088 Color: 2
Size: 400 Color: 6

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13193 Color: 16
Size: 2063 Color: 12
Size: 456 Color: 9

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13290 Color: 10
Size: 1470 Color: 13
Size: 952 Color: 15

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13318 Color: 11
Size: 1996 Color: 14
Size: 398 Color: 6

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13333 Color: 4
Size: 1931 Color: 17
Size: 448 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 3
Size: 1982 Color: 8
Size: 396 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13414 Color: 13
Size: 2028 Color: 0
Size: 270 Color: 10

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13438 Color: 2
Size: 1606 Color: 5
Size: 668 Color: 11

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13524 Color: 3
Size: 1592 Color: 0
Size: 596 Color: 10

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13530 Color: 16
Size: 1538 Color: 11
Size: 644 Color: 12

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13626 Color: 18
Size: 1494 Color: 13
Size: 592 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13642 Color: 17
Size: 1308 Color: 6
Size: 762 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13694 Color: 11
Size: 1514 Color: 18
Size: 504 Color: 8

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 6
Size: 1480 Color: 19
Size: 496 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13738 Color: 5
Size: 1142 Color: 19
Size: 832 Color: 8

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13834 Color: 11
Size: 1566 Color: 2
Size: 312 Color: 14

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13804 Color: 2
Size: 1028 Color: 14
Size: 880 Color: 12

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 18
Size: 1104 Color: 11
Size: 792 Color: 13

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 9
Size: 1308 Color: 7
Size: 534 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 7
Size: 992 Color: 11
Size: 824 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13950 Color: 12
Size: 1378 Color: 4
Size: 384 Color: 19

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13956 Color: 16
Size: 1404 Color: 6
Size: 352 Color: 11

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13994 Color: 2
Size: 1462 Color: 8
Size: 256 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 14000 Color: 8
Size: 1008 Color: 13
Size: 704 Color: 9

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 14082 Color: 11
Size: 1362 Color: 5
Size: 268 Color: 5

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 18
Size: 1236 Color: 13
Size: 414 Color: 19

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 7
Size: 1000 Color: 11
Size: 640 Color: 9

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 8702 Color: 13
Size: 6545 Color: 0
Size: 464 Color: 13

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 8849 Color: 1
Size: 6542 Color: 5
Size: 320 Color: 8

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 9534 Color: 17
Size: 5017 Color: 13
Size: 1160 Color: 6

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 10921 Color: 19
Size: 4502 Color: 6
Size: 288 Color: 5

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 11145 Color: 6
Size: 4026 Color: 5
Size: 540 Color: 0

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 11573 Color: 3
Size: 3586 Color: 19
Size: 552 Color: 7

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 11908 Color: 12
Size: 2691 Color: 18
Size: 1112 Color: 12

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 12610 Color: 15
Size: 1997 Color: 10
Size: 1104 Color: 1

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 12674 Color: 14
Size: 2077 Color: 18
Size: 960 Color: 1

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 12735 Color: 11
Size: 2684 Color: 10
Size: 292 Color: 10

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 12789 Color: 6
Size: 2650 Color: 2
Size: 272 Color: 19

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 12953 Color: 5
Size: 2294 Color: 9
Size: 464 Color: 16

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 13093 Color: 14
Size: 2008 Color: 12
Size: 610 Color: 19

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 13173 Color: 14
Size: 1822 Color: 4
Size: 716 Color: 1

Bin 87: 1 of cap free
Amount of items: 2
Items: 
Size: 13177 Color: 10
Size: 2534 Color: 19

Bin 88: 1 of cap free
Amount of items: 3
Items: 
Size: 13221 Color: 18
Size: 2262 Color: 11
Size: 228 Color: 5

Bin 89: 1 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 5
Size: 2427 Color: 9

Bin 90: 1 of cap free
Amount of items: 2
Items: 
Size: 13397 Color: 5
Size: 2314 Color: 4

Bin 91: 1 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 8
Size: 1875 Color: 7

Bin 92: 2 of cap free
Amount of items: 6
Items: 
Size: 7859 Color: 15
Size: 2101 Color: 13
Size: 1936 Color: 17
Size: 1898 Color: 7
Size: 1596 Color: 14
Size: 320 Color: 10

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 7870 Color: 5
Size: 6532 Color: 8
Size: 1308 Color: 16

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 9208 Color: 15
Size: 6198 Color: 14
Size: 304 Color: 14

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 9646 Color: 10
Size: 5888 Color: 19
Size: 176 Color: 6

Bin 96: 2 of cap free
Amount of items: 3
Items: 
Size: 10152 Color: 4
Size: 5150 Color: 1
Size: 408 Color: 11

Bin 97: 2 of cap free
Amount of items: 3
Items: 
Size: 10363 Color: 17
Size: 5003 Color: 9
Size: 344 Color: 17

Bin 98: 2 of cap free
Amount of items: 3
Items: 
Size: 10854 Color: 7
Size: 4164 Color: 13
Size: 692 Color: 15

Bin 99: 2 of cap free
Amount of items: 3
Items: 
Size: 11672 Color: 9
Size: 3534 Color: 11
Size: 504 Color: 0

Bin 100: 2 of cap free
Amount of items: 3
Items: 
Size: 11929 Color: 18
Size: 3141 Color: 8
Size: 640 Color: 15

Bin 101: 2 of cap free
Amount of items: 3
Items: 
Size: 12121 Color: 11
Size: 3061 Color: 3
Size: 528 Color: 10

Bin 102: 2 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 6
Size: 2982 Color: 9
Size: 380 Color: 11

Bin 103: 2 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 15
Size: 1964 Color: 17
Size: 1152 Color: 12

Bin 104: 2 of cap free
Amount of items: 3
Items: 
Size: 13149 Color: 7
Size: 2481 Color: 0
Size: 80 Color: 17

Bin 105: 2 of cap free
Amount of items: 2
Items: 
Size: 13254 Color: 0
Size: 2456 Color: 19

Bin 106: 2 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 19
Size: 2386 Color: 14

Bin 107: 2 of cap free
Amount of items: 2
Items: 
Size: 13480 Color: 16
Size: 2230 Color: 14

Bin 108: 2 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 19
Size: 2120 Color: 9

Bin 109: 2 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 9
Size: 2050 Color: 15

Bin 110: 2 of cap free
Amount of items: 2
Items: 
Size: 13974 Color: 10
Size: 1736 Color: 3

Bin 111: 2 of cap free
Amount of items: 2
Items: 
Size: 14134 Color: 2
Size: 1576 Color: 10

Bin 112: 3 of cap free
Amount of items: 5
Items: 
Size: 7860 Color: 19
Size: 3704 Color: 7
Size: 2437 Color: 11
Size: 1196 Color: 10
Size: 512 Color: 8

Bin 113: 3 of cap free
Amount of items: 3
Items: 
Size: 8833 Color: 5
Size: 5580 Color: 17
Size: 1296 Color: 11

Bin 114: 3 of cap free
Amount of items: 2
Items: 
Size: 9988 Color: 16
Size: 5721 Color: 18

Bin 115: 3 of cap free
Amount of items: 3
Items: 
Size: 11928 Color: 9
Size: 3451 Color: 14
Size: 330 Color: 10

Bin 116: 3 of cap free
Amount of items: 3
Items: 
Size: 12696 Color: 10
Size: 2113 Color: 5
Size: 900 Color: 16

Bin 117: 4 of cap free
Amount of items: 3
Items: 
Size: 7878 Color: 19
Size: 7142 Color: 15
Size: 688 Color: 1

Bin 118: 4 of cap free
Amount of items: 3
Items: 
Size: 8278 Color: 13
Size: 6518 Color: 5
Size: 912 Color: 18

Bin 119: 4 of cap free
Amount of items: 3
Items: 
Size: 8712 Color: 11
Size: 6832 Color: 7
Size: 164 Color: 11

Bin 120: 4 of cap free
Amount of items: 3
Items: 
Size: 9709 Color: 5
Size: 5687 Color: 2
Size: 312 Color: 11

Bin 121: 4 of cap free
Amount of items: 3
Items: 
Size: 10946 Color: 1
Size: 4042 Color: 14
Size: 720 Color: 11

Bin 122: 4 of cap free
Amount of items: 3
Items: 
Size: 11850 Color: 7
Size: 3602 Color: 18
Size: 256 Color: 13

Bin 123: 4 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 4
Size: 2532 Color: 6

Bin 124: 4 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 7
Size: 2404 Color: 3

Bin 125: 4 of cap free
Amount of items: 2
Items: 
Size: 13992 Color: 0
Size: 1716 Color: 6

Bin 126: 4 of cap free
Amount of items: 2
Items: 
Size: 14026 Color: 2
Size: 1682 Color: 1

Bin 127: 5 of cap free
Amount of items: 8
Items: 
Size: 7857 Color: 10
Size: 1368 Color: 15
Size: 1304 Color: 2
Size: 1300 Color: 6
Size: 1212 Color: 18
Size: 1136 Color: 10
Size: 1072 Color: 14
Size: 458 Color: 14

Bin 128: 5 of cap free
Amount of items: 2
Items: 
Size: 12503 Color: 7
Size: 3204 Color: 3

Bin 129: 6 of cap free
Amount of items: 3
Items: 
Size: 7892 Color: 11
Size: 7650 Color: 15
Size: 164 Color: 5

Bin 130: 6 of cap free
Amount of items: 3
Items: 
Size: 7902 Color: 8
Size: 7308 Color: 2
Size: 496 Color: 7

Bin 131: 6 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 9
Size: 7350 Color: 3
Size: 176 Color: 5

Bin 132: 6 of cap free
Amount of items: 3
Items: 
Size: 9052 Color: 10
Size: 6168 Color: 14
Size: 486 Color: 11

Bin 133: 6 of cap free
Amount of items: 3
Items: 
Size: 11394 Color: 7
Size: 4072 Color: 1
Size: 240 Color: 11

Bin 134: 6 of cap free
Amount of items: 2
Items: 
Size: 13186 Color: 4
Size: 2520 Color: 2

Bin 135: 7 of cap free
Amount of items: 2
Items: 
Size: 12483 Color: 15
Size: 3222 Color: 16

Bin 136: 7 of cap free
Amount of items: 2
Items: 
Size: 13356 Color: 12
Size: 2349 Color: 16

Bin 137: 7 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 3
Size: 2193 Color: 16

Bin 138: 8 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 16
Size: 1940 Color: 18

Bin 139: 9 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 5
Size: 1983 Color: 17

Bin 140: 10 of cap free
Amount of items: 2
Items: 
Size: 10882 Color: 4
Size: 4820 Color: 0

Bin 141: 10 of cap free
Amount of items: 3
Items: 
Size: 12386 Color: 4
Size: 3220 Color: 13
Size: 96 Color: 10

Bin 142: 10 of cap free
Amount of items: 2
Items: 
Size: 13898 Color: 13
Size: 1804 Color: 17

Bin 143: 10 of cap free
Amount of items: 2
Items: 
Size: 14046 Color: 18
Size: 1656 Color: 1

Bin 144: 11 of cap free
Amount of items: 2
Items: 
Size: 11880 Color: 1
Size: 3821 Color: 9

Bin 145: 12 of cap free
Amount of items: 2
Items: 
Size: 10788 Color: 7
Size: 4912 Color: 2

Bin 146: 12 of cap free
Amount of items: 2
Items: 
Size: 13782 Color: 19
Size: 1918 Color: 7

Bin 147: 12 of cap free
Amount of items: 2
Items: 
Size: 13852 Color: 0
Size: 1848 Color: 7

Bin 148: 13 of cap free
Amount of items: 4
Items: 
Size: 7864 Color: 11
Size: 4764 Color: 11
Size: 2751 Color: 15
Size: 320 Color: 7

Bin 149: 13 of cap free
Amount of items: 2
Items: 
Size: 13463 Color: 14
Size: 2236 Color: 3

Bin 150: 14 of cap free
Amount of items: 3
Items: 
Size: 8518 Color: 5
Size: 6548 Color: 17
Size: 632 Color: 19

Bin 151: 15 of cap free
Amount of items: 3
Items: 
Size: 8889 Color: 18
Size: 6552 Color: 15
Size: 256 Color: 13

Bin 152: 16 of cap free
Amount of items: 3
Items: 
Size: 8312 Color: 7
Size: 5556 Color: 1
Size: 1828 Color: 10

Bin 153: 16 of cap free
Amount of items: 2
Items: 
Size: 11048 Color: 2
Size: 4648 Color: 9

Bin 154: 16 of cap free
Amount of items: 2
Items: 
Size: 12884 Color: 4
Size: 2812 Color: 7

Bin 155: 16 of cap free
Amount of items: 2
Items: 
Size: 13832 Color: 14
Size: 1864 Color: 4

Bin 156: 18 of cap free
Amount of items: 2
Items: 
Size: 10408 Color: 16
Size: 5286 Color: 10

Bin 157: 18 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 2
Size: 1770 Color: 4

Bin 158: 20 of cap free
Amount of items: 2
Items: 
Size: 11474 Color: 6
Size: 4218 Color: 14

Bin 159: 20 of cap free
Amount of items: 2
Items: 
Size: 13562 Color: 18
Size: 2130 Color: 15

Bin 160: 21 of cap free
Amount of items: 2
Items: 
Size: 9693 Color: 6
Size: 5998 Color: 12

Bin 161: 21 of cap free
Amount of items: 2
Items: 
Size: 11559 Color: 10
Size: 4132 Color: 15

Bin 162: 21 of cap free
Amount of items: 2
Items: 
Size: 12519 Color: 2
Size: 3172 Color: 3

Bin 163: 22 of cap free
Amount of items: 2
Items: 
Size: 9840 Color: 16
Size: 5850 Color: 13

Bin 164: 22 of cap free
Amount of items: 2
Items: 
Size: 10314 Color: 15
Size: 5376 Color: 16

Bin 165: 23 of cap free
Amount of items: 2
Items: 
Size: 12801 Color: 3
Size: 2888 Color: 6

Bin 166: 23 of cap free
Amount of items: 2
Items: 
Size: 13317 Color: 7
Size: 2372 Color: 16

Bin 167: 23 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 13
Size: 2301 Color: 10

Bin 168: 26 of cap free
Amount of items: 7
Items: 
Size: 7858 Color: 3
Size: 1528 Color: 7
Size: 1502 Color: 11
Size: 1468 Color: 2
Size: 1434 Color: 16
Size: 1304 Color: 3
Size: 592 Color: 8

Bin 169: 26 of cap free
Amount of items: 4
Items: 
Size: 7862 Color: 0
Size: 4459 Color: 3
Size: 2661 Color: 13
Size: 704 Color: 15

Bin 170: 26 of cap free
Amount of items: 2
Items: 
Size: 10616 Color: 2
Size: 5070 Color: 12

Bin 171: 26 of cap free
Amount of items: 2
Items: 
Size: 14076 Color: 3
Size: 1610 Color: 13

Bin 172: 29 of cap free
Amount of items: 2
Items: 
Size: 11876 Color: 7
Size: 3807 Color: 9

Bin 173: 29 of cap free
Amount of items: 2
Items: 
Size: 12041 Color: 6
Size: 3642 Color: 9

Bin 174: 32 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 6
Size: 3896 Color: 9

Bin 175: 33 of cap free
Amount of items: 2
Items: 
Size: 13237 Color: 1
Size: 2442 Color: 10

Bin 176: 38 of cap free
Amount of items: 2
Items: 
Size: 14028 Color: 1
Size: 1646 Color: 13

Bin 177: 39 of cap free
Amount of items: 2
Items: 
Size: 13556 Color: 16
Size: 2117 Color: 6

Bin 178: 40 of cap free
Amount of items: 2
Items: 
Size: 12868 Color: 9
Size: 2804 Color: 10

Bin 179: 42 of cap free
Amount of items: 2
Items: 
Size: 7894 Color: 10
Size: 7776 Color: 9

Bin 180: 43 of cap free
Amount of items: 2
Items: 
Size: 12676 Color: 9
Size: 2993 Color: 5

Bin 181: 45 of cap free
Amount of items: 2
Items: 
Size: 11416 Color: 6
Size: 4251 Color: 7

Bin 182: 46 of cap free
Amount of items: 3
Items: 
Size: 10298 Color: 15
Size: 5160 Color: 5
Size: 208 Color: 1

Bin 183: 47 of cap free
Amount of items: 2
Items: 
Size: 9932 Color: 10
Size: 5733 Color: 13

Bin 184: 48 of cap free
Amount of items: 2
Items: 
Size: 13922 Color: 16
Size: 1742 Color: 17

Bin 185: 52 of cap free
Amount of items: 2
Items: 
Size: 13074 Color: 0
Size: 2586 Color: 12

Bin 186: 57 of cap free
Amount of items: 3
Items: 
Size: 7876 Color: 16
Size: 6547 Color: 15
Size: 1232 Color: 17

Bin 187: 58 of cap free
Amount of items: 2
Items: 
Size: 9370 Color: 10
Size: 6284 Color: 15

Bin 188: 62 of cap free
Amount of items: 2
Items: 
Size: 9020 Color: 10
Size: 6630 Color: 16

Bin 189: 67 of cap free
Amount of items: 2
Items: 
Size: 12184 Color: 7
Size: 3461 Color: 10

Bin 190: 68 of cap free
Amount of items: 38
Items: 
Size: 552 Color: 11
Size: 548 Color: 9
Size: 532 Color: 1
Size: 516 Color: 18
Size: 516 Color: 16
Size: 504 Color: 4
Size: 488 Color: 0
Size: 484 Color: 0
Size: 480 Color: 19
Size: 476 Color: 16
Size: 472 Color: 19
Size: 472 Color: 5
Size: 432 Color: 12
Size: 422 Color: 8
Size: 418 Color: 11
Size: 416 Color: 8
Size: 416 Color: 3
Size: 400 Color: 19
Size: 400 Color: 7
Size: 400 Color: 1
Size: 396 Color: 13
Size: 392 Color: 0
Size: 384 Color: 8
Size: 376 Color: 15
Size: 376 Color: 3
Size: 374 Color: 16
Size: 370 Color: 10
Size: 360 Color: 14
Size: 360 Color: 12
Size: 356 Color: 1
Size: 352 Color: 13
Size: 352 Color: 12
Size: 352 Color: 4
Size: 312 Color: 12
Size: 304 Color: 9
Size: 304 Color: 5
Size: 300 Color: 2
Size: 280 Color: 17

Bin 191: 69 of cap free
Amount of items: 2
Items: 
Size: 11129 Color: 18
Size: 4514 Color: 4

Bin 192: 78 of cap free
Amount of items: 21
Items: 
Size: 1002 Color: 1
Size: 944 Color: 18
Size: 912 Color: 12
Size: 896 Color: 17
Size: 880 Color: 11
Size: 848 Color: 5
Size: 824 Color: 5
Size: 816 Color: 5
Size: 804 Color: 17
Size: 804 Color: 10
Size: 768 Color: 15
Size: 768 Color: 9
Size: 760 Color: 19
Size: 736 Color: 16
Size: 632 Color: 14
Size: 598 Color: 11
Size: 560 Color: 17
Size: 560 Color: 8
Size: 560 Color: 4
Size: 538 Color: 19
Size: 424 Color: 8

Bin 193: 80 of cap free
Amount of items: 2
Items: 
Size: 12398 Color: 4
Size: 3234 Color: 2

Bin 194: 88 of cap free
Amount of items: 2
Items: 
Size: 12850 Color: 8
Size: 2774 Color: 3

Bin 195: 100 of cap free
Amount of items: 2
Items: 
Size: 10840 Color: 8
Size: 4772 Color: 18

Bin 196: 106 of cap free
Amount of items: 2
Items: 
Size: 12153 Color: 18
Size: 3453 Color: 8

Bin 197: 122 of cap free
Amount of items: 2
Items: 
Size: 11342 Color: 17
Size: 4248 Color: 12

Bin 198: 126 of cap free
Amount of items: 2
Items: 
Size: 11834 Color: 13
Size: 3752 Color: 4

Bin 199: 13308 of cap free
Amount of items: 9
Items: 
Size: 304 Color: 0
Size: 296 Color: 9
Size: 296 Color: 8
Size: 288 Color: 1
Size: 284 Color: 19
Size: 272 Color: 17
Size: 272 Color: 16
Size: 208 Color: 10
Size: 184 Color: 3

Total size: 3110976
Total free space: 15712

