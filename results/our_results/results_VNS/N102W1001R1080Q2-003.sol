Capicity Bin: 1001
Lower Bound: 47

Bins used: 51
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 1
Size: 134 Color: 0
Size: 106 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 0
Size: 104 Color: 1
Size: 101 Color: 1

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 0
Size: 204 Color: 1

Bin 4: 1 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 1
Size: 344 Color: 0

Bin 5: 1 of cap free
Amount of items: 3
Items: 
Size: 716 Color: 1
Size: 143 Color: 1
Size: 141 Color: 0

Bin 6: 2 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 0
Size: 415 Color: 1

Bin 7: 2 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 302 Color: 1

Bin 8: 3 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 1
Size: 446 Color: 0

Bin 9: 3 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 0
Size: 275 Color: 1

Bin 10: 3 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 0
Size: 146 Color: 1
Size: 118 Color: 1

Bin 11: 4 of cap free
Amount of items: 3
Items: 
Size: 616 Color: 0
Size: 226 Color: 1
Size: 155 Color: 1

Bin 12: 5 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 0
Size: 328 Color: 1

Bin 13: 5 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 242 Color: 0

Bin 14: 7 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 0
Size: 391 Color: 1

Bin 15: 7 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 0
Size: 357 Color: 1

Bin 16: 8 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 1
Size: 357 Color: 0

Bin 17: 10 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 1
Size: 475 Color: 0

Bin 18: 11 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 246 Color: 1

Bin 19: 14 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 372 Color: 0

Bin 20: 15 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 191 Color: 0

Bin 21: 17 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 0
Size: 479 Color: 1

Bin 22: 18 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 0
Size: 241 Color: 1

Bin 23: 25 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 448 Color: 1

Bin 24: 28 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 445 Color: 1

Bin 25: 28 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 189 Color: 1

Bin 26: 29 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 220 Color: 0

Bin 27: 31 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 0
Size: 375 Color: 1

Bin 28: 34 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 0
Size: 317 Color: 1

Bin 29: 36 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 181 Color: 1

Bin 30: 37 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 169 Color: 0

Bin 31: 38 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 0
Size: 437 Color: 1

Bin 32: 41 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 281 Color: 0

Bin 33: 43 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 1
Size: 163 Color: 1
Size: 146 Color: 0

Bin 34: 46 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 0
Size: 307 Color: 1

Bin 35: 46 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 211 Color: 0

Bin 36: 47 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 1
Size: 347 Color: 0

Bin 37: 48 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 0
Size: 170 Color: 1

Bin 38: 55 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 1
Size: 277 Color: 0

Bin 39: 65 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 1
Size: 338 Color: 0

Bin 40: 72 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 428 Color: 1

Bin 41: 112 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 0
Size: 295 Color: 1

Bin 42: 154 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 0
Size: 270 Color: 1

Bin 43: 154 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 1
Size: 276 Color: 0

Bin 44: 232 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 0
Size: 268 Color: 1

Bin 45: 305 of cap free
Amount of items: 2
Items: 
Size: 471 Color: 0
Size: 225 Color: 1

Bin 46: 436 of cap free
Amount of items: 1
Items: 
Size: 565 Color: 1

Bin 47: 457 of cap free
Amount of items: 1
Items: 
Size: 544 Color: 1

Bin 48: 461 of cap free
Amount of items: 1
Items: 
Size: 540 Color: 1

Bin 49: 462 of cap free
Amount of items: 1
Items: 
Size: 539 Color: 1

Bin 50: 470 of cap free
Amount of items: 1
Items: 
Size: 531 Color: 1

Bin 51: 506 of cap free
Amount of items: 1
Items: 
Size: 495 Color: 0

Total size: 46417
Total free space: 4634

