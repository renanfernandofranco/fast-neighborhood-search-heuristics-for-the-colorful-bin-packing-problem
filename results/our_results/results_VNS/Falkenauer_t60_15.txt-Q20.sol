Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 17
Size: 261 Color: 13
Size: 252 Color: 19

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 10
Size: 328 Color: 17
Size: 312 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 11
Size: 291 Color: 13
Size: 258 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 12
Size: 291 Color: 15
Size: 267 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 14
Size: 296 Color: 7
Size: 250 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 15
Size: 262 Color: 7
Size: 257 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 4
Size: 274 Color: 1
Size: 255 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 13
Size: 271 Color: 0
Size: 266 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8
Size: 323 Color: 18
Size: 251 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 9
Size: 329 Color: 5
Size: 279 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 17
Size: 300 Color: 14
Size: 252 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 336 Color: 3
Size: 281 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 2
Size: 299 Color: 19
Size: 292 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 9
Size: 299 Color: 8
Size: 288 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 3
Size: 263 Color: 18
Size: 252 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 18
Size: 288 Color: 7
Size: 261 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 3
Size: 347 Color: 18
Size: 264 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 10
Size: 274 Color: 17
Size: 254 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 16
Size: 256 Color: 14
Size: 253 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 4
Size: 296 Color: 11
Size: 273 Color: 9

Total size: 20000
Total free space: 0

