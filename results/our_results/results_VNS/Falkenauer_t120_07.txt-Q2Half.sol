Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 254 Color: 0
Size: 262 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 294 Color: 1
Size: 273 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 308 Color: 1
Size: 266 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 364 Color: 1
Size: 262 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 347 Color: 1
Size: 276 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 250 Color: 0
Size: 328 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 342 Color: 1
Size: 255 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 335 Color: 1
Size: 251 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 256 Color: 1
Size: 254 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 312 Color: 1
Size: 252 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 352 Color: 1
Size: 273 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 315 Color: 1
Size: 275 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 361 Color: 1
Size: 276 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 1
Size: 250 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 336 Color: 1
Size: 268 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 302 Color: 1
Size: 252 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 306 Color: 1
Size: 252 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 357 Color: 1
Size: 270 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 290 Color: 1
Size: 274 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 318 Color: 1
Size: 310 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 278 Color: 0
Size: 276 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 339 Color: 1
Size: 274 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 258 Color: 1
Size: 257 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 292 Color: 1
Size: 250 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 287 Color: 1
Size: 268 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 305 Color: 0
Size: 272 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 329 Color: 1
Size: 269 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 258 Color: 1
Size: 252 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 308 Color: 1
Size: 265 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 306 Color: 1
Size: 275 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 323 Color: 1
Size: 302 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 270 Color: 1
Size: 260 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 334 Color: 1
Size: 280 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 254 Color: 1
Size: 253 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 352 Color: 1
Size: 276 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 349 Color: 1
Size: 263 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 270 Color: 1
Size: 258 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 285 Color: 1
Size: 253 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 330 Color: 1
Size: 262 Color: 0

Total size: 40000
Total free space: 0

