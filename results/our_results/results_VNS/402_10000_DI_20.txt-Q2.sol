Capicity Bin: 7824
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 20
Items: 
Size: 504 Color: 1
Size: 494 Color: 1
Size: 488 Color: 1
Size: 484 Color: 1
Size: 456 Color: 1
Size: 448 Color: 1
Size: 440 Color: 0
Size: 434 Color: 1
Size: 434 Color: 1
Size: 432 Color: 1
Size: 376 Color: 0
Size: 352 Color: 0
Size: 352 Color: 0
Size: 328 Color: 0
Size: 328 Color: 0
Size: 308 Color: 0
Size: 308 Color: 0
Size: 304 Color: 0
Size: 296 Color: 0
Size: 258 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 3921 Color: 0
Size: 1834 Color: 1
Size: 1547 Color: 1
Size: 354 Color: 0
Size: 168 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3917 Color: 1
Size: 3257 Color: 0
Size: 650 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 0
Size: 3440 Color: 1
Size: 216 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4470 Color: 1
Size: 2798 Color: 0
Size: 556 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 1
Size: 2795 Color: 0
Size: 558 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4556 Color: 1
Size: 3048 Color: 0
Size: 220 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5174 Color: 1
Size: 2398 Color: 1
Size: 252 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5195 Color: 0
Size: 2191 Color: 1
Size: 438 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5215 Color: 0
Size: 2433 Color: 1
Size: 176 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5219 Color: 0
Size: 2433 Color: 1
Size: 172 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5235 Color: 1
Size: 2159 Color: 1
Size: 430 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5330 Color: 1
Size: 2210 Color: 0
Size: 284 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5434 Color: 0
Size: 1774 Color: 1
Size: 616 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5970 Color: 0
Size: 1644 Color: 1
Size: 210 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6196 Color: 1
Size: 1436 Color: 1
Size: 192 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6246 Color: 1
Size: 1478 Color: 1
Size: 100 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 0
Size: 1056 Color: 1
Size: 476 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 1
Size: 1068 Color: 1
Size: 408 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 0
Size: 746 Color: 0
Size: 682 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6436 Color: 0
Size: 1268 Color: 1
Size: 120 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6706 Color: 0
Size: 672 Color: 1
Size: 446 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6741 Color: 0
Size: 903 Color: 0
Size: 180 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6794 Color: 1
Size: 666 Color: 0
Size: 364 Color: 0

Bin 25: 1 of cap free
Amount of items: 9
Items: 
Size: 3913 Color: 1
Size: 648 Color: 1
Size: 648 Color: 0
Size: 556 Color: 1
Size: 552 Color: 0
Size: 544 Color: 1
Size: 442 Color: 0
Size: 280 Color: 0
Size: 240 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 4855 Color: 0
Size: 2778 Color: 1
Size: 190 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 5147 Color: 1
Size: 2180 Color: 1
Size: 496 Color: 0

Bin 28: 1 of cap free
Amount of items: 4
Items: 
Size: 5212 Color: 1
Size: 2363 Color: 0
Size: 160 Color: 1
Size: 88 Color: 0

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 5291 Color: 1
Size: 2532 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 5699 Color: 0
Size: 1900 Color: 0
Size: 224 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 1
Size: 1771 Color: 1
Size: 200 Color: 0

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 1
Size: 1669 Color: 0
Size: 120 Color: 1

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 6108 Color: 1
Size: 1715 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 6186 Color: 1
Size: 1081 Color: 1
Size: 556 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6248 Color: 0
Size: 1429 Color: 0
Size: 146 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6421 Color: 0
Size: 1020 Color: 1
Size: 382 Color: 1

Bin 37: 1 of cap free
Amount of items: 4
Items: 
Size: 6455 Color: 1
Size: 1208 Color: 0
Size: 112 Color: 1
Size: 48 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6601 Color: 0
Size: 880 Color: 0
Size: 342 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6693 Color: 0
Size: 934 Color: 1
Size: 196 Color: 0

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 4486 Color: 1
Size: 2904 Color: 0
Size: 432 Color: 0

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 1
Size: 2079 Color: 0
Size: 572 Color: 0

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 5335 Color: 0
Size: 2487 Color: 1

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 5351 Color: 0
Size: 2075 Color: 0
Size: 396 Color: 1

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 1
Size: 1906 Color: 0
Size: 216 Color: 1

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 6206 Color: 0
Size: 1480 Color: 1
Size: 136 Color: 0

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 6529 Color: 1
Size: 1293 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 0
Size: 1086 Color: 1
Size: 132 Color: 1

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 4422 Color: 0
Size: 3251 Color: 1
Size: 148 Color: 0

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 6111 Color: 0
Size: 1562 Color: 1
Size: 148 Color: 0

Bin 50: 3 of cap free
Amount of items: 2
Items: 
Size: 6964 Color: 1
Size: 857 Color: 0

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 4879 Color: 0
Size: 2781 Color: 1
Size: 160 Color: 0

Bin 52: 4 of cap free
Amount of items: 2
Items: 
Size: 6557 Color: 0
Size: 1263 Color: 1

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 6727 Color: 1
Size: 957 Color: 0
Size: 136 Color: 1

Bin 54: 4 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 0
Size: 890 Color: 1

Bin 55: 5 of cap free
Amount of items: 2
Items: 
Size: 5823 Color: 1
Size: 1996 Color: 0

Bin 56: 5 of cap free
Amount of items: 2
Items: 
Size: 6273 Color: 1
Size: 1546 Color: 0

Bin 57: 5 of cap free
Amount of items: 2
Items: 
Size: 6309 Color: 0
Size: 1510 Color: 1

Bin 58: 6 of cap free
Amount of items: 2
Items: 
Size: 6366 Color: 0
Size: 1452 Color: 1

Bin 59: 6 of cap free
Amount of items: 2
Items: 
Size: 6548 Color: 0
Size: 1270 Color: 1

Bin 60: 6 of cap free
Amount of items: 2
Items: 
Size: 6677 Color: 1
Size: 1141 Color: 0

Bin 61: 6 of cap free
Amount of items: 2
Items: 
Size: 6797 Color: 1
Size: 1021 Color: 0

Bin 62: 6 of cap free
Amount of items: 2
Items: 
Size: 7024 Color: 1
Size: 794 Color: 0

Bin 63: 7 of cap free
Amount of items: 3
Items: 
Size: 4385 Color: 0
Size: 3124 Color: 1
Size: 308 Color: 1

Bin 64: 7 of cap free
Amount of items: 2
Items: 
Size: 6581 Color: 0
Size: 1236 Color: 1

Bin 65: 7 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 1
Size: 974 Color: 0
Size: 60 Color: 1

Bin 66: 7 of cap free
Amount of items: 2
Items: 
Size: 6780 Color: 0
Size: 1037 Color: 1

Bin 67: 8 of cap free
Amount of items: 3
Items: 
Size: 4092 Color: 1
Size: 3116 Color: 0
Size: 608 Color: 1

Bin 68: 8 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 0
Size: 2327 Color: 1
Size: 366 Color: 0

Bin 69: 8 of cap free
Amount of items: 2
Items: 
Size: 5732 Color: 0
Size: 2084 Color: 1

Bin 70: 8 of cap free
Amount of items: 2
Items: 
Size: 6938 Color: 0
Size: 878 Color: 1

Bin 71: 8 of cap free
Amount of items: 2
Items: 
Size: 6996 Color: 0
Size: 820 Color: 1

Bin 72: 9 of cap free
Amount of items: 3
Items: 
Size: 4841 Color: 0
Size: 2838 Color: 0
Size: 136 Color: 1

Bin 73: 9 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 0
Size: 1057 Color: 1

Bin 74: 10 of cap free
Amount of items: 4
Items: 
Size: 5996 Color: 1
Size: 1650 Color: 0
Size: 104 Color: 1
Size: 64 Color: 0

Bin 75: 10 of cap free
Amount of items: 2
Items: 
Size: 6854 Color: 1
Size: 960 Color: 0

Bin 76: 10 of cap free
Amount of items: 2
Items: 
Size: 6914 Color: 0
Size: 900 Color: 1

Bin 77: 11 of cap free
Amount of items: 2
Items: 
Size: 5307 Color: 0
Size: 2506 Color: 1

Bin 78: 11 of cap free
Amount of items: 2
Items: 
Size: 5619 Color: 0
Size: 2194 Color: 1

Bin 79: 11 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 1
Size: 1196 Color: 0

Bin 80: 11 of cap free
Amount of items: 2
Items: 
Size: 6642 Color: 1
Size: 1171 Color: 0

Bin 81: 11 of cap free
Amount of items: 2
Items: 
Size: 6748 Color: 1
Size: 1065 Color: 0

Bin 82: 12 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 1
Size: 972 Color: 0
Size: 68 Color: 1

Bin 83: 13 of cap free
Amount of items: 4
Items: 
Size: 4788 Color: 0
Size: 2171 Color: 1
Size: 724 Color: 1
Size: 128 Color: 0

Bin 84: 13 of cap free
Amount of items: 2
Items: 
Size: 6825 Color: 1
Size: 986 Color: 0

Bin 85: 15 of cap free
Amount of items: 2
Items: 
Size: 6874 Color: 1
Size: 935 Color: 0

Bin 86: 16 of cap free
Amount of items: 2
Items: 
Size: 5969 Color: 0
Size: 1839 Color: 1

Bin 87: 16 of cap free
Amount of items: 2
Items: 
Size: 6234 Color: 0
Size: 1574 Color: 1

Bin 88: 16 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 0
Size: 900 Color: 1

Bin 89: 17 of cap free
Amount of items: 3
Items: 
Size: 3925 Color: 0
Size: 3258 Color: 0
Size: 624 Color: 1

Bin 90: 17 of cap free
Amount of items: 3
Items: 
Size: 4418 Color: 1
Size: 3261 Color: 0
Size: 128 Color: 1

Bin 91: 17 of cap free
Amount of items: 2
Items: 
Size: 5332 Color: 1
Size: 2475 Color: 0

Bin 92: 17 of cap free
Amount of items: 2
Items: 
Size: 6059 Color: 1
Size: 1748 Color: 0

Bin 93: 20 of cap free
Amount of items: 2
Items: 
Size: 4950 Color: 1
Size: 2854 Color: 0

Bin 94: 22 of cap free
Amount of items: 2
Items: 
Size: 6484 Color: 1
Size: 1318 Color: 0

Bin 95: 23 of cap free
Amount of items: 2
Items: 
Size: 5626 Color: 1
Size: 2175 Color: 0

Bin 96: 24 of cap free
Amount of items: 3
Items: 
Size: 5954 Color: 1
Size: 1122 Color: 0
Size: 724 Color: 0

Bin 97: 25 of cap free
Amount of items: 2
Items: 
Size: 6522 Color: 1
Size: 1277 Color: 0

Bin 98: 27 of cap free
Amount of items: 2
Items: 
Size: 5698 Color: 1
Size: 2099 Color: 0

Bin 99: 28 of cap free
Amount of items: 2
Items: 
Size: 6658 Color: 0
Size: 1138 Color: 1

Bin 100: 28 of cap free
Amount of items: 2
Items: 
Size: 6986 Color: 0
Size: 810 Color: 1

Bin 101: 30 of cap free
Amount of items: 2
Items: 
Size: 5548 Color: 0
Size: 2246 Color: 1

Bin 102: 32 of cap free
Amount of items: 2
Items: 
Size: 5068 Color: 1
Size: 2724 Color: 0

Bin 103: 33 of cap free
Amount of items: 2
Items: 
Size: 6482 Color: 1
Size: 1309 Color: 0

Bin 104: 33 of cap free
Amount of items: 2
Items: 
Size: 6784 Color: 1
Size: 1007 Color: 0

Bin 105: 34 of cap free
Amount of items: 2
Items: 
Size: 3918 Color: 1
Size: 3872 Color: 0

Bin 106: 35 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 0
Size: 945 Color: 1

Bin 107: 36 of cap free
Amount of items: 2
Items: 
Size: 5331 Color: 1
Size: 2457 Color: 0

Bin 108: 37 of cap free
Amount of items: 2
Items: 
Size: 6293 Color: 0
Size: 1494 Color: 1

Bin 109: 37 of cap free
Amount of items: 2
Items: 
Size: 6569 Color: 1
Size: 1218 Color: 0

Bin 110: 40 of cap free
Amount of items: 2
Items: 
Size: 6660 Color: 1
Size: 1124 Color: 0

Bin 111: 40 of cap free
Amount of items: 2
Items: 
Size: 7020 Color: 1
Size: 764 Color: 0

Bin 112: 42 of cap free
Amount of items: 2
Items: 
Size: 5482 Color: 1
Size: 2300 Color: 0

Bin 113: 45 of cap free
Amount of items: 2
Items: 
Size: 6255 Color: 1
Size: 1524 Color: 0

Bin 114: 51 of cap free
Amount of items: 2
Items: 
Size: 6302 Color: 1
Size: 1471 Color: 0

Bin 115: 52 of cap free
Amount of items: 2
Items: 
Size: 6829 Color: 0
Size: 943 Color: 1

Bin 116: 55 of cap free
Amount of items: 2
Items: 
Size: 4902 Color: 0
Size: 2867 Color: 1

Bin 117: 55 of cap free
Amount of items: 2
Items: 
Size: 5538 Color: 0
Size: 2231 Color: 1

Bin 118: 56 of cap free
Amount of items: 2
Items: 
Size: 7006 Color: 1
Size: 762 Color: 0

Bin 119: 56 of cap free
Amount of items: 2
Items: 
Size: 7026 Color: 0
Size: 742 Color: 1

Bin 120: 59 of cap free
Amount of items: 2
Items: 
Size: 5846 Color: 1
Size: 1919 Color: 0

Bin 121: 63 of cap free
Amount of items: 2
Items: 
Size: 5767 Color: 1
Size: 1994 Color: 0

Bin 122: 68 of cap free
Amount of items: 2
Items: 
Size: 4494 Color: 0
Size: 3262 Color: 1

Bin 123: 68 of cap free
Amount of items: 2
Items: 
Size: 5243 Color: 1
Size: 2513 Color: 0

Bin 124: 74 of cap free
Amount of items: 7
Items: 
Size: 3914 Color: 0
Size: 676 Color: 0
Size: 650 Color: 1
Size: 650 Color: 0
Size: 648 Color: 1
Size: 648 Color: 0
Size: 564 Color: 1

Bin 125: 76 of cap free
Amount of items: 29
Items: 
Size: 418 Color: 1
Size: 414 Color: 1
Size: 414 Color: 1
Size: 380 Color: 1
Size: 376 Color: 1
Size: 344 Color: 1
Size: 338 Color: 1
Size: 332 Color: 1
Size: 294 Color: 1
Size: 260 Color: 0
Size: 260 Color: 0
Size: 254 Color: 1
Size: 252 Color: 0
Size: 248 Color: 0
Size: 248 Color: 0
Size: 240 Color: 0
Size: 232 Color: 1
Size: 232 Color: 1
Size: 228 Color: 0
Size: 216 Color: 0
Size: 214 Color: 0
Size: 208 Color: 1
Size: 206 Color: 0
Size: 202 Color: 1
Size: 200 Color: 0
Size: 190 Color: 0
Size: 188 Color: 0
Size: 184 Color: 0
Size: 176 Color: 0

Bin 126: 76 of cap free
Amount of items: 6
Items: 
Size: 3916 Color: 1
Size: 876 Color: 1
Size: 869 Color: 0
Size: 833 Color: 0
Size: 702 Color: 1
Size: 552 Color: 0

Bin 127: 76 of cap free
Amount of items: 2
Items: 
Size: 4966 Color: 0
Size: 2782 Color: 1

Bin 128: 77 of cap free
Amount of items: 2
Items: 
Size: 4487 Color: 0
Size: 3260 Color: 1

Bin 129: 78 of cap free
Amount of items: 2
Items: 
Size: 6462 Color: 0
Size: 1284 Color: 1

Bin 130: 87 of cap free
Amount of items: 2
Items: 
Size: 4484 Color: 0
Size: 3253 Color: 1

Bin 131: 87 of cap free
Amount of items: 2
Items: 
Size: 5299 Color: 0
Size: 2438 Color: 1

Bin 132: 90 of cap free
Amount of items: 2
Items: 
Size: 5523 Color: 0
Size: 2211 Color: 1

Bin 133: 5408 of cap free
Amount of items: 16
Items: 
Size: 192 Color: 1
Size: 176 Color: 0
Size: 170 Color: 1
Size: 166 Color: 1
Size: 160 Color: 1
Size: 156 Color: 1
Size: 152 Color: 1
Size: 152 Color: 0
Size: 152 Color: 0
Size: 152 Color: 0
Size: 148 Color: 1
Size: 148 Color: 1
Size: 144 Color: 0
Size: 128 Color: 0
Size: 112 Color: 0
Size: 108 Color: 0

Total size: 1032768
Total free space: 7824

