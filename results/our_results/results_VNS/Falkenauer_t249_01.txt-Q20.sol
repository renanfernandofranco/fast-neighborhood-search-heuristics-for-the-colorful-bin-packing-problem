Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7
Size: 306 Color: 5
Size: 302 Color: 19

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 5
Size: 316 Color: 8
Size: 260 Color: 11

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 14
Size: 336 Color: 16
Size: 282 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 253 Color: 13
Size: 258 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 15
Size: 252 Color: 7
Size: 251 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 16
Size: 325 Color: 3
Size: 293 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 10
Size: 253 Color: 13
Size: 250 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9
Size: 269 Color: 16
Size: 251 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 6
Size: 322 Color: 0
Size: 277 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 17
Size: 311 Color: 8
Size: 291 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 15
Size: 360 Color: 19
Size: 255 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 6
Size: 323 Color: 3
Size: 252 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 16
Size: 303 Color: 9
Size: 254 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 16
Size: 310 Color: 13
Size: 265 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 12
Size: 353 Color: 7
Size: 270 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 10
Size: 340 Color: 2
Size: 292 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 16
Size: 363 Color: 13
Size: 267 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 18
Size: 283 Color: 16
Size: 283 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 267 Color: 2
Size: 264 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 7
Size: 301 Color: 11
Size: 273 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 19
Size: 312 Color: 18
Size: 260 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 16
Size: 276 Color: 9
Size: 256 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 8
Size: 364 Color: 17
Size: 254 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 13
Size: 279 Color: 11
Size: 277 Color: 7

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 11
Size: 369 Color: 18
Size: 250 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 329 Color: 3
Size: 280 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 303 Color: 5
Size: 251 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 343 Color: 18
Size: 250 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 360 Color: 17
Size: 255 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 19
Size: 314 Color: 6
Size: 276 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 4
Size: 253 Color: 19
Size: 250 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 297 Color: 10
Size: 271 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8
Size: 293 Color: 17
Size: 269 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 368 Color: 16
Size: 260 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 2
Size: 288 Color: 8
Size: 252 Color: 17

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 14
Size: 289 Color: 18
Size: 259 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 11
Size: 344 Color: 3
Size: 269 Color: 7

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 12
Size: 263 Color: 5
Size: 250 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 6
Size: 358 Color: 19
Size: 263 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 12
Size: 359 Color: 18
Size: 276 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 13
Size: 301 Color: 1
Size: 295 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 11
Size: 354 Color: 15
Size: 261 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 11
Size: 321 Color: 8
Size: 254 Color: 12

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 18
Size: 317 Color: 15
Size: 311 Color: 19

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 18
Size: 258 Color: 13
Size: 250 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 14
Size: 323 Color: 11
Size: 284 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 10
Size: 333 Color: 15
Size: 270 Color: 16

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 5
Size: 277 Color: 12
Size: 257 Color: 13

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 10
Size: 281 Color: 7
Size: 262 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 11
Size: 332 Color: 0
Size: 253 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 12
Size: 329 Color: 11
Size: 255 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 17
Size: 328 Color: 13
Size: 250 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 5
Size: 361 Color: 2
Size: 263 Color: 15

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 13
Size: 319 Color: 7
Size: 259 Color: 17

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 15
Size: 305 Color: 2
Size: 255 Color: 14

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 17
Size: 331 Color: 13
Size: 254 Color: 19

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 4
Size: 258 Color: 14
Size: 254 Color: 11

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 17
Size: 257 Color: 7
Size: 252 Color: 10

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 2
Size: 279 Color: 4
Size: 257 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 15
Size: 332 Color: 18
Size: 288 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 309 Color: 14
Size: 308 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 334 Color: 16
Size: 257 Color: 11

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 10
Size: 334 Color: 10
Size: 331 Color: 13

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 10
Size: 361 Color: 8
Size: 258 Color: 14

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 3
Size: 299 Color: 11
Size: 260 Color: 7

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 11
Size: 296 Color: 10
Size: 273 Color: 5

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 15
Size: 309 Color: 7
Size: 284 Color: 4

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 278 Color: 12
Size: 271 Color: 10

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 18
Size: 325 Color: 3
Size: 293 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 8
Size: 255 Color: 7
Size: 254 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 13
Size: 253 Color: 18
Size: 253 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 4
Size: 298 Color: 10
Size: 271 Color: 7

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 8
Size: 251 Color: 2
Size: 250 Color: 17

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 17
Size: 272 Color: 11
Size: 267 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9
Size: 278 Color: 12
Size: 258 Color: 9

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 9
Size: 289 Color: 13
Size: 283 Color: 18

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 3
Size: 291 Color: 1
Size: 250 Color: 12

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 2
Size: 309 Color: 16
Size: 254 Color: 14

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 321 Color: 2
Size: 279 Color: 10

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 4
Size: 281 Color: 11
Size: 267 Color: 9

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 3
Size: 284 Color: 7
Size: 279 Color: 19

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 9
Size: 282 Color: 11
Size: 269 Color: 6

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 13
Size: 291 Color: 7
Size: 258 Color: 18

Total size: 83000
Total free space: 0

