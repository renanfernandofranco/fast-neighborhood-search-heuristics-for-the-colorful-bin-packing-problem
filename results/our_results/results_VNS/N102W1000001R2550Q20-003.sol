Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 383869 Color: 11
Size: 362716 Color: 12
Size: 253416 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 362441 Color: 11
Size: 283524 Color: 13
Size: 354036 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 373758 Color: 17
Size: 309444 Color: 5
Size: 316799 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 381347 Color: 9
Size: 321256 Color: 17
Size: 297398 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 406350 Color: 10
Size: 308939 Color: 7
Size: 284712 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 415404 Color: 12
Size: 318851 Color: 16
Size: 265746 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 463091 Color: 19
Size: 272661 Color: 0
Size: 264249 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 357098 Color: 4
Size: 320227 Color: 11
Size: 322676 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 393763 Color: 14
Size: 308417 Color: 5
Size: 297821 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 435988 Color: 8
Size: 298432 Color: 14
Size: 265581 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 394439 Color: 4
Size: 334391 Color: 17
Size: 271171 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 368313 Color: 14
Size: 315457 Color: 0
Size: 316231 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 342842 Color: 15
Size: 365541 Color: 10
Size: 291618 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 484445 Color: 6
Size: 260292 Color: 4
Size: 255264 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 371847 Color: 10
Size: 299399 Color: 5
Size: 328755 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 359802 Color: 7
Size: 354725 Color: 6
Size: 285474 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 369240 Color: 19
Size: 353449 Color: 5
Size: 277312 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 476412 Color: 14
Size: 268781 Color: 12
Size: 254808 Color: 9

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 352772 Color: 6
Size: 329081 Color: 16
Size: 318148 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375609 Color: 13
Size: 321223 Color: 3
Size: 303169 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 389886 Color: 19
Size: 350281 Color: 1
Size: 259834 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 385426 Color: 2
Size: 343737 Color: 6
Size: 270838 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 407214 Color: 18
Size: 340286 Color: 10
Size: 252501 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 407480 Color: 15
Size: 327914 Color: 13
Size: 264607 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 432699 Color: 10
Size: 289428 Color: 1
Size: 277874 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 434449 Color: 10
Size: 286721 Color: 15
Size: 278831 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 428404 Color: 11
Size: 320712 Color: 5
Size: 250885 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 443400 Color: 15
Size: 301770 Color: 16
Size: 254831 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 436434 Color: 10
Size: 308875 Color: 7
Size: 254692 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 454411 Color: 16
Size: 282001 Color: 0
Size: 263589 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 456141 Color: 3
Size: 283099 Color: 12
Size: 260761 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 462519 Color: 7
Size: 284285 Color: 16
Size: 253197 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 482156 Color: 14
Size: 263145 Color: 6
Size: 254700 Color: 19

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 493339 Color: 7
Size: 256330 Color: 3
Size: 250332 Color: 19

Total size: 34000034
Total free space: 0

