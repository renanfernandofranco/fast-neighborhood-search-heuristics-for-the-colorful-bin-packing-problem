Capicity Bin: 1000001
Lower Bound: 896

Bins used: 899
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 734505 Color: 8
Size: 146005 Color: 13
Size: 119491 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 735349 Color: 3
Size: 159799 Color: 9
Size: 104853 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 733178 Color: 18
Size: 144708 Color: 12
Size: 122115 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 678469 Color: 1
Size: 192462 Color: 0
Size: 129070 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 692560 Color: 5
Size: 177670 Color: 18
Size: 129771 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 781485 Color: 1
Size: 109308 Color: 15
Size: 109208 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 675903 Color: 9
Size: 190185 Color: 19
Size: 133913 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 578976 Color: 19
Size: 225959 Color: 3
Size: 195066 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 621205 Color: 19
Size: 256943 Color: 1
Size: 121853 Color: 1

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 612174 Color: 9
Size: 145714 Color: 19
Size: 121167 Color: 1
Size: 120946 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 509200 Color: 7
Size: 306465 Color: 5
Size: 184336 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 643563 Color: 16
Size: 251085 Color: 11
Size: 105353 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 685657 Color: 5
Size: 206569 Color: 14
Size: 107775 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 616059 Color: 8
Size: 227417 Color: 3
Size: 156525 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 737039 Color: 13
Size: 157372 Color: 9
Size: 105590 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 511271 Color: 2
Size: 331106 Color: 7
Size: 157624 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 650466 Color: 19
Size: 177783 Color: 1
Size: 171752 Color: 2

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 517045 Color: 9
Size: 195778 Color: 5
Size: 149973 Color: 8
Size: 137205 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 603790 Color: 13
Size: 198956 Color: 1
Size: 197255 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 643638 Color: 11
Size: 216715 Color: 1
Size: 139648 Color: 17

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 656948 Color: 1
Size: 343053 Color: 9

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 730042 Color: 13
Size: 138621 Color: 16
Size: 131338 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 579537 Color: 2
Size: 220592 Color: 3
Size: 199872 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 383643 Color: 3
Size: 317191 Color: 0
Size: 299167 Color: 4

Bin 25: 0 of cap free
Amount of items: 4
Items: 
Size: 509510 Color: 3
Size: 200865 Color: 2
Size: 164395 Color: 18
Size: 125231 Color: 19

Bin 26: 0 of cap free
Amount of items: 5
Items: 
Size: 296160 Color: 8
Size: 291184 Color: 16
Size: 151291 Color: 2
Size: 140112 Color: 0
Size: 121254 Color: 9

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 577667 Color: 9
Size: 422334 Color: 1

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 612819 Color: 14
Size: 387182 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 637629 Color: 5
Size: 184734 Color: 14
Size: 177638 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 665560 Color: 10
Size: 188851 Color: 19
Size: 145590 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 677022 Color: 12
Size: 180694 Color: 19
Size: 142285 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 684330 Color: 2
Size: 180669 Color: 2
Size: 135002 Color: 12

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 694939 Color: 5
Size: 175852 Color: 2
Size: 129210 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 698693 Color: 15
Size: 172704 Color: 15
Size: 128604 Color: 5

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 703697 Color: 8
Size: 296304 Color: 17

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 730074 Color: 4
Size: 269927 Color: 9

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 750257 Color: 2
Size: 126701 Color: 9
Size: 123043 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 792841 Color: 17
Size: 106357 Color: 11
Size: 100803 Color: 9

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 680016 Color: 13
Size: 172202 Color: 4
Size: 147782 Color: 13

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 752810 Color: 15
Size: 126452 Color: 14
Size: 120738 Color: 2

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 704074 Color: 15
Size: 187518 Color: 8
Size: 108408 Color: 15

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 686373 Color: 10
Size: 204559 Color: 8
Size: 109068 Color: 12

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 717916 Color: 13
Size: 158288 Color: 9
Size: 123796 Color: 4

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 681037 Color: 9
Size: 203218 Color: 18
Size: 115745 Color: 8

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 698407 Color: 0
Size: 192919 Color: 11
Size: 108674 Color: 9

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 787205 Color: 19
Size: 111705 Color: 5
Size: 101090 Color: 3

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 710206 Color: 3
Size: 153716 Color: 18
Size: 136078 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 642054 Color: 9
Size: 235981 Color: 9
Size: 121965 Color: 3

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 346204 Color: 19
Size: 336620 Color: 11
Size: 317176 Color: 12

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 781538 Color: 7
Size: 110488 Color: 2
Size: 107974 Color: 10

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 346272 Color: 14
Size: 330075 Color: 17
Size: 323653 Color: 10

Bin 52: 1 of cap free
Amount of items: 4
Items: 
Size: 649321 Color: 5
Size: 122020 Color: 9
Size: 120531 Color: 2
Size: 108128 Color: 1

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 763550 Color: 3
Size: 119419 Color: 4
Size: 117031 Color: 7

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 743567 Color: 6
Size: 150776 Color: 9
Size: 105657 Color: 18

Bin 55: 1 of cap free
Amount of items: 4
Items: 
Size: 298516 Color: 18
Size: 298254 Color: 12
Size: 295413 Color: 16
Size: 107817 Color: 8

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 668522 Color: 13
Size: 196744 Color: 0
Size: 134733 Color: 18

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 673575 Color: 1
Size: 170058 Color: 7
Size: 156366 Color: 2

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 675892 Color: 13
Size: 182462 Color: 14
Size: 141645 Color: 4

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 678804 Color: 5
Size: 187624 Color: 19
Size: 133571 Color: 13

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 706284 Color: 16
Size: 148362 Color: 8
Size: 145353 Color: 10

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 681442 Color: 8
Size: 160837 Color: 19
Size: 157720 Color: 10

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 615835 Color: 16
Size: 267708 Color: 15
Size: 116456 Color: 17

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 639889 Color: 10
Size: 256409 Color: 16
Size: 103701 Color: 7

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 743791 Color: 13
Size: 138971 Color: 8
Size: 117237 Color: 16

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 700435 Color: 3
Size: 175999 Color: 19
Size: 123565 Color: 19

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 511067 Color: 9
Size: 383214 Color: 9
Size: 105718 Color: 14

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 775836 Color: 4
Size: 224163 Color: 19

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 742164 Color: 15
Size: 143371 Color: 12
Size: 114464 Color: 15

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 640500 Color: 17
Size: 359499 Color: 13

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 561981 Color: 15
Size: 438017 Color: 8

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 710414 Color: 11
Size: 154618 Color: 16
Size: 134966 Color: 13

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 760411 Color: 4
Size: 239587 Color: 5

Bin 73: 3 of cap free
Amount of items: 2
Items: 
Size: 602269 Color: 11
Size: 397729 Color: 10

Bin 74: 3 of cap free
Amount of items: 2
Items: 
Size: 764679 Color: 16
Size: 235319 Color: 3

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 785047 Color: 7
Size: 109162 Color: 18
Size: 105789 Color: 19

Bin 76: 3 of cap free
Amount of items: 3
Items: 
Size: 600369 Color: 1
Size: 200078 Color: 4
Size: 199551 Color: 15

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 743785 Color: 8
Size: 148493 Color: 18
Size: 107720 Color: 19

Bin 78: 4 of cap free
Amount of items: 3
Items: 
Size: 674365 Color: 1
Size: 164847 Color: 6
Size: 160785 Color: 18

Bin 79: 4 of cap free
Amount of items: 3
Items: 
Size: 699825 Color: 1
Size: 175700 Color: 1
Size: 124472 Color: 0

Bin 80: 4 of cap free
Amount of items: 3
Items: 
Size: 703955 Color: 18
Size: 151867 Color: 1
Size: 144175 Color: 1

Bin 81: 4 of cap free
Amount of items: 3
Items: 
Size: 783811 Color: 7
Size: 110360 Color: 3
Size: 105826 Color: 15

Bin 82: 4 of cap free
Amount of items: 3
Items: 
Size: 750087 Color: 14
Size: 139438 Color: 12
Size: 110472 Color: 15

Bin 83: 4 of cap free
Amount of items: 3
Items: 
Size: 787630 Color: 10
Size: 111436 Color: 8
Size: 100931 Color: 10

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 571731 Color: 15
Size: 428266 Color: 11

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 615378 Color: 11
Size: 253646 Color: 14
Size: 130973 Color: 12

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 415752 Color: 19
Size: 293110 Color: 12
Size: 291135 Color: 17

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 509268 Color: 13
Size: 344959 Color: 3
Size: 145770 Color: 6

Bin 88: 5 of cap free
Amount of items: 3
Items: 
Size: 689361 Color: 19
Size: 158794 Color: 17
Size: 151841 Color: 8

Bin 89: 5 of cap free
Amount of items: 3
Items: 
Size: 706617 Color: 19
Size: 153684 Color: 16
Size: 139695 Color: 10

Bin 90: 5 of cap free
Amount of items: 3
Items: 
Size: 684532 Color: 0
Size: 172996 Color: 17
Size: 142468 Color: 1

Bin 91: 5 of cap free
Amount of items: 3
Items: 
Size: 753939 Color: 16
Size: 130897 Color: 10
Size: 115160 Color: 6

Bin 92: 5 of cap free
Amount of items: 3
Items: 
Size: 760468 Color: 19
Size: 127211 Color: 16
Size: 112317 Color: 0

Bin 93: 5 of cap free
Amount of items: 3
Items: 
Size: 734677 Color: 10
Size: 148856 Color: 14
Size: 116463 Color: 3

Bin 94: 5 of cap free
Amount of items: 3
Items: 
Size: 693362 Color: 0
Size: 189538 Color: 0
Size: 117096 Color: 7

Bin 95: 5 of cap free
Amount of items: 3
Items: 
Size: 746836 Color: 6
Size: 132389 Color: 2
Size: 120771 Color: 5

Bin 96: 5 of cap free
Amount of items: 3
Items: 
Size: 719475 Color: 5
Size: 158894 Color: 5
Size: 121627 Color: 12

Bin 97: 6 of cap free
Amount of items: 3
Items: 
Size: 756145 Color: 17
Size: 122132 Color: 12
Size: 121718 Color: 7

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 779782 Color: 6
Size: 115583 Color: 13
Size: 104630 Color: 6

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 656490 Color: 4
Size: 343505 Color: 17

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 509238 Color: 16
Size: 255034 Color: 6
Size: 235723 Color: 13

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 530468 Color: 6
Size: 469527 Color: 4

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 393180 Color: 3
Size: 313517 Color: 17
Size: 293298 Color: 11

Bin 103: 7 of cap free
Amount of items: 3
Items: 
Size: 709201 Color: 19
Size: 149733 Color: 15
Size: 141060 Color: 1

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 658872 Color: 12
Size: 341122 Color: 0

Bin 105: 7 of cap free
Amount of items: 3
Items: 
Size: 521541 Color: 17
Size: 248211 Color: 2
Size: 230242 Color: 11

Bin 106: 7 of cap free
Amount of items: 3
Items: 
Size: 353299 Color: 0
Size: 333720 Color: 11
Size: 312975 Color: 11

Bin 107: 8 of cap free
Amount of items: 3
Items: 
Size: 710984 Color: 1
Size: 148569 Color: 17
Size: 140440 Color: 3

Bin 108: 8 of cap free
Amount of items: 3
Items: 
Size: 698868 Color: 11
Size: 171413 Color: 9
Size: 129712 Color: 2

Bin 109: 8 of cap free
Amount of items: 3
Items: 
Size: 566657 Color: 9
Size: 237498 Color: 9
Size: 195838 Color: 10

Bin 110: 8 of cap free
Amount of items: 3
Items: 
Size: 656187 Color: 15
Size: 185430 Color: 18
Size: 158376 Color: 19

Bin 111: 8 of cap free
Amount of items: 3
Items: 
Size: 510192 Color: 19
Size: 289296 Color: 0
Size: 200505 Color: 7

Bin 112: 8 of cap free
Amount of items: 3
Items: 
Size: 372596 Color: 8
Size: 313822 Color: 12
Size: 313575 Color: 13

Bin 113: 9 of cap free
Amount of items: 2
Items: 
Size: 513210 Color: 8
Size: 486782 Color: 2

Bin 114: 9 of cap free
Amount of items: 3
Items: 
Size: 681944 Color: 9
Size: 161519 Color: 8
Size: 156529 Color: 18

Bin 115: 9 of cap free
Amount of items: 2
Items: 
Size: 775368 Color: 1
Size: 224624 Color: 3

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 508898 Color: 5
Size: 491094 Color: 19

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 605954 Color: 2
Size: 394037 Color: 14

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 672662 Color: 2
Size: 327329 Color: 19

Bin 119: 10 of cap free
Amount of items: 3
Items: 
Size: 683021 Color: 9
Size: 164677 Color: 5
Size: 152293 Color: 1

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 616186 Color: 13
Size: 383805 Color: 7

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 653382 Color: 14
Size: 223202 Color: 6
Size: 123407 Color: 0

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 668690 Color: 19
Size: 331301 Color: 18

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 653981 Color: 6
Size: 346009 Color: 8

Bin 124: 11 of cap free
Amount of items: 3
Items: 
Size: 689006 Color: 2
Size: 173482 Color: 14
Size: 137502 Color: 5

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 764470 Color: 4
Size: 235520 Color: 17

Bin 126: 11 of cap free
Amount of items: 3
Items: 
Size: 652493 Color: 2
Size: 194322 Color: 16
Size: 153175 Color: 17

Bin 127: 12 of cap free
Amount of items: 3
Items: 
Size: 657270 Color: 12
Size: 174361 Color: 1
Size: 168358 Color: 2

Bin 128: 12 of cap free
Amount of items: 3
Items: 
Size: 692406 Color: 14
Size: 165455 Color: 1
Size: 142128 Color: 8

Bin 129: 12 of cap free
Amount of items: 3
Items: 
Size: 708175 Color: 18
Size: 164772 Color: 3
Size: 127042 Color: 4

Bin 130: 12 of cap free
Amount of items: 3
Items: 
Size: 615405 Color: 15
Size: 196770 Color: 18
Size: 187814 Color: 7

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 660414 Color: 1
Size: 339575 Color: 19

Bin 132: 12 of cap free
Amount of items: 4
Items: 
Size: 282750 Color: 14
Size: 265532 Color: 7
Size: 245844 Color: 10
Size: 205863 Color: 19

Bin 133: 13 of cap free
Amount of items: 3
Items: 
Size: 347414 Color: 3
Size: 339830 Color: 0
Size: 312744 Color: 10

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 663734 Color: 1
Size: 336254 Color: 4

Bin 135: 13 of cap free
Amount of items: 3
Items: 
Size: 684963 Color: 9
Size: 195179 Color: 3
Size: 119846 Color: 8

Bin 136: 13 of cap free
Amount of items: 3
Items: 
Size: 337150 Color: 11
Size: 336442 Color: 0
Size: 326396 Color: 2

Bin 137: 13 of cap free
Amount of items: 4
Items: 
Size: 580535 Color: 19
Size: 190746 Color: 8
Size: 116090 Color: 0
Size: 112617 Color: 15

Bin 138: 13 of cap free
Amount of items: 3
Items: 
Size: 605857 Color: 0
Size: 198321 Color: 19
Size: 195810 Color: 8

Bin 139: 14 of cap free
Amount of items: 3
Items: 
Size: 360126 Color: 1
Size: 336534 Color: 14
Size: 303327 Color: 8

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 530646 Color: 4
Size: 469340 Color: 5

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 762730 Color: 6
Size: 237256 Color: 2

Bin 142: 15 of cap free
Amount of items: 3
Items: 
Size: 672424 Color: 16
Size: 170167 Color: 2
Size: 157395 Color: 9

Bin 143: 15 of cap free
Amount of items: 3
Items: 
Size: 616537 Color: 4
Size: 282389 Color: 9
Size: 101060 Color: 14

Bin 144: 15 of cap free
Amount of items: 2
Items: 
Size: 768560 Color: 7
Size: 231426 Color: 1

Bin 145: 16 of cap free
Amount of items: 2
Items: 
Size: 660898 Color: 12
Size: 339087 Color: 16

Bin 146: 16 of cap free
Amount of items: 2
Items: 
Size: 761663 Color: 2
Size: 238322 Color: 9

Bin 147: 16 of cap free
Amount of items: 3
Items: 
Size: 678794 Color: 3
Size: 166688 Color: 0
Size: 154503 Color: 4

Bin 148: 16 of cap free
Amount of items: 3
Items: 
Size: 667784 Color: 18
Size: 189418 Color: 16
Size: 142783 Color: 14

Bin 149: 17 of cap free
Amount of items: 3
Items: 
Size: 663210 Color: 14
Size: 200748 Color: 11
Size: 136026 Color: 11

Bin 150: 17 of cap free
Amount of items: 3
Items: 
Size: 679776 Color: 15
Size: 191655 Color: 11
Size: 128553 Color: 11

Bin 151: 17 of cap free
Amount of items: 3
Items: 
Size: 359722 Color: 15
Size: 344821 Color: 14
Size: 295441 Color: 4

Bin 152: 17 of cap free
Amount of items: 3
Items: 
Size: 779420 Color: 15
Size: 116181 Color: 0
Size: 104383 Color: 16

Bin 153: 17 of cap free
Amount of items: 3
Items: 
Size: 530390 Color: 19
Size: 237227 Color: 16
Size: 232367 Color: 0

Bin 154: 17 of cap free
Amount of items: 3
Items: 
Size: 578755 Color: 16
Size: 313556 Color: 12
Size: 107673 Color: 12

Bin 155: 18 of cap free
Amount of items: 3
Items: 
Size: 681546 Color: 2
Size: 173774 Color: 4
Size: 144663 Color: 15

Bin 156: 18 of cap free
Amount of items: 2
Items: 
Size: 685058 Color: 8
Size: 314925 Color: 10

Bin 157: 18 of cap free
Amount of items: 3
Items: 
Size: 674456 Color: 6
Size: 182233 Color: 5
Size: 143294 Color: 10

Bin 158: 18 of cap free
Amount of items: 3
Items: 
Size: 524030 Color: 13
Size: 240319 Color: 10
Size: 235634 Color: 18

Bin 159: 18 of cap free
Amount of items: 2
Items: 
Size: 677274 Color: 4
Size: 322709 Color: 15

Bin 160: 19 of cap free
Amount of items: 2
Items: 
Size: 518439 Color: 3
Size: 481543 Color: 9

Bin 161: 19 of cap free
Amount of items: 2
Items: 
Size: 555390 Color: 11
Size: 444592 Color: 1

Bin 162: 19 of cap free
Amount of items: 3
Items: 
Size: 679774 Color: 10
Size: 180593 Color: 5
Size: 139615 Color: 13

Bin 163: 19 of cap free
Amount of items: 2
Items: 
Size: 725328 Color: 8
Size: 274654 Color: 7

Bin 164: 19 of cap free
Amount of items: 3
Items: 
Size: 520712 Color: 8
Size: 242324 Color: 8
Size: 236946 Color: 0

Bin 165: 20 of cap free
Amount of items: 2
Items: 
Size: 646557 Color: 4
Size: 353424 Color: 3

Bin 166: 21 of cap free
Amount of items: 3
Items: 
Size: 676902 Color: 6
Size: 163715 Color: 9
Size: 159363 Color: 6

Bin 167: 21 of cap free
Amount of items: 2
Items: 
Size: 782331 Color: 5
Size: 217649 Color: 13

Bin 168: 21 of cap free
Amount of items: 3
Items: 
Size: 756319 Color: 11
Size: 142247 Color: 7
Size: 101414 Color: 3

Bin 169: 21 of cap free
Amount of items: 3
Items: 
Size: 346101 Color: 6
Size: 331225 Color: 11
Size: 322654 Color: 15

Bin 170: 22 of cap free
Amount of items: 2
Items: 
Size: 566193 Color: 2
Size: 433786 Color: 5

Bin 171: 22 of cap free
Amount of items: 3
Items: 
Size: 655356 Color: 2
Size: 183670 Color: 17
Size: 160953 Color: 5

Bin 172: 23 of cap free
Amount of items: 2
Items: 
Size: 600952 Color: 8
Size: 399026 Color: 16

Bin 173: 23 of cap free
Amount of items: 2
Items: 
Size: 646918 Color: 17
Size: 353060 Color: 5

Bin 174: 23 of cap free
Amount of items: 3
Items: 
Size: 790101 Color: 12
Size: 109250 Color: 16
Size: 100627 Color: 6

Bin 175: 23 of cap free
Amount of items: 3
Items: 
Size: 572266 Color: 2
Size: 233630 Color: 9
Size: 194082 Color: 2

Bin 176: 24 of cap free
Amount of items: 2
Items: 
Size: 564550 Color: 1
Size: 435427 Color: 19

Bin 177: 24 of cap free
Amount of items: 3
Items: 
Size: 388378 Color: 9
Size: 307827 Color: 19
Size: 303772 Color: 19

Bin 178: 25 of cap free
Amount of items: 2
Items: 
Size: 547376 Color: 14
Size: 452600 Color: 19

Bin 179: 25 of cap free
Amount of items: 3
Items: 
Size: 677005 Color: 16
Size: 184037 Color: 4
Size: 138934 Color: 5

Bin 180: 25 of cap free
Amount of items: 2
Items: 
Size: 691707 Color: 14
Size: 308269 Color: 3

Bin 181: 25 of cap free
Amount of items: 2
Items: 
Size: 733619 Color: 5
Size: 266357 Color: 10

Bin 182: 26 of cap free
Amount of items: 2
Items: 
Size: 584477 Color: 4
Size: 415498 Color: 0

Bin 183: 26 of cap free
Amount of items: 3
Items: 
Size: 681607 Color: 14
Size: 178590 Color: 3
Size: 139778 Color: 3

Bin 184: 27 of cap free
Amount of items: 3
Items: 
Size: 704069 Color: 19
Size: 167013 Color: 10
Size: 128892 Color: 12

Bin 185: 27 of cap free
Amount of items: 3
Items: 
Size: 650761 Color: 7
Size: 182762 Color: 12
Size: 166451 Color: 19

Bin 186: 28 of cap free
Amount of items: 3
Items: 
Size: 656823 Color: 11
Size: 225052 Color: 6
Size: 118098 Color: 19

Bin 187: 28 of cap free
Amount of items: 3
Items: 
Size: 357900 Color: 4
Size: 328906 Color: 14
Size: 313167 Color: 12

Bin 188: 28 of cap free
Amount of items: 2
Items: 
Size: 653916 Color: 14
Size: 346057 Color: 17

Bin 189: 28 of cap free
Amount of items: 2
Items: 
Size: 743519 Color: 7
Size: 256454 Color: 1

Bin 190: 28 of cap free
Amount of items: 3
Items: 
Size: 619434 Color: 18
Size: 191871 Color: 2
Size: 188668 Color: 0

Bin 191: 28 of cap free
Amount of items: 3
Items: 
Size: 393610 Color: 2
Size: 308578 Color: 8
Size: 297785 Color: 0

Bin 192: 29 of cap free
Amount of items: 2
Items: 
Size: 601175 Color: 15
Size: 398797 Color: 5

Bin 193: 30 of cap free
Amount of items: 2
Items: 
Size: 588372 Color: 9
Size: 411599 Color: 10

Bin 194: 30 of cap free
Amount of items: 3
Items: 
Size: 623766 Color: 14
Size: 197094 Color: 18
Size: 179111 Color: 2

Bin 195: 30 of cap free
Amount of items: 2
Items: 
Size: 774860 Color: 7
Size: 225111 Color: 1

Bin 196: 31 of cap free
Amount of items: 2
Items: 
Size: 642376 Color: 18
Size: 357594 Color: 7

Bin 197: 31 of cap free
Amount of items: 2
Items: 
Size: 535568 Color: 14
Size: 464402 Color: 7

Bin 198: 31 of cap free
Amount of items: 3
Items: 
Size: 360445 Color: 6
Size: 328705 Color: 5
Size: 310820 Color: 19

Bin 199: 33 of cap free
Amount of items: 2
Items: 
Size: 584662 Color: 12
Size: 415306 Color: 8

Bin 200: 33 of cap free
Amount of items: 2
Items: 
Size: 736223 Color: 14
Size: 263745 Color: 2

Bin 201: 33 of cap free
Amount of items: 2
Items: 
Size: 770737 Color: 3
Size: 229231 Color: 0

Bin 202: 33 of cap free
Amount of items: 3
Items: 
Size: 777099 Color: 11
Size: 114538 Color: 15
Size: 108331 Color: 16

Bin 203: 34 of cap free
Amount of items: 2
Items: 
Size: 749699 Color: 7
Size: 250268 Color: 1

Bin 204: 35 of cap free
Amount of items: 2
Items: 
Size: 574822 Color: 2
Size: 425144 Color: 16

Bin 205: 35 of cap free
Amount of items: 2
Items: 
Size: 609983 Color: 3
Size: 389983 Color: 19

Bin 206: 35 of cap free
Amount of items: 3
Items: 
Size: 519388 Color: 2
Size: 243261 Color: 15
Size: 237317 Color: 13

Bin 207: 35 of cap free
Amount of items: 3
Items: 
Size: 556691 Color: 12
Size: 249280 Color: 5
Size: 193995 Color: 16

Bin 208: 35 of cap free
Amount of items: 3
Items: 
Size: 337309 Color: 9
Size: 331377 Color: 19
Size: 331280 Color: 14

Bin 209: 37 of cap free
Amount of items: 2
Items: 
Size: 581569 Color: 9
Size: 418395 Color: 6

Bin 210: 37 of cap free
Amount of items: 2
Items: 
Size: 678244 Color: 17
Size: 321720 Color: 8

Bin 211: 37 of cap free
Amount of items: 3
Items: 
Size: 619074 Color: 19
Size: 193093 Color: 9
Size: 187797 Color: 15

Bin 212: 37 of cap free
Amount of items: 3
Items: 
Size: 524103 Color: 16
Size: 246266 Color: 17
Size: 229595 Color: 12

Bin 213: 38 of cap free
Amount of items: 2
Items: 
Size: 524808 Color: 5
Size: 475155 Color: 3

Bin 214: 38 of cap free
Amount of items: 3
Items: 
Size: 654647 Color: 19
Size: 181479 Color: 16
Size: 163837 Color: 10

Bin 215: 38 of cap free
Amount of items: 2
Items: 
Size: 728945 Color: 5
Size: 271018 Color: 4

Bin 216: 38 of cap free
Amount of items: 2
Items: 
Size: 764264 Color: 13
Size: 235699 Color: 14

Bin 217: 38 of cap free
Amount of items: 2
Items: 
Size: 594566 Color: 11
Size: 405397 Color: 10

Bin 218: 39 of cap free
Amount of items: 2
Items: 
Size: 746728 Color: 2
Size: 253234 Color: 11

Bin 219: 40 of cap free
Amount of items: 2
Items: 
Size: 598054 Color: 6
Size: 401907 Color: 16

Bin 220: 40 of cap free
Amount of items: 3
Items: 
Size: 622066 Color: 16
Size: 198641 Color: 0
Size: 179254 Color: 13

Bin 221: 40 of cap free
Amount of items: 2
Items: 
Size: 795344 Color: 8
Size: 204617 Color: 9

Bin 222: 40 of cap free
Amount of items: 2
Items: 
Size: 794713 Color: 5
Size: 205248 Color: 7

Bin 223: 41 of cap free
Amount of items: 2
Items: 
Size: 526994 Color: 18
Size: 472966 Color: 19

Bin 224: 41 of cap free
Amount of items: 2
Items: 
Size: 553701 Color: 8
Size: 446259 Color: 4

Bin 225: 41 of cap free
Amount of items: 2
Items: 
Size: 649571 Color: 14
Size: 350389 Color: 16

Bin 226: 41 of cap free
Amount of items: 2
Items: 
Size: 716191 Color: 0
Size: 283769 Color: 12

Bin 227: 41 of cap free
Amount of items: 3
Items: 
Size: 720710 Color: 12
Size: 160834 Color: 16
Size: 118416 Color: 8

Bin 228: 42 of cap free
Amount of items: 2
Items: 
Size: 575044 Color: 3
Size: 424915 Color: 2

Bin 229: 43 of cap free
Amount of items: 2
Items: 
Size: 524006 Color: 3
Size: 475952 Color: 16

Bin 230: 43 of cap free
Amount of items: 2
Items: 
Size: 559228 Color: 3
Size: 440730 Color: 4

Bin 231: 43 of cap free
Amount of items: 2
Items: 
Size: 714317 Color: 16
Size: 285641 Color: 8

Bin 232: 43 of cap free
Amount of items: 2
Items: 
Size: 616593 Color: 9
Size: 383365 Color: 13

Bin 233: 44 of cap free
Amount of items: 2
Items: 
Size: 537178 Color: 18
Size: 462779 Color: 2

Bin 234: 44 of cap free
Amount of items: 3
Items: 
Size: 673759 Color: 4
Size: 186828 Color: 12
Size: 139370 Color: 3

Bin 235: 44 of cap free
Amount of items: 2
Items: 
Size: 719824 Color: 17
Size: 280133 Color: 13

Bin 236: 44 of cap free
Amount of items: 2
Items: 
Size: 797438 Color: 10
Size: 202519 Color: 17

Bin 237: 45 of cap free
Amount of items: 2
Items: 
Size: 573009 Color: 14
Size: 426947 Color: 13

Bin 238: 46 of cap free
Amount of items: 2
Items: 
Size: 712066 Color: 2
Size: 287889 Color: 6

Bin 239: 47 of cap free
Amount of items: 2
Items: 
Size: 626886 Color: 8
Size: 373068 Color: 7

Bin 240: 47 of cap free
Amount of items: 3
Items: 
Size: 519326 Color: 18
Size: 294908 Color: 17
Size: 185720 Color: 15

Bin 241: 47 of cap free
Amount of items: 3
Items: 
Size: 364280 Color: 12
Size: 331328 Color: 4
Size: 304346 Color: 9

Bin 242: 48 of cap free
Amount of items: 2
Items: 
Size: 754324 Color: 2
Size: 245629 Color: 13

Bin 243: 48 of cap free
Amount of items: 2
Items: 
Size: 781615 Color: 8
Size: 218338 Color: 19

Bin 244: 49 of cap free
Amount of items: 2
Items: 
Size: 788347 Color: 12
Size: 211605 Color: 14

Bin 245: 51 of cap free
Amount of items: 2
Items: 
Size: 556964 Color: 14
Size: 442986 Color: 3

Bin 246: 52 of cap free
Amount of items: 2
Items: 
Size: 713998 Color: 10
Size: 285951 Color: 6

Bin 247: 52 of cap free
Amount of items: 2
Items: 
Size: 519948 Color: 2
Size: 480001 Color: 6

Bin 248: 53 of cap free
Amount of items: 2
Items: 
Size: 606200 Color: 2
Size: 393748 Color: 4

Bin 249: 53 of cap free
Amount of items: 2
Items: 
Size: 517936 Color: 6
Size: 482012 Color: 4

Bin 250: 54 of cap free
Amount of items: 2
Items: 
Size: 565167 Color: 12
Size: 434780 Color: 19

Bin 251: 54 of cap free
Amount of items: 2
Items: 
Size: 638971 Color: 18
Size: 360976 Color: 16

Bin 252: 55 of cap free
Amount of items: 3
Items: 
Size: 658321 Color: 1
Size: 171139 Color: 3
Size: 170486 Color: 16

Bin 253: 55 of cap free
Amount of items: 3
Items: 
Size: 429173 Color: 4
Size: 285548 Color: 4
Size: 285225 Color: 1

Bin 254: 56 of cap free
Amount of items: 3
Items: 
Size: 521812 Color: 5
Size: 245064 Color: 18
Size: 233069 Color: 13

Bin 255: 56 of cap free
Amount of items: 2
Items: 
Size: 585386 Color: 3
Size: 414559 Color: 4

Bin 256: 56 of cap free
Amount of items: 2
Items: 
Size: 590533 Color: 18
Size: 409412 Color: 8

Bin 257: 56 of cap free
Amount of items: 3
Items: 
Size: 671958 Color: 5
Size: 180308 Color: 4
Size: 147679 Color: 3

Bin 258: 56 of cap free
Amount of items: 2
Items: 
Size: 646738 Color: 16
Size: 353207 Color: 13

Bin 259: 57 of cap free
Amount of items: 3
Items: 
Size: 580725 Color: 0
Size: 233806 Color: 6
Size: 185413 Color: 14

Bin 260: 57 of cap free
Amount of items: 2
Items: 
Size: 572659 Color: 3
Size: 427285 Color: 5

Bin 261: 57 of cap free
Amount of items: 3
Items: 
Size: 676263 Color: 2
Size: 174339 Color: 9
Size: 149342 Color: 5

Bin 262: 57 of cap free
Amount of items: 2
Items: 
Size: 600428 Color: 11
Size: 399516 Color: 6

Bin 263: 57 of cap free
Amount of items: 2
Items: 
Size: 581268 Color: 6
Size: 418676 Color: 16

Bin 264: 58 of cap free
Amount of items: 2
Items: 
Size: 528094 Color: 3
Size: 471849 Color: 12

Bin 265: 58 of cap free
Amount of items: 2
Items: 
Size: 691313 Color: 8
Size: 308630 Color: 7

Bin 266: 59 of cap free
Amount of items: 2
Items: 
Size: 718716 Color: 5
Size: 281226 Color: 14

Bin 267: 59 of cap free
Amount of items: 2
Items: 
Size: 531271 Color: 11
Size: 468671 Color: 17

Bin 268: 61 of cap free
Amount of items: 2
Items: 
Size: 555039 Color: 5
Size: 444901 Color: 14

Bin 269: 62 of cap free
Amount of items: 3
Items: 
Size: 735095 Color: 19
Size: 150557 Color: 13
Size: 114287 Color: 11

Bin 270: 62 of cap free
Amount of items: 2
Items: 
Size: 611238 Color: 2
Size: 388701 Color: 19

Bin 271: 62 of cap free
Amount of items: 3
Items: 
Size: 573291 Color: 5
Size: 230152 Color: 13
Size: 196496 Color: 13

Bin 272: 63 of cap free
Amount of items: 2
Items: 
Size: 715831 Color: 2
Size: 284107 Color: 4

Bin 273: 64 of cap free
Amount of items: 2
Items: 
Size: 792350 Color: 11
Size: 207587 Color: 5

Bin 274: 64 of cap free
Amount of items: 2
Items: 
Size: 798995 Color: 17
Size: 200942 Color: 11

Bin 275: 64 of cap free
Amount of items: 2
Items: 
Size: 656376 Color: 6
Size: 343561 Color: 14

Bin 276: 64 of cap free
Amount of items: 2
Items: 
Size: 648415 Color: 17
Size: 351522 Color: 6

Bin 277: 65 of cap free
Amount of items: 2
Items: 
Size: 563537 Color: 3
Size: 436399 Color: 15

Bin 278: 65 of cap free
Amount of items: 2
Items: 
Size: 591014 Color: 14
Size: 408922 Color: 1

Bin 279: 65 of cap free
Amount of items: 2
Items: 
Size: 748337 Color: 17
Size: 251599 Color: 8

Bin 280: 65 of cap free
Amount of items: 2
Items: 
Size: 758352 Color: 18
Size: 241584 Color: 5

Bin 281: 66 of cap free
Amount of items: 2
Items: 
Size: 515128 Color: 11
Size: 484807 Color: 10

Bin 282: 66 of cap free
Amount of items: 3
Items: 
Size: 657399 Color: 14
Size: 189781 Color: 12
Size: 152755 Color: 16

Bin 283: 67 of cap free
Amount of items: 2
Items: 
Size: 547160 Color: 2
Size: 452774 Color: 7

Bin 284: 67 of cap free
Amount of items: 3
Items: 
Size: 635004 Color: 17
Size: 215741 Color: 19
Size: 149189 Color: 9

Bin 285: 68 of cap free
Amount of items: 2
Items: 
Size: 548005 Color: 15
Size: 451928 Color: 7

Bin 286: 68 of cap free
Amount of items: 2
Items: 
Size: 615035 Color: 1
Size: 384898 Color: 10

Bin 287: 68 of cap free
Amount of items: 2
Items: 
Size: 676467 Color: 0
Size: 323466 Color: 9

Bin 288: 69 of cap free
Amount of items: 2
Items: 
Size: 632886 Color: 7
Size: 367046 Color: 10

Bin 289: 70 of cap free
Amount of items: 2
Items: 
Size: 549631 Color: 12
Size: 450300 Color: 19

Bin 290: 70 of cap free
Amount of items: 2
Items: 
Size: 587119 Color: 8
Size: 412812 Color: 15

Bin 291: 70 of cap free
Amount of items: 2
Items: 
Size: 705597 Color: 8
Size: 294334 Color: 14

Bin 292: 70 of cap free
Amount of items: 2
Items: 
Size: 749952 Color: 11
Size: 249979 Color: 2

Bin 293: 70 of cap free
Amount of items: 2
Items: 
Size: 789949 Color: 10
Size: 209982 Color: 1

Bin 294: 71 of cap free
Amount of items: 2
Items: 
Size: 536129 Color: 5
Size: 463801 Color: 0

Bin 295: 71 of cap free
Amount of items: 2
Items: 
Size: 612868 Color: 14
Size: 387062 Color: 3

Bin 296: 71 of cap free
Amount of items: 2
Items: 
Size: 771273 Color: 15
Size: 228657 Color: 10

Bin 297: 71 of cap free
Amount of items: 2
Items: 
Size: 600075 Color: 4
Size: 399855 Color: 9

Bin 298: 72 of cap free
Amount of items: 3
Items: 
Size: 790486 Color: 0
Size: 107083 Color: 3
Size: 102360 Color: 13

Bin 299: 73 of cap free
Amount of items: 2
Items: 
Size: 583106 Color: 13
Size: 416822 Color: 5

Bin 300: 74 of cap free
Amount of items: 3
Items: 
Size: 361646 Color: 1
Size: 339652 Color: 17
Size: 298629 Color: 18

Bin 301: 74 of cap free
Amount of items: 2
Items: 
Size: 596779 Color: 1
Size: 403148 Color: 6

Bin 302: 74 of cap free
Amount of items: 3
Items: 
Size: 686982 Color: 16
Size: 160499 Color: 10
Size: 152446 Color: 9

Bin 303: 74 of cap free
Amount of items: 2
Items: 
Size: 741144 Color: 8
Size: 258783 Color: 13

Bin 304: 75 of cap free
Amount of items: 2
Items: 
Size: 547417 Color: 8
Size: 452509 Color: 5

Bin 305: 76 of cap free
Amount of items: 2
Items: 
Size: 535019 Color: 3
Size: 464906 Color: 16

Bin 306: 76 of cap free
Amount of items: 2
Items: 
Size: 720024 Color: 15
Size: 279901 Color: 0

Bin 307: 77 of cap free
Amount of items: 2
Items: 
Size: 507678 Color: 16
Size: 492246 Color: 19

Bin 308: 77 of cap free
Amount of items: 2
Items: 
Size: 598744 Color: 16
Size: 401180 Color: 1

Bin 309: 78 of cap free
Amount of items: 2
Items: 
Size: 590675 Color: 5
Size: 409248 Color: 0

Bin 310: 78 of cap free
Amount of items: 2
Items: 
Size: 515361 Color: 12
Size: 484562 Color: 14

Bin 311: 79 of cap free
Amount of items: 2
Items: 
Size: 633210 Color: 1
Size: 366712 Color: 11

Bin 312: 80 of cap free
Amount of items: 2
Items: 
Size: 678896 Color: 11
Size: 321025 Color: 1

Bin 313: 81 of cap free
Amount of items: 2
Items: 
Size: 670097 Color: 11
Size: 329823 Color: 14

Bin 314: 81 of cap free
Amount of items: 2
Items: 
Size: 738227 Color: 9
Size: 261693 Color: 8

Bin 315: 81 of cap free
Amount of items: 2
Items: 
Size: 764238 Color: 13
Size: 235682 Color: 3

Bin 316: 81 of cap free
Amount of items: 2
Items: 
Size: 571440 Color: 14
Size: 428480 Color: 13

Bin 317: 81 of cap free
Amount of items: 3
Items: 
Size: 392488 Color: 5
Size: 316286 Color: 17
Size: 291146 Color: 9

Bin 318: 82 of cap free
Amount of items: 2
Items: 
Size: 772878 Color: 15
Size: 227041 Color: 19

Bin 319: 83 of cap free
Amount of items: 2
Items: 
Size: 725199 Color: 14
Size: 274719 Color: 18

Bin 320: 84 of cap free
Amount of items: 2
Items: 
Size: 621492 Color: 3
Size: 378425 Color: 1

Bin 321: 84 of cap free
Amount of items: 3
Items: 
Size: 521480 Color: 11
Size: 284924 Color: 9
Size: 193513 Color: 15

Bin 322: 84 of cap free
Amount of items: 2
Items: 
Size: 795018 Color: 15
Size: 204899 Color: 17

Bin 323: 85 of cap free
Amount of items: 2
Items: 
Size: 513136 Color: 4
Size: 486780 Color: 3

Bin 324: 85 of cap free
Amount of items: 2
Items: 
Size: 514856 Color: 6
Size: 485060 Color: 7

Bin 325: 85 of cap free
Amount of items: 4
Items: 
Size: 303320 Color: 15
Size: 298461 Color: 18
Size: 272064 Color: 11
Size: 126071 Color: 8

Bin 326: 86 of cap free
Amount of items: 2
Items: 
Size: 658224 Color: 15
Size: 341691 Color: 18

Bin 327: 86 of cap free
Amount of items: 2
Items: 
Size: 775323 Color: 11
Size: 224592 Color: 10

Bin 328: 86 of cap free
Amount of items: 2
Items: 
Size: 750267 Color: 14
Size: 249648 Color: 0

Bin 329: 87 of cap free
Amount of items: 2
Items: 
Size: 546468 Color: 1
Size: 453446 Color: 11

Bin 330: 87 of cap free
Amount of items: 2
Items: 
Size: 516613 Color: 9
Size: 483301 Color: 4

Bin 331: 88 of cap free
Amount of items: 3
Items: 
Size: 356268 Color: 3
Size: 325700 Color: 3
Size: 317945 Color: 7

Bin 332: 89 of cap free
Amount of items: 2
Items: 
Size: 542379 Color: 14
Size: 457533 Color: 5

Bin 333: 89 of cap free
Amount of items: 2
Items: 
Size: 590032 Color: 18
Size: 409880 Color: 2

Bin 334: 90 of cap free
Amount of items: 2
Items: 
Size: 576364 Color: 14
Size: 423547 Color: 13

Bin 335: 92 of cap free
Amount of items: 2
Items: 
Size: 557973 Color: 18
Size: 441936 Color: 15

Bin 336: 92 of cap free
Amount of items: 2
Items: 
Size: 722205 Color: 14
Size: 277704 Color: 5

Bin 337: 92 of cap free
Amount of items: 2
Items: 
Size: 557587 Color: 14
Size: 442322 Color: 13

Bin 338: 93 of cap free
Amount of items: 2
Items: 
Size: 763678 Color: 3
Size: 236230 Color: 12

Bin 339: 95 of cap free
Amount of items: 3
Items: 
Size: 345439 Color: 2
Size: 328285 Color: 13
Size: 326182 Color: 12

Bin 340: 95 of cap free
Amount of items: 2
Items: 
Size: 622365 Color: 17
Size: 377541 Color: 9

Bin 341: 96 of cap free
Amount of items: 2
Items: 
Size: 733481 Color: 2
Size: 266424 Color: 12

Bin 342: 96 of cap free
Amount of items: 2
Items: 
Size: 775476 Color: 16
Size: 224429 Color: 4

Bin 343: 97 of cap free
Amount of items: 2
Items: 
Size: 698220 Color: 10
Size: 301684 Color: 13

Bin 344: 98 of cap free
Amount of items: 2
Items: 
Size: 535519 Color: 14
Size: 464384 Color: 0

Bin 345: 100 of cap free
Amount of items: 2
Items: 
Size: 708437 Color: 9
Size: 291464 Color: 13

Bin 346: 100 of cap free
Amount of items: 2
Items: 
Size: 539215 Color: 4
Size: 460686 Color: 17

Bin 347: 101 of cap free
Amount of items: 2
Items: 
Size: 545804 Color: 10
Size: 454096 Color: 11

Bin 348: 103 of cap free
Amount of items: 2
Items: 
Size: 714811 Color: 11
Size: 285087 Color: 13

Bin 349: 104 of cap free
Amount of items: 2
Items: 
Size: 722992 Color: 13
Size: 276905 Color: 18

Bin 350: 104 of cap free
Amount of items: 2
Items: 
Size: 623003 Color: 19
Size: 376894 Color: 14

Bin 351: 105 of cap free
Amount of items: 2
Items: 
Size: 611569 Color: 0
Size: 388327 Color: 1

Bin 352: 105 of cap free
Amount of items: 2
Items: 
Size: 624219 Color: 11
Size: 375677 Color: 13

Bin 353: 105 of cap free
Amount of items: 2
Items: 
Size: 596918 Color: 2
Size: 402978 Color: 3

Bin 354: 106 of cap free
Amount of items: 2
Items: 
Size: 642360 Color: 0
Size: 357535 Color: 10

Bin 355: 108 of cap free
Amount of items: 2
Items: 
Size: 539364 Color: 9
Size: 460529 Color: 11

Bin 356: 109 of cap free
Amount of items: 3
Items: 
Size: 656342 Color: 9
Size: 187453 Color: 18
Size: 156097 Color: 1

Bin 357: 109 of cap free
Amount of items: 2
Items: 
Size: 611987 Color: 9
Size: 387905 Color: 17

Bin 358: 109 of cap free
Amount of items: 3
Items: 
Size: 642492 Color: 8
Size: 194867 Color: 17
Size: 162533 Color: 6

Bin 359: 109 of cap free
Amount of items: 2
Items: 
Size: 508459 Color: 13
Size: 491433 Color: 16

Bin 360: 110 of cap free
Amount of items: 2
Items: 
Size: 699543 Color: 14
Size: 300348 Color: 19

Bin 361: 110 of cap free
Amount of items: 2
Items: 
Size: 709581 Color: 5
Size: 290310 Color: 13

Bin 362: 112 of cap free
Amount of items: 2
Items: 
Size: 783405 Color: 13
Size: 216484 Color: 1

Bin 363: 114 of cap free
Amount of items: 2
Items: 
Size: 616829 Color: 8
Size: 383058 Color: 7

Bin 364: 115 of cap free
Amount of items: 3
Items: 
Size: 767635 Color: 17
Size: 124506 Color: 14
Size: 107745 Color: 14

Bin 365: 115 of cap free
Amount of items: 2
Items: 
Size: 523239 Color: 14
Size: 476647 Color: 9

Bin 366: 116 of cap free
Amount of items: 2
Items: 
Size: 784361 Color: 14
Size: 215524 Color: 7

Bin 367: 117 of cap free
Amount of items: 3
Items: 
Size: 665631 Color: 11
Size: 192171 Color: 9
Size: 142082 Color: 19

Bin 368: 117 of cap free
Amount of items: 2
Items: 
Size: 582522 Color: 15
Size: 417362 Color: 13

Bin 369: 117 of cap free
Amount of items: 2
Items: 
Size: 585902 Color: 18
Size: 413982 Color: 3

Bin 370: 117 of cap free
Amount of items: 2
Items: 
Size: 677608 Color: 0
Size: 322276 Color: 12

Bin 371: 118 of cap free
Amount of items: 2
Items: 
Size: 529854 Color: 5
Size: 470029 Color: 3

Bin 372: 118 of cap free
Amount of items: 2
Items: 
Size: 740025 Color: 19
Size: 259858 Color: 13

Bin 373: 119 of cap free
Amount of items: 3
Items: 
Size: 520702 Color: 3
Size: 246313 Color: 11
Size: 232867 Color: 13

Bin 374: 119 of cap free
Amount of items: 2
Items: 
Size: 640987 Color: 17
Size: 358895 Color: 14

Bin 375: 119 of cap free
Amount of items: 2
Items: 
Size: 793968 Color: 19
Size: 205914 Color: 1

Bin 376: 120 of cap free
Amount of items: 2
Items: 
Size: 590169 Color: 5
Size: 409712 Color: 16

Bin 377: 122 of cap free
Amount of items: 3
Items: 
Size: 637696 Color: 2
Size: 233800 Color: 10
Size: 128383 Color: 0

Bin 378: 124 of cap free
Amount of items: 2
Items: 
Size: 702096 Color: 10
Size: 297781 Color: 4

Bin 379: 124 of cap free
Amount of items: 2
Items: 
Size: 637207 Color: 12
Size: 362670 Color: 1

Bin 380: 125 of cap free
Amount of items: 2
Items: 
Size: 761537 Color: 0
Size: 238339 Color: 3

Bin 381: 126 of cap free
Amount of items: 3
Items: 
Size: 586919 Color: 4
Size: 262129 Color: 12
Size: 150827 Color: 7

Bin 382: 127 of cap free
Amount of items: 2
Items: 
Size: 663906 Color: 16
Size: 335968 Color: 19

Bin 383: 128 of cap free
Amount of items: 2
Items: 
Size: 781470 Color: 2
Size: 218403 Color: 1

Bin 384: 129 of cap free
Amount of items: 2
Items: 
Size: 508105 Color: 9
Size: 491767 Color: 0

Bin 385: 130 of cap free
Amount of items: 2
Items: 
Size: 737918 Color: 10
Size: 261953 Color: 12

Bin 386: 130 of cap free
Amount of items: 2
Items: 
Size: 781881 Color: 4
Size: 217990 Color: 18

Bin 387: 130 of cap free
Amount of items: 2
Items: 
Size: 799355 Color: 14
Size: 200516 Color: 13

Bin 388: 131 of cap free
Amount of items: 2
Items: 
Size: 706783 Color: 12
Size: 293087 Color: 7

Bin 389: 131 of cap free
Amount of items: 3
Items: 
Size: 533735 Color: 13
Size: 240597 Color: 5
Size: 225538 Color: 5

Bin 390: 132 of cap free
Amount of items: 2
Items: 
Size: 556401 Color: 19
Size: 443468 Color: 8

Bin 391: 133 of cap free
Amount of items: 2
Items: 
Size: 609486 Color: 6
Size: 390382 Color: 19

Bin 392: 133 of cap free
Amount of items: 2
Items: 
Size: 762170 Color: 9
Size: 237698 Color: 3

Bin 393: 134 of cap free
Amount of items: 2
Items: 
Size: 698378 Color: 8
Size: 301489 Color: 2

Bin 394: 138 of cap free
Amount of items: 2
Items: 
Size: 721571 Color: 3
Size: 278292 Color: 6

Bin 395: 140 of cap free
Amount of items: 2
Items: 
Size: 555611 Color: 13
Size: 444250 Color: 6

Bin 396: 140 of cap free
Amount of items: 2
Items: 
Size: 581746 Color: 4
Size: 418115 Color: 10

Bin 397: 141 of cap free
Amount of items: 2
Items: 
Size: 651349 Color: 3
Size: 348511 Color: 14

Bin 398: 142 of cap free
Amount of items: 2
Items: 
Size: 576969 Color: 18
Size: 422890 Color: 14

Bin 399: 144 of cap free
Amount of items: 2
Items: 
Size: 574918 Color: 19
Size: 424939 Color: 3

Bin 400: 144 of cap free
Amount of items: 2
Items: 
Size: 780927 Color: 3
Size: 218930 Color: 9

Bin 401: 145 of cap free
Amount of items: 2
Items: 
Size: 545331 Color: 9
Size: 454525 Color: 13

Bin 402: 146 of cap free
Amount of items: 2
Items: 
Size: 735198 Color: 2
Size: 264657 Color: 8

Bin 403: 148 of cap free
Amount of items: 2
Items: 
Size: 577156 Color: 7
Size: 422697 Color: 17

Bin 404: 148 of cap free
Amount of items: 2
Items: 
Size: 649083 Color: 18
Size: 350770 Color: 16

Bin 405: 151 of cap free
Amount of items: 2
Items: 
Size: 511588 Color: 2
Size: 488262 Color: 16

Bin 406: 151 of cap free
Amount of items: 2
Items: 
Size: 659851 Color: 17
Size: 339999 Color: 14

Bin 407: 154 of cap free
Amount of items: 2
Items: 
Size: 587680 Color: 7
Size: 412167 Color: 10

Bin 408: 156 of cap free
Amount of items: 2
Items: 
Size: 699531 Color: 1
Size: 300314 Color: 16

Bin 409: 156 of cap free
Amount of items: 3
Items: 
Size: 548845 Color: 10
Size: 254513 Color: 17
Size: 196487 Color: 12

Bin 410: 157 of cap free
Amount of items: 2
Items: 
Size: 672894 Color: 1
Size: 326950 Color: 13

Bin 411: 157 of cap free
Amount of items: 2
Items: 
Size: 540686 Color: 10
Size: 459158 Color: 2

Bin 412: 158 of cap free
Amount of items: 3
Items: 
Size: 344152 Color: 19
Size: 329316 Color: 5
Size: 326375 Color: 17

Bin 413: 159 of cap free
Amount of items: 2
Items: 
Size: 784723 Color: 5
Size: 215119 Color: 8

Bin 414: 160 of cap free
Amount of items: 2
Items: 
Size: 610453 Color: 7
Size: 389388 Color: 17

Bin 415: 164 of cap free
Amount of items: 2
Items: 
Size: 520996 Color: 1
Size: 478841 Color: 17

Bin 416: 166 of cap free
Amount of items: 2
Items: 
Size: 583810 Color: 1
Size: 416025 Color: 5

Bin 417: 166 of cap free
Amount of items: 2
Items: 
Size: 786218 Color: 2
Size: 213617 Color: 17

Bin 418: 168 of cap free
Amount of items: 3
Items: 
Size: 401420 Color: 6
Size: 309226 Color: 11
Size: 289187 Color: 15

Bin 419: 169 of cap free
Amount of items: 2
Items: 
Size: 573609 Color: 2
Size: 426223 Color: 15

Bin 420: 170 of cap free
Amount of items: 2
Items: 
Size: 768698 Color: 12
Size: 231133 Color: 15

Bin 421: 170 of cap free
Amount of items: 2
Items: 
Size: 652676 Color: 15
Size: 347155 Color: 9

Bin 422: 171 of cap free
Amount of items: 3
Items: 
Size: 351976 Color: 11
Size: 331265 Color: 15
Size: 316589 Color: 17

Bin 423: 171 of cap free
Amount of items: 2
Items: 
Size: 681011 Color: 19
Size: 318819 Color: 11

Bin 424: 171 of cap free
Amount of items: 2
Items: 
Size: 774137 Color: 0
Size: 225693 Color: 4

Bin 425: 171 of cap free
Amount of items: 2
Items: 
Size: 553775 Color: 12
Size: 446055 Color: 4

Bin 426: 171 of cap free
Amount of items: 3
Items: 
Size: 764919 Color: 16
Size: 122488 Color: 17
Size: 112423 Color: 9

Bin 427: 172 of cap free
Amount of items: 2
Items: 
Size: 509763 Color: 0
Size: 490066 Color: 14

Bin 428: 173 of cap free
Amount of items: 2
Items: 
Size: 709866 Color: 0
Size: 289962 Color: 8

Bin 429: 173 of cap free
Amount of items: 2
Items: 
Size: 592532 Color: 12
Size: 407296 Color: 1

Bin 430: 173 of cap free
Amount of items: 3
Items: 
Size: 446605 Color: 12
Size: 281010 Color: 13
Size: 272213 Color: 9

Bin 431: 177 of cap free
Amount of items: 3
Items: 
Size: 568494 Color: 0
Size: 235246 Color: 6
Size: 196084 Color: 15

Bin 432: 177 of cap free
Amount of items: 2
Items: 
Size: 619161 Color: 5
Size: 380663 Color: 19

Bin 433: 178 of cap free
Amount of items: 2
Items: 
Size: 763987 Color: 13
Size: 235836 Color: 6

Bin 434: 179 of cap free
Amount of items: 2
Items: 
Size: 521774 Color: 6
Size: 478048 Color: 14

Bin 435: 180 of cap free
Amount of items: 2
Items: 
Size: 742277 Color: 10
Size: 257544 Color: 18

Bin 436: 183 of cap free
Amount of items: 2
Items: 
Size: 501524 Color: 14
Size: 498294 Color: 9

Bin 437: 184 of cap free
Amount of items: 2
Items: 
Size: 720361 Color: 10
Size: 279456 Color: 3

Bin 438: 185 of cap free
Amount of items: 2
Items: 
Size: 648174 Color: 10
Size: 351642 Color: 13

Bin 439: 187 of cap free
Amount of items: 2
Items: 
Size: 739586 Color: 16
Size: 260228 Color: 2

Bin 440: 187 of cap free
Amount of items: 2
Items: 
Size: 792296 Color: 2
Size: 207518 Color: 19

Bin 441: 188 of cap free
Amount of items: 2
Items: 
Size: 559707 Color: 8
Size: 440106 Color: 10

Bin 442: 191 of cap free
Amount of items: 2
Items: 
Size: 549939 Color: 11
Size: 449871 Color: 9

Bin 443: 193 of cap free
Amount of items: 2
Items: 
Size: 744610 Color: 14
Size: 255198 Color: 10

Bin 444: 193 of cap free
Amount of items: 2
Items: 
Size: 706169 Color: 5
Size: 293639 Color: 8

Bin 445: 194 of cap free
Amount of items: 2
Items: 
Size: 757561 Color: 9
Size: 242246 Color: 4

Bin 446: 195 of cap free
Amount of items: 2
Items: 
Size: 531609 Color: 10
Size: 468197 Color: 4

Bin 447: 196 of cap free
Amount of items: 2
Items: 
Size: 566338 Color: 0
Size: 433467 Color: 2

Bin 448: 196 of cap free
Amount of items: 2
Items: 
Size: 738774 Color: 3
Size: 261031 Color: 7

Bin 449: 205 of cap free
Amount of items: 2
Items: 
Size: 651790 Color: 5
Size: 348006 Color: 19

Bin 450: 206 of cap free
Amount of items: 2
Items: 
Size: 525956 Color: 12
Size: 473839 Color: 16

Bin 451: 207 of cap free
Amount of items: 2
Items: 
Size: 799557 Color: 9
Size: 200237 Color: 14

Bin 452: 208 of cap free
Amount of items: 2
Items: 
Size: 716378 Color: 4
Size: 283415 Color: 2

Bin 453: 210 of cap free
Amount of items: 2
Items: 
Size: 668278 Color: 8
Size: 331513 Color: 3

Bin 454: 210 of cap free
Amount of items: 2
Items: 
Size: 663595 Color: 12
Size: 336196 Color: 19

Bin 455: 212 of cap free
Amount of items: 2
Items: 
Size: 579651 Color: 11
Size: 420138 Color: 9

Bin 456: 213 of cap free
Amount of items: 2
Items: 
Size: 523206 Color: 19
Size: 476582 Color: 13

Bin 457: 214 of cap free
Amount of items: 3
Items: 
Size: 713935 Color: 13
Size: 159422 Color: 5
Size: 126430 Color: 3

Bin 458: 214 of cap free
Amount of items: 2
Items: 
Size: 550634 Color: 3
Size: 449153 Color: 8

Bin 459: 214 of cap free
Amount of items: 2
Items: 
Size: 651292 Color: 11
Size: 348495 Color: 5

Bin 460: 215 of cap free
Amount of items: 2
Items: 
Size: 656247 Color: 11
Size: 343539 Color: 6

Bin 461: 217 of cap free
Amount of items: 2
Items: 
Size: 645565 Color: 15
Size: 354219 Color: 8

Bin 462: 218 of cap free
Amount of items: 2
Items: 
Size: 571659 Color: 13
Size: 428124 Color: 19

Bin 463: 219 of cap free
Amount of items: 2
Items: 
Size: 647025 Color: 3
Size: 352757 Color: 7

Bin 464: 221 of cap free
Amount of items: 2
Items: 
Size: 522849 Color: 2
Size: 476931 Color: 3

Bin 465: 221 of cap free
Amount of items: 2
Items: 
Size: 534017 Color: 10
Size: 465763 Color: 17

Bin 466: 221 of cap free
Amount of items: 2
Items: 
Size: 741658 Color: 14
Size: 258122 Color: 19

Bin 467: 222 of cap free
Amount of items: 2
Items: 
Size: 669868 Color: 17
Size: 329911 Color: 8

Bin 468: 222 of cap free
Amount of items: 2
Items: 
Size: 718209 Color: 7
Size: 281570 Color: 12

Bin 469: 226 of cap free
Amount of items: 2
Items: 
Size: 518279 Color: 17
Size: 481496 Color: 6

Bin 470: 227 of cap free
Amount of items: 2
Items: 
Size: 582198 Color: 3
Size: 417576 Color: 7

Bin 471: 228 of cap free
Amount of items: 2
Items: 
Size: 615385 Color: 2
Size: 384388 Color: 17

Bin 472: 232 of cap free
Amount of items: 2
Items: 
Size: 515699 Color: 17
Size: 484070 Color: 1

Bin 473: 232 of cap free
Amount of items: 2
Items: 
Size: 524400 Color: 4
Size: 475369 Color: 17

Bin 474: 233 of cap free
Amount of items: 2
Items: 
Size: 749926 Color: 0
Size: 249842 Color: 17

Bin 475: 234 of cap free
Amount of items: 2
Items: 
Size: 543096 Color: 1
Size: 456671 Color: 12

Bin 476: 235 of cap free
Amount of items: 3
Items: 
Size: 696586 Color: 19
Size: 203071 Color: 0
Size: 100109 Color: 8

Bin 477: 235 of cap free
Amount of items: 3
Items: 
Size: 385068 Color: 0
Size: 310232 Color: 13
Size: 304466 Color: 19

Bin 478: 236 of cap free
Amount of items: 2
Items: 
Size: 638372 Color: 12
Size: 361393 Color: 11

Bin 479: 236 of cap free
Amount of items: 2
Items: 
Size: 708165 Color: 2
Size: 291600 Color: 15

Bin 480: 238 of cap free
Amount of items: 2
Items: 
Size: 555184 Color: 16
Size: 444579 Color: 5

Bin 481: 238 of cap free
Amount of items: 2
Items: 
Size: 660742 Color: 15
Size: 339021 Color: 17

Bin 482: 240 of cap free
Amount of items: 2
Items: 
Size: 558697 Color: 11
Size: 441064 Color: 19

Bin 483: 242 of cap free
Amount of items: 2
Items: 
Size: 747223 Color: 11
Size: 252536 Color: 12

Bin 484: 244 of cap free
Amount of items: 2
Items: 
Size: 758517 Color: 9
Size: 241240 Color: 10

Bin 485: 250 of cap free
Amount of items: 2
Items: 
Size: 536853 Color: 0
Size: 462898 Color: 16

Bin 486: 251 of cap free
Amount of items: 2
Items: 
Size: 687997 Color: 17
Size: 311753 Color: 6

Bin 487: 251 of cap free
Amount of items: 2
Items: 
Size: 732701 Color: 15
Size: 267049 Color: 1

Bin 488: 252 of cap free
Amount of items: 2
Items: 
Size: 683960 Color: 2
Size: 315789 Color: 1

Bin 489: 254 of cap free
Amount of items: 2
Items: 
Size: 512226 Color: 7
Size: 487521 Color: 14

Bin 490: 256 of cap free
Amount of items: 2
Items: 
Size: 768669 Color: 19
Size: 231076 Color: 4

Bin 491: 260 of cap free
Amount of items: 2
Items: 
Size: 528555 Color: 9
Size: 471186 Color: 3

Bin 492: 261 of cap free
Amount of items: 2
Items: 
Size: 586657 Color: 10
Size: 413083 Color: 2

Bin 493: 265 of cap free
Amount of items: 3
Items: 
Size: 647700 Color: 7
Size: 182979 Color: 11
Size: 169057 Color: 4

Bin 494: 266 of cap free
Amount of items: 2
Items: 
Size: 676942 Color: 4
Size: 322793 Color: 19

Bin 495: 266 of cap free
Amount of items: 3
Items: 
Size: 345671 Color: 19
Size: 330441 Color: 13
Size: 323623 Color: 17

Bin 496: 268 of cap free
Amount of items: 2
Items: 
Size: 724485 Color: 10
Size: 275248 Color: 13

Bin 497: 270 of cap free
Amount of items: 2
Items: 
Size: 506481 Color: 15
Size: 493250 Color: 19

Bin 498: 276 of cap free
Amount of items: 2
Items: 
Size: 547499 Color: 3
Size: 452226 Color: 13

Bin 499: 279 of cap free
Amount of items: 2
Items: 
Size: 622314 Color: 19
Size: 377408 Color: 13

Bin 500: 283 of cap free
Amount of items: 2
Items: 
Size: 583809 Color: 14
Size: 415909 Color: 2

Bin 501: 283 of cap free
Amount of items: 2
Items: 
Size: 772966 Color: 5
Size: 226752 Color: 2

Bin 502: 284 of cap free
Amount of items: 3
Items: 
Size: 520295 Color: 16
Size: 242858 Color: 15
Size: 236564 Color: 4

Bin 503: 284 of cap free
Amount of items: 2
Items: 
Size: 665824 Color: 10
Size: 333893 Color: 18

Bin 504: 287 of cap free
Amount of items: 2
Items: 
Size: 693921 Color: 4
Size: 305793 Color: 19

Bin 505: 292 of cap free
Amount of items: 2
Items: 
Size: 741221 Color: 4
Size: 258488 Color: 18

Bin 506: 293 of cap free
Amount of items: 2
Items: 
Size: 665618 Color: 14
Size: 334090 Color: 10

Bin 507: 294 of cap free
Amount of items: 3
Items: 
Size: 360250 Color: 10
Size: 331763 Color: 17
Size: 307694 Color: 2

Bin 508: 294 of cap free
Amount of items: 2
Items: 
Size: 748685 Color: 10
Size: 251022 Color: 12

Bin 509: 301 of cap free
Amount of items: 2
Items: 
Size: 525345 Color: 0
Size: 474355 Color: 1

Bin 510: 302 of cap free
Amount of items: 2
Items: 
Size: 549026 Color: 12
Size: 450673 Color: 4

Bin 511: 304 of cap free
Amount of items: 2
Items: 
Size: 551581 Color: 4
Size: 448116 Color: 19

Bin 512: 308 of cap free
Amount of items: 2
Items: 
Size: 617031 Color: 8
Size: 382662 Color: 18

Bin 513: 308 of cap free
Amount of items: 2
Items: 
Size: 694485 Color: 17
Size: 305208 Color: 0

Bin 514: 308 of cap free
Amount of items: 2
Items: 
Size: 717680 Color: 10
Size: 282013 Color: 13

Bin 515: 312 of cap free
Amount of items: 2
Items: 
Size: 541563 Color: 16
Size: 458126 Color: 0

Bin 516: 315 of cap free
Amount of items: 2
Items: 
Size: 562335 Color: 1
Size: 437351 Color: 10

Bin 517: 315 of cap free
Amount of items: 2
Items: 
Size: 572128 Color: 11
Size: 427558 Color: 2

Bin 518: 316 of cap free
Amount of items: 2
Items: 
Size: 698858 Color: 7
Size: 300827 Color: 12

Bin 519: 316 of cap free
Amount of items: 2
Items: 
Size: 767175 Color: 0
Size: 232510 Color: 9

Bin 520: 321 of cap free
Amount of items: 2
Items: 
Size: 631351 Color: 19
Size: 368329 Color: 6

Bin 521: 324 of cap free
Amount of items: 2
Items: 
Size: 583325 Color: 6
Size: 416352 Color: 3

Bin 522: 325 of cap free
Amount of items: 2
Items: 
Size: 688397 Color: 5
Size: 311279 Color: 19

Bin 523: 325 of cap free
Amount of items: 2
Items: 
Size: 729119 Color: 19
Size: 270557 Color: 0

Bin 524: 326 of cap free
Amount of items: 2
Items: 
Size: 722605 Color: 10
Size: 277070 Color: 4

Bin 525: 327 of cap free
Amount of items: 2
Items: 
Size: 643139 Color: 10
Size: 356535 Color: 0

Bin 526: 328 of cap free
Amount of items: 2
Items: 
Size: 564691 Color: 14
Size: 434982 Color: 0

Bin 527: 329 of cap free
Amount of items: 2
Items: 
Size: 640926 Color: 17
Size: 358746 Color: 13

Bin 528: 331 of cap free
Amount of items: 2
Items: 
Size: 544798 Color: 3
Size: 454872 Color: 5

Bin 529: 333 of cap free
Amount of items: 2
Items: 
Size: 533180 Color: 11
Size: 466488 Color: 15

Bin 530: 336 of cap free
Amount of items: 2
Items: 
Size: 606814 Color: 7
Size: 392851 Color: 18

Bin 531: 336 of cap free
Amount of items: 2
Items: 
Size: 736000 Color: 14
Size: 263665 Color: 6

Bin 532: 336 of cap free
Amount of items: 2
Items: 
Size: 742969 Color: 5
Size: 256696 Color: 10

Bin 533: 338 of cap free
Amount of items: 2
Items: 
Size: 594469 Color: 1
Size: 405194 Color: 18

Bin 534: 345 of cap free
Amount of items: 2
Items: 
Size: 653212 Color: 11
Size: 346444 Color: 19

Bin 535: 348 of cap free
Amount of items: 2
Items: 
Size: 729599 Color: 11
Size: 270054 Color: 1

Bin 536: 350 of cap free
Amount of items: 2
Items: 
Size: 676408 Color: 13
Size: 323243 Color: 19

Bin 537: 354 of cap free
Amount of items: 2
Items: 
Size: 672733 Color: 4
Size: 326914 Color: 7

Bin 538: 356 of cap free
Amount of items: 2
Items: 
Size: 561808 Color: 10
Size: 437837 Color: 0

Bin 539: 356 of cap free
Amount of items: 2
Items: 
Size: 758097 Color: 0
Size: 241548 Color: 8

Bin 540: 357 of cap free
Amount of items: 2
Items: 
Size: 553590 Color: 15
Size: 446054 Color: 9

Bin 541: 360 of cap free
Amount of items: 2
Items: 
Size: 624409 Color: 5
Size: 375232 Color: 15

Bin 542: 367 of cap free
Amount of items: 2
Items: 
Size: 732281 Color: 2
Size: 267353 Color: 15

Bin 543: 368 of cap free
Amount of items: 2
Items: 
Size: 790077 Color: 3
Size: 209556 Color: 16

Bin 544: 370 of cap free
Amount of items: 2
Items: 
Size: 784902 Color: 0
Size: 214729 Color: 14

Bin 545: 371 of cap free
Amount of items: 2
Items: 
Size: 793893 Color: 1
Size: 205737 Color: 3

Bin 546: 371 of cap free
Amount of items: 2
Items: 
Size: 501362 Color: 16
Size: 498268 Color: 19

Bin 547: 372 of cap free
Amount of items: 2
Items: 
Size: 749846 Color: 18
Size: 249783 Color: 13

Bin 548: 380 of cap free
Amount of items: 2
Items: 
Size: 739398 Color: 10
Size: 260223 Color: 11

Bin 549: 390 of cap free
Amount of items: 2
Items: 
Size: 794770 Color: 10
Size: 204841 Color: 18

Bin 550: 392 of cap free
Amount of items: 2
Items: 
Size: 629769 Color: 9
Size: 369840 Color: 5

Bin 551: 393 of cap free
Amount of items: 2
Items: 
Size: 779559 Color: 17
Size: 220049 Color: 11

Bin 552: 393 of cap free
Amount of items: 2
Items: 
Size: 510216 Color: 13
Size: 489392 Color: 16

Bin 553: 395 of cap free
Amount of items: 2
Items: 
Size: 500043 Color: 16
Size: 499563 Color: 9

Bin 554: 395 of cap free
Amount of items: 2
Items: 
Size: 691550 Color: 16
Size: 308056 Color: 12

Bin 555: 397 of cap free
Amount of items: 2
Items: 
Size: 657820 Color: 13
Size: 341784 Color: 0

Bin 556: 399 of cap free
Amount of items: 2
Items: 
Size: 565639 Color: 2
Size: 433963 Color: 18

Bin 557: 403 of cap free
Amount of items: 2
Items: 
Size: 744459 Color: 1
Size: 255139 Color: 8

Bin 558: 406 of cap free
Amount of items: 2
Items: 
Size: 574618 Color: 18
Size: 424977 Color: 19

Bin 559: 413 of cap free
Amount of items: 2
Items: 
Size: 665121 Color: 6
Size: 334467 Color: 18

Bin 560: 413 of cap free
Amount of items: 2
Items: 
Size: 591445 Color: 17
Size: 408143 Color: 2

Bin 561: 413 of cap free
Amount of items: 2
Items: 
Size: 747624 Color: 14
Size: 251964 Color: 15

Bin 562: 414 of cap free
Amount of items: 2
Items: 
Size: 512070 Color: 2
Size: 487517 Color: 7

Bin 563: 416 of cap free
Amount of items: 2
Items: 
Size: 629122 Color: 9
Size: 370463 Color: 1

Bin 564: 421 of cap free
Amount of items: 2
Items: 
Size: 786154 Color: 11
Size: 213426 Color: 1

Bin 565: 421 of cap free
Amount of items: 2
Items: 
Size: 669208 Color: 16
Size: 330372 Color: 14

Bin 566: 422 of cap free
Amount of items: 2
Items: 
Size: 755628 Color: 15
Size: 243951 Color: 6

Bin 567: 423 of cap free
Amount of items: 2
Items: 
Size: 524873 Color: 5
Size: 474705 Color: 1

Bin 568: 423 of cap free
Amount of items: 2
Items: 
Size: 683885 Color: 7
Size: 315693 Color: 15

Bin 569: 431 of cap free
Amount of items: 2
Items: 
Size: 775872 Color: 7
Size: 223698 Color: 17

Bin 570: 432 of cap free
Amount of items: 2
Items: 
Size: 685606 Color: 7
Size: 313963 Color: 12

Bin 571: 437 of cap free
Amount of items: 2
Items: 
Size: 771542 Color: 3
Size: 228022 Color: 9

Bin 572: 437 of cap free
Amount of items: 2
Items: 
Size: 514049 Color: 9
Size: 485515 Color: 15

Bin 573: 446 of cap free
Amount of items: 2
Items: 
Size: 503416 Color: 9
Size: 496139 Color: 2

Bin 574: 448 of cap free
Amount of items: 2
Items: 
Size: 587210 Color: 0
Size: 412343 Color: 3

Bin 575: 449 of cap free
Amount of items: 2
Items: 
Size: 664385 Color: 12
Size: 335167 Color: 4

Bin 576: 451 of cap free
Amount of items: 2
Items: 
Size: 599182 Color: 12
Size: 400368 Color: 4

Bin 577: 453 of cap free
Amount of items: 2
Items: 
Size: 795847 Color: 13
Size: 203701 Color: 17

Bin 578: 457 of cap free
Amount of items: 2
Items: 
Size: 534905 Color: 10
Size: 464639 Color: 4

Bin 579: 458 of cap free
Amount of items: 2
Items: 
Size: 718143 Color: 0
Size: 281400 Color: 5

Bin 580: 468 of cap free
Amount of items: 2
Items: 
Size: 595655 Color: 14
Size: 403878 Color: 4

Bin 581: 474 of cap free
Amount of items: 2
Items: 
Size: 555113 Color: 15
Size: 444414 Color: 14

Bin 582: 475 of cap free
Amount of items: 2
Items: 
Size: 601651 Color: 4
Size: 397875 Color: 12

Bin 583: 476 of cap free
Amount of items: 2
Items: 
Size: 732631 Color: 15
Size: 266894 Color: 7

Bin 584: 483 of cap free
Amount of items: 2
Items: 
Size: 648052 Color: 10
Size: 351466 Color: 0

Bin 585: 493 of cap free
Amount of items: 2
Items: 
Size: 531014 Color: 14
Size: 468494 Color: 10

Bin 586: 497 of cap free
Amount of items: 2
Items: 
Size: 542171 Color: 1
Size: 457333 Color: 8

Bin 587: 497 of cap free
Amount of items: 2
Items: 
Size: 644019 Color: 14
Size: 355485 Color: 6

Bin 588: 500 of cap free
Amount of items: 2
Items: 
Size: 593323 Color: 6
Size: 406178 Color: 7

Bin 589: 501 of cap free
Amount of items: 2
Items: 
Size: 579629 Color: 17
Size: 419871 Color: 8

Bin 590: 508 of cap free
Amount of items: 2
Items: 
Size: 777871 Color: 15
Size: 221622 Color: 3

Bin 591: 510 of cap free
Amount of items: 2
Items: 
Size: 625490 Color: 0
Size: 374001 Color: 14

Bin 592: 513 of cap free
Amount of items: 2
Items: 
Size: 510165 Color: 9
Size: 489323 Color: 18

Bin 593: 513 of cap free
Amount of items: 2
Items: 
Size: 569520 Color: 1
Size: 429968 Color: 2

Bin 594: 516 of cap free
Amount of items: 2
Items: 
Size: 750829 Color: 3
Size: 248656 Color: 12

Bin 595: 518 of cap free
Amount of items: 2
Items: 
Size: 623673 Color: 5
Size: 375810 Color: 4

Bin 596: 518 of cap free
Amount of items: 2
Items: 
Size: 789505 Color: 7
Size: 209978 Color: 4

Bin 597: 519 of cap free
Amount of items: 2
Items: 
Size: 561735 Color: 9
Size: 437747 Color: 2

Bin 598: 522 of cap free
Amount of items: 2
Items: 
Size: 726038 Color: 1
Size: 273441 Color: 15

Bin 599: 524 of cap free
Amount of items: 2
Items: 
Size: 576947 Color: 9
Size: 422530 Color: 16

Bin 600: 525 of cap free
Amount of items: 2
Items: 
Size: 525817 Color: 2
Size: 473659 Color: 4

Bin 601: 529 of cap free
Amount of items: 2
Items: 
Size: 716364 Color: 9
Size: 283108 Color: 12

Bin 602: 538 of cap free
Amount of items: 2
Items: 
Size: 724376 Color: 4
Size: 275087 Color: 12

Bin 603: 539 of cap free
Amount of items: 2
Items: 
Size: 602553 Color: 17
Size: 396909 Color: 11

Bin 604: 539 of cap free
Amount of items: 2
Items: 
Size: 769049 Color: 7
Size: 230413 Color: 10

Bin 605: 559 of cap free
Amount of items: 2
Items: 
Size: 578155 Color: 17
Size: 421287 Color: 10

Bin 606: 566 of cap free
Amount of items: 2
Items: 
Size: 583093 Color: 12
Size: 416342 Color: 11

Bin 607: 569 of cap free
Amount of items: 2
Items: 
Size: 692978 Color: 4
Size: 306454 Color: 19

Bin 608: 576 of cap free
Amount of items: 2
Items: 
Size: 594238 Color: 18
Size: 405187 Color: 5

Bin 609: 576 of cap free
Amount of items: 2
Items: 
Size: 618129 Color: 16
Size: 381296 Color: 3

Bin 610: 577 of cap free
Amount of items: 2
Items: 
Size: 715184 Color: 8
Size: 284240 Color: 0

Bin 611: 579 of cap free
Amount of items: 2
Items: 
Size: 772053 Color: 6
Size: 227369 Color: 7

Bin 612: 589 of cap free
Amount of items: 2
Items: 
Size: 661066 Color: 1
Size: 338346 Color: 12

Bin 613: 596 of cap free
Amount of items: 2
Items: 
Size: 712373 Color: 8
Size: 287032 Color: 19

Bin 614: 598 of cap free
Amount of items: 2
Items: 
Size: 592724 Color: 2
Size: 406679 Color: 4

Bin 615: 602 of cap free
Amount of items: 2
Items: 
Size: 751844 Color: 17
Size: 247555 Color: 9

Bin 616: 603 of cap free
Amount of items: 2
Items: 
Size: 721718 Color: 1
Size: 277680 Color: 11

Bin 617: 605 of cap free
Amount of items: 2
Items: 
Size: 514747 Color: 12
Size: 484649 Color: 13

Bin 618: 606 of cap free
Amount of items: 2
Items: 
Size: 605337 Color: 6
Size: 394058 Color: 14

Bin 619: 613 of cap free
Amount of items: 2
Items: 
Size: 574800 Color: 19
Size: 424588 Color: 2

Bin 620: 613 of cap free
Amount of items: 2
Items: 
Size: 739862 Color: 16
Size: 259526 Color: 4

Bin 621: 613 of cap free
Amount of items: 2
Items: 
Size: 561688 Color: 16
Size: 437700 Color: 17

Bin 622: 615 of cap free
Amount of items: 2
Items: 
Size: 634764 Color: 3
Size: 364622 Color: 14

Bin 623: 624 of cap free
Amount of items: 2
Items: 
Size: 500184 Color: 9
Size: 499193 Color: 1

Bin 624: 632 of cap free
Amount of items: 2
Items: 
Size: 559584 Color: 9
Size: 439785 Color: 3

Bin 625: 639 of cap free
Amount of items: 2
Items: 
Size: 526536 Color: 10
Size: 472826 Color: 8

Bin 626: 639 of cap free
Amount of items: 2
Items: 
Size: 538295 Color: 13
Size: 461067 Color: 1

Bin 627: 640 of cap free
Amount of items: 2
Items: 
Size: 690465 Color: 15
Size: 308896 Color: 11

Bin 628: 640 of cap free
Amount of items: 2
Items: 
Size: 555588 Color: 5
Size: 443773 Color: 4

Bin 629: 647 of cap free
Amount of items: 2
Items: 
Size: 657796 Color: 5
Size: 341558 Color: 2

Bin 630: 647 of cap free
Amount of items: 2
Items: 
Size: 681505 Color: 7
Size: 317849 Color: 3

Bin 631: 651 of cap free
Amount of items: 2
Items: 
Size: 782184 Color: 0
Size: 217166 Color: 14

Bin 632: 655 of cap free
Amount of items: 2
Items: 
Size: 571912 Color: 11
Size: 427434 Color: 6

Bin 633: 663 of cap free
Amount of items: 2
Items: 
Size: 528539 Color: 1
Size: 470799 Color: 5

Bin 634: 663 of cap free
Amount of items: 2
Items: 
Size: 776702 Color: 13
Size: 222636 Color: 19

Bin 635: 665 of cap free
Amount of items: 2
Items: 
Size: 570473 Color: 14
Size: 428863 Color: 0

Bin 636: 666 of cap free
Amount of items: 2
Items: 
Size: 628107 Color: 12
Size: 371228 Color: 6

Bin 637: 669 of cap free
Amount of items: 2
Items: 
Size: 728183 Color: 12
Size: 271149 Color: 9

Bin 638: 674 of cap free
Amount of items: 2
Items: 
Size: 720882 Color: 9
Size: 278445 Color: 19

Bin 639: 679 of cap free
Amount of items: 2
Items: 
Size: 580671 Color: 16
Size: 418651 Color: 11

Bin 640: 685 of cap free
Amount of items: 2
Items: 
Size: 507771 Color: 1
Size: 491545 Color: 14

Bin 641: 688 of cap free
Amount of items: 2
Items: 
Size: 669790 Color: 19
Size: 329523 Color: 12

Bin 642: 689 of cap free
Amount of items: 2
Items: 
Size: 532004 Color: 17
Size: 467308 Color: 3

Bin 643: 694 of cap free
Amount of items: 2
Items: 
Size: 761680 Color: 2
Size: 237627 Color: 18

Bin 644: 696 of cap free
Amount of items: 2
Items: 
Size: 511897 Color: 3
Size: 487408 Color: 0

Bin 645: 697 of cap free
Amount of items: 2
Items: 
Size: 636823 Color: 4
Size: 362481 Color: 6

Bin 646: 710 of cap free
Amount of items: 2
Items: 
Size: 640762 Color: 10
Size: 358529 Color: 17

Bin 647: 712 of cap free
Amount of items: 2
Items: 
Size: 752689 Color: 11
Size: 246600 Color: 16

Bin 648: 713 of cap free
Amount of items: 2
Items: 
Size: 760422 Color: 11
Size: 238866 Color: 14

Bin 649: 715 of cap free
Amount of items: 2
Items: 
Size: 595525 Color: 16
Size: 403761 Color: 7

Bin 650: 726 of cap free
Amount of items: 2
Items: 
Size: 618952 Color: 19
Size: 380323 Color: 6

Bin 651: 738 of cap free
Amount of items: 2
Items: 
Size: 631007 Color: 2
Size: 368256 Color: 7

Bin 652: 763 of cap free
Amount of items: 2
Items: 
Size: 652207 Color: 17
Size: 347031 Color: 9

Bin 653: 775 of cap free
Amount of items: 2
Items: 
Size: 742582 Color: 16
Size: 256644 Color: 0

Bin 654: 776 of cap free
Amount of items: 2
Items: 
Size: 610959 Color: 8
Size: 388266 Color: 17

Bin 655: 779 of cap free
Amount of items: 2
Items: 
Size: 757985 Color: 0
Size: 241237 Color: 18

Bin 656: 779 of cap free
Amount of items: 2
Items: 
Size: 517480 Color: 14
Size: 481742 Color: 12

Bin 657: 781 of cap free
Amount of items: 2
Items: 
Size: 501104 Color: 0
Size: 498116 Color: 10

Bin 658: 783 of cap free
Amount of items: 2
Items: 
Size: 707818 Color: 14
Size: 291400 Color: 11

Bin 659: 793 of cap free
Amount of items: 2
Items: 
Size: 741142 Color: 14
Size: 258066 Color: 0

Bin 660: 800 of cap free
Amount of items: 2
Items: 
Size: 690340 Color: 0
Size: 308861 Color: 1

Bin 661: 811 of cap free
Amount of items: 2
Items: 
Size: 755246 Color: 16
Size: 243944 Color: 2

Bin 662: 815 of cap free
Amount of items: 2
Items: 
Size: 795610 Color: 11
Size: 203576 Color: 12

Bin 663: 818 of cap free
Amount of items: 2
Items: 
Size: 517445 Color: 19
Size: 481738 Color: 11

Bin 664: 818 of cap free
Amount of items: 2
Items: 
Size: 526462 Color: 4
Size: 472721 Color: 15

Bin 665: 828 of cap free
Amount of items: 2
Items: 
Size: 528516 Color: 13
Size: 470657 Color: 16

Bin 666: 832 of cap free
Amount of items: 2
Items: 
Size: 651174 Color: 6
Size: 347995 Color: 12

Bin 667: 833 of cap free
Amount of items: 2
Items: 
Size: 550256 Color: 0
Size: 448912 Color: 6

Bin 668: 837 of cap free
Amount of items: 2
Items: 
Size: 792334 Color: 19
Size: 206830 Color: 15

Bin 669: 866 of cap free
Amount of items: 2
Items: 
Size: 638950 Color: 11
Size: 360185 Color: 17

Bin 670: 876 of cap free
Amount of items: 2
Items: 
Size: 535667 Color: 15
Size: 463458 Color: 3

Bin 671: 878 of cap free
Amount of items: 2
Items: 
Size: 620311 Color: 15
Size: 378812 Color: 9

Bin 672: 881 of cap free
Amount of items: 2
Items: 
Size: 744330 Color: 12
Size: 254790 Color: 1

Bin 673: 893 of cap free
Amount of items: 2
Items: 
Size: 540059 Color: 0
Size: 459049 Color: 9

Bin 674: 893 of cap free
Amount of items: 2
Items: 
Size: 514737 Color: 9
Size: 484371 Color: 0

Bin 675: 897 of cap free
Amount of items: 2
Items: 
Size: 506396 Color: 3
Size: 492708 Color: 18

Bin 676: 900 of cap free
Amount of items: 2
Items: 
Size: 501035 Color: 13
Size: 498066 Color: 15

Bin 677: 901 of cap free
Amount of items: 2
Items: 
Size: 553071 Color: 9
Size: 446029 Color: 19

Bin 678: 911 of cap free
Amount of items: 2
Items: 
Size: 745233 Color: 1
Size: 253857 Color: 4

Bin 679: 922 of cap free
Amount of items: 2
Items: 
Size: 749755 Color: 9
Size: 249324 Color: 11

Bin 680: 927 of cap free
Amount of items: 2
Items: 
Size: 763899 Color: 2
Size: 235175 Color: 6

Bin 681: 928 of cap free
Amount of items: 2
Items: 
Size: 602499 Color: 6
Size: 396574 Color: 15

Bin 682: 929 of cap free
Amount of items: 2
Items: 
Size: 777809 Color: 0
Size: 221263 Color: 17

Bin 683: 930 of cap free
Amount of items: 2
Items: 
Size: 744171 Color: 14
Size: 254900 Color: 12

Bin 684: 945 of cap free
Amount of items: 2
Items: 
Size: 645276 Color: 16
Size: 353780 Color: 8

Bin 685: 945 of cap free
Amount of items: 2
Items: 
Size: 795580 Color: 4
Size: 203476 Color: 15

Bin 686: 952 of cap free
Amount of items: 2
Items: 
Size: 687860 Color: 19
Size: 311189 Color: 7

Bin 687: 959 of cap free
Amount of items: 2
Items: 
Size: 576772 Color: 12
Size: 422270 Color: 13

Bin 688: 962 of cap free
Amount of items: 2
Items: 
Size: 634419 Color: 16
Size: 364620 Color: 13

Bin 689: 968 of cap free
Amount of items: 2
Items: 
Size: 673120 Color: 16
Size: 325913 Color: 13

Bin 690: 991 of cap free
Amount of items: 2
Items: 
Size: 568107 Color: 13
Size: 430903 Color: 19

Bin 691: 995 of cap free
Amount of items: 2
Items: 
Size: 702638 Color: 15
Size: 296368 Color: 17

Bin 692: 998 of cap free
Amount of items: 2
Items: 
Size: 592144 Color: 11
Size: 406859 Color: 2

Bin 693: 1005 of cap free
Amount of items: 2
Items: 
Size: 651105 Color: 15
Size: 347891 Color: 19

Bin 694: 1022 of cap free
Amount of items: 3
Items: 
Size: 657645 Color: 12
Size: 186425 Color: 2
Size: 154909 Color: 11

Bin 695: 1028 of cap free
Amount of items: 2
Items: 
Size: 625074 Color: 0
Size: 373899 Color: 11

Bin 696: 1055 of cap free
Amount of items: 3
Items: 
Size: 711237 Color: 14
Size: 165793 Color: 15
Size: 121916 Color: 7

Bin 697: 1058 of cap free
Amount of items: 2
Items: 
Size: 630653 Color: 6
Size: 368290 Color: 2

Bin 698: 1066 of cap free
Amount of items: 2
Items: 
Size: 768620 Color: 11
Size: 230315 Color: 16

Bin 699: 1077 of cap free
Amount of items: 2
Items: 
Size: 530749 Color: 12
Size: 468175 Color: 1

Bin 700: 1092 of cap free
Amount of items: 2
Items: 
Size: 640554 Color: 11
Size: 358355 Color: 6

Bin 701: 1097 of cap free
Amount of items: 2
Items: 
Size: 520937 Color: 6
Size: 477967 Color: 7

Bin 702: 1130 of cap free
Amount of items: 2
Items: 
Size: 599048 Color: 14
Size: 399823 Color: 10

Bin 703: 1139 of cap free
Amount of items: 2
Items: 
Size: 596906 Color: 4
Size: 401956 Color: 10

Bin 704: 1146 of cap free
Amount of items: 2
Items: 
Size: 521281 Color: 7
Size: 477574 Color: 1

Bin 705: 1152 of cap free
Amount of items: 2
Items: 
Size: 636499 Color: 10
Size: 362350 Color: 3

Bin 706: 1171 of cap free
Amount of items: 2
Items: 
Size: 557415 Color: 9
Size: 441415 Color: 11

Bin 707: 1183 of cap free
Amount of items: 2
Items: 
Size: 781729 Color: 3
Size: 217089 Color: 4

Bin 708: 1183 of cap free
Amount of items: 2
Items: 
Size: 785457 Color: 6
Size: 213361 Color: 16

Bin 709: 1205 of cap free
Amount of items: 2
Items: 
Size: 602195 Color: 19
Size: 396601 Color: 6

Bin 710: 1212 of cap free
Amount of items: 2
Items: 
Size: 630600 Color: 16
Size: 368189 Color: 0

Bin 711: 1216 of cap free
Amount of items: 2
Items: 
Size: 612343 Color: 2
Size: 386442 Color: 18

Bin 712: 1236 of cap free
Amount of items: 2
Items: 
Size: 779181 Color: 8
Size: 219584 Color: 3

Bin 713: 1245 of cap free
Amount of items: 2
Items: 
Size: 634279 Color: 2
Size: 364477 Color: 6

Bin 714: 1251 of cap free
Amount of items: 2
Items: 
Size: 739226 Color: 0
Size: 259524 Color: 13

Bin 715: 1260 of cap free
Amount of items: 2
Items: 
Size: 740612 Color: 3
Size: 258129 Color: 14

Bin 716: 1264 of cap free
Amount of items: 2
Items: 
Size: 698218 Color: 8
Size: 300519 Color: 14

Bin 717: 1269 of cap free
Amount of items: 2
Items: 
Size: 574180 Color: 1
Size: 424552 Color: 11

Bin 718: 1290 of cap free
Amount of items: 2
Items: 
Size: 598944 Color: 15
Size: 399767 Color: 6

Bin 719: 1300 of cap free
Amount of items: 2
Items: 
Size: 540021 Color: 13
Size: 458680 Color: 10

Bin 720: 1311 of cap free
Amount of items: 2
Items: 
Size: 544724 Color: 1
Size: 453966 Color: 4

Bin 721: 1332 of cap free
Amount of items: 2
Items: 
Size: 777765 Color: 4
Size: 220904 Color: 14

Bin 722: 1391 of cap free
Amount of items: 2
Items: 
Size: 538430 Color: 1
Size: 460180 Color: 19

Bin 723: 1408 of cap free
Amount of items: 2
Items: 
Size: 616206 Color: 19
Size: 382387 Color: 13

Bin 724: 1430 of cap free
Amount of items: 2
Items: 
Size: 623475 Color: 17
Size: 375096 Color: 15

Bin 725: 1461 of cap free
Amount of items: 2
Items: 
Size: 506130 Color: 11
Size: 492410 Color: 3

Bin 726: 1472 of cap free
Amount of items: 2
Items: 
Size: 679010 Color: 12
Size: 319519 Color: 7

Bin 727: 1478 of cap free
Amount of items: 2
Items: 
Size: 564638 Color: 1
Size: 433885 Color: 17

Bin 728: 1485 of cap free
Amount of items: 2
Items: 
Size: 554859 Color: 13
Size: 443657 Color: 12

Bin 729: 1509 of cap free
Amount of items: 2
Items: 
Size: 539862 Color: 10
Size: 458630 Color: 17

Bin 730: 1521 of cap free
Amount of items: 2
Items: 
Size: 720855 Color: 17
Size: 277625 Color: 2

Bin 731: 1523 of cap free
Amount of items: 2
Items: 
Size: 793684 Color: 5
Size: 204794 Color: 9

Bin 732: 1532 of cap free
Amount of items: 2
Items: 
Size: 687347 Color: 0
Size: 311122 Color: 17

Bin 733: 1536 of cap free
Amount of items: 2
Items: 
Size: 574039 Color: 11
Size: 424426 Color: 10

Bin 734: 1542 of cap free
Amount of items: 2
Items: 
Size: 712306 Color: 19
Size: 286153 Color: 10

Bin 735: 1547 of cap free
Amount of items: 2
Items: 
Size: 616172 Color: 4
Size: 382282 Color: 14

Bin 736: 1564 of cap free
Amount of items: 2
Items: 
Size: 707094 Color: 8
Size: 291343 Color: 19

Bin 737: 1580 of cap free
Amount of items: 2
Items: 
Size: 557018 Color: 18
Size: 441403 Color: 17

Bin 738: 1638 of cap free
Amount of items: 2
Items: 
Size: 528363 Color: 9
Size: 470000 Color: 3

Bin 739: 1644 of cap free
Amount of items: 2
Items: 
Size: 748091 Color: 18
Size: 250266 Color: 12

Bin 740: 1695 of cap free
Amount of items: 2
Items: 
Size: 634127 Color: 7
Size: 364179 Color: 2

Bin 741: 1747 of cap free
Amount of items: 2
Items: 
Size: 704922 Color: 9
Size: 293332 Color: 18

Bin 742: 1755 of cap free
Amount of items: 2
Items: 
Size: 678841 Color: 0
Size: 319405 Color: 18

Bin 743: 1775 of cap free
Amount of items: 2
Items: 
Size: 606066 Color: 19
Size: 392160 Color: 15

Bin 744: 1782 of cap free
Amount of items: 2
Items: 
Size: 695659 Color: 7
Size: 302560 Color: 17

Bin 745: 1788 of cap free
Amount of items: 2
Items: 
Size: 623173 Color: 14
Size: 375040 Color: 17

Bin 746: 1790 of cap free
Amount of items: 2
Items: 
Size: 552275 Color: 17
Size: 445936 Color: 15

Bin 747: 1813 of cap free
Amount of items: 2
Items: 
Size: 528213 Color: 13
Size: 469975 Color: 18

Bin 748: 1856 of cap free
Amount of items: 2
Items: 
Size: 635977 Color: 15
Size: 362168 Color: 8

Bin 749: 1884 of cap free
Amount of items: 2
Items: 
Size: 751713 Color: 11
Size: 246404 Color: 12

Bin 750: 1918 of cap free
Amount of items: 2
Items: 
Size: 781188 Color: 17
Size: 216895 Color: 1

Bin 751: 1921 of cap free
Amount of items: 2
Items: 
Size: 556857 Color: 10
Size: 441223 Color: 11

Bin 752: 1956 of cap free
Amount of items: 2
Items: 
Size: 702352 Color: 5
Size: 295693 Color: 18

Bin 753: 1957 of cap free
Amount of items: 2
Items: 
Size: 684609 Color: 11
Size: 313435 Color: 10

Bin 754: 1960 of cap free
Amount of items: 2
Items: 
Size: 731087 Color: 12
Size: 266954 Color: 15

Bin 755: 1996 of cap free
Amount of items: 2
Items: 
Size: 774619 Color: 0
Size: 223386 Color: 7

Bin 756: 2022 of cap free
Amount of items: 2
Items: 
Size: 697905 Color: 9
Size: 300074 Color: 0

Bin 757: 2034 of cap free
Amount of items: 2
Items: 
Size: 499982 Color: 14
Size: 497985 Color: 7

Bin 758: 2047 of cap free
Amount of items: 3
Items: 
Size: 678789 Color: 18
Size: 191283 Color: 6
Size: 127882 Color: 16

Bin 759: 2068 of cap free
Amount of items: 2
Items: 
Size: 702449 Color: 18
Size: 295484 Color: 11

Bin 760: 2083 of cap free
Amount of items: 2
Items: 
Size: 601350 Color: 13
Size: 396568 Color: 3

Bin 761: 2115 of cap free
Amount of items: 2
Items: 
Size: 784716 Color: 17
Size: 213170 Color: 11

Bin 762: 2197 of cap free
Amount of items: 2
Items: 
Size: 671934 Color: 13
Size: 325870 Color: 10

Bin 763: 2204 of cap free
Amount of items: 2
Items: 
Size: 652145 Color: 19
Size: 345652 Color: 5

Bin 764: 2239 of cap free
Amount of items: 2
Items: 
Size: 534487 Color: 7
Size: 463275 Color: 3

Bin 765: 2259 of cap free
Amount of items: 2
Items: 
Size: 626625 Color: 3
Size: 371117 Color: 14

Bin 766: 2327 of cap free
Amount of items: 2
Items: 
Size: 657321 Color: 7
Size: 340353 Color: 14

Bin 767: 2335 of cap free
Amount of items: 2
Items: 
Size: 527784 Color: 10
Size: 469882 Color: 18

Bin 768: 2342 of cap free
Amount of items: 2
Items: 
Size: 601325 Color: 0
Size: 396334 Color: 13

Bin 769: 2343 of cap free
Amount of items: 2
Items: 
Size: 757458 Color: 16
Size: 240200 Color: 6

Bin 770: 2345 of cap free
Amount of items: 2
Items: 
Size: 731083 Color: 9
Size: 266573 Color: 17

Bin 771: 2400 of cap free
Amount of items: 2
Items: 
Size: 738114 Color: 17
Size: 259487 Color: 4

Bin 772: 2406 of cap free
Amount of items: 2
Items: 
Size: 567384 Color: 2
Size: 430211 Color: 1

Bin 773: 2435 of cap free
Amount of items: 2
Items: 
Size: 543608 Color: 11
Size: 453958 Color: 12

Bin 774: 2500 of cap free
Amount of items: 2
Items: 
Size: 513703 Color: 16
Size: 483798 Color: 10

Bin 775: 2534 of cap free
Amount of items: 2
Items: 
Size: 663882 Color: 13
Size: 333585 Color: 7

Bin 776: 2581 of cap free
Amount of items: 2
Items: 
Size: 652115 Color: 5
Size: 345305 Color: 0

Bin 777: 2654 of cap free
Amount of items: 2
Items: 
Size: 675433 Color: 12
Size: 321914 Color: 16

Bin 778: 2676 of cap free
Amount of items: 2
Items: 
Size: 730824 Color: 8
Size: 266501 Color: 2

Bin 779: 2703 of cap free
Amount of items: 2
Items: 
Size: 534463 Color: 16
Size: 462835 Color: 18

Bin 780: 2713 of cap free
Amount of items: 2
Items: 
Size: 513661 Color: 19
Size: 483627 Color: 12

Bin 781: 2739 of cap free
Amount of items: 2
Items: 
Size: 601270 Color: 8
Size: 395992 Color: 2

Bin 782: 2761 of cap free
Amount of items: 2
Items: 
Size: 570072 Color: 9
Size: 427168 Color: 5

Bin 783: 2825 of cap free
Amount of items: 2
Items: 
Size: 601205 Color: 15
Size: 395971 Color: 8

Bin 784: 2889 of cap free
Amount of items: 2
Items: 
Size: 612175 Color: 17
Size: 384937 Color: 1

Bin 785: 2922 of cap free
Amount of items: 2
Items: 
Size: 770930 Color: 14
Size: 226149 Color: 16

Bin 786: 2927 of cap free
Amount of items: 2
Items: 
Size: 585286 Color: 18
Size: 411788 Color: 13

Bin 787: 3026 of cap free
Amount of items: 2
Items: 
Size: 656959 Color: 10
Size: 340016 Color: 17

Bin 788: 3039 of cap free
Amount of items: 2
Items: 
Size: 730777 Color: 18
Size: 266185 Color: 10

Bin 789: 3056 of cap free
Amount of items: 2
Items: 
Size: 784162 Color: 19
Size: 212783 Color: 8

Bin 790: 3110 of cap free
Amount of items: 2
Items: 
Size: 628971 Color: 12
Size: 367920 Color: 8

Bin 791: 3127 of cap free
Amount of items: 2
Items: 
Size: 519658 Color: 10
Size: 477216 Color: 16

Bin 792: 3149 of cap free
Amount of items: 2
Items: 
Size: 720224 Color: 18
Size: 276628 Color: 7

Bin 793: 3164 of cap free
Amount of items: 2
Items: 
Size: 505703 Color: 8
Size: 491134 Color: 9

Bin 794: 3193 of cap free
Amount of items: 2
Items: 
Size: 519578 Color: 9
Size: 477230 Color: 10

Bin 795: 3273 of cap free
Amount of items: 2
Items: 
Size: 730556 Color: 0
Size: 266172 Color: 19

Bin 796: 3468 of cap free
Amount of items: 2
Items: 
Size: 584795 Color: 17
Size: 411738 Color: 16

Bin 797: 3555 of cap free
Amount of items: 2
Items: 
Size: 537915 Color: 9
Size: 458531 Color: 3

Bin 798: 3557 of cap free
Amount of items: 2
Items: 
Size: 720125 Color: 12
Size: 276319 Color: 6

Bin 799: 3561 of cap free
Amount of items: 2
Items: 
Size: 590665 Color: 5
Size: 405775 Color: 8

Bin 800: 3580 of cap free
Amount of items: 2
Items: 
Size: 656471 Color: 2
Size: 339950 Color: 12

Bin 801: 3582 of cap free
Amount of items: 2
Items: 
Size: 533659 Color: 2
Size: 462760 Color: 19

Bin 802: 3618 of cap free
Amount of items: 2
Items: 
Size: 730418 Color: 5
Size: 265965 Color: 18

Bin 803: 3840 of cap free
Amount of items: 2
Items: 
Size: 622949 Color: 17
Size: 373212 Color: 3

Bin 804: 3910 of cap free
Amount of items: 2
Items: 
Size: 600843 Color: 3
Size: 395248 Color: 19

Bin 805: 4067 of cap free
Amount of items: 2
Items: 
Size: 616065 Color: 0
Size: 379869 Color: 19

Bin 806: 4199 of cap free
Amount of items: 2
Items: 
Size: 584155 Color: 19
Size: 411647 Color: 12

Bin 807: 4277 of cap free
Amount of items: 2
Items: 
Size: 783626 Color: 8
Size: 212098 Color: 9

Bin 808: 4353 of cap free
Amount of items: 2
Items: 
Size: 656102 Color: 7
Size: 339546 Color: 8

Bin 809: 4358 of cap free
Amount of items: 2
Items: 
Size: 663246 Color: 15
Size: 332397 Color: 5

Bin 810: 4359 of cap free
Amount of items: 2
Items: 
Size: 747107 Color: 11
Size: 248535 Color: 19

Bin 811: 4463 of cap free
Amount of items: 2
Items: 
Size: 552074 Color: 12
Size: 443464 Color: 2

Bin 812: 4533 of cap free
Amount of items: 2
Items: 
Size: 633606 Color: 6
Size: 361862 Color: 14

Bin 813: 4534 of cap free
Amount of items: 2
Items: 
Size: 628963 Color: 17
Size: 366504 Color: 18

Bin 814: 4665 of cap free
Amount of items: 2
Items: 
Size: 600534 Color: 1
Size: 394802 Color: 9

Bin 815: 5002 of cap free
Amount of items: 2
Items: 
Size: 546112 Color: 18
Size: 448887 Color: 0

Bin 816: 5172 of cap free
Amount of items: 2
Items: 
Size: 649962 Color: 4
Size: 344867 Color: 12

Bin 817: 5253 of cap free
Amount of items: 2
Items: 
Size: 497842 Color: 14
Size: 496906 Color: 13

Bin 818: 5363 of cap free
Amount of items: 2
Items: 
Size: 719619 Color: 9
Size: 275019 Color: 16

Bin 819: 5468 of cap free
Amount of items: 2
Items: 
Size: 704922 Color: 5
Size: 289611 Color: 10

Bin 820: 5559 of cap free
Amount of items: 2
Items: 
Size: 567325 Color: 11
Size: 427117 Color: 3

Bin 821: 5574 of cap free
Amount of items: 2
Items: 
Size: 719459 Color: 11
Size: 274968 Color: 17

Bin 822: 5624 of cap free
Amount of items: 2
Items: 
Size: 567308 Color: 13
Size: 427069 Color: 11

Bin 823: 5693 of cap free
Amount of items: 2
Items: 
Size: 704884 Color: 5
Size: 289424 Color: 9

Bin 824: 5928 of cap free
Amount of items: 2
Items: 
Size: 567250 Color: 10
Size: 426823 Color: 11

Bin 825: 5976 of cap free
Amount of items: 2
Items: 
Size: 719419 Color: 0
Size: 274606 Color: 3

Bin 826: 6021 of cap free
Amount of items: 2
Items: 
Size: 622913 Color: 9
Size: 371067 Color: 5

Bin 827: 6134 of cap free
Amount of items: 2
Items: 
Size: 735935 Color: 4
Size: 257932 Color: 12

Bin 828: 6228 of cap free
Amount of items: 2
Items: 
Size: 798354 Color: 1
Size: 195419 Color: 13

Bin 829: 6414 of cap free
Amount of items: 2
Items: 
Size: 719238 Color: 17
Size: 274349 Color: 5

Bin 830: 6443 of cap free
Amount of items: 2
Items: 
Size: 598474 Color: 6
Size: 395084 Color: 1

Bin 831: 6458 of cap free
Amount of items: 2
Items: 
Size: 582778 Color: 19
Size: 410765 Color: 1

Bin 832: 6598 of cap free
Amount of items: 2
Items: 
Size: 496735 Color: 11
Size: 496668 Color: 10

Bin 833: 6740 of cap free
Amount of items: 2
Items: 
Size: 566868 Color: 1
Size: 426393 Color: 16

Bin 834: 7443 of cap free
Amount of items: 2
Items: 
Size: 582069 Color: 19
Size: 410489 Color: 7

Bin 835: 7469 of cap free
Amount of items: 2
Items: 
Size: 551542 Color: 13
Size: 440990 Color: 8

Bin 836: 7575 of cap free
Amount of items: 2
Items: 
Size: 797575 Color: 10
Size: 194851 Color: 2

Bin 837: 7681 of cap free
Amount of items: 2
Items: 
Size: 496630 Color: 14
Size: 495690 Color: 7

Bin 838: 7763 of cap free
Amount of items: 2
Items: 
Size: 780498 Color: 8
Size: 211740 Color: 1

Bin 839: 8208 of cap free
Amount of items: 2
Items: 
Size: 519315 Color: 10
Size: 472478 Color: 17

Bin 840: 8401 of cap free
Amount of items: 2
Items: 
Size: 719178 Color: 14
Size: 272422 Color: 13

Bin 841: 8465 of cap free
Amount of items: 2
Items: 
Size: 719134 Color: 6
Size: 272402 Color: 4

Bin 842: 8513 of cap free
Amount of items: 2
Items: 
Size: 797411 Color: 0
Size: 194077 Color: 8

Bin 843: 8555 of cap free
Amount of items: 2
Items: 
Size: 519062 Color: 17
Size: 472384 Color: 2

Bin 844: 8930 of cap free
Amount of items: 2
Items: 
Size: 753816 Color: 13
Size: 237255 Color: 4

Bin 845: 9132 of cap free
Amount of items: 2
Items: 
Size: 566647 Color: 17
Size: 424222 Color: 6

Bin 846: 9225 of cap free
Amount of items: 2
Items: 
Size: 797137 Color: 15
Size: 193639 Color: 5

Bin 847: 10117 of cap free
Amount of items: 2
Items: 
Size: 531862 Color: 16
Size: 458022 Color: 19

Bin 848: 10248 of cap free
Amount of items: 2
Items: 
Size: 495098 Color: 0
Size: 494655 Color: 5

Bin 849: 10306 of cap free
Amount of items: 2
Items: 
Size: 700531 Color: 12
Size: 289164 Color: 9

Bin 850: 10601 of cap free
Amount of items: 2
Items: 
Size: 717345 Color: 7
Size: 272055 Color: 11

Bin 851: 10738 of cap free
Amount of items: 2
Items: 
Size: 622763 Color: 5
Size: 366500 Color: 6

Bin 852: 10896 of cap free
Amount of items: 2
Items: 
Size: 667209 Color: 5
Size: 321896 Color: 1

Bin 853: 11307 of cap free
Amount of items: 2
Items: 
Size: 597285 Color: 10
Size: 391409 Color: 17

Bin 854: 11509 of cap free
Amount of items: 2
Items: 
Size: 530545 Color: 11
Size: 457947 Color: 1

Bin 855: 11572 of cap free
Amount of items: 2
Items: 
Size: 564289 Color: 11
Size: 424140 Color: 7

Bin 856: 12095 of cap free
Amount of items: 2
Items: 
Size: 597242 Color: 10
Size: 390664 Color: 9

Bin 857: 12350 of cap free
Amount of items: 2
Items: 
Size: 530448 Color: 9
Size: 457203 Color: 14

Bin 858: 12506 of cap free
Amount of items: 2
Items: 
Size: 563964 Color: 17
Size: 423531 Color: 5

Bin 859: 12658 of cap free
Amount of items: 2
Items: 
Size: 530291 Color: 10
Size: 457052 Color: 5

Bin 860: 13903 of cap free
Amount of items: 2
Items: 
Size: 595581 Color: 7
Size: 390517 Color: 0

Bin 861: 13923 of cap free
Amount of items: 2
Items: 
Size: 546402 Color: 0
Size: 439676 Color: 16

Bin 862: 14444 of cap free
Amount of items: 2
Items: 
Size: 546044 Color: 15
Size: 439513 Color: 7

Bin 863: 14555 of cap free
Amount of items: 2
Items: 
Size: 580266 Color: 10
Size: 405180 Color: 2

Bin 864: 14595 of cap free
Amount of items: 2
Items: 
Size: 494326 Color: 1
Size: 491080 Color: 12

Bin 865: 15323 of cap free
Amount of items: 2
Items: 
Size: 594930 Color: 9
Size: 389748 Color: 1

Bin 866: 17524 of cap free
Amount of items: 2
Items: 
Size: 543055 Color: 16
Size: 439422 Color: 4

Bin 867: 17799 of cap free
Amount of items: 2
Items: 
Size: 543005 Color: 11
Size: 439197 Color: 6

Bin 868: 18104 of cap free
Amount of items: 2
Items: 
Size: 491066 Color: 0
Size: 490831 Color: 16

Bin 869: 21653 of cap free
Amount of items: 2
Items: 
Size: 692522 Color: 13
Size: 285826 Color: 19

Bin 870: 21799 of cap free
Amount of items: 2
Items: 
Size: 662002 Color: 8
Size: 316200 Color: 3

Bin 871: 22007 of cap free
Amount of items: 2
Items: 
Size: 793445 Color: 13
Size: 184549 Color: 2

Bin 872: 24387 of cap free
Amount of items: 2
Items: 
Size: 793232 Color: 6
Size: 182382 Color: 2

Bin 873: 25827 of cap free
Amount of items: 2
Items: 
Size: 791947 Color: 15
Size: 182227 Color: 6

Bin 874: 27087 of cap free
Amount of items: 2
Items: 
Size: 791288 Color: 4
Size: 181626 Color: 18

Bin 875: 28978 of cap free
Amount of items: 2
Items: 
Size: 655464 Color: 10
Size: 315559 Color: 8

Bin 876: 30274 of cap free
Amount of items: 2
Items: 
Size: 580162 Color: 14
Size: 389565 Color: 4

Bin 877: 30745 of cap free
Amount of items: 2
Items: 
Size: 791829 Color: 18
Size: 177427 Color: 11

Bin 878: 32210 of cap free
Amount of items: 2
Items: 
Size: 579566 Color: 1
Size: 388225 Color: 3

Bin 879: 37892 of cap free
Amount of items: 2
Items: 
Size: 789117 Color: 13
Size: 172992 Color: 12

Bin 880: 40213 of cap free
Amount of items: 2
Items: 
Size: 646918 Color: 0
Size: 312870 Color: 4

Bin 881: 41879 of cap free
Amount of items: 2
Items: 
Size: 788504 Color: 10
Size: 169618 Color: 0

Bin 882: 42323 of cap free
Amount of items: 2
Items: 
Size: 518919 Color: 14
Size: 438759 Color: 18

Bin 883: 43473 of cap free
Amount of items: 2
Items: 
Size: 646635 Color: 0
Size: 309893 Color: 16

Bin 884: 44087 of cap free
Amount of items: 2
Items: 
Size: 518848 Color: 11
Size: 437066 Color: 1

Bin 885: 44151 of cap free
Amount of items: 2
Items: 
Size: 518547 Color: 5
Size: 437303 Color: 11

Bin 886: 45692 of cap free
Amount of items: 2
Items: 
Size: 517239 Color: 5
Size: 437070 Color: 11

Bin 887: 58223 of cap free
Amount of items: 2
Items: 
Size: 779109 Color: 3
Size: 162669 Color: 15

Bin 888: 59906 of cap free
Amount of items: 2
Items: 
Size: 777672 Color: 7
Size: 162423 Color: 1

Bin 889: 59980 of cap free
Amount of items: 2
Items: 
Size: 516595 Color: 11
Size: 423426 Color: 12

Bin 890: 60352 of cap free
Amount of items: 2
Items: 
Size: 516380 Color: 14
Size: 423269 Color: 3

Bin 891: 67817 of cap free
Amount of items: 2
Items: 
Size: 777588 Color: 18
Size: 154596 Color: 12

Bin 892: 68203 of cap free
Amount of items: 2
Items: 
Size: 646306 Color: 6
Size: 285492 Color: 14

Bin 893: 225453 of cap free
Amount of items: 1
Items: 
Size: 774548 Color: 2

Bin 894: 226025 of cap free
Amount of items: 1
Items: 
Size: 773976 Color: 17

Bin 895: 231542 of cap free
Amount of items: 1
Items: 
Size: 768459 Color: 14

Bin 896: 231925 of cap free
Amount of items: 1
Items: 
Size: 768076 Color: 1

Bin 897: 234619 of cap free
Amount of items: 1
Items: 
Size: 765382 Color: 8

Bin 898: 234641 of cap free
Amount of items: 1
Items: 
Size: 765360 Color: 2

Bin 899: 237158 of cap free
Amount of items: 1
Items: 
Size: 762843 Color: 0

Total size: 895477482
Total free space: 3523417

