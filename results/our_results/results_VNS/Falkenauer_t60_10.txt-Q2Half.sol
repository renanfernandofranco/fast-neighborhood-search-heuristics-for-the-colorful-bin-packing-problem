Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 277 Color: 0
Size: 323 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 299 Color: 0
Size: 335 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 265 Color: 1
Size: 257 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 357 Color: 1
Size: 260 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 301 Color: 1
Size: 280 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 347 Color: 1
Size: 250 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 271 Color: 1
Size: 265 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 256 Color: 1
Size: 253 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 284 Color: 1
Size: 268 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 332 Color: 1
Size: 251 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 294 Color: 1
Size: 266 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 310 Color: 1
Size: 266 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 292 Color: 1
Size: 280 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 322 Color: 1
Size: 318 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 355 Color: 1
Size: 257 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 351 Color: 1
Size: 251 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 291 Color: 1
Size: 270 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 320 Color: 1
Size: 257 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 285 Color: 1
Size: 274 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 278 Color: 1
Size: 250 Color: 0

Total size: 20000
Total free space: 0

