Capicity Bin: 5392
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3656 Color: 1
Size: 1396 Color: 1
Size: 340 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3284 Color: 1
Size: 1708 Color: 1
Size: 400 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4280 Color: 1
Size: 982 Color: 1
Size: 130 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3612 Color: 1
Size: 1656 Color: 1
Size: 124 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 4006 Color: 1
Size: 1150 Color: 1
Size: 172 Color: 0
Size: 64 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 2700 Color: 1
Size: 2356 Color: 1
Size: 192 Color: 0
Size: 144 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2712 Color: 1
Size: 2248 Color: 1
Size: 432 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 2708 Color: 1
Size: 2244 Color: 1
Size: 232 Color: 0
Size: 208 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2702 Color: 1
Size: 2242 Color: 1
Size: 448 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2698 Color: 1
Size: 2008 Color: 1
Size: 686 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 3383 Color: 1
Size: 1625 Color: 1
Size: 192 Color: 0
Size: 192 Color: 0

Bin 12: 0 of cap free
Amount of items: 5
Items: 
Size: 1764 Color: 1
Size: 1448 Color: 1
Size: 1144 Color: 1
Size: 924 Color: 0
Size: 112 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 1
Size: 862 Color: 1
Size: 200 Color: 0

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 2740 Color: 1
Size: 2092 Color: 1
Size: 448 Color: 0
Size: 112 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 3065 Color: 1
Size: 1941 Color: 1
Size: 386 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 3069 Color: 1
Size: 1937 Color: 1
Size: 386 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 3326 Color: 1
Size: 1722 Color: 1
Size: 344 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 3376 Color: 1
Size: 1816 Color: 1
Size: 200 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 3431 Color: 1
Size: 1771 Color: 1
Size: 190 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 3610 Color: 1
Size: 1486 Color: 1
Size: 296 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 3646 Color: 1
Size: 1500 Color: 1
Size: 246 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 3676 Color: 1
Size: 1436 Color: 1
Size: 280 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 3685 Color: 1
Size: 1411 Color: 1
Size: 296 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 3692 Color: 1
Size: 1348 Color: 1
Size: 352 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 3731 Color: 1
Size: 1221 Color: 1
Size: 440 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 3784 Color: 1
Size: 1448 Color: 1
Size: 160 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 3804 Color: 1
Size: 1468 Color: 1
Size: 120 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 3826 Color: 1
Size: 1458 Color: 1
Size: 108 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 3834 Color: 1
Size: 1166 Color: 1
Size: 392 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 3864 Color: 1
Size: 1420 Color: 1
Size: 108 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4020 Color: 1
Size: 1180 Color: 1
Size: 192 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4024 Color: 1
Size: 1248 Color: 1
Size: 120 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4038 Color: 1
Size: 1130 Color: 1
Size: 224 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 1
Size: 1120 Color: 1
Size: 220 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4091 Color: 1
Size: 1085 Color: 1
Size: 216 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 1
Size: 1172 Color: 1
Size: 52 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4172 Color: 1
Size: 996 Color: 1
Size: 224 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4182 Color: 1
Size: 994 Color: 1
Size: 216 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4202 Color: 1
Size: 794 Color: 1
Size: 396 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4204 Color: 1
Size: 1020 Color: 1
Size: 168 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 4221 Color: 1
Size: 977 Color: 1
Size: 194 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4229 Color: 1
Size: 723 Color: 1
Size: 440 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 1
Size: 936 Color: 1
Size: 140 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 4318 Color: 1
Size: 1010 Color: 1
Size: 64 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 4335 Color: 1
Size: 769 Color: 1
Size: 288 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 4341 Color: 1
Size: 791 Color: 1
Size: 260 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 4345 Color: 1
Size: 873 Color: 1
Size: 174 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4349 Color: 1
Size: 795 Color: 1
Size: 248 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 4362 Color: 1
Size: 898 Color: 1
Size: 132 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 4376 Color: 1
Size: 856 Color: 1
Size: 160 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 4388 Color: 1
Size: 948 Color: 1
Size: 56 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 4420 Color: 1
Size: 812 Color: 1
Size: 160 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 4439 Color: 1
Size: 671 Color: 1
Size: 282 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4450 Color: 1
Size: 766 Color: 1
Size: 176 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 1
Size: 900 Color: 1
Size: 16 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 4488 Color: 1
Size: 568 Color: 1
Size: 336 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 4506 Color: 1
Size: 610 Color: 1
Size: 276 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 4518 Color: 1
Size: 722 Color: 1
Size: 152 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 4525 Color: 1
Size: 709 Color: 1
Size: 158 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 1
Size: 776 Color: 1
Size: 64 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 4556 Color: 1
Size: 740 Color: 1
Size: 96 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 4578 Color: 1
Size: 638 Color: 1
Size: 176 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 4588 Color: 1
Size: 700 Color: 1
Size: 104 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 4589 Color: 1
Size: 575 Color: 1
Size: 228 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 4607 Color: 1
Size: 609 Color: 1
Size: 176 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4612 Color: 1
Size: 764 Color: 1
Size: 16 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 4615 Color: 1
Size: 649 Color: 1
Size: 128 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 4630 Color: 1
Size: 632 Color: 1
Size: 130 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4648 Color: 1
Size: 652 Color: 1
Size: 92 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4661 Color: 1
Size: 611 Color: 1
Size: 120 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 4662 Color: 1
Size: 594 Color: 1
Size: 136 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 4663 Color: 1
Size: 571 Color: 1
Size: 158 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 4668 Color: 1
Size: 608 Color: 1
Size: 116 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 4680 Color: 1
Size: 564 Color: 1
Size: 148 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 4682 Color: 1
Size: 470 Color: 1
Size: 240 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 4712 Color: 1
Size: 552 Color: 1
Size: 128 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 4715 Color: 1
Size: 565 Color: 1
Size: 112 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 4716 Color: 1
Size: 546 Color: 1
Size: 130 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 4738 Color: 1
Size: 540 Color: 1
Size: 114 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 4744 Color: 1
Size: 496 Color: 1
Size: 152 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 1
Size: 516 Color: 1
Size: 128 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 4770 Color: 1
Size: 522 Color: 1
Size: 100 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 1
Size: 514 Color: 1
Size: 100 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 4780 Color: 1
Size: 334 Color: 1
Size: 278 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 4802 Color: 1
Size: 494 Color: 1
Size: 96 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 1
Size: 492 Color: 1
Size: 96 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 4830 Color: 1
Size: 448 Color: 1
Size: 114 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 4832 Color: 1
Size: 386 Color: 1
Size: 174 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4850 Color: 1
Size: 454 Color: 1
Size: 88 Color: 0

Bin 90: 1 of cap free
Amount of items: 3
Items: 
Size: 3180 Color: 1
Size: 1763 Color: 1
Size: 448 Color: 0

Bin 91: 1 of cap free
Amount of items: 3
Items: 
Size: 3316 Color: 1
Size: 1931 Color: 1
Size: 144 Color: 0

Bin 92: 1 of cap free
Amount of items: 3
Items: 
Size: 3346 Color: 1
Size: 1933 Color: 1
Size: 112 Color: 0

Bin 93: 1 of cap free
Amount of items: 3
Items: 
Size: 3379 Color: 1
Size: 1732 Color: 1
Size: 280 Color: 0

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 3644 Color: 1
Size: 1423 Color: 1
Size: 324 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 3699 Color: 1
Size: 1516 Color: 1
Size: 176 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 3719 Color: 1
Size: 1288 Color: 1
Size: 384 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 3916 Color: 1
Size: 1091 Color: 1
Size: 384 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 4085 Color: 1
Size: 1146 Color: 1
Size: 160 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 4132 Color: 1
Size: 1227 Color: 1
Size: 32 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 4218 Color: 1
Size: 877 Color: 1
Size: 296 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 1
Size: 971 Color: 1
Size: 128 Color: 0

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 1
Size: 600 Color: 1
Size: 320 Color: 0

Bin 103: 1 of cap free
Amount of items: 3
Items: 
Size: 4603 Color: 1
Size: 604 Color: 1
Size: 184 Color: 0

Bin 104: 1 of cap free
Amount of items: 3
Items: 
Size: 4703 Color: 1
Size: 592 Color: 1
Size: 96 Color: 0

Bin 105: 1 of cap free
Amount of items: 3
Items: 
Size: 3002 Color: 1
Size: 2245 Color: 1
Size: 144 Color: 0

Bin 106: 1 of cap free
Amount of items: 3
Items: 
Size: 2699 Color: 1
Size: 1844 Color: 1
Size: 848 Color: 0

Bin 107: 1 of cap free
Amount of items: 3
Items: 
Size: 3416 Color: 1
Size: 1679 Color: 1
Size: 296 Color: 0

Bin 108: 1 of cap free
Amount of items: 3
Items: 
Size: 3712 Color: 1
Size: 1395 Color: 1
Size: 284 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 3034 Color: 1
Size: 2244 Color: 1
Size: 112 Color: 0

Bin 110: 2 of cap free
Amount of items: 3
Items: 
Size: 4543 Color: 1
Size: 655 Color: 1
Size: 192 Color: 0

Bin 111: 2 of cap free
Amount of items: 3
Items: 
Size: 4443 Color: 1
Size: 659 Color: 1
Size: 288 Color: 0

Bin 112: 2 of cap free
Amount of items: 3
Items: 
Size: 3354 Color: 1
Size: 1840 Color: 1
Size: 196 Color: 0

Bin 113: 2 of cap free
Amount of items: 3
Items: 
Size: 3740 Color: 1
Size: 1306 Color: 1
Size: 344 Color: 0

Bin 114: 2 of cap free
Amount of items: 3
Items: 
Size: 4014 Color: 1
Size: 1032 Color: 1
Size: 344 Color: 0

Bin 115: 2 of cap free
Amount of items: 3
Items: 
Size: 3348 Color: 1
Size: 1706 Color: 1
Size: 336 Color: 0

Bin 116: 2 of cap free
Amount of items: 3
Items: 
Size: 4611 Color: 1
Size: 651 Color: 1
Size: 128 Color: 0

Bin 117: 3 of cap free
Amount of items: 3
Items: 
Size: 3443 Color: 1
Size: 1702 Color: 1
Size: 244 Color: 0

Bin 118: 3 of cap free
Amount of items: 5
Items: 
Size: 2697 Color: 1
Size: 2228 Color: 1
Size: 208 Color: 0
Size: 144 Color: 0
Size: 112 Color: 0

Bin 119: 4 of cap free
Amount of items: 3
Items: 
Size: 4526 Color: 1
Size: 742 Color: 1
Size: 120 Color: 0

Bin 120: 5 of cap free
Amount of items: 3
Items: 
Size: 2884 Color: 1
Size: 2247 Color: 1
Size: 256 Color: 0

Bin 121: 5 of cap free
Amount of items: 3
Items: 
Size: 3580 Color: 1
Size: 1631 Color: 1
Size: 176 Color: 0

Bin 122: 6 of cap free
Amount of items: 3
Items: 
Size: 2984 Color: 1
Size: 1986 Color: 1
Size: 416 Color: 0

Bin 123: 6 of cap free
Amount of items: 3
Items: 
Size: 3633 Color: 1
Size: 1385 Color: 1
Size: 368 Color: 0

Bin 124: 8 of cap free
Amount of items: 3
Items: 
Size: 3010 Color: 1
Size: 2310 Color: 1
Size: 64 Color: 0

Bin 125: 10 of cap free
Amount of items: 3
Items: 
Size: 3224 Color: 1
Size: 1966 Color: 1
Size: 192 Color: 0

Bin 126: 12 of cap free
Amount of items: 3
Items: 
Size: 2724 Color: 1
Size: 1484 Color: 0
Size: 1172 Color: 1

Bin 127: 48 of cap free
Amount of items: 5
Items: 
Size: 2144 Color: 1
Size: 1052 Color: 0
Size: 881 Color: 1
Size: 871 Color: 1
Size: 396 Color: 0

Bin 128: 51 of cap free
Amount of items: 3
Items: 
Size: 3927 Color: 1
Size: 1302 Color: 1
Size: 112 Color: 0

Bin 129: 77 of cap free
Amount of items: 3
Items: 
Size: 3077 Color: 1
Size: 1994 Color: 1
Size: 244 Color: 0

Bin 130: 644 of cap free
Amount of items: 2
Items: 
Size: 3988 Color: 1
Size: 760 Color: 0

Bin 131: 685 of cap free
Amount of items: 1
Items: 
Size: 4707 Color: 1

Bin 132: 1471 of cap free
Amount of items: 1
Items: 
Size: 3921 Color: 1

Bin 133: 2319 of cap free
Amount of items: 1
Items: 
Size: 3073 Color: 1

Total size: 711744
Total free space: 5392

