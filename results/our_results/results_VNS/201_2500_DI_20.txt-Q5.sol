Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1391 Color: 2
Size: 969 Color: 4
Size: 96 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1444 Color: 4
Size: 840 Color: 3
Size: 172 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 4
Size: 814 Color: 2
Size: 84 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 4
Size: 618 Color: 3
Size: 40 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 4
Size: 438 Color: 0
Size: 204 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1835 Color: 3
Size: 519 Color: 0
Size: 102 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 0
Size: 530 Color: 1
Size: 32 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1874 Color: 4
Size: 486 Color: 3
Size: 96 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1901 Color: 4
Size: 451 Color: 3
Size: 104 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1923 Color: 1
Size: 459 Color: 0
Size: 74 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 3
Size: 378 Color: 1
Size: 136 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1948 Color: 3
Size: 272 Color: 0
Size: 236 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 3
Size: 250 Color: 4
Size: 200 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 4
Size: 338 Color: 3
Size: 64 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 4
Size: 206 Color: 0
Size: 144 Color: 4

Bin 16: 0 of cap free
Amount of items: 4
Items: 
Size: 2124 Color: 1
Size: 308 Color: 2
Size: 16 Color: 2
Size: 8 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 1
Size: 168 Color: 0
Size: 130 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 4
Size: 220 Color: 0
Size: 56 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 1
Size: 132 Color: 3
Size: 114 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 3
Size: 1020 Color: 2
Size: 204 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 2
Size: 747 Color: 0
Size: 120 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 0
Size: 409 Color: 2
Size: 48 Color: 3

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 3
Size: 374 Color: 2
Size: 80 Color: 0

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 2074 Color: 3
Size: 381 Color: 4

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 4
Size: 844 Color: 2
Size: 168 Color: 0

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 0
Size: 845 Color: 1
Size: 48 Color: 3

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 661 Color: 0
Size: 120 Color: 4

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1804 Color: 3
Size: 548 Color: 4
Size: 102 Color: 0

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 0
Size: 571 Color: 2
Size: 40 Color: 4

Bin 30: 2 of cap free
Amount of items: 4
Items: 
Size: 2090 Color: 1
Size: 340 Color: 3
Size: 16 Color: 0
Size: 8 Color: 3

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 2092 Color: 0
Size: 306 Color: 4
Size: 56 Color: 2

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 2170 Color: 4
Size: 284 Color: 2

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1915 Color: 1
Size: 538 Color: 2

Bin 34: 4 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 2
Size: 690 Color: 0
Size: 40 Color: 4

Bin 35: 4 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 1
Size: 647 Color: 2
Size: 32 Color: 1

Bin 36: 5 of cap free
Amount of items: 3
Items: 
Size: 1545 Color: 0
Size: 846 Color: 2
Size: 60 Color: 1

Bin 37: 5 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 3
Size: 731 Color: 1
Size: 90 Color: 1

Bin 38: 5 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 4
Size: 614 Color: 3
Size: 72 Color: 0

Bin 39: 5 of cap free
Amount of items: 3
Items: 
Size: 1907 Color: 0
Size: 504 Color: 3
Size: 40 Color: 2

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 1967 Color: 0
Size: 484 Color: 2

Bin 41: 6 of cap free
Amount of items: 2
Items: 
Size: 2196 Color: 3
Size: 254 Color: 1

Bin 42: 7 of cap free
Amount of items: 2
Items: 
Size: 2004 Color: 3
Size: 445 Color: 2

Bin 43: 9 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 2
Size: 1018 Color: 2
Size: 200 Color: 3

Bin 44: 9 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 1
Size: 511 Color: 4
Size: 52 Color: 4

Bin 45: 10 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 3
Size: 665 Color: 1
Size: 100 Color: 4

Bin 46: 11 of cap free
Amount of items: 5
Items: 
Size: 1389 Color: 0
Size: 428 Color: 1
Size: 414 Color: 3
Size: 150 Color: 3
Size: 64 Color: 2

Bin 47: 11 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 4
Size: 576 Color: 0
Size: 204 Color: 4

Bin 48: 11 of cap free
Amount of items: 2
Items: 
Size: 1982 Color: 1
Size: 463 Color: 4

Bin 49: 12 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 1
Size: 984 Color: 2
Size: 64 Color: 0

Bin 50: 12 of cap free
Amount of items: 2
Items: 
Size: 1553 Color: 1
Size: 891 Color: 2

Bin 51: 12 of cap free
Amount of items: 3
Items: 
Size: 2052 Color: 4
Size: 384 Color: 2
Size: 8 Color: 3

Bin 52: 14 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 0
Size: 874 Color: 4
Size: 32 Color: 4

Bin 53: 15 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 2
Size: 750 Color: 3
Size: 32 Color: 4

Bin 54: 15 of cap free
Amount of items: 2
Items: 
Size: 1688 Color: 0
Size: 753 Color: 1

Bin 55: 16 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 3
Size: 1022 Color: 4
Size: 128 Color: 1
Size: 60 Color: 1

Bin 56: 16 of cap free
Amount of items: 2
Items: 
Size: 1521 Color: 4
Size: 919 Color: 2

Bin 57: 17 of cap free
Amount of items: 2
Items: 
Size: 1934 Color: 4
Size: 505 Color: 3

Bin 58: 20 of cap free
Amount of items: 10
Items: 
Size: 430 Color: 0
Size: 406 Color: 1
Size: 380 Color: 3
Size: 294 Color: 2
Size: 272 Color: 3
Size: 168 Color: 4
Size: 150 Color: 2
Size: 148 Color: 4
Size: 96 Color: 0
Size: 92 Color: 0

Bin 59: 21 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 0
Size: 761 Color: 2
Size: 84 Color: 2

Bin 60: 25 of cap free
Amount of items: 2
Items: 
Size: 1410 Color: 4
Size: 1021 Color: 3

Bin 61: 26 of cap free
Amount of items: 4
Items: 
Size: 1238 Color: 2
Size: 1028 Color: 1
Size: 112 Color: 0
Size: 52 Color: 3

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1851 Color: 4
Size: 577 Color: 2

Bin 63: 30 of cap free
Amount of items: 3
Items: 
Size: 1708 Color: 1
Size: 628 Color: 2
Size: 90 Color: 2

Bin 64: 41 of cap free
Amount of items: 4
Items: 
Size: 1443 Color: 0
Size: 724 Color: 4
Size: 176 Color: 1
Size: 72 Color: 3

Bin 65: 49 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 0
Size: 1023 Color: 2
Size: 148 Color: 1

Bin 66: 1956 of cap free
Amount of items: 6
Items: 
Size: 104 Color: 1
Size: 88 Color: 4
Size: 88 Color: 1
Size: 88 Color: 0
Size: 80 Color: 4
Size: 52 Color: 3

Total size: 159640
Total free space: 2456

