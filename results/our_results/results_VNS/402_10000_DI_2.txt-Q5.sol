Capicity Bin: 7472
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3738 Color: 0
Size: 1011 Color: 0
Size: 959 Color: 0
Size: 620 Color: 2
Size: 616 Color: 1
Size: 368 Color: 1
Size: 160 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4710 Color: 1
Size: 2302 Color: 1
Size: 460 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4923 Color: 3
Size: 1841 Color: 0
Size: 708 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 3
Size: 2044 Color: 0
Size: 168 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5281 Color: 1
Size: 2023 Color: 2
Size: 168 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 2
Size: 1676 Color: 0
Size: 328 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5535 Color: 3
Size: 1643 Color: 0
Size: 294 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 4
Size: 1362 Color: 1
Size: 290 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5985 Color: 0
Size: 1073 Color: 4
Size: 414 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6025 Color: 0
Size: 1207 Color: 3
Size: 240 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6077 Color: 0
Size: 971 Color: 3
Size: 424 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 3
Size: 1124 Color: 4
Size: 216 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6149 Color: 3
Size: 1103 Color: 0
Size: 220 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6273 Color: 0
Size: 1001 Color: 3
Size: 198 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 1
Size: 790 Color: 2
Size: 408 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6307 Color: 1
Size: 765 Color: 2
Size: 400 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6314 Color: 3
Size: 870 Color: 0
Size: 288 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 3
Size: 678 Color: 1
Size: 478 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6323 Color: 4
Size: 925 Color: 3
Size: 224 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6390 Color: 0
Size: 544 Color: 3
Size: 538 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 2
Size: 964 Color: 0
Size: 80 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 0
Size: 861 Color: 1
Size: 172 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 0
Size: 546 Color: 3
Size: 472 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6476 Color: 0
Size: 674 Color: 4
Size: 322 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 0
Size: 821 Color: 3
Size: 164 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6509 Color: 4
Size: 803 Color: 2
Size: 160 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 3
Size: 550 Color: 1
Size: 396 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6542 Color: 1
Size: 622 Color: 2
Size: 308 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 3
Size: 755 Color: 0
Size: 150 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6592 Color: 4
Size: 724 Color: 1
Size: 156 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 3
Size: 504 Color: 0
Size: 370 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 3
Size: 708 Color: 4
Size: 152 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6686 Color: 0
Size: 568 Color: 2
Size: 218 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 3
Size: 658 Color: 0
Size: 152 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 0
Size: 644 Color: 2
Size: 136 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 4
Size: 542 Color: 0
Size: 212 Color: 3

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 4235 Color: 2
Size: 2628 Color: 2
Size: 608 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 0
Size: 2375 Color: 0
Size: 804 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 4420 Color: 0
Size: 2685 Color: 2
Size: 366 Color: 1

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 4620 Color: 2
Size: 2731 Color: 0
Size: 120 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5021 Color: 2
Size: 2260 Color: 0
Size: 190 Color: 4

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5172 Color: 2
Size: 2043 Color: 0
Size: 256 Color: 4

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5333 Color: 3
Size: 2002 Color: 0
Size: 136 Color: 3

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5555 Color: 0
Size: 1844 Color: 3
Size: 72 Color: 1

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5614 Color: 1
Size: 1615 Color: 0
Size: 242 Color: 1

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5727 Color: 0
Size: 1550 Color: 2
Size: 194 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5743 Color: 0
Size: 1380 Color: 1
Size: 348 Color: 4

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5867 Color: 2
Size: 1556 Color: 0
Size: 48 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 5980 Color: 2
Size: 1339 Color: 0
Size: 152 Color: 3

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 2
Size: 1324 Color: 0
Size: 138 Color: 1

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6155 Color: 4
Size: 1132 Color: 4
Size: 184 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6166 Color: 3
Size: 1241 Color: 1
Size: 64 Color: 3

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 0
Size: 986 Color: 2
Size: 202 Color: 3

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6331 Color: 3
Size: 972 Color: 2
Size: 168 Color: 0

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6463 Color: 4
Size: 742 Color: 4
Size: 266 Color: 0

Bin 56: 2 of cap free
Amount of items: 15
Items: 
Size: 855 Color: 0
Size: 855 Color: 0
Size: 778 Color: 0
Size: 620 Color: 0
Size: 536 Color: 4
Size: 472 Color: 3
Size: 472 Color: 2
Size: 472 Color: 1
Size: 456 Color: 2
Size: 448 Color: 4
Size: 424 Color: 2
Size: 410 Color: 1
Size: 368 Color: 2
Size: 160 Color: 1
Size: 144 Color: 3

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 4623 Color: 2
Size: 2719 Color: 4
Size: 128 Color: 0

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 5090 Color: 0
Size: 2380 Color: 3

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5241 Color: 1
Size: 2125 Color: 2
Size: 104 Color: 1

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5716 Color: 3
Size: 1226 Color: 0
Size: 528 Color: 1

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5838 Color: 4
Size: 1090 Color: 0
Size: 542 Color: 3

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 5892 Color: 2
Size: 1538 Color: 1
Size: 40 Color: 1

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6068 Color: 2
Size: 928 Color: 4
Size: 474 Color: 0

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 4
Size: 939 Color: 3
Size: 272 Color: 0

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 6347 Color: 2
Size: 991 Color: 0
Size: 132 Color: 1

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 6430 Color: 1
Size: 1040 Color: 3

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 4171 Color: 2
Size: 3102 Color: 0
Size: 196 Color: 2

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 4981 Color: 1
Size: 2290 Color: 0
Size: 198 Color: 2

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 5074 Color: 1
Size: 2395 Color: 2

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 6503 Color: 1
Size: 966 Color: 4

Bin 71: 4 of cap free
Amount of items: 5
Items: 
Size: 3737 Color: 4
Size: 1475 Color: 0
Size: 1374 Color: 0
Size: 538 Color: 4
Size: 344 Color: 1

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 0
Size: 2654 Color: 2
Size: 730 Color: 4

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 1
Size: 2666 Color: 2
Size: 528 Color: 3

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 4764 Color: 4
Size: 2560 Color: 3
Size: 144 Color: 0

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 4
Size: 736 Color: 2
Size: 24 Color: 1

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 4211 Color: 4
Size: 3084 Color: 4
Size: 172 Color: 0

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 4576 Color: 3
Size: 2711 Color: 2
Size: 180 Color: 0

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 4639 Color: 2
Size: 2828 Color: 3

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 6516 Color: 1
Size: 951 Color: 3

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 4243 Color: 3
Size: 2691 Color: 4
Size: 532 Color: 1

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 4726 Color: 1
Size: 2740 Color: 4

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 0
Size: 2014 Color: 3
Size: 232 Color: 3

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 5703 Color: 1
Size: 1762 Color: 4

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 6290 Color: 4
Size: 1175 Color: 1

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 6363 Color: 4
Size: 1102 Color: 2

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 4
Size: 3106 Color: 0
Size: 170 Color: 1

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 5883 Color: 4
Size: 1581 Color: 3

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 6593 Color: 2
Size: 871 Color: 4

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 6628 Color: 2
Size: 836 Color: 3

Bin 90: 9 of cap free
Amount of items: 3
Items: 
Size: 3746 Color: 3
Size: 2652 Color: 2
Size: 1065 Color: 4

Bin 91: 10 of cap free
Amount of items: 3
Items: 
Size: 5442 Color: 0
Size: 1876 Color: 3
Size: 144 Color: 1

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 6586 Color: 4
Size: 876 Color: 2

Bin 93: 11 of cap free
Amount of items: 3
Items: 
Size: 3754 Color: 1
Size: 2776 Color: 0
Size: 931 Color: 1

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 5993 Color: 3
Size: 1468 Color: 1

Bin 95: 12 of cap free
Amount of items: 3
Items: 
Size: 5976 Color: 4
Size: 1396 Color: 3
Size: 88 Color: 0

Bin 96: 13 of cap free
Amount of items: 5
Items: 
Size: 3741 Color: 1
Size: 1694 Color: 0
Size: 850 Color: 3
Size: 686 Color: 3
Size: 488 Color: 4

Bin 97: 13 of cap free
Amount of items: 2
Items: 
Size: 4195 Color: 2
Size: 3264 Color: 1

Bin 98: 13 of cap free
Amount of items: 2
Items: 
Size: 5575 Color: 1
Size: 1884 Color: 3

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 6018 Color: 4
Size: 1441 Color: 1

Bin 100: 13 of cap free
Amount of items: 3
Items: 
Size: 6150 Color: 4
Size: 1221 Color: 2
Size: 88 Color: 2

Bin 101: 13 of cap free
Amount of items: 2
Items: 
Size: 6650 Color: 3
Size: 809 Color: 1

Bin 102: 15 of cap free
Amount of items: 3
Items: 
Size: 5508 Color: 3
Size: 1861 Color: 2
Size: 88 Color: 1

Bin 103: 15 of cap free
Amount of items: 2
Items: 
Size: 5826 Color: 1
Size: 1631 Color: 2

Bin 104: 15 of cap free
Amount of items: 2
Items: 
Size: 6002 Color: 1
Size: 1455 Color: 4

Bin 105: 15 of cap free
Amount of items: 2
Items: 
Size: 6555 Color: 4
Size: 902 Color: 2

Bin 106: 17 of cap free
Amount of items: 2
Items: 
Size: 5378 Color: 2
Size: 2077 Color: 3

Bin 107: 18 of cap free
Amount of items: 2
Items: 
Size: 6355 Color: 3
Size: 1099 Color: 2

Bin 108: 20 of cap free
Amount of items: 2
Items: 
Size: 5028 Color: 3
Size: 2424 Color: 2

Bin 109: 20 of cap free
Amount of items: 2
Items: 
Size: 6219 Color: 0
Size: 1233 Color: 2

Bin 110: 21 of cap free
Amount of items: 3
Items: 
Size: 3780 Color: 4
Size: 3111 Color: 1
Size: 560 Color: 2

Bin 111: 21 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 0
Size: 2699 Color: 2
Size: 220 Color: 4

Bin 112: 23 of cap free
Amount of items: 2
Items: 
Size: 5515 Color: 1
Size: 1934 Color: 2

Bin 113: 24 of cap free
Amount of items: 2
Items: 
Size: 6308 Color: 1
Size: 1140 Color: 4

Bin 114: 24 of cap free
Amount of items: 2
Items: 
Size: 6446 Color: 1
Size: 1002 Color: 3

Bin 115: 26 of cap free
Amount of items: 3
Items: 
Size: 5612 Color: 0
Size: 1214 Color: 4
Size: 620 Color: 4

Bin 116: 31 of cap free
Amount of items: 2
Items: 
Size: 6116 Color: 1
Size: 1325 Color: 3

Bin 117: 32 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 1
Size: 1172 Color: 4

Bin 118: 33 of cap free
Amount of items: 2
Items: 
Size: 6195 Color: 1
Size: 1244 Color: 3

Bin 119: 39 of cap free
Amount of items: 2
Items: 
Size: 6447 Color: 3
Size: 986 Color: 1

Bin 120: 47 of cap free
Amount of items: 2
Items: 
Size: 5501 Color: 2
Size: 1924 Color: 0

Bin 121: 49 of cap free
Amount of items: 2
Items: 
Size: 5362 Color: 3
Size: 2061 Color: 2

Bin 122: 52 of cap free
Amount of items: 2
Items: 
Size: 3740 Color: 0
Size: 3680 Color: 3

Bin 123: 59 of cap free
Amount of items: 2
Items: 
Size: 5630 Color: 3
Size: 1783 Color: 4

Bin 124: 67 of cap free
Amount of items: 2
Items: 
Size: 5265 Color: 3
Size: 2140 Color: 1

Bin 125: 69 of cap free
Amount of items: 2
Items: 
Size: 4290 Color: 2
Size: 3113 Color: 4

Bin 126: 69 of cap free
Amount of items: 2
Items: 
Size: 5804 Color: 1
Size: 1599 Color: 3

Bin 127: 89 of cap free
Amount of items: 2
Items: 
Size: 4219 Color: 3
Size: 3164 Color: 1

Bin 128: 100 of cap free
Amount of items: 25
Items: 
Size: 424 Color: 0
Size: 416 Color: 4
Size: 376 Color: 4
Size: 368 Color: 4
Size: 356 Color: 0
Size: 336 Color: 4
Size: 328 Color: 2
Size: 326 Color: 3
Size: 318 Color: 2
Size: 316 Color: 0
Size: 304 Color: 1
Size: 304 Color: 1
Size: 288 Color: 4
Size: 272 Color: 4
Size: 272 Color: 3
Size: 272 Color: 2
Size: 264 Color: 2
Size: 248 Color: 1
Size: 246 Color: 3
Size: 246 Color: 2
Size: 244 Color: 1
Size: 240 Color: 1
Size: 220 Color: 0
Size: 196 Color: 3
Size: 192 Color: 1

Bin 129: 107 of cap free
Amount of items: 2
Items: 
Size: 4251 Color: 1
Size: 3114 Color: 2

Bin 130: 110 of cap free
Amount of items: 2
Items: 
Size: 5001 Color: 4
Size: 2361 Color: 2

Bin 131: 112 of cap free
Amount of items: 2
Items: 
Size: 4908 Color: 3
Size: 2452 Color: 4

Bin 132: 122 of cap free
Amount of items: 2
Items: 
Size: 4599 Color: 4
Size: 2751 Color: 3

Bin 133: 5706 of cap free
Amount of items: 10
Items: 
Size: 216 Color: 4
Size: 192 Color: 2
Size: 192 Color: 1
Size: 190 Color: 3
Size: 186 Color: 2
Size: 186 Color: 1
Size: 180 Color: 0
Size: 160 Color: 3
Size: 136 Color: 2
Size: 128 Color: 3

Total size: 986304
Total free space: 7472

