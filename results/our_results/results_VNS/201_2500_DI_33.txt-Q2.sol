Capicity Bin: 2428
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1263 Color: 0
Size: 975 Color: 1
Size: 190 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 1
Size: 951 Color: 0
Size: 190 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 0
Size: 1011 Color: 1
Size: 126 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 1
Size: 791 Color: 1
Size: 130 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1663 Color: 0
Size: 655 Color: 1
Size: 78 Color: 1
Size: 32 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 1
Size: 702 Color: 0
Size: 60 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 0
Size: 670 Color: 1
Size: 60 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 0
Size: 523 Color: 0
Size: 104 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1834 Color: 0
Size: 530 Color: 0
Size: 64 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 1
Size: 386 Color: 0
Size: 64 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 1
Size: 371 Color: 0
Size: 78 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1987 Color: 0
Size: 369 Color: 0
Size: 72 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 1
Size: 391 Color: 0
Size: 54 Color: 1

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 1
Size: 937 Color: 1
Size: 200 Color: 0

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 0
Size: 769 Color: 1
Size: 72 Color: 0

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 0
Size: 609 Color: 0
Size: 24 Color: 1

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 1798 Color: 1
Size: 629 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 1
Size: 494 Color: 0
Size: 130 Color: 1

Bin 19: 1 of cap free
Amount of items: 4
Items: 
Size: 1838 Color: 0
Size: 271 Color: 1
Size: 246 Color: 1
Size: 72 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1889 Color: 1
Size: 434 Color: 1
Size: 104 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 1
Size: 307 Color: 0
Size: 154 Color: 1

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 1
Size: 453 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 0
Size: 375 Color: 1
Size: 96 Color: 0

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 2083 Color: 0
Size: 344 Color: 1

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 2102 Color: 0
Size: 325 Color: 1

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 2135 Color: 1
Size: 292 Color: 0

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 1
Size: 836 Color: 1
Size: 48 Color: 0

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 1
Size: 659 Color: 0
Size: 92 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1959 Color: 0
Size: 273 Color: 0
Size: 194 Color: 1

Bin 30: 3 of cap free
Amount of items: 9
Items: 
Size: 631 Color: 0
Size: 526 Color: 0
Size: 522 Color: 0
Size: 202 Color: 0
Size: 160 Color: 1
Size: 154 Color: 1
Size: 154 Color: 1
Size: 40 Color: 1
Size: 36 Color: 0

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1283 Color: 1
Size: 1010 Color: 1
Size: 132 Color: 0

Bin 32: 3 of cap free
Amount of items: 4
Items: 
Size: 1667 Color: 0
Size: 662 Color: 1
Size: 52 Color: 1
Size: 44 Color: 0

Bin 33: 4 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 1
Size: 378 Color: 1
Size: 76 Color: 0

Bin 34: 5 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 639 Color: 0
Size: 126 Color: 1

Bin 35: 6 of cap free
Amount of items: 3
Items: 
Size: 1499 Color: 0
Size: 771 Color: 1
Size: 152 Color: 0

Bin 36: 7 of cap free
Amount of items: 2
Items: 
Size: 2026 Color: 0
Size: 395 Color: 1

Bin 37: 7 of cap free
Amount of items: 2
Items: 
Size: 2039 Color: 0
Size: 382 Color: 1

Bin 38: 8 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 1
Size: 638 Color: 0
Size: 156 Color: 0

Bin 39: 9 of cap free
Amount of items: 4
Items: 
Size: 2162 Color: 1
Size: 245 Color: 0
Size: 8 Color: 0
Size: 4 Color: 1

Bin 40: 10 of cap free
Amount of items: 2
Items: 
Size: 1639 Color: 1
Size: 779 Color: 0

Bin 41: 10 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 0
Size: 775 Color: 1

Bin 42: 11 of cap free
Amount of items: 4
Items: 
Size: 1215 Color: 0
Size: 950 Color: 0
Size: 208 Color: 1
Size: 44 Color: 1

Bin 43: 13 of cap free
Amount of items: 2
Items: 
Size: 1219 Color: 1
Size: 1196 Color: 0

Bin 44: 13 of cap free
Amount of items: 2
Items: 
Size: 2105 Color: 1
Size: 310 Color: 0

Bin 45: 14 of cap free
Amount of items: 2
Items: 
Size: 1459 Color: 1
Size: 955 Color: 0

Bin 46: 14 of cap free
Amount of items: 2
Items: 
Size: 1779 Color: 0
Size: 635 Color: 1

Bin 47: 15 of cap free
Amount of items: 3
Items: 
Size: 1495 Color: 1
Size: 814 Color: 0
Size: 104 Color: 1

Bin 48: 15 of cap free
Amount of items: 2
Items: 
Size: 1671 Color: 0
Size: 742 Color: 1

Bin 49: 16 of cap free
Amount of items: 2
Items: 
Size: 1802 Color: 1
Size: 610 Color: 0

Bin 50: 17 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 1
Size: 971 Color: 0
Size: 222 Color: 1

Bin 51: 18 of cap free
Amount of items: 2
Items: 
Size: 1350 Color: 1
Size: 1060 Color: 0

Bin 52: 18 of cap free
Amount of items: 2
Items: 
Size: 2101 Color: 0
Size: 309 Color: 1

Bin 53: 20 of cap free
Amount of items: 2
Items: 
Size: 1259 Color: 1
Size: 1149 Color: 0

Bin 54: 20 of cap free
Amount of items: 2
Items: 
Size: 1910 Color: 0
Size: 498 Color: 1

Bin 55: 20 of cap free
Amount of items: 2
Items: 
Size: 2134 Color: 1
Size: 274 Color: 0

Bin 56: 22 of cap free
Amount of items: 2
Items: 
Size: 1885 Color: 0
Size: 521 Color: 1

Bin 57: 22 of cap free
Amount of items: 2
Items: 
Size: 1955 Color: 0
Size: 451 Color: 1

Bin 58: 23 of cap free
Amount of items: 2
Items: 
Size: 1503 Color: 0
Size: 902 Color: 1

Bin 59: 24 of cap free
Amount of items: 2
Items: 
Size: 2022 Color: 0
Size: 382 Color: 1

Bin 60: 24 of cap free
Amount of items: 2
Items: 
Size: 2062 Color: 1
Size: 342 Color: 0

Bin 61: 25 of cap free
Amount of items: 2
Items: 
Size: 1454 Color: 0
Size: 949 Color: 1

Bin 62: 26 of cap free
Amount of items: 19
Items: 
Size: 194 Color: 0
Size: 188 Color: 0
Size: 188 Color: 0
Size: 176 Color: 0
Size: 152 Color: 1
Size: 144 Color: 0
Size: 140 Color: 1
Size: 126 Color: 1
Size: 124 Color: 1
Size: 124 Color: 1
Size: 120 Color: 1
Size: 108 Color: 0
Size: 104 Color: 1
Size: 104 Color: 0
Size: 104 Color: 0
Size: 96 Color: 1
Size: 90 Color: 1
Size: 88 Color: 0
Size: 32 Color: 1

Bin 63: 28 of cap free
Amount of items: 2
Items: 
Size: 2094 Color: 0
Size: 306 Color: 1

Bin 64: 29 of cap free
Amount of items: 2
Items: 
Size: 2061 Color: 1
Size: 338 Color: 0

Bin 65: 31 of cap free
Amount of items: 2
Items: 
Size: 1481 Color: 1
Size: 916 Color: 0

Bin 66: 1856 of cap free
Amount of items: 9
Items: 
Size: 84 Color: 1
Size: 76 Color: 1
Size: 74 Color: 0
Size: 74 Color: 0
Size: 64 Color: 1
Size: 60 Color: 1
Size: 52 Color: 0
Size: 48 Color: 0
Size: 40 Color: 0

Total size: 157820
Total free space: 2428

