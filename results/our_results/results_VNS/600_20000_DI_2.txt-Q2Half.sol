Capicity Bin: 15744
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 1
Size: 2356 Color: 1
Size: 464 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 13149 Color: 1
Size: 2163 Color: 1
Size: 432 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10712 Color: 1
Size: 4664 Color: 1
Size: 368 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7885 Color: 1
Size: 6551 Color: 1
Size: 1308 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12432 Color: 1
Size: 2980 Color: 1
Size: 332 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 1
Size: 2040 Color: 1
Size: 368 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12034 Color: 1
Size: 3242 Color: 1
Size: 468 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 1
Size: 2090 Color: 1
Size: 416 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8945 Color: 1
Size: 5667 Color: 1
Size: 1132 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 1
Size: 1448 Color: 1
Size: 628 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11976 Color: 1
Size: 3096 Color: 1
Size: 672 Color: 0

Bin 12: 0 of cap free
Amount of items: 4
Items: 
Size: 12690 Color: 1
Size: 1922 Color: 1
Size: 604 Color: 0
Size: 528 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11694 Color: 1
Size: 3186 Color: 1
Size: 864 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11327 Color: 1
Size: 3681 Color: 1
Size: 736 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 9296 Color: 1
Size: 5392 Color: 1
Size: 1056 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11432 Color: 1
Size: 3688 Color: 1
Size: 624 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12662 Color: 1
Size: 1978 Color: 1
Size: 1104 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13108 Color: 1
Size: 2028 Color: 1
Size: 608 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7881 Color: 1
Size: 6553 Color: 1
Size: 1310 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8982 Color: 1
Size: 5638 Color: 1
Size: 1124 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7880 Color: 1
Size: 6568 Color: 1
Size: 1296 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11954 Color: 1
Size: 2482 Color: 1
Size: 1308 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14088 Color: 1
Size: 1384 Color: 1
Size: 272 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14064 Color: 1
Size: 1648 Color: 1
Size: 32 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7884 Color: 1
Size: 6556 Color: 1
Size: 1304 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8952 Color: 1
Size: 5936 Color: 1
Size: 856 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13936 Color: 1
Size: 1360 Color: 1
Size: 448 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12852 Color: 1
Size: 2188 Color: 1
Size: 704 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 1
Size: 1936 Color: 1
Size: 344 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7900 Color: 1
Size: 6540 Color: 1
Size: 1304 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7877 Color: 1
Size: 6557 Color: 1
Size: 1310 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13932 Color: 1
Size: 1468 Color: 1
Size: 344 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12922 Color: 1
Size: 2022 Color: 1
Size: 800 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 10587 Color: 1
Size: 4299 Color: 1
Size: 858 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 9855 Color: 1
Size: 4909 Color: 1
Size: 980 Color: 0

Bin 36: 0 of cap free
Amount of items: 4
Items: 
Size: 13424 Color: 1
Size: 1424 Color: 1
Size: 640 Color: 0
Size: 256 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 1
Size: 1732 Color: 1
Size: 24 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 1
Size: 1336 Color: 1
Size: 288 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7888 Color: 1
Size: 6576 Color: 1
Size: 1280 Color: 0

Bin 40: 0 of cap free
Amount of items: 4
Items: 
Size: 13124 Color: 1
Size: 1644 Color: 1
Size: 576 Color: 0
Size: 400 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13315 Color: 1
Size: 1953 Color: 1
Size: 476 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 11832 Color: 1
Size: 3272 Color: 1
Size: 640 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13340 Color: 1
Size: 2004 Color: 1
Size: 400 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 10604 Color: 1
Size: 4282 Color: 1
Size: 858 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 1
Size: 2536 Color: 1
Size: 576 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14000 Color: 1
Size: 1456 Color: 1
Size: 288 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 1
Size: 1708 Color: 1
Size: 336 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 11944 Color: 1
Size: 3146 Color: 1
Size: 654 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12224 Color: 1
Size: 2768 Color: 1
Size: 752 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 10856 Color: 1
Size: 3608 Color: 1
Size: 1280 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 7890 Color: 1
Size: 6546 Color: 1
Size: 1308 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12893 Color: 1
Size: 2377 Color: 1
Size: 474 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13804 Color: 1
Size: 1548 Color: 1
Size: 392 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13304 Color: 1
Size: 2040 Color: 1
Size: 400 Color: 0

Bin 55: 0 of cap free
Amount of items: 5
Items: 
Size: 11922 Color: 1
Size: 1682 Color: 1
Size: 1324 Color: 1
Size: 752 Color: 0
Size: 64 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 9976 Color: 1
Size: 5344 Color: 1
Size: 424 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12752 Color: 1
Size: 2576 Color: 1
Size: 416 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 1
Size: 1752 Color: 1
Size: 304 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 10603 Color: 1
Size: 4285 Color: 1
Size: 856 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 8998 Color: 1
Size: 5622 Color: 1
Size: 1124 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 1
Size: 3308 Color: 1
Size: 656 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 12500 Color: 1
Size: 2652 Color: 1
Size: 592 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 9829 Color: 1
Size: 4931 Color: 1
Size: 984 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 1
Size: 1388 Color: 1
Size: 464 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 1
Size: 1578 Color: 1
Size: 312 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 1
Size: 1896 Color: 1
Size: 288 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 12400 Color: 1
Size: 2488 Color: 1
Size: 856 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 8988 Color: 1
Size: 5636 Color: 1
Size: 1120 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 11805 Color: 1
Size: 3275 Color: 1
Size: 664 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 12906 Color: 1
Size: 2570 Color: 1
Size: 268 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 12285 Color: 1
Size: 3107 Color: 1
Size: 352 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 1
Size: 1632 Color: 1
Size: 296 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 14152 Color: 1
Size: 1368 Color: 1
Size: 224 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 11831 Color: 1
Size: 2185 Color: 1
Size: 1728 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 11498 Color: 1
Size: 3542 Color: 1
Size: 704 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13322 Color: 1
Size: 1802 Color: 1
Size: 620 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7898 Color: 1
Size: 6542 Color: 1
Size: 1304 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13442 Color: 1
Size: 1358 Color: 1
Size: 944 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 1
Size: 1912 Color: 1
Size: 1056 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 9840 Color: 1
Size: 5628 Color: 1
Size: 276 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 1
Size: 1774 Color: 1
Size: 352 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 1
Size: 2008 Color: 1
Size: 336 Color: 0

Bin 83: 0 of cap free
Amount of items: 7
Items: 
Size: 4334 Color: 1
Size: 4276 Color: 1
Size: 3094 Color: 1
Size: 2944 Color: 1
Size: 552 Color: 0
Size: 288 Color: 0
Size: 256 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 7882 Color: 1
Size: 6554 Color: 1
Size: 1308 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 8996 Color: 1
Size: 6456 Color: 1
Size: 292 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 12946 Color: 1
Size: 2362 Color: 1
Size: 436 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7932 Color: 1
Size: 6516 Color: 1
Size: 1296 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13476 Color: 1
Size: 1688 Color: 1
Size: 580 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 9006 Color: 1
Size: 5618 Color: 1
Size: 1120 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 1
Size: 1512 Color: 1
Size: 512 Color: 0

Bin 91: 0 of cap free
Amount of items: 4
Items: 
Size: 10576 Color: 1
Size: 3504 Color: 1
Size: 1120 Color: 0
Size: 544 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 12617 Color: 1
Size: 2607 Color: 1
Size: 520 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 12382 Color: 1
Size: 2802 Color: 1
Size: 560 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 1
Size: 2852 Color: 1
Size: 328 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 14008 Color: 1
Size: 1120 Color: 1
Size: 616 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 11819 Color: 1
Size: 3271 Color: 1
Size: 654 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 12235 Color: 1
Size: 2925 Color: 1
Size: 584 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 1
Size: 2590 Color: 1
Size: 1152 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 8008 Color: 1
Size: 6888 Color: 1
Size: 848 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 1
Size: 3208 Color: 1
Size: 160 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 1
Size: 2394 Color: 1
Size: 1310 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 11182 Color: 1
Size: 4310 Color: 1
Size: 252 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 9096 Color: 1
Size: 5672 Color: 1
Size: 976 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 1
Size: 2248 Color: 1
Size: 264 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 1
Size: 3076 Color: 1
Size: 756 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 11732 Color: 1
Size: 2708 Color: 1
Size: 1304 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 13508 Color: 1
Size: 1832 Color: 1
Size: 404 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 1
Size: 2600 Color: 1
Size: 400 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 10606 Color: 1
Size: 3378 Color: 1
Size: 1760 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 13148 Color: 1
Size: 1764 Color: 1
Size: 832 Color: 0

Bin 111: 0 of cap free
Amount of items: 5
Items: 
Size: 10620 Color: 1
Size: 2288 Color: 1
Size: 1572 Color: 1
Size: 800 Color: 0
Size: 464 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 9846 Color: 1
Size: 4918 Color: 1
Size: 980 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 12212 Color: 1
Size: 3176 Color: 1
Size: 356 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 1
Size: 1516 Color: 1
Size: 284 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 11194 Color: 1
Size: 3966 Color: 1
Size: 584 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 11320 Color: 1
Size: 3772 Color: 1
Size: 652 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 1
Size: 3144 Color: 1
Size: 356 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 1
Size: 4900 Color: 1
Size: 976 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 1
Size: 2096 Color: 1
Size: 376 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 1
Size: 3296 Color: 1
Size: 432 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 13772 Color: 1
Size: 1620 Color: 1
Size: 352 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 13130 Color: 1
Size: 2182 Color: 1
Size: 432 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 11748 Color: 1
Size: 3492 Color: 1
Size: 504 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 1
Size: 3824 Color: 1
Size: 448 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 10896 Color: 1
Size: 4088 Color: 1
Size: 760 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 11315 Color: 1
Size: 3691 Color: 1
Size: 738 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 11970 Color: 1
Size: 2814 Color: 1
Size: 960 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 12914 Color: 1
Size: 2334 Color: 1
Size: 496 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 12855 Color: 1
Size: 2409 Color: 1
Size: 480 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 9878 Color: 1
Size: 4890 Color: 1
Size: 976 Color: 0

Bin 131: 0 of cap free
Amount of items: 4
Items: 
Size: 9856 Color: 1
Size: 4808 Color: 1
Size: 568 Color: 0
Size: 512 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 13616 Color: 1
Size: 1776 Color: 1
Size: 352 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 1
Size: 5686 Color: 1
Size: 738 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 13474 Color: 1
Size: 1730 Color: 1
Size: 540 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 13405 Color: 1
Size: 1951 Color: 1
Size: 388 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 13710 Color: 1
Size: 2022 Color: 1
Size: 12 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 8990 Color: 1
Size: 5630 Color: 1
Size: 1124 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 11220 Color: 1
Size: 3542 Color: 1
Size: 982 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 1
Size: 1892 Color: 1
Size: 536 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 1
Size: 4048 Color: 1
Size: 468 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 1
Size: 2504 Color: 1
Size: 528 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 1
Size: 2948 Color: 1
Size: 376 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 7876 Color: 1
Size: 6668 Color: 1
Size: 1200 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 13886 Color: 1
Size: 1474 Color: 1
Size: 384 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 8971 Color: 1
Size: 5645 Color: 1
Size: 1128 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 11888 Color: 1
Size: 3120 Color: 1
Size: 736 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 13374 Color: 1
Size: 1922 Color: 1
Size: 448 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 8950 Color: 1
Size: 5662 Color: 1
Size: 1132 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 1
Size: 2264 Color: 1
Size: 432 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 11184 Color: 1
Size: 3632 Color: 1
Size: 928 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 12682 Color: 1
Size: 2645 Color: 1
Size: 416 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 13401 Color: 1
Size: 1894 Color: 1
Size: 448 Color: 0

Bin 153: 1 of cap free
Amount of items: 5
Items: 
Size: 10152 Color: 1
Size: 2412 Color: 1
Size: 2351 Color: 1
Size: 508 Color: 0
Size: 320 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 12613 Color: 1
Size: 2554 Color: 1
Size: 576 Color: 0

Bin 155: 1 of cap free
Amount of items: 5
Items: 
Size: 7916 Color: 1
Size: 3616 Color: 1
Size: 3261 Color: 1
Size: 560 Color: 0
Size: 390 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 9851 Color: 1
Size: 5544 Color: 1
Size: 348 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 12172 Color: 1
Size: 3283 Color: 1
Size: 288 Color: 0

Bin 158: 1 of cap free
Amount of items: 3
Items: 
Size: 5888 Color: 0
Size: 4944 Color: 1
Size: 4911 Color: 1

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 13008 Color: 1
Size: 2354 Color: 1
Size: 380 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 10574 Color: 1
Size: 4336 Color: 1
Size: 832 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 13824 Color: 1
Size: 1558 Color: 1
Size: 360 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 1
Size: 1382 Color: 1
Size: 584 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 10546 Color: 1
Size: 4892 Color: 1
Size: 304 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 13570 Color: 1
Size: 1868 Color: 1
Size: 304 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 13636 Color: 1
Size: 1786 Color: 1
Size: 320 Color: 0

Bin 166: 3 of cap free
Amount of items: 3
Items: 
Size: 12571 Color: 1
Size: 2434 Color: 1
Size: 736 Color: 0

Bin 167: 3 of cap free
Amount of items: 3
Items: 
Size: 11201 Color: 1
Size: 4284 Color: 1
Size: 256 Color: 0

Bin 168: 4 of cap free
Amount of items: 3
Items: 
Size: 13990 Color: 1
Size: 1438 Color: 1
Size: 312 Color: 0

Bin 169: 4 of cap free
Amount of items: 3
Items: 
Size: 9876 Color: 1
Size: 5368 Color: 1
Size: 496 Color: 0

Bin 170: 4 of cap free
Amount of items: 3
Items: 
Size: 8656 Color: 1
Size: 6524 Color: 1
Size: 560 Color: 0

Bin 171: 4 of cap free
Amount of items: 3
Items: 
Size: 8922 Color: 1
Size: 6562 Color: 1
Size: 256 Color: 0

Bin 172: 5 of cap free
Amount of items: 3
Items: 
Size: 13123 Color: 1
Size: 2072 Color: 1
Size: 544 Color: 0

Bin 173: 5 of cap free
Amount of items: 3
Items: 
Size: 11815 Color: 1
Size: 3348 Color: 1
Size: 576 Color: 0

Bin 174: 5 of cap free
Amount of items: 5
Items: 
Size: 7912 Color: 1
Size: 4295 Color: 1
Size: 2808 Color: 1
Size: 468 Color: 0
Size: 256 Color: 0

Bin 175: 6 of cap free
Amount of items: 3
Items: 
Size: 12874 Color: 1
Size: 2800 Color: 1
Size: 64 Color: 0

Bin 176: 6 of cap free
Amount of items: 3
Items: 
Size: 14022 Color: 1
Size: 1520 Color: 1
Size: 196 Color: 0

Bin 177: 7 of cap free
Amount of items: 3
Items: 
Size: 11311 Color: 1
Size: 3802 Color: 1
Size: 624 Color: 0

Bin 178: 7 of cap free
Amount of items: 3
Items: 
Size: 12925 Color: 1
Size: 2412 Color: 1
Size: 400 Color: 0

Bin 179: 7 of cap free
Amount of items: 3
Items: 
Size: 11662 Color: 1
Size: 3787 Color: 1
Size: 288 Color: 0

Bin 180: 8 of cap free
Amount of items: 3
Items: 
Size: 10591 Color: 1
Size: 4185 Color: 1
Size: 960 Color: 0

Bin 181: 8 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 1
Size: 2432 Color: 1
Size: 272 Color: 0

Bin 182: 8 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 1
Size: 4200 Color: 1
Size: 624 Color: 0

Bin 183: 9 of cap free
Amount of items: 3
Items: 
Size: 13582 Color: 1
Size: 2025 Color: 1
Size: 128 Color: 0

Bin 184: 10 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 1
Size: 3122 Color: 1
Size: 288 Color: 0

Bin 185: 11 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 1
Size: 2883 Color: 1
Size: 480 Color: 0

Bin 186: 14 of cap free
Amount of items: 3
Items: 
Size: 13730 Color: 1
Size: 1584 Color: 1
Size: 416 Color: 0

Bin 187: 30 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 1
Size: 2164 Color: 1
Size: 400 Color: 0

Bin 188: 63 of cap free
Amount of items: 5
Items: 
Size: 7874 Color: 1
Size: 3695 Color: 1
Size: 2772 Color: 1
Size: 860 Color: 0
Size: 480 Color: 0

Bin 189: 113 of cap free
Amount of items: 3
Items: 
Size: 11211 Color: 1
Size: 3764 Color: 1
Size: 656 Color: 0

Bin 190: 174 of cap free
Amount of items: 3
Items: 
Size: 7873 Color: 1
Size: 6561 Color: 1
Size: 1136 Color: 0

Bin 191: 980 of cap free
Amount of items: 2
Items: 
Size: 14156 Color: 1
Size: 608 Color: 0

Bin 192: 1616 of cap free
Amount of items: 1
Items: 
Size: 14128 Color: 1

Bin 193: 1626 of cap free
Amount of items: 1
Items: 
Size: 14118 Color: 1

Bin 194: 1658 of cap free
Amount of items: 1
Items: 
Size: 14086 Color: 1

Bin 195: 1660 of cap free
Amount of items: 1
Items: 
Size: 14084 Color: 1

Bin 196: 1766 of cap free
Amount of items: 1
Items: 
Size: 13978 Color: 1

Bin 197: 1872 of cap free
Amount of items: 1
Items: 
Size: 13872 Color: 1

Bin 198: 1884 of cap free
Amount of items: 1
Items: 
Size: 13860 Color: 1

Bin 199: 2142 of cap free
Amount of items: 1
Items: 
Size: 13602 Color: 1

Total size: 3117312
Total free space: 15744

