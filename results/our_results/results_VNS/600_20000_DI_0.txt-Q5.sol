Capicity Bin: 16256
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 21
Items: 
Size: 928 Color: 4
Size: 924 Color: 1
Size: 916 Color: 3
Size: 912 Color: 0
Size: 908 Color: 0
Size: 904 Color: 4
Size: 896 Color: 4
Size: 896 Color: 0
Size: 856 Color: 0
Size: 856 Color: 0
Size: 832 Color: 2
Size: 816 Color: 3
Size: 816 Color: 1
Size: 812 Color: 3
Size: 788 Color: 0
Size: 784 Color: 3
Size: 544 Color: 2
Size: 544 Color: 2
Size: 532 Color: 2
Size: 424 Color: 4
Size: 368 Color: 2

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 8133 Color: 3
Size: 3336 Color: 1
Size: 2927 Color: 1
Size: 1588 Color: 3
Size: 272 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9142 Color: 4
Size: 6764 Color: 0
Size: 350 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 2
Size: 6568 Color: 0
Size: 352 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11513 Color: 2
Size: 3953 Color: 1
Size: 790 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11736 Color: 1
Size: 4200 Color: 0
Size: 320 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11756 Color: 0
Size: 4076 Color: 1
Size: 424 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 1
Size: 2648 Color: 2
Size: 1660 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 4
Size: 3752 Color: 1
Size: 480 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 2
Size: 2630 Color: 1
Size: 1544 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12114 Color: 3
Size: 3454 Color: 1
Size: 688 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12135 Color: 1
Size: 3429 Color: 3
Size: 692 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12131 Color: 3
Size: 3435 Color: 1
Size: 690 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 4
Size: 3528 Color: 1
Size: 272 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12596 Color: 1
Size: 3172 Color: 3
Size: 488 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12559 Color: 3
Size: 3081 Color: 3
Size: 616 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 1
Size: 3176 Color: 3
Size: 432 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13006 Color: 1
Size: 2542 Color: 2
Size: 708 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13014 Color: 1
Size: 2058 Color: 2
Size: 1184 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12960 Color: 2
Size: 2448 Color: 1
Size: 848 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13096 Color: 3
Size: 2568 Color: 2
Size: 592 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13264 Color: 1
Size: 1516 Color: 3
Size: 1476 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 2
Size: 1948 Color: 1
Size: 1040 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13279 Color: 2
Size: 2481 Color: 1
Size: 496 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 4
Size: 2622 Color: 0
Size: 310 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13278 Color: 1
Size: 1906 Color: 2
Size: 1072 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 1
Size: 2476 Color: 4
Size: 412 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 2
Size: 2356 Color: 1
Size: 464 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13430 Color: 1
Size: 2702 Color: 0
Size: 124 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13507 Color: 2
Size: 2129 Color: 2
Size: 620 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 1
Size: 1688 Color: 3
Size: 928 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13650 Color: 2
Size: 2174 Color: 3
Size: 432 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13686 Color: 3
Size: 2280 Color: 0
Size: 290 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13782 Color: 3
Size: 1434 Color: 2
Size: 1040 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13852 Color: 1
Size: 1864 Color: 2
Size: 540 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13966 Color: 1
Size: 1930 Color: 0
Size: 360 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13923 Color: 4
Size: 1945 Color: 4
Size: 388 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 4
Size: 1944 Color: 2
Size: 320 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14009 Color: 0
Size: 1545 Color: 1
Size: 702 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 2
Size: 1352 Color: 1
Size: 864 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 1
Size: 1168 Color: 4
Size: 1020 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14078 Color: 3
Size: 1746 Color: 2
Size: 432 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14089 Color: 4
Size: 1755 Color: 2
Size: 412 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14152 Color: 2
Size: 1352 Color: 3
Size: 752 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 1
Size: 1558 Color: 0
Size: 488 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14212 Color: 1
Size: 1352 Color: 1
Size: 692 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14235 Color: 1
Size: 1481 Color: 3
Size: 540 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14242 Color: 3
Size: 1022 Color: 3
Size: 992 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14266 Color: 4
Size: 1706 Color: 1
Size: 284 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14339 Color: 1
Size: 1459 Color: 0
Size: 458 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 3
Size: 1184 Color: 4
Size: 744 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14354 Color: 4
Size: 1582 Color: 2
Size: 320 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 2
Size: 1564 Color: 0
Size: 304 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14403 Color: 1
Size: 1389 Color: 1
Size: 464 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14494 Color: 1
Size: 1160 Color: 3
Size: 602 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14492 Color: 4
Size: 1400 Color: 1
Size: 364 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 4
Size: 1344 Color: 2
Size: 380 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14538 Color: 3
Size: 1354 Color: 4
Size: 364 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14572 Color: 3
Size: 1172 Color: 3
Size: 512 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 2
Size: 1296 Color: 4
Size: 348 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14618 Color: 3
Size: 1366 Color: 0
Size: 272 Color: 2

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 10123 Color: 4
Size: 5844 Color: 2
Size: 288 Color: 0

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10127 Color: 2
Size: 5832 Color: 4
Size: 296 Color: 0

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 10810 Color: 1
Size: 5109 Color: 2
Size: 336 Color: 1

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 10879 Color: 2
Size: 4984 Color: 1
Size: 392 Color: 0

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 0
Size: 3947 Color: 3
Size: 280 Color: 3

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12143 Color: 3
Size: 3204 Color: 1
Size: 908 Color: 4

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12452 Color: 1
Size: 3451 Color: 4
Size: 352 Color: 4

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13061 Color: 0
Size: 1822 Color: 3
Size: 1372 Color: 1

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13267 Color: 1
Size: 2492 Color: 3
Size: 496 Color: 4

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13515 Color: 2
Size: 2380 Color: 1
Size: 360 Color: 3

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13705 Color: 3
Size: 2358 Color: 1
Size: 192 Color: 2

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13716 Color: 4
Size: 1379 Color: 1
Size: 1160 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13863 Color: 0
Size: 2296 Color: 1
Size: 96 Color: 2

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 14387 Color: 4
Size: 1344 Color: 2
Size: 524 Color: 0

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 14507 Color: 1
Size: 1444 Color: 0
Size: 304 Color: 2

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 10786 Color: 1
Size: 4988 Color: 2
Size: 480 Color: 4

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 10802 Color: 4
Size: 5044 Color: 1
Size: 408 Color: 4

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11148 Color: 2
Size: 4316 Color: 1
Size: 790 Color: 0

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 0
Size: 4542 Color: 0
Size: 488 Color: 1

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 11370 Color: 1
Size: 4562 Color: 4
Size: 322 Color: 2

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 1
Size: 3756 Color: 0
Size: 496 Color: 3

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 1
Size: 2062 Color: 2
Size: 1546 Color: 2

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 12745 Color: 1
Size: 3001 Color: 4
Size: 508 Color: 2

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 1
Size: 3010 Color: 4
Size: 256 Color: 3

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 12987 Color: 3
Size: 2667 Color: 1
Size: 600 Color: 3

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 13110 Color: 3
Size: 2808 Color: 1
Size: 336 Color: 4

Bin 88: 2 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 2
Size: 2330 Color: 3

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 2
Size: 1818 Color: 2
Size: 508 Color: 1

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 11521 Color: 3
Size: 4380 Color: 1
Size: 352 Color: 0

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 12035 Color: 1
Size: 3942 Color: 0
Size: 276 Color: 0

Bin 92: 3 of cap free
Amount of items: 2
Items: 
Size: 12657 Color: 3
Size: 3596 Color: 2

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 13528 Color: 4
Size: 2725 Color: 3

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 13703 Color: 4
Size: 2550 Color: 3

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 12115 Color: 0
Size: 3961 Color: 3
Size: 176 Color: 0

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 14248 Color: 3
Size: 2004 Color: 4

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 14408 Color: 0
Size: 1844 Color: 4

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 1
Size: 6773 Color: 0
Size: 686 Color: 3

Bin 99: 5 of cap free
Amount of items: 3
Items: 
Size: 11084 Color: 1
Size: 4481 Color: 3
Size: 686 Color: 2

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 11505 Color: 2
Size: 3546 Color: 1
Size: 1200 Color: 3

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 1
Size: 3439 Color: 3
Size: 392 Color: 3

Bin 102: 5 of cap free
Amount of items: 3
Items: 
Size: 12392 Color: 0
Size: 3519 Color: 4
Size: 340 Color: 2

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 3
Size: 2359 Color: 2

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 14444 Color: 2
Size: 1807 Color: 4

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 10204 Color: 4
Size: 5772 Color: 2
Size: 274 Color: 0

Bin 106: 6 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 3
Size: 3772 Color: 1
Size: 1830 Color: 2

Bin 107: 6 of cap free
Amount of items: 3
Items: 
Size: 10706 Color: 4
Size: 5144 Color: 3
Size: 400 Color: 4

Bin 108: 6 of cap free
Amount of items: 3
Items: 
Size: 11048 Color: 4
Size: 4546 Color: 1
Size: 656 Color: 0

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 13198 Color: 3
Size: 3052 Color: 4

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 14162 Color: 2
Size: 2088 Color: 4

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 10678 Color: 1
Size: 5115 Color: 1
Size: 456 Color: 2

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 13325 Color: 4
Size: 2662 Color: 0
Size: 262 Color: 4

Bin 113: 7 of cap free
Amount of items: 2
Items: 
Size: 13586 Color: 4
Size: 2663 Color: 0

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 10160 Color: 4
Size: 6088 Color: 1

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 2
Size: 2482 Color: 4

Bin 116: 8 of cap free
Amount of items: 3
Items: 
Size: 14606 Color: 3
Size: 1608 Color: 4
Size: 34 Color: 4

Bin 117: 9 of cap free
Amount of items: 3
Items: 
Size: 12590 Color: 1
Size: 1972 Color: 4
Size: 1685 Color: 4

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 14479 Color: 2
Size: 1768 Color: 0

Bin 119: 10 of cap free
Amount of items: 3
Items: 
Size: 9448 Color: 2
Size: 6008 Color: 0
Size: 790 Color: 2

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 14062 Color: 0
Size: 2184 Color: 3

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 14268 Color: 4
Size: 1978 Color: 2

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 4
Size: 1662 Color: 3

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 14591 Color: 0
Size: 1655 Color: 2

Bin 124: 11 of cap free
Amount of items: 3
Items: 
Size: 10088 Color: 0
Size: 5869 Color: 3
Size: 288 Color: 3

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 10877 Color: 3
Size: 5368 Color: 4

Bin 126: 12 of cap free
Amount of items: 3
Items: 
Size: 9244 Color: 1
Size: 6616 Color: 4
Size: 384 Color: 0

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 10244 Color: 4
Size: 6000 Color: 2

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 2
Size: 2444 Color: 0

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 14440 Color: 0
Size: 1804 Color: 2

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 12139 Color: 4
Size: 4104 Color: 2

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 14269 Color: 2
Size: 1974 Color: 3

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 10010 Color: 1
Size: 6232 Color: 2

Bin 133: 14 of cap free
Amount of items: 3
Items: 
Size: 10738 Color: 1
Size: 5104 Color: 4
Size: 400 Color: 4

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 14226 Color: 3
Size: 2016 Color: 0

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 14332 Color: 0
Size: 1910 Color: 3

Bin 136: 16 of cap free
Amount of items: 3
Items: 
Size: 9138 Color: 0
Size: 6774 Color: 2
Size: 328 Color: 3

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 14014 Color: 0
Size: 2226 Color: 2

Bin 138: 18 of cap free
Amount of items: 3
Items: 
Size: 9978 Color: 4
Size: 5812 Color: 0
Size: 448 Color: 1

Bin 139: 18 of cap free
Amount of items: 2
Items: 
Size: 11004 Color: 0
Size: 5234 Color: 4

Bin 140: 18 of cap free
Amount of items: 2
Items: 
Size: 13102 Color: 4
Size: 3136 Color: 2

Bin 141: 19 of cap free
Amount of items: 2
Items: 
Size: 12964 Color: 4
Size: 3273 Color: 0

Bin 142: 20 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 3
Size: 2724 Color: 0

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 14094 Color: 0
Size: 2142 Color: 3

Bin 144: 21 of cap free
Amount of items: 2
Items: 
Size: 14362 Color: 4
Size: 1873 Color: 2

Bin 145: 24 of cap free
Amount of items: 4
Items: 
Size: 8392 Color: 0
Size: 4260 Color: 1
Size: 3108 Color: 2
Size: 472 Color: 3

Bin 146: 26 of cap free
Amount of items: 2
Items: 
Size: 11788 Color: 3
Size: 4442 Color: 2

Bin 147: 27 of cap free
Amount of items: 2
Items: 
Size: 14151 Color: 4
Size: 2078 Color: 1

Bin 148: 28 of cap free
Amount of items: 2
Items: 
Size: 13062 Color: 3
Size: 3166 Color: 0

Bin 149: 29 of cap free
Amount of items: 3
Items: 
Size: 9816 Color: 4
Size: 5867 Color: 2
Size: 544 Color: 3

Bin 150: 29 of cap free
Amount of items: 2
Items: 
Size: 11116 Color: 2
Size: 5111 Color: 4

Bin 151: 29 of cap free
Amount of items: 2
Items: 
Size: 14100 Color: 2
Size: 2127 Color: 4

Bin 152: 29 of cap free
Amount of items: 3
Items: 
Size: 14603 Color: 1
Size: 1604 Color: 3
Size: 20 Color: 3

Bin 153: 30 of cap free
Amount of items: 2
Items: 
Size: 12458 Color: 2
Size: 3768 Color: 4

Bin 154: 30 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 4
Size: 2710 Color: 3
Size: 112 Color: 0

Bin 155: 30 of cap free
Amount of items: 2
Items: 
Size: 14518 Color: 0
Size: 1708 Color: 3

Bin 156: 32 of cap free
Amount of items: 5
Items: 
Size: 8132 Color: 2
Size: 2443 Color: 4
Size: 2208 Color: 3
Size: 1882 Color: 1
Size: 1559 Color: 0

Bin 157: 32 of cap free
Amount of items: 4
Items: 
Size: 9217 Color: 1
Size: 5012 Color: 3
Size: 1699 Color: 3
Size: 296 Color: 0

Bin 158: 32 of cap free
Amount of items: 2
Items: 
Size: 11880 Color: 2
Size: 4344 Color: 4

Bin 159: 33 of cap free
Amount of items: 3
Items: 
Size: 12653 Color: 4
Size: 3466 Color: 0
Size: 104 Color: 0

Bin 160: 33 of cap free
Amount of items: 2
Items: 
Size: 13206 Color: 2
Size: 3017 Color: 4

Bin 161: 35 of cap free
Amount of items: 2
Items: 
Size: 12264 Color: 3
Size: 3957 Color: 2

Bin 162: 36 of cap free
Amount of items: 6
Items: 
Size: 8134 Color: 1
Size: 2124 Color: 4
Size: 2056 Color: 2
Size: 1802 Color: 1
Size: 1352 Color: 0
Size: 752 Color: 2

Bin 163: 38 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 4
Size: 1828 Color: 1

Bin 164: 41 of cap free
Amount of items: 2
Items: 
Size: 11732 Color: 2
Size: 4483 Color: 3

Bin 165: 41 of cap free
Amount of items: 2
Items: 
Size: 13930 Color: 3
Size: 2285 Color: 0

Bin 166: 42 of cap free
Amount of items: 2
Items: 
Size: 10280 Color: 4
Size: 5934 Color: 2

Bin 167: 43 of cap free
Amount of items: 4
Items: 
Size: 8148 Color: 2
Size: 3482 Color: 1
Size: 3431 Color: 1
Size: 1152 Color: 3

Bin 168: 46 of cap free
Amount of items: 2
Items: 
Size: 11530 Color: 2
Size: 4680 Color: 4

Bin 169: 46 of cap free
Amount of items: 2
Items: 
Size: 13462 Color: 4
Size: 2748 Color: 3

Bin 170: 48 of cap free
Amount of items: 2
Items: 
Size: 12526 Color: 2
Size: 3682 Color: 4

Bin 171: 48 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 3
Size: 2164 Color: 4

Bin 172: 50 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 0
Size: 3302 Color: 4

Bin 173: 52 of cap free
Amount of items: 3
Items: 
Size: 12902 Color: 3
Size: 3110 Color: 4
Size: 192 Color: 2

Bin 174: 54 of cap free
Amount of items: 3
Items: 
Size: 13292 Color: 2
Size: 2798 Color: 4
Size: 112 Color: 0

Bin 175: 58 of cap free
Amount of items: 2
Items: 
Size: 13790 Color: 4
Size: 2408 Color: 0

Bin 176: 60 of cap free
Amount of items: 3
Items: 
Size: 11402 Color: 2
Size: 4602 Color: 4
Size: 192 Color: 3

Bin 177: 64 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 4
Size: 3016 Color: 3

Bin 178: 68 of cap free
Amount of items: 2
Items: 
Size: 12532 Color: 4
Size: 3656 Color: 2

Bin 179: 79 of cap free
Amount of items: 2
Items: 
Size: 13886 Color: 2
Size: 2291 Color: 3

Bin 180: 84 of cap free
Amount of items: 29
Items: 
Size: 736 Color: 0
Size: 720 Color: 0
Size: 712 Color: 1
Size: 704 Color: 3
Size: 704 Color: 0
Size: 686 Color: 1
Size: 684 Color: 0
Size: 632 Color: 4
Size: 632 Color: 3
Size: 632 Color: 0
Size: 624 Color: 4
Size: 616 Color: 3
Size: 608 Color: 3
Size: 598 Color: 3
Size: 584 Color: 4
Size: 556 Color: 1
Size: 544 Color: 3
Size: 532 Color: 3
Size: 524 Color: 4
Size: 512 Color: 3
Size: 512 Color: 2
Size: 448 Color: 1
Size: 428 Color: 2
Size: 412 Color: 2
Size: 392 Color: 1
Size: 384 Color: 4
Size: 360 Color: 2
Size: 360 Color: 1
Size: 336 Color: 2

Bin 181: 84 of cap free
Amount of items: 2
Items: 
Size: 12098 Color: 3
Size: 4074 Color: 0

Bin 182: 84 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 2
Size: 2512 Color: 0

Bin 183: 95 of cap free
Amount of items: 2
Items: 
Size: 12637 Color: 3
Size: 3524 Color: 4

Bin 184: 98 of cap free
Amount of items: 2
Items: 
Size: 10952 Color: 4
Size: 5206 Color: 3

Bin 185: 104 of cap free
Amount of items: 2
Items: 
Size: 8136 Color: 1
Size: 8016 Color: 2

Bin 186: 116 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 4
Size: 5844 Color: 1
Size: 1024 Color: 0

Bin 187: 121 of cap free
Amount of items: 2
Items: 
Size: 11509 Color: 4
Size: 4626 Color: 0

Bin 188: 128 of cap free
Amount of items: 2
Items: 
Size: 11336 Color: 3
Size: 4792 Color: 2

Bin 189: 134 of cap free
Amount of items: 2
Items: 
Size: 11838 Color: 3
Size: 4284 Color: 4

Bin 190: 141 of cap free
Amount of items: 9
Items: 
Size: 8129 Color: 4
Size: 1152 Color: 2
Size: 1044 Color: 3
Size: 1022 Color: 4
Size: 1000 Color: 3
Size: 992 Color: 3
Size: 992 Color: 0
Size: 912 Color: 0
Size: 872 Color: 2

Bin 191: 152 of cap free
Amount of items: 2
Items: 
Size: 9332 Color: 2
Size: 6772 Color: 1

Bin 192: 158 of cap free
Amount of items: 3
Items: 
Size: 10276 Color: 3
Size: 4650 Color: 1
Size: 1172 Color: 0

Bin 193: 168 of cap free
Amount of items: 2
Items: 
Size: 8968 Color: 0
Size: 7120 Color: 1

Bin 194: 190 of cap free
Amount of items: 7
Items: 
Size: 8130 Color: 3
Size: 1528 Color: 3
Size: 1470 Color: 4
Size: 1450 Color: 1
Size: 1248 Color: 1
Size: 1232 Color: 2
Size: 1008 Color: 0

Bin 195: 196 of cap free
Amount of items: 2
Items: 
Size: 9284 Color: 3
Size: 6776 Color: 4

Bin 196: 207 of cap free
Amount of items: 2
Items: 
Size: 10119 Color: 3
Size: 5930 Color: 1

Bin 197: 233 of cap free
Amount of items: 2
Items: 
Size: 9252 Color: 4
Size: 6771 Color: 3

Bin 198: 271 of cap free
Amount of items: 2
Items: 
Size: 9215 Color: 2
Size: 6770 Color: 4

Bin 199: 11500 of cap free
Amount of items: 14
Items: 
Size: 468 Color: 0
Size: 444 Color: 0
Size: 416 Color: 0
Size: 374 Color: 4
Size: 330 Color: 4
Size: 328 Color: 1
Size: 312 Color: 1
Size: 312 Color: 1
Size: 308 Color: 3
Size: 308 Color: 2
Size: 292 Color: 2
Size: 288 Color: 4
Size: 288 Color: 2
Size: 288 Color: 2

Total size: 3218688
Total free space: 16256

