Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1244 Color: 1
Size: 1028 Color: 4
Size: 136 Color: 2
Size: 64 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 0
Size: 1000 Color: 4
Size: 68 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1466 Color: 0
Size: 892 Color: 4
Size: 114 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 1
Size: 766 Color: 4
Size: 152 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 4
Size: 684 Color: 0
Size: 40 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1850 Color: 4
Size: 538 Color: 1
Size: 84 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 1
Size: 514 Color: 3
Size: 100 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 1
Size: 546 Color: 3
Size: 44 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1948 Color: 3
Size: 484 Color: 2
Size: 40 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1953 Color: 3
Size: 385 Color: 0
Size: 134 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 1
Size: 462 Color: 0
Size: 36 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 2
Size: 364 Color: 0
Size: 72 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 0
Size: 322 Color: 4
Size: 48 Color: 3

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 2116 Color: 4
Size: 332 Color: 3
Size: 16 Color: 0
Size: 8 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2138 Color: 1
Size: 200 Color: 0
Size: 134 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 3
Size: 300 Color: 2
Size: 24 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2218 Color: 0
Size: 156 Color: 2
Size: 98 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 3
Size: 176 Color: 0
Size: 84 Color: 1

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1652 Color: 4
Size: 787 Color: 0
Size: 32 Color: 2

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 3
Size: 685 Color: 2
Size: 52 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1828 Color: 4
Size: 567 Color: 1
Size: 76 Color: 4

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1877 Color: 4
Size: 546 Color: 1
Size: 48 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 1
Size: 427 Color: 4
Size: 152 Color: 0

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 0
Size: 367 Color: 2
Size: 86 Color: 3

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 2076 Color: 0
Size: 331 Color: 3
Size: 64 Color: 2

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 3
Size: 1027 Color: 1
Size: 204 Color: 4

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1540 Color: 0
Size: 930 Color: 1

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 2
Size: 673 Color: 4
Size: 132 Color: 0

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 1
Size: 501 Color: 0
Size: 176 Color: 2

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 3
Size: 604 Color: 0
Size: 48 Color: 3

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 0
Size: 384 Color: 2
Size: 104 Color: 4

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 3
Size: 373 Color: 1
Size: 80 Color: 0

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 2025 Color: 0
Size: 381 Color: 2
Size: 64 Color: 4

Bin 34: 3 of cap free
Amount of items: 7
Items: 
Size: 1237 Color: 1
Size: 342 Color: 0
Size: 242 Color: 0
Size: 228 Color: 3
Size: 204 Color: 3
Size: 132 Color: 2
Size: 84 Color: 3

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 4
Size: 576 Color: 0
Size: 236 Color: 4

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 1
Size: 618 Color: 4
Size: 74 Color: 0

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 1241 Color: 3
Size: 1227 Color: 0

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 0
Size: 1018 Color: 3
Size: 204 Color: 4

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 1
Size: 662 Color: 2
Size: 96 Color: 0

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 3
Size: 302 Color: 0
Size: 8 Color: 1

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 2
Size: 491 Color: 3
Size: 272 Color: 3

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 2033 Color: 1
Size: 434 Color: 3

Bin 43: 6 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 0
Size: 842 Color: 3
Size: 72 Color: 3

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 1673 Color: 0
Size: 793 Color: 4

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1785 Color: 2
Size: 681 Color: 1

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 1885 Color: 4
Size: 581 Color: 1

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 2062 Color: 3
Size: 404 Color: 2

Bin 48: 6 of cap free
Amount of items: 3
Items: 
Size: 2182 Color: 1
Size: 276 Color: 4
Size: 8 Color: 1

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 2204 Color: 3
Size: 262 Color: 4

Bin 50: 7 of cap free
Amount of items: 4
Items: 
Size: 1238 Color: 1
Size: 881 Color: 4
Size: 272 Color: 1
Size: 74 Color: 0

Bin 51: 7 of cap free
Amount of items: 2
Items: 
Size: 1961 Color: 4
Size: 504 Color: 1

Bin 52: 10 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 4
Size: 840 Color: 2
Size: 164 Color: 3

Bin 53: 10 of cap free
Amount of items: 2
Items: 
Size: 1922 Color: 1
Size: 540 Color: 4

Bin 54: 13 of cap free
Amount of items: 2
Items: 
Size: 2077 Color: 2
Size: 382 Color: 1

Bin 55: 14 of cap free
Amount of items: 2
Items: 
Size: 1996 Color: 3
Size: 462 Color: 2

Bin 56: 17 of cap free
Amount of items: 2
Items: 
Size: 2011 Color: 1
Size: 444 Color: 2

Bin 57: 18 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 2
Size: 667 Color: 4
Size: 136 Color: 3

Bin 58: 20 of cap free
Amount of items: 2
Items: 
Size: 1529 Color: 4
Size: 923 Color: 3

Bin 59: 21 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 4
Size: 497 Color: 4
Size: 433 Color: 0

Bin 60: 22 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 3
Size: 780 Color: 4
Size: 32 Color: 4

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1415 Color: 0
Size: 1029 Color: 2

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1871 Color: 3
Size: 573 Color: 4

Bin 63: 30 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 3
Size: 1030 Color: 0
Size: 158 Color: 1

Bin 64: 33 of cap free
Amount of items: 2
Items: 
Size: 1417 Color: 0
Size: 1022 Color: 3

Bin 65: 46 of cap free
Amount of items: 14
Items: 
Size: 770 Color: 1
Size: 214 Color: 4
Size: 204 Color: 3
Size: 200 Color: 0
Size: 120 Color: 4
Size: 120 Color: 2
Size: 114 Color: 0
Size: 112 Color: 0
Size: 108 Color: 1
Size: 100 Color: 3
Size: 100 Color: 2
Size: 96 Color: 4
Size: 96 Color: 1
Size: 72 Color: 2

Bin 66: 2048 of cap free
Amount of items: 6
Items: 
Size: 88 Color: 3
Size: 88 Color: 1
Size: 72 Color: 2
Size: 64 Color: 0
Size: 56 Color: 3
Size: 56 Color: 2

Total size: 160680
Total free space: 2472

