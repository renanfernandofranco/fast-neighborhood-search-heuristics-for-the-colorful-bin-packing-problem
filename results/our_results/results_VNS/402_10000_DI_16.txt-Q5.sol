Capicity Bin: 8352
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 2
Size: 3348 Color: 3
Size: 208 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4866 Color: 1
Size: 2906 Color: 0
Size: 580 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5318 Color: 0
Size: 2894 Color: 4
Size: 140 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 4
Size: 2660 Color: 1
Size: 216 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5786 Color: 1
Size: 2218 Color: 0
Size: 348 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6060 Color: 0
Size: 1916 Color: 2
Size: 376 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6252 Color: 3
Size: 1924 Color: 2
Size: 176 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 0
Size: 1756 Color: 2
Size: 184 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6650 Color: 2
Size: 1164 Color: 0
Size: 538 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 0
Size: 1460 Color: 2
Size: 168 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 4
Size: 1258 Color: 2
Size: 292 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6846 Color: 1
Size: 814 Color: 3
Size: 692 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6964 Color: 2
Size: 1056 Color: 1
Size: 332 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 1
Size: 1030 Color: 3
Size: 376 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6969 Color: 4
Size: 1225 Color: 2
Size: 158 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6998 Color: 2
Size: 1162 Color: 0
Size: 192 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 1
Size: 860 Color: 4
Size: 472 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7052 Color: 4
Size: 1084 Color: 2
Size: 216 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 3
Size: 778 Color: 2
Size: 496 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7110 Color: 0
Size: 1078 Color: 2
Size: 164 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7129 Color: 3
Size: 1057 Color: 0
Size: 166 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 2
Size: 942 Color: 4
Size: 260 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 2
Size: 788 Color: 0
Size: 360 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7215 Color: 2
Size: 799 Color: 4
Size: 338 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7238 Color: 3
Size: 640 Color: 2
Size: 474 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 3
Size: 694 Color: 4
Size: 388 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 3
Size: 1002 Color: 1
Size: 64 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7305 Color: 3
Size: 873 Color: 2
Size: 174 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 1
Size: 692 Color: 0
Size: 336 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7338 Color: 4
Size: 718 Color: 0
Size: 296 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7378 Color: 4
Size: 694 Color: 2
Size: 280 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7363 Color: 2
Size: 701 Color: 1
Size: 288 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7396 Color: 4
Size: 804 Color: 4
Size: 152 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7422 Color: 2
Size: 778 Color: 1
Size: 152 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7442 Color: 2
Size: 716 Color: 4
Size: 194 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7452 Color: 4
Size: 756 Color: 2
Size: 144 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7458 Color: 0
Size: 746 Color: 3
Size: 148 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7466 Color: 3
Size: 742 Color: 4
Size: 144 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7484 Color: 0
Size: 762 Color: 4
Size: 106 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7500 Color: 1
Size: 692 Color: 4
Size: 160 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7503 Color: 2
Size: 709 Color: 3
Size: 140 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 3
Size: 3279 Color: 2
Size: 310 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 4803 Color: 3
Size: 3348 Color: 2
Size: 200 Color: 4

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 1
Size: 3004 Color: 2
Size: 224 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 4
Size: 2691 Color: 0
Size: 200 Color: 2

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5709 Color: 1
Size: 2494 Color: 4
Size: 148 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6021 Color: 0
Size: 2162 Color: 2
Size: 168 Color: 1

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6315 Color: 2
Size: 2036 Color: 4

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 0
Size: 1580 Color: 4
Size: 284 Color: 2

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 2
Size: 1341 Color: 3
Size: 436 Color: 1

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 3
Size: 1021 Color: 2
Size: 688 Color: 1

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 1
Size: 1442 Color: 3
Size: 204 Color: 2

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 0
Size: 1294 Color: 4
Size: 174 Color: 2

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 7075 Color: 0
Size: 876 Color: 2
Size: 400 Color: 4

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 7222 Color: 0
Size: 949 Color: 2
Size: 180 Color: 4

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 7511 Color: 1
Size: 600 Color: 0
Size: 240 Color: 4

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 4
Size: 2852 Color: 3
Size: 1158 Color: 1

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5362 Color: 2
Size: 2848 Color: 2
Size: 140 Color: 0

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 5391 Color: 1
Size: 2959 Color: 4

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5505 Color: 1
Size: 2613 Color: 4
Size: 232 Color: 2

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 5730 Color: 1
Size: 2620 Color: 4

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 5916 Color: 0
Size: 2186 Color: 2
Size: 248 Color: 2

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6175 Color: 2
Size: 1943 Color: 2
Size: 232 Color: 4

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 0
Size: 1678 Color: 1
Size: 332 Color: 2

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 7062 Color: 4
Size: 1288 Color: 1

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 4194 Color: 0
Size: 3431 Color: 0
Size: 724 Color: 1

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 6374 Color: 2
Size: 1815 Color: 1
Size: 160 Color: 3

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 1
Size: 1620 Color: 2
Size: 154 Color: 4

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 7213 Color: 4
Size: 1136 Color: 1

Bin 70: 3 of cap free
Amount of items: 4
Items: 
Size: 7364 Color: 4
Size: 825 Color: 1
Size: 104 Color: 2
Size: 56 Color: 1

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 4882 Color: 3
Size: 3466 Color: 4

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 3
Size: 2908 Color: 0
Size: 284 Color: 1

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 6330 Color: 4
Size: 1686 Color: 1
Size: 332 Color: 0

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 3
Size: 1894 Color: 4
Size: 112 Color: 4

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 7427 Color: 0
Size: 921 Color: 3

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 4202 Color: 1
Size: 3481 Color: 2
Size: 664 Color: 3

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 6094 Color: 1
Size: 1142 Color: 3
Size: 1111 Color: 0

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 6358 Color: 3
Size: 1453 Color: 2
Size: 536 Color: 0

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 7457 Color: 0
Size: 890 Color: 1

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 5694 Color: 2
Size: 2404 Color: 2
Size: 248 Color: 0

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6982 Color: 4
Size: 1364 Color: 3

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 7334 Color: 1
Size: 1012 Color: 4

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 4747 Color: 0
Size: 3462 Color: 4
Size: 136 Color: 4

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 0
Size: 1053 Color: 0
Size: 688 Color: 2

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 4
Size: 1205 Color: 3

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 7510 Color: 3
Size: 833 Color: 0
Size: 2 Color: 4

Bin 87: 8 of cap free
Amount of items: 5
Items: 
Size: 4177 Color: 1
Size: 2083 Color: 2
Size: 1116 Color: 3
Size: 528 Color: 4
Size: 440 Color: 4

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 6516 Color: 1
Size: 1828 Color: 3

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 7308 Color: 0
Size: 1036 Color: 1

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 7494 Color: 1
Size: 850 Color: 0

Bin 91: 9 of cap free
Amount of items: 3
Items: 
Size: 6789 Color: 3
Size: 1466 Color: 1
Size: 88 Color: 4

Bin 92: 9 of cap free
Amount of items: 3
Items: 
Size: 7395 Color: 4
Size: 828 Color: 1
Size: 120 Color: 4

Bin 93: 10 of cap free
Amount of items: 5
Items: 
Size: 4181 Color: 1
Size: 2164 Color: 0
Size: 954 Color: 4
Size: 747 Color: 2
Size: 296 Color: 1

Bin 94: 10 of cap free
Amount of items: 3
Items: 
Size: 4284 Color: 0
Size: 3482 Color: 0
Size: 576 Color: 4

Bin 95: 10 of cap free
Amount of items: 3
Items: 
Size: 4637 Color: 0
Size: 3477 Color: 1
Size: 228 Color: 1

Bin 96: 10 of cap free
Amount of items: 2
Items: 
Size: 4868 Color: 0
Size: 3474 Color: 4

Bin 97: 10 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 3
Size: 1882 Color: 1

Bin 98: 10 of cap free
Amount of items: 2
Items: 
Size: 6680 Color: 0
Size: 1662 Color: 3

Bin 99: 10 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 4
Size: 1426 Color: 3

Bin 100: 11 of cap free
Amount of items: 2
Items: 
Size: 7247 Color: 3
Size: 1094 Color: 1

Bin 101: 14 of cap free
Amount of items: 2
Items: 
Size: 6966 Color: 4
Size: 1372 Color: 1

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 7276 Color: 0
Size: 1062 Color: 3

Bin 103: 15 of cap free
Amount of items: 3
Items: 
Size: 5762 Color: 0
Size: 1422 Color: 3
Size: 1153 Color: 4

Bin 104: 15 of cap free
Amount of items: 2
Items: 
Size: 7085 Color: 4
Size: 1252 Color: 0

Bin 105: 16 of cap free
Amount of items: 2
Items: 
Size: 7033 Color: 4
Size: 1303 Color: 1

Bin 106: 18 of cap free
Amount of items: 2
Items: 
Size: 6852 Color: 3
Size: 1482 Color: 4

Bin 107: 22 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 1
Size: 1650 Color: 4
Size: 86 Color: 0

Bin 108: 22 of cap free
Amount of items: 2
Items: 
Size: 7156 Color: 3
Size: 1174 Color: 0

Bin 109: 24 of cap free
Amount of items: 2
Items: 
Size: 6267 Color: 0
Size: 2061 Color: 4

Bin 110: 27 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 1
Size: 1699 Color: 4

Bin 111: 28 of cap free
Amount of items: 30
Items: 
Size: 504 Color: 3
Size: 428 Color: 4
Size: 424 Color: 1
Size: 400 Color: 1
Size: 376 Color: 2
Size: 362 Color: 4
Size: 344 Color: 4
Size: 344 Color: 0
Size: 328 Color: 2
Size: 320 Color: 4
Size: 312 Color: 1
Size: 272 Color: 0
Size: 268 Color: 2
Size: 264 Color: 0
Size: 256 Color: 2
Size: 244 Color: 1
Size: 232 Color: 4
Size: 230 Color: 3
Size: 228 Color: 2
Size: 224 Color: 0
Size: 216 Color: 0
Size: 212 Color: 4
Size: 212 Color: 4
Size: 210 Color: 2
Size: 208 Color: 4
Size: 202 Color: 2
Size: 188 Color: 4
Size: 188 Color: 4
Size: 168 Color: 0
Size: 160 Color: 0

Bin 112: 28 of cap free
Amount of items: 2
Items: 
Size: 5330 Color: 2
Size: 2994 Color: 1

Bin 113: 28 of cap free
Amount of items: 3
Items: 
Size: 5879 Color: 2
Size: 2373 Color: 4
Size: 72 Color: 1

Bin 114: 28 of cap free
Amount of items: 2
Items: 
Size: 7478 Color: 4
Size: 846 Color: 3

Bin 115: 30 of cap free
Amount of items: 2
Items: 
Size: 4178 Color: 4
Size: 4144 Color: 0

Bin 116: 30 of cap free
Amount of items: 2
Items: 
Size: 7118 Color: 1
Size: 1204 Color: 0

Bin 117: 32 of cap free
Amount of items: 2
Items: 
Size: 6708 Color: 1
Size: 1612 Color: 0

Bin 118: 35 of cap free
Amount of items: 2
Items: 
Size: 7353 Color: 3
Size: 964 Color: 4

Bin 119: 36 of cap free
Amount of items: 2
Items: 
Size: 5212 Color: 1
Size: 3104 Color: 3

Bin 120: 37 of cap free
Amount of items: 3
Items: 
Size: 4186 Color: 2
Size: 3358 Color: 4
Size: 771 Color: 1

Bin 121: 38 of cap free
Amount of items: 2
Items: 
Size: 7412 Color: 0
Size: 902 Color: 1

Bin 122: 42 of cap free
Amount of items: 2
Items: 
Size: 4778 Color: 3
Size: 3532 Color: 4

Bin 123: 46 of cap free
Amount of items: 2
Items: 
Size: 6164 Color: 2
Size: 2142 Color: 3

Bin 124: 47 of cap free
Amount of items: 2
Items: 
Size: 6824 Color: 3
Size: 1481 Color: 4

Bin 125: 54 of cap free
Amount of items: 2
Items: 
Size: 6743 Color: 0
Size: 1555 Color: 2

Bin 126: 64 of cap free
Amount of items: 2
Items: 
Size: 4748 Color: 3
Size: 3540 Color: 1

Bin 127: 66 of cap free
Amount of items: 2
Items: 
Size: 5756 Color: 2
Size: 2530 Color: 1

Bin 128: 67 of cap free
Amount of items: 2
Items: 
Size: 6082 Color: 4
Size: 2203 Color: 1

Bin 129: 75 of cap free
Amount of items: 3
Items: 
Size: 4185 Color: 4
Size: 3656 Color: 0
Size: 436 Color: 2

Bin 130: 86 of cap free
Amount of items: 11
Items: 
Size: 2690 Color: 1
Size: 916 Color: 2
Size: 730 Color: 3
Size: 590 Color: 0
Size: 576 Color: 1
Size: 520 Color: 1
Size: 520 Color: 0
Size: 440 Color: 4
Size: 440 Color: 4
Size: 432 Color: 0
Size: 412 Color: 2

Bin 131: 92 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 3
Size: 3476 Color: 1
Size: 596 Color: 2

Bin 132: 111 of cap free
Amount of items: 2
Items: 
Size: 4180 Color: 1
Size: 4061 Color: 3

Bin 133: 6800 of cap free
Amount of items: 10
Items: 
Size: 184 Color: 1
Size: 168 Color: 1
Size: 160 Color: 3
Size: 160 Color: 0
Size: 152 Color: 1
Size: 148 Color: 3
Size: 148 Color: 2
Size: 144 Color: 2
Size: 144 Color: 2
Size: 144 Color: 0

Total size: 1102464
Total free space: 8352

