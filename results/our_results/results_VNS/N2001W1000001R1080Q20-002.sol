Capicity Bin: 1000001
Lower Bound: 905

Bins used: 908
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 540386 Color: 9
Size: 340011 Color: 3
Size: 119604 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 784571 Color: 9
Size: 112639 Color: 8
Size: 102791 Color: 11

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 711904 Color: 17
Size: 169915 Color: 7
Size: 118182 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 546235 Color: 8
Size: 309808 Color: 15
Size: 143958 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 694813 Color: 12
Size: 171357 Color: 8
Size: 133831 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 523661 Color: 2
Size: 358193 Color: 11
Size: 118147 Color: 19

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 364074 Color: 4
Size: 326859 Color: 8
Size: 154563 Color: 9
Size: 154505 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 705400 Color: 7
Size: 180660 Color: 16
Size: 113941 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 730206 Color: 12
Size: 164640 Color: 1
Size: 105155 Color: 3

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 547204 Color: 7
Size: 157335 Color: 10
Size: 156090 Color: 5
Size: 139372 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 706938 Color: 16
Size: 185879 Color: 19
Size: 107184 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 535362 Color: 9
Size: 259591 Color: 15
Size: 205048 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 717631 Color: 18
Size: 165650 Color: 18
Size: 116720 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 735767 Color: 13
Size: 157204 Color: 4
Size: 107030 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 711068 Color: 12
Size: 164911 Color: 11
Size: 124022 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 773053 Color: 4
Size: 125150 Color: 15
Size: 101798 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 653603 Color: 4
Size: 222812 Color: 18
Size: 123586 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 715284 Color: 13
Size: 161172 Color: 12
Size: 123545 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 753053 Color: 5
Size: 144284 Color: 11
Size: 102664 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 742466 Color: 15
Size: 134583 Color: 12
Size: 122952 Color: 17

Bin 21: 0 of cap free
Amount of items: 4
Items: 
Size: 358703 Color: 8
Size: 316594 Color: 17
Size: 168549 Color: 11
Size: 156155 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 783485 Color: 12
Size: 109247 Color: 4
Size: 107269 Color: 5

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 501586 Color: 2
Size: 194838 Color: 17
Size: 191264 Color: 9
Size: 112313 Color: 18

Bin 24: 0 of cap free
Amount of items: 4
Items: 
Size: 508157 Color: 12
Size: 194458 Color: 14
Size: 189086 Color: 5
Size: 108300 Color: 19

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 744161 Color: 17
Size: 140821 Color: 2
Size: 115019 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 534804 Color: 12
Size: 254514 Color: 15
Size: 210683 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 570474 Color: 13
Size: 303492 Color: 16
Size: 126035 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 538156 Color: 4
Size: 332106 Color: 6
Size: 129739 Color: 18

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 656742 Color: 17
Size: 195840 Color: 4
Size: 147419 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 737052 Color: 15
Size: 151194 Color: 7
Size: 111755 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 498686 Color: 8
Size: 382069 Color: 16
Size: 119246 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 761328 Color: 13
Size: 135919 Color: 16
Size: 102754 Color: 9

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 686817 Color: 2
Size: 197318 Color: 3
Size: 115866 Color: 8

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 765729 Color: 7
Size: 234272 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 687819 Color: 13
Size: 179569 Color: 5
Size: 132613 Color: 16

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 711755 Color: 5
Size: 166291 Color: 8
Size: 121955 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 540078 Color: 9
Size: 330945 Color: 14
Size: 128978 Color: 19

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 536367 Color: 1
Size: 316124 Color: 11
Size: 147510 Color: 8

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 557180 Color: 0
Size: 442821 Color: 7

Bin 40: 0 of cap free
Amount of items: 4
Items: 
Size: 542544 Color: 17
Size: 234514 Color: 12
Size: 112250 Color: 17
Size: 110693 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 677029 Color: 15
Size: 206296 Color: 9
Size: 116676 Color: 7

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 718991 Color: 17
Size: 175221 Color: 14
Size: 105789 Color: 13

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 552174 Color: 17
Size: 294894 Color: 8
Size: 152933 Color: 12

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 649694 Color: 1
Size: 225291 Color: 13
Size: 125016 Color: 9

Bin 45: 0 of cap free
Amount of items: 4
Items: 
Size: 550098 Color: 7
Size: 154301 Color: 2
Size: 151131 Color: 2
Size: 144471 Color: 18

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 508381 Color: 17
Size: 295589 Color: 16
Size: 196031 Color: 7

Bin 47: 0 of cap free
Amount of items: 4
Items: 
Size: 529344 Color: 12
Size: 236840 Color: 16
Size: 131358 Color: 10
Size: 102459 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 642117 Color: 6
Size: 252032 Color: 3
Size: 105852 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 636842 Color: 9
Size: 246465 Color: 2
Size: 116694 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 735013 Color: 12
Size: 161139 Color: 6
Size: 103849 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 578542 Color: 8
Size: 295244 Color: 18
Size: 126215 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 685400 Color: 15
Size: 187025 Color: 19
Size: 127576 Color: 14

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 694143 Color: 13
Size: 170941 Color: 11
Size: 134917 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 694554 Color: 1
Size: 159494 Color: 11
Size: 145953 Color: 12

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 547608 Color: 16
Size: 283250 Color: 17
Size: 169143 Color: 11

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 750214 Color: 12
Size: 139694 Color: 3
Size: 110093 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 590743 Color: 0
Size: 242359 Color: 9
Size: 166899 Color: 19

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 530428 Color: 16
Size: 252381 Color: 1
Size: 217192 Color: 17

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 562036 Color: 7
Size: 437965 Color: 19

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 573256 Color: 15
Size: 313912 Color: 12
Size: 112833 Color: 11

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 502269 Color: 19
Size: 380681 Color: 11
Size: 117051 Color: 5

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 767666 Color: 8
Size: 129202 Color: 10
Size: 103133 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 535602 Color: 5
Size: 364101 Color: 11
Size: 100298 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 582790 Color: 0
Size: 294637 Color: 16
Size: 122574 Color: 12

Bin 65: 0 of cap free
Amount of items: 5
Items: 
Size: 276345 Color: 1
Size: 260134 Color: 16
Size: 163382 Color: 7
Size: 150387 Color: 2
Size: 149753 Color: 12

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 720039 Color: 0
Size: 148676 Color: 13
Size: 131286 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 723410 Color: 11
Size: 146103 Color: 5
Size: 130488 Color: 1

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 749106 Color: 2
Size: 136240 Color: 14
Size: 114654 Color: 15

Bin 69: 1 of cap free
Amount of items: 4
Items: 
Size: 354850 Color: 13
Size: 330487 Color: 7
Size: 166228 Color: 13
Size: 148435 Color: 7

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 549740 Color: 6
Size: 292389 Color: 16
Size: 157871 Color: 3

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 619471 Color: 5
Size: 191476 Color: 19
Size: 189053 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 706409 Color: 16
Size: 166373 Color: 17
Size: 127218 Color: 3

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 520948 Color: 13
Size: 364142 Color: 2
Size: 114910 Color: 12

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 701474 Color: 1
Size: 158465 Color: 15
Size: 140061 Color: 6

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 506150 Color: 17
Size: 370918 Color: 6
Size: 122932 Color: 17

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 629371 Color: 11
Size: 261811 Color: 14
Size: 108818 Color: 12

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 708569 Color: 10
Size: 165733 Color: 17
Size: 125698 Color: 1

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 665690 Color: 1
Size: 198601 Color: 2
Size: 135709 Color: 19

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 616820 Color: 11
Size: 202309 Color: 19
Size: 180871 Color: 14

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 772379 Color: 13
Size: 116589 Color: 19
Size: 111032 Color: 19

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 678388 Color: 7
Size: 198001 Color: 13
Size: 123611 Color: 10

Bin 82: 1 of cap free
Amount of items: 5
Items: 
Size: 416233 Color: 11
Size: 200191 Color: 0
Size: 140180 Color: 17
Size: 136796 Color: 7
Size: 106600 Color: 6

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 653651 Color: 3
Size: 208741 Color: 9
Size: 137608 Color: 15

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 560531 Color: 12
Size: 325780 Color: 16
Size: 113689 Color: 5

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 557723 Color: 6
Size: 338089 Color: 1
Size: 104188 Color: 15

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 545226 Color: 4
Size: 245251 Color: 7
Size: 209523 Color: 16

Bin 87: 1 of cap free
Amount of items: 3
Items: 
Size: 685022 Color: 2
Size: 166835 Color: 6
Size: 148143 Color: 7

Bin 88: 1 of cap free
Amount of items: 3
Items: 
Size: 662196 Color: 18
Size: 221412 Color: 14
Size: 116392 Color: 19

Bin 89: 1 of cap free
Amount of items: 3
Items: 
Size: 677416 Color: 14
Size: 204080 Color: 7
Size: 118504 Color: 14

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 583611 Color: 4
Size: 259671 Color: 9
Size: 156717 Color: 19

Bin 91: 2 of cap free
Amount of items: 2
Items: 
Size: 578919 Color: 6
Size: 421080 Color: 2

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 703911 Color: 13
Size: 156712 Color: 15
Size: 139376 Color: 6

Bin 93: 2 of cap free
Amount of items: 2
Items: 
Size: 725019 Color: 8
Size: 274980 Color: 11

Bin 94: 2 of cap free
Amount of items: 2
Items: 
Size: 744926 Color: 5
Size: 255073 Color: 12

Bin 95: 2 of cap free
Amount of items: 2
Items: 
Size: 782641 Color: 4
Size: 217358 Color: 18

Bin 96: 2 of cap free
Amount of items: 3
Items: 
Size: 530589 Color: 11
Size: 258025 Color: 15
Size: 211385 Color: 16

Bin 97: 2 of cap free
Amount of items: 3
Items: 
Size: 729982 Color: 4
Size: 138297 Color: 0
Size: 131720 Color: 5

Bin 98: 2 of cap free
Amount of items: 2
Items: 
Size: 750422 Color: 18
Size: 249577 Color: 1

Bin 99: 2 of cap free
Amount of items: 3
Items: 
Size: 618580 Color: 1
Size: 218423 Color: 8
Size: 162996 Color: 6

Bin 100: 2 of cap free
Amount of items: 3
Items: 
Size: 636684 Color: 18
Size: 192556 Color: 8
Size: 170759 Color: 4

Bin 101: 2 of cap free
Amount of items: 3
Items: 
Size: 706453 Color: 11
Size: 148566 Color: 18
Size: 144980 Color: 7

Bin 102: 2 of cap free
Amount of items: 2
Items: 
Size: 612727 Color: 15
Size: 387272 Color: 5

Bin 103: 2 of cap free
Amount of items: 3
Items: 
Size: 542130 Color: 7
Size: 308364 Color: 18
Size: 149505 Color: 4

Bin 104: 2 of cap free
Amount of items: 3
Items: 
Size: 647767 Color: 19
Size: 203795 Color: 7
Size: 148437 Color: 5

Bin 105: 2 of cap free
Amount of items: 3
Items: 
Size: 702074 Color: 13
Size: 182489 Color: 19
Size: 115436 Color: 8

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 550467 Color: 19
Size: 247319 Color: 5
Size: 202213 Color: 16

Bin 107: 2 of cap free
Amount of items: 2
Items: 
Size: 682884 Color: 9
Size: 317115 Color: 13

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 772391 Color: 1
Size: 113928 Color: 12
Size: 113680 Color: 13

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 502102 Color: 6
Size: 261254 Color: 4
Size: 236643 Color: 19

Bin 110: 2 of cap free
Amount of items: 3
Items: 
Size: 625120 Color: 15
Size: 189430 Color: 11
Size: 185449 Color: 10

Bin 111: 2 of cap free
Amount of items: 3
Items: 
Size: 583385 Color: 13
Size: 219016 Color: 8
Size: 197598 Color: 5

Bin 112: 2 of cap free
Amount of items: 3
Items: 
Size: 691449 Color: 4
Size: 174715 Color: 2
Size: 133835 Color: 11

Bin 113: 2 of cap free
Amount of items: 3
Items: 
Size: 652271 Color: 1
Size: 180570 Color: 0
Size: 167158 Color: 16

Bin 114: 2 of cap free
Amount of items: 3
Items: 
Size: 706289 Color: 5
Size: 172636 Color: 19
Size: 121074 Color: 13

Bin 115: 2 of cap free
Amount of items: 3
Items: 
Size: 535775 Color: 10
Size: 327805 Color: 1
Size: 136419 Color: 4

Bin 116: 2 of cap free
Amount of items: 3
Items: 
Size: 418679 Color: 16
Size: 386003 Color: 5
Size: 195317 Color: 17

Bin 117: 3 of cap free
Amount of items: 3
Items: 
Size: 556231 Color: 19
Size: 314650 Color: 17
Size: 129117 Color: 4

Bin 118: 3 of cap free
Amount of items: 3
Items: 
Size: 694115 Color: 16
Size: 157796 Color: 2
Size: 148087 Color: 9

Bin 119: 3 of cap free
Amount of items: 3
Items: 
Size: 744069 Color: 13
Size: 146907 Color: 3
Size: 109022 Color: 15

Bin 120: 3 of cap free
Amount of items: 3
Items: 
Size: 726796 Color: 19
Size: 140213 Color: 16
Size: 132989 Color: 16

Bin 121: 3 of cap free
Amount of items: 3
Items: 
Size: 585384 Color: 11
Size: 219348 Color: 16
Size: 195266 Color: 11

Bin 122: 3 of cap free
Amount of items: 3
Items: 
Size: 666735 Color: 2
Size: 218094 Color: 13
Size: 115169 Color: 6

Bin 123: 3 of cap free
Amount of items: 2
Items: 
Size: 706769 Color: 2
Size: 293229 Color: 16

Bin 124: 3 of cap free
Amount of items: 2
Items: 
Size: 708617 Color: 15
Size: 291381 Color: 2

Bin 125: 3 of cap free
Amount of items: 3
Items: 
Size: 530347 Color: 5
Size: 248069 Color: 11
Size: 221582 Color: 1

Bin 126: 3 of cap free
Amount of items: 3
Items: 
Size: 504438 Color: 4
Size: 365747 Color: 2
Size: 129813 Color: 12

Bin 127: 3 of cap free
Amount of items: 3
Items: 
Size: 505962 Color: 6
Size: 350837 Color: 7
Size: 143199 Color: 14

Bin 128: 4 of cap free
Amount of items: 3
Items: 
Size: 367132 Color: 0
Size: 320086 Color: 3
Size: 312779 Color: 4

Bin 129: 4 of cap free
Amount of items: 2
Items: 
Size: 757819 Color: 3
Size: 242178 Color: 6

Bin 130: 4 of cap free
Amount of items: 2
Items: 
Size: 743393 Color: 13
Size: 256604 Color: 2

Bin 131: 4 of cap free
Amount of items: 3
Items: 
Size: 546881 Color: 12
Size: 280192 Color: 3
Size: 172924 Color: 7

Bin 132: 4 of cap free
Amount of items: 3
Items: 
Size: 364060 Color: 10
Size: 319250 Color: 11
Size: 316687 Color: 13

Bin 133: 4 of cap free
Amount of items: 3
Items: 
Size: 649979 Color: 2
Size: 219360 Color: 4
Size: 130658 Color: 12

Bin 134: 4 of cap free
Amount of items: 3
Items: 
Size: 638213 Color: 12
Size: 182010 Color: 14
Size: 179774 Color: 8

Bin 135: 4 of cap free
Amount of items: 3
Items: 
Size: 696371 Color: 17
Size: 154081 Color: 11
Size: 149545 Color: 7

Bin 136: 4 of cap free
Amount of items: 3
Items: 
Size: 345558 Color: 5
Size: 327283 Color: 4
Size: 327156 Color: 3

Bin 137: 4 of cap free
Amount of items: 3
Items: 
Size: 512895 Color: 9
Size: 381602 Color: 10
Size: 105500 Color: 11

Bin 138: 5 of cap free
Amount of items: 3
Items: 
Size: 695286 Color: 9
Size: 174948 Color: 5
Size: 129762 Color: 13

Bin 139: 5 of cap free
Amount of items: 3
Items: 
Size: 540088 Color: 18
Size: 338001 Color: 7
Size: 121907 Color: 11

Bin 140: 5 of cap free
Amount of items: 3
Items: 
Size: 353967 Color: 17
Size: 333268 Color: 6
Size: 312761 Color: 0

Bin 141: 5 of cap free
Amount of items: 3
Items: 
Size: 549436 Color: 10
Size: 334761 Color: 7
Size: 115799 Color: 3

Bin 142: 5 of cap free
Amount of items: 2
Items: 
Size: 577981 Color: 16
Size: 422015 Color: 18

Bin 143: 6 of cap free
Amount of items: 3
Items: 
Size: 632266 Color: 4
Size: 210093 Color: 7
Size: 157636 Color: 4

Bin 144: 6 of cap free
Amount of items: 3
Items: 
Size: 647914 Color: 18
Size: 202948 Color: 17
Size: 149133 Color: 5

Bin 145: 6 of cap free
Amount of items: 2
Items: 
Size: 521231 Color: 9
Size: 478764 Color: 18

Bin 146: 6 of cap free
Amount of items: 3
Items: 
Size: 660882 Color: 11
Size: 170669 Color: 18
Size: 168444 Color: 12

Bin 147: 6 of cap free
Amount of items: 3
Items: 
Size: 744890 Color: 17
Size: 152391 Color: 10
Size: 102714 Color: 15

Bin 148: 6 of cap free
Amount of items: 3
Items: 
Size: 654853 Color: 5
Size: 209846 Color: 14
Size: 135296 Color: 12

Bin 149: 7 of cap free
Amount of items: 2
Items: 
Size: 549615 Color: 9
Size: 450379 Color: 13

Bin 150: 7 of cap free
Amount of items: 3
Items: 
Size: 666059 Color: 17
Size: 183549 Color: 9
Size: 150386 Color: 8

Bin 151: 7 of cap free
Amount of items: 3
Items: 
Size: 508437 Color: 15
Size: 252078 Color: 8
Size: 239479 Color: 8

Bin 152: 7 of cap free
Amount of items: 2
Items: 
Size: 761935 Color: 17
Size: 238059 Color: 15

Bin 153: 7 of cap free
Amount of items: 2
Items: 
Size: 564203 Color: 16
Size: 435791 Color: 11

Bin 154: 8 of cap free
Amount of items: 2
Items: 
Size: 617570 Color: 1
Size: 382423 Color: 12

Bin 155: 8 of cap free
Amount of items: 2
Items: 
Size: 516383 Color: 18
Size: 483610 Color: 12

Bin 156: 8 of cap free
Amount of items: 3
Items: 
Size: 760959 Color: 3
Size: 125841 Color: 14
Size: 113193 Color: 2

Bin 157: 8 of cap free
Amount of items: 3
Items: 
Size: 583447 Color: 5
Size: 220175 Color: 8
Size: 196371 Color: 10

Bin 158: 8 of cap free
Amount of items: 2
Items: 
Size: 612049 Color: 4
Size: 387944 Color: 7

Bin 159: 8 of cap free
Amount of items: 2
Items: 
Size: 731378 Color: 5
Size: 268615 Color: 6

Bin 160: 9 of cap free
Amount of items: 3
Items: 
Size: 624974 Color: 15
Size: 188789 Color: 19
Size: 186229 Color: 7

Bin 161: 10 of cap free
Amount of items: 3
Items: 
Size: 429696 Color: 15
Size: 291844 Color: 17
Size: 278451 Color: 11

Bin 162: 10 of cap free
Amount of items: 2
Items: 
Size: 557559 Color: 6
Size: 442432 Color: 5

Bin 163: 10 of cap free
Amount of items: 3
Items: 
Size: 685570 Color: 12
Size: 165251 Color: 5
Size: 149170 Color: 17

Bin 164: 11 of cap free
Amount of items: 3
Items: 
Size: 638931 Color: 3
Size: 181971 Color: 6
Size: 179088 Color: 0

Bin 165: 11 of cap free
Amount of items: 2
Items: 
Size: 513401 Color: 15
Size: 486589 Color: 3

Bin 166: 11 of cap free
Amount of items: 3
Items: 
Size: 632976 Color: 19
Size: 198379 Color: 2
Size: 168635 Color: 6

Bin 167: 13 of cap free
Amount of items: 2
Items: 
Size: 725138 Color: 16
Size: 274850 Color: 5

Bin 168: 13 of cap free
Amount of items: 2
Items: 
Size: 618796 Color: 2
Size: 381192 Color: 8

Bin 169: 13 of cap free
Amount of items: 2
Items: 
Size: 623361 Color: 14
Size: 376627 Color: 9

Bin 170: 13 of cap free
Amount of items: 3
Items: 
Size: 618923 Color: 9
Size: 200158 Color: 2
Size: 180907 Color: 17

Bin 171: 13 of cap free
Amount of items: 2
Items: 
Size: 644845 Color: 16
Size: 355143 Color: 1

Bin 172: 13 of cap free
Amount of items: 3
Items: 
Size: 393146 Color: 16
Size: 317672 Color: 11
Size: 289170 Color: 1

Bin 173: 13 of cap free
Amount of items: 3
Items: 
Size: 367608 Color: 11
Size: 316920 Color: 11
Size: 315460 Color: 18

Bin 174: 14 of cap free
Amount of items: 3
Items: 
Size: 639397 Color: 5
Size: 205591 Color: 15
Size: 154999 Color: 10

Bin 175: 14 of cap free
Amount of items: 2
Items: 
Size: 502793 Color: 16
Size: 497194 Color: 12

Bin 176: 14 of cap free
Amount of items: 3
Items: 
Size: 591578 Color: 4
Size: 306296 Color: 16
Size: 102113 Color: 6

Bin 177: 14 of cap free
Amount of items: 2
Items: 
Size: 659533 Color: 6
Size: 340454 Color: 10

Bin 178: 15 of cap free
Amount of items: 3
Items: 
Size: 694603 Color: 0
Size: 175032 Color: 7
Size: 130351 Color: 9

Bin 179: 15 of cap free
Amount of items: 2
Items: 
Size: 623697 Color: 19
Size: 376289 Color: 10

Bin 180: 15 of cap free
Amount of items: 2
Items: 
Size: 650857 Color: 11
Size: 349129 Color: 10

Bin 181: 17 of cap free
Amount of items: 2
Items: 
Size: 565817 Color: 17
Size: 434167 Color: 4

Bin 182: 17 of cap free
Amount of items: 3
Items: 
Size: 709054 Color: 15
Size: 166191 Color: 16
Size: 124739 Color: 18

Bin 183: 18 of cap free
Amount of items: 2
Items: 
Size: 587937 Color: 17
Size: 412046 Color: 9

Bin 184: 18 of cap free
Amount of items: 2
Items: 
Size: 613298 Color: 1
Size: 386685 Color: 16

Bin 185: 18 of cap free
Amount of items: 2
Items: 
Size: 528116 Color: 7
Size: 471867 Color: 1

Bin 186: 18 of cap free
Amount of items: 2
Items: 
Size: 738103 Color: 13
Size: 261880 Color: 0

Bin 187: 18 of cap free
Amount of items: 3
Items: 
Size: 389288 Color: 10
Size: 306421 Color: 6
Size: 304274 Color: 10

Bin 188: 19 of cap free
Amount of items: 2
Items: 
Size: 535176 Color: 3
Size: 464806 Color: 11

Bin 189: 19 of cap free
Amount of items: 3
Items: 
Size: 650520 Color: 7
Size: 224214 Color: 9
Size: 125248 Color: 8

Bin 190: 19 of cap free
Amount of items: 2
Items: 
Size: 579202 Color: 14
Size: 420780 Color: 12

Bin 191: 19 of cap free
Amount of items: 3
Items: 
Size: 694230 Color: 16
Size: 187517 Color: 9
Size: 118235 Color: 10

Bin 192: 19 of cap free
Amount of items: 2
Items: 
Size: 652023 Color: 8
Size: 347959 Color: 11

Bin 193: 20 of cap free
Amount of items: 2
Items: 
Size: 752997 Color: 4
Size: 246984 Color: 0

Bin 194: 20 of cap free
Amount of items: 2
Items: 
Size: 537232 Color: 1
Size: 462749 Color: 7

Bin 195: 20 of cap free
Amount of items: 2
Items: 
Size: 607539 Color: 5
Size: 392442 Color: 2

Bin 196: 21 of cap free
Amount of items: 2
Items: 
Size: 656546 Color: 9
Size: 343434 Color: 0

Bin 197: 22 of cap free
Amount of items: 2
Items: 
Size: 640827 Color: 19
Size: 359152 Color: 2

Bin 198: 22 of cap free
Amount of items: 3
Items: 
Size: 659211 Color: 19
Size: 176806 Color: 3
Size: 163962 Color: 7

Bin 199: 23 of cap free
Amount of items: 3
Items: 
Size: 637921 Color: 9
Size: 181537 Color: 13
Size: 180520 Color: 8

Bin 200: 24 of cap free
Amount of items: 2
Items: 
Size: 517036 Color: 6
Size: 482941 Color: 13

Bin 201: 24 of cap free
Amount of items: 2
Items: 
Size: 589873 Color: 16
Size: 410104 Color: 3

Bin 202: 24 of cap free
Amount of items: 2
Items: 
Size: 580576 Color: 6
Size: 419401 Color: 19

Bin 203: 24 of cap free
Amount of items: 2
Items: 
Size: 578067 Color: 16
Size: 421910 Color: 2

Bin 204: 24 of cap free
Amount of items: 2
Items: 
Size: 650560 Color: 19
Size: 349417 Color: 4

Bin 205: 25 of cap free
Amount of items: 2
Items: 
Size: 738558 Color: 16
Size: 261418 Color: 15

Bin 206: 27 of cap free
Amount of items: 3
Items: 
Size: 647978 Color: 7
Size: 213900 Color: 4
Size: 138096 Color: 12

Bin 207: 29 of cap free
Amount of items: 2
Items: 
Size: 757167 Color: 3
Size: 242805 Color: 0

Bin 208: 30 of cap free
Amount of items: 2
Items: 
Size: 720358 Color: 16
Size: 279613 Color: 7

Bin 209: 30 of cap free
Amount of items: 2
Items: 
Size: 717782 Color: 17
Size: 282189 Color: 5

Bin 210: 30 of cap free
Amount of items: 2
Items: 
Size: 566159 Color: 7
Size: 433812 Color: 9

Bin 211: 30 of cap free
Amount of items: 2
Items: 
Size: 629980 Color: 8
Size: 369991 Color: 15

Bin 212: 32 of cap free
Amount of items: 2
Items: 
Size: 752297 Color: 12
Size: 247672 Color: 3

Bin 213: 32 of cap free
Amount of items: 3
Items: 
Size: 687404 Color: 1
Size: 168098 Color: 2
Size: 144467 Color: 17

Bin 214: 32 of cap free
Amount of items: 2
Items: 
Size: 795120 Color: 17
Size: 204849 Color: 13

Bin 215: 32 of cap free
Amount of items: 2
Items: 
Size: 628219 Color: 16
Size: 371750 Color: 14

Bin 216: 33 of cap free
Amount of items: 2
Items: 
Size: 590017 Color: 9
Size: 409951 Color: 14

Bin 217: 33 of cap free
Amount of items: 2
Items: 
Size: 535587 Color: 16
Size: 464381 Color: 8

Bin 218: 34 of cap free
Amount of items: 2
Items: 
Size: 530916 Color: 5
Size: 469051 Color: 10

Bin 219: 34 of cap free
Amount of items: 2
Items: 
Size: 520489 Color: 10
Size: 479478 Color: 6

Bin 220: 35 of cap free
Amount of items: 2
Items: 
Size: 511192 Color: 4
Size: 488774 Color: 11

Bin 221: 35 of cap free
Amount of items: 3
Items: 
Size: 546657 Color: 13
Size: 234230 Color: 17
Size: 219079 Color: 16

Bin 222: 35 of cap free
Amount of items: 2
Items: 
Size: 541808 Color: 7
Size: 458158 Color: 12

Bin 223: 35 of cap free
Amount of items: 3
Items: 
Size: 492593 Color: 3
Size: 255127 Color: 15
Size: 252246 Color: 4

Bin 224: 36 of cap free
Amount of items: 2
Items: 
Size: 760535 Color: 12
Size: 239430 Color: 9

Bin 225: 36 of cap free
Amount of items: 2
Items: 
Size: 503689 Color: 6
Size: 496276 Color: 8

Bin 226: 36 of cap free
Amount of items: 2
Items: 
Size: 736877 Color: 17
Size: 263088 Color: 13

Bin 227: 37 of cap free
Amount of items: 2
Items: 
Size: 544742 Color: 13
Size: 455222 Color: 18

Bin 228: 37 of cap free
Amount of items: 2
Items: 
Size: 585744 Color: 3
Size: 414220 Color: 11

Bin 229: 38 of cap free
Amount of items: 3
Items: 
Size: 548642 Color: 16
Size: 302988 Color: 8
Size: 148333 Color: 1

Bin 230: 38 of cap free
Amount of items: 2
Items: 
Size: 575486 Color: 19
Size: 424477 Color: 15

Bin 231: 39 of cap free
Amount of items: 2
Items: 
Size: 749976 Color: 7
Size: 249986 Color: 15

Bin 232: 39 of cap free
Amount of items: 2
Items: 
Size: 564197 Color: 8
Size: 435765 Color: 4

Bin 233: 40 of cap free
Amount of items: 2
Items: 
Size: 785775 Color: 2
Size: 214186 Color: 6

Bin 234: 40 of cap free
Amount of items: 2
Items: 
Size: 640676 Color: 17
Size: 359285 Color: 5

Bin 235: 41 of cap free
Amount of items: 2
Items: 
Size: 728222 Color: 14
Size: 271738 Color: 8

Bin 236: 41 of cap free
Amount of items: 2
Items: 
Size: 619531 Color: 17
Size: 380429 Color: 4

Bin 237: 41 of cap free
Amount of items: 2
Items: 
Size: 569936 Color: 3
Size: 430024 Color: 4

Bin 238: 41 of cap free
Amount of items: 2
Items: 
Size: 560160 Color: 17
Size: 439800 Color: 8

Bin 239: 41 of cap free
Amount of items: 2
Items: 
Size: 624051 Color: 7
Size: 375909 Color: 9

Bin 240: 42 of cap free
Amount of items: 2
Items: 
Size: 770171 Color: 15
Size: 229788 Color: 0

Bin 241: 42 of cap free
Amount of items: 2
Items: 
Size: 601755 Color: 4
Size: 398204 Color: 7

Bin 242: 43 of cap free
Amount of items: 2
Items: 
Size: 539498 Color: 14
Size: 460460 Color: 10

Bin 243: 43 of cap free
Amount of items: 2
Items: 
Size: 628503 Color: 17
Size: 371455 Color: 12

Bin 244: 43 of cap free
Amount of items: 2
Items: 
Size: 584971 Color: 7
Size: 414987 Color: 9

Bin 245: 43 of cap free
Amount of items: 2
Items: 
Size: 509690 Color: 9
Size: 490268 Color: 17

Bin 246: 43 of cap free
Amount of items: 3
Items: 
Size: 362832 Color: 1
Size: 325031 Color: 15
Size: 312095 Color: 15

Bin 247: 44 of cap free
Amount of items: 2
Items: 
Size: 679517 Color: 13
Size: 320440 Color: 1

Bin 248: 45 of cap free
Amount of items: 3
Items: 
Size: 656018 Color: 10
Size: 195651 Color: 12
Size: 148287 Color: 9

Bin 249: 46 of cap free
Amount of items: 2
Items: 
Size: 681063 Color: 15
Size: 318892 Color: 10

Bin 250: 46 of cap free
Amount of items: 2
Items: 
Size: 724580 Color: 2
Size: 275375 Color: 18

Bin 251: 46 of cap free
Amount of items: 2
Items: 
Size: 507726 Color: 3
Size: 492229 Color: 9

Bin 252: 48 of cap free
Amount of items: 2
Items: 
Size: 528442 Color: 9
Size: 471511 Color: 13

Bin 253: 48 of cap free
Amount of items: 2
Items: 
Size: 500346 Color: 13
Size: 499607 Color: 19

Bin 254: 49 of cap free
Amount of items: 2
Items: 
Size: 776911 Color: 0
Size: 223041 Color: 12

Bin 255: 49 of cap free
Amount of items: 2
Items: 
Size: 500871 Color: 7
Size: 499081 Color: 13

Bin 256: 49 of cap free
Amount of items: 2
Items: 
Size: 795732 Color: 0
Size: 204220 Color: 4

Bin 257: 51 of cap free
Amount of items: 3
Items: 
Size: 350881 Color: 15
Size: 334757 Color: 13
Size: 314312 Color: 12

Bin 258: 52 of cap free
Amount of items: 2
Items: 
Size: 655056 Color: 19
Size: 344893 Color: 4

Bin 259: 52 of cap free
Amount of items: 2
Items: 
Size: 597180 Color: 16
Size: 402769 Color: 11

Bin 260: 52 of cap free
Amount of items: 3
Items: 
Size: 733564 Color: 3
Size: 136416 Color: 13
Size: 129969 Color: 17

Bin 261: 53 of cap free
Amount of items: 2
Items: 
Size: 741607 Color: 18
Size: 258341 Color: 16

Bin 262: 53 of cap free
Amount of items: 2
Items: 
Size: 727582 Color: 5
Size: 272366 Color: 19

Bin 263: 53 of cap free
Amount of items: 3
Items: 
Size: 383088 Color: 16
Size: 310204 Color: 14
Size: 306656 Color: 4

Bin 264: 54 of cap free
Amount of items: 2
Items: 
Size: 758602 Color: 7
Size: 241345 Color: 10

Bin 265: 54 of cap free
Amount of items: 2
Items: 
Size: 508040 Color: 17
Size: 491907 Color: 0

Bin 266: 54 of cap free
Amount of items: 2
Items: 
Size: 559332 Color: 3
Size: 440615 Color: 10

Bin 267: 55 of cap free
Amount of items: 2
Items: 
Size: 665946 Color: 10
Size: 334000 Color: 16

Bin 268: 56 of cap free
Amount of items: 3
Items: 
Size: 637547 Color: 15
Size: 183107 Color: 0
Size: 179291 Color: 3

Bin 269: 56 of cap free
Amount of items: 2
Items: 
Size: 728748 Color: 16
Size: 271197 Color: 8

Bin 270: 58 of cap free
Amount of items: 2
Items: 
Size: 716222 Color: 1
Size: 283721 Color: 12

Bin 271: 59 of cap free
Amount of items: 2
Items: 
Size: 549198 Color: 15
Size: 450744 Color: 12

Bin 272: 60 of cap free
Amount of items: 3
Items: 
Size: 627779 Color: 7
Size: 194853 Color: 8
Size: 177309 Color: 14

Bin 273: 61 of cap free
Amount of items: 2
Items: 
Size: 576135 Color: 12
Size: 423805 Color: 0

Bin 274: 61 of cap free
Amount of items: 2
Items: 
Size: 733337 Color: 10
Size: 266603 Color: 17

Bin 275: 63 of cap free
Amount of items: 2
Items: 
Size: 577848 Color: 0
Size: 422090 Color: 8

Bin 276: 63 of cap free
Amount of items: 3
Items: 
Size: 545618 Color: 15
Size: 266686 Color: 17
Size: 187634 Color: 17

Bin 277: 64 of cap free
Amount of items: 3
Items: 
Size: 631463 Color: 16
Size: 191565 Color: 11
Size: 176909 Color: 18

Bin 278: 64 of cap free
Amount of items: 2
Items: 
Size: 754820 Color: 5
Size: 245117 Color: 8

Bin 279: 65 of cap free
Amount of items: 3
Items: 
Size: 546906 Color: 16
Size: 256999 Color: 2
Size: 196031 Color: 6

Bin 280: 66 of cap free
Amount of items: 2
Items: 
Size: 556178 Color: 14
Size: 443757 Color: 6

Bin 281: 67 of cap free
Amount of items: 2
Items: 
Size: 521635 Color: 13
Size: 478299 Color: 15

Bin 282: 67 of cap free
Amount of items: 2
Items: 
Size: 523592 Color: 17
Size: 476342 Color: 4

Bin 283: 67 of cap free
Amount of items: 2
Items: 
Size: 612862 Color: 5
Size: 387072 Color: 10

Bin 284: 68 of cap free
Amount of items: 2
Items: 
Size: 575384 Color: 10
Size: 424549 Color: 7

Bin 285: 68 of cap free
Amount of items: 2
Items: 
Size: 686827 Color: 7
Size: 313106 Color: 6

Bin 286: 69 of cap free
Amount of items: 3
Items: 
Size: 417366 Color: 15
Size: 324691 Color: 5
Size: 257875 Color: 3

Bin 287: 70 of cap free
Amount of items: 2
Items: 
Size: 587157 Color: 15
Size: 412774 Color: 17

Bin 288: 70 of cap free
Amount of items: 2
Items: 
Size: 561340 Color: 4
Size: 438591 Color: 17

Bin 289: 71 of cap free
Amount of items: 2
Items: 
Size: 648184 Color: 16
Size: 351746 Color: 2

Bin 290: 71 of cap free
Amount of items: 2
Items: 
Size: 669982 Color: 12
Size: 329948 Color: 11

Bin 291: 71 of cap free
Amount of items: 2
Items: 
Size: 553722 Color: 5
Size: 446208 Color: 15

Bin 292: 71 of cap free
Amount of items: 2
Items: 
Size: 512650 Color: 8
Size: 487280 Color: 6

Bin 293: 72 of cap free
Amount of items: 2
Items: 
Size: 512038 Color: 5
Size: 487891 Color: 4

Bin 294: 73 of cap free
Amount of items: 2
Items: 
Size: 597475 Color: 19
Size: 402453 Color: 17

Bin 295: 73 of cap free
Amount of items: 3
Items: 
Size: 377319 Color: 19
Size: 313970 Color: 1
Size: 308639 Color: 4

Bin 296: 74 of cap free
Amount of items: 2
Items: 
Size: 797684 Color: 2
Size: 202243 Color: 17

Bin 297: 74 of cap free
Amount of items: 2
Items: 
Size: 548758 Color: 16
Size: 451169 Color: 6

Bin 298: 75 of cap free
Amount of items: 2
Items: 
Size: 724874 Color: 13
Size: 275052 Color: 5

Bin 299: 75 of cap free
Amount of items: 2
Items: 
Size: 534992 Color: 10
Size: 464934 Color: 1

Bin 300: 76 of cap free
Amount of items: 2
Items: 
Size: 526163 Color: 5
Size: 473762 Color: 12

Bin 301: 77 of cap free
Amount of items: 2
Items: 
Size: 730128 Color: 0
Size: 269796 Color: 3

Bin 302: 77 of cap free
Amount of items: 2
Items: 
Size: 512294 Color: 17
Size: 487630 Color: 7

Bin 303: 77 of cap free
Amount of items: 2
Items: 
Size: 514635 Color: 7
Size: 485289 Color: 0

Bin 304: 77 of cap free
Amount of items: 2
Items: 
Size: 578262 Color: 7
Size: 421662 Color: 16

Bin 305: 79 of cap free
Amount of items: 2
Items: 
Size: 622843 Color: 5
Size: 377079 Color: 15

Bin 306: 79 of cap free
Amount of items: 2
Items: 
Size: 695414 Color: 5
Size: 304508 Color: 6

Bin 307: 80 of cap free
Amount of items: 2
Items: 
Size: 626333 Color: 2
Size: 373588 Color: 19

Bin 308: 80 of cap free
Amount of items: 2
Items: 
Size: 618623 Color: 6
Size: 381298 Color: 8

Bin 309: 81 of cap free
Amount of items: 3
Items: 
Size: 695304 Color: 19
Size: 185977 Color: 11
Size: 118639 Color: 0

Bin 310: 81 of cap free
Amount of items: 2
Items: 
Size: 734726 Color: 9
Size: 265194 Color: 18

Bin 311: 82 of cap free
Amount of items: 2
Items: 
Size: 650337 Color: 16
Size: 349582 Color: 15

Bin 312: 82 of cap free
Amount of items: 2
Items: 
Size: 529709 Color: 19
Size: 470210 Color: 0

Bin 313: 84 of cap free
Amount of items: 2
Items: 
Size: 715095 Color: 1
Size: 284822 Color: 0

Bin 314: 84 of cap free
Amount of items: 2
Items: 
Size: 632397 Color: 15
Size: 367520 Color: 14

Bin 315: 84 of cap free
Amount of items: 2
Items: 
Size: 669085 Color: 15
Size: 330832 Color: 12

Bin 316: 84 of cap free
Amount of items: 2
Items: 
Size: 556028 Color: 4
Size: 443889 Color: 7

Bin 317: 85 of cap free
Amount of items: 2
Items: 
Size: 703676 Color: 0
Size: 296240 Color: 3

Bin 318: 85 of cap free
Amount of items: 2
Items: 
Size: 752110 Color: 19
Size: 247806 Color: 6

Bin 319: 85 of cap free
Amount of items: 2
Items: 
Size: 769561 Color: 15
Size: 230355 Color: 14

Bin 320: 85 of cap free
Amount of items: 2
Items: 
Size: 639809 Color: 16
Size: 360107 Color: 3

Bin 321: 86 of cap free
Amount of items: 2
Items: 
Size: 650209 Color: 10
Size: 349706 Color: 12

Bin 322: 86 of cap free
Amount of items: 3
Items: 
Size: 462931 Color: 18
Size: 383252 Color: 13
Size: 153732 Color: 19

Bin 323: 88 of cap free
Amount of items: 2
Items: 
Size: 690681 Color: 12
Size: 309232 Color: 4

Bin 324: 88 of cap free
Amount of items: 3
Items: 
Size: 712802 Color: 8
Size: 150371 Color: 19
Size: 136740 Color: 4

Bin 325: 88 of cap free
Amount of items: 2
Items: 
Size: 573014 Color: 14
Size: 426899 Color: 2

Bin 326: 88 of cap free
Amount of items: 2
Items: 
Size: 501841 Color: 3
Size: 498072 Color: 15

Bin 327: 90 of cap free
Amount of items: 2
Items: 
Size: 533959 Color: 5
Size: 465952 Color: 9

Bin 328: 90 of cap free
Amount of items: 2
Items: 
Size: 527966 Color: 19
Size: 471945 Color: 10

Bin 329: 91 of cap free
Amount of items: 2
Items: 
Size: 612949 Color: 10
Size: 386961 Color: 9

Bin 330: 91 of cap free
Amount of items: 2
Items: 
Size: 550633 Color: 19
Size: 449277 Color: 2

Bin 331: 91 of cap free
Amount of items: 2
Items: 
Size: 541193 Color: 0
Size: 458717 Color: 11

Bin 332: 92 of cap free
Amount of items: 2
Items: 
Size: 551681 Color: 16
Size: 448228 Color: 0

Bin 333: 93 of cap free
Amount of items: 2
Items: 
Size: 715743 Color: 7
Size: 284165 Color: 15

Bin 334: 93 of cap free
Amount of items: 2
Items: 
Size: 552759 Color: 11
Size: 447149 Color: 19

Bin 335: 93 of cap free
Amount of items: 3
Items: 
Size: 572484 Color: 15
Size: 269099 Color: 7
Size: 158325 Color: 7

Bin 336: 93 of cap free
Amount of items: 2
Items: 
Size: 625441 Color: 3
Size: 374467 Color: 13

Bin 337: 94 of cap free
Amount of items: 2
Items: 
Size: 510231 Color: 5
Size: 489676 Color: 11

Bin 338: 96 of cap free
Amount of items: 2
Items: 
Size: 537176 Color: 18
Size: 462729 Color: 9

Bin 339: 97 of cap free
Amount of items: 2
Items: 
Size: 739069 Color: 1
Size: 260835 Color: 17

Bin 340: 98 of cap free
Amount of items: 2
Items: 
Size: 593014 Color: 11
Size: 406889 Color: 5

Bin 341: 100 of cap free
Amount of items: 2
Items: 
Size: 653912 Color: 17
Size: 345989 Color: 13

Bin 342: 101 of cap free
Amount of items: 2
Items: 
Size: 570988 Color: 11
Size: 428912 Color: 18

Bin 343: 101 of cap free
Amount of items: 2
Items: 
Size: 588321 Color: 18
Size: 411579 Color: 14

Bin 344: 102 of cap free
Amount of items: 2
Items: 
Size: 649693 Color: 10
Size: 350206 Color: 8

Bin 345: 102 of cap free
Amount of items: 2
Items: 
Size: 684693 Color: 10
Size: 315206 Color: 6

Bin 346: 103 of cap free
Amount of items: 2
Items: 
Size: 700505 Color: 15
Size: 299393 Color: 11

Bin 347: 104 of cap free
Amount of items: 2
Items: 
Size: 508781 Color: 5
Size: 491116 Color: 1

Bin 348: 105 of cap free
Amount of items: 3
Items: 
Size: 450450 Color: 0
Size: 274822 Color: 14
Size: 274624 Color: 7

Bin 349: 107 of cap free
Amount of items: 2
Items: 
Size: 674345 Color: 19
Size: 325549 Color: 3

Bin 350: 107 of cap free
Amount of items: 2
Items: 
Size: 637786 Color: 7
Size: 362108 Color: 17

Bin 351: 108 of cap free
Amount of items: 2
Items: 
Size: 636063 Color: 18
Size: 363830 Color: 5

Bin 352: 109 of cap free
Amount of items: 2
Items: 
Size: 606328 Color: 5
Size: 393564 Color: 10

Bin 353: 110 of cap free
Amount of items: 2
Items: 
Size: 707095 Color: 2
Size: 292796 Color: 16

Bin 354: 111 of cap free
Amount of items: 2
Items: 
Size: 717563 Color: 7
Size: 282327 Color: 16

Bin 355: 112 of cap free
Amount of items: 2
Items: 
Size: 782137 Color: 8
Size: 217752 Color: 19

Bin 356: 112 of cap free
Amount of items: 2
Items: 
Size: 740693 Color: 19
Size: 259196 Color: 3

Bin 357: 113 of cap free
Amount of items: 2
Items: 
Size: 525742 Color: 1
Size: 474146 Color: 5

Bin 358: 114 of cap free
Amount of items: 2
Items: 
Size: 719607 Color: 5
Size: 280280 Color: 3

Bin 359: 114 of cap free
Amount of items: 2
Items: 
Size: 641997 Color: 9
Size: 357890 Color: 4

Bin 360: 115 of cap free
Amount of items: 2
Items: 
Size: 668940 Color: 16
Size: 330946 Color: 19

Bin 361: 116 of cap free
Amount of items: 2
Items: 
Size: 767752 Color: 13
Size: 232133 Color: 17

Bin 362: 116 of cap free
Amount of items: 2
Items: 
Size: 728621 Color: 10
Size: 271264 Color: 14

Bin 363: 117 of cap free
Amount of items: 2
Items: 
Size: 682409 Color: 4
Size: 317475 Color: 11

Bin 364: 120 of cap free
Amount of items: 2
Items: 
Size: 709505 Color: 18
Size: 290376 Color: 14

Bin 365: 121 of cap free
Amount of items: 2
Items: 
Size: 636867 Color: 17
Size: 363013 Color: 7

Bin 366: 122 of cap free
Amount of items: 2
Items: 
Size: 608535 Color: 19
Size: 391344 Color: 6

Bin 367: 122 of cap free
Amount of items: 2
Items: 
Size: 586536 Color: 19
Size: 413343 Color: 3

Bin 368: 123 of cap free
Amount of items: 2
Items: 
Size: 660331 Color: 17
Size: 339547 Color: 8

Bin 369: 123 of cap free
Amount of items: 2
Items: 
Size: 638197 Color: 2
Size: 361681 Color: 12

Bin 370: 126 of cap free
Amount of items: 2
Items: 
Size: 500105 Color: 18
Size: 499770 Color: 16

Bin 371: 126 of cap free
Amount of items: 2
Items: 
Size: 652724 Color: 14
Size: 347151 Color: 18

Bin 372: 127 of cap free
Amount of items: 2
Items: 
Size: 536561 Color: 16
Size: 463313 Color: 5

Bin 373: 128 of cap free
Amount of items: 2
Items: 
Size: 688590 Color: 0
Size: 311283 Color: 2

Bin 374: 128 of cap free
Amount of items: 2
Items: 
Size: 702862 Color: 18
Size: 297011 Color: 11

Bin 375: 129 of cap free
Amount of items: 2
Items: 
Size: 600794 Color: 4
Size: 399078 Color: 3

Bin 376: 129 of cap free
Amount of items: 3
Items: 
Size: 695785 Color: 14
Size: 169948 Color: 10
Size: 134139 Color: 1

Bin 377: 130 of cap free
Amount of items: 2
Items: 
Size: 758253 Color: 12
Size: 241618 Color: 7

Bin 378: 130 of cap free
Amount of items: 2
Items: 
Size: 630381 Color: 10
Size: 369490 Color: 9

Bin 379: 132 of cap free
Amount of items: 2
Items: 
Size: 658533 Color: 0
Size: 341336 Color: 9

Bin 380: 132 of cap free
Amount of items: 3
Items: 
Size: 703291 Color: 19
Size: 162619 Color: 5
Size: 133959 Color: 10

Bin 381: 133 of cap free
Amount of items: 2
Items: 
Size: 761520 Color: 0
Size: 238348 Color: 5

Bin 382: 133 of cap free
Amount of items: 2
Items: 
Size: 638832 Color: 14
Size: 361036 Color: 7

Bin 383: 134 of cap free
Amount of items: 2
Items: 
Size: 573524 Color: 17
Size: 426343 Color: 9

Bin 384: 135 of cap free
Amount of items: 2
Items: 
Size: 520859 Color: 19
Size: 479007 Color: 10

Bin 385: 136 of cap free
Amount of items: 2
Items: 
Size: 516596 Color: 18
Size: 483269 Color: 8

Bin 386: 137 of cap free
Amount of items: 2
Items: 
Size: 603618 Color: 18
Size: 396246 Color: 3

Bin 387: 137 of cap free
Amount of items: 2
Items: 
Size: 609443 Color: 14
Size: 390421 Color: 7

Bin 388: 139 of cap free
Amount of items: 2
Items: 
Size: 529462 Color: 18
Size: 470400 Color: 13

Bin 389: 139 of cap free
Amount of items: 2
Items: 
Size: 558070 Color: 9
Size: 441792 Color: 18

Bin 390: 141 of cap free
Amount of items: 2
Items: 
Size: 718638 Color: 14
Size: 281222 Color: 7

Bin 391: 141 of cap free
Amount of items: 2
Items: 
Size: 526490 Color: 1
Size: 473370 Color: 4

Bin 392: 142 of cap free
Amount of items: 2
Items: 
Size: 525155 Color: 8
Size: 474704 Color: 13

Bin 393: 143 of cap free
Amount of items: 2
Items: 
Size: 719869 Color: 6
Size: 279989 Color: 14

Bin 394: 144 of cap free
Amount of items: 2
Items: 
Size: 643651 Color: 12
Size: 356206 Color: 5

Bin 395: 145 of cap free
Amount of items: 2
Items: 
Size: 643180 Color: 5
Size: 356676 Color: 12

Bin 396: 145 of cap free
Amount of items: 2
Items: 
Size: 584039 Color: 11
Size: 415817 Color: 0

Bin 397: 147 of cap free
Amount of items: 2
Items: 
Size: 557178 Color: 3
Size: 442676 Color: 0

Bin 398: 147 of cap free
Amount of items: 2
Items: 
Size: 629270 Color: 19
Size: 370584 Color: 12

Bin 399: 147 of cap free
Amount of items: 2
Items: 
Size: 699744 Color: 0
Size: 300110 Color: 9

Bin 400: 149 of cap free
Amount of items: 3
Items: 
Size: 624036 Color: 19
Size: 198569 Color: 1
Size: 177247 Color: 14

Bin 401: 149 of cap free
Amount of items: 2
Items: 
Size: 780932 Color: 13
Size: 218920 Color: 3

Bin 402: 150 of cap free
Amount of items: 3
Items: 
Size: 698308 Color: 12
Size: 165127 Color: 8
Size: 136416 Color: 1

Bin 403: 151 of cap free
Amount of items: 2
Items: 
Size: 542276 Color: 14
Size: 457574 Color: 5

Bin 404: 151 of cap free
Amount of items: 2
Items: 
Size: 599195 Color: 19
Size: 400655 Color: 12

Bin 405: 151 of cap free
Amount of items: 2
Items: 
Size: 616361 Color: 0
Size: 383489 Color: 17

Bin 406: 151 of cap free
Amount of items: 2
Items: 
Size: 607920 Color: 5
Size: 391930 Color: 4

Bin 407: 152 of cap free
Amount of items: 2
Items: 
Size: 551184 Color: 18
Size: 448665 Color: 2

Bin 408: 153 of cap free
Amount of items: 2
Items: 
Size: 664933 Color: 18
Size: 334915 Color: 16

Bin 409: 154 of cap free
Amount of items: 2
Items: 
Size: 769280 Color: 0
Size: 230567 Color: 13

Bin 410: 155 of cap free
Amount of items: 2
Items: 
Size: 604008 Color: 0
Size: 395838 Color: 15

Bin 411: 155 of cap free
Amount of items: 2
Items: 
Size: 557368 Color: 12
Size: 442478 Color: 3

Bin 412: 156 of cap free
Amount of items: 2
Items: 
Size: 799709 Color: 8
Size: 200136 Color: 16

Bin 413: 159 of cap free
Amount of items: 2
Items: 
Size: 651274 Color: 16
Size: 348568 Color: 9

Bin 414: 160 of cap free
Amount of items: 2
Items: 
Size: 643222 Color: 12
Size: 356619 Color: 0

Bin 415: 160 of cap free
Amount of items: 2
Items: 
Size: 544137 Color: 12
Size: 455704 Color: 13

Bin 416: 161 of cap free
Amount of items: 2
Items: 
Size: 620216 Color: 1
Size: 379624 Color: 16

Bin 417: 164 of cap free
Amount of items: 2
Items: 
Size: 784212 Color: 12
Size: 215625 Color: 18

Bin 418: 167 of cap free
Amount of items: 2
Items: 
Size: 565615 Color: 9
Size: 434219 Color: 15

Bin 419: 168 of cap free
Amount of items: 2
Items: 
Size: 756512 Color: 18
Size: 243321 Color: 8

Bin 420: 168 of cap free
Amount of items: 2
Items: 
Size: 742031 Color: 12
Size: 257802 Color: 9

Bin 421: 168 of cap free
Amount of items: 2
Items: 
Size: 757893 Color: 10
Size: 241940 Color: 5

Bin 422: 169 of cap free
Amount of items: 2
Items: 
Size: 772852 Color: 3
Size: 226980 Color: 0

Bin 423: 169 of cap free
Amount of items: 2
Items: 
Size: 680175 Color: 17
Size: 319657 Color: 7

Bin 424: 170 of cap free
Amount of items: 2
Items: 
Size: 732805 Color: 15
Size: 267026 Color: 14

Bin 425: 170 of cap free
Amount of items: 2
Items: 
Size: 506025 Color: 18
Size: 493806 Color: 15

Bin 426: 171 of cap free
Amount of items: 2
Items: 
Size: 681733 Color: 4
Size: 318097 Color: 15

Bin 427: 173 of cap free
Amount of items: 2
Items: 
Size: 506913 Color: 2
Size: 492915 Color: 5

Bin 428: 173 of cap free
Amount of items: 2
Items: 
Size: 777833 Color: 19
Size: 221995 Color: 1

Bin 429: 173 of cap free
Amount of items: 3
Items: 
Size: 398676 Color: 4
Size: 306818 Color: 18
Size: 294334 Color: 10

Bin 430: 174 of cap free
Amount of items: 2
Items: 
Size: 601714 Color: 9
Size: 398113 Color: 12

Bin 431: 175 of cap free
Amount of items: 2
Items: 
Size: 550904 Color: 12
Size: 448922 Color: 17

Bin 432: 176 of cap free
Amount of items: 2
Items: 
Size: 710113 Color: 9
Size: 289712 Color: 3

Bin 433: 178 of cap free
Amount of items: 2
Items: 
Size: 630370 Color: 3
Size: 369453 Color: 14

Bin 434: 179 of cap free
Amount of items: 2
Items: 
Size: 780067 Color: 0
Size: 219755 Color: 15

Bin 435: 182 of cap free
Amount of items: 2
Items: 
Size: 665752 Color: 10
Size: 334067 Color: 4

Bin 436: 183 of cap free
Amount of items: 2
Items: 
Size: 708654 Color: 2
Size: 291164 Color: 1

Bin 437: 183 of cap free
Amount of items: 2
Items: 
Size: 614825 Color: 7
Size: 384993 Color: 2

Bin 438: 184 of cap free
Amount of items: 2
Items: 
Size: 577840 Color: 3
Size: 421977 Color: 10

Bin 439: 184 of cap free
Amount of items: 2
Items: 
Size: 562245 Color: 4
Size: 437572 Color: 8

Bin 440: 184 of cap free
Amount of items: 2
Items: 
Size: 771291 Color: 14
Size: 228526 Color: 4

Bin 441: 184 of cap free
Amount of items: 2
Items: 
Size: 556167 Color: 17
Size: 443650 Color: 3

Bin 442: 186 of cap free
Amount of items: 2
Items: 
Size: 629777 Color: 18
Size: 370038 Color: 10

Bin 443: 188 of cap free
Amount of items: 2
Items: 
Size: 646769 Color: 9
Size: 353044 Color: 15

Bin 444: 189 of cap free
Amount of items: 2
Items: 
Size: 572727 Color: 4
Size: 427085 Color: 2

Bin 445: 191 of cap free
Amount of items: 2
Items: 
Size: 683271 Color: 15
Size: 316539 Color: 4

Bin 446: 191 of cap free
Amount of items: 2
Items: 
Size: 715037 Color: 13
Size: 284773 Color: 7

Bin 447: 191 of cap free
Amount of items: 2
Items: 
Size: 546427 Color: 1
Size: 453383 Color: 14

Bin 448: 196 of cap free
Amount of items: 2
Items: 
Size: 751358 Color: 13
Size: 248447 Color: 7

Bin 449: 198 of cap free
Amount of items: 2
Items: 
Size: 570913 Color: 0
Size: 428890 Color: 12

Bin 450: 198 of cap free
Amount of items: 2
Items: 
Size: 785193 Color: 7
Size: 214610 Color: 2

Bin 451: 199 of cap free
Amount of items: 2
Items: 
Size: 608893 Color: 13
Size: 390909 Color: 8

Bin 452: 202 of cap free
Amount of items: 2
Items: 
Size: 757284 Color: 5
Size: 242515 Color: 2

Bin 453: 202 of cap free
Amount of items: 2
Items: 
Size: 774422 Color: 10
Size: 225377 Color: 15

Bin 454: 203 of cap free
Amount of items: 2
Items: 
Size: 515358 Color: 11
Size: 484440 Color: 2

Bin 455: 206 of cap free
Amount of items: 2
Items: 
Size: 670312 Color: 0
Size: 329483 Color: 1

Bin 456: 206 of cap free
Amount of items: 2
Items: 
Size: 666013 Color: 2
Size: 333782 Color: 13

Bin 457: 206 of cap free
Amount of items: 2
Items: 
Size: 774055 Color: 3
Size: 225740 Color: 0

Bin 458: 208 of cap free
Amount of items: 2
Items: 
Size: 594897 Color: 1
Size: 404896 Color: 3

Bin 459: 209 of cap free
Amount of items: 2
Items: 
Size: 510165 Color: 9
Size: 489627 Color: 16

Bin 460: 210 of cap free
Amount of items: 2
Items: 
Size: 758995 Color: 14
Size: 240796 Color: 13

Bin 461: 210 of cap free
Amount of items: 2
Items: 
Size: 710093 Color: 12
Size: 289698 Color: 3

Bin 462: 210 of cap free
Amount of items: 2
Items: 
Size: 671925 Color: 10
Size: 327866 Color: 12

Bin 463: 213 of cap free
Amount of items: 2
Items: 
Size: 721759 Color: 7
Size: 278029 Color: 5

Bin 464: 213 of cap free
Amount of items: 2
Items: 
Size: 613887 Color: 7
Size: 385901 Color: 19

Bin 465: 213 of cap free
Amount of items: 2
Items: 
Size: 789709 Color: 0
Size: 210079 Color: 15

Bin 466: 213 of cap free
Amount of items: 2
Items: 
Size: 589636 Color: 5
Size: 410152 Color: 15

Bin 467: 217 of cap free
Amount of items: 2
Items: 
Size: 501361 Color: 2
Size: 498423 Color: 16

Bin 468: 218 of cap free
Amount of items: 2
Items: 
Size: 548211 Color: 5
Size: 451572 Color: 10

Bin 469: 219 of cap free
Amount of items: 2
Items: 
Size: 529395 Color: 18
Size: 470387 Color: 19

Bin 470: 219 of cap free
Amount of items: 2
Items: 
Size: 581274 Color: 12
Size: 418508 Color: 10

Bin 471: 220 of cap free
Amount of items: 2
Items: 
Size: 602482 Color: 12
Size: 397299 Color: 11

Bin 472: 221 of cap free
Amount of items: 2
Items: 
Size: 637683 Color: 3
Size: 362097 Color: 2

Bin 473: 222 of cap free
Amount of items: 2
Items: 
Size: 778091 Color: 14
Size: 221688 Color: 16

Bin 474: 224 of cap free
Amount of items: 2
Items: 
Size: 673497 Color: 5
Size: 326280 Color: 2

Bin 475: 224 of cap free
Amount of items: 2
Items: 
Size: 706368 Color: 4
Size: 293409 Color: 1

Bin 476: 225 of cap free
Amount of items: 2
Items: 
Size: 740404 Color: 10
Size: 259372 Color: 5

Bin 477: 226 of cap free
Amount of items: 2
Items: 
Size: 502814 Color: 7
Size: 496961 Color: 12

Bin 478: 228 of cap free
Amount of items: 2
Items: 
Size: 597016 Color: 12
Size: 402757 Color: 15

Bin 479: 228 of cap free
Amount of items: 2
Items: 
Size: 564353 Color: 7
Size: 435420 Color: 2

Bin 480: 229 of cap free
Amount of items: 2
Items: 
Size: 703203 Color: 7
Size: 296569 Color: 18

Bin 481: 235 of cap free
Amount of items: 2
Items: 
Size: 767215 Color: 6
Size: 232551 Color: 7

Bin 482: 237 of cap free
Amount of items: 2
Items: 
Size: 632274 Color: 9
Size: 367490 Color: 8

Bin 483: 238 of cap free
Amount of items: 2
Items: 
Size: 542275 Color: 15
Size: 457488 Color: 19

Bin 484: 239 of cap free
Amount of items: 2
Items: 
Size: 621882 Color: 8
Size: 377880 Color: 14

Bin 485: 240 of cap free
Amount of items: 2
Items: 
Size: 592899 Color: 2
Size: 406862 Color: 9

Bin 486: 241 of cap free
Amount of items: 2
Items: 
Size: 619478 Color: 5
Size: 380282 Color: 10

Bin 487: 241 of cap free
Amount of items: 3
Items: 
Size: 707761 Color: 9
Size: 182041 Color: 9
Size: 109958 Color: 0

Bin 488: 242 of cap free
Amount of items: 2
Items: 
Size: 713583 Color: 4
Size: 286176 Color: 11

Bin 489: 244 of cap free
Amount of items: 2
Items: 
Size: 748556 Color: 12
Size: 251201 Color: 4

Bin 490: 248 of cap free
Amount of items: 2
Items: 
Size: 569327 Color: 18
Size: 430426 Color: 15

Bin 491: 249 of cap free
Amount of items: 2
Items: 
Size: 563195 Color: 16
Size: 436557 Color: 13

Bin 492: 250 of cap free
Amount of items: 2
Items: 
Size: 760079 Color: 11
Size: 239672 Color: 0

Bin 493: 252 of cap free
Amount of items: 2
Items: 
Size: 615486 Color: 19
Size: 384263 Color: 7

Bin 494: 256 of cap free
Amount of items: 2
Items: 
Size: 764192 Color: 17
Size: 235553 Color: 2

Bin 495: 257 of cap free
Amount of items: 2
Items: 
Size: 753038 Color: 18
Size: 246706 Color: 12

Bin 496: 258 of cap free
Amount of items: 2
Items: 
Size: 604843 Color: 6
Size: 394900 Color: 9

Bin 497: 260 of cap free
Amount of items: 2
Items: 
Size: 722002 Color: 19
Size: 277739 Color: 17

Bin 498: 261 of cap free
Amount of items: 2
Items: 
Size: 520332 Color: 11
Size: 479408 Color: 6

Bin 499: 265 of cap free
Amount of items: 2
Items: 
Size: 744493 Color: 19
Size: 255243 Color: 17

Bin 500: 266 of cap free
Amount of items: 2
Items: 
Size: 605821 Color: 1
Size: 393914 Color: 7

Bin 501: 266 of cap free
Amount of items: 2
Items: 
Size: 755247 Color: 13
Size: 244488 Color: 12

Bin 502: 266 of cap free
Amount of items: 2
Items: 
Size: 783476 Color: 19
Size: 216259 Color: 9

Bin 503: 269 of cap free
Amount of items: 2
Items: 
Size: 765933 Color: 12
Size: 233799 Color: 14

Bin 504: 270 of cap free
Amount of items: 2
Items: 
Size: 508921 Color: 11
Size: 490810 Color: 19

Bin 505: 270 of cap free
Amount of items: 2
Items: 
Size: 558491 Color: 10
Size: 441240 Color: 7

Bin 506: 271 of cap free
Amount of items: 2
Items: 
Size: 547308 Color: 4
Size: 452422 Color: 8

Bin 507: 271 of cap free
Amount of items: 2
Items: 
Size: 590584 Color: 18
Size: 409146 Color: 14

Bin 508: 272 of cap free
Amount of items: 2
Items: 
Size: 638117 Color: 1
Size: 361612 Color: 7

Bin 509: 273 of cap free
Amount of items: 2
Items: 
Size: 710455 Color: 17
Size: 289273 Color: 11

Bin 510: 280 of cap free
Amount of items: 2
Items: 
Size: 599551 Color: 9
Size: 400170 Color: 4

Bin 511: 285 of cap free
Amount of items: 2
Items: 
Size: 773754 Color: 5
Size: 225962 Color: 8

Bin 512: 286 of cap free
Amount of items: 2
Items: 
Size: 507689 Color: 6
Size: 492026 Color: 19

Bin 513: 291 of cap free
Amount of items: 2
Items: 
Size: 641988 Color: 7
Size: 357722 Color: 17

Bin 514: 291 of cap free
Amount of items: 2
Items: 
Size: 743268 Color: 11
Size: 256442 Color: 10

Bin 515: 291 of cap free
Amount of items: 2
Items: 
Size: 620477 Color: 15
Size: 379233 Color: 16

Bin 516: 293 of cap free
Amount of items: 2
Items: 
Size: 609587 Color: 14
Size: 390121 Color: 11

Bin 517: 295 of cap free
Amount of items: 2
Items: 
Size: 566500 Color: 15
Size: 433206 Color: 18

Bin 518: 298 of cap free
Amount of items: 2
Items: 
Size: 594025 Color: 4
Size: 405678 Color: 14

Bin 519: 299 of cap free
Amount of items: 2
Items: 
Size: 707752 Color: 15
Size: 291950 Color: 6

Bin 520: 301 of cap free
Amount of items: 2
Items: 
Size: 607320 Color: 6
Size: 392380 Color: 7

Bin 521: 301 of cap free
Amount of items: 2
Items: 
Size: 794122 Color: 16
Size: 205578 Color: 13

Bin 522: 303 of cap free
Amount of items: 2
Items: 
Size: 616658 Color: 2
Size: 383040 Color: 15

Bin 523: 304 of cap free
Amount of items: 2
Items: 
Size: 553887 Color: 5
Size: 445810 Color: 18

Bin 524: 304 of cap free
Amount of items: 2
Items: 
Size: 781656 Color: 11
Size: 218041 Color: 12

Bin 525: 307 of cap free
Amount of items: 2
Items: 
Size: 655249 Color: 13
Size: 344445 Color: 19

Bin 526: 311 of cap free
Amount of items: 2
Items: 
Size: 673051 Color: 7
Size: 326639 Color: 5

Bin 527: 312 of cap free
Amount of items: 2
Items: 
Size: 505896 Color: 1
Size: 493793 Color: 9

Bin 528: 312 of cap free
Amount of items: 2
Items: 
Size: 640585 Color: 7
Size: 359104 Color: 4

Bin 529: 313 of cap free
Amount of items: 2
Items: 
Size: 634560 Color: 9
Size: 365128 Color: 5

Bin 530: 313 of cap free
Amount of items: 2
Items: 
Size: 561263 Color: 17
Size: 438425 Color: 12

Bin 531: 314 of cap free
Amount of items: 2
Items: 
Size: 762081 Color: 17
Size: 237606 Color: 19

Bin 532: 315 of cap free
Amount of items: 2
Items: 
Size: 628616 Color: 1
Size: 371070 Color: 9

Bin 533: 317 of cap free
Amount of items: 2
Items: 
Size: 555805 Color: 5
Size: 443879 Color: 11

Bin 534: 318 of cap free
Amount of items: 2
Items: 
Size: 704536 Color: 18
Size: 295147 Color: 3

Bin 535: 320 of cap free
Amount of items: 2
Items: 
Size: 557582 Color: 7
Size: 442099 Color: 5

Bin 536: 323 of cap free
Amount of items: 2
Items: 
Size: 501681 Color: 14
Size: 497997 Color: 0

Bin 537: 325 of cap free
Amount of items: 2
Items: 
Size: 727542 Color: 14
Size: 272134 Color: 2

Bin 538: 327 of cap free
Amount of items: 2
Items: 
Size: 554399 Color: 7
Size: 445275 Color: 12

Bin 539: 327 of cap free
Amount of items: 2
Items: 
Size: 758385 Color: 2
Size: 241289 Color: 11

Bin 540: 332 of cap free
Amount of items: 2
Items: 
Size: 681217 Color: 9
Size: 318452 Color: 16

Bin 541: 332 of cap free
Amount of items: 2
Items: 
Size: 771581 Color: 17
Size: 228088 Color: 10

Bin 542: 332 of cap free
Amount of items: 2
Items: 
Size: 653869 Color: 5
Size: 345800 Color: 12

Bin 543: 332 of cap free
Amount of items: 2
Items: 
Size: 531882 Color: 6
Size: 467787 Color: 15

Bin 544: 335 of cap free
Amount of items: 2
Items: 
Size: 782068 Color: 2
Size: 217598 Color: 7

Bin 545: 339 of cap free
Amount of items: 2
Items: 
Size: 683165 Color: 1
Size: 316497 Color: 11

Bin 546: 342 of cap free
Amount of items: 2
Items: 
Size: 660752 Color: 2
Size: 338907 Color: 15

Bin 547: 345 of cap free
Amount of items: 2
Items: 
Size: 644768 Color: 18
Size: 354888 Color: 0

Bin 548: 347 of cap free
Amount of items: 2
Items: 
Size: 737275 Color: 17
Size: 262379 Color: 2

Bin 549: 348 of cap free
Amount of items: 2
Items: 
Size: 521735 Color: 3
Size: 477918 Color: 11

Bin 550: 350 of cap free
Amount of items: 2
Items: 
Size: 668406 Color: 6
Size: 331245 Color: 8

Bin 551: 361 of cap free
Amount of items: 2
Items: 
Size: 656595 Color: 1
Size: 343045 Color: 6

Bin 552: 361 of cap free
Amount of items: 2
Items: 
Size: 797418 Color: 19
Size: 202222 Color: 11

Bin 553: 363 of cap free
Amount of items: 2
Items: 
Size: 694147 Color: 1
Size: 305491 Color: 4

Bin 554: 365 of cap free
Amount of items: 2
Items: 
Size: 668939 Color: 14
Size: 330697 Color: 16

Bin 555: 367 of cap free
Amount of items: 2
Items: 
Size: 519737 Color: 15
Size: 479897 Color: 5

Bin 556: 373 of cap free
Amount of items: 2
Items: 
Size: 566840 Color: 2
Size: 432788 Color: 17

Bin 557: 377 of cap free
Amount of items: 2
Items: 
Size: 545684 Color: 1
Size: 453940 Color: 4

Bin 558: 377 of cap free
Amount of items: 2
Items: 
Size: 766411 Color: 10
Size: 233213 Color: 8

Bin 559: 382 of cap free
Amount of items: 2
Items: 
Size: 783697 Color: 9
Size: 215922 Color: 3

Bin 560: 389 of cap free
Amount of items: 2
Items: 
Size: 677659 Color: 7
Size: 321953 Color: 8

Bin 561: 389 of cap free
Amount of items: 2
Items: 
Size: 542883 Color: 14
Size: 456729 Color: 9

Bin 562: 389 of cap free
Amount of items: 2
Items: 
Size: 614303 Color: 1
Size: 385309 Color: 15

Bin 563: 396 of cap free
Amount of items: 2
Items: 
Size: 582700 Color: 19
Size: 416905 Color: 4

Bin 564: 400 of cap free
Amount of items: 2
Items: 
Size: 714513 Color: 18
Size: 285088 Color: 12

Bin 565: 407 of cap free
Amount of items: 2
Items: 
Size: 526719 Color: 18
Size: 472875 Color: 8

Bin 566: 414 of cap free
Amount of items: 2
Items: 
Size: 720468 Color: 1
Size: 279119 Color: 8

Bin 567: 416 of cap free
Amount of items: 2
Items: 
Size: 533744 Color: 16
Size: 465841 Color: 0

Bin 568: 416 of cap free
Amount of items: 2
Items: 
Size: 517814 Color: 8
Size: 481771 Color: 13

Bin 569: 416 of cap free
Amount of items: 2
Items: 
Size: 746640 Color: 10
Size: 252945 Color: 0

Bin 570: 417 of cap free
Amount of items: 2
Items: 
Size: 584625 Color: 0
Size: 414959 Color: 9

Bin 571: 427 of cap free
Amount of items: 2
Items: 
Size: 500677 Color: 3
Size: 498897 Color: 17

Bin 572: 428 of cap free
Amount of items: 2
Items: 
Size: 606745 Color: 0
Size: 392828 Color: 4

Bin 573: 428 of cap free
Amount of items: 2
Items: 
Size: 531397 Color: 13
Size: 468176 Color: 15

Bin 574: 433 of cap free
Amount of items: 2
Items: 
Size: 711259 Color: 0
Size: 288309 Color: 8

Bin 575: 434 of cap free
Amount of items: 2
Items: 
Size: 780823 Color: 10
Size: 218744 Color: 17

Bin 576: 435 of cap free
Amount of items: 2
Items: 
Size: 606589 Color: 9
Size: 392977 Color: 0

Bin 577: 436 of cap free
Amount of items: 2
Items: 
Size: 686363 Color: 17
Size: 313202 Color: 7

Bin 578: 438 of cap free
Amount of items: 2
Items: 
Size: 574013 Color: 7
Size: 425550 Color: 11

Bin 579: 443 of cap free
Amount of items: 2
Items: 
Size: 553884 Color: 18
Size: 445674 Color: 13

Bin 580: 444 of cap free
Amount of items: 2
Items: 
Size: 627257 Color: 9
Size: 372300 Color: 10

Bin 581: 448 of cap free
Amount of items: 2
Items: 
Size: 639997 Color: 18
Size: 359556 Color: 16

Bin 582: 452 of cap free
Amount of items: 2
Items: 
Size: 589053 Color: 16
Size: 410496 Color: 3

Bin 583: 454 of cap free
Amount of items: 2
Items: 
Size: 583815 Color: 17
Size: 415732 Color: 9

Bin 584: 454 of cap free
Amount of items: 2
Items: 
Size: 644136 Color: 14
Size: 355411 Color: 16

Bin 585: 455 of cap free
Amount of items: 2
Items: 
Size: 595266 Color: 5
Size: 404280 Color: 19

Bin 586: 456 of cap free
Amount of items: 2
Items: 
Size: 523311 Color: 8
Size: 476234 Color: 13

Bin 587: 460 of cap free
Amount of items: 2
Items: 
Size: 749889 Color: 13
Size: 249652 Color: 10

Bin 588: 466 of cap free
Amount of items: 2
Items: 
Size: 610605 Color: 5
Size: 388930 Color: 12

Bin 589: 471 of cap free
Amount of items: 2
Items: 
Size: 504604 Color: 12
Size: 494926 Color: 19

Bin 590: 474 of cap free
Amount of items: 2
Items: 
Size: 644121 Color: 5
Size: 355406 Color: 12

Bin 591: 474 of cap free
Amount of items: 2
Items: 
Size: 638554 Color: 17
Size: 360973 Color: 9

Bin 592: 478 of cap free
Amount of items: 2
Items: 
Size: 603879 Color: 16
Size: 395644 Color: 6

Bin 593: 481 of cap free
Amount of items: 2
Items: 
Size: 748391 Color: 7
Size: 251129 Color: 0

Bin 594: 491 of cap free
Amount of items: 2
Items: 
Size: 565858 Color: 15
Size: 433652 Color: 4

Bin 595: 491 of cap free
Amount of items: 2
Items: 
Size: 543454 Color: 13
Size: 456056 Color: 8

Bin 596: 502 of cap free
Amount of items: 2
Items: 
Size: 569831 Color: 9
Size: 429668 Color: 7

Bin 597: 505 of cap free
Amount of items: 2
Items: 
Size: 794752 Color: 10
Size: 204744 Color: 7

Bin 598: 509 of cap free
Amount of items: 2
Items: 
Size: 568670 Color: 1
Size: 430822 Color: 17

Bin 599: 511 of cap free
Amount of items: 2
Items: 
Size: 633961 Color: 14
Size: 365529 Color: 2

Bin 600: 515 of cap free
Amount of items: 2
Items: 
Size: 625408 Color: 9
Size: 374078 Color: 11

Bin 601: 515 of cap free
Amount of items: 2
Items: 
Size: 798253 Color: 7
Size: 201233 Color: 13

Bin 602: 516 of cap free
Amount of items: 2
Items: 
Size: 519075 Color: 19
Size: 480410 Color: 11

Bin 603: 518 of cap free
Amount of items: 2
Items: 
Size: 509303 Color: 18
Size: 490180 Color: 8

Bin 604: 519 of cap free
Amount of items: 2
Items: 
Size: 601636 Color: 12
Size: 397846 Color: 6

Bin 605: 522 of cap free
Amount of items: 2
Items: 
Size: 690441 Color: 1
Size: 309038 Color: 16

Bin 606: 522 of cap free
Amount of items: 2
Items: 
Size: 617267 Color: 17
Size: 382212 Color: 2

Bin 607: 523 of cap free
Amount of items: 2
Items: 
Size: 708590 Color: 13
Size: 290888 Color: 17

Bin 608: 523 of cap free
Amount of items: 2
Items: 
Size: 592841 Color: 17
Size: 406637 Color: 16

Bin 609: 524 of cap free
Amount of items: 2
Items: 
Size: 765692 Color: 5
Size: 233785 Color: 8

Bin 610: 531 of cap free
Amount of items: 2
Items: 
Size: 538836 Color: 0
Size: 460634 Color: 14

Bin 611: 532 of cap free
Amount of items: 2
Items: 
Size: 505736 Color: 16
Size: 493733 Color: 14

Bin 612: 533 of cap free
Amount of items: 2
Items: 
Size: 777552 Color: 10
Size: 221916 Color: 6

Bin 613: 534 of cap free
Amount of items: 2
Items: 
Size: 637906 Color: 15
Size: 361561 Color: 2

Bin 614: 535 of cap free
Amount of items: 2
Items: 
Size: 734626 Color: 5
Size: 264840 Color: 12

Bin 615: 544 of cap free
Amount of items: 2
Items: 
Size: 556441 Color: 9
Size: 443016 Color: 8

Bin 616: 545 of cap free
Amount of items: 2
Items: 
Size: 588834 Color: 19
Size: 410622 Color: 16

Bin 617: 547 of cap free
Amount of items: 2
Items: 
Size: 591356 Color: 1
Size: 408098 Color: 3

Bin 618: 550 of cap free
Amount of items: 2
Items: 
Size: 788044 Color: 13
Size: 211407 Color: 19

Bin 619: 551 of cap free
Amount of items: 2
Items: 
Size: 774964 Color: 7
Size: 224486 Color: 19

Bin 620: 552 of cap free
Amount of items: 2
Items: 
Size: 673759 Color: 1
Size: 325690 Color: 13

Bin 621: 553 of cap free
Amount of items: 2
Items: 
Size: 516379 Color: 3
Size: 483069 Color: 0

Bin 622: 553 of cap free
Amount of items: 2
Items: 
Size: 709444 Color: 13
Size: 290004 Color: 15

Bin 623: 555 of cap free
Amount of items: 2
Items: 
Size: 740685 Color: 10
Size: 258761 Color: 17

Bin 624: 556 of cap free
Amount of items: 2
Items: 
Size: 768825 Color: 17
Size: 230620 Color: 0

Bin 625: 558 of cap free
Amount of items: 2
Items: 
Size: 513025 Color: 4
Size: 486418 Color: 18

Bin 626: 560 of cap free
Amount of items: 2
Items: 
Size: 598062 Color: 14
Size: 401379 Color: 7

Bin 627: 565 of cap free
Amount of items: 2
Items: 
Size: 700156 Color: 6
Size: 299280 Color: 18

Bin 628: 574 of cap free
Amount of items: 2
Items: 
Size: 744387 Color: 17
Size: 255040 Color: 14

Bin 629: 577 of cap free
Amount of items: 2
Items: 
Size: 762792 Color: 15
Size: 236632 Color: 10

Bin 630: 578 of cap free
Amount of items: 2
Items: 
Size: 561661 Color: 6
Size: 437762 Color: 14

Bin 631: 579 of cap free
Amount of items: 2
Items: 
Size: 761824 Color: 4
Size: 237598 Color: 12

Bin 632: 584 of cap free
Amount of items: 2
Items: 
Size: 514461 Color: 7
Size: 484956 Color: 3

Bin 633: 587 of cap free
Amount of items: 2
Items: 
Size: 778886 Color: 4
Size: 220528 Color: 15

Bin 634: 593 of cap free
Amount of items: 2
Items: 
Size: 799626 Color: 7
Size: 199782 Color: 10

Bin 635: 603 of cap free
Amount of items: 2
Items: 
Size: 664549 Color: 15
Size: 334849 Color: 13

Bin 636: 608 of cap free
Amount of items: 2
Items: 
Size: 635116 Color: 12
Size: 364277 Color: 2

Bin 637: 616 of cap free
Amount of items: 2
Items: 
Size: 585625 Color: 5
Size: 413760 Color: 4

Bin 638: 617 of cap free
Amount of items: 2
Items: 
Size: 795500 Color: 14
Size: 203884 Color: 15

Bin 639: 620 of cap free
Amount of items: 2
Items: 
Size: 543947 Color: 14
Size: 455434 Color: 16

Bin 640: 622 of cap free
Amount of items: 2
Items: 
Size: 784194 Color: 17
Size: 215185 Color: 5

Bin 641: 625 of cap free
Amount of items: 2
Items: 
Size: 724327 Color: 10
Size: 275049 Color: 7

Bin 642: 626 of cap free
Amount of items: 2
Items: 
Size: 533534 Color: 19
Size: 465841 Color: 7

Bin 643: 626 of cap free
Amount of items: 2
Items: 
Size: 652264 Color: 11
Size: 347111 Color: 14

Bin 644: 634 of cap free
Amount of items: 2
Items: 
Size: 678332 Color: 8
Size: 321035 Color: 18

Bin 645: 636 of cap free
Amount of items: 2
Items: 
Size: 720460 Color: 13
Size: 278905 Color: 9

Bin 646: 637 of cap free
Amount of items: 2
Items: 
Size: 589513 Color: 16
Size: 409851 Color: 6

Bin 647: 649 of cap free
Amount of items: 2
Items: 
Size: 518391 Color: 1
Size: 480961 Color: 13

Bin 648: 657 of cap free
Amount of items: 2
Items: 
Size: 644071 Color: 19
Size: 355273 Color: 3

Bin 649: 658 of cap free
Amount of items: 2
Items: 
Size: 629655 Color: 3
Size: 369688 Color: 4

Bin 650: 662 of cap free
Amount of items: 2
Items: 
Size: 731875 Color: 4
Size: 267464 Color: 5

Bin 651: 687 of cap free
Amount of items: 2
Items: 
Size: 692045 Color: 18
Size: 307269 Color: 10

Bin 652: 690 of cap free
Amount of items: 2
Items: 
Size: 769991 Color: 8
Size: 229320 Color: 16

Bin 653: 693 of cap free
Amount of items: 2
Items: 
Size: 760025 Color: 2
Size: 239283 Color: 9

Bin 654: 701 of cap free
Amount of items: 2
Items: 
Size: 660645 Color: 2
Size: 338655 Color: 10

Bin 655: 712 of cap free
Amount of items: 2
Items: 
Size: 524076 Color: 14
Size: 475213 Color: 18

Bin 656: 717 of cap free
Amount of items: 2
Items: 
Size: 658240 Color: 13
Size: 341044 Color: 18

Bin 657: 729 of cap free
Amount of items: 2
Items: 
Size: 652181 Color: 11
Size: 347091 Color: 9

Bin 658: 733 of cap free
Amount of items: 2
Items: 
Size: 594015 Color: 8
Size: 405253 Color: 5

Bin 659: 736 of cap free
Amount of items: 2
Items: 
Size: 624228 Color: 19
Size: 375037 Color: 10

Bin 660: 740 of cap free
Amount of items: 2
Items: 
Size: 595835 Color: 9
Size: 403426 Color: 7

Bin 661: 741 of cap free
Amount of items: 2
Items: 
Size: 665493 Color: 15
Size: 333767 Color: 10

Bin 662: 745 of cap free
Amount of items: 2
Items: 
Size: 708470 Color: 13
Size: 290786 Color: 16

Bin 663: 745 of cap free
Amount of items: 2
Items: 
Size: 677560 Color: 7
Size: 321696 Color: 8

Bin 664: 751 of cap free
Amount of items: 2
Items: 
Size: 688231 Color: 7
Size: 311019 Color: 8

Bin 665: 762 of cap free
Amount of items: 2
Items: 
Size: 521560 Color: 3
Size: 477679 Color: 11

Bin 666: 767 of cap free
Amount of items: 2
Items: 
Size: 768697 Color: 9
Size: 230537 Color: 4

Bin 667: 773 of cap free
Amount of items: 2
Items: 
Size: 764011 Color: 15
Size: 235217 Color: 5

Bin 668: 778 of cap free
Amount of items: 2
Items: 
Size: 570524 Color: 6
Size: 428699 Color: 16

Bin 669: 791 of cap free
Amount of items: 2
Items: 
Size: 651191 Color: 12
Size: 348019 Color: 17

Bin 670: 798 of cap free
Amount of items: 2
Items: 
Size: 785402 Color: 0
Size: 213801 Color: 2

Bin 671: 799 of cap free
Amount of items: 2
Items: 
Size: 511162 Color: 3
Size: 488040 Color: 15

Bin 672: 802 of cap free
Amount of items: 2
Items: 
Size: 563812 Color: 18
Size: 435387 Color: 13

Bin 673: 804 of cap free
Amount of items: 2
Items: 
Size: 591299 Color: 8
Size: 407898 Color: 17

Bin 674: 806 of cap free
Amount of items: 2
Items: 
Size: 741485 Color: 15
Size: 257710 Color: 1

Bin 675: 815 of cap free
Amount of items: 2
Items: 
Size: 527851 Color: 16
Size: 471335 Color: 4

Bin 676: 819 of cap free
Amount of items: 2
Items: 
Size: 586981 Color: 10
Size: 412201 Color: 19

Bin 677: 834 of cap free
Amount of items: 2
Items: 
Size: 636647 Color: 9
Size: 362520 Color: 18

Bin 678: 837 of cap free
Amount of items: 2
Items: 
Size: 502589 Color: 0
Size: 496575 Color: 14

Bin 679: 840 of cap free
Amount of items: 2
Items: 
Size: 680886 Color: 11
Size: 318275 Color: 14

Bin 680: 845 of cap free
Amount of items: 2
Items: 
Size: 524058 Color: 13
Size: 475098 Color: 16

Bin 681: 846 of cap free
Amount of items: 2
Items: 
Size: 664476 Color: 5
Size: 334679 Color: 8

Bin 682: 849 of cap free
Amount of items: 2
Items: 
Size: 541023 Color: 8
Size: 458129 Color: 16

Bin 683: 856 of cap free
Amount of items: 2
Items: 
Size: 721746 Color: 6
Size: 277399 Color: 9

Bin 684: 883 of cap free
Amount of items: 2
Items: 
Size: 572249 Color: 5
Size: 426869 Color: 8

Bin 685: 897 of cap free
Amount of items: 2
Items: 
Size: 526277 Color: 8
Size: 472827 Color: 10

Bin 686: 927 of cap free
Amount of items: 2
Items: 
Size: 776211 Color: 16
Size: 222863 Color: 10

Bin 687: 933 of cap free
Amount of items: 2
Items: 
Size: 598047 Color: 5
Size: 401021 Color: 14

Bin 688: 936 of cap free
Amount of items: 2
Items: 
Size: 729446 Color: 8
Size: 269619 Color: 7

Bin 689: 936 of cap free
Amount of items: 2
Items: 
Size: 644620 Color: 3
Size: 354445 Color: 18

Bin 690: 944 of cap free
Amount of items: 2
Items: 
Size: 646584 Color: 15
Size: 352473 Color: 18

Bin 691: 950 of cap free
Amount of items: 2
Items: 
Size: 557337 Color: 9
Size: 441714 Color: 19

Bin 692: 956 of cap free
Amount of items: 2
Items: 
Size: 548181 Color: 17
Size: 450864 Color: 0

Bin 693: 964 of cap free
Amount of items: 2
Items: 
Size: 557951 Color: 19
Size: 441086 Color: 9

Bin 694: 968 of cap free
Amount of items: 2
Items: 
Size: 612510 Color: 1
Size: 386523 Color: 3

Bin 695: 972 of cap free
Amount of items: 2
Items: 
Size: 603403 Color: 10
Size: 395626 Color: 1

Bin 696: 994 of cap free
Amount of items: 2
Items: 
Size: 748359 Color: 17
Size: 250648 Color: 18

Bin 697: 1010 of cap free
Amount of items: 2
Items: 
Size: 690366 Color: 14
Size: 308625 Color: 17

Bin 698: 1012 of cap free
Amount of items: 2
Items: 
Size: 516264 Color: 8
Size: 482725 Color: 15

Bin 699: 1016 of cap free
Amount of items: 2
Items: 
Size: 592768 Color: 2
Size: 406217 Color: 10

Bin 700: 1021 of cap free
Amount of items: 2
Items: 
Size: 599160 Color: 3
Size: 399820 Color: 16

Bin 701: 1030 of cap free
Amount of items: 2
Items: 
Size: 636615 Color: 7
Size: 362356 Color: 1

Bin 702: 1031 of cap free
Amount of items: 3
Items: 
Size: 746614 Color: 4
Size: 142156 Color: 1
Size: 110200 Color: 7

Bin 703: 1031 of cap free
Amount of items: 2
Items: 
Size: 529301 Color: 14
Size: 469669 Color: 16

Bin 704: 1031 of cap free
Amount of items: 2
Items: 
Size: 737920 Color: 4
Size: 261050 Color: 14

Bin 705: 1037 of cap free
Amount of items: 2
Items: 
Size: 780391 Color: 14
Size: 218573 Color: 18

Bin 706: 1039 of cap free
Amount of items: 2
Items: 
Size: 596859 Color: 6
Size: 402103 Color: 18

Bin 707: 1043 of cap free
Amount of items: 2
Items: 
Size: 698258 Color: 15
Size: 300700 Color: 7

Bin 708: 1043 of cap free
Amount of items: 3
Items: 
Size: 716858 Color: 9
Size: 163961 Color: 0
Size: 118139 Color: 5

Bin 709: 1047 of cap free
Amount of items: 2
Items: 
Size: 540899 Color: 18
Size: 458055 Color: 8

Bin 710: 1048 of cap free
Amount of items: 2
Items: 
Size: 768667 Color: 1
Size: 230286 Color: 4

Bin 711: 1063 of cap free
Amount of items: 2
Items: 
Size: 695588 Color: 2
Size: 303350 Color: 11

Bin 712: 1069 of cap free
Amount of items: 2
Items: 
Size: 725979 Color: 9
Size: 272953 Color: 14

Bin 713: 1075 of cap free
Amount of items: 2
Items: 
Size: 514032 Color: 5
Size: 484894 Color: 9

Bin 714: 1091 of cap free
Amount of items: 2
Items: 
Size: 754793 Color: 5
Size: 244117 Color: 4

Bin 715: 1110 of cap free
Amount of items: 2
Items: 
Size: 573436 Color: 4
Size: 425455 Color: 18

Bin 716: 1112 of cap free
Amount of items: 2
Items: 
Size: 563529 Color: 12
Size: 435360 Color: 15

Bin 717: 1113 of cap free
Amount of items: 2
Items: 
Size: 697025 Color: 3
Size: 301863 Color: 16

Bin 718: 1118 of cap free
Amount of items: 2
Items: 
Size: 700878 Color: 13
Size: 298005 Color: 16

Bin 719: 1120 of cap free
Amount of items: 2
Items: 
Size: 633887 Color: 8
Size: 364994 Color: 2

Bin 720: 1123 of cap free
Amount of items: 2
Items: 
Size: 700058 Color: 12
Size: 298820 Color: 13

Bin 721: 1133 of cap free
Amount of items: 2
Items: 
Size: 606569 Color: 19
Size: 392299 Color: 5

Bin 722: 1148 of cap free
Amount of items: 2
Items: 
Size: 789484 Color: 6
Size: 209369 Color: 17

Bin 723: 1167 of cap free
Amount of items: 2
Items: 
Size: 611298 Color: 15
Size: 387536 Color: 7

Bin 724: 1170 of cap free
Amount of items: 2
Items: 
Size: 769730 Color: 19
Size: 229101 Color: 14

Bin 725: 1183 of cap free
Amount of items: 2
Items: 
Size: 550836 Color: 1
Size: 447982 Color: 19

Bin 726: 1197 of cap free
Amount of items: 2
Items: 
Size: 526199 Color: 12
Size: 472605 Color: 10

Bin 727: 1209 of cap free
Amount of items: 2
Items: 
Size: 580474 Color: 6
Size: 418318 Color: 14

Bin 728: 1211 of cap free
Amount of items: 2
Items: 
Size: 780259 Color: 17
Size: 218531 Color: 19

Bin 729: 1214 of cap free
Amount of items: 2
Items: 
Size: 521310 Color: 5
Size: 477477 Color: 7

Bin 730: 1220 of cap free
Amount of items: 2
Items: 
Size: 646402 Color: 6
Size: 352379 Color: 13

Bin 731: 1220 of cap free
Amount of items: 2
Items: 
Size: 727161 Color: 13
Size: 271620 Color: 4

Bin 732: 1221 of cap free
Amount of items: 2
Items: 
Size: 504322 Color: 0
Size: 494458 Color: 11

Bin 733: 1225 of cap free
Amount of items: 2
Items: 
Size: 533044 Color: 15
Size: 465732 Color: 2

Bin 734: 1226 of cap free
Amount of items: 2
Items: 
Size: 691997 Color: 4
Size: 306778 Color: 0

Bin 735: 1238 of cap free
Amount of items: 2
Items: 
Size: 731330 Color: 18
Size: 267433 Color: 4

Bin 736: 1239 of cap free
Amount of items: 2
Items: 
Size: 606537 Color: 18
Size: 392225 Color: 7

Bin 737: 1255 of cap free
Amount of items: 2
Items: 
Size: 746418 Color: 17
Size: 252328 Color: 12

Bin 738: 1255 of cap free
Amount of items: 2
Items: 
Size: 510757 Color: 3
Size: 487989 Color: 6

Bin 739: 1256 of cap free
Amount of items: 2
Items: 
Size: 752671 Color: 18
Size: 246074 Color: 14

Bin 740: 1259 of cap free
Amount of items: 2
Items: 
Size: 588897 Color: 16
Size: 409845 Color: 4

Bin 741: 1293 of cap free
Amount of items: 2
Items: 
Size: 797967 Color: 13
Size: 200741 Color: 18

Bin 742: 1299 of cap free
Amount of items: 2
Items: 
Size: 746416 Color: 14
Size: 252286 Color: 15

Bin 743: 1302 of cap free
Amount of items: 2
Items: 
Size: 523829 Color: 12
Size: 474870 Color: 15

Bin 744: 1322 of cap free
Amount of items: 2
Items: 
Size: 660155 Color: 0
Size: 338524 Color: 11

Bin 745: 1324 of cap free
Amount of items: 2
Items: 
Size: 620408 Color: 17
Size: 378269 Color: 2

Bin 746: 1324 of cap free
Amount of items: 2
Items: 
Size: 593940 Color: 5
Size: 404737 Color: 10

Bin 747: 1340 of cap free
Amount of items: 2
Items: 
Size: 716638 Color: 9
Size: 282023 Color: 14

Bin 748: 1342 of cap free
Amount of items: 2
Items: 
Size: 749513 Color: 15
Size: 249146 Color: 13

Bin 749: 1359 of cap free
Amount of items: 2
Items: 
Size: 673396 Color: 14
Size: 325246 Color: 9

Bin 750: 1372 of cap free
Amount of items: 2
Items: 
Size: 727071 Color: 0
Size: 271558 Color: 12

Bin 751: 1380 of cap free
Amount of items: 2
Items: 
Size: 706067 Color: 11
Size: 292554 Color: 10

Bin 752: 1415 of cap free
Amount of items: 2
Items: 
Size: 787725 Color: 19
Size: 210861 Color: 7

Bin 753: 1418 of cap free
Amount of items: 2
Items: 
Size: 719802 Color: 15
Size: 278781 Color: 0

Bin 754: 1421 of cap free
Amount of items: 2
Items: 
Size: 591098 Color: 8
Size: 407482 Color: 9

Bin 755: 1429 of cap free
Amount of items: 2
Items: 
Size: 639037 Color: 18
Size: 359535 Color: 19

Bin 756: 1456 of cap free
Amount of items: 2
Items: 
Size: 530970 Color: 18
Size: 467575 Color: 0

Bin 757: 1469 of cap free
Amount of items: 2
Items: 
Size: 657949 Color: 14
Size: 340583 Color: 9

Bin 758: 1581 of cap free
Amount of items: 2
Items: 
Size: 602976 Color: 18
Size: 395444 Color: 2

Bin 759: 1583 of cap free
Amount of items: 2
Items: 
Size: 535495 Color: 4
Size: 462923 Color: 1

Bin 760: 1594 of cap free
Amount of items: 2
Items: 
Size: 547798 Color: 11
Size: 450609 Color: 19

Bin 761: 1594 of cap free
Amount of items: 2
Items: 
Size: 602972 Color: 6
Size: 395435 Color: 5

Bin 762: 1629 of cap free
Amount of items: 2
Items: 
Size: 499539 Color: 0
Size: 498833 Color: 1

Bin 763: 1640 of cap free
Amount of items: 2
Items: 
Size: 598989 Color: 7
Size: 399372 Color: 16

Bin 764: 1673 of cap free
Amount of items: 2
Items: 
Size: 670547 Color: 5
Size: 327781 Color: 7

Bin 765: 1700 of cap free
Amount of items: 2
Items: 
Size: 625208 Color: 13
Size: 373093 Color: 7

Bin 766: 1706 of cap free
Amount of items: 2
Items: 
Size: 765135 Color: 7
Size: 233160 Color: 12

Bin 767: 1767 of cap free
Amount of items: 2
Items: 
Size: 510696 Color: 0
Size: 487538 Color: 16

Bin 768: 1795 of cap free
Amount of items: 2
Items: 
Size: 752423 Color: 1
Size: 245783 Color: 13

Bin 769: 1844 of cap free
Amount of items: 2
Items: 
Size: 714025 Color: 0
Size: 284132 Color: 5

Bin 770: 1934 of cap free
Amount of items: 2
Items: 
Size: 629525 Color: 16
Size: 368542 Color: 3

Bin 771: 1945 of cap free
Amount of items: 2
Items: 
Size: 510661 Color: 18
Size: 487395 Color: 11

Bin 772: 1951 of cap free
Amount of items: 2
Items: 
Size: 532541 Color: 5
Size: 465509 Color: 12

Bin 773: 2050 of cap free
Amount of items: 2
Items: 
Size: 560469 Color: 14
Size: 437482 Color: 10

Bin 774: 2069 of cap free
Amount of items: 2
Items: 
Size: 754384 Color: 9
Size: 243548 Color: 11

Bin 775: 2074 of cap free
Amount of items: 2
Items: 
Size: 765051 Color: 16
Size: 232876 Color: 11

Bin 776: 2092 of cap free
Amount of items: 2
Items: 
Size: 639053 Color: 19
Size: 358856 Color: 11

Bin 777: 2094 of cap free
Amount of items: 2
Items: 
Size: 588255 Color: 9
Size: 409652 Color: 12

Bin 778: 2164 of cap free
Amount of items: 2
Items: 
Size: 737139 Color: 12
Size: 260698 Color: 11

Bin 779: 2232 of cap free
Amount of items: 2
Items: 
Size: 616307 Color: 16
Size: 381462 Color: 4

Bin 780: 2246 of cap free
Amount of items: 2
Items: 
Size: 736950 Color: 14
Size: 260805 Color: 12

Bin 781: 2262 of cap free
Amount of items: 2
Items: 
Size: 629500 Color: 11
Size: 368239 Color: 18

Bin 782: 2300 of cap free
Amount of items: 2
Items: 
Size: 565047 Color: 14
Size: 432654 Color: 16

Bin 783: 2326 of cap free
Amount of items: 2
Items: 
Size: 650813 Color: 12
Size: 346862 Color: 5

Bin 784: 2424 of cap free
Amount of items: 2
Items: 
Size: 650706 Color: 2
Size: 346871 Color: 12

Bin 785: 2439 of cap free
Amount of items: 2
Items: 
Size: 504064 Color: 2
Size: 493498 Color: 8

Bin 786: 2448 of cap free
Amount of items: 2
Items: 
Size: 659675 Color: 7
Size: 337878 Color: 15

Bin 787: 2547 of cap free
Amount of items: 2
Items: 
Size: 606253 Color: 8
Size: 391201 Color: 3

Bin 788: 2624 of cap free
Amount of items: 2
Items: 
Size: 602777 Color: 18
Size: 394600 Color: 9

Bin 789: 2626 of cap free
Amount of items: 2
Items: 
Size: 736695 Color: 11
Size: 260680 Color: 14

Bin 790: 2636 of cap free
Amount of items: 2
Items: 
Size: 523005 Color: 16
Size: 474360 Color: 8

Bin 791: 2690 of cap free
Amount of items: 2
Items: 
Size: 695503 Color: 3
Size: 301808 Color: 7

Bin 792: 2697 of cap free
Amount of items: 2
Items: 
Size: 509861 Color: 8
Size: 487443 Color: 18

Bin 793: 2711 of cap free
Amount of items: 2
Items: 
Size: 542145 Color: 16
Size: 455145 Color: 17

Bin 794: 2727 of cap free
Amount of items: 2
Items: 
Size: 503818 Color: 9
Size: 493456 Color: 4

Bin 795: 2756 of cap free
Amount of items: 2
Items: 
Size: 629445 Color: 3
Size: 367800 Color: 16

Bin 796: 2759 of cap free
Amount of items: 2
Items: 
Size: 590879 Color: 10
Size: 406363 Color: 2

Bin 797: 2807 of cap free
Amount of items: 2
Items: 
Size: 650549 Color: 16
Size: 346645 Color: 6

Bin 798: 2852 of cap free
Amount of items: 2
Items: 
Size: 730569 Color: 13
Size: 266580 Color: 11

Bin 799: 2913 of cap free
Amount of items: 2
Items: 
Size: 518268 Color: 14
Size: 478820 Color: 5

Bin 800: 2954 of cap free
Amount of items: 2
Items: 
Size: 676042 Color: 7
Size: 321005 Color: 6

Bin 801: 3011 of cap free
Amount of items: 2
Items: 
Size: 572174 Color: 13
Size: 424816 Color: 8

Bin 802: 3073 of cap free
Amount of items: 2
Items: 
Size: 736511 Color: 18
Size: 260417 Color: 0

Bin 803: 3146 of cap free
Amount of items: 2
Items: 
Size: 730552 Color: 9
Size: 266303 Color: 2

Bin 804: 3189 of cap free
Amount of items: 2
Items: 
Size: 606142 Color: 12
Size: 390670 Color: 8

Bin 805: 3219 of cap free
Amount of items: 2
Items: 
Size: 718156 Color: 15
Size: 278626 Color: 19

Bin 806: 3220 of cap free
Amount of items: 2
Items: 
Size: 580020 Color: 11
Size: 416761 Color: 14

Bin 807: 3223 of cap free
Amount of items: 2
Items: 
Size: 663301 Color: 7
Size: 333477 Color: 17

Bin 808: 3238 of cap free
Amount of items: 2
Items: 
Size: 590581 Color: 15
Size: 406182 Color: 10

Bin 809: 3239 of cap free
Amount of items: 2
Items: 
Size: 687972 Color: 15
Size: 308790 Color: 14

Bin 810: 3284 of cap free
Amount of items: 2
Items: 
Size: 642339 Color: 10
Size: 354378 Color: 5

Bin 811: 3285 of cap free
Amount of items: 2
Items: 
Size: 606121 Color: 0
Size: 390595 Color: 4

Bin 812: 3358 of cap free
Amount of items: 2
Items: 
Size: 787253 Color: 1
Size: 209390 Color: 6

Bin 813: 3366 of cap free
Amount of items: 2
Items: 
Size: 642452 Color: 5
Size: 354183 Color: 4

Bin 814: 3369 of cap free
Amount of items: 2
Items: 
Size: 730507 Color: 15
Size: 266125 Color: 9

Bin 815: 3509 of cap free
Amount of items: 2
Items: 
Size: 680008 Color: 17
Size: 316484 Color: 7

Bin 816: 3509 of cap free
Amount of items: 2
Items: 
Size: 637722 Color: 2
Size: 358770 Color: 16

Bin 817: 3591 of cap free
Amount of items: 2
Items: 
Size: 676032 Color: 12
Size: 320378 Color: 15

Bin 818: 3642 of cap free
Amount of items: 2
Items: 
Size: 736124 Color: 10
Size: 260235 Color: 19

Bin 819: 3688 of cap free
Amount of items: 2
Items: 
Size: 605753 Color: 13
Size: 390560 Color: 12

Bin 820: 3779 of cap free
Amount of items: 2
Items: 
Size: 619851 Color: 2
Size: 376371 Color: 17

Bin 821: 3830 of cap free
Amount of items: 2
Items: 
Size: 530954 Color: 19
Size: 465217 Color: 14

Bin 822: 3869 of cap free
Amount of items: 2
Items: 
Size: 675810 Color: 4
Size: 320322 Color: 2

Bin 823: 3906 of cap free
Amount of items: 2
Items: 
Size: 517321 Color: 19
Size: 478774 Color: 2

Bin 824: 3980 of cap free
Amount of items: 2
Items: 
Size: 555091 Color: 13
Size: 440930 Color: 5

Bin 825: 4019 of cap free
Amount of items: 2
Items: 
Size: 797546 Color: 11
Size: 198436 Color: 16

Bin 826: 4214 of cap free
Amount of items: 2
Items: 
Size: 768523 Color: 8
Size: 227264 Color: 16

Bin 827: 4287 of cap free
Amount of items: 2
Items: 
Size: 579077 Color: 8
Size: 416637 Color: 9

Bin 828: 4398 of cap free
Amount of items: 2
Items: 
Size: 668387 Color: 0
Size: 327216 Color: 8

Bin 829: 4454 of cap free
Amount of items: 2
Items: 
Size: 554874 Color: 11
Size: 440673 Color: 5

Bin 830: 4462 of cap free
Amount of items: 2
Items: 
Size: 605663 Color: 7
Size: 389876 Color: 11

Bin 831: 4562 of cap free
Amount of items: 2
Items: 
Size: 657703 Color: 7
Size: 337736 Color: 3

Bin 832: 4568 of cap free
Amount of items: 2
Items: 
Size: 752231 Color: 16
Size: 243202 Color: 14

Bin 833: 4647 of cap free
Amount of items: 2
Items: 
Size: 605545 Color: 19
Size: 389809 Color: 11

Bin 834: 4719 of cap free
Amount of items: 2
Items: 
Size: 797376 Color: 8
Size: 197906 Color: 15

Bin 835: 4874 of cap free
Amount of items: 2
Items: 
Size: 562542 Color: 0
Size: 432585 Color: 19

Bin 836: 4953 of cap free
Amount of items: 2
Items: 
Size: 502166 Color: 7
Size: 492882 Color: 14

Bin 837: 5120 of cap free
Amount of items: 2
Items: 
Size: 728887 Color: 11
Size: 265994 Color: 8

Bin 838: 5231 of cap free
Amount of items: 2
Items: 
Size: 718127 Color: 14
Size: 276643 Color: 16

Bin 839: 5280 of cap free
Amount of items: 2
Items: 
Size: 571854 Color: 15
Size: 422867 Color: 10

Bin 840: 5371 of cap free
Amount of items: 2
Items: 
Size: 657279 Color: 5
Size: 337351 Color: 11

Bin 841: 5573 of cap free
Amount of items: 2
Items: 
Size: 635955 Color: 15
Size: 358473 Color: 17

Bin 842: 5816 of cap free
Amount of items: 2
Items: 
Size: 751198 Color: 0
Size: 242987 Color: 6

Bin 843: 5847 of cap free
Amount of items: 2
Items: 
Size: 797160 Color: 4
Size: 196994 Color: 15

Bin 844: 6589 of cap free
Amount of items: 2
Items: 
Size: 515953 Color: 15
Size: 477459 Color: 14

Bin 845: 6669 of cap free
Amount of items: 2
Items: 
Size: 735829 Color: 16
Size: 257503 Color: 9

Bin 846: 6885 of cap free
Amount of items: 2
Items: 
Size: 571553 Color: 3
Size: 421563 Color: 8

Bin 847: 6894 of cap free
Amount of items: 2
Items: 
Size: 655839 Color: 11
Size: 337268 Color: 10

Bin 848: 7168 of cap free
Amount of items: 2
Items: 
Size: 571337 Color: 5
Size: 421496 Color: 16

Bin 849: 7245 of cap free
Amount of items: 2
Items: 
Size: 694823 Color: 2
Size: 297933 Color: 5

Bin 850: 7377 of cap free
Amount of items: 2
Items: 
Size: 515331 Color: 5
Size: 477293 Color: 9

Bin 851: 7488 of cap free
Amount of items: 2
Items: 
Size: 590365 Color: 17
Size: 402148 Color: 6

Bin 852: 7521 of cap free
Amount of items: 2
Items: 
Size: 560307 Color: 4
Size: 432173 Color: 17

Bin 853: 7532 of cap free
Amount of items: 2
Items: 
Size: 667846 Color: 2
Size: 324623 Color: 8

Bin 854: 7755 of cap free
Amount of items: 2
Items: 
Size: 735251 Color: 5
Size: 256995 Color: 7

Bin 855: 7770 of cap free
Amount of items: 2
Items: 
Size: 751153 Color: 13
Size: 241078 Color: 15

Bin 856: 7774 of cap free
Amount of items: 2
Items: 
Size: 796939 Color: 4
Size: 195288 Color: 0

Bin 857: 7810 of cap free
Amount of items: 2
Items: 
Size: 655133 Color: 3
Size: 337058 Color: 1

Bin 858: 7897 of cap free
Amount of items: 2
Items: 
Size: 602466 Color: 14
Size: 389638 Color: 18

Bin 859: 8077 of cap free
Amount of items: 2
Items: 
Size: 559999 Color: 6
Size: 431925 Color: 12

Bin 860: 8356 of cap free
Amount of items: 2
Items: 
Size: 774261 Color: 0
Size: 217384 Color: 2

Bin 861: 8367 of cap free
Amount of items: 2
Items: 
Size: 751124 Color: 18
Size: 240510 Color: 11

Bin 862: 8522 of cap free
Amount of items: 2
Items: 
Size: 751075 Color: 0
Size: 240404 Color: 16

Bin 863: 8703 of cap free
Amount of items: 2
Items: 
Size: 796773 Color: 14
Size: 194525 Color: 7

Bin 864: 8711 of cap free
Amount of items: 2
Items: 
Size: 559831 Color: 8
Size: 431459 Color: 5

Bin 865: 8850 of cap free
Amount of items: 2
Items: 
Size: 694679 Color: 4
Size: 296472 Color: 18

Bin 866: 9667 of cap free
Amount of items: 2
Items: 
Size: 773271 Color: 8
Size: 217063 Color: 15

Bin 867: 9907 of cap free
Amount of items: 2
Items: 
Size: 773092 Color: 2
Size: 217002 Color: 10

Bin 868: 10342 of cap free
Amount of items: 2
Items: 
Size: 724157 Color: 5
Size: 265502 Color: 4

Bin 869: 10863 of cap free
Amount of items: 2
Items: 
Size: 712793 Color: 17
Size: 276345 Color: 14

Bin 870: 11434 of cap free
Amount of items: 2
Items: 
Size: 796371 Color: 4
Size: 192196 Color: 2

Bin 871: 11477 of cap free
Amount of items: 2
Items: 
Size: 749402 Color: 12
Size: 239122 Color: 7

Bin 872: 11797 of cap free
Amount of items: 2
Items: 
Size: 602437 Color: 13
Size: 385767 Color: 15

Bin 873: 12114 of cap free
Amount of items: 2
Items: 
Size: 795339 Color: 9
Size: 192548 Color: 4

Bin 874: 12364 of cap free
Amount of items: 2
Items: 
Size: 654287 Color: 19
Size: 333350 Color: 2

Bin 875: 12442 of cap free
Amount of items: 2
Items: 
Size: 515148 Color: 4
Size: 472411 Color: 19

Bin 876: 12842 of cap free
Amount of items: 2
Items: 
Size: 587911 Color: 9
Size: 399248 Color: 0

Bin 877: 13309 of cap free
Amount of items: 2
Items: 
Size: 588185 Color: 0
Size: 398507 Color: 10

Bin 878: 13478 of cap free
Amount of items: 2
Items: 
Size: 601549 Color: 13
Size: 384974 Color: 10

Bin 879: 14124 of cap free
Amount of items: 2
Items: 
Size: 794668 Color: 10
Size: 191209 Color: 16

Bin 880: 14288 of cap free
Amount of items: 2
Items: 
Size: 653401 Color: 4
Size: 332312 Color: 14

Bin 881: 14773 of cap free
Amount of items: 2
Items: 
Size: 513923 Color: 16
Size: 471305 Color: 0

Bin 882: 16328 of cap free
Amount of items: 2
Items: 
Size: 553387 Color: 2
Size: 430286 Color: 19

Bin 883: 16744 of cap free
Amount of items: 2
Items: 
Size: 793393 Color: 9
Size: 189864 Color: 1

Bin 884: 17120 of cap free
Amount of items: 2
Items: 
Size: 553300 Color: 12
Size: 429581 Color: 1

Bin 885: 17886 of cap free
Amount of items: 2
Items: 
Size: 649914 Color: 2
Size: 332201 Color: 3

Bin 886: 18937 of cap free
Amount of items: 2
Items: 
Size: 792288 Color: 14
Size: 188776 Color: 9

Bin 887: 19436 of cap free
Amount of items: 2
Items: 
Size: 509283 Color: 11
Size: 471282 Color: 2

Bin 888: 20570 of cap free
Amount of items: 2
Items: 
Size: 508697 Color: 16
Size: 470734 Color: 10

Bin 889: 22825 of cap free
Amount of items: 2
Items: 
Size: 792281 Color: 8
Size: 184895 Color: 11

Bin 890: 26895 of cap free
Amount of items: 2
Items: 
Size: 790808 Color: 13
Size: 182298 Color: 18

Bin 891: 28291 of cap free
Amount of items: 2
Items: 
Size: 585325 Color: 4
Size: 386385 Color: 13

Bin 892: 30670 of cap free
Amount of items: 2
Items: 
Size: 584530 Color: 18
Size: 384801 Color: 19

Bin 893: 32457 of cap free
Amount of items: 2
Items: 
Size: 583676 Color: 9
Size: 383868 Color: 1

Bin 894: 33463 of cap free
Amount of items: 2
Items: 
Size: 787271 Color: 6
Size: 179267 Color: 14

Bin 895: 35884 of cap free
Amount of items: 2
Items: 
Size: 649432 Color: 0
Size: 314685 Color: 4

Bin 896: 39256 of cap free
Amount of items: 2
Items: 
Size: 787047 Color: 16
Size: 173698 Color: 11

Bin 897: 58073 of cap free
Amount of items: 2
Items: 
Size: 786770 Color: 16
Size: 155158 Color: 12

Bin 898: 213336 of cap free
Amount of items: 1
Items: 
Size: 786665 Color: 7

Bin 899: 214645 of cap free
Amount of items: 1
Items: 
Size: 785356 Color: 2

Bin 900: 216540 of cap free
Amount of items: 1
Items: 
Size: 783461 Color: 9

Bin 901: 216646 of cap free
Amount of items: 1
Items: 
Size: 783355 Color: 2

Bin 902: 217001 of cap free
Amount of items: 1
Items: 
Size: 783000 Color: 10

Bin 903: 232828 of cap free
Amount of items: 1
Items: 
Size: 767173 Color: 6

Bin 904: 235998 of cap free
Amount of items: 1
Items: 
Size: 764003 Color: 5

Bin 905: 238556 of cap free
Amount of items: 1
Items: 
Size: 761445 Color: 16

Bin 906: 238918 of cap free
Amount of items: 1
Items: 
Size: 761083 Color: 16

Bin 907: 254823 of cap free
Amount of items: 1
Items: 
Size: 745178 Color: 3

Bin 908: 288783 of cap free
Amount of items: 1
Items: 
Size: 711218 Color: 6

Total size: 904149250
Total free space: 3851658

