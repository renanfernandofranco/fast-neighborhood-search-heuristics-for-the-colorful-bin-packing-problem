Capicity Bin: 1000001
Lower Bound: 4509

Bins used: 4512
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 726612 Color: 1
Size: 161536 Color: 0
Size: 111853 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 740450 Color: 0
Size: 146849 Color: 0
Size: 112702 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 761734 Color: 1
Size: 126512 Color: 0
Size: 111755 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 512357 Color: 0
Size: 347056 Color: 1
Size: 140588 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 775505 Color: 1
Size: 112709 Color: 1
Size: 111787 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 739267 Color: 1
Size: 151997 Color: 1
Size: 108737 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 447937 Color: 1
Size: 388771 Color: 0
Size: 163293 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 553850 Color: 0
Size: 266627 Color: 1
Size: 179524 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 777176 Color: 0
Size: 112031 Color: 0
Size: 110794 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 756537 Color: 0
Size: 140385 Color: 1
Size: 103079 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 783037 Color: 0
Size: 114213 Color: 1
Size: 102751 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 775339 Color: 0
Size: 112383 Color: 1
Size: 112279 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 765420 Color: 1
Size: 130357 Color: 0
Size: 104224 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 762289 Color: 0
Size: 132720 Color: 1
Size: 104992 Color: 1

Bin 15: 0 of cap free
Amount of items: 5
Items: 
Size: 215480 Color: 1
Size: 212415 Color: 0
Size: 198945 Color: 1
Size: 194173 Color: 0
Size: 178988 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 759407 Color: 0
Size: 139624 Color: 1
Size: 100970 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 761707 Color: 1
Size: 136754 Color: 0
Size: 101540 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 769844 Color: 0
Size: 122116 Color: 1
Size: 108041 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 774879 Color: 0
Size: 113913 Color: 0
Size: 111209 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 766483 Color: 0
Size: 128738 Color: 0
Size: 104780 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 512365 Color: 0
Size: 349734 Color: 0
Size: 137902 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 762016 Color: 0
Size: 136544 Color: 1
Size: 101441 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 785414 Color: 1
Size: 111613 Color: 0
Size: 102974 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 784057 Color: 1
Size: 111011 Color: 0
Size: 104933 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 774144 Color: 1
Size: 113477 Color: 0
Size: 112380 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 791122 Color: 1
Size: 108352 Color: 0
Size: 100527 Color: 0

Bin 27: 0 of cap free
Amount of items: 7
Items: 
Size: 161661 Color: 1
Size: 151422 Color: 1
Size: 150731 Color: 0
Size: 141312 Color: 0
Size: 135123 Color: 1
Size: 130302 Color: 0
Size: 129450 Color: 0

Bin 28: 0 of cap free
Amount of items: 5
Items: 
Size: 259217 Color: 0
Size: 252327 Color: 0
Size: 237272 Color: 1
Size: 148673 Color: 1
Size: 102512 Color: 0

Bin 29: 0 of cap free
Amount of items: 4
Items: 
Size: 379038 Color: 1
Size: 261593 Color: 0
Size: 246220 Color: 0
Size: 113150 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 743505 Color: 0
Size: 154599 Color: 1
Size: 101897 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 748568 Color: 1
Size: 131768 Color: 0
Size: 119665 Color: 0

Bin 32: 0 of cap free
Amount of items: 6
Items: 
Size: 299866 Color: 0
Size: 169206 Color: 1
Size: 148024 Color: 1
Size: 143117 Color: 1
Size: 125816 Color: 0
Size: 113972 Color: 0

Bin 33: 0 of cap free
Amount of items: 5
Items: 
Size: 400881 Color: 0
Size: 161725 Color: 0
Size: 154610 Color: 1
Size: 154473 Color: 1
Size: 128312 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 759404 Color: 0
Size: 137325 Color: 1
Size: 103272 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 776811 Color: 1
Size: 120638 Color: 0
Size: 102552 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 738385 Color: 1
Size: 150142 Color: 0
Size: 111474 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 762830 Color: 0
Size: 134335 Color: 1
Size: 102836 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 798620 Color: 0
Size: 100977 Color: 1
Size: 100404 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 541088 Color: 0
Size: 350074 Color: 1
Size: 108839 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 790247 Color: 0
Size: 107107 Color: 1
Size: 102647 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 402272 Color: 1
Size: 375009 Color: 1
Size: 222720 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 751846 Color: 1
Size: 141540 Color: 1
Size: 106615 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 787978 Color: 1
Size: 106085 Color: 0
Size: 105938 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 797694 Color: 0
Size: 102152 Color: 0
Size: 100155 Color: 1

Bin 45: 0 of cap free
Amount of items: 5
Items: 
Size: 264887 Color: 0
Size: 258358 Color: 0
Size: 250845 Color: 1
Size: 119679 Color: 1
Size: 106232 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 377688 Color: 1
Size: 337540 Color: 0
Size: 284773 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 391529 Color: 0
Size: 379043 Color: 1
Size: 229429 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 714648 Color: 0
Size: 184205 Color: 0
Size: 101148 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 447784 Color: 1
Size: 446964 Color: 0
Size: 105253 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 686847 Color: 0
Size: 202574 Color: 1
Size: 110580 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 722565 Color: 1
Size: 170146 Color: 0
Size: 107290 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 598673 Color: 0
Size: 208414 Color: 1
Size: 192914 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 792029 Color: 1
Size: 104501 Color: 0
Size: 103471 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 672955 Color: 0
Size: 167217 Color: 1
Size: 159829 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 763799 Color: 1
Size: 123543 Color: 0
Size: 112659 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 610087 Color: 1
Size: 203253 Color: 0
Size: 186661 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 700035 Color: 1
Size: 177612 Color: 0
Size: 122354 Color: 1

Bin 58: 0 of cap free
Amount of items: 6
Items: 
Size: 198142 Color: 0
Size: 182556 Color: 0
Size: 180127 Color: 1
Size: 173099 Color: 1
Size: 165656 Color: 1
Size: 100421 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 724902 Color: 0
Size: 155432 Color: 1
Size: 119667 Color: 0

Bin 60: 0 of cap free
Amount of items: 6
Items: 
Size: 205068 Color: 1
Size: 171214 Color: 0
Size: 170250 Color: 0
Size: 166932 Color: 1
Size: 153284 Color: 1
Size: 133253 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 735198 Color: 1
Size: 161304 Color: 1
Size: 103499 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 609296 Color: 0
Size: 261290 Color: 1
Size: 129415 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 501158 Color: 0
Size: 263825 Color: 1
Size: 235018 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 626308 Color: 1
Size: 262229 Color: 0
Size: 111464 Color: 0

Bin 65: 0 of cap free
Amount of items: 6
Items: 
Size: 234451 Color: 1
Size: 181466 Color: 0
Size: 175631 Color: 1
Size: 161910 Color: 0
Size: 142653 Color: 1
Size: 103890 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 735791 Color: 0
Size: 158986 Color: 1
Size: 105224 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 796439 Color: 1
Size: 102727 Color: 0
Size: 100835 Color: 1

Bin 68: 0 of cap free
Amount of items: 5
Items: 
Size: 314101 Color: 0
Size: 213415 Color: 0
Size: 185426 Color: 1
Size: 153057 Color: 1
Size: 134002 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 780553 Color: 0
Size: 112580 Color: 1
Size: 106868 Color: 1

Bin 70: 0 of cap free
Amount of items: 6
Items: 
Size: 178562 Color: 0
Size: 177216 Color: 0
Size: 177107 Color: 0
Size: 169537 Color: 1
Size: 169504 Color: 1
Size: 128075 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 718200 Color: 1
Size: 141550 Color: 0
Size: 140251 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 721223 Color: 0
Size: 147143 Color: 1
Size: 131635 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 756845 Color: 1
Size: 129817 Color: 1
Size: 113339 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 656805 Color: 0
Size: 209529 Color: 1
Size: 133667 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 761742 Color: 0
Size: 123536 Color: 1
Size: 114723 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 751854 Color: 1
Size: 124230 Color: 0
Size: 123917 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 733692 Color: 0
Size: 160107 Color: 1
Size: 106202 Color: 1

Bin 78: 0 of cap free
Amount of items: 7
Items: 
Size: 166386 Color: 1
Size: 160225 Color: 1
Size: 160202 Color: 1
Size: 152466 Color: 0
Size: 152185 Color: 0
Size: 106683 Color: 0
Size: 101854 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 744046 Color: 1
Size: 143314 Color: 0
Size: 112641 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 721299 Color: 1
Size: 141662 Color: 1
Size: 137040 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 748475 Color: 1
Size: 150265 Color: 0
Size: 101261 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 775328 Color: 0
Size: 123376 Color: 1
Size: 101297 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 575895 Color: 1
Size: 301239 Color: 0
Size: 122867 Color: 0

Bin 84: 0 of cap free
Amount of items: 7
Items: 
Size: 157420 Color: 0
Size: 148865 Color: 0
Size: 144703 Color: 1
Size: 144654 Color: 0
Size: 139770 Color: 1
Size: 138161 Color: 0
Size: 126428 Color: 1

Bin 85: 0 of cap free
Amount of items: 5
Items: 
Size: 284624 Color: 0
Size: 193748 Color: 0
Size: 192787 Color: 1
Size: 192263 Color: 1
Size: 136579 Color: 1

Bin 86: 0 of cap free
Amount of items: 7
Items: 
Size: 176355 Color: 1
Size: 151645 Color: 1
Size: 142176 Color: 1
Size: 140135 Color: 1
Size: 135528 Color: 0
Size: 135185 Color: 0
Size: 118977 Color: 0

Bin 87: 0 of cap free
Amount of items: 5
Items: 
Size: 236491 Color: 0
Size: 191899 Color: 0
Size: 191052 Color: 0
Size: 190390 Color: 1
Size: 190169 Color: 1

Bin 88: 0 of cap free
Amount of items: 5
Items: 
Size: 239946 Color: 1
Size: 235104 Color: 1
Size: 199238 Color: 0
Size: 187575 Color: 0
Size: 138138 Color: 0

Bin 89: 0 of cap free
Amount of items: 6
Items: 
Size: 362994 Color: 1
Size: 135904 Color: 0
Size: 133648 Color: 1
Size: 129777 Color: 0
Size: 127704 Color: 1
Size: 109974 Color: 0

Bin 90: 0 of cap free
Amount of items: 7
Items: 
Size: 220399 Color: 0
Size: 133000 Color: 1
Size: 132368 Color: 1
Size: 131223 Color: 1
Size: 131135 Color: 1
Size: 127329 Color: 0
Size: 124547 Color: 0

Bin 91: 0 of cap free
Amount of items: 8
Items: 
Size: 153083 Color: 1
Size: 126698 Color: 1
Size: 124007 Color: 0
Size: 122954 Color: 1
Size: 122456 Color: 0
Size: 120537 Color: 1
Size: 120435 Color: 0
Size: 109831 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 770702 Color: 0
Size: 120386 Color: 1
Size: 108913 Color: 1

Bin 93: 0 of cap free
Amount of items: 5
Items: 
Size: 219871 Color: 0
Size: 219080 Color: 0
Size: 199266 Color: 1
Size: 186525 Color: 1
Size: 175259 Color: 0

Bin 94: 0 of cap free
Amount of items: 5
Items: 
Size: 224360 Color: 0
Size: 221978 Color: 0
Size: 199853 Color: 1
Size: 184755 Color: 1
Size: 169055 Color: 0

Bin 95: 0 of cap free
Amount of items: 5
Items: 
Size: 244650 Color: 0
Size: 244578 Color: 0
Size: 214325 Color: 1
Size: 175558 Color: 1
Size: 120890 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 408312 Color: 1
Size: 366582 Color: 0
Size: 225107 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 422155 Color: 1
Size: 394472 Color: 0
Size: 183374 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 422452 Color: 1
Size: 415871 Color: 0
Size: 161678 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 443435 Color: 0
Size: 369415 Color: 0
Size: 187151 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 443521 Color: 0
Size: 402348 Color: 1
Size: 154132 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 444260 Color: 0
Size: 379413 Color: 1
Size: 176328 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 451192 Color: 0
Size: 370688 Color: 1
Size: 178121 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 468459 Color: 1
Size: 337515 Color: 0
Size: 194027 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 468058 Color: 0
Size: 389516 Color: 0
Size: 142427 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 468674 Color: 0
Size: 369104 Color: 1
Size: 162223 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 479603 Color: 0
Size: 347673 Color: 1
Size: 172725 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 483230 Color: 0
Size: 359887 Color: 0
Size: 156884 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 483734 Color: 0
Size: 343960 Color: 1
Size: 172307 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 492374 Color: 0
Size: 336002 Color: 1
Size: 171625 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 492532 Color: 0
Size: 330037 Color: 1
Size: 177432 Color: 0

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 503360 Color: 1
Size: 496641 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 508141 Color: 0
Size: 336923 Color: 0
Size: 154937 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 508569 Color: 0
Size: 307429 Color: 0
Size: 184003 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 510820 Color: 1
Size: 344367 Color: 1
Size: 144814 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 512750 Color: 0
Size: 339113 Color: 0
Size: 148138 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 513515 Color: 1
Size: 375283 Color: 1
Size: 111203 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 513629 Color: 0
Size: 335685 Color: 1
Size: 150687 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 513833 Color: 1
Size: 287519 Color: 0
Size: 198649 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 513870 Color: 0
Size: 314294 Color: 1
Size: 171837 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 514593 Color: 0
Size: 307497 Color: 0
Size: 177911 Color: 1

Bin 121: 0 of cap free
Amount of items: 2
Items: 
Size: 516762 Color: 1
Size: 483239 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 521256 Color: 0
Size: 344567 Color: 1
Size: 134178 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 525774 Color: 0
Size: 305738 Color: 0
Size: 168489 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 533196 Color: 1
Size: 273887 Color: 0
Size: 192918 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 534210 Color: 0
Size: 315583 Color: 1
Size: 150208 Color: 1

Bin 126: 0 of cap free
Amount of items: 2
Items: 
Size: 535461 Color: 0
Size: 464540 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 535849 Color: 0
Size: 287444 Color: 0
Size: 176708 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 537014 Color: 1
Size: 250545 Color: 1
Size: 212442 Color: 0

Bin 129: 0 of cap free
Amount of items: 2
Items: 
Size: 537481 Color: 1
Size: 462520 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 538056 Color: 1
Size: 336735 Color: 0
Size: 125210 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 539764 Color: 0
Size: 305591 Color: 0
Size: 154646 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 541130 Color: 0
Size: 315176 Color: 0
Size: 143695 Color: 1

Bin 133: 0 of cap free
Amount of items: 2
Items: 
Size: 548100 Color: 0
Size: 451901 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 553539 Color: 0
Size: 279562 Color: 1
Size: 166900 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 554163 Color: 0
Size: 252337 Color: 0
Size: 193501 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 554347 Color: 0
Size: 251145 Color: 1
Size: 194509 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 554379 Color: 0
Size: 267849 Color: 0
Size: 177773 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 554554 Color: 0
Size: 335696 Color: 1
Size: 109751 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 561908 Color: 1
Size: 307475 Color: 0
Size: 130618 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 561939 Color: 1
Size: 308854 Color: 0
Size: 129208 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 565713 Color: 0
Size: 265116 Color: 0
Size: 169172 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 566664 Color: 1
Size: 252607 Color: 0
Size: 180730 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 566698 Color: 1
Size: 267207 Color: 0
Size: 166096 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 567439 Color: 0
Size: 252733 Color: 0
Size: 179829 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 567469 Color: 0
Size: 246691 Color: 0
Size: 185841 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 567583 Color: 0
Size: 256356 Color: 0
Size: 176062 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 567511 Color: 1
Size: 301589 Color: 0
Size: 130901 Color: 1

Bin 148: 0 of cap free
Amount of items: 2
Items: 
Size: 569639 Color: 1
Size: 430362 Color: 0

Bin 149: 0 of cap free
Amount of items: 2
Items: 
Size: 574780 Color: 1
Size: 425221 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 576920 Color: 1
Size: 238382 Color: 0
Size: 184699 Color: 0

Bin 151: 0 of cap free
Amount of items: 2
Items: 
Size: 584143 Color: 1
Size: 415858 Color: 0

Bin 152: 0 of cap free
Amount of items: 2
Items: 
Size: 585838 Color: 1
Size: 414163 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 586084 Color: 1
Size: 244500 Color: 0
Size: 169417 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 586649 Color: 1
Size: 256387 Color: 0
Size: 156965 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 591320 Color: 1
Size: 286675 Color: 1
Size: 122006 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 591623 Color: 1
Size: 239019 Color: 0
Size: 169359 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 591915 Color: 0
Size: 243300 Color: 0
Size: 164786 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 592571 Color: 0
Size: 230000 Color: 1
Size: 177430 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 592867 Color: 1
Size: 266219 Color: 0
Size: 140915 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 598316 Color: 1
Size: 217314 Color: 0
Size: 184371 Color: 1

Bin 161: 0 of cap free
Amount of items: 2
Items: 
Size: 600251 Color: 1
Size: 399750 Color: 0

Bin 162: 0 of cap free
Amount of items: 2
Items: 
Size: 602276 Color: 0
Size: 397725 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 607028 Color: 1
Size: 217634 Color: 0
Size: 175339 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 607069 Color: 0
Size: 225004 Color: 0
Size: 167928 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 607547 Color: 0
Size: 225613 Color: 0
Size: 166841 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 607986 Color: 0
Size: 198883 Color: 1
Size: 193132 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 610016 Color: 1
Size: 244187 Color: 0
Size: 145798 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 610116 Color: 0
Size: 219157 Color: 0
Size: 170728 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 610317 Color: 0
Size: 252729 Color: 0
Size: 136955 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 612030 Color: 0
Size: 246503 Color: 0
Size: 141468 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 613041 Color: 0
Size: 195624 Color: 1
Size: 191336 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 612948 Color: 1
Size: 251454 Color: 1
Size: 135599 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 617238 Color: 1
Size: 194644 Color: 0
Size: 188119 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 617256 Color: 1
Size: 225008 Color: 0
Size: 157737 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 621123 Color: 0
Size: 197354 Color: 1
Size: 181524 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 621388 Color: 1
Size: 190992 Color: 0
Size: 187621 Color: 1

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 625108 Color: 0
Size: 238402 Color: 1
Size: 136491 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 625259 Color: 0
Size: 196839 Color: 1
Size: 177903 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 625390 Color: 1
Size: 189110 Color: 1
Size: 185501 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 625475 Color: 1
Size: 195863 Color: 0
Size: 178663 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 630872 Color: 1
Size: 190208 Color: 0
Size: 178921 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 633393 Color: 0
Size: 198561 Color: 1
Size: 168047 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 633446 Color: 1
Size: 190926 Color: 0
Size: 175629 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 633454 Color: 0
Size: 187879 Color: 0
Size: 178668 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 633700 Color: 1
Size: 229530 Color: 1
Size: 136771 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 633723 Color: 0
Size: 197384 Color: 0
Size: 168894 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 633997 Color: 1
Size: 219141 Color: 0
Size: 146863 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 637268 Color: 0
Size: 247591 Color: 1
Size: 115142 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 638206 Color: 0
Size: 181514 Color: 0
Size: 180281 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 640146 Color: 1
Size: 233715 Color: 1
Size: 126140 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 640185 Color: 0
Size: 244497 Color: 1
Size: 115319 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 640279 Color: 1
Size: 181608 Color: 0
Size: 178114 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 640258 Color: 0
Size: 184924 Color: 1
Size: 174819 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 642368 Color: 0
Size: 183732 Color: 1
Size: 173901 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 642246 Color: 1
Size: 183135 Color: 1
Size: 174620 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 642708 Color: 0
Size: 221157 Color: 0
Size: 136136 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 643594 Color: 1
Size: 181868 Color: 0
Size: 174539 Color: 1

Bin 198: 0 of cap free
Amount of items: 2
Items: 
Size: 645266 Color: 0
Size: 354735 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 646303 Color: 1
Size: 193159 Color: 0
Size: 160539 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 650060 Color: 0
Size: 194451 Color: 1
Size: 155490 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 650326 Color: 1
Size: 189388 Color: 1
Size: 160287 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 650335 Color: 1
Size: 186940 Color: 0
Size: 162726 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 650343 Color: 1
Size: 176113 Color: 1
Size: 173545 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 650231 Color: 0
Size: 191153 Color: 1
Size: 158617 Color: 0

Bin 205: 0 of cap free
Amount of items: 2
Items: 
Size: 650971 Color: 0
Size: 349030 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 651048 Color: 1
Size: 188722 Color: 1
Size: 160231 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 651160 Color: 1
Size: 181982 Color: 0
Size: 166859 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 651696 Color: 1
Size: 178766 Color: 0
Size: 169539 Color: 1

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 651754 Color: 1
Size: 180973 Color: 0
Size: 167274 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 653625 Color: 1
Size: 191712 Color: 0
Size: 154664 Color: 0

Bin 211: 0 of cap free
Amount of items: 2
Items: 
Size: 657077 Color: 0
Size: 342924 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 661142 Color: 1
Size: 178352 Color: 0
Size: 160507 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 663360 Color: 0
Size: 180436 Color: 1
Size: 156205 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 663385 Color: 0
Size: 169932 Color: 0
Size: 166684 Color: 1

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 663536 Color: 1
Size: 336465 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 663502 Color: 0
Size: 168658 Color: 1
Size: 167841 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 663702 Color: 1
Size: 189070 Color: 0
Size: 147229 Color: 1

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 663788 Color: 1
Size: 178618 Color: 0
Size: 157595 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 665176 Color: 0
Size: 179081 Color: 1
Size: 155744 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 665204 Color: 0
Size: 174275 Color: 1
Size: 160522 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 665244 Color: 0
Size: 181119 Color: 1
Size: 153638 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 666121 Color: 1
Size: 193321 Color: 1
Size: 140559 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 666188 Color: 1
Size: 170590 Color: 0
Size: 163223 Color: 0

Bin 224: 0 of cap free
Amount of items: 2
Items: 
Size: 667436 Color: 0
Size: 332565 Color: 1

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 668657 Color: 0
Size: 215096 Color: 1
Size: 116248 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 669842 Color: 1
Size: 183368 Color: 0
Size: 146791 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 670091 Color: 0
Size: 170761 Color: 0
Size: 159149 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 670106 Color: 0
Size: 170864 Color: 1
Size: 159031 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 670252 Color: 1
Size: 172443 Color: 1
Size: 157306 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 670356 Color: 0
Size: 176829 Color: 0
Size: 152816 Color: 1

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 671599 Color: 0
Size: 171214 Color: 1
Size: 157188 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 671768 Color: 0
Size: 177026 Color: 1
Size: 151207 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 671770 Color: 0
Size: 181246 Color: 0
Size: 146985 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 671785 Color: 1
Size: 178799 Color: 1
Size: 149417 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 671773 Color: 0
Size: 174931 Color: 1
Size: 153297 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 671913 Color: 1
Size: 166178 Color: 1
Size: 161910 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 671915 Color: 1
Size: 175661 Color: 1
Size: 152425 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 672082 Color: 0
Size: 170328 Color: 0
Size: 157591 Color: 1

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 672139 Color: 0
Size: 166932 Color: 1
Size: 160930 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 672248 Color: 0
Size: 196931 Color: 0
Size: 130822 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 672391 Color: 1
Size: 192876 Color: 0
Size: 134734 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 672420 Color: 0
Size: 167900 Color: 0
Size: 159681 Color: 1

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 672583 Color: 1
Size: 164649 Color: 0
Size: 162769 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 672728 Color: 0
Size: 175802 Color: 0
Size: 151471 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 672623 Color: 1
Size: 197491 Color: 1
Size: 129887 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 672816 Color: 1
Size: 164666 Color: 0
Size: 162519 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 674657 Color: 0
Size: 189889 Color: 0
Size: 135455 Color: 1

Bin 248: 0 of cap free
Amount of items: 2
Items: 
Size: 675448 Color: 0
Size: 324553 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 675434 Color: 1
Size: 170751 Color: 0
Size: 153816 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 675735 Color: 1
Size: 192681 Color: 0
Size: 131585 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 675786 Color: 1
Size: 181081 Color: 1
Size: 143134 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 675805 Color: 1
Size: 174027 Color: 0
Size: 150169 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 676932 Color: 0
Size: 169745 Color: 1
Size: 153324 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 677167 Color: 0
Size: 173560 Color: 1
Size: 149274 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 678450 Color: 1
Size: 195166 Color: 1
Size: 126385 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 678581 Color: 0
Size: 163059 Color: 1
Size: 158361 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 678956 Color: 0
Size: 193672 Color: 1
Size: 127373 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 679093 Color: 0
Size: 194298 Color: 0
Size: 126610 Color: 1

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 679112 Color: 0
Size: 320889 Color: 1

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 679176 Color: 1
Size: 191868 Color: 1
Size: 128957 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 679119 Color: 0
Size: 161087 Color: 0
Size: 159795 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 686502 Color: 1
Size: 175012 Color: 0
Size: 138487 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 687285 Color: 1
Size: 167756 Color: 0
Size: 144960 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 687855 Color: 0
Size: 163697 Color: 0
Size: 148449 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 688289 Color: 0
Size: 184608 Color: 1
Size: 127104 Color: 1

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 688309 Color: 0
Size: 163305 Color: 1
Size: 148387 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 688479 Color: 1
Size: 189519 Color: 1
Size: 122003 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 688835 Color: 0
Size: 174558 Color: 0
Size: 136608 Color: 1

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 689890 Color: 1
Size: 186635 Color: 0
Size: 123476 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 690058 Color: 0
Size: 169113 Color: 1
Size: 140830 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 690653 Color: 0
Size: 164833 Color: 0
Size: 144515 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 690675 Color: 0
Size: 180129 Color: 1
Size: 129197 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 690812 Color: 0
Size: 169944 Color: 0
Size: 139245 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 690988 Color: 1
Size: 173612 Color: 0
Size: 135401 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 690924 Color: 0
Size: 177389 Color: 0
Size: 131688 Color: 1

Bin 276: 0 of cap free
Amount of items: 2
Items: 
Size: 694299 Color: 0
Size: 305702 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 695466 Color: 0
Size: 165766 Color: 0
Size: 138769 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 695745 Color: 0
Size: 171361 Color: 1
Size: 132895 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 695867 Color: 1
Size: 170822 Color: 0
Size: 133312 Color: 1

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 695765 Color: 0
Size: 165051 Color: 0
Size: 139185 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 696339 Color: 0
Size: 156248 Color: 0
Size: 147414 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 696311 Color: 1
Size: 186620 Color: 0
Size: 117070 Color: 1

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 696352 Color: 0
Size: 175347 Color: 1
Size: 128302 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 697645 Color: 0
Size: 177468 Color: 1
Size: 124888 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 698058 Color: 0
Size: 186404 Color: 1
Size: 115539 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 698086 Color: 0
Size: 177466 Color: 1
Size: 124449 Color: 1

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 698541 Color: 0
Size: 164690 Color: 1
Size: 136770 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 699071 Color: 1
Size: 163576 Color: 0
Size: 137354 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 699113 Color: 1
Size: 153723 Color: 0
Size: 147165 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 699074 Color: 0
Size: 160962 Color: 1
Size: 139965 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 699237 Color: 0
Size: 176933 Color: 0
Size: 123831 Color: 1

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 699515 Color: 1
Size: 163588 Color: 1
Size: 136898 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 699527 Color: 0
Size: 162714 Color: 0
Size: 137760 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 699543 Color: 1
Size: 175291 Color: 1
Size: 125167 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 699530 Color: 0
Size: 161943 Color: 1
Size: 138528 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 699828 Color: 1
Size: 158299 Color: 1
Size: 141874 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 699801 Color: 0
Size: 171024 Color: 1
Size: 129176 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 700811 Color: 0
Size: 170076 Color: 1
Size: 129114 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 701490 Color: 0
Size: 153750 Color: 1
Size: 144761 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 701670 Color: 0
Size: 171284 Color: 1
Size: 127047 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 701862 Color: 0
Size: 168066 Color: 0
Size: 130073 Color: 1

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 701903 Color: 0
Size: 174074 Color: 1
Size: 124024 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 701874 Color: 1
Size: 156221 Color: 1
Size: 141906 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 701921 Color: 0
Size: 157264 Color: 0
Size: 140816 Color: 1

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 701982 Color: 1
Size: 169618 Color: 1
Size: 128401 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 701935 Color: 0
Size: 161450 Color: 1
Size: 136616 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 702191 Color: 1
Size: 168842 Color: 1
Size: 128968 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 702351 Color: 0
Size: 156981 Color: 0
Size: 140669 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 702447 Color: 0
Size: 181676 Color: 0
Size: 115878 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 705173 Color: 0
Size: 148363 Color: 0
Size: 146465 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 705176 Color: 1
Size: 160796 Color: 0
Size: 134029 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 705414 Color: 1
Size: 155253 Color: 0
Size: 139334 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 705668 Color: 1
Size: 160416 Color: 0
Size: 133917 Color: 1

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 705741 Color: 0
Size: 148518 Color: 0
Size: 145742 Color: 1

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 705750 Color: 1
Size: 176534 Color: 0
Size: 117717 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 706063 Color: 1
Size: 153167 Color: 1
Size: 140771 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 706195 Color: 0
Size: 175338 Color: 1
Size: 118468 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 706558 Color: 0
Size: 172612 Color: 0
Size: 120831 Color: 1

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 707160 Color: 0
Size: 157740 Color: 1
Size: 135101 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 707403 Color: 0
Size: 152936 Color: 1
Size: 139662 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 707965 Color: 0
Size: 151725 Color: 1
Size: 140311 Color: 1

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 708244 Color: 1
Size: 156601 Color: 0
Size: 135156 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 712397 Color: 0
Size: 164162 Color: 1
Size: 123442 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 712401 Color: 1
Size: 144436 Color: 0
Size: 143164 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 715875 Color: 1
Size: 148051 Color: 0
Size: 136075 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 715986 Color: 1
Size: 153936 Color: 1
Size: 130079 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 716091 Color: 0
Size: 149521 Color: 1
Size: 134389 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 716083 Color: 1
Size: 158506 Color: 0
Size: 125412 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 716273 Color: 0
Size: 164615 Color: 0
Size: 119113 Color: 1

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 716260 Color: 1
Size: 159285 Color: 1
Size: 124456 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 716346 Color: 0
Size: 159076 Color: 1
Size: 124579 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 717343 Color: 1
Size: 154694 Color: 0
Size: 127964 Color: 1

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 720178 Color: 0
Size: 141334 Color: 1
Size: 138489 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 720718 Color: 0
Size: 156401 Color: 0
Size: 122882 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 720669 Color: 1
Size: 150580 Color: 0
Size: 128752 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 720837 Color: 1
Size: 158744 Color: 0
Size: 120420 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 721113 Color: 1
Size: 172013 Color: 1
Size: 106875 Color: 0

Bin 338: 0 of cap free
Amount of items: 2
Items: 
Size: 721748 Color: 1
Size: 278253 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 723374 Color: 0
Size: 143776 Color: 1
Size: 132851 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 723602 Color: 1
Size: 157281 Color: 0
Size: 119118 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 723670 Color: 1
Size: 138335 Color: 0
Size: 137996 Color: 1

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 723690 Color: 0
Size: 151109 Color: 0
Size: 125202 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 726131 Color: 0
Size: 154289 Color: 1
Size: 119581 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 726125 Color: 1
Size: 138969 Color: 0
Size: 134907 Color: 1

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 726565 Color: 0
Size: 145805 Color: 0
Size: 127631 Color: 1

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 726610 Color: 1
Size: 148645 Color: 0
Size: 124746 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 726975 Color: 0
Size: 152167 Color: 0
Size: 120859 Color: 1

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 727102 Color: 1
Size: 156348 Color: 0
Size: 116551 Color: 1

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 727032 Color: 0
Size: 150729 Color: 0
Size: 122240 Color: 1

Bin 350: 0 of cap free
Amount of items: 2
Items: 
Size: 727292 Color: 0
Size: 272709 Color: 1

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 730261 Color: 1
Size: 145264 Color: 0
Size: 124476 Color: 1

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 730273 Color: 0
Size: 147755 Color: 0
Size: 121973 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 734544 Color: 1
Size: 139001 Color: 0
Size: 126456 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 735023 Color: 1
Size: 154299 Color: 0
Size: 110679 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 734937 Color: 0
Size: 149583 Color: 0
Size: 115481 Color: 1

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 736404 Color: 0
Size: 148036 Color: 1
Size: 115561 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 738193 Color: 0
Size: 140858 Color: 0
Size: 120950 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 738354 Color: 1
Size: 138617 Color: 0
Size: 123030 Color: 1

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 738304 Color: 0
Size: 133526 Color: 0
Size: 128171 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 738482 Color: 1
Size: 137228 Color: 0
Size: 124291 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 738641 Color: 1
Size: 140754 Color: 0
Size: 120606 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 739063 Color: 1
Size: 147545 Color: 1
Size: 113393 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 739742 Color: 0
Size: 136668 Color: 1
Size: 123591 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 739786 Color: 0
Size: 140552 Color: 1
Size: 119663 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 740081 Color: 0
Size: 136909 Color: 0
Size: 123011 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 740307 Color: 0
Size: 144588 Color: 1
Size: 115106 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 740313 Color: 0
Size: 149012 Color: 1
Size: 110676 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 742971 Color: 1
Size: 137656 Color: 1
Size: 119374 Color: 0

Bin 369: 0 of cap free
Amount of items: 2
Items: 
Size: 744040 Color: 0
Size: 255961 Color: 1

Bin 370: 0 of cap free
Amount of items: 2
Items: 
Size: 744242 Color: 0
Size: 255759 Color: 1

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 745354 Color: 0
Size: 141570 Color: 0
Size: 113077 Color: 1

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 752072 Color: 0
Size: 247929 Color: 1

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 757304 Color: 0
Size: 242697 Color: 1

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 761602 Color: 0
Size: 128713 Color: 1
Size: 109686 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 762804 Color: 0
Size: 123704 Color: 1
Size: 113493 Color: 0

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 765173 Color: 1
Size: 234828 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 766880 Color: 1
Size: 121013 Color: 0
Size: 112108 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 766885 Color: 1
Size: 127076 Color: 0
Size: 106040 Color: 1

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 769980 Color: 0
Size: 124448 Color: 0
Size: 105573 Color: 1

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 772793 Color: 0
Size: 121524 Color: 1
Size: 105684 Color: 0

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 773468 Color: 0
Size: 226533 Color: 1

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 775393 Color: 0
Size: 118549 Color: 1
Size: 106059 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 775582 Color: 0
Size: 115249 Color: 1
Size: 109170 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 775598 Color: 0
Size: 121732 Color: 0
Size: 102671 Color: 1

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 775683 Color: 0
Size: 119052 Color: 1
Size: 105266 Color: 1

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 775872 Color: 0
Size: 119382 Color: 1
Size: 104747 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 775988 Color: 0
Size: 112966 Color: 1
Size: 111047 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 776384 Color: 0
Size: 122090 Color: 0
Size: 101527 Color: 1

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 777725 Color: 0
Size: 120274 Color: 0
Size: 102002 Color: 1

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 778676 Color: 0
Size: 112703 Color: 1
Size: 108622 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 779700 Color: 1
Size: 114564 Color: 0
Size: 105737 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 779764 Color: 0
Size: 116333 Color: 0
Size: 103904 Color: 1

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 786482 Color: 0
Size: 109734 Color: 1
Size: 103785 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 786528 Color: 0
Size: 107268 Color: 1
Size: 106205 Color: 1

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 789435 Color: 0
Size: 107327 Color: 1
Size: 103239 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 790118 Color: 0
Size: 105106 Color: 1
Size: 104777 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 791627 Color: 0
Size: 107731 Color: 0
Size: 100643 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 797841 Color: 1
Size: 101258 Color: 0
Size: 100902 Color: 1

Bin 399: 1 of cap free
Amount of items: 5
Items: 
Size: 231168 Color: 0
Size: 231139 Color: 0
Size: 203624 Color: 1
Size: 191038 Color: 1
Size: 143031 Color: 1

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 422612 Color: 1
Size: 339303 Color: 0
Size: 238085 Color: 1

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 425052 Color: 1
Size: 394124 Color: 0
Size: 180824 Color: 0

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 437851 Color: 0
Size: 407488 Color: 1
Size: 154661 Color: 0

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 438052 Color: 0
Size: 369297 Color: 1
Size: 192651 Color: 0

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 439150 Color: 0
Size: 377346 Color: 1
Size: 183504 Color: 0

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 449595 Color: 0
Size: 378920 Color: 1
Size: 171485 Color: 0

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 451200 Color: 0
Size: 405050 Color: 1
Size: 143750 Color: 1

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 454445 Color: 0
Size: 407279 Color: 1
Size: 138276 Color: 1

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 468048 Color: 1
Size: 346925 Color: 1
Size: 185027 Color: 0

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 468854 Color: 1
Size: 379188 Color: 1
Size: 151958 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 470084 Color: 1
Size: 337780 Color: 0
Size: 192136 Color: 0

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 483427 Color: 0
Size: 363049 Color: 1
Size: 153524 Color: 0

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 489909 Color: 0
Size: 314198 Color: 1
Size: 195893 Color: 0

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 490935 Color: 0
Size: 315721 Color: 1
Size: 193344 Color: 1

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 493367 Color: 0
Size: 375107 Color: 0
Size: 131526 Color: 1

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 495986 Color: 0
Size: 342324 Color: 1
Size: 161690 Color: 1

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 502599 Color: 0
Size: 313155 Color: 1
Size: 184246 Color: 0

Bin 417: 1 of cap free
Amount of items: 2
Items: 
Size: 509676 Color: 0
Size: 490324 Color: 1

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 510708 Color: 0
Size: 346124 Color: 1
Size: 143168 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 513098 Color: 0
Size: 315516 Color: 1
Size: 171386 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 513157 Color: 0
Size: 374788 Color: 1
Size: 112055 Color: 0

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 521383 Color: 0
Size: 360706 Color: 0
Size: 117911 Color: 1

Bin 422: 1 of cap free
Amount of items: 2
Items: 
Size: 532596 Color: 0
Size: 467404 Color: 1

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 534040 Color: 0
Size: 310841 Color: 0
Size: 155119 Color: 1

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 534254 Color: 0
Size: 248327 Color: 1
Size: 217419 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 537046 Color: 0
Size: 283833 Color: 1
Size: 179121 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 538018 Color: 1
Size: 313224 Color: 0
Size: 148758 Color: 1

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 537808 Color: 0
Size: 314506 Color: 1
Size: 147686 Color: 0

Bin 428: 1 of cap free
Amount of items: 2
Items: 
Size: 543002 Color: 0
Size: 456998 Color: 1

Bin 429: 1 of cap free
Amount of items: 2
Items: 
Size: 544759 Color: 1
Size: 455241 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 544724 Color: 0
Size: 312360 Color: 1
Size: 142916 Color: 0

Bin 431: 1 of cap free
Amount of items: 2
Items: 
Size: 546865 Color: 0
Size: 453135 Color: 1

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 552368 Color: 0
Size: 336731 Color: 0
Size: 110901 Color: 1

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 554597 Color: 0
Size: 304530 Color: 1
Size: 140873 Color: 0

Bin 434: 1 of cap free
Amount of items: 2
Items: 
Size: 556145 Color: 1
Size: 443855 Color: 0

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 557664 Color: 0
Size: 250707 Color: 1
Size: 191629 Color: 0

Bin 436: 1 of cap free
Amount of items: 2
Items: 
Size: 559036 Color: 1
Size: 440964 Color: 0

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 566731 Color: 1
Size: 301459 Color: 0
Size: 131810 Color: 1

Bin 438: 1 of cap free
Amount of items: 2
Items: 
Size: 567144 Color: 1
Size: 432856 Color: 0

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 567330 Color: 0
Size: 247601 Color: 1
Size: 185069 Color: 0

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 567358 Color: 1
Size: 267876 Color: 0
Size: 164766 Color: 1

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 567417 Color: 1
Size: 273878 Color: 0
Size: 158705 Color: 1

Bin 442: 1 of cap free
Amount of items: 2
Items: 
Size: 567665 Color: 0
Size: 432335 Color: 1

Bin 443: 1 of cap free
Amount of items: 2
Items: 
Size: 568770 Color: 1
Size: 431230 Color: 0

Bin 444: 1 of cap free
Amount of items: 2
Items: 
Size: 568846 Color: 1
Size: 431154 Color: 0

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 572804 Color: 0
Size: 282742 Color: 1
Size: 144454 Color: 0

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 586685 Color: 1
Size: 302111 Color: 0
Size: 111204 Color: 0

Bin 447: 1 of cap free
Amount of items: 2
Items: 
Size: 590427 Color: 0
Size: 409573 Color: 1

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 591217 Color: 0
Size: 266360 Color: 1
Size: 142423 Color: 0

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 591236 Color: 0
Size: 283521 Color: 1
Size: 125243 Color: 0

Bin 450: 1 of cap free
Amount of items: 2
Items: 
Size: 594463 Color: 1
Size: 405537 Color: 0

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 601085 Color: 1
Size: 245836 Color: 0
Size: 153079 Color: 1

Bin 452: 1 of cap free
Amount of items: 2
Items: 
Size: 601984 Color: 0
Size: 398016 Color: 1

Bin 453: 1 of cap free
Amount of items: 2
Items: 
Size: 607983 Color: 1
Size: 392017 Color: 0

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 609639 Color: 0
Size: 197486 Color: 1
Size: 192875 Color: 0

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 616892 Color: 0
Size: 192666 Color: 1
Size: 190442 Color: 0

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 617078 Color: 1
Size: 194596 Color: 0
Size: 188326 Color: 1

Bin 457: 1 of cap free
Amount of items: 2
Items: 
Size: 618495 Color: 1
Size: 381505 Color: 0

Bin 458: 1 of cap free
Amount of items: 2
Items: 
Size: 619448 Color: 1
Size: 380552 Color: 0

Bin 459: 1 of cap free
Amount of items: 2
Items: 
Size: 619967 Color: 0
Size: 380033 Color: 1

Bin 460: 1 of cap free
Amount of items: 2
Items: 
Size: 625420 Color: 0
Size: 374580 Color: 1

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 633515 Color: 0
Size: 193649 Color: 1
Size: 172836 Color: 0

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 634466 Color: 1
Size: 187094 Color: 0
Size: 178440 Color: 0

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 637094 Color: 0
Size: 233851 Color: 1
Size: 129055 Color: 1

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 637158 Color: 0
Size: 195239 Color: 1
Size: 167603 Color: 0

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 643603 Color: 1
Size: 220999 Color: 0
Size: 135398 Color: 0

Bin 466: 1 of cap free
Amount of items: 2
Items: 
Size: 646374 Color: 1
Size: 353626 Color: 0

Bin 467: 1 of cap free
Amount of items: 2
Items: 
Size: 649225 Color: 0
Size: 350775 Color: 1

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 663444 Color: 1
Size: 219687 Color: 0
Size: 116869 Color: 0

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 663457 Color: 1
Size: 215015 Color: 1
Size: 121528 Color: 0

Bin 470: 1 of cap free
Amount of items: 2
Items: 
Size: 663757 Color: 1
Size: 336243 Color: 0

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 663775 Color: 0
Size: 178837 Color: 1
Size: 157388 Color: 0

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 665975 Color: 1
Size: 183989 Color: 1
Size: 150036 Color: 0

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 666087 Color: 1
Size: 181088 Color: 0
Size: 152825 Color: 0

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 670050 Color: 1
Size: 194291 Color: 0
Size: 135659 Color: 1

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 672057 Color: 0
Size: 175284 Color: 1
Size: 152659 Color: 0

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 678373 Color: 0
Size: 179084 Color: 0
Size: 142543 Color: 1

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 681745 Color: 1
Size: 191234 Color: 0
Size: 127021 Color: 0

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 688521 Color: 0
Size: 181675 Color: 0
Size: 129804 Color: 1

Bin 479: 1 of cap free
Amount of items: 3
Items: 
Size: 689083 Color: 0
Size: 156900 Color: 1
Size: 154017 Color: 1

Bin 480: 1 of cap free
Amount of items: 3
Items: 
Size: 696299 Color: 1
Size: 178826 Color: 0
Size: 124875 Color: 1

Bin 481: 1 of cap free
Amount of items: 2
Items: 
Size: 696958 Color: 0
Size: 303042 Color: 1

Bin 482: 1 of cap free
Amount of items: 2
Items: 
Size: 697726 Color: 0
Size: 302274 Color: 1

Bin 483: 1 of cap free
Amount of items: 3
Items: 
Size: 699275 Color: 1
Size: 150471 Color: 1
Size: 150254 Color: 0

Bin 484: 1 of cap free
Amount of items: 3
Items: 
Size: 700875 Color: 0
Size: 173030 Color: 1
Size: 126095 Color: 1

Bin 485: 1 of cap free
Amount of items: 2
Items: 
Size: 701135 Color: 0
Size: 298865 Color: 1

Bin 486: 1 of cap free
Amount of items: 3
Items: 
Size: 701915 Color: 0
Size: 155277 Color: 0
Size: 142808 Color: 1

Bin 487: 1 of cap free
Amount of items: 3
Items: 
Size: 706186 Color: 0
Size: 161930 Color: 0
Size: 131884 Color: 1

Bin 488: 1 of cap free
Amount of items: 3
Items: 
Size: 706573 Color: 0
Size: 191905 Color: 1
Size: 101522 Color: 1

Bin 489: 1 of cap free
Amount of items: 3
Items: 
Size: 707744 Color: 0
Size: 168239 Color: 1
Size: 124017 Color: 0

Bin 490: 1 of cap free
Amount of items: 3
Items: 
Size: 716628 Color: 1
Size: 155332 Color: 0
Size: 128040 Color: 1

Bin 491: 1 of cap free
Amount of items: 3
Items: 
Size: 720394 Color: 0
Size: 156154 Color: 1
Size: 123452 Color: 1

Bin 492: 1 of cap free
Amount of items: 3
Items: 
Size: 720735 Color: 0
Size: 167964 Color: 1
Size: 111301 Color: 0

Bin 493: 1 of cap free
Amount of items: 3
Items: 
Size: 720760 Color: 1
Size: 150981 Color: 1
Size: 128259 Color: 0

Bin 494: 1 of cap free
Amount of items: 3
Items: 
Size: 721236 Color: 1
Size: 145208 Color: 0
Size: 133556 Color: 0

Bin 495: 1 of cap free
Amount of items: 3
Items: 
Size: 723567 Color: 0
Size: 160894 Color: 0
Size: 115539 Color: 1

Bin 496: 1 of cap free
Amount of items: 3
Items: 
Size: 726177 Color: 0
Size: 142506 Color: 0
Size: 131317 Color: 1

Bin 497: 1 of cap free
Amount of items: 3
Items: 
Size: 726188 Color: 1
Size: 145400 Color: 1
Size: 128412 Color: 0

Bin 498: 1 of cap free
Amount of items: 3
Items: 
Size: 726705 Color: 0
Size: 147266 Color: 1
Size: 126029 Color: 0

Bin 499: 1 of cap free
Amount of items: 3
Items: 
Size: 729980 Color: 0
Size: 141894 Color: 0
Size: 128126 Color: 1

Bin 500: 1 of cap free
Amount of items: 2
Items: 
Size: 733100 Color: 1
Size: 266900 Color: 0

Bin 501: 1 of cap free
Amount of items: 3
Items: 
Size: 738392 Color: 1
Size: 143448 Color: 0
Size: 118160 Color: 1

Bin 502: 1 of cap free
Amount of items: 3
Items: 
Size: 738608 Color: 1
Size: 151403 Color: 0
Size: 109989 Color: 1

Bin 503: 1 of cap free
Amount of items: 3
Items: 
Size: 740192 Color: 0
Size: 131586 Color: 1
Size: 128222 Color: 1

Bin 504: 1 of cap free
Amount of items: 3
Items: 
Size: 740209 Color: 0
Size: 155387 Color: 0
Size: 104404 Color: 1

Bin 505: 1 of cap free
Amount of items: 3
Items: 
Size: 743086 Color: 0
Size: 136613 Color: 1
Size: 120301 Color: 0

Bin 506: 1 of cap free
Amount of items: 3
Items: 
Size: 755892 Color: 1
Size: 127636 Color: 0
Size: 116472 Color: 0

Bin 507: 1 of cap free
Amount of items: 3
Items: 
Size: 759374 Color: 0
Size: 124462 Color: 1
Size: 116164 Color: 0

Bin 508: 1 of cap free
Amount of items: 3
Items: 
Size: 761721 Color: 1
Size: 119734 Color: 0
Size: 118545 Color: 1

Bin 509: 1 of cap free
Amount of items: 3
Items: 
Size: 762832 Color: 1
Size: 120723 Color: 0
Size: 116445 Color: 1

Bin 510: 1 of cap free
Amount of items: 2
Items: 
Size: 777307 Color: 1
Size: 222693 Color: 0

Bin 511: 1 of cap free
Amount of items: 3
Items: 
Size: 777668 Color: 1
Size: 111403 Color: 1
Size: 110929 Color: 0

Bin 512: 1 of cap free
Amount of items: 3
Items: 
Size: 779879 Color: 0
Size: 113150 Color: 0
Size: 106971 Color: 1

Bin 513: 1 of cap free
Amount of items: 3
Items: 
Size: 780520 Color: 0
Size: 114811 Color: 1
Size: 104669 Color: 0

Bin 514: 1 of cap free
Amount of items: 2
Items: 
Size: 781538 Color: 1
Size: 218462 Color: 0

Bin 515: 1 of cap free
Amount of items: 3
Items: 
Size: 782990 Color: 1
Size: 112860 Color: 0
Size: 104150 Color: 1

Bin 516: 1 of cap free
Amount of items: 3
Items: 
Size: 783530 Color: 1
Size: 109156 Color: 0
Size: 107314 Color: 1

Bin 517: 1 of cap free
Amount of items: 2
Items: 
Size: 788097 Color: 0
Size: 211903 Color: 1

Bin 518: 1 of cap free
Amount of items: 3
Items: 
Size: 789129 Color: 0
Size: 108924 Color: 0
Size: 101947 Color: 1

Bin 519: 1 of cap free
Amount of items: 2
Items: 
Size: 796314 Color: 1
Size: 203686 Color: 0

Bin 520: 1 of cap free
Amount of items: 3
Items: 
Size: 798623 Color: 1
Size: 101248 Color: 1
Size: 100129 Color: 0

Bin 521: 1 of cap free
Amount of items: 2
Items: 
Size: 799784 Color: 1
Size: 200216 Color: 0

Bin 522: 1 of cap free
Amount of items: 5
Items: 
Size: 231126 Color: 0
Size: 226392 Color: 0
Size: 201576 Color: 1
Size: 195039 Color: 1
Size: 145867 Color: 0

Bin 523: 1 of cap free
Amount of items: 3
Items: 
Size: 490228 Color: 0
Size: 389308 Color: 0
Size: 120464 Color: 1

Bin 524: 1 of cap free
Amount of items: 3
Items: 
Size: 763198 Color: 0
Size: 127909 Color: 1
Size: 108893 Color: 0

Bin 525: 1 of cap free
Amount of items: 3
Items: 
Size: 435504 Color: 1
Size: 375114 Color: 0
Size: 189382 Color: 1

Bin 526: 1 of cap free
Amount of items: 3
Items: 
Size: 795776 Color: 0
Size: 103619 Color: 1
Size: 100605 Color: 1

Bin 527: 1 of cap free
Amount of items: 3
Items: 
Size: 475780 Color: 0
Size: 372341 Color: 1
Size: 151879 Color: 0

Bin 528: 1 of cap free
Amount of items: 3
Items: 
Size: 780436 Color: 0
Size: 119484 Color: 0
Size: 100080 Color: 1

Bin 529: 1 of cap free
Amount of items: 5
Items: 
Size: 233099 Color: 0
Size: 220167 Color: 0
Size: 204383 Color: 1
Size: 192349 Color: 1
Size: 150002 Color: 1

Bin 530: 1 of cap free
Amount of items: 3
Items: 
Size: 777740 Color: 0
Size: 121407 Color: 1
Size: 100853 Color: 0

Bin 531: 1 of cap free
Amount of items: 3
Items: 
Size: 760196 Color: 0
Size: 126095 Color: 0
Size: 113709 Color: 1

Bin 532: 1 of cap free
Amount of items: 3
Items: 
Size: 762654 Color: 1
Size: 125464 Color: 0
Size: 111882 Color: 1

Bin 533: 1 of cap free
Amount of items: 3
Items: 
Size: 759505 Color: 0
Size: 137093 Color: 1
Size: 103402 Color: 0

Bin 534: 1 of cap free
Amount of items: 3
Items: 
Size: 772612 Color: 0
Size: 121993 Color: 1
Size: 105395 Color: 1

Bin 535: 1 of cap free
Amount of items: 3
Items: 
Size: 797940 Color: 1
Size: 101308 Color: 1
Size: 100752 Color: 0

Bin 536: 1 of cap free
Amount of items: 3
Items: 
Size: 762781 Color: 0
Size: 136060 Color: 0
Size: 101159 Color: 1

Bin 537: 1 of cap free
Amount of items: 6
Items: 
Size: 171666 Color: 0
Size: 171501 Color: 0
Size: 171402 Color: 0
Size: 167365 Color: 1
Size: 167299 Color: 1
Size: 150767 Color: 1

Bin 538: 1 of cap free
Amount of items: 7
Items: 
Size: 157377 Color: 1
Size: 156098 Color: 1
Size: 148753 Color: 0
Size: 148122 Color: 0
Size: 135248 Color: 1
Size: 128446 Color: 0
Size: 125956 Color: 1

Bin 539: 1 of cap free
Amount of items: 3
Items: 
Size: 722163 Color: 0
Size: 164966 Color: 0
Size: 112871 Color: 1

Bin 540: 1 of cap free
Amount of items: 3
Items: 
Size: 782496 Color: 0
Size: 115151 Color: 1
Size: 102353 Color: 0

Bin 541: 1 of cap free
Amount of items: 3
Items: 
Size: 779541 Color: 0
Size: 110773 Color: 1
Size: 109686 Color: 1

Bin 542: 1 of cap free
Amount of items: 6
Items: 
Size: 274795 Color: 1
Size: 155974 Color: 1
Size: 155406 Color: 1
Size: 150077 Color: 0
Size: 147791 Color: 0
Size: 115957 Color: 0

Bin 543: 1 of cap free
Amount of items: 3
Items: 
Size: 598982 Color: 1
Size: 217060 Color: 1
Size: 183958 Color: 0

Bin 544: 1 of cap free
Amount of items: 7
Items: 
Size: 174684 Color: 0
Size: 139920 Color: 0
Size: 139764 Color: 1
Size: 138708 Color: 1
Size: 138694 Color: 1
Size: 134192 Color: 0
Size: 134038 Color: 0

Bin 545: 1 of cap free
Amount of items: 5
Items: 
Size: 245127 Color: 1
Size: 189191 Color: 0
Size: 188876 Color: 0
Size: 188478 Color: 1
Size: 188328 Color: 1

Bin 546: 1 of cap free
Amount of items: 7
Items: 
Size: 194148 Color: 0
Size: 136392 Color: 1
Size: 136196 Color: 1
Size: 135853 Color: 1
Size: 133140 Color: 0
Size: 132370 Color: 0
Size: 131901 Color: 0

Bin 547: 1 of cap free
Amount of items: 5
Items: 
Size: 247952 Color: 0
Size: 188378 Color: 0
Size: 188056 Color: 0
Size: 187816 Color: 1
Size: 187798 Color: 1

Bin 548: 1 of cap free
Amount of items: 5
Items: 
Size: 260348 Color: 0
Size: 185419 Color: 1
Size: 185401 Color: 1
Size: 184483 Color: 0
Size: 184349 Color: 1

Bin 549: 2 of cap free
Amount of items: 5
Items: 
Size: 226383 Color: 0
Size: 224569 Color: 0
Size: 200889 Color: 1
Size: 188283 Color: 1
Size: 159875 Color: 1

Bin 550: 2 of cap free
Amount of items: 3
Items: 
Size: 450098 Color: 0
Size: 371237 Color: 1
Size: 178664 Color: 1

Bin 551: 2 of cap free
Amount of items: 3
Items: 
Size: 469050 Color: 0
Size: 370985 Color: 1
Size: 159964 Color: 0

Bin 552: 2 of cap free
Amount of items: 3
Items: 
Size: 477345 Color: 0
Size: 389720 Color: 0
Size: 132934 Color: 1

Bin 553: 2 of cap free
Amount of items: 3
Items: 
Size: 479601 Color: 0
Size: 286565 Color: 1
Size: 233833 Color: 1

Bin 554: 2 of cap free
Amount of items: 3
Items: 
Size: 481238 Color: 0
Size: 339215 Color: 0
Size: 179546 Color: 1

Bin 555: 2 of cap free
Amount of items: 3
Items: 
Size: 483513 Color: 0
Size: 378489 Color: 1
Size: 137997 Color: 1

Bin 556: 2 of cap free
Amount of items: 2
Items: 
Size: 507977 Color: 1
Size: 492022 Color: 0

Bin 557: 2 of cap free
Amount of items: 3
Items: 
Size: 508206 Color: 1
Size: 309383 Color: 0
Size: 182410 Color: 1

Bin 558: 2 of cap free
Amount of items: 3
Items: 
Size: 508377 Color: 1
Size: 282698 Color: 1
Size: 208924 Color: 0

Bin 559: 2 of cap free
Amount of items: 3
Items: 
Size: 510878 Color: 0
Size: 315139 Color: 1
Size: 173982 Color: 0

Bin 560: 2 of cap free
Amount of items: 3
Items: 
Size: 510849 Color: 1
Size: 303114 Color: 0
Size: 186036 Color: 1

Bin 561: 2 of cap free
Amount of items: 3
Items: 
Size: 511103 Color: 1
Size: 304354 Color: 0
Size: 184542 Color: 1

Bin 562: 2 of cap free
Amount of items: 3
Items: 
Size: 513494 Color: 0
Size: 315508 Color: 1
Size: 170997 Color: 1

Bin 563: 2 of cap free
Amount of items: 3
Items: 
Size: 514126 Color: 1
Size: 315528 Color: 1
Size: 170345 Color: 0

Bin 564: 2 of cap free
Amount of items: 3
Items: 
Size: 514689 Color: 0
Size: 335703 Color: 1
Size: 149607 Color: 0

Bin 565: 2 of cap free
Amount of items: 2
Items: 
Size: 532250 Color: 1
Size: 467749 Color: 0

Bin 566: 2 of cap free
Amount of items: 3
Items: 
Size: 532999 Color: 0
Size: 303960 Color: 0
Size: 163040 Color: 1

Bin 567: 2 of cap free
Amount of items: 3
Items: 
Size: 532979 Color: 1
Size: 290626 Color: 0
Size: 176394 Color: 1

Bin 568: 2 of cap free
Amount of items: 3
Items: 
Size: 533143 Color: 0
Size: 310685 Color: 0
Size: 156171 Color: 1

Bin 569: 2 of cap free
Amount of items: 3
Items: 
Size: 537855 Color: 0
Size: 315746 Color: 1
Size: 146398 Color: 0

Bin 570: 2 of cap free
Amount of items: 2
Items: 
Size: 548292 Color: 0
Size: 451707 Color: 1

Bin 571: 2 of cap free
Amount of items: 3
Items: 
Size: 553894 Color: 0
Size: 279518 Color: 1
Size: 166587 Color: 1

Bin 572: 2 of cap free
Amount of items: 2
Items: 
Size: 563358 Color: 1
Size: 436641 Color: 0

Bin 573: 2 of cap free
Amount of items: 3
Items: 
Size: 567476 Color: 1
Size: 217988 Color: 0
Size: 214535 Color: 1

Bin 574: 2 of cap free
Amount of items: 3
Items: 
Size: 568341 Color: 0
Size: 244913 Color: 0
Size: 186745 Color: 1

Bin 575: 2 of cap free
Amount of items: 2
Items: 
Size: 577824 Color: 1
Size: 422175 Color: 0

Bin 576: 2 of cap free
Amount of items: 2
Items: 
Size: 582258 Color: 1
Size: 417741 Color: 0

Bin 577: 2 of cap free
Amount of items: 3
Items: 
Size: 586155 Color: 1
Size: 221675 Color: 0
Size: 192169 Color: 0

Bin 578: 2 of cap free
Amount of items: 3
Items: 
Size: 591732 Color: 1
Size: 238243 Color: 1
Size: 170024 Color: 0

Bin 579: 2 of cap free
Amount of items: 2
Items: 
Size: 594466 Color: 1
Size: 405533 Color: 0

Bin 580: 2 of cap free
Amount of items: 2
Items: 
Size: 597506 Color: 1
Size: 402493 Color: 0

Bin 581: 2 of cap free
Amount of items: 3
Items: 
Size: 601080 Color: 0
Size: 238471 Color: 0
Size: 160448 Color: 1

Bin 582: 2 of cap free
Amount of items: 3
Items: 
Size: 601386 Color: 1
Size: 219132 Color: 0
Size: 179481 Color: 0

Bin 583: 2 of cap free
Amount of items: 2
Items: 
Size: 613737 Color: 0
Size: 386262 Color: 1

Bin 584: 2 of cap free
Amount of items: 3
Items: 
Size: 617136 Color: 1
Size: 217484 Color: 0
Size: 165379 Color: 0

Bin 585: 2 of cap free
Amount of items: 2
Items: 
Size: 621496 Color: 1
Size: 378503 Color: 0

Bin 586: 2 of cap free
Amount of items: 3
Items: 
Size: 625318 Color: 1
Size: 218696 Color: 0
Size: 155985 Color: 1

Bin 587: 2 of cap free
Amount of items: 3
Items: 
Size: 633579 Color: 1
Size: 193044 Color: 1
Size: 173376 Color: 0

Bin 588: 2 of cap free
Amount of items: 2
Items: 
Size: 645437 Color: 1
Size: 354562 Color: 0

Bin 589: 2 of cap free
Amount of items: 2
Items: 
Size: 646749 Color: 1
Size: 353250 Color: 0

Bin 590: 2 of cap free
Amount of items: 2
Items: 
Size: 650699 Color: 0
Size: 349300 Color: 1

Bin 591: 2 of cap free
Amount of items: 3
Items: 
Size: 690655 Color: 0
Size: 171725 Color: 1
Size: 137619 Color: 1

Bin 592: 2 of cap free
Amount of items: 3
Items: 
Size: 695641 Color: 0
Size: 187172 Color: 1
Size: 117186 Color: 1

Bin 593: 2 of cap free
Amount of items: 3
Items: 
Size: 697681 Color: 0
Size: 164317 Color: 1
Size: 138001 Color: 1

Bin 594: 2 of cap free
Amount of items: 3
Items: 
Size: 702380 Color: 1
Size: 171899 Color: 1
Size: 125720 Color: 0

Bin 595: 2 of cap free
Amount of items: 2
Items: 
Size: 704334 Color: 1
Size: 295665 Color: 0

Bin 596: 2 of cap free
Amount of items: 3
Items: 
Size: 706075 Color: 0
Size: 159885 Color: 0
Size: 134039 Color: 1

Bin 597: 2 of cap free
Amount of items: 2
Items: 
Size: 712099 Color: 0
Size: 287900 Color: 1

Bin 598: 2 of cap free
Amount of items: 3
Items: 
Size: 717467 Color: 1
Size: 147324 Color: 0
Size: 135208 Color: 0

Bin 599: 2 of cap free
Amount of items: 3
Items: 
Size: 723426 Color: 0
Size: 140029 Color: 1
Size: 136544 Color: 0

Bin 600: 2 of cap free
Amount of items: 3
Items: 
Size: 723597 Color: 1
Size: 140376 Color: 0
Size: 136026 Color: 0

Bin 601: 2 of cap free
Amount of items: 2
Items: 
Size: 724933 Color: 1
Size: 275066 Color: 0

Bin 602: 2 of cap free
Amount of items: 3
Items: 
Size: 727356 Color: 1
Size: 147145 Color: 0
Size: 125498 Color: 1

Bin 603: 2 of cap free
Amount of items: 2
Items: 
Size: 732257 Color: 1
Size: 267742 Color: 0

Bin 604: 2 of cap free
Amount of items: 3
Items: 
Size: 741456 Color: 0
Size: 133788 Color: 0
Size: 124755 Color: 1

Bin 605: 2 of cap free
Amount of items: 2
Items: 
Size: 751912 Color: 0
Size: 248087 Color: 1

Bin 606: 2 of cap free
Amount of items: 3
Items: 
Size: 762152 Color: 1
Size: 120901 Color: 0
Size: 116946 Color: 1

Bin 607: 2 of cap free
Amount of items: 3
Items: 
Size: 762218 Color: 1
Size: 123444 Color: 0
Size: 114337 Color: 1

Bin 608: 2 of cap free
Amount of items: 2
Items: 
Size: 771986 Color: 0
Size: 228013 Color: 1

Bin 609: 2 of cap free
Amount of items: 2
Items: 
Size: 772306 Color: 1
Size: 227693 Color: 0

Bin 610: 2 of cap free
Amount of items: 3
Items: 
Size: 777761 Color: 1
Size: 111867 Color: 1
Size: 110371 Color: 0

Bin 611: 2 of cap free
Amount of items: 3
Items: 
Size: 778565 Color: 0
Size: 118522 Color: 0
Size: 102912 Color: 1

Bin 612: 2 of cap free
Amount of items: 3
Items: 
Size: 779401 Color: 0
Size: 115541 Color: 1
Size: 105057 Color: 1

Bin 613: 2 of cap free
Amount of items: 3
Items: 
Size: 787345 Color: 0
Size: 107096 Color: 1
Size: 105558 Color: 1

Bin 614: 2 of cap free
Amount of items: 2
Items: 
Size: 799766 Color: 0
Size: 200233 Color: 1

Bin 615: 2 of cap free
Amount of items: 3
Items: 
Size: 506000 Color: 0
Size: 339065 Color: 0
Size: 154934 Color: 1

Bin 616: 2 of cap free
Amount of items: 3
Items: 
Size: 554512 Color: 0
Size: 248657 Color: 0
Size: 196830 Color: 1

Bin 617: 2 of cap free
Amount of items: 3
Items: 
Size: 772799 Color: 0
Size: 124378 Color: 0
Size: 102822 Color: 1

Bin 618: 2 of cap free
Amount of items: 3
Items: 
Size: 554046 Color: 0
Size: 317783 Color: 1
Size: 128170 Color: 0

Bin 619: 2 of cap free
Amount of items: 3
Items: 
Size: 475060 Color: 0
Size: 392344 Color: 1
Size: 132595 Color: 0

Bin 620: 2 of cap free
Amount of items: 4
Items: 
Size: 285950 Color: 1
Size: 284707 Color: 0
Size: 282806 Color: 1
Size: 146536 Color: 0

Bin 621: 2 of cap free
Amount of items: 3
Items: 
Size: 753480 Color: 0
Size: 134963 Color: 1
Size: 111556 Color: 0

Bin 622: 2 of cap free
Amount of items: 3
Items: 
Size: 739389 Color: 1
Size: 150630 Color: 0
Size: 109980 Color: 1

Bin 623: 2 of cap free
Amount of items: 4
Items: 
Size: 258292 Color: 0
Size: 256478 Color: 0
Size: 247087 Color: 1
Size: 238142 Color: 1

Bin 624: 2 of cap free
Amount of items: 3
Items: 
Size: 354499 Color: 0
Size: 336154 Color: 1
Size: 309346 Color: 0

Bin 625: 2 of cap free
Amount of items: 5
Items: 
Size: 242369 Color: 0
Size: 189985 Color: 0
Size: 189376 Color: 0
Size: 189247 Color: 1
Size: 189022 Color: 1

Bin 626: 2 of cap free
Amount of items: 5
Items: 
Size: 250716 Color: 0
Size: 188020 Color: 0
Size: 187335 Color: 1
Size: 187172 Color: 1
Size: 186756 Color: 1

Bin 627: 3 of cap free
Amount of items: 7
Items: 
Size: 155383 Color: 1
Size: 154650 Color: 1
Size: 146848 Color: 1
Size: 146667 Color: 0
Size: 145015 Color: 0
Size: 132093 Color: 0
Size: 119342 Color: 1

Bin 628: 3 of cap free
Amount of items: 3
Items: 
Size: 780464 Color: 1
Size: 112379 Color: 0
Size: 107155 Color: 1

Bin 629: 3 of cap free
Amount of items: 5
Items: 
Size: 232008 Color: 0
Size: 231179 Color: 0
Size: 203697 Color: 1
Size: 195492 Color: 1
Size: 137622 Color: 0

Bin 630: 3 of cap free
Amount of items: 3
Items: 
Size: 437916 Color: 0
Size: 407741 Color: 1
Size: 154341 Color: 1

Bin 631: 3 of cap free
Amount of items: 3
Items: 
Size: 439160 Color: 0
Size: 422035 Color: 1
Size: 138803 Color: 1

Bin 632: 3 of cap free
Amount of items: 3
Items: 
Size: 444072 Color: 0
Size: 410841 Color: 1
Size: 145085 Color: 0

Bin 633: 3 of cap free
Amount of items: 3
Items: 
Size: 449330 Color: 1
Size: 369777 Color: 0
Size: 180891 Color: 0

Bin 634: 3 of cap free
Amount of items: 3
Items: 
Size: 454349 Color: 0
Size: 418434 Color: 1
Size: 127215 Color: 0

Bin 635: 3 of cap free
Amount of items: 3
Items: 
Size: 465861 Color: 1
Size: 371071 Color: 1
Size: 163066 Color: 0

Bin 636: 3 of cap free
Amount of items: 3
Items: 
Size: 468481 Color: 1
Size: 374438 Color: 1
Size: 157079 Color: 0

Bin 637: 3 of cap free
Amount of items: 3
Items: 
Size: 468874 Color: 0
Size: 377608 Color: 1
Size: 153516 Color: 0

Bin 638: 3 of cap free
Amount of items: 3
Items: 
Size: 471977 Color: 1
Size: 371395 Color: 0
Size: 156626 Color: 1

Bin 639: 3 of cap free
Amount of items: 2
Items: 
Size: 501056 Color: 1
Size: 498942 Color: 0

Bin 640: 3 of cap free
Amount of items: 3
Items: 
Size: 508981 Color: 0
Size: 375081 Color: 0
Size: 115936 Color: 1

Bin 641: 3 of cap free
Amount of items: 3
Items: 
Size: 514607 Color: 1
Size: 313209 Color: 0
Size: 172182 Color: 1

Bin 642: 3 of cap free
Amount of items: 2
Items: 
Size: 515751 Color: 0
Size: 484247 Color: 1

Bin 643: 3 of cap free
Amount of items: 2
Items: 
Size: 537681 Color: 0
Size: 462317 Color: 1

Bin 644: 3 of cap free
Amount of items: 3
Items: 
Size: 537975 Color: 0
Size: 280115 Color: 1
Size: 181908 Color: 0

Bin 645: 3 of cap free
Amount of items: 2
Items: 
Size: 546345 Color: 0
Size: 453653 Color: 1

Bin 646: 3 of cap free
Amount of items: 3
Items: 
Size: 554132 Color: 1
Size: 266230 Color: 0
Size: 179636 Color: 1

Bin 647: 3 of cap free
Amount of items: 2
Items: 
Size: 556914 Color: 1
Size: 443084 Color: 0

Bin 648: 3 of cap free
Amount of items: 2
Items: 
Size: 557076 Color: 1
Size: 442922 Color: 0

Bin 649: 3 of cap free
Amount of items: 3
Items: 
Size: 557958 Color: 1
Size: 245462 Color: 0
Size: 196578 Color: 0

Bin 650: 3 of cap free
Amount of items: 3
Items: 
Size: 562657 Color: 1
Size: 305152 Color: 0
Size: 132189 Color: 1

Bin 651: 3 of cap free
Amount of items: 3
Items: 
Size: 562679 Color: 1
Size: 267121 Color: 0
Size: 170198 Color: 0

Bin 652: 3 of cap free
Amount of items: 2
Items: 
Size: 562720 Color: 1
Size: 437278 Color: 0

Bin 653: 3 of cap free
Amount of items: 2
Items: 
Size: 566249 Color: 1
Size: 433749 Color: 0

Bin 654: 3 of cap free
Amount of items: 2
Items: 
Size: 567773 Color: 1
Size: 432225 Color: 0

Bin 655: 3 of cap free
Amount of items: 2
Items: 
Size: 568936 Color: 0
Size: 431062 Color: 1

Bin 656: 3 of cap free
Amount of items: 2
Items: 
Size: 575790 Color: 0
Size: 424208 Color: 1

Bin 657: 3 of cap free
Amount of items: 2
Items: 
Size: 578470 Color: 0
Size: 421528 Color: 1

Bin 658: 3 of cap free
Amount of items: 2
Items: 
Size: 585227 Color: 0
Size: 414771 Color: 1

Bin 659: 3 of cap free
Amount of items: 2
Items: 
Size: 585909 Color: 0
Size: 414089 Color: 1

Bin 660: 3 of cap free
Amount of items: 3
Items: 
Size: 591816 Color: 1
Size: 226313 Color: 0
Size: 181869 Color: 1

Bin 661: 3 of cap free
Amount of items: 2
Items: 
Size: 598627 Color: 0
Size: 401371 Color: 1

Bin 662: 3 of cap free
Amount of items: 3
Items: 
Size: 601028 Color: 1
Size: 245849 Color: 0
Size: 153121 Color: 0

Bin 663: 3 of cap free
Amount of items: 3
Items: 
Size: 612212 Color: 0
Size: 195731 Color: 1
Size: 192055 Color: 1

Bin 664: 3 of cap free
Amount of items: 3
Items: 
Size: 621446 Color: 1
Size: 217647 Color: 0
Size: 160905 Color: 0

Bin 665: 3 of cap free
Amount of items: 3
Items: 
Size: 636780 Color: 0
Size: 188624 Color: 0
Size: 174594 Color: 1

Bin 666: 3 of cap free
Amount of items: 2
Items: 
Size: 637079 Color: 1
Size: 362919 Color: 0

Bin 667: 3 of cap free
Amount of items: 2
Items: 
Size: 639503 Color: 0
Size: 360495 Color: 1

Bin 668: 3 of cap free
Amount of items: 2
Items: 
Size: 654984 Color: 1
Size: 345014 Color: 0

Bin 669: 3 of cap free
Amount of items: 2
Items: 
Size: 656490 Color: 0
Size: 343508 Color: 1

Bin 670: 3 of cap free
Amount of items: 2
Items: 
Size: 656548 Color: 1
Size: 343450 Color: 0

Bin 671: 3 of cap free
Amount of items: 2
Items: 
Size: 656740 Color: 1
Size: 343258 Color: 0

Bin 672: 3 of cap free
Amount of items: 3
Items: 
Size: 657082 Color: 1
Size: 176442 Color: 0
Size: 166474 Color: 0

Bin 673: 3 of cap free
Amount of items: 3
Items: 
Size: 664165 Color: 1
Size: 188650 Color: 0
Size: 147183 Color: 0

Bin 674: 3 of cap free
Amount of items: 3
Items: 
Size: 664360 Color: 0
Size: 196865 Color: 1
Size: 138773 Color: 1

Bin 675: 3 of cap free
Amount of items: 2
Items: 
Size: 665650 Color: 1
Size: 334348 Color: 0

Bin 676: 3 of cap free
Amount of items: 2
Items: 
Size: 668164 Color: 0
Size: 331834 Color: 1

Bin 677: 3 of cap free
Amount of items: 2
Items: 
Size: 670637 Color: 0
Size: 329361 Color: 1

Bin 678: 3 of cap free
Amount of items: 2
Items: 
Size: 676593 Color: 1
Size: 323405 Color: 0

Bin 679: 3 of cap free
Amount of items: 2
Items: 
Size: 713338 Color: 1
Size: 286660 Color: 0

Bin 680: 3 of cap free
Amount of items: 2
Items: 
Size: 724647 Color: 0
Size: 275351 Color: 1

Bin 681: 3 of cap free
Amount of items: 2
Items: 
Size: 727225 Color: 1
Size: 272773 Color: 0

Bin 682: 3 of cap free
Amount of items: 3
Items: 
Size: 727444 Color: 1
Size: 154110 Color: 0
Size: 118444 Color: 0

Bin 683: 3 of cap free
Amount of items: 2
Items: 
Size: 727902 Color: 1
Size: 272096 Color: 0

Bin 684: 3 of cap free
Amount of items: 2
Items: 
Size: 729250 Color: 0
Size: 270748 Color: 1

Bin 685: 3 of cap free
Amount of items: 2
Items: 
Size: 735341 Color: 1
Size: 264657 Color: 0

Bin 686: 3 of cap free
Amount of items: 3
Items: 
Size: 739181 Color: 0
Size: 147566 Color: 1
Size: 113251 Color: 0

Bin 687: 3 of cap free
Amount of items: 2
Items: 
Size: 755908 Color: 1
Size: 244090 Color: 0

Bin 688: 3 of cap free
Amount of items: 3
Items: 
Size: 762045 Color: 0
Size: 137053 Color: 0
Size: 100900 Color: 1

Bin 689: 3 of cap free
Amount of items: 3
Items: 
Size: 762612 Color: 1
Size: 124216 Color: 1
Size: 113170 Color: 0

Bin 690: 3 of cap free
Amount of items: 3
Items: 
Size: 769858 Color: 1
Size: 121487 Color: 1
Size: 108653 Color: 0

Bin 691: 3 of cap free
Amount of items: 2
Items: 
Size: 769992 Color: 0
Size: 230006 Color: 1

Bin 692: 3 of cap free
Amount of items: 2
Items: 
Size: 773433 Color: 0
Size: 226565 Color: 1

Bin 693: 3 of cap free
Amount of items: 3
Items: 
Size: 778941 Color: 0
Size: 113088 Color: 1
Size: 107969 Color: 1

Bin 694: 3 of cap free
Amount of items: 2
Items: 
Size: 786788 Color: 1
Size: 213210 Color: 0

Bin 695: 3 of cap free
Amount of items: 2
Items: 
Size: 787015 Color: 1
Size: 212983 Color: 0

Bin 696: 3 of cap free
Amount of items: 2
Items: 
Size: 790476 Color: 0
Size: 209522 Color: 1

Bin 697: 3 of cap free
Amount of items: 2
Items: 
Size: 793211 Color: 1
Size: 206787 Color: 0

Bin 698: 3 of cap free
Amount of items: 2
Items: 
Size: 796233 Color: 0
Size: 203765 Color: 1

Bin 699: 3 of cap free
Amount of items: 3
Items: 
Size: 394462 Color: 0
Size: 357265 Color: 0
Size: 248271 Color: 1

Bin 700: 3 of cap free
Amount of items: 3
Items: 
Size: 490141 Color: 0
Size: 371144 Color: 1
Size: 138713 Color: 1

Bin 701: 3 of cap free
Amount of items: 3
Items: 
Size: 510923 Color: 0
Size: 317304 Color: 0
Size: 171771 Color: 1

Bin 702: 3 of cap free
Amount of items: 3
Items: 
Size: 780395 Color: 1
Size: 110562 Color: 0
Size: 109041 Color: 1

Bin 703: 3 of cap free
Amount of items: 5
Items: 
Size: 234500 Color: 1
Size: 208971 Color: 0
Size: 197318 Color: 0
Size: 197180 Color: 0
Size: 162029 Color: 1

Bin 704: 3 of cap free
Amount of items: 3
Items: 
Size: 768366 Color: 0
Size: 130499 Color: 0
Size: 101133 Color: 1

Bin 705: 3 of cap free
Amount of items: 5
Items: 
Size: 212237 Color: 1
Size: 197705 Color: 1
Size: 197039 Color: 0
Size: 196525 Color: 0
Size: 196492 Color: 1

Bin 706: 3 of cap free
Amount of items: 5
Items: 
Size: 216295 Color: 0
Size: 196348 Color: 0
Size: 196083 Color: 0
Size: 195804 Color: 1
Size: 195468 Color: 1

Bin 707: 4 of cap free
Amount of items: 3
Items: 
Size: 474700 Color: 1
Size: 366628 Color: 0
Size: 158669 Color: 0

Bin 708: 4 of cap free
Amount of items: 3
Items: 
Size: 777175 Color: 1
Size: 118867 Color: 1
Size: 103955 Color: 0

Bin 709: 4 of cap free
Amount of items: 5
Items: 
Size: 246237 Color: 0
Size: 245720 Color: 0
Size: 231746 Color: 1
Size: 140340 Color: 1
Size: 135954 Color: 0

Bin 710: 4 of cap free
Amount of items: 3
Items: 
Size: 454486 Color: 0
Size: 363401 Color: 1
Size: 182110 Color: 0

Bin 711: 4 of cap free
Amount of items: 3
Items: 
Size: 469371 Color: 1
Size: 408249 Color: 1
Size: 122377 Color: 0

Bin 712: 4 of cap free
Amount of items: 3
Items: 
Size: 471277 Color: 0
Size: 372242 Color: 1
Size: 156478 Color: 1

Bin 713: 4 of cap free
Amount of items: 3
Items: 
Size: 480056 Color: 0
Size: 392715 Color: 0
Size: 127226 Color: 1

Bin 714: 4 of cap free
Amount of items: 3
Items: 
Size: 483003 Color: 0
Size: 377634 Color: 1
Size: 139360 Color: 1

Bin 715: 4 of cap free
Amount of items: 3
Items: 
Size: 492146 Color: 0
Size: 283565 Color: 1
Size: 224286 Color: 0

Bin 716: 4 of cap free
Amount of items: 3
Items: 
Size: 492455 Color: 0
Size: 371516 Color: 0
Size: 136026 Color: 1

Bin 717: 4 of cap free
Amount of items: 3
Items: 
Size: 492158 Color: 1
Size: 335861 Color: 1
Size: 171978 Color: 0

Bin 718: 4 of cap free
Amount of items: 3
Items: 
Size: 513522 Color: 0
Size: 315757 Color: 0
Size: 170718 Color: 1

Bin 719: 4 of cap free
Amount of items: 2
Items: 
Size: 525996 Color: 1
Size: 474001 Color: 0

Bin 720: 4 of cap free
Amount of items: 3
Items: 
Size: 535667 Color: 1
Size: 296610 Color: 0
Size: 167720 Color: 1

Bin 721: 4 of cap free
Amount of items: 3
Items: 
Size: 538125 Color: 1
Size: 273013 Color: 1
Size: 188859 Color: 0

Bin 722: 4 of cap free
Amount of items: 2
Items: 
Size: 538251 Color: 0
Size: 461746 Color: 1

Bin 723: 4 of cap free
Amount of items: 3
Items: 
Size: 557687 Color: 1
Size: 244182 Color: 0
Size: 198128 Color: 1

Bin 724: 4 of cap free
Amount of items: 2
Items: 
Size: 566934 Color: 1
Size: 433063 Color: 0

Bin 725: 4 of cap free
Amount of items: 2
Items: 
Size: 575935 Color: 1
Size: 424062 Color: 0

Bin 726: 4 of cap free
Amount of items: 2
Items: 
Size: 581654 Color: 1
Size: 418343 Color: 0

Bin 727: 4 of cap free
Amount of items: 3
Items: 
Size: 585155 Color: 1
Size: 225436 Color: 0
Size: 189406 Color: 0

Bin 728: 4 of cap free
Amount of items: 3
Items: 
Size: 591385 Color: 1
Size: 244619 Color: 0
Size: 163993 Color: 1

Bin 729: 4 of cap free
Amount of items: 3
Items: 
Size: 591820 Color: 0
Size: 265725 Color: 1
Size: 142452 Color: 0

Bin 730: 4 of cap free
Amount of items: 2
Items: 
Size: 594365 Color: 1
Size: 405632 Color: 0

Bin 731: 4 of cap free
Amount of items: 3
Items: 
Size: 597056 Color: 1
Size: 245076 Color: 0
Size: 157865 Color: 0

Bin 732: 4 of cap free
Amount of items: 2
Items: 
Size: 603152 Color: 1
Size: 396845 Color: 0

Bin 733: 4 of cap free
Amount of items: 2
Items: 
Size: 654261 Color: 0
Size: 345736 Color: 1

Bin 734: 4 of cap free
Amount of items: 2
Items: 
Size: 659339 Color: 0
Size: 340658 Color: 1

Bin 735: 4 of cap free
Amount of items: 2
Items: 
Size: 668846 Color: 1
Size: 331151 Color: 0

Bin 736: 4 of cap free
Amount of items: 2
Items: 
Size: 676327 Color: 1
Size: 323670 Color: 0

Bin 737: 4 of cap free
Amount of items: 2
Items: 
Size: 690070 Color: 1
Size: 309927 Color: 0

Bin 738: 4 of cap free
Amount of items: 2
Items: 
Size: 703906 Color: 0
Size: 296091 Color: 1

Bin 739: 4 of cap free
Amount of items: 2
Items: 
Size: 708891 Color: 1
Size: 291106 Color: 0

Bin 740: 4 of cap free
Amount of items: 2
Items: 
Size: 711744 Color: 0
Size: 288253 Color: 1

Bin 741: 4 of cap free
Amount of items: 3
Items: 
Size: 716771 Color: 1
Size: 160690 Color: 0
Size: 122536 Color: 0

Bin 742: 4 of cap free
Amount of items: 2
Items: 
Size: 719330 Color: 0
Size: 280667 Color: 1

Bin 743: 4 of cap free
Amount of items: 3
Items: 
Size: 759378 Color: 0
Size: 120598 Color: 1
Size: 120021 Color: 1

Bin 744: 4 of cap free
Amount of items: 2
Items: 
Size: 769547 Color: 1
Size: 230450 Color: 0

Bin 745: 4 of cap free
Amount of items: 3
Items: 
Size: 775149 Color: 1
Size: 117407 Color: 0
Size: 107441 Color: 1

Bin 746: 4 of cap free
Amount of items: 3
Items: 
Size: 776430 Color: 0
Size: 112081 Color: 0
Size: 111486 Color: 1

Bin 747: 4 of cap free
Amount of items: 2
Items: 
Size: 777482 Color: 1
Size: 222515 Color: 0

Bin 748: 4 of cap free
Amount of items: 3
Items: 
Size: 779564 Color: 0
Size: 115422 Color: 1
Size: 105011 Color: 1

Bin 749: 4 of cap free
Amount of items: 2
Items: 
Size: 788258 Color: 0
Size: 211739 Color: 1

Bin 750: 4 of cap free
Amount of items: 3
Items: 
Size: 791454 Color: 1
Size: 104596 Color: 1
Size: 103947 Color: 0

Bin 751: 4 of cap free
Amount of items: 2
Items: 
Size: 796509 Color: 0
Size: 203488 Color: 1

Bin 752: 4 of cap free
Amount of items: 3
Items: 
Size: 762577 Color: 0
Size: 122954 Color: 1
Size: 114466 Color: 1

Bin 753: 4 of cap free
Amount of items: 2
Items: 
Size: 785871 Color: 0
Size: 214126 Color: 1

Bin 754: 4 of cap free
Amount of items: 6
Items: 
Size: 168059 Color: 0
Size: 167744 Color: 0
Size: 167491 Color: 0
Size: 166007 Color: 1
Size: 165870 Color: 1
Size: 164826 Color: 1

Bin 755: 4 of cap free
Amount of items: 3
Items: 
Size: 763188 Color: 0
Size: 133734 Color: 1
Size: 103075 Color: 1

Bin 756: 4 of cap free
Amount of items: 7
Items: 
Size: 159074 Color: 1
Size: 158514 Color: 1
Size: 150663 Color: 1
Size: 150345 Color: 0
Size: 150092 Color: 0
Size: 116862 Color: 0
Size: 114447 Color: 1

Bin 757: 4 of cap free
Amount of items: 3
Items: 
Size: 394486 Color: 0
Size: 370362 Color: 1
Size: 235149 Color: 1

Bin 758: 4 of cap free
Amount of items: 3
Items: 
Size: 343771 Color: 1
Size: 343537 Color: 1
Size: 312689 Color: 0

Bin 759: 4 of cap free
Amount of items: 3
Items: 
Size: 364576 Color: 1
Size: 344499 Color: 1
Size: 290922 Color: 0

Bin 760: 4 of cap free
Amount of items: 6
Items: 
Size: 250153 Color: 0
Size: 239664 Color: 1
Size: 131176 Color: 1
Size: 129278 Color: 1
Size: 125369 Color: 0
Size: 124357 Color: 0

Bin 761: 5 of cap free
Amount of items: 3
Items: 
Size: 372168 Color: 1
Size: 371471 Color: 0
Size: 256357 Color: 0

Bin 762: 5 of cap free
Amount of items: 3
Items: 
Size: 407764 Color: 1
Size: 407142 Color: 1
Size: 185090 Color: 0

Bin 763: 5 of cap free
Amount of items: 3
Items: 
Size: 425129 Color: 1
Size: 422283 Color: 1
Size: 152584 Color: 0

Bin 764: 5 of cap free
Amount of items: 3
Items: 
Size: 468959 Color: 1
Size: 370020 Color: 0
Size: 161017 Color: 1

Bin 765: 5 of cap free
Amount of items: 2
Items: 
Size: 513706 Color: 0
Size: 486290 Color: 1

Bin 766: 5 of cap free
Amount of items: 2
Items: 
Size: 516894 Color: 0
Size: 483102 Color: 1

Bin 767: 5 of cap free
Amount of items: 2
Items: 
Size: 519021 Color: 0
Size: 480975 Color: 1

Bin 768: 5 of cap free
Amount of items: 2
Items: 
Size: 522119 Color: 1
Size: 477877 Color: 0

Bin 769: 5 of cap free
Amount of items: 2
Items: 
Size: 526157 Color: 0
Size: 473839 Color: 1

Bin 770: 5 of cap free
Amount of items: 2
Items: 
Size: 538489 Color: 1
Size: 461507 Color: 0

Bin 771: 5 of cap free
Amount of items: 2
Items: 
Size: 543849 Color: 0
Size: 456147 Color: 1

Bin 772: 5 of cap free
Amount of items: 3
Items: 
Size: 554198 Color: 1
Size: 287283 Color: 1
Size: 158515 Color: 0

Bin 773: 5 of cap free
Amount of items: 2
Items: 
Size: 556222 Color: 1
Size: 443774 Color: 0

Bin 774: 5 of cap free
Amount of items: 3
Items: 
Size: 566802 Color: 1
Size: 315394 Color: 0
Size: 117800 Color: 0

Bin 775: 5 of cap free
Amount of items: 2
Items: 
Size: 581603 Color: 1
Size: 418393 Color: 0

Bin 776: 5 of cap free
Amount of items: 3
Items: 
Size: 592866 Color: 1
Size: 251544 Color: 1
Size: 155586 Color: 0

Bin 777: 5 of cap free
Amount of items: 2
Items: 
Size: 630852 Color: 0
Size: 369144 Color: 1

Bin 778: 5 of cap free
Amount of items: 2
Items: 
Size: 639556 Color: 0
Size: 360440 Color: 1

Bin 779: 5 of cap free
Amount of items: 2
Items: 
Size: 646998 Color: 0
Size: 352998 Color: 1

Bin 780: 5 of cap free
Amount of items: 2
Items: 
Size: 651168 Color: 0
Size: 348828 Color: 1

Bin 781: 5 of cap free
Amount of items: 2
Items: 
Size: 651663 Color: 0
Size: 348333 Color: 1

Bin 782: 5 of cap free
Amount of items: 2
Items: 
Size: 671383 Color: 1
Size: 328613 Color: 0

Bin 783: 5 of cap free
Amount of items: 2
Items: 
Size: 676107 Color: 1
Size: 323889 Color: 0

Bin 784: 5 of cap free
Amount of items: 2
Items: 
Size: 688061 Color: 1
Size: 311935 Color: 0

Bin 785: 5 of cap free
Amount of items: 2
Items: 
Size: 688403 Color: 0
Size: 311593 Color: 1

Bin 786: 5 of cap free
Amount of items: 2
Items: 
Size: 733963 Color: 1
Size: 266033 Color: 0

Bin 787: 5 of cap free
Amount of items: 2
Items: 
Size: 777387 Color: 0
Size: 222609 Color: 1

Bin 788: 5 of cap free
Amount of items: 3
Items: 
Size: 779835 Color: 1
Size: 113868 Color: 0
Size: 106293 Color: 1

Bin 789: 5 of cap free
Amount of items: 2
Items: 
Size: 787966 Color: 1
Size: 212030 Color: 0

Bin 790: 5 of cap free
Amount of items: 2
Items: 
Size: 790730 Color: 1
Size: 209266 Color: 0

Bin 791: 5 of cap free
Amount of items: 3
Items: 
Size: 493311 Color: 0
Size: 375047 Color: 1
Size: 131638 Color: 0

Bin 792: 5 of cap free
Amount of items: 2
Items: 
Size: 791168 Color: 1
Size: 208828 Color: 0

Bin 793: 5 of cap free
Amount of items: 2
Items: 
Size: 787511 Color: 1
Size: 212485 Color: 0

Bin 794: 5 of cap free
Amount of items: 3
Items: 
Size: 729398 Color: 0
Size: 137026 Color: 1
Size: 133572 Color: 0

Bin 795: 6 of cap free
Amount of items: 3
Items: 
Size: 449894 Color: 0
Size: 430938 Color: 1
Size: 119163 Color: 1

Bin 796: 6 of cap free
Amount of items: 3
Items: 
Size: 404605 Color: 1
Size: 357264 Color: 0
Size: 238126 Color: 1

Bin 797: 6 of cap free
Amount of items: 3
Items: 
Size: 759447 Color: 0
Size: 123687 Color: 1
Size: 116861 Color: 0

Bin 798: 6 of cap free
Amount of items: 3
Items: 
Size: 450040 Color: 0
Size: 369474 Color: 1
Size: 180481 Color: 0

Bin 799: 6 of cap free
Amount of items: 3
Items: 
Size: 468237 Color: 1
Size: 387859 Color: 0
Size: 143899 Color: 0

Bin 800: 6 of cap free
Amount of items: 2
Items: 
Size: 509515 Color: 1
Size: 490480 Color: 0

Bin 801: 6 of cap free
Amount of items: 2
Items: 
Size: 512546 Color: 0
Size: 487449 Color: 1

Bin 802: 6 of cap free
Amount of items: 2
Items: 
Size: 519442 Color: 1
Size: 480553 Color: 0

Bin 803: 6 of cap free
Amount of items: 2
Items: 
Size: 531360 Color: 0
Size: 468635 Color: 1

Bin 804: 6 of cap free
Amount of items: 2
Items: 
Size: 533585 Color: 0
Size: 466410 Color: 1

Bin 805: 6 of cap free
Amount of items: 3
Items: 
Size: 537763 Color: 0
Size: 309993 Color: 0
Size: 152239 Color: 1

Bin 806: 6 of cap free
Amount of items: 2
Items: 
Size: 558301 Color: 1
Size: 441694 Color: 0

Bin 807: 6 of cap free
Amount of items: 2
Items: 
Size: 560842 Color: 1
Size: 439153 Color: 0

Bin 808: 6 of cap free
Amount of items: 2
Items: 
Size: 562816 Color: 1
Size: 437179 Color: 0

Bin 809: 6 of cap free
Amount of items: 3
Items: 
Size: 562970 Color: 1
Size: 256817 Color: 0
Size: 180208 Color: 0

Bin 810: 6 of cap free
Amount of items: 2
Items: 
Size: 567020 Color: 0
Size: 432975 Color: 1

Bin 811: 6 of cap free
Amount of items: 2
Items: 
Size: 568588 Color: 1
Size: 431407 Color: 0

Bin 812: 6 of cap free
Amount of items: 2
Items: 
Size: 571561 Color: 1
Size: 428434 Color: 0

Bin 813: 6 of cap free
Amount of items: 2
Items: 
Size: 583286 Color: 1
Size: 416709 Color: 0

Bin 814: 6 of cap free
Amount of items: 2
Items: 
Size: 585004 Color: 0
Size: 414991 Color: 1

Bin 815: 6 of cap free
Amount of items: 2
Items: 
Size: 590277 Color: 0
Size: 409718 Color: 1

Bin 816: 6 of cap free
Amount of items: 2
Items: 
Size: 592300 Color: 0
Size: 407695 Color: 1

Bin 817: 6 of cap free
Amount of items: 2
Items: 
Size: 608742 Color: 0
Size: 391253 Color: 1

Bin 818: 6 of cap free
Amount of items: 2
Items: 
Size: 616155 Color: 0
Size: 383840 Color: 1

Bin 819: 6 of cap free
Amount of items: 2
Items: 
Size: 624739 Color: 1
Size: 375256 Color: 0

Bin 820: 6 of cap free
Amount of items: 2
Items: 
Size: 628542 Color: 0
Size: 371453 Color: 1

Bin 821: 6 of cap free
Amount of items: 2
Items: 
Size: 631430 Color: 1
Size: 368565 Color: 0

Bin 822: 6 of cap free
Amount of items: 2
Items: 
Size: 644957 Color: 1
Size: 355038 Color: 0

Bin 823: 6 of cap free
Amount of items: 2
Items: 
Size: 645403 Color: 1
Size: 354592 Color: 0

Bin 824: 6 of cap free
Amount of items: 2
Items: 
Size: 661878 Color: 1
Size: 338117 Color: 0

Bin 825: 6 of cap free
Amount of items: 2
Items: 
Size: 671712 Color: 1
Size: 328283 Color: 0

Bin 826: 6 of cap free
Amount of items: 2
Items: 
Size: 677890 Color: 1
Size: 322105 Color: 0

Bin 827: 6 of cap free
Amount of items: 2
Items: 
Size: 686157 Color: 1
Size: 313838 Color: 0

Bin 828: 6 of cap free
Amount of items: 2
Items: 
Size: 703386 Color: 0
Size: 296609 Color: 1

Bin 829: 6 of cap free
Amount of items: 2
Items: 
Size: 706481 Color: 1
Size: 293514 Color: 0

Bin 830: 6 of cap free
Amount of items: 2
Items: 
Size: 720231 Color: 1
Size: 279764 Color: 0

Bin 831: 6 of cap free
Amount of items: 2
Items: 
Size: 721818 Color: 1
Size: 278177 Color: 0

Bin 832: 6 of cap free
Amount of items: 2
Items: 
Size: 729584 Color: 1
Size: 270411 Color: 0

Bin 833: 6 of cap free
Amount of items: 2
Items: 
Size: 753458 Color: 0
Size: 246537 Color: 1

Bin 834: 6 of cap free
Amount of items: 2
Items: 
Size: 754735 Color: 0
Size: 245260 Color: 1

Bin 835: 6 of cap free
Amount of items: 3
Items: 
Size: 756613 Color: 0
Size: 136309 Color: 1
Size: 107073 Color: 0

Bin 836: 6 of cap free
Amount of items: 2
Items: 
Size: 761035 Color: 0
Size: 238960 Color: 1

Bin 837: 6 of cap free
Amount of items: 3
Items: 
Size: 791865 Color: 0
Size: 104454 Color: 1
Size: 103676 Color: 0

Bin 838: 6 of cap free
Amount of items: 2
Items: 
Size: 601351 Color: 1
Size: 398644 Color: 0

Bin 839: 6 of cap free
Amount of items: 4
Items: 
Size: 268013 Color: 0
Size: 265430 Color: 1
Size: 264928 Color: 0
Size: 201624 Color: 1

Bin 840: 6 of cap free
Amount of items: 7
Items: 
Size: 159967 Color: 1
Size: 143147 Color: 1
Size: 143140 Color: 1
Size: 142843 Color: 1
Size: 140809 Color: 0
Size: 136256 Color: 0
Size: 133833 Color: 0

Bin 841: 6 of cap free
Amount of items: 3
Items: 
Size: 381642 Color: 1
Size: 313025 Color: 1
Size: 305328 Color: 0

Bin 842: 6 of cap free
Amount of items: 5
Items: 
Size: 263379 Color: 0
Size: 184408 Color: 0
Size: 184267 Color: 0
Size: 184036 Color: 1
Size: 183905 Color: 1

Bin 843: 6 of cap free
Amount of items: 5
Items: 
Size: 277254 Color: 1
Size: 182463 Color: 0
Size: 182362 Color: 0
Size: 179131 Color: 1
Size: 178785 Color: 1

Bin 844: 7 of cap free
Amount of items: 3
Items: 
Size: 444349 Color: 0
Size: 391524 Color: 0
Size: 164121 Color: 1

Bin 845: 7 of cap free
Amount of items: 2
Items: 
Size: 605174 Color: 1
Size: 394820 Color: 0

Bin 846: 7 of cap free
Amount of items: 3
Items: 
Size: 418038 Color: 1
Size: 414004 Color: 0
Size: 167952 Color: 1

Bin 847: 7 of cap free
Amount of items: 2
Items: 
Size: 503588 Color: 0
Size: 496406 Color: 1

Bin 848: 7 of cap free
Amount of items: 2
Items: 
Size: 510724 Color: 1
Size: 489270 Color: 0

Bin 849: 7 of cap free
Amount of items: 2
Items: 
Size: 530906 Color: 0
Size: 469088 Color: 1

Bin 850: 7 of cap free
Amount of items: 2
Items: 
Size: 535070 Color: 0
Size: 464924 Color: 1

Bin 851: 7 of cap free
Amount of items: 2
Items: 
Size: 542155 Color: 0
Size: 457839 Color: 1

Bin 852: 7 of cap free
Amount of items: 2
Items: 
Size: 543116 Color: 0
Size: 456878 Color: 1

Bin 853: 7 of cap free
Amount of items: 3
Items: 
Size: 549832 Color: 0
Size: 264808 Color: 0
Size: 185354 Color: 1

Bin 854: 7 of cap free
Amount of items: 3
Items: 
Size: 553585 Color: 0
Size: 248715 Color: 1
Size: 197694 Color: 1

Bin 855: 7 of cap free
Amount of items: 2
Items: 
Size: 567977 Color: 0
Size: 432017 Color: 1

Bin 856: 7 of cap free
Amount of items: 2
Items: 
Size: 573683 Color: 1
Size: 426311 Color: 0

Bin 857: 7 of cap free
Amount of items: 2
Items: 
Size: 593664 Color: 0
Size: 406330 Color: 1

Bin 858: 7 of cap free
Amount of items: 2
Items: 
Size: 595420 Color: 0
Size: 404574 Color: 1

Bin 859: 7 of cap free
Amount of items: 2
Items: 
Size: 615041 Color: 1
Size: 384953 Color: 0

Bin 860: 7 of cap free
Amount of items: 2
Items: 
Size: 615589 Color: 0
Size: 384405 Color: 1

Bin 861: 7 of cap free
Amount of items: 2
Items: 
Size: 620222 Color: 1
Size: 379772 Color: 0

Bin 862: 7 of cap free
Amount of items: 2
Items: 
Size: 623280 Color: 1
Size: 376714 Color: 0

Bin 863: 7 of cap free
Amount of items: 2
Items: 
Size: 625084 Color: 1
Size: 374910 Color: 0

Bin 864: 7 of cap free
Amount of items: 2
Items: 
Size: 634847 Color: 1
Size: 365147 Color: 0

Bin 865: 7 of cap free
Amount of items: 2
Items: 
Size: 673678 Color: 0
Size: 326316 Color: 1

Bin 866: 7 of cap free
Amount of items: 2
Items: 
Size: 683129 Color: 0
Size: 316865 Color: 1

Bin 867: 7 of cap free
Amount of items: 2
Items: 
Size: 683404 Color: 0
Size: 316590 Color: 1

Bin 868: 7 of cap free
Amount of items: 2
Items: 
Size: 692434 Color: 1
Size: 307560 Color: 0

Bin 869: 7 of cap free
Amount of items: 2
Items: 
Size: 696131 Color: 1
Size: 303863 Color: 0

Bin 870: 7 of cap free
Amount of items: 2
Items: 
Size: 705919 Color: 0
Size: 294075 Color: 1

Bin 871: 7 of cap free
Amount of items: 2
Items: 
Size: 728783 Color: 1
Size: 271211 Color: 0

Bin 872: 7 of cap free
Amount of items: 2
Items: 
Size: 735255 Color: 0
Size: 264739 Color: 1

Bin 873: 7 of cap free
Amount of items: 2
Items: 
Size: 750828 Color: 0
Size: 249166 Color: 1

Bin 874: 7 of cap free
Amount of items: 2
Items: 
Size: 752343 Color: 0
Size: 247651 Color: 1

Bin 875: 7 of cap free
Amount of items: 2
Items: 
Size: 752971 Color: 0
Size: 247023 Color: 1

Bin 876: 7 of cap free
Amount of items: 2
Items: 
Size: 769899 Color: 1
Size: 230095 Color: 0

Bin 877: 7 of cap free
Amount of items: 2
Items: 
Size: 790032 Color: 1
Size: 209962 Color: 0

Bin 878: 7 of cap free
Amount of items: 2
Items: 
Size: 799062 Color: 1
Size: 200932 Color: 0

Bin 879: 7 of cap free
Amount of items: 3
Items: 
Size: 407974 Color: 1
Size: 387126 Color: 0
Size: 204894 Color: 0

Bin 880: 7 of cap free
Amount of items: 6
Items: 
Size: 169964 Color: 1
Size: 166851 Color: 0
Size: 166508 Color: 0
Size: 165578 Color: 1
Size: 165575 Color: 0
Size: 165518 Color: 1

Bin 881: 8 of cap free
Amount of items: 3
Items: 
Size: 408092 Color: 1
Size: 392539 Color: 0
Size: 199362 Color: 1

Bin 882: 8 of cap free
Amount of items: 3
Items: 
Size: 483901 Color: 0
Size: 286554 Color: 1
Size: 229538 Color: 1

Bin 883: 8 of cap free
Amount of items: 2
Items: 
Size: 504998 Color: 1
Size: 494995 Color: 0

Bin 884: 8 of cap free
Amount of items: 2
Items: 
Size: 505140 Color: 1
Size: 494853 Color: 0

Bin 885: 8 of cap free
Amount of items: 2
Items: 
Size: 513742 Color: 0
Size: 486251 Color: 1

Bin 886: 8 of cap free
Amount of items: 2
Items: 
Size: 533133 Color: 1
Size: 466860 Color: 0

Bin 887: 8 of cap free
Amount of items: 2
Items: 
Size: 535942 Color: 1
Size: 464051 Color: 0

Bin 888: 8 of cap free
Amount of items: 2
Items: 
Size: 551191 Color: 1
Size: 448802 Color: 0

Bin 889: 8 of cap free
Amount of items: 2
Items: 
Size: 553616 Color: 1
Size: 446377 Color: 0

Bin 890: 8 of cap free
Amount of items: 2
Items: 
Size: 559297 Color: 1
Size: 440696 Color: 0

Bin 891: 8 of cap free
Amount of items: 2
Items: 
Size: 564435 Color: 1
Size: 435558 Color: 0

Bin 892: 8 of cap free
Amount of items: 2
Items: 
Size: 578149 Color: 0
Size: 421844 Color: 1

Bin 893: 8 of cap free
Amount of items: 3
Items: 
Size: 589273 Color: 1
Size: 252632 Color: 0
Size: 158088 Color: 0

Bin 894: 8 of cap free
Amount of items: 2
Items: 
Size: 597480 Color: 0
Size: 402513 Color: 1

Bin 895: 8 of cap free
Amount of items: 2
Items: 
Size: 603243 Color: 0
Size: 396750 Color: 1

Bin 896: 8 of cap free
Amount of items: 2
Items: 
Size: 635903 Color: 0
Size: 364090 Color: 1

Bin 897: 8 of cap free
Amount of items: 2
Items: 
Size: 652201 Color: 1
Size: 347792 Color: 0

Bin 898: 8 of cap free
Amount of items: 2
Items: 
Size: 659050 Color: 0
Size: 340943 Color: 1

Bin 899: 8 of cap free
Amount of items: 2
Items: 
Size: 667010 Color: 1
Size: 332983 Color: 0

Bin 900: 8 of cap free
Amount of items: 2
Items: 
Size: 668264 Color: 1
Size: 331729 Color: 0

Bin 901: 8 of cap free
Amount of items: 2
Items: 
Size: 694408 Color: 0
Size: 305585 Color: 1

Bin 902: 8 of cap free
Amount of items: 2
Items: 
Size: 699722 Color: 1
Size: 300271 Color: 0

Bin 903: 8 of cap free
Amount of items: 2
Items: 
Size: 735889 Color: 1
Size: 264104 Color: 0

Bin 904: 8 of cap free
Amount of items: 2
Items: 
Size: 745060 Color: 0
Size: 254933 Color: 1

Bin 905: 8 of cap free
Amount of items: 2
Items: 
Size: 746450 Color: 1
Size: 253543 Color: 0

Bin 906: 8 of cap free
Amount of items: 3
Items: 
Size: 773337 Color: 0
Size: 119382 Color: 0
Size: 107274 Color: 1

Bin 907: 8 of cap free
Amount of items: 3
Items: 
Size: 776555 Color: 0
Size: 113066 Color: 1
Size: 110372 Color: 1

Bin 908: 9 of cap free
Amount of items: 3
Items: 
Size: 613504 Color: 1
Size: 262113 Color: 0
Size: 124375 Color: 1

Bin 909: 9 of cap free
Amount of items: 3
Items: 
Size: 769825 Color: 1
Size: 122216 Color: 1
Size: 107951 Color: 0

Bin 910: 9 of cap free
Amount of items: 5
Items: 
Size: 248328 Color: 0
Size: 246403 Color: 0
Size: 233638 Color: 1
Size: 159316 Color: 1
Size: 112307 Color: 0

Bin 911: 9 of cap free
Amount of items: 3
Items: 
Size: 405347 Color: 1
Size: 369999 Color: 0
Size: 224646 Color: 0

Bin 912: 9 of cap free
Amount of items: 3
Items: 
Size: 468015 Color: 1
Size: 356904 Color: 0
Size: 175073 Color: 0

Bin 913: 9 of cap free
Amount of items: 2
Items: 
Size: 511665 Color: 0
Size: 488327 Color: 1

Bin 914: 9 of cap free
Amount of items: 2
Items: 
Size: 518250 Color: 0
Size: 481742 Color: 1

Bin 915: 9 of cap free
Amount of items: 2
Items: 
Size: 521771 Color: 0
Size: 478221 Color: 1

Bin 916: 9 of cap free
Amount of items: 3
Items: 
Size: 554610 Color: 1
Size: 307469 Color: 0
Size: 137913 Color: 1

Bin 917: 9 of cap free
Amount of items: 2
Items: 
Size: 567916 Color: 1
Size: 432076 Color: 0

Bin 918: 9 of cap free
Amount of items: 2
Items: 
Size: 578916 Color: 0
Size: 421076 Color: 1

Bin 919: 9 of cap free
Amount of items: 3
Items: 
Size: 598400 Color: 0
Size: 217301 Color: 0
Size: 184291 Color: 1

Bin 920: 9 of cap free
Amount of items: 2
Items: 
Size: 598590 Color: 0
Size: 401402 Color: 1

Bin 921: 9 of cap free
Amount of items: 2
Items: 
Size: 623183 Color: 1
Size: 376809 Color: 0

Bin 922: 9 of cap free
Amount of items: 2
Items: 
Size: 638236 Color: 1
Size: 361756 Color: 0

Bin 923: 9 of cap free
Amount of items: 2
Items: 
Size: 663821 Color: 1
Size: 336171 Color: 0

Bin 924: 9 of cap free
Amount of items: 2
Items: 
Size: 668341 Color: 1
Size: 331651 Color: 0

Bin 925: 9 of cap free
Amount of items: 2
Items: 
Size: 674350 Color: 1
Size: 325642 Color: 0

Bin 926: 9 of cap free
Amount of items: 2
Items: 
Size: 681913 Color: 0
Size: 318079 Color: 1

Bin 927: 9 of cap free
Amount of items: 2
Items: 
Size: 693495 Color: 0
Size: 306497 Color: 1

Bin 928: 9 of cap free
Amount of items: 2
Items: 
Size: 700143 Color: 1
Size: 299849 Color: 0

Bin 929: 9 of cap free
Amount of items: 2
Items: 
Size: 709347 Color: 0
Size: 290645 Color: 1

Bin 930: 9 of cap free
Amount of items: 2
Items: 
Size: 719844 Color: 1
Size: 280148 Color: 0

Bin 931: 9 of cap free
Amount of items: 2
Items: 
Size: 764359 Color: 0
Size: 235633 Color: 1

Bin 932: 9 of cap free
Amount of items: 2
Items: 
Size: 768955 Color: 1
Size: 231037 Color: 0

Bin 933: 9 of cap free
Amount of items: 2
Items: 
Size: 769395 Color: 1
Size: 230597 Color: 0

Bin 934: 9 of cap free
Amount of items: 3
Items: 
Size: 777693 Color: 0
Size: 115446 Color: 0
Size: 106853 Color: 1

Bin 935: 9 of cap free
Amount of items: 3
Items: 
Size: 779888 Color: 1
Size: 116712 Color: 1
Size: 103392 Color: 0

Bin 936: 9 of cap free
Amount of items: 2
Items: 
Size: 785277 Color: 1
Size: 214715 Color: 0

Bin 937: 9 of cap free
Amount of items: 2
Items: 
Size: 795936 Color: 1
Size: 204056 Color: 0

Bin 938: 9 of cap free
Amount of items: 2
Items: 
Size: 799924 Color: 0
Size: 200068 Color: 1

Bin 939: 9 of cap free
Amount of items: 3
Items: 
Size: 736493 Color: 0
Size: 141085 Color: 0
Size: 122414 Color: 1

Bin 940: 9 of cap free
Amount of items: 3
Items: 
Size: 401650 Color: 1
Size: 341845 Color: 0
Size: 256497 Color: 0

Bin 941: 9 of cap free
Amount of items: 3
Items: 
Size: 402658 Color: 1
Size: 302212 Color: 0
Size: 295122 Color: 0

Bin 942: 9 of cap free
Amount of items: 4
Items: 
Size: 256804 Color: 0
Size: 256784 Color: 0
Size: 249377 Color: 1
Size: 237027 Color: 1

Bin 943: 10 of cap free
Amount of items: 3
Items: 
Size: 698701 Color: 0
Size: 154465 Color: 1
Size: 146825 Color: 1

Bin 944: 10 of cap free
Amount of items: 3
Items: 
Size: 591579 Color: 1
Size: 264880 Color: 0
Size: 143532 Color: 0

Bin 945: 10 of cap free
Amount of items: 3
Items: 
Size: 390748 Color: 0
Size: 353201 Color: 1
Size: 256042 Color: 0

Bin 946: 10 of cap free
Amount of items: 3
Items: 
Size: 479738 Color: 0
Size: 359850 Color: 0
Size: 160403 Color: 1

Bin 947: 10 of cap free
Amount of items: 3
Items: 
Size: 496178 Color: 0
Size: 311746 Color: 0
Size: 192067 Color: 1

Bin 948: 10 of cap free
Amount of items: 2
Items: 
Size: 503211 Color: 0
Size: 496780 Color: 1

Bin 949: 10 of cap free
Amount of items: 2
Items: 
Size: 503559 Color: 1
Size: 496432 Color: 0

Bin 950: 10 of cap free
Amount of items: 3
Items: 
Size: 513101 Color: 0
Size: 338291 Color: 1
Size: 148599 Color: 1

Bin 951: 10 of cap free
Amount of items: 2
Items: 
Size: 518861 Color: 0
Size: 481130 Color: 1

Bin 952: 10 of cap free
Amount of items: 2
Items: 
Size: 536678 Color: 0
Size: 463313 Color: 1

Bin 953: 10 of cap free
Amount of items: 2
Items: 
Size: 545030 Color: 0
Size: 454961 Color: 1

Bin 954: 10 of cap free
Amount of items: 2
Items: 
Size: 553697 Color: 0
Size: 446294 Color: 1

Bin 955: 10 of cap free
Amount of items: 3
Items: 
Size: 554374 Color: 0
Size: 304617 Color: 1
Size: 141000 Color: 1

Bin 956: 10 of cap free
Amount of items: 3
Items: 
Size: 580682 Color: 1
Size: 256389 Color: 0
Size: 162920 Color: 0

Bin 957: 10 of cap free
Amount of items: 2
Items: 
Size: 617020 Color: 1
Size: 382971 Color: 0

Bin 958: 10 of cap free
Amount of items: 2
Items: 
Size: 632223 Color: 1
Size: 367768 Color: 0

Bin 959: 10 of cap free
Amount of items: 2
Items: 
Size: 644785 Color: 1
Size: 355206 Color: 0

Bin 960: 10 of cap free
Amount of items: 2
Items: 
Size: 654083 Color: 1
Size: 345908 Color: 0

Bin 961: 10 of cap free
Amount of items: 2
Items: 
Size: 682333 Color: 0
Size: 317658 Color: 1

Bin 962: 10 of cap free
Amount of items: 2
Items: 
Size: 683451 Color: 0
Size: 316540 Color: 1

Bin 963: 10 of cap free
Amount of items: 2
Items: 
Size: 694292 Color: 0
Size: 305699 Color: 1

Bin 964: 10 of cap free
Amount of items: 2
Items: 
Size: 702122 Color: 1
Size: 297869 Color: 0

Bin 965: 10 of cap free
Amount of items: 2
Items: 
Size: 711104 Color: 0
Size: 288887 Color: 1

Bin 966: 10 of cap free
Amount of items: 2
Items: 
Size: 756419 Color: 0
Size: 243572 Color: 1

Bin 967: 10 of cap free
Amount of items: 2
Items: 
Size: 772675 Color: 0
Size: 227316 Color: 1

Bin 968: 10 of cap free
Amount of items: 2
Items: 
Size: 777805 Color: 1
Size: 222186 Color: 0

Bin 969: 10 of cap free
Amount of items: 2
Items: 
Size: 779218 Color: 1
Size: 220773 Color: 0

Bin 970: 10 of cap free
Amount of items: 3
Items: 
Size: 691232 Color: 1
Size: 203609 Color: 0
Size: 105150 Color: 0

Bin 971: 10 of cap free
Amount of items: 2
Items: 
Size: 762654 Color: 0
Size: 237337 Color: 1

Bin 972: 10 of cap free
Amount of items: 3
Items: 
Size: 425136 Color: 0
Size: 287430 Color: 1
Size: 287425 Color: 0

Bin 973: 10 of cap free
Amount of items: 2
Items: 
Size: 709832 Color: 1
Size: 290159 Color: 0

Bin 974: 11 of cap free
Amount of items: 3
Items: 
Size: 435213 Color: 1
Size: 388784 Color: 0
Size: 175993 Color: 0

Bin 975: 11 of cap free
Amount of items: 3
Items: 
Size: 479946 Color: 0
Size: 305098 Color: 1
Size: 214946 Color: 1

Bin 976: 11 of cap free
Amount of items: 3
Items: 
Size: 512950 Color: 0
Size: 312334 Color: 1
Size: 174706 Color: 1

Bin 977: 11 of cap free
Amount of items: 3
Items: 
Size: 517500 Color: 1
Size: 296677 Color: 0
Size: 185813 Color: 0

Bin 978: 11 of cap free
Amount of items: 2
Items: 
Size: 523787 Color: 1
Size: 476203 Color: 0

Bin 979: 11 of cap free
Amount of items: 2
Items: 
Size: 542943 Color: 1
Size: 457047 Color: 0

Bin 980: 11 of cap free
Amount of items: 2
Items: 
Size: 568900 Color: 1
Size: 431090 Color: 0

Bin 981: 11 of cap free
Amount of items: 2
Items: 
Size: 577400 Color: 0
Size: 422590 Color: 1

Bin 982: 11 of cap free
Amount of items: 2
Items: 
Size: 615539 Color: 0
Size: 384451 Color: 1

Bin 983: 11 of cap free
Amount of items: 2
Items: 
Size: 656871 Color: 0
Size: 343119 Color: 1

Bin 984: 11 of cap free
Amount of items: 2
Items: 
Size: 669409 Color: 1
Size: 330581 Color: 0

Bin 985: 11 of cap free
Amount of items: 2
Items: 
Size: 673762 Color: 1
Size: 326228 Color: 0

Bin 986: 11 of cap free
Amount of items: 2
Items: 
Size: 684330 Color: 0
Size: 315660 Color: 1

Bin 987: 11 of cap free
Amount of items: 2
Items: 
Size: 690670 Color: 1
Size: 309320 Color: 0

Bin 988: 11 of cap free
Amount of items: 2
Items: 
Size: 702367 Color: 0
Size: 297623 Color: 1

Bin 989: 11 of cap free
Amount of items: 2
Items: 
Size: 708430 Color: 0
Size: 291560 Color: 1

Bin 990: 11 of cap free
Amount of items: 2
Items: 
Size: 710056 Color: 0
Size: 289934 Color: 1

Bin 991: 11 of cap free
Amount of items: 2
Items: 
Size: 710656 Color: 1
Size: 289334 Color: 0

Bin 992: 11 of cap free
Amount of items: 2
Items: 
Size: 767409 Color: 1
Size: 232581 Color: 0

Bin 993: 11 of cap free
Amount of items: 2
Items: 
Size: 767605 Color: 1
Size: 232385 Color: 0

Bin 994: 11 of cap free
Amount of items: 2
Items: 
Size: 775176 Color: 0
Size: 224814 Color: 1

Bin 995: 11 of cap free
Amount of items: 3
Items: 
Size: 779128 Color: 0
Size: 118487 Color: 0
Size: 102375 Color: 1

Bin 996: 11 of cap free
Amount of items: 2
Items: 
Size: 788713 Color: 1
Size: 211277 Color: 0

Bin 997: 11 of cap free
Amount of items: 2
Items: 
Size: 792889 Color: 1
Size: 207101 Color: 0

Bin 998: 11 of cap free
Amount of items: 2
Items: 
Size: 796324 Color: 0
Size: 203666 Color: 1

Bin 999: 11 of cap free
Amount of items: 4
Items: 
Size: 282125 Color: 1
Size: 268956 Color: 0
Size: 268790 Color: 0
Size: 180119 Color: 1

Bin 1000: 11 of cap free
Amount of items: 3
Items: 
Size: 751968 Color: 1
Size: 144365 Color: 0
Size: 103657 Color: 0

Bin 1001: 12 of cap free
Amount of items: 3
Items: 
Size: 474910 Color: 0
Size: 377900 Color: 1
Size: 147179 Color: 0

Bin 1002: 12 of cap free
Amount of items: 3
Items: 
Size: 448614 Color: 1
Size: 407779 Color: 1
Size: 143596 Color: 0

Bin 1003: 12 of cap free
Amount of items: 3
Items: 
Size: 449591 Color: 0
Size: 378325 Color: 1
Size: 172073 Color: 1

Bin 1004: 12 of cap free
Amount of items: 2
Items: 
Size: 508959 Color: 0
Size: 491030 Color: 1

Bin 1005: 12 of cap free
Amount of items: 2
Items: 
Size: 513042 Color: 1
Size: 486947 Color: 0

Bin 1006: 12 of cap free
Amount of items: 2
Items: 
Size: 519952 Color: 0
Size: 480037 Color: 1

Bin 1007: 12 of cap free
Amount of items: 3
Items: 
Size: 530553 Color: 0
Size: 301288 Color: 0
Size: 168148 Color: 1

Bin 1008: 12 of cap free
Amount of items: 2
Items: 
Size: 533393 Color: 0
Size: 466596 Color: 1

Bin 1009: 12 of cap free
Amount of items: 2
Items: 
Size: 533783 Color: 0
Size: 466206 Color: 1

Bin 1010: 12 of cap free
Amount of items: 2
Items: 
Size: 545080 Color: 1
Size: 454909 Color: 0

Bin 1011: 12 of cap free
Amount of items: 2
Items: 
Size: 573995 Color: 0
Size: 425994 Color: 1

Bin 1012: 12 of cap free
Amount of items: 2
Items: 
Size: 581797 Color: 1
Size: 418192 Color: 0

Bin 1013: 12 of cap free
Amount of items: 2
Items: 
Size: 583642 Color: 0
Size: 416347 Color: 1

Bin 1014: 12 of cap free
Amount of items: 2
Items: 
Size: 597154 Color: 1
Size: 402835 Color: 0

Bin 1015: 12 of cap free
Amount of items: 2
Items: 
Size: 610154 Color: 0
Size: 389835 Color: 1

Bin 1016: 12 of cap free
Amount of items: 2
Items: 
Size: 616728 Color: 0
Size: 383261 Color: 1

Bin 1017: 12 of cap free
Amount of items: 2
Items: 
Size: 620021 Color: 1
Size: 379968 Color: 0

Bin 1018: 12 of cap free
Amount of items: 2
Items: 
Size: 668714 Color: 0
Size: 331275 Color: 1

Bin 1019: 12 of cap free
Amount of items: 2
Items: 
Size: 670876 Color: 0
Size: 329113 Color: 1

Bin 1020: 12 of cap free
Amount of items: 2
Items: 
Size: 677894 Color: 0
Size: 322095 Color: 1

Bin 1021: 12 of cap free
Amount of items: 2
Items: 
Size: 721696 Color: 0
Size: 278293 Color: 1

Bin 1022: 12 of cap free
Amount of items: 2
Items: 
Size: 733050 Color: 0
Size: 266939 Color: 1

Bin 1023: 12 of cap free
Amount of items: 2
Items: 
Size: 742288 Color: 1
Size: 257701 Color: 0

Bin 1024: 12 of cap free
Amount of items: 2
Items: 
Size: 744256 Color: 1
Size: 255733 Color: 0

Bin 1025: 12 of cap free
Amount of items: 2
Items: 
Size: 747567 Color: 1
Size: 252422 Color: 0

Bin 1026: 12 of cap free
Amount of items: 2
Items: 
Size: 758420 Color: 0
Size: 241569 Color: 1

Bin 1027: 12 of cap free
Amount of items: 2
Items: 
Size: 761897 Color: 1
Size: 238092 Color: 0

Bin 1028: 12 of cap free
Amount of items: 2
Items: 
Size: 763600 Color: 1
Size: 236389 Color: 0

Bin 1029: 12 of cap free
Amount of items: 2
Items: 
Size: 766757 Color: 1
Size: 233232 Color: 0

Bin 1030: 12 of cap free
Amount of items: 2
Items: 
Size: 770029 Color: 0
Size: 229960 Color: 1

Bin 1031: 12 of cap free
Amount of items: 2
Items: 
Size: 778986 Color: 0
Size: 221003 Color: 1

Bin 1032: 12 of cap free
Amount of items: 2
Items: 
Size: 781506 Color: 0
Size: 218483 Color: 1

Bin 1033: 12 of cap free
Amount of items: 6
Items: 
Size: 175216 Color: 0
Size: 174683 Color: 0
Size: 172906 Color: 0
Size: 168548 Color: 1
Size: 168058 Color: 1
Size: 140578 Color: 1

Bin 1034: 12 of cap free
Amount of items: 3
Items: 
Size: 392795 Color: 0
Size: 377165 Color: 1
Size: 230029 Color: 1

Bin 1035: 12 of cap free
Amount of items: 4
Items: 
Size: 394740 Color: 1
Size: 277682 Color: 0
Size: 189568 Color: 1
Size: 137999 Color: 0

Bin 1036: 13 of cap free
Amount of items: 3
Items: 
Size: 481892 Color: 0
Size: 379526 Color: 1
Size: 138570 Color: 1

Bin 1037: 13 of cap free
Amount of items: 3
Items: 
Size: 521331 Color: 0
Size: 339951 Color: 1
Size: 138706 Color: 1

Bin 1038: 13 of cap free
Amount of items: 2
Items: 
Size: 733364 Color: 0
Size: 266624 Color: 1

Bin 1039: 13 of cap free
Amount of items: 3
Items: 
Size: 418391 Color: 1
Size: 315767 Color: 0
Size: 265830 Color: 1

Bin 1040: 13 of cap free
Amount of items: 3
Items: 
Size: 477193 Color: 0
Size: 369582 Color: 1
Size: 153213 Color: 1

Bin 1041: 13 of cap free
Amount of items: 2
Items: 
Size: 512098 Color: 0
Size: 487890 Color: 1

Bin 1042: 13 of cap free
Amount of items: 2
Items: 
Size: 512615 Color: 1
Size: 487373 Color: 0

Bin 1043: 13 of cap free
Amount of items: 2
Items: 
Size: 514937 Color: 1
Size: 485051 Color: 0

Bin 1044: 13 of cap free
Amount of items: 2
Items: 
Size: 556628 Color: 0
Size: 443360 Color: 1

Bin 1045: 13 of cap free
Amount of items: 2
Items: 
Size: 564364 Color: 0
Size: 435624 Color: 1

Bin 1046: 13 of cap free
Amount of items: 2
Items: 
Size: 580304 Color: 0
Size: 419684 Color: 1

Bin 1047: 13 of cap free
Amount of items: 2
Items: 
Size: 600406 Color: 1
Size: 399582 Color: 0

Bin 1048: 13 of cap free
Amount of items: 2
Items: 
Size: 606278 Color: 0
Size: 393710 Color: 1

Bin 1049: 13 of cap free
Amount of items: 2
Items: 
Size: 607499 Color: 0
Size: 392489 Color: 1

Bin 1050: 13 of cap free
Amount of items: 2
Items: 
Size: 611309 Color: 0
Size: 388679 Color: 1

Bin 1051: 13 of cap free
Amount of items: 2
Items: 
Size: 623769 Color: 1
Size: 376219 Color: 0

Bin 1052: 13 of cap free
Amount of items: 2
Items: 
Size: 625385 Color: 1
Size: 374603 Color: 0

Bin 1053: 13 of cap free
Amount of items: 2
Items: 
Size: 629108 Color: 0
Size: 370880 Color: 1

Bin 1054: 13 of cap free
Amount of items: 2
Items: 
Size: 642750 Color: 0
Size: 357238 Color: 1

Bin 1055: 13 of cap free
Amount of items: 2
Items: 
Size: 650363 Color: 0
Size: 349625 Color: 1

Bin 1056: 13 of cap free
Amount of items: 2
Items: 
Size: 661804 Color: 0
Size: 338184 Color: 1

Bin 1057: 13 of cap free
Amount of items: 2
Items: 
Size: 662251 Color: 0
Size: 337737 Color: 1

Bin 1058: 13 of cap free
Amount of items: 2
Items: 
Size: 670205 Color: 0
Size: 329783 Color: 1

Bin 1059: 13 of cap free
Amount of items: 2
Items: 
Size: 676663 Color: 0
Size: 323325 Color: 1

Bin 1060: 13 of cap free
Amount of items: 2
Items: 
Size: 679623 Color: 1
Size: 320365 Color: 0

Bin 1061: 13 of cap free
Amount of items: 2
Items: 
Size: 683776 Color: 0
Size: 316212 Color: 1

Bin 1062: 13 of cap free
Amount of items: 2
Items: 
Size: 696667 Color: 0
Size: 303321 Color: 1

Bin 1063: 13 of cap free
Amount of items: 2
Items: 
Size: 709272 Color: 1
Size: 290716 Color: 0

Bin 1064: 13 of cap free
Amount of items: 2
Items: 
Size: 710726 Color: 0
Size: 289262 Color: 1

Bin 1065: 13 of cap free
Amount of items: 2
Items: 
Size: 712719 Color: 0
Size: 287269 Color: 1

Bin 1066: 13 of cap free
Amount of items: 2
Items: 
Size: 714224 Color: 0
Size: 285764 Color: 1

Bin 1067: 13 of cap free
Amount of items: 2
Items: 
Size: 733028 Color: 0
Size: 266960 Color: 1

Bin 1068: 13 of cap free
Amount of items: 2
Items: 
Size: 777442 Color: 0
Size: 222546 Color: 1

Bin 1069: 13 of cap free
Amount of items: 2
Items: 
Size: 777750 Color: 0
Size: 222238 Color: 1

Bin 1070: 13 of cap free
Amount of items: 2
Items: 
Size: 787066 Color: 1
Size: 212922 Color: 0

Bin 1071: 13 of cap free
Amount of items: 3
Items: 
Size: 509263 Color: 0
Size: 384324 Color: 1
Size: 106401 Color: 1

Bin 1072: 13 of cap free
Amount of items: 5
Items: 
Size: 288380 Color: 0
Size: 247612 Color: 0
Size: 161013 Color: 1
Size: 157661 Color: 1
Size: 145322 Color: 1

Bin 1073: 14 of cap free
Amount of items: 2
Items: 
Size: 531325 Color: 0
Size: 468662 Color: 1

Bin 1074: 14 of cap free
Amount of items: 2
Items: 
Size: 540332 Color: 0
Size: 459655 Color: 1

Bin 1075: 14 of cap free
Amount of items: 2
Items: 
Size: 542146 Color: 1
Size: 457841 Color: 0

Bin 1076: 14 of cap free
Amount of items: 2
Items: 
Size: 549870 Color: 1
Size: 450117 Color: 0

Bin 1077: 14 of cap free
Amount of items: 2
Items: 
Size: 577559 Color: 0
Size: 422428 Color: 1

Bin 1078: 14 of cap free
Amount of items: 2
Items: 
Size: 608963 Color: 1
Size: 391024 Color: 0

Bin 1079: 14 of cap free
Amount of items: 2
Items: 
Size: 617683 Color: 0
Size: 382304 Color: 1

Bin 1080: 14 of cap free
Amount of items: 2
Items: 
Size: 641311 Color: 0
Size: 358676 Color: 1

Bin 1081: 14 of cap free
Amount of items: 2
Items: 
Size: 641489 Color: 1
Size: 358498 Color: 0

Bin 1082: 14 of cap free
Amount of items: 2
Items: 
Size: 648088 Color: 1
Size: 351899 Color: 0

Bin 1083: 14 of cap free
Amount of items: 2
Items: 
Size: 655475 Color: 0
Size: 344512 Color: 1

Bin 1084: 14 of cap free
Amount of items: 2
Items: 
Size: 660124 Color: 1
Size: 339863 Color: 0

Bin 1085: 14 of cap free
Amount of items: 2
Items: 
Size: 665079 Color: 0
Size: 334908 Color: 1

Bin 1086: 14 of cap free
Amount of items: 2
Items: 
Size: 676662 Color: 1
Size: 323325 Color: 0

Bin 1087: 14 of cap free
Amount of items: 2
Items: 
Size: 696094 Color: 0
Size: 303893 Color: 1

Bin 1088: 14 of cap free
Amount of items: 2
Items: 
Size: 738163 Color: 1
Size: 261824 Color: 0

Bin 1089: 14 of cap free
Amount of items: 2
Items: 
Size: 752910 Color: 1
Size: 247077 Color: 0

Bin 1090: 14 of cap free
Amount of items: 2
Items: 
Size: 755158 Color: 0
Size: 244829 Color: 1

Bin 1091: 14 of cap free
Amount of items: 2
Items: 
Size: 762830 Color: 1
Size: 237157 Color: 0

Bin 1092: 14 of cap free
Amount of items: 2
Items: 
Size: 768376 Color: 0
Size: 231611 Color: 1

Bin 1093: 14 of cap free
Amount of items: 2
Items: 
Size: 783021 Color: 1
Size: 216966 Color: 0

Bin 1094: 14 of cap free
Amount of items: 2
Items: 
Size: 793718 Color: 0
Size: 206269 Color: 1

Bin 1095: 14 of cap free
Amount of items: 2
Items: 
Size: 795313 Color: 0
Size: 204674 Color: 1

Bin 1096: 14 of cap free
Amount of items: 2
Items: 
Size: 799276 Color: 0
Size: 200711 Color: 1

Bin 1097: 14 of cap free
Amount of items: 6
Items: 
Size: 210946 Color: 0
Size: 161013 Color: 1
Size: 160837 Color: 1
Size: 160603 Color: 1
Size: 153456 Color: 0
Size: 153132 Color: 0

Bin 1098: 15 of cap free
Amount of items: 5
Items: 
Size: 211866 Color: 0
Size: 211740 Color: 0
Size: 198527 Color: 1
Size: 197532 Color: 1
Size: 180321 Color: 1

Bin 1099: 15 of cap free
Amount of items: 5
Items: 
Size: 218880 Color: 0
Size: 218859 Color: 0
Size: 199011 Color: 1
Size: 198201 Color: 1
Size: 165035 Color: 1

Bin 1100: 15 of cap free
Amount of items: 3
Items: 
Size: 410600 Color: 1
Size: 394587 Color: 0
Size: 194799 Color: 1

Bin 1101: 15 of cap free
Amount of items: 2
Items: 
Size: 503224 Color: 1
Size: 496762 Color: 0

Bin 1102: 15 of cap free
Amount of items: 2
Items: 
Size: 509874 Color: 0
Size: 490112 Color: 1

Bin 1103: 15 of cap free
Amount of items: 2
Items: 
Size: 513004 Color: 1
Size: 486982 Color: 0

Bin 1104: 15 of cap free
Amount of items: 2
Items: 
Size: 550760 Color: 1
Size: 449226 Color: 0

Bin 1105: 15 of cap free
Amount of items: 2
Items: 
Size: 590705 Color: 1
Size: 409281 Color: 0

Bin 1106: 15 of cap free
Amount of items: 2
Items: 
Size: 602876 Color: 0
Size: 397110 Color: 1

Bin 1107: 15 of cap free
Amount of items: 2
Items: 
Size: 605854 Color: 1
Size: 394132 Color: 0

Bin 1108: 15 of cap free
Amount of items: 2
Items: 
Size: 606588 Color: 1
Size: 393398 Color: 0

Bin 1109: 15 of cap free
Amount of items: 2
Items: 
Size: 621850 Color: 0
Size: 378136 Color: 1

Bin 1110: 15 of cap free
Amount of items: 2
Items: 
Size: 645780 Color: 1
Size: 354206 Color: 0

Bin 1111: 15 of cap free
Amount of items: 2
Items: 
Size: 668198 Color: 1
Size: 331788 Color: 0

Bin 1112: 15 of cap free
Amount of items: 2
Items: 
Size: 680553 Color: 0
Size: 319433 Color: 1

Bin 1113: 15 of cap free
Amount of items: 2
Items: 
Size: 686660 Color: 0
Size: 313326 Color: 1

Bin 1114: 15 of cap free
Amount of items: 2
Items: 
Size: 702081 Color: 1
Size: 297905 Color: 0

Bin 1115: 15 of cap free
Amount of items: 2
Items: 
Size: 722443 Color: 1
Size: 277543 Color: 0

Bin 1116: 15 of cap free
Amount of items: 2
Items: 
Size: 748383 Color: 0
Size: 251603 Color: 1

Bin 1117: 15 of cap free
Amount of items: 2
Items: 
Size: 753429 Color: 0
Size: 246557 Color: 1

Bin 1118: 15 of cap free
Amount of items: 2
Items: 
Size: 754238 Color: 0
Size: 245748 Color: 1

Bin 1119: 15 of cap free
Amount of items: 2
Items: 
Size: 761892 Color: 0
Size: 238094 Color: 1

Bin 1120: 15 of cap free
Amount of items: 2
Items: 
Size: 776741 Color: 1
Size: 223245 Color: 0

Bin 1121: 15 of cap free
Amount of items: 2
Items: 
Size: 780169 Color: 0
Size: 219817 Color: 1

Bin 1122: 15 of cap free
Amount of items: 2
Items: 
Size: 785273 Color: 0
Size: 214713 Color: 1

Bin 1123: 15 of cap free
Amount of items: 2
Items: 
Size: 787031 Color: 0
Size: 212955 Color: 1

Bin 1124: 16 of cap free
Amount of items: 2
Items: 
Size: 500049 Color: 0
Size: 499936 Color: 1

Bin 1125: 16 of cap free
Amount of items: 2
Items: 
Size: 501608 Color: 0
Size: 498377 Color: 1

Bin 1126: 16 of cap free
Amount of items: 2
Items: 
Size: 505715 Color: 1
Size: 494270 Color: 0

Bin 1127: 16 of cap free
Amount of items: 2
Items: 
Size: 517881 Color: 1
Size: 482104 Color: 0

Bin 1128: 16 of cap free
Amount of items: 2
Items: 
Size: 540208 Color: 0
Size: 459777 Color: 1

Bin 1129: 16 of cap free
Amount of items: 2
Items: 
Size: 543235 Color: 0
Size: 456750 Color: 1

Bin 1130: 16 of cap free
Amount of items: 3
Items: 
Size: 554204 Color: 0
Size: 248231 Color: 1
Size: 197550 Color: 1

Bin 1131: 16 of cap free
Amount of items: 2
Items: 
Size: 585973 Color: 0
Size: 414012 Color: 1

Bin 1132: 16 of cap free
Amount of items: 2
Items: 
Size: 590266 Color: 1
Size: 409719 Color: 0

Bin 1133: 16 of cap free
Amount of items: 2
Items: 
Size: 619058 Color: 1
Size: 380927 Color: 0

Bin 1134: 16 of cap free
Amount of items: 2
Items: 
Size: 624129 Color: 1
Size: 375856 Color: 0

Bin 1135: 16 of cap free
Amount of items: 2
Items: 
Size: 651768 Color: 1
Size: 348217 Color: 0

Bin 1136: 16 of cap free
Amount of items: 2
Items: 
Size: 652856 Color: 1
Size: 347129 Color: 0

Bin 1137: 16 of cap free
Amount of items: 2
Items: 
Size: 665375 Color: 1
Size: 334610 Color: 0

Bin 1138: 16 of cap free
Amount of items: 2
Items: 
Size: 739373 Color: 0
Size: 260612 Color: 1

Bin 1139: 16 of cap free
Amount of items: 2
Items: 
Size: 754693 Color: 0
Size: 245292 Color: 1

Bin 1140: 16 of cap free
Amount of items: 2
Items: 
Size: 755789 Color: 0
Size: 244196 Color: 1

Bin 1141: 16 of cap free
Amount of items: 2
Items: 
Size: 758860 Color: 1
Size: 241125 Color: 0

Bin 1142: 16 of cap free
Amount of items: 2
Items: 
Size: 767360 Color: 1
Size: 232625 Color: 0

Bin 1143: 16 of cap free
Amount of items: 2
Items: 
Size: 768731 Color: 0
Size: 231254 Color: 1

Bin 1144: 16 of cap free
Amount of items: 2
Items: 
Size: 768780 Color: 0
Size: 231205 Color: 1

Bin 1145: 16 of cap free
Amount of items: 2
Items: 
Size: 786968 Color: 1
Size: 213017 Color: 0

Bin 1146: 16 of cap free
Amount of items: 2
Items: 
Size: 790515 Color: 1
Size: 209470 Color: 0

Bin 1147: 16 of cap free
Amount of items: 2
Items: 
Size: 748662 Color: 1
Size: 251323 Color: 0

Bin 1148: 16 of cap free
Amount of items: 5
Items: 
Size: 365568 Color: 1
Size: 164218 Color: 1
Size: 159865 Color: 0
Size: 158949 Color: 0
Size: 151385 Color: 0

Bin 1149: 17 of cap free
Amount of items: 2
Items: 
Size: 660921 Color: 1
Size: 339063 Color: 0

Bin 1150: 17 of cap free
Amount of items: 3
Items: 
Size: 445447 Color: 0
Size: 410941 Color: 1
Size: 143596 Color: 1

Bin 1151: 17 of cap free
Amount of items: 2
Items: 
Size: 513809 Color: 0
Size: 486175 Color: 1

Bin 1152: 17 of cap free
Amount of items: 2
Items: 
Size: 513883 Color: 0
Size: 486101 Color: 1

Bin 1153: 17 of cap free
Amount of items: 2
Items: 
Size: 520676 Color: 0
Size: 479308 Color: 1

Bin 1154: 17 of cap free
Amount of items: 2
Items: 
Size: 527641 Color: 1
Size: 472343 Color: 0

Bin 1155: 17 of cap free
Amount of items: 2
Items: 
Size: 543745 Color: 0
Size: 456239 Color: 1

Bin 1156: 17 of cap free
Amount of items: 2
Items: 
Size: 547315 Color: 1
Size: 452669 Color: 0

Bin 1157: 17 of cap free
Amount of items: 2
Items: 
Size: 572029 Color: 1
Size: 427955 Color: 0

Bin 1158: 17 of cap free
Amount of items: 2
Items: 
Size: 578976 Color: 0
Size: 421008 Color: 1

Bin 1159: 17 of cap free
Amount of items: 2
Items: 
Size: 581737 Color: 0
Size: 418247 Color: 1

Bin 1160: 17 of cap free
Amount of items: 2
Items: 
Size: 608561 Color: 0
Size: 391423 Color: 1

Bin 1161: 17 of cap free
Amount of items: 2
Items: 
Size: 609955 Color: 1
Size: 390029 Color: 0

Bin 1162: 17 of cap free
Amount of items: 2
Items: 
Size: 609993 Color: 0
Size: 389991 Color: 1

Bin 1163: 17 of cap free
Amount of items: 2
Items: 
Size: 612670 Color: 0
Size: 387314 Color: 1

Bin 1164: 17 of cap free
Amount of items: 2
Items: 
Size: 623741 Color: 1
Size: 376243 Color: 0

Bin 1165: 17 of cap free
Amount of items: 2
Items: 
Size: 635115 Color: 1
Size: 364869 Color: 0

Bin 1166: 17 of cap free
Amount of items: 2
Items: 
Size: 645814 Color: 0
Size: 354170 Color: 1

Bin 1167: 17 of cap free
Amount of items: 2
Items: 
Size: 653881 Color: 0
Size: 346103 Color: 1

Bin 1168: 17 of cap free
Amount of items: 2
Items: 
Size: 654753 Color: 0
Size: 345231 Color: 1

Bin 1169: 17 of cap free
Amount of items: 2
Items: 
Size: 657258 Color: 0
Size: 342726 Color: 1

Bin 1170: 17 of cap free
Amount of items: 2
Items: 
Size: 677610 Color: 1
Size: 322374 Color: 0

Bin 1171: 17 of cap free
Amount of items: 2
Items: 
Size: 745291 Color: 1
Size: 254693 Color: 0

Bin 1172: 17 of cap free
Amount of items: 2
Items: 
Size: 750164 Color: 0
Size: 249820 Color: 1

Bin 1173: 17 of cap free
Amount of items: 2
Items: 
Size: 774753 Color: 1
Size: 225231 Color: 0

Bin 1174: 17 of cap free
Amount of items: 2
Items: 
Size: 790411 Color: 0
Size: 209573 Color: 1

Bin 1175: 17 of cap free
Amount of items: 3
Items: 
Size: 705635 Color: 1
Size: 150326 Color: 0
Size: 144023 Color: 0

Bin 1176: 17 of cap free
Amount of items: 3
Items: 
Size: 346787 Color: 0
Size: 337452 Color: 0
Size: 315745 Color: 1

Bin 1177: 17 of cap free
Amount of items: 5
Items: 
Size: 237600 Color: 1
Size: 191014 Color: 0
Size: 190986 Color: 0
Size: 190834 Color: 0
Size: 189550 Color: 1

Bin 1178: 18 of cap free
Amount of items: 3
Items: 
Size: 483330 Color: 0
Size: 370719 Color: 1
Size: 145934 Color: 1

Bin 1179: 18 of cap free
Amount of items: 2
Items: 
Size: 500803 Color: 1
Size: 499180 Color: 0

Bin 1180: 18 of cap free
Amount of items: 2
Items: 
Size: 502150 Color: 0
Size: 497833 Color: 1

Bin 1181: 18 of cap free
Amount of items: 2
Items: 
Size: 512381 Color: 0
Size: 487602 Color: 1

Bin 1182: 18 of cap free
Amount of items: 2
Items: 
Size: 519892 Color: 1
Size: 480091 Color: 0

Bin 1183: 18 of cap free
Amount of items: 2
Items: 
Size: 526492 Color: 0
Size: 473491 Color: 1

Bin 1184: 18 of cap free
Amount of items: 2
Items: 
Size: 541175 Color: 1
Size: 458808 Color: 0

Bin 1185: 18 of cap free
Amount of items: 2
Items: 
Size: 580128 Color: 1
Size: 419855 Color: 0

Bin 1186: 18 of cap free
Amount of items: 2
Items: 
Size: 598207 Color: 1
Size: 401776 Color: 0

Bin 1187: 18 of cap free
Amount of items: 2
Items: 
Size: 611748 Color: 1
Size: 388235 Color: 0

Bin 1188: 18 of cap free
Amount of items: 2
Items: 
Size: 627096 Color: 0
Size: 372887 Color: 1

Bin 1189: 18 of cap free
Amount of items: 2
Items: 
Size: 631727 Color: 1
Size: 368256 Color: 0

Bin 1190: 18 of cap free
Amount of items: 2
Items: 
Size: 633357 Color: 0
Size: 366626 Color: 1

Bin 1191: 18 of cap free
Amount of items: 2
Items: 
Size: 635085 Color: 0
Size: 364898 Color: 1

Bin 1192: 18 of cap free
Amount of items: 2
Items: 
Size: 640443 Color: 1
Size: 359540 Color: 0

Bin 1193: 18 of cap free
Amount of items: 2
Items: 
Size: 645193 Color: 0
Size: 354790 Color: 1

Bin 1194: 18 of cap free
Amount of items: 2
Items: 
Size: 653651 Color: 1
Size: 346332 Color: 0

Bin 1195: 18 of cap free
Amount of items: 2
Items: 
Size: 662278 Color: 1
Size: 337705 Color: 0

Bin 1196: 18 of cap free
Amount of items: 2
Items: 
Size: 674548 Color: 0
Size: 325435 Color: 1

Bin 1197: 18 of cap free
Amount of items: 2
Items: 
Size: 691533 Color: 1
Size: 308450 Color: 0

Bin 1198: 18 of cap free
Amount of items: 2
Items: 
Size: 691753 Color: 0
Size: 308230 Color: 1

Bin 1199: 18 of cap free
Amount of items: 2
Items: 
Size: 692019 Color: 0
Size: 307964 Color: 1

Bin 1200: 18 of cap free
Amount of items: 2
Items: 
Size: 699414 Color: 0
Size: 300569 Color: 1

Bin 1201: 18 of cap free
Amount of items: 2
Items: 
Size: 704758 Color: 1
Size: 295225 Color: 0

Bin 1202: 18 of cap free
Amount of items: 2
Items: 
Size: 718083 Color: 1
Size: 281900 Color: 0

Bin 1203: 18 of cap free
Amount of items: 2
Items: 
Size: 768899 Color: 1
Size: 231084 Color: 0

Bin 1204: 18 of cap free
Amount of items: 5
Items: 
Size: 278190 Color: 1
Size: 247548 Color: 0
Size: 187915 Color: 0
Size: 151878 Color: 1
Size: 134452 Color: 0

Bin 1205: 18 of cap free
Amount of items: 3
Items: 
Size: 510917 Color: 0
Size: 280179 Color: 1
Size: 208887 Color: 0

Bin 1206: 18 of cap free
Amount of items: 6
Items: 
Size: 170785 Color: 0
Size: 170044 Color: 0
Size: 169669 Color: 0
Size: 166385 Color: 1
Size: 166262 Color: 1
Size: 156838 Color: 1

Bin 1207: 18 of cap free
Amount of items: 3
Items: 
Size: 371536 Color: 0
Size: 346244 Color: 1
Size: 282203 Color: 1

Bin 1208: 18 of cap free
Amount of items: 5
Items: 
Size: 376739 Color: 0
Size: 157656 Color: 0
Size: 157477 Color: 1
Size: 157449 Color: 1
Size: 150662 Color: 0

Bin 1209: 19 of cap free
Amount of items: 2
Items: 
Size: 505548 Color: 1
Size: 494434 Color: 0

Bin 1210: 19 of cap free
Amount of items: 2
Items: 
Size: 535444 Color: 1
Size: 464538 Color: 0

Bin 1211: 19 of cap free
Amount of items: 2
Items: 
Size: 536830 Color: 0
Size: 463152 Color: 1

Bin 1212: 19 of cap free
Amount of items: 2
Items: 
Size: 547662 Color: 0
Size: 452320 Color: 1

Bin 1213: 19 of cap free
Amount of items: 2
Items: 
Size: 565846 Color: 1
Size: 434136 Color: 0

Bin 1214: 19 of cap free
Amount of items: 2
Items: 
Size: 581956 Color: 1
Size: 418026 Color: 0

Bin 1215: 19 of cap free
Amount of items: 2
Items: 
Size: 619123 Color: 1
Size: 380859 Color: 0

Bin 1216: 19 of cap free
Amount of items: 2
Items: 
Size: 621431 Color: 0
Size: 378551 Color: 1

Bin 1217: 19 of cap free
Amount of items: 2
Items: 
Size: 626829 Color: 0
Size: 373153 Color: 1

Bin 1218: 19 of cap free
Amount of items: 2
Items: 
Size: 671225 Color: 0
Size: 328757 Color: 1

Bin 1219: 19 of cap free
Amount of items: 2
Items: 
Size: 671359 Color: 1
Size: 328623 Color: 0

Bin 1220: 19 of cap free
Amount of items: 2
Items: 
Size: 672786 Color: 0
Size: 327196 Color: 1

Bin 1221: 19 of cap free
Amount of items: 2
Items: 
Size: 687059 Color: 0
Size: 312923 Color: 1

Bin 1222: 19 of cap free
Amount of items: 2
Items: 
Size: 705745 Color: 0
Size: 294237 Color: 1

Bin 1223: 19 of cap free
Amount of items: 2
Items: 
Size: 721270 Color: 0
Size: 278712 Color: 1

Bin 1224: 19 of cap free
Amount of items: 2
Items: 
Size: 730434 Color: 0
Size: 269548 Color: 1

Bin 1225: 19 of cap free
Amount of items: 2
Items: 
Size: 735157 Color: 0
Size: 264825 Color: 1

Bin 1226: 19 of cap free
Amount of items: 2
Items: 
Size: 756249 Color: 1
Size: 243733 Color: 0

Bin 1227: 19 of cap free
Amount of items: 2
Items: 
Size: 794347 Color: 0
Size: 205635 Color: 1

Bin 1228: 19 of cap free
Amount of items: 5
Items: 
Size: 397218 Color: 1
Size: 207253 Color: 1
Size: 134605 Color: 1
Size: 130524 Color: 0
Size: 130382 Color: 0

Bin 1229: 19 of cap free
Amount of items: 5
Items: 
Size: 276795 Color: 1
Size: 182162 Color: 0
Size: 181733 Color: 0
Size: 181668 Color: 0
Size: 177624 Color: 1

Bin 1230: 20 of cap free
Amount of items: 3
Items: 
Size: 469264 Color: 1
Size: 394155 Color: 0
Size: 136562 Color: 1

Bin 1231: 20 of cap free
Amount of items: 2
Items: 
Size: 501280 Color: 1
Size: 498701 Color: 0

Bin 1232: 20 of cap free
Amount of items: 2
Items: 
Size: 501865 Color: 1
Size: 498116 Color: 0

Bin 1233: 20 of cap free
Amount of items: 2
Items: 
Size: 511250 Color: 1
Size: 488731 Color: 0

Bin 1234: 20 of cap free
Amount of items: 2
Items: 
Size: 511629 Color: 0
Size: 488352 Color: 1

Bin 1235: 20 of cap free
Amount of items: 2
Items: 
Size: 523415 Color: 0
Size: 476566 Color: 1

Bin 1236: 20 of cap free
Amount of items: 2
Items: 
Size: 532040 Color: 0
Size: 467941 Color: 1

Bin 1237: 20 of cap free
Amount of items: 2
Items: 
Size: 536016 Color: 0
Size: 463965 Color: 1

Bin 1238: 20 of cap free
Amount of items: 2
Items: 
Size: 547437 Color: 1
Size: 452544 Color: 0

Bin 1239: 20 of cap free
Amount of items: 2
Items: 
Size: 578413 Color: 1
Size: 421568 Color: 0

Bin 1240: 20 of cap free
Amount of items: 2
Items: 
Size: 586200 Color: 1
Size: 413781 Color: 0

Bin 1241: 20 of cap free
Amount of items: 2
Items: 
Size: 597745 Color: 0
Size: 402236 Color: 1

Bin 1242: 20 of cap free
Amount of items: 2
Items: 
Size: 604582 Color: 0
Size: 395399 Color: 1

Bin 1243: 20 of cap free
Amount of items: 2
Items: 
Size: 631816 Color: 0
Size: 368165 Color: 1

Bin 1244: 20 of cap free
Amount of items: 2
Items: 
Size: 640696 Color: 1
Size: 359285 Color: 0

Bin 1245: 20 of cap free
Amount of items: 2
Items: 
Size: 657902 Color: 0
Size: 342079 Color: 1

Bin 1246: 20 of cap free
Amount of items: 2
Items: 
Size: 703918 Color: 0
Size: 296063 Color: 1

Bin 1247: 20 of cap free
Amount of items: 2
Items: 
Size: 715609 Color: 0
Size: 284372 Color: 1

Bin 1248: 20 of cap free
Amount of items: 2
Items: 
Size: 751299 Color: 0
Size: 248682 Color: 1

Bin 1249: 20 of cap free
Amount of items: 2
Items: 
Size: 753957 Color: 1
Size: 246024 Color: 0

Bin 1250: 20 of cap free
Amount of items: 2
Items: 
Size: 765789 Color: 0
Size: 234192 Color: 1

Bin 1251: 20 of cap free
Amount of items: 2
Items: 
Size: 773615 Color: 0
Size: 226366 Color: 1

Bin 1252: 20 of cap free
Amount of items: 2
Items: 
Size: 792911 Color: 0
Size: 207070 Color: 1

Bin 1253: 20 of cap free
Amount of items: 2
Items: 
Size: 799196 Color: 1
Size: 200785 Color: 0

Bin 1254: 20 of cap free
Amount of items: 3
Items: 
Size: 391927 Color: 0
Size: 342534 Color: 1
Size: 265520 Color: 1

Bin 1255: 20 of cap free
Amount of items: 6
Items: 
Size: 169325 Color: 0
Size: 168559 Color: 0
Size: 167891 Color: 0
Size: 166676 Color: 1
Size: 166240 Color: 1
Size: 161290 Color: 1

Bin 1256: 20 of cap free
Amount of items: 2
Items: 
Size: 791879 Color: 1
Size: 208102 Color: 0

Bin 1257: 20 of cap free
Amount of items: 6
Items: 
Size: 188005 Color: 0
Size: 163110 Color: 1
Size: 163050 Color: 1
Size: 162535 Color: 1
Size: 161642 Color: 0
Size: 161639 Color: 0

Bin 1258: 21 of cap free
Amount of items: 3
Items: 
Size: 410897 Color: 1
Size: 392759 Color: 0
Size: 196324 Color: 0

Bin 1259: 21 of cap free
Amount of items: 3
Items: 
Size: 422580 Color: 1
Size: 414546 Color: 0
Size: 162854 Color: 0

Bin 1260: 21 of cap free
Amount of items: 2
Items: 
Size: 500540 Color: 1
Size: 499440 Color: 0

Bin 1261: 21 of cap free
Amount of items: 2
Items: 
Size: 516912 Color: 1
Size: 483068 Color: 0

Bin 1262: 21 of cap free
Amount of items: 2
Items: 
Size: 546103 Color: 0
Size: 453877 Color: 1

Bin 1263: 21 of cap free
Amount of items: 2
Items: 
Size: 556733 Color: 0
Size: 443247 Color: 1

Bin 1264: 21 of cap free
Amount of items: 2
Items: 
Size: 578694 Color: 1
Size: 421286 Color: 0

Bin 1265: 21 of cap free
Amount of items: 2
Items: 
Size: 592616 Color: 1
Size: 407364 Color: 0

Bin 1266: 21 of cap free
Amount of items: 2
Items: 
Size: 609529 Color: 1
Size: 390451 Color: 0

Bin 1267: 21 of cap free
Amount of items: 2
Items: 
Size: 627340 Color: 0
Size: 372640 Color: 1

Bin 1268: 21 of cap free
Amount of items: 2
Items: 
Size: 656434 Color: 0
Size: 343546 Color: 1

Bin 1269: 21 of cap free
Amount of items: 2
Items: 
Size: 656680 Color: 1
Size: 343300 Color: 0

Bin 1270: 21 of cap free
Amount of items: 2
Items: 
Size: 667212 Color: 0
Size: 332768 Color: 1

Bin 1271: 21 of cap free
Amount of items: 2
Items: 
Size: 701228 Color: 1
Size: 298752 Color: 0

Bin 1272: 21 of cap free
Amount of items: 2
Items: 
Size: 708686 Color: 0
Size: 291294 Color: 1

Bin 1273: 21 of cap free
Amount of items: 2
Items: 
Size: 719646 Color: 1
Size: 280334 Color: 0

Bin 1274: 21 of cap free
Amount of items: 2
Items: 
Size: 729434 Color: 1
Size: 270546 Color: 0

Bin 1275: 21 of cap free
Amount of items: 2
Items: 
Size: 752240 Color: 0
Size: 247740 Color: 1

Bin 1276: 21 of cap free
Amount of items: 2
Items: 
Size: 753717 Color: 0
Size: 246263 Color: 1

Bin 1277: 21 of cap free
Amount of items: 2
Items: 
Size: 780781 Color: 1
Size: 219199 Color: 0

Bin 1278: 21 of cap free
Amount of items: 2
Items: 
Size: 784045 Color: 1
Size: 215935 Color: 0

Bin 1279: 22 of cap free
Amount of items: 3
Items: 
Size: 391639 Color: 0
Size: 374809 Color: 0
Size: 233531 Color: 1

Bin 1280: 22 of cap free
Amount of items: 2
Items: 
Size: 519828 Color: 1
Size: 480151 Color: 0

Bin 1281: 22 of cap free
Amount of items: 2
Items: 
Size: 520425 Color: 1
Size: 479554 Color: 0

Bin 1282: 22 of cap free
Amount of items: 2
Items: 
Size: 526726 Color: 0
Size: 473253 Color: 1

Bin 1283: 22 of cap free
Amount of items: 2
Items: 
Size: 541337 Color: 1
Size: 458642 Color: 0

Bin 1284: 22 of cap free
Amount of items: 2
Items: 
Size: 559927 Color: 0
Size: 440052 Color: 1

Bin 1285: 22 of cap free
Amount of items: 2
Items: 
Size: 576274 Color: 0
Size: 423705 Color: 1

Bin 1286: 22 of cap free
Amount of items: 2
Items: 
Size: 582370 Color: 1
Size: 417609 Color: 0

Bin 1287: 22 of cap free
Amount of items: 2
Items: 
Size: 594233 Color: 0
Size: 405746 Color: 1

Bin 1288: 22 of cap free
Amount of items: 2
Items: 
Size: 596961 Color: 0
Size: 403018 Color: 1

Bin 1289: 22 of cap free
Amount of items: 2
Items: 
Size: 599607 Color: 1
Size: 400372 Color: 0

Bin 1290: 22 of cap free
Amount of items: 2
Items: 
Size: 601961 Color: 0
Size: 398018 Color: 1

Bin 1291: 22 of cap free
Amount of items: 2
Items: 
Size: 609618 Color: 1
Size: 390361 Color: 0

Bin 1292: 22 of cap free
Amount of items: 2
Items: 
Size: 618755 Color: 1
Size: 381224 Color: 0

Bin 1293: 22 of cap free
Amount of items: 2
Items: 
Size: 641076 Color: 1
Size: 358903 Color: 0

Bin 1294: 22 of cap free
Amount of items: 2
Items: 
Size: 648498 Color: 1
Size: 351481 Color: 0

Bin 1295: 22 of cap free
Amount of items: 2
Items: 
Size: 654994 Color: 0
Size: 344985 Color: 1

Bin 1296: 22 of cap free
Amount of items: 2
Items: 
Size: 671820 Color: 0
Size: 328159 Color: 1

Bin 1297: 22 of cap free
Amount of items: 2
Items: 
Size: 675327 Color: 1
Size: 324652 Color: 0

Bin 1298: 22 of cap free
Amount of items: 2
Items: 
Size: 679647 Color: 1
Size: 320332 Color: 0

Bin 1299: 22 of cap free
Amount of items: 2
Items: 
Size: 682322 Color: 0
Size: 317657 Color: 1

Bin 1300: 22 of cap free
Amount of items: 2
Items: 
Size: 683227 Color: 1
Size: 316752 Color: 0

Bin 1301: 22 of cap free
Amount of items: 2
Items: 
Size: 720310 Color: 0
Size: 279669 Color: 1

Bin 1302: 22 of cap free
Amount of items: 2
Items: 
Size: 728483 Color: 1
Size: 271496 Color: 0

Bin 1303: 22 of cap free
Amount of items: 2
Items: 
Size: 749498 Color: 0
Size: 250481 Color: 1

Bin 1304: 22 of cap free
Amount of items: 2
Items: 
Size: 781004 Color: 1
Size: 218975 Color: 0

Bin 1305: 22 of cap free
Amount of items: 2
Items: 
Size: 787884 Color: 1
Size: 212095 Color: 0

Bin 1306: 22 of cap free
Amount of items: 2
Items: 
Size: 790256 Color: 1
Size: 209723 Color: 0

Bin 1307: 22 of cap free
Amount of items: 2
Items: 
Size: 792590 Color: 0
Size: 207389 Color: 1

Bin 1308: 23 of cap free
Amount of items: 3
Items: 
Size: 418388 Color: 1
Size: 297318 Color: 0
Size: 284272 Color: 0

Bin 1309: 23 of cap free
Amount of items: 2
Items: 
Size: 502658 Color: 0
Size: 497320 Color: 1

Bin 1310: 23 of cap free
Amount of items: 2
Items: 
Size: 505108 Color: 0
Size: 494870 Color: 1

Bin 1311: 23 of cap free
Amount of items: 2
Items: 
Size: 509293 Color: 1
Size: 490685 Color: 0

Bin 1312: 23 of cap free
Amount of items: 2
Items: 
Size: 546107 Color: 1
Size: 453871 Color: 0

Bin 1313: 23 of cap free
Amount of items: 2
Items: 
Size: 553849 Color: 1
Size: 446129 Color: 0

Bin 1314: 23 of cap free
Amount of items: 2
Items: 
Size: 588716 Color: 1
Size: 411262 Color: 0

Bin 1315: 23 of cap free
Amount of items: 2
Items: 
Size: 594981 Color: 1
Size: 404997 Color: 0

Bin 1316: 23 of cap free
Amount of items: 2
Items: 
Size: 613410 Color: 1
Size: 386568 Color: 0

Bin 1317: 23 of cap free
Amount of items: 2
Items: 
Size: 613592 Color: 1
Size: 386386 Color: 0

Bin 1318: 23 of cap free
Amount of items: 2
Items: 
Size: 638052 Color: 1
Size: 361926 Color: 0

Bin 1319: 23 of cap free
Amount of items: 2
Items: 
Size: 639941 Color: 1
Size: 360037 Color: 0

Bin 1320: 23 of cap free
Amount of items: 2
Items: 
Size: 642975 Color: 1
Size: 357003 Color: 0

Bin 1321: 23 of cap free
Amount of items: 2
Items: 
Size: 646067 Color: 0
Size: 353911 Color: 1

Bin 1322: 23 of cap free
Amount of items: 2
Items: 
Size: 654619 Color: 0
Size: 345359 Color: 1

Bin 1323: 23 of cap free
Amount of items: 2
Items: 
Size: 657801 Color: 0
Size: 342177 Color: 1

Bin 1324: 23 of cap free
Amount of items: 2
Items: 
Size: 660974 Color: 0
Size: 339004 Color: 1

Bin 1325: 23 of cap free
Amount of items: 2
Items: 
Size: 672944 Color: 1
Size: 327034 Color: 0

Bin 1326: 23 of cap free
Amount of items: 2
Items: 
Size: 676447 Color: 1
Size: 323531 Color: 0

Bin 1327: 23 of cap free
Amount of items: 2
Items: 
Size: 682145 Color: 1
Size: 317833 Color: 0

Bin 1328: 23 of cap free
Amount of items: 2
Items: 
Size: 684809 Color: 0
Size: 315169 Color: 1

Bin 1329: 23 of cap free
Amount of items: 2
Items: 
Size: 685881 Color: 0
Size: 314097 Color: 1

Bin 1330: 23 of cap free
Amount of items: 2
Items: 
Size: 717210 Color: 0
Size: 282768 Color: 1

Bin 1331: 23 of cap free
Amount of items: 2
Items: 
Size: 729628 Color: 0
Size: 270350 Color: 1

Bin 1332: 23 of cap free
Amount of items: 2
Items: 
Size: 732320 Color: 0
Size: 267658 Color: 1

Bin 1333: 23 of cap free
Amount of items: 2
Items: 
Size: 733762 Color: 0
Size: 266216 Color: 1

Bin 1334: 23 of cap free
Amount of items: 2
Items: 
Size: 739992 Color: 0
Size: 259986 Color: 1

Bin 1335: 23 of cap free
Amount of items: 2
Items: 
Size: 749951 Color: 1
Size: 250027 Color: 0

Bin 1336: 23 of cap free
Amount of items: 2
Items: 
Size: 770166 Color: 1
Size: 229812 Color: 0

Bin 1337: 23 of cap free
Amount of items: 2
Items: 
Size: 796029 Color: 0
Size: 203949 Color: 1

Bin 1338: 24 of cap free
Amount of items: 2
Items: 
Size: 504103 Color: 0
Size: 495874 Color: 1

Bin 1339: 24 of cap free
Amount of items: 2
Items: 
Size: 511367 Color: 1
Size: 488610 Color: 0

Bin 1340: 24 of cap free
Amount of items: 2
Items: 
Size: 522813 Color: 0
Size: 477164 Color: 1

Bin 1341: 24 of cap free
Amount of items: 2
Items: 
Size: 524722 Color: 0
Size: 475255 Color: 1

Bin 1342: 24 of cap free
Amount of items: 2
Items: 
Size: 529746 Color: 0
Size: 470231 Color: 1

Bin 1343: 24 of cap free
Amount of items: 2
Items: 
Size: 533553 Color: 1
Size: 466424 Color: 0

Bin 1344: 24 of cap free
Amount of items: 2
Items: 
Size: 537615 Color: 0
Size: 462362 Color: 1

Bin 1345: 24 of cap free
Amount of items: 2
Items: 
Size: 563569 Color: 0
Size: 436408 Color: 1

Bin 1346: 24 of cap free
Amount of items: 2
Items: 
Size: 576477 Color: 1
Size: 423500 Color: 0

Bin 1347: 24 of cap free
Amount of items: 2
Items: 
Size: 581734 Color: 0
Size: 418243 Color: 1

Bin 1348: 24 of cap free
Amount of items: 2
Items: 
Size: 591981 Color: 0
Size: 407996 Color: 1

Bin 1349: 24 of cap free
Amount of items: 2
Items: 
Size: 625664 Color: 1
Size: 374313 Color: 0

Bin 1350: 24 of cap free
Amount of items: 2
Items: 
Size: 626358 Color: 1
Size: 373619 Color: 0

Bin 1351: 24 of cap free
Amount of items: 2
Items: 
Size: 629157 Color: 0
Size: 370820 Color: 1

Bin 1352: 24 of cap free
Amount of items: 2
Items: 
Size: 641919 Color: 0
Size: 358058 Color: 1

Bin 1353: 24 of cap free
Amount of items: 2
Items: 
Size: 655244 Color: 0
Size: 344733 Color: 1

Bin 1354: 24 of cap free
Amount of items: 2
Items: 
Size: 680361 Color: 1
Size: 319616 Color: 0

Bin 1355: 24 of cap free
Amount of items: 2
Items: 
Size: 686246 Color: 1
Size: 313731 Color: 0

Bin 1356: 24 of cap free
Amount of items: 2
Items: 
Size: 694528 Color: 1
Size: 305449 Color: 0

Bin 1357: 24 of cap free
Amount of items: 2
Items: 
Size: 702763 Color: 0
Size: 297214 Color: 1

Bin 1358: 24 of cap free
Amount of items: 2
Items: 
Size: 709937 Color: 0
Size: 290040 Color: 1

Bin 1359: 24 of cap free
Amount of items: 2
Items: 
Size: 724316 Color: 0
Size: 275661 Color: 1

Bin 1360: 24 of cap free
Amount of items: 2
Items: 
Size: 751204 Color: 1
Size: 248773 Color: 0

Bin 1361: 24 of cap free
Amount of items: 2
Items: 
Size: 754058 Color: 0
Size: 245919 Color: 1

Bin 1362: 24 of cap free
Amount of items: 2
Items: 
Size: 779128 Color: 0
Size: 220849 Color: 1

Bin 1363: 24 of cap free
Amount of items: 3
Items: 
Size: 438137 Color: 0
Size: 410529 Color: 1
Size: 151311 Color: 1

Bin 1364: 25 of cap free
Amount of items: 2
Items: 
Size: 515735 Color: 0
Size: 484241 Color: 1

Bin 1365: 25 of cap free
Amount of items: 2
Items: 
Size: 632452 Color: 0
Size: 367524 Color: 1

Bin 1366: 25 of cap free
Amount of items: 2
Items: 
Size: 636154 Color: 0
Size: 363822 Color: 1

Bin 1367: 25 of cap free
Amount of items: 2
Items: 
Size: 647270 Color: 0
Size: 352706 Color: 1

Bin 1368: 25 of cap free
Amount of items: 2
Items: 
Size: 648693 Color: 1
Size: 351283 Color: 0

Bin 1369: 25 of cap free
Amount of items: 2
Items: 
Size: 651718 Color: 0
Size: 348258 Color: 1

Bin 1370: 25 of cap free
Amount of items: 2
Items: 
Size: 681284 Color: 0
Size: 318692 Color: 1

Bin 1371: 25 of cap free
Amount of items: 2
Items: 
Size: 682865 Color: 0
Size: 317111 Color: 1

Bin 1372: 25 of cap free
Amount of items: 2
Items: 
Size: 711431 Color: 0
Size: 288545 Color: 1

Bin 1373: 25 of cap free
Amount of items: 2
Items: 
Size: 714389 Color: 1
Size: 285587 Color: 0

Bin 1374: 25 of cap free
Amount of items: 2
Items: 
Size: 717068 Color: 0
Size: 282908 Color: 1

Bin 1375: 25 of cap free
Amount of items: 2
Items: 
Size: 723988 Color: 1
Size: 275988 Color: 0

Bin 1376: 25 of cap free
Amount of items: 2
Items: 
Size: 728901 Color: 1
Size: 271075 Color: 0

Bin 1377: 25 of cap free
Amount of items: 2
Items: 
Size: 732620 Color: 1
Size: 267356 Color: 0

Bin 1378: 25 of cap free
Amount of items: 2
Items: 
Size: 737916 Color: 0
Size: 262060 Color: 1

Bin 1379: 25 of cap free
Amount of items: 2
Items: 
Size: 743293 Color: 0
Size: 256683 Color: 1

Bin 1380: 25 of cap free
Amount of items: 2
Items: 
Size: 744907 Color: 0
Size: 255069 Color: 1

Bin 1381: 25 of cap free
Amount of items: 2
Items: 
Size: 747597 Color: 0
Size: 252379 Color: 1

Bin 1382: 25 of cap free
Amount of items: 2
Items: 
Size: 752117 Color: 1
Size: 247859 Color: 0

Bin 1383: 25 of cap free
Amount of items: 2
Items: 
Size: 768291 Color: 1
Size: 231685 Color: 0

Bin 1384: 25 of cap free
Amount of items: 2
Items: 
Size: 779691 Color: 1
Size: 220285 Color: 0

Bin 1385: 25 of cap free
Amount of items: 2
Items: 
Size: 788764 Color: 0
Size: 211212 Color: 1

Bin 1386: 25 of cap free
Amount of items: 2
Items: 
Size: 799646 Color: 0
Size: 200330 Color: 1

Bin 1387: 26 of cap free
Amount of items: 2
Items: 
Size: 554510 Color: 0
Size: 445465 Color: 1

Bin 1388: 26 of cap free
Amount of items: 2
Items: 
Size: 502930 Color: 1
Size: 497045 Color: 0

Bin 1389: 26 of cap free
Amount of items: 2
Items: 
Size: 505380 Color: 0
Size: 494595 Color: 1

Bin 1390: 26 of cap free
Amount of items: 2
Items: 
Size: 515499 Color: 0
Size: 484476 Color: 1

Bin 1391: 26 of cap free
Amount of items: 2
Items: 
Size: 563783 Color: 1
Size: 436192 Color: 0

Bin 1392: 26 of cap free
Amount of items: 2
Items: 
Size: 567637 Color: 1
Size: 432338 Color: 0

Bin 1393: 26 of cap free
Amount of items: 2
Items: 
Size: 571689 Color: 0
Size: 428286 Color: 1

Bin 1394: 26 of cap free
Amount of items: 2
Items: 
Size: 574324 Color: 0
Size: 425651 Color: 1

Bin 1395: 26 of cap free
Amount of items: 2
Items: 
Size: 595980 Color: 0
Size: 403995 Color: 1

Bin 1396: 26 of cap free
Amount of items: 2
Items: 
Size: 613915 Color: 1
Size: 386060 Color: 0

Bin 1397: 26 of cap free
Amount of items: 2
Items: 
Size: 616896 Color: 0
Size: 383079 Color: 1

Bin 1398: 26 of cap free
Amount of items: 2
Items: 
Size: 642047 Color: 1
Size: 357928 Color: 0

Bin 1399: 26 of cap free
Amount of items: 2
Items: 
Size: 713453 Color: 1
Size: 286522 Color: 0

Bin 1400: 26 of cap free
Amount of items: 2
Items: 
Size: 716737 Color: 0
Size: 283238 Color: 1

Bin 1401: 26 of cap free
Amount of items: 2
Items: 
Size: 718209 Color: 1
Size: 281766 Color: 0

Bin 1402: 26 of cap free
Amount of items: 2
Items: 
Size: 762392 Color: 1
Size: 237583 Color: 0

Bin 1403: 26 of cap free
Amount of items: 2
Items: 
Size: 768513 Color: 0
Size: 231462 Color: 1

Bin 1404: 26 of cap free
Amount of items: 2
Items: 
Size: 768573 Color: 0
Size: 231402 Color: 1

Bin 1405: 26 of cap free
Amount of items: 2
Items: 
Size: 794083 Color: 0
Size: 205892 Color: 1

Bin 1406: 26 of cap free
Amount of items: 2
Items: 
Size: 796440 Color: 1
Size: 203535 Color: 0

Bin 1407: 27 of cap free
Amount of items: 2
Items: 
Size: 503497 Color: 1
Size: 496477 Color: 0

Bin 1408: 27 of cap free
Amount of items: 2
Items: 
Size: 525503 Color: 0
Size: 474471 Color: 1

Bin 1409: 27 of cap free
Amount of items: 2
Items: 
Size: 531243 Color: 0
Size: 468731 Color: 1

Bin 1410: 27 of cap free
Amount of items: 2
Items: 
Size: 535540 Color: 1
Size: 464434 Color: 0

Bin 1411: 27 of cap free
Amount of items: 2
Items: 
Size: 560435 Color: 0
Size: 439539 Color: 1

Bin 1412: 27 of cap free
Amount of items: 2
Items: 
Size: 587255 Color: 1
Size: 412719 Color: 0

Bin 1413: 27 of cap free
Amount of items: 2
Items: 
Size: 606730 Color: 1
Size: 393244 Color: 0

Bin 1414: 27 of cap free
Amount of items: 2
Items: 
Size: 638341 Color: 1
Size: 361633 Color: 0

Bin 1415: 27 of cap free
Amount of items: 2
Items: 
Size: 641311 Color: 0
Size: 358663 Color: 1

Bin 1416: 27 of cap free
Amount of items: 2
Items: 
Size: 643383 Color: 1
Size: 356591 Color: 0

Bin 1417: 27 of cap free
Amount of items: 2
Items: 
Size: 656187 Color: 0
Size: 343787 Color: 1

Bin 1418: 27 of cap free
Amount of items: 2
Items: 
Size: 679329 Color: 1
Size: 320645 Color: 0

Bin 1419: 27 of cap free
Amount of items: 2
Items: 
Size: 681793 Color: 0
Size: 318181 Color: 1

Bin 1420: 27 of cap free
Amount of items: 2
Items: 
Size: 706351 Color: 0
Size: 293623 Color: 1

Bin 1421: 27 of cap free
Amount of items: 2
Items: 
Size: 714894 Color: 0
Size: 285080 Color: 1

Bin 1422: 27 of cap free
Amount of items: 2
Items: 
Size: 723482 Color: 1
Size: 276492 Color: 0

Bin 1423: 27 of cap free
Amount of items: 2
Items: 
Size: 737520 Color: 1
Size: 262454 Color: 0

Bin 1424: 27 of cap free
Amount of items: 2
Items: 
Size: 738199 Color: 1
Size: 261775 Color: 0

Bin 1425: 27 of cap free
Amount of items: 2
Items: 
Size: 754120 Color: 1
Size: 245854 Color: 0

Bin 1426: 27 of cap free
Amount of items: 2
Items: 
Size: 757823 Color: 0
Size: 242151 Color: 1

Bin 1427: 27 of cap free
Amount of items: 2
Items: 
Size: 764521 Color: 0
Size: 235453 Color: 1

Bin 1428: 27 of cap free
Amount of items: 2
Items: 
Size: 769315 Color: 1
Size: 230659 Color: 0

Bin 1429: 27 of cap free
Amount of items: 3
Items: 
Size: 764050 Color: 1
Size: 131848 Color: 0
Size: 104076 Color: 0

Bin 1430: 27 of cap free
Amount of items: 5
Items: 
Size: 257410 Color: 1
Size: 186783 Color: 0
Size: 185575 Color: 1
Size: 185244 Color: 0
Size: 184962 Color: 0

Bin 1431: 28 of cap free
Amount of items: 2
Items: 
Size: 517370 Color: 1
Size: 482603 Color: 0

Bin 1432: 28 of cap free
Amount of items: 2
Items: 
Size: 530959 Color: 0
Size: 469014 Color: 1

Bin 1433: 28 of cap free
Amount of items: 2
Items: 
Size: 556143 Color: 1
Size: 443830 Color: 0

Bin 1434: 28 of cap free
Amount of items: 2
Items: 
Size: 560553 Color: 1
Size: 439420 Color: 0

Bin 1435: 28 of cap free
Amount of items: 2
Items: 
Size: 611182 Color: 0
Size: 388791 Color: 1

Bin 1436: 28 of cap free
Amount of items: 2
Items: 
Size: 614833 Color: 0
Size: 385140 Color: 1

Bin 1437: 28 of cap free
Amount of items: 2
Items: 
Size: 656740 Color: 0
Size: 343233 Color: 1

Bin 1438: 28 of cap free
Amount of items: 2
Items: 
Size: 662740 Color: 1
Size: 337233 Color: 0

Bin 1439: 28 of cap free
Amount of items: 2
Items: 
Size: 667115 Color: 0
Size: 332858 Color: 1

Bin 1440: 28 of cap free
Amount of items: 2
Items: 
Size: 681329 Color: 1
Size: 318644 Color: 0

Bin 1441: 28 of cap free
Amount of items: 2
Items: 
Size: 700936 Color: 0
Size: 299037 Color: 1

Bin 1442: 28 of cap free
Amount of items: 2
Items: 
Size: 708756 Color: 0
Size: 291217 Color: 1

Bin 1443: 28 of cap free
Amount of items: 2
Items: 
Size: 748211 Color: 1
Size: 251762 Color: 0

Bin 1444: 28 of cap free
Amount of items: 2
Items: 
Size: 778461 Color: 0
Size: 221512 Color: 1

Bin 1445: 28 of cap free
Amount of items: 2
Items: 
Size: 789147 Color: 1
Size: 210826 Color: 0

Bin 1446: 28 of cap free
Amount of items: 2
Items: 
Size: 743491 Color: 0
Size: 256482 Color: 1

Bin 1447: 29 of cap free
Amount of items: 3
Items: 
Size: 601463 Color: 1
Size: 256653 Color: 1
Size: 141856 Color: 0

Bin 1448: 29 of cap free
Amount of items: 2
Items: 
Size: 506124 Color: 1
Size: 493848 Color: 0

Bin 1449: 29 of cap free
Amount of items: 2
Items: 
Size: 545789 Color: 1
Size: 454183 Color: 0

Bin 1450: 29 of cap free
Amount of items: 2
Items: 
Size: 550338 Color: 1
Size: 449634 Color: 0

Bin 1451: 29 of cap free
Amount of items: 2
Items: 
Size: 553006 Color: 1
Size: 446966 Color: 0

Bin 1452: 29 of cap free
Amount of items: 2
Items: 
Size: 588662 Color: 1
Size: 411310 Color: 0

Bin 1453: 29 of cap free
Amount of items: 2
Items: 
Size: 602196 Color: 0
Size: 397776 Color: 1

Bin 1454: 29 of cap free
Amount of items: 2
Items: 
Size: 611992 Color: 1
Size: 387980 Color: 0

Bin 1455: 29 of cap free
Amount of items: 2
Items: 
Size: 612634 Color: 1
Size: 387338 Color: 0

Bin 1456: 29 of cap free
Amount of items: 2
Items: 
Size: 616596 Color: 1
Size: 383376 Color: 0

Bin 1457: 29 of cap free
Amount of items: 2
Items: 
Size: 623686 Color: 1
Size: 376286 Color: 0

Bin 1458: 29 of cap free
Amount of items: 2
Items: 
Size: 625021 Color: 0
Size: 374951 Color: 1

Bin 1459: 29 of cap free
Amount of items: 2
Items: 
Size: 638641 Color: 0
Size: 361331 Color: 1

Bin 1460: 29 of cap free
Amount of items: 2
Items: 
Size: 646507 Color: 0
Size: 353465 Color: 1

Bin 1461: 29 of cap free
Amount of items: 2
Items: 
Size: 646759 Color: 0
Size: 353213 Color: 1

Bin 1462: 29 of cap free
Amount of items: 2
Items: 
Size: 655370 Color: 0
Size: 344602 Color: 1

Bin 1463: 29 of cap free
Amount of items: 2
Items: 
Size: 658527 Color: 0
Size: 341445 Color: 1

Bin 1464: 29 of cap free
Amount of items: 2
Items: 
Size: 661094 Color: 0
Size: 338878 Color: 1

Bin 1465: 29 of cap free
Amount of items: 2
Items: 
Size: 731840 Color: 0
Size: 268132 Color: 1

Bin 1466: 29 of cap free
Amount of items: 2
Items: 
Size: 753835 Color: 1
Size: 246137 Color: 0

Bin 1467: 29 of cap free
Amount of items: 2
Items: 
Size: 799360 Color: 1
Size: 200612 Color: 0

Bin 1468: 29 of cap free
Amount of items: 5
Items: 
Size: 285248 Color: 1
Size: 184962 Color: 0
Size: 180288 Color: 0
Size: 175317 Color: 1
Size: 174157 Color: 1

Bin 1469: 30 of cap free
Amount of items: 3
Items: 
Size: 377382 Color: 1
Size: 374905 Color: 0
Size: 247684 Color: 1

Bin 1470: 30 of cap free
Amount of items: 2
Items: 
Size: 525159 Color: 0
Size: 474812 Color: 1

Bin 1471: 30 of cap free
Amount of items: 2
Items: 
Size: 530729 Color: 1
Size: 469242 Color: 0

Bin 1472: 30 of cap free
Amount of items: 2
Items: 
Size: 547035 Color: 1
Size: 452936 Color: 0

Bin 1473: 30 of cap free
Amount of items: 2
Items: 
Size: 573054 Color: 1
Size: 426917 Color: 0

Bin 1474: 30 of cap free
Amount of items: 2
Items: 
Size: 606194 Color: 0
Size: 393777 Color: 1

Bin 1475: 30 of cap free
Amount of items: 2
Items: 
Size: 642371 Color: 0
Size: 357600 Color: 1

Bin 1476: 30 of cap free
Amount of items: 2
Items: 
Size: 648621 Color: 1
Size: 351350 Color: 0

Bin 1477: 30 of cap free
Amount of items: 2
Items: 
Size: 666002 Color: 0
Size: 333969 Color: 1

Bin 1478: 30 of cap free
Amount of items: 2
Items: 
Size: 678199 Color: 0
Size: 321772 Color: 1

Bin 1479: 30 of cap free
Amount of items: 2
Items: 
Size: 680661 Color: 1
Size: 319310 Color: 0

Bin 1480: 30 of cap free
Amount of items: 2
Items: 
Size: 747449 Color: 1
Size: 252522 Color: 0

Bin 1481: 30 of cap free
Amount of items: 2
Items: 
Size: 756661 Color: 0
Size: 243310 Color: 1

Bin 1482: 30 of cap free
Amount of items: 2
Items: 
Size: 761216 Color: 0
Size: 238755 Color: 1

Bin 1483: 30 of cap free
Amount of items: 2
Items: 
Size: 765510 Color: 0
Size: 234461 Color: 1

Bin 1484: 30 of cap free
Amount of items: 2
Items: 
Size: 776130 Color: 0
Size: 223841 Color: 1

Bin 1485: 30 of cap free
Amount of items: 2
Items: 
Size: 785257 Color: 1
Size: 214714 Color: 0

Bin 1486: 30 of cap free
Amount of items: 2
Items: 
Size: 770548 Color: 0
Size: 229423 Color: 1

Bin 1487: 30 of cap free
Amount of items: 3
Items: 
Size: 393058 Color: 0
Size: 392374 Color: 0
Size: 214539 Color: 1

Bin 1488: 31 of cap free
Amount of items: 2
Items: 
Size: 500266 Color: 1
Size: 499704 Color: 0

Bin 1489: 31 of cap free
Amount of items: 2
Items: 
Size: 550979 Color: 1
Size: 448991 Color: 0

Bin 1490: 31 of cap free
Amount of items: 2
Items: 
Size: 561645 Color: 1
Size: 438325 Color: 0

Bin 1491: 31 of cap free
Amount of items: 2
Items: 
Size: 577324 Color: 0
Size: 422646 Color: 1

Bin 1492: 31 of cap free
Amount of items: 2
Items: 
Size: 589197 Color: 1
Size: 410773 Color: 0

Bin 1493: 31 of cap free
Amount of items: 2
Items: 
Size: 589705 Color: 0
Size: 410265 Color: 1

Bin 1494: 31 of cap free
Amount of items: 2
Items: 
Size: 594442 Color: 0
Size: 405528 Color: 1

Bin 1495: 31 of cap free
Amount of items: 2
Items: 
Size: 596731 Color: 0
Size: 403239 Color: 1

Bin 1496: 31 of cap free
Amount of items: 2
Items: 
Size: 613299 Color: 0
Size: 386671 Color: 1

Bin 1497: 31 of cap free
Amount of items: 2
Items: 
Size: 617377 Color: 0
Size: 382593 Color: 1

Bin 1498: 31 of cap free
Amount of items: 2
Items: 
Size: 632827 Color: 1
Size: 367143 Color: 0

Bin 1499: 31 of cap free
Amount of items: 2
Items: 
Size: 632982 Color: 1
Size: 366988 Color: 0

Bin 1500: 31 of cap free
Amount of items: 2
Items: 
Size: 645999 Color: 1
Size: 353971 Color: 0

Bin 1501: 31 of cap free
Amount of items: 2
Items: 
Size: 648989 Color: 1
Size: 350981 Color: 0

Bin 1502: 31 of cap free
Amount of items: 2
Items: 
Size: 665911 Color: 1
Size: 334059 Color: 0

Bin 1503: 31 of cap free
Amount of items: 2
Items: 
Size: 708332 Color: 1
Size: 291638 Color: 0

Bin 1504: 31 of cap free
Amount of items: 2
Items: 
Size: 727794 Color: 0
Size: 272176 Color: 1

Bin 1505: 31 of cap free
Amount of items: 2
Items: 
Size: 754078 Color: 1
Size: 245892 Color: 0

Bin 1506: 31 of cap free
Amount of items: 2
Items: 
Size: 777926 Color: 0
Size: 222044 Color: 1

Bin 1507: 32 of cap free
Amount of items: 2
Items: 
Size: 502674 Color: 1
Size: 497295 Color: 0

Bin 1508: 32 of cap free
Amount of items: 2
Items: 
Size: 518769 Color: 1
Size: 481200 Color: 0

Bin 1509: 32 of cap free
Amount of items: 2
Items: 
Size: 534583 Color: 1
Size: 465386 Color: 0

Bin 1510: 32 of cap free
Amount of items: 2
Items: 
Size: 553528 Color: 0
Size: 446441 Color: 1

Bin 1511: 32 of cap free
Amount of items: 2
Items: 
Size: 566166 Color: 1
Size: 433803 Color: 0

Bin 1512: 32 of cap free
Amount of items: 2
Items: 
Size: 586889 Color: 0
Size: 413080 Color: 1

Bin 1513: 32 of cap free
Amount of items: 2
Items: 
Size: 594928 Color: 1
Size: 405041 Color: 0

Bin 1514: 32 of cap free
Amount of items: 2
Items: 
Size: 614136 Color: 1
Size: 385833 Color: 0

Bin 1515: 32 of cap free
Amount of items: 2
Items: 
Size: 625898 Color: 0
Size: 374071 Color: 1

Bin 1516: 32 of cap free
Amount of items: 2
Items: 
Size: 631159 Color: 0
Size: 368810 Color: 1

Bin 1517: 32 of cap free
Amount of items: 2
Items: 
Size: 677561 Color: 0
Size: 322408 Color: 1

Bin 1518: 32 of cap free
Amount of items: 2
Items: 
Size: 696833 Color: 0
Size: 303136 Color: 1

Bin 1519: 32 of cap free
Amount of items: 2
Items: 
Size: 724437 Color: 1
Size: 275532 Color: 0

Bin 1520: 32 of cap free
Amount of items: 2
Items: 
Size: 742141 Color: 0
Size: 257828 Color: 1

Bin 1521: 32 of cap free
Amount of items: 2
Items: 
Size: 786036 Color: 1
Size: 213933 Color: 0

Bin 1522: 32 of cap free
Amount of items: 2
Items: 
Size: 797249 Color: 1
Size: 202720 Color: 0

Bin 1523: 32 of cap free
Amount of items: 3
Items: 
Size: 369198 Color: 1
Size: 316939 Color: 0
Size: 313832 Color: 0

Bin 1524: 32 of cap free
Amount of items: 5
Items: 
Size: 230423 Color: 1
Size: 193408 Color: 0
Size: 193078 Color: 0
Size: 191775 Color: 1
Size: 191285 Color: 1

Bin 1525: 33 of cap free
Amount of items: 2
Items: 
Size: 524447 Color: 1
Size: 475521 Color: 0

Bin 1526: 33 of cap free
Amount of items: 2
Items: 
Size: 526761 Color: 1
Size: 473207 Color: 0

Bin 1527: 33 of cap free
Amount of items: 2
Items: 
Size: 534999 Color: 1
Size: 464969 Color: 0

Bin 1528: 33 of cap free
Amount of items: 2
Items: 
Size: 558298 Color: 1
Size: 441670 Color: 0

Bin 1529: 33 of cap free
Amount of items: 2
Items: 
Size: 561068 Color: 1
Size: 438900 Color: 0

Bin 1530: 33 of cap free
Amount of items: 2
Items: 
Size: 592653 Color: 0
Size: 407315 Color: 1

Bin 1531: 33 of cap free
Amount of items: 2
Items: 
Size: 598076 Color: 0
Size: 401892 Color: 1

Bin 1532: 33 of cap free
Amount of items: 2
Items: 
Size: 617595 Color: 0
Size: 382373 Color: 1

Bin 1533: 33 of cap free
Amount of items: 2
Items: 
Size: 632683 Color: 1
Size: 367285 Color: 0

Bin 1534: 33 of cap free
Amount of items: 2
Items: 
Size: 644948 Color: 1
Size: 355020 Color: 0

Bin 1535: 33 of cap free
Amount of items: 2
Items: 
Size: 666834 Color: 0
Size: 333134 Color: 1

Bin 1536: 33 of cap free
Amount of items: 2
Items: 
Size: 668401 Color: 0
Size: 331567 Color: 1

Bin 1537: 33 of cap free
Amount of items: 2
Items: 
Size: 671325 Color: 0
Size: 328643 Color: 1

Bin 1538: 33 of cap free
Amount of items: 2
Items: 
Size: 710156 Color: 1
Size: 289812 Color: 0

Bin 1539: 33 of cap free
Amount of items: 2
Items: 
Size: 731563 Color: 0
Size: 268405 Color: 1

Bin 1540: 33 of cap free
Amount of items: 2
Items: 
Size: 767730 Color: 0
Size: 232238 Color: 1

Bin 1541: 33 of cap free
Amount of items: 2
Items: 
Size: 773020 Color: 1
Size: 226948 Color: 0

Bin 1542: 33 of cap free
Amount of items: 2
Items: 
Size: 683357 Color: 1
Size: 316611 Color: 0

Bin 1543: 34 of cap free
Amount of items: 2
Items: 
Size: 504321 Color: 1
Size: 495646 Color: 0

Bin 1544: 34 of cap free
Amount of items: 2
Items: 
Size: 538467 Color: 0
Size: 461500 Color: 1

Bin 1545: 34 of cap free
Amount of items: 2
Items: 
Size: 575659 Color: 0
Size: 424308 Color: 1

Bin 1546: 34 of cap free
Amount of items: 2
Items: 
Size: 577002 Color: 0
Size: 422965 Color: 1

Bin 1547: 34 of cap free
Amount of items: 2
Items: 
Size: 595600 Color: 0
Size: 404367 Color: 1

Bin 1548: 34 of cap free
Amount of items: 2
Items: 
Size: 633075 Color: 0
Size: 366892 Color: 1

Bin 1549: 34 of cap free
Amount of items: 2
Items: 
Size: 663665 Color: 1
Size: 336302 Color: 0

Bin 1550: 34 of cap free
Amount of items: 2
Items: 
Size: 682343 Color: 1
Size: 317624 Color: 0

Bin 1551: 34 of cap free
Amount of items: 2
Items: 
Size: 685168 Color: 0
Size: 314799 Color: 1

Bin 1552: 34 of cap free
Amount of items: 2
Items: 
Size: 707842 Color: 1
Size: 292125 Color: 0

Bin 1553: 34 of cap free
Amount of items: 2
Items: 
Size: 708050 Color: 0
Size: 291917 Color: 1

Bin 1554: 34 of cap free
Amount of items: 2
Items: 
Size: 723893 Color: 1
Size: 276074 Color: 0

Bin 1555: 34 of cap free
Amount of items: 2
Items: 
Size: 765503 Color: 1
Size: 234464 Color: 0

Bin 1556: 34 of cap free
Amount of items: 2
Items: 
Size: 765837 Color: 0
Size: 234130 Color: 1

Bin 1557: 34 of cap free
Amount of items: 2
Items: 
Size: 517544 Color: 1
Size: 482423 Color: 0

Bin 1558: 35 of cap free
Amount of items: 2
Items: 
Size: 505117 Color: 1
Size: 494849 Color: 0

Bin 1559: 35 of cap free
Amount of items: 2
Items: 
Size: 546683 Color: 1
Size: 453283 Color: 0

Bin 1560: 35 of cap free
Amount of items: 2
Items: 
Size: 556402 Color: 1
Size: 443564 Color: 0

Bin 1561: 35 of cap free
Amount of items: 2
Items: 
Size: 569414 Color: 1
Size: 430552 Color: 0

Bin 1562: 35 of cap free
Amount of items: 2
Items: 
Size: 576827 Color: 0
Size: 423139 Color: 1

Bin 1563: 35 of cap free
Amount of items: 2
Items: 
Size: 601856 Color: 0
Size: 398110 Color: 1

Bin 1564: 35 of cap free
Amount of items: 2
Items: 
Size: 622719 Color: 0
Size: 377247 Color: 1

Bin 1565: 35 of cap free
Amount of items: 2
Items: 
Size: 629458 Color: 1
Size: 370508 Color: 0

Bin 1566: 35 of cap free
Amount of items: 2
Items: 
Size: 646931 Color: 1
Size: 353035 Color: 0

Bin 1567: 35 of cap free
Amount of items: 2
Items: 
Size: 647791 Color: 1
Size: 352175 Color: 0

Bin 1568: 35 of cap free
Amount of items: 2
Items: 
Size: 673921 Color: 0
Size: 326045 Color: 1

Bin 1569: 35 of cap free
Amount of items: 2
Items: 
Size: 675351 Color: 0
Size: 324615 Color: 1

Bin 1570: 35 of cap free
Amount of items: 2
Items: 
Size: 723111 Color: 1
Size: 276855 Color: 0

Bin 1571: 35 of cap free
Amount of items: 2
Items: 
Size: 750442 Color: 1
Size: 249524 Color: 0

Bin 1572: 35 of cap free
Amount of items: 2
Items: 
Size: 783362 Color: 1
Size: 216604 Color: 0

Bin 1573: 35 of cap free
Amount of items: 2
Items: 
Size: 786440 Color: 0
Size: 213526 Color: 1

Bin 1574: 35 of cap free
Amount of items: 2
Items: 
Size: 795150 Color: 0
Size: 204816 Color: 1

Bin 1575: 36 of cap free
Amount of items: 2
Items: 
Size: 507923 Color: 0
Size: 492042 Color: 1

Bin 1576: 36 of cap free
Amount of items: 2
Items: 
Size: 517784 Color: 0
Size: 482181 Color: 1

Bin 1577: 36 of cap free
Amount of items: 2
Items: 
Size: 530555 Color: 1
Size: 469410 Color: 0

Bin 1578: 36 of cap free
Amount of items: 2
Items: 
Size: 536572 Color: 1
Size: 463393 Color: 0

Bin 1579: 36 of cap free
Amount of items: 2
Items: 
Size: 559694 Color: 1
Size: 440271 Color: 0

Bin 1580: 36 of cap free
Amount of items: 2
Items: 
Size: 589878 Color: 0
Size: 410087 Color: 1

Bin 1581: 36 of cap free
Amount of items: 2
Items: 
Size: 606642 Color: 0
Size: 393323 Color: 1

Bin 1582: 36 of cap free
Amount of items: 2
Items: 
Size: 612727 Color: 1
Size: 387238 Color: 0

Bin 1583: 36 of cap free
Amount of items: 2
Items: 
Size: 618607 Color: 0
Size: 381358 Color: 1

Bin 1584: 36 of cap free
Amount of items: 2
Items: 
Size: 641486 Color: 0
Size: 358479 Color: 1

Bin 1585: 36 of cap free
Amount of items: 2
Items: 
Size: 659470 Color: 0
Size: 340495 Color: 1

Bin 1586: 36 of cap free
Amount of items: 2
Items: 
Size: 674750 Color: 1
Size: 325215 Color: 0

Bin 1587: 36 of cap free
Amount of items: 2
Items: 
Size: 682598 Color: 1
Size: 317367 Color: 0

Bin 1588: 36 of cap free
Amount of items: 2
Items: 
Size: 692083 Color: 0
Size: 307882 Color: 1

Bin 1589: 36 of cap free
Amount of items: 2
Items: 
Size: 698635 Color: 1
Size: 301330 Color: 0

Bin 1590: 36 of cap free
Amount of items: 2
Items: 
Size: 705154 Color: 0
Size: 294811 Color: 1

Bin 1591: 36 of cap free
Amount of items: 2
Items: 
Size: 756746 Color: 1
Size: 243219 Color: 0

Bin 1592: 36 of cap free
Amount of items: 2
Items: 
Size: 791726 Color: 0
Size: 208239 Color: 1

Bin 1593: 36 of cap free
Amount of items: 2
Items: 
Size: 793141 Color: 0
Size: 206824 Color: 1

Bin 1594: 37 of cap free
Amount of items: 2
Items: 
Size: 500975 Color: 0
Size: 498989 Color: 1

Bin 1595: 37 of cap free
Amount of items: 2
Items: 
Size: 510012 Color: 0
Size: 489952 Color: 1

Bin 1596: 37 of cap free
Amount of items: 2
Items: 
Size: 563403 Color: 1
Size: 436561 Color: 0

Bin 1597: 37 of cap free
Amount of items: 2
Items: 
Size: 583208 Color: 1
Size: 416756 Color: 0

Bin 1598: 37 of cap free
Amount of items: 2
Items: 
Size: 596162 Color: 0
Size: 403802 Color: 1

Bin 1599: 37 of cap free
Amount of items: 2
Items: 
Size: 616651 Color: 1
Size: 383313 Color: 0

Bin 1600: 37 of cap free
Amount of items: 2
Items: 
Size: 651566 Color: 1
Size: 348398 Color: 0

Bin 1601: 37 of cap free
Amount of items: 2
Items: 
Size: 653537 Color: 1
Size: 346427 Color: 0

Bin 1602: 37 of cap free
Amount of items: 2
Items: 
Size: 670716 Color: 1
Size: 329248 Color: 0

Bin 1603: 37 of cap free
Amount of items: 2
Items: 
Size: 679577 Color: 1
Size: 320387 Color: 0

Bin 1604: 37 of cap free
Amount of items: 2
Items: 
Size: 684453 Color: 1
Size: 315511 Color: 0

Bin 1605: 37 of cap free
Amount of items: 2
Items: 
Size: 692397 Color: 0
Size: 307567 Color: 1

Bin 1606: 37 of cap free
Amount of items: 2
Items: 
Size: 705007 Color: 0
Size: 294957 Color: 1

Bin 1607: 37 of cap free
Amount of items: 2
Items: 
Size: 705561 Color: 1
Size: 294403 Color: 0

Bin 1608: 37 of cap free
Amount of items: 2
Items: 
Size: 729041 Color: 1
Size: 270923 Color: 0

Bin 1609: 37 of cap free
Amount of items: 2
Items: 
Size: 732950 Color: 0
Size: 267014 Color: 1

Bin 1610: 37 of cap free
Amount of items: 2
Items: 
Size: 758762 Color: 1
Size: 241202 Color: 0

Bin 1611: 38 of cap free
Amount of items: 2
Items: 
Size: 524967 Color: 0
Size: 474996 Color: 1

Bin 1612: 38 of cap free
Amount of items: 2
Items: 
Size: 561486 Color: 0
Size: 438477 Color: 1

Bin 1613: 38 of cap free
Amount of items: 2
Items: 
Size: 582136 Color: 1
Size: 417827 Color: 0

Bin 1614: 38 of cap free
Amount of items: 2
Items: 
Size: 621613 Color: 0
Size: 378350 Color: 1

Bin 1615: 38 of cap free
Amount of items: 2
Items: 
Size: 628609 Color: 1
Size: 371354 Color: 0

Bin 1616: 38 of cap free
Amount of items: 2
Items: 
Size: 633298 Color: 1
Size: 366665 Color: 0

Bin 1617: 38 of cap free
Amount of items: 2
Items: 
Size: 648883 Color: 1
Size: 351080 Color: 0

Bin 1618: 38 of cap free
Amount of items: 2
Items: 
Size: 668666 Color: 0
Size: 331297 Color: 1

Bin 1619: 38 of cap free
Amount of items: 2
Items: 
Size: 684669 Color: 1
Size: 315294 Color: 0

Bin 1620: 38 of cap free
Amount of items: 2
Items: 
Size: 696960 Color: 0
Size: 303003 Color: 1

Bin 1621: 38 of cap free
Amount of items: 2
Items: 
Size: 709248 Color: 1
Size: 290715 Color: 0

Bin 1622: 38 of cap free
Amount of items: 2
Items: 
Size: 724178 Color: 0
Size: 275785 Color: 1

Bin 1623: 38 of cap free
Amount of items: 2
Items: 
Size: 731119 Color: 0
Size: 268844 Color: 1

Bin 1624: 38 of cap free
Amount of items: 2
Items: 
Size: 755618 Color: 0
Size: 244345 Color: 1

Bin 1625: 38 of cap free
Amount of items: 2
Items: 
Size: 763543 Color: 0
Size: 236420 Color: 1

Bin 1626: 38 of cap free
Amount of items: 2
Items: 
Size: 771704 Color: 1
Size: 228259 Color: 0

Bin 1627: 38 of cap free
Amount of items: 2
Items: 
Size: 781158 Color: 0
Size: 218805 Color: 1

Bin 1628: 38 of cap free
Amount of items: 2
Items: 
Size: 792711 Color: 0
Size: 207252 Color: 1

Bin 1629: 39 of cap free
Amount of items: 2
Items: 
Size: 525292 Color: 0
Size: 474670 Color: 1

Bin 1630: 39 of cap free
Amount of items: 2
Items: 
Size: 521675 Color: 1
Size: 478287 Color: 0

Bin 1631: 39 of cap free
Amount of items: 2
Items: 
Size: 534742 Color: 1
Size: 465220 Color: 0

Bin 1632: 39 of cap free
Amount of items: 2
Items: 
Size: 541809 Color: 0
Size: 458153 Color: 1

Bin 1633: 39 of cap free
Amount of items: 2
Items: 
Size: 553778 Color: 0
Size: 446184 Color: 1

Bin 1634: 39 of cap free
Amount of items: 2
Items: 
Size: 578897 Color: 1
Size: 421065 Color: 0

Bin 1635: 39 of cap free
Amount of items: 2
Items: 
Size: 579278 Color: 0
Size: 420684 Color: 1

Bin 1636: 39 of cap free
Amount of items: 2
Items: 
Size: 580370 Color: 0
Size: 419592 Color: 1

Bin 1637: 39 of cap free
Amount of items: 2
Items: 
Size: 587997 Color: 0
Size: 411965 Color: 1

Bin 1638: 39 of cap free
Amount of items: 2
Items: 
Size: 608632 Color: 1
Size: 391330 Color: 0

Bin 1639: 39 of cap free
Amount of items: 2
Items: 
Size: 611932 Color: 0
Size: 388030 Color: 1

Bin 1640: 39 of cap free
Amount of items: 2
Items: 
Size: 613447 Color: 0
Size: 386515 Color: 1

Bin 1641: 39 of cap free
Amount of items: 2
Items: 
Size: 639723 Color: 1
Size: 360239 Color: 0

Bin 1642: 39 of cap free
Amount of items: 2
Items: 
Size: 708286 Color: 1
Size: 291676 Color: 0

Bin 1643: 39 of cap free
Amount of items: 2
Items: 
Size: 720366 Color: 0
Size: 279596 Color: 1

Bin 1644: 39 of cap free
Amount of items: 2
Items: 
Size: 751194 Color: 0
Size: 248768 Color: 1

Bin 1645: 39 of cap free
Amount of items: 2
Items: 
Size: 761828 Color: 1
Size: 238134 Color: 0

Bin 1646: 39 of cap free
Amount of items: 3
Items: 
Size: 759927 Color: 0
Size: 125457 Color: 1
Size: 114578 Color: 0

Bin 1647: 39 of cap free
Amount of items: 3
Items: 
Size: 744737 Color: 0
Size: 129331 Color: 0
Size: 125894 Color: 1

Bin 1648: 40 of cap free
Amount of items: 2
Items: 
Size: 503483 Color: 0
Size: 496478 Color: 1

Bin 1649: 40 of cap free
Amount of items: 2
Items: 
Size: 540658 Color: 0
Size: 459303 Color: 1

Bin 1650: 40 of cap free
Amount of items: 2
Items: 
Size: 590066 Color: 0
Size: 409895 Color: 1

Bin 1651: 40 of cap free
Amount of items: 2
Items: 
Size: 606888 Color: 0
Size: 393073 Color: 1

Bin 1652: 40 of cap free
Amount of items: 2
Items: 
Size: 613981 Color: 1
Size: 385980 Color: 0

Bin 1653: 40 of cap free
Amount of items: 2
Items: 
Size: 618922 Color: 1
Size: 381039 Color: 0

Bin 1654: 40 of cap free
Amount of items: 2
Items: 
Size: 684702 Color: 0
Size: 315259 Color: 1

Bin 1655: 40 of cap free
Amount of items: 2
Items: 
Size: 729340 Color: 0
Size: 270621 Color: 1

Bin 1656: 40 of cap free
Amount of items: 2
Items: 
Size: 741270 Color: 0
Size: 258691 Color: 1

Bin 1657: 40 of cap free
Amount of items: 2
Items: 
Size: 777622 Color: 0
Size: 222339 Color: 1

Bin 1658: 40 of cap free
Amount of items: 2
Items: 
Size: 794489 Color: 1
Size: 205472 Color: 0

Bin 1659: 40 of cap free
Amount of items: 5
Items: 
Size: 236001 Color: 0
Size: 192124 Color: 0
Size: 190755 Color: 1
Size: 190629 Color: 1
Size: 190452 Color: 1

Bin 1660: 41 of cap free
Amount of items: 2
Items: 
Size: 552026 Color: 0
Size: 447934 Color: 1

Bin 1661: 41 of cap free
Amount of items: 2
Items: 
Size: 505799 Color: 1
Size: 494161 Color: 0

Bin 1662: 41 of cap free
Amount of items: 2
Items: 
Size: 543633 Color: 1
Size: 456327 Color: 0

Bin 1663: 41 of cap free
Amount of items: 2
Items: 
Size: 566255 Color: 1
Size: 433705 Color: 0

Bin 1664: 41 of cap free
Amount of items: 2
Items: 
Size: 573497 Color: 0
Size: 426463 Color: 1

Bin 1665: 41 of cap free
Amount of items: 2
Items: 
Size: 592950 Color: 1
Size: 407010 Color: 0

Bin 1666: 41 of cap free
Amount of items: 2
Items: 
Size: 597804 Color: 0
Size: 402156 Color: 1

Bin 1667: 41 of cap free
Amount of items: 2
Items: 
Size: 647453 Color: 0
Size: 352507 Color: 1

Bin 1668: 41 of cap free
Amount of items: 2
Items: 
Size: 677837 Color: 1
Size: 322123 Color: 0

Bin 1669: 41 of cap free
Amount of items: 2
Items: 
Size: 693533 Color: 1
Size: 306427 Color: 0

Bin 1670: 41 of cap free
Amount of items: 2
Items: 
Size: 696186 Color: 0
Size: 303774 Color: 1

Bin 1671: 41 of cap free
Amount of items: 2
Items: 
Size: 703963 Color: 0
Size: 295997 Color: 1

Bin 1672: 41 of cap free
Amount of items: 2
Items: 
Size: 705202 Color: 0
Size: 294758 Color: 1

Bin 1673: 41 of cap free
Amount of items: 2
Items: 
Size: 711519 Color: 1
Size: 288441 Color: 0

Bin 1674: 41 of cap free
Amount of items: 2
Items: 
Size: 714196 Color: 1
Size: 285764 Color: 0

Bin 1675: 41 of cap free
Amount of items: 2
Items: 
Size: 763079 Color: 1
Size: 236881 Color: 0

Bin 1676: 41 of cap free
Amount of items: 2
Items: 
Size: 766854 Color: 0
Size: 233106 Color: 1

Bin 1677: 41 of cap free
Amount of items: 2
Items: 
Size: 778838 Color: 0
Size: 221122 Color: 1

Bin 1678: 42 of cap free
Amount of items: 3
Items: 
Size: 378296 Color: 1
Size: 371227 Color: 0
Size: 250436 Color: 1

Bin 1679: 42 of cap free
Amount of items: 2
Items: 
Size: 514104 Color: 1
Size: 485855 Color: 0

Bin 1680: 42 of cap free
Amount of items: 2
Items: 
Size: 555746 Color: 1
Size: 444213 Color: 0

Bin 1681: 42 of cap free
Amount of items: 2
Items: 
Size: 586283 Color: 1
Size: 413676 Color: 0

Bin 1682: 42 of cap free
Amount of items: 2
Items: 
Size: 627696 Color: 0
Size: 372263 Color: 1

Bin 1683: 42 of cap free
Amount of items: 2
Items: 
Size: 640890 Color: 0
Size: 359069 Color: 1

Bin 1684: 42 of cap free
Amount of items: 2
Items: 
Size: 680559 Color: 1
Size: 319400 Color: 0

Bin 1685: 42 of cap free
Amount of items: 2
Items: 
Size: 716937 Color: 0
Size: 283022 Color: 1

Bin 1686: 42 of cap free
Amount of items: 2
Items: 
Size: 737429 Color: 0
Size: 262530 Color: 1

Bin 1687: 42 of cap free
Amount of items: 2
Items: 
Size: 743549 Color: 1
Size: 256410 Color: 0

Bin 1688: 42 of cap free
Amount of items: 2
Items: 
Size: 779394 Color: 1
Size: 220565 Color: 0

Bin 1689: 42 of cap free
Amount of items: 2
Items: 
Size: 789152 Color: 0
Size: 210807 Color: 1

Bin 1690: 43 of cap free
Amount of items: 3
Items: 
Size: 751789 Color: 0
Size: 129068 Color: 0
Size: 119101 Color: 1

Bin 1691: 43 of cap free
Amount of items: 2
Items: 
Size: 509792 Color: 0
Size: 490166 Color: 1

Bin 1692: 43 of cap free
Amount of items: 2
Items: 
Size: 516934 Color: 1
Size: 483024 Color: 0

Bin 1693: 43 of cap free
Amount of items: 2
Items: 
Size: 531785 Color: 0
Size: 468173 Color: 1

Bin 1694: 43 of cap free
Amount of items: 2
Items: 
Size: 556777 Color: 1
Size: 443181 Color: 0

Bin 1695: 43 of cap free
Amount of items: 2
Items: 
Size: 562324 Color: 1
Size: 437634 Color: 0

Bin 1696: 43 of cap free
Amount of items: 2
Items: 
Size: 565931 Color: 1
Size: 434027 Color: 0

Bin 1697: 43 of cap free
Amount of items: 2
Items: 
Size: 575662 Color: 1
Size: 424296 Color: 0

Bin 1698: 43 of cap free
Amount of items: 2
Items: 
Size: 577622 Color: 0
Size: 422336 Color: 1

Bin 1699: 43 of cap free
Amount of items: 2
Items: 
Size: 605699 Color: 0
Size: 394259 Color: 1

Bin 1700: 43 of cap free
Amount of items: 2
Items: 
Size: 623536 Color: 1
Size: 376422 Color: 0

Bin 1701: 43 of cap free
Amount of items: 2
Items: 
Size: 626409 Color: 1
Size: 373549 Color: 0

Bin 1702: 43 of cap free
Amount of items: 2
Items: 
Size: 665488 Color: 0
Size: 334470 Color: 1

Bin 1703: 43 of cap free
Amount of items: 2
Items: 
Size: 694523 Color: 0
Size: 305435 Color: 1

Bin 1704: 43 of cap free
Amount of items: 2
Items: 
Size: 697695 Color: 0
Size: 302263 Color: 1

Bin 1705: 43 of cap free
Amount of items: 2
Items: 
Size: 728235 Color: 1
Size: 271723 Color: 0

Bin 1706: 43 of cap free
Amount of items: 2
Items: 
Size: 784598 Color: 1
Size: 215360 Color: 0

Bin 1707: 43 of cap free
Amount of items: 4
Items: 
Size: 257335 Color: 0
Size: 253434 Color: 0
Size: 244927 Color: 1
Size: 244262 Color: 1

Bin 1708: 44 of cap free
Amount of items: 2
Items: 
Size: 513557 Color: 0
Size: 486400 Color: 1

Bin 1709: 44 of cap free
Amount of items: 2
Items: 
Size: 523655 Color: 1
Size: 476302 Color: 0

Bin 1710: 44 of cap free
Amount of items: 2
Items: 
Size: 558291 Color: 0
Size: 441666 Color: 1

Bin 1711: 44 of cap free
Amount of items: 2
Items: 
Size: 587269 Color: 0
Size: 412688 Color: 1

Bin 1712: 44 of cap free
Amount of items: 2
Items: 
Size: 639467 Color: 0
Size: 360490 Color: 1

Bin 1713: 44 of cap free
Amount of items: 2
Items: 
Size: 650715 Color: 1
Size: 349242 Color: 0

Bin 1714: 44 of cap free
Amount of items: 2
Items: 
Size: 653173 Color: 0
Size: 346784 Color: 1

Bin 1715: 44 of cap free
Amount of items: 2
Items: 
Size: 680657 Color: 1
Size: 319300 Color: 0

Bin 1716: 44 of cap free
Amount of items: 2
Items: 
Size: 702490 Color: 0
Size: 297467 Color: 1

Bin 1717: 44 of cap free
Amount of items: 2
Items: 
Size: 714756 Color: 0
Size: 285201 Color: 1

Bin 1718: 44 of cap free
Amount of items: 2
Items: 
Size: 715692 Color: 0
Size: 284265 Color: 1

Bin 1719: 44 of cap free
Amount of items: 2
Items: 
Size: 720038 Color: 1
Size: 279919 Color: 0

Bin 1720: 44 of cap free
Amount of items: 2
Items: 
Size: 727461 Color: 1
Size: 272496 Color: 0

Bin 1721: 44 of cap free
Amount of items: 2
Items: 
Size: 730962 Color: 1
Size: 268995 Color: 0

Bin 1722: 44 of cap free
Amount of items: 2
Items: 
Size: 734972 Color: 1
Size: 264985 Color: 0

Bin 1723: 44 of cap free
Amount of items: 2
Items: 
Size: 742308 Color: 0
Size: 257649 Color: 1

Bin 1724: 44 of cap free
Amount of items: 2
Items: 
Size: 772472 Color: 1
Size: 227485 Color: 0

Bin 1725: 44 of cap free
Amount of items: 2
Items: 
Size: 774279 Color: 0
Size: 225678 Color: 1

Bin 1726: 44 of cap free
Amount of items: 2
Items: 
Size: 782385 Color: 1
Size: 217572 Color: 0

Bin 1727: 44 of cap free
Amount of items: 2
Items: 
Size: 783235 Color: 1
Size: 216722 Color: 0

Bin 1728: 45 of cap free
Amount of items: 2
Items: 
Size: 549540 Color: 1
Size: 450416 Color: 0

Bin 1729: 45 of cap free
Amount of items: 2
Items: 
Size: 572061 Color: 0
Size: 427895 Color: 1

Bin 1730: 45 of cap free
Amount of items: 2
Items: 
Size: 588208 Color: 0
Size: 411748 Color: 1

Bin 1731: 45 of cap free
Amount of items: 2
Items: 
Size: 648633 Color: 0
Size: 351323 Color: 1

Bin 1732: 45 of cap free
Amount of items: 2
Items: 
Size: 661089 Color: 1
Size: 338867 Color: 0

Bin 1733: 45 of cap free
Amount of items: 2
Items: 
Size: 665419 Color: 1
Size: 334537 Color: 0

Bin 1734: 45 of cap free
Amount of items: 2
Items: 
Size: 679980 Color: 0
Size: 319976 Color: 1

Bin 1735: 45 of cap free
Amount of items: 2
Items: 
Size: 695943 Color: 1
Size: 304013 Color: 0

Bin 1736: 45 of cap free
Amount of items: 2
Items: 
Size: 700184 Color: 1
Size: 299772 Color: 0

Bin 1737: 45 of cap free
Amount of items: 2
Items: 
Size: 711184 Color: 1
Size: 288772 Color: 0

Bin 1738: 45 of cap free
Amount of items: 2
Items: 
Size: 716468 Color: 1
Size: 283488 Color: 0

Bin 1739: 45 of cap free
Amount of items: 2
Items: 
Size: 754028 Color: 1
Size: 245928 Color: 0

Bin 1740: 45 of cap free
Amount of items: 2
Items: 
Size: 767874 Color: 0
Size: 232082 Color: 1

Bin 1741: 45 of cap free
Amount of items: 2
Items: 
Size: 780064 Color: 1
Size: 219892 Color: 0

Bin 1742: 46 of cap free
Amount of items: 2
Items: 
Size: 552975 Color: 0
Size: 446980 Color: 1

Bin 1743: 46 of cap free
Amount of items: 2
Items: 
Size: 602683 Color: 0
Size: 397272 Color: 1

Bin 1744: 46 of cap free
Amount of items: 2
Items: 
Size: 644972 Color: 0
Size: 354983 Color: 1

Bin 1745: 46 of cap free
Amount of items: 2
Items: 
Size: 696217 Color: 1
Size: 303738 Color: 0

Bin 1746: 46 of cap free
Amount of items: 2
Items: 
Size: 700665 Color: 1
Size: 299290 Color: 0

Bin 1747: 46 of cap free
Amount of items: 2
Items: 
Size: 755638 Color: 1
Size: 244317 Color: 0

Bin 1748: 46 of cap free
Amount of items: 2
Items: 
Size: 762077 Color: 1
Size: 237878 Color: 0

Bin 1749: 46 of cap free
Amount of items: 2
Items: 
Size: 768140 Color: 0
Size: 231815 Color: 1

Bin 1750: 46 of cap free
Amount of items: 2
Items: 
Size: 791539 Color: 0
Size: 208416 Color: 1

Bin 1751: 46 of cap free
Amount of items: 3
Items: 
Size: 715235 Color: 1
Size: 178582 Color: 1
Size: 106138 Color: 0

Bin 1752: 47 of cap free
Amount of items: 2
Items: 
Size: 509552 Color: 0
Size: 490402 Color: 1

Bin 1753: 47 of cap free
Amount of items: 2
Items: 
Size: 520835 Color: 0
Size: 479119 Color: 1

Bin 1754: 47 of cap free
Amount of items: 2
Items: 
Size: 525219 Color: 1
Size: 474735 Color: 0

Bin 1755: 47 of cap free
Amount of items: 2
Items: 
Size: 533538 Color: 1
Size: 466416 Color: 0

Bin 1756: 47 of cap free
Amount of items: 2
Items: 
Size: 538687 Color: 1
Size: 461267 Color: 0

Bin 1757: 47 of cap free
Amount of items: 2
Items: 
Size: 580303 Color: 0
Size: 419651 Color: 1

Bin 1758: 47 of cap free
Amount of items: 2
Items: 
Size: 581503 Color: 1
Size: 418451 Color: 0

Bin 1759: 47 of cap free
Amount of items: 2
Items: 
Size: 582972 Color: 1
Size: 416982 Color: 0

Bin 1760: 47 of cap free
Amount of items: 2
Items: 
Size: 603058 Color: 0
Size: 396896 Color: 1

Bin 1761: 47 of cap free
Amount of items: 2
Items: 
Size: 604630 Color: 0
Size: 395324 Color: 1

Bin 1762: 47 of cap free
Amount of items: 2
Items: 
Size: 612722 Color: 1
Size: 387232 Color: 0

Bin 1763: 47 of cap free
Amount of items: 2
Items: 
Size: 613843 Color: 1
Size: 386111 Color: 0

Bin 1764: 47 of cap free
Amount of items: 2
Items: 
Size: 618658 Color: 1
Size: 381296 Color: 0

Bin 1765: 47 of cap free
Amount of items: 2
Items: 
Size: 620273 Color: 0
Size: 379681 Color: 1

Bin 1766: 47 of cap free
Amount of items: 2
Items: 
Size: 634598 Color: 1
Size: 365356 Color: 0

Bin 1767: 47 of cap free
Amount of items: 2
Items: 
Size: 679556 Color: 0
Size: 320398 Color: 1

Bin 1768: 47 of cap free
Amount of items: 2
Items: 
Size: 702575 Color: 1
Size: 297379 Color: 0

Bin 1769: 47 of cap free
Amount of items: 2
Items: 
Size: 730739 Color: 0
Size: 269215 Color: 1

Bin 1770: 47 of cap free
Amount of items: 2
Items: 
Size: 732542 Color: 1
Size: 267412 Color: 0

Bin 1771: 47 of cap free
Amount of items: 2
Items: 
Size: 741000 Color: 1
Size: 258954 Color: 0

Bin 1772: 47 of cap free
Amount of items: 2
Items: 
Size: 741866 Color: 0
Size: 258088 Color: 1

Bin 1773: 47 of cap free
Amount of items: 2
Items: 
Size: 772617 Color: 0
Size: 227337 Color: 1

Bin 1774: 47 of cap free
Amount of items: 2
Items: 
Size: 800000 Color: 0
Size: 199954 Color: 1

Bin 1775: 48 of cap free
Amount of items: 2
Items: 
Size: 511495 Color: 0
Size: 488458 Color: 1

Bin 1776: 48 of cap free
Amount of items: 2
Items: 
Size: 527452 Color: 1
Size: 472501 Color: 0

Bin 1777: 48 of cap free
Amount of items: 2
Items: 
Size: 528048 Color: 1
Size: 471905 Color: 0

Bin 1778: 48 of cap free
Amount of items: 2
Items: 
Size: 535528 Color: 0
Size: 464425 Color: 1

Bin 1779: 48 of cap free
Amount of items: 2
Items: 
Size: 555431 Color: 1
Size: 444522 Color: 0

Bin 1780: 48 of cap free
Amount of items: 2
Items: 
Size: 582532 Color: 0
Size: 417421 Color: 1

Bin 1781: 48 of cap free
Amount of items: 2
Items: 
Size: 599666 Color: 0
Size: 400287 Color: 1

Bin 1782: 48 of cap free
Amount of items: 2
Items: 
Size: 607365 Color: 0
Size: 392588 Color: 1

Bin 1783: 48 of cap free
Amount of items: 2
Items: 
Size: 696902 Color: 0
Size: 303051 Color: 1

Bin 1784: 48 of cap free
Amount of items: 2
Items: 
Size: 728320 Color: 0
Size: 271633 Color: 1

Bin 1785: 48 of cap free
Amount of items: 2
Items: 
Size: 734132 Color: 1
Size: 265821 Color: 0

Bin 1786: 48 of cap free
Amount of items: 2
Items: 
Size: 775819 Color: 0
Size: 224134 Color: 1

Bin 1787: 48 of cap free
Amount of items: 5
Items: 
Size: 269318 Color: 1
Size: 184074 Color: 0
Size: 183443 Color: 1
Size: 183418 Color: 1
Size: 179700 Color: 0

Bin 1788: 49 of cap free
Amount of items: 2
Items: 
Size: 518834 Color: 0
Size: 481118 Color: 1

Bin 1789: 49 of cap free
Amount of items: 2
Items: 
Size: 519743 Color: 1
Size: 480209 Color: 0

Bin 1790: 49 of cap free
Amount of items: 2
Items: 
Size: 545749 Color: 0
Size: 454203 Color: 1

Bin 1791: 49 of cap free
Amount of items: 2
Items: 
Size: 559094 Color: 0
Size: 440858 Color: 1

Bin 1792: 49 of cap free
Amount of items: 2
Items: 
Size: 568163 Color: 1
Size: 431789 Color: 0

Bin 1793: 49 of cap free
Amount of items: 2
Items: 
Size: 574362 Color: 1
Size: 425590 Color: 0

Bin 1794: 49 of cap free
Amount of items: 2
Items: 
Size: 654273 Color: 0
Size: 345679 Color: 1

Bin 1795: 49 of cap free
Amount of items: 2
Items: 
Size: 655791 Color: 0
Size: 344161 Color: 1

Bin 1796: 49 of cap free
Amount of items: 2
Items: 
Size: 688819 Color: 1
Size: 311133 Color: 0

Bin 1797: 49 of cap free
Amount of items: 2
Items: 
Size: 697938 Color: 0
Size: 302014 Color: 1

Bin 1798: 49 of cap free
Amount of items: 2
Items: 
Size: 720978 Color: 1
Size: 278974 Color: 0

Bin 1799: 49 of cap free
Amount of items: 2
Items: 
Size: 734634 Color: 0
Size: 265318 Color: 1

Bin 1800: 49 of cap free
Amount of items: 2
Items: 
Size: 745308 Color: 0
Size: 254644 Color: 1

Bin 1801: 49 of cap free
Amount of items: 2
Items: 
Size: 756953 Color: 1
Size: 242999 Color: 0

Bin 1802: 49 of cap free
Amount of items: 2
Items: 
Size: 767773 Color: 1
Size: 232179 Color: 0

Bin 1803: 49 of cap free
Amount of items: 2
Items: 
Size: 797593 Color: 0
Size: 202359 Color: 1

Bin 1804: 49 of cap free
Amount of items: 2
Items: 
Size: 744729 Color: 0
Size: 255223 Color: 1

Bin 1805: 50 of cap free
Amount of items: 3
Items: 
Size: 379312 Color: 1
Size: 371696 Color: 0
Size: 248943 Color: 1

Bin 1806: 50 of cap free
Amount of items: 3
Items: 
Size: 414527 Color: 0
Size: 390988 Color: 0
Size: 194436 Color: 1

Bin 1807: 50 of cap free
Amount of items: 2
Items: 
Size: 500963 Color: 0
Size: 498988 Color: 1

Bin 1808: 50 of cap free
Amount of items: 2
Items: 
Size: 511866 Color: 0
Size: 488085 Color: 1

Bin 1809: 50 of cap free
Amount of items: 2
Items: 
Size: 517668 Color: 0
Size: 482283 Color: 1

Bin 1810: 50 of cap free
Amount of items: 2
Items: 
Size: 543346 Color: 0
Size: 456605 Color: 1

Bin 1811: 50 of cap free
Amount of items: 2
Items: 
Size: 567271 Color: 0
Size: 432680 Color: 1

Bin 1812: 50 of cap free
Amount of items: 2
Items: 
Size: 575781 Color: 1
Size: 424170 Color: 0

Bin 1813: 50 of cap free
Amount of items: 2
Items: 
Size: 625867 Color: 1
Size: 374084 Color: 0

Bin 1814: 50 of cap free
Amount of items: 2
Items: 
Size: 689060 Color: 1
Size: 310891 Color: 0

Bin 1815: 50 of cap free
Amount of items: 2
Items: 
Size: 703950 Color: 1
Size: 296001 Color: 0

Bin 1816: 50 of cap free
Amount of items: 2
Items: 
Size: 725421 Color: 1
Size: 274530 Color: 0

Bin 1817: 50 of cap free
Amount of items: 2
Items: 
Size: 734067 Color: 0
Size: 265884 Color: 1

Bin 1818: 50 of cap free
Amount of items: 3
Items: 
Size: 721857 Color: 0
Size: 155811 Color: 0
Size: 122283 Color: 1

Bin 1819: 51 of cap free
Amount of items: 2
Items: 
Size: 510646 Color: 1
Size: 489304 Color: 0

Bin 1820: 51 of cap free
Amount of items: 2
Items: 
Size: 512979 Color: 1
Size: 486971 Color: 0

Bin 1821: 51 of cap free
Amount of items: 2
Items: 
Size: 514903 Color: 1
Size: 485047 Color: 0

Bin 1822: 51 of cap free
Amount of items: 2
Items: 
Size: 523339 Color: 0
Size: 476611 Color: 1

Bin 1823: 51 of cap free
Amount of items: 2
Items: 
Size: 533930 Color: 1
Size: 466020 Color: 0

Bin 1824: 51 of cap free
Amount of items: 2
Items: 
Size: 542190 Color: 0
Size: 457760 Color: 1

Bin 1825: 51 of cap free
Amount of items: 2
Items: 
Size: 542798 Color: 1
Size: 457152 Color: 0

Bin 1826: 51 of cap free
Amount of items: 2
Items: 
Size: 544376 Color: 1
Size: 455574 Color: 0

Bin 1827: 51 of cap free
Amount of items: 2
Items: 
Size: 550259 Color: 1
Size: 449691 Color: 0

Bin 1828: 51 of cap free
Amount of items: 2
Items: 
Size: 550082 Color: 0
Size: 449868 Color: 1

Bin 1829: 51 of cap free
Amount of items: 2
Items: 
Size: 557353 Color: 0
Size: 442597 Color: 1

Bin 1830: 51 of cap free
Amount of items: 2
Items: 
Size: 570962 Color: 1
Size: 428988 Color: 0

Bin 1831: 51 of cap free
Amount of items: 2
Items: 
Size: 632032 Color: 0
Size: 367918 Color: 1

Bin 1832: 51 of cap free
Amount of items: 2
Items: 
Size: 735577 Color: 0
Size: 264373 Color: 1

Bin 1833: 51 of cap free
Amount of items: 2
Items: 
Size: 750602 Color: 0
Size: 249348 Color: 1

Bin 1834: 51 of cap free
Amount of items: 2
Items: 
Size: 794552 Color: 1
Size: 205398 Color: 0

Bin 1835: 51 of cap free
Amount of items: 6
Items: 
Size: 198076 Color: 1
Size: 179160 Color: 0
Size: 162265 Color: 1
Size: 162108 Color: 1
Size: 158570 Color: 0
Size: 139771 Color: 0

Bin 1836: 52 of cap free
Amount of items: 2
Items: 
Size: 528937 Color: 0
Size: 471012 Color: 1

Bin 1837: 52 of cap free
Amount of items: 2
Items: 
Size: 530812 Color: 1
Size: 469137 Color: 0

Bin 1838: 52 of cap free
Amount of items: 2
Items: 
Size: 537250 Color: 1
Size: 462699 Color: 0

Bin 1839: 52 of cap free
Amount of items: 2
Items: 
Size: 548947 Color: 1
Size: 451002 Color: 0

Bin 1840: 52 of cap free
Amount of items: 2
Items: 
Size: 566005 Color: 1
Size: 433944 Color: 0

Bin 1841: 52 of cap free
Amount of items: 2
Items: 
Size: 660781 Color: 0
Size: 339168 Color: 1

Bin 1842: 52 of cap free
Amount of items: 2
Items: 
Size: 686377 Color: 1
Size: 313572 Color: 0

Bin 1843: 52 of cap free
Amount of items: 2
Items: 
Size: 762497 Color: 1
Size: 237452 Color: 0

Bin 1844: 52 of cap free
Amount of items: 2
Items: 
Size: 763108 Color: 0
Size: 236841 Color: 1

Bin 1845: 52 of cap free
Amount of items: 2
Items: 
Size: 770547 Color: 1
Size: 229402 Color: 0

Bin 1846: 52 of cap free
Amount of items: 2
Items: 
Size: 798699 Color: 0
Size: 201250 Color: 1

Bin 1847: 53 of cap free
Amount of items: 2
Items: 
Size: 506301 Color: 1
Size: 493647 Color: 0

Bin 1848: 53 of cap free
Amount of items: 2
Items: 
Size: 529096 Color: 0
Size: 470852 Color: 1

Bin 1849: 53 of cap free
Amount of items: 2
Items: 
Size: 555991 Color: 1
Size: 443957 Color: 0

Bin 1850: 53 of cap free
Amount of items: 2
Items: 
Size: 556979 Color: 0
Size: 442969 Color: 1

Bin 1851: 53 of cap free
Amount of items: 2
Items: 
Size: 581595 Color: 1
Size: 418353 Color: 0

Bin 1852: 53 of cap free
Amount of items: 2
Items: 
Size: 607832 Color: 1
Size: 392116 Color: 0

Bin 1853: 53 of cap free
Amount of items: 2
Items: 
Size: 620894 Color: 0
Size: 379054 Color: 1

Bin 1854: 53 of cap free
Amount of items: 2
Items: 
Size: 714562 Color: 0
Size: 285386 Color: 1

Bin 1855: 53 of cap free
Amount of items: 2
Items: 
Size: 736745 Color: 0
Size: 263203 Color: 1

Bin 1856: 53 of cap free
Amount of items: 2
Items: 
Size: 761921 Color: 1
Size: 238027 Color: 0

Bin 1857: 53 of cap free
Amount of items: 2
Items: 
Size: 765251 Color: 1
Size: 234697 Color: 0

Bin 1858: 53 of cap free
Amount of items: 2
Items: 
Size: 775028 Color: 0
Size: 224920 Color: 1

Bin 1859: 53 of cap free
Amount of items: 2
Items: 
Size: 782170 Color: 1
Size: 217778 Color: 0

Bin 1860: 53 of cap free
Amount of items: 2
Items: 
Size: 661713 Color: 1
Size: 338235 Color: 0

Bin 1861: 54 of cap free
Amount of items: 2
Items: 
Size: 508201 Color: 1
Size: 491746 Color: 0

Bin 1862: 54 of cap free
Amount of items: 2
Items: 
Size: 508679 Color: 0
Size: 491268 Color: 1

Bin 1863: 54 of cap free
Amount of items: 2
Items: 
Size: 510338 Color: 0
Size: 489609 Color: 1

Bin 1864: 54 of cap free
Amount of items: 2
Items: 
Size: 515519 Color: 1
Size: 484428 Color: 0

Bin 1865: 54 of cap free
Amount of items: 2
Items: 
Size: 527687 Color: 1
Size: 472260 Color: 0

Bin 1866: 54 of cap free
Amount of items: 2
Items: 
Size: 531614 Color: 0
Size: 468333 Color: 1

Bin 1867: 54 of cap free
Amount of items: 2
Items: 
Size: 540805 Color: 0
Size: 459142 Color: 1

Bin 1868: 54 of cap free
Amount of items: 2
Items: 
Size: 545177 Color: 1
Size: 454770 Color: 0

Bin 1869: 54 of cap free
Amount of items: 2
Items: 
Size: 580453 Color: 1
Size: 419494 Color: 0

Bin 1870: 54 of cap free
Amount of items: 2
Items: 
Size: 580949 Color: 1
Size: 418998 Color: 0

Bin 1871: 54 of cap free
Amount of items: 2
Items: 
Size: 604767 Color: 0
Size: 395180 Color: 1

Bin 1872: 54 of cap free
Amount of items: 2
Items: 
Size: 614185 Color: 1
Size: 385762 Color: 0

Bin 1873: 54 of cap free
Amount of items: 2
Items: 
Size: 650963 Color: 1
Size: 348984 Color: 0

Bin 1874: 54 of cap free
Amount of items: 2
Items: 
Size: 657693 Color: 1
Size: 342254 Color: 0

Bin 1875: 54 of cap free
Amount of items: 2
Items: 
Size: 680967 Color: 1
Size: 318980 Color: 0

Bin 1876: 54 of cap free
Amount of items: 2
Items: 
Size: 728214 Color: 0
Size: 271733 Color: 1

Bin 1877: 54 of cap free
Amount of items: 2
Items: 
Size: 729740 Color: 1
Size: 270207 Color: 0

Bin 1878: 54 of cap free
Amount of items: 2
Items: 
Size: 742412 Color: 0
Size: 257535 Color: 1

Bin 1879: 54 of cap free
Amount of items: 2
Items: 
Size: 756895 Color: 1
Size: 243052 Color: 0

Bin 1880: 55 of cap free
Amount of items: 2
Items: 
Size: 517635 Color: 1
Size: 482311 Color: 0

Bin 1881: 55 of cap free
Amount of items: 2
Items: 
Size: 538180 Color: 1
Size: 461766 Color: 0

Bin 1882: 55 of cap free
Amount of items: 2
Items: 
Size: 555372 Color: 1
Size: 444574 Color: 0

Bin 1883: 55 of cap free
Amount of items: 2
Items: 
Size: 581335 Color: 1
Size: 418611 Color: 0

Bin 1884: 55 of cap free
Amount of items: 2
Items: 
Size: 608167 Color: 0
Size: 391779 Color: 1

Bin 1885: 55 of cap free
Amount of items: 2
Items: 
Size: 668847 Color: 0
Size: 331099 Color: 1

Bin 1886: 55 of cap free
Amount of items: 2
Items: 
Size: 671320 Color: 0
Size: 328626 Color: 1

Bin 1887: 55 of cap free
Amount of items: 2
Items: 
Size: 686581 Color: 1
Size: 313365 Color: 0

Bin 1888: 55 of cap free
Amount of items: 2
Items: 
Size: 712302 Color: 1
Size: 287644 Color: 0

Bin 1889: 56 of cap free
Amount of items: 2
Items: 
Size: 512738 Color: 1
Size: 487207 Color: 0

Bin 1890: 56 of cap free
Amount of items: 2
Items: 
Size: 537558 Color: 0
Size: 462387 Color: 1

Bin 1891: 56 of cap free
Amount of items: 2
Items: 
Size: 543292 Color: 1
Size: 456653 Color: 0

Bin 1892: 56 of cap free
Amount of items: 2
Items: 
Size: 565375 Color: 0
Size: 434570 Color: 1

Bin 1893: 56 of cap free
Amount of items: 2
Items: 
Size: 608397 Color: 1
Size: 391548 Color: 0

Bin 1894: 56 of cap free
Amount of items: 2
Items: 
Size: 658592 Color: 0
Size: 341353 Color: 1

Bin 1895: 56 of cap free
Amount of items: 2
Items: 
Size: 688780 Color: 0
Size: 311165 Color: 1

Bin 1896: 56 of cap free
Amount of items: 2
Items: 
Size: 725955 Color: 1
Size: 273990 Color: 0

Bin 1897: 56 of cap free
Amount of items: 2
Items: 
Size: 744449 Color: 0
Size: 255496 Color: 1

Bin 1898: 56 of cap free
Amount of items: 2
Items: 
Size: 795936 Color: 0
Size: 204009 Color: 1

Bin 1899: 57 of cap free
Amount of items: 3
Items: 
Size: 413970 Color: 0
Size: 404996 Color: 1
Size: 180978 Color: 1

Bin 1900: 57 of cap free
Amount of items: 2
Items: 
Size: 532699 Color: 0
Size: 467245 Color: 1

Bin 1901: 57 of cap free
Amount of items: 2
Items: 
Size: 549632 Color: 1
Size: 450312 Color: 0

Bin 1902: 57 of cap free
Amount of items: 2
Items: 
Size: 558794 Color: 1
Size: 441150 Color: 0

Bin 1903: 57 of cap free
Amount of items: 2
Items: 
Size: 588955 Color: 1
Size: 410989 Color: 0

Bin 1904: 57 of cap free
Amount of items: 2
Items: 
Size: 590822 Color: 0
Size: 409122 Color: 1

Bin 1905: 57 of cap free
Amount of items: 2
Items: 
Size: 597884 Color: 1
Size: 402060 Color: 0

Bin 1906: 57 of cap free
Amount of items: 2
Items: 
Size: 611872 Color: 0
Size: 388072 Color: 1

Bin 1907: 57 of cap free
Amount of items: 2
Items: 
Size: 632342 Color: 0
Size: 367602 Color: 1

Bin 1908: 57 of cap free
Amount of items: 2
Items: 
Size: 634933 Color: 1
Size: 365011 Color: 0

Bin 1909: 57 of cap free
Amount of items: 2
Items: 
Size: 647826 Color: 0
Size: 352118 Color: 1

Bin 1910: 57 of cap free
Amount of items: 2
Items: 
Size: 664963 Color: 0
Size: 334981 Color: 1

Bin 1911: 57 of cap free
Amount of items: 2
Items: 
Size: 671467 Color: 0
Size: 328477 Color: 1

Bin 1912: 57 of cap free
Amount of items: 2
Items: 
Size: 705086 Color: 0
Size: 294858 Color: 1

Bin 1913: 57 of cap free
Amount of items: 2
Items: 
Size: 732211 Color: 0
Size: 267733 Color: 1

Bin 1914: 57 of cap free
Amount of items: 2
Items: 
Size: 747937 Color: 1
Size: 252007 Color: 0

Bin 1915: 57 of cap free
Amount of items: 2
Items: 
Size: 754567 Color: 1
Size: 245377 Color: 0

Bin 1916: 57 of cap free
Amount of items: 2
Items: 
Size: 765891 Color: 1
Size: 234053 Color: 0

Bin 1917: 57 of cap free
Amount of items: 2
Items: 
Size: 771862 Color: 1
Size: 228082 Color: 0

Bin 1918: 57 of cap free
Amount of items: 2
Items: 
Size: 772204 Color: 0
Size: 227740 Color: 1

Bin 1919: 58 of cap free
Amount of items: 2
Items: 
Size: 511939 Color: 0
Size: 488004 Color: 1

Bin 1920: 58 of cap free
Amount of items: 2
Items: 
Size: 529513 Color: 0
Size: 470430 Color: 1

Bin 1921: 58 of cap free
Amount of items: 2
Items: 
Size: 536769 Color: 0
Size: 463174 Color: 1

Bin 1922: 58 of cap free
Amount of items: 2
Items: 
Size: 615272 Color: 1
Size: 384671 Color: 0

Bin 1923: 58 of cap free
Amount of items: 2
Items: 
Size: 628657 Color: 1
Size: 371286 Color: 0

Bin 1924: 58 of cap free
Amount of items: 2
Items: 
Size: 640985 Color: 0
Size: 358958 Color: 1

Bin 1925: 58 of cap free
Amount of items: 2
Items: 
Size: 659666 Color: 1
Size: 340277 Color: 0

Bin 1926: 58 of cap free
Amount of items: 2
Items: 
Size: 673142 Color: 1
Size: 326801 Color: 0

Bin 1927: 58 of cap free
Amount of items: 2
Items: 
Size: 695821 Color: 0
Size: 304122 Color: 1

Bin 1928: 58 of cap free
Amount of items: 2
Items: 
Size: 699259 Color: 1
Size: 300684 Color: 0

Bin 1929: 58 of cap free
Amount of items: 2
Items: 
Size: 709824 Color: 1
Size: 290119 Color: 0

Bin 1930: 58 of cap free
Amount of items: 2
Items: 
Size: 721668 Color: 0
Size: 278275 Color: 1

Bin 1931: 58 of cap free
Amount of items: 2
Items: 
Size: 737569 Color: 0
Size: 262374 Color: 1

Bin 1932: 58 of cap free
Amount of items: 2
Items: 
Size: 738630 Color: 0
Size: 261313 Color: 1

Bin 1933: 58 of cap free
Amount of items: 2
Items: 
Size: 770840 Color: 0
Size: 229103 Color: 1

Bin 1934: 58 of cap free
Amount of items: 6
Items: 
Size: 187040 Color: 0
Size: 176561 Color: 1
Size: 164089 Color: 0
Size: 164086 Color: 0
Size: 163293 Color: 1
Size: 144874 Color: 1

Bin 1935: 59 of cap free
Amount of items: 2
Items: 
Size: 521226 Color: 1
Size: 478716 Color: 0

Bin 1936: 59 of cap free
Amount of items: 2
Items: 
Size: 530327 Color: 1
Size: 469615 Color: 0

Bin 1937: 59 of cap free
Amount of items: 2
Items: 
Size: 590250 Color: 0
Size: 409692 Color: 1

Bin 1938: 59 of cap free
Amount of items: 2
Items: 
Size: 602357 Color: 1
Size: 397585 Color: 0

Bin 1939: 59 of cap free
Amount of items: 2
Items: 
Size: 609885 Color: 1
Size: 390057 Color: 0

Bin 1940: 59 of cap free
Amount of items: 2
Items: 
Size: 619863 Color: 1
Size: 380079 Color: 0

Bin 1941: 59 of cap free
Amount of items: 2
Items: 
Size: 642938 Color: 0
Size: 357004 Color: 1

Bin 1942: 59 of cap free
Amount of items: 2
Items: 
Size: 660647 Color: 0
Size: 339295 Color: 1

Bin 1943: 59 of cap free
Amount of items: 2
Items: 
Size: 739522 Color: 0
Size: 260420 Color: 1

Bin 1944: 59 of cap free
Amount of items: 2
Items: 
Size: 765360 Color: 1
Size: 234582 Color: 0

Bin 1945: 59 of cap free
Amount of items: 2
Items: 
Size: 766608 Color: 0
Size: 233334 Color: 1

Bin 1946: 59 of cap free
Amount of items: 2
Items: 
Size: 778345 Color: 0
Size: 221597 Color: 1

Bin 1947: 59 of cap free
Amount of items: 3
Items: 
Size: 347843 Color: 1
Size: 336581 Color: 0
Size: 315518 Color: 1

Bin 1948: 59 of cap free
Amount of items: 5
Items: 
Size: 240225 Color: 1
Size: 190538 Color: 0
Size: 190477 Color: 0
Size: 189457 Color: 1
Size: 189245 Color: 1

Bin 1949: 60 of cap free
Amount of items: 2
Items: 
Size: 504297 Color: 1
Size: 495644 Color: 0

Bin 1950: 60 of cap free
Amount of items: 2
Items: 
Size: 504820 Color: 0
Size: 495121 Color: 1

Bin 1951: 60 of cap free
Amount of items: 2
Items: 
Size: 537080 Color: 1
Size: 462861 Color: 0

Bin 1952: 60 of cap free
Amount of items: 2
Items: 
Size: 598555 Color: 1
Size: 401386 Color: 0

Bin 1953: 60 of cap free
Amount of items: 2
Items: 
Size: 627455 Color: 0
Size: 372486 Color: 1

Bin 1954: 60 of cap free
Amount of items: 2
Items: 
Size: 635142 Color: 0
Size: 364799 Color: 1

Bin 1955: 60 of cap free
Amount of items: 2
Items: 
Size: 642850 Color: 1
Size: 357091 Color: 0

Bin 1956: 60 of cap free
Amount of items: 2
Items: 
Size: 660720 Color: 1
Size: 339221 Color: 0

Bin 1957: 60 of cap free
Amount of items: 2
Items: 
Size: 700629 Color: 0
Size: 299312 Color: 1

Bin 1958: 60 of cap free
Amount of items: 2
Items: 
Size: 708397 Color: 1
Size: 291544 Color: 0

Bin 1959: 60 of cap free
Amount of items: 2
Items: 
Size: 751506 Color: 1
Size: 248435 Color: 0

Bin 1960: 60 of cap free
Amount of items: 2
Items: 
Size: 793032 Color: 1
Size: 206909 Color: 0

Bin 1961: 60 of cap free
Amount of items: 2
Items: 
Size: 796882 Color: 0
Size: 203059 Color: 1

Bin 1962: 60 of cap free
Amount of items: 5
Items: 
Size: 254377 Color: 0
Size: 187229 Color: 0
Size: 186455 Color: 1
Size: 186007 Color: 1
Size: 185873 Color: 1

Bin 1963: 61 of cap free
Amount of items: 2
Items: 
Size: 504708 Color: 1
Size: 495232 Color: 0

Bin 1964: 61 of cap free
Amount of items: 2
Items: 
Size: 553102 Color: 0
Size: 446838 Color: 1

Bin 1965: 61 of cap free
Amount of items: 2
Items: 
Size: 611392 Color: 0
Size: 388548 Color: 1

Bin 1966: 61 of cap free
Amount of items: 2
Items: 
Size: 637149 Color: 1
Size: 362791 Color: 0

Bin 1967: 61 of cap free
Amount of items: 2
Items: 
Size: 648868 Color: 1
Size: 351072 Color: 0

Bin 1968: 61 of cap free
Amount of items: 2
Items: 
Size: 651942 Color: 0
Size: 347998 Color: 1

Bin 1969: 61 of cap free
Amount of items: 2
Items: 
Size: 682508 Color: 1
Size: 317432 Color: 0

Bin 1970: 61 of cap free
Amount of items: 2
Items: 
Size: 686630 Color: 0
Size: 313310 Color: 1

Bin 1971: 61 of cap free
Amount of items: 2
Items: 
Size: 702770 Color: 1
Size: 297170 Color: 0

Bin 1972: 61 of cap free
Amount of items: 2
Items: 
Size: 712980 Color: 0
Size: 286960 Color: 1

Bin 1973: 61 of cap free
Amount of items: 2
Items: 
Size: 713331 Color: 0
Size: 286609 Color: 1

Bin 1974: 61 of cap free
Amount of items: 2
Items: 
Size: 722364 Color: 1
Size: 277576 Color: 0

Bin 1975: 61 of cap free
Amount of items: 2
Items: 
Size: 731234 Color: 0
Size: 268706 Color: 1

Bin 1976: 61 of cap free
Amount of items: 2
Items: 
Size: 731562 Color: 0
Size: 268378 Color: 1

Bin 1977: 61 of cap free
Amount of items: 2
Items: 
Size: 755596 Color: 0
Size: 244344 Color: 1

Bin 1978: 61 of cap free
Amount of items: 2
Items: 
Size: 792199 Color: 0
Size: 207741 Color: 1

Bin 1979: 62 of cap free
Amount of items: 2
Items: 
Size: 687279 Color: 1
Size: 312660 Color: 0

Bin 1980: 62 of cap free
Amount of items: 2
Items: 
Size: 504429 Color: 0
Size: 495510 Color: 1

Bin 1981: 62 of cap free
Amount of items: 2
Items: 
Size: 506174 Color: 0
Size: 493765 Color: 1

Bin 1982: 62 of cap free
Amount of items: 2
Items: 
Size: 526191 Color: 0
Size: 473748 Color: 1

Bin 1983: 62 of cap free
Amount of items: 2
Items: 
Size: 528673 Color: 0
Size: 471266 Color: 1

Bin 1984: 62 of cap free
Amount of items: 2
Items: 
Size: 542931 Color: 0
Size: 457008 Color: 1

Bin 1985: 62 of cap free
Amount of items: 2
Items: 
Size: 565452 Color: 0
Size: 434487 Color: 1

Bin 1986: 62 of cap free
Amount of items: 2
Items: 
Size: 568775 Color: 1
Size: 431164 Color: 0

Bin 1987: 62 of cap free
Amount of items: 2
Items: 
Size: 595575 Color: 1
Size: 404364 Color: 0

Bin 1988: 62 of cap free
Amount of items: 2
Items: 
Size: 623958 Color: 1
Size: 375981 Color: 0

Bin 1989: 62 of cap free
Amount of items: 2
Items: 
Size: 624375 Color: 1
Size: 375564 Color: 0

Bin 1990: 62 of cap free
Amount of items: 2
Items: 
Size: 626053 Color: 0
Size: 373886 Color: 1

Bin 1991: 62 of cap free
Amount of items: 2
Items: 
Size: 652801 Color: 0
Size: 347138 Color: 1

Bin 1992: 62 of cap free
Amount of items: 2
Items: 
Size: 711595 Color: 0
Size: 288344 Color: 1

Bin 1993: 62 of cap free
Amount of items: 2
Items: 
Size: 740789 Color: 0
Size: 259150 Color: 1

Bin 1994: 62 of cap free
Amount of items: 2
Items: 
Size: 758395 Color: 0
Size: 241544 Color: 1

Bin 1995: 62 of cap free
Amount of items: 2
Items: 
Size: 770177 Color: 0
Size: 229762 Color: 1

Bin 1996: 62 of cap free
Amount of items: 2
Items: 
Size: 783513 Color: 1
Size: 216426 Color: 0

Bin 1997: 62 of cap free
Amount of items: 2
Items: 
Size: 784732 Color: 0
Size: 215207 Color: 1

Bin 1998: 62 of cap free
Amount of items: 2
Items: 
Size: 785673 Color: 1
Size: 214266 Color: 0

Bin 1999: 62 of cap free
Amount of items: 6
Items: 
Size: 172679 Color: 0
Size: 172604 Color: 0
Size: 172430 Color: 0
Size: 168398 Color: 1
Size: 168322 Color: 1
Size: 145506 Color: 1

Bin 2000: 63 of cap free
Amount of items: 2
Items: 
Size: 515879 Color: 0
Size: 484059 Color: 1

Bin 2001: 63 of cap free
Amount of items: 2
Items: 
Size: 542252 Color: 0
Size: 457686 Color: 1

Bin 2002: 63 of cap free
Amount of items: 2
Items: 
Size: 556330 Color: 1
Size: 443608 Color: 0

Bin 2003: 63 of cap free
Amount of items: 2
Items: 
Size: 578769 Color: 1
Size: 421169 Color: 0

Bin 2004: 63 of cap free
Amount of items: 2
Items: 
Size: 646480 Color: 0
Size: 353458 Color: 1

Bin 2005: 63 of cap free
Amount of items: 2
Items: 
Size: 646754 Color: 1
Size: 353184 Color: 0

Bin 2006: 63 of cap free
Amount of items: 2
Items: 
Size: 699237 Color: 0
Size: 300701 Color: 1

Bin 2007: 63 of cap free
Amount of items: 2
Items: 
Size: 700948 Color: 1
Size: 298990 Color: 0

Bin 2008: 63 of cap free
Amount of items: 2
Items: 
Size: 751837 Color: 0
Size: 248101 Color: 1

Bin 2009: 63 of cap free
Amount of items: 2
Items: 
Size: 780708 Color: 1
Size: 219230 Color: 0

Bin 2010: 64 of cap free
Amount of items: 2
Items: 
Size: 735060 Color: 1
Size: 264877 Color: 0

Bin 2011: 64 of cap free
Amount of items: 2
Items: 
Size: 542498 Color: 0
Size: 457439 Color: 1

Bin 2012: 64 of cap free
Amount of items: 2
Items: 
Size: 546231 Color: 0
Size: 453706 Color: 1

Bin 2013: 64 of cap free
Amount of items: 2
Items: 
Size: 592920 Color: 0
Size: 407017 Color: 1

Bin 2014: 64 of cap free
Amount of items: 2
Items: 
Size: 636218 Color: 1
Size: 363719 Color: 0

Bin 2015: 64 of cap free
Amount of items: 2
Items: 
Size: 651675 Color: 1
Size: 348262 Color: 0

Bin 2016: 64 of cap free
Amount of items: 2
Items: 
Size: 652297 Color: 1
Size: 347640 Color: 0

Bin 2017: 64 of cap free
Amount of items: 2
Items: 
Size: 662901 Color: 1
Size: 337036 Color: 0

Bin 2018: 64 of cap free
Amount of items: 2
Items: 
Size: 671092 Color: 0
Size: 328845 Color: 1

Bin 2019: 64 of cap free
Amount of items: 2
Items: 
Size: 705401 Color: 0
Size: 294536 Color: 1

Bin 2020: 64 of cap free
Amount of items: 2
Items: 
Size: 767530 Color: 0
Size: 232407 Color: 1

Bin 2021: 64 of cap free
Amount of items: 3
Items: 
Size: 392267 Color: 0
Size: 304924 Color: 1
Size: 302746 Color: 0

Bin 2022: 65 of cap free
Amount of items: 2
Items: 
Size: 520265 Color: 1
Size: 479671 Color: 0

Bin 2023: 65 of cap free
Amount of items: 2
Items: 
Size: 554721 Color: 1
Size: 445215 Color: 0

Bin 2024: 65 of cap free
Amount of items: 2
Items: 
Size: 585046 Color: 1
Size: 414890 Color: 0

Bin 2025: 65 of cap free
Amount of items: 2
Items: 
Size: 617417 Color: 0
Size: 382519 Color: 1

Bin 2026: 65 of cap free
Amount of items: 2
Items: 
Size: 629836 Color: 1
Size: 370100 Color: 0

Bin 2027: 65 of cap free
Amount of items: 2
Items: 
Size: 653251 Color: 1
Size: 346685 Color: 0

Bin 2028: 65 of cap free
Amount of items: 2
Items: 
Size: 654272 Color: 0
Size: 345664 Color: 1

Bin 2029: 65 of cap free
Amount of items: 2
Items: 
Size: 664788 Color: 0
Size: 335148 Color: 1

Bin 2030: 65 of cap free
Amount of items: 2
Items: 
Size: 671202 Color: 1
Size: 328734 Color: 0

Bin 2031: 65 of cap free
Amount of items: 2
Items: 
Size: 741241 Color: 1
Size: 258695 Color: 0

Bin 2032: 65 of cap free
Amount of items: 2
Items: 
Size: 754191 Color: 1
Size: 245745 Color: 0

Bin 2033: 65 of cap free
Amount of items: 2
Items: 
Size: 775513 Color: 0
Size: 224423 Color: 1

Bin 2034: 65 of cap free
Amount of items: 2
Items: 
Size: 785536 Color: 0
Size: 214400 Color: 1

Bin 2035: 65 of cap free
Amount of items: 2
Items: 
Size: 790014 Color: 1
Size: 209922 Color: 0

Bin 2036: 66 of cap free
Amount of items: 2
Items: 
Size: 683341 Color: 1
Size: 316594 Color: 0

Bin 2037: 66 of cap free
Amount of items: 2
Items: 
Size: 523840 Color: 0
Size: 476095 Color: 1

Bin 2038: 66 of cap free
Amount of items: 2
Items: 
Size: 532479 Color: 0
Size: 467456 Color: 1

Bin 2039: 66 of cap free
Amount of items: 2
Items: 
Size: 545895 Color: 0
Size: 454040 Color: 1

Bin 2040: 66 of cap free
Amount of items: 2
Items: 
Size: 574652 Color: 1
Size: 425283 Color: 0

Bin 2041: 66 of cap free
Amount of items: 2
Items: 
Size: 596355 Color: 0
Size: 403580 Color: 1

Bin 2042: 66 of cap free
Amount of items: 2
Items: 
Size: 601880 Color: 1
Size: 398055 Color: 0

Bin 2043: 66 of cap free
Amount of items: 2
Items: 
Size: 615305 Color: 0
Size: 384630 Color: 1

Bin 2044: 66 of cap free
Amount of items: 2
Items: 
Size: 647019 Color: 1
Size: 352916 Color: 0

Bin 2045: 66 of cap free
Amount of items: 2
Items: 
Size: 686906 Color: 1
Size: 313029 Color: 0

Bin 2046: 66 of cap free
Amount of items: 2
Items: 
Size: 727844 Color: 1
Size: 272091 Color: 0

Bin 2047: 66 of cap free
Amount of items: 2
Items: 
Size: 781574 Color: 0
Size: 218361 Color: 1

Bin 2048: 66 of cap free
Amount of items: 2
Items: 
Size: 786825 Color: 1
Size: 213110 Color: 0

Bin 2049: 66 of cap free
Amount of items: 2
Items: 
Size: 774135 Color: 1
Size: 225800 Color: 0

Bin 2050: 67 of cap free
Amount of items: 2
Items: 
Size: 722159 Color: 0
Size: 277775 Color: 1

Bin 2051: 67 of cap free
Amount of items: 2
Items: 
Size: 519175 Color: 0
Size: 480759 Color: 1

Bin 2052: 67 of cap free
Amount of items: 2
Items: 
Size: 532738 Color: 1
Size: 467196 Color: 0

Bin 2053: 67 of cap free
Amount of items: 2
Items: 
Size: 574101 Color: 1
Size: 425833 Color: 0

Bin 2054: 67 of cap free
Amount of items: 2
Items: 
Size: 610137 Color: 0
Size: 389797 Color: 1

Bin 2055: 67 of cap free
Amount of items: 2
Items: 
Size: 634808 Color: 0
Size: 365126 Color: 1

Bin 2056: 67 of cap free
Amount of items: 2
Items: 
Size: 638014 Color: 0
Size: 361920 Color: 1

Bin 2057: 67 of cap free
Amount of items: 2
Items: 
Size: 654503 Color: 0
Size: 345431 Color: 1

Bin 2058: 67 of cap free
Amount of items: 2
Items: 
Size: 712408 Color: 0
Size: 287526 Color: 1

Bin 2059: 67 of cap free
Amount of items: 2
Items: 
Size: 715013 Color: 0
Size: 284921 Color: 1

Bin 2060: 67 of cap free
Amount of items: 2
Items: 
Size: 754411 Color: 1
Size: 245523 Color: 0

Bin 2061: 67 of cap free
Amount of items: 2
Items: 
Size: 770920 Color: 1
Size: 229014 Color: 0

Bin 2062: 67 of cap free
Amount of items: 2
Items: 
Size: 792774 Color: 1
Size: 207160 Color: 0

Bin 2063: 67 of cap free
Amount of items: 6
Items: 
Size: 279757 Color: 0
Size: 149295 Color: 1
Size: 148458 Color: 1
Size: 148098 Color: 1
Size: 140606 Color: 0
Size: 133720 Color: 0

Bin 2064: 67 of cap free
Amount of items: 3
Items: 
Size: 369300 Color: 1
Size: 315960 Color: 0
Size: 314674 Color: 1

Bin 2065: 68 of cap free
Amount of items: 2
Items: 
Size: 499978 Color: 1
Size: 499955 Color: 0

Bin 2066: 68 of cap free
Amount of items: 2
Items: 
Size: 564126 Color: 0
Size: 435807 Color: 1

Bin 2067: 68 of cap free
Amount of items: 2
Items: 
Size: 613474 Color: 1
Size: 386459 Color: 0

Bin 2068: 68 of cap free
Amount of items: 2
Items: 
Size: 656885 Color: 0
Size: 343048 Color: 1

Bin 2069: 68 of cap free
Amount of items: 2
Items: 
Size: 667705 Color: 0
Size: 332228 Color: 1

Bin 2070: 68 of cap free
Amount of items: 2
Items: 
Size: 684339 Color: 1
Size: 315594 Color: 0

Bin 2071: 68 of cap free
Amount of items: 2
Items: 
Size: 689684 Color: 1
Size: 310249 Color: 0

Bin 2072: 68 of cap free
Amount of items: 2
Items: 
Size: 717344 Color: 0
Size: 282589 Color: 1

Bin 2073: 68 of cap free
Amount of items: 2
Items: 
Size: 718460 Color: 0
Size: 281473 Color: 1

Bin 2074: 69 of cap free
Amount of items: 2
Items: 
Size: 525717 Color: 1
Size: 474215 Color: 0

Bin 2075: 69 of cap free
Amount of items: 2
Items: 
Size: 541958 Color: 0
Size: 457974 Color: 1

Bin 2076: 69 of cap free
Amount of items: 2
Items: 
Size: 558201 Color: 1
Size: 441731 Color: 0

Bin 2077: 69 of cap free
Amount of items: 2
Items: 
Size: 563381 Color: 0
Size: 436551 Color: 1

Bin 2078: 69 of cap free
Amount of items: 2
Items: 
Size: 587136 Color: 1
Size: 412796 Color: 0

Bin 2079: 69 of cap free
Amount of items: 2
Items: 
Size: 596071 Color: 1
Size: 403861 Color: 0

Bin 2080: 69 of cap free
Amount of items: 2
Items: 
Size: 613615 Color: 0
Size: 386317 Color: 1

Bin 2081: 69 of cap free
Amount of items: 2
Items: 
Size: 778814 Color: 0
Size: 221118 Color: 1

Bin 2082: 70 of cap free
Amount of items: 2
Items: 
Size: 526024 Color: 0
Size: 473907 Color: 1

Bin 2083: 70 of cap free
Amount of items: 2
Items: 
Size: 536333 Color: 0
Size: 463598 Color: 1

Bin 2084: 70 of cap free
Amount of items: 2
Items: 
Size: 548360 Color: 0
Size: 451571 Color: 1

Bin 2085: 70 of cap free
Amount of items: 2
Items: 
Size: 568139 Color: 0
Size: 431792 Color: 1

Bin 2086: 70 of cap free
Amount of items: 2
Items: 
Size: 599518 Color: 0
Size: 400413 Color: 1

Bin 2087: 70 of cap free
Amount of items: 2
Items: 
Size: 606043 Color: 1
Size: 393888 Color: 0

Bin 2088: 70 of cap free
Amount of items: 2
Items: 
Size: 647272 Color: 1
Size: 352659 Color: 0

Bin 2089: 70 of cap free
Amount of items: 2
Items: 
Size: 663878 Color: 1
Size: 336053 Color: 0

Bin 2090: 70 of cap free
Amount of items: 2
Items: 
Size: 701218 Color: 1
Size: 298713 Color: 0

Bin 2091: 70 of cap free
Amount of items: 2
Items: 
Size: 708022 Color: 0
Size: 291909 Color: 1

Bin 2092: 70 of cap free
Amount of items: 2
Items: 
Size: 715398 Color: 1
Size: 284533 Color: 0

Bin 2093: 70 of cap free
Amount of items: 2
Items: 
Size: 733387 Color: 0
Size: 266544 Color: 1

Bin 2094: 70 of cap free
Amount of items: 2
Items: 
Size: 753684 Color: 0
Size: 246247 Color: 1

Bin 2095: 71 of cap free
Amount of items: 2
Items: 
Size: 547072 Color: 0
Size: 452858 Color: 1

Bin 2096: 71 of cap free
Amount of items: 2
Items: 
Size: 608184 Color: 1
Size: 391746 Color: 0

Bin 2097: 71 of cap free
Amount of items: 2
Items: 
Size: 612406 Color: 0
Size: 387524 Color: 1

Bin 2098: 71 of cap free
Amount of items: 2
Items: 
Size: 671817 Color: 0
Size: 328113 Color: 1

Bin 2099: 71 of cap free
Amount of items: 2
Items: 
Size: 688594 Color: 0
Size: 311336 Color: 1

Bin 2100: 71 of cap free
Amount of items: 2
Items: 
Size: 757217 Color: 0
Size: 242713 Color: 1

Bin 2101: 72 of cap free
Amount of items: 2
Items: 
Size: 721748 Color: 0
Size: 278181 Color: 1

Bin 2102: 72 of cap free
Amount of items: 2
Items: 
Size: 552930 Color: 1
Size: 446999 Color: 0

Bin 2103: 72 of cap free
Amount of items: 2
Items: 
Size: 558424 Color: 1
Size: 441505 Color: 0

Bin 2104: 72 of cap free
Amount of items: 2
Items: 
Size: 581872 Color: 0
Size: 418057 Color: 1

Bin 2105: 72 of cap free
Amount of items: 2
Items: 
Size: 582690 Color: 0
Size: 417239 Color: 1

Bin 2106: 72 of cap free
Amount of items: 2
Items: 
Size: 612535 Color: 0
Size: 387394 Color: 1

Bin 2107: 72 of cap free
Amount of items: 2
Items: 
Size: 627229 Color: 1
Size: 372700 Color: 0

Bin 2108: 72 of cap free
Amount of items: 2
Items: 
Size: 641110 Color: 1
Size: 358819 Color: 0

Bin 2109: 72 of cap free
Amount of items: 2
Items: 
Size: 654547 Color: 1
Size: 345382 Color: 0

Bin 2110: 72 of cap free
Amount of items: 2
Items: 
Size: 719257 Color: 0
Size: 280672 Color: 1

Bin 2111: 72 of cap free
Amount of items: 2
Items: 
Size: 792770 Color: 1
Size: 207159 Color: 0

Bin 2112: 73 of cap free
Amount of items: 2
Items: 
Size: 605240 Color: 0
Size: 394688 Color: 1

Bin 2113: 73 of cap free
Amount of items: 2
Items: 
Size: 565852 Color: 0
Size: 434076 Color: 1

Bin 2114: 73 of cap free
Amount of items: 2
Items: 
Size: 624640 Color: 0
Size: 375288 Color: 1

Bin 2115: 73 of cap free
Amount of items: 2
Items: 
Size: 635939 Color: 1
Size: 363989 Color: 0

Bin 2116: 73 of cap free
Amount of items: 2
Items: 
Size: 673266 Color: 1
Size: 326662 Color: 0

Bin 2117: 73 of cap free
Amount of items: 2
Items: 
Size: 675169 Color: 0
Size: 324759 Color: 1

Bin 2118: 73 of cap free
Amount of items: 2
Items: 
Size: 780899 Color: 0
Size: 219029 Color: 1

Bin 2119: 73 of cap free
Amount of items: 2
Items: 
Size: 798906 Color: 0
Size: 201022 Color: 1

Bin 2120: 74 of cap free
Amount of items: 2
Items: 
Size: 541806 Color: 0
Size: 458121 Color: 1

Bin 2121: 74 of cap free
Amount of items: 2
Items: 
Size: 554175 Color: 0
Size: 445752 Color: 1

Bin 2122: 74 of cap free
Amount of items: 2
Items: 
Size: 588772 Color: 0
Size: 411155 Color: 1

Bin 2123: 74 of cap free
Amount of items: 2
Items: 
Size: 658143 Color: 0
Size: 341784 Color: 1

Bin 2124: 74 of cap free
Amount of items: 2
Items: 
Size: 659657 Color: 1
Size: 340270 Color: 0

Bin 2125: 74 of cap free
Amount of items: 2
Items: 
Size: 665273 Color: 0
Size: 334654 Color: 1

Bin 2126: 74 of cap free
Amount of items: 2
Items: 
Size: 691593 Color: 1
Size: 308334 Color: 0

Bin 2127: 74 of cap free
Amount of items: 2
Items: 
Size: 729685 Color: 0
Size: 270242 Color: 1

Bin 2128: 74 of cap free
Amount of items: 2
Items: 
Size: 735641 Color: 1
Size: 264286 Color: 0

Bin 2129: 74 of cap free
Amount of items: 2
Items: 
Size: 764814 Color: 1
Size: 235113 Color: 0

Bin 2130: 74 of cap free
Amount of items: 2
Items: 
Size: 774872 Color: 0
Size: 225055 Color: 1

Bin 2131: 74 of cap free
Amount of items: 5
Items: 
Size: 273802 Color: 1
Size: 182779 Color: 1
Size: 182712 Color: 0
Size: 182654 Color: 0
Size: 177980 Color: 0

Bin 2132: 75 of cap free
Amount of items: 2
Items: 
Size: 529591 Color: 1
Size: 470335 Color: 0

Bin 2133: 75 of cap free
Amount of items: 2
Items: 
Size: 547263 Color: 1
Size: 452663 Color: 0

Bin 2134: 75 of cap free
Amount of items: 2
Items: 
Size: 551800 Color: 0
Size: 448126 Color: 1

Bin 2135: 75 of cap free
Amount of items: 2
Items: 
Size: 572714 Color: 0
Size: 427212 Color: 1

Bin 2136: 75 of cap free
Amount of items: 2
Items: 
Size: 575020 Color: 0
Size: 424906 Color: 1

Bin 2137: 75 of cap free
Amount of items: 2
Items: 
Size: 576885 Color: 1
Size: 423041 Color: 0

Bin 2138: 75 of cap free
Amount of items: 2
Items: 
Size: 597215 Color: 1
Size: 402711 Color: 0

Bin 2139: 75 of cap free
Amount of items: 2
Items: 
Size: 641577 Color: 0
Size: 358349 Color: 1

Bin 2140: 75 of cap free
Amount of items: 2
Items: 
Size: 693266 Color: 1
Size: 306660 Color: 0

Bin 2141: 75 of cap free
Amount of items: 2
Items: 
Size: 703405 Color: 0
Size: 296521 Color: 1

Bin 2142: 75 of cap free
Amount of items: 2
Items: 
Size: 721977 Color: 1
Size: 277949 Color: 0

Bin 2143: 75 of cap free
Amount of items: 2
Items: 
Size: 733026 Color: 0
Size: 266900 Color: 1

Bin 2144: 75 of cap free
Amount of items: 2
Items: 
Size: 649729 Color: 0
Size: 350197 Color: 1

Bin 2145: 76 of cap free
Amount of items: 2
Items: 
Size: 504525 Color: 1
Size: 495400 Color: 0

Bin 2146: 76 of cap free
Amount of items: 2
Items: 
Size: 520089 Color: 0
Size: 479836 Color: 1

Bin 2147: 76 of cap free
Amount of items: 2
Items: 
Size: 630351 Color: 1
Size: 369574 Color: 0

Bin 2148: 76 of cap free
Amount of items: 2
Items: 
Size: 643753 Color: 1
Size: 356172 Color: 0

Bin 2149: 76 of cap free
Amount of items: 2
Items: 
Size: 655761 Color: 1
Size: 344164 Color: 0

Bin 2150: 76 of cap free
Amount of items: 2
Items: 
Size: 669321 Color: 1
Size: 330604 Color: 0

Bin 2151: 76 of cap free
Amount of items: 2
Items: 
Size: 682458 Color: 0
Size: 317467 Color: 1

Bin 2152: 76 of cap free
Amount of items: 2
Items: 
Size: 694750 Color: 0
Size: 305175 Color: 1

Bin 2153: 76 of cap free
Amount of items: 2
Items: 
Size: 696560 Color: 1
Size: 303365 Color: 0

Bin 2154: 76 of cap free
Amount of items: 2
Items: 
Size: 702139 Color: 1
Size: 297786 Color: 0

Bin 2155: 76 of cap free
Amount of items: 2
Items: 
Size: 707731 Color: 0
Size: 292194 Color: 1

Bin 2156: 76 of cap free
Amount of items: 2
Items: 
Size: 748306 Color: 0
Size: 251619 Color: 1

Bin 2157: 76 of cap free
Amount of items: 2
Items: 
Size: 754723 Color: 1
Size: 245202 Color: 0

Bin 2158: 76 of cap free
Amount of items: 2
Items: 
Size: 782492 Color: 0
Size: 217433 Color: 1

Bin 2159: 76 of cap free
Amount of items: 5
Items: 
Size: 220665 Color: 1
Size: 195520 Color: 0
Size: 195459 Color: 0
Size: 194615 Color: 1
Size: 193666 Color: 1

Bin 2160: 77 of cap free
Amount of items: 2
Items: 
Size: 543606 Color: 0
Size: 456318 Color: 1

Bin 2161: 77 of cap free
Amount of items: 2
Items: 
Size: 567189 Color: 1
Size: 432735 Color: 0

Bin 2162: 77 of cap free
Amount of items: 2
Items: 
Size: 570536 Color: 0
Size: 429388 Color: 1

Bin 2163: 77 of cap free
Amount of items: 2
Items: 
Size: 630818 Color: 1
Size: 369106 Color: 0

Bin 2164: 77 of cap free
Amount of items: 2
Items: 
Size: 650881 Color: 1
Size: 349043 Color: 0

Bin 2165: 77 of cap free
Amount of items: 2
Items: 
Size: 659131 Color: 0
Size: 340793 Color: 1

Bin 2166: 77 of cap free
Amount of items: 2
Items: 
Size: 679967 Color: 0
Size: 319957 Color: 1

Bin 2167: 77 of cap free
Amount of items: 2
Items: 
Size: 691073 Color: 0
Size: 308851 Color: 1

Bin 2168: 77 of cap free
Amount of items: 2
Items: 
Size: 764481 Color: 0
Size: 235443 Color: 1

Bin 2169: 78 of cap free
Amount of items: 2
Items: 
Size: 528619 Color: 1
Size: 471304 Color: 0

Bin 2170: 78 of cap free
Amount of items: 2
Items: 
Size: 544834 Color: 1
Size: 455089 Color: 0

Bin 2171: 78 of cap free
Amount of items: 2
Items: 
Size: 546681 Color: 1
Size: 453242 Color: 0

Bin 2172: 78 of cap free
Amount of items: 2
Items: 
Size: 625610 Color: 0
Size: 374313 Color: 1

Bin 2173: 78 of cap free
Amount of items: 2
Items: 
Size: 640599 Color: 0
Size: 359324 Color: 1

Bin 2174: 78 of cap free
Amount of items: 2
Items: 
Size: 648722 Color: 1
Size: 351201 Color: 0

Bin 2175: 78 of cap free
Amount of items: 2
Items: 
Size: 663039 Color: 0
Size: 336884 Color: 1

Bin 2176: 78 of cap free
Amount of items: 2
Items: 
Size: 674295 Color: 1
Size: 325628 Color: 0

Bin 2177: 78 of cap free
Amount of items: 2
Items: 
Size: 680771 Color: 1
Size: 319152 Color: 0

Bin 2178: 78 of cap free
Amount of items: 2
Items: 
Size: 712302 Color: 1
Size: 287621 Color: 0

Bin 2179: 78 of cap free
Amount of items: 2
Items: 
Size: 718297 Color: 0
Size: 281626 Color: 1

Bin 2180: 78 of cap free
Amount of items: 2
Items: 
Size: 724974 Color: 0
Size: 274949 Color: 1

Bin 2181: 78 of cap free
Amount of items: 2
Items: 
Size: 736854 Color: 0
Size: 263069 Color: 1

Bin 2182: 78 of cap free
Amount of items: 2
Items: 
Size: 782999 Color: 1
Size: 216924 Color: 0

Bin 2183: 79 of cap free
Amount of items: 2
Items: 
Size: 554667 Color: 0
Size: 445255 Color: 1

Bin 2184: 79 of cap free
Amount of items: 2
Items: 
Size: 555343 Color: 0
Size: 444579 Color: 1

Bin 2185: 79 of cap free
Amount of items: 2
Items: 
Size: 574047 Color: 0
Size: 425875 Color: 1

Bin 2186: 79 of cap free
Amount of items: 2
Items: 
Size: 613422 Color: 0
Size: 386500 Color: 1

Bin 2187: 79 of cap free
Amount of items: 2
Items: 
Size: 657344 Color: 1
Size: 342578 Color: 0

Bin 2188: 79 of cap free
Amount of items: 2
Items: 
Size: 676646 Color: 1
Size: 323276 Color: 0

Bin 2189: 79 of cap free
Amount of items: 2
Items: 
Size: 716826 Color: 1
Size: 283096 Color: 0

Bin 2190: 79 of cap free
Amount of items: 2
Items: 
Size: 725821 Color: 1
Size: 274101 Color: 0

Bin 2191: 79 of cap free
Amount of items: 2
Items: 
Size: 750002 Color: 1
Size: 249920 Color: 0

Bin 2192: 79 of cap free
Amount of items: 2
Items: 
Size: 761306 Color: 0
Size: 238616 Color: 1

Bin 2193: 79 of cap free
Amount of items: 2
Items: 
Size: 787561 Color: 0
Size: 212361 Color: 1

Bin 2194: 80 of cap free
Amount of items: 2
Items: 
Size: 504962 Color: 1
Size: 494959 Color: 0

Bin 2195: 80 of cap free
Amount of items: 2
Items: 
Size: 582821 Color: 1
Size: 417100 Color: 0

Bin 2196: 80 of cap free
Amount of items: 2
Items: 
Size: 603553 Color: 0
Size: 396368 Color: 1

Bin 2197: 80 of cap free
Amount of items: 2
Items: 
Size: 605601 Color: 1
Size: 394320 Color: 0

Bin 2198: 80 of cap free
Amount of items: 2
Items: 
Size: 649496 Color: 1
Size: 350425 Color: 0

Bin 2199: 80 of cap free
Amount of items: 2
Items: 
Size: 691838 Color: 0
Size: 308083 Color: 1

Bin 2200: 80 of cap free
Amount of items: 2
Items: 
Size: 709840 Color: 0
Size: 290081 Color: 1

Bin 2201: 80 of cap free
Amount of items: 2
Items: 
Size: 777914 Color: 0
Size: 222007 Color: 1

Bin 2202: 80 of cap free
Amount of items: 2
Items: 
Size: 799620 Color: 1
Size: 200301 Color: 0

Bin 2203: 81 of cap free
Amount of items: 2
Items: 
Size: 504622 Color: 1
Size: 495298 Color: 0

Bin 2204: 81 of cap free
Amount of items: 2
Items: 
Size: 533268 Color: 1
Size: 466652 Color: 0

Bin 2205: 81 of cap free
Amount of items: 2
Items: 
Size: 537447 Color: 0
Size: 462473 Color: 1

Bin 2206: 81 of cap free
Amount of items: 2
Items: 
Size: 577737 Color: 0
Size: 422183 Color: 1

Bin 2207: 81 of cap free
Amount of items: 2
Items: 
Size: 584012 Color: 0
Size: 415908 Color: 1

Bin 2208: 81 of cap free
Amount of items: 2
Items: 
Size: 600108 Color: 0
Size: 399812 Color: 1

Bin 2209: 81 of cap free
Amount of items: 2
Items: 
Size: 604567 Color: 1
Size: 395353 Color: 0

Bin 2210: 81 of cap free
Amount of items: 2
Items: 
Size: 617943 Color: 0
Size: 381977 Color: 1

Bin 2211: 81 of cap free
Amount of items: 2
Items: 
Size: 618961 Color: 0
Size: 380959 Color: 1

Bin 2212: 81 of cap free
Amount of items: 2
Items: 
Size: 688978 Color: 1
Size: 310942 Color: 0

Bin 2213: 81 of cap free
Amount of items: 2
Items: 
Size: 712214 Color: 0
Size: 287706 Color: 1

Bin 2214: 81 of cap free
Amount of items: 2
Items: 
Size: 746179 Color: 1
Size: 253741 Color: 0

Bin 2215: 81 of cap free
Amount of items: 2
Items: 
Size: 789639 Color: 1
Size: 210281 Color: 0

Bin 2216: 82 of cap free
Amount of items: 2
Items: 
Size: 783794 Color: 1
Size: 216125 Color: 0

Bin 2217: 82 of cap free
Amount of items: 2
Items: 
Size: 501807 Color: 1
Size: 498112 Color: 0

Bin 2218: 82 of cap free
Amount of items: 2
Items: 
Size: 568488 Color: 0
Size: 431431 Color: 1

Bin 2219: 82 of cap free
Amount of items: 2
Items: 
Size: 592419 Color: 1
Size: 407500 Color: 0

Bin 2220: 82 of cap free
Amount of items: 2
Items: 
Size: 642781 Color: 0
Size: 357138 Color: 1

Bin 2221: 82 of cap free
Amount of items: 2
Items: 
Size: 699153 Color: 0
Size: 300766 Color: 1

Bin 2222: 82 of cap free
Amount of items: 2
Items: 
Size: 720929 Color: 0
Size: 278990 Color: 1

Bin 2223: 82 of cap free
Amount of items: 2
Items: 
Size: 724229 Color: 0
Size: 275690 Color: 1

Bin 2224: 82 of cap free
Amount of items: 2
Items: 
Size: 743824 Color: 1
Size: 256095 Color: 0

Bin 2225: 82 of cap free
Amount of items: 2
Items: 
Size: 771956 Color: 0
Size: 227963 Color: 1

Bin 2226: 82 of cap free
Amount of items: 2
Items: 
Size: 793775 Color: 0
Size: 206144 Color: 1

Bin 2227: 83 of cap free
Amount of items: 2
Items: 
Size: 504091 Color: 1
Size: 495827 Color: 0

Bin 2228: 83 of cap free
Amount of items: 2
Items: 
Size: 528752 Color: 0
Size: 471166 Color: 1

Bin 2229: 83 of cap free
Amount of items: 2
Items: 
Size: 618580 Color: 0
Size: 381338 Color: 1

Bin 2230: 83 of cap free
Amount of items: 2
Items: 
Size: 676335 Color: 0
Size: 323583 Color: 1

Bin 2231: 83 of cap free
Amount of items: 2
Items: 
Size: 685760 Color: 1
Size: 314158 Color: 0

Bin 2232: 83 of cap free
Amount of items: 2
Items: 
Size: 686211 Color: 1
Size: 313707 Color: 0

Bin 2233: 83 of cap free
Amount of items: 2
Items: 
Size: 704084 Color: 1
Size: 295834 Color: 0

Bin 2234: 83 of cap free
Amount of items: 2
Items: 
Size: 716436 Color: 1
Size: 283482 Color: 0

Bin 2235: 83 of cap free
Amount of items: 2
Items: 
Size: 722928 Color: 1
Size: 276990 Color: 0

Bin 2236: 83 of cap free
Amount of items: 2
Items: 
Size: 739498 Color: 1
Size: 260420 Color: 0

Bin 2237: 83 of cap free
Amount of items: 2
Items: 
Size: 743355 Color: 0
Size: 256563 Color: 1

Bin 2238: 83 of cap free
Amount of items: 2
Items: 
Size: 756246 Color: 0
Size: 243672 Color: 1

Bin 2239: 83 of cap free
Amount of items: 2
Items: 
Size: 775759 Color: 1
Size: 224159 Color: 0

Bin 2240: 83 of cap free
Amount of items: 2
Items: 
Size: 781759 Color: 1
Size: 218159 Color: 0

Bin 2241: 83 of cap free
Amount of items: 2
Items: 
Size: 743466 Color: 0
Size: 256452 Color: 1

Bin 2242: 84 of cap free
Amount of items: 2
Items: 
Size: 502428 Color: 0
Size: 497489 Color: 1

Bin 2243: 84 of cap free
Amount of items: 2
Items: 
Size: 505736 Color: 0
Size: 494181 Color: 1

Bin 2244: 84 of cap free
Amount of items: 2
Items: 
Size: 517731 Color: 1
Size: 482186 Color: 0

Bin 2245: 84 of cap free
Amount of items: 2
Items: 
Size: 528109 Color: 1
Size: 471808 Color: 0

Bin 2246: 84 of cap free
Amount of items: 2
Items: 
Size: 542657 Color: 1
Size: 457260 Color: 0

Bin 2247: 84 of cap free
Amount of items: 2
Items: 
Size: 566340 Color: 0
Size: 433577 Color: 1

Bin 2248: 84 of cap free
Amount of items: 2
Items: 
Size: 583269 Color: 0
Size: 416648 Color: 1

Bin 2249: 84 of cap free
Amount of items: 2
Items: 
Size: 614669 Color: 0
Size: 385248 Color: 1

Bin 2250: 84 of cap free
Amount of items: 2
Items: 
Size: 665381 Color: 0
Size: 334536 Color: 1

Bin 2251: 84 of cap free
Amount of items: 2
Items: 
Size: 681926 Color: 1
Size: 317991 Color: 0

Bin 2252: 84 of cap free
Amount of items: 2
Items: 
Size: 738092 Color: 0
Size: 261825 Color: 1

Bin 2253: 84 of cap free
Amount of items: 2
Items: 
Size: 750690 Color: 1
Size: 249227 Color: 0

Bin 2254: 84 of cap free
Amount of items: 2
Items: 
Size: 791401 Color: 1
Size: 208516 Color: 0

Bin 2255: 85 of cap free
Amount of items: 2
Items: 
Size: 508259 Color: 0
Size: 491657 Color: 1

Bin 2256: 85 of cap free
Amount of items: 2
Items: 
Size: 550654 Color: 1
Size: 449262 Color: 0

Bin 2257: 85 of cap free
Amount of items: 2
Items: 
Size: 566609 Color: 0
Size: 433307 Color: 1

Bin 2258: 85 of cap free
Amount of items: 2
Items: 
Size: 735734 Color: 1
Size: 264182 Color: 0

Bin 2259: 85 of cap free
Amount of items: 2
Items: 
Size: 739969 Color: 0
Size: 259947 Color: 1

Bin 2260: 85 of cap free
Amount of items: 2
Items: 
Size: 760119 Color: 0
Size: 239797 Color: 1

Bin 2261: 86 of cap free
Amount of items: 2
Items: 
Size: 563253 Color: 1
Size: 436662 Color: 0

Bin 2262: 86 of cap free
Amount of items: 2
Items: 
Size: 566827 Color: 0
Size: 433088 Color: 1

Bin 2263: 86 of cap free
Amount of items: 2
Items: 
Size: 578187 Color: 1
Size: 421728 Color: 0

Bin 2264: 86 of cap free
Amount of items: 2
Items: 
Size: 637105 Color: 0
Size: 362810 Color: 1

Bin 2265: 86 of cap free
Amount of items: 2
Items: 
Size: 664570 Color: 0
Size: 335345 Color: 1

Bin 2266: 86 of cap free
Amount of items: 2
Items: 
Size: 743739 Color: 0
Size: 256176 Color: 1

Bin 2267: 86 of cap free
Amount of items: 2
Items: 
Size: 788642 Color: 1
Size: 211273 Color: 0

Bin 2268: 86 of cap free
Amount of items: 2
Items: 
Size: 788983 Color: 0
Size: 210932 Color: 1

Bin 2269: 86 of cap free
Amount of items: 2
Items: 
Size: 552252 Color: 0
Size: 447663 Color: 1

Bin 2270: 87 of cap free
Amount of items: 2
Items: 
Size: 751950 Color: 1
Size: 247964 Color: 0

Bin 2271: 87 of cap free
Amount of items: 2
Items: 
Size: 502094 Color: 0
Size: 497820 Color: 1

Bin 2272: 87 of cap free
Amount of items: 2
Items: 
Size: 562604 Color: 0
Size: 437310 Color: 1

Bin 2273: 87 of cap free
Amount of items: 2
Items: 
Size: 589566 Color: 1
Size: 410348 Color: 0

Bin 2274: 87 of cap free
Amount of items: 2
Items: 
Size: 625712 Color: 1
Size: 374202 Color: 0

Bin 2275: 87 of cap free
Amount of items: 2
Items: 
Size: 655650 Color: 0
Size: 344264 Color: 1

Bin 2276: 87 of cap free
Amount of items: 2
Items: 
Size: 693463 Color: 0
Size: 306451 Color: 1

Bin 2277: 87 of cap free
Amount of items: 2
Items: 
Size: 795565 Color: 1
Size: 204349 Color: 0

Bin 2278: 88 of cap free
Amount of items: 2
Items: 
Size: 503525 Color: 1
Size: 496388 Color: 0

Bin 2279: 88 of cap free
Amount of items: 2
Items: 
Size: 507168 Color: 0
Size: 492745 Color: 1

Bin 2280: 88 of cap free
Amount of items: 2
Items: 
Size: 692821 Color: 0
Size: 307092 Color: 1

Bin 2281: 88 of cap free
Amount of items: 2
Items: 
Size: 765033 Color: 1
Size: 234880 Color: 0

Bin 2282: 88 of cap free
Amount of items: 2
Items: 
Size: 768765 Color: 0
Size: 231148 Color: 1

Bin 2283: 89 of cap free
Amount of items: 2
Items: 
Size: 572322 Color: 0
Size: 427590 Color: 1

Bin 2284: 89 of cap free
Amount of items: 2
Items: 
Size: 587510 Color: 0
Size: 412402 Color: 1

Bin 2285: 89 of cap free
Amount of items: 2
Items: 
Size: 590551 Color: 1
Size: 409361 Color: 0

Bin 2286: 89 of cap free
Amount of items: 2
Items: 
Size: 596633 Color: 0
Size: 403279 Color: 1

Bin 2287: 89 of cap free
Amount of items: 2
Items: 
Size: 609445 Color: 0
Size: 390467 Color: 1

Bin 2288: 89 of cap free
Amount of items: 2
Items: 
Size: 626874 Color: 0
Size: 373038 Color: 1

Bin 2289: 89 of cap free
Amount of items: 2
Items: 
Size: 640225 Color: 1
Size: 359687 Color: 0

Bin 2290: 89 of cap free
Amount of items: 2
Items: 
Size: 659820 Color: 0
Size: 340092 Color: 1

Bin 2291: 89 of cap free
Amount of items: 2
Items: 
Size: 680539 Color: 1
Size: 319373 Color: 0

Bin 2292: 89 of cap free
Amount of items: 2
Items: 
Size: 707036 Color: 0
Size: 292876 Color: 1

Bin 2293: 89 of cap free
Amount of items: 2
Items: 
Size: 741624 Color: 0
Size: 258288 Color: 1

Bin 2294: 90 of cap free
Amount of items: 2
Items: 
Size: 598294 Color: 0
Size: 401617 Color: 1

Bin 2295: 90 of cap free
Amount of items: 2
Items: 
Size: 526761 Color: 1
Size: 473150 Color: 0

Bin 2296: 90 of cap free
Amount of items: 2
Items: 
Size: 537224 Color: 0
Size: 462687 Color: 1

Bin 2297: 90 of cap free
Amount of items: 2
Items: 
Size: 581278 Color: 0
Size: 418633 Color: 1

Bin 2298: 90 of cap free
Amount of items: 2
Items: 
Size: 603928 Color: 1
Size: 395983 Color: 0

Bin 2299: 90 of cap free
Amount of items: 2
Items: 
Size: 621620 Color: 1
Size: 378291 Color: 0

Bin 2300: 90 of cap free
Amount of items: 2
Items: 
Size: 650015 Color: 0
Size: 349896 Color: 1

Bin 2301: 90 of cap free
Amount of items: 2
Items: 
Size: 707937 Color: 1
Size: 291974 Color: 0

Bin 2302: 90 of cap free
Amount of items: 2
Items: 
Size: 733385 Color: 1
Size: 266526 Color: 0

Bin 2303: 90 of cap free
Amount of items: 2
Items: 
Size: 734547 Color: 1
Size: 265364 Color: 0

Bin 2304: 90 of cap free
Amount of items: 2
Items: 
Size: 752603 Color: 0
Size: 247308 Color: 1

Bin 2305: 90 of cap free
Amount of items: 2
Items: 
Size: 794614 Color: 1
Size: 205297 Color: 0

Bin 2306: 90 of cap free
Amount of items: 2
Items: 
Size: 799121 Color: 0
Size: 200790 Color: 1

Bin 2307: 91 of cap free
Amount of items: 2
Items: 
Size: 541153 Color: 1
Size: 458757 Color: 0

Bin 2308: 91 of cap free
Amount of items: 2
Items: 
Size: 578895 Color: 1
Size: 421015 Color: 0

Bin 2309: 91 of cap free
Amount of items: 2
Items: 
Size: 624237 Color: 0
Size: 375673 Color: 1

Bin 2310: 91 of cap free
Amount of items: 2
Items: 
Size: 628812 Color: 1
Size: 371098 Color: 0

Bin 2311: 91 of cap free
Amount of items: 2
Items: 
Size: 637148 Color: 1
Size: 362762 Color: 0

Bin 2312: 91 of cap free
Amount of items: 2
Items: 
Size: 675021 Color: 0
Size: 324889 Color: 1

Bin 2313: 91 of cap free
Amount of items: 2
Items: 
Size: 681630 Color: 1
Size: 318280 Color: 0

Bin 2314: 91 of cap free
Amount of items: 2
Items: 
Size: 704964 Color: 0
Size: 294946 Color: 1

Bin 2315: 92 of cap free
Amount of items: 2
Items: 
Size: 781217 Color: 0
Size: 218692 Color: 1

Bin 2316: 92 of cap free
Amount of items: 2
Items: 
Size: 795039 Color: 1
Size: 204870 Color: 0

Bin 2317: 92 of cap free
Amount of items: 2
Items: 
Size: 536454 Color: 0
Size: 463455 Color: 1

Bin 2318: 92 of cap free
Amount of items: 2
Items: 
Size: 569771 Color: 1
Size: 430138 Color: 0

Bin 2319: 92 of cap free
Amount of items: 2
Items: 
Size: 574478 Color: 1
Size: 425431 Color: 0

Bin 2320: 92 of cap free
Amount of items: 2
Items: 
Size: 612443 Color: 1
Size: 387466 Color: 0

Bin 2321: 92 of cap free
Amount of items: 2
Items: 
Size: 736251 Color: 1
Size: 263658 Color: 0

Bin 2322: 92 of cap free
Amount of items: 2
Items: 
Size: 766313 Color: 1
Size: 233596 Color: 0

Bin 2323: 93 of cap free
Amount of items: 2
Items: 
Size: 519910 Color: 0
Size: 479998 Color: 1

Bin 2324: 93 of cap free
Amount of items: 2
Items: 
Size: 540661 Color: 1
Size: 459247 Color: 0

Bin 2325: 93 of cap free
Amount of items: 2
Items: 
Size: 585835 Color: 0
Size: 414073 Color: 1

Bin 2326: 93 of cap free
Amount of items: 2
Items: 
Size: 657226 Color: 0
Size: 342682 Color: 1

Bin 2327: 93 of cap free
Amount of items: 2
Items: 
Size: 676895 Color: 1
Size: 323013 Color: 0

Bin 2328: 94 of cap free
Amount of items: 2
Items: 
Size: 697744 Color: 1
Size: 302163 Color: 0

Bin 2329: 94 of cap free
Amount of items: 2
Items: 
Size: 599085 Color: 0
Size: 400822 Color: 1

Bin 2330: 94 of cap free
Amount of items: 2
Items: 
Size: 635118 Color: 0
Size: 364789 Color: 1

Bin 2331: 94 of cap free
Amount of items: 2
Items: 
Size: 688439 Color: 1
Size: 311468 Color: 0

Bin 2332: 94 of cap free
Amount of items: 2
Items: 
Size: 694682 Color: 1
Size: 305225 Color: 0

Bin 2333: 94 of cap free
Amount of items: 2
Items: 
Size: 737771 Color: 0
Size: 262136 Color: 1

Bin 2334: 94 of cap free
Amount of items: 2
Items: 
Size: 749837 Color: 0
Size: 250070 Color: 1

Bin 2335: 94 of cap free
Amount of items: 2
Items: 
Size: 764312 Color: 0
Size: 235595 Color: 1

Bin 2336: 94 of cap free
Amount of items: 2
Items: 
Size: 775865 Color: 1
Size: 224042 Color: 0

Bin 2337: 95 of cap free
Amount of items: 2
Items: 
Size: 550549 Color: 1
Size: 449357 Color: 0

Bin 2338: 95 of cap free
Amount of items: 2
Items: 
Size: 579617 Color: 1
Size: 420289 Color: 0

Bin 2339: 95 of cap free
Amount of items: 2
Items: 
Size: 583926 Color: 1
Size: 415980 Color: 0

Bin 2340: 95 of cap free
Amount of items: 2
Items: 
Size: 770279 Color: 1
Size: 229627 Color: 0

Bin 2341: 96 of cap free
Amount of items: 2
Items: 
Size: 542904 Color: 0
Size: 457001 Color: 1

Bin 2342: 96 of cap free
Amount of items: 2
Items: 
Size: 615877 Color: 1
Size: 384028 Color: 0

Bin 2343: 96 of cap free
Amount of items: 2
Items: 
Size: 621886 Color: 1
Size: 378019 Color: 0

Bin 2344: 96 of cap free
Amount of items: 2
Items: 
Size: 701226 Color: 0
Size: 298679 Color: 1

Bin 2345: 96 of cap free
Amount of items: 2
Items: 
Size: 767873 Color: 0
Size: 232032 Color: 1

Bin 2346: 96 of cap free
Amount of items: 2
Items: 
Size: 770756 Color: 1
Size: 229149 Color: 0

Bin 2347: 97 of cap free
Amount of items: 2
Items: 
Size: 533968 Color: 0
Size: 465936 Color: 1

Bin 2348: 97 of cap free
Amount of items: 2
Items: 
Size: 580918 Color: 0
Size: 418986 Color: 1

Bin 2349: 97 of cap free
Amount of items: 2
Items: 
Size: 632925 Color: 0
Size: 366979 Color: 1

Bin 2350: 97 of cap free
Amount of items: 2
Items: 
Size: 722923 Color: 1
Size: 276981 Color: 0

Bin 2351: 98 of cap free
Amount of items: 2
Items: 
Size: 550781 Color: 1
Size: 449122 Color: 0

Bin 2352: 98 of cap free
Amount of items: 2
Items: 
Size: 593616 Color: 0
Size: 406287 Color: 1

Bin 2353: 98 of cap free
Amount of items: 2
Items: 
Size: 628554 Color: 0
Size: 371349 Color: 1

Bin 2354: 98 of cap free
Amount of items: 2
Items: 
Size: 635761 Color: 0
Size: 364142 Color: 1

Bin 2355: 98 of cap free
Amount of items: 2
Items: 
Size: 636421 Color: 0
Size: 363482 Color: 1

Bin 2356: 98 of cap free
Amount of items: 2
Items: 
Size: 639889 Color: 0
Size: 360014 Color: 1

Bin 2357: 98 of cap free
Amount of items: 2
Items: 
Size: 677481 Color: 1
Size: 322422 Color: 0

Bin 2358: 98 of cap free
Amount of items: 2
Items: 
Size: 698717 Color: 0
Size: 301186 Color: 1

Bin 2359: 98 of cap free
Amount of items: 2
Items: 
Size: 708379 Color: 0
Size: 291524 Color: 1

Bin 2360: 98 of cap free
Amount of items: 2
Items: 
Size: 712032 Color: 1
Size: 287871 Color: 0

Bin 2361: 98 of cap free
Amount of items: 2
Items: 
Size: 798094 Color: 0
Size: 201809 Color: 1

Bin 2362: 99 of cap free
Amount of items: 2
Items: 
Size: 504521 Color: 1
Size: 495381 Color: 0

Bin 2363: 99 of cap free
Amount of items: 2
Items: 
Size: 536058 Color: 1
Size: 463844 Color: 0

Bin 2364: 99 of cap free
Amount of items: 2
Items: 
Size: 569173 Color: 1
Size: 430729 Color: 0

Bin 2365: 99 of cap free
Amount of items: 2
Items: 
Size: 579520 Color: 0
Size: 420382 Color: 1

Bin 2366: 99 of cap free
Amount of items: 2
Items: 
Size: 591181 Color: 0
Size: 408721 Color: 1

Bin 2367: 99 of cap free
Amount of items: 2
Items: 
Size: 653253 Color: 0
Size: 346649 Color: 1

Bin 2368: 99 of cap free
Amount of items: 2
Items: 
Size: 682910 Color: 1
Size: 316992 Color: 0

Bin 2369: 99 of cap free
Amount of items: 2
Items: 
Size: 683630 Color: 0
Size: 316272 Color: 1

Bin 2370: 99 of cap free
Amount of items: 2
Items: 
Size: 700907 Color: 0
Size: 298995 Color: 1

Bin 2371: 99 of cap free
Amount of items: 2
Items: 
Size: 767037 Color: 1
Size: 232865 Color: 0

Bin 2372: 100 of cap free
Amount of items: 2
Items: 
Size: 771224 Color: 1
Size: 228677 Color: 0

Bin 2373: 100 of cap free
Amount of items: 2
Items: 
Size: 712489 Color: 0
Size: 287412 Color: 1

Bin 2374: 100 of cap free
Amount of items: 2
Items: 
Size: 722632 Color: 0
Size: 277269 Color: 1

Bin 2375: 100 of cap free
Amount of items: 2
Items: 
Size: 726493 Color: 0
Size: 273408 Color: 1

Bin 2376: 100 of cap free
Amount of items: 2
Items: 
Size: 777432 Color: 0
Size: 222469 Color: 1

Bin 2377: 100 of cap free
Amount of items: 2
Items: 
Size: 791688 Color: 0
Size: 208213 Color: 1

Bin 2378: 101 of cap free
Amount of items: 2
Items: 
Size: 531190 Color: 1
Size: 468710 Color: 0

Bin 2379: 101 of cap free
Amount of items: 2
Items: 
Size: 586263 Color: 0
Size: 413637 Color: 1

Bin 2380: 101 of cap free
Amount of items: 2
Items: 
Size: 601717 Color: 0
Size: 398183 Color: 1

Bin 2381: 101 of cap free
Amount of items: 2
Items: 
Size: 639564 Color: 0
Size: 360336 Color: 1

Bin 2382: 101 of cap free
Amount of items: 2
Items: 
Size: 659967 Color: 1
Size: 339933 Color: 0

Bin 2383: 101 of cap free
Amount of items: 2
Items: 
Size: 740289 Color: 1
Size: 259611 Color: 0

Bin 2384: 101 of cap free
Amount of items: 2
Items: 
Size: 764238 Color: 1
Size: 235662 Color: 0

Bin 2385: 101 of cap free
Amount of items: 2
Items: 
Size: 794193 Color: 1
Size: 205707 Color: 0

Bin 2386: 102 of cap free
Amount of items: 2
Items: 
Size: 521433 Color: 1
Size: 478466 Color: 0

Bin 2387: 102 of cap free
Amount of items: 2
Items: 
Size: 525969 Color: 1
Size: 473930 Color: 0

Bin 2388: 102 of cap free
Amount of items: 2
Items: 
Size: 543089 Color: 0
Size: 456810 Color: 1

Bin 2389: 102 of cap free
Amount of items: 2
Items: 
Size: 559359 Color: 0
Size: 440540 Color: 1

Bin 2390: 102 of cap free
Amount of items: 2
Items: 
Size: 644579 Color: 1
Size: 355320 Color: 0

Bin 2391: 102 of cap free
Amount of items: 2
Items: 
Size: 660511 Color: 0
Size: 339388 Color: 1

Bin 2392: 102 of cap free
Amount of items: 2
Items: 
Size: 687240 Color: 0
Size: 312659 Color: 1

Bin 2393: 102 of cap free
Amount of items: 2
Items: 
Size: 750487 Color: 0
Size: 249412 Color: 1

Bin 2394: 103 of cap free
Amount of items: 2
Items: 
Size: 572462 Color: 0
Size: 427436 Color: 1

Bin 2395: 103 of cap free
Amount of items: 2
Items: 
Size: 725671 Color: 1
Size: 274227 Color: 0

Bin 2396: 103 of cap free
Amount of items: 2
Items: 
Size: 741756 Color: 1
Size: 258142 Color: 0

Bin 2397: 104 of cap free
Amount of items: 2
Items: 
Size: 525213 Color: 1
Size: 474684 Color: 0

Bin 2398: 104 of cap free
Amount of items: 2
Items: 
Size: 526859 Color: 0
Size: 473038 Color: 1

Bin 2399: 104 of cap free
Amount of items: 2
Items: 
Size: 542938 Color: 1
Size: 456959 Color: 0

Bin 2400: 104 of cap free
Amount of items: 2
Items: 
Size: 575313 Color: 0
Size: 424584 Color: 1

Bin 2401: 104 of cap free
Amount of items: 2
Items: 
Size: 599181 Color: 1
Size: 400716 Color: 0

Bin 2402: 104 of cap free
Amount of items: 2
Items: 
Size: 611482 Color: 1
Size: 388415 Color: 0

Bin 2403: 104 of cap free
Amount of items: 2
Items: 
Size: 706603 Color: 1
Size: 293294 Color: 0

Bin 2404: 104 of cap free
Amount of items: 2
Items: 
Size: 708384 Color: 1
Size: 291513 Color: 0

Bin 2405: 104 of cap free
Amount of items: 2
Items: 
Size: 724812 Color: 1
Size: 275085 Color: 0

Bin 2406: 105 of cap free
Amount of items: 2
Items: 
Size: 549397 Color: 0
Size: 450499 Color: 1

Bin 2407: 105 of cap free
Amount of items: 2
Items: 
Size: 602145 Color: 0
Size: 397751 Color: 1

Bin 2408: 105 of cap free
Amount of items: 2
Items: 
Size: 624930 Color: 1
Size: 374966 Color: 0

Bin 2409: 105 of cap free
Amount of items: 2
Items: 
Size: 668338 Color: 0
Size: 331558 Color: 1

Bin 2410: 105 of cap free
Amount of items: 2
Items: 
Size: 748184 Color: 0
Size: 251712 Color: 1

Bin 2411: 106 of cap free
Amount of items: 2
Items: 
Size: 500393 Color: 1
Size: 499502 Color: 0

Bin 2412: 106 of cap free
Amount of items: 2
Items: 
Size: 531835 Color: 0
Size: 468060 Color: 1

Bin 2413: 106 of cap free
Amount of items: 2
Items: 
Size: 538374 Color: 1
Size: 461521 Color: 0

Bin 2414: 106 of cap free
Amount of items: 2
Items: 
Size: 566496 Color: 1
Size: 433399 Color: 0

Bin 2415: 106 of cap free
Amount of items: 2
Items: 
Size: 595005 Color: 1
Size: 404890 Color: 0

Bin 2416: 106 of cap free
Amount of items: 2
Items: 
Size: 610843 Color: 1
Size: 389052 Color: 0

Bin 2417: 106 of cap free
Amount of items: 2
Items: 
Size: 655943 Color: 1
Size: 343952 Color: 0

Bin 2418: 106 of cap free
Amount of items: 2
Items: 
Size: 758645 Color: 1
Size: 241250 Color: 0

Bin 2419: 106 of cap free
Amount of items: 2
Items: 
Size: 781652 Color: 0
Size: 218243 Color: 1

Bin 2420: 107 of cap free
Amount of items: 2
Items: 
Size: 503877 Color: 0
Size: 496017 Color: 1

Bin 2421: 107 of cap free
Amount of items: 2
Items: 
Size: 564525 Color: 0
Size: 435369 Color: 1

Bin 2422: 107 of cap free
Amount of items: 2
Items: 
Size: 572454 Color: 1
Size: 427440 Color: 0

Bin 2423: 107 of cap free
Amount of items: 2
Items: 
Size: 580541 Color: 1
Size: 419353 Color: 0

Bin 2424: 107 of cap free
Amount of items: 2
Items: 
Size: 607457 Color: 0
Size: 392437 Color: 1

Bin 2425: 107 of cap free
Amount of items: 2
Items: 
Size: 641778 Color: 0
Size: 358116 Color: 1

Bin 2426: 107 of cap free
Amount of items: 2
Items: 
Size: 716695 Color: 0
Size: 283199 Color: 1

Bin 2427: 107 of cap free
Amount of items: 2
Items: 
Size: 729488 Color: 0
Size: 270406 Color: 1

Bin 2428: 107 of cap free
Amount of items: 2
Items: 
Size: 776282 Color: 1
Size: 223612 Color: 0

Bin 2429: 107 of cap free
Amount of items: 2
Items: 
Size: 787848 Color: 0
Size: 212046 Color: 1

Bin 2430: 108 of cap free
Amount of items: 2
Items: 
Size: 795539 Color: 0
Size: 204354 Color: 1

Bin 2431: 108 of cap free
Amount of items: 2
Items: 
Size: 561348 Color: 1
Size: 438545 Color: 0

Bin 2432: 108 of cap free
Amount of items: 2
Items: 
Size: 566328 Color: 1
Size: 433565 Color: 0

Bin 2433: 108 of cap free
Amount of items: 2
Items: 
Size: 580707 Color: 0
Size: 419186 Color: 1

Bin 2434: 108 of cap free
Amount of items: 2
Items: 
Size: 584728 Color: 1
Size: 415165 Color: 0

Bin 2435: 108 of cap free
Amount of items: 2
Items: 
Size: 589219 Color: 0
Size: 410674 Color: 1

Bin 2436: 108 of cap free
Amount of items: 2
Items: 
Size: 594680 Color: 1
Size: 405213 Color: 0

Bin 2437: 108 of cap free
Amount of items: 2
Items: 
Size: 630101 Color: 0
Size: 369792 Color: 1

Bin 2438: 108 of cap free
Amount of items: 2
Items: 
Size: 660638 Color: 0
Size: 339255 Color: 1

Bin 2439: 108 of cap free
Amount of items: 2
Items: 
Size: 664557 Color: 0
Size: 335336 Color: 1

Bin 2440: 108 of cap free
Amount of items: 2
Items: 
Size: 699948 Color: 0
Size: 299945 Color: 1

Bin 2441: 108 of cap free
Amount of items: 2
Items: 
Size: 756364 Color: 1
Size: 243529 Color: 0

Bin 2442: 108 of cap free
Amount of items: 2
Items: 
Size: 790907 Color: 0
Size: 208986 Color: 1

Bin 2443: 109 of cap free
Amount of items: 2
Items: 
Size: 623168 Color: 1
Size: 376724 Color: 0

Bin 2444: 109 of cap free
Amount of items: 2
Items: 
Size: 526485 Color: 0
Size: 473407 Color: 1

Bin 2445: 109 of cap free
Amount of items: 2
Items: 
Size: 605049 Color: 0
Size: 394843 Color: 1

Bin 2446: 109 of cap free
Amount of items: 2
Items: 
Size: 797700 Color: 1
Size: 202192 Color: 0

Bin 2447: 110 of cap free
Amount of items: 2
Items: 
Size: 525816 Color: 0
Size: 474075 Color: 1

Bin 2448: 110 of cap free
Amount of items: 2
Items: 
Size: 534922 Color: 0
Size: 464969 Color: 1

Bin 2449: 110 of cap free
Amount of items: 2
Items: 
Size: 562264 Color: 0
Size: 437627 Color: 1

Bin 2450: 110 of cap free
Amount of items: 2
Items: 
Size: 569395 Color: 0
Size: 430496 Color: 1

Bin 2451: 110 of cap free
Amount of items: 2
Items: 
Size: 625318 Color: 1
Size: 374573 Color: 0

Bin 2452: 110 of cap free
Amount of items: 2
Items: 
Size: 668491 Color: 1
Size: 331400 Color: 0

Bin 2453: 110 of cap free
Amount of items: 2
Items: 
Size: 670533 Color: 0
Size: 329358 Color: 1

Bin 2454: 110 of cap free
Amount of items: 2
Items: 
Size: 737896 Color: 0
Size: 261995 Color: 1

Bin 2455: 110 of cap free
Amount of items: 2
Items: 
Size: 758944 Color: 1
Size: 240947 Color: 0

Bin 2456: 110 of cap free
Amount of items: 2
Items: 
Size: 792987 Color: 1
Size: 206904 Color: 0

Bin 2457: 111 of cap free
Amount of items: 2
Items: 
Size: 602676 Color: 0
Size: 397214 Color: 1

Bin 2458: 111 of cap free
Amount of items: 2
Items: 
Size: 505112 Color: 1
Size: 494778 Color: 0

Bin 2459: 111 of cap free
Amount of items: 2
Items: 
Size: 506239 Color: 0
Size: 493651 Color: 1

Bin 2460: 111 of cap free
Amount of items: 2
Items: 
Size: 535660 Color: 1
Size: 464230 Color: 0

Bin 2461: 111 of cap free
Amount of items: 2
Items: 
Size: 549506 Color: 0
Size: 450384 Color: 1

Bin 2462: 111 of cap free
Amount of items: 2
Items: 
Size: 568491 Color: 1
Size: 431399 Color: 0

Bin 2463: 111 of cap free
Amount of items: 2
Items: 
Size: 638682 Color: 1
Size: 361208 Color: 0

Bin 2464: 111 of cap free
Amount of items: 2
Items: 
Size: 735854 Color: 1
Size: 264036 Color: 0

Bin 2465: 111 of cap free
Amount of items: 2
Items: 
Size: 770084 Color: 1
Size: 229806 Color: 0

Bin 2466: 112 of cap free
Amount of items: 2
Items: 
Size: 546756 Color: 0
Size: 453133 Color: 1

Bin 2467: 112 of cap free
Amount of items: 2
Items: 
Size: 559199 Color: 1
Size: 440690 Color: 0

Bin 2468: 112 of cap free
Amount of items: 2
Items: 
Size: 631309 Color: 1
Size: 368580 Color: 0

Bin 2469: 112 of cap free
Amount of items: 2
Items: 
Size: 654509 Color: 1
Size: 345380 Color: 0

Bin 2470: 112 of cap free
Amount of items: 2
Items: 
Size: 713595 Color: 1
Size: 286294 Color: 0

Bin 2471: 112 of cap free
Amount of items: 2
Items: 
Size: 742946 Color: 1
Size: 256943 Color: 0

Bin 2472: 112 of cap free
Amount of items: 2
Items: 
Size: 746300 Color: 1
Size: 253589 Color: 0

Bin 2473: 112 of cap free
Amount of items: 2
Items: 
Size: 793439 Color: 1
Size: 206450 Color: 0

Bin 2474: 113 of cap free
Amount of items: 2
Items: 
Size: 506474 Color: 1
Size: 493414 Color: 0

Bin 2475: 113 of cap free
Amount of items: 2
Items: 
Size: 511627 Color: 0
Size: 488261 Color: 1

Bin 2476: 113 of cap free
Amount of items: 2
Items: 
Size: 553206 Color: 0
Size: 446682 Color: 1

Bin 2477: 113 of cap free
Amount of items: 2
Items: 
Size: 622596 Color: 1
Size: 377292 Color: 0

Bin 2478: 113 of cap free
Amount of items: 2
Items: 
Size: 652022 Color: 1
Size: 347866 Color: 0

Bin 2479: 113 of cap free
Amount of items: 2
Items: 
Size: 676788 Color: 0
Size: 323100 Color: 1

Bin 2480: 113 of cap free
Amount of items: 2
Items: 
Size: 692011 Color: 1
Size: 307877 Color: 0

Bin 2481: 113 of cap free
Amount of items: 2
Items: 
Size: 704290 Color: 0
Size: 295598 Color: 1

Bin 2482: 113 of cap free
Amount of items: 2
Items: 
Size: 717262 Color: 1
Size: 282626 Color: 0

Bin 2483: 113 of cap free
Amount of items: 2
Items: 
Size: 720924 Color: 1
Size: 278964 Color: 0

Bin 2484: 114 of cap free
Amount of items: 2
Items: 
Size: 558723 Color: 0
Size: 441164 Color: 1

Bin 2485: 114 of cap free
Amount of items: 2
Items: 
Size: 677158 Color: 1
Size: 322729 Color: 0

Bin 2486: 114 of cap free
Amount of items: 2
Items: 
Size: 678275 Color: 0
Size: 321612 Color: 1

Bin 2487: 114 of cap free
Amount of items: 2
Items: 
Size: 701047 Color: 1
Size: 298840 Color: 0

Bin 2488: 114 of cap free
Amount of items: 2
Items: 
Size: 784327 Color: 1
Size: 215560 Color: 0

Bin 2489: 115 of cap free
Amount of items: 2
Items: 
Size: 512218 Color: 0
Size: 487668 Color: 1

Bin 2490: 115 of cap free
Amount of items: 2
Items: 
Size: 556222 Color: 0
Size: 443664 Color: 1

Bin 2491: 115 of cap free
Amount of items: 2
Items: 
Size: 667416 Color: 1
Size: 332470 Color: 0

Bin 2492: 115 of cap free
Amount of items: 2
Items: 
Size: 675071 Color: 1
Size: 324815 Color: 0

Bin 2493: 115 of cap free
Amount of items: 2
Items: 
Size: 698400 Color: 0
Size: 301486 Color: 1

Bin 2494: 115 of cap free
Amount of items: 2
Items: 
Size: 757334 Color: 1
Size: 242552 Color: 0

Bin 2495: 115 of cap free
Amount of items: 2
Items: 
Size: 529717 Color: 1
Size: 470169 Color: 0

Bin 2496: 116 of cap free
Amount of items: 2
Items: 
Size: 501925 Color: 1
Size: 497960 Color: 0

Bin 2497: 116 of cap free
Amount of items: 2
Items: 
Size: 504560 Color: 0
Size: 495325 Color: 1

Bin 2498: 116 of cap free
Amount of items: 2
Items: 
Size: 513473 Color: 1
Size: 486412 Color: 0

Bin 2499: 116 of cap free
Amount of items: 2
Items: 
Size: 547936 Color: 0
Size: 451949 Color: 1

Bin 2500: 116 of cap free
Amount of items: 2
Items: 
Size: 609591 Color: 1
Size: 390294 Color: 0

Bin 2501: 116 of cap free
Amount of items: 2
Items: 
Size: 653917 Color: 0
Size: 345968 Color: 1

Bin 2502: 116 of cap free
Amount of items: 2
Items: 
Size: 662544 Color: 0
Size: 337341 Color: 1

Bin 2503: 116 of cap free
Amount of items: 2
Items: 
Size: 681670 Color: 0
Size: 318215 Color: 1

Bin 2504: 116 of cap free
Amount of items: 2
Items: 
Size: 731079 Color: 1
Size: 268806 Color: 0

Bin 2505: 116 of cap free
Amount of items: 2
Items: 
Size: 733145 Color: 1
Size: 266740 Color: 0

Bin 2506: 117 of cap free
Amount of items: 2
Items: 
Size: 506734 Color: 1
Size: 493150 Color: 0

Bin 2507: 117 of cap free
Amount of items: 2
Items: 
Size: 520568 Color: 1
Size: 479316 Color: 0

Bin 2508: 117 of cap free
Amount of items: 2
Items: 
Size: 531737 Color: 1
Size: 468147 Color: 0

Bin 2509: 117 of cap free
Amount of items: 2
Items: 
Size: 571880 Color: 1
Size: 428004 Color: 0

Bin 2510: 117 of cap free
Amount of items: 2
Items: 
Size: 623927 Color: 0
Size: 375957 Color: 1

Bin 2511: 117 of cap free
Amount of items: 2
Items: 
Size: 639559 Color: 1
Size: 360325 Color: 0

Bin 2512: 117 of cap free
Amount of items: 2
Items: 
Size: 701388 Color: 1
Size: 298496 Color: 0

Bin 2513: 117 of cap free
Amount of items: 2
Items: 
Size: 719621 Color: 1
Size: 280263 Color: 0

Bin 2514: 117 of cap free
Amount of items: 2
Items: 
Size: 763239 Color: 0
Size: 236645 Color: 1

Bin 2515: 117 of cap free
Amount of items: 2
Items: 
Size: 771973 Color: 1
Size: 227911 Color: 0

Bin 2516: 117 of cap free
Amount of items: 2
Items: 
Size: 799296 Color: 0
Size: 200588 Color: 1

Bin 2517: 118 of cap free
Amount of items: 2
Items: 
Size: 795708 Color: 0
Size: 204175 Color: 1

Bin 2518: 118 of cap free
Amount of items: 2
Items: 
Size: 507909 Color: 1
Size: 491974 Color: 0

Bin 2519: 118 of cap free
Amount of items: 2
Items: 
Size: 530111 Color: 1
Size: 469772 Color: 0

Bin 2520: 118 of cap free
Amount of items: 2
Items: 
Size: 545921 Color: 1
Size: 453962 Color: 0

Bin 2521: 118 of cap free
Amount of items: 2
Items: 
Size: 562463 Color: 1
Size: 437420 Color: 0

Bin 2522: 118 of cap free
Amount of items: 2
Items: 
Size: 662327 Color: 1
Size: 337556 Color: 0

Bin 2523: 118 of cap free
Amount of items: 2
Items: 
Size: 716403 Color: 1
Size: 283480 Color: 0

Bin 2524: 118 of cap free
Amount of items: 2
Items: 
Size: 772164 Color: 1
Size: 227719 Color: 0

Bin 2525: 119 of cap free
Amount of items: 2
Items: 
Size: 553772 Color: 0
Size: 446110 Color: 1

Bin 2526: 119 of cap free
Amount of items: 2
Items: 
Size: 666817 Color: 1
Size: 333065 Color: 0

Bin 2527: 119 of cap free
Amount of items: 2
Items: 
Size: 729998 Color: 1
Size: 269884 Color: 0

Bin 2528: 119 of cap free
Amount of items: 2
Items: 
Size: 753670 Color: 0
Size: 246212 Color: 1

Bin 2529: 119 of cap free
Amount of items: 2
Items: 
Size: 786858 Color: 0
Size: 213024 Color: 1

Bin 2530: 120 of cap free
Amount of items: 2
Items: 
Size: 653374 Color: 1
Size: 346507 Color: 0

Bin 2531: 120 of cap free
Amount of items: 2
Items: 
Size: 727458 Color: 0
Size: 272423 Color: 1

Bin 2532: 120 of cap free
Amount of items: 2
Items: 
Size: 735720 Color: 1
Size: 264161 Color: 0

Bin 2533: 120 of cap free
Amount of items: 2
Items: 
Size: 748176 Color: 0
Size: 251705 Color: 1

Bin 2534: 120 of cap free
Amount of items: 2
Items: 
Size: 750188 Color: 0
Size: 249693 Color: 1

Bin 2535: 121 of cap free
Amount of items: 2
Items: 
Size: 561705 Color: 1
Size: 438175 Color: 0

Bin 2536: 121 of cap free
Amount of items: 2
Items: 
Size: 575194 Color: 1
Size: 424686 Color: 0

Bin 2537: 121 of cap free
Amount of items: 2
Items: 
Size: 621784 Color: 0
Size: 378096 Color: 1

Bin 2538: 121 of cap free
Amount of items: 2
Items: 
Size: 648687 Color: 1
Size: 351193 Color: 0

Bin 2539: 121 of cap free
Amount of items: 2
Items: 
Size: 723845 Color: 1
Size: 276035 Color: 0

Bin 2540: 121 of cap free
Amount of items: 2
Items: 
Size: 782019 Color: 1
Size: 217861 Color: 0

Bin 2541: 121 of cap free
Amount of items: 2
Items: 
Size: 783351 Color: 0
Size: 216529 Color: 1

Bin 2542: 121 of cap free
Amount of items: 2
Items: 
Size: 785219 Color: 0
Size: 214661 Color: 1

Bin 2543: 121 of cap free
Amount of items: 2
Items: 
Size: 796875 Color: 0
Size: 203005 Color: 1

Bin 2544: 122 of cap free
Amount of items: 2
Items: 
Size: 510913 Color: 0
Size: 488966 Color: 1

Bin 2545: 122 of cap free
Amount of items: 2
Items: 
Size: 511775 Color: 1
Size: 488104 Color: 0

Bin 2546: 122 of cap free
Amount of items: 2
Items: 
Size: 540283 Color: 1
Size: 459596 Color: 0

Bin 2547: 122 of cap free
Amount of items: 2
Items: 
Size: 549091 Color: 0
Size: 450788 Color: 1

Bin 2548: 122 of cap free
Amount of items: 2
Items: 
Size: 556652 Color: 1
Size: 443227 Color: 0

Bin 2549: 122 of cap free
Amount of items: 2
Items: 
Size: 614406 Color: 1
Size: 385473 Color: 0

Bin 2550: 122 of cap free
Amount of items: 2
Items: 
Size: 666881 Color: 0
Size: 332998 Color: 1

Bin 2551: 122 of cap free
Amount of items: 2
Items: 
Size: 690749 Color: 0
Size: 309130 Color: 1

Bin 2552: 122 of cap free
Amount of items: 2
Items: 
Size: 725913 Color: 1
Size: 273966 Color: 0

Bin 2553: 122 of cap free
Amount of items: 2
Items: 
Size: 765651 Color: 1
Size: 234228 Color: 0

Bin 2554: 122 of cap free
Amount of items: 2
Items: 
Size: 790701 Color: 0
Size: 209178 Color: 1

Bin 2555: 123 of cap free
Amount of items: 2
Items: 
Size: 556527 Color: 0
Size: 443351 Color: 1

Bin 2556: 123 of cap free
Amount of items: 2
Items: 
Size: 672424 Color: 0
Size: 327454 Color: 1

Bin 2557: 123 of cap free
Amount of items: 2
Items: 
Size: 699638 Color: 1
Size: 300240 Color: 0

Bin 2558: 123 of cap free
Amount of items: 2
Items: 
Size: 729655 Color: 0
Size: 270223 Color: 1

Bin 2559: 123 of cap free
Amount of items: 2
Items: 
Size: 778088 Color: 1
Size: 221790 Color: 0

Bin 2560: 124 of cap free
Amount of items: 2
Items: 
Size: 508250 Color: 0
Size: 491627 Color: 1

Bin 2561: 124 of cap free
Amount of items: 2
Items: 
Size: 511561 Color: 1
Size: 488316 Color: 0

Bin 2562: 124 of cap free
Amount of items: 2
Items: 
Size: 582807 Color: 0
Size: 417070 Color: 1

Bin 2563: 124 of cap free
Amount of items: 2
Items: 
Size: 721349 Color: 0
Size: 278528 Color: 1

Bin 2564: 125 of cap free
Amount of items: 2
Items: 
Size: 501543 Color: 0
Size: 498333 Color: 1

Bin 2565: 125 of cap free
Amount of items: 2
Items: 
Size: 532901 Color: 0
Size: 466975 Color: 1

Bin 2566: 125 of cap free
Amount of items: 2
Items: 
Size: 558362 Color: 0
Size: 441514 Color: 1

Bin 2567: 125 of cap free
Amount of items: 2
Items: 
Size: 614169 Color: 1
Size: 385707 Color: 0

Bin 2568: 125 of cap free
Amount of items: 2
Items: 
Size: 656408 Color: 1
Size: 343468 Color: 0

Bin 2569: 125 of cap free
Amount of items: 2
Items: 
Size: 667235 Color: 0
Size: 332641 Color: 1

Bin 2570: 125 of cap free
Amount of items: 2
Items: 
Size: 680512 Color: 1
Size: 319364 Color: 0

Bin 2571: 125 of cap free
Amount of items: 2
Items: 
Size: 695457 Color: 1
Size: 304419 Color: 0

Bin 2572: 125 of cap free
Amount of items: 2
Items: 
Size: 783465 Color: 1
Size: 216411 Color: 0

Bin 2573: 125 of cap free
Amount of items: 2
Items: 
Size: 740667 Color: 1
Size: 259209 Color: 0

Bin 2574: 126 of cap free
Amount of items: 2
Items: 
Size: 529085 Color: 0
Size: 470790 Color: 1

Bin 2575: 126 of cap free
Amount of items: 2
Items: 
Size: 547439 Color: 0
Size: 452436 Color: 1

Bin 2576: 126 of cap free
Amount of items: 2
Items: 
Size: 580891 Color: 0
Size: 418984 Color: 1

Bin 2577: 126 of cap free
Amount of items: 2
Items: 
Size: 601261 Color: 0
Size: 398614 Color: 1

Bin 2578: 126 of cap free
Amount of items: 2
Items: 
Size: 640659 Color: 1
Size: 359216 Color: 0

Bin 2579: 126 of cap free
Amount of items: 2
Items: 
Size: 679626 Color: 0
Size: 320249 Color: 1

Bin 2580: 126 of cap free
Amount of items: 2
Items: 
Size: 701613 Color: 1
Size: 298262 Color: 0

Bin 2581: 126 of cap free
Amount of items: 2
Items: 
Size: 731979 Color: 0
Size: 267896 Color: 1

Bin 2582: 126 of cap free
Amount of items: 2
Items: 
Size: 777606 Color: 1
Size: 222269 Color: 0

Bin 2583: 127 of cap free
Amount of items: 2
Items: 
Size: 503237 Color: 0
Size: 496637 Color: 1

Bin 2584: 127 of cap free
Amount of items: 2
Items: 
Size: 629806 Color: 0
Size: 370068 Color: 1

Bin 2585: 127 of cap free
Amount of items: 2
Items: 
Size: 666362 Color: 0
Size: 333512 Color: 1

Bin 2586: 127 of cap free
Amount of items: 2
Items: 
Size: 742250 Color: 1
Size: 257624 Color: 0

Bin 2587: 127 of cap free
Amount of items: 2
Items: 
Size: 774027 Color: 0
Size: 225847 Color: 1

Bin 2588: 128 of cap free
Amount of items: 2
Items: 
Size: 566810 Color: 0
Size: 433063 Color: 1

Bin 2589: 128 of cap free
Amount of items: 2
Items: 
Size: 590599 Color: 0
Size: 409274 Color: 1

Bin 2590: 128 of cap free
Amount of items: 2
Items: 
Size: 668654 Color: 0
Size: 331219 Color: 1

Bin 2591: 128 of cap free
Amount of items: 2
Items: 
Size: 727947 Color: 1
Size: 271926 Color: 0

Bin 2592: 129 of cap free
Amount of items: 2
Items: 
Size: 671188 Color: 1
Size: 328684 Color: 0

Bin 2593: 129 of cap free
Amount of items: 2
Items: 
Size: 690939 Color: 0
Size: 308933 Color: 1

Bin 2594: 129 of cap free
Amount of items: 2
Items: 
Size: 700306 Color: 1
Size: 299566 Color: 0

Bin 2595: 129 of cap free
Amount of items: 2
Items: 
Size: 718046 Color: 1
Size: 281826 Color: 0

Bin 2596: 129 of cap free
Amount of items: 2
Items: 
Size: 760673 Color: 0
Size: 239199 Color: 1

Bin 2597: 129 of cap free
Amount of items: 2
Items: 
Size: 742539 Color: 0
Size: 257333 Color: 1

Bin 2598: 130 of cap free
Amount of items: 2
Items: 
Size: 706062 Color: 1
Size: 293809 Color: 0

Bin 2599: 130 of cap free
Amount of items: 2
Items: 
Size: 539352 Color: 0
Size: 460519 Color: 1

Bin 2600: 130 of cap free
Amount of items: 2
Items: 
Size: 614953 Color: 1
Size: 384918 Color: 0

Bin 2601: 130 of cap free
Amount of items: 2
Items: 
Size: 654954 Color: 1
Size: 344917 Color: 0

Bin 2602: 130 of cap free
Amount of items: 2
Items: 
Size: 685117 Color: 0
Size: 314754 Color: 1

Bin 2603: 130 of cap free
Amount of items: 2
Items: 
Size: 685330 Color: 0
Size: 314541 Color: 1

Bin 2604: 130 of cap free
Amount of items: 2
Items: 
Size: 749073 Color: 1
Size: 250798 Color: 0

Bin 2605: 130 of cap free
Amount of items: 2
Items: 
Size: 742610 Color: 1
Size: 257261 Color: 0

Bin 2606: 131 of cap free
Amount of items: 2
Items: 
Size: 507447 Color: 0
Size: 492423 Color: 1

Bin 2607: 131 of cap free
Amount of items: 2
Items: 
Size: 582291 Color: 0
Size: 417579 Color: 1

Bin 2608: 131 of cap free
Amount of items: 2
Items: 
Size: 616374 Color: 0
Size: 383496 Color: 1

Bin 2609: 131 of cap free
Amount of items: 2
Items: 
Size: 655298 Color: 1
Size: 344572 Color: 0

Bin 2610: 131 of cap free
Amount of items: 2
Items: 
Size: 704730 Color: 0
Size: 295140 Color: 1

Bin 2611: 131 of cap free
Amount of items: 2
Items: 
Size: 737522 Color: 0
Size: 262348 Color: 1

Bin 2612: 131 of cap free
Amount of items: 2
Items: 
Size: 798214 Color: 0
Size: 201656 Color: 1

Bin 2613: 132 of cap free
Amount of items: 2
Items: 
Size: 577402 Color: 1
Size: 422467 Color: 0

Bin 2614: 132 of cap free
Amount of items: 2
Items: 
Size: 623739 Color: 0
Size: 376130 Color: 1

Bin 2615: 132 of cap free
Amount of items: 2
Items: 
Size: 710198 Color: 1
Size: 289671 Color: 0

Bin 2616: 132 of cap free
Amount of items: 2
Items: 
Size: 756340 Color: 0
Size: 243529 Color: 1

Bin 2617: 132 of cap free
Amount of items: 2
Items: 
Size: 773129 Color: 0
Size: 226740 Color: 1

Bin 2618: 132 of cap free
Amount of items: 2
Items: 
Size: 552229 Color: 0
Size: 447640 Color: 1

Bin 2619: 133 of cap free
Amount of items: 2
Items: 
Size: 504962 Color: 1
Size: 494906 Color: 0

Bin 2620: 133 of cap free
Amount of items: 2
Items: 
Size: 584230 Color: 1
Size: 415638 Color: 0

Bin 2621: 133 of cap free
Amount of items: 2
Items: 
Size: 584724 Color: 1
Size: 415144 Color: 0

Bin 2622: 133 of cap free
Amount of items: 2
Items: 
Size: 606447 Color: 1
Size: 393421 Color: 0

Bin 2623: 133 of cap free
Amount of items: 2
Items: 
Size: 631023 Color: 1
Size: 368845 Color: 0

Bin 2624: 133 of cap free
Amount of items: 2
Items: 
Size: 636675 Color: 0
Size: 363193 Color: 1

Bin 2625: 133 of cap free
Amount of items: 2
Items: 
Size: 657743 Color: 0
Size: 342125 Color: 1

Bin 2626: 133 of cap free
Amount of items: 2
Items: 
Size: 684633 Color: 1
Size: 315235 Color: 0

Bin 2627: 133 of cap free
Amount of items: 2
Items: 
Size: 718640 Color: 1
Size: 281228 Color: 0

Bin 2628: 133 of cap free
Amount of items: 2
Items: 
Size: 725953 Color: 0
Size: 273915 Color: 1

Bin 2629: 133 of cap free
Amount of items: 2
Items: 
Size: 728550 Color: 1
Size: 271318 Color: 0

Bin 2630: 133 of cap free
Amount of items: 2
Items: 
Size: 775441 Color: 1
Size: 224427 Color: 0

Bin 2631: 134 of cap free
Amount of items: 2
Items: 
Size: 570889 Color: 1
Size: 428978 Color: 0

Bin 2632: 134 of cap free
Amount of items: 2
Items: 
Size: 617578 Color: 0
Size: 382289 Color: 1

Bin 2633: 134 of cap free
Amount of items: 2
Items: 
Size: 633156 Color: 1
Size: 366711 Color: 0

Bin 2634: 134 of cap free
Amount of items: 2
Items: 
Size: 685554 Color: 0
Size: 314313 Color: 1

Bin 2635: 134 of cap free
Amount of items: 2
Items: 
Size: 698880 Color: 0
Size: 300987 Color: 1

Bin 2636: 134 of cap free
Amount of items: 2
Items: 
Size: 723420 Color: 1
Size: 276447 Color: 0

Bin 2637: 134 of cap free
Amount of items: 2
Items: 
Size: 781990 Color: 0
Size: 217877 Color: 1

Bin 2638: 135 of cap free
Amount of items: 2
Items: 
Size: 570067 Color: 0
Size: 429799 Color: 1

Bin 2639: 135 of cap free
Amount of items: 2
Items: 
Size: 632420 Color: 1
Size: 367446 Color: 0

Bin 2640: 135 of cap free
Amount of items: 2
Items: 
Size: 643227 Color: 0
Size: 356639 Color: 1

Bin 2641: 135 of cap free
Amount of items: 2
Items: 
Size: 693669 Color: 0
Size: 306197 Color: 1

Bin 2642: 136 of cap free
Amount of items: 2
Items: 
Size: 796777 Color: 1
Size: 203088 Color: 0

Bin 2643: 136 of cap free
Amount of items: 2
Items: 
Size: 618278 Color: 0
Size: 381587 Color: 1

Bin 2644: 136 of cap free
Amount of items: 2
Items: 
Size: 564820 Color: 0
Size: 435045 Color: 1

Bin 2645: 136 of cap free
Amount of items: 2
Items: 
Size: 603233 Color: 1
Size: 396632 Color: 0

Bin 2646: 136 of cap free
Amount of items: 2
Items: 
Size: 616770 Color: 1
Size: 383095 Color: 0

Bin 2647: 136 of cap free
Amount of items: 2
Items: 
Size: 636503 Color: 1
Size: 363362 Color: 0

Bin 2648: 136 of cap free
Amount of items: 2
Items: 
Size: 737030 Color: 1
Size: 262835 Color: 0

Bin 2649: 137 of cap free
Amount of items: 2
Items: 
Size: 656402 Color: 0
Size: 343462 Color: 1

Bin 2650: 137 of cap free
Amount of items: 2
Items: 
Size: 567909 Color: 0
Size: 431955 Color: 1

Bin 2651: 137 of cap free
Amount of items: 2
Items: 
Size: 609301 Color: 1
Size: 390563 Color: 0

Bin 2652: 137 of cap free
Amount of items: 2
Items: 
Size: 623907 Color: 1
Size: 375957 Color: 0

Bin 2653: 137 of cap free
Amount of items: 2
Items: 
Size: 649712 Color: 1
Size: 350152 Color: 0

Bin 2654: 137 of cap free
Amount of items: 2
Items: 
Size: 687893 Color: 0
Size: 311971 Color: 1

Bin 2655: 137 of cap free
Amount of items: 2
Items: 
Size: 697890 Color: 0
Size: 301974 Color: 1

Bin 2656: 137 of cap free
Amount of items: 2
Items: 
Size: 713745 Color: 0
Size: 286119 Color: 1

Bin 2657: 137 of cap free
Amount of items: 2
Items: 
Size: 767630 Color: 0
Size: 232234 Color: 1

Bin 2658: 138 of cap free
Amount of items: 2
Items: 
Size: 593093 Color: 1
Size: 406770 Color: 0

Bin 2659: 138 of cap free
Amount of items: 2
Items: 
Size: 618921 Color: 0
Size: 380942 Color: 1

Bin 2660: 138 of cap free
Amount of items: 2
Items: 
Size: 648036 Color: 1
Size: 351827 Color: 0

Bin 2661: 138 of cap free
Amount of items: 2
Items: 
Size: 708344 Color: 0
Size: 291519 Color: 1

Bin 2662: 138 of cap free
Amount of items: 2
Items: 
Size: 722993 Color: 0
Size: 276870 Color: 1

Bin 2663: 138 of cap free
Amount of items: 2
Items: 
Size: 726949 Color: 1
Size: 272914 Color: 0

Bin 2664: 139 of cap free
Amount of items: 2
Items: 
Size: 644661 Color: 0
Size: 355201 Color: 1

Bin 2665: 139 of cap free
Amount of items: 2
Items: 
Size: 715546 Color: 0
Size: 284316 Color: 1

Bin 2666: 140 of cap free
Amount of items: 2
Items: 
Size: 556306 Color: 1
Size: 443555 Color: 0

Bin 2667: 140 of cap free
Amount of items: 2
Items: 
Size: 578474 Color: 1
Size: 421387 Color: 0

Bin 2668: 140 of cap free
Amount of items: 2
Items: 
Size: 693241 Color: 1
Size: 306620 Color: 0

Bin 2669: 140 of cap free
Amount of items: 2
Items: 
Size: 739452 Color: 1
Size: 260409 Color: 0

Bin 2670: 140 of cap free
Amount of items: 2
Items: 
Size: 775718 Color: 1
Size: 224143 Color: 0

Bin 2671: 141 of cap free
Amount of items: 2
Items: 
Size: 523318 Color: 1
Size: 476542 Color: 0

Bin 2672: 141 of cap free
Amount of items: 2
Items: 
Size: 582199 Color: 1
Size: 417661 Color: 0

Bin 2673: 141 of cap free
Amount of items: 2
Items: 
Size: 674725 Color: 0
Size: 325135 Color: 1

Bin 2674: 141 of cap free
Amount of items: 2
Items: 
Size: 734297 Color: 0
Size: 265563 Color: 1

Bin 2675: 141 of cap free
Amount of items: 2
Items: 
Size: 771967 Color: 1
Size: 227893 Color: 0

Bin 2676: 142 of cap free
Amount of items: 2
Items: 
Size: 517299 Color: 0
Size: 482560 Color: 1

Bin 2677: 142 of cap free
Amount of items: 2
Items: 
Size: 534477 Color: 1
Size: 465382 Color: 0

Bin 2678: 142 of cap free
Amount of items: 2
Items: 
Size: 560588 Color: 0
Size: 439271 Color: 1

Bin 2679: 142 of cap free
Amount of items: 2
Items: 
Size: 638184 Color: 1
Size: 361675 Color: 0

Bin 2680: 142 of cap free
Amount of items: 2
Items: 
Size: 712662 Color: 1
Size: 287197 Color: 0

Bin 2681: 142 of cap free
Amount of items: 2
Items: 
Size: 786014 Color: 1
Size: 213845 Color: 0

Bin 2682: 143 of cap free
Amount of items: 2
Items: 
Size: 558711 Color: 0
Size: 441147 Color: 1

Bin 2683: 143 of cap free
Amount of items: 2
Items: 
Size: 647600 Color: 1
Size: 352258 Color: 0

Bin 2684: 143 of cap free
Amount of items: 2
Items: 
Size: 695191 Color: 1
Size: 304667 Color: 0

Bin 2685: 143 of cap free
Amount of items: 2
Items: 
Size: 770769 Color: 0
Size: 229089 Color: 1

Bin 2686: 143 of cap free
Amount of items: 2
Items: 
Size: 794879 Color: 1
Size: 204979 Color: 0

Bin 2687: 144 of cap free
Amount of items: 3
Items: 
Size: 562247 Color: 1
Size: 266339 Color: 0
Size: 171271 Color: 0

Bin 2688: 144 of cap free
Amount of items: 2
Items: 
Size: 525677 Color: 1
Size: 474180 Color: 0

Bin 2689: 144 of cap free
Amount of items: 2
Items: 
Size: 568465 Color: 0
Size: 431392 Color: 1

Bin 2690: 144 of cap free
Amount of items: 2
Items: 
Size: 571853 Color: 0
Size: 428004 Color: 1

Bin 2691: 144 of cap free
Amount of items: 2
Items: 
Size: 745928 Color: 0
Size: 253929 Color: 1

Bin 2692: 144 of cap free
Amount of items: 2
Items: 
Size: 779108 Color: 0
Size: 220749 Color: 1

Bin 2693: 145 of cap free
Amount of items: 6
Items: 
Size: 281040 Color: 0
Size: 154261 Color: 1
Size: 146464 Color: 1
Size: 143077 Color: 1
Size: 140227 Color: 0
Size: 134787 Color: 0

Bin 2694: 145 of cap free
Amount of items: 2
Items: 
Size: 571561 Color: 1
Size: 428295 Color: 0

Bin 2695: 145 of cap free
Amount of items: 2
Items: 
Size: 589963 Color: 0
Size: 409893 Color: 1

Bin 2696: 145 of cap free
Amount of items: 2
Items: 
Size: 696175 Color: 0
Size: 303681 Color: 1

Bin 2697: 145 of cap free
Amount of items: 2
Items: 
Size: 795565 Color: 1
Size: 204291 Color: 0

Bin 2698: 146 of cap free
Amount of items: 2
Items: 
Size: 544287 Color: 1
Size: 455568 Color: 0

Bin 2699: 146 of cap free
Amount of items: 2
Items: 
Size: 592665 Color: 1
Size: 407190 Color: 0

Bin 2700: 146 of cap free
Amount of items: 2
Items: 
Size: 663278 Color: 0
Size: 336577 Color: 1

Bin 2701: 146 of cap free
Amount of items: 2
Items: 
Size: 702967 Color: 1
Size: 296888 Color: 0

Bin 2702: 147 of cap free
Amount of items: 2
Items: 
Size: 500779 Color: 1
Size: 499075 Color: 0

Bin 2703: 147 of cap free
Amount of items: 2
Items: 
Size: 509873 Color: 1
Size: 489981 Color: 0

Bin 2704: 147 of cap free
Amount of items: 2
Items: 
Size: 515036 Color: 1
Size: 484818 Color: 0

Bin 2705: 147 of cap free
Amount of items: 2
Items: 
Size: 555703 Color: 0
Size: 444151 Color: 1

Bin 2706: 147 of cap free
Amount of items: 2
Items: 
Size: 578863 Color: 1
Size: 420991 Color: 0

Bin 2707: 147 of cap free
Amount of items: 2
Items: 
Size: 656228 Color: 1
Size: 343626 Color: 0

Bin 2708: 147 of cap free
Amount of items: 2
Items: 
Size: 738828 Color: 0
Size: 261026 Color: 1

Bin 2709: 147 of cap free
Amount of items: 2
Items: 
Size: 752136 Color: 0
Size: 247718 Color: 1

Bin 2710: 148 of cap free
Amount of items: 2
Items: 
Size: 595065 Color: 0
Size: 404788 Color: 1

Bin 2711: 148 of cap free
Amount of items: 2
Items: 
Size: 668644 Color: 1
Size: 331209 Color: 0

Bin 2712: 148 of cap free
Amount of items: 2
Items: 
Size: 673503 Color: 0
Size: 326350 Color: 1

Bin 2713: 148 of cap free
Amount of items: 2
Items: 
Size: 693152 Color: 0
Size: 306701 Color: 1

Bin 2714: 148 of cap free
Amount of items: 2
Items: 
Size: 696214 Color: 1
Size: 303639 Color: 0

Bin 2715: 148 of cap free
Amount of items: 2
Items: 
Size: 723229 Color: 1
Size: 276624 Color: 0

Bin 2716: 149 of cap free
Amount of items: 2
Items: 
Size: 593609 Color: 1
Size: 406243 Color: 0

Bin 2717: 149 of cap free
Amount of items: 2
Items: 
Size: 602832 Color: 1
Size: 397020 Color: 0

Bin 2718: 149 of cap free
Amount of items: 2
Items: 
Size: 655124 Color: 0
Size: 344728 Color: 1

Bin 2719: 149 of cap free
Amount of items: 2
Items: 
Size: 769270 Color: 0
Size: 230582 Color: 1

Bin 2720: 149 of cap free
Amount of items: 2
Items: 
Size: 793550 Color: 0
Size: 206302 Color: 1

Bin 2721: 150 of cap free
Amount of items: 2
Items: 
Size: 535656 Color: 0
Size: 464195 Color: 1

Bin 2722: 150 of cap free
Amount of items: 2
Items: 
Size: 543582 Color: 0
Size: 456269 Color: 1

Bin 2723: 150 of cap free
Amount of items: 2
Items: 
Size: 566325 Color: 0
Size: 433526 Color: 1

Bin 2724: 150 of cap free
Amount of items: 2
Items: 
Size: 605008 Color: 1
Size: 394843 Color: 0

Bin 2725: 150 of cap free
Amount of items: 2
Items: 
Size: 610723 Color: 0
Size: 389128 Color: 1

Bin 2726: 150 of cap free
Amount of items: 2
Items: 
Size: 662128 Color: 0
Size: 337723 Color: 1

Bin 2727: 150 of cap free
Amount of items: 2
Items: 
Size: 667059 Color: 1
Size: 332792 Color: 0

Bin 2728: 150 of cap free
Amount of items: 2
Items: 
Size: 668136 Color: 0
Size: 331715 Color: 1

Bin 2729: 150 of cap free
Amount of items: 2
Items: 
Size: 700244 Color: 0
Size: 299607 Color: 1

Bin 2730: 150 of cap free
Amount of items: 2
Items: 
Size: 759846 Color: 1
Size: 240005 Color: 0

Bin 2731: 150 of cap free
Amount of items: 2
Items: 
Size: 788516 Color: 0
Size: 211335 Color: 1

Bin 2732: 150 of cap free
Amount of items: 2
Items: 
Size: 797200 Color: 0
Size: 202651 Color: 1

Bin 2733: 151 of cap free
Amount of items: 2
Items: 
Size: 501716 Color: 0
Size: 498134 Color: 1

Bin 2734: 151 of cap free
Amount of items: 2
Items: 
Size: 542468 Color: 0
Size: 457382 Color: 1

Bin 2735: 151 of cap free
Amount of items: 2
Items: 
Size: 551499 Color: 1
Size: 448351 Color: 0

Bin 2736: 151 of cap free
Amount of items: 2
Items: 
Size: 553300 Color: 1
Size: 446550 Color: 0

Bin 2737: 151 of cap free
Amount of items: 2
Items: 
Size: 570341 Color: 0
Size: 429509 Color: 1

Bin 2738: 151 of cap free
Amount of items: 2
Items: 
Size: 576972 Color: 0
Size: 422878 Color: 1

Bin 2739: 151 of cap free
Amount of items: 2
Items: 
Size: 599354 Color: 1
Size: 400496 Color: 0

Bin 2740: 151 of cap free
Amount of items: 2
Items: 
Size: 641527 Color: 0
Size: 358323 Color: 1

Bin 2741: 152 of cap free
Amount of items: 2
Items: 
Size: 532659 Color: 0
Size: 467190 Color: 1

Bin 2742: 152 of cap free
Amount of items: 2
Items: 
Size: 544626 Color: 1
Size: 455223 Color: 0

Bin 2743: 152 of cap free
Amount of items: 2
Items: 
Size: 554703 Color: 1
Size: 445146 Color: 0

Bin 2744: 152 of cap free
Amount of items: 2
Items: 
Size: 562016 Color: 0
Size: 437833 Color: 1

Bin 2745: 152 of cap free
Amount of items: 2
Items: 
Size: 593261 Color: 0
Size: 406588 Color: 1

Bin 2746: 152 of cap free
Amount of items: 2
Items: 
Size: 718250 Color: 1
Size: 281599 Color: 0

Bin 2747: 152 of cap free
Amount of items: 2
Items: 
Size: 775861 Color: 1
Size: 223988 Color: 0

Bin 2748: 153 of cap free
Amount of items: 2
Items: 
Size: 520317 Color: 0
Size: 479531 Color: 1

Bin 2749: 153 of cap free
Amount of items: 2
Items: 
Size: 578793 Color: 0
Size: 421055 Color: 1

Bin 2750: 153 of cap free
Amount of items: 2
Items: 
Size: 590648 Color: 1
Size: 409200 Color: 0

Bin 2751: 153 of cap free
Amount of items: 2
Items: 
Size: 590789 Color: 0
Size: 409059 Color: 1

Bin 2752: 153 of cap free
Amount of items: 2
Items: 
Size: 617026 Color: 0
Size: 382822 Color: 1

Bin 2753: 154 of cap free
Amount of items: 2
Items: 
Size: 586237 Color: 1
Size: 413610 Color: 0

Bin 2754: 154 of cap free
Amount of items: 2
Items: 
Size: 648929 Color: 0
Size: 350918 Color: 1

Bin 2755: 154 of cap free
Amount of items: 2
Items: 
Size: 697475 Color: 0
Size: 302372 Color: 1

Bin 2756: 154 of cap free
Amount of items: 2
Items: 
Size: 712743 Color: 0
Size: 287104 Color: 1

Bin 2757: 154 of cap free
Amount of items: 2
Items: 
Size: 732193 Color: 0
Size: 267654 Color: 1

Bin 2758: 154 of cap free
Amount of items: 2
Items: 
Size: 774491 Color: 0
Size: 225356 Color: 1

Bin 2759: 155 of cap free
Amount of items: 2
Items: 
Size: 561249 Color: 0
Size: 438597 Color: 1

Bin 2760: 155 of cap free
Amount of items: 2
Items: 
Size: 579808 Color: 0
Size: 420038 Color: 1

Bin 2761: 155 of cap free
Amount of items: 2
Items: 
Size: 589784 Color: 1
Size: 410062 Color: 0

Bin 2762: 155 of cap free
Amount of items: 2
Items: 
Size: 651048 Color: 0
Size: 348798 Color: 1

Bin 2763: 155 of cap free
Amount of items: 2
Items: 
Size: 670817 Color: 1
Size: 329029 Color: 0

Bin 2764: 155 of cap free
Amount of items: 2
Items: 
Size: 798721 Color: 1
Size: 201125 Color: 0

Bin 2765: 156 of cap free
Amount of items: 2
Items: 
Size: 503828 Color: 0
Size: 496017 Color: 1

Bin 2766: 156 of cap free
Amount of items: 2
Items: 
Size: 543133 Color: 1
Size: 456712 Color: 0

Bin 2767: 156 of cap free
Amount of items: 2
Items: 
Size: 564595 Color: 1
Size: 435250 Color: 0

Bin 2768: 156 of cap free
Amount of items: 2
Items: 
Size: 670791 Color: 0
Size: 329054 Color: 1

Bin 2769: 156 of cap free
Amount of items: 2
Items: 
Size: 683470 Color: 0
Size: 316375 Color: 1

Bin 2770: 156 of cap free
Amount of items: 2
Items: 
Size: 704216 Color: 1
Size: 295629 Color: 0

Bin 2771: 156 of cap free
Amount of items: 2
Items: 
Size: 716817 Color: 1
Size: 283028 Color: 0

Bin 2772: 156 of cap free
Amount of items: 2
Items: 
Size: 727601 Color: 1
Size: 272244 Color: 0

Bin 2773: 156 of cap free
Amount of items: 2
Items: 
Size: 780895 Color: 0
Size: 218950 Color: 1

Bin 2774: 156 of cap free
Amount of items: 2
Items: 
Size: 791208 Color: 0
Size: 208637 Color: 1

Bin 2775: 157 of cap free
Amount of items: 2
Items: 
Size: 544673 Color: 0
Size: 455171 Color: 1

Bin 2776: 157 of cap free
Amount of items: 2
Items: 
Size: 572429 Color: 0
Size: 427415 Color: 1

Bin 2777: 157 of cap free
Amount of items: 2
Items: 
Size: 686267 Color: 0
Size: 313577 Color: 1

Bin 2778: 157 of cap free
Amount of items: 2
Items: 
Size: 713556 Color: 0
Size: 286288 Color: 1

Bin 2779: 157 of cap free
Amount of items: 2
Items: 
Size: 718631 Color: 1
Size: 281213 Color: 0

Bin 2780: 157 of cap free
Amount of items: 2
Items: 
Size: 791998 Color: 0
Size: 207846 Color: 1

Bin 2781: 158 of cap free
Amount of items: 2
Items: 
Size: 527246 Color: 1
Size: 472597 Color: 0

Bin 2782: 158 of cap free
Amount of items: 2
Items: 
Size: 541820 Color: 1
Size: 458023 Color: 0

Bin 2783: 158 of cap free
Amount of items: 2
Items: 
Size: 565444 Color: 1
Size: 434399 Color: 0

Bin 2784: 158 of cap free
Amount of items: 2
Items: 
Size: 588937 Color: 1
Size: 410906 Color: 0

Bin 2785: 158 of cap free
Amount of items: 2
Items: 
Size: 589191 Color: 0
Size: 410652 Color: 1

Bin 2786: 158 of cap free
Amount of items: 2
Items: 
Size: 602373 Color: 0
Size: 397470 Color: 1

Bin 2787: 158 of cap free
Amount of items: 2
Items: 
Size: 731550 Color: 1
Size: 268293 Color: 0

Bin 2788: 159 of cap free
Amount of items: 2
Items: 
Size: 579853 Color: 1
Size: 419989 Color: 0

Bin 2789: 159 of cap free
Amount of items: 2
Items: 
Size: 598416 Color: 0
Size: 401426 Color: 1

Bin 2790: 159 of cap free
Amount of items: 2
Items: 
Size: 610957 Color: 0
Size: 388885 Color: 1

Bin 2791: 159 of cap free
Amount of items: 2
Items: 
Size: 622423 Color: 0
Size: 377419 Color: 1

Bin 2792: 159 of cap free
Amount of items: 2
Items: 
Size: 650863 Color: 1
Size: 348979 Color: 0

Bin 2793: 159 of cap free
Amount of items: 2
Items: 
Size: 772686 Color: 1
Size: 227156 Color: 0

Bin 2794: 160 of cap free
Amount of items: 2
Items: 
Size: 523814 Color: 1
Size: 476027 Color: 0

Bin 2795: 160 of cap free
Amount of items: 2
Items: 
Size: 564861 Color: 1
Size: 434980 Color: 0

Bin 2796: 160 of cap free
Amount of items: 2
Items: 
Size: 623471 Color: 1
Size: 376370 Color: 0

Bin 2797: 160 of cap free
Amount of items: 2
Items: 
Size: 669387 Color: 0
Size: 330454 Color: 1

Bin 2798: 160 of cap free
Amount of items: 2
Items: 
Size: 679197 Color: 0
Size: 320644 Color: 1

Bin 2799: 161 of cap free
Amount of items: 2
Items: 
Size: 679947 Color: 0
Size: 319893 Color: 1

Bin 2800: 161 of cap free
Amount of items: 2
Items: 
Size: 717309 Color: 0
Size: 282531 Color: 1

Bin 2801: 161 of cap free
Amount of items: 2
Items: 
Size: 725485 Color: 1
Size: 274355 Color: 0

Bin 2802: 161 of cap free
Amount of items: 2
Items: 
Size: 745206 Color: 0
Size: 254634 Color: 1

Bin 2803: 161 of cap free
Amount of items: 2
Items: 
Size: 750752 Color: 0
Size: 249088 Color: 1

Bin 2804: 161 of cap free
Amount of items: 2
Items: 
Size: 793991 Color: 0
Size: 205849 Color: 1

Bin 2805: 162 of cap free
Amount of items: 2
Items: 
Size: 606612 Color: 0
Size: 393227 Color: 1

Bin 2806: 162 of cap free
Amount of items: 2
Items: 
Size: 645924 Color: 1
Size: 353915 Color: 0

Bin 2807: 162 of cap free
Amount of items: 2
Items: 
Size: 669687 Color: 0
Size: 330152 Color: 1

Bin 2808: 162 of cap free
Amount of items: 2
Items: 
Size: 787229 Color: 1
Size: 212610 Color: 0

Bin 2809: 162 of cap free
Amount of items: 2
Items: 
Size: 751443 Color: 0
Size: 248396 Color: 1

Bin 2810: 163 of cap free
Amount of items: 2
Items: 
Size: 526132 Color: 1
Size: 473706 Color: 0

Bin 2811: 163 of cap free
Amount of items: 2
Items: 
Size: 540497 Color: 1
Size: 459341 Color: 0

Bin 2812: 163 of cap free
Amount of items: 2
Items: 
Size: 551683 Color: 1
Size: 448155 Color: 0

Bin 2813: 163 of cap free
Amount of items: 2
Items: 
Size: 564128 Color: 1
Size: 435710 Color: 0

Bin 2814: 163 of cap free
Amount of items: 2
Items: 
Size: 588770 Color: 0
Size: 411068 Color: 1

Bin 2815: 163 of cap free
Amount of items: 2
Items: 
Size: 633235 Color: 0
Size: 366603 Color: 1

Bin 2816: 163 of cap free
Amount of items: 2
Items: 
Size: 789787 Color: 0
Size: 210051 Color: 1

Bin 2817: 164 of cap free
Amount of items: 2
Items: 
Size: 770483 Color: 0
Size: 229354 Color: 1

Bin 2818: 164 of cap free
Amount of items: 2
Items: 
Size: 532160 Color: 0
Size: 467677 Color: 1

Bin 2819: 164 of cap free
Amount of items: 2
Items: 
Size: 537026 Color: 1
Size: 462811 Color: 0

Bin 2820: 164 of cap free
Amount of items: 2
Items: 
Size: 609577 Color: 1
Size: 390260 Color: 0

Bin 2821: 164 of cap free
Amount of items: 2
Items: 
Size: 695862 Color: 1
Size: 303975 Color: 0

Bin 2822: 165 of cap free
Amount of items: 2
Items: 
Size: 583194 Color: 0
Size: 416642 Color: 1

Bin 2823: 165 of cap free
Amount of items: 2
Items: 
Size: 612665 Color: 1
Size: 387171 Color: 0

Bin 2824: 165 of cap free
Amount of items: 2
Items: 
Size: 645318 Color: 1
Size: 354518 Color: 0

Bin 2825: 165 of cap free
Amount of items: 2
Items: 
Size: 661825 Color: 1
Size: 338011 Color: 0

Bin 2826: 165 of cap free
Amount of items: 2
Items: 
Size: 719794 Color: 1
Size: 280042 Color: 0

Bin 2827: 165 of cap free
Amount of items: 2
Items: 
Size: 761991 Color: 0
Size: 237845 Color: 1

Bin 2828: 165 of cap free
Amount of items: 2
Items: 
Size: 786671 Color: 0
Size: 213165 Color: 1

Bin 2829: 166 of cap free
Amount of items: 2
Items: 
Size: 509449 Color: 0
Size: 490386 Color: 1

Bin 2830: 166 of cap free
Amount of items: 2
Items: 
Size: 517011 Color: 0
Size: 482824 Color: 1

Bin 2831: 166 of cap free
Amount of items: 2
Items: 
Size: 536488 Color: 1
Size: 463347 Color: 0

Bin 2832: 166 of cap free
Amount of items: 2
Items: 
Size: 594096 Color: 0
Size: 405739 Color: 1

Bin 2833: 166 of cap free
Amount of items: 2
Items: 
Size: 721974 Color: 1
Size: 277861 Color: 0

Bin 2834: 167 of cap free
Amount of items: 2
Items: 
Size: 730897 Color: 1
Size: 268937 Color: 0

Bin 2835: 167 of cap free
Amount of items: 2
Items: 
Size: 595166 Color: 1
Size: 404668 Color: 0

Bin 2836: 167 of cap free
Amount of items: 2
Items: 
Size: 670168 Color: 0
Size: 329666 Color: 1

Bin 2837: 167 of cap free
Amount of items: 2
Items: 
Size: 678860 Color: 0
Size: 320974 Color: 1

Bin 2838: 168 of cap free
Amount of items: 2
Items: 
Size: 524880 Color: 0
Size: 474953 Color: 1

Bin 2839: 168 of cap free
Amount of items: 2
Items: 
Size: 589671 Color: 0
Size: 410162 Color: 1

Bin 2840: 168 of cap free
Amount of items: 2
Items: 
Size: 624433 Color: 0
Size: 375400 Color: 1

Bin 2841: 168 of cap free
Amount of items: 2
Items: 
Size: 668883 Color: 1
Size: 330950 Color: 0

Bin 2842: 168 of cap free
Amount of items: 2
Items: 
Size: 701420 Color: 0
Size: 298413 Color: 1

Bin 2843: 168 of cap free
Amount of items: 2
Items: 
Size: 756321 Color: 1
Size: 243512 Color: 0

Bin 2844: 168 of cap free
Amount of items: 3
Items: 
Size: 369913 Color: 0
Size: 317519 Color: 0
Size: 312401 Color: 1

Bin 2845: 169 of cap free
Amount of items: 2
Items: 
Size: 515017 Color: 1
Size: 484815 Color: 0

Bin 2846: 169 of cap free
Amount of items: 2
Items: 
Size: 536122 Color: 0
Size: 463710 Color: 1

Bin 2847: 169 of cap free
Amount of items: 2
Items: 
Size: 594674 Color: 1
Size: 405158 Color: 0

Bin 2848: 169 of cap free
Amount of items: 2
Items: 
Size: 608334 Color: 0
Size: 391498 Color: 1

Bin 2849: 169 of cap free
Amount of items: 2
Items: 
Size: 750412 Color: 1
Size: 249420 Color: 0

Bin 2850: 169 of cap free
Amount of items: 2
Items: 
Size: 760423 Color: 1
Size: 239409 Color: 0

Bin 2851: 169 of cap free
Amount of items: 2
Items: 
Size: 786334 Color: 1
Size: 213498 Color: 0

Bin 2852: 170 of cap free
Amount of items: 2
Items: 
Size: 537884 Color: 1
Size: 461947 Color: 0

Bin 2853: 170 of cap free
Amount of items: 2
Items: 
Size: 619649 Color: 1
Size: 380182 Color: 0

Bin 2854: 170 of cap free
Amount of items: 2
Items: 
Size: 634548 Color: 1
Size: 365283 Color: 0

Bin 2855: 170 of cap free
Amount of items: 2
Items: 
Size: 664838 Color: 1
Size: 334993 Color: 0

Bin 2856: 171 of cap free
Amount of items: 2
Items: 
Size: 526141 Color: 0
Size: 473689 Color: 1

Bin 2857: 171 of cap free
Amount of items: 2
Items: 
Size: 585287 Color: 0
Size: 414543 Color: 1

Bin 2858: 171 of cap free
Amount of items: 2
Items: 
Size: 650270 Color: 0
Size: 349560 Color: 1

Bin 2859: 171 of cap free
Amount of items: 2
Items: 
Size: 657936 Color: 0
Size: 341894 Color: 1

Bin 2860: 171 of cap free
Amount of items: 2
Items: 
Size: 717497 Color: 0
Size: 282333 Color: 1

Bin 2861: 171 of cap free
Amount of items: 2
Items: 
Size: 719217 Color: 0
Size: 280613 Color: 1

Bin 2862: 171 of cap free
Amount of items: 2
Items: 
Size: 730626 Color: 0
Size: 269204 Color: 1

Bin 2863: 172 of cap free
Amount of items: 2
Items: 
Size: 623258 Color: 0
Size: 376571 Color: 1

Bin 2864: 172 of cap free
Amount of items: 2
Items: 
Size: 665637 Color: 1
Size: 334192 Color: 0

Bin 2865: 172 of cap free
Amount of items: 2
Items: 
Size: 780092 Color: 0
Size: 219737 Color: 1

Bin 2866: 173 of cap free
Amount of items: 2
Items: 
Size: 574170 Color: 1
Size: 425658 Color: 0

Bin 2867: 173 of cap free
Amount of items: 2
Items: 
Size: 616527 Color: 1
Size: 383301 Color: 0

Bin 2868: 173 of cap free
Amount of items: 2
Items: 
Size: 727150 Color: 1
Size: 272678 Color: 0

Bin 2869: 174 of cap free
Amount of items: 2
Items: 
Size: 656394 Color: 0
Size: 343433 Color: 1

Bin 2870: 174 of cap free
Amount of items: 2
Items: 
Size: 521808 Color: 1
Size: 478019 Color: 0

Bin 2871: 174 of cap free
Amount of items: 2
Items: 
Size: 646916 Color: 1
Size: 352911 Color: 0

Bin 2872: 175 of cap free
Amount of items: 2
Items: 
Size: 704789 Color: 1
Size: 295037 Color: 0

Bin 2873: 175 of cap free
Amount of items: 2
Items: 
Size: 541289 Color: 1
Size: 458537 Color: 0

Bin 2874: 175 of cap free
Amount of items: 2
Items: 
Size: 604233 Color: 1
Size: 395593 Color: 0

Bin 2875: 175 of cap free
Amount of items: 2
Items: 
Size: 662651 Color: 1
Size: 337175 Color: 0

Bin 2876: 175 of cap free
Amount of items: 2
Items: 
Size: 699534 Color: 0
Size: 300292 Color: 1

Bin 2877: 175 of cap free
Amount of items: 2
Items: 
Size: 718433 Color: 1
Size: 281393 Color: 0

Bin 2878: 176 of cap free
Amount of items: 2
Items: 
Size: 622217 Color: 0
Size: 377608 Color: 1

Bin 2879: 176 of cap free
Amount of items: 2
Items: 
Size: 507267 Color: 1
Size: 492558 Color: 0

Bin 2880: 176 of cap free
Amount of items: 2
Items: 
Size: 614192 Color: 0
Size: 385633 Color: 1

Bin 2881: 176 of cap free
Amount of items: 2
Items: 
Size: 654501 Color: 1
Size: 345324 Color: 0

Bin 2882: 176 of cap free
Amount of items: 2
Items: 
Size: 691947 Color: 0
Size: 307878 Color: 1

Bin 2883: 176 of cap free
Amount of items: 2
Items: 
Size: 743969 Color: 0
Size: 255856 Color: 1

Bin 2884: 176 of cap free
Amount of items: 2
Items: 
Size: 745322 Color: 1
Size: 254503 Color: 0

Bin 2885: 176 of cap free
Amount of items: 2
Items: 
Size: 770686 Color: 1
Size: 229139 Color: 0

Bin 2886: 176 of cap free
Amount of items: 2
Items: 
Size: 788507 Color: 0
Size: 211318 Color: 1

Bin 2887: 177 of cap free
Amount of items: 2
Items: 
Size: 794861 Color: 0
Size: 204963 Color: 1

Bin 2888: 177 of cap free
Amount of items: 2
Items: 
Size: 528270 Color: 0
Size: 471554 Color: 1

Bin 2889: 177 of cap free
Amount of items: 2
Items: 
Size: 636300 Color: 1
Size: 363524 Color: 0

Bin 2890: 177 of cap free
Amount of items: 2
Items: 
Size: 692000 Color: 1
Size: 307824 Color: 0

Bin 2891: 177 of cap free
Amount of items: 2
Items: 
Size: 694440 Color: 1
Size: 305384 Color: 0

Bin 2892: 177 of cap free
Amount of items: 2
Items: 
Size: 700262 Color: 1
Size: 299562 Color: 0

Bin 2893: 177 of cap free
Amount of items: 2
Items: 
Size: 717483 Color: 1
Size: 282341 Color: 0

Bin 2894: 177 of cap free
Amount of items: 2
Items: 
Size: 623158 Color: 1
Size: 376666 Color: 0

Bin 2895: 178 of cap free
Amount of items: 2
Items: 
Size: 618419 Color: 1
Size: 381404 Color: 0

Bin 2896: 178 of cap free
Amount of items: 2
Items: 
Size: 643774 Color: 0
Size: 356049 Color: 1

Bin 2897: 179 of cap free
Amount of items: 2
Items: 
Size: 543752 Color: 1
Size: 456070 Color: 0

Bin 2898: 179 of cap free
Amount of items: 2
Items: 
Size: 620940 Color: 1
Size: 378882 Color: 0

Bin 2899: 179 of cap free
Amount of items: 2
Items: 
Size: 691782 Color: 1
Size: 308040 Color: 0

Bin 2900: 179 of cap free
Amount of items: 2
Items: 
Size: 693714 Color: 1
Size: 306108 Color: 0

Bin 2901: 179 of cap free
Amount of items: 2
Items: 
Size: 700591 Color: 1
Size: 299231 Color: 0

Bin 2902: 179 of cap free
Amount of items: 2
Items: 
Size: 701144 Color: 0
Size: 298678 Color: 1

Bin 2903: 179 of cap free
Amount of items: 2
Items: 
Size: 791306 Color: 1
Size: 208516 Color: 0

Bin 2904: 179 of cap free
Amount of items: 2
Items: 
Size: 753405 Color: 0
Size: 246417 Color: 1

Bin 2905: 180 of cap free
Amount of items: 2
Items: 
Size: 588360 Color: 0
Size: 411461 Color: 1

Bin 2906: 180 of cap free
Amount of items: 2
Items: 
Size: 596395 Color: 1
Size: 403426 Color: 0

Bin 2907: 180 of cap free
Amount of items: 2
Items: 
Size: 646120 Color: 1
Size: 353701 Color: 0

Bin 2908: 180 of cap free
Amount of items: 2
Items: 
Size: 646649 Color: 1
Size: 353172 Color: 0

Bin 2909: 180 of cap free
Amount of items: 2
Items: 
Size: 652896 Color: 1
Size: 346925 Color: 0

Bin 2910: 180 of cap free
Amount of items: 2
Items: 
Size: 662100 Color: 0
Size: 337721 Color: 1

Bin 2911: 180 of cap free
Amount of items: 2
Items: 
Size: 799090 Color: 1
Size: 200731 Color: 0

Bin 2912: 181 of cap free
Amount of items: 2
Items: 
Size: 509593 Color: 1
Size: 490227 Color: 0

Bin 2913: 181 of cap free
Amount of items: 2
Items: 
Size: 539519 Color: 0
Size: 460301 Color: 1

Bin 2914: 181 of cap free
Amount of items: 2
Items: 
Size: 553514 Color: 1
Size: 446306 Color: 0

Bin 2915: 181 of cap free
Amount of items: 2
Items: 
Size: 619835 Color: 1
Size: 379985 Color: 0

Bin 2916: 181 of cap free
Amount of items: 2
Items: 
Size: 668426 Color: 1
Size: 331394 Color: 0

Bin 2917: 181 of cap free
Amount of items: 2
Items: 
Size: 712969 Color: 1
Size: 286851 Color: 0

Bin 2918: 181 of cap free
Amount of items: 2
Items: 
Size: 739451 Color: 0
Size: 260369 Color: 1

Bin 2919: 181 of cap free
Amount of items: 2
Items: 
Size: 785141 Color: 1
Size: 214679 Color: 0

Bin 2920: 182 of cap free
Amount of items: 2
Items: 
Size: 542111 Color: 1
Size: 457708 Color: 0

Bin 2921: 182 of cap free
Amount of items: 2
Items: 
Size: 569155 Color: 1
Size: 430664 Color: 0

Bin 2922: 182 of cap free
Amount of items: 2
Items: 
Size: 765861 Color: 1
Size: 233958 Color: 0

Bin 2923: 183 of cap free
Amount of items: 2
Items: 
Size: 619913 Color: 0
Size: 379905 Color: 1

Bin 2924: 183 of cap free
Amount of items: 2
Items: 
Size: 620397 Color: 1
Size: 379421 Color: 0

Bin 2925: 183 of cap free
Amount of items: 2
Items: 
Size: 718892 Color: 0
Size: 280926 Color: 1

Bin 2926: 183 of cap free
Amount of items: 2
Items: 
Size: 791116 Color: 1
Size: 208702 Color: 0

Bin 2927: 183 of cap free
Amount of items: 2
Items: 
Size: 749236 Color: 1
Size: 250582 Color: 0

Bin 2928: 184 of cap free
Amount of items: 2
Items: 
Size: 783751 Color: 0
Size: 216066 Color: 1

Bin 2929: 184 of cap free
Amount of items: 2
Items: 
Size: 509280 Color: 1
Size: 490537 Color: 0

Bin 2930: 184 of cap free
Amount of items: 2
Items: 
Size: 510661 Color: 0
Size: 489156 Color: 1

Bin 2931: 184 of cap free
Amount of items: 2
Items: 
Size: 569906 Color: 1
Size: 429911 Color: 0

Bin 2932: 184 of cap free
Amount of items: 2
Items: 
Size: 712002 Color: 1
Size: 287815 Color: 0

Bin 2933: 185 of cap free
Amount of items: 2
Items: 
Size: 602613 Color: 0
Size: 397203 Color: 1

Bin 2934: 185 of cap free
Amount of items: 2
Items: 
Size: 615685 Color: 0
Size: 384131 Color: 1

Bin 2935: 185 of cap free
Amount of items: 2
Items: 
Size: 641839 Color: 1
Size: 357977 Color: 0

Bin 2936: 185 of cap free
Amount of items: 2
Items: 
Size: 659636 Color: 1
Size: 340180 Color: 0

Bin 2937: 186 of cap free
Amount of items: 2
Items: 
Size: 783500 Color: 0
Size: 216315 Color: 1

Bin 2938: 186 of cap free
Amount of items: 2
Items: 
Size: 588545 Color: 0
Size: 411270 Color: 1

Bin 2939: 186 of cap free
Amount of items: 2
Items: 
Size: 592649 Color: 1
Size: 407166 Color: 0

Bin 2940: 186 of cap free
Amount of items: 2
Items: 
Size: 634113 Color: 1
Size: 365702 Color: 0

Bin 2941: 186 of cap free
Amount of items: 2
Items: 
Size: 684324 Color: 1
Size: 315491 Color: 0

Bin 2942: 186 of cap free
Amount of items: 2
Items: 
Size: 720044 Color: 0
Size: 279771 Color: 1

Bin 2943: 186 of cap free
Amount of items: 2
Items: 
Size: 738070 Color: 1
Size: 261745 Color: 0

Bin 2944: 186 of cap free
Amount of items: 2
Items: 
Size: 753156 Color: 0
Size: 246659 Color: 1

Bin 2945: 186 of cap free
Amount of items: 2
Items: 
Size: 797716 Color: 0
Size: 202099 Color: 1

Bin 2946: 187 of cap free
Amount of items: 2
Items: 
Size: 609031 Color: 1
Size: 390783 Color: 0

Bin 2947: 187 of cap free
Amount of items: 2
Items: 
Size: 707001 Color: 0
Size: 292813 Color: 1

Bin 2948: 187 of cap free
Amount of items: 2
Items: 
Size: 797140 Color: 1
Size: 202674 Color: 0

Bin 2949: 187 of cap free
Amount of items: 2
Items: 
Size: 764413 Color: 0
Size: 235401 Color: 1

Bin 2950: 188 of cap free
Amount of items: 2
Items: 
Size: 650858 Color: 1
Size: 348955 Color: 0

Bin 2951: 188 of cap free
Amount of items: 2
Items: 
Size: 692597 Color: 1
Size: 307216 Color: 0

Bin 2952: 188 of cap free
Amount of items: 2
Items: 
Size: 765141 Color: 0
Size: 234672 Color: 1

Bin 2953: 190 of cap free
Amount of items: 2
Items: 
Size: 514083 Color: 0
Size: 485728 Color: 1

Bin 2954: 190 of cap free
Amount of items: 2
Items: 
Size: 539312 Color: 0
Size: 460499 Color: 1

Bin 2955: 190 of cap free
Amount of items: 2
Items: 
Size: 654938 Color: 1
Size: 344873 Color: 0

Bin 2956: 190 of cap free
Amount of items: 2
Items: 
Size: 691481 Color: 1
Size: 308330 Color: 0

Bin 2957: 190 of cap free
Amount of items: 2
Items: 
Size: 707451 Color: 1
Size: 292360 Color: 0

Bin 2958: 190 of cap free
Amount of items: 2
Items: 
Size: 739860 Color: 1
Size: 259951 Color: 0

Bin 2959: 191 of cap free
Amount of items: 2
Items: 
Size: 574743 Color: 1
Size: 425067 Color: 0

Bin 2960: 191 of cap free
Amount of items: 2
Items: 
Size: 545669 Color: 0
Size: 454141 Color: 1

Bin 2961: 191 of cap free
Amount of items: 2
Items: 
Size: 626774 Color: 1
Size: 373036 Color: 0

Bin 2962: 191 of cap free
Amount of items: 2
Items: 
Size: 726395 Color: 1
Size: 273415 Color: 0

Bin 2963: 191 of cap free
Amount of items: 2
Items: 
Size: 758463 Color: 0
Size: 241347 Color: 1

Bin 2964: 191 of cap free
Amount of items: 2
Items: 
Size: 763189 Color: 1
Size: 236621 Color: 0

Bin 2965: 192 of cap free
Amount of items: 2
Items: 
Size: 501108 Color: 1
Size: 498701 Color: 0

Bin 2966: 192 of cap free
Amount of items: 2
Items: 
Size: 534113 Color: 1
Size: 465696 Color: 0

Bin 2967: 192 of cap free
Amount of items: 2
Items: 
Size: 710097 Color: 0
Size: 289712 Color: 1

Bin 2968: 193 of cap free
Amount of items: 2
Items: 
Size: 627237 Color: 0
Size: 372571 Color: 1

Bin 2969: 193 of cap free
Amount of items: 2
Items: 
Size: 633040 Color: 0
Size: 366768 Color: 1

Bin 2970: 195 of cap free
Amount of items: 2
Items: 
Size: 532441 Color: 1
Size: 467365 Color: 0

Bin 2971: 195 of cap free
Amount of items: 2
Items: 
Size: 561992 Color: 0
Size: 437814 Color: 1

Bin 2972: 195 of cap free
Amount of items: 2
Items: 
Size: 624685 Color: 1
Size: 375121 Color: 0

Bin 2973: 195 of cap free
Amount of items: 2
Items: 
Size: 686557 Color: 1
Size: 313249 Color: 0

Bin 2974: 195 of cap free
Amount of items: 2
Items: 
Size: 797155 Color: 0
Size: 202651 Color: 1

Bin 2975: 196 of cap free
Amount of items: 2
Items: 
Size: 647917 Color: 0
Size: 351888 Color: 1

Bin 2976: 196 of cap free
Amount of items: 2
Items: 
Size: 677947 Color: 1
Size: 321858 Color: 0

Bin 2977: 196 of cap free
Amount of items: 2
Items: 
Size: 771439 Color: 0
Size: 228366 Color: 1

Bin 2978: 197 of cap free
Amount of items: 2
Items: 
Size: 694914 Color: 0
Size: 304890 Color: 1

Bin 2979: 197 of cap free
Amount of items: 2
Items: 
Size: 518503 Color: 0
Size: 481301 Color: 1

Bin 2980: 197 of cap free
Amount of items: 2
Items: 
Size: 552630 Color: 0
Size: 447174 Color: 1

Bin 2981: 197 of cap free
Amount of items: 2
Items: 
Size: 599152 Color: 1
Size: 400652 Color: 0

Bin 2982: 197 of cap free
Amount of items: 2
Items: 
Size: 677527 Color: 0
Size: 322277 Color: 1

Bin 2983: 197 of cap free
Amount of items: 2
Items: 
Size: 714895 Color: 1
Size: 284909 Color: 0

Bin 2984: 197 of cap free
Amount of items: 2
Items: 
Size: 771151 Color: 0
Size: 228653 Color: 1

Bin 2985: 197 of cap free
Amount of items: 2
Items: 
Size: 784179 Color: 0
Size: 215625 Color: 1

Bin 2986: 198 of cap free
Amount of items: 2
Items: 
Size: 628050 Color: 0
Size: 371753 Color: 1

Bin 2987: 199 of cap free
Amount of items: 2
Items: 
Size: 571803 Color: 1
Size: 427999 Color: 0

Bin 2988: 199 of cap free
Amount of items: 2
Items: 
Size: 574441 Color: 0
Size: 425361 Color: 1

Bin 2989: 199 of cap free
Amount of items: 2
Items: 
Size: 680158 Color: 1
Size: 319644 Color: 0

Bin 2990: 199 of cap free
Amount of items: 2
Items: 
Size: 691983 Color: 1
Size: 307819 Color: 0

Bin 2991: 199 of cap free
Amount of items: 2
Items: 
Size: 702870 Color: 0
Size: 296932 Color: 1

Bin 2992: 200 of cap free
Amount of items: 2
Items: 
Size: 603658 Color: 0
Size: 396143 Color: 1

Bin 2993: 200 of cap free
Amount of items: 2
Items: 
Size: 611302 Color: 0
Size: 388499 Color: 1

Bin 2994: 200 of cap free
Amount of items: 2
Items: 
Size: 631035 Color: 0
Size: 368766 Color: 1

Bin 2995: 200 of cap free
Amount of items: 2
Items: 
Size: 679495 Color: 1
Size: 320306 Color: 0

Bin 2996: 200 of cap free
Amount of items: 2
Items: 
Size: 755895 Color: 0
Size: 243906 Color: 1

Bin 2997: 200 of cap free
Amount of items: 2
Items: 
Size: 778062 Color: 1
Size: 221739 Color: 0

Bin 2998: 201 of cap free
Amount of items: 2
Items: 
Size: 578544 Color: 0
Size: 421256 Color: 1

Bin 2999: 201 of cap free
Amount of items: 2
Items: 
Size: 604169 Color: 0
Size: 395631 Color: 1

Bin 3000: 201 of cap free
Amount of items: 2
Items: 
Size: 705119 Color: 1
Size: 294681 Color: 0

Bin 3001: 202 of cap free
Amount of items: 2
Items: 
Size: 589333 Color: 1
Size: 410466 Color: 0

Bin 3002: 202 of cap free
Amount of items: 2
Items: 
Size: 649492 Color: 1
Size: 350307 Color: 0

Bin 3003: 202 of cap free
Amount of items: 2
Items: 
Size: 676052 Color: 0
Size: 323747 Color: 1

Bin 3004: 203 of cap free
Amount of items: 2
Items: 
Size: 505975 Color: 1
Size: 493823 Color: 0

Bin 3005: 203 of cap free
Amount of items: 2
Items: 
Size: 513651 Color: 0
Size: 486147 Color: 1

Bin 3006: 203 of cap free
Amount of items: 2
Items: 
Size: 629805 Color: 0
Size: 369993 Color: 1

Bin 3007: 203 of cap free
Amount of items: 2
Items: 
Size: 744380 Color: 0
Size: 255418 Color: 1

Bin 3008: 203 of cap free
Amount of items: 2
Items: 
Size: 652088 Color: 0
Size: 347710 Color: 1

Bin 3009: 204 of cap free
Amount of items: 2
Items: 
Size: 510064 Color: 0
Size: 489733 Color: 1

Bin 3010: 204 of cap free
Amount of items: 2
Items: 
Size: 512279 Color: 1
Size: 487518 Color: 0

Bin 3011: 204 of cap free
Amount of items: 2
Items: 
Size: 517260 Color: 0
Size: 482537 Color: 1

Bin 3012: 205 of cap free
Amount of items: 2
Items: 
Size: 538357 Color: 0
Size: 461439 Color: 1

Bin 3013: 205 of cap free
Amount of items: 2
Items: 
Size: 595487 Color: 1
Size: 404309 Color: 0

Bin 3014: 205 of cap free
Amount of items: 2
Items: 
Size: 630084 Color: 0
Size: 369712 Color: 1

Bin 3015: 205 of cap free
Amount of items: 2
Items: 
Size: 767394 Color: 0
Size: 232402 Color: 1

Bin 3016: 206 of cap free
Amount of items: 2
Items: 
Size: 560001 Color: 1
Size: 439794 Color: 0

Bin 3017: 206 of cap free
Amount of items: 2
Items: 
Size: 588072 Color: 0
Size: 411723 Color: 1

Bin 3018: 206 of cap free
Amount of items: 2
Items: 
Size: 766793 Color: 0
Size: 233002 Color: 1

Bin 3019: 206 of cap free
Amount of items: 2
Items: 
Size: 767602 Color: 0
Size: 232193 Color: 1

Bin 3020: 206 of cap free
Amount of items: 2
Items: 
Size: 792830 Color: 0
Size: 206965 Color: 1

Bin 3021: 207 of cap free
Amount of items: 2
Items: 
Size: 743313 Color: 1
Size: 256481 Color: 0

Bin 3022: 207 of cap free
Amount of items: 2
Items: 
Size: 509272 Color: 1
Size: 490522 Color: 0

Bin 3023: 207 of cap free
Amount of items: 2
Items: 
Size: 545660 Color: 0
Size: 454134 Color: 1

Bin 3024: 207 of cap free
Amount of items: 2
Items: 
Size: 586489 Color: 0
Size: 413305 Color: 1

Bin 3025: 207 of cap free
Amount of items: 2
Items: 
Size: 645681 Color: 0
Size: 354113 Color: 1

Bin 3026: 207 of cap free
Amount of items: 2
Items: 
Size: 649264 Color: 1
Size: 350530 Color: 0

Bin 3027: 208 of cap free
Amount of items: 2
Items: 
Size: 598530 Color: 1
Size: 401263 Color: 0

Bin 3028: 208 of cap free
Amount of items: 2
Items: 
Size: 699115 Color: 1
Size: 300678 Color: 0

Bin 3029: 209 of cap free
Amount of items: 2
Items: 
Size: 566801 Color: 0
Size: 432991 Color: 1

Bin 3030: 209 of cap free
Amount of items: 2
Items: 
Size: 583647 Color: 1
Size: 416145 Color: 0

Bin 3031: 209 of cap free
Amount of items: 2
Items: 
Size: 651261 Color: 1
Size: 348531 Color: 0

Bin 3032: 209 of cap free
Amount of items: 2
Items: 
Size: 733908 Color: 0
Size: 265884 Color: 1

Bin 3033: 210 of cap free
Amount of items: 2
Items: 
Size: 515365 Color: 1
Size: 484426 Color: 0

Bin 3034: 210 of cap free
Amount of items: 2
Items: 
Size: 536464 Color: 1
Size: 463327 Color: 0

Bin 3035: 210 of cap free
Amount of items: 2
Items: 
Size: 592288 Color: 0
Size: 407503 Color: 1

Bin 3036: 210 of cap free
Amount of items: 2
Items: 
Size: 661059 Color: 1
Size: 338732 Color: 0

Bin 3037: 210 of cap free
Amount of items: 2
Items: 
Size: 678716 Color: 1
Size: 321075 Color: 0

Bin 3038: 210 of cap free
Amount of items: 2
Items: 
Size: 705323 Color: 0
Size: 294468 Color: 1

Bin 3039: 211 of cap free
Amount of items: 2
Items: 
Size: 518838 Color: 1
Size: 480952 Color: 0

Bin 3040: 211 of cap free
Amount of items: 2
Items: 
Size: 530077 Color: 0
Size: 469713 Color: 1

Bin 3041: 211 of cap free
Amount of items: 2
Items: 
Size: 676135 Color: 1
Size: 323655 Color: 0

Bin 3042: 211 of cap free
Amount of items: 2
Items: 
Size: 790678 Color: 0
Size: 209112 Color: 1

Bin 3043: 212 of cap free
Amount of items: 2
Items: 
Size: 624677 Color: 1
Size: 375112 Color: 0

Bin 3044: 212 of cap free
Amount of items: 2
Items: 
Size: 569401 Color: 1
Size: 430388 Color: 0

Bin 3045: 212 of cap free
Amount of items: 2
Items: 
Size: 622565 Color: 1
Size: 377224 Color: 0

Bin 3046: 212 of cap free
Amount of items: 2
Items: 
Size: 713516 Color: 1
Size: 286273 Color: 0

Bin 3047: 213 of cap free
Amount of items: 2
Items: 
Size: 513136 Color: 1
Size: 486652 Color: 0

Bin 3048: 213 of cap free
Amount of items: 2
Items: 
Size: 516039 Color: 0
Size: 483749 Color: 1

Bin 3049: 214 of cap free
Amount of items: 2
Items: 
Size: 534831 Color: 1
Size: 464956 Color: 0

Bin 3050: 214 of cap free
Amount of items: 2
Items: 
Size: 545141 Color: 1
Size: 454646 Color: 0

Bin 3051: 214 of cap free
Amount of items: 2
Items: 
Size: 547646 Color: 0
Size: 452141 Color: 1

Bin 3052: 214 of cap free
Amount of items: 2
Items: 
Size: 579425 Color: 0
Size: 420362 Color: 1

Bin 3053: 214 of cap free
Amount of items: 2
Items: 
Size: 594067 Color: 1
Size: 405720 Color: 0

Bin 3054: 214 of cap free
Amount of items: 2
Items: 
Size: 600486 Color: 1
Size: 399301 Color: 0

Bin 3055: 214 of cap free
Amount of items: 2
Items: 
Size: 706280 Color: 1
Size: 293507 Color: 0

Bin 3056: 214 of cap free
Amount of items: 2
Items: 
Size: 738802 Color: 0
Size: 260985 Color: 1

Bin 3057: 215 of cap free
Amount of items: 2
Items: 
Size: 503455 Color: 1
Size: 496331 Color: 0

Bin 3058: 215 of cap free
Amount of items: 2
Items: 
Size: 543061 Color: 0
Size: 456725 Color: 1

Bin 3059: 215 of cap free
Amount of items: 2
Items: 
Size: 612668 Color: 0
Size: 387118 Color: 1

Bin 3060: 215 of cap free
Amount of items: 2
Items: 
Size: 644022 Color: 1
Size: 355764 Color: 0

Bin 3061: 215 of cap free
Amount of items: 2
Items: 
Size: 658693 Color: 1
Size: 341093 Color: 0

Bin 3062: 215 of cap free
Amount of items: 2
Items: 
Size: 696888 Color: 1
Size: 302898 Color: 0

Bin 3063: 215 of cap free
Amount of items: 2
Items: 
Size: 745069 Color: 1
Size: 254717 Color: 0

Bin 3064: 215 of cap free
Amount of items: 2
Items: 
Size: 672912 Color: 0
Size: 326874 Color: 1

Bin 3065: 215 of cap free
Amount of items: 2
Items: 
Size: 616003 Color: 0
Size: 383783 Color: 1

Bin 3066: 216 of cap free
Amount of items: 2
Items: 
Size: 600071 Color: 1
Size: 399714 Color: 0

Bin 3067: 216 of cap free
Amount of items: 2
Items: 
Size: 641076 Color: 1
Size: 358709 Color: 0

Bin 3068: 216 of cap free
Amount of items: 2
Items: 
Size: 679706 Color: 1
Size: 320079 Color: 0

Bin 3069: 216 of cap free
Amount of items: 2
Items: 
Size: 733081 Color: 1
Size: 266704 Color: 0

Bin 3070: 216 of cap free
Amount of items: 2
Items: 
Size: 753631 Color: 0
Size: 246154 Color: 1

Bin 3071: 216 of cap free
Amount of items: 2
Items: 
Size: 761090 Color: 1
Size: 238695 Color: 0

Bin 3072: 217 of cap free
Amount of items: 2
Items: 
Size: 626468 Color: 1
Size: 373316 Color: 0

Bin 3073: 218 of cap free
Amount of items: 2
Items: 
Size: 540268 Color: 0
Size: 459515 Color: 1

Bin 3074: 218 of cap free
Amount of items: 2
Items: 
Size: 600963 Color: 1
Size: 398820 Color: 0

Bin 3075: 218 of cap free
Amount of items: 2
Items: 
Size: 682562 Color: 0
Size: 317221 Color: 1

Bin 3076: 218 of cap free
Amount of items: 2
Items: 
Size: 707818 Color: 1
Size: 291965 Color: 0

Bin 3077: 219 of cap free
Amount of items: 2
Items: 
Size: 697070 Color: 0
Size: 302712 Color: 1

Bin 3078: 220 of cap free
Amount of items: 2
Items: 
Size: 504913 Color: 0
Size: 494868 Color: 1

Bin 3079: 220 of cap free
Amount of items: 2
Items: 
Size: 567052 Color: 1
Size: 432729 Color: 0

Bin 3080: 220 of cap free
Amount of items: 2
Items: 
Size: 700036 Color: 1
Size: 299745 Color: 0

Bin 3081: 221 of cap free
Amount of items: 2
Items: 
Size: 524262 Color: 0
Size: 475518 Color: 1

Bin 3082: 221 of cap free
Amount of items: 2
Items: 
Size: 625769 Color: 0
Size: 374011 Color: 1

Bin 3083: 221 of cap free
Amount of items: 2
Items: 
Size: 718982 Color: 1
Size: 280798 Color: 0

Bin 3084: 221 of cap free
Amount of items: 2
Items: 
Size: 729464 Color: 1
Size: 270316 Color: 0

Bin 3085: 221 of cap free
Amount of items: 2
Items: 
Size: 783740 Color: 0
Size: 216040 Color: 1

Bin 3086: 222 of cap free
Amount of items: 2
Items: 
Size: 598177 Color: 0
Size: 401602 Color: 1

Bin 3087: 222 of cap free
Amount of items: 2
Items: 
Size: 586984 Color: 0
Size: 412795 Color: 1

Bin 3088: 222 of cap free
Amount of items: 2
Items: 
Size: 661384 Color: 0
Size: 338395 Color: 1

Bin 3089: 223 of cap free
Amount of items: 2
Items: 
Size: 721617 Color: 0
Size: 278161 Color: 1

Bin 3090: 223 of cap free
Amount of items: 2
Items: 
Size: 769197 Color: 0
Size: 230581 Color: 1

Bin 3091: 224 of cap free
Amount of items: 2
Items: 
Size: 551678 Color: 1
Size: 448099 Color: 0

Bin 3092: 224 of cap free
Amount of items: 2
Items: 
Size: 728468 Color: 1
Size: 271309 Color: 0

Bin 3093: 224 of cap free
Amount of items: 2
Items: 
Size: 761975 Color: 0
Size: 237802 Color: 1

Bin 3094: 224 of cap free
Amount of items: 2
Items: 
Size: 767028 Color: 1
Size: 232749 Color: 0

Bin 3095: 225 of cap free
Amount of items: 2
Items: 
Size: 599124 Color: 1
Size: 400652 Color: 0

Bin 3096: 225 of cap free
Amount of items: 2
Items: 
Size: 669773 Color: 1
Size: 330003 Color: 0

Bin 3097: 225 of cap free
Amount of items: 2
Items: 
Size: 771872 Color: 0
Size: 227904 Color: 1

Bin 3098: 225 of cap free
Amount of items: 2
Items: 
Size: 787212 Color: 1
Size: 212564 Color: 0

Bin 3099: 226 of cap free
Amount of items: 2
Items: 
Size: 704743 Color: 1
Size: 295032 Color: 0

Bin 3100: 226 of cap free
Amount of items: 2
Items: 
Size: 634087 Color: 1
Size: 365688 Color: 0

Bin 3101: 226 of cap free
Amount of items: 2
Items: 
Size: 648588 Color: 1
Size: 351187 Color: 0

Bin 3102: 226 of cap free
Amount of items: 2
Items: 
Size: 702869 Color: 0
Size: 296906 Color: 1

Bin 3103: 226 of cap free
Amount of items: 2
Items: 
Size: 754123 Color: 0
Size: 245652 Color: 1

Bin 3104: 227 of cap free
Amount of items: 2
Items: 
Size: 730622 Color: 0
Size: 269152 Color: 1

Bin 3105: 227 of cap free
Amount of items: 2
Items: 
Size: 677127 Color: 1
Size: 322647 Color: 0

Bin 3106: 227 of cap free
Amount of items: 2
Items: 
Size: 724942 Color: 1
Size: 274832 Color: 0

Bin 3107: 228 of cap free
Amount of items: 2
Items: 
Size: 615649 Color: 0
Size: 384124 Color: 1

Bin 3108: 228 of cap free
Amount of items: 2
Items: 
Size: 751390 Color: 0
Size: 248383 Color: 1

Bin 3109: 228 of cap free
Amount of items: 2
Items: 
Size: 532591 Color: 0
Size: 467182 Color: 1

Bin 3110: 228 of cap free
Amount of items: 2
Items: 
Size: 653948 Color: 1
Size: 345825 Color: 0

Bin 3111: 228 of cap free
Amount of items: 2
Items: 
Size: 671626 Color: 1
Size: 328147 Color: 0

Bin 3112: 228 of cap free
Amount of items: 2
Items: 
Size: 704501 Color: 0
Size: 295272 Color: 1

Bin 3113: 230 of cap free
Amount of items: 2
Items: 
Size: 547471 Color: 1
Size: 452300 Color: 0

Bin 3114: 230 of cap free
Amount of items: 2
Items: 
Size: 587834 Color: 0
Size: 411937 Color: 1

Bin 3115: 230 of cap free
Amount of items: 2
Items: 
Size: 757309 Color: 1
Size: 242462 Color: 0

Bin 3116: 230 of cap free
Amount of items: 2
Items: 
Size: 770895 Color: 1
Size: 228876 Color: 0

Bin 3117: 231 of cap free
Amount of items: 2
Items: 
Size: 638627 Color: 0
Size: 361143 Color: 1

Bin 3118: 231 of cap free
Amount of items: 2
Items: 
Size: 668832 Color: 1
Size: 330938 Color: 0

Bin 3119: 231 of cap free
Amount of items: 2
Items: 
Size: 725425 Color: 0
Size: 274345 Color: 1

Bin 3120: 232 of cap free
Amount of items: 2
Items: 
Size: 769750 Color: 0
Size: 230019 Color: 1

Bin 3121: 232 of cap free
Amount of items: 2
Items: 
Size: 508198 Color: 0
Size: 491571 Color: 1

Bin 3122: 232 of cap free
Amount of items: 2
Items: 
Size: 508805 Color: 0
Size: 490964 Color: 1

Bin 3123: 232 of cap free
Amount of items: 2
Items: 
Size: 605938 Color: 0
Size: 393831 Color: 1

Bin 3124: 232 of cap free
Amount of items: 2
Items: 
Size: 657248 Color: 1
Size: 342521 Color: 0

Bin 3125: 232 of cap free
Amount of items: 2
Items: 
Size: 743683 Color: 0
Size: 256086 Color: 1

Bin 3126: 232 of cap free
Amount of items: 2
Items: 
Size: 754647 Color: 1
Size: 245122 Color: 0

Bin 3127: 232 of cap free
Amount of items: 2
Items: 
Size: 768477 Color: 1
Size: 231292 Color: 0

Bin 3128: 233 of cap free
Amount of items: 2
Items: 
Size: 518733 Color: 0
Size: 481035 Color: 1

Bin 3129: 234 of cap free
Amount of items: 2
Items: 
Size: 645414 Color: 0
Size: 354353 Color: 1

Bin 3130: 234 of cap free
Amount of items: 2
Items: 
Size: 739822 Color: 0
Size: 259945 Color: 1

Bin 3131: 234 of cap free
Amount of items: 2
Items: 
Size: 797709 Color: 0
Size: 202058 Color: 1

Bin 3132: 235 of cap free
Amount of items: 2
Items: 
Size: 500371 Color: 0
Size: 499395 Color: 1

Bin 3133: 235 of cap free
Amount of items: 2
Items: 
Size: 661464 Color: 1
Size: 338302 Color: 0

Bin 3134: 236 of cap free
Amount of items: 2
Items: 
Size: 636479 Color: 1
Size: 363286 Color: 0

Bin 3135: 237 of cap free
Amount of items: 2
Items: 
Size: 617805 Color: 0
Size: 381959 Color: 1

Bin 3136: 237 of cap free
Amount of items: 2
Items: 
Size: 740245 Color: 1
Size: 259519 Color: 0

Bin 3137: 238 of cap free
Amount of items: 2
Items: 
Size: 744694 Color: 0
Size: 255069 Color: 1

Bin 3138: 238 of cap free
Amount of items: 2
Items: 
Size: 533868 Color: 0
Size: 465895 Color: 1

Bin 3139: 238 of cap free
Amount of items: 2
Items: 
Size: 614683 Color: 1
Size: 385080 Color: 0

Bin 3140: 238 of cap free
Amount of items: 2
Items: 
Size: 677473 Color: 1
Size: 322290 Color: 0

Bin 3141: 239 of cap free
Amount of items: 2
Items: 
Size: 776754 Color: 0
Size: 223008 Color: 1

Bin 3142: 239 of cap free
Amount of items: 2
Items: 
Size: 729016 Color: 0
Size: 270746 Color: 1

Bin 3143: 239 of cap free
Amount of items: 2
Items: 
Size: 760579 Color: 0
Size: 239183 Color: 1

Bin 3144: 240 of cap free
Amount of items: 2
Items: 
Size: 510322 Color: 0
Size: 489439 Color: 1

Bin 3145: 240 of cap free
Amount of items: 2
Items: 
Size: 515633 Color: 0
Size: 484128 Color: 1

Bin 3146: 240 of cap free
Amount of items: 2
Items: 
Size: 575855 Color: 0
Size: 423906 Color: 1

Bin 3147: 240 of cap free
Amount of items: 2
Items: 
Size: 611360 Color: 1
Size: 388401 Color: 0

Bin 3148: 240 of cap free
Amount of items: 2
Items: 
Size: 755194 Color: 0
Size: 244567 Color: 1

Bin 3149: 241 of cap free
Amount of items: 2
Items: 
Size: 766732 Color: 1
Size: 233028 Color: 0

Bin 3150: 241 of cap free
Amount of items: 2
Items: 
Size: 532862 Color: 0
Size: 466898 Color: 1

Bin 3151: 241 of cap free
Amount of items: 2
Items: 
Size: 558274 Color: 0
Size: 441486 Color: 1

Bin 3152: 241 of cap free
Amount of items: 2
Items: 
Size: 692811 Color: 0
Size: 306949 Color: 1

Bin 3153: 242 of cap free
Amount of items: 2
Items: 
Size: 602023 Color: 0
Size: 397736 Color: 1

Bin 3154: 242 of cap free
Amount of items: 2
Items: 
Size: 620172 Color: 0
Size: 379587 Color: 1

Bin 3155: 242 of cap free
Amount of items: 2
Items: 
Size: 644846 Color: 0
Size: 354913 Color: 1

Bin 3156: 242 of cap free
Amount of items: 2
Items: 
Size: 674153 Color: 1
Size: 325606 Color: 0

Bin 3157: 242 of cap free
Amount of items: 2
Items: 
Size: 764145 Color: 1
Size: 235614 Color: 0

Bin 3158: 243 of cap free
Amount of items: 2
Items: 
Size: 530074 Color: 1
Size: 469684 Color: 0

Bin 3159: 243 of cap free
Amount of items: 2
Items: 
Size: 580271 Color: 0
Size: 419487 Color: 1

Bin 3160: 244 of cap free
Amount of items: 2
Items: 
Size: 510487 Color: 1
Size: 489270 Color: 0

Bin 3161: 244 of cap free
Amount of items: 2
Items: 
Size: 520775 Color: 0
Size: 478982 Color: 1

Bin 3162: 244 of cap free
Amount of items: 2
Items: 
Size: 577303 Color: 1
Size: 422454 Color: 0

Bin 3163: 245 of cap free
Amount of items: 2
Items: 
Size: 729136 Color: 1
Size: 270620 Color: 0

Bin 3164: 246 of cap free
Amount of items: 2
Items: 
Size: 604169 Color: 1
Size: 395586 Color: 0

Bin 3165: 246 of cap free
Amount of items: 2
Items: 
Size: 626814 Color: 0
Size: 372941 Color: 1

Bin 3166: 246 of cap free
Amount of items: 2
Items: 
Size: 632270 Color: 0
Size: 367485 Color: 1

Bin 3167: 246 of cap free
Amount of items: 2
Items: 
Size: 643774 Color: 0
Size: 355981 Color: 1

Bin 3168: 246 of cap free
Amount of items: 2
Items: 
Size: 669306 Color: 0
Size: 330449 Color: 1

Bin 3169: 246 of cap free
Amount of items: 2
Items: 
Size: 670459 Color: 0
Size: 329296 Color: 1

Bin 3170: 246 of cap free
Amount of items: 2
Items: 
Size: 742120 Color: 0
Size: 257635 Color: 1

Bin 3171: 248 of cap free
Amount of items: 2
Items: 
Size: 582207 Color: 0
Size: 417546 Color: 1

Bin 3172: 249 of cap free
Amount of items: 2
Items: 
Size: 548547 Color: 1
Size: 451205 Color: 0

Bin 3173: 249 of cap free
Amount of items: 2
Items: 
Size: 597728 Color: 1
Size: 402024 Color: 0

Bin 3174: 249 of cap free
Amount of items: 2
Items: 
Size: 701992 Color: 0
Size: 297760 Color: 1

Bin 3175: 249 of cap free
Amount of items: 2
Items: 
Size: 761183 Color: 0
Size: 238569 Color: 1

Bin 3176: 250 of cap free
Amount of items: 2
Items: 
Size: 510101 Color: 1
Size: 489650 Color: 0

Bin 3177: 250 of cap free
Amount of items: 2
Items: 
Size: 521818 Color: 0
Size: 477933 Color: 1

Bin 3178: 250 of cap free
Amount of items: 2
Items: 
Size: 695789 Color: 0
Size: 303962 Color: 1

Bin 3179: 251 of cap free
Amount of items: 2
Items: 
Size: 564795 Color: 1
Size: 434955 Color: 0

Bin 3180: 251 of cap free
Amount of items: 2
Items: 
Size: 653368 Color: 1
Size: 346382 Color: 0

Bin 3181: 251 of cap free
Amount of items: 2
Items: 
Size: 781605 Color: 1
Size: 218145 Color: 0

Bin 3182: 252 of cap free
Amount of items: 2
Items: 
Size: 655760 Color: 0
Size: 343989 Color: 1

Bin 3183: 253 of cap free
Amount of items: 3
Items: 
Size: 359669 Color: 0
Size: 356475 Color: 0
Size: 283604 Color: 1

Bin 3184: 253 of cap free
Amount of items: 2
Items: 
Size: 599462 Color: 0
Size: 400286 Color: 1

Bin 3185: 253 of cap free
Amount of items: 2
Items: 
Size: 674568 Color: 1
Size: 325180 Color: 0

Bin 3186: 254 of cap free
Amount of items: 2
Items: 
Size: 506572 Color: 0
Size: 493175 Color: 1

Bin 3187: 254 of cap free
Amount of items: 2
Items: 
Size: 524570 Color: 1
Size: 475177 Color: 0

Bin 3188: 254 of cap free
Amount of items: 2
Items: 
Size: 600665 Color: 0
Size: 399082 Color: 1

Bin 3189: 254 of cap free
Amount of items: 2
Items: 
Size: 659363 Color: 0
Size: 340384 Color: 1

Bin 3190: 255 of cap free
Amount of items: 2
Items: 
Size: 517943 Color: 1
Size: 481803 Color: 0

Bin 3191: 255 of cap free
Amount of items: 2
Items: 
Size: 583638 Color: 1
Size: 416108 Color: 0

Bin 3192: 255 of cap free
Amount of items: 2
Items: 
Size: 747879 Color: 1
Size: 251867 Color: 0

Bin 3193: 256 of cap free
Amount of items: 2
Items: 
Size: 628032 Color: 0
Size: 371713 Color: 1

Bin 3194: 256 of cap free
Amount of items: 2
Items: 
Size: 669667 Color: 0
Size: 330078 Color: 1

Bin 3195: 256 of cap free
Amount of items: 2
Items: 
Size: 727943 Color: 1
Size: 271802 Color: 0

Bin 3196: 256 of cap free
Amount of items: 2
Items: 
Size: 769156 Color: 1
Size: 230589 Color: 0

Bin 3197: 256 of cap free
Amount of items: 2
Items: 
Size: 786296 Color: 1
Size: 213449 Color: 0

Bin 3198: 257 of cap free
Amount of items: 2
Items: 
Size: 573210 Color: 1
Size: 426534 Color: 0

Bin 3199: 257 of cap free
Amount of items: 2
Items: 
Size: 583275 Color: 1
Size: 416469 Color: 0

Bin 3200: 257 of cap free
Amount of items: 2
Items: 
Size: 693227 Color: 1
Size: 306517 Color: 0

Bin 3201: 257 of cap free
Amount of items: 2
Items: 
Size: 710575 Color: 1
Size: 289169 Color: 0

Bin 3202: 258 of cap free
Amount of items: 2
Items: 
Size: 763794 Color: 0
Size: 235949 Color: 1

Bin 3203: 258 of cap free
Amount of items: 2
Items: 
Size: 650246 Color: 0
Size: 349497 Color: 1

Bin 3204: 259 of cap free
Amount of items: 2
Items: 
Size: 591922 Color: 1
Size: 407820 Color: 0

Bin 3205: 260 of cap free
Amount of items: 2
Items: 
Size: 540202 Color: 1
Size: 459539 Color: 0

Bin 3206: 261 of cap free
Amount of items: 2
Items: 
Size: 505604 Color: 1
Size: 494136 Color: 0

Bin 3207: 261 of cap free
Amount of items: 2
Items: 
Size: 537418 Color: 0
Size: 462322 Color: 1

Bin 3208: 261 of cap free
Amount of items: 2
Items: 
Size: 724316 Color: 1
Size: 275424 Color: 0

Bin 3209: 262 of cap free
Amount of items: 2
Items: 
Size: 590250 Color: 0
Size: 409489 Color: 1

Bin 3210: 264 of cap free
Amount of items: 2
Items: 
Size: 617293 Color: 0
Size: 382444 Color: 1

Bin 3211: 264 of cap free
Amount of items: 2
Items: 
Size: 623413 Color: 1
Size: 376324 Color: 0

Bin 3212: 264 of cap free
Amount of items: 2
Items: 
Size: 748566 Color: 0
Size: 251171 Color: 1

Bin 3213: 264 of cap free
Amount of items: 2
Items: 
Size: 757720 Color: 0
Size: 242017 Color: 1

Bin 3214: 265 of cap free
Amount of items: 2
Items: 
Size: 573308 Color: 0
Size: 426428 Color: 1

Bin 3215: 265 of cap free
Amount of items: 2
Items: 
Size: 576269 Color: 0
Size: 423467 Color: 1

Bin 3216: 265 of cap free
Amount of items: 2
Items: 
Size: 714867 Color: 0
Size: 284869 Color: 1

Bin 3217: 266 of cap free
Amount of items: 2
Items: 
Size: 602794 Color: 1
Size: 396941 Color: 0

Bin 3218: 266 of cap free
Amount of items: 2
Items: 
Size: 603871 Color: 1
Size: 395864 Color: 0

Bin 3219: 267 of cap free
Amount of items: 2
Items: 
Size: 705491 Color: 1
Size: 294243 Color: 0

Bin 3220: 267 of cap free
Amount of items: 2
Items: 
Size: 660507 Color: 0
Size: 339227 Color: 1

Bin 3221: 267 of cap free
Amount of items: 2
Items: 
Size: 768449 Color: 0
Size: 231285 Color: 1

Bin 3222: 267 of cap free
Amount of items: 2
Items: 
Size: 759525 Color: 0
Size: 240209 Color: 1

Bin 3223: 268 of cap free
Amount of items: 2
Items: 
Size: 636639 Color: 0
Size: 363094 Color: 1

Bin 3224: 268 of cap free
Amount of items: 2
Items: 
Size: 680998 Color: 0
Size: 318735 Color: 1

Bin 3225: 268 of cap free
Amount of items: 2
Items: 
Size: 697473 Color: 1
Size: 302260 Color: 0

Bin 3226: 268 of cap free
Amount of items: 2
Items: 
Size: 698088 Color: 1
Size: 301645 Color: 0

Bin 3227: 268 of cap free
Amount of items: 2
Items: 
Size: 714051 Color: 1
Size: 285682 Color: 0

Bin 3228: 268 of cap free
Amount of items: 2
Items: 
Size: 724219 Color: 0
Size: 275514 Color: 1

Bin 3229: 269 of cap free
Amount of items: 2
Items: 
Size: 504500 Color: 1
Size: 495232 Color: 0

Bin 3230: 269 of cap free
Amount of items: 2
Items: 
Size: 618600 Color: 1
Size: 381132 Color: 0

Bin 3231: 269 of cap free
Amount of items: 2
Items: 
Size: 689615 Color: 1
Size: 310117 Color: 0

Bin 3232: 269 of cap free
Amount of items: 2
Items: 
Size: 719458 Color: 0
Size: 280274 Color: 1

Bin 3233: 269 of cap free
Amount of items: 2
Items: 
Size: 788848 Color: 0
Size: 210884 Color: 1

Bin 3234: 270 of cap free
Amount of items: 2
Items: 
Size: 613131 Color: 1
Size: 386600 Color: 0

Bin 3235: 270 of cap free
Amount of items: 2
Items: 
Size: 614024 Color: 1
Size: 385707 Color: 0

Bin 3236: 271 of cap free
Amount of items: 2
Items: 
Size: 538304 Color: 1
Size: 461426 Color: 0

Bin 3237: 271 of cap free
Amount of items: 2
Items: 
Size: 631486 Color: 1
Size: 368244 Color: 0

Bin 3238: 271 of cap free
Amount of items: 2
Items: 
Size: 731683 Color: 0
Size: 268047 Color: 1

Bin 3239: 271 of cap free
Amount of items: 2
Items: 
Size: 776945 Color: 1
Size: 222785 Color: 0

Bin 3240: 272 of cap free
Amount of items: 2
Items: 
Size: 609478 Color: 1
Size: 390251 Color: 0

Bin 3241: 272 of cap free
Amount of items: 2
Items: 
Size: 738766 Color: 1
Size: 260963 Color: 0

Bin 3242: 272 of cap free
Amount of items: 2
Items: 
Size: 746691 Color: 0
Size: 253038 Color: 1

Bin 3243: 273 of cap free
Amount of items: 2
Items: 
Size: 541493 Color: 0
Size: 458235 Color: 1

Bin 3244: 273 of cap free
Amount of items: 2
Items: 
Size: 585557 Color: 1
Size: 414171 Color: 0

Bin 3245: 273 of cap free
Amount of items: 2
Items: 
Size: 706565 Color: 1
Size: 293163 Color: 0

Bin 3246: 273 of cap free
Amount of items: 2
Items: 
Size: 606819 Color: 1
Size: 392909 Color: 0

Bin 3247: 274 of cap free
Amount of items: 2
Items: 
Size: 510098 Color: 1
Size: 489629 Color: 0

Bin 3248: 274 of cap free
Amount of items: 2
Items: 
Size: 546322 Color: 1
Size: 453405 Color: 0

Bin 3249: 274 of cap free
Amount of items: 2
Items: 
Size: 690116 Color: 1
Size: 309611 Color: 0

Bin 3250: 274 of cap free
Amount of items: 2
Items: 
Size: 757194 Color: 0
Size: 242533 Color: 1

Bin 3251: 274 of cap free
Amount of items: 2
Items: 
Size: 773961 Color: 1
Size: 225766 Color: 0

Bin 3252: 275 of cap free
Amount of items: 2
Items: 
Size: 774602 Color: 1
Size: 225124 Color: 0

Bin 3253: 276 of cap free
Amount of items: 2
Items: 
Size: 532569 Color: 0
Size: 467156 Color: 1

Bin 3254: 276 of cap free
Amount of items: 2
Items: 
Size: 603224 Color: 1
Size: 396501 Color: 0

Bin 3255: 276 of cap free
Amount of items: 2
Items: 
Size: 613239 Color: 0
Size: 386486 Color: 1

Bin 3256: 276 of cap free
Amount of items: 2
Items: 
Size: 657060 Color: 0
Size: 342665 Color: 1

Bin 3257: 277 of cap free
Amount of items: 2
Items: 
Size: 764615 Color: 1
Size: 235109 Color: 0

Bin 3258: 278 of cap free
Amount of items: 2
Items: 
Size: 505839 Color: 0
Size: 493884 Color: 1

Bin 3259: 278 of cap free
Amount of items: 2
Items: 
Size: 557307 Color: 1
Size: 442416 Color: 0

Bin 3260: 278 of cap free
Amount of items: 2
Items: 
Size: 565053 Color: 1
Size: 434670 Color: 0

Bin 3261: 279 of cap free
Amount of items: 2
Items: 
Size: 500342 Color: 0
Size: 499380 Color: 1

Bin 3262: 280 of cap free
Amount of items: 2
Items: 
Size: 775064 Color: 1
Size: 224657 Color: 0

Bin 3263: 281 of cap free
Amount of items: 2
Items: 
Size: 515287 Color: 0
Size: 484433 Color: 1

Bin 3264: 281 of cap free
Amount of items: 2
Items: 
Size: 538781 Color: 0
Size: 460939 Color: 1

Bin 3265: 281 of cap free
Amount of items: 2
Items: 
Size: 600030 Color: 0
Size: 399690 Color: 1

Bin 3266: 282 of cap free
Amount of items: 2
Items: 
Size: 637629 Color: 0
Size: 362090 Color: 1

Bin 3267: 282 of cap free
Amount of items: 2
Items: 
Size: 716812 Color: 0
Size: 282907 Color: 1

Bin 3268: 283 of cap free
Amount of items: 2
Items: 
Size: 680434 Color: 1
Size: 319284 Color: 0

Bin 3269: 284 of cap free
Amount of items: 2
Items: 
Size: 538304 Color: 1
Size: 461413 Color: 0

Bin 3270: 284 of cap free
Amount of items: 2
Items: 
Size: 641754 Color: 0
Size: 357963 Color: 1

Bin 3271: 284 of cap free
Amount of items: 2
Items: 
Size: 696438 Color: 0
Size: 303279 Color: 1

Bin 3272: 284 of cap free
Amount of items: 2
Items: 
Size: 728464 Color: 1
Size: 271253 Color: 0

Bin 3273: 285 of cap free
Amount of items: 2
Items: 
Size: 505902 Color: 1
Size: 493814 Color: 0

Bin 3274: 285 of cap free
Amount of items: 2
Items: 
Size: 530304 Color: 0
Size: 469412 Color: 1

Bin 3275: 285 of cap free
Amount of items: 2
Items: 
Size: 711945 Color: 1
Size: 287771 Color: 0

Bin 3276: 286 of cap free
Amount of items: 2
Items: 
Size: 531444 Color: 1
Size: 468271 Color: 0

Bin 3277: 286 of cap free
Amount of items: 2
Items: 
Size: 534789 Color: 1
Size: 464926 Color: 0

Bin 3278: 286 of cap free
Amount of items: 2
Items: 
Size: 543104 Color: 1
Size: 456611 Color: 0

Bin 3279: 286 of cap free
Amount of items: 2
Items: 
Size: 593240 Color: 1
Size: 406475 Color: 0

Bin 3280: 287 of cap free
Amount of items: 2
Items: 
Size: 630056 Color: 0
Size: 369658 Color: 1

Bin 3281: 287 of cap free
Amount of items: 2
Items: 
Size: 689453 Color: 0
Size: 310261 Color: 1

Bin 3282: 288 of cap free
Amount of items: 2
Items: 
Size: 620610 Color: 1
Size: 379103 Color: 0

Bin 3283: 288 of cap free
Amount of items: 2
Items: 
Size: 647399 Color: 0
Size: 352314 Color: 1

Bin 3284: 288 of cap free
Amount of items: 2
Items: 
Size: 746875 Color: 1
Size: 252838 Color: 0

Bin 3285: 288 of cap free
Amount of items: 2
Items: 
Size: 752621 Color: 1
Size: 247092 Color: 0

Bin 3286: 289 of cap free
Amount of items: 2
Items: 
Size: 523728 Color: 1
Size: 475984 Color: 0

Bin 3287: 289 of cap free
Amount of items: 2
Items: 
Size: 677078 Color: 1
Size: 322634 Color: 0

Bin 3288: 290 of cap free
Amount of items: 2
Items: 
Size: 566153 Color: 1
Size: 433558 Color: 0

Bin 3289: 290 of cap free
Amount of items: 2
Items: 
Size: 573297 Color: 0
Size: 426414 Color: 1

Bin 3290: 290 of cap free
Amount of items: 2
Items: 
Size: 698295 Color: 0
Size: 301416 Color: 1

Bin 3291: 290 of cap free
Amount of items: 2
Items: 
Size: 719468 Color: 1
Size: 280243 Color: 0

Bin 3292: 290 of cap free
Amount of items: 2
Items: 
Size: 729971 Color: 1
Size: 269740 Color: 0

Bin 3293: 290 of cap free
Amount of items: 2
Items: 
Size: 755155 Color: 0
Size: 244556 Color: 1

Bin 3294: 291 of cap free
Amount of items: 2
Items: 
Size: 720023 Color: 1
Size: 279687 Color: 0

Bin 3295: 292 of cap free
Amount of items: 2
Items: 
Size: 730304 Color: 0
Size: 269405 Color: 1

Bin 3296: 293 of cap free
Amount of items: 2
Items: 
Size: 798247 Color: 1
Size: 201461 Color: 0

Bin 3297: 293 of cap free
Amount of items: 2
Items: 
Size: 531197 Color: 0
Size: 468511 Color: 1

Bin 3298: 293 of cap free
Amount of items: 2
Items: 
Size: 624343 Color: 1
Size: 375365 Color: 0

Bin 3299: 293 of cap free
Amount of items: 2
Items: 
Size: 744636 Color: 1
Size: 255072 Color: 0

Bin 3300: 294 of cap free
Amount of items: 2
Items: 
Size: 535645 Color: 1
Size: 464062 Color: 0

Bin 3301: 294 of cap free
Amount of items: 2
Items: 
Size: 656868 Color: 1
Size: 342839 Color: 0

Bin 3302: 294 of cap free
Amount of items: 2
Items: 
Size: 661010 Color: 0
Size: 338697 Color: 1

Bin 3303: 294 of cap free
Amount of items: 2
Items: 
Size: 760097 Color: 0
Size: 239610 Color: 1

Bin 3304: 295 of cap free
Amount of items: 2
Items: 
Size: 626307 Color: 0
Size: 373399 Color: 1

Bin 3305: 295 of cap free
Amount of items: 2
Items: 
Size: 634708 Color: 0
Size: 364998 Color: 1

Bin 3306: 295 of cap free
Amount of items: 2
Items: 
Size: 643116 Color: 0
Size: 356590 Color: 1

Bin 3307: 295 of cap free
Amount of items: 2
Items: 
Size: 726849 Color: 1
Size: 272857 Color: 0

Bin 3308: 295 of cap free
Amount of items: 2
Items: 
Size: 734099 Color: 1
Size: 265607 Color: 0

Bin 3309: 296 of cap free
Amount of items: 2
Items: 
Size: 548472 Color: 0
Size: 451233 Color: 1

Bin 3310: 296 of cap free
Amount of items: 2
Items: 
Size: 578384 Color: 1
Size: 421321 Color: 0

Bin 3311: 296 of cap free
Amount of items: 2
Items: 
Size: 773093 Color: 0
Size: 226612 Color: 1

Bin 3312: 297 of cap free
Amount of items: 2
Items: 
Size: 656277 Color: 0
Size: 343427 Color: 1

Bin 3313: 297 of cap free
Amount of items: 2
Items: 
Size: 709444 Color: 1
Size: 290260 Color: 0

Bin 3314: 297 of cap free
Amount of items: 2
Items: 
Size: 735227 Color: 0
Size: 264477 Color: 1

Bin 3315: 298 of cap free
Amount of items: 2
Items: 
Size: 731436 Color: 1
Size: 268267 Color: 0

Bin 3316: 298 of cap free
Amount of items: 2
Items: 
Size: 770059 Color: 0
Size: 229644 Color: 1

Bin 3317: 299 of cap free
Amount of items: 2
Items: 
Size: 538340 Color: 0
Size: 461362 Color: 1

Bin 3318: 299 of cap free
Amount of items: 2
Items: 
Size: 544625 Color: 0
Size: 455077 Color: 1

Bin 3319: 299 of cap free
Amount of items: 2
Items: 
Size: 711016 Color: 1
Size: 288686 Color: 0

Bin 3320: 299 of cap free
Amount of items: 2
Items: 
Size: 795526 Color: 1
Size: 204176 Color: 0

Bin 3321: 300 of cap free
Amount of items: 2
Items: 
Size: 509255 Color: 1
Size: 490446 Color: 0

Bin 3322: 300 of cap free
Amount of items: 2
Items: 
Size: 516574 Color: 0
Size: 483127 Color: 1

Bin 3323: 300 of cap free
Amount of items: 2
Items: 
Size: 610933 Color: 0
Size: 388768 Color: 1

Bin 3324: 300 of cap free
Amount of items: 2
Items: 
Size: 776103 Color: 0
Size: 223598 Color: 1

Bin 3325: 300 of cap free
Amount of items: 2
Items: 
Size: 794461 Color: 0
Size: 205240 Color: 1

Bin 3326: 301 of cap free
Amount of items: 2
Items: 
Size: 619072 Color: 0
Size: 380628 Color: 1

Bin 3327: 301 of cap free
Amount of items: 2
Items: 
Size: 696756 Color: 0
Size: 302944 Color: 1

Bin 3328: 302 of cap free
Amount of items: 2
Items: 
Size: 758839 Color: 1
Size: 240860 Color: 0

Bin 3329: 303 of cap free
Amount of items: 2
Items: 
Size: 768433 Color: 1
Size: 231265 Color: 0

Bin 3330: 303 of cap free
Amount of items: 3
Items: 
Size: 494429 Color: 1
Size: 313000 Color: 1
Size: 192269 Color: 0

Bin 3331: 304 of cap free
Amount of items: 2
Items: 
Size: 745507 Color: 1
Size: 254190 Color: 0

Bin 3332: 304 of cap free
Amount of items: 2
Items: 
Size: 557290 Color: 1
Size: 442407 Color: 0

Bin 3333: 304 of cap free
Amount of items: 2
Items: 
Size: 726369 Color: 0
Size: 273328 Color: 1

Bin 3334: 304 of cap free
Amount of items: 2
Items: 
Size: 731655 Color: 0
Size: 268042 Color: 1

Bin 3335: 306 of cap free
Amount of items: 2
Items: 
Size: 505583 Color: 1
Size: 494112 Color: 0

Bin 3336: 306 of cap free
Amount of items: 2
Items: 
Size: 512492 Color: 0
Size: 487203 Color: 1

Bin 3337: 306 of cap free
Amount of items: 2
Items: 
Size: 540723 Color: 0
Size: 458972 Color: 1

Bin 3338: 306 of cap free
Amount of items: 2
Items: 
Size: 641020 Color: 1
Size: 358675 Color: 0

Bin 3339: 306 of cap free
Amount of items: 2
Items: 
Size: 644536 Color: 0
Size: 355159 Color: 1

Bin 3340: 306 of cap free
Amount of items: 2
Items: 
Size: 706248 Color: 0
Size: 293447 Color: 1

Bin 3341: 307 of cap free
Amount of items: 2
Items: 
Size: 526604 Color: 1
Size: 473090 Color: 0

Bin 3342: 307 of cap free
Amount of items: 2
Items: 
Size: 528606 Color: 0
Size: 471088 Color: 1

Bin 3343: 307 of cap free
Amount of items: 2
Items: 
Size: 601538 Color: 0
Size: 398156 Color: 1

Bin 3344: 308 of cap free
Amount of items: 2
Items: 
Size: 542117 Color: 0
Size: 457576 Color: 1

Bin 3345: 308 of cap free
Amount of items: 2
Items: 
Size: 565375 Color: 1
Size: 434318 Color: 0

Bin 3346: 308 of cap free
Amount of items: 2
Items: 
Size: 655601 Color: 1
Size: 344092 Color: 0

Bin 3347: 308 of cap free
Amount of items: 2
Items: 
Size: 678247 Color: 1
Size: 321446 Color: 0

Bin 3348: 309 of cap free
Amount of items: 2
Items: 
Size: 584999 Color: 1
Size: 414693 Color: 0

Bin 3349: 309 of cap free
Amount of items: 2
Items: 
Size: 649185 Color: 0
Size: 350507 Color: 1

Bin 3350: 310 of cap free
Amount of items: 2
Items: 
Size: 787737 Color: 1
Size: 211954 Color: 0

Bin 3351: 311 of cap free
Amount of items: 2
Items: 
Size: 548126 Color: 0
Size: 451564 Color: 1

Bin 3352: 311 of cap free
Amount of items: 2
Items: 
Size: 660974 Color: 1
Size: 338716 Color: 0

Bin 3353: 312 of cap free
Amount of items: 2
Items: 
Size: 704674 Color: 1
Size: 295015 Color: 0

Bin 3354: 312 of cap free
Amount of items: 2
Items: 
Size: 519140 Color: 0
Size: 480549 Color: 1

Bin 3355: 312 of cap free
Amount of items: 2
Items: 
Size: 587369 Color: 1
Size: 412320 Color: 0

Bin 3356: 312 of cap free
Amount of items: 2
Items: 
Size: 673538 Color: 1
Size: 326151 Color: 0

Bin 3357: 312 of cap free
Amount of items: 2
Items: 
Size: 741372 Color: 1
Size: 258317 Color: 0

Bin 3358: 313 of cap free
Amount of items: 2
Items: 
Size: 577302 Color: 1
Size: 422386 Color: 0

Bin 3359: 313 of cap free
Amount of items: 2
Items: 
Size: 697351 Color: 0
Size: 302337 Color: 1

Bin 3360: 314 of cap free
Amount of items: 2
Items: 
Size: 588036 Color: 1
Size: 411651 Color: 0

Bin 3361: 315 of cap free
Amount of items: 2
Items: 
Size: 779625 Color: 1
Size: 220061 Color: 0

Bin 3362: 315 of cap free
Amount of items: 2
Items: 
Size: 615069 Color: 0
Size: 384617 Color: 1

Bin 3363: 315 of cap free
Amount of items: 2
Items: 
Size: 652837 Color: 1
Size: 346849 Color: 0

Bin 3364: 316 of cap free
Amount of items: 2
Items: 
Size: 640500 Color: 1
Size: 359185 Color: 0

Bin 3365: 316 of cap free
Amount of items: 2
Items: 
Size: 678155 Color: 0
Size: 321530 Color: 1

Bin 3366: 316 of cap free
Amount of items: 2
Items: 
Size: 682935 Color: 0
Size: 316750 Color: 1

Bin 3367: 317 of cap free
Amount of items: 2
Items: 
Size: 600660 Color: 0
Size: 399024 Color: 1

Bin 3368: 317 of cap free
Amount of items: 2
Items: 
Size: 669766 Color: 1
Size: 329918 Color: 0

Bin 3369: 318 of cap free
Amount of items: 2
Items: 
Size: 514887 Color: 1
Size: 484796 Color: 0

Bin 3370: 319 of cap free
Amount of items: 2
Items: 
Size: 632224 Color: 0
Size: 367458 Color: 1

Bin 3371: 319 of cap free
Amount of items: 2
Items: 
Size: 716347 Color: 1
Size: 283335 Color: 0

Bin 3372: 320 of cap free
Amount of items: 2
Items: 
Size: 646302 Color: 1
Size: 353379 Color: 0

Bin 3373: 320 of cap free
Amount of items: 2
Items: 
Size: 729951 Color: 1
Size: 269730 Color: 0

Bin 3374: 321 of cap free
Amount of items: 2
Items: 
Size: 606794 Color: 1
Size: 392886 Color: 0

Bin 3375: 321 of cap free
Amount of items: 2
Items: 
Size: 537815 Color: 1
Size: 461865 Color: 0

Bin 3376: 322 of cap free
Amount of items: 2
Items: 
Size: 771197 Color: 1
Size: 228482 Color: 0

Bin 3377: 322 of cap free
Amount of items: 2
Items: 
Size: 530291 Color: 0
Size: 469388 Color: 1

Bin 3378: 322 of cap free
Amount of items: 2
Items: 
Size: 670457 Color: 0
Size: 329222 Color: 1

Bin 3379: 323 of cap free
Amount of items: 2
Items: 
Size: 517603 Color: 0
Size: 482075 Color: 1

Bin 3380: 323 of cap free
Amount of items: 2
Items: 
Size: 695126 Color: 1
Size: 304552 Color: 0

Bin 3381: 323 of cap free
Amount of items: 2
Items: 
Size: 788498 Color: 0
Size: 211180 Color: 1

Bin 3382: 324 of cap free
Amount of items: 2
Items: 
Size: 539684 Color: 1
Size: 459993 Color: 0

Bin 3383: 324 of cap free
Amount of items: 2
Items: 
Size: 630928 Color: 0
Size: 368749 Color: 1

Bin 3384: 324 of cap free
Amount of items: 2
Items: 
Size: 658414 Color: 0
Size: 341263 Color: 1

Bin 3385: 324 of cap free
Amount of items: 2
Items: 
Size: 703386 Color: 0
Size: 296291 Color: 1

Bin 3386: 324 of cap free
Amount of items: 2
Items: 
Size: 716813 Color: 1
Size: 282864 Color: 0

Bin 3387: 324 of cap free
Amount of items: 2
Items: 
Size: 724294 Color: 1
Size: 275383 Color: 0

Bin 3388: 325 of cap free
Amount of items: 2
Items: 
Size: 503118 Color: 0
Size: 496558 Color: 1

Bin 3389: 325 of cap free
Amount of items: 2
Items: 
Size: 619519 Color: 0
Size: 380157 Color: 1

Bin 3390: 326 of cap free
Amount of items: 2
Items: 
Size: 508795 Color: 0
Size: 490880 Color: 1

Bin 3391: 326 of cap free
Amount of items: 2
Items: 
Size: 664546 Color: 0
Size: 335129 Color: 1

Bin 3392: 326 of cap free
Amount of items: 2
Items: 
Size: 752503 Color: 0
Size: 247172 Color: 1

Bin 3393: 326 of cap free
Amount of items: 2
Items: 
Size: 766181 Color: 1
Size: 233494 Color: 0

Bin 3394: 327 of cap free
Amount of items: 2
Items: 
Size: 667412 Color: 1
Size: 332262 Color: 0

Bin 3395: 327 of cap free
Amount of items: 2
Items: 
Size: 670692 Color: 1
Size: 328982 Color: 0

Bin 3396: 327 of cap free
Amount of items: 2
Items: 
Size: 738010 Color: 1
Size: 261664 Color: 0

Bin 3397: 328 of cap free
Amount of items: 2
Items: 
Size: 730514 Color: 1
Size: 269159 Color: 0

Bin 3398: 328 of cap free
Amount of items: 2
Items: 
Size: 791967 Color: 0
Size: 207706 Color: 1

Bin 3399: 330 of cap free
Amount of items: 2
Items: 
Size: 511367 Color: 1
Size: 488304 Color: 0

Bin 3400: 330 of cap free
Amount of items: 2
Items: 
Size: 686185 Color: 1
Size: 313486 Color: 0

Bin 3401: 331 of cap free
Amount of items: 2
Items: 
Size: 593124 Color: 0
Size: 406546 Color: 1

Bin 3402: 331 of cap free
Amount of items: 2
Items: 
Size: 758802 Color: 0
Size: 240868 Color: 1

Bin 3403: 332 of cap free
Amount of items: 6
Items: 
Size: 174672 Color: 0
Size: 165501 Color: 0
Size: 165479 Color: 0
Size: 165254 Color: 1
Size: 164486 Color: 1
Size: 164277 Color: 1

Bin 3404: 333 of cap free
Amount of items: 2
Items: 
Size: 620166 Color: 0
Size: 379502 Color: 1

Bin 3405: 333 of cap free
Amount of items: 2
Items: 
Size: 527182 Color: 1
Size: 472486 Color: 0

Bin 3406: 333 of cap free
Amount of items: 2
Items: 
Size: 561182 Color: 1
Size: 438486 Color: 0

Bin 3407: 333 of cap free
Amount of items: 2
Items: 
Size: 631531 Color: 0
Size: 368137 Color: 1

Bin 3408: 333 of cap free
Amount of items: 2
Items: 
Size: 652451 Color: 0
Size: 347217 Color: 1

Bin 3409: 333 of cap free
Amount of items: 2
Items: 
Size: 666228 Color: 0
Size: 333440 Color: 1

Bin 3410: 333 of cap free
Amount of items: 2
Items: 
Size: 705296 Color: 0
Size: 294372 Color: 1

Bin 3411: 333 of cap free
Amount of items: 2
Items: 
Size: 682775 Color: 1
Size: 316893 Color: 0

Bin 3412: 334 of cap free
Amount of items: 2
Items: 
Size: 632280 Color: 1
Size: 367387 Color: 0

Bin 3413: 334 of cap free
Amount of items: 2
Items: 
Size: 655161 Color: 1
Size: 344506 Color: 0

Bin 3414: 334 of cap free
Amount of items: 2
Items: 
Size: 765751 Color: 0
Size: 233916 Color: 1

Bin 3415: 335 of cap free
Amount of items: 2
Items: 
Size: 622705 Color: 0
Size: 376961 Color: 1

Bin 3416: 335 of cap free
Amount of items: 2
Items: 
Size: 708644 Color: 0
Size: 291022 Color: 1

Bin 3417: 336 of cap free
Amount of items: 2
Items: 
Size: 502586 Color: 1
Size: 497079 Color: 0

Bin 3418: 337 of cap free
Amount of items: 3
Items: 
Size: 568328 Color: 1
Size: 309239 Color: 0
Size: 122097 Color: 1

Bin 3419: 337 of cap free
Amount of items: 2
Items: 
Size: 528260 Color: 0
Size: 471404 Color: 1

Bin 3420: 338 of cap free
Amount of items: 2
Items: 
Size: 585505 Color: 1
Size: 414158 Color: 0

Bin 3421: 339 of cap free
Amount of items: 2
Items: 
Size: 721442 Color: 1
Size: 278220 Color: 0

Bin 3422: 340 of cap free
Amount of items: 2
Items: 
Size: 534904 Color: 0
Size: 464757 Color: 1

Bin 3423: 340 of cap free
Amount of items: 2
Items: 
Size: 556029 Color: 0
Size: 443632 Color: 1

Bin 3424: 340 of cap free
Amount of items: 2
Items: 
Size: 596140 Color: 0
Size: 403521 Color: 1

Bin 3425: 340 of cap free
Amount of items: 2
Items: 
Size: 657931 Color: 0
Size: 341730 Color: 1

Bin 3426: 340 of cap free
Amount of items: 2
Items: 
Size: 706947 Color: 1
Size: 292714 Color: 0

Bin 3427: 340 of cap free
Amount of items: 2
Items: 
Size: 750006 Color: 0
Size: 249655 Color: 1

Bin 3428: 340 of cap free
Amount of items: 2
Items: 
Size: 792347 Color: 1
Size: 207314 Color: 0

Bin 3429: 341 of cap free
Amount of items: 2
Items: 
Size: 518390 Color: 0
Size: 481270 Color: 1

Bin 3430: 341 of cap free
Amount of items: 2
Items: 
Size: 529003 Color: 1
Size: 470657 Color: 0

Bin 3431: 341 of cap free
Amount of items: 2
Items: 
Size: 551544 Color: 0
Size: 448116 Color: 1

Bin 3432: 341 of cap free
Amount of items: 2
Items: 
Size: 671257 Color: 0
Size: 328403 Color: 1

Bin 3433: 342 of cap free
Amount of items: 2
Items: 
Size: 603169 Color: 1
Size: 396490 Color: 0

Bin 3434: 343 of cap free
Amount of items: 2
Items: 
Size: 710139 Color: 1
Size: 289519 Color: 0

Bin 3435: 344 of cap free
Amount of items: 2
Items: 
Size: 687359 Color: 0
Size: 312298 Color: 1

Bin 3436: 344 of cap free
Amount of items: 2
Items: 
Size: 521406 Color: 1
Size: 478251 Color: 0

Bin 3437: 344 of cap free
Amount of items: 2
Items: 
Size: 537364 Color: 0
Size: 462293 Color: 1

Bin 3438: 344 of cap free
Amount of items: 2
Items: 
Size: 551656 Color: 1
Size: 448001 Color: 0

Bin 3439: 346 of cap free
Amount of items: 2
Items: 
Size: 762301 Color: 0
Size: 237354 Color: 1

Bin 3440: 347 of cap free
Amount of items: 2
Items: 
Size: 567805 Color: 1
Size: 431849 Color: 0

Bin 3441: 349 of cap free
Amount of items: 2
Items: 
Size: 749548 Color: 1
Size: 250104 Color: 0

Bin 3442: 349 of cap free
Amount of items: 2
Items: 
Size: 664541 Color: 0
Size: 335111 Color: 1

Bin 3443: 350 of cap free
Amount of items: 2
Items: 
Size: 555226 Color: 1
Size: 444425 Color: 0

Bin 3444: 350 of cap free
Amount of items: 2
Items: 
Size: 639408 Color: 0
Size: 360243 Color: 1

Bin 3445: 350 of cap free
Amount of items: 2
Items: 
Size: 657169 Color: 1
Size: 342482 Color: 0

Bin 3446: 350 of cap free
Amount of items: 2
Items: 
Size: 678692 Color: 0
Size: 320959 Color: 1

Bin 3447: 350 of cap free
Amount of items: 2
Items: 
Size: 686210 Color: 0
Size: 313441 Color: 1

Bin 3448: 351 of cap free
Amount of items: 2
Items: 
Size: 570520 Color: 0
Size: 429130 Color: 1

Bin 3449: 351 of cap free
Amount of items: 2
Items: 
Size: 616646 Color: 0
Size: 383004 Color: 1

Bin 3450: 352 of cap free
Amount of items: 2
Items: 
Size: 657058 Color: 0
Size: 342591 Color: 1

Bin 3451: 353 of cap free
Amount of items: 2
Items: 
Size: 792698 Color: 0
Size: 206950 Color: 1

Bin 3452: 353 of cap free
Amount of items: 2
Items: 
Size: 519378 Color: 1
Size: 480270 Color: 0

Bin 3453: 353 of cap free
Amount of items: 2
Items: 
Size: 532041 Color: 1
Size: 467607 Color: 0

Bin 3454: 353 of cap free
Amount of items: 2
Items: 
Size: 745719 Color: 0
Size: 253929 Color: 1

Bin 3455: 353 of cap free
Amount of items: 2
Items: 
Size: 770259 Color: 1
Size: 229389 Color: 0

Bin 3456: 354 of cap free
Amount of items: 2
Items: 
Size: 686173 Color: 1
Size: 313474 Color: 0

Bin 3457: 354 of cap free
Amount of items: 2
Items: 
Size: 732953 Color: 1
Size: 266694 Color: 0

Bin 3458: 354 of cap free
Amount of items: 2
Items: 
Size: 687085 Color: 1
Size: 312562 Color: 0

Bin 3459: 355 of cap free
Amount of items: 2
Items: 
Size: 677455 Color: 0
Size: 322191 Color: 1

Bin 3460: 357 of cap free
Amount of items: 2
Items: 
Size: 502570 Color: 0
Size: 497074 Color: 1

Bin 3461: 357 of cap free
Amount of items: 2
Items: 
Size: 647763 Color: 0
Size: 351881 Color: 1

Bin 3462: 357 of cap free
Amount of items: 2
Items: 
Size: 700103 Color: 0
Size: 299541 Color: 1

Bin 3463: 358 of cap free
Amount of items: 2
Items: 
Size: 774354 Color: 0
Size: 225289 Color: 1

Bin 3464: 359 of cap free
Amount of items: 2
Items: 
Size: 777789 Color: 0
Size: 221853 Color: 1

Bin 3465: 360 of cap free
Amount of items: 2
Items: 
Size: 790191 Color: 0
Size: 209450 Color: 1

Bin 3466: 361 of cap free
Amount of items: 2
Items: 
Size: 622457 Color: 1
Size: 377183 Color: 0

Bin 3467: 363 of cap free
Amount of items: 2
Items: 
Size: 783325 Color: 0
Size: 216313 Color: 1

Bin 3468: 363 of cap free
Amount of items: 2
Items: 
Size: 526449 Color: 0
Size: 473189 Color: 1

Bin 3469: 364 of cap free
Amount of items: 2
Items: 
Size: 559684 Color: 0
Size: 439953 Color: 1

Bin 3470: 364 of cap free
Amount of items: 2
Items: 
Size: 575150 Color: 1
Size: 424487 Color: 0

Bin 3471: 364 of cap free
Amount of items: 2
Items: 
Size: 575244 Color: 0
Size: 424393 Color: 1

Bin 3472: 365 of cap free
Amount of items: 2
Items: 
Size: 668579 Color: 0
Size: 331057 Color: 1

Bin 3473: 365 of cap free
Amount of items: 2
Items: 
Size: 679826 Color: 0
Size: 319810 Color: 1

Bin 3474: 365 of cap free
Amount of items: 2
Items: 
Size: 706969 Color: 0
Size: 292667 Color: 1

Bin 3475: 366 of cap free
Amount of items: 2
Items: 
Size: 508188 Color: 0
Size: 491447 Color: 1

Bin 3476: 366 of cap free
Amount of items: 2
Items: 
Size: 550854 Color: 0
Size: 448781 Color: 1

Bin 3477: 366 of cap free
Amount of items: 2
Items: 
Size: 774809 Color: 0
Size: 224826 Color: 1

Bin 3478: 367 of cap free
Amount of items: 2
Items: 
Size: 553061 Color: 0
Size: 446573 Color: 1

Bin 3479: 367 of cap free
Amount of items: 2
Items: 
Size: 688347 Color: 1
Size: 311287 Color: 0

Bin 3480: 367 of cap free
Amount of items: 2
Items: 
Size: 715533 Color: 0
Size: 284101 Color: 1

Bin 3481: 368 of cap free
Amount of items: 2
Items: 
Size: 578772 Color: 0
Size: 420861 Color: 1

Bin 3482: 368 of cap free
Amount of items: 2
Items: 
Size: 766656 Color: 1
Size: 232977 Color: 0

Bin 3483: 369 of cap free
Amount of items: 2
Items: 
Size: 529006 Color: 0
Size: 470626 Color: 1

Bin 3484: 369 of cap free
Amount of items: 2
Items: 
Size: 614035 Color: 0
Size: 385597 Color: 1

Bin 3485: 369 of cap free
Amount of items: 2
Items: 
Size: 691808 Color: 0
Size: 307824 Color: 1

Bin 3486: 370 of cap free
Amount of items: 2
Items: 
Size: 727273 Color: 0
Size: 272358 Color: 1

Bin 3487: 371 of cap free
Amount of items: 2
Items: 
Size: 544213 Color: 1
Size: 455417 Color: 0

Bin 3488: 372 of cap free
Amount of items: 2
Items: 
Size: 508765 Color: 0
Size: 490864 Color: 1

Bin 3489: 373 of cap free
Amount of items: 2
Items: 
Size: 580176 Color: 0
Size: 419452 Color: 1

Bin 3490: 375 of cap free
Amount of items: 2
Items: 
Size: 657052 Color: 0
Size: 342574 Color: 1

Bin 3491: 376 of cap free
Amount of items: 2
Items: 
Size: 785821 Color: 0
Size: 213804 Color: 1

Bin 3492: 377 of cap free
Amount of items: 2
Items: 
Size: 597096 Color: 1
Size: 402528 Color: 0

Bin 3493: 377 of cap free
Amount of items: 2
Items: 
Size: 712905 Color: 0
Size: 286719 Color: 1

Bin 3494: 377 of cap free
Amount of items: 2
Items: 
Size: 779901 Color: 0
Size: 219723 Color: 1

Bin 3495: 378 of cap free
Amount of items: 2
Items: 
Size: 631427 Color: 1
Size: 368196 Color: 0

Bin 3496: 379 of cap free
Amount of items: 2
Items: 
Size: 499917 Color: 0
Size: 499705 Color: 1

Bin 3497: 380 of cap free
Amount of items: 2
Items: 
Size: 648134 Color: 0
Size: 351487 Color: 1

Bin 3498: 381 of cap free
Amount of items: 2
Items: 
Size: 609260 Color: 0
Size: 390360 Color: 1

Bin 3499: 382 of cap free
Amount of items: 2
Items: 
Size: 653143 Color: 0
Size: 346476 Color: 1

Bin 3500: 382 of cap free
Amount of items: 2
Items: 
Size: 678126 Color: 0
Size: 321493 Color: 1

Bin 3501: 383 of cap free
Amount of items: 2
Items: 
Size: 629124 Color: 1
Size: 370494 Color: 0

Bin 3502: 384 of cap free
Amount of items: 2
Items: 
Size: 501079 Color: 1
Size: 498538 Color: 0

Bin 3503: 384 of cap free
Amount of items: 2
Items: 
Size: 663232 Color: 0
Size: 336385 Color: 1

Bin 3504: 386 of cap free
Amount of items: 2
Items: 
Size: 560575 Color: 0
Size: 439040 Color: 1

Bin 3505: 386 of cap free
Amount of items: 2
Items: 
Size: 582115 Color: 1
Size: 417500 Color: 0

Bin 3506: 386 of cap free
Amount of items: 2
Items: 
Size: 642205 Color: 0
Size: 357410 Color: 1

Bin 3507: 387 of cap free
Amount of items: 2
Items: 
Size: 551505 Color: 0
Size: 448109 Color: 1

Bin 3508: 390 of cap free
Amount of items: 2
Items: 
Size: 681588 Color: 0
Size: 318023 Color: 1

Bin 3509: 390 of cap free
Amount of items: 2
Items: 
Size: 683444 Color: 0
Size: 316167 Color: 1

Bin 3510: 391 of cap free
Amount of items: 2
Items: 
Size: 754042 Color: 0
Size: 245568 Color: 1

Bin 3511: 393 of cap free
Amount of items: 2
Items: 
Size: 559966 Color: 1
Size: 439642 Color: 0

Bin 3512: 393 of cap free
Amount of items: 2
Items: 
Size: 566929 Color: 1
Size: 432679 Color: 0

Bin 3513: 393 of cap free
Amount of items: 2
Items: 
Size: 648563 Color: 1
Size: 351045 Color: 0

Bin 3514: 393 of cap free
Amount of items: 2
Items: 
Size: 738654 Color: 1
Size: 260954 Color: 0

Bin 3515: 394 of cap free
Amount of items: 2
Items: 
Size: 776631 Color: 0
Size: 222976 Color: 1

Bin 3516: 394 of cap free
Amount of items: 2
Items: 
Size: 523257 Color: 0
Size: 476350 Color: 1

Bin 3517: 395 of cap free
Amount of items: 2
Items: 
Size: 555256 Color: 0
Size: 444350 Color: 1

Bin 3518: 395 of cap free
Amount of items: 2
Items: 
Size: 611278 Color: 0
Size: 388328 Color: 1

Bin 3519: 395 of cap free
Amount of items: 2
Items: 
Size: 700466 Color: 1
Size: 299140 Color: 0

Bin 3520: 397 of cap free
Amount of items: 2
Items: 
Size: 684985 Color: 1
Size: 314619 Color: 0

Bin 3521: 398 of cap free
Amount of items: 2
Items: 
Size: 647509 Color: 1
Size: 352094 Color: 0

Bin 3522: 399 of cap free
Amount of items: 2
Items: 
Size: 782408 Color: 0
Size: 217194 Color: 1

Bin 3523: 399 of cap free
Amount of items: 2
Items: 
Size: 593607 Color: 0
Size: 405995 Color: 1

Bin 3524: 399 of cap free
Amount of items: 2
Items: 
Size: 758356 Color: 1
Size: 241246 Color: 0

Bin 3525: 403 of cap free
Amount of items: 2
Items: 
Size: 741516 Color: 0
Size: 258082 Color: 1

Bin 3526: 404 of cap free
Amount of items: 2
Items: 
Size: 566084 Color: 0
Size: 433513 Color: 1

Bin 3527: 405 of cap free
Amount of items: 2
Items: 
Size: 704603 Color: 1
Size: 294993 Color: 0

Bin 3528: 405 of cap free
Amount of items: 2
Items: 
Size: 679818 Color: 0
Size: 319778 Color: 1

Bin 3529: 405 of cap free
Amount of items: 2
Items: 
Size: 704885 Color: 0
Size: 294711 Color: 1

Bin 3530: 405 of cap free
Amount of items: 2
Items: 
Size: 710139 Color: 1
Size: 289457 Color: 0

Bin 3531: 408 of cap free
Amount of items: 2
Items: 
Size: 548463 Color: 0
Size: 451130 Color: 1

Bin 3532: 408 of cap free
Amount of items: 2
Items: 
Size: 758050 Color: 0
Size: 241543 Color: 1

Bin 3533: 409 of cap free
Amount of items: 2
Items: 
Size: 643687 Color: 0
Size: 355905 Color: 1

Bin 3534: 409 of cap free
Amount of items: 2
Items: 
Size: 669163 Color: 1
Size: 330429 Color: 0

Bin 3535: 409 of cap free
Amount of items: 2
Items: 
Size: 718209 Color: 1
Size: 281383 Color: 0

Bin 3536: 410 of cap free
Amount of items: 2
Items: 
Size: 667499 Color: 0
Size: 332092 Color: 1

Bin 3537: 415 of cap free
Amount of items: 2
Items: 
Size: 524146 Color: 0
Size: 475440 Color: 1

Bin 3538: 416 of cap free
Amount of items: 2
Items: 
Size: 654444 Color: 0
Size: 345141 Color: 1

Bin 3539: 416 of cap free
Amount of items: 2
Items: 
Size: 659305 Color: 0
Size: 340280 Color: 1

Bin 3540: 416 of cap free
Amount of items: 2
Items: 
Size: 696402 Color: 1
Size: 303183 Color: 0

Bin 3541: 417 of cap free
Amount of items: 2
Items: 
Size: 516474 Color: 0
Size: 483110 Color: 1

Bin 3542: 417 of cap free
Amount of items: 2
Items: 
Size: 576748 Color: 0
Size: 422836 Color: 1

Bin 3543: 418 of cap free
Amount of items: 2
Items: 
Size: 786192 Color: 1
Size: 213391 Color: 0

Bin 3544: 418 of cap free
Amount of items: 2
Items: 
Size: 574012 Color: 0
Size: 425571 Color: 1

Bin 3545: 419 of cap free
Amount of items: 2
Items: 
Size: 724233 Color: 1
Size: 275349 Color: 0

Bin 3546: 419 of cap free
Amount of items: 2
Items: 
Size: 728077 Color: 0
Size: 271505 Color: 1

Bin 3547: 420 of cap free
Amount of items: 2
Items: 
Size: 572282 Color: 0
Size: 427299 Color: 1

Bin 3548: 421 of cap free
Amount of items: 2
Items: 
Size: 543596 Color: 1
Size: 455984 Color: 0

Bin 3549: 421 of cap free
Amount of items: 2
Items: 
Size: 545648 Color: 0
Size: 453932 Color: 1

Bin 3550: 421 of cap free
Amount of items: 2
Items: 
Size: 561973 Color: 0
Size: 437607 Color: 1

Bin 3551: 421 of cap free
Amount of items: 2
Items: 
Size: 634050 Color: 1
Size: 365530 Color: 0

Bin 3552: 421 of cap free
Amount of items: 2
Items: 
Size: 776932 Color: 1
Size: 222648 Color: 0

Bin 3553: 422 of cap free
Amount of items: 2
Items: 
Size: 770678 Color: 0
Size: 228901 Color: 1

Bin 3554: 423 of cap free
Amount of items: 2
Items: 
Size: 604939 Color: 0
Size: 394639 Color: 1

Bin 3555: 423 of cap free
Amount of items: 2
Items: 
Size: 768446 Color: 0
Size: 231132 Color: 1

Bin 3556: 424 of cap free
Amount of items: 2
Items: 
Size: 786660 Color: 1
Size: 212917 Color: 0

Bin 3557: 424 of cap free
Amount of items: 2
Items: 
Size: 634755 Color: 1
Size: 364822 Color: 0

Bin 3558: 424 of cap free
Amount of items: 2
Items: 
Size: 764028 Color: 1
Size: 235549 Color: 0

Bin 3559: 425 of cap free
Amount of items: 2
Items: 
Size: 796547 Color: 1
Size: 203029 Color: 0

Bin 3560: 426 of cap free
Amount of items: 2
Items: 
Size: 674063 Color: 0
Size: 325512 Color: 1

Bin 3561: 429 of cap free
Amount of items: 2
Items: 
Size: 549163 Color: 1
Size: 450409 Color: 0

Bin 3562: 429 of cap free
Amount of items: 2
Items: 
Size: 567427 Color: 0
Size: 432145 Color: 1

Bin 3563: 429 of cap free
Amount of items: 2
Items: 
Size: 710941 Color: 1
Size: 288631 Color: 0

Bin 3564: 430 of cap free
Amount of items: 2
Items: 
Size: 508081 Color: 1
Size: 491490 Color: 0

Bin 3565: 430 of cap free
Amount of items: 2
Items: 
Size: 587258 Color: 0
Size: 412313 Color: 1

Bin 3566: 430 of cap free
Amount of items: 2
Items: 
Size: 674994 Color: 1
Size: 324577 Color: 0

Bin 3567: 432 of cap free
Amount of items: 2
Items: 
Size: 789309 Color: 1
Size: 210260 Color: 0

Bin 3568: 433 of cap free
Amount of items: 2
Items: 
Size: 722799 Color: 0
Size: 276769 Color: 1

Bin 3569: 433 of cap free
Amount of items: 2
Items: 
Size: 546582 Color: 0
Size: 452986 Color: 1

Bin 3570: 433 of cap free
Amount of items: 2
Items: 
Size: 631489 Color: 0
Size: 368079 Color: 1

Bin 3571: 434 of cap free
Amount of items: 2
Items: 
Size: 652430 Color: 0
Size: 347137 Color: 1

Bin 3572: 435 of cap free
Amount of items: 2
Items: 
Size: 524532 Color: 1
Size: 475034 Color: 0

Bin 3573: 436 of cap free
Amount of items: 2
Items: 
Size: 559927 Color: 1
Size: 439638 Color: 0

Bin 3574: 436 of cap free
Amount of items: 2
Items: 
Size: 560564 Color: 0
Size: 439001 Color: 1

Bin 3575: 436 of cap free
Amount of items: 2
Items: 
Size: 760194 Color: 1
Size: 239371 Color: 0

Bin 3576: 437 of cap free
Amount of items: 2
Items: 
Size: 531444 Color: 1
Size: 468120 Color: 0

Bin 3577: 437 of cap free
Amount of items: 2
Items: 
Size: 583598 Color: 1
Size: 415966 Color: 0

Bin 3578: 437 of cap free
Amount of items: 2
Items: 
Size: 778197 Color: 0
Size: 221367 Color: 1

Bin 3579: 438 of cap free
Amount of items: 2
Items: 
Size: 552469 Color: 0
Size: 447094 Color: 1

Bin 3580: 440 of cap free
Amount of items: 2
Items: 
Size: 781507 Color: 1
Size: 218054 Color: 0

Bin 3581: 442 of cap free
Amount of items: 2
Items: 
Size: 566735 Color: 0
Size: 432824 Color: 1

Bin 3582: 443 of cap free
Amount of items: 2
Items: 
Size: 624208 Color: 1
Size: 375350 Color: 0

Bin 3583: 443 of cap free
Amount of items: 2
Items: 
Size: 795415 Color: 1
Size: 204143 Color: 0

Bin 3584: 444 of cap free
Amount of items: 2
Items: 
Size: 728331 Color: 1
Size: 271226 Color: 0

Bin 3585: 445 of cap free
Amount of items: 2
Items: 
Size: 653933 Color: 1
Size: 345623 Color: 0

Bin 3586: 446 of cap free
Amount of items: 2
Items: 
Size: 571603 Color: 0
Size: 427952 Color: 1

Bin 3587: 446 of cap free
Amount of items: 2
Items: 
Size: 702850 Color: 0
Size: 296705 Color: 1

Bin 3588: 447 of cap free
Amount of items: 2
Items: 
Size: 783724 Color: 1
Size: 215830 Color: 0

Bin 3589: 450 of cap free
Amount of items: 2
Items: 
Size: 586369 Color: 0
Size: 413182 Color: 1

Bin 3590: 451 of cap free
Amount of items: 2
Items: 
Size: 561119 Color: 0
Size: 438431 Color: 1

Bin 3591: 452 of cap free
Amount of items: 2
Items: 
Size: 616036 Color: 1
Size: 383513 Color: 0

Bin 3592: 452 of cap free
Amount of items: 2
Items: 
Size: 752422 Color: 0
Size: 247127 Color: 1

Bin 3593: 454 of cap free
Amount of items: 2
Items: 
Size: 613108 Color: 1
Size: 386439 Color: 0

Bin 3594: 454 of cap free
Amount of items: 2
Items: 
Size: 683203 Color: 1
Size: 316344 Color: 0

Bin 3595: 455 of cap free
Amount of items: 2
Items: 
Size: 675853 Color: 0
Size: 323693 Color: 1

Bin 3596: 455 of cap free
Amount of items: 2
Items: 
Size: 784177 Color: 0
Size: 215369 Color: 1

Bin 3597: 457 of cap free
Amount of items: 2
Items: 
Size: 561960 Color: 0
Size: 437584 Color: 1

Bin 3598: 457 of cap free
Amount of items: 2
Items: 
Size: 603132 Color: 1
Size: 396412 Color: 0

Bin 3599: 458 of cap free
Amount of items: 2
Items: 
Size: 558071 Color: 0
Size: 441472 Color: 1

Bin 3600: 459 of cap free
Amount of items: 2
Items: 
Size: 797487 Color: 0
Size: 202055 Color: 1

Bin 3601: 460 of cap free
Amount of items: 2
Items: 
Size: 518294 Color: 1
Size: 481247 Color: 0

Bin 3602: 460 of cap free
Amount of items: 2
Items: 
Size: 590542 Color: 1
Size: 408999 Color: 0

Bin 3603: 464 of cap free
Amount of items: 2
Items: 
Size: 729899 Color: 1
Size: 269638 Color: 0

Bin 3604: 465 of cap free
Amount of items: 2
Items: 
Size: 630713 Color: 1
Size: 368823 Color: 0

Bin 3605: 467 of cap free
Amount of items: 2
Items: 
Size: 575103 Color: 1
Size: 424431 Color: 0

Bin 3606: 468 of cap free
Amount of items: 2
Items: 
Size: 543045 Color: 1
Size: 456488 Color: 0

Bin 3607: 468 of cap free
Amount of items: 2
Items: 
Size: 637868 Color: 1
Size: 361665 Color: 0

Bin 3608: 469 of cap free
Amount of items: 2
Items: 
Size: 792648 Color: 0
Size: 206884 Color: 1

Bin 3609: 469 of cap free
Amount of items: 2
Items: 
Size: 705454 Color: 1
Size: 294078 Color: 0

Bin 3610: 470 of cap free
Amount of items: 2
Items: 
Size: 511547 Color: 0
Size: 487984 Color: 1

Bin 3611: 470 of cap free
Amount of items: 2
Items: 
Size: 708587 Color: 1
Size: 290944 Color: 0

Bin 3612: 471 of cap free
Amount of items: 2
Items: 
Size: 593590 Color: 0
Size: 405940 Color: 1

Bin 3613: 471 of cap free
Amount of items: 2
Items: 
Size: 719948 Color: 0
Size: 279582 Color: 1

Bin 3614: 474 of cap free
Amount of items: 2
Items: 
Size: 724779 Color: 1
Size: 274748 Color: 0

Bin 3615: 475 of cap free
Amount of items: 2
Items: 
Size: 538239 Color: 0
Size: 461287 Color: 1

Bin 3616: 475 of cap free
Amount of items: 2
Items: 
Size: 576697 Color: 0
Size: 422829 Color: 1

Bin 3617: 475 of cap free
Amount of items: 2
Items: 
Size: 604857 Color: 1
Size: 394669 Color: 0

Bin 3618: 476 of cap free
Amount of items: 2
Items: 
Size: 634540 Color: 0
Size: 364985 Color: 1

Bin 3619: 477 of cap free
Amount of items: 2
Items: 
Size: 564088 Color: 1
Size: 435436 Color: 0

Bin 3620: 478 of cap free
Amount of items: 2
Items: 
Size: 504460 Color: 1
Size: 495063 Color: 0

Bin 3621: 479 of cap free
Amount of items: 2
Items: 
Size: 579806 Color: 1
Size: 419716 Color: 0

Bin 3622: 479 of cap free
Amount of items: 2
Items: 
Size: 618412 Color: 1
Size: 381110 Color: 0

Bin 3623: 480 of cap free
Amount of items: 2
Items: 
Size: 663211 Color: 0
Size: 336310 Color: 1

Bin 3624: 480 of cap free
Amount of items: 2
Items: 
Size: 684958 Color: 1
Size: 314563 Color: 0

Bin 3625: 481 of cap free
Amount of items: 2
Items: 
Size: 578060 Color: 0
Size: 421460 Color: 1

Bin 3626: 481 of cap free
Amount of items: 2
Items: 
Size: 713950 Color: 1
Size: 285570 Color: 0

Bin 3627: 483 of cap free
Amount of items: 2
Items: 
Size: 599322 Color: 0
Size: 400196 Color: 1

Bin 3628: 485 of cap free
Amount of items: 2
Items: 
Size: 519328 Color: 1
Size: 480188 Color: 0

Bin 3629: 485 of cap free
Amount of items: 2
Items: 
Size: 735514 Color: 1
Size: 264002 Color: 0

Bin 3630: 486 of cap free
Amount of items: 2
Items: 
Size: 689476 Color: 1
Size: 310039 Color: 0

Bin 3631: 486 of cap free
Amount of items: 2
Items: 
Size: 735813 Color: 0
Size: 263702 Color: 1

Bin 3632: 487 of cap free
Amount of items: 2
Items: 
Size: 618412 Color: 1
Size: 381102 Color: 0

Bin 3633: 487 of cap free
Amount of items: 2
Items: 
Size: 767409 Color: 1
Size: 232105 Color: 0

Bin 3634: 489 of cap free
Amount of items: 2
Items: 
Size: 562965 Color: 0
Size: 436547 Color: 1

Bin 3635: 490 of cap free
Amount of items: 2
Items: 
Size: 705446 Color: 1
Size: 294065 Color: 0

Bin 3636: 490 of cap free
Amount of items: 2
Items: 
Size: 588816 Color: 1
Size: 410695 Color: 0

Bin 3637: 490 of cap free
Amount of items: 2
Items: 
Size: 660402 Color: 0
Size: 339109 Color: 1

Bin 3638: 491 of cap free
Amount of items: 2
Items: 
Size: 780352 Color: 1
Size: 219158 Color: 0

Bin 3639: 493 of cap free
Amount of items: 2
Items: 
Size: 623675 Color: 0
Size: 375833 Color: 1

Bin 3640: 493 of cap free
Amount of items: 2
Items: 
Size: 626731 Color: 0
Size: 372777 Color: 1

Bin 3641: 496 of cap free
Amount of items: 2
Items: 
Size: 769144 Color: 1
Size: 230361 Color: 0

Bin 3642: 497 of cap free
Amount of items: 2
Items: 
Size: 742566 Color: 1
Size: 256938 Color: 0

Bin 3643: 497 of cap free
Amount of items: 2
Items: 
Size: 606366 Color: 0
Size: 393138 Color: 1

Bin 3644: 498 of cap free
Amount of items: 2
Items: 
Size: 736774 Color: 1
Size: 262729 Color: 0

Bin 3645: 501 of cap free
Amount of items: 2
Items: 
Size: 753372 Color: 0
Size: 246128 Color: 1

Bin 3646: 501 of cap free
Amount of items: 2
Items: 
Size: 539605 Color: 1
Size: 459895 Color: 0

Bin 3647: 501 of cap free
Amount of items: 2
Items: 
Size: 679810 Color: 0
Size: 319690 Color: 1

Bin 3648: 501 of cap free
Amount of items: 2
Items: 
Size: 791945 Color: 0
Size: 207555 Color: 1

Bin 3649: 502 of cap free
Amount of items: 2
Items: 
Size: 689429 Color: 0
Size: 310070 Color: 1

Bin 3650: 502 of cap free
Amount of items: 2
Items: 
Size: 693678 Color: 1
Size: 305821 Color: 0

Bin 3651: 503 of cap free
Amount of items: 2
Items: 
Size: 731355 Color: 1
Size: 268143 Color: 0

Bin 3652: 503 of cap free
Amount of items: 2
Items: 
Size: 740693 Color: 0
Size: 258805 Color: 1

Bin 3653: 504 of cap free
Amount of items: 2
Items: 
Size: 612333 Color: 1
Size: 387164 Color: 0

Bin 3654: 505 of cap free
Amount of items: 2
Items: 
Size: 599889 Color: 0
Size: 399607 Color: 1

Bin 3655: 511 of cap free
Amount of items: 2
Items: 
Size: 754028 Color: 0
Size: 245462 Color: 1

Bin 3656: 513 of cap free
Amount of items: 2
Items: 
Size: 758298 Color: 1
Size: 241190 Color: 0

Bin 3657: 514 of cap free
Amount of items: 2
Items: 
Size: 601343 Color: 1
Size: 398144 Color: 0

Bin 3658: 514 of cap free
Amount of items: 2
Items: 
Size: 747221 Color: 0
Size: 252266 Color: 1

Bin 3659: 515 of cap free
Amount of items: 2
Items: 
Size: 733986 Color: 1
Size: 265500 Color: 0

Bin 3660: 515 of cap free
Amount of items: 2
Items: 
Size: 782406 Color: 0
Size: 217080 Color: 1

Bin 3661: 516 of cap free
Amount of items: 2
Items: 
Size: 590538 Color: 1
Size: 408947 Color: 0

Bin 3662: 516 of cap free
Amount of items: 2
Items: 
Size: 665644 Color: 0
Size: 333841 Color: 1

Bin 3663: 516 of cap free
Amount of items: 2
Items: 
Size: 680860 Color: 0
Size: 318625 Color: 1

Bin 3664: 520 of cap free
Amount of items: 2
Items: 
Size: 786538 Color: 0
Size: 212943 Color: 1

Bin 3665: 522 of cap free
Amount of items: 2
Items: 
Size: 643658 Color: 0
Size: 355821 Color: 1

Bin 3666: 522 of cap free
Amount of items: 2
Items: 
Size: 700069 Color: 0
Size: 299410 Color: 1

Bin 3667: 524 of cap free
Amount of items: 2
Items: 
Size: 549072 Color: 1
Size: 450405 Color: 0

Bin 3668: 524 of cap free
Amount of items: 2
Items: 
Size: 552946 Color: 0
Size: 446531 Color: 1

Bin 3669: 524 of cap free
Amount of items: 2
Items: 
Size: 582067 Color: 1
Size: 417410 Color: 0

Bin 3670: 525 of cap free
Amount of items: 2
Items: 
Size: 793111 Color: 1
Size: 206365 Color: 0

Bin 3671: 526 of cap free
Amount of items: 2
Items: 
Size: 561030 Color: 1
Size: 438445 Color: 0

Bin 3672: 526 of cap free
Amount of items: 2
Items: 
Size: 585571 Color: 0
Size: 413904 Color: 1

Bin 3673: 527 of cap free
Amount of items: 2
Items: 
Size: 763131 Color: 1
Size: 236343 Color: 0

Bin 3674: 528 of cap free
Amount of items: 2
Items: 
Size: 550499 Color: 1
Size: 448974 Color: 0

Bin 3675: 528 of cap free
Amount of items: 2
Items: 
Size: 555134 Color: 0
Size: 444339 Color: 1

Bin 3676: 528 of cap free
Amount of items: 2
Items: 
Size: 729841 Color: 1
Size: 269632 Color: 0

Bin 3677: 529 of cap free
Amount of items: 2
Items: 
Size: 541353 Color: 0
Size: 458119 Color: 1

Bin 3678: 529 of cap free
Amount of items: 2
Items: 
Size: 757469 Color: 0
Size: 242003 Color: 1

Bin 3679: 532 of cap free
Amount of items: 2
Items: 
Size: 606334 Color: 0
Size: 393135 Color: 1

Bin 3680: 533 of cap free
Amount of items: 2
Items: 
Size: 513093 Color: 1
Size: 486375 Color: 0

Bin 3681: 533 of cap free
Amount of items: 2
Items: 
Size: 606120 Color: 1
Size: 393348 Color: 0

Bin 3682: 533 of cap free
Amount of items: 2
Items: 
Size: 623138 Color: 0
Size: 376330 Color: 1

Bin 3683: 537 of cap free
Amount of items: 2
Items: 
Size: 559554 Color: 0
Size: 439910 Color: 1

Bin 3684: 538 of cap free
Amount of items: 2
Items: 
Size: 515337 Color: 1
Size: 484126 Color: 0

Bin 3685: 539 of cap free
Amount of items: 2
Items: 
Size: 693649 Color: 1
Size: 305813 Color: 0

Bin 3686: 540 of cap free
Amount of items: 2
Items: 
Size: 539198 Color: 0
Size: 460263 Color: 1

Bin 3687: 541 of cap free
Amount of items: 2
Items: 
Size: 703346 Color: 0
Size: 296114 Color: 1

Bin 3688: 543 of cap free
Amount of items: 2
Items: 
Size: 667432 Color: 0
Size: 332026 Color: 1

Bin 3689: 543 of cap free
Amount of items: 2
Items: 
Size: 677395 Color: 0
Size: 322063 Color: 1

Bin 3690: 544 of cap free
Amount of items: 2
Items: 
Size: 760926 Color: 0
Size: 238531 Color: 1

Bin 3691: 545 of cap free
Amount of items: 2
Items: 
Size: 568356 Color: 0
Size: 431100 Color: 1

Bin 3692: 545 of cap free
Amount of items: 2
Items: 
Size: 723203 Color: 1
Size: 276253 Color: 0

Bin 3693: 546 of cap free
Amount of items: 2
Items: 
Size: 596333 Color: 1
Size: 403122 Color: 0

Bin 3694: 548 of cap free
Amount of items: 2
Items: 
Size: 595149 Color: 1
Size: 404304 Color: 0

Bin 3695: 549 of cap free
Amount of items: 2
Items: 
Size: 776846 Color: 1
Size: 222606 Color: 0

Bin 3696: 550 of cap free
Amount of items: 2
Items: 
Size: 641153 Color: 0
Size: 358298 Color: 1

Bin 3697: 551 of cap free
Amount of items: 2
Items: 
Size: 731331 Color: 1
Size: 268119 Color: 0

Bin 3698: 551 of cap free
Amount of items: 2
Items: 
Size: 758786 Color: 0
Size: 240664 Color: 1

Bin 3699: 551 of cap free
Amount of items: 2
Items: 
Size: 770073 Color: 1
Size: 229377 Color: 0

Bin 3700: 554 of cap free
Amount of items: 2
Items: 
Size: 519310 Color: 1
Size: 480137 Color: 0

Bin 3701: 554 of cap free
Amount of items: 2
Items: 
Size: 550752 Color: 0
Size: 448695 Color: 1

Bin 3702: 556 of cap free
Amount of items: 2
Items: 
Size: 732213 Color: 1
Size: 267232 Color: 0

Bin 3703: 557 of cap free
Amount of items: 2
Items: 
Size: 729397 Color: 0
Size: 270047 Color: 1

Bin 3704: 557 of cap free
Amount of items: 2
Items: 
Size: 613127 Color: 0
Size: 386317 Color: 1

Bin 3705: 558 of cap free
Amount of items: 2
Items: 
Size: 784113 Color: 0
Size: 215330 Color: 1

Bin 3706: 559 of cap free
Amount of items: 2
Items: 
Size: 579783 Color: 1
Size: 419659 Color: 0

Bin 3707: 559 of cap free
Amount of items: 2
Items: 
Size: 605673 Color: 0
Size: 393769 Color: 1

Bin 3708: 560 of cap free
Amount of items: 2
Items: 
Size: 513996 Color: 0
Size: 485445 Color: 1

Bin 3709: 563 of cap free
Amount of items: 2
Items: 
Size: 542445 Color: 0
Size: 456993 Color: 1

Bin 3710: 564 of cap free
Amount of items: 2
Items: 
Size: 576630 Color: 0
Size: 422807 Color: 1

Bin 3711: 564 of cap free
Amount of items: 2
Items: 
Size: 737103 Color: 0
Size: 262334 Color: 1

Bin 3712: 564 of cap free
Amount of items: 2
Items: 
Size: 776600 Color: 0
Size: 222837 Color: 1

Bin 3713: 565 of cap free
Amount of items: 2
Items: 
Size: 621610 Color: 1
Size: 377826 Color: 0

Bin 3714: 565 of cap free
Amount of items: 2
Items: 
Size: 785773 Color: 0
Size: 213663 Color: 1

Bin 3715: 568 of cap free
Amount of items: 2
Items: 
Size: 669031 Color: 0
Size: 330402 Color: 1

Bin 3716: 571 of cap free
Amount of items: 2
Items: 
Size: 536122 Color: 0
Size: 463308 Color: 1

Bin 3717: 572 of cap free
Amount of items: 2
Items: 
Size: 724738 Color: 1
Size: 274691 Color: 0

Bin 3718: 572 of cap free
Amount of items: 2
Items: 
Size: 747679 Color: 1
Size: 251750 Color: 0

Bin 3719: 573 of cap free
Amount of items: 2
Items: 
Size: 575055 Color: 1
Size: 424373 Color: 0

Bin 3720: 573 of cap free
Amount of items: 2
Items: 
Size: 603092 Color: 1
Size: 396336 Color: 0

Bin 3721: 573 of cap free
Amount of items: 2
Items: 
Size: 639214 Color: 0
Size: 360214 Color: 1

Bin 3722: 573 of cap free
Amount of items: 2
Items: 
Size: 640334 Color: 1
Size: 359094 Color: 0

Bin 3723: 577 of cap free
Amount of items: 2
Items: 
Size: 575208 Color: 0
Size: 424216 Color: 1

Bin 3724: 578 of cap free
Amount of items: 2
Items: 
Size: 576622 Color: 0
Size: 422801 Color: 1

Bin 3725: 580 of cap free
Amount of items: 2
Items: 
Size: 683971 Color: 1
Size: 315450 Color: 0

Bin 3726: 580 of cap free
Amount of items: 2
Items: 
Size: 626028 Color: 0
Size: 373393 Color: 1

Bin 3727: 580 of cap free
Amount of items: 2
Items: 
Size: 669680 Color: 1
Size: 329741 Color: 0

Bin 3728: 583 of cap free
Amount of items: 2
Items: 
Size: 645393 Color: 0
Size: 354025 Color: 1

Bin 3729: 585 of cap free
Amount of items: 2
Items: 
Size: 590187 Color: 0
Size: 409229 Color: 1

Bin 3730: 585 of cap free
Amount of items: 2
Items: 
Size: 724050 Color: 0
Size: 275366 Color: 1

Bin 3731: 588 of cap free
Amount of items: 2
Items: 
Size: 571469 Color: 1
Size: 427944 Color: 0

Bin 3732: 588 of cap free
Amount of items: 2
Items: 
Size: 664518 Color: 0
Size: 334895 Color: 1

Bin 3733: 591 of cap free
Amount of items: 2
Items: 
Size: 656140 Color: 1
Size: 343270 Color: 0

Bin 3734: 593 of cap free
Amount of items: 2
Items: 
Size: 703859 Color: 1
Size: 295549 Color: 0

Bin 3735: 593 of cap free
Amount of items: 2
Items: 
Size: 722755 Color: 0
Size: 276653 Color: 1

Bin 3736: 595 of cap free
Amount of items: 2
Items: 
Size: 711227 Color: 0
Size: 288179 Color: 1

Bin 3737: 596 of cap free
Amount of items: 2
Items: 
Size: 530022 Color: 0
Size: 469383 Color: 1

Bin 3738: 597 of cap free
Amount of items: 2
Items: 
Size: 519003 Color: 0
Size: 480401 Color: 1

Bin 3739: 598 of cap free
Amount of items: 2
Items: 
Size: 587765 Color: 1
Size: 411638 Color: 0

Bin 3740: 600 of cap free
Amount of items: 2
Items: 
Size: 701352 Color: 1
Size: 298049 Color: 0

Bin 3741: 600 of cap free
Amount of items: 2
Items: 
Size: 747194 Color: 0
Size: 252207 Color: 1

Bin 3742: 601 of cap free
Amount of items: 2
Items: 
Size: 575200 Color: 0
Size: 424200 Color: 1

Bin 3743: 602 of cap free
Amount of items: 2
Items: 
Size: 758737 Color: 0
Size: 240662 Color: 1

Bin 3744: 603 of cap free
Amount of items: 2
Items: 
Size: 590497 Color: 1
Size: 408901 Color: 0

Bin 3745: 603 of cap free
Amount of items: 2
Items: 
Size: 695022 Color: 1
Size: 304376 Color: 0

Bin 3746: 606 of cap free
Amount of items: 2
Items: 
Size: 502399 Color: 1
Size: 496996 Color: 0

Bin 3747: 606 of cap free
Amount of items: 2
Items: 
Size: 632252 Color: 1
Size: 367143 Color: 0

Bin 3748: 607 of cap free
Amount of items: 2
Items: 
Size: 572198 Color: 0
Size: 427196 Color: 1

Bin 3749: 607 of cap free
Amount of items: 2
Items: 
Size: 577062 Color: 1
Size: 422332 Color: 0

Bin 3750: 607 of cap free
Amount of items: 2
Items: 
Size: 767378 Color: 0
Size: 232016 Color: 1

Bin 3751: 608 of cap free
Amount of items: 2
Items: 
Size: 641666 Color: 1
Size: 357727 Color: 0

Bin 3752: 610 of cap free
Amount of items: 2
Items: 
Size: 721245 Color: 0
Size: 278146 Color: 1

Bin 3753: 610 of cap free
Amount of items: 2
Items: 
Size: 544535 Color: 0
Size: 454856 Color: 1

Bin 3754: 611 of cap free
Amount of items: 2
Items: 
Size: 609259 Color: 0
Size: 390131 Color: 1

Bin 3755: 614 of cap free
Amount of items: 2
Items: 
Size: 511419 Color: 0
Size: 487968 Color: 1

Bin 3756: 618 of cap free
Amount of items: 2
Items: 
Size: 601924 Color: 0
Size: 397459 Color: 1

Bin 3757: 618 of cap free
Amount of items: 2
Items: 
Size: 631201 Color: 1
Size: 368182 Color: 0

Bin 3758: 618 of cap free
Amount of items: 2
Items: 
Size: 799432 Color: 0
Size: 199951 Color: 1

Bin 3759: 620 of cap free
Amount of items: 2
Items: 
Size: 771849 Color: 0
Size: 227532 Color: 1

Bin 3760: 622 of cap free
Amount of items: 2
Items: 
Size: 536073 Color: 0
Size: 463306 Color: 1

Bin 3761: 622 of cap free
Amount of items: 2
Items: 
Size: 735474 Color: 1
Size: 263905 Color: 0

Bin 3762: 623 of cap free
Amount of items: 2
Items: 
Size: 570309 Color: 0
Size: 429069 Color: 1

Bin 3763: 623 of cap free
Amount of items: 2
Items: 
Size: 666761 Color: 1
Size: 332617 Color: 0

Bin 3764: 624 of cap free
Amount of items: 2
Items: 
Size: 771783 Color: 1
Size: 227594 Color: 0

Bin 3765: 628 of cap free
Amount of items: 2
Items: 
Size: 506442 Color: 0
Size: 492931 Color: 1

Bin 3766: 628 of cap free
Amount of items: 2
Items: 
Size: 680117 Color: 1
Size: 319256 Color: 0

Bin 3767: 629 of cap free
Amount of items: 2
Items: 
Size: 787666 Color: 0
Size: 211706 Color: 1

Bin 3768: 630 of cap free
Amount of items: 2
Items: 
Size: 603062 Color: 1
Size: 396309 Color: 0

Bin 3769: 630 of cap free
Amount of items: 2
Items: 
Size: 603450 Color: 0
Size: 395921 Color: 1

Bin 3770: 630 of cap free
Amount of items: 2
Items: 
Size: 708574 Color: 1
Size: 290797 Color: 0

Bin 3771: 632 of cap free
Amount of items: 2
Items: 
Size: 680116 Color: 1
Size: 319253 Color: 0

Bin 3772: 633 of cap free
Amount of items: 2
Items: 
Size: 649891 Color: 0
Size: 349477 Color: 1

Bin 3773: 634 of cap free
Amount of items: 2
Items: 
Size: 718765 Color: 0
Size: 280602 Color: 1

Bin 3774: 634 of cap free
Amount of items: 2
Items: 
Size: 782316 Color: 0
Size: 217051 Color: 1

Bin 3775: 635 of cap free
Amount of items: 2
Items: 
Size: 519282 Color: 1
Size: 480084 Color: 0

Bin 3776: 637 of cap free
Amount of items: 2
Items: 
Size: 525712 Color: 0
Size: 473652 Color: 1

Bin 3777: 638 of cap free
Amount of items: 2
Items: 
Size: 644489 Color: 0
Size: 354874 Color: 1

Bin 3778: 639 of cap free
Amount of items: 2
Items: 
Size: 716523 Color: 0
Size: 282839 Color: 1

Bin 3779: 640 of cap free
Amount of items: 2
Items: 
Size: 612985 Color: 1
Size: 386376 Color: 0

Bin 3780: 640 of cap free
Amount of items: 2
Items: 
Size: 616630 Color: 0
Size: 382731 Color: 1

Bin 3781: 641 of cap free
Amount of items: 2
Items: 
Size: 785106 Color: 1
Size: 214254 Color: 0

Bin 3782: 642 of cap free
Amount of items: 2
Items: 
Size: 743586 Color: 0
Size: 255773 Color: 1

Bin 3783: 643 of cap free
Amount of items: 2
Items: 
Size: 639209 Color: 0
Size: 360149 Color: 1

Bin 3784: 649 of cap free
Amount of items: 2
Items: 
Size: 674925 Color: 1
Size: 324427 Color: 0

Bin 3785: 654 of cap free
Amount of items: 2
Items: 
Size: 526340 Color: 1
Size: 473007 Color: 0

Bin 3786: 656 of cap free
Amount of items: 2
Items: 
Size: 521589 Color: 0
Size: 477756 Color: 1

Bin 3787: 657 of cap free
Amount of items: 2
Items: 
Size: 549906 Color: 0
Size: 449438 Color: 1

Bin 3788: 659 of cap free
Amount of items: 2
Items: 
Size: 553457 Color: 1
Size: 445885 Color: 0

Bin 3789: 660 of cap free
Amount of items: 2
Items: 
Size: 562805 Color: 0
Size: 436536 Color: 1

Bin 3790: 661 of cap free
Amount of items: 2
Items: 
Size: 738389 Color: 0
Size: 260951 Color: 1

Bin 3791: 661 of cap free
Amount of items: 2
Items: 
Size: 539145 Color: 0
Size: 460195 Color: 1

Bin 3792: 662 of cap free
Amount of items: 2
Items: 
Size: 799613 Color: 1
Size: 199726 Color: 0

Bin 3793: 663 of cap free
Amount of items: 2
Items: 
Size: 718033 Color: 0
Size: 281305 Color: 1

Bin 3794: 665 of cap free
Amount of items: 2
Items: 
Size: 666166 Color: 0
Size: 333170 Color: 1

Bin 3795: 666 of cap free
Amount of items: 2
Items: 
Size: 578357 Color: 1
Size: 420978 Color: 0

Bin 3796: 666 of cap free
Amount of items: 2
Items: 
Size: 637715 Color: 1
Size: 361620 Color: 0

Bin 3797: 666 of cap free
Amount of items: 2
Items: 
Size: 764304 Color: 0
Size: 235031 Color: 1

Bin 3798: 668 of cap free
Amount of items: 2
Items: 
Size: 713831 Color: 1
Size: 285502 Color: 0

Bin 3799: 671 of cap free
Amount of items: 2
Items: 
Size: 681177 Color: 1
Size: 318153 Color: 0

Bin 3800: 672 of cap free
Amount of items: 2
Items: 
Size: 557184 Color: 0
Size: 442145 Color: 1

Bin 3801: 673 of cap free
Amount of items: 2
Items: 
Size: 542852 Color: 1
Size: 456476 Color: 0

Bin 3802: 673 of cap free
Amount of items: 2
Items: 
Size: 758692 Color: 0
Size: 240636 Color: 1

Bin 3803: 676 of cap free
Amount of items: 2
Items: 
Size: 721806 Color: 1
Size: 277519 Color: 0

Bin 3804: 677 of cap free
Amount of items: 2
Items: 
Size: 528999 Color: 0
Size: 470325 Color: 1

Bin 3805: 677 of cap free
Amount of items: 2
Items: 
Size: 675943 Color: 1
Size: 323381 Color: 0

Bin 3806: 679 of cap free
Amount of items: 2
Items: 
Size: 656841 Color: 0
Size: 342481 Color: 1

Bin 3807: 679 of cap free
Amount of items: 2
Items: 
Size: 796752 Color: 0
Size: 202570 Color: 1

Bin 3808: 682 of cap free
Amount of items: 2
Items: 
Size: 505578 Color: 1
Size: 493741 Color: 0

Bin 3809: 682 of cap free
Amount of items: 2
Items: 
Size: 622250 Color: 1
Size: 377069 Color: 0

Bin 3810: 687 of cap free
Amount of items: 2
Items: 
Size: 501528 Color: 0
Size: 497786 Color: 1

Bin 3811: 689 of cap free
Amount of items: 2
Items: 
Size: 556960 Color: 1
Size: 442352 Color: 0

Bin 3812: 690 of cap free
Amount of items: 2
Items: 
Size: 768988 Color: 0
Size: 230323 Color: 1

Bin 3813: 690 of cap free
Amount of items: 2
Items: 
Size: 725672 Color: 0
Size: 273639 Color: 1

Bin 3814: 692 of cap free
Amount of items: 2
Items: 
Size: 565342 Color: 0
Size: 433967 Color: 1

Bin 3815: 692 of cap free
Amount of items: 2
Items: 
Size: 574010 Color: 0
Size: 425299 Color: 1

Bin 3816: 692 of cap free
Amount of items: 2
Items: 
Size: 595896 Color: 0
Size: 403413 Color: 1

Bin 3817: 692 of cap free
Amount of items: 2
Items: 
Size: 709652 Color: 0
Size: 289657 Color: 1

Bin 3818: 693 of cap free
Amount of items: 2
Items: 
Size: 795270 Color: 1
Size: 204038 Color: 0

Bin 3819: 695 of cap free
Amount of items: 2
Items: 
Size: 524345 Color: 1
Size: 474961 Color: 0

Bin 3820: 696 of cap free
Amount of items: 2
Items: 
Size: 746410 Color: 0
Size: 252895 Color: 1

Bin 3821: 699 of cap free
Amount of items: 2
Items: 
Size: 583825 Color: 0
Size: 415477 Color: 1

Bin 3822: 700 of cap free
Amount of items: 2
Items: 
Size: 606333 Color: 0
Size: 392968 Color: 1

Bin 3823: 701 of cap free
Amount of items: 2
Items: 
Size: 656834 Color: 0
Size: 342466 Color: 1

Bin 3824: 703 of cap free
Amount of items: 2
Items: 
Size: 653186 Color: 1
Size: 346112 Color: 0

Bin 3825: 706 of cap free
Amount of items: 2
Items: 
Size: 783278 Color: 0
Size: 216017 Color: 1

Bin 3826: 707 of cap free
Amount of items: 2
Items: 
Size: 527151 Color: 1
Size: 472143 Color: 0

Bin 3827: 707 of cap free
Amount of items: 2
Items: 
Size: 551486 Color: 1
Size: 447808 Color: 0

Bin 3828: 707 of cap free
Amount of items: 2
Items: 
Size: 734979 Color: 0
Size: 264315 Color: 1

Bin 3829: 710 of cap free
Amount of items: 2
Items: 
Size: 660297 Color: 0
Size: 338994 Color: 1

Bin 3830: 712 of cap free
Amount of items: 2
Items: 
Size: 713806 Color: 1
Size: 285483 Color: 0

Bin 3831: 713 of cap free
Amount of items: 2
Items: 
Size: 659212 Color: 0
Size: 340076 Color: 1

Bin 3832: 714 of cap free
Amount of items: 2
Items: 
Size: 720017 Color: 1
Size: 279270 Color: 0

Bin 3833: 714 of cap free
Amount of items: 2
Items: 
Size: 747160 Color: 0
Size: 252127 Color: 1

Bin 3834: 718 of cap free
Amount of items: 2
Items: 
Size: 617489 Color: 1
Size: 381794 Color: 0

Bin 3835: 719 of cap free
Amount of items: 2
Items: 
Size: 767325 Color: 0
Size: 231957 Color: 1

Bin 3836: 723 of cap free
Amount of items: 2
Items: 
Size: 526380 Color: 0
Size: 472898 Color: 1

Bin 3837: 725 of cap free
Amount of items: 2
Items: 
Size: 613072 Color: 0
Size: 386204 Color: 1

Bin 3838: 725 of cap free
Amount of items: 2
Items: 
Size: 661711 Color: 0
Size: 337565 Color: 1

Bin 3839: 727 of cap free
Amount of items: 2
Items: 
Size: 654228 Color: 0
Size: 345046 Color: 1

Bin 3840: 729 of cap free
Amount of items: 2
Items: 
Size: 654798 Color: 1
Size: 344474 Color: 0

Bin 3841: 729 of cap free
Amount of items: 2
Items: 
Size: 693520 Color: 1
Size: 305752 Color: 0

Bin 3842: 730 of cap free
Amount of items: 2
Items: 
Size: 526319 Color: 1
Size: 472952 Color: 0

Bin 3843: 731 of cap free
Amount of items: 2
Items: 
Size: 572884 Color: 0
Size: 426386 Color: 1

Bin 3844: 732 of cap free
Amount of items: 2
Items: 
Size: 729100 Color: 1
Size: 270169 Color: 0

Bin 3845: 734 of cap free
Amount of items: 2
Items: 
Size: 656809 Color: 0
Size: 342458 Color: 1

Bin 3846: 737 of cap free
Amount of items: 2
Items: 
Size: 749935 Color: 0
Size: 249329 Color: 1

Bin 3847: 737 of cap free
Amount of items: 2
Items: 
Size: 661409 Color: 1
Size: 337855 Color: 0

Bin 3848: 737 of cap free
Amount of items: 2
Items: 
Size: 742094 Color: 0
Size: 257170 Color: 1

Bin 3849: 740 of cap free
Amount of items: 2
Items: 
Size: 506706 Color: 1
Size: 492555 Color: 0

Bin 3850: 740 of cap free
Amount of items: 2
Items: 
Size: 524073 Color: 0
Size: 475188 Color: 1

Bin 3851: 740 of cap free
Amount of items: 2
Items: 
Size: 656783 Color: 1
Size: 342478 Color: 0

Bin 3852: 740 of cap free
Amount of items: 2
Items: 
Size: 671601 Color: 1
Size: 327660 Color: 0

Bin 3853: 740 of cap free
Amount of items: 2
Items: 
Size: 760068 Color: 1
Size: 239193 Color: 0

Bin 3854: 742 of cap free
Amount of items: 2
Items: 
Size: 662995 Color: 0
Size: 336264 Color: 1

Bin 3855: 746 of cap free
Amount of items: 2
Items: 
Size: 575007 Color: 1
Size: 424248 Color: 0

Bin 3856: 747 of cap free
Amount of items: 2
Items: 
Size: 670415 Color: 0
Size: 328839 Color: 1

Bin 3857: 747 of cap free
Amount of items: 2
Items: 
Size: 776832 Color: 1
Size: 222422 Color: 0

Bin 3858: 749 of cap free
Amount of items: 2
Items: 
Size: 768043 Color: 1
Size: 231209 Color: 0

Bin 3859: 750 of cap free
Amount of items: 2
Items: 
Size: 612979 Color: 1
Size: 386272 Color: 0

Bin 3860: 751 of cap free
Amount of items: 2
Items: 
Size: 685832 Color: 0
Size: 313418 Color: 1

Bin 3861: 751 of cap free
Amount of items: 2
Items: 
Size: 747157 Color: 0
Size: 252093 Color: 1

Bin 3862: 752 of cap free
Amount of items: 2
Items: 
Size: 720000 Color: 1
Size: 279249 Color: 0

Bin 3863: 752 of cap free
Amount of items: 2
Items: 
Size: 637710 Color: 1
Size: 361539 Color: 0

Bin 3864: 754 of cap free
Amount of items: 2
Items: 
Size: 629043 Color: 1
Size: 370204 Color: 0

Bin 3865: 755 of cap free
Amount of items: 2
Items: 
Size: 615265 Color: 1
Size: 383981 Color: 0

Bin 3866: 758 of cap free
Amount of items: 2
Items: 
Size: 564009 Color: 1
Size: 435234 Color: 0

Bin 3867: 762 of cap free
Amount of items: 2
Items: 
Size: 752263 Color: 1
Size: 246976 Color: 0

Bin 3868: 762 of cap free
Amount of items: 2
Items: 
Size: 546409 Color: 0
Size: 452830 Color: 1

Bin 3869: 763 of cap free
Amount of items: 2
Items: 
Size: 622207 Color: 1
Size: 377031 Color: 0

Bin 3870: 764 of cap free
Amount of items: 2
Items: 
Size: 607194 Color: 0
Size: 392043 Color: 1

Bin 3871: 765 of cap free
Amount of items: 2
Items: 
Size: 534472 Color: 1
Size: 464764 Color: 0

Bin 3872: 766 of cap free
Amount of items: 2
Items: 
Size: 739749 Color: 1
Size: 259486 Color: 0

Bin 3873: 768 of cap free
Amount of items: 2
Items: 
Size: 731259 Color: 1
Size: 267974 Color: 0

Bin 3874: 769 of cap free
Amount of items: 2
Items: 
Size: 614387 Color: 1
Size: 384845 Color: 0

Bin 3875: 769 of cap free
Amount of items: 2
Items: 
Size: 718021 Color: 1
Size: 281211 Color: 0

Bin 3876: 770 of cap free
Amount of items: 2
Items: 
Size: 675793 Color: 0
Size: 323438 Color: 1

Bin 3877: 774 of cap free
Amount of items: 2
Items: 
Size: 755779 Color: 0
Size: 243448 Color: 1

Bin 3878: 784 of cap free
Amount of items: 2
Items: 
Size: 505627 Color: 0
Size: 493590 Color: 1

Bin 3879: 785 of cap free
Amount of items: 2
Items: 
Size: 785026 Color: 1
Size: 214190 Color: 0

Bin 3880: 785 of cap free
Amount of items: 2
Items: 
Size: 634486 Color: 1
Size: 364730 Color: 0

Bin 3881: 785 of cap free
Amount of items: 2
Items: 
Size: 664037 Color: 1
Size: 335179 Color: 0

Bin 3882: 786 of cap free
Amount of items: 2
Items: 
Size: 683125 Color: 1
Size: 316090 Color: 0

Bin 3883: 787 of cap free
Amount of items: 2
Items: 
Size: 596256 Color: 1
Size: 402958 Color: 0

Bin 3884: 791 of cap free
Amount of items: 2
Items: 
Size: 783243 Color: 0
Size: 215967 Color: 1

Bin 3885: 793 of cap free
Amount of items: 2
Items: 
Size: 717934 Color: 0
Size: 281274 Color: 1

Bin 3886: 795 of cap free
Amount of items: 2
Items: 
Size: 638244 Color: 0
Size: 360962 Color: 1

Bin 3887: 803 of cap free
Amount of items: 2
Items: 
Size: 608238 Color: 0
Size: 390960 Color: 1

Bin 3888: 805 of cap free
Amount of items: 2
Items: 
Size: 678181 Color: 1
Size: 321015 Color: 0

Bin 3889: 807 of cap free
Amount of items: 2
Items: 
Size: 744674 Color: 0
Size: 254520 Color: 1

Bin 3890: 807 of cap free
Amount of items: 2
Items: 
Size: 562702 Color: 0
Size: 436492 Color: 1

Bin 3891: 807 of cap free
Amount of items: 2
Items: 
Size: 706539 Color: 1
Size: 292655 Color: 0

Bin 3892: 817 of cap free
Amount of items: 2
Items: 
Size: 620333 Color: 1
Size: 378851 Color: 0

Bin 3893: 820 of cap free
Amount of items: 2
Items: 
Size: 709601 Color: 0
Size: 289580 Color: 1

Bin 3894: 823 of cap free
Amount of items: 2
Items: 
Size: 535399 Color: 1
Size: 463779 Color: 0

Bin 3895: 833 of cap free
Amount of items: 2
Items: 
Size: 713804 Color: 1
Size: 285364 Color: 0

Bin 3896: 835 of cap free
Amount of items: 2
Items: 
Size: 632652 Color: 0
Size: 366514 Color: 1

Bin 3897: 835 of cap free
Amount of items: 2
Items: 
Size: 691438 Color: 1
Size: 307728 Color: 0

Bin 3898: 836 of cap free
Amount of items: 2
Items: 
Size: 661319 Color: 1
Size: 337846 Color: 0

Bin 3899: 836 of cap free
Amount of items: 2
Items: 
Size: 736445 Color: 1
Size: 262720 Color: 0

Bin 3900: 838 of cap free
Amount of items: 2
Items: 
Size: 670406 Color: 0
Size: 328757 Color: 1

Bin 3901: 839 of cap free
Amount of items: 2
Items: 
Size: 574998 Color: 1
Size: 424164 Color: 0

Bin 3902: 840 of cap free
Amount of items: 2
Items: 
Size: 539349 Color: 1
Size: 459812 Color: 0

Bin 3903: 840 of cap free
Amount of items: 2
Items: 
Size: 668823 Color: 1
Size: 330338 Color: 0

Bin 3904: 842 of cap free
Amount of items: 2
Items: 
Size: 733822 Color: 1
Size: 265337 Color: 0

Bin 3905: 842 of cap free
Amount of items: 2
Items: 
Size: 760047 Color: 1
Size: 239112 Color: 0

Bin 3906: 843 of cap free
Amount of items: 2
Items: 
Size: 782150 Color: 0
Size: 217008 Color: 1

Bin 3907: 846 of cap free
Amount of items: 2
Items: 
Size: 703608 Color: 1
Size: 295547 Color: 0

Bin 3908: 846 of cap free
Amount of items: 2
Items: 
Size: 727886 Color: 0
Size: 271269 Color: 1

Bin 3909: 850 of cap free
Amount of items: 2
Items: 
Size: 742011 Color: 0
Size: 257140 Color: 1

Bin 3910: 853 of cap free
Amount of items: 2
Items: 
Size: 534655 Color: 0
Size: 464493 Color: 1

Bin 3911: 853 of cap free
Amount of items: 2
Items: 
Size: 793346 Color: 0
Size: 205802 Color: 1

Bin 3912: 855 of cap free
Amount of items: 2
Items: 
Size: 613037 Color: 0
Size: 386109 Color: 1

Bin 3913: 856 of cap free
Amount of items: 2
Items: 
Size: 774341 Color: 0
Size: 224804 Color: 1

Bin 3914: 856 of cap free
Amount of items: 2
Items: 
Size: 663976 Color: 1
Size: 335169 Color: 0

Bin 3915: 859 of cap free
Amount of items: 2
Items: 
Size: 594029 Color: 1
Size: 405113 Color: 0

Bin 3916: 860 of cap free
Amount of items: 2
Items: 
Size: 534428 Color: 1
Size: 464713 Color: 0

Bin 3917: 861 of cap free
Amount of items: 2
Items: 
Size: 540951 Color: 1
Size: 458189 Color: 0

Bin 3918: 863 of cap free
Amount of items: 2
Items: 
Size: 502238 Color: 1
Size: 496900 Color: 0

Bin 3919: 867 of cap free
Amount of items: 2
Items: 
Size: 721157 Color: 0
Size: 277977 Color: 1

Bin 3920: 869 of cap free
Amount of items: 2
Items: 
Size: 550452 Color: 1
Size: 448680 Color: 0

Bin 3921: 869 of cap free
Amount of items: 2
Items: 
Size: 643482 Color: 0
Size: 355650 Color: 1

Bin 3922: 870 of cap free
Amount of items: 2
Items: 
Size: 699992 Color: 1
Size: 299139 Color: 0

Bin 3923: 872 of cap free
Amount of items: 2
Items: 
Size: 742003 Color: 0
Size: 257126 Color: 1

Bin 3924: 872 of cap free
Amount of items: 2
Items: 
Size: 635448 Color: 0
Size: 363681 Color: 1

Bin 3925: 872 of cap free
Amount of items: 2
Items: 
Size: 709569 Color: 0
Size: 289560 Color: 1

Bin 3926: 875 of cap free
Amount of items: 3
Items: 
Size: 468316 Color: 1
Size: 342447 Color: 1
Size: 188363 Color: 0

Bin 3927: 876 of cap free
Amount of items: 2
Items: 
Size: 558664 Color: 1
Size: 440461 Color: 0

Bin 3928: 877 of cap free
Amount of items: 2
Items: 
Size: 735316 Color: 1
Size: 263808 Color: 0

Bin 3929: 886 of cap free
Amount of items: 2
Items: 
Size: 794234 Color: 0
Size: 204881 Color: 1

Bin 3930: 887 of cap free
Amount of items: 2
Items: 
Size: 752178 Color: 1
Size: 246936 Color: 0

Bin 3931: 890 of cap free
Amount of items: 2
Items: 
Size: 538214 Color: 0
Size: 460897 Color: 1

Bin 3932: 892 of cap free
Amount of items: 2
Items: 
Size: 529710 Color: 1
Size: 469399 Color: 0

Bin 3933: 893 of cap free
Amount of items: 2
Items: 
Size: 687061 Color: 1
Size: 312047 Color: 0

Bin 3934: 894 of cap free
Amount of items: 2
Items: 
Size: 580516 Color: 1
Size: 418591 Color: 0

Bin 3935: 895 of cap free
Amount of items: 2
Items: 
Size: 541188 Color: 0
Size: 457918 Color: 1

Bin 3936: 898 of cap free
Amount of items: 2
Items: 
Size: 649108 Color: 1
Size: 349995 Color: 0

Bin 3937: 899 of cap free
Amount of items: 2
Items: 
Size: 521425 Color: 0
Size: 477677 Color: 1

Bin 3938: 900 of cap free
Amount of items: 2
Items: 
Size: 653134 Color: 0
Size: 345967 Color: 1

Bin 3939: 902 of cap free
Amount of items: 2
Items: 
Size: 722491 Color: 0
Size: 276608 Color: 1

Bin 3940: 903 of cap free
Amount of items: 2
Items: 
Size: 675684 Color: 0
Size: 323414 Color: 1

Bin 3941: 905 of cap free
Amount of items: 2
Items: 
Size: 560965 Color: 1
Size: 438131 Color: 0

Bin 3942: 905 of cap free
Amount of items: 2
Items: 
Size: 499699 Color: 1
Size: 499397 Color: 0

Bin 3943: 907 of cap free
Amount of items: 2
Items: 
Size: 503085 Color: 0
Size: 496009 Color: 1

Bin 3944: 908 of cap free
Amount of items: 2
Items: 
Size: 583770 Color: 0
Size: 415323 Color: 1

Bin 3945: 914 of cap free
Amount of items: 2
Items: 
Size: 714348 Color: 0
Size: 284739 Color: 1

Bin 3946: 924 of cap free
Amount of items: 2
Items: 
Size: 710548 Color: 1
Size: 288529 Color: 0

Bin 3947: 926 of cap free
Amount of items: 2
Items: 
Size: 577008 Color: 1
Size: 422067 Color: 0

Bin 3948: 930 of cap free
Amount of items: 2
Items: 
Size: 758270 Color: 1
Size: 240801 Color: 0

Bin 3949: 934 of cap free
Amount of items: 2
Items: 
Size: 546352 Color: 0
Size: 452715 Color: 1

Bin 3950: 935 of cap free
Amount of items: 2
Items: 
Size: 578353 Color: 1
Size: 420713 Color: 0

Bin 3951: 938 of cap free
Amount of items: 2
Items: 
Size: 583762 Color: 0
Size: 415301 Color: 1

Bin 3952: 943 of cap free
Amount of items: 2
Items: 
Size: 544315 Color: 0
Size: 454743 Color: 1

Bin 3953: 944 of cap free
Amount of items: 2
Items: 
Size: 779270 Color: 1
Size: 219787 Color: 0

Bin 3954: 944 of cap free
Amount of items: 2
Items: 
Size: 638242 Color: 0
Size: 360815 Color: 1

Bin 3955: 949 of cap free
Amount of items: 2
Items: 
Size: 521050 Color: 1
Size: 478002 Color: 0

Bin 3956: 950 of cap free
Amount of items: 2
Items: 
Size: 635869 Color: 1
Size: 363182 Color: 0

Bin 3957: 951 of cap free
Amount of items: 2
Items: 
Size: 562570 Color: 0
Size: 436480 Color: 1

Bin 3958: 960 of cap free
Amount of items: 2
Items: 
Size: 584380 Color: 1
Size: 414661 Color: 0

Bin 3959: 962 of cap free
Amount of items: 2
Items: 
Size: 620234 Color: 1
Size: 378805 Color: 0

Bin 3960: 964 of cap free
Amount of items: 2
Items: 
Size: 535328 Color: 1
Size: 463709 Color: 0

Bin 3961: 966 of cap free
Amount of items: 2
Items: 
Size: 659457 Color: 1
Size: 339578 Color: 0

Bin 3962: 969 of cap free
Amount of items: 2
Items: 
Size: 542430 Color: 0
Size: 456602 Color: 1

Bin 3963: 971 of cap free
Amount of items: 2
Items: 
Size: 641086 Color: 0
Size: 357944 Color: 1

Bin 3964: 976 of cap free
Amount of items: 2
Items: 
Size: 659014 Color: 0
Size: 340011 Color: 1

Bin 3965: 977 of cap free
Amount of items: 2
Items: 
Size: 583732 Color: 0
Size: 415292 Color: 1

Bin 3966: 978 of cap free
Amount of items: 2
Items: 
Size: 632149 Color: 1
Size: 366874 Color: 0

Bin 3967: 979 of cap free
Amount of items: 2
Items: 
Size: 758251 Color: 1
Size: 240771 Color: 0

Bin 3968: 980 of cap free
Amount of items: 2
Items: 
Size: 748940 Color: 1
Size: 250081 Color: 0

Bin 3969: 982 of cap free
Amount of items: 2
Items: 
Size: 744670 Color: 0
Size: 254349 Color: 1

Bin 3970: 983 of cap free
Amount of items: 2
Items: 
Size: 790175 Color: 0
Size: 208843 Color: 1

Bin 3971: 985 of cap free
Amount of items: 2
Items: 
Size: 517241 Color: 1
Size: 481775 Color: 0

Bin 3972: 985 of cap free
Amount of items: 2
Items: 
Size: 531985 Color: 1
Size: 467031 Color: 0

Bin 3973: 993 of cap free
Amount of items: 2
Items: 
Size: 789183 Color: 1
Size: 209825 Color: 0

Bin 3974: 994 of cap free
Amount of items: 2
Items: 
Size: 641429 Color: 1
Size: 357578 Color: 0

Bin 3975: 996 of cap free
Amount of items: 2
Items: 
Size: 651013 Color: 0
Size: 347992 Color: 1

Bin 3976: 999 of cap free
Amount of items: 2
Items: 
Size: 671518 Color: 1
Size: 327484 Color: 0

Bin 3977: 1008 of cap free
Amount of items: 2
Items: 
Size: 750110 Color: 1
Size: 248883 Color: 0

Bin 3978: 1010 of cap free
Amount of items: 2
Items: 
Size: 499663 Color: 1
Size: 499328 Color: 0

Bin 3979: 1013 of cap free
Amount of items: 2
Items: 
Size: 536059 Color: 0
Size: 462929 Color: 1

Bin 3980: 1013 of cap free
Amount of items: 2
Items: 
Size: 618412 Color: 1
Size: 380576 Color: 0

Bin 3981: 1014 of cap free
Amount of items: 2
Items: 
Size: 571111 Color: 1
Size: 427876 Color: 0

Bin 3982: 1016 of cap free
Amount of items: 2
Items: 
Size: 589928 Color: 0
Size: 409057 Color: 1

Bin 3983: 1017 of cap free
Amount of items: 2
Items: 
Size: 558662 Color: 1
Size: 440322 Color: 0

Bin 3984: 1017 of cap free
Amount of items: 2
Items: 
Size: 659412 Color: 1
Size: 339572 Color: 0

Bin 3985: 1018 of cap free
Amount of items: 2
Items: 
Size: 752176 Color: 1
Size: 246807 Color: 0

Bin 3986: 1021 of cap free
Amount of items: 2
Items: 
Size: 667075 Color: 0
Size: 331905 Color: 1

Bin 3987: 1022 of cap free
Amount of items: 2
Items: 
Size: 736387 Color: 1
Size: 262592 Color: 0

Bin 3988: 1022 of cap free
Amount of items: 2
Items: 
Size: 599887 Color: 1
Size: 399092 Color: 0

Bin 3989: 1029 of cap free
Amount of items: 2
Items: 
Size: 714286 Color: 0
Size: 284686 Color: 1

Bin 3990: 1032 of cap free
Amount of items: 2
Items: 
Size: 608118 Color: 0
Size: 390851 Color: 1

Bin 3991: 1032 of cap free
Amount of items: 2
Items: 
Size: 657926 Color: 0
Size: 341043 Color: 1

Bin 3992: 1034 of cap free
Amount of items: 2
Items: 
Size: 656645 Color: 0
Size: 342322 Color: 1

Bin 3993: 1034 of cap free
Amount of items: 2
Items: 
Size: 601917 Color: 0
Size: 397050 Color: 1

Bin 3994: 1039 of cap free
Amount of items: 2
Items: 
Size: 792863 Color: 1
Size: 206099 Color: 0

Bin 3995: 1040 of cap free
Amount of items: 2
Items: 
Size: 708018 Color: 0
Size: 290943 Color: 1

Bin 3996: 1043 of cap free
Amount of items: 2
Items: 
Size: 556650 Color: 1
Size: 442308 Color: 0

Bin 3997: 1044 of cap free
Amount of items: 2
Items: 
Size: 701295 Color: 1
Size: 297662 Color: 0

Bin 3998: 1046 of cap free
Amount of items: 2
Items: 
Size: 523178 Color: 1
Size: 475777 Color: 0

Bin 3999: 1047 of cap free
Amount of items: 2
Items: 
Size: 501217 Color: 0
Size: 497737 Color: 1

Bin 4000: 1051 of cap free
Amount of items: 2
Items: 
Size: 554828 Color: 0
Size: 444122 Color: 1

Bin 4001: 1057 of cap free
Amount of items: 2
Items: 
Size: 753955 Color: 0
Size: 244989 Color: 1

Bin 4002: 1058 of cap free
Amount of items: 2
Items: 
Size: 556879 Color: 0
Size: 442064 Color: 1

Bin 4003: 1059 of cap free
Amount of items: 2
Items: 
Size: 551467 Color: 1
Size: 447475 Color: 0

Bin 4004: 1062 of cap free
Amount of items: 2
Items: 
Size: 602745 Color: 1
Size: 396194 Color: 0

Bin 4005: 1062 of cap free
Amount of items: 2
Items: 
Size: 743174 Color: 0
Size: 255765 Color: 1

Bin 4006: 1066 of cap free
Amount of items: 2
Items: 
Size: 637553 Color: 1
Size: 361382 Color: 0

Bin 4007: 1069 of cap free
Amount of items: 2
Items: 
Size: 760449 Color: 0
Size: 238483 Color: 1

Bin 4008: 1073 of cap free
Amount of items: 2
Items: 
Size: 762936 Color: 1
Size: 235992 Color: 0

Bin 4009: 1073 of cap free
Amount of items: 2
Items: 
Size: 727791 Color: 1
Size: 271137 Color: 0

Bin 4010: 1077 of cap free
Amount of items: 2
Items: 
Size: 620201 Color: 1
Size: 378723 Color: 0

Bin 4011: 1080 of cap free
Amount of items: 2
Items: 
Size: 516427 Color: 0
Size: 482494 Color: 1

Bin 4012: 1085 of cap free
Amount of items: 2
Items: 
Size: 635740 Color: 1
Size: 363176 Color: 0

Bin 4013: 1086 of cap free
Amount of items: 2
Items: 
Size: 625602 Color: 1
Size: 373313 Color: 0

Bin 4014: 1098 of cap free
Amount of items: 2
Items: 
Size: 799390 Color: 1
Size: 199513 Color: 0

Bin 4015: 1101 of cap free
Amount of items: 2
Items: 
Size: 708128 Color: 1
Size: 290772 Color: 0

Bin 4016: 1105 of cap free
Amount of items: 2
Items: 
Size: 727772 Color: 1
Size: 271124 Color: 0

Bin 4017: 1107 of cap free
Amount of items: 2
Items: 
Size: 705427 Color: 1
Size: 293467 Color: 0

Bin 4018: 1108 of cap free
Amount of items: 2
Items: 
Size: 610656 Color: 0
Size: 388237 Color: 1

Bin 4019: 1108 of cap free
Amount of items: 2
Items: 
Size: 635736 Color: 1
Size: 363157 Color: 0

Bin 4020: 1109 of cap free
Amount of items: 2
Items: 
Size: 709551 Color: 0
Size: 289341 Color: 1

Bin 4021: 1115 of cap free
Amount of items: 2
Items: 
Size: 546272 Color: 1
Size: 452614 Color: 0

Bin 4022: 1115 of cap free
Amount of items: 2
Items: 
Size: 660214 Color: 0
Size: 338672 Color: 1

Bin 4023: 1118 of cap free
Amount of items: 2
Items: 
Size: 677036 Color: 1
Size: 321847 Color: 0

Bin 4024: 1124 of cap free
Amount of items: 2
Items: 
Size: 558641 Color: 1
Size: 440236 Color: 0

Bin 4025: 1128 of cap free
Amount of items: 2
Items: 
Size: 659345 Color: 1
Size: 339528 Color: 0

Bin 4026: 1131 of cap free
Amount of items: 2
Items: 
Size: 599798 Color: 1
Size: 399072 Color: 0

Bin 4027: 1134 of cap free
Amount of items: 2
Items: 
Size: 503022 Color: 0
Size: 495845 Color: 1

Bin 4028: 1134 of cap free
Amount of items: 2
Items: 
Size: 516412 Color: 0
Size: 482455 Color: 1

Bin 4029: 1135 of cap free
Amount of items: 2
Items: 
Size: 764213 Color: 0
Size: 234653 Color: 1

Bin 4030: 1137 of cap free
Amount of items: 2
Items: 
Size: 502120 Color: 1
Size: 496744 Color: 0

Bin 4031: 1137 of cap free
Amount of items: 2
Items: 
Size: 529509 Color: 1
Size: 469355 Color: 0

Bin 4032: 1141 of cap free
Amount of items: 2
Items: 
Size: 771780 Color: 1
Size: 227080 Color: 0

Bin 4033: 1142 of cap free
Amount of items: 2
Items: 
Size: 654221 Color: 0
Size: 344638 Color: 1

Bin 4034: 1143 of cap free
Amount of items: 2
Items: 
Size: 520969 Color: 1
Size: 477889 Color: 0

Bin 4035: 1143 of cap free
Amount of items: 2
Items: 
Size: 647386 Color: 0
Size: 351472 Color: 1

Bin 4036: 1146 of cap free
Amount of items: 2
Items: 
Size: 579676 Color: 0
Size: 419179 Color: 1

Bin 4037: 1158 of cap free
Amount of items: 2
Items: 
Size: 623064 Color: 0
Size: 375779 Color: 1

Bin 4038: 1170 of cap free
Amount of items: 2
Items: 
Size: 512697 Color: 1
Size: 486134 Color: 0

Bin 4039: 1170 of cap free
Amount of items: 2
Items: 
Size: 771761 Color: 1
Size: 227070 Color: 0

Bin 4040: 1171 of cap free
Amount of items: 2
Items: 
Size: 756829 Color: 0
Size: 242001 Color: 1

Bin 4041: 1173 of cap free
Amount of items: 2
Items: 
Size: 736717 Color: 0
Size: 262111 Color: 1

Bin 4042: 1175 of cap free
Amount of items: 2
Items: 
Size: 758222 Color: 1
Size: 240604 Color: 0

Bin 4043: 1178 of cap free
Amount of items: 2
Items: 
Size: 724216 Color: 1
Size: 274607 Color: 0

Bin 4044: 1180 of cap free
Amount of items: 2
Items: 
Size: 710807 Color: 0
Size: 288014 Color: 1

Bin 4045: 1181 of cap free
Amount of items: 2
Items: 
Size: 587809 Color: 0
Size: 411011 Color: 1

Bin 4046: 1186 of cap free
Amount of items: 2
Items: 
Size: 736248 Color: 1
Size: 262567 Color: 0

Bin 4047: 1188 of cap free
Amount of items: 2
Items: 
Size: 728769 Color: 0
Size: 270044 Color: 1

Bin 4048: 1189 of cap free
Amount of items: 2
Items: 
Size: 510743 Color: 1
Size: 488069 Color: 0

Bin 4049: 1189 of cap free
Amount of items: 2
Items: 
Size: 727768 Color: 1
Size: 271044 Color: 0

Bin 4050: 1191 of cap free
Amount of items: 2
Items: 
Size: 766093 Color: 1
Size: 232717 Color: 0

Bin 4051: 1197 of cap free
Amount of items: 2
Items: 
Size: 677358 Color: 0
Size: 321446 Color: 1

Bin 4052: 1200 of cap free
Amount of items: 2
Items: 
Size: 511145 Color: 0
Size: 487656 Color: 1

Bin 4053: 1204 of cap free
Amount of items: 2
Items: 
Size: 682753 Color: 1
Size: 316044 Color: 0

Bin 4054: 1204 of cap free
Amount of items: 2
Items: 
Size: 748725 Color: 1
Size: 250072 Color: 0

Bin 4055: 1208 of cap free
Amount of items: 2
Items: 
Size: 630668 Color: 1
Size: 368125 Color: 0

Bin 4056: 1211 of cap free
Amount of items: 2
Items: 
Size: 520923 Color: 1
Size: 477867 Color: 0

Bin 4057: 1214 of cap free
Amount of items: 2
Items: 
Size: 731177 Color: 0
Size: 267610 Color: 1

Bin 4058: 1215 of cap free
Amount of items: 2
Items: 
Size: 647377 Color: 0
Size: 351409 Color: 1

Bin 4059: 1216 of cap free
Amount of items: 2
Items: 
Size: 553062 Color: 1
Size: 445723 Color: 0

Bin 4060: 1217 of cap free
Amount of items: 2
Items: 
Size: 739704 Color: 1
Size: 259080 Color: 0

Bin 4061: 1227 of cap free
Amount of items: 2
Items: 
Size: 593069 Color: 0
Size: 405705 Color: 1

Bin 4062: 1231 of cap free
Amount of items: 2
Items: 
Size: 568137 Color: 0
Size: 430633 Color: 1

Bin 4063: 1236 of cap free
Amount of items: 2
Items: 
Size: 653185 Color: 1
Size: 345580 Color: 0

Bin 4064: 1238 of cap free
Amount of items: 2
Items: 
Size: 756786 Color: 0
Size: 241977 Color: 1

Bin 4065: 1245 of cap free
Amount of items: 2
Items: 
Size: 583721 Color: 0
Size: 415035 Color: 1

Bin 4066: 1245 of cap free
Amount of items: 2
Items: 
Size: 610550 Color: 0
Size: 388206 Color: 1

Bin 4067: 1247 of cap free
Amount of items: 2
Items: 
Size: 739688 Color: 1
Size: 259066 Color: 0

Bin 4068: 1249 of cap free
Amount of items: 2
Items: 
Size: 680798 Color: 0
Size: 317954 Color: 1

Bin 4069: 1252 of cap free
Amount of items: 2
Items: 
Size: 788965 Color: 1
Size: 209784 Color: 0

Bin 4070: 1252 of cap free
Amount of items: 2
Items: 
Size: 583719 Color: 0
Size: 415030 Color: 1

Bin 4071: 1252 of cap free
Amount of items: 2
Items: 
Size: 595851 Color: 0
Size: 402898 Color: 1

Bin 4072: 1257 of cap free
Amount of items: 2
Items: 
Size: 540755 Color: 1
Size: 457989 Color: 0

Bin 4073: 1263 of cap free
Amount of items: 2
Items: 
Size: 632141 Color: 1
Size: 366597 Color: 0

Bin 4074: 1273 of cap free
Amount of items: 2
Items: 
Size: 534572 Color: 0
Size: 464156 Color: 1

Bin 4075: 1275 of cap free
Amount of items: 2
Items: 
Size: 776508 Color: 1
Size: 222218 Color: 0

Bin 4076: 1281 of cap free
Amount of items: 2
Items: 
Size: 596242 Color: 1
Size: 402478 Color: 0

Bin 4077: 1297 of cap free
Amount of items: 2
Items: 
Size: 532403 Color: 0
Size: 466301 Color: 1

Bin 4078: 1298 of cap free
Amount of items: 2
Items: 
Size: 668321 Color: 0
Size: 330382 Color: 1

Bin 4079: 1300 of cap free
Amount of items: 2
Items: 
Size: 663819 Color: 1
Size: 334882 Color: 0

Bin 4080: 1302 of cap free
Amount of items: 2
Items: 
Size: 682814 Color: 0
Size: 315885 Color: 1

Bin 4081: 1308 of cap free
Amount of items: 2
Items: 
Size: 714085 Color: 0
Size: 284608 Color: 1

Bin 4082: 1313 of cap free
Amount of items: 2
Items: 
Size: 680754 Color: 0
Size: 317934 Color: 1

Bin 4083: 1315 of cap free
Amount of items: 2
Items: 
Size: 558529 Color: 1
Size: 440157 Color: 0

Bin 4084: 1329 of cap free
Amount of items: 2
Items: 
Size: 512563 Color: 1
Size: 486109 Color: 0

Bin 4085: 1333 of cap free
Amount of items: 2
Items: 
Size: 792749 Color: 1
Size: 205919 Color: 0

Bin 4086: 1333 of cap free
Amount of items: 2
Items: 
Size: 640822 Color: 0
Size: 357846 Color: 1

Bin 4087: 1339 of cap free
Amount of items: 2
Items: 
Size: 537323 Color: 1
Size: 461339 Color: 0

Bin 4088: 1341 of cap free
Amount of items: 2
Items: 
Size: 535860 Color: 0
Size: 462800 Color: 1

Bin 4089: 1345 of cap free
Amount of items: 2
Items: 
Size: 692911 Color: 1
Size: 305745 Color: 0

Bin 4090: 1346 of cap free
Amount of items: 2
Items: 
Size: 501915 Color: 1
Size: 496740 Color: 0

Bin 4091: 1350 of cap free
Amount of items: 2
Items: 
Size: 773962 Color: 0
Size: 224689 Color: 1

Bin 4092: 1354 of cap free
Amount of items: 2
Items: 
Size: 596186 Color: 1
Size: 402461 Color: 0

Bin 4093: 1356 of cap free
Amount of items: 2
Items: 
Size: 535319 Color: 1
Size: 463326 Color: 0

Bin 4094: 1360 of cap free
Amount of items: 2
Items: 
Size: 702663 Color: 0
Size: 295978 Color: 1

Bin 4095: 1369 of cap free
Amount of items: 2
Items: 
Size: 721353 Color: 1
Size: 277279 Color: 0

Bin 4096: 1370 of cap free
Amount of items: 2
Items: 
Size: 499369 Color: 1
Size: 499262 Color: 0

Bin 4097: 1377 of cap free
Amount of items: 2
Items: 
Size: 567613 Color: 1
Size: 431011 Color: 0

Bin 4098: 1385 of cap free
Amount of items: 2
Items: 
Size: 532381 Color: 0
Size: 466235 Color: 1

Bin 4099: 1386 of cap free
Amount of items: 2
Items: 
Size: 551330 Color: 1
Size: 447285 Color: 0

Bin 4100: 1390 of cap free
Amount of items: 2
Items: 
Size: 643021 Color: 0
Size: 355590 Color: 1

Bin 4101: 1394 of cap free
Amount of items: 2
Items: 
Size: 704266 Color: 0
Size: 294341 Color: 1

Bin 4102: 1398 of cap free
Amount of items: 2
Items: 
Size: 529393 Color: 1
Size: 469210 Color: 0

Bin 4103: 1402 of cap free
Amount of items: 2
Items: 
Size: 680683 Color: 0
Size: 317916 Color: 1

Bin 4104: 1402 of cap free
Amount of items: 2
Items: 
Size: 699473 Color: 1
Size: 299126 Color: 0

Bin 4105: 1408 of cap free
Amount of items: 2
Items: 
Size: 771756 Color: 1
Size: 226837 Color: 0

Bin 4106: 1409 of cap free
Amount of items: 2
Items: 
Size: 739642 Color: 1
Size: 258950 Color: 0

Bin 4107: 1423 of cap free
Amount of items: 2
Items: 
Size: 579666 Color: 0
Size: 418912 Color: 1

Bin 4108: 1424 of cap free
Amount of items: 2
Items: 
Size: 565177 Color: 0
Size: 433400 Color: 1

Bin 4109: 1432 of cap free
Amount of items: 2
Items: 
Size: 757996 Color: 1
Size: 240573 Color: 0

Bin 4110: 1436 of cap free
Amount of items: 2
Items: 
Size: 577964 Color: 1
Size: 420601 Color: 0

Bin 4111: 1441 of cap free
Amount of items: 2
Items: 
Size: 575810 Color: 0
Size: 422750 Color: 1

Bin 4112: 1441 of cap free
Amount of items: 2
Items: 
Size: 637523 Color: 1
Size: 361037 Color: 0

Bin 4113: 1442 of cap free
Amount of items: 2
Items: 
Size: 605608 Color: 0
Size: 392951 Color: 1

Bin 4114: 1445 of cap free
Amount of items: 2
Items: 
Size: 529361 Color: 1
Size: 469195 Color: 0

Bin 4115: 1446 of cap free
Amount of items: 2
Items: 
Size: 625575 Color: 1
Size: 372980 Color: 0

Bin 4116: 1451 of cap free
Amount of items: 2
Items: 
Size: 739637 Color: 1
Size: 258913 Color: 0

Bin 4117: 1452 of cap free
Amount of items: 2
Items: 
Size: 649185 Color: 0
Size: 349364 Color: 1

Bin 4118: 1453 of cap free
Amount of items: 2
Items: 
Size: 601906 Color: 0
Size: 396642 Color: 1

Bin 4119: 1455 of cap free
Amount of items: 2
Items: 
Size: 560115 Color: 0
Size: 438431 Color: 1

Bin 4120: 1456 of cap free
Amount of items: 2
Items: 
Size: 765852 Color: 1
Size: 232693 Color: 0

Bin 4121: 1457 of cap free
Amount of items: 2
Items: 
Size: 744451 Color: 1
Size: 254093 Color: 0

Bin 4122: 1458 of cap free
Amount of items: 2
Items: 
Size: 766781 Color: 0
Size: 231762 Color: 1

Bin 4123: 1458 of cap free
Amount of items: 2
Items: 
Size: 714058 Color: 0
Size: 284485 Color: 1

Bin 4124: 1467 of cap free
Amount of items: 2
Items: 
Size: 505617 Color: 0
Size: 492917 Color: 1

Bin 4125: 1472 of cap free
Amount of items: 2
Items: 
Size: 588591 Color: 1
Size: 409938 Color: 0

Bin 4126: 1473 of cap free
Amount of items: 2
Items: 
Size: 713206 Color: 1
Size: 285322 Color: 0

Bin 4127: 1486 of cap free
Amount of items: 2
Items: 
Size: 659008 Color: 1
Size: 339507 Color: 0

Bin 4128: 1488 of cap free
Amount of items: 2
Items: 
Size: 760073 Color: 0
Size: 238440 Color: 1

Bin 4129: 1495 of cap free
Amount of items: 2
Items: 
Size: 637500 Color: 1
Size: 361006 Color: 0

Bin 4130: 1496 of cap free
Amount of items: 2
Items: 
Size: 702575 Color: 0
Size: 295930 Color: 1

Bin 4131: 1498 of cap free
Amount of items: 2
Items: 
Size: 612975 Color: 0
Size: 385528 Color: 1

Bin 4132: 1501 of cap free
Amount of items: 2
Items: 
Size: 799281 Color: 1
Size: 199219 Color: 0

Bin 4133: 1504 of cap free
Amount of items: 2
Items: 
Size: 589866 Color: 0
Size: 408631 Color: 1

Bin 4134: 1508 of cap free
Amount of items: 2
Items: 
Size: 512523 Color: 1
Size: 485970 Color: 0

Bin 4135: 1517 of cap free
Amount of items: 2
Items: 
Size: 649122 Color: 0
Size: 349362 Color: 1

Bin 4136: 1517 of cap free
Amount of items: 2
Items: 
Size: 596153 Color: 1
Size: 402331 Color: 0

Bin 4137: 1520 of cap free
Amount of items: 2
Items: 
Size: 621547 Color: 1
Size: 376934 Color: 0

Bin 4138: 1523 of cap free
Amount of items: 2
Items: 
Size: 653169 Color: 1
Size: 345309 Color: 0

Bin 4139: 1525 of cap free
Amount of items: 2
Items: 
Size: 736206 Color: 1
Size: 262270 Color: 0

Bin 4140: 1532 of cap free
Amount of items: 2
Items: 
Size: 583560 Color: 0
Size: 414909 Color: 1

Bin 4141: 1535 of cap free
Amount of items: 2
Items: 
Size: 797483 Color: 0
Size: 200983 Color: 1

Bin 4142: 1546 of cap free
Amount of items: 2
Items: 
Size: 540475 Color: 1
Size: 457980 Color: 0

Bin 4143: 1552 of cap free
Amount of items: 2
Items: 
Size: 743104 Color: 0
Size: 255345 Color: 1

Bin 4144: 1553 of cap free
Amount of items: 2
Items: 
Size: 518289 Color: 0
Size: 480159 Color: 1

Bin 4145: 1555 of cap free
Amount of items: 2
Items: 
Size: 645575 Color: 1
Size: 352871 Color: 0

Bin 4146: 1557 of cap free
Amount of items: 2
Items: 
Size: 741320 Color: 0
Size: 257124 Color: 1

Bin 4147: 1561 of cap free
Amount of items: 2
Items: 
Size: 586167 Color: 0
Size: 412273 Color: 1

Bin 4148: 1568 of cap free
Amount of items: 2
Items: 
Size: 783107 Color: 0
Size: 215326 Color: 1

Bin 4149: 1568 of cap free
Amount of items: 2
Items: 
Size: 570831 Color: 1
Size: 427602 Color: 0

Bin 4150: 1574 of cap free
Amount of items: 2
Items: 
Size: 637465 Color: 1
Size: 360962 Color: 0

Bin 4151: 1576 of cap free
Amount of items: 2
Items: 
Size: 589853 Color: 0
Size: 408572 Color: 1

Bin 4152: 1589 of cap free
Amount of items: 2
Items: 
Size: 731017 Color: 0
Size: 267395 Color: 1

Bin 4153: 1594 of cap free
Amount of items: 2
Items: 
Size: 746395 Color: 0
Size: 252012 Color: 1

Bin 4154: 1604 of cap free
Amount of items: 2
Items: 
Size: 742173 Color: 1
Size: 256224 Color: 0

Bin 4155: 1609 of cap free
Amount of items: 2
Items: 
Size: 724183 Color: 1
Size: 274209 Color: 0

Bin 4156: 1615 of cap free
Amount of items: 2
Items: 
Size: 741317 Color: 0
Size: 257069 Color: 1

Bin 4157: 1616 of cap free
Amount of items: 2
Items: 
Size: 746383 Color: 0
Size: 252002 Color: 1

Bin 4158: 1639 of cap free
Amount of items: 2
Items: 
Size: 595785 Color: 0
Size: 402577 Color: 1

Bin 4159: 1640 of cap free
Amount of items: 2
Items: 
Size: 523723 Color: 0
Size: 474638 Color: 1

Bin 4160: 1642 of cap free
Amount of items: 2
Items: 
Size: 518214 Color: 0
Size: 480145 Color: 1

Bin 4161: 1642 of cap free
Amount of items: 2
Items: 
Size: 546195 Color: 1
Size: 452164 Color: 0

Bin 4162: 1648 of cap free
Amount of items: 2
Items: 
Size: 572173 Color: 0
Size: 426180 Color: 1

Bin 4163: 1655 of cap free
Amount of items: 2
Items: 
Size: 680514 Color: 0
Size: 317832 Color: 1

Bin 4164: 1657 of cap free
Amount of items: 2
Items: 
Size: 757944 Color: 1
Size: 240400 Color: 0

Bin 4165: 1659 of cap free
Amount of items: 2
Items: 
Size: 671068 Color: 1
Size: 327274 Color: 0

Bin 4166: 1664 of cap free
Amount of items: 2
Items: 
Size: 637430 Color: 1
Size: 360907 Color: 0

Bin 4167: 1665 of cap free
Amount of items: 2
Items: 
Size: 525534 Color: 0
Size: 472802 Color: 1

Bin 4168: 1665 of cap free
Amount of items: 2
Items: 
Size: 538178 Color: 0
Size: 460158 Color: 1

Bin 4169: 1666 of cap free
Amount of items: 2
Items: 
Size: 588447 Color: 1
Size: 409888 Color: 0

Bin 4170: 1667 of cap free
Amount of items: 2
Items: 
Size: 623047 Color: 0
Size: 375287 Color: 1

Bin 4171: 1670 of cap free
Amount of items: 2
Items: 
Size: 788750 Color: 1
Size: 209581 Color: 0

Bin 4172: 1682 of cap free
Amount of items: 2
Items: 
Size: 501624 Color: 1
Size: 496695 Color: 0

Bin 4173: 1682 of cap free
Amount of items: 2
Items: 
Size: 599597 Color: 1
Size: 398722 Color: 0

Bin 4174: 1690 of cap free
Amount of items: 3
Items: 
Size: 691365 Color: 0
Size: 177753 Color: 0
Size: 129193 Color: 1

Bin 4175: 1700 of cap free
Amount of items: 2
Items: 
Size: 523688 Color: 0
Size: 474613 Color: 1

Bin 4176: 1710 of cap free
Amount of items: 2
Items: 
Size: 501603 Color: 1
Size: 496688 Color: 0

Bin 4177: 1711 of cap free
Amount of items: 2
Items: 
Size: 554662 Color: 0
Size: 443628 Color: 1

Bin 4178: 1716 of cap free
Amount of items: 2
Items: 
Size: 499274 Color: 1
Size: 499011 Color: 0

Bin 4179: 1717 of cap free
Amount of items: 2
Items: 
Size: 663803 Color: 1
Size: 334481 Color: 0

Bin 4180: 1728 of cap free
Amount of items: 2
Items: 
Size: 673430 Color: 0
Size: 324843 Color: 1

Bin 4181: 1729 of cap free
Amount of items: 2
Items: 
Size: 679416 Color: 1
Size: 318856 Color: 0

Bin 4182: 1736 of cap free
Amount of items: 2
Items: 
Size: 625574 Color: 1
Size: 372691 Color: 0

Bin 4183: 1749 of cap free
Amount of items: 2
Items: 
Size: 615178 Color: 1
Size: 383074 Color: 0

Bin 4184: 1754 of cap free
Amount of items: 2
Items: 
Size: 766488 Color: 0
Size: 231759 Color: 1

Bin 4185: 1760 of cap free
Amount of items: 2
Items: 
Size: 739598 Color: 1
Size: 258643 Color: 0

Bin 4186: 1761 of cap free
Amount of items: 2
Items: 
Size: 744213 Color: 1
Size: 254027 Color: 0

Bin 4187: 1768 of cap free
Amount of items: 2
Items: 
Size: 538087 Color: 0
Size: 460146 Color: 1

Bin 4188: 1771 of cap free
Amount of items: 3
Items: 
Size: 776235 Color: 1
Size: 113447 Color: 0
Size: 108548 Color: 1

Bin 4189: 1773 of cap free
Amount of items: 2
Items: 
Size: 599568 Color: 1
Size: 398660 Color: 0

Bin 4190: 1784 of cap free
Amount of items: 2
Items: 
Size: 579348 Color: 0
Size: 418869 Color: 1

Bin 4191: 1785 of cap free
Amount of items: 2
Items: 
Size: 757822 Color: 1
Size: 240394 Color: 0

Bin 4192: 1793 of cap free
Amount of items: 2
Items: 
Size: 525430 Color: 0
Size: 472778 Color: 1

Bin 4193: 1804 of cap free
Amount of items: 2
Items: 
Size: 602640 Color: 1
Size: 395557 Color: 0

Bin 4194: 1819 of cap free
Amount of items: 2
Items: 
Size: 556838 Color: 0
Size: 441344 Color: 1

Bin 4195: 1826 of cap free
Amount of items: 2
Items: 
Size: 499223 Color: 1
Size: 498952 Color: 0

Bin 4196: 1827 of cap free
Amount of items: 2
Items: 
Size: 792273 Color: 1
Size: 205901 Color: 0

Bin 4197: 1834 of cap free
Amount of items: 2
Items: 
Size: 709989 Color: 1
Size: 288178 Color: 0

Bin 4198: 1839 of cap free
Amount of items: 2
Items: 
Size: 691298 Color: 0
Size: 306864 Color: 1

Bin 4199: 1845 of cap free
Amount of items: 2
Items: 
Size: 512196 Color: 1
Size: 485960 Color: 0

Bin 4200: 1847 of cap free
Amount of items: 2
Items: 
Size: 721334 Color: 1
Size: 276820 Color: 0

Bin 4201: 1848 of cap free
Amount of items: 2
Items: 
Size: 595678 Color: 0
Size: 402475 Color: 1

Bin 4202: 1851 of cap free
Amount of items: 2
Items: 
Size: 570751 Color: 1
Size: 427399 Color: 0

Bin 4203: 1856 of cap free
Amount of items: 2
Items: 
Size: 531997 Color: 0
Size: 466148 Color: 1

Bin 4204: 1858 of cap free
Amount of items: 2
Items: 
Size: 788631 Color: 1
Size: 209512 Color: 0

Bin 4205: 1859 of cap free
Amount of items: 2
Items: 
Size: 736529 Color: 0
Size: 261613 Color: 1

Bin 4206: 1862 of cap free
Amount of items: 2
Items: 
Size: 520700 Color: 1
Size: 477439 Color: 0

Bin 4207: 1869 of cap free
Amount of items: 2
Items: 
Size: 637342 Color: 1
Size: 360790 Color: 0

Bin 4208: 1873 of cap free
Amount of items: 2
Items: 
Size: 625759 Color: 0
Size: 372369 Color: 1

Bin 4209: 1874 of cap free
Amount of items: 2
Items: 
Size: 570735 Color: 1
Size: 427392 Color: 0

Bin 4210: 1877 of cap free
Amount of items: 2
Items: 
Size: 658628 Color: 1
Size: 339496 Color: 0

Bin 4211: 1882 of cap free
Amount of items: 2
Items: 
Size: 554601 Color: 0
Size: 443518 Color: 1

Bin 4212: 1885 of cap free
Amount of items: 2
Items: 
Size: 637334 Color: 1
Size: 360782 Color: 0

Bin 4213: 1901 of cap free
Amount of items: 2
Items: 
Size: 792221 Color: 1
Size: 205879 Color: 0

Bin 4214: 1908 of cap free
Amount of items: 2
Items: 
Size: 552223 Color: 0
Size: 445870 Color: 1

Bin 4215: 1910 of cap free
Amount of items: 2
Items: 
Size: 595657 Color: 0
Size: 402434 Color: 1

Bin 4216: 1913 of cap free
Amount of items: 2
Items: 
Size: 686871 Color: 1
Size: 311217 Color: 0

Bin 4217: 1924 of cap free
Amount of items: 2
Items: 
Size: 645509 Color: 1
Size: 352568 Color: 0

Bin 4218: 1932 of cap free
Amount of items: 2
Items: 
Size: 798946 Color: 1
Size: 199123 Color: 0

Bin 4219: 1937 of cap free
Amount of items: 2
Items: 
Size: 561590 Color: 0
Size: 436474 Color: 1

Bin 4220: 1958 of cap free
Amount of items: 2
Items: 
Size: 563968 Color: 1
Size: 434075 Color: 0

Bin 4221: 1963 of cap free
Amount of items: 2
Items: 
Size: 763655 Color: 0
Size: 234383 Color: 1

Bin 4222: 1976 of cap free
Amount of items: 2
Items: 
Size: 756702 Color: 0
Size: 241323 Color: 1

Bin 4223: 1978 of cap free
Amount of items: 2
Items: 
Size: 504435 Color: 1
Size: 493588 Color: 0

Bin 4224: 1983 of cap free
Amount of items: 2
Items: 
Size: 579256 Color: 0
Size: 418762 Color: 1

Bin 4225: 1986 of cap free
Amount of items: 2
Items: 
Size: 499081 Color: 1
Size: 498934 Color: 0

Bin 4226: 1995 of cap free
Amount of items: 2
Items: 
Size: 788572 Color: 1
Size: 209434 Color: 0

Bin 4227: 1997 of cap free
Amount of items: 2
Items: 
Size: 593950 Color: 1
Size: 404054 Color: 0

Bin 4228: 2000 of cap free
Amount of items: 2
Items: 
Size: 546059 Color: 1
Size: 451942 Color: 0

Bin 4229: 2019 of cap free
Amount of items: 2
Items: 
Size: 544288 Color: 0
Size: 453694 Color: 1

Bin 4230: 2020 of cap free
Amount of items: 2
Items: 
Size: 762577 Color: 1
Size: 235404 Color: 0

Bin 4231: 2023 of cap free
Amount of items: 2
Items: 
Size: 517108 Color: 1
Size: 480870 Color: 0

Bin 4232: 2030 of cap free
Amount of items: 2
Items: 
Size: 673393 Color: 0
Size: 324578 Color: 1

Bin 4233: 2044 of cap free
Amount of items: 2
Items: 
Size: 528881 Color: 1
Size: 469076 Color: 0

Bin 4234: 2047 of cap free
Amount of items: 2
Items: 
Size: 668238 Color: 1
Size: 329716 Color: 0

Bin 4235: 2049 of cap free
Amount of items: 2
Items: 
Size: 505606 Color: 0
Size: 492346 Color: 1

Bin 4236: 2052 of cap free
Amount of items: 2
Items: 
Size: 658602 Color: 1
Size: 339347 Color: 0

Bin 4237: 2054 of cap free
Amount of items: 2
Items: 
Size: 625504 Color: 1
Size: 372443 Color: 0

Bin 4238: 2055 of cap free
Amount of items: 2
Items: 
Size: 589526 Color: 0
Size: 408420 Color: 1

Bin 4239: 2062 of cap free
Amount of items: 2
Items: 
Size: 583110 Color: 0
Size: 414829 Color: 1

Bin 4240: 2069 of cap free
Amount of items: 2
Items: 
Size: 625504 Color: 1
Size: 372428 Color: 0

Bin 4241: 2081 of cap free
Amount of items: 2
Items: 
Size: 724776 Color: 0
Size: 273144 Color: 1

Bin 4242: 2095 of cap free
Amount of items: 2
Items: 
Size: 779038 Color: 0
Size: 218868 Color: 1

Bin 4243: 2098 of cap free
Amount of items: 2
Items: 
Size: 547617 Color: 0
Size: 450286 Color: 1

Bin 4244: 2109 of cap free
Amount of items: 2
Items: 
Size: 528829 Color: 1
Size: 469063 Color: 0

Bin 4245: 2112 of cap free
Amount of items: 2
Items: 
Size: 658546 Color: 1
Size: 339343 Color: 0

Bin 4246: 2115 of cap free
Amount of items: 2
Items: 
Size: 520516 Color: 1
Size: 477370 Color: 0

Bin 4247: 2121 of cap free
Amount of items: 2
Items: 
Size: 640557 Color: 0
Size: 357323 Color: 1

Bin 4248: 2126 of cap free
Amount of items: 2
Items: 
Size: 673467 Color: 1
Size: 324408 Color: 0

Bin 4249: 2132 of cap free
Amount of items: 2
Items: 
Size: 542648 Color: 1
Size: 455221 Color: 0

Bin 4250: 2135 of cap free
Amount of items: 2
Items: 
Size: 520513 Color: 1
Size: 477353 Color: 0

Bin 4251: 2136 of cap free
Amount of items: 2
Items: 
Size: 502417 Color: 0
Size: 495448 Color: 1

Bin 4252: 2138 of cap free
Amount of items: 2
Items: 
Size: 743972 Color: 1
Size: 253891 Color: 0

Bin 4253: 2140 of cap free
Amount of items: 2
Items: 
Size: 518087 Color: 0
Size: 479774 Color: 1

Bin 4254: 2145 of cap free
Amount of items: 2
Items: 
Size: 798925 Color: 1
Size: 198931 Color: 0

Bin 4255: 2160 of cap free
Amount of items: 2
Items: 
Size: 792164 Color: 1
Size: 205677 Color: 0

Bin 4256: 2189 of cap free
Amount of items: 2
Items: 
Size: 746253 Color: 0
Size: 251559 Color: 1

Bin 4257: 2210 of cap free
Amount of items: 2
Items: 
Size: 616254 Color: 0
Size: 381537 Color: 1

Bin 4258: 2211 of cap free
Amount of items: 2
Items: 
Size: 561578 Color: 0
Size: 436212 Color: 1

Bin 4259: 2214 of cap free
Amount of items: 2
Items: 
Size: 612342 Color: 0
Size: 385445 Color: 1

Bin 4260: 2214 of cap free
Amount of items: 2
Items: 
Size: 637024 Color: 1
Size: 360763 Color: 0

Bin 4261: 2227 of cap free
Amount of items: 2
Items: 
Size: 663799 Color: 1
Size: 333975 Color: 0

Bin 4262: 2253 of cap free
Amount of items: 2
Items: 
Size: 673356 Color: 0
Size: 324392 Color: 1

Bin 4263: 2260 of cap free
Amount of items: 2
Items: 
Size: 784918 Color: 1
Size: 212823 Color: 0

Bin 4264: 2260 of cap free
Amount of items: 2
Items: 
Size: 643011 Color: 0
Size: 354730 Color: 1

Bin 4265: 2261 of cap free
Amount of items: 2
Items: 
Size: 570645 Color: 1
Size: 427095 Color: 0

Bin 4266: 2270 of cap free
Amount of items: 2
Items: 
Size: 570641 Color: 1
Size: 427090 Color: 0

Bin 4267: 2273 of cap free
Amount of items: 2
Items: 
Size: 673369 Color: 1
Size: 324359 Color: 0

Bin 4268: 2278 of cap free
Amount of items: 2
Items: 
Size: 730616 Color: 0
Size: 267107 Color: 1

Bin 4269: 2318 of cap free
Amount of items: 2
Items: 
Size: 546045 Color: 1
Size: 451638 Color: 0

Bin 4270: 2324 of cap free
Amount of items: 2
Items: 
Size: 558136 Color: 1
Size: 439541 Color: 0

Bin 4271: 2348 of cap free
Amount of items: 2
Items: 
Size: 517933 Color: 0
Size: 479720 Color: 1

Bin 4272: 2352 of cap free
Amount of items: 2
Items: 
Size: 525873 Color: 1
Size: 471776 Color: 0

Bin 4273: 2364 of cap free
Amount of items: 2
Items: 
Size: 563616 Color: 1
Size: 434021 Color: 0

Bin 4274: 2383 of cap free
Amount of items: 2
Items: 
Size: 757247 Color: 1
Size: 240371 Color: 0

Bin 4275: 2386 of cap free
Amount of items: 2
Items: 
Size: 667997 Color: 1
Size: 329618 Color: 0

Bin 4276: 2389 of cap free
Amount of items: 2
Items: 
Size: 504030 Color: 1
Size: 493582 Color: 0

Bin 4277: 2395 of cap free
Amount of items: 2
Items: 
Size: 593558 Color: 1
Size: 404048 Color: 0

Bin 4278: 2404 of cap free
Amount of items: 2
Items: 
Size: 571579 Color: 0
Size: 426018 Color: 1

Bin 4279: 2405 of cap free
Amount of items: 2
Items: 
Size: 525861 Color: 1
Size: 471735 Color: 0

Bin 4280: 2417 of cap free
Amount of items: 2
Items: 
Size: 587753 Color: 1
Size: 409831 Color: 0

Bin 4281: 2419 of cap free
Amount of items: 2
Items: 
Size: 642881 Color: 0
Size: 354701 Color: 1

Bin 4282: 2419 of cap free
Amount of items: 2
Items: 
Size: 673203 Color: 0
Size: 324379 Color: 1

Bin 4283: 2439 of cap free
Amount of items: 2
Items: 
Size: 731250 Color: 1
Size: 266312 Color: 0

Bin 4284: 2443 of cap free
Amount of items: 2
Items: 
Size: 679791 Color: 0
Size: 317767 Color: 1

Bin 4285: 2461 of cap free
Amount of items: 2
Items: 
Size: 558063 Color: 1
Size: 439477 Color: 0

Bin 4286: 2474 of cap free
Amount of items: 2
Items: 
Size: 502409 Color: 0
Size: 495118 Color: 1

Bin 4287: 2492 of cap free
Amount of items: 2
Items: 
Size: 762488 Color: 1
Size: 235021 Color: 0

Bin 4288: 2498 of cap free
Amount of items: 2
Items: 
Size: 582988 Color: 0
Size: 414515 Color: 1

Bin 4289: 2506 of cap free
Amount of items: 2
Items: 
Size: 637366 Color: 0
Size: 360129 Color: 1

Bin 4290: 2528 of cap free
Amount of items: 2
Items: 
Size: 578747 Color: 0
Size: 418726 Color: 1

Bin 4291: 2532 of cap free
Amount of items: 2
Items: 
Size: 673141 Color: 1
Size: 324328 Color: 0

Bin 4292: 2534 of cap free
Amount of items: 2
Items: 
Size: 542604 Color: 1
Size: 454863 Color: 0

Bin 4293: 2555 of cap free
Amount of items: 2
Items: 
Size: 505144 Color: 0
Size: 492302 Color: 1

Bin 4294: 2558 of cap free
Amount of items: 2
Items: 
Size: 763538 Color: 0
Size: 233905 Color: 1

Bin 4295: 2565 of cap free
Amount of items: 2
Items: 
Size: 570418 Color: 1
Size: 427018 Color: 0

Bin 4296: 2566 of cap free
Amount of items: 2
Items: 
Size: 512140 Color: 1
Size: 485295 Color: 0

Bin 4297: 2570 of cap free
Amount of items: 2
Items: 
Size: 582954 Color: 0
Size: 414477 Color: 1

Bin 4298: 2571 of cap free
Amount of items: 2
Items: 
Size: 776015 Color: 1
Size: 221415 Color: 0

Bin 4299: 2586 of cap free
Amount of items: 2
Items: 
Size: 798639 Color: 1
Size: 198776 Color: 0

Bin 4300: 2588 of cap free
Amount of items: 2
Items: 
Size: 614378 Color: 1
Size: 383035 Color: 0

Bin 4301: 2597 of cap free
Amount of items: 2
Items: 
Size: 673124 Color: 1
Size: 324280 Color: 0

Bin 4302: 2603 of cap free
Amount of items: 2
Items: 
Size: 709408 Color: 0
Size: 287990 Color: 1

Bin 4303: 2606 of cap free
Amount of items: 2
Items: 
Size: 632611 Color: 0
Size: 364784 Color: 1

Bin 4304: 2612 of cap free
Amount of items: 2
Items: 
Size: 784832 Color: 1
Size: 212557 Color: 0

Bin 4305: 2617 of cap free
Amount of items: 2
Items: 
Size: 502286 Color: 0
Size: 495098 Color: 1

Bin 4306: 2643 of cap free
Amount of items: 2
Items: 
Size: 576805 Color: 1
Size: 420553 Color: 0

Bin 4307: 2650 of cap free
Amount of items: 2
Items: 
Size: 525861 Color: 1
Size: 471490 Color: 0

Bin 4308: 2652 of cap free
Amount of items: 2
Items: 
Size: 557975 Color: 1
Size: 439374 Color: 0

Bin 4309: 2668 of cap free
Amount of items: 2
Items: 
Size: 614300 Color: 1
Size: 383033 Color: 0

Bin 4310: 2682 of cap free
Amount of items: 2
Items: 
Size: 563469 Color: 1
Size: 433850 Color: 0

Bin 4311: 2702 of cap free
Amount of items: 2
Items: 
Size: 498850 Color: 1
Size: 498449 Color: 0

Bin 4312: 2723 of cap free
Amount of items: 2
Items: 
Size: 576780 Color: 1
Size: 420498 Color: 0

Bin 4313: 2723 of cap free
Amount of items: 2
Items: 
Size: 587696 Color: 1
Size: 409582 Color: 0

Bin 4314: 2733 of cap free
Amount of items: 2
Items: 
Size: 768919 Color: 0
Size: 228349 Color: 1

Bin 4315: 2736 of cap free
Amount of items: 2
Items: 
Size: 690680 Color: 0
Size: 306585 Color: 1

Bin 4316: 2743 of cap free
Amount of items: 2
Items: 
Size: 797474 Color: 0
Size: 199784 Color: 1

Bin 4317: 2756 of cap free
Amount of items: 2
Items: 
Size: 757032 Color: 1
Size: 240213 Color: 0

Bin 4318: 2761 of cap free
Amount of items: 2
Items: 
Size: 763410 Color: 0
Size: 233830 Color: 1

Bin 4319: 2763 of cap free
Amount of items: 2
Items: 
Size: 717992 Color: 1
Size: 279246 Color: 0

Bin 4320: 2770 of cap free
Amount of items: 2
Items: 
Size: 637319 Color: 0
Size: 359912 Color: 1

Bin 4321: 2772 of cap free
Amount of items: 2
Items: 
Size: 672904 Color: 0
Size: 324325 Color: 1

Bin 4322: 2787 of cap free
Amount of items: 2
Items: 
Size: 792000 Color: 1
Size: 205214 Color: 0

Bin 4323: 2796 of cap free
Amount of items: 2
Items: 
Size: 601978 Color: 1
Size: 395227 Color: 0

Bin 4324: 2804 of cap free
Amount of items: 2
Items: 
Size: 748431 Color: 1
Size: 248766 Color: 0

Bin 4325: 2816 of cap free
Amount of items: 2
Items: 
Size: 545657 Color: 1
Size: 451528 Color: 0

Bin 4326: 2845 of cap free
Amount of items: 2
Items: 
Size: 576759 Color: 1
Size: 420397 Color: 0

Bin 4327: 2859 of cap free
Amount of items: 2
Items: 
Size: 607189 Color: 0
Size: 389953 Color: 1

Bin 4328: 2886 of cap free
Amount of items: 2
Items: 
Size: 724032 Color: 0
Size: 273083 Color: 1

Bin 4329: 2893 of cap free
Amount of items: 2
Items: 
Size: 570107 Color: 1
Size: 427001 Color: 0

Bin 4330: 2905 of cap free
Amount of items: 2
Items: 
Size: 679277 Color: 1
Size: 317819 Color: 0

Bin 4331: 2910 of cap free
Amount of items: 2
Items: 
Size: 615637 Color: 0
Size: 381454 Color: 1

Bin 4332: 2915 of cap free
Amount of items: 2
Items: 
Size: 539118 Color: 1
Size: 457968 Color: 0

Bin 4333: 2918 of cap free
Amount of items: 2
Items: 
Size: 727546 Color: 1
Size: 269537 Color: 0

Bin 4334: 2934 of cap free
Amount of items: 2
Items: 
Size: 570104 Color: 1
Size: 426963 Color: 0

Bin 4335: 2947 of cap free
Amount of items: 2
Items: 
Size: 784507 Color: 1
Size: 212547 Color: 0

Bin 4336: 2967 of cap free
Amount of items: 2
Items: 
Size: 709198 Color: 0
Size: 287836 Color: 1

Bin 4337: 2979 of cap free
Amount of items: 2
Items: 
Size: 565138 Color: 0
Size: 431884 Color: 1

Bin 4338: 3005 of cap free
Amount of items: 2
Items: 
Size: 709400 Color: 1
Size: 287596 Color: 0

Bin 4339: 3040 of cap free
Amount of items: 2
Items: 
Size: 644936 Color: 1
Size: 352025 Color: 0

Bin 4340: 3048 of cap free
Amount of items: 2
Items: 
Size: 525531 Color: 1
Size: 471422 Color: 0

Bin 4341: 3092 of cap free
Amount of items: 2
Items: 
Size: 632200 Color: 0
Size: 364709 Color: 1

Bin 4342: 3110 of cap free
Amount of items: 2
Items: 
Size: 544201 Color: 0
Size: 452690 Color: 1

Bin 4343: 3117 of cap free
Amount of items: 2
Items: 
Size: 781763 Color: 0
Size: 215121 Color: 1

Bin 4344: 3131 of cap free
Amount of items: 2
Items: 
Size: 652802 Color: 1
Size: 344068 Color: 0

Bin 4345: 3131 of cap free
Amount of items: 2
Items: 
Size: 512099 Color: 1
Size: 484771 Color: 0

Bin 4346: 3132 of cap free
Amount of items: 2
Items: 
Size: 615600 Color: 0
Size: 381269 Color: 1

Bin 4347: 3174 of cap free
Amount of items: 2
Items: 
Size: 632128 Color: 0
Size: 364699 Color: 1

Bin 4348: 3176 of cap free
Amount of items: 2
Items: 
Size: 709126 Color: 0
Size: 287699 Color: 1

Bin 4349: 3177 of cap free
Amount of items: 2
Items: 
Size: 533629 Color: 1
Size: 463195 Color: 0

Bin 4350: 3191 of cap free
Amount of items: 2
Items: 
Size: 533625 Color: 1
Size: 463185 Color: 0

Bin 4351: 3209 of cap free
Amount of items: 2
Items: 
Size: 613910 Color: 1
Size: 382882 Color: 0

Bin 4352: 3221 of cap free
Amount of items: 2
Items: 
Size: 587223 Color: 1
Size: 409557 Color: 0

Bin 4353: 3229 of cap free
Amount of items: 2
Items: 
Size: 547412 Color: 0
Size: 449360 Color: 1

Bin 4354: 3302 of cap free
Amount of items: 2
Items: 
Size: 615495 Color: 0
Size: 381204 Color: 1

Bin 4355: 3315 of cap free
Amount of items: 2
Items: 
Size: 533512 Color: 1
Size: 463174 Color: 0

Bin 4356: 3339 of cap free
Amount of items: 2
Items: 
Size: 798218 Color: 1
Size: 198444 Color: 0

Bin 4357: 3353 of cap free
Amount of items: 2
Items: 
Size: 540590 Color: 0
Size: 456058 Color: 1

Bin 4358: 3399 of cap free
Amount of items: 2
Items: 
Size: 538930 Color: 1
Size: 457672 Color: 0

Bin 4359: 3423 of cap free
Amount of items: 2
Items: 
Size: 631968 Color: 0
Size: 364610 Color: 1

Bin 4360: 3433 of cap free
Amount of items: 2
Items: 
Size: 563175 Color: 1
Size: 433393 Color: 0

Bin 4361: 3446 of cap free
Amount of items: 2
Items: 
Size: 615406 Color: 0
Size: 381149 Color: 1

Bin 4362: 3491 of cap free
Amount of items: 2
Items: 
Size: 569625 Color: 1
Size: 426885 Color: 0

Bin 4363: 3495 of cap free
Amount of items: 2
Items: 
Size: 540575 Color: 0
Size: 455931 Color: 1

Bin 4364: 3613 of cap free
Amount of items: 2
Items: 
Size: 552919 Color: 1
Size: 443469 Color: 0

Bin 4365: 3619 of cap free
Amount of items: 2
Items: 
Size: 592378 Color: 1
Size: 404004 Color: 0

Bin 4366: 3670 of cap free
Amount of items: 2
Items: 
Size: 635603 Color: 1
Size: 360728 Color: 0

Bin 4367: 3719 of cap free
Amount of items: 2
Items: 
Size: 644487 Color: 1
Size: 351795 Color: 0

Bin 4368: 3733 of cap free
Amount of items: 2
Items: 
Size: 571102 Color: 0
Size: 425166 Color: 1

Bin 4369: 3749 of cap free
Amount of items: 2
Items: 
Size: 666679 Color: 1
Size: 329573 Color: 0

Bin 4370: 3751 of cap free
Amount of items: 2
Items: 
Size: 672899 Color: 0
Size: 323351 Color: 1

Bin 4371: 3821 of cap free
Amount of items: 2
Items: 
Size: 512075 Color: 1
Size: 484105 Color: 0

Bin 4372: 3894 of cap free
Amount of items: 2
Items: 
Size: 576553 Color: 1
Size: 419554 Color: 0

Bin 4373: 3898 of cap free
Amount of items: 2
Items: 
Size: 587212 Color: 1
Size: 408891 Color: 0

Bin 4374: 3903 of cap free
Amount of items: 2
Items: 
Size: 614983 Color: 0
Size: 381115 Color: 1

Bin 4375: 3916 of cap free
Amount of items: 2
Items: 
Size: 631486 Color: 0
Size: 364599 Color: 1

Bin 4376: 3917 of cap free
Amount of items: 2
Items: 
Size: 644312 Color: 1
Size: 351772 Color: 0

Bin 4377: 3951 of cap free
Amount of items: 2
Items: 
Size: 511978 Color: 1
Size: 484072 Color: 0

Bin 4378: 3953 of cap free
Amount of items: 2
Items: 
Size: 797837 Color: 1
Size: 198211 Color: 0

Bin 4379: 4034 of cap free
Amount of items: 2
Items: 
Size: 666649 Color: 1
Size: 329318 Color: 0

Bin 4380: 4048 of cap free
Amount of items: 2
Items: 
Size: 672872 Color: 0
Size: 323081 Color: 1

Bin 4381: 4215 of cap free
Amount of items: 2
Items: 
Size: 644021 Color: 1
Size: 351765 Color: 0

Bin 4382: 4242 of cap free
Amount of items: 2
Items: 
Size: 586981 Color: 1
Size: 408778 Color: 0

Bin 4383: 4242 of cap free
Amount of items: 2
Items: 
Size: 565136 Color: 0
Size: 430623 Color: 1

Bin 4384: 4266 of cap free
Amount of items: 2
Items: 
Size: 768929 Color: 1
Size: 226806 Color: 0

Bin 4385: 4278 of cap free
Amount of items: 2
Items: 
Size: 672776 Color: 0
Size: 322947 Color: 1

Bin 4386: 4306 of cap free
Amount of items: 2
Items: 
Size: 672771 Color: 0
Size: 322924 Color: 1

Bin 4387: 4317 of cap free
Amount of items: 2
Items: 
Size: 586916 Color: 1
Size: 408768 Color: 0

Bin 4388: 4332 of cap free
Amount of items: 2
Items: 
Size: 619432 Color: 1
Size: 376237 Color: 0

Bin 4389: 4367 of cap free
Amount of items: 2
Items: 
Size: 523042 Color: 0
Size: 472592 Color: 1

Bin 4390: 4372 of cap free
Amount of items: 2
Items: 
Size: 516350 Color: 0
Size: 479279 Color: 1

Bin 4391: 4420 of cap free
Amount of items: 2
Items: 
Size: 643997 Color: 1
Size: 351584 Color: 0

Bin 4392: 4468 of cap free
Amount of items: 2
Items: 
Size: 498847 Color: 1
Size: 496686 Color: 0

Bin 4393: 4472 of cap free
Amount of items: 2
Items: 
Size: 619429 Color: 1
Size: 376100 Color: 0

Bin 4394: 4485 of cap free
Amount of items: 2
Items: 
Size: 728734 Color: 0
Size: 266782 Color: 1

Bin 4395: 4512 of cap free
Amount of items: 2
Items: 
Size: 569120 Color: 1
Size: 426369 Color: 0

Bin 4396: 4539 of cap free
Amount of items: 2
Items: 
Size: 523015 Color: 0
Size: 472447 Color: 1

Bin 4397: 4545 of cap free
Amount of items: 2
Items: 
Size: 599837 Color: 0
Size: 395619 Color: 1

Bin 4398: 4568 of cap free
Amount of items: 2
Items: 
Size: 559311 Color: 0
Size: 436122 Color: 1

Bin 4399: 4586 of cap free
Amount of items: 2
Items: 
Size: 586685 Color: 1
Size: 408730 Color: 0

Bin 4400: 4600 of cap free
Amount of items: 2
Items: 
Size: 522960 Color: 0
Size: 472441 Color: 1

Bin 4401: 4607 of cap free
Amount of items: 2
Items: 
Size: 569056 Color: 1
Size: 426338 Color: 0

Bin 4402: 4639 of cap free
Amount of items: 2
Items: 
Size: 565024 Color: 0
Size: 430338 Color: 1

Bin 4403: 4651 of cap free
Amount of items: 2
Items: 
Size: 783446 Color: 1
Size: 211904 Color: 0

Bin 4404: 4728 of cap free
Amount of items: 2
Items: 
Size: 768882 Color: 1
Size: 226391 Color: 0

Bin 4405: 4736 of cap free
Amount of items: 2
Items: 
Size: 689370 Color: 0
Size: 305895 Color: 1

Bin 4406: 4773 of cap free
Amount of items: 2
Items: 
Size: 689365 Color: 0
Size: 305863 Color: 1

Bin 4407: 4813 of cap free
Amount of items: 2
Items: 
Size: 522774 Color: 0
Size: 472414 Color: 1

Bin 4408: 4839 of cap free
Amount of items: 2
Items: 
Size: 559194 Color: 0
Size: 435968 Color: 1

Bin 4409: 4924 of cap free
Amount of items: 2
Items: 
Size: 614943 Color: 0
Size: 380134 Color: 1

Bin 4410: 4937 of cap free
Amount of items: 2
Items: 
Size: 741203 Color: 0
Size: 253861 Color: 1

Bin 4411: 4958 of cap free
Amount of items: 2
Items: 
Size: 652626 Color: 1
Size: 342417 Color: 0

Bin 4412: 4972 of cap free
Amount of items: 2
Items: 
Size: 522721 Color: 0
Size: 472308 Color: 1

Bin 4413: 4977 of cap free
Amount of items: 2
Items: 
Size: 614898 Color: 0
Size: 380126 Color: 1

Bin 4414: 5102 of cap free
Amount of items: 2
Items: 
Size: 522705 Color: 0
Size: 472194 Color: 1

Bin 4415: 5167 of cap free
Amount of items: 2
Items: 
Size: 559062 Color: 0
Size: 435772 Color: 1

Bin 4416: 5236 of cap free
Amount of items: 2
Items: 
Size: 522573 Color: 0
Size: 472192 Color: 1

Bin 4417: 5255 of cap free
Amount of items: 2
Items: 
Size: 796715 Color: 0
Size: 198031 Color: 1

Bin 4418: 5320 of cap free
Amount of items: 2
Items: 
Size: 522553 Color: 0
Size: 472128 Color: 1

Bin 4419: 5322 of cap free
Amount of items: 2
Items: 
Size: 576353 Color: 1
Size: 418326 Color: 0

Bin 4420: 5368 of cap free
Amount of items: 2
Items: 
Size: 689124 Color: 0
Size: 305509 Color: 1

Bin 4421: 5371 of cap free
Amount of items: 2
Items: 
Size: 676823 Color: 1
Size: 317807 Color: 0

Bin 4422: 5373 of cap free
Amount of items: 2
Items: 
Size: 652258 Color: 1
Size: 342370 Color: 0

Bin 4423: 5393 of cap free
Amount of items: 2
Items: 
Size: 559047 Color: 0
Size: 435561 Color: 1

Bin 4424: 5428 of cap free
Amount of items: 2
Items: 
Size: 576271 Color: 1
Size: 418302 Color: 0

Bin 4425: 5429 of cap free
Amount of items: 2
Items: 
Size: 652217 Color: 1
Size: 342355 Color: 0

Bin 4426: 5435 of cap free
Amount of items: 2
Items: 
Size: 679193 Color: 0
Size: 315373 Color: 1

Bin 4427: 5440 of cap free
Amount of items: 2
Items: 
Size: 599063 Color: 0
Size: 395498 Color: 1

Bin 4428: 5465 of cap free
Amount of items: 2
Items: 
Size: 515270 Color: 0
Size: 479266 Color: 1

Bin 4429: 5470 of cap free
Amount of items: 2
Items: 
Size: 689120 Color: 0
Size: 305411 Color: 1

Bin 4430: 5481 of cap free
Amount of items: 2
Items: 
Size: 652184 Color: 1
Size: 342336 Color: 0

Bin 4431: 5486 of cap free
Amount of items: 2
Items: 
Size: 522474 Color: 0
Size: 472041 Color: 1

Bin 4432: 5540 of cap free
Amount of items: 2
Items: 
Size: 558940 Color: 0
Size: 435521 Color: 1

Bin 4433: 5555 of cap free
Amount of items: 2
Items: 
Size: 510460 Color: 1
Size: 483986 Color: 0

Bin 4434: 5561 of cap free
Amount of items: 2
Items: 
Size: 640470 Color: 0
Size: 353970 Color: 1

Bin 4435: 5575 of cap free
Amount of items: 2
Items: 
Size: 598955 Color: 0
Size: 395471 Color: 1

Bin 4436: 5630 of cap free
Amount of items: 2
Items: 
Size: 576239 Color: 1
Size: 418132 Color: 0

Bin 4437: 5686 of cap free
Amount of items: 2
Items: 
Size: 640450 Color: 0
Size: 353865 Color: 1

Bin 4438: 5727 of cap free
Amount of items: 2
Items: 
Size: 652179 Color: 1
Size: 342095 Color: 0

Bin 4439: 5736 of cap free
Amount of items: 2
Items: 
Size: 522398 Color: 0
Size: 471867 Color: 1

Bin 4440: 5807 of cap free
Amount of items: 2
Items: 
Size: 515234 Color: 0
Size: 478960 Color: 1

Bin 4441: 5827 of cap free
Amount of items: 2
Items: 
Size: 598937 Color: 0
Size: 395237 Color: 1

Bin 4442: 5835 of cap free
Amount of items: 2
Items: 
Size: 599552 Color: 1
Size: 394614 Color: 0

Bin 4443: 5982 of cap free
Amount of items: 2
Items: 
Size: 515118 Color: 0
Size: 478901 Color: 1

Bin 4444: 6001 of cap free
Amount of items: 2
Items: 
Size: 576057 Color: 1
Size: 417943 Color: 0

Bin 4445: 6071 of cap free
Amount of items: 2
Items: 
Size: 564987 Color: 0
Size: 428943 Color: 1

Bin 4446: 6076 of cap free
Amount of items: 2
Items: 
Size: 515109 Color: 0
Size: 478816 Color: 1

Bin 4447: 6133 of cap free
Amount of items: 2
Items: 
Size: 614611 Color: 0
Size: 379257 Color: 1

Bin 4448: 6144 of cap free
Amount of items: 2
Items: 
Size: 567530 Color: 1
Size: 426327 Color: 0

Bin 4449: 6209 of cap free
Amount of items: 2
Items: 
Size: 514989 Color: 0
Size: 478803 Color: 1

Bin 4450: 6264 of cap free
Amount of items: 2
Items: 
Size: 717765 Color: 1
Size: 275972 Color: 0

Bin 4451: 6342 of cap free
Amount of items: 2
Items: 
Size: 651850 Color: 1
Size: 341809 Color: 0

Bin 4452: 6357 of cap free
Amount of items: 2
Items: 
Size: 514941 Color: 0
Size: 478703 Color: 1

Bin 4453: 6388 of cap free
Amount of items: 2
Items: 
Size: 797334 Color: 1
Size: 196279 Color: 0

Bin 4454: 6572 of cap free
Amount of items: 2
Items: 
Size: 598847 Color: 0
Size: 394582 Color: 1

Bin 4455: 6622 of cap free
Amount of items: 2
Items: 
Size: 501198 Color: 0
Size: 492181 Color: 1

Bin 4456: 6644 of cap free
Amount of items: 2
Items: 
Size: 538808 Color: 1
Size: 454549 Color: 0

Bin 4457: 6711 of cap free
Amount of items: 2
Items: 
Size: 575900 Color: 1
Size: 417390 Color: 0

Bin 4458: 6807 of cap free
Amount of items: 2
Items: 
Size: 591550 Color: 1
Size: 401644 Color: 0

Bin 4459: 6863 of cap free
Amount of items: 2
Items: 
Size: 618409 Color: 1
Size: 374729 Color: 0

Bin 4460: 6905 of cap free
Amount of items: 2
Items: 
Size: 575884 Color: 1
Size: 417212 Color: 0

Bin 4461: 6985 of cap free
Amount of items: 2
Items: 
Size: 591495 Color: 1
Size: 401521 Color: 0

Bin 4462: 7243 of cap free
Amount of items: 2
Items: 
Size: 540071 Color: 0
Size: 452687 Color: 1

Bin 4463: 7434 of cap free
Amount of items: 2
Items: 
Size: 739289 Color: 1
Size: 253278 Color: 0

Bin 4464: 7481 of cap free
Amount of items: 2
Items: 
Size: 591352 Color: 1
Size: 401168 Color: 0

Bin 4465: 7501 of cap free
Amount of items: 2
Items: 
Size: 684918 Color: 1
Size: 307582 Color: 0

Bin 4466: 8198 of cap free
Amount of items: 2
Items: 
Size: 574609 Color: 1
Size: 417194 Color: 0

Bin 4467: 8529 of cap free
Amount of items: 2
Items: 
Size: 796330 Color: 1
Size: 195142 Color: 0

Bin 4468: 8653 of cap free
Amount of items: 2
Items: 
Size: 536829 Color: 1
Size: 454519 Color: 0

Bin 4469: 10190 of cap free
Amount of items: 2
Items: 
Size: 723955 Color: 0
Size: 265856 Color: 1

Bin 4470: 10750 of cap free
Amount of items: 2
Items: 
Size: 793232 Color: 0
Size: 196019 Color: 1

Bin 4471: 10833 of cap free
Amount of items: 2
Items: 
Size: 562848 Color: 1
Size: 426320 Color: 0

Bin 4472: 10838 of cap free
Amount of items: 2
Items: 
Size: 796271 Color: 1
Size: 192892 Color: 0

Bin 4473: 11177 of cap free
Amount of items: 2
Items: 
Size: 651822 Color: 1
Size: 337002 Color: 0

Bin 4474: 11834 of cap free
Amount of items: 2
Items: 
Size: 796222 Color: 1
Size: 191945 Color: 0

Bin 4475: 11836 of cap free
Amount of items: 2
Items: 
Size: 650844 Color: 0
Size: 337321 Color: 1

Bin 4476: 12242 of cap free
Amount of items: 2
Items: 
Size: 792476 Color: 0
Size: 195283 Color: 1

Bin 4477: 12683 of cap free
Amount of items: 2
Items: 
Size: 796088 Color: 1
Size: 191230 Color: 0

Bin 4478: 14198 of cap free
Amount of items: 2
Items: 
Size: 795984 Color: 1
Size: 189819 Color: 0

Bin 4479: 15140 of cap free
Amount of items: 2
Items: 
Size: 648771 Color: 0
Size: 336090 Color: 1

Bin 4480: 15583 of cap free
Amount of items: 2
Items: 
Size: 736443 Color: 0
Size: 247975 Color: 1

Bin 4481: 16087 of cap free
Amount of items: 2
Items: 
Size: 792459 Color: 0
Size: 191455 Color: 1

Bin 4482: 16678 of cap free
Amount of items: 2
Items: 
Size: 614599 Color: 0
Size: 368724 Color: 1

Bin 4483: 16764 of cap free
Amount of items: 2
Items: 
Size: 795182 Color: 1
Size: 188055 Color: 0

Bin 4484: 17651 of cap free
Amount of items: 2
Items: 
Size: 564965 Color: 0
Size: 417385 Color: 1

Bin 4485: 17684 of cap free
Amount of items: 2
Items: 
Size: 791429 Color: 0
Size: 190888 Color: 1

Bin 4486: 19078 of cap free
Amount of items: 2
Items: 
Size: 790121 Color: 0
Size: 190802 Color: 1

Bin 4487: 19699 of cap free
Amount of items: 2
Items: 
Size: 643728 Color: 1
Size: 336574 Color: 0

Bin 4488: 21029 of cap free
Amount of items: 2
Items: 
Size: 789532 Color: 0
Size: 189440 Color: 1

Bin 4489: 22156 of cap free
Amount of items: 2
Items: 
Size: 789325 Color: 0
Size: 188520 Color: 1

Bin 4490: 24209 of cap free
Amount of items: 2
Items: 
Size: 789238 Color: 0
Size: 186554 Color: 1

Bin 4491: 24233 of cap free
Amount of items: 2
Items: 
Size: 791824 Color: 1
Size: 183944 Color: 0

Bin 4492: 27702 of cap free
Amount of items: 2
Items: 
Size: 791697 Color: 1
Size: 180602 Color: 0

Bin 4493: 28456 of cap free
Amount of items: 2
Items: 
Size: 599507 Color: 1
Size: 372038 Color: 0

Bin 4494: 29592 of cap free
Amount of items: 2
Items: 
Size: 787475 Color: 0
Size: 182934 Color: 1

Bin 4495: 30155 of cap free
Amount of items: 2
Items: 
Size: 787080 Color: 0
Size: 182766 Color: 1

Bin 4496: 30980 of cap free
Amount of items: 2
Items: 
Size: 791575 Color: 1
Size: 177446 Color: 0

Bin 4497: 32649 of cap free
Amount of items: 2
Items: 
Size: 631349 Color: 0
Size: 336003 Color: 1

Bin 4498: 34201 of cap free
Amount of items: 2
Items: 
Size: 712632 Color: 1
Size: 253168 Color: 0

Bin 4499: 36539 of cap free
Amount of items: 2
Items: 
Size: 781484 Color: 0
Size: 181978 Color: 1

Bin 4500: 42948 of cap free
Amount of items: 2
Items: 
Size: 788366 Color: 1
Size: 168687 Color: 0

Bin 4501: 43953 of cap free
Amount of items: 2
Items: 
Size: 707799 Color: 1
Size: 248249 Color: 0

Bin 4502: 51548 of cap free
Amount of items: 2
Items: 
Size: 778924 Color: 0
Size: 169529 Color: 1

Bin 4503: 59888 of cap free
Amount of items: 2
Items: 
Size: 776420 Color: 0
Size: 163693 Color: 1

Bin 4504: 75580 of cap free
Amount of items: 2
Items: 
Size: 776017 Color: 0
Size: 148404 Color: 1

Bin 4505: 78324 of cap free
Amount of items: 2
Items: 
Size: 775904 Color: 0
Size: 145773 Color: 1

Bin 4506: 78359 of cap free
Amount of items: 2
Items: 
Size: 598724 Color: 0
Size: 322918 Color: 1

Bin 4507: 78711 of cap free
Amount of items: 2
Items: 
Size: 788334 Color: 1
Size: 132956 Color: 0

Bin 4508: 93523 of cap free
Amount of items: 2
Items: 
Size: 773401 Color: 0
Size: 133077 Color: 1

Bin 4509: 216681 of cap free
Amount of items: 1
Items: 
Size: 783320 Color: 1

Bin 4510: 226146 of cap free
Amount of items: 1
Items: 
Size: 773855 Color: 1

Bin 4511: 227499 of cap free
Amount of items: 1
Items: 
Size: 772502 Color: 0

Bin 4512: 238182 of cap free
Amount of items: 1
Items: 
Size: 761819 Color: 1

Total size: 4508025388
Total free space: 3979124

