Capicity Bin: 1956
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 1
Size: 479 Color: 1
Size: 78 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1306 Color: 1
Size: 594 Color: 1
Size: 56 Color: 0

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 1232 Color: 1
Size: 724 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 1
Size: 215 Color: 1
Size: 42 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1498 Color: 1
Size: 254 Color: 1
Size: 112 Color: 0
Size: 92 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1695 Color: 1
Size: 219 Color: 1
Size: 42 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 1
Size: 231 Color: 1
Size: 66 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 1
Size: 451 Color: 1
Size: 90 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 1
Size: 331 Color: 1
Size: 32 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 1
Size: 182 Color: 1
Size: 68 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 1
Size: 210 Color: 1
Size: 60 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 1
Size: 303 Color: 1
Size: 94 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 1
Size: 726 Color: 1
Size: 144 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1641 Color: 1
Size: 263 Color: 1
Size: 52 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1538 Color: 1
Size: 382 Color: 1
Size: 36 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1223 Color: 1
Size: 661 Color: 1
Size: 72 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1349 Color: 1
Size: 491 Color: 1
Size: 116 Color: 0

Bin 18: 0 of cap free
Amount of items: 5
Items: 
Size: 1382 Color: 1
Size: 254 Color: 1
Size: 222 Color: 1
Size: 50 Color: 0
Size: 48 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1367 Color: 1
Size: 491 Color: 1
Size: 98 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 979 Color: 1
Size: 815 Color: 1
Size: 162 Color: 0

Bin 21: 0 of cap free
Amount of items: 7
Items: 
Size: 703 Color: 1
Size: 366 Color: 1
Size: 286 Color: 1
Size: 220 Color: 0
Size: 199 Color: 1
Size: 114 Color: 0
Size: 68 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 1
Size: 329 Color: 1
Size: 64 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1348 Color: 1
Size: 542 Color: 1
Size: 66 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1703 Color: 1
Size: 211 Color: 1
Size: 42 Color: 0

Bin 25: 0 of cap free
Amount of items: 5
Items: 
Size: 982 Color: 1
Size: 561 Color: 1
Size: 259 Color: 1
Size: 78 Color: 0
Size: 76 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 1
Size: 238 Color: 1
Size: 44 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1680 Color: 1
Size: 232 Color: 1
Size: 44 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1707 Color: 1
Size: 209 Color: 1
Size: 40 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 1
Size: 318 Color: 1
Size: 60 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 1
Size: 403 Color: 1
Size: 50 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 983 Color: 1
Size: 811 Color: 1
Size: 162 Color: 0

Bin 32: 0 of cap free
Amount of items: 5
Items: 
Size: 1263 Color: 1
Size: 543 Color: 1
Size: 72 Color: 1
Size: 40 Color: 0
Size: 38 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 1
Size: 301 Color: 1
Size: 60 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 1
Size: 450 Color: 1
Size: 88 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 987 Color: 1
Size: 809 Color: 1
Size: 160 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1518 Color: 1
Size: 350 Color: 1
Size: 88 Color: 0

Bin 37: 0 of cap free
Amount of items: 5
Items: 
Size: 1378 Color: 1
Size: 456 Color: 1
Size: 64 Color: 1
Size: 42 Color: 0
Size: 16 Color: 0

Bin 38: 0 of cap free
Amount of items: 5
Items: 
Size: 654 Color: 1
Size: 579 Color: 1
Size: 575 Color: 1
Size: 108 Color: 0
Size: 40 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1221 Color: 1
Size: 701 Color: 1
Size: 34 Color: 0

Bin 40: 0 of cap free
Amount of items: 5
Items: 
Size: 1113 Color: 1
Size: 482 Color: 1
Size: 251 Color: 1
Size: 64 Color: 0
Size: 46 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1363 Color: 1
Size: 549 Color: 1
Size: 44 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 1227 Color: 1
Size: 656 Color: 1
Size: 72 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 1
Size: 173 Color: 1
Size: 128 Color: 0

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 1735 Color: 1
Size: 220 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1174 Color: 1
Size: 667 Color: 1
Size: 114 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1483 Color: 1
Size: 404 Color: 1
Size: 68 Color: 0

Bin 47: 1 of cap free
Amount of items: 4
Items: 
Size: 1383 Color: 1
Size: 290 Color: 1
Size: 160 Color: 0
Size: 122 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 1
Size: 185 Color: 1
Size: 140 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 1
Size: 304 Color: 1
Size: 4 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 1
Size: 441 Color: 1
Size: 36 Color: 0

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 1
Size: 236 Color: 1
Size: 40 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 1519 Color: 1
Size: 395 Color: 1
Size: 40 Color: 0

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 1267 Color: 1
Size: 611 Color: 1
Size: 76 Color: 0

Bin 54: 3 of cap free
Amount of items: 5
Items: 
Size: 1283 Color: 1
Size: 365 Color: 1
Size: 265 Color: 1
Size: 20 Color: 0
Size: 20 Color: 0

Bin 55: 7 of cap free
Amount of items: 3
Items: 
Size: 1115 Color: 1
Size: 814 Color: 1
Size: 20 Color: 0

Bin 56: 7 of cap free
Amount of items: 3
Items: 
Size: 1487 Color: 1
Size: 402 Color: 1
Size: 60 Color: 0

Bin 57: 12 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 1
Size: 558 Color: 1
Size: 140 Color: 0

Bin 58: 34 of cap free
Amount of items: 3
Items: 
Size: 1387 Color: 1
Size: 495 Color: 1
Size: 40 Color: 0

Bin 59: 40 of cap free
Amount of items: 3
Items: 
Size: 1427 Color: 1
Size: 391 Color: 1
Size: 98 Color: 0

Bin 60: 207 of cap free
Amount of items: 1
Items: 
Size: 1749 Color: 1

Bin 61: 218 of cap free
Amount of items: 1
Items: 
Size: 1738 Color: 1

Bin 62: 237 of cap free
Amount of items: 1
Items: 
Size: 1719 Color: 1

Bin 63: 266 of cap free
Amount of items: 1
Items: 
Size: 1690 Color: 1

Bin 64: 272 of cap free
Amount of items: 1
Items: 
Size: 1684 Color: 1

Bin 65: 293 of cap free
Amount of items: 1
Items: 
Size: 1663 Color: 1

Bin 66: 346 of cap free
Amount of items: 1
Items: 
Size: 1610 Color: 1

Total size: 127140
Total free space: 1956

