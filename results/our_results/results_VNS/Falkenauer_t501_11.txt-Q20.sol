Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 326 Color: 2
Size: 254 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 5
Size: 251 Color: 13
Size: 250 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 18
Size: 302 Color: 17
Size: 250 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 19
Size: 357 Color: 5
Size: 285 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 17
Size: 367 Color: 12
Size: 253 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9
Size: 296 Color: 19
Size: 262 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 364 Color: 0
Size: 255 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 3
Size: 320 Color: 3
Size: 257 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 11
Size: 297 Color: 16
Size: 289 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 11
Size: 365 Color: 7
Size: 261 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 11
Size: 356 Color: 1
Size: 276 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 3
Size: 350 Color: 6
Size: 265 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 368 Color: 16
Size: 261 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 5
Size: 258 Color: 2
Size: 257 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 2
Size: 325 Color: 1
Size: 278 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 16
Size: 269 Color: 18
Size: 266 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 270 Color: 9
Size: 266 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 359 Color: 5
Size: 278 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 19
Size: 312 Color: 5
Size: 275 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 8
Size: 297 Color: 2
Size: 254 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 15
Size: 328 Color: 7
Size: 253 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 18
Size: 270 Color: 18
Size: 262 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 369 Color: 19
Size: 253 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 291 Color: 13
Size: 260 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 15
Size: 360 Color: 19
Size: 254 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7
Size: 303 Color: 13
Size: 299 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 352 Color: 1
Size: 269 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 19
Size: 345 Color: 15
Size: 265 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 9
Size: 325 Color: 19
Size: 302 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 3
Size: 357 Color: 18
Size: 283 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 10
Size: 314 Color: 7
Size: 282 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 16
Size: 285 Color: 19
Size: 252 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 14
Size: 322 Color: 12
Size: 316 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 2
Size: 360 Color: 17
Size: 271 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 6
Size: 302 Color: 8
Size: 291 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 328 Color: 1
Size: 288 Color: 13

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 11
Size: 323 Color: 3
Size: 317 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 19
Size: 252 Color: 5
Size: 250 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 322 Color: 19
Size: 252 Color: 5

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 5
Size: 324 Color: 6
Size: 312 Color: 19

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 14
Size: 338 Color: 15
Size: 268 Color: 8

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 19
Size: 285 Color: 16
Size: 256 Color: 17

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 342 Color: 5
Size: 275 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 3
Size: 300 Color: 13
Size: 283 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 8
Size: 289 Color: 7
Size: 258 Color: 18

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 12
Size: 271 Color: 7
Size: 260 Color: 10

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 5
Size: 255 Color: 6
Size: 253 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 2
Size: 289 Color: 17
Size: 261 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 2
Size: 357 Color: 8
Size: 283 Color: 13

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 371 Color: 5
Size: 255 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 262 Color: 10
Size: 255 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 4
Size: 328 Color: 13
Size: 280 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 16
Size: 291 Color: 15
Size: 280 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 3
Size: 292 Color: 9
Size: 256 Color: 9

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 13
Size: 289 Color: 0
Size: 278 Color: 7

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 9
Size: 287 Color: 14
Size: 281 Color: 7

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 19
Size: 316 Color: 2
Size: 275 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 266 Color: 7
Size: 256 Color: 17

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 5
Size: 274 Color: 15
Size: 251 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 5
Size: 307 Color: 0
Size: 267 Color: 13

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 9
Size: 371 Color: 6
Size: 255 Color: 15

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7
Size: 350 Color: 6
Size: 267 Color: 13

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 8
Size: 280 Color: 8
Size: 257 Color: 5

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 11
Size: 334 Color: 1
Size: 284 Color: 17

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 11
Size: 292 Color: 12
Size: 251 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 3
Size: 300 Color: 0
Size: 291 Color: 15

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 12
Size: 338 Color: 16
Size: 279 Color: 13

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9
Size: 277 Color: 8
Size: 269 Color: 19

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 19
Size: 298 Color: 11
Size: 259 Color: 7

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 16
Size: 285 Color: 3
Size: 284 Color: 15

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 13
Size: 257 Color: 1
Size: 253 Color: 15

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 5
Size: 367 Color: 18
Size: 264 Color: 18

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 16
Size: 346 Color: 19
Size: 270 Color: 8

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 14
Size: 334 Color: 19
Size: 258 Color: 18

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9
Size: 258 Color: 2
Size: 252 Color: 18

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 9
Size: 335 Color: 0
Size: 251 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 8
Size: 355 Color: 9
Size: 267 Color: 18

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 12
Size: 315 Color: 19
Size: 312 Color: 7

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 8
Size: 286 Color: 16
Size: 256 Color: 17

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 12
Size: 345 Color: 5
Size: 255 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 12
Size: 353 Color: 9
Size: 278 Color: 9

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 9
Size: 280 Color: 10
Size: 279 Color: 19

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 17
Size: 268 Color: 3
Size: 261 Color: 5

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 362 Color: 19
Size: 261 Color: 11

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 12
Size: 320 Color: 10
Size: 262 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 359 Color: 14
Size: 259 Color: 5

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 310 Color: 12
Size: 291 Color: 10

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 9
Size: 317 Color: 11
Size: 285 Color: 10

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 8
Size: 284 Color: 19
Size: 267 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 17
Size: 296 Color: 12
Size: 295 Color: 17

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 5
Size: 269 Color: 0
Size: 257 Color: 8

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 262 Color: 6
Size: 255 Color: 6

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 17
Size: 320 Color: 4
Size: 250 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 297 Color: 14
Size: 252 Color: 16

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 5
Size: 279 Color: 4
Size: 276 Color: 7

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 9
Size: 296 Color: 4
Size: 250 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 258 Color: 6
Size: 254 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 16
Size: 254 Color: 7
Size: 250 Color: 3

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 11
Size: 259 Color: 11
Size: 253 Color: 5

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 2
Size: 315 Color: 19
Size: 266 Color: 8

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 18
Size: 315 Color: 3
Size: 288 Color: 16

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 10
Size: 330 Color: 3
Size: 277 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 10
Size: 339 Color: 4
Size: 262 Color: 18

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 13
Size: 278 Color: 1
Size: 271 Color: 9

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 11
Size: 347 Color: 4
Size: 293 Color: 5

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 309 Color: 18
Size: 290 Color: 6

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 11
Size: 256 Color: 2
Size: 253 Color: 9

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 19
Size: 285 Color: 12
Size: 282 Color: 15

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 12
Size: 351 Color: 5
Size: 258 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 7
Size: 359 Color: 0
Size: 276 Color: 14

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 8
Size: 339 Color: 2
Size: 267 Color: 16

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 5
Size: 313 Color: 15
Size: 278 Color: 19

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 17
Size: 313 Color: 0
Size: 264 Color: 11

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 16
Size: 326 Color: 2
Size: 303 Color: 15

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 15
Size: 284 Color: 18
Size: 278 Color: 18

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 11
Size: 297 Color: 13
Size: 286 Color: 6

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 0
Size: 289 Color: 18
Size: 278 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 275 Color: 6
Size: 263 Color: 5

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9
Size: 251 Color: 9
Size: 251 Color: 2

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 14
Size: 326 Color: 18
Size: 307 Color: 11

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 4
Size: 318 Color: 17
Size: 254 Color: 15

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 12
Size: 332 Color: 11
Size: 289 Color: 18

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 19
Size: 274 Color: 19
Size: 255 Color: 10

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 263 Color: 18
Size: 252 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 18
Size: 357 Color: 12
Size: 282 Color: 11

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 12
Size: 296 Color: 17
Size: 253 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 4
Size: 366 Color: 14
Size: 258 Color: 12

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8
Size: 319 Color: 2
Size: 259 Color: 6

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 10
Size: 291 Color: 8
Size: 275 Color: 3

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 13
Size: 352 Color: 6
Size: 289 Color: 6

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 351 Color: 13
Size: 274 Color: 7

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 16
Size: 315 Color: 2
Size: 309 Color: 3

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 15
Size: 253 Color: 10
Size: 252 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 19
Size: 330 Color: 17
Size: 297 Color: 16

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 6
Size: 305 Color: 5
Size: 300 Color: 4

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 3
Size: 290 Color: 8
Size: 255 Color: 13

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 16
Size: 280 Color: 15
Size: 253 Color: 14

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 7
Size: 270 Color: 8
Size: 265 Color: 19

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 10
Size: 285 Color: 10
Size: 256 Color: 16

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 17
Size: 326 Color: 19
Size: 315 Color: 7

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 15
Size: 320 Color: 14
Size: 266 Color: 11

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 288 Color: 6
Size: 271 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 19
Size: 297 Color: 19
Size: 250 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 16
Size: 357 Color: 4
Size: 270 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 7
Size: 301 Color: 8
Size: 282 Color: 13

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 16
Size: 296 Color: 0
Size: 268 Color: 19

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 14
Size: 346 Color: 8
Size: 268 Color: 17

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 330 Color: 9
Size: 274 Color: 9

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 6
Size: 358 Color: 10
Size: 280 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 8
Size: 371 Color: 11
Size: 252 Color: 9

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 267 Color: 9
Size: 254 Color: 5

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 8
Size: 273 Color: 14
Size: 254 Color: 8

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 301 Color: 16
Size: 270 Color: 12

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7
Size: 341 Color: 9
Size: 280 Color: 7

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 5
Size: 270 Color: 13
Size: 260 Color: 5

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 15
Size: 274 Color: 5
Size: 264 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 10
Size: 321 Color: 16
Size: 269 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 11
Size: 282 Color: 15
Size: 257 Color: 15

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 7
Size: 285 Color: 13
Size: 261 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 9
Size: 341 Color: 0
Size: 281 Color: 9

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 14
Size: 320 Color: 15
Size: 268 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 10
Size: 352 Color: 16
Size: 267 Color: 5

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 19
Size: 345 Color: 16
Size: 275 Color: 5

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 19
Size: 352 Color: 13
Size: 263 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 16
Size: 300 Color: 11
Size: 287 Color: 12

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 267 Color: 1
Size: 253 Color: 17

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 3
Size: 337 Color: 14
Size: 278 Color: 17

Total size: 167000
Total free space: 0

