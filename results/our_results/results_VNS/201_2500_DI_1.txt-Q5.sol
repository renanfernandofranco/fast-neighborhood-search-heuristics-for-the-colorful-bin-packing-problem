Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1418 Color: 0
Size: 902 Color: 3
Size: 96 Color: 1
Size: 48 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 4
Size: 978 Color: 1
Size: 80 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 3
Size: 763 Color: 0
Size: 152 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 2
Size: 780 Color: 4
Size: 40 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 0
Size: 665 Color: 3
Size: 132 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 2
Size: 622 Color: 2
Size: 120 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 0
Size: 576 Color: 4
Size: 82 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 1
Size: 604 Color: 4
Size: 40 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 0
Size: 518 Color: 1
Size: 100 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1870 Color: 2
Size: 498 Color: 1
Size: 96 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 0
Size: 454 Color: 1
Size: 76 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 4
Size: 540 Color: 0
Size: 40 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 2
Size: 482 Color: 4
Size: 40 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 3
Size: 404 Color: 1
Size: 72 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1990 Color: 0
Size: 398 Color: 4
Size: 76 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 1
Size: 244 Color: 3
Size: 214 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 1
Size: 370 Color: 4
Size: 40 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2019 Color: 0
Size: 371 Color: 3
Size: 74 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 1
Size: 338 Color: 0
Size: 64 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 0
Size: 272 Color: 1
Size: 84 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 1
Size: 236 Color: 3
Size: 64 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 2
Size: 234 Color: 1
Size: 72 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 4
Size: 252 Color: 1
Size: 40 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 1
Size: 172 Color: 0
Size: 112 Color: 2

Bin 25: 0 of cap free
Amount of items: 5
Items: 
Size: 2188 Color: 4
Size: 258 Color: 2
Size: 8 Color: 3
Size: 8 Color: 1
Size: 2 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 3
Size: 182 Color: 1
Size: 72 Color: 4

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 3
Size: 1023 Color: 1
Size: 204 Color: 3

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 3
Size: 1026 Color: 4
Size: 200 Color: 4

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 0
Size: 1049 Color: 4
Size: 172 Color: 1

Bin 30: 1 of cap free
Amount of items: 4
Items: 
Size: 1714 Color: 3
Size: 384 Color: 4
Size: 305 Color: 4
Size: 60 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 2
Size: 550 Color: 1
Size: 148 Color: 4

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 1971 Color: 2
Size: 390 Color: 1
Size: 102 Color: 3

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 2059 Color: 0
Size: 338 Color: 1
Size: 66 Color: 2

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 3
Size: 253 Color: 1
Size: 120 Color: 3

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 2099 Color: 4
Size: 308 Color: 0
Size: 56 Color: 1

Bin 36: 2 of cap free
Amount of items: 4
Items: 
Size: 1233 Color: 4
Size: 626 Color: 0
Size: 515 Color: 1
Size: 88 Color: 1

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1383 Color: 4
Size: 1079 Color: 2

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 1
Size: 882 Color: 0
Size: 48 Color: 4

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 1544 Color: 3
Size: 874 Color: 1
Size: 44 Color: 2

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 3
Size: 782 Color: 3
Size: 50 Color: 1

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 1978 Color: 4
Size: 484 Color: 2

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 3
Size: 406 Color: 2
Size: 28 Color: 1

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 2098 Color: 4
Size: 364 Color: 3

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 2130 Color: 4
Size: 332 Color: 3

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 1
Size: 339 Color: 0
Size: 200 Color: 4

Bin 46: 4 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 0
Size: 920 Color: 1
Size: 90 Color: 3

Bin 47: 5 of cap free
Amount of items: 4
Items: 
Size: 1409 Color: 0
Size: 782 Color: 3
Size: 204 Color: 1
Size: 64 Color: 4

Bin 48: 5 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 2
Size: 411 Color: 3
Size: 108 Color: 1

Bin 49: 6 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 3
Size: 684 Color: 2
Size: 176 Color: 1

Bin 50: 6 of cap free
Amount of items: 2
Items: 
Size: 2014 Color: 3
Size: 444 Color: 0

Bin 51: 6 of cap free
Amount of items: 2
Items: 
Size: 2186 Color: 0
Size: 272 Color: 4

Bin 52: 7 of cap free
Amount of items: 3
Items: 
Size: 1915 Color: 1
Size: 490 Color: 2
Size: 52 Color: 3

Bin 53: 7 of cap free
Amount of items: 2
Items: 
Size: 1998 Color: 3
Size: 459 Color: 0

Bin 54: 9 of cap free
Amount of items: 13
Items: 
Size: 504 Color: 1
Size: 382 Color: 4
Size: 281 Color: 3
Size: 244 Color: 3
Size: 152 Color: 1
Size: 152 Color: 1
Size: 136 Color: 2
Size: 124 Color: 3
Size: 116 Color: 2
Size: 104 Color: 4
Size: 96 Color: 0
Size: 88 Color: 0
Size: 76 Color: 0

Bin 55: 10 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 0
Size: 758 Color: 2

Bin 56: 14 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 3
Size: 840 Color: 0
Size: 80 Color: 4

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 2068 Color: 4
Size: 378 Color: 3

Bin 58: 21 of cap free
Amount of items: 2
Items: 
Size: 2161 Color: 0
Size: 282 Color: 4

Bin 59: 23 of cap free
Amount of items: 2
Items: 
Size: 2127 Color: 2
Size: 314 Color: 0

Bin 60: 25 of cap free
Amount of items: 2
Items: 
Size: 1558 Color: 1
Size: 881 Color: 4

Bin 61: 26 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 2
Size: 892 Color: 1
Size: 136 Color: 3

Bin 62: 26 of cap free
Amount of items: 2
Items: 
Size: 1740 Color: 3
Size: 698 Color: 2

Bin 63: 28 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 2
Size: 1028 Color: 4
Size: 174 Color: 0

Bin 64: 34 of cap free
Amount of items: 2
Items: 
Size: 1847 Color: 4
Size: 583 Color: 3

Bin 65: 46 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 4
Size: 1022 Color: 0

Bin 66: 2108 of cap free
Amount of items: 6
Items: 
Size: 80 Color: 3
Size: 72 Color: 0
Size: 60 Color: 2
Size: 48 Color: 4
Size: 48 Color: 3
Size: 48 Color: 1

Total size: 160160
Total free space: 2464

