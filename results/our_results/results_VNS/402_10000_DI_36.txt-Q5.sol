Capicity Bin: 8256
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4682 Color: 3
Size: 3434 Color: 0
Size: 140 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4747 Color: 2
Size: 2925 Color: 0
Size: 584 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 2
Size: 2916 Color: 0
Size: 184 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5302 Color: 1
Size: 2744 Color: 1
Size: 210 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5420 Color: 1
Size: 2534 Color: 1
Size: 302 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5629 Color: 3
Size: 2191 Color: 0
Size: 436 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5696 Color: 4
Size: 2488 Color: 0
Size: 72 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5922 Color: 2
Size: 1272 Color: 1
Size: 1062 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6200 Color: 0
Size: 1864 Color: 3
Size: 192 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6358 Color: 0
Size: 1582 Color: 3
Size: 316 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6470 Color: 3
Size: 1106 Color: 4
Size: 680 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6479 Color: 2
Size: 1481 Color: 2
Size: 296 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 3
Size: 1335 Color: 2
Size: 266 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 1
Size: 976 Color: 0
Size: 556 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6804 Color: 3
Size: 1212 Color: 2
Size: 240 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6814 Color: 4
Size: 834 Color: 3
Size: 608 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6856 Color: 4
Size: 1284 Color: 3
Size: 116 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6914 Color: 3
Size: 1022 Color: 0
Size: 320 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6930 Color: 1
Size: 1202 Color: 4
Size: 124 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 2
Size: 836 Color: 3
Size: 480 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 0
Size: 1032 Color: 3
Size: 272 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6982 Color: 2
Size: 1174 Color: 3
Size: 100 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 1
Size: 1096 Color: 3
Size: 156 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 2
Size: 970 Color: 3
Size: 272 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7032 Color: 4
Size: 964 Color: 1
Size: 260 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7104 Color: 0
Size: 908 Color: 1
Size: 244 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7120 Color: 4
Size: 1044 Color: 3
Size: 92 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7124 Color: 4
Size: 912 Color: 3
Size: 220 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 3
Size: 686 Color: 2
Size: 388 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7176 Color: 2
Size: 680 Color: 3
Size: 400 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7242 Color: 2
Size: 512 Color: 0
Size: 502 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7260 Color: 1
Size: 792 Color: 4
Size: 204 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7304 Color: 1
Size: 808 Color: 3
Size: 144 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7338 Color: 0
Size: 742 Color: 4
Size: 176 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7380 Color: 2
Size: 684 Color: 3
Size: 192 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7430 Color: 0
Size: 442 Color: 3
Size: 384 Color: 4

Bin 37: 1 of cap free
Amount of items: 7
Items: 
Size: 4129 Color: 3
Size: 922 Color: 2
Size: 904 Color: 4
Size: 740 Color: 3
Size: 624 Color: 2
Size: 568 Color: 1
Size: 368 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 4333 Color: 4
Size: 3422 Color: 3
Size: 500 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 4739 Color: 1
Size: 3028 Color: 4
Size: 488 Color: 3

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5601 Color: 4
Size: 2182 Color: 2
Size: 472 Color: 3

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5544 Color: 1
Size: 2507 Color: 1
Size: 204 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6095 Color: 4
Size: 1892 Color: 1
Size: 268 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 3
Size: 1947 Color: 1
Size: 192 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6249 Color: 2
Size: 1862 Color: 1
Size: 144 Color: 3

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6374 Color: 3
Size: 1673 Color: 4
Size: 208 Color: 4

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 4
Size: 1555 Color: 3
Size: 196 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6694 Color: 1
Size: 1321 Color: 3
Size: 240 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6760 Color: 3
Size: 1183 Color: 1
Size: 312 Color: 2

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 4
Size: 1225 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 4138 Color: 2
Size: 3404 Color: 3
Size: 712 Color: 2

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 4146 Color: 1
Size: 3804 Color: 4
Size: 304 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 5286 Color: 2
Size: 2372 Color: 1
Size: 596 Color: 0

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 3
Size: 2364 Color: 4
Size: 236 Color: 0

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 6253 Color: 2
Size: 1643 Color: 1
Size: 358 Color: 3

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 3
Size: 1726 Color: 4
Size: 212 Color: 2

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 6463 Color: 3
Size: 1359 Color: 0
Size: 432 Color: 1

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 2
Size: 1396 Color: 3
Size: 270 Color: 1

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6671 Color: 1
Size: 1519 Color: 0
Size: 64 Color: 3

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 7298 Color: 1
Size: 732 Color: 3
Size: 224 Color: 1

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 5249 Color: 0
Size: 2784 Color: 4
Size: 220 Color: 3

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 4
Size: 2201 Color: 2
Size: 216 Color: 1

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 5987 Color: 2
Size: 2138 Color: 4
Size: 128 Color: 1

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 6617 Color: 2
Size: 1268 Color: 1
Size: 368 Color: 3

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 6837 Color: 2
Size: 1094 Color: 3
Size: 322 Color: 2

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6963 Color: 2
Size: 1290 Color: 0

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 7108 Color: 0
Size: 1145 Color: 2

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 4666 Color: 4
Size: 3442 Color: 3
Size: 144 Color: 2

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 4898 Color: 0
Size: 3140 Color: 4
Size: 214 Color: 3

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 4968 Color: 1
Size: 3064 Color: 1
Size: 220 Color: 3

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 5184 Color: 0
Size: 2013 Color: 0
Size: 1055 Color: 3

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 5906 Color: 4
Size: 2170 Color: 0
Size: 176 Color: 1

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 5917 Color: 4
Size: 1887 Color: 3
Size: 448 Color: 3

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 6598 Color: 2
Size: 1654 Color: 4

Bin 74: 5 of cap free
Amount of items: 5
Items: 
Size: 4133 Color: 1
Size: 1814 Color: 3
Size: 1358 Color: 0
Size: 690 Color: 0
Size: 256 Color: 2

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 4172 Color: 0
Size: 3775 Color: 4
Size: 304 Color: 1

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 4667 Color: 0
Size: 3448 Color: 4
Size: 136 Color: 1

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 5923 Color: 0
Size: 2264 Color: 4
Size: 64 Color: 1

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 5988 Color: 4
Size: 1975 Color: 0
Size: 288 Color: 2

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 6787 Color: 0
Size: 1464 Color: 4

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 4584 Color: 3
Size: 2994 Color: 1
Size: 672 Color: 0

Bin 81: 6 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 1
Size: 2870 Color: 1
Size: 240 Color: 3

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 5644 Color: 1
Size: 2478 Color: 3
Size: 128 Color: 0

Bin 83: 6 of cap free
Amount of items: 3
Items: 
Size: 5960 Color: 2
Size: 1778 Color: 3
Size: 512 Color: 3

Bin 84: 6 of cap free
Amount of items: 3
Items: 
Size: 6026 Color: 0
Size: 1928 Color: 0
Size: 296 Color: 3

Bin 85: 6 of cap free
Amount of items: 2
Items: 
Size: 6630 Color: 2
Size: 1620 Color: 0

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 6883 Color: 2
Size: 1367 Color: 4

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 7416 Color: 0
Size: 834 Color: 4

Bin 88: 7 of cap free
Amount of items: 6
Items: 
Size: 4132 Color: 0
Size: 1176 Color: 2
Size: 1111 Color: 2
Size: 1002 Color: 0
Size: 684 Color: 1
Size: 144 Color: 2

Bin 89: 7 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 0
Size: 2802 Color: 2
Size: 264 Color: 3

Bin 90: 7 of cap free
Amount of items: 2
Items: 
Size: 5318 Color: 4
Size: 2931 Color: 3

Bin 91: 8 of cap free
Amount of items: 3
Items: 
Size: 4154 Color: 0
Size: 3328 Color: 2
Size: 766 Color: 1

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 6344 Color: 2
Size: 1904 Color: 1

Bin 93: 8 of cap free
Amount of items: 2
Items: 
Size: 6946 Color: 2
Size: 1302 Color: 4

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 7210 Color: 1
Size: 1038 Color: 4

Bin 95: 8 of cap free
Amount of items: 2
Items: 
Size: 7258 Color: 1
Size: 990 Color: 4

Bin 96: 8 of cap free
Amount of items: 2
Items: 
Size: 7300 Color: 2
Size: 948 Color: 1

Bin 97: 8 of cap free
Amount of items: 2
Items: 
Size: 7320 Color: 0
Size: 928 Color: 2

Bin 98: 8 of cap free
Amount of items: 4
Items: 
Size: 7372 Color: 4
Size: 828 Color: 2
Size: 32 Color: 3
Size: 16 Color: 1

Bin 99: 9 of cap free
Amount of items: 2
Items: 
Size: 6285 Color: 2
Size: 1962 Color: 4

Bin 100: 9 of cap free
Amount of items: 2
Items: 
Size: 6991 Color: 0
Size: 1256 Color: 4

Bin 101: 10 of cap free
Amount of items: 2
Items: 
Size: 4820 Color: 0
Size: 3426 Color: 4

Bin 102: 10 of cap free
Amount of items: 2
Items: 
Size: 5784 Color: 3
Size: 2462 Color: 4

Bin 103: 10 of cap free
Amount of items: 2
Items: 
Size: 6226 Color: 4
Size: 2020 Color: 2

Bin 104: 11 of cap free
Amount of items: 2
Items: 
Size: 7166 Color: 0
Size: 1079 Color: 4

Bin 105: 12 of cap free
Amount of items: 9
Items: 
Size: 4130 Color: 0
Size: 684 Color: 2
Size: 586 Color: 3
Size: 586 Color: 3
Size: 504 Color: 2
Size: 492 Color: 4
Size: 492 Color: 1
Size: 436 Color: 0
Size: 334 Color: 4

Bin 106: 12 of cap free
Amount of items: 3
Items: 
Size: 5218 Color: 3
Size: 2588 Color: 1
Size: 438 Color: 0

Bin 107: 12 of cap free
Amount of items: 2
Items: 
Size: 7172 Color: 4
Size: 1072 Color: 1

Bin 108: 12 of cap free
Amount of items: 2
Items: 
Size: 7370 Color: 1
Size: 874 Color: 4

Bin 109: 18 of cap free
Amount of items: 2
Items: 
Size: 6710 Color: 0
Size: 1528 Color: 1

Bin 110: 20 of cap free
Amount of items: 2
Items: 
Size: 6435 Color: 2
Size: 1801 Color: 1

Bin 111: 21 of cap free
Amount of items: 3
Items: 
Size: 5241 Color: 1
Size: 2450 Color: 3
Size: 544 Color: 2

Bin 112: 21 of cap free
Amount of items: 2
Items: 
Size: 6627 Color: 1
Size: 1608 Color: 2

Bin 113: 22 of cap free
Amount of items: 2
Items: 
Size: 6744 Color: 0
Size: 1490 Color: 4

Bin 114: 24 of cap free
Amount of items: 2
Items: 
Size: 6850 Color: 4
Size: 1382 Color: 2

Bin 115: 27 of cap free
Amount of items: 3
Items: 
Size: 4136 Color: 4
Size: 3441 Color: 4
Size: 652 Color: 2

Bin 116: 31 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 0
Size: 1951 Color: 4

Bin 117: 32 of cap free
Amount of items: 2
Items: 
Size: 6740 Color: 0
Size: 1484 Color: 1

Bin 118: 35 of cap free
Amount of items: 2
Items: 
Size: 6925 Color: 4
Size: 1296 Color: 0

Bin 119: 38 of cap free
Amount of items: 2
Items: 
Size: 5028 Color: 4
Size: 3190 Color: 1

Bin 120: 40 of cap free
Amount of items: 2
Items: 
Size: 4492 Color: 0
Size: 3724 Color: 3

Bin 121: 40 of cap free
Amount of items: 2
Items: 
Size: 7094 Color: 4
Size: 1122 Color: 2

Bin 122: 44 of cap free
Amount of items: 2
Items: 
Size: 6424 Color: 2
Size: 1788 Color: 1

Bin 123: 48 of cap free
Amount of items: 2
Items: 
Size: 5995 Color: 4
Size: 2213 Color: 2

Bin 124: 57 of cap free
Amount of items: 2
Items: 
Size: 5638 Color: 1
Size: 2561 Color: 4

Bin 125: 58 of cap free
Amount of items: 2
Items: 
Size: 6126 Color: 2
Size: 2072 Color: 3

Bin 126: 60 of cap free
Amount of items: 2
Items: 
Size: 6484 Color: 4
Size: 1712 Color: 0

Bin 127: 71 of cap free
Amount of items: 2
Items: 
Size: 4914 Color: 2
Size: 3271 Color: 4

Bin 128: 76 of cap free
Amount of items: 2
Items: 
Size: 4731 Color: 2
Size: 3449 Color: 3

Bin 129: 77 of cap free
Amount of items: 3
Items: 
Size: 4140 Color: 3
Size: 2939 Color: 1
Size: 1100 Color: 0

Bin 130: 100 of cap free
Amount of items: 2
Items: 
Size: 5288 Color: 4
Size: 2868 Color: 3

Bin 131: 106 of cap free
Amount of items: 28
Items: 
Size: 472 Color: 3
Size: 400 Color: 3
Size: 400 Color: 1
Size: 388 Color: 2
Size: 376 Color: 3
Size: 360 Color: 0
Size: 352 Color: 2
Size: 352 Color: 1
Size: 348 Color: 0
Size: 328 Color: 4
Size: 328 Color: 2
Size: 304 Color: 4
Size: 288 Color: 0
Size: 276 Color: 1
Size: 256 Color: 0
Size: 248 Color: 2
Size: 248 Color: 1
Size: 248 Color: 1
Size: 240 Color: 3
Size: 240 Color: 0
Size: 238 Color: 2
Size: 232 Color: 1
Size: 228 Color: 3
Size: 216 Color: 2
Size: 216 Color: 0
Size: 208 Color: 1
Size: 184 Color: 3
Size: 176 Color: 3

Bin 132: 126 of cap free
Amount of items: 2
Items: 
Size: 5617 Color: 1
Size: 2513 Color: 2

Bin 133: 6712 of cap free
Amount of items: 9
Items: 
Size: 200 Color: 1
Size: 200 Color: 0
Size: 176 Color: 4
Size: 176 Color: 4
Size: 172 Color: 0
Size: 164 Color: 3
Size: 160 Color: 3
Size: 152 Color: 1
Size: 144 Color: 4

Total size: 1089792
Total free space: 8256

