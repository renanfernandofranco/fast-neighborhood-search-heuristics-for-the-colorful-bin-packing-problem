Capicity Bin: 1888
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1090 Color: 18
Size: 666 Color: 13
Size: 132 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 18
Size: 501 Color: 5
Size: 100 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 19
Size: 522 Color: 7
Size: 40 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1367 Color: 11
Size: 313 Color: 12
Size: 208 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 16
Size: 398 Color: 5
Size: 96 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1473 Color: 7
Size: 365 Color: 7
Size: 50 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1495 Color: 19
Size: 269 Color: 14
Size: 124 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 7
Size: 170 Color: 18
Size: 156 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 15
Size: 217 Color: 11
Size: 86 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 14
Size: 213 Color: 1
Size: 62 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 2
Size: 208 Color: 13
Size: 58 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 18
Size: 199 Color: 19
Size: 42 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 10
Size: 190 Color: 1
Size: 36 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 7
Size: 198 Color: 19
Size: 20 Color: 15

Bin 15: 1 of cap free
Amount of items: 4
Items: 
Size: 946 Color: 16
Size: 493 Color: 10
Size: 392 Color: 5
Size: 56 Color: 14

Bin 16: 1 of cap free
Amount of items: 5
Items: 
Size: 947 Color: 13
Size: 497 Color: 19
Size: 327 Color: 7
Size: 72 Color: 1
Size: 44 Color: 13

Bin 17: 1 of cap free
Amount of items: 4
Items: 
Size: 1081 Color: 8
Size: 738 Color: 9
Size: 36 Color: 8
Size: 32 Color: 7

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 13
Size: 640 Color: 0
Size: 50 Color: 8

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 2
Size: 573 Color: 15
Size: 60 Color: 13

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 9
Size: 522 Color: 14
Size: 64 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1304 Color: 13
Size: 491 Color: 5
Size: 92 Color: 9

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1425 Color: 12
Size: 436 Color: 19
Size: 26 Color: 7

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1500 Color: 2
Size: 387 Color: 15

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 14
Size: 329 Color: 1
Size: 52 Color: 4

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 10
Size: 324 Color: 9
Size: 50 Color: 17

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 4
Size: 261 Color: 2
Size: 92 Color: 17

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1537 Color: 8
Size: 350 Color: 13

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 15
Size: 322 Color: 11
Size: 16 Color: 19

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 5
Size: 182 Color: 2
Size: 134 Color: 13

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1577 Color: 4
Size: 274 Color: 14
Size: 36 Color: 17

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 14
Size: 298 Color: 0
Size: 8 Color: 2

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 19
Size: 253 Color: 8
Size: 48 Color: 13

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 1633 Color: 19
Size: 254 Color: 4

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 10
Size: 132 Color: 18
Size: 104 Color: 13

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 4
Size: 209 Color: 11

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1126 Color: 5
Size: 760 Color: 9

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1209 Color: 1
Size: 677 Color: 15

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 7
Size: 435 Color: 12
Size: 52 Color: 15

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 1593 Color: 18
Size: 293 Color: 7

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 1629 Color: 15
Size: 257 Color: 11

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1184 Color: 14
Size: 669 Color: 8
Size: 32 Color: 3

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 10
Size: 567 Color: 13
Size: 56 Color: 19

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 1451 Color: 0
Size: 358 Color: 17
Size: 76 Color: 0

Bin 44: 3 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 17
Size: 283 Color: 3

Bin 45: 3 of cap free
Amount of items: 2
Items: 
Size: 1654 Color: 8
Size: 231 Color: 3

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 18
Size: 191 Color: 0
Size: 8 Color: 16

Bin 47: 4 of cap free
Amount of items: 18
Items: 
Size: 178 Color: 7
Size: 156 Color: 13
Size: 156 Color: 7
Size: 144 Color: 8
Size: 134 Color: 17
Size: 134 Color: 17
Size: 114 Color: 19
Size: 112 Color: 11
Size: 104 Color: 19
Size: 98 Color: 11
Size: 98 Color: 2
Size: 80 Color: 9
Size: 68 Color: 18
Size: 68 Color: 15
Size: 68 Color: 14
Size: 68 Color: 8
Size: 64 Color: 4
Size: 40 Color: 5

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 17
Size: 414 Color: 13

Bin 49: 4 of cap free
Amount of items: 2
Items: 
Size: 1642 Color: 9
Size: 242 Color: 16

Bin 50: 5 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 2
Size: 606 Color: 9
Size: 76 Color: 5

Bin 51: 5 of cap free
Amount of items: 2
Items: 
Size: 1661 Color: 1
Size: 222 Color: 16

Bin 52: 7 of cap free
Amount of items: 3
Items: 
Size: 1517 Color: 7
Size: 292 Color: 4
Size: 72 Color: 13

Bin 53: 10 of cap free
Amount of items: 2
Items: 
Size: 1297 Color: 1
Size: 581 Color: 16

Bin 54: 11 of cap free
Amount of items: 5
Items: 
Size: 945 Color: 14
Size: 347 Color: 11
Size: 265 Color: 12
Size: 206 Color: 4
Size: 114 Color: 3

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1089 Color: 15
Size: 787 Color: 19

Bin 56: 12 of cap free
Amount of items: 2
Items: 
Size: 1414 Color: 7
Size: 462 Color: 6

Bin 57: 14 of cap free
Amount of items: 2
Items: 
Size: 1193 Color: 6
Size: 681 Color: 1

Bin 58: 17 of cap free
Amount of items: 3
Items: 
Size: 1006 Color: 14
Size: 785 Color: 0
Size: 80 Color: 17

Bin 59: 17 of cap free
Amount of items: 2
Items: 
Size: 1462 Color: 13
Size: 409 Color: 18

Bin 60: 18 of cap free
Amount of items: 2
Items: 
Size: 1293 Color: 0
Size: 577 Color: 2

Bin 61: 24 of cap free
Amount of items: 2
Items: 
Size: 1334 Color: 12
Size: 530 Color: 6

Bin 62: 25 of cap free
Amount of items: 3
Items: 
Size: 1187 Color: 5
Size: 638 Color: 10
Size: 38 Color: 19

Bin 63: 25 of cap free
Amount of items: 2
Items: 
Size: 1190 Color: 4
Size: 673 Color: 15

Bin 64: 29 of cap free
Amount of items: 2
Items: 
Size: 1073 Color: 5
Size: 786 Color: 15

Bin 65: 30 of cap free
Amount of items: 3
Items: 
Size: 1077 Color: 19
Size: 667 Color: 15
Size: 114 Color: 14

Bin 66: 1566 of cap free
Amount of items: 8
Items: 
Size: 48 Color: 12
Size: 44 Color: 15
Size: 44 Color: 10
Size: 44 Color: 4
Size: 42 Color: 13
Size: 36 Color: 9
Size: 32 Color: 7
Size: 32 Color: 5

Total size: 122720
Total free space: 1888

