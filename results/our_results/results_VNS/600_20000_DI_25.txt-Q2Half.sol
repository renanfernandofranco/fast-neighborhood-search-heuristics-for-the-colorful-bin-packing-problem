Capicity Bin: 16000
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 1
Size: 4584 Color: 1
Size: 324 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8936 Color: 1
Size: 6328 Color: 1
Size: 736 Color: 0

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 8001 Color: 1
Size: 4092 Color: 1
Size: 1859 Color: 1
Size: 1328 Color: 0
Size: 720 Color: 0

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 8003 Color: 1
Size: 5139 Color: 1
Size: 1490 Color: 1
Size: 1096 Color: 0
Size: 272 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8002 Color: 1
Size: 6666 Color: 1
Size: 1332 Color: 0

Bin 6: 0 of cap free
Amount of items: 7
Items: 
Size: 3921 Color: 1
Size: 3894 Color: 1
Size: 2069 Color: 1
Size: 2016 Color: 0
Size: 1784 Color: 1
Size: 1328 Color: 0
Size: 988 Color: 0

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 11652 Color: 1
Size: 3300 Color: 1
Size: 696 Color: 0
Size: 352 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8008 Color: 1
Size: 6664 Color: 1
Size: 1328 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8010 Color: 1
Size: 6662 Color: 1
Size: 1328 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8040 Color: 1
Size: 6660 Color: 1
Size: 1300 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10025 Color: 1
Size: 4981 Color: 1
Size: 994 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10062 Color: 1
Size: 4938 Color: 1
Size: 1000 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 10068 Color: 1
Size: 5404 Color: 1
Size: 528 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 1
Size: 4724 Color: 1
Size: 936 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 10341 Color: 1
Size: 4717 Color: 1
Size: 942 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 10356 Color: 1
Size: 4708 Color: 1
Size: 936 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 10392 Color: 1
Size: 4550 Color: 1
Size: 1058 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 10558 Color: 1
Size: 4538 Color: 1
Size: 904 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 10605 Color: 1
Size: 4497 Color: 1
Size: 898 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 10946 Color: 1
Size: 4344 Color: 1
Size: 710 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 1
Size: 3752 Color: 1
Size: 1152 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11288 Color: 1
Size: 4164 Color: 1
Size: 548 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11534 Color: 1
Size: 3722 Color: 1
Size: 744 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11580 Color: 1
Size: 3684 Color: 1
Size: 736 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11586 Color: 1
Size: 3682 Color: 1
Size: 732 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11602 Color: 1
Size: 3666 Color: 1
Size: 732 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11624 Color: 1
Size: 4088 Color: 1
Size: 288 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11666 Color: 1
Size: 3484 Color: 1
Size: 850 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11668 Color: 1
Size: 4044 Color: 1
Size: 288 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 1
Size: 3432 Color: 1
Size: 788 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11828 Color: 1
Size: 3524 Color: 1
Size: 648 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11866 Color: 1
Size: 3318 Color: 1
Size: 816 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12022 Color: 1
Size: 3130 Color: 1
Size: 848 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12078 Color: 1
Size: 3434 Color: 1
Size: 488 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12102 Color: 1
Size: 2906 Color: 1
Size: 992 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12187 Color: 1
Size: 3179 Color: 1
Size: 634 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12230 Color: 1
Size: 3270 Color: 1
Size: 500 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12246 Color: 1
Size: 3250 Color: 1
Size: 504 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 1
Size: 3208 Color: 1
Size: 512 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12283 Color: 1
Size: 3099 Color: 1
Size: 618 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12338 Color: 1
Size: 2970 Color: 1
Size: 692 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12347 Color: 1
Size: 3045 Color: 1
Size: 608 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 1
Size: 3112 Color: 1
Size: 448 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12444 Color: 1
Size: 2964 Color: 1
Size: 592 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12514 Color: 1
Size: 3084 Color: 1
Size: 402 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12574 Color: 1
Size: 2858 Color: 1
Size: 568 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 1
Size: 2936 Color: 1
Size: 432 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12685 Color: 1
Size: 2763 Color: 1
Size: 552 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12698 Color: 1
Size: 2514 Color: 1
Size: 788 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 1
Size: 2744 Color: 1
Size: 544 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12740 Color: 1
Size: 2724 Color: 1
Size: 536 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12743 Color: 1
Size: 2633 Color: 1
Size: 624 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 1
Size: 2968 Color: 1
Size: 288 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12756 Color: 1
Size: 2708 Color: 1
Size: 536 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12788 Color: 1
Size: 2584 Color: 1
Size: 628 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12800 Color: 1
Size: 2728 Color: 1
Size: 472 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12834 Color: 1
Size: 2642 Color: 1
Size: 524 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12850 Color: 1
Size: 2722 Color: 1
Size: 428 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 12890 Color: 1
Size: 2530 Color: 1
Size: 580 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 2684 Color: 1
Size: 412 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 1
Size: 2450 Color: 1
Size: 584 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 1
Size: 2206 Color: 1
Size: 818 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 12986 Color: 1
Size: 2626 Color: 1
Size: 388 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 1
Size: 2011 Color: 1
Size: 984 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13053 Color: 1
Size: 2457 Color: 1
Size: 490 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 1
Size: 2400 Color: 1
Size: 540 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13062 Color: 1
Size: 2162 Color: 1
Size: 776 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13076 Color: 1
Size: 2444 Color: 1
Size: 480 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13101 Color: 1
Size: 2417 Color: 1
Size: 482 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13158 Color: 1
Size: 2318 Color: 1
Size: 524 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 1
Size: 2302 Color: 1
Size: 460 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13253 Color: 1
Size: 2291 Color: 1
Size: 456 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13274 Color: 1
Size: 2274 Color: 1
Size: 452 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 1
Size: 2170 Color: 1
Size: 528 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13308 Color: 1
Size: 2356 Color: 1
Size: 336 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 1
Size: 2136 Color: 1
Size: 528 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13365 Color: 1
Size: 2197 Color: 1
Size: 438 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 1
Size: 2252 Color: 1
Size: 380 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13370 Color: 1
Size: 2194 Color: 1
Size: 436 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13388 Color: 1
Size: 2180 Color: 1
Size: 432 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13398 Color: 1
Size: 1986 Color: 1
Size: 616 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13448 Color: 1
Size: 2200 Color: 1
Size: 352 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13492 Color: 1
Size: 2232 Color: 1
Size: 276 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 1
Size: 2074 Color: 1
Size: 412 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13519 Color: 1
Size: 1573 Color: 1
Size: 908 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13538 Color: 1
Size: 1942 Color: 1
Size: 520 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 1
Size: 2036 Color: 1
Size: 400 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13587 Color: 1
Size: 1741 Color: 1
Size: 672 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13624 Color: 1
Size: 1992 Color: 1
Size: 384 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 1
Size: 1877 Color: 1
Size: 476 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 1
Size: 2192 Color: 1
Size: 140 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 13670 Color: 1
Size: 2054 Color: 1
Size: 276 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 13686 Color: 1
Size: 1772 Color: 1
Size: 542 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 1
Size: 1912 Color: 1
Size: 368 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 13758 Color: 1
Size: 1870 Color: 1
Size: 372 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 13773 Color: 1
Size: 1857 Color: 1
Size: 370 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 13774 Color: 1
Size: 1858 Color: 1
Size: 368 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 13777 Color: 1
Size: 1853 Color: 1
Size: 370 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 13793 Color: 1
Size: 2155 Color: 1
Size: 52 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 1
Size: 1816 Color: 1
Size: 352 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 13876 Color: 1
Size: 1692 Color: 1
Size: 432 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 13882 Color: 1
Size: 1766 Color: 1
Size: 352 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 1
Size: 1988 Color: 1
Size: 112 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 1
Size: 1948 Color: 1
Size: 124 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 13942 Color: 1
Size: 1684 Color: 1
Size: 374 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 1
Size: 1720 Color: 1
Size: 336 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 13945 Color: 1
Size: 1713 Color: 1
Size: 342 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 13954 Color: 1
Size: 1846 Color: 1
Size: 200 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 13972 Color: 1
Size: 1736 Color: 1
Size: 292 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 13997 Color: 1
Size: 1671 Color: 1
Size: 332 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14022 Color: 1
Size: 1570 Color: 1
Size: 408 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 1
Size: 1718 Color: 1
Size: 232 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14066 Color: 1
Size: 1638 Color: 1
Size: 296 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 1
Size: 1604 Color: 1
Size: 328 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14076 Color: 1
Size: 1556 Color: 1
Size: 368 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14106 Color: 1
Size: 1582 Color: 1
Size: 312 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 1
Size: 1386 Color: 1
Size: 496 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 1
Size: 1672 Color: 1
Size: 208 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14125 Color: 1
Size: 1563 Color: 1
Size: 312 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 1
Size: 1548 Color: 1
Size: 312 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14148 Color: 1
Size: 1512 Color: 1
Size: 340 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 1
Size: 1464 Color: 1
Size: 336 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14214 Color: 1
Size: 1394 Color: 1
Size: 392 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14242 Color: 1
Size: 976 Color: 1
Size: 782 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 1
Size: 1368 Color: 1
Size: 384 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 1
Size: 1404 Color: 1
Size: 272 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14330 Color: 1
Size: 1356 Color: 1
Size: 314 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14338 Color: 1
Size: 1626 Color: 1
Size: 36 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14346 Color: 1
Size: 1382 Color: 1
Size: 272 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14358 Color: 1
Size: 1370 Color: 1
Size: 272 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 1
Size: 1280 Color: 1
Size: 360 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 1
Size: 1444 Color: 1
Size: 176 Color: 0

Bin 133: 1 of cap free
Amount of items: 3
Items: 
Size: 9076 Color: 1
Size: 6099 Color: 1
Size: 824 Color: 0

Bin 134: 1 of cap free
Amount of items: 3
Items: 
Size: 10078 Color: 1
Size: 5297 Color: 1
Size: 624 Color: 0

Bin 135: 1 of cap free
Amount of items: 3
Items: 
Size: 10825 Color: 1
Size: 4950 Color: 1
Size: 224 Color: 0

Bin 136: 1 of cap free
Amount of items: 3
Items: 
Size: 11081 Color: 1
Size: 4230 Color: 1
Size: 688 Color: 0

Bin 137: 1 of cap free
Amount of items: 3
Items: 
Size: 11250 Color: 1
Size: 4101 Color: 1
Size: 648 Color: 0

Bin 138: 1 of cap free
Amount of items: 3
Items: 
Size: 11297 Color: 1
Size: 3614 Color: 1
Size: 1088 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 11330 Color: 1
Size: 4253 Color: 1
Size: 416 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 1
Size: 2715 Color: 1
Size: 1116 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 12260 Color: 1
Size: 3251 Color: 1
Size: 488 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 1
Size: 3555 Color: 1
Size: 96 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 12611 Color: 1
Size: 2452 Color: 1
Size: 936 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 12738 Color: 1
Size: 2497 Color: 1
Size: 764 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 12847 Color: 1
Size: 2544 Color: 1
Size: 608 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 13117 Color: 1
Size: 2370 Color: 1
Size: 512 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 13300 Color: 1
Size: 2403 Color: 1
Size: 296 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 13354 Color: 1
Size: 2147 Color: 1
Size: 498 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 13749 Color: 1
Size: 1930 Color: 1
Size: 320 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 13833 Color: 1
Size: 1844 Color: 1
Size: 322 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 13909 Color: 1
Size: 1650 Color: 1
Size: 440 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 14083 Color: 1
Size: 1612 Color: 1
Size: 304 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 9410 Color: 1
Size: 5725 Color: 1
Size: 864 Color: 0

Bin 154: 2 of cap free
Amount of items: 3
Items: 
Size: 8197 Color: 1
Size: 6665 Color: 1
Size: 1136 Color: 0

Bin 155: 2 of cap free
Amount of items: 3
Items: 
Size: 9988 Color: 1
Size: 5418 Color: 1
Size: 592 Color: 0

Bin 156: 2 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 1
Size: 4652 Color: 1
Size: 416 Color: 0

Bin 157: 2 of cap free
Amount of items: 3
Items: 
Size: 11839 Color: 1
Size: 3823 Color: 1
Size: 336 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 1
Size: 2754 Color: 1
Size: 228 Color: 0

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 1
Size: 2092 Color: 1
Size: 288 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 1
Size: 1466 Color: 1
Size: 264 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 10504 Color: 1
Size: 5014 Color: 1
Size: 480 Color: 0

Bin 162: 3 of cap free
Amount of items: 3
Items: 
Size: 9432 Color: 1
Size: 6133 Color: 1
Size: 432 Color: 0

Bin 163: 3 of cap free
Amount of items: 3
Items: 
Size: 11413 Color: 1
Size: 3656 Color: 1
Size: 928 Color: 0

Bin 164: 3 of cap free
Amount of items: 3
Items: 
Size: 11735 Color: 1
Size: 3446 Color: 1
Size: 816 Color: 0

Bin 165: 3 of cap free
Amount of items: 3
Items: 
Size: 12300 Color: 1
Size: 2913 Color: 1
Size: 784 Color: 0

Bin 166: 3 of cap free
Amount of items: 3
Items: 
Size: 8185 Color: 1
Size: 6668 Color: 1
Size: 1144 Color: 0

Bin 167: 4 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 1
Size: 6708 Color: 1
Size: 272 Color: 0

Bin 168: 4 of cap free
Amount of items: 3
Items: 
Size: 9972 Color: 1
Size: 5336 Color: 1
Size: 688 Color: 0

Bin 169: 4 of cap free
Amount of items: 3
Items: 
Size: 10420 Color: 1
Size: 5048 Color: 1
Size: 528 Color: 0

Bin 170: 4 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 1
Size: 5092 Color: 1
Size: 112 Color: 0

Bin 171: 4 of cap free
Amount of items: 3
Items: 
Size: 11896 Color: 1
Size: 3764 Color: 1
Size: 336 Color: 0

Bin 172: 5 of cap free
Amount of items: 3
Items: 
Size: 13714 Color: 1
Size: 1961 Color: 1
Size: 320 Color: 0

Bin 173: 5 of cap free
Amount of items: 3
Items: 
Size: 8830 Color: 1
Size: 6513 Color: 1
Size: 652 Color: 0

Bin 174: 5 of cap free
Amount of items: 3
Items: 
Size: 10897 Color: 1
Size: 3946 Color: 1
Size: 1152 Color: 0

Bin 175: 5 of cap free
Amount of items: 3
Items: 
Size: 12101 Color: 1
Size: 3142 Color: 1
Size: 752 Color: 0

Bin 176: 6 of cap free
Amount of items: 3
Items: 
Size: 9164 Color: 1
Size: 6070 Color: 1
Size: 760 Color: 0

Bin 177: 6 of cap free
Amount of items: 3
Items: 
Size: 9502 Color: 1
Size: 5832 Color: 1
Size: 660 Color: 0

Bin 178: 6 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 1
Size: 5494 Color: 1
Size: 540 Color: 0

Bin 179: 6 of cap free
Amount of items: 3
Items: 
Size: 11266 Color: 1
Size: 3928 Color: 1
Size: 800 Color: 0

Bin 180: 7 of cap free
Amount of items: 3
Items: 
Size: 9141 Color: 1
Size: 5772 Color: 1
Size: 1080 Color: 0

Bin 181: 7 of cap free
Amount of items: 3
Items: 
Size: 9545 Color: 1
Size: 5968 Color: 1
Size: 480 Color: 0

Bin 182: 9 of cap free
Amount of items: 3
Items: 
Size: 13788 Color: 1
Size: 1807 Color: 1
Size: 396 Color: 0

Bin 183: 9 of cap free
Amount of items: 3
Items: 
Size: 13425 Color: 1
Size: 1302 Color: 0
Size: 1264 Color: 0

Bin 184: 10 of cap free
Amount of items: 3
Items: 
Size: 9986 Color: 1
Size: 5700 Color: 1
Size: 304 Color: 0

Bin 185: 12 of cap free
Amount of items: 3
Items: 
Size: 8012 Color: 1
Size: 6976 Color: 1
Size: 1000 Color: 0

Bin 186: 14 of cap free
Amount of items: 3
Items: 
Size: 10542 Color: 1
Size: 5012 Color: 1
Size: 432 Color: 0

Bin 187: 18 of cap free
Amount of items: 3
Items: 
Size: 11036 Color: 1
Size: 4226 Color: 1
Size: 720 Color: 0

Bin 188: 24 of cap free
Amount of items: 3
Items: 
Size: 7936 Color: 1
Size: 7656 Color: 1
Size: 384 Color: 0

Bin 189: 26 of cap free
Amount of items: 3
Items: 
Size: 9084 Color: 1
Size: 5978 Color: 1
Size: 912 Color: 0

Bin 190: 29 of cap free
Amount of items: 3
Items: 
Size: 9131 Color: 1
Size: 6536 Color: 1
Size: 304 Color: 0

Bin 191: 31 of cap free
Amount of items: 5
Items: 
Size: 8408 Color: 1
Size: 3469 Color: 1
Size: 2488 Color: 1
Size: 1332 Color: 0
Size: 272 Color: 0

Bin 192: 31 of cap free
Amount of items: 3
Items: 
Size: 9645 Color: 1
Size: 5480 Color: 1
Size: 844 Color: 0

Bin 193: 41 of cap free
Amount of items: 4
Items: 
Size: 8004 Color: 1
Size: 7459 Color: 1
Size: 304 Color: 0
Size: 192 Color: 0

Bin 194: 68 of cap free
Amount of items: 3
Items: 
Size: 11512 Color: 1
Size: 3228 Color: 1
Size: 1192 Color: 0

Bin 195: 168 of cap free
Amount of items: 3
Items: 
Size: 8814 Color: 1
Size: 3962 Color: 1
Size: 3056 Color: 0

Bin 196: 1887 of cap free
Amount of items: 1
Items: 
Size: 14113 Color: 1

Bin 197: 1976 of cap free
Amount of items: 1
Items: 
Size: 14024 Color: 1

Bin 198: 2020 of cap free
Amount of items: 1
Items: 
Size: 13980 Color: 1

Bin 199: 9497 of cap free
Amount of items: 1
Items: 
Size: 6503 Color: 1

Total size: 3168000
Total free space: 16000

