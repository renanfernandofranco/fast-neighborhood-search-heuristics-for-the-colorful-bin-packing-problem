Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 358 Color: 4
Size: 251 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 3
Size: 250 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 309 Color: 3
Size: 288 Color: 1
Size: 403 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 4
Size: 253 Color: 1
Size: 252 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 2
Size: 263 Color: 2
Size: 258 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 259 Color: 3
Size: 257 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 273 Color: 2
Size: 255 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 2
Size: 310 Color: 3
Size: 260 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 299 Color: 3
Size: 275 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 4
Size: 279 Color: 3
Size: 257 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 339 Color: 3
Size: 264 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 3
Size: 326 Color: 1
Size: 259 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 0
Size: 342 Color: 2
Size: 304 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 344 Color: 4
Size: 278 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 2
Size: 303 Color: 3
Size: 252 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 305 Color: 1
Size: 263 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 4
Size: 327 Color: 1
Size: 256 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 3
Size: 302 Color: 1
Size: 275 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 2
Size: 343 Color: 4
Size: 260 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 4
Size: 327 Color: 4
Size: 308 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 280 Color: 3
Size: 260 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 2
Size: 251 Color: 2
Size: 251 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 356 Color: 1
Size: 272 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 330 Color: 0
Size: 256 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 290 Color: 3
Size: 282 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 345 Color: 3
Size: 283 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 4
Size: 332 Color: 3
Size: 329 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 335 Color: 3
Size: 274 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 4
Size: 347 Color: 0
Size: 266 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 2
Size: 329 Color: 1
Size: 264 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 293 Color: 2
Size: 284 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 361 Color: 4
Size: 260 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 3
Size: 326 Color: 4
Size: 325 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 352 Color: 4
Size: 269 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 2
Size: 281 Color: 3
Size: 250 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 295 Color: 3
Size: 299 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 3
Size: 260 Color: 3
Size: 257 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 350 Color: 0
Size: 266 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 314 Color: 0
Size: 311 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 3
Size: 285 Color: 4
Size: 275 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 276 Color: 0
Size: 268 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 287 Color: 4
Size: 264 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 360 Color: 4
Size: 256 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 351 Color: 4
Size: 269 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 280 Color: 4
Size: 272 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 361 Color: 3
Size: 263 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 328 Color: 4
Size: 291 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 2
Size: 282 Color: 0
Size: 254 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 0
Size: 279 Color: 3
Size: 268 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 355 Color: 0
Size: 262 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 306 Color: 3
Size: 252 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 366 Color: 1
Size: 256 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 324 Color: 4
Size: 301 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 261 Color: 0
Size: 255 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 0
Size: 294 Color: 4
Size: 279 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 358 Color: 3
Size: 267 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 339 Color: 3
Size: 285 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 3
Size: 305 Color: 4
Size: 280 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 2
Size: 308 Color: 0
Size: 270 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 314 Color: 2
Size: 273 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 4
Size: 262 Color: 0
Size: 251 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 3
Size: 352 Color: 0
Size: 293 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 350 Color: 2
Size: 257 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 276 Color: 2
Size: 275 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 271 Color: 2
Size: 266 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 259 Color: 2
Size: 256 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 331 Color: 3
Size: 290 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 309 Color: 4
Size: 289 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 367 Color: 2
Size: 259 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 0
Size: 321 Color: 1
Size: 254 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 2
Size: 254 Color: 2
Size: 251 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 3
Size: 303 Color: 2
Size: 260 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 0
Size: 264 Color: 3
Size: 260 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 314 Color: 0
Size: 308 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 2
Size: 311 Color: 3
Size: 266 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 2
Size: 332 Color: 0
Size: 318 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 3
Size: 300 Color: 0
Size: 267 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 297 Color: 2
Size: 293 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 3
Size: 356 Color: 2
Size: 281 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 2
Size: 302 Color: 1
Size: 274 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 297 Color: 2
Size: 284 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 4
Size: 287 Color: 4
Size: 283 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 358 Color: 0
Size: 260 Color: 3

Total size: 83000
Total free space: 0

