Capicity Bin: 1000001
Lower Bound: 43

Bins used: 45
Amount of Colors: 5

Bin 1: 338 of cap free
Amount of items: 2
Items: 
Size: 740749 Color: 3
Size: 258914 Color: 2

Bin 2: 383 of cap free
Amount of items: 3
Items: 
Size: 595170 Color: 4
Size: 249463 Color: 1
Size: 154985 Color: 0

Bin 3: 736 of cap free
Amount of items: 2
Items: 
Size: 615157 Color: 3
Size: 384108 Color: 1

Bin 4: 871 of cap free
Amount of items: 3
Items: 
Size: 560009 Color: 0
Size: 268047 Color: 1
Size: 171074 Color: 2

Bin 5: 1253 of cap free
Amount of items: 2
Items: 
Size: 751346 Color: 3
Size: 247402 Color: 1

Bin 6: 1315 of cap free
Amount of items: 3
Items: 
Size: 763222 Color: 4
Size: 129468 Color: 3
Size: 105996 Color: 1

Bin 7: 1567 of cap free
Amount of items: 3
Items: 
Size: 653613 Color: 1
Size: 172894 Color: 0
Size: 171927 Color: 4

Bin 8: 1757 of cap free
Amount of items: 3
Items: 
Size: 729941 Color: 1
Size: 167604 Color: 3
Size: 100699 Color: 4

Bin 9: 1764 of cap free
Amount of items: 3
Items: 
Size: 650858 Color: 4
Size: 182129 Color: 2
Size: 165250 Color: 1

Bin 10: 2662 of cap free
Amount of items: 2
Items: 
Size: 788908 Color: 3
Size: 208431 Color: 0

Bin 11: 3059 of cap free
Amount of items: 2
Items: 
Size: 759236 Color: 2
Size: 237706 Color: 3

Bin 12: 3616 of cap free
Amount of items: 3
Items: 
Size: 533057 Color: 2
Size: 256535 Color: 0
Size: 206793 Color: 2

Bin 13: 3774 of cap free
Amount of items: 3
Items: 
Size: 638825 Color: 3
Size: 190131 Color: 1
Size: 167271 Color: 3

Bin 14: 3858 of cap free
Amount of items: 2
Items: 
Size: 641027 Color: 3
Size: 355116 Color: 0

Bin 15: 6188 of cap free
Amount of items: 3
Items: 
Size: 709936 Color: 1
Size: 183229 Color: 3
Size: 100648 Color: 0

Bin 16: 6335 of cap free
Amount of items: 3
Items: 
Size: 695856 Color: 1
Size: 155621 Color: 3
Size: 142189 Color: 2

Bin 17: 7469 of cap free
Amount of items: 2
Items: 
Size: 564623 Color: 2
Size: 427909 Color: 4

Bin 18: 8678 of cap free
Amount of items: 2
Items: 
Size: 734676 Color: 1
Size: 256647 Color: 0

Bin 19: 9010 of cap free
Amount of items: 2
Items: 
Size: 776140 Color: 4
Size: 214851 Color: 0

Bin 20: 9643 of cap free
Amount of items: 3
Items: 
Size: 507674 Color: 1
Size: 266989 Color: 4
Size: 215695 Color: 3

Bin 21: 10679 of cap free
Amount of items: 2
Items: 
Size: 764251 Color: 2
Size: 225071 Color: 3

Bin 22: 11705 of cap free
Amount of items: 3
Items: 
Size: 633572 Color: 4
Size: 214306 Color: 1
Size: 140418 Color: 0

Bin 23: 13669 of cap free
Amount of items: 2
Items: 
Size: 597173 Color: 3
Size: 389159 Color: 2

Bin 24: 14069 of cap free
Amount of items: 2
Items: 
Size: 591588 Color: 2
Size: 394344 Color: 3

Bin 25: 16880 of cap free
Amount of items: 2
Items: 
Size: 787021 Color: 1
Size: 196100 Color: 3

Bin 26: 16940 of cap free
Amount of items: 2
Items: 
Size: 557059 Color: 2
Size: 426002 Color: 0

Bin 27: 17388 of cap free
Amount of items: 2
Items: 
Size: 656394 Color: 3
Size: 326219 Color: 2

Bin 28: 21848 of cap free
Amount of items: 2
Items: 
Size: 506714 Color: 0
Size: 471439 Color: 1

Bin 29: 23385 of cap free
Amount of items: 2
Items: 
Size: 496019 Color: 3
Size: 480597 Color: 2

Bin 30: 26910 of cap free
Amount of items: 2
Items: 
Size: 569972 Color: 2
Size: 403119 Color: 1

Bin 31: 28696 of cap free
Amount of items: 2
Items: 
Size: 576460 Color: 0
Size: 394845 Color: 1

Bin 32: 40858 of cap free
Amount of items: 2
Items: 
Size: 622406 Color: 0
Size: 336737 Color: 3

Bin 33: 43227 of cap free
Amount of items: 2
Items: 
Size: 542318 Color: 2
Size: 414456 Color: 3

Bin 34: 53104 of cap free
Amount of items: 2
Items: 
Size: 559853 Color: 3
Size: 387044 Color: 2

Bin 35: 53946 of cap free
Amount of items: 2
Items: 
Size: 610485 Color: 0
Size: 335570 Color: 1

Bin 36: 106199 of cap free
Amount of items: 2
Items: 
Size: 572973 Color: 0
Size: 320829 Color: 1

Bin 37: 143216 of cap free
Amount of items: 2
Items: 
Size: 543668 Color: 4
Size: 313117 Color: 1

Bin 38: 151328 of cap free
Amount of items: 2
Items: 
Size: 529778 Color: 3
Size: 318895 Color: 2

Bin 39: 155145 of cap free
Amount of items: 2
Items: 
Size: 542836 Color: 3
Size: 302020 Color: 1

Bin 40: 166087 of cap free
Amount of items: 2
Items: 
Size: 534684 Color: 0
Size: 299230 Color: 1

Bin 41: 180707 of cap free
Amount of items: 2
Items: 
Size: 513410 Color: 4
Size: 305884 Color: 2

Bin 42: 184985 of cap free
Amount of items: 2
Items: 
Size: 532094 Color: 2
Size: 282922 Color: 1

Bin 43: 206776 of cap free
Amount of items: 2
Items: 
Size: 493826 Color: 1
Size: 299399 Color: 3

Bin 44: 236659 of cap free
Amount of items: 2
Items: 
Size: 482050 Color: 1
Size: 281292 Color: 3

Bin 45: 455919 of cap free
Amount of items: 2
Items: 
Size: 288302 Color: 2
Size: 255780 Color: 3

Total size: 42545444
Total free space: 2454601

