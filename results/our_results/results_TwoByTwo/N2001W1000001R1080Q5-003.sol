Capicity Bin: 1000001
Lower Bound: 897

Bins used: 902
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 583302 Color: 0
Size: 416699 Color: 3

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 584735 Color: 1
Size: 415266 Color: 4

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 702073 Color: 2
Size: 297928 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 719956 Color: 0
Size: 141873 Color: 3
Size: 138172 Color: 1

Bin 5: 1 of cap free
Amount of items: 2
Items: 
Size: 685790 Color: 0
Size: 314210 Color: 4

Bin 6: 1 of cap free
Amount of items: 3
Items: 
Size: 730273 Color: 3
Size: 136344 Color: 0
Size: 133383 Color: 4

Bin 7: 2 of cap free
Amount of items: 3
Items: 
Size: 442732 Color: 4
Size: 280720 Color: 4
Size: 276547 Color: 1

Bin 8: 2 of cap free
Amount of items: 3
Items: 
Size: 633002 Color: 0
Size: 187822 Color: 1
Size: 179175 Color: 4

Bin 9: 3 of cap free
Amount of items: 3
Items: 
Size: 630654 Color: 1
Size: 190288 Color: 2
Size: 179056 Color: 2

Bin 10: 4 of cap free
Amount of items: 2
Items: 
Size: 618885 Color: 0
Size: 381112 Color: 2

Bin 11: 4 of cap free
Amount of items: 2
Items: 
Size: 690985 Color: 0
Size: 309012 Color: 2

Bin 12: 5 of cap free
Amount of items: 2
Items: 
Size: 554086 Color: 3
Size: 445910 Color: 0

Bin 13: 5 of cap free
Amount of items: 2
Items: 
Size: 582371 Color: 0
Size: 417625 Color: 1

Bin 14: 5 of cap free
Amount of items: 2
Items: 
Size: 666520 Color: 0
Size: 333476 Color: 3

Bin 15: 5 of cap free
Amount of items: 3
Items: 
Size: 789360 Color: 3
Size: 106291 Color: 2
Size: 104345 Color: 1

Bin 16: 6 of cap free
Amount of items: 2
Items: 
Size: 546328 Color: 0
Size: 453667 Color: 3

Bin 17: 6 of cap free
Amount of items: 3
Items: 
Size: 652738 Color: 3
Size: 175775 Color: 1
Size: 171482 Color: 4

Bin 18: 6 of cap free
Amount of items: 2
Items: 
Size: 787001 Color: 1
Size: 212994 Color: 0

Bin 19: 8 of cap free
Amount of items: 2
Items: 
Size: 557598 Color: 1
Size: 442395 Color: 0

Bin 20: 8 of cap free
Amount of items: 3
Items: 
Size: 735495 Color: 3
Size: 133497 Color: 4
Size: 131001 Color: 1

Bin 21: 9 of cap free
Amount of items: 3
Items: 
Size: 375050 Color: 0
Size: 339363 Color: 2
Size: 285579 Color: 4

Bin 22: 9 of cap free
Amount of items: 2
Items: 
Size: 682853 Color: 3
Size: 317139 Color: 2

Bin 23: 9 of cap free
Amount of items: 2
Items: 
Size: 777560 Color: 3
Size: 222432 Color: 1

Bin 24: 10 of cap free
Amount of items: 3
Items: 
Size: 452458 Color: 4
Size: 286485 Color: 3
Size: 261048 Color: 1

Bin 25: 10 of cap free
Amount of items: 2
Items: 
Size: 554423 Color: 2
Size: 445568 Color: 1

Bin 26: 10 of cap free
Amount of items: 2
Items: 
Size: 629948 Color: 4
Size: 370043 Color: 0

Bin 27: 10 of cap free
Amount of items: 3
Items: 
Size: 649720 Color: 3
Size: 177094 Color: 2
Size: 173177 Color: 2

Bin 28: 10 of cap free
Amount of items: 2
Items: 
Size: 791910 Color: 1
Size: 208081 Color: 0

Bin 29: 12 of cap free
Amount of items: 3
Items: 
Size: 627169 Color: 1
Size: 186484 Color: 2
Size: 186336 Color: 4

Bin 30: 12 of cap free
Amount of items: 2
Items: 
Size: 734455 Color: 1
Size: 265534 Color: 4

Bin 31: 12 of cap free
Amount of items: 3
Items: 
Size: 737731 Color: 4
Size: 132007 Color: 2
Size: 130251 Color: 3

Bin 32: 13 of cap free
Amount of items: 3
Items: 
Size: 672099 Color: 3
Size: 165804 Color: 1
Size: 162085 Color: 1

Bin 33: 14 of cap free
Amount of items: 2
Items: 
Size: 554927 Color: 4
Size: 445060 Color: 3

Bin 34: 15 of cap free
Amount of items: 2
Items: 
Size: 526184 Color: 0
Size: 473802 Color: 2

Bin 35: 15 of cap free
Amount of items: 3
Items: 
Size: 673074 Color: 3
Size: 166544 Color: 0
Size: 160368 Color: 4

Bin 36: 15 of cap free
Amount of items: 2
Items: 
Size: 735477 Color: 1
Size: 264509 Color: 2

Bin 37: 16 of cap free
Amount of items: 2
Items: 
Size: 619539 Color: 1
Size: 380446 Color: 2

Bin 38: 16 of cap free
Amount of items: 2
Items: 
Size: 779113 Color: 3
Size: 220872 Color: 0

Bin 39: 16 of cap free
Amount of items: 3
Items: 
Size: 786460 Color: 3
Size: 109190 Color: 4
Size: 104335 Color: 1

Bin 40: 17 of cap free
Amount of items: 3
Items: 
Size: 670849 Color: 3
Size: 168145 Color: 1
Size: 160990 Color: 2

Bin 41: 19 of cap free
Amount of items: 2
Items: 
Size: 675941 Color: 1
Size: 324041 Color: 0

Bin 42: 20 of cap free
Amount of items: 2
Items: 
Size: 555275 Color: 4
Size: 444706 Color: 0

Bin 43: 20 of cap free
Amount of items: 2
Items: 
Size: 676240 Color: 0
Size: 323741 Color: 3

Bin 44: 20 of cap free
Amount of items: 2
Items: 
Size: 679638 Color: 1
Size: 320343 Color: 0

Bin 45: 21 of cap free
Amount of items: 2
Items: 
Size: 601436 Color: 3
Size: 398544 Color: 2

Bin 46: 21 of cap free
Amount of items: 2
Items: 
Size: 783257 Color: 4
Size: 216723 Color: 2

Bin 47: 23 of cap free
Amount of items: 2
Items: 
Size: 795244 Color: 4
Size: 204734 Color: 0

Bin 48: 24 of cap free
Amount of items: 2
Items: 
Size: 516629 Color: 1
Size: 483348 Color: 4

Bin 49: 25 of cap free
Amount of items: 2
Items: 
Size: 780450 Color: 4
Size: 219526 Color: 0

Bin 50: 26 of cap free
Amount of items: 3
Items: 
Size: 615639 Color: 1
Size: 193274 Color: 0
Size: 191062 Color: 0

Bin 51: 27 of cap free
Amount of items: 3
Items: 
Size: 595777 Color: 2
Size: 209938 Color: 1
Size: 194259 Color: 0

Bin 52: 30 of cap free
Amount of items: 2
Items: 
Size: 734302 Color: 0
Size: 265669 Color: 1

Bin 53: 32 of cap free
Amount of items: 2
Items: 
Size: 674065 Color: 2
Size: 325904 Color: 4

Bin 54: 32 of cap free
Amount of items: 2
Items: 
Size: 781642 Color: 0
Size: 218327 Color: 4

Bin 55: 33 of cap free
Amount of items: 2
Items: 
Size: 678743 Color: 4
Size: 321225 Color: 0

Bin 56: 33 of cap free
Amount of items: 3
Items: 
Size: 787058 Color: 3
Size: 109238 Color: 2
Size: 103672 Color: 2

Bin 57: 34 of cap free
Amount of items: 2
Items: 
Size: 553589 Color: 2
Size: 446378 Color: 4

Bin 58: 34 of cap free
Amount of items: 2
Items: 
Size: 647515 Color: 0
Size: 352452 Color: 4

Bin 59: 34 of cap free
Amount of items: 3
Items: 
Size: 731663 Color: 3
Size: 134914 Color: 1
Size: 133390 Color: 1

Bin 60: 35 of cap free
Amount of items: 3
Items: 
Size: 535304 Color: 0
Size: 240513 Color: 1
Size: 224149 Color: 3

Bin 61: 35 of cap free
Amount of items: 2
Items: 
Size: 544419 Color: 1
Size: 455547 Color: 2

Bin 62: 35 of cap free
Amount of items: 2
Items: 
Size: 591248 Color: 1
Size: 408718 Color: 0

Bin 63: 37 of cap free
Amount of items: 3
Items: 
Size: 618617 Color: 2
Size: 191639 Color: 3
Size: 189708 Color: 4

Bin 64: 37 of cap free
Amount of items: 2
Items: 
Size: 635199 Color: 2
Size: 364765 Color: 1

Bin 65: 40 of cap free
Amount of items: 2
Items: 
Size: 520286 Color: 4
Size: 479675 Color: 3

Bin 66: 40 of cap free
Amount of items: 3
Items: 
Size: 531058 Color: 2
Size: 239412 Color: 0
Size: 229491 Color: 3

Bin 67: 42 of cap free
Amount of items: 3
Items: 
Size: 754298 Color: 3
Size: 124809 Color: 1
Size: 120852 Color: 1

Bin 68: 43 of cap free
Amount of items: 2
Items: 
Size: 789090 Color: 0
Size: 210868 Color: 2

Bin 69: 44 of cap free
Amount of items: 2
Items: 
Size: 684623 Color: 3
Size: 315334 Color: 4

Bin 70: 45 of cap free
Amount of items: 2
Items: 
Size: 603558 Color: 3
Size: 396398 Color: 0

Bin 71: 45 of cap free
Amount of items: 3
Items: 
Size: 689749 Color: 3
Size: 155572 Color: 4
Size: 154635 Color: 1

Bin 72: 46 of cap free
Amount of items: 2
Items: 
Size: 772459 Color: 1
Size: 227496 Color: 4

Bin 73: 49 of cap free
Amount of items: 3
Items: 
Size: 391105 Color: 3
Size: 318062 Color: 2
Size: 290785 Color: 0

Bin 74: 49 of cap free
Amount of items: 2
Items: 
Size: 644561 Color: 2
Size: 355391 Color: 3

Bin 75: 49 of cap free
Amount of items: 3
Items: 
Size: 785580 Color: 4
Size: 107696 Color: 3
Size: 106676 Color: 1

Bin 76: 51 of cap free
Amount of items: 2
Items: 
Size: 642322 Color: 2
Size: 357628 Color: 3

Bin 77: 51 of cap free
Amount of items: 2
Items: 
Size: 732877 Color: 4
Size: 267073 Color: 3

Bin 78: 52 of cap free
Amount of items: 2
Items: 
Size: 635593 Color: 3
Size: 364356 Color: 2

Bin 79: 52 of cap free
Amount of items: 2
Items: 
Size: 736246 Color: 0
Size: 263703 Color: 3

Bin 80: 52 of cap free
Amount of items: 3
Items: 
Size: 798029 Color: 1
Size: 101119 Color: 3
Size: 100801 Color: 2

Bin 81: 53 of cap free
Amount of items: 3
Items: 
Size: 787761 Color: 2
Size: 107975 Color: 3
Size: 104212 Color: 2

Bin 82: 55 of cap free
Amount of items: 2
Items: 
Size: 609658 Color: 2
Size: 390288 Color: 1

Bin 83: 55 of cap free
Amount of items: 3
Items: 
Size: 729797 Color: 2
Size: 139680 Color: 4
Size: 130469 Color: 1

Bin 84: 56 of cap free
Amount of items: 2
Items: 
Size: 521523 Color: 1
Size: 478422 Color: 4

Bin 85: 56 of cap free
Amount of items: 2
Items: 
Size: 539872 Color: 4
Size: 460073 Color: 0

Bin 86: 56 of cap free
Amount of items: 2
Items: 
Size: 695154 Color: 1
Size: 304791 Color: 0

Bin 87: 56 of cap free
Amount of items: 2
Items: 
Size: 749163 Color: 0
Size: 250782 Color: 1

Bin 88: 57 of cap free
Amount of items: 2
Items: 
Size: 725920 Color: 0
Size: 274024 Color: 1

Bin 89: 58 of cap free
Amount of items: 3
Items: 
Size: 779337 Color: 3
Size: 110309 Color: 4
Size: 110297 Color: 1

Bin 90: 60 of cap free
Amount of items: 2
Items: 
Size: 594551 Color: 3
Size: 405390 Color: 4

Bin 91: 60 of cap free
Amount of items: 2
Items: 
Size: 693297 Color: 2
Size: 306644 Color: 0

Bin 92: 61 of cap free
Amount of items: 3
Items: 
Size: 752592 Color: 3
Size: 124534 Color: 0
Size: 122814 Color: 0

Bin 93: 62 of cap free
Amount of items: 2
Items: 
Size: 596090 Color: 3
Size: 403849 Color: 0

Bin 94: 62 of cap free
Amount of items: 3
Items: 
Size: 620998 Color: 3
Size: 198169 Color: 1
Size: 180772 Color: 4

Bin 95: 63 of cap free
Amount of items: 2
Items: 
Size: 550954 Color: 0
Size: 448984 Color: 4

Bin 96: 65 of cap free
Amount of items: 3
Items: 
Size: 598587 Color: 1
Size: 207178 Color: 3
Size: 194171 Color: 2

Bin 97: 65 of cap free
Amount of items: 2
Items: 
Size: 701706 Color: 4
Size: 298230 Color: 1

Bin 98: 65 of cap free
Amount of items: 3
Items: 
Size: 766159 Color: 3
Size: 119133 Color: 1
Size: 114644 Color: 0

Bin 99: 66 of cap free
Amount of items: 3
Items: 
Size: 673727 Color: 3
Size: 164101 Color: 0
Size: 162107 Color: 4

Bin 100: 67 of cap free
Amount of items: 2
Items: 
Size: 510958 Color: 1
Size: 488976 Color: 4

Bin 101: 67 of cap free
Amount of items: 2
Items: 
Size: 567093 Color: 2
Size: 432841 Color: 1

Bin 102: 68 of cap free
Amount of items: 2
Items: 
Size: 570153 Color: 4
Size: 429780 Color: 1

Bin 103: 69 of cap free
Amount of items: 2
Items: 
Size: 512706 Color: 2
Size: 487226 Color: 0

Bin 104: 69 of cap free
Amount of items: 2
Items: 
Size: 590261 Color: 0
Size: 409671 Color: 1

Bin 105: 69 of cap free
Amount of items: 3
Items: 
Size: 757737 Color: 3
Size: 122701 Color: 0
Size: 119494 Color: 2

Bin 106: 70 of cap free
Amount of items: 2
Items: 
Size: 573704 Color: 0
Size: 426227 Color: 2

Bin 107: 70 of cap free
Amount of items: 2
Items: 
Size: 750164 Color: 1
Size: 249767 Color: 2

Bin 108: 71 of cap free
Amount of items: 2
Items: 
Size: 634451 Color: 3
Size: 365479 Color: 4

Bin 109: 71 of cap free
Amount of items: 2
Items: 
Size: 774715 Color: 1
Size: 225215 Color: 2

Bin 110: 72 of cap free
Amount of items: 2
Items: 
Size: 686347 Color: 3
Size: 313582 Color: 2

Bin 111: 72 of cap free
Amount of items: 3
Items: 
Size: 764869 Color: 3
Size: 118072 Color: 2
Size: 116988 Color: 2

Bin 112: 74 of cap free
Amount of items: 2
Items: 
Size: 505798 Color: 4
Size: 494129 Color: 0

Bin 113: 74 of cap free
Amount of items: 2
Items: 
Size: 660164 Color: 2
Size: 339763 Color: 4

Bin 114: 74 of cap free
Amount of items: 3
Items: 
Size: 674731 Color: 3
Size: 163581 Color: 1
Size: 161615 Color: 4

Bin 115: 74 of cap free
Amount of items: 2
Items: 
Size: 769880 Color: 3
Size: 230047 Color: 4

Bin 116: 75 of cap free
Amount of items: 2
Items: 
Size: 535155 Color: 3
Size: 464771 Color: 4

Bin 117: 76 of cap free
Amount of items: 2
Items: 
Size: 548148 Color: 2
Size: 451777 Color: 0

Bin 118: 77 of cap free
Amount of items: 2
Items: 
Size: 723046 Color: 2
Size: 276878 Color: 3

Bin 119: 78 of cap free
Amount of items: 2
Items: 
Size: 593531 Color: 4
Size: 406392 Color: 2

Bin 120: 78 of cap free
Amount of items: 3
Items: 
Size: 631055 Color: 3
Size: 188860 Color: 1
Size: 180008 Color: 4

Bin 121: 78 of cap free
Amount of items: 2
Items: 
Size: 745695 Color: 1
Size: 254228 Color: 4

Bin 122: 78 of cap free
Amount of items: 3
Items: 
Size: 794477 Color: 3
Size: 104247 Color: 4
Size: 101199 Color: 4

Bin 123: 79 of cap free
Amount of items: 3
Items: 
Size: 361091 Color: 1
Size: 352038 Color: 2
Size: 286793 Color: 0

Bin 124: 80 of cap free
Amount of items: 2
Items: 
Size: 582225 Color: 2
Size: 417696 Color: 0

Bin 125: 80 of cap free
Amount of items: 2
Items: 
Size: 581305 Color: 1
Size: 418616 Color: 4

Bin 126: 80 of cap free
Amount of items: 2
Items: 
Size: 676717 Color: 1
Size: 323204 Color: 3

Bin 127: 81 of cap free
Amount of items: 3
Items: 
Size: 603234 Color: 1
Size: 206800 Color: 3
Size: 189886 Color: 4

Bin 128: 82 of cap free
Amount of items: 2
Items: 
Size: 635538 Color: 2
Size: 364381 Color: 1

Bin 129: 83 of cap free
Amount of items: 2
Items: 
Size: 501745 Color: 2
Size: 498173 Color: 4

Bin 130: 84 of cap free
Amount of items: 2
Items: 
Size: 512792 Color: 2
Size: 487125 Color: 4

Bin 131: 85 of cap free
Amount of items: 2
Items: 
Size: 590111 Color: 2
Size: 409805 Color: 3

Bin 132: 86 of cap free
Amount of items: 2
Items: 
Size: 702596 Color: 3
Size: 297319 Color: 4

Bin 133: 87 of cap free
Amount of items: 2
Items: 
Size: 630783 Color: 3
Size: 369131 Color: 1

Bin 134: 87 of cap free
Amount of items: 3
Items: 
Size: 730688 Color: 1
Size: 137575 Color: 3
Size: 131651 Color: 0

Bin 135: 88 of cap free
Amount of items: 2
Items: 
Size: 618211 Color: 0
Size: 381702 Color: 1

Bin 136: 89 of cap free
Amount of items: 2
Items: 
Size: 757892 Color: 0
Size: 242020 Color: 4

Bin 137: 92 of cap free
Amount of items: 2
Items: 
Size: 636065 Color: 2
Size: 363844 Color: 4

Bin 138: 93 of cap free
Amount of items: 2
Items: 
Size: 639301 Color: 2
Size: 360607 Color: 3

Bin 139: 93 of cap free
Amount of items: 3
Items: 
Size: 656258 Color: 3
Size: 172405 Color: 1
Size: 171245 Color: 2

Bin 140: 94 of cap free
Amount of items: 2
Items: 
Size: 574792 Color: 4
Size: 425115 Color: 3

Bin 141: 94 of cap free
Amount of items: 3
Items: 
Size: 625403 Color: 2
Size: 191962 Color: 1
Size: 182542 Color: 0

Bin 142: 94 of cap free
Amount of items: 3
Items: 
Size: 664325 Color: 2
Size: 168687 Color: 3
Size: 166895 Color: 1

Bin 143: 94 of cap free
Amount of items: 3
Items: 
Size: 743574 Color: 3
Size: 128918 Color: 0
Size: 127415 Color: 4

Bin 144: 96 of cap free
Amount of items: 3
Items: 
Size: 455786 Color: 4
Size: 286551 Color: 1
Size: 257568 Color: 0

Bin 145: 96 of cap free
Amount of items: 2
Items: 
Size: 530419 Color: 0
Size: 469486 Color: 2

Bin 146: 97 of cap free
Amount of items: 2
Items: 
Size: 609210 Color: 3
Size: 390694 Color: 4

Bin 147: 97 of cap free
Amount of items: 2
Items: 
Size: 614596 Color: 0
Size: 385308 Color: 3

Bin 148: 97 of cap free
Amount of items: 3
Items: 
Size: 705833 Color: 3
Size: 150238 Color: 0
Size: 143833 Color: 0

Bin 149: 98 of cap free
Amount of items: 2
Items: 
Size: 564905 Color: 2
Size: 434998 Color: 4

Bin 150: 98 of cap free
Amount of items: 2
Items: 
Size: 586204 Color: 0
Size: 413699 Color: 4

Bin 151: 99 of cap free
Amount of items: 2
Items: 
Size: 637212 Color: 3
Size: 362690 Color: 4

Bin 152: 100 of cap free
Amount of items: 2
Items: 
Size: 586808 Color: 4
Size: 413093 Color: 3

Bin 153: 100 of cap free
Amount of items: 3
Items: 
Size: 643215 Color: 3
Size: 178844 Color: 1
Size: 177842 Color: 2

Bin 154: 100 of cap free
Amount of items: 2
Items: 
Size: 772416 Color: 0
Size: 227485 Color: 1

Bin 155: 102 of cap free
Amount of items: 2
Items: 
Size: 687783 Color: 0
Size: 312116 Color: 2

Bin 156: 102 of cap free
Amount of items: 2
Items: 
Size: 770097 Color: 2
Size: 229802 Color: 1

Bin 157: 105 of cap free
Amount of items: 2
Items: 
Size: 543389 Color: 3
Size: 456507 Color: 2

Bin 158: 105 of cap free
Amount of items: 2
Items: 
Size: 755084 Color: 4
Size: 244812 Color: 0

Bin 159: 105 of cap free
Amount of items: 3
Items: 
Size: 781727 Color: 1
Size: 110829 Color: 0
Size: 107340 Color: 3

Bin 160: 106 of cap free
Amount of items: 3
Items: 
Size: 359382 Color: 4
Size: 351092 Color: 0
Size: 289421 Color: 1

Bin 161: 106 of cap free
Amount of items: 2
Items: 
Size: 713163 Color: 0
Size: 286732 Color: 1

Bin 162: 107 of cap free
Amount of items: 2
Items: 
Size: 762923 Color: 4
Size: 236971 Color: 1

Bin 163: 107 of cap free
Amount of items: 2
Items: 
Size: 771273 Color: 3
Size: 228621 Color: 1

Bin 164: 108 of cap free
Amount of items: 2
Items: 
Size: 609854 Color: 2
Size: 390039 Color: 3

Bin 165: 108 of cap free
Amount of items: 2
Items: 
Size: 657049 Color: 3
Size: 342844 Color: 0

Bin 166: 110 of cap free
Amount of items: 3
Items: 
Size: 494527 Color: 3
Size: 252822 Color: 3
Size: 252542 Color: 1

Bin 167: 110 of cap free
Amount of items: 2
Items: 
Size: 683399 Color: 2
Size: 316492 Color: 4

Bin 168: 111 of cap free
Amount of items: 3
Items: 
Size: 738643 Color: 3
Size: 134168 Color: 4
Size: 127079 Color: 1

Bin 169: 112 of cap free
Amount of items: 2
Items: 
Size: 502924 Color: 4
Size: 496965 Color: 0

Bin 170: 113 of cap free
Amount of items: 2
Items: 
Size: 548283 Color: 3
Size: 451605 Color: 0

Bin 171: 114 of cap free
Amount of items: 2
Items: 
Size: 647231 Color: 2
Size: 352656 Color: 4

Bin 172: 115 of cap free
Amount of items: 3
Items: 
Size: 543631 Color: 2
Size: 234032 Color: 0
Size: 222223 Color: 3

Bin 173: 116 of cap free
Amount of items: 2
Items: 
Size: 532964 Color: 3
Size: 466921 Color: 1

Bin 174: 116 of cap free
Amount of items: 3
Items: 
Size: 720503 Color: 1
Size: 141362 Color: 3
Size: 138020 Color: 0

Bin 175: 117 of cap free
Amount of items: 3
Items: 
Size: 635862 Color: 0
Size: 185874 Color: 1
Size: 178148 Color: 4

Bin 176: 119 of cap free
Amount of items: 2
Items: 
Size: 796442 Color: 1
Size: 203440 Color: 4

Bin 177: 120 of cap free
Amount of items: 3
Items: 
Size: 651977 Color: 2
Size: 175548 Color: 3
Size: 172356 Color: 1

Bin 178: 120 of cap free
Amount of items: 2
Items: 
Size: 763890 Color: 4
Size: 235991 Color: 2

Bin 179: 121 of cap free
Amount of items: 3
Items: 
Size: 497964 Color: 2
Size: 251300 Color: 2
Size: 250616 Color: 4

Bin 180: 122 of cap free
Amount of items: 3
Items: 
Size: 362309 Color: 2
Size: 352294 Color: 3
Size: 285276 Color: 4

Bin 181: 122 of cap free
Amount of items: 3
Items: 
Size: 670878 Color: 2
Size: 166613 Color: 1
Size: 162388 Color: 4

Bin 182: 124 of cap free
Amount of items: 2
Items: 
Size: 765897 Color: 0
Size: 233980 Color: 4

Bin 183: 126 of cap free
Amount of items: 2
Items: 
Size: 587267 Color: 0
Size: 412608 Color: 3

Bin 184: 128 of cap free
Amount of items: 3
Items: 
Size: 622232 Color: 1
Size: 195219 Color: 4
Size: 182422 Color: 2

Bin 185: 128 of cap free
Amount of items: 2
Items: 
Size: 770326 Color: 0
Size: 229547 Color: 2

Bin 186: 129 of cap free
Amount of items: 3
Items: 
Size: 684914 Color: 1
Size: 158889 Color: 0
Size: 156069 Color: 4

Bin 187: 130 of cap free
Amount of items: 3
Items: 
Size: 560143 Color: 1
Size: 223590 Color: 0
Size: 216138 Color: 3

Bin 188: 130 of cap free
Amount of items: 2
Items: 
Size: 763397 Color: 0
Size: 236474 Color: 2

Bin 189: 131 of cap free
Amount of items: 2
Items: 
Size: 660706 Color: 0
Size: 339164 Color: 4

Bin 190: 131 of cap free
Amount of items: 2
Items: 
Size: 679987 Color: 2
Size: 319883 Color: 4

Bin 191: 133 of cap free
Amount of items: 2
Items: 
Size: 612755 Color: 0
Size: 387113 Color: 1

Bin 192: 133 of cap free
Amount of items: 2
Items: 
Size: 645034 Color: 2
Size: 354834 Color: 1

Bin 193: 135 of cap free
Amount of items: 2
Items: 
Size: 756306 Color: 0
Size: 243560 Color: 2

Bin 194: 136 of cap free
Amount of items: 2
Items: 
Size: 599498 Color: 2
Size: 400367 Color: 3

Bin 195: 136 of cap free
Amount of items: 2
Items: 
Size: 664107 Color: 4
Size: 335758 Color: 0

Bin 196: 136 of cap free
Amount of items: 3
Items: 
Size: 696112 Color: 4
Size: 156679 Color: 1
Size: 147074 Color: 2

Bin 197: 136 of cap free
Amount of items: 3
Items: 
Size: 723665 Color: 2
Size: 140050 Color: 3
Size: 136150 Color: 1

Bin 198: 137 of cap free
Amount of items: 2
Items: 
Size: 567772 Color: 4
Size: 432092 Color: 3

Bin 199: 137 of cap free
Amount of items: 3
Items: 
Size: 651665 Color: 3
Size: 174659 Color: 2
Size: 173540 Color: 4

Bin 200: 137 of cap free
Amount of items: 2
Items: 
Size: 775989 Color: 2
Size: 223875 Color: 0

Bin 201: 138 of cap free
Amount of items: 2
Items: 
Size: 554567 Color: 3
Size: 445296 Color: 2

Bin 202: 139 of cap free
Amount of items: 2
Items: 
Size: 778529 Color: 0
Size: 221333 Color: 4

Bin 203: 141 of cap free
Amount of items: 3
Items: 
Size: 648492 Color: 1
Size: 179855 Color: 3
Size: 171513 Color: 4

Bin 204: 142 of cap free
Amount of items: 2
Items: 
Size: 568752 Color: 4
Size: 431107 Color: 1

Bin 205: 142 of cap free
Amount of items: 3
Items: 
Size: 653979 Color: 1
Size: 175036 Color: 3
Size: 170844 Color: 1

Bin 206: 143 of cap free
Amount of items: 3
Items: 
Size: 630671 Color: 1
Size: 190294 Color: 3
Size: 178893 Color: 4

Bin 207: 146 of cap free
Amount of items: 2
Items: 
Size: 685063 Color: 0
Size: 314792 Color: 4

Bin 208: 147 of cap free
Amount of items: 3
Items: 
Size: 535569 Color: 0
Size: 239327 Color: 1
Size: 224958 Color: 3

Bin 209: 147 of cap free
Amount of items: 2
Items: 
Size: 631863 Color: 2
Size: 367991 Color: 3

Bin 210: 148 of cap free
Amount of items: 3
Items: 
Size: 631867 Color: 2
Size: 188365 Color: 1
Size: 179621 Color: 2

Bin 211: 148 of cap free
Amount of items: 2
Items: 
Size: 664046 Color: 4
Size: 335807 Color: 3

Bin 212: 148 of cap free
Amount of items: 2
Items: 
Size: 710913 Color: 1
Size: 288940 Color: 2

Bin 213: 149 of cap free
Amount of items: 3
Items: 
Size: 696887 Color: 3
Size: 155846 Color: 1
Size: 147119 Color: 2

Bin 214: 150 of cap free
Amount of items: 2
Items: 
Size: 700937 Color: 1
Size: 298914 Color: 2

Bin 215: 153 of cap free
Amount of items: 3
Items: 
Size: 733382 Color: 4
Size: 136606 Color: 3
Size: 129860 Color: 1

Bin 216: 154 of cap free
Amount of items: 2
Items: 
Size: 502247 Color: 1
Size: 497600 Color: 4

Bin 217: 154 of cap free
Amount of items: 3
Items: 
Size: 736953 Color: 4
Size: 131674 Color: 3
Size: 131220 Color: 4

Bin 218: 154 of cap free
Amount of items: 2
Items: 
Size: 766354 Color: 4
Size: 233493 Color: 3

Bin 219: 155 of cap free
Amount of items: 3
Items: 
Size: 593803 Color: 3
Size: 210268 Color: 1
Size: 195775 Color: 2

Bin 220: 156 of cap free
Amount of items: 3
Items: 
Size: 533237 Color: 0
Size: 237335 Color: 3
Size: 229273 Color: 3

Bin 221: 156 of cap free
Amount of items: 3
Items: 
Size: 558557 Color: 1
Size: 223723 Color: 0
Size: 217565 Color: 3

Bin 222: 158 of cap free
Amount of items: 3
Items: 
Size: 645973 Color: 3
Size: 177619 Color: 2
Size: 176251 Color: 1

Bin 223: 158 of cap free
Amount of items: 3
Items: 
Size: 656353 Color: 1
Size: 172446 Color: 2
Size: 171044 Color: 3

Bin 224: 159 of cap free
Amount of items: 2
Items: 
Size: 544813 Color: 3
Size: 455029 Color: 0

Bin 225: 160 of cap free
Amount of items: 2
Items: 
Size: 745718 Color: 2
Size: 254123 Color: 3

Bin 226: 163 of cap free
Amount of items: 3
Items: 
Size: 455143 Color: 4
Size: 283589 Color: 2
Size: 261106 Color: 3

Bin 227: 163 of cap free
Amount of items: 3
Items: 
Size: 631595 Color: 2
Size: 188757 Color: 1
Size: 179486 Color: 3

Bin 228: 163 of cap free
Amount of items: 2
Items: 
Size: 745220 Color: 2
Size: 254618 Color: 1

Bin 229: 164 of cap free
Amount of items: 3
Items: 
Size: 525698 Color: 1
Size: 240162 Color: 4
Size: 233977 Color: 1

Bin 230: 164 of cap free
Amount of items: 3
Items: 
Size: 712301 Color: 1
Size: 146793 Color: 4
Size: 140743 Color: 2

Bin 231: 165 of cap free
Amount of items: 2
Items: 
Size: 652644 Color: 4
Size: 347192 Color: 1

Bin 232: 165 of cap free
Amount of items: 3
Items: 
Size: 754862 Color: 2
Size: 126583 Color: 3
Size: 118391 Color: 2

Bin 233: 166 of cap free
Amount of items: 2
Items: 
Size: 633434 Color: 0
Size: 366401 Color: 4

Bin 234: 168 of cap free
Amount of items: 2
Items: 
Size: 797158 Color: 2
Size: 202675 Color: 0

Bin 235: 169 of cap free
Amount of items: 2
Items: 
Size: 736385 Color: 2
Size: 263447 Color: 1

Bin 236: 174 of cap free
Amount of items: 2
Items: 
Size: 758589 Color: 1
Size: 241238 Color: 4

Bin 237: 175 of cap free
Amount of items: 2
Items: 
Size: 726216 Color: 4
Size: 273610 Color: 2

Bin 238: 175 of cap free
Amount of items: 2
Items: 
Size: 750625 Color: 3
Size: 249201 Color: 0

Bin 239: 177 of cap free
Amount of items: 3
Items: 
Size: 792495 Color: 3
Size: 103848 Color: 2
Size: 103481 Color: 4

Bin 240: 178 of cap free
Amount of items: 3
Items: 
Size: 769654 Color: 1
Size: 115218 Color: 4
Size: 114951 Color: 4

Bin 241: 185 of cap free
Amount of items: 3
Items: 
Size: 774267 Color: 1
Size: 112973 Color: 0
Size: 112576 Color: 3

Bin 242: 186 of cap free
Amount of items: 3
Items: 
Size: 704523 Color: 2
Size: 152973 Color: 3
Size: 142319 Color: 1

Bin 243: 187 of cap free
Amount of items: 2
Items: 
Size: 767895 Color: 2
Size: 231919 Color: 4

Bin 244: 188 of cap free
Amount of items: 2
Items: 
Size: 752853 Color: 1
Size: 246960 Color: 2

Bin 245: 190 of cap free
Amount of items: 3
Items: 
Size: 784438 Color: 1
Size: 110433 Color: 2
Size: 104940 Color: 3

Bin 246: 191 of cap free
Amount of items: 2
Items: 
Size: 691635 Color: 4
Size: 308175 Color: 2

Bin 247: 191 of cap free
Amount of items: 2
Items: 
Size: 712495 Color: 3
Size: 287315 Color: 1

Bin 248: 192 of cap free
Amount of items: 2
Items: 
Size: 573158 Color: 4
Size: 426651 Color: 3

Bin 249: 193 of cap free
Amount of items: 2
Items: 
Size: 596175 Color: 2
Size: 403633 Color: 3

Bin 250: 193 of cap free
Amount of items: 3
Items: 
Size: 723419 Color: 3
Size: 138898 Color: 4
Size: 137491 Color: 1

Bin 251: 195 of cap free
Amount of items: 3
Items: 
Size: 451910 Color: 4
Size: 288121 Color: 1
Size: 259775 Color: 0

Bin 252: 196 of cap free
Amount of items: 2
Items: 
Size: 536469 Color: 1
Size: 463336 Color: 3

Bin 253: 196 of cap free
Amount of items: 2
Items: 
Size: 788326 Color: 0
Size: 211479 Color: 4

Bin 254: 197 of cap free
Amount of items: 2
Items: 
Size: 599739 Color: 3
Size: 400065 Color: 0

Bin 255: 198 of cap free
Amount of items: 2
Items: 
Size: 513309 Color: 4
Size: 486494 Color: 3

Bin 256: 198 of cap free
Amount of items: 2
Items: 
Size: 639452 Color: 4
Size: 360351 Color: 2

Bin 257: 200 of cap free
Amount of items: 2
Items: 
Size: 598352 Color: 0
Size: 401449 Color: 1

Bin 258: 200 of cap free
Amount of items: 2
Items: 
Size: 779140 Color: 2
Size: 220661 Color: 4

Bin 259: 202 of cap free
Amount of items: 2
Items: 
Size: 624738 Color: 3
Size: 375061 Color: 0

Bin 260: 206 of cap free
Amount of items: 2
Items: 
Size: 598883 Color: 1
Size: 400912 Color: 3

Bin 261: 207 of cap free
Amount of items: 2
Items: 
Size: 699375 Color: 4
Size: 300419 Color: 2

Bin 262: 207 of cap free
Amount of items: 2
Items: 
Size: 701815 Color: 3
Size: 297979 Color: 4

Bin 263: 209 of cap free
Amount of items: 2
Items: 
Size: 607796 Color: 3
Size: 391996 Color: 4

Bin 264: 209 of cap free
Amount of items: 2
Items: 
Size: 676271 Color: 1
Size: 323521 Color: 0

Bin 265: 213 of cap free
Amount of items: 2
Items: 
Size: 734352 Color: 0
Size: 265436 Color: 3

Bin 266: 214 of cap free
Amount of items: 3
Items: 
Size: 675887 Color: 4
Size: 164654 Color: 3
Size: 159246 Color: 1

Bin 267: 214 of cap free
Amount of items: 2
Items: 
Size: 725469 Color: 4
Size: 274318 Color: 3

Bin 268: 214 of cap free
Amount of items: 2
Items: 
Size: 738046 Color: 0
Size: 261741 Color: 1

Bin 269: 215 of cap free
Amount of items: 3
Items: 
Size: 537319 Color: 0
Size: 238913 Color: 1
Size: 223554 Color: 4

Bin 270: 216 of cap free
Amount of items: 2
Items: 
Size: 795688 Color: 2
Size: 204097 Color: 1

Bin 271: 217 of cap free
Amount of items: 2
Items: 
Size: 766506 Color: 0
Size: 233278 Color: 1

Bin 272: 222 of cap free
Amount of items: 2
Items: 
Size: 697252 Color: 2
Size: 302527 Color: 4

Bin 273: 222 of cap free
Amount of items: 2
Items: 
Size: 706290 Color: 2
Size: 293489 Color: 3

Bin 274: 223 of cap free
Amount of items: 2
Items: 
Size: 548188 Color: 3
Size: 451590 Color: 1

Bin 275: 223 of cap free
Amount of items: 2
Items: 
Size: 602435 Color: 3
Size: 397343 Color: 1

Bin 276: 226 of cap free
Amount of items: 2
Items: 
Size: 506939 Color: 4
Size: 492836 Color: 0

Bin 277: 226 of cap free
Amount of items: 2
Items: 
Size: 684031 Color: 1
Size: 315744 Color: 0

Bin 278: 227 of cap free
Amount of items: 3
Items: 
Size: 710935 Color: 1
Size: 150487 Color: 3
Size: 138352 Color: 0

Bin 279: 231 of cap free
Amount of items: 2
Items: 
Size: 597733 Color: 1
Size: 402037 Color: 4

Bin 280: 233 of cap free
Amount of items: 2
Items: 
Size: 592138 Color: 1
Size: 407630 Color: 2

Bin 281: 233 of cap free
Amount of items: 2
Items: 
Size: 607543 Color: 4
Size: 392225 Color: 2

Bin 282: 233 of cap free
Amount of items: 3
Items: 
Size: 647754 Color: 3
Size: 178445 Color: 4
Size: 173569 Color: 2

Bin 283: 235 of cap free
Amount of items: 3
Items: 
Size: 623990 Color: 4
Size: 191063 Color: 1
Size: 184713 Color: 4

Bin 284: 235 of cap free
Amount of items: 3
Items: 
Size: 762957 Color: 2
Size: 119252 Color: 3
Size: 117557 Color: 0

Bin 285: 236 of cap free
Amount of items: 2
Items: 
Size: 587517 Color: 0
Size: 412248 Color: 1

Bin 286: 236 of cap free
Amount of items: 2
Items: 
Size: 655968 Color: 0
Size: 343797 Color: 2

Bin 287: 241 of cap free
Amount of items: 3
Items: 
Size: 363068 Color: 4
Size: 346323 Color: 0
Size: 290369 Color: 3

Bin 288: 241 of cap free
Amount of items: 2
Items: 
Size: 756826 Color: 4
Size: 242934 Color: 2

Bin 289: 242 of cap free
Amount of items: 2
Items: 
Size: 623463 Color: 0
Size: 376296 Color: 1

Bin 290: 243 of cap free
Amount of items: 3
Items: 
Size: 659810 Color: 3
Size: 171188 Color: 4
Size: 168760 Color: 2

Bin 291: 243 of cap free
Amount of items: 3
Items: 
Size: 789722 Color: 0
Size: 105406 Color: 3
Size: 104630 Color: 4

Bin 292: 246 of cap free
Amount of items: 2
Items: 
Size: 533437 Color: 3
Size: 466318 Color: 1

Bin 293: 249 of cap free
Amount of items: 3
Items: 
Size: 580219 Color: 1
Size: 209782 Color: 4
Size: 209751 Color: 4

Bin 294: 251 of cap free
Amount of items: 3
Items: 
Size: 757096 Color: 3
Size: 121704 Color: 1
Size: 120950 Color: 2

Bin 295: 254 of cap free
Amount of items: 2
Items: 
Size: 645621 Color: 4
Size: 354126 Color: 3

Bin 296: 255 of cap free
Amount of items: 2
Items: 
Size: 531561 Color: 2
Size: 468185 Color: 0

Bin 297: 256 of cap free
Amount of items: 3
Items: 
Size: 439103 Color: 4
Size: 291507 Color: 3
Size: 269135 Color: 1

Bin 298: 256 of cap free
Amount of items: 2
Items: 
Size: 799369 Color: 2
Size: 200376 Color: 3

Bin 299: 257 of cap free
Amount of items: 2
Items: 
Size: 542335 Color: 3
Size: 457409 Color: 4

Bin 300: 257 of cap free
Amount of items: 2
Items: 
Size: 547713 Color: 4
Size: 452031 Color: 1

Bin 301: 258 of cap free
Amount of items: 3
Items: 
Size: 365002 Color: 0
Size: 317801 Color: 1
Size: 316940 Color: 2

Bin 302: 258 of cap free
Amount of items: 3
Items: 
Size: 543314 Color: 3
Size: 234313 Color: 0
Size: 222116 Color: 4

Bin 303: 259 of cap free
Amount of items: 2
Items: 
Size: 659002 Color: 0
Size: 340740 Color: 3

Bin 304: 261 of cap free
Amount of items: 2
Items: 
Size: 509495 Color: 1
Size: 490245 Color: 0

Bin 305: 262 of cap free
Amount of items: 2
Items: 
Size: 575996 Color: 3
Size: 423743 Color: 1

Bin 306: 263 of cap free
Amount of items: 2
Items: 
Size: 555684 Color: 1
Size: 444054 Color: 0

Bin 307: 264 of cap free
Amount of items: 3
Items: 
Size: 736555 Color: 2
Size: 134090 Color: 3
Size: 129092 Color: 2

Bin 308: 265 of cap free
Amount of items: 2
Items: 
Size: 546335 Color: 1
Size: 453401 Color: 4

Bin 309: 267 of cap free
Amount of items: 2
Items: 
Size: 578690 Color: 0
Size: 421044 Color: 1

Bin 310: 268 of cap free
Amount of items: 2
Items: 
Size: 593218 Color: 1
Size: 406515 Color: 4

Bin 311: 268 of cap free
Amount of items: 3
Items: 
Size: 715721 Color: 2
Size: 145265 Color: 4
Size: 138747 Color: 2

Bin 312: 270 of cap free
Amount of items: 2
Items: 
Size: 715661 Color: 2
Size: 284070 Color: 3

Bin 313: 274 of cap free
Amount of items: 2
Items: 
Size: 753346 Color: 4
Size: 246381 Color: 2

Bin 314: 276 of cap free
Amount of items: 3
Items: 
Size: 713060 Color: 4
Size: 145093 Color: 1
Size: 141572 Color: 1

Bin 315: 280 of cap free
Amount of items: 2
Items: 
Size: 720103 Color: 0
Size: 279618 Color: 1

Bin 316: 283 of cap free
Amount of items: 2
Items: 
Size: 621006 Color: 2
Size: 378712 Color: 4

Bin 317: 283 of cap free
Amount of items: 3
Items: 
Size: 781535 Color: 3
Size: 110328 Color: 0
Size: 107855 Color: 1

Bin 318: 284 of cap free
Amount of items: 2
Items: 
Size: 574369 Color: 4
Size: 425348 Color: 1

Bin 319: 287 of cap free
Amount of items: 3
Items: 
Size: 670042 Color: 3
Size: 167615 Color: 2
Size: 162057 Color: 1

Bin 320: 291 of cap free
Amount of items: 2
Items: 
Size: 582367 Color: 3
Size: 417343 Color: 0

Bin 321: 294 of cap free
Amount of items: 2
Items: 
Size: 768265 Color: 4
Size: 231442 Color: 2

Bin 322: 298 of cap free
Amount of items: 3
Items: 
Size: 740183 Color: 0
Size: 132163 Color: 3
Size: 127357 Color: 1

Bin 323: 299 of cap free
Amount of items: 2
Items: 
Size: 711967 Color: 2
Size: 287735 Color: 0

Bin 324: 300 of cap free
Amount of items: 3
Items: 
Size: 711237 Color: 2
Size: 149952 Color: 4
Size: 138512 Color: 0

Bin 325: 300 of cap free
Amount of items: 2
Items: 
Size: 780556 Color: 1
Size: 219145 Color: 2

Bin 326: 301 of cap free
Amount of items: 2
Items: 
Size: 747601 Color: 0
Size: 252099 Color: 4

Bin 327: 302 of cap free
Amount of items: 2
Items: 
Size: 703211 Color: 0
Size: 296488 Color: 1

Bin 328: 303 of cap free
Amount of items: 2
Items: 
Size: 650020 Color: 4
Size: 349678 Color: 3

Bin 329: 304 of cap free
Amount of items: 3
Items: 
Size: 726525 Color: 3
Size: 139368 Color: 1
Size: 133804 Color: 4

Bin 330: 305 of cap free
Amount of items: 3
Items: 
Size: 756621 Color: 0
Size: 125210 Color: 3
Size: 117865 Color: 4

Bin 331: 305 of cap free
Amount of items: 3
Items: 
Size: 760771 Color: 3
Size: 120457 Color: 2
Size: 118468 Color: 0

Bin 332: 306 of cap free
Amount of items: 2
Items: 
Size: 593593 Color: 2
Size: 406102 Color: 0

Bin 333: 307 of cap free
Amount of items: 2
Items: 
Size: 636955 Color: 2
Size: 362739 Color: 3

Bin 334: 309 of cap free
Amount of items: 2
Items: 
Size: 649721 Color: 2
Size: 349971 Color: 1

Bin 335: 311 of cap free
Amount of items: 2
Items: 
Size: 556343 Color: 3
Size: 443347 Color: 1

Bin 336: 311 of cap free
Amount of items: 2
Items: 
Size: 673310 Color: 2
Size: 326380 Color: 0

Bin 337: 312 of cap free
Amount of items: 2
Items: 
Size: 778198 Color: 1
Size: 221491 Color: 4

Bin 338: 313 of cap free
Amount of items: 3
Items: 
Size: 699530 Color: 3
Size: 152678 Color: 0
Size: 147480 Color: 0

Bin 339: 314 of cap free
Amount of items: 2
Items: 
Size: 638490 Color: 0
Size: 361197 Color: 3

Bin 340: 314 of cap free
Amount of items: 2
Items: 
Size: 708607 Color: 0
Size: 291080 Color: 4

Bin 341: 314 of cap free
Amount of items: 3
Items: 
Size: 785539 Color: 3
Size: 107309 Color: 0
Size: 106839 Color: 4

Bin 342: 316 of cap free
Amount of items: 2
Items: 
Size: 586976 Color: 2
Size: 412709 Color: 1

Bin 343: 318 of cap free
Amount of items: 2
Items: 
Size: 569351 Color: 1
Size: 430332 Color: 2

Bin 344: 320 of cap free
Amount of items: 3
Items: 
Size: 797717 Color: 3
Size: 101378 Color: 0
Size: 100586 Color: 2

Bin 345: 321 of cap free
Amount of items: 2
Items: 
Size: 530550 Color: 0
Size: 469130 Color: 1

Bin 346: 322 of cap free
Amount of items: 2
Items: 
Size: 512400 Color: 3
Size: 487279 Color: 4

Bin 347: 323 of cap free
Amount of items: 2
Items: 
Size: 784564 Color: 1
Size: 215114 Color: 0

Bin 348: 325 of cap free
Amount of items: 2
Items: 
Size: 516608 Color: 3
Size: 483068 Color: 1

Bin 349: 327 of cap free
Amount of items: 2
Items: 
Size: 603701 Color: 3
Size: 395973 Color: 4

Bin 350: 327 of cap free
Amount of items: 3
Items: 
Size: 630557 Color: 1
Size: 186496 Color: 0
Size: 182621 Color: 3

Bin 351: 328 of cap free
Amount of items: 3
Items: 
Size: 357839 Color: 2
Size: 325902 Color: 3
Size: 315932 Color: 1

Bin 352: 329 of cap free
Amount of items: 2
Items: 
Size: 520371 Color: 0
Size: 479301 Color: 2

Bin 353: 329 of cap free
Amount of items: 2
Items: 
Size: 606620 Color: 0
Size: 393052 Color: 1

Bin 354: 330 of cap free
Amount of items: 2
Items: 
Size: 741906 Color: 2
Size: 257765 Color: 1

Bin 355: 333 of cap free
Amount of items: 2
Items: 
Size: 537655 Color: 2
Size: 462013 Color: 4

Bin 356: 333 of cap free
Amount of items: 2
Items: 
Size: 796804 Color: 1
Size: 202864 Color: 0

Bin 357: 336 of cap free
Amount of items: 2
Items: 
Size: 504312 Color: 4
Size: 495353 Color: 3

Bin 358: 337 of cap free
Amount of items: 2
Items: 
Size: 691068 Color: 1
Size: 308596 Color: 3

Bin 359: 337 of cap free
Amount of items: 3
Items: 
Size: 762648 Color: 3
Size: 119601 Color: 1
Size: 117415 Color: 0

Bin 360: 338 of cap free
Amount of items: 2
Items: 
Size: 594078 Color: 4
Size: 405585 Color: 0

Bin 361: 338 of cap free
Amount of items: 2
Items: 
Size: 772270 Color: 0
Size: 227393 Color: 1

Bin 362: 339 of cap free
Amount of items: 2
Items: 
Size: 613826 Color: 2
Size: 385836 Color: 0

Bin 363: 340 of cap free
Amount of items: 2
Items: 
Size: 792195 Color: 1
Size: 207466 Color: 4

Bin 364: 341 of cap free
Amount of items: 3
Items: 
Size: 681325 Color: 1
Size: 159647 Color: 3
Size: 158688 Color: 2

Bin 365: 342 of cap free
Amount of items: 2
Items: 
Size: 585551 Color: 1
Size: 414108 Color: 4

Bin 366: 342 of cap free
Amount of items: 2
Items: 
Size: 778588 Color: 4
Size: 221071 Color: 1

Bin 367: 344 of cap free
Amount of items: 2
Items: 
Size: 602810 Color: 4
Size: 396847 Color: 2

Bin 368: 344 of cap free
Amount of items: 3
Items: 
Size: 709885 Color: 0
Size: 149476 Color: 2
Size: 140296 Color: 3

Bin 369: 349 of cap free
Amount of items: 2
Items: 
Size: 504908 Color: 1
Size: 494744 Color: 2

Bin 370: 350 of cap free
Amount of items: 2
Items: 
Size: 517792 Color: 2
Size: 481859 Color: 4

Bin 371: 358 of cap free
Amount of items: 2
Items: 
Size: 600938 Color: 3
Size: 398705 Color: 0

Bin 372: 361 of cap free
Amount of items: 2
Items: 
Size: 667673 Color: 4
Size: 331967 Color: 2

Bin 373: 363 of cap free
Amount of items: 2
Items: 
Size: 519411 Color: 4
Size: 480227 Color: 3

Bin 374: 363 of cap free
Amount of items: 2
Items: 
Size: 650754 Color: 1
Size: 348884 Color: 2

Bin 375: 366 of cap free
Amount of items: 3
Items: 
Size: 702809 Color: 2
Size: 152600 Color: 4
Size: 144226 Color: 0

Bin 376: 366 of cap free
Amount of items: 2
Items: 
Size: 736878 Color: 1
Size: 262757 Color: 0

Bin 377: 366 of cap free
Amount of items: 2
Items: 
Size: 796206 Color: 2
Size: 203429 Color: 0

Bin 378: 371 of cap free
Amount of items: 2
Items: 
Size: 741484 Color: 1
Size: 258146 Color: 2

Bin 379: 379 of cap free
Amount of items: 2
Items: 
Size: 752941 Color: 2
Size: 246681 Color: 4

Bin 380: 380 of cap free
Amount of items: 2
Items: 
Size: 640648 Color: 3
Size: 358973 Color: 1

Bin 381: 386 of cap free
Amount of items: 2
Items: 
Size: 658889 Color: 0
Size: 340726 Color: 4

Bin 382: 386 of cap free
Amount of items: 2
Items: 
Size: 724976 Color: 1
Size: 274639 Color: 2

Bin 383: 386 of cap free
Amount of items: 2
Items: 
Size: 798176 Color: 2
Size: 201439 Color: 4

Bin 384: 387 of cap free
Amount of items: 2
Items: 
Size: 571135 Color: 4
Size: 428479 Color: 0

Bin 385: 388 of cap free
Amount of items: 2
Items: 
Size: 681326 Color: 4
Size: 318287 Color: 0

Bin 386: 391 of cap free
Amount of items: 2
Items: 
Size: 767034 Color: 0
Size: 232576 Color: 2

Bin 387: 395 of cap free
Amount of items: 2
Items: 
Size: 769305 Color: 2
Size: 230301 Color: 4

Bin 388: 396 of cap free
Amount of items: 3
Items: 
Size: 792430 Color: 3
Size: 105281 Color: 0
Size: 101894 Color: 2

Bin 389: 397 of cap free
Amount of items: 2
Items: 
Size: 539666 Color: 1
Size: 459938 Color: 4

Bin 390: 397 of cap free
Amount of items: 3
Items: 
Size: 704588 Color: 2
Size: 150347 Color: 2
Size: 144669 Color: 1

Bin 391: 398 of cap free
Amount of items: 2
Items: 
Size: 695000 Color: 3
Size: 304603 Color: 0

Bin 392: 398 of cap free
Amount of items: 2
Items: 
Size: 740220 Color: 0
Size: 259383 Color: 4

Bin 393: 401 of cap free
Amount of items: 3
Items: 
Size: 759357 Color: 1
Size: 121205 Color: 3
Size: 119038 Color: 0

Bin 394: 402 of cap free
Amount of items: 3
Items: 
Size: 707190 Color: 3
Size: 152040 Color: 4
Size: 140369 Color: 0

Bin 395: 403 of cap free
Amount of items: 2
Items: 
Size: 540952 Color: 1
Size: 458646 Color: 4

Bin 396: 403 of cap free
Amount of items: 2
Items: 
Size: 546420 Color: 2
Size: 453178 Color: 0

Bin 397: 407 of cap free
Amount of items: 2
Items: 
Size: 602607 Color: 2
Size: 396987 Color: 4

Bin 398: 407 of cap free
Amount of items: 2
Items: 
Size: 781969 Color: 4
Size: 217625 Color: 2

Bin 399: 411 of cap free
Amount of items: 2
Items: 
Size: 583385 Color: 1
Size: 416205 Color: 4

Bin 400: 412 of cap free
Amount of items: 2
Items: 
Size: 683343 Color: 1
Size: 316246 Color: 3

Bin 401: 414 of cap free
Amount of items: 2
Items: 
Size: 548927 Color: 1
Size: 450660 Color: 4

Bin 402: 416 of cap free
Amount of items: 2
Items: 
Size: 581566 Color: 1
Size: 418019 Color: 4

Bin 403: 416 of cap free
Amount of items: 3
Items: 
Size: 744126 Color: 3
Size: 129995 Color: 1
Size: 125464 Color: 1

Bin 404: 416 of cap free
Amount of items: 2
Items: 
Size: 744690 Color: 4
Size: 254895 Color: 2

Bin 405: 422 of cap free
Amount of items: 2
Items: 
Size: 683201 Color: 4
Size: 316378 Color: 1

Bin 406: 422 of cap free
Amount of items: 2
Items: 
Size: 706256 Color: 4
Size: 293323 Color: 1

Bin 407: 425 of cap free
Amount of items: 2
Items: 
Size: 663571 Color: 1
Size: 336005 Color: 0

Bin 408: 427 of cap free
Amount of items: 2
Items: 
Size: 592588 Color: 1
Size: 406986 Color: 4

Bin 409: 428 of cap free
Amount of items: 2
Items: 
Size: 575942 Color: 3
Size: 423631 Color: 1

Bin 410: 429 of cap free
Amount of items: 2
Items: 
Size: 766952 Color: 0
Size: 232620 Color: 3

Bin 411: 433 of cap free
Amount of items: 2
Items: 
Size: 539729 Color: 4
Size: 459839 Color: 0

Bin 412: 433 of cap free
Amount of items: 2
Items: 
Size: 698246 Color: 4
Size: 301322 Color: 0

Bin 413: 434 of cap free
Amount of items: 2
Items: 
Size: 694824 Color: 4
Size: 304743 Color: 2

Bin 414: 436 of cap free
Amount of items: 2
Items: 
Size: 645956 Color: 2
Size: 353609 Color: 4

Bin 415: 437 of cap free
Amount of items: 2
Items: 
Size: 534050 Color: 1
Size: 465514 Color: 3

Bin 416: 437 of cap free
Amount of items: 2
Items: 
Size: 720522 Color: 1
Size: 279042 Color: 4

Bin 417: 448 of cap free
Amount of items: 2
Items: 
Size: 585742 Color: 2
Size: 413811 Color: 0

Bin 418: 450 of cap free
Amount of items: 2
Items: 
Size: 795044 Color: 2
Size: 204507 Color: 0

Bin 419: 451 of cap free
Amount of items: 2
Items: 
Size: 630691 Color: 2
Size: 368859 Color: 4

Bin 420: 458 of cap free
Amount of items: 2
Items: 
Size: 694027 Color: 3
Size: 305516 Color: 4

Bin 421: 458 of cap free
Amount of items: 2
Items: 
Size: 728111 Color: 1
Size: 271432 Color: 0

Bin 422: 462 of cap free
Amount of items: 3
Items: 
Size: 771508 Color: 2
Size: 115467 Color: 3
Size: 112564 Color: 2

Bin 423: 463 of cap free
Amount of items: 2
Items: 
Size: 568478 Color: 2
Size: 431060 Color: 4

Bin 424: 466 of cap free
Amount of items: 2
Items: 
Size: 684435 Color: 3
Size: 315100 Color: 4

Bin 425: 466 of cap free
Amount of items: 2
Items: 
Size: 779377 Color: 0
Size: 220158 Color: 4

Bin 426: 467 of cap free
Amount of items: 3
Items: 
Size: 639811 Color: 0
Size: 184457 Color: 1
Size: 175266 Color: 2

Bin 427: 469 of cap free
Amount of items: 3
Items: 
Size: 713251 Color: 3
Size: 146938 Color: 4
Size: 139343 Color: 2

Bin 428: 470 of cap free
Amount of items: 2
Items: 
Size: 727132 Color: 2
Size: 272399 Color: 3

Bin 429: 472 of cap free
Amount of items: 2
Items: 
Size: 693260 Color: 2
Size: 306269 Color: 0

Bin 430: 472 of cap free
Amount of items: 3
Items: 
Size: 795485 Color: 3
Size: 102080 Color: 0
Size: 101964 Color: 4

Bin 431: 477 of cap free
Amount of items: 3
Items: 
Size: 598863 Color: 3
Size: 207342 Color: 1
Size: 193319 Color: 2

Bin 432: 483 of cap free
Amount of items: 2
Items: 
Size: 524692 Color: 1
Size: 474826 Color: 0

Bin 433: 491 of cap free
Amount of items: 2
Items: 
Size: 588133 Color: 2
Size: 411377 Color: 4

Bin 434: 499 of cap free
Amount of items: 2
Items: 
Size: 568387 Color: 3
Size: 431115 Color: 2

Bin 435: 499 of cap free
Amount of items: 2
Items: 
Size: 740435 Color: 0
Size: 259067 Color: 3

Bin 436: 500 of cap free
Amount of items: 2
Items: 
Size: 600452 Color: 3
Size: 399049 Color: 1

Bin 437: 504 of cap free
Amount of items: 2
Items: 
Size: 733065 Color: 2
Size: 266432 Color: 0

Bin 438: 511 of cap free
Amount of items: 2
Items: 
Size: 529742 Color: 1
Size: 469748 Color: 4

Bin 439: 513 of cap free
Amount of items: 2
Items: 
Size: 649243 Color: 4
Size: 350245 Color: 2

Bin 440: 515 of cap free
Amount of items: 2
Items: 
Size: 690073 Color: 2
Size: 309413 Color: 3

Bin 441: 524 of cap free
Amount of items: 2
Items: 
Size: 575598 Color: 0
Size: 423879 Color: 3

Bin 442: 528 of cap free
Amount of items: 2
Items: 
Size: 755737 Color: 4
Size: 243736 Color: 1

Bin 443: 530 of cap free
Amount of items: 2
Items: 
Size: 521427 Color: 0
Size: 478044 Color: 1

Bin 444: 537 of cap free
Amount of items: 2
Items: 
Size: 615797 Color: 2
Size: 383667 Color: 3

Bin 445: 540 of cap free
Amount of items: 2
Items: 
Size: 773227 Color: 3
Size: 226234 Color: 1

Bin 446: 542 of cap free
Amount of items: 3
Items: 
Size: 535210 Color: 1
Size: 239919 Color: 0
Size: 224330 Color: 2

Bin 447: 542 of cap free
Amount of items: 2
Items: 
Size: 770228 Color: 4
Size: 229231 Color: 1

Bin 448: 542 of cap free
Amount of items: 3
Items: 
Size: 782493 Color: 3
Size: 110829 Color: 0
Size: 106137 Color: 4

Bin 449: 543 of cap free
Amount of items: 2
Items: 
Size: 567550 Color: 4
Size: 431908 Color: 2

Bin 450: 552 of cap free
Amount of items: 3
Items: 
Size: 673247 Color: 1
Size: 166009 Color: 3
Size: 160193 Color: 1

Bin 451: 555 of cap free
Amount of items: 2
Items: 
Size: 743106 Color: 1
Size: 256340 Color: 4

Bin 452: 557 of cap free
Amount of items: 2
Items: 
Size: 588368 Color: 4
Size: 411076 Color: 3

Bin 453: 557 of cap free
Amount of items: 2
Items: 
Size: 711064 Color: 1
Size: 288380 Color: 4

Bin 454: 558 of cap free
Amount of items: 2
Items: 
Size: 589065 Color: 2
Size: 410378 Color: 3

Bin 455: 562 of cap free
Amount of items: 3
Items: 
Size: 661713 Color: 3
Size: 170295 Color: 4
Size: 167431 Color: 2

Bin 456: 563 of cap free
Amount of items: 3
Items: 
Size: 701922 Color: 2
Size: 152839 Color: 3
Size: 144677 Color: 2

Bin 457: 567 of cap free
Amount of items: 2
Items: 
Size: 719557 Color: 3
Size: 279877 Color: 0

Bin 458: 568 of cap free
Amount of items: 2
Items: 
Size: 604934 Color: 0
Size: 394499 Color: 3

Bin 459: 569 of cap free
Amount of items: 2
Items: 
Size: 584722 Color: 0
Size: 414710 Color: 3

Bin 460: 570 of cap free
Amount of items: 3
Items: 
Size: 699712 Color: 0
Size: 154128 Color: 3
Size: 145591 Color: 2

Bin 461: 573 of cap free
Amount of items: 2
Items: 
Size: 602362 Color: 0
Size: 397066 Color: 1

Bin 462: 581 of cap free
Amount of items: 2
Items: 
Size: 546169 Color: 3
Size: 453251 Color: 4

Bin 463: 581 of cap free
Amount of items: 2
Items: 
Size: 731856 Color: 0
Size: 267564 Color: 4

Bin 464: 586 of cap free
Amount of items: 2
Items: 
Size: 658704 Color: 1
Size: 340711 Color: 0

Bin 465: 586 of cap free
Amount of items: 2
Items: 
Size: 667199 Color: 1
Size: 332216 Color: 4

Bin 466: 588 of cap free
Amount of items: 3
Items: 
Size: 533308 Color: 0
Size: 234811 Color: 4
Size: 231294 Color: 2

Bin 467: 600 of cap free
Amount of items: 2
Items: 
Size: 556275 Color: 4
Size: 443126 Color: 3

Bin 468: 603 of cap free
Amount of items: 2
Items: 
Size: 728040 Color: 1
Size: 271358 Color: 2

Bin 469: 605 of cap free
Amount of items: 2
Items: 
Size: 629418 Color: 4
Size: 369978 Color: 2

Bin 470: 607 of cap free
Amount of items: 2
Items: 
Size: 563781 Color: 1
Size: 435613 Color: 0

Bin 471: 609 of cap free
Amount of items: 2
Items: 
Size: 716953 Color: 4
Size: 282439 Color: 1

Bin 472: 613 of cap free
Amount of items: 2
Items: 
Size: 583313 Color: 3
Size: 416075 Color: 2

Bin 473: 618 of cap free
Amount of items: 2
Items: 
Size: 652019 Color: 2
Size: 347364 Color: 4

Bin 474: 618 of cap free
Amount of items: 2
Items: 
Size: 760037 Color: 0
Size: 239346 Color: 1

Bin 475: 619 of cap free
Amount of items: 2
Items: 
Size: 704007 Color: 2
Size: 295375 Color: 1

Bin 476: 623 of cap free
Amount of items: 2
Items: 
Size: 570272 Color: 3
Size: 429106 Color: 2

Bin 477: 624 of cap free
Amount of items: 2
Items: 
Size: 608717 Color: 0
Size: 390660 Color: 1

Bin 478: 624 of cap free
Amount of items: 3
Items: 
Size: 715001 Color: 2
Size: 147501 Color: 4
Size: 136875 Color: 2

Bin 479: 626 of cap free
Amount of items: 2
Items: 
Size: 580310 Color: 2
Size: 419065 Color: 1

Bin 480: 628 of cap free
Amount of items: 2
Items: 
Size: 620848 Color: 4
Size: 378525 Color: 3

Bin 481: 634 of cap free
Amount of items: 2
Items: 
Size: 780303 Color: 4
Size: 219064 Color: 3

Bin 482: 636 of cap free
Amount of items: 2
Items: 
Size: 517855 Color: 4
Size: 481510 Color: 3

Bin 483: 637 of cap free
Amount of items: 2
Items: 
Size: 623797 Color: 3
Size: 375567 Color: 2

Bin 484: 639 of cap free
Amount of items: 3
Items: 
Size: 783826 Color: 3
Size: 111076 Color: 1
Size: 104460 Color: 2

Bin 485: 643 of cap free
Amount of items: 2
Items: 
Size: 699302 Color: 0
Size: 300056 Color: 3

Bin 486: 643 of cap free
Amount of items: 2
Items: 
Size: 724913 Color: 0
Size: 274445 Color: 2

Bin 487: 650 of cap free
Amount of items: 2
Items: 
Size: 617409 Color: 0
Size: 381942 Color: 2

Bin 488: 654 of cap free
Amount of items: 2
Items: 
Size: 684602 Color: 0
Size: 314745 Color: 3

Bin 489: 655 of cap free
Amount of items: 2
Items: 
Size: 721886 Color: 0
Size: 277460 Color: 2

Bin 490: 657 of cap free
Amount of items: 2
Items: 
Size: 504035 Color: 1
Size: 495309 Color: 4

Bin 491: 657 of cap free
Amount of items: 2
Items: 
Size: 772010 Color: 2
Size: 227334 Color: 4

Bin 492: 661 of cap free
Amount of items: 2
Items: 
Size: 532707 Color: 2
Size: 466633 Color: 4

Bin 493: 662 of cap free
Amount of items: 2
Items: 
Size: 524259 Color: 0
Size: 475080 Color: 2

Bin 494: 663 of cap free
Amount of items: 2
Items: 
Size: 663255 Color: 2
Size: 336083 Color: 1

Bin 495: 664 of cap free
Amount of items: 3
Items: 
Size: 748034 Color: 4
Size: 125684 Color: 3
Size: 125619 Color: 1

Bin 496: 668 of cap free
Amount of items: 2
Items: 
Size: 643412 Color: 4
Size: 355921 Color: 2

Bin 497: 670 of cap free
Amount of items: 2
Items: 
Size: 795357 Color: 0
Size: 203974 Color: 2

Bin 498: 671 of cap free
Amount of items: 3
Items: 
Size: 385157 Color: 4
Size: 333072 Color: 2
Size: 281101 Color: 4

Bin 499: 672 of cap free
Amount of items: 2
Items: 
Size: 668253 Color: 2
Size: 331076 Color: 0

Bin 500: 681 of cap free
Amount of items: 3
Items: 
Size: 572374 Color: 4
Size: 213487 Color: 2
Size: 213459 Color: 2

Bin 501: 681 of cap free
Amount of items: 3
Items: 
Size: 603570 Color: 1
Size: 200786 Color: 4
Size: 194964 Color: 4

Bin 502: 682 of cap free
Amount of items: 2
Items: 
Size: 761203 Color: 0
Size: 238116 Color: 3

Bin 503: 692 of cap free
Amount of items: 2
Items: 
Size: 608168 Color: 3
Size: 391141 Color: 2

Bin 504: 694 of cap free
Amount of items: 3
Items: 
Size: 546268 Color: 4
Size: 232107 Color: 0
Size: 220932 Color: 2

Bin 505: 695 of cap free
Amount of items: 2
Items: 
Size: 750693 Color: 0
Size: 248613 Color: 4

Bin 506: 696 of cap free
Amount of items: 2
Items: 
Size: 781899 Color: 4
Size: 217406 Color: 0

Bin 507: 697 of cap free
Amount of items: 2
Items: 
Size: 600909 Color: 0
Size: 398395 Color: 4

Bin 508: 698 of cap free
Amount of items: 2
Items: 
Size: 577113 Color: 4
Size: 422190 Color: 2

Bin 509: 706 of cap free
Amount of items: 2
Items: 
Size: 744582 Color: 0
Size: 254713 Color: 3

Bin 510: 712 of cap free
Amount of items: 2
Items: 
Size: 537753 Color: 4
Size: 461536 Color: 1

Bin 511: 713 of cap free
Amount of items: 2
Items: 
Size: 677681 Color: 4
Size: 321607 Color: 1

Bin 512: 713 of cap free
Amount of items: 2
Items: 
Size: 695983 Color: 3
Size: 303305 Color: 2

Bin 513: 716 of cap free
Amount of items: 2
Items: 
Size: 756502 Color: 1
Size: 242783 Color: 2

Bin 514: 719 of cap free
Amount of items: 2
Items: 
Size: 758208 Color: 0
Size: 241074 Color: 3

Bin 515: 724 of cap free
Amount of items: 2
Items: 
Size: 542599 Color: 0
Size: 456678 Color: 1

Bin 516: 724 of cap free
Amount of items: 2
Items: 
Size: 690058 Color: 1
Size: 309219 Color: 2

Bin 517: 725 of cap free
Amount of items: 2
Items: 
Size: 594906 Color: 2
Size: 404370 Color: 1

Bin 518: 725 of cap free
Amount of items: 2
Items: 
Size: 732294 Color: 2
Size: 266982 Color: 3

Bin 519: 727 of cap free
Amount of items: 2
Items: 
Size: 574399 Color: 2
Size: 424875 Color: 0

Bin 520: 733 of cap free
Amount of items: 3
Items: 
Size: 354506 Color: 0
Size: 325139 Color: 3
Size: 319623 Color: 1

Bin 521: 734 of cap free
Amount of items: 2
Items: 
Size: 602387 Color: 3
Size: 396880 Color: 4

Bin 522: 736 of cap free
Amount of items: 2
Items: 
Size: 721074 Color: 3
Size: 278191 Color: 2

Bin 523: 741 of cap free
Amount of items: 2
Items: 
Size: 594016 Color: 0
Size: 405244 Color: 4

Bin 524: 745 of cap free
Amount of items: 2
Items: 
Size: 612875 Color: 4
Size: 386381 Color: 0

Bin 525: 745 of cap free
Amount of items: 3
Items: 
Size: 629583 Color: 1
Size: 185860 Color: 3
Size: 183813 Color: 2

Bin 526: 747 of cap free
Amount of items: 2
Items: 
Size: 746429 Color: 0
Size: 252825 Color: 4

Bin 527: 750 of cap free
Amount of items: 2
Items: 
Size: 773116 Color: 3
Size: 226135 Color: 4

Bin 528: 756 of cap free
Amount of items: 2
Items: 
Size: 745623 Color: 0
Size: 253622 Color: 3

Bin 529: 762 of cap free
Amount of items: 3
Items: 
Size: 723408 Color: 0
Size: 140371 Color: 4
Size: 135460 Color: 2

Bin 530: 771 of cap free
Amount of items: 2
Items: 
Size: 554109 Color: 4
Size: 445121 Color: 1

Bin 531: 782 of cap free
Amount of items: 2
Items: 
Size: 585895 Color: 0
Size: 413324 Color: 2

Bin 532: 782 of cap free
Amount of items: 2
Items: 
Size: 708657 Color: 2
Size: 290562 Color: 3

Bin 533: 785 of cap free
Amount of items: 3
Items: 
Size: 497399 Color: 2
Size: 251047 Color: 0
Size: 250770 Color: 2

Bin 534: 785 of cap free
Amount of items: 3
Items: 
Size: 645907 Color: 4
Size: 179465 Color: 1
Size: 173844 Color: 0

Bin 535: 787 of cap free
Amount of items: 2
Items: 
Size: 681063 Color: 4
Size: 318151 Color: 2

Bin 536: 788 of cap free
Amount of items: 2
Items: 
Size: 717950 Color: 2
Size: 281263 Color: 4

Bin 537: 796 of cap free
Amount of items: 2
Items: 
Size: 526269 Color: 3
Size: 472936 Color: 2

Bin 538: 804 of cap free
Amount of items: 2
Items: 
Size: 751807 Color: 1
Size: 247390 Color: 0

Bin 539: 815 of cap free
Amount of items: 2
Items: 
Size: 563058 Color: 2
Size: 436128 Color: 4

Bin 540: 817 of cap free
Amount of items: 2
Items: 
Size: 661795 Color: 2
Size: 337389 Color: 3

Bin 541: 820 of cap free
Amount of items: 2
Items: 
Size: 689640 Color: 0
Size: 309541 Color: 2

Bin 542: 822 of cap free
Amount of items: 2
Items: 
Size: 513814 Color: 2
Size: 485365 Color: 1

Bin 543: 822 of cap free
Amount of items: 2
Items: 
Size: 671137 Color: 4
Size: 328042 Color: 3

Bin 544: 823 of cap free
Amount of items: 2
Items: 
Size: 575735 Color: 3
Size: 423443 Color: 1

Bin 545: 825 of cap free
Amount of items: 2
Items: 
Size: 521871 Color: 0
Size: 477305 Color: 4

Bin 546: 828 of cap free
Amount of items: 3
Items: 
Size: 373859 Color: 4
Size: 341655 Color: 2
Size: 283659 Color: 1

Bin 547: 830 of cap free
Amount of items: 2
Items: 
Size: 702373 Color: 3
Size: 296798 Color: 1

Bin 548: 833 of cap free
Amount of items: 2
Items: 
Size: 719360 Color: 2
Size: 279808 Color: 3

Bin 549: 842 of cap free
Amount of items: 2
Items: 
Size: 577667 Color: 2
Size: 421492 Color: 4

Bin 550: 845 of cap free
Amount of items: 2
Items: 
Size: 592332 Color: 2
Size: 406824 Color: 4

Bin 551: 849 of cap free
Amount of items: 2
Items: 
Size: 573196 Color: 3
Size: 425956 Color: 0

Bin 552: 850 of cap free
Amount of items: 2
Items: 
Size: 540799 Color: 1
Size: 458352 Color: 3

Bin 553: 851 of cap free
Amount of items: 2
Items: 
Size: 573215 Color: 0
Size: 425935 Color: 4

Bin 554: 859 of cap free
Amount of items: 2
Items: 
Size: 627869 Color: 4
Size: 371273 Color: 3

Bin 555: 869 of cap free
Amount of items: 2
Items: 
Size: 520734 Color: 2
Size: 478398 Color: 0

Bin 556: 869 of cap free
Amount of items: 2
Items: 
Size: 639953 Color: 4
Size: 359179 Color: 0

Bin 557: 877 of cap free
Amount of items: 2
Items: 
Size: 797773 Color: 1
Size: 201351 Color: 2

Bin 558: 879 of cap free
Amount of items: 2
Items: 
Size: 733805 Color: 2
Size: 265317 Color: 1

Bin 559: 880 of cap free
Amount of items: 2
Items: 
Size: 684488 Color: 4
Size: 314633 Color: 2

Bin 560: 894 of cap free
Amount of items: 2
Items: 
Size: 530570 Color: 4
Size: 468537 Color: 0

Bin 561: 898 of cap free
Amount of items: 3
Items: 
Size: 685794 Color: 3
Size: 157010 Color: 4
Size: 156299 Color: 1

Bin 562: 901 of cap free
Amount of items: 2
Items: 
Size: 772806 Color: 2
Size: 226294 Color: 4

Bin 563: 902 of cap free
Amount of items: 2
Items: 
Size: 536593 Color: 3
Size: 462506 Color: 2

Bin 564: 907 of cap free
Amount of items: 2
Items: 
Size: 503266 Color: 0
Size: 495828 Color: 4

Bin 565: 909 of cap free
Amount of items: 2
Items: 
Size: 621936 Color: 3
Size: 377156 Color: 2

Bin 566: 916 of cap free
Amount of items: 3
Items: 
Size: 570751 Color: 0
Size: 214965 Color: 1
Size: 213369 Color: 3

Bin 567: 925 of cap free
Amount of items: 2
Items: 
Size: 554449 Color: 1
Size: 444627 Color: 0

Bin 568: 925 of cap free
Amount of items: 2
Items: 
Size: 699773 Color: 4
Size: 299303 Color: 0

Bin 569: 929 of cap free
Amount of items: 2
Items: 
Size: 767981 Color: 4
Size: 231091 Color: 3

Bin 570: 932 of cap free
Amount of items: 2
Items: 
Size: 778301 Color: 1
Size: 220768 Color: 3

Bin 571: 934 of cap free
Amount of items: 2
Items: 
Size: 734995 Color: 4
Size: 264072 Color: 0

Bin 572: 948 of cap free
Amount of items: 2
Items: 
Size: 668638 Color: 3
Size: 330415 Color: 4

Bin 573: 952 of cap free
Amount of items: 2
Items: 
Size: 706473 Color: 1
Size: 292576 Color: 0

Bin 574: 954 of cap free
Amount of items: 3
Items: 
Size: 533485 Color: 1
Size: 239659 Color: 0
Size: 225903 Color: 3

Bin 575: 959 of cap free
Amount of items: 2
Items: 
Size: 618323 Color: 4
Size: 380719 Color: 0

Bin 576: 961 of cap free
Amount of items: 2
Items: 
Size: 752625 Color: 4
Size: 246415 Color: 3

Bin 577: 971 of cap free
Amount of items: 2
Items: 
Size: 671139 Color: 2
Size: 327891 Color: 0

Bin 578: 980 of cap free
Amount of items: 3
Items: 
Size: 387064 Color: 4
Size: 339631 Color: 2
Size: 272326 Color: 1

Bin 579: 980 of cap free
Amount of items: 3
Items: 
Size: 610092 Color: 2
Size: 207798 Color: 1
Size: 181131 Color: 0

Bin 580: 991 of cap free
Amount of items: 2
Items: 
Size: 737389 Color: 4
Size: 261621 Color: 1

Bin 581: 992 of cap free
Amount of items: 3
Items: 
Size: 773282 Color: 3
Size: 112916 Color: 4
Size: 112811 Color: 0

Bin 582: 998 of cap free
Amount of items: 2
Items: 
Size: 733697 Color: 2
Size: 265306 Color: 0

Bin 583: 998 of cap free
Amount of items: 2
Items: 
Size: 741418 Color: 1
Size: 257585 Color: 0

Bin 584: 1000 of cap free
Amount of items: 2
Items: 
Size: 609318 Color: 4
Size: 389683 Color: 3

Bin 585: 1000 of cap free
Amount of items: 2
Items: 
Size: 661766 Color: 1
Size: 337235 Color: 2

Bin 586: 1003 of cap free
Amount of items: 3
Items: 
Size: 527953 Color: 2
Size: 236593 Color: 3
Size: 234452 Color: 1

Bin 587: 1003 of cap free
Amount of items: 2
Items: 
Size: 723861 Color: 2
Size: 275137 Color: 0

Bin 588: 1011 of cap free
Amount of items: 2
Items: 
Size: 517884 Color: 1
Size: 481106 Color: 2

Bin 589: 1011 of cap free
Amount of items: 2
Items: 
Size: 747516 Color: 4
Size: 251474 Color: 3

Bin 590: 1021 of cap free
Amount of items: 2
Items: 
Size: 785319 Color: 1
Size: 213661 Color: 4

Bin 591: 1045 of cap free
Amount of items: 3
Items: 
Size: 345157 Color: 1
Size: 341905 Color: 0
Size: 311894 Color: 2

Bin 592: 1047 of cap free
Amount of items: 2
Items: 
Size: 797750 Color: 4
Size: 201204 Color: 3

Bin 593: 1049 of cap free
Amount of items: 3
Items: 
Size: 673075 Color: 1
Size: 166759 Color: 3
Size: 159118 Color: 0

Bin 594: 1053 of cap free
Amount of items: 2
Items: 
Size: 643736 Color: 2
Size: 355212 Color: 1

Bin 595: 1064 of cap free
Amount of items: 2
Items: 
Size: 505449 Color: 4
Size: 493488 Color: 2

Bin 596: 1064 of cap free
Amount of items: 2
Items: 
Size: 528303 Color: 1
Size: 470634 Color: 3

Bin 597: 1086 of cap free
Amount of items: 2
Items: 
Size: 562871 Color: 2
Size: 436044 Color: 3

Bin 598: 1089 of cap free
Amount of items: 2
Items: 
Size: 538123 Color: 4
Size: 460789 Color: 0

Bin 599: 1090 of cap free
Amount of items: 2
Items: 
Size: 630915 Color: 4
Size: 367996 Color: 0

Bin 600: 1095 of cap free
Amount of items: 2
Items: 
Size: 533010 Color: 3
Size: 465896 Color: 0

Bin 601: 1098 of cap free
Amount of items: 3
Items: 
Size: 718560 Color: 4
Size: 143366 Color: 4
Size: 136977 Color: 2

Bin 602: 1100 of cap free
Amount of items: 2
Items: 
Size: 550927 Color: 2
Size: 447974 Color: 0

Bin 603: 1101 of cap free
Amount of items: 2
Items: 
Size: 545848 Color: 4
Size: 453052 Color: 1

Bin 604: 1101 of cap free
Amount of items: 2
Items: 
Size: 706235 Color: 4
Size: 292665 Color: 3

Bin 605: 1105 of cap free
Amount of items: 2
Items: 
Size: 623706 Color: 4
Size: 375190 Color: 3

Bin 606: 1127 of cap free
Amount of items: 2
Items: 
Size: 697184 Color: 0
Size: 301690 Color: 4

Bin 607: 1129 of cap free
Amount of items: 2
Items: 
Size: 650473 Color: 0
Size: 348399 Color: 1

Bin 608: 1129 of cap free
Amount of items: 2
Items: 
Size: 665853 Color: 2
Size: 333019 Color: 3

Bin 609: 1136 of cap free
Amount of items: 2
Items: 
Size: 558127 Color: 2
Size: 440738 Color: 1

Bin 610: 1138 of cap free
Amount of items: 3
Items: 
Size: 385013 Color: 4
Size: 339576 Color: 2
Size: 274274 Color: 1

Bin 611: 1153 of cap free
Amount of items: 3
Items: 
Size: 624411 Color: 3
Size: 191372 Color: 1
Size: 183065 Color: 0

Bin 612: 1155 of cap free
Amount of items: 2
Items: 
Size: 636462 Color: 0
Size: 362384 Color: 1

Bin 613: 1164 of cap free
Amount of items: 2
Items: 
Size: 799440 Color: 1
Size: 199397 Color: 4

Bin 614: 1176 of cap free
Amount of items: 2
Items: 
Size: 568072 Color: 2
Size: 430753 Color: 4

Bin 615: 1195 of cap free
Amount of items: 2
Items: 
Size: 545604 Color: 3
Size: 453202 Color: 4

Bin 616: 1201 of cap free
Amount of items: 2
Items: 
Size: 706257 Color: 2
Size: 292543 Color: 4

Bin 617: 1208 of cap free
Amount of items: 2
Items: 
Size: 518442 Color: 1
Size: 480351 Color: 4

Bin 618: 1211 of cap free
Amount of items: 2
Items: 
Size: 672006 Color: 0
Size: 326784 Color: 2

Bin 619: 1213 of cap free
Amount of items: 2
Items: 
Size: 795436 Color: 3
Size: 203352 Color: 4

Bin 620: 1225 of cap free
Amount of items: 2
Items: 
Size: 596777 Color: 3
Size: 401999 Color: 1

Bin 621: 1231 of cap free
Amount of items: 2
Items: 
Size: 516578 Color: 3
Size: 482192 Color: 2

Bin 622: 1245 of cap free
Amount of items: 2
Items: 
Size: 561150 Color: 0
Size: 437606 Color: 2

Bin 623: 1265 of cap free
Amount of items: 2
Items: 
Size: 550867 Color: 4
Size: 447869 Color: 1

Bin 624: 1276 of cap free
Amount of items: 3
Items: 
Size: 353897 Color: 2
Size: 326749 Color: 3
Size: 318079 Color: 4

Bin 625: 1291 of cap free
Amount of items: 2
Items: 
Size: 541361 Color: 2
Size: 457349 Color: 0

Bin 626: 1311 of cap free
Amount of items: 2
Items: 
Size: 510712 Color: 4
Size: 487978 Color: 3

Bin 627: 1325 of cap free
Amount of items: 2
Items: 
Size: 505883 Color: 2
Size: 492793 Color: 4

Bin 628: 1331 of cap free
Amount of items: 3
Items: 
Size: 637608 Color: 4
Size: 183527 Color: 1
Size: 177535 Color: 0

Bin 629: 1340 of cap free
Amount of items: 2
Items: 
Size: 714991 Color: 3
Size: 283670 Color: 2

Bin 630: 1351 of cap free
Amount of items: 2
Items: 
Size: 569751 Color: 4
Size: 428899 Color: 3

Bin 631: 1352 of cap free
Amount of items: 2
Items: 
Size: 734198 Color: 1
Size: 264451 Color: 3

Bin 632: 1361 of cap free
Amount of items: 2
Items: 
Size: 714861 Color: 0
Size: 283779 Color: 4

Bin 633: 1372 of cap free
Amount of items: 2
Items: 
Size: 632354 Color: 2
Size: 366275 Color: 3

Bin 634: 1383 of cap free
Amount of items: 2
Items: 
Size: 577056 Color: 1
Size: 421562 Color: 2

Bin 635: 1389 of cap free
Amount of items: 2
Items: 
Size: 507226 Color: 0
Size: 491386 Color: 3

Bin 636: 1389 of cap free
Amount of items: 2
Items: 
Size: 714668 Color: 1
Size: 283944 Color: 3

Bin 637: 1399 of cap free
Amount of items: 2
Items: 
Size: 730983 Color: 2
Size: 267619 Color: 0

Bin 638: 1413 of cap free
Amount of items: 2
Items: 
Size: 522547 Color: 0
Size: 476041 Color: 2

Bin 639: 1416 of cap free
Amount of items: 2
Items: 
Size: 593726 Color: 0
Size: 404859 Color: 4

Bin 640: 1424 of cap free
Amount of items: 2
Items: 
Size: 675538 Color: 2
Size: 323039 Color: 3

Bin 641: 1430 of cap free
Amount of items: 2
Items: 
Size: 513617 Color: 2
Size: 484954 Color: 0

Bin 642: 1438 of cap free
Amount of items: 2
Items: 
Size: 690924 Color: 3
Size: 307639 Color: 4

Bin 643: 1446 of cap free
Amount of items: 2
Items: 
Size: 618134 Color: 1
Size: 380421 Color: 3

Bin 644: 1447 of cap free
Amount of items: 2
Items: 
Size: 630658 Color: 3
Size: 367896 Color: 2

Bin 645: 1458 of cap free
Amount of items: 2
Items: 
Size: 546020 Color: 3
Size: 452523 Color: 0

Bin 646: 1461 of cap free
Amount of items: 2
Items: 
Size: 722989 Color: 1
Size: 275551 Color: 3

Bin 647: 1462 of cap free
Amount of items: 2
Items: 
Size: 614990 Color: 3
Size: 383549 Color: 0

Bin 648: 1462 of cap free
Amount of items: 2
Items: 
Size: 643033 Color: 4
Size: 355506 Color: 0

Bin 649: 1498 of cap free
Amount of items: 2
Items: 
Size: 649511 Color: 2
Size: 348992 Color: 3

Bin 650: 1501 of cap free
Amount of items: 2
Items: 
Size: 575459 Color: 1
Size: 423041 Color: 2

Bin 651: 1510 of cap free
Amount of items: 2
Items: 
Size: 609054 Color: 0
Size: 389437 Color: 4

Bin 652: 1515 of cap free
Amount of items: 3
Items: 
Size: 751979 Color: 3
Size: 123519 Color: 2
Size: 122988 Color: 2

Bin 653: 1520 of cap free
Amount of items: 3
Items: 
Size: 450306 Color: 1
Size: 294382 Color: 3
Size: 253793 Color: 0

Bin 654: 1523 of cap free
Amount of items: 2
Items: 
Size: 790622 Color: 4
Size: 207856 Color: 1

Bin 655: 1527 of cap free
Amount of items: 3
Items: 
Size: 669054 Color: 3
Size: 168644 Color: 4
Size: 160776 Color: 1

Bin 656: 1543 of cap free
Amount of items: 2
Items: 
Size: 752576 Color: 0
Size: 245882 Color: 1

Bin 657: 1551 of cap free
Amount of items: 2
Items: 
Size: 665238 Color: 4
Size: 333212 Color: 2

Bin 658: 1570 of cap free
Amount of items: 2
Items: 
Size: 658489 Color: 4
Size: 339942 Color: 0

Bin 659: 1579 of cap free
Amount of items: 2
Items: 
Size: 699916 Color: 2
Size: 298506 Color: 4

Bin 660: 1586 of cap free
Amount of items: 2
Items: 
Size: 774057 Color: 2
Size: 224358 Color: 4

Bin 661: 1589 of cap free
Amount of items: 2
Items: 
Size: 617188 Color: 0
Size: 381224 Color: 1

Bin 662: 1593 of cap free
Amount of items: 3
Items: 
Size: 482964 Color: 0
Size: 261612 Color: 1
Size: 253832 Color: 2

Bin 663: 1593 of cap free
Amount of items: 2
Items: 
Size: 772267 Color: 2
Size: 226141 Color: 3

Bin 664: 1603 of cap free
Amount of items: 2
Items: 
Size: 756199 Color: 0
Size: 242199 Color: 3

Bin 665: 1622 of cap free
Amount of items: 2
Items: 
Size: 690597 Color: 2
Size: 307782 Color: 1

Bin 666: 1634 of cap free
Amount of items: 2
Items: 
Size: 532345 Color: 1
Size: 466022 Color: 4

Bin 667: 1643 of cap free
Amount of items: 2
Items: 
Size: 673155 Color: 2
Size: 325203 Color: 1

Bin 668: 1650 of cap free
Amount of items: 2
Items: 
Size: 687137 Color: 2
Size: 311214 Color: 3

Bin 669: 1656 of cap free
Amount of items: 3
Items: 
Size: 557745 Color: 1
Size: 222477 Color: 0
Size: 218123 Color: 3

Bin 670: 1667 of cap free
Amount of items: 2
Items: 
Size: 686002 Color: 2
Size: 312332 Color: 4

Bin 671: 1683 of cap free
Amount of items: 2
Items: 
Size: 675832 Color: 2
Size: 322486 Color: 4

Bin 672: 1689 of cap free
Amount of items: 2
Items: 
Size: 616633 Color: 3
Size: 381679 Color: 0

Bin 673: 1689 of cap free
Amount of items: 2
Items: 
Size: 676898 Color: 4
Size: 321414 Color: 0

Bin 674: 1710 of cap free
Amount of items: 2
Items: 
Size: 618223 Color: 0
Size: 380068 Color: 4

Bin 675: 1720 of cap free
Amount of items: 3
Items: 
Size: 569388 Color: 1
Size: 215247 Color: 1
Size: 213646 Color: 3

Bin 676: 1721 of cap free
Amount of items: 2
Items: 
Size: 781240 Color: 1
Size: 217040 Color: 0

Bin 677: 1725 of cap free
Amount of items: 2
Items: 
Size: 697000 Color: 1
Size: 301276 Color: 4

Bin 678: 1729 of cap free
Amount of items: 2
Items: 
Size: 661411 Color: 1
Size: 336861 Color: 3

Bin 679: 1734 of cap free
Amount of items: 3
Items: 
Size: 497306 Color: 3
Size: 250564 Color: 2
Size: 250397 Color: 3

Bin 680: 1740 of cap free
Amount of items: 2
Items: 
Size: 548147 Color: 1
Size: 450114 Color: 3

Bin 681: 1757 of cap free
Amount of items: 2
Items: 
Size: 761512 Color: 2
Size: 236732 Color: 1

Bin 682: 1767 of cap free
Amount of items: 2
Items: 
Size: 580725 Color: 1
Size: 417509 Color: 3

Bin 683: 1769 of cap free
Amount of items: 2
Items: 
Size: 552791 Color: 1
Size: 445441 Color: 4

Bin 684: 1780 of cap free
Amount of items: 2
Items: 
Size: 621980 Color: 2
Size: 376241 Color: 4

Bin 685: 1791 of cap free
Amount of items: 2
Items: 
Size: 598483 Color: 0
Size: 399727 Color: 4

Bin 686: 1819 of cap free
Amount of items: 2
Items: 
Size: 743515 Color: 4
Size: 254667 Color: 2

Bin 687: 1845 of cap free
Amount of items: 2
Items: 
Size: 644715 Color: 4
Size: 353441 Color: 3

Bin 688: 1872 of cap free
Amount of items: 2
Items: 
Size: 589488 Color: 3
Size: 408641 Color: 1

Bin 689: 1881 of cap free
Amount of items: 2
Items: 
Size: 663533 Color: 0
Size: 334587 Color: 3

Bin 690: 1883 of cap free
Amount of items: 2
Items: 
Size: 572426 Color: 3
Size: 425692 Color: 4

Bin 691: 1904 of cap free
Amount of items: 2
Items: 
Size: 593266 Color: 0
Size: 404831 Color: 4

Bin 692: 1912 of cap free
Amount of items: 2
Items: 
Size: 654319 Color: 0
Size: 343770 Color: 4

Bin 693: 1944 of cap free
Amount of items: 2
Items: 
Size: 565035 Color: 4
Size: 433022 Color: 1

Bin 694: 1948 of cap free
Amount of items: 3
Items: 
Size: 547751 Color: 0
Size: 234380 Color: 0
Size: 215922 Color: 3

Bin 695: 1962 of cap free
Amount of items: 2
Items: 
Size: 703610 Color: 2
Size: 294429 Color: 0

Bin 696: 1980 of cap free
Amount of items: 2
Items: 
Size: 577419 Color: 3
Size: 420602 Color: 1

Bin 697: 2000 of cap free
Amount of items: 2
Items: 
Size: 685032 Color: 0
Size: 312969 Color: 1

Bin 698: 2027 of cap free
Amount of items: 2
Items: 
Size: 696922 Color: 2
Size: 301052 Color: 0

Bin 699: 2036 of cap free
Amount of items: 2
Items: 
Size: 588746 Color: 3
Size: 409219 Color: 2

Bin 700: 2061 of cap free
Amount of items: 2
Items: 
Size: 786596 Color: 2
Size: 211344 Color: 1

Bin 701: 2069 of cap free
Amount of items: 2
Items: 
Size: 558052 Color: 2
Size: 439880 Color: 1

Bin 702: 2109 of cap free
Amount of items: 2
Items: 
Size: 663960 Color: 0
Size: 333932 Color: 4

Bin 703: 2130 of cap free
Amount of items: 2
Items: 
Size: 679807 Color: 1
Size: 318064 Color: 3

Bin 704: 2152 of cap free
Amount of items: 2
Items: 
Size: 606500 Color: 4
Size: 391349 Color: 3

Bin 705: 2159 of cap free
Amount of items: 2
Items: 
Size: 781304 Color: 3
Size: 216538 Color: 0

Bin 706: 2173 of cap free
Amount of items: 3
Items: 
Size: 729753 Color: 1
Size: 137405 Color: 3
Size: 130670 Color: 4

Bin 707: 2185 of cap free
Amount of items: 2
Items: 
Size: 500588 Color: 3
Size: 497228 Color: 4

Bin 708: 2190 of cap free
Amount of items: 2
Items: 
Size: 608327 Color: 2
Size: 389484 Color: 1

Bin 709: 2190 of cap free
Amount of items: 3
Items: 
Size: 698977 Color: 3
Size: 150026 Color: 1
Size: 148808 Color: 2

Bin 710: 2196 of cap free
Amount of items: 2
Items: 
Size: 550185 Color: 1
Size: 447620 Color: 3

Bin 711: 2241 of cap free
Amount of items: 2
Items: 
Size: 794837 Color: 0
Size: 202923 Color: 1

Bin 712: 2259 of cap free
Amount of items: 2
Items: 
Size: 569549 Color: 0
Size: 428193 Color: 2

Bin 713: 2275 of cap free
Amount of items: 2
Items: 
Size: 573081 Color: 0
Size: 424645 Color: 3

Bin 714: 2283 of cap free
Amount of items: 3
Items: 
Size: 529012 Color: 0
Size: 236319 Color: 1
Size: 232387 Color: 3

Bin 715: 2288 of cap free
Amount of items: 2
Items: 
Size: 556246 Color: 1
Size: 441467 Color: 0

Bin 716: 2312 of cap free
Amount of items: 2
Items: 
Size: 552817 Color: 4
Size: 444872 Color: 2

Bin 717: 2319 of cap free
Amount of items: 2
Items: 
Size: 639428 Color: 4
Size: 358254 Color: 1

Bin 718: 2328 of cap free
Amount of items: 2
Items: 
Size: 503250 Color: 3
Size: 494423 Color: 1

Bin 719: 2361 of cap free
Amount of items: 2
Items: 
Size: 779461 Color: 0
Size: 218179 Color: 3

Bin 720: 2369 of cap free
Amount of items: 2
Items: 
Size: 720453 Color: 3
Size: 277179 Color: 1

Bin 721: 2410 of cap free
Amount of items: 2
Items: 
Size: 525717 Color: 1
Size: 471874 Color: 0

Bin 722: 2421 of cap free
Amount of items: 2
Items: 
Size: 642646 Color: 3
Size: 354934 Color: 4

Bin 723: 2457 of cap free
Amount of items: 2
Items: 
Size: 540588 Color: 3
Size: 456956 Color: 2

Bin 724: 2472 of cap free
Amount of items: 2
Items: 
Size: 751759 Color: 2
Size: 245770 Color: 1

Bin 725: 2483 of cap free
Amount of items: 2
Items: 
Size: 532089 Color: 4
Size: 465429 Color: 0

Bin 726: 2484 of cap free
Amount of items: 2
Items: 
Size: 513605 Color: 1
Size: 483912 Color: 4

Bin 727: 2484 of cap free
Amount of items: 2
Items: 
Size: 699029 Color: 4
Size: 298488 Color: 2

Bin 728: 2502 of cap free
Amount of items: 2
Items: 
Size: 552763 Color: 3
Size: 444736 Color: 2

Bin 729: 2529 of cap free
Amount of items: 2
Items: 
Size: 696488 Color: 2
Size: 300984 Color: 4

Bin 730: 2553 of cap free
Amount of items: 2
Items: 
Size: 520892 Color: 0
Size: 476556 Color: 1

Bin 731: 2573 of cap free
Amount of items: 2
Items: 
Size: 658315 Color: 4
Size: 339113 Color: 1

Bin 732: 2587 of cap free
Amount of items: 2
Items: 
Size: 569364 Color: 3
Size: 428050 Color: 0

Bin 733: 2638 of cap free
Amount of items: 2
Items: 
Size: 557550 Color: 2
Size: 439813 Color: 1

Bin 734: 2639 of cap free
Amount of items: 2
Items: 
Size: 572491 Color: 4
Size: 424871 Color: 2

Bin 735: 2677 of cap free
Amount of items: 2
Items: 
Size: 688853 Color: 4
Size: 308471 Color: 3

Bin 736: 2694 of cap free
Amount of items: 2
Items: 
Size: 531824 Color: 2
Size: 465483 Color: 4

Bin 737: 2695 of cap free
Amount of items: 2
Items: 
Size: 562584 Color: 4
Size: 434722 Color: 2

Bin 738: 2752 of cap free
Amount of items: 2
Items: 
Size: 540536 Color: 3
Size: 456713 Color: 0

Bin 739: 2764 of cap free
Amount of items: 2
Items: 
Size: 513490 Color: 0
Size: 483747 Color: 2

Bin 740: 2800 of cap free
Amount of items: 2
Items: 
Size: 781096 Color: 2
Size: 216105 Color: 0

Bin 741: 2812 of cap free
Amount of items: 2
Items: 
Size: 601883 Color: 4
Size: 395306 Color: 0

Bin 742: 2814 of cap free
Amount of items: 2
Items: 
Size: 709465 Color: 0
Size: 287722 Color: 4

Bin 743: 2871 of cap free
Amount of items: 3
Items: 
Size: 577053 Color: 2
Size: 210375 Color: 3
Size: 209702 Color: 0

Bin 744: 2893 of cap free
Amount of items: 2
Items: 
Size: 771121 Color: 3
Size: 225987 Color: 4

Bin 745: 2906 of cap free
Amount of items: 2
Items: 
Size: 577111 Color: 2
Size: 419984 Color: 1

Bin 746: 2926 of cap free
Amount of items: 2
Items: 
Size: 520773 Color: 3
Size: 476302 Color: 1

Bin 747: 2957 of cap free
Amount of items: 2
Items: 
Size: 561770 Color: 3
Size: 435274 Color: 4

Bin 748: 2958 of cap free
Amount of items: 2
Items: 
Size: 540246 Color: 1
Size: 456797 Color: 3

Bin 749: 2979 of cap free
Amount of items: 2
Items: 
Size: 642707 Color: 0
Size: 354315 Color: 1

Bin 750: 2982 of cap free
Amount of items: 2
Items: 
Size: 592220 Color: 4
Size: 404799 Color: 3

Bin 751: 2988 of cap free
Amount of items: 2
Items: 
Size: 626385 Color: 2
Size: 370628 Color: 3

Bin 752: 3012 of cap free
Amount of items: 2
Items: 
Size: 561168 Color: 0
Size: 435821 Color: 1

Bin 753: 3016 of cap free
Amount of items: 2
Items: 
Size: 598645 Color: 4
Size: 398340 Color: 3

Bin 754: 3083 of cap free
Amount of items: 2
Items: 
Size: 601801 Color: 3
Size: 395117 Color: 1

Bin 755: 3095 of cap free
Amount of items: 2
Items: 
Size: 704640 Color: 3
Size: 292266 Color: 4

Bin 756: 3104 of cap free
Amount of items: 2
Items: 
Size: 661649 Color: 2
Size: 335248 Color: 0

Bin 757: 3109 of cap free
Amount of items: 2
Items: 
Size: 639031 Color: 2
Size: 357861 Color: 1

Bin 758: 3151 of cap free
Amount of items: 2
Items: 
Size: 563738 Color: 1
Size: 433112 Color: 0

Bin 759: 3162 of cap free
Amount of items: 2
Items: 
Size: 666950 Color: 2
Size: 329889 Color: 1

Bin 760: 3192 of cap free
Amount of items: 2
Items: 
Size: 689207 Color: 1
Size: 307602 Color: 2

Bin 761: 3256 of cap free
Amount of items: 2
Items: 
Size: 604848 Color: 1
Size: 391897 Color: 4

Bin 762: 3290 of cap free
Amount of items: 2
Items: 
Size: 540355 Color: 4
Size: 456356 Color: 3

Bin 763: 3302 of cap free
Amount of items: 2
Items: 
Size: 675632 Color: 1
Size: 321067 Color: 4

Bin 764: 3357 of cap free
Amount of items: 3
Items: 
Size: 387922 Color: 0
Size: 331862 Color: 2
Size: 276860 Color: 1

Bin 765: 3437 of cap free
Amount of items: 2
Items: 
Size: 511680 Color: 3
Size: 484884 Color: 0

Bin 766: 3491 of cap free
Amount of items: 2
Items: 
Size: 628785 Color: 1
Size: 367725 Color: 0

Bin 767: 3525 of cap free
Amount of items: 2
Items: 
Size: 598329 Color: 0
Size: 398147 Color: 3

Bin 768: 3536 of cap free
Amount of items: 2
Items: 
Size: 557765 Color: 3
Size: 438700 Color: 0

Bin 769: 3555 of cap free
Amount of items: 2
Items: 
Size: 727062 Color: 2
Size: 269384 Color: 3

Bin 770: 3559 of cap free
Amount of items: 2
Items: 
Size: 761182 Color: 1
Size: 235260 Color: 2

Bin 771: 3565 of cap free
Amount of items: 2
Items: 
Size: 532724 Color: 4
Size: 463712 Color: 3

Bin 772: 3652 of cap free
Amount of items: 3
Items: 
Size: 451904 Color: 3
Size: 285554 Color: 2
Size: 258891 Color: 0

Bin 773: 3702 of cap free
Amount of items: 2
Items: 
Size: 780609 Color: 3
Size: 215690 Color: 2

Bin 774: 3719 of cap free
Amount of items: 2
Items: 
Size: 592155 Color: 3
Size: 404127 Color: 2

Bin 775: 3721 of cap free
Amount of items: 3
Items: 
Size: 600770 Color: 1
Size: 197763 Color: 4
Size: 197747 Color: 2

Bin 776: 3734 of cap free
Amount of items: 2
Items: 
Size: 741569 Color: 1
Size: 254698 Color: 3

Bin 777: 3774 of cap free
Amount of items: 2
Items: 
Size: 684915 Color: 2
Size: 311312 Color: 1

Bin 778: 3835 of cap free
Amount of items: 2
Items: 
Size: 513297 Color: 0
Size: 482869 Color: 3

Bin 779: 3982 of cap free
Amount of items: 2
Items: 
Size: 649115 Color: 4
Size: 346904 Color: 0

Bin 780: 4070 of cap free
Amount of items: 2
Items: 
Size: 630045 Color: 0
Size: 365886 Color: 2

Bin 781: 4091 of cap free
Amount of items: 2
Items: 
Size: 635116 Color: 4
Size: 360794 Color: 3

Bin 782: 4208 of cap free
Amount of items: 2
Items: 
Size: 675276 Color: 0
Size: 320517 Color: 2

Bin 783: 4253 of cap free
Amount of items: 2
Items: 
Size: 780302 Color: 2
Size: 215446 Color: 0

Bin 784: 4271 of cap free
Amount of items: 2
Items: 
Size: 703468 Color: 3
Size: 292262 Color: 2

Bin 785: 4278 of cap free
Amount of items: 2
Items: 
Size: 732577 Color: 2
Size: 263146 Color: 4

Bin 786: 4384 of cap free
Amount of items: 2
Items: 
Size: 621351 Color: 4
Size: 374266 Color: 1

Bin 787: 4439 of cap free
Amount of items: 2
Items: 
Size: 703745 Color: 2
Size: 291817 Color: 4

Bin 788: 4538 of cap free
Amount of items: 2
Items: 
Size: 771113 Color: 3
Size: 224350 Color: 1

Bin 789: 4582 of cap free
Amount of items: 2
Items: 
Size: 591387 Color: 3
Size: 404032 Color: 0

Bin 790: 4586 of cap free
Amount of items: 2
Items: 
Size: 512887 Color: 3
Size: 482528 Color: 1

Bin 791: 4607 of cap free
Amount of items: 2
Items: 
Size: 561765 Color: 4
Size: 433629 Color: 3

Bin 792: 4647 of cap free
Amount of items: 2
Items: 
Size: 666185 Color: 2
Size: 329169 Color: 0

Bin 793: 4698 of cap free
Amount of items: 3
Items: 
Size: 353523 Color: 0
Size: 351763 Color: 3
Size: 290017 Color: 1

Bin 794: 4703 of cap free
Amount of items: 2
Items: 
Size: 557056 Color: 4
Size: 438242 Color: 0

Bin 795: 4707 of cap free
Amount of items: 2
Items: 
Size: 538587 Color: 1
Size: 456707 Color: 4

Bin 796: 4813 of cap free
Amount of items: 2
Items: 
Size: 670444 Color: 0
Size: 324744 Color: 4

Bin 797: 4874 of cap free
Amount of items: 2
Items: 
Size: 556118 Color: 4
Size: 439009 Color: 3

Bin 798: 5069 of cap free
Amount of items: 2
Items: 
Size: 598063 Color: 2
Size: 396869 Color: 4

Bin 799: 5265 of cap free
Amount of items: 3
Items: 
Size: 475621 Color: 0
Size: 268962 Color: 2
Size: 250153 Color: 1

Bin 800: 5279 of cap free
Amount of items: 3
Items: 
Size: 698616 Color: 3
Size: 148787 Color: 1
Size: 147319 Color: 0

Bin 801: 5387 of cap free
Amount of items: 2
Items: 
Size: 696310 Color: 4
Size: 298304 Color: 0

Bin 802: 5406 of cap free
Amount of items: 2
Items: 
Size: 693726 Color: 3
Size: 300869 Color: 4

Bin 803: 5439 of cap free
Amount of items: 2
Items: 
Size: 512246 Color: 0
Size: 482316 Color: 1

Bin 804: 5524 of cap free
Amount of items: 2
Items: 
Size: 661359 Color: 0
Size: 333118 Color: 2

Bin 805: 5536 of cap free
Amount of items: 2
Items: 
Size: 684601 Color: 0
Size: 309864 Color: 1

Bin 806: 5551 of cap free
Amount of items: 2
Items: 
Size: 661314 Color: 4
Size: 333136 Color: 2

Bin 807: 5567 of cap free
Amount of items: 2
Items: 
Size: 717166 Color: 2
Size: 277268 Color: 3

Bin 808: 5653 of cap free
Amount of items: 2
Items: 
Size: 563635 Color: 1
Size: 430713 Color: 0

Bin 809: 5794 of cap free
Amount of items: 2
Items: 
Size: 661190 Color: 0
Size: 333017 Color: 3

Bin 810: 5823 of cap free
Amount of items: 3
Items: 
Size: 375001 Color: 0
Size: 327018 Color: 3
Size: 292159 Color: 0

Bin 811: 5957 of cap free
Amount of items: 2
Items: 
Size: 751483 Color: 4
Size: 242561 Color: 2

Bin 812: 5975 of cap free
Amount of items: 2
Items: 
Size: 605686 Color: 0
Size: 388340 Color: 1

Bin 813: 6033 of cap free
Amount of items: 2
Items: 
Size: 751746 Color: 3
Size: 242222 Color: 2

Bin 814: 6036 of cap free
Amount of items: 2
Items: 
Size: 696405 Color: 2
Size: 297560 Color: 3

Bin 815: 6039 of cap free
Amount of items: 2
Items: 
Size: 530981 Color: 1
Size: 462981 Color: 3

Bin 816: 6040 of cap free
Amount of items: 3
Items: 
Size: 515652 Color: 1
Size: 239388 Color: 0
Size: 238921 Color: 4

Bin 817: 6084 of cap free
Amount of items: 2
Items: 
Size: 549921 Color: 2
Size: 443996 Color: 1

Bin 818: 6464 of cap free
Amount of items: 2
Items: 
Size: 512359 Color: 2
Size: 481178 Color: 1

Bin 819: 6740 of cap free
Amount of items: 2
Items: 
Size: 620532 Color: 4
Size: 372729 Color: 1

Bin 820: 6857 of cap free
Amount of items: 2
Items: 
Size: 696179 Color: 4
Size: 296965 Color: 3

Bin 821: 6864 of cap free
Amount of items: 2
Items: 
Size: 752163 Color: 0
Size: 240974 Color: 3

Bin 822: 6883 of cap free
Amount of items: 2
Items: 
Size: 724499 Color: 4
Size: 268619 Color: 3

Bin 823: 6948 of cap free
Amount of items: 2
Items: 
Size: 793831 Color: 4
Size: 199222 Color: 2

Bin 824: 6999 of cap free
Amount of items: 2
Items: 
Size: 510460 Color: 4
Size: 482542 Color: 3

Bin 825: 7063 of cap free
Amount of items: 2
Items: 
Size: 597263 Color: 2
Size: 395675 Color: 4

Bin 826: 7079 of cap free
Amount of items: 2
Items: 
Size: 511966 Color: 0
Size: 480956 Color: 1

Bin 827: 7088 of cap free
Amount of items: 2
Items: 
Size: 796664 Color: 4
Size: 196249 Color: 3

Bin 828: 7501 of cap free
Amount of items: 2
Items: 
Size: 511683 Color: 0
Size: 480817 Color: 3

Bin 829: 7750 of cap free
Amount of items: 2
Items: 
Size: 695291 Color: 4
Size: 296960 Color: 3

Bin 830: 7806 of cap free
Amount of items: 2
Items: 
Size: 660405 Color: 2
Size: 331790 Color: 3

Bin 831: 7878 of cap free
Amount of items: 3
Items: 
Size: 617602 Color: 4
Size: 192741 Color: 3
Size: 181780 Color: 2

Bin 832: 7897 of cap free
Amount of items: 2
Items: 
Size: 626918 Color: 3
Size: 365186 Color: 2

Bin 833: 8011 of cap free
Amount of items: 2
Items: 
Size: 596481 Color: 3
Size: 395509 Color: 4

Bin 834: 8049 of cap free
Amount of items: 2
Items: 
Size: 795944 Color: 4
Size: 196008 Color: 3

Bin 835: 8183 of cap free
Amount of items: 2
Items: 
Size: 618067 Color: 1
Size: 373751 Color: 3

Bin 836: 8207 of cap free
Amount of items: 2
Items: 
Size: 540382 Color: 4
Size: 451412 Color: 0

Bin 837: 8425 of cap free
Amount of items: 2
Items: 
Size: 684106 Color: 1
Size: 307470 Color: 3

Bin 838: 8451 of cap free
Amount of items: 2
Items: 
Size: 506709 Color: 1
Size: 484841 Color: 0

Bin 839: 8538 of cap free
Amount of items: 2
Items: 
Size: 496776 Color: 3
Size: 494687 Color: 0

Bin 840: 8596 of cap free
Amount of items: 2
Items: 
Size: 749944 Color: 0
Size: 241461 Color: 1

Bin 841: 8616 of cap free
Amount of items: 2
Items: 
Size: 750594 Color: 3
Size: 240791 Color: 4

Bin 842: 8841 of cap free
Amount of items: 2
Items: 
Size: 581438 Color: 2
Size: 409722 Color: 3

Bin 843: 8915 of cap free
Amount of items: 3
Items: 
Size: 455540 Color: 1
Size: 268575 Color: 2
Size: 266971 Color: 2

Bin 844: 9138 of cap free
Amount of items: 3
Items: 
Size: 352015 Color: 4
Size: 348850 Color: 3
Size: 289998 Color: 1

Bin 845: 9148 of cap free
Amount of items: 2
Items: 
Size: 795372 Color: 1
Size: 195481 Color: 3

Bin 846: 9614 of cap free
Amount of items: 3
Items: 
Size: 352226 Color: 3
Size: 319841 Color: 4
Size: 318320 Color: 4

Bin 847: 9655 of cap free
Amount of items: 2
Items: 
Size: 684267 Color: 2
Size: 306079 Color: 1

Bin 848: 10320 of cap free
Amount of items: 3
Items: 
Size: 596806 Color: 1
Size: 196669 Color: 4
Size: 196206 Color: 4

Bin 849: 10481 of cap free
Amount of items: 2
Items: 
Size: 683785 Color: 1
Size: 305735 Color: 3

Bin 850: 10540 of cap free
Amount of items: 2
Items: 
Size: 703606 Color: 3
Size: 285855 Color: 4

Bin 851: 10686 of cap free
Amount of items: 2
Items: 
Size: 527344 Color: 1
Size: 461971 Color: 4

Bin 852: 11080 of cap free
Amount of items: 2
Items: 
Size: 509034 Color: 0
Size: 479887 Color: 3

Bin 853: 11142 of cap free
Amount of items: 2
Items: 
Size: 793527 Color: 0
Size: 195332 Color: 3

Bin 854: 11288 of cap free
Amount of items: 2
Items: 
Size: 580075 Color: 2
Size: 408638 Color: 3

Bin 855: 11866 of cap free
Amount of items: 2
Items: 
Size: 579996 Color: 0
Size: 408139 Color: 3

Bin 856: 12653 of cap free
Amount of items: 2
Items: 
Size: 614766 Color: 2
Size: 372582 Color: 1

Bin 857: 13358 of cap free
Amount of items: 2
Items: 
Size: 688644 Color: 1
Size: 297999 Color: 0

Bin 858: 13431 of cap free
Amount of items: 2
Items: 
Size: 511332 Color: 2
Size: 475238 Color: 4

Bin 859: 13991 of cap free
Amount of items: 2
Items: 
Size: 596460 Color: 3
Size: 389550 Color: 0

Bin 860: 14427 of cap free
Amount of items: 3
Items: 
Size: 374444 Color: 0
Size: 320478 Color: 4
Size: 290652 Color: 1

Bin 861: 15233 of cap free
Amount of items: 2
Items: 
Size: 614276 Color: 2
Size: 370492 Color: 1

Bin 862: 17050 of cap free
Amount of items: 2
Items: 
Size: 506644 Color: 0
Size: 476307 Color: 3

Bin 863: 17134 of cap free
Amount of items: 2
Items: 
Size: 528445 Color: 3
Size: 454422 Color: 2

Bin 864: 17941 of cap free
Amount of items: 2
Items: 
Size: 726276 Color: 2
Size: 255784 Color: 4

Bin 865: 18316 of cap free
Amount of items: 3
Items: 
Size: 353362 Color: 2
Size: 316420 Color: 4
Size: 311903 Color: 2

Bin 866: 18536 of cap free
Amount of items: 2
Items: 
Size: 506554 Color: 0
Size: 474911 Color: 3

Bin 867: 19156 of cap free
Amount of items: 2
Items: 
Size: 611899 Color: 4
Size: 368946 Color: 1

Bin 868: 22675 of cap free
Amount of items: 2
Items: 
Size: 505703 Color: 3
Size: 471623 Color: 1

Bin 869: 22848 of cap free
Amount of items: 2
Items: 
Size: 573011 Color: 0
Size: 404142 Color: 3

Bin 870: 23853 of cap free
Amount of items: 2
Items: 
Size: 586715 Color: 3
Size: 389433 Color: 4

Bin 871: 24783 of cap free
Amount of items: 2
Items: 
Size: 527810 Color: 1
Size: 447408 Color: 2

Bin 872: 25197 of cap free
Amount of items: 2
Items: 
Size: 722085 Color: 1
Size: 252719 Color: 4

Bin 873: 26186 of cap free
Amount of items: 2
Items: 
Size: 721859 Color: 0
Size: 251956 Color: 4

Bin 874: 26717 of cap free
Amount of items: 2
Items: 
Size: 678135 Color: 1
Size: 295149 Color: 3

Bin 875: 27307 of cap free
Amount of items: 2
Items: 
Size: 677626 Color: 0
Size: 295068 Color: 3

Bin 876: 28797 of cap free
Amount of items: 2
Items: 
Size: 719458 Color: 3
Size: 251746 Color: 4

Bin 877: 28985 of cap free
Amount of items: 2
Items: 
Size: 567662 Color: 2
Size: 403354 Color: 3

Bin 878: 30502 of cap free
Amount of items: 2
Items: 
Size: 612261 Color: 2
Size: 357238 Color: 3

Bin 879: 38632 of cap free
Amount of items: 2
Items: 
Size: 572322 Color: 3
Size: 389047 Color: 0

Bin 880: 40706 of cap free
Amount of items: 2
Items: 
Size: 593055 Color: 0
Size: 366240 Color: 1

Bin 881: 41757 of cap free
Amount of items: 2
Items: 
Size: 525497 Color: 0
Size: 432747 Color: 2

Bin 882: 42298 of cap free
Amount of items: 2
Items: 
Size: 591937 Color: 0
Size: 365766 Color: 1

Bin 883: 44218 of cap free
Amount of items: 2
Items: 
Size: 567243 Color: 4
Size: 388540 Color: 0

Bin 884: 44894 of cap free
Amount of items: 2
Items: 
Size: 566798 Color: 4
Size: 388309 Color: 0

Bin 885: 46771 of cap free
Amount of items: 2
Items: 
Size: 587920 Color: 4
Size: 365310 Color: 1

Bin 886: 49382 of cap free
Amount of items: 2
Items: 
Size: 519817 Color: 1
Size: 430802 Color: 2

Bin 887: 55290 of cap free
Amount of items: 2
Items: 
Size: 482923 Color: 0
Size: 461788 Color: 3

Bin 888: 68392 of cap free
Amount of items: 2
Items: 
Size: 567461 Color: 2
Size: 364148 Color: 1

Bin 889: 73741 of cap free
Amount of items: 2
Items: 
Size: 464817 Color: 0
Size: 461443 Color: 1

Bin 890: 103044 of cap free
Amount of items: 2
Items: 
Size: 507813 Color: 4
Size: 389144 Color: 2

Bin 891: 109021 of cap free
Amount of items: 2
Items: 
Size: 503157 Color: 1
Size: 387823 Color: 2

Bin 892: 123875 of cap free
Amount of items: 2
Items: 
Size: 501243 Color: 3
Size: 374883 Color: 2

Bin 893: 124903 of cap free
Amount of items: 2
Items: 
Size: 500248 Color: 3
Size: 374850 Color: 2

Bin 894: 142782 of cap free
Amount of items: 2
Items: 
Size: 482888 Color: 0
Size: 374331 Color: 2

Bin 895: 160699 of cap free
Amount of items: 2
Items: 
Size: 465088 Color: 0
Size: 374214 Color: 2

Bin 896: 296851 of cap free
Amount of items: 2
Items: 
Size: 351904 Color: 0
Size: 351246 Color: 2

Bin 897: 300622 of cap free
Amount of items: 2
Items: 
Size: 352252 Color: 2
Size: 347127 Color: 4

Bin 898: 301342 of cap free
Amount of items: 2
Items: 
Size: 353156 Color: 2
Size: 345503 Color: 3

Bin 899: 310209 of cap free
Amount of items: 2
Items: 
Size: 346723 Color: 0
Size: 343069 Color: 1

Bin 900: 311264 of cap free
Amount of items: 2
Items: 
Size: 344936 Color: 3
Size: 343801 Color: 1

Bin 901: 311560 of cap free
Amount of items: 2
Items: 
Size: 344921 Color: 3
Size: 343520 Color: 4

Bin 902: 660493 of cap free
Amount of items: 1
Items: 
Size: 339508 Color: 4

Total size: 896617650
Total free space: 5383252

