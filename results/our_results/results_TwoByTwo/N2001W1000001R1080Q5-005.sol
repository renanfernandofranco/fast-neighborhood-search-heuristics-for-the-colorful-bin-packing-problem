Capicity Bin: 1000001
Lower Bound: 875

Bins used: 884
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 550669 Color: 2
Size: 449332 Color: 3

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 703163 Color: 1
Size: 296838 Color: 4

Bin 3: 1 of cap free
Amount of items: 3
Items: 
Size: 729785 Color: 2
Size: 135891 Color: 0
Size: 134324 Color: 0

Bin 4: 1 of cap free
Amount of items: 3
Items: 
Size: 744967 Color: 2
Size: 128928 Color: 4
Size: 126105 Color: 3

Bin 5: 2 of cap free
Amount of items: 2
Items: 
Size: 607517 Color: 4
Size: 392482 Color: 1

Bin 6: 2 of cap free
Amount of items: 3
Items: 
Size: 621677 Color: 2
Size: 195259 Color: 0
Size: 183063 Color: 4

Bin 7: 3 of cap free
Amount of items: 2
Items: 
Size: 629328 Color: 3
Size: 370670 Color: 4

Bin 8: 3 of cap free
Amount of items: 3
Items: 
Size: 640033 Color: 2
Size: 180027 Color: 3
Size: 179938 Color: 0

Bin 9: 3 of cap free
Amount of items: 3
Items: 
Size: 641648 Color: 2
Size: 180849 Color: 4
Size: 177501 Color: 1

Bin 10: 3 of cap free
Amount of items: 3
Items: 
Size: 689214 Color: 2
Size: 156552 Color: 1
Size: 154232 Color: 3

Bin 11: 3 of cap free
Amount of items: 3
Items: 
Size: 738864 Color: 2
Size: 138389 Color: 0
Size: 122745 Color: 1

Bin 12: 4 of cap free
Amount of items: 3
Items: 
Size: 495747 Color: 2
Size: 255410 Color: 1
Size: 248840 Color: 3

Bin 13: 4 of cap free
Amount of items: 2
Items: 
Size: 509038 Color: 1
Size: 490959 Color: 3

Bin 14: 4 of cap free
Amount of items: 2
Items: 
Size: 551540 Color: 2
Size: 448457 Color: 0

Bin 15: 4 of cap free
Amount of items: 2
Items: 
Size: 676167 Color: 3
Size: 323830 Color: 4

Bin 16: 5 of cap free
Amount of items: 3
Items: 
Size: 734982 Color: 2
Size: 133518 Color: 3
Size: 131496 Color: 0

Bin 17: 5 of cap free
Amount of items: 3
Items: 
Size: 772182 Color: 3
Size: 120538 Color: 2
Size: 107276 Color: 1

Bin 18: 6 of cap free
Amount of items: 2
Items: 
Size: 514029 Color: 4
Size: 485966 Color: 2

Bin 19: 6 of cap free
Amount of items: 3
Items: 
Size: 619529 Color: 2
Size: 194830 Color: 1
Size: 185636 Color: 4

Bin 20: 6 of cap free
Amount of items: 2
Items: 
Size: 670876 Color: 1
Size: 329119 Color: 0

Bin 21: 7 of cap free
Amount of items: 3
Items: 
Size: 736722 Color: 2
Size: 133432 Color: 3
Size: 129840 Color: 1

Bin 22: 8 of cap free
Amount of items: 2
Items: 
Size: 556761 Color: 3
Size: 443232 Color: 1

Bin 23: 8 of cap free
Amount of items: 2
Items: 
Size: 562754 Color: 3
Size: 437239 Color: 2

Bin 24: 8 of cap free
Amount of items: 2
Items: 
Size: 643521 Color: 2
Size: 356472 Color: 1

Bin 25: 8 of cap free
Amount of items: 3
Items: 
Size: 645799 Color: 2
Size: 179166 Color: 0
Size: 175028 Color: 3

Bin 26: 8 of cap free
Amount of items: 2
Items: 
Size: 683483 Color: 0
Size: 316510 Color: 1

Bin 27: 8 of cap free
Amount of items: 2
Items: 
Size: 700092 Color: 0
Size: 299901 Color: 2

Bin 28: 8 of cap free
Amount of items: 3
Items: 
Size: 766033 Color: 1
Size: 123017 Color: 2
Size: 110943 Color: 3

Bin 29: 9 of cap free
Amount of items: 3
Items: 
Size: 605250 Color: 2
Size: 205170 Color: 0
Size: 189572 Color: 1

Bin 30: 9 of cap free
Amount of items: 3
Items: 
Size: 622547 Color: 2
Size: 190499 Color: 0
Size: 186946 Color: 0

Bin 31: 9 of cap free
Amount of items: 2
Items: 
Size: 688800 Color: 3
Size: 311192 Color: 0

Bin 32: 9 of cap free
Amount of items: 3
Items: 
Size: 758391 Color: 2
Size: 123027 Color: 4
Size: 118574 Color: 1

Bin 33: 10 of cap free
Amount of items: 2
Items: 
Size: 550459 Color: 1
Size: 449532 Color: 2

Bin 34: 10 of cap free
Amount of items: 3
Items: 
Size: 601050 Color: 2
Size: 211965 Color: 1
Size: 186976 Color: 4

Bin 35: 10 of cap free
Amount of items: 3
Items: 
Size: 672062 Color: 2
Size: 167120 Color: 4
Size: 160809 Color: 3

Bin 36: 11 of cap free
Amount of items: 3
Items: 
Size: 725778 Color: 2
Size: 141598 Color: 1
Size: 132614 Color: 4

Bin 37: 12 of cap free
Amount of items: 3
Items: 
Size: 732244 Color: 3
Size: 139443 Color: 2
Size: 128302 Color: 0

Bin 38: 13 of cap free
Amount of items: 3
Items: 
Size: 612688 Color: 2
Size: 206452 Color: 3
Size: 180848 Color: 3

Bin 39: 13 of cap free
Amount of items: 2
Items: 
Size: 616199 Color: 3
Size: 383789 Color: 1

Bin 40: 13 of cap free
Amount of items: 3
Items: 
Size: 680113 Color: 2
Size: 163950 Color: 0
Size: 155925 Color: 0

Bin 41: 13 of cap free
Amount of items: 3
Items: 
Size: 790835 Color: 4
Size: 107014 Color: 2
Size: 102139 Color: 1

Bin 42: 14 of cap free
Amount of items: 3
Items: 
Size: 461102 Color: 1
Size: 271810 Color: 4
Size: 267075 Color: 4

Bin 43: 14 of cap free
Amount of items: 3
Items: 
Size: 733680 Color: 1
Size: 141637 Color: 2
Size: 124670 Color: 4

Bin 44: 14 of cap free
Amount of items: 3
Items: 
Size: 738563 Color: 0
Size: 139228 Color: 2
Size: 122196 Color: 3

Bin 45: 15 of cap free
Amount of items: 2
Items: 
Size: 655311 Color: 0
Size: 344675 Color: 1

Bin 46: 16 of cap free
Amount of items: 3
Items: 
Size: 451947 Color: 1
Size: 286699 Color: 4
Size: 261339 Color: 0

Bin 47: 16 of cap free
Amount of items: 3
Items: 
Size: 672970 Color: 2
Size: 164334 Color: 0
Size: 162681 Color: 1

Bin 48: 16 of cap free
Amount of items: 3
Items: 
Size: 759611 Color: 2
Size: 127471 Color: 0
Size: 112903 Color: 4

Bin 49: 16 of cap free
Amount of items: 3
Items: 
Size: 770542 Color: 2
Size: 120043 Color: 1
Size: 109400 Color: 0

Bin 50: 17 of cap free
Amount of items: 2
Items: 
Size: 525389 Color: 2
Size: 474595 Color: 3

Bin 51: 17 of cap free
Amount of items: 3
Items: 
Size: 678973 Color: 2
Size: 168549 Color: 3
Size: 152462 Color: 0

Bin 52: 17 of cap free
Amount of items: 2
Items: 
Size: 710499 Color: 3
Size: 289485 Color: 0

Bin 53: 19 of cap free
Amount of items: 2
Items: 
Size: 574448 Color: 1
Size: 425534 Color: 4

Bin 54: 19 of cap free
Amount of items: 3
Items: 
Size: 602325 Color: 2
Size: 213571 Color: 1
Size: 184086 Color: 1

Bin 55: 19 of cap free
Amount of items: 3
Items: 
Size: 711719 Color: 2
Size: 151264 Color: 0
Size: 136999 Color: 1

Bin 56: 19 of cap free
Amount of items: 3
Items: 
Size: 729275 Color: 2
Size: 146649 Color: 0
Size: 124058 Color: 1

Bin 57: 20 of cap free
Amount of items: 2
Items: 
Size: 773325 Color: 1
Size: 226656 Color: 4

Bin 58: 21 of cap free
Amount of items: 3
Items: 
Size: 620905 Color: 2
Size: 192535 Color: 3
Size: 186540 Color: 3

Bin 59: 22 of cap free
Amount of items: 2
Items: 
Size: 605297 Color: 3
Size: 394682 Color: 1

Bin 60: 24 of cap free
Amount of items: 3
Items: 
Size: 638722 Color: 2
Size: 183671 Color: 4
Size: 177584 Color: 1

Bin 61: 24 of cap free
Amount of items: 3
Items: 
Size: 668325 Color: 2
Size: 166553 Color: 3
Size: 165099 Color: 0

Bin 62: 24 of cap free
Amount of items: 2
Items: 
Size: 722641 Color: 3
Size: 277336 Color: 0

Bin 63: 25 of cap free
Amount of items: 3
Items: 
Size: 741498 Color: 3
Size: 138706 Color: 2
Size: 119772 Color: 4

Bin 64: 26 of cap free
Amount of items: 3
Items: 
Size: 368326 Color: 4
Size: 327020 Color: 0
Size: 304629 Color: 3

Bin 65: 26 of cap free
Amount of items: 3
Items: 
Size: 616298 Color: 2
Size: 193707 Color: 1
Size: 189970 Color: 3

Bin 66: 26 of cap free
Amount of items: 2
Items: 
Size: 626474 Color: 0
Size: 373501 Color: 1

Bin 67: 26 of cap free
Amount of items: 2
Items: 
Size: 755642 Color: 1
Size: 244333 Color: 4

Bin 68: 27 of cap free
Amount of items: 3
Items: 
Size: 715183 Color: 2
Size: 142587 Color: 3
Size: 142204 Color: 4

Bin 69: 28 of cap free
Amount of items: 3
Items: 
Size: 594620 Color: 2
Size: 216082 Color: 4
Size: 189271 Color: 1

Bin 70: 28 of cap free
Amount of items: 3
Items: 
Size: 781125 Color: 1
Size: 110890 Color: 1
Size: 107958 Color: 2

Bin 71: 29 of cap free
Amount of items: 2
Items: 
Size: 699807 Color: 4
Size: 300165 Color: 3

Bin 72: 29 of cap free
Amount of items: 2
Items: 
Size: 714664 Color: 3
Size: 285308 Color: 0

Bin 73: 29 of cap free
Amount of items: 3
Items: 
Size: 743979 Color: 2
Size: 138962 Color: 3
Size: 117031 Color: 3

Bin 74: 30 of cap free
Amount of items: 3
Items: 
Size: 341783 Color: 0
Size: 330227 Color: 4
Size: 327961 Color: 4

Bin 75: 30 of cap free
Amount of items: 3
Items: 
Size: 429424 Color: 4
Size: 298279 Color: 0
Size: 272268 Color: 2

Bin 76: 30 of cap free
Amount of items: 3
Items: 
Size: 620086 Color: 2
Size: 191679 Color: 0
Size: 188206 Color: 3

Bin 77: 30 of cap free
Amount of items: 2
Items: 
Size: 671798 Color: 1
Size: 328173 Color: 3

Bin 78: 31 of cap free
Amount of items: 2
Items: 
Size: 563728 Color: 1
Size: 436242 Color: 0

Bin 79: 32 of cap free
Amount of items: 3
Items: 
Size: 663595 Color: 3
Size: 169514 Color: 4
Size: 166860 Color: 3

Bin 80: 33 of cap free
Amount of items: 2
Items: 
Size: 551663 Color: 1
Size: 448305 Color: 2

Bin 81: 34 of cap free
Amount of items: 3
Items: 
Size: 700894 Color: 2
Size: 152940 Color: 3
Size: 146133 Color: 0

Bin 82: 34 of cap free
Amount of items: 3
Items: 
Size: 730840 Color: 2
Size: 149196 Color: 1
Size: 119931 Color: 1

Bin 83: 37 of cap free
Amount of items: 3
Items: 
Size: 726053 Color: 2
Size: 146582 Color: 0
Size: 127329 Color: 3

Bin 84: 38 of cap free
Amount of items: 3
Items: 
Size: 723991 Color: 2
Size: 152169 Color: 3
Size: 123803 Color: 0

Bin 85: 39 of cap free
Amount of items: 2
Items: 
Size: 681069 Color: 4
Size: 318893 Color: 3

Bin 86: 40 of cap free
Amount of items: 2
Items: 
Size: 577351 Color: 1
Size: 422610 Color: 0

Bin 87: 40 of cap free
Amount of items: 2
Items: 
Size: 639463 Color: 1
Size: 360498 Color: 0

Bin 88: 41 of cap free
Amount of items: 3
Items: 
Size: 599378 Color: 2
Size: 211413 Color: 0
Size: 189169 Color: 4

Bin 89: 41 of cap free
Amount of items: 3
Items: 
Size: 616167 Color: 2
Size: 192168 Color: 0
Size: 191625 Color: 4

Bin 90: 42 of cap free
Amount of items: 3
Items: 
Size: 447061 Color: 0
Size: 282174 Color: 1
Size: 270724 Color: 2

Bin 91: 42 of cap free
Amount of items: 3
Items: 
Size: 719196 Color: 2
Size: 145918 Color: 4
Size: 134845 Color: 1

Bin 92: 42 of cap free
Amount of items: 2
Items: 
Size: 795885 Color: 3
Size: 204074 Color: 4

Bin 93: 43 of cap free
Amount of items: 3
Items: 
Size: 650762 Color: 0
Size: 175417 Color: 3
Size: 173779 Color: 0

Bin 94: 43 of cap free
Amount of items: 2
Items: 
Size: 707927 Color: 1
Size: 292031 Color: 0

Bin 95: 45 of cap free
Amount of items: 3
Items: 
Size: 681314 Color: 2
Size: 159371 Color: 3
Size: 159271 Color: 4

Bin 96: 46 of cap free
Amount of items: 3
Items: 
Size: 684489 Color: 2
Size: 160400 Color: 3
Size: 155066 Color: 4

Bin 97: 47 of cap free
Amount of items: 3
Items: 
Size: 606640 Color: 2
Size: 206510 Color: 4
Size: 186804 Color: 0

Bin 98: 47 of cap free
Amount of items: 3
Items: 
Size: 776399 Color: 2
Size: 112209 Color: 4
Size: 111346 Color: 4

Bin 99: 48 of cap free
Amount of items: 2
Items: 
Size: 632406 Color: 3
Size: 367547 Color: 2

Bin 100: 48 of cap free
Amount of items: 2
Items: 
Size: 723551 Color: 4
Size: 276402 Color: 1

Bin 101: 48 of cap free
Amount of items: 3
Items: 
Size: 771130 Color: 2
Size: 114866 Color: 4
Size: 113957 Color: 4

Bin 102: 49 of cap free
Amount of items: 3
Items: 
Size: 638123 Color: 1
Size: 186900 Color: 2
Size: 174929 Color: 4

Bin 103: 49 of cap free
Amount of items: 2
Items: 
Size: 708109 Color: 0
Size: 291843 Color: 1

Bin 104: 50 of cap free
Amount of items: 2
Items: 
Size: 607951 Color: 4
Size: 392000 Color: 1

Bin 105: 51 of cap free
Amount of items: 3
Items: 
Size: 736457 Color: 2
Size: 142729 Color: 3
Size: 120764 Color: 4

Bin 106: 52 of cap free
Amount of items: 2
Items: 
Size: 593263 Color: 3
Size: 406686 Color: 4

Bin 107: 52 of cap free
Amount of items: 2
Items: 
Size: 747379 Color: 0
Size: 252570 Color: 4

Bin 108: 54 of cap free
Amount of items: 2
Items: 
Size: 715083 Color: 0
Size: 284864 Color: 4

Bin 109: 55 of cap free
Amount of items: 2
Items: 
Size: 515399 Color: 4
Size: 484547 Color: 0

Bin 110: 55 of cap free
Amount of items: 2
Items: 
Size: 655562 Color: 2
Size: 344384 Color: 4

Bin 111: 56 of cap free
Amount of items: 2
Items: 
Size: 577049 Color: 0
Size: 422896 Color: 1

Bin 112: 56 of cap free
Amount of items: 3
Items: 
Size: 713472 Color: 2
Size: 152288 Color: 3
Size: 134185 Color: 0

Bin 113: 58 of cap free
Amount of items: 3
Items: 
Size: 607543 Color: 2
Size: 206285 Color: 0
Size: 186115 Color: 4

Bin 114: 58 of cap free
Amount of items: 2
Items: 
Size: 622084 Color: 1
Size: 377859 Color: 3

Bin 115: 58 of cap free
Amount of items: 2
Items: 
Size: 767664 Color: 4
Size: 232279 Color: 2

Bin 116: 58 of cap free
Amount of items: 2
Items: 
Size: 772846 Color: 4
Size: 227097 Color: 2

Bin 117: 60 of cap free
Amount of items: 3
Items: 
Size: 748354 Color: 2
Size: 135179 Color: 0
Size: 116408 Color: 4

Bin 118: 62 of cap free
Amount of items: 2
Items: 
Size: 576968 Color: 4
Size: 422971 Color: 2

Bin 119: 62 of cap free
Amount of items: 2
Items: 
Size: 632255 Color: 3
Size: 367684 Color: 0

Bin 120: 64 of cap free
Amount of items: 3
Items: 
Size: 604655 Color: 1
Size: 212065 Color: 2
Size: 183217 Color: 0

Bin 121: 64 of cap free
Amount of items: 3
Items: 
Size: 675650 Color: 4
Size: 172407 Color: 2
Size: 151880 Color: 3

Bin 122: 65 of cap free
Amount of items: 2
Items: 
Size: 513678 Color: 0
Size: 486258 Color: 2

Bin 123: 65 of cap free
Amount of items: 2
Items: 
Size: 561199 Color: 3
Size: 438737 Color: 1

Bin 124: 65 of cap free
Amount of items: 3
Items: 
Size: 748213 Color: 2
Size: 134859 Color: 0
Size: 116864 Color: 4

Bin 125: 67 of cap free
Amount of items: 2
Items: 
Size: 542427 Color: 2
Size: 457507 Color: 3

Bin 126: 68 of cap free
Amount of items: 2
Items: 
Size: 728742 Color: 4
Size: 271191 Color: 1

Bin 127: 69 of cap free
Amount of items: 3
Items: 
Size: 607171 Color: 1
Size: 211709 Color: 2
Size: 181052 Color: 1

Bin 128: 70 of cap free
Amount of items: 2
Items: 
Size: 533308 Color: 2
Size: 466623 Color: 3

Bin 129: 71 of cap free
Amount of items: 2
Items: 
Size: 659942 Color: 4
Size: 339988 Color: 3

Bin 130: 72 of cap free
Amount of items: 2
Items: 
Size: 606278 Color: 3
Size: 393651 Color: 4

Bin 131: 72 of cap free
Amount of items: 3
Items: 
Size: 784720 Color: 2
Size: 109915 Color: 0
Size: 105294 Color: 4

Bin 132: 73 of cap free
Amount of items: 2
Items: 
Size: 657969 Color: 2
Size: 341959 Color: 0

Bin 133: 73 of cap free
Amount of items: 2
Items: 
Size: 754250 Color: 1
Size: 245678 Color: 3

Bin 134: 74 of cap free
Amount of items: 2
Items: 
Size: 672550 Color: 3
Size: 327377 Color: 4

Bin 135: 76 of cap free
Amount of items: 2
Items: 
Size: 532979 Color: 0
Size: 466946 Color: 4

Bin 136: 76 of cap free
Amount of items: 2
Items: 
Size: 659264 Color: 2
Size: 340661 Color: 3

Bin 137: 76 of cap free
Amount of items: 2
Items: 
Size: 777790 Color: 4
Size: 222135 Color: 2

Bin 138: 78 of cap free
Amount of items: 2
Items: 
Size: 501760 Color: 4
Size: 498163 Color: 2

Bin 139: 79 of cap free
Amount of items: 2
Items: 
Size: 700423 Color: 1
Size: 299499 Color: 4

Bin 140: 79 of cap free
Amount of items: 3
Items: 
Size: 798345 Color: 4
Size: 101192 Color: 2
Size: 100385 Color: 0

Bin 141: 81 of cap free
Amount of items: 2
Items: 
Size: 724456 Color: 1
Size: 275464 Color: 0

Bin 142: 81 of cap free
Amount of items: 2
Items: 
Size: 734870 Color: 0
Size: 265050 Color: 4

Bin 143: 81 of cap free
Amount of items: 3
Items: 
Size: 745506 Color: 2
Size: 131636 Color: 3
Size: 122778 Color: 4

Bin 144: 82 of cap free
Amount of items: 2
Items: 
Size: 509720 Color: 2
Size: 490199 Color: 3

Bin 145: 82 of cap free
Amount of items: 2
Items: 
Size: 768536 Color: 1
Size: 231383 Color: 4

Bin 146: 82 of cap free
Amount of items: 3
Items: 
Size: 769410 Color: 2
Size: 118305 Color: 0
Size: 112204 Color: 1

Bin 147: 83 of cap free
Amount of items: 2
Items: 
Size: 639729 Color: 0
Size: 360189 Color: 3

Bin 148: 83 of cap free
Amount of items: 3
Items: 
Size: 728346 Color: 1
Size: 145091 Color: 2
Size: 126481 Color: 3

Bin 149: 84 of cap free
Amount of items: 2
Items: 
Size: 539818 Color: 3
Size: 460099 Color: 4

Bin 150: 84 of cap free
Amount of items: 2
Items: 
Size: 574486 Color: 2
Size: 425431 Color: 3

Bin 151: 85 of cap free
Amount of items: 3
Items: 
Size: 463043 Color: 1
Size: 271284 Color: 0
Size: 265589 Color: 4

Bin 152: 85 of cap free
Amount of items: 2
Items: 
Size: 591075 Color: 3
Size: 408841 Color: 4

Bin 153: 86 of cap free
Amount of items: 2
Items: 
Size: 563665 Color: 3
Size: 436250 Color: 4

Bin 154: 86 of cap free
Amount of items: 2
Items: 
Size: 583364 Color: 4
Size: 416551 Color: 2

Bin 155: 87 of cap free
Amount of items: 2
Items: 
Size: 568776 Color: 0
Size: 431138 Color: 3

Bin 156: 88 of cap free
Amount of items: 3
Items: 
Size: 614247 Color: 3
Size: 204274 Color: 2
Size: 181392 Color: 1

Bin 157: 88 of cap free
Amount of items: 3
Items: 
Size: 724761 Color: 2
Size: 144036 Color: 4
Size: 131116 Color: 3

Bin 158: 88 of cap free
Amount of items: 3
Items: 
Size: 733071 Color: 4
Size: 142146 Color: 2
Size: 124696 Color: 3

Bin 159: 88 of cap free
Amount of items: 2
Items: 
Size: 752190 Color: 0
Size: 247723 Color: 3

Bin 160: 89 of cap free
Amount of items: 2
Items: 
Size: 549190 Color: 3
Size: 450722 Color: 2

Bin 161: 90 of cap free
Amount of items: 2
Items: 
Size: 705674 Color: 3
Size: 294237 Color: 4

Bin 162: 90 of cap free
Amount of items: 2
Items: 
Size: 726671 Color: 1
Size: 273240 Color: 0

Bin 163: 90 of cap free
Amount of items: 2
Items: 
Size: 772878 Color: 3
Size: 227033 Color: 4

Bin 164: 92 of cap free
Amount of items: 3
Items: 
Size: 790690 Color: 2
Size: 105003 Color: 4
Size: 104216 Color: 4

Bin 165: 94 of cap free
Amount of items: 2
Items: 
Size: 612130 Color: 4
Size: 387777 Color: 2

Bin 166: 95 of cap free
Amount of items: 2
Items: 
Size: 637814 Color: 0
Size: 362092 Color: 4

Bin 167: 95 of cap free
Amount of items: 3
Items: 
Size: 699444 Color: 0
Size: 161172 Color: 2
Size: 139290 Color: 3

Bin 168: 95 of cap free
Amount of items: 3
Items: 
Size: 724478 Color: 2
Size: 144856 Color: 1
Size: 130572 Color: 0

Bin 169: 96 of cap free
Amount of items: 2
Items: 
Size: 528022 Color: 2
Size: 471883 Color: 3

Bin 170: 96 of cap free
Amount of items: 2
Items: 
Size: 577136 Color: 4
Size: 422769 Color: 2

Bin 171: 96 of cap free
Amount of items: 2
Items: 
Size: 612253 Color: 4
Size: 387652 Color: 0

Bin 172: 96 of cap free
Amount of items: 2
Items: 
Size: 654461 Color: 1
Size: 345444 Color: 0

Bin 173: 97 of cap free
Amount of items: 2
Items: 
Size: 506410 Color: 4
Size: 493494 Color: 1

Bin 174: 97 of cap free
Amount of items: 2
Items: 
Size: 567256 Color: 3
Size: 432648 Color: 1

Bin 175: 98 of cap free
Amount of items: 3
Items: 
Size: 697391 Color: 2
Size: 156030 Color: 3
Size: 146482 Color: 3

Bin 176: 98 of cap free
Amount of items: 2
Items: 
Size: 702921 Color: 4
Size: 296982 Color: 2

Bin 177: 99 of cap free
Amount of items: 3
Items: 
Size: 370506 Color: 0
Size: 316311 Color: 1
Size: 313085 Color: 3

Bin 178: 100 of cap free
Amount of items: 2
Items: 
Size: 535840 Color: 1
Size: 464061 Color: 2

Bin 179: 100 of cap free
Amount of items: 2
Items: 
Size: 653662 Color: 4
Size: 346239 Color: 2

Bin 180: 100 of cap free
Amount of items: 3
Items: 
Size: 719567 Color: 2
Size: 143250 Color: 4
Size: 137084 Color: 0

Bin 181: 101 of cap free
Amount of items: 2
Items: 
Size: 669614 Color: 4
Size: 330286 Color: 1

Bin 182: 105 of cap free
Amount of items: 2
Items: 
Size: 676045 Color: 4
Size: 323851 Color: 0

Bin 183: 105 of cap free
Amount of items: 3
Items: 
Size: 681735 Color: 2
Size: 163194 Color: 4
Size: 154967 Color: 0

Bin 184: 106 of cap free
Amount of items: 3
Items: 
Size: 589590 Color: 2
Size: 216455 Color: 0
Size: 193850 Color: 3

Bin 185: 106 of cap free
Amount of items: 3
Items: 
Size: 599529 Color: 2
Size: 212678 Color: 4
Size: 187688 Color: 0

Bin 186: 106 of cap free
Amount of items: 3
Items: 
Size: 750594 Color: 0
Size: 134683 Color: 2
Size: 114618 Color: 0

Bin 187: 107 of cap free
Amount of items: 3
Items: 
Size: 672830 Color: 2
Size: 172954 Color: 0
Size: 154110 Color: 4

Bin 188: 108 of cap free
Amount of items: 2
Items: 
Size: 769777 Color: 4
Size: 230116 Color: 0

Bin 189: 109 of cap free
Amount of items: 2
Items: 
Size: 570479 Color: 1
Size: 429413 Color: 2

Bin 190: 110 of cap free
Amount of items: 2
Items: 
Size: 522288 Color: 1
Size: 477603 Color: 0

Bin 191: 110 of cap free
Amount of items: 2
Items: 
Size: 523181 Color: 1
Size: 476710 Color: 3

Bin 192: 111 of cap free
Amount of items: 2
Items: 
Size: 514352 Color: 1
Size: 485538 Color: 2

Bin 193: 111 of cap free
Amount of items: 3
Items: 
Size: 609248 Color: 2
Size: 206554 Color: 4
Size: 184088 Color: 0

Bin 194: 111 of cap free
Amount of items: 2
Items: 
Size: 627648 Color: 0
Size: 372242 Color: 4

Bin 195: 112 of cap free
Amount of items: 3
Items: 
Size: 724157 Color: 1
Size: 138502 Color: 2
Size: 137230 Color: 4

Bin 196: 113 of cap free
Amount of items: 3
Items: 
Size: 786062 Color: 2
Size: 107426 Color: 4
Size: 106400 Color: 4

Bin 197: 115 of cap free
Amount of items: 2
Items: 
Size: 765134 Color: 2
Size: 234752 Color: 4

Bin 198: 116 of cap free
Amount of items: 3
Items: 
Size: 378802 Color: 1
Size: 310626 Color: 0
Size: 310457 Color: 0

Bin 199: 118 of cap free
Amount of items: 3
Items: 
Size: 451206 Color: 2
Size: 287126 Color: 3
Size: 261551 Color: 2

Bin 200: 118 of cap free
Amount of items: 2
Items: 
Size: 523376 Color: 1
Size: 476507 Color: 4

Bin 201: 118 of cap free
Amount of items: 2
Items: 
Size: 628968 Color: 3
Size: 370915 Color: 1

Bin 202: 119 of cap free
Amount of items: 2
Items: 
Size: 557785 Color: 0
Size: 442097 Color: 3

Bin 203: 119 of cap free
Amount of items: 2
Items: 
Size: 569957 Color: 4
Size: 429925 Color: 0

Bin 204: 119 of cap free
Amount of items: 3
Items: 
Size: 642100 Color: 2
Size: 180864 Color: 1
Size: 176918 Color: 4

Bin 205: 120 of cap free
Amount of items: 2
Items: 
Size: 537443 Color: 3
Size: 462438 Color: 1

Bin 206: 120 of cap free
Amount of items: 3
Items: 
Size: 596176 Color: 4
Size: 216123 Color: 2
Size: 187582 Color: 4

Bin 207: 120 of cap free
Amount of items: 3
Items: 
Size: 671377 Color: 2
Size: 167522 Color: 1
Size: 160982 Color: 1

Bin 208: 121 of cap free
Amount of items: 3
Items: 
Size: 740232 Color: 3
Size: 141125 Color: 2
Size: 118523 Color: 1

Bin 209: 122 of cap free
Amount of items: 2
Items: 
Size: 595390 Color: 3
Size: 404489 Color: 1

Bin 210: 122 of cap free
Amount of items: 3
Items: 
Size: 668091 Color: 2
Size: 166079 Color: 0
Size: 165709 Color: 1

Bin 211: 122 of cap free
Amount of items: 3
Items: 
Size: 701838 Color: 2
Size: 156095 Color: 1
Size: 141946 Color: 0

Bin 212: 124 of cap free
Amount of items: 3
Items: 
Size: 601645 Color: 2
Size: 211757 Color: 1
Size: 186475 Color: 1

Bin 213: 125 of cap free
Amount of items: 2
Items: 
Size: 794468 Color: 1
Size: 205408 Color: 0

Bin 214: 126 of cap free
Amount of items: 2
Items: 
Size: 523501 Color: 1
Size: 476374 Color: 0

Bin 215: 126 of cap free
Amount of items: 3
Items: 
Size: 596174 Color: 2
Size: 215607 Color: 3
Size: 188094 Color: 1

Bin 216: 129 of cap free
Amount of items: 2
Items: 
Size: 526783 Color: 2
Size: 473089 Color: 0

Bin 217: 129 of cap free
Amount of items: 3
Items: 
Size: 760207 Color: 0
Size: 126760 Color: 2
Size: 112905 Color: 0

Bin 218: 131 of cap free
Amount of items: 2
Items: 
Size: 553454 Color: 3
Size: 446416 Color: 4

Bin 219: 131 of cap free
Amount of items: 2
Items: 
Size: 744438 Color: 4
Size: 255432 Color: 0

Bin 220: 134 of cap free
Amount of items: 2
Items: 
Size: 590528 Color: 2
Size: 409339 Color: 4

Bin 221: 136 of cap free
Amount of items: 2
Items: 
Size: 515733 Color: 1
Size: 484132 Color: 0

Bin 222: 136 of cap free
Amount of items: 2
Items: 
Size: 680381 Color: 4
Size: 319484 Color: 2

Bin 223: 137 of cap free
Amount of items: 2
Items: 
Size: 500209 Color: 2
Size: 499655 Color: 3

Bin 224: 137 of cap free
Amount of items: 2
Items: 
Size: 564326 Color: 0
Size: 435538 Color: 1

Bin 225: 137 of cap free
Amount of items: 2
Items: 
Size: 759364 Color: 4
Size: 240500 Color: 0

Bin 226: 138 of cap free
Amount of items: 2
Items: 
Size: 634754 Color: 0
Size: 365109 Color: 4

Bin 227: 141 of cap free
Amount of items: 2
Items: 
Size: 695933 Color: 4
Size: 303927 Color: 1

Bin 228: 141 of cap free
Amount of items: 3
Items: 
Size: 742470 Color: 1
Size: 137361 Color: 2
Size: 120029 Color: 3

Bin 229: 142 of cap free
Amount of items: 2
Items: 
Size: 546716 Color: 1
Size: 453143 Color: 2

Bin 230: 142 of cap free
Amount of items: 2
Items: 
Size: 681805 Color: 0
Size: 318054 Color: 1

Bin 231: 144 of cap free
Amount of items: 2
Items: 
Size: 598906 Color: 4
Size: 400951 Color: 1

Bin 232: 145 of cap free
Amount of items: 2
Items: 
Size: 525721 Color: 1
Size: 474135 Color: 4

Bin 233: 145 of cap free
Amount of items: 2
Items: 
Size: 530335 Color: 1
Size: 469521 Color: 3

Bin 234: 145 of cap free
Amount of items: 3
Items: 
Size: 670461 Color: 1
Size: 176843 Color: 2
Size: 152552 Color: 0

Bin 235: 146 of cap free
Amount of items: 2
Items: 
Size: 644487 Color: 4
Size: 355368 Color: 3

Bin 236: 147 of cap free
Amount of items: 2
Items: 
Size: 567383 Color: 2
Size: 432471 Color: 0

Bin 237: 147 of cap free
Amount of items: 2
Items: 
Size: 789932 Color: 1
Size: 209922 Color: 0

Bin 238: 148 of cap free
Amount of items: 2
Items: 
Size: 780845 Color: 4
Size: 219008 Color: 2

Bin 239: 149 of cap free
Amount of items: 3
Items: 
Size: 448476 Color: 0
Size: 282358 Color: 1
Size: 269018 Color: 1

Bin 240: 149 of cap free
Amount of items: 2
Items: 
Size: 569517 Color: 3
Size: 430335 Color: 0

Bin 241: 150 of cap free
Amount of items: 2
Items: 
Size: 713253 Color: 4
Size: 286598 Color: 2

Bin 242: 151 of cap free
Amount of items: 2
Items: 
Size: 560818 Color: 0
Size: 439032 Color: 4

Bin 243: 155 of cap free
Amount of items: 2
Items: 
Size: 565223 Color: 4
Size: 434623 Color: 2

Bin 244: 155 of cap free
Amount of items: 3
Items: 
Size: 725172 Color: 2
Size: 146561 Color: 0
Size: 128113 Color: 3

Bin 245: 156 of cap free
Amount of items: 2
Items: 
Size: 543979 Color: 0
Size: 455866 Color: 3

Bin 246: 156 of cap free
Amount of items: 2
Items: 
Size: 653288 Color: 0
Size: 346557 Color: 1

Bin 247: 156 of cap free
Amount of items: 3
Items: 
Size: 791029 Color: 3
Size: 106499 Color: 2
Size: 102317 Color: 3

Bin 248: 157 of cap free
Amount of items: 2
Items: 
Size: 629960 Color: 4
Size: 369884 Color: 0

Bin 249: 158 of cap free
Amount of items: 2
Items: 
Size: 677871 Color: 0
Size: 321972 Color: 4

Bin 250: 158 of cap free
Amount of items: 2
Items: 
Size: 706430 Color: 4
Size: 293413 Color: 2

Bin 251: 160 of cap free
Amount of items: 2
Items: 
Size: 728516 Color: 0
Size: 271325 Color: 4

Bin 252: 161 of cap free
Amount of items: 2
Items: 
Size: 751544 Color: 1
Size: 248296 Color: 3

Bin 253: 162 of cap free
Amount of items: 2
Items: 
Size: 770396 Color: 1
Size: 229443 Color: 4

Bin 254: 164 of cap free
Amount of items: 3
Items: 
Size: 594176 Color: 2
Size: 218112 Color: 3
Size: 187549 Color: 4

Bin 255: 164 of cap free
Amount of items: 2
Items: 
Size: 799220 Color: 0
Size: 200617 Color: 1

Bin 256: 165 of cap free
Amount of items: 2
Items: 
Size: 568199 Color: 0
Size: 431637 Color: 3

Bin 257: 166 of cap free
Amount of items: 2
Items: 
Size: 757719 Color: 2
Size: 242116 Color: 3

Bin 258: 170 of cap free
Amount of items: 3
Items: 
Size: 593320 Color: 3
Size: 213837 Color: 2
Size: 192674 Color: 4

Bin 259: 171 of cap free
Amount of items: 2
Items: 
Size: 546061 Color: 3
Size: 453769 Color: 2

Bin 260: 173 of cap free
Amount of items: 2
Items: 
Size: 758869 Color: 0
Size: 240959 Color: 1

Bin 261: 174 of cap free
Amount of items: 3
Items: 
Size: 421032 Color: 0
Size: 301957 Color: 3
Size: 276838 Color: 1

Bin 262: 175 of cap free
Amount of items: 2
Items: 
Size: 506048 Color: 3
Size: 493778 Color: 4

Bin 263: 176 of cap free
Amount of items: 2
Items: 
Size: 593815 Color: 0
Size: 406010 Color: 1

Bin 264: 177 of cap free
Amount of items: 3
Items: 
Size: 482671 Color: 3
Size: 259721 Color: 2
Size: 257432 Color: 4

Bin 265: 177 of cap free
Amount of items: 3
Items: 
Size: 682825 Color: 2
Size: 163210 Color: 4
Size: 153789 Color: 0

Bin 266: 178 of cap free
Amount of items: 2
Items: 
Size: 572151 Color: 4
Size: 427672 Color: 3

Bin 267: 179 of cap free
Amount of items: 2
Items: 
Size: 665552 Color: 2
Size: 334270 Color: 1

Bin 268: 180 of cap free
Amount of items: 2
Items: 
Size: 758426 Color: 3
Size: 241395 Color: 1

Bin 269: 182 of cap free
Amount of items: 3
Items: 
Size: 594051 Color: 1
Size: 217106 Color: 2
Size: 188662 Color: 1

Bin 270: 182 of cap free
Amount of items: 2
Items: 
Size: 668004 Color: 1
Size: 331815 Color: 0

Bin 271: 184 of cap free
Amount of items: 2
Items: 
Size: 590516 Color: 3
Size: 409301 Color: 0

Bin 272: 186 of cap free
Amount of items: 2
Items: 
Size: 512147 Color: 1
Size: 487668 Color: 3

Bin 273: 186 of cap free
Amount of items: 2
Items: 
Size: 526922 Color: 4
Size: 472893 Color: 3

Bin 274: 186 of cap free
Amount of items: 2
Items: 
Size: 680871 Color: 1
Size: 318944 Color: 4

Bin 275: 186 of cap free
Amount of items: 2
Items: 
Size: 757413 Color: 4
Size: 242402 Color: 0

Bin 276: 190 of cap free
Amount of items: 2
Items: 
Size: 534885 Color: 4
Size: 464926 Color: 2

Bin 277: 190 of cap free
Amount of items: 2
Items: 
Size: 593549 Color: 1
Size: 406262 Color: 4

Bin 278: 192 of cap free
Amount of items: 2
Items: 
Size: 573675 Color: 3
Size: 426134 Color: 0

Bin 279: 192 of cap free
Amount of items: 2
Items: 
Size: 702036 Color: 1
Size: 297773 Color: 4

Bin 280: 193 of cap free
Amount of items: 3
Items: 
Size: 657203 Color: 4
Size: 171732 Color: 1
Size: 170873 Color: 1

Bin 281: 193 of cap free
Amount of items: 2
Items: 
Size: 661450 Color: 2
Size: 338358 Color: 1

Bin 282: 194 of cap free
Amount of items: 2
Items: 
Size: 733129 Color: 0
Size: 266678 Color: 1

Bin 283: 197 of cap free
Amount of items: 2
Items: 
Size: 602973 Color: 0
Size: 396831 Color: 1

Bin 284: 198 of cap free
Amount of items: 3
Items: 
Size: 584668 Color: 3
Size: 217767 Color: 2
Size: 197368 Color: 0

Bin 285: 198 of cap free
Amount of items: 2
Items: 
Size: 666166 Color: 1
Size: 333637 Color: 0

Bin 286: 198 of cap free
Amount of items: 3
Items: 
Size: 731460 Color: 4
Size: 145294 Color: 2
Size: 123049 Color: 3

Bin 287: 200 of cap free
Amount of items: 2
Items: 
Size: 794382 Color: 4
Size: 205419 Color: 2

Bin 288: 201 of cap free
Amount of items: 2
Items: 
Size: 526307 Color: 1
Size: 473493 Color: 4

Bin 289: 203 of cap free
Amount of items: 2
Items: 
Size: 724773 Color: 1
Size: 275025 Color: 0

Bin 290: 207 of cap free
Amount of items: 2
Items: 
Size: 511214 Color: 4
Size: 488580 Color: 3

Bin 291: 207 of cap free
Amount of items: 2
Items: 
Size: 777642 Color: 4
Size: 222152 Color: 1

Bin 292: 209 of cap free
Amount of items: 2
Items: 
Size: 539554 Color: 0
Size: 460238 Color: 1

Bin 293: 209 of cap free
Amount of items: 3
Items: 
Size: 585934 Color: 1
Size: 219380 Color: 2
Size: 194478 Color: 4

Bin 294: 211 of cap free
Amount of items: 3
Items: 
Size: 377367 Color: 3
Size: 315124 Color: 2
Size: 307299 Color: 4

Bin 295: 212 of cap free
Amount of items: 2
Items: 
Size: 581903 Color: 0
Size: 417886 Color: 4

Bin 296: 212 of cap free
Amount of items: 2
Items: 
Size: 615246 Color: 3
Size: 384543 Color: 0

Bin 297: 215 of cap free
Amount of items: 2
Items: 
Size: 752739 Color: 4
Size: 247047 Color: 1

Bin 298: 216 of cap free
Amount of items: 3
Items: 
Size: 762209 Color: 3
Size: 124473 Color: 2
Size: 113103 Color: 1

Bin 299: 217 of cap free
Amount of items: 3
Items: 
Size: 756386 Color: 4
Size: 131534 Color: 2
Size: 111864 Color: 1

Bin 300: 218 of cap free
Amount of items: 3
Items: 
Size: 488566 Color: 2
Size: 257890 Color: 4
Size: 253327 Color: 0

Bin 301: 219 of cap free
Amount of items: 3
Items: 
Size: 784027 Color: 2
Size: 109508 Color: 0
Size: 106247 Color: 3

Bin 302: 221 of cap free
Amount of items: 2
Items: 
Size: 667506 Color: 0
Size: 332274 Color: 4

Bin 303: 221 of cap free
Amount of items: 2
Items: 
Size: 691288 Color: 1
Size: 308492 Color: 3

Bin 304: 223 of cap free
Amount of items: 2
Items: 
Size: 529104 Color: 0
Size: 470674 Color: 4

Bin 305: 223 of cap free
Amount of items: 2
Items: 
Size: 680160 Color: 3
Size: 319618 Color: 4

Bin 306: 224 of cap free
Amount of items: 2
Items: 
Size: 546871 Color: 2
Size: 452906 Color: 4

Bin 307: 224 of cap free
Amount of items: 2
Items: 
Size: 662812 Color: 4
Size: 336965 Color: 2

Bin 308: 224 of cap free
Amount of items: 2
Items: 
Size: 681265 Color: 3
Size: 318512 Color: 2

Bin 309: 224 of cap free
Amount of items: 2
Items: 
Size: 735657 Color: 0
Size: 264120 Color: 4

Bin 310: 224 of cap free
Amount of items: 2
Items: 
Size: 787680 Color: 1
Size: 212097 Color: 3

Bin 311: 225 of cap free
Amount of items: 2
Items: 
Size: 561064 Color: 0
Size: 438712 Color: 2

Bin 312: 225 of cap free
Amount of items: 3
Items: 
Size: 626930 Color: 1
Size: 194123 Color: 2
Size: 178723 Color: 3

Bin 313: 225 of cap free
Amount of items: 3
Items: 
Size: 667143 Color: 1
Size: 175084 Color: 2
Size: 157549 Color: 3

Bin 314: 226 of cap free
Amount of items: 3
Items: 
Size: 779356 Color: 2
Size: 113720 Color: 1
Size: 106699 Color: 3

Bin 315: 227 of cap free
Amount of items: 2
Items: 
Size: 625266 Color: 2
Size: 374508 Color: 4

Bin 316: 227 of cap free
Amount of items: 2
Items: 
Size: 686282 Color: 0
Size: 313492 Color: 2

Bin 317: 228 of cap free
Amount of items: 3
Items: 
Size: 487022 Color: 3
Size: 258557 Color: 4
Size: 254194 Color: 2

Bin 318: 229 of cap free
Amount of items: 3
Items: 
Size: 501890 Color: 2
Size: 249286 Color: 0
Size: 248596 Color: 2

Bin 319: 230 of cap free
Amount of items: 3
Items: 
Size: 795070 Color: 2
Size: 103427 Color: 4
Size: 101274 Color: 3

Bin 320: 231 of cap free
Amount of items: 3
Items: 
Size: 624334 Color: 0
Size: 194358 Color: 2
Size: 181078 Color: 3

Bin 321: 232 of cap free
Amount of items: 2
Items: 
Size: 534832 Color: 0
Size: 464937 Color: 4

Bin 322: 233 of cap free
Amount of items: 2
Items: 
Size: 656229 Color: 3
Size: 343539 Color: 4

Bin 323: 234 of cap free
Amount of items: 2
Items: 
Size: 590498 Color: 4
Size: 409269 Color: 1

Bin 324: 236 of cap free
Amount of items: 3
Items: 
Size: 432753 Color: 0
Size: 285498 Color: 0
Size: 281514 Color: 1

Bin 325: 240 of cap free
Amount of items: 3
Items: 
Size: 662564 Color: 2
Size: 169152 Color: 1
Size: 168045 Color: 3

Bin 326: 241 of cap free
Amount of items: 2
Items: 
Size: 763807 Color: 0
Size: 235953 Color: 1

Bin 327: 242 of cap free
Amount of items: 3
Items: 
Size: 412763 Color: 2
Size: 310404 Color: 3
Size: 276592 Color: 1

Bin 328: 242 of cap free
Amount of items: 2
Items: 
Size: 637413 Color: 0
Size: 362346 Color: 1

Bin 329: 243 of cap free
Amount of items: 2
Items: 
Size: 510607 Color: 2
Size: 489151 Color: 3

Bin 330: 243 of cap free
Amount of items: 2
Items: 
Size: 551122 Color: 1
Size: 448636 Color: 0

Bin 331: 243 of cap free
Amount of items: 2
Items: 
Size: 622684 Color: 0
Size: 377074 Color: 2

Bin 332: 244 of cap free
Amount of items: 2
Items: 
Size: 624334 Color: 4
Size: 375423 Color: 2

Bin 333: 245 of cap free
Amount of items: 2
Items: 
Size: 786826 Color: 4
Size: 212930 Color: 2

Bin 334: 246 of cap free
Amount of items: 3
Items: 
Size: 374217 Color: 1
Size: 314197 Color: 1
Size: 311341 Color: 3

Bin 335: 248 of cap free
Amount of items: 3
Items: 
Size: 790324 Color: 2
Size: 105536 Color: 3
Size: 103893 Color: 3

Bin 336: 249 of cap free
Amount of items: 2
Items: 
Size: 653760 Color: 2
Size: 345992 Color: 1

Bin 337: 250 of cap free
Amount of items: 3
Items: 
Size: 724361 Color: 2
Size: 138773 Color: 0
Size: 136617 Color: 4

Bin 338: 251 of cap free
Amount of items: 3
Items: 
Size: 636276 Color: 2
Size: 183532 Color: 4
Size: 179942 Color: 1

Bin 339: 257 of cap free
Amount of items: 2
Items: 
Size: 757984 Color: 1
Size: 241760 Color: 0

Bin 340: 261 of cap free
Amount of items: 2
Items: 
Size: 625467 Color: 3
Size: 374273 Color: 1

Bin 341: 263 of cap free
Amount of items: 2
Items: 
Size: 649148 Color: 0
Size: 350590 Color: 3

Bin 342: 263 of cap free
Amount of items: 2
Items: 
Size: 786795 Color: 1
Size: 212943 Color: 0

Bin 343: 265 of cap free
Amount of items: 2
Items: 
Size: 590913 Color: 1
Size: 408823 Color: 3

Bin 344: 268 of cap free
Amount of items: 2
Items: 
Size: 771582 Color: 1
Size: 228151 Color: 0

Bin 345: 269 of cap free
Amount of items: 2
Items: 
Size: 700995 Color: 3
Size: 298737 Color: 1

Bin 346: 269 of cap free
Amount of items: 3
Items: 
Size: 749319 Color: 4
Size: 135243 Color: 2
Size: 115170 Color: 0

Bin 347: 270 of cap free
Amount of items: 2
Items: 
Size: 516108 Color: 2
Size: 483623 Color: 4

Bin 348: 276 of cap free
Amount of items: 3
Items: 
Size: 758884 Color: 0
Size: 128972 Color: 2
Size: 111869 Color: 1

Bin 349: 277 of cap free
Amount of items: 2
Items: 
Size: 538908 Color: 2
Size: 460816 Color: 1

Bin 350: 278 of cap free
Amount of items: 3
Items: 
Size: 600369 Color: 0
Size: 213536 Color: 2
Size: 185818 Color: 4

Bin 351: 279 of cap free
Amount of items: 3
Items: 
Size: 639856 Color: 1
Size: 181558 Color: 2
Size: 178308 Color: 0

Bin 352: 279 of cap free
Amount of items: 2
Items: 
Size: 724958 Color: 0
Size: 274764 Color: 3

Bin 353: 282 of cap free
Amount of items: 2
Items: 
Size: 660062 Color: 2
Size: 339657 Color: 4

Bin 354: 283 of cap free
Amount of items: 2
Items: 
Size: 736704 Color: 3
Size: 263014 Color: 0

Bin 355: 284 of cap free
Amount of items: 2
Items: 
Size: 653691 Color: 1
Size: 346026 Color: 0

Bin 356: 285 of cap free
Amount of items: 2
Items: 
Size: 527644 Color: 4
Size: 472072 Color: 0

Bin 357: 288 of cap free
Amount of items: 2
Items: 
Size: 635358 Color: 4
Size: 364355 Color: 0

Bin 358: 289 of cap free
Amount of items: 2
Items: 
Size: 777571 Color: 3
Size: 222141 Color: 4

Bin 359: 293 of cap free
Amount of items: 2
Items: 
Size: 648311 Color: 1
Size: 351397 Color: 2

Bin 360: 295 of cap free
Amount of items: 3
Items: 
Size: 587133 Color: 3
Size: 217729 Color: 4
Size: 194844 Color: 0

Bin 361: 295 of cap free
Amount of items: 3
Items: 
Size: 709273 Color: 2
Size: 148470 Color: 0
Size: 141963 Color: 0

Bin 362: 298 of cap free
Amount of items: 2
Items: 
Size: 742519 Color: 4
Size: 257184 Color: 0

Bin 363: 300 of cap free
Amount of items: 2
Items: 
Size: 559135 Color: 1
Size: 440566 Color: 0

Bin 364: 300 of cap free
Amount of items: 3
Items: 
Size: 715526 Color: 2
Size: 151500 Color: 0
Size: 132675 Color: 4

Bin 365: 301 of cap free
Amount of items: 2
Items: 
Size: 740413 Color: 0
Size: 259287 Color: 1

Bin 366: 302 of cap free
Amount of items: 2
Items: 
Size: 719408 Color: 0
Size: 280291 Color: 3

Bin 367: 304 of cap free
Amount of items: 3
Items: 
Size: 372176 Color: 4
Size: 320034 Color: 0
Size: 307487 Color: 0

Bin 368: 304 of cap free
Amount of items: 2
Items: 
Size: 643574 Color: 4
Size: 356123 Color: 2

Bin 369: 305 of cap free
Amount of items: 2
Items: 
Size: 756033 Color: 0
Size: 243663 Color: 3

Bin 370: 306 of cap free
Amount of items: 2
Items: 
Size: 518976 Color: 1
Size: 480719 Color: 4

Bin 371: 306 of cap free
Amount of items: 3
Items: 
Size: 582024 Color: 2
Size: 214114 Color: 4
Size: 203557 Color: 3

Bin 372: 308 of cap free
Amount of items: 3
Items: 
Size: 748212 Color: 4
Size: 134326 Color: 2
Size: 117155 Color: 1

Bin 373: 313 of cap free
Amount of items: 2
Items: 
Size: 788836 Color: 3
Size: 210852 Color: 4

Bin 374: 314 of cap free
Amount of items: 2
Items: 
Size: 563338 Color: 2
Size: 436349 Color: 3

Bin 375: 315 of cap free
Amount of items: 2
Items: 
Size: 620626 Color: 0
Size: 379060 Color: 4

Bin 376: 317 of cap free
Amount of items: 3
Items: 
Size: 763671 Color: 1
Size: 125026 Color: 2
Size: 110987 Color: 1

Bin 377: 318 of cap free
Amount of items: 2
Items: 
Size: 524685 Color: 3
Size: 474998 Color: 1

Bin 378: 318 of cap free
Amount of items: 2
Items: 
Size: 590085 Color: 2
Size: 409598 Color: 0

Bin 379: 321 of cap free
Amount of items: 3
Items: 
Size: 517149 Color: 1
Size: 247491 Color: 4
Size: 235040 Color: 3

Bin 380: 322 of cap free
Amount of items: 2
Items: 
Size: 581009 Color: 4
Size: 418670 Color: 3

Bin 381: 323 of cap free
Amount of items: 2
Items: 
Size: 559283 Color: 0
Size: 440395 Color: 3

Bin 382: 324 of cap free
Amount of items: 3
Items: 
Size: 452708 Color: 0
Size: 275419 Color: 2
Size: 271550 Color: 3

Bin 383: 325 of cap free
Amount of items: 2
Items: 
Size: 509542 Color: 2
Size: 490134 Color: 3

Bin 384: 325 of cap free
Amount of items: 3
Items: 
Size: 674185 Color: 0
Size: 173076 Color: 2
Size: 152415 Color: 3

Bin 385: 325 of cap free
Amount of items: 3
Items: 
Size: 720339 Color: 1
Size: 150839 Color: 2
Size: 128498 Color: 1

Bin 386: 326 of cap free
Amount of items: 3
Items: 
Size: 735398 Color: 4
Size: 148280 Color: 2
Size: 115997 Color: 3

Bin 387: 327 of cap free
Amount of items: 2
Items: 
Size: 668541 Color: 1
Size: 331133 Color: 4

Bin 388: 329 of cap free
Amount of items: 2
Items: 
Size: 535977 Color: 1
Size: 463695 Color: 2

Bin 389: 330 of cap free
Amount of items: 2
Items: 
Size: 528249 Color: 1
Size: 471422 Color: 3

Bin 390: 330 of cap free
Amount of items: 3
Items: 
Size: 752211 Color: 4
Size: 133420 Color: 2
Size: 114040 Color: 1

Bin 391: 332 of cap free
Amount of items: 2
Items: 
Size: 765566 Color: 3
Size: 234103 Color: 4

Bin 392: 333 of cap free
Amount of items: 3
Items: 
Size: 783447 Color: 1
Size: 110964 Color: 2
Size: 105257 Color: 4

Bin 393: 336 of cap free
Amount of items: 2
Items: 
Size: 677122 Color: 4
Size: 322543 Color: 2

Bin 394: 337 of cap free
Amount of items: 2
Items: 
Size: 503398 Color: 0
Size: 496266 Color: 1

Bin 395: 337 of cap free
Amount of items: 2
Items: 
Size: 787276 Color: 4
Size: 212388 Color: 3

Bin 396: 338 of cap free
Amount of items: 3
Items: 
Size: 656382 Color: 0
Size: 173087 Color: 1
Size: 170194 Color: 4

Bin 397: 339 of cap free
Amount of items: 2
Items: 
Size: 698784 Color: 1
Size: 300878 Color: 4

Bin 398: 340 of cap free
Amount of items: 2
Items: 
Size: 662143 Color: 4
Size: 337518 Color: 3

Bin 399: 343 of cap free
Amount of items: 3
Items: 
Size: 778370 Color: 1
Size: 115121 Color: 2
Size: 106167 Color: 4

Bin 400: 346 of cap free
Amount of items: 2
Items: 
Size: 784506 Color: 1
Size: 215149 Color: 4

Bin 401: 347 of cap free
Amount of items: 3
Items: 
Size: 649546 Color: 1
Size: 175710 Color: 2
Size: 174398 Color: 4

Bin 402: 350 of cap free
Amount of items: 2
Items: 
Size: 720934 Color: 0
Size: 278717 Color: 1

Bin 403: 350 of cap free
Amount of items: 2
Items: 
Size: 764605 Color: 0
Size: 235046 Color: 2

Bin 404: 353 of cap free
Amount of items: 2
Items: 
Size: 512467 Color: 4
Size: 487181 Color: 2

Bin 405: 353 of cap free
Amount of items: 2
Items: 
Size: 618748 Color: 0
Size: 380900 Color: 1

Bin 406: 354 of cap free
Amount of items: 2
Items: 
Size: 589805 Color: 3
Size: 409842 Color: 2

Bin 407: 354 of cap free
Amount of items: 2
Items: 
Size: 727634 Color: 4
Size: 272013 Color: 0

Bin 408: 355 of cap free
Amount of items: 2
Items: 
Size: 670682 Color: 4
Size: 328964 Color: 1

Bin 409: 356 of cap free
Amount of items: 3
Items: 
Size: 381950 Color: 0
Size: 313256 Color: 0
Size: 304439 Color: 3

Bin 410: 359 of cap free
Amount of items: 2
Items: 
Size: 501679 Color: 3
Size: 497963 Color: 2

Bin 411: 359 of cap free
Amount of items: 2
Items: 
Size: 538029 Color: 0
Size: 461613 Color: 4

Bin 412: 361 of cap free
Amount of items: 3
Items: 
Size: 568170 Color: 3
Size: 216748 Color: 3
Size: 214722 Color: 1

Bin 413: 362 of cap free
Amount of items: 2
Items: 
Size: 562047 Color: 1
Size: 437592 Color: 2

Bin 414: 364 of cap free
Amount of items: 2
Items: 
Size: 519023 Color: 4
Size: 480614 Color: 2

Bin 415: 364 of cap free
Amount of items: 2
Items: 
Size: 594334 Color: 3
Size: 405303 Color: 1

Bin 416: 365 of cap free
Amount of items: 2
Items: 
Size: 744803 Color: 0
Size: 254833 Color: 4

Bin 417: 372 of cap free
Amount of items: 2
Items: 
Size: 694515 Color: 1
Size: 305114 Color: 4

Bin 418: 376 of cap free
Amount of items: 2
Items: 
Size: 623017 Color: 3
Size: 376608 Color: 0

Bin 419: 380 of cap free
Amount of items: 2
Items: 
Size: 659881 Color: 0
Size: 339740 Color: 4

Bin 420: 381 of cap free
Amount of items: 2
Items: 
Size: 503102 Color: 1
Size: 496518 Color: 2

Bin 421: 382 of cap free
Amount of items: 3
Items: 
Size: 700434 Color: 2
Size: 150762 Color: 0
Size: 148423 Color: 4

Bin 422: 383 of cap free
Amount of items: 2
Items: 
Size: 684266 Color: 3
Size: 315352 Color: 1

Bin 423: 384 of cap free
Amount of items: 2
Items: 
Size: 559736 Color: 3
Size: 439881 Color: 2

Bin 424: 384 of cap free
Amount of items: 2
Items: 
Size: 600831 Color: 4
Size: 398786 Color: 3

Bin 425: 385 of cap free
Amount of items: 2
Items: 
Size: 511727 Color: 2
Size: 487889 Color: 3

Bin 426: 387 of cap free
Amount of items: 2
Items: 
Size: 565880 Color: 0
Size: 433734 Color: 3

Bin 427: 388 of cap free
Amount of items: 2
Items: 
Size: 653076 Color: 0
Size: 346537 Color: 4

Bin 428: 390 of cap free
Amount of items: 2
Items: 
Size: 602623 Color: 4
Size: 396988 Color: 2

Bin 429: 398 of cap free
Amount of items: 2
Items: 
Size: 676413 Color: 3
Size: 323190 Color: 2

Bin 430: 398 of cap free
Amount of items: 2
Items: 
Size: 782714 Color: 1
Size: 216889 Color: 0

Bin 431: 399 of cap free
Amount of items: 2
Items: 
Size: 601646 Color: 1
Size: 397956 Color: 4

Bin 432: 403 of cap free
Amount of items: 2
Items: 
Size: 789509 Color: 0
Size: 210089 Color: 4

Bin 433: 406 of cap free
Amount of items: 2
Items: 
Size: 662414 Color: 3
Size: 337181 Color: 1

Bin 434: 409 of cap free
Amount of items: 3
Items: 
Size: 361251 Color: 3
Size: 323739 Color: 4
Size: 314602 Color: 1

Bin 435: 411 of cap free
Amount of items: 3
Items: 
Size: 377763 Color: 4
Size: 314922 Color: 0
Size: 306905 Color: 0

Bin 436: 411 of cap free
Amount of items: 2
Items: 
Size: 686936 Color: 1
Size: 312654 Color: 3

Bin 437: 412 of cap free
Amount of items: 3
Items: 
Size: 353942 Color: 3
Size: 325172 Color: 4
Size: 320475 Color: 4

Bin 438: 419 of cap free
Amount of items: 2
Items: 
Size: 521533 Color: 4
Size: 478049 Color: 0

Bin 439: 419 of cap free
Amount of items: 2
Items: 
Size: 543296 Color: 0
Size: 456286 Color: 2

Bin 440: 421 of cap free
Amount of items: 2
Items: 
Size: 506583 Color: 0
Size: 492997 Color: 2

Bin 441: 421 of cap free
Amount of items: 2
Items: 
Size: 592547 Color: 3
Size: 407033 Color: 1

Bin 442: 423 of cap free
Amount of items: 2
Items: 
Size: 782607 Color: 3
Size: 216971 Color: 1

Bin 443: 424 of cap free
Amount of items: 3
Items: 
Size: 756219 Color: 0
Size: 130834 Color: 2
Size: 112524 Color: 4

Bin 444: 428 of cap free
Amount of items: 2
Items: 
Size: 747350 Color: 3
Size: 252223 Color: 2

Bin 445: 429 of cap free
Amount of items: 2
Items: 
Size: 571172 Color: 1
Size: 428400 Color: 2

Bin 446: 430 of cap free
Amount of items: 2
Items: 
Size: 748051 Color: 1
Size: 251520 Color: 4

Bin 447: 432 of cap free
Amount of items: 2
Items: 
Size: 683065 Color: 3
Size: 316504 Color: 4

Bin 448: 432 of cap free
Amount of items: 3
Items: 
Size: 709958 Color: 2
Size: 145225 Color: 4
Size: 144386 Color: 0

Bin 449: 433 of cap free
Amount of items: 2
Items: 
Size: 649818 Color: 0
Size: 349750 Color: 3

Bin 450: 434 of cap free
Amount of items: 3
Items: 
Size: 627871 Color: 4
Size: 193566 Color: 2
Size: 178130 Color: 3

Bin 451: 434 of cap free
Amount of items: 2
Items: 
Size: 715809 Color: 4
Size: 283758 Color: 0

Bin 452: 436 of cap free
Amount of items: 2
Items: 
Size: 625964 Color: 4
Size: 373601 Color: 3

Bin 453: 436 of cap free
Amount of items: 2
Items: 
Size: 645484 Color: 4
Size: 354081 Color: 0

Bin 454: 438 of cap free
Amount of items: 2
Items: 
Size: 502104 Color: 1
Size: 497459 Color: 4

Bin 455: 438 of cap free
Amount of items: 2
Items: 
Size: 557283 Color: 2
Size: 442280 Color: 4

Bin 456: 438 of cap free
Amount of items: 2
Items: 
Size: 795882 Color: 0
Size: 203681 Color: 3

Bin 457: 445 of cap free
Amount of items: 2
Items: 
Size: 640748 Color: 0
Size: 358808 Color: 4

Bin 458: 447 of cap free
Amount of items: 3
Items: 
Size: 570432 Color: 2
Size: 217528 Color: 3
Size: 211594 Color: 0

Bin 459: 447 of cap free
Amount of items: 2
Items: 
Size: 703817 Color: 4
Size: 295737 Color: 1

Bin 460: 449 of cap free
Amount of items: 2
Items: 
Size: 624440 Color: 2
Size: 375112 Color: 0

Bin 461: 450 of cap free
Amount of items: 2
Items: 
Size: 798310 Color: 3
Size: 201241 Color: 0

Bin 462: 451 of cap free
Amount of items: 2
Items: 
Size: 731884 Color: 0
Size: 267666 Color: 3

Bin 463: 451 of cap free
Amount of items: 2
Items: 
Size: 754683 Color: 1
Size: 244867 Color: 3

Bin 464: 452 of cap free
Amount of items: 2
Items: 
Size: 534279 Color: 3
Size: 465270 Color: 1

Bin 465: 453 of cap free
Amount of items: 3
Items: 
Size: 637912 Color: 4
Size: 183372 Color: 2
Size: 178264 Color: 1

Bin 466: 456 of cap free
Amount of items: 2
Items: 
Size: 522266 Color: 1
Size: 477279 Color: 0

Bin 467: 460 of cap free
Amount of items: 2
Items: 
Size: 515419 Color: 0
Size: 484122 Color: 2

Bin 468: 463 of cap free
Amount of items: 2
Items: 
Size: 742422 Color: 4
Size: 257116 Color: 0

Bin 469: 463 of cap free
Amount of items: 2
Items: 
Size: 752180 Color: 0
Size: 247358 Color: 2

Bin 470: 466 of cap free
Amount of items: 3
Items: 
Size: 341487 Color: 3
Size: 336295 Color: 2
Size: 321753 Color: 0

Bin 471: 466 of cap free
Amount of items: 2
Items: 
Size: 683364 Color: 1
Size: 316171 Color: 2

Bin 472: 467 of cap free
Amount of items: 2
Items: 
Size: 574639 Color: 3
Size: 424895 Color: 4

Bin 473: 474 of cap free
Amount of items: 3
Items: 
Size: 726680 Color: 0
Size: 147035 Color: 2
Size: 125812 Color: 4

Bin 474: 478 of cap free
Amount of items: 2
Items: 
Size: 663034 Color: 1
Size: 336489 Color: 4

Bin 475: 484 of cap free
Amount of items: 2
Items: 
Size: 673116 Color: 0
Size: 326401 Color: 1

Bin 476: 485 of cap free
Amount of items: 2
Items: 
Size: 575681 Color: 1
Size: 423835 Color: 4

Bin 477: 491 of cap free
Amount of items: 2
Items: 
Size: 797164 Color: 0
Size: 202346 Color: 3

Bin 478: 504 of cap free
Amount of items: 2
Items: 
Size: 557575 Color: 1
Size: 441922 Color: 2

Bin 479: 510 of cap free
Amount of items: 2
Items: 
Size: 607928 Color: 0
Size: 391563 Color: 1

Bin 480: 511 of cap free
Amount of items: 2
Items: 
Size: 644058 Color: 0
Size: 355432 Color: 4

Bin 481: 521 of cap free
Amount of items: 2
Items: 
Size: 719884 Color: 1
Size: 279596 Color: 2

Bin 482: 534 of cap free
Amount of items: 2
Items: 
Size: 661944 Color: 0
Size: 337523 Color: 2

Bin 483: 535 of cap free
Amount of items: 2
Items: 
Size: 777507 Color: 3
Size: 221959 Color: 0

Bin 484: 546 of cap free
Amount of items: 2
Items: 
Size: 646336 Color: 4
Size: 353119 Color: 1

Bin 485: 550 of cap free
Amount of items: 2
Items: 
Size: 700084 Color: 0
Size: 299367 Color: 3

Bin 486: 557 of cap free
Amount of items: 3
Items: 
Size: 685907 Color: 3
Size: 164196 Color: 4
Size: 149341 Color: 4

Bin 487: 565 of cap free
Amount of items: 2
Items: 
Size: 739235 Color: 3
Size: 260201 Color: 0

Bin 488: 566 of cap free
Amount of items: 2
Items: 
Size: 599300 Color: 3
Size: 400135 Color: 0

Bin 489: 568 of cap free
Amount of items: 2
Items: 
Size: 602519 Color: 1
Size: 396914 Color: 0

Bin 490: 570 of cap free
Amount of items: 2
Items: 
Size: 759270 Color: 4
Size: 240161 Color: 2

Bin 491: 573 of cap free
Amount of items: 3
Items: 
Size: 593822 Color: 1
Size: 214666 Color: 2
Size: 190940 Color: 3

Bin 492: 576 of cap free
Amount of items: 2
Items: 
Size: 706139 Color: 0
Size: 293286 Color: 4

Bin 493: 581 of cap free
Amount of items: 3
Items: 
Size: 699022 Color: 4
Size: 160890 Color: 2
Size: 139508 Color: 1

Bin 494: 581 of cap free
Amount of items: 2
Items: 
Size: 710566 Color: 0
Size: 288854 Color: 1

Bin 495: 583 of cap free
Amount of items: 3
Items: 
Size: 369377 Color: 4
Size: 315126 Color: 3
Size: 314915 Color: 3

Bin 496: 583 of cap free
Amount of items: 3
Items: 
Size: 593167 Color: 4
Size: 217782 Color: 2
Size: 188469 Color: 4

Bin 497: 583 of cap free
Amount of items: 3
Items: 
Size: 709007 Color: 2
Size: 157753 Color: 0
Size: 132658 Color: 0

Bin 498: 591 of cap free
Amount of items: 2
Items: 
Size: 781049 Color: 4
Size: 218361 Color: 1

Bin 499: 597 of cap free
Amount of items: 2
Items: 
Size: 577940 Color: 0
Size: 421464 Color: 1

Bin 500: 599 of cap free
Amount of items: 3
Items: 
Size: 481326 Color: 3
Size: 261670 Color: 1
Size: 256406 Color: 4

Bin 501: 604 of cap free
Amount of items: 3
Items: 
Size: 373353 Color: 4
Size: 313744 Color: 0
Size: 312300 Color: 0

Bin 502: 604 of cap free
Amount of items: 2
Items: 
Size: 627671 Color: 4
Size: 371726 Color: 1

Bin 503: 606 of cap free
Amount of items: 3
Items: 
Size: 447356 Color: 3
Size: 276106 Color: 4
Size: 275933 Color: 0

Bin 504: 606 of cap free
Amount of items: 2
Items: 
Size: 557600 Color: 4
Size: 441795 Color: 1

Bin 505: 608 of cap free
Amount of items: 2
Items: 
Size: 531348 Color: 0
Size: 468045 Color: 2

Bin 506: 609 of cap free
Amount of items: 2
Items: 
Size: 618008 Color: 3
Size: 381384 Color: 2

Bin 507: 611 of cap free
Amount of items: 2
Items: 
Size: 647782 Color: 1
Size: 351608 Color: 3

Bin 508: 612 of cap free
Amount of items: 2
Items: 
Size: 508435 Color: 0
Size: 490954 Color: 2

Bin 509: 616 of cap free
Amount of items: 2
Items: 
Size: 555393 Color: 2
Size: 443992 Color: 4

Bin 510: 617 of cap free
Amount of items: 2
Items: 
Size: 694182 Color: 1
Size: 305202 Color: 2

Bin 511: 617 of cap free
Amount of items: 2
Items: 
Size: 779802 Color: 4
Size: 219582 Color: 0

Bin 512: 622 of cap free
Amount of items: 2
Items: 
Size: 654271 Color: 0
Size: 345108 Color: 3

Bin 513: 628 of cap free
Amount of items: 2
Items: 
Size: 617356 Color: 3
Size: 382017 Color: 1

Bin 514: 628 of cap free
Amount of items: 2
Items: 
Size: 722437 Color: 0
Size: 276936 Color: 3

Bin 515: 631 of cap free
Amount of items: 3
Items: 
Size: 535415 Color: 4
Size: 232687 Color: 2
Size: 231268 Color: 2

Bin 516: 636 of cap free
Amount of items: 3
Items: 
Size: 339084 Color: 0
Size: 332637 Color: 3
Size: 327644 Color: 2

Bin 517: 637 of cap free
Amount of items: 3
Items: 
Size: 669949 Color: 2
Size: 167042 Color: 3
Size: 162373 Color: 0

Bin 518: 638 of cap free
Amount of items: 2
Items: 
Size: 596294 Color: 4
Size: 403069 Color: 3

Bin 519: 638 of cap free
Amount of items: 2
Items: 
Size: 776066 Color: 1
Size: 223297 Color: 2

Bin 520: 639 of cap free
Amount of items: 3
Items: 
Size: 429350 Color: 4
Size: 287707 Color: 0
Size: 282305 Color: 1

Bin 521: 653 of cap free
Amount of items: 2
Items: 
Size: 628210 Color: 4
Size: 371138 Color: 2

Bin 522: 654 of cap free
Amount of items: 3
Items: 
Size: 661756 Color: 2
Size: 169012 Color: 1
Size: 168579 Color: 4

Bin 523: 655 of cap free
Amount of items: 2
Items: 
Size: 652530 Color: 3
Size: 346816 Color: 0

Bin 524: 657 of cap free
Amount of items: 2
Items: 
Size: 629450 Color: 4
Size: 369894 Color: 2

Bin 525: 662 of cap free
Amount of items: 2
Items: 
Size: 705671 Color: 3
Size: 293668 Color: 0

Bin 526: 670 of cap free
Amount of items: 2
Items: 
Size: 543982 Color: 3
Size: 455349 Color: 4

Bin 527: 670 of cap free
Amount of items: 2
Items: 
Size: 677090 Color: 4
Size: 322241 Color: 1

Bin 528: 678 of cap free
Amount of items: 2
Items: 
Size: 519950 Color: 2
Size: 479373 Color: 0

Bin 529: 685 of cap free
Amount of items: 3
Items: 
Size: 459719 Color: 1
Size: 273536 Color: 0
Size: 266061 Color: 2

Bin 530: 687 of cap free
Amount of items: 2
Items: 
Size: 751263 Color: 4
Size: 248051 Color: 1

Bin 531: 688 of cap free
Amount of items: 2
Items: 
Size: 633045 Color: 3
Size: 366268 Color: 2

Bin 532: 690 of cap free
Amount of items: 2
Items: 
Size: 682435 Color: 3
Size: 316876 Color: 0

Bin 533: 695 of cap free
Amount of items: 2
Items: 
Size: 549565 Color: 0
Size: 449741 Color: 3

Bin 534: 702 of cap free
Amount of items: 2
Items: 
Size: 531140 Color: 0
Size: 468159 Color: 1

Bin 535: 703 of cap free
Amount of items: 3
Items: 
Size: 769305 Color: 1
Size: 120690 Color: 2
Size: 109303 Color: 0

Bin 536: 703 of cap free
Amount of items: 2
Items: 
Size: 771081 Color: 3
Size: 228217 Color: 2

Bin 537: 710 of cap free
Amount of items: 2
Items: 
Size: 563052 Color: 4
Size: 436239 Color: 3

Bin 538: 712 of cap free
Amount of items: 2
Items: 
Size: 649227 Color: 0
Size: 350062 Color: 2

Bin 539: 712 of cap free
Amount of items: 2
Items: 
Size: 670541 Color: 4
Size: 328748 Color: 1

Bin 540: 717 of cap free
Amount of items: 2
Items: 
Size: 748461 Color: 4
Size: 250823 Color: 1

Bin 541: 722 of cap free
Amount of items: 3
Items: 
Size: 672422 Color: 4
Size: 172733 Color: 2
Size: 154124 Color: 1

Bin 542: 728 of cap free
Amount of items: 2
Items: 
Size: 516038 Color: 1
Size: 483235 Color: 3

Bin 543: 731 of cap free
Amount of items: 2
Items: 
Size: 729470 Color: 3
Size: 269800 Color: 0

Bin 544: 742 of cap free
Amount of items: 2
Items: 
Size: 794039 Color: 4
Size: 205220 Color: 0

Bin 545: 750 of cap free
Amount of items: 3
Items: 
Size: 632665 Color: 1
Size: 187540 Color: 2
Size: 179046 Color: 3

Bin 546: 751 of cap free
Amount of items: 2
Items: 
Size: 679798 Color: 4
Size: 319452 Color: 2

Bin 547: 755 of cap free
Amount of items: 3
Items: 
Size: 720800 Color: 4
Size: 151152 Color: 2
Size: 127294 Color: 1

Bin 548: 757 of cap free
Amount of items: 2
Items: 
Size: 708845 Color: 4
Size: 290399 Color: 1

Bin 549: 774 of cap free
Amount of items: 3
Items: 
Size: 610096 Color: 1
Size: 207099 Color: 2
Size: 182032 Color: 3

Bin 550: 775 of cap free
Amount of items: 2
Items: 
Size: 532696 Color: 4
Size: 466530 Color: 3

Bin 551: 791 of cap free
Amount of items: 2
Items: 
Size: 594030 Color: 4
Size: 405180 Color: 1

Bin 552: 795 of cap free
Amount of items: 2
Items: 
Size: 506404 Color: 3
Size: 492802 Color: 2

Bin 553: 798 of cap free
Amount of items: 2
Items: 
Size: 726701 Color: 0
Size: 272502 Color: 4

Bin 554: 799 of cap free
Amount of items: 2
Items: 
Size: 519891 Color: 2
Size: 479311 Color: 3

Bin 555: 802 of cap free
Amount of items: 3
Items: 
Size: 462624 Color: 1
Size: 269428 Color: 2
Size: 267147 Color: 4

Bin 556: 805 of cap free
Amount of items: 2
Items: 
Size: 614860 Color: 3
Size: 384336 Color: 4

Bin 557: 825 of cap free
Amount of items: 3
Items: 
Size: 607544 Color: 1
Size: 209865 Color: 2
Size: 181767 Color: 0

Bin 558: 828 of cap free
Amount of items: 2
Items: 
Size: 555081 Color: 0
Size: 444092 Color: 1

Bin 559: 832 of cap free
Amount of items: 2
Items: 
Size: 550639 Color: 0
Size: 448530 Color: 1

Bin 560: 834 of cap free
Amount of items: 2
Items: 
Size: 759853 Color: 3
Size: 239314 Color: 2

Bin 561: 835 of cap free
Amount of items: 2
Items: 
Size: 571863 Color: 2
Size: 427303 Color: 3

Bin 562: 838 of cap free
Amount of items: 2
Items: 
Size: 624836 Color: 0
Size: 374327 Color: 3

Bin 563: 848 of cap free
Amount of items: 2
Items: 
Size: 568211 Color: 1
Size: 430942 Color: 2

Bin 564: 857 of cap free
Amount of items: 3
Items: 
Size: 797159 Color: 4
Size: 101735 Color: 2
Size: 100250 Color: 0

Bin 565: 862 of cap free
Amount of items: 2
Items: 
Size: 693351 Color: 0
Size: 305788 Color: 1

Bin 566: 862 of cap free
Amount of items: 2
Items: 
Size: 753959 Color: 1
Size: 245180 Color: 2

Bin 567: 876 of cap free
Amount of items: 3
Items: 
Size: 540979 Color: 4
Size: 229211 Color: 1
Size: 228935 Color: 3

Bin 568: 878 of cap free
Amount of items: 2
Items: 
Size: 654229 Color: 2
Size: 344894 Color: 3

Bin 569: 894 of cap free
Amount of items: 2
Items: 
Size: 505428 Color: 4
Size: 493679 Color: 3

Bin 570: 899 of cap free
Amount of items: 2
Items: 
Size: 619210 Color: 3
Size: 379892 Color: 0

Bin 571: 900 of cap free
Amount of items: 2
Items: 
Size: 569437 Color: 0
Size: 429664 Color: 4

Bin 572: 907 of cap free
Amount of items: 2
Items: 
Size: 791419 Color: 4
Size: 207675 Color: 1

Bin 573: 913 of cap free
Amount of items: 2
Items: 
Size: 513357 Color: 2
Size: 485731 Color: 4

Bin 574: 916 of cap free
Amount of items: 2
Items: 
Size: 772801 Color: 1
Size: 226284 Color: 4

Bin 575: 917 of cap free
Amount of items: 2
Items: 
Size: 528712 Color: 4
Size: 470372 Color: 2

Bin 576: 921 of cap free
Amount of items: 2
Items: 
Size: 776128 Color: 1
Size: 222952 Color: 4

Bin 577: 923 of cap free
Amount of items: 2
Items: 
Size: 759240 Color: 3
Size: 239838 Color: 0

Bin 578: 926 of cap free
Amount of items: 2
Items: 
Size: 646924 Color: 1
Size: 352151 Color: 2

Bin 579: 926 of cap free
Amount of items: 2
Items: 
Size: 673881 Color: 4
Size: 325194 Color: 1

Bin 580: 931 of cap free
Amount of items: 2
Items: 
Size: 575184 Color: 4
Size: 423886 Color: 1

Bin 581: 932 of cap free
Amount of items: 2
Items: 
Size: 656167 Color: 3
Size: 342902 Color: 2

Bin 582: 936 of cap free
Amount of items: 3
Items: 
Size: 727547 Color: 0
Size: 138529 Color: 2
Size: 132989 Color: 1

Bin 583: 942 of cap free
Amount of items: 3
Items: 
Size: 589231 Color: 2
Size: 214948 Color: 0
Size: 194880 Color: 0

Bin 584: 956 of cap free
Amount of items: 2
Items: 
Size: 600461 Color: 1
Size: 398584 Color: 0

Bin 585: 982 of cap free
Amount of items: 2
Items: 
Size: 577570 Color: 3
Size: 421449 Color: 4

Bin 586: 995 of cap free
Amount of items: 2
Items: 
Size: 768677 Color: 3
Size: 230329 Color: 0

Bin 587: 1005 of cap free
Amount of items: 2
Items: 
Size: 673715 Color: 0
Size: 325281 Color: 2

Bin 588: 1030 of cap free
Amount of items: 2
Items: 
Size: 616616 Color: 0
Size: 382355 Color: 2

Bin 589: 1032 of cap free
Amount of items: 2
Items: 
Size: 791620 Color: 3
Size: 207349 Color: 2

Bin 590: 1033 of cap free
Amount of items: 2
Items: 
Size: 635186 Color: 1
Size: 363782 Color: 3

Bin 591: 1037 of cap free
Amount of items: 2
Items: 
Size: 759894 Color: 0
Size: 239070 Color: 4

Bin 592: 1045 of cap free
Amount of items: 2
Items: 
Size: 773171 Color: 1
Size: 225785 Color: 2

Bin 593: 1049 of cap free
Amount of items: 2
Items: 
Size: 680480 Color: 1
Size: 318472 Color: 0

Bin 594: 1051 of cap free
Amount of items: 2
Items: 
Size: 508302 Color: 2
Size: 490648 Color: 1

Bin 595: 1051 of cap free
Amount of items: 2
Items: 
Size: 643903 Color: 1
Size: 355047 Color: 4

Bin 596: 1058 of cap free
Amount of items: 2
Items: 
Size: 670446 Color: 3
Size: 328497 Color: 0

Bin 597: 1061 of cap free
Amount of items: 2
Items: 
Size: 574192 Color: 1
Size: 424748 Color: 4

Bin 598: 1063 of cap free
Amount of items: 2
Items: 
Size: 585962 Color: 3
Size: 412976 Color: 1

Bin 599: 1064 of cap free
Amount of items: 2
Items: 
Size: 777539 Color: 3
Size: 221398 Color: 2

Bin 600: 1068 of cap free
Amount of items: 2
Items: 
Size: 725249 Color: 4
Size: 273684 Color: 0

Bin 601: 1071 of cap free
Amount of items: 2
Items: 
Size: 567532 Color: 4
Size: 431398 Color: 3

Bin 602: 1086 of cap free
Amount of items: 2
Items: 
Size: 676727 Color: 3
Size: 322188 Color: 4

Bin 603: 1091 of cap free
Amount of items: 2
Items: 
Size: 755968 Color: 3
Size: 242942 Color: 4

Bin 604: 1093 of cap free
Amount of items: 2
Items: 
Size: 526021 Color: 4
Size: 472887 Color: 2

Bin 605: 1129 of cap free
Amount of items: 2
Items: 
Size: 553146 Color: 0
Size: 445726 Color: 1

Bin 606: 1139 of cap free
Amount of items: 2
Items: 
Size: 698607 Color: 1
Size: 300255 Color: 3

Bin 607: 1140 of cap free
Amount of items: 2
Items: 
Size: 788732 Color: 3
Size: 210129 Color: 2

Bin 608: 1141 of cap free
Amount of items: 2
Items: 
Size: 698583 Color: 1
Size: 300277 Color: 2

Bin 609: 1148 of cap free
Amount of items: 3
Items: 
Size: 457426 Color: 1
Size: 272384 Color: 4
Size: 269043 Color: 2

Bin 610: 1150 of cap free
Amount of items: 2
Items: 
Size: 713003 Color: 4
Size: 285848 Color: 3

Bin 611: 1165 of cap free
Amount of items: 2
Items: 
Size: 581358 Color: 2
Size: 417478 Color: 4

Bin 612: 1173 of cap free
Amount of items: 3
Items: 
Size: 645597 Color: 0
Size: 178154 Color: 2
Size: 175077 Color: 3

Bin 613: 1175 of cap free
Amount of items: 2
Items: 
Size: 588128 Color: 1
Size: 410698 Color: 3

Bin 614: 1175 of cap free
Amount of items: 2
Items: 
Size: 714870 Color: 4
Size: 283956 Color: 2

Bin 615: 1181 of cap free
Amount of items: 3
Items: 
Size: 338770 Color: 1
Size: 330085 Color: 4
Size: 329965 Color: 4

Bin 616: 1188 of cap free
Amount of items: 2
Items: 
Size: 666172 Color: 2
Size: 332641 Color: 4

Bin 617: 1192 of cap free
Amount of items: 2
Items: 
Size: 534170 Color: 4
Size: 464639 Color: 0

Bin 618: 1192 of cap free
Amount of items: 2
Items: 
Size: 562818 Color: 3
Size: 435991 Color: 1

Bin 619: 1199 of cap free
Amount of items: 2
Items: 
Size: 763599 Color: 4
Size: 235203 Color: 3

Bin 620: 1200 of cap free
Amount of items: 2
Items: 
Size: 520900 Color: 4
Size: 477901 Color: 3

Bin 621: 1223 of cap free
Amount of items: 2
Items: 
Size: 650698 Color: 3
Size: 348080 Color: 4

Bin 622: 1230 of cap free
Amount of items: 2
Items: 
Size: 744166 Color: 1
Size: 254605 Color: 4

Bin 623: 1245 of cap free
Amount of items: 2
Items: 
Size: 640464 Color: 1
Size: 358292 Color: 3

Bin 624: 1248 of cap free
Amount of items: 2
Items: 
Size: 708581 Color: 0
Size: 290172 Color: 1

Bin 625: 1254 of cap free
Amount of items: 2
Items: 
Size: 771224 Color: 1
Size: 227523 Color: 3

Bin 626: 1255 of cap free
Amount of items: 2
Items: 
Size: 600264 Color: 3
Size: 398482 Color: 4

Bin 627: 1259 of cap free
Amount of items: 2
Items: 
Size: 709873 Color: 0
Size: 288869 Color: 2

Bin 628: 1260 of cap free
Amount of items: 3
Items: 
Size: 586630 Color: 2
Size: 217620 Color: 0
Size: 194491 Color: 4

Bin 629: 1265 of cap free
Amount of items: 2
Items: 
Size: 678443 Color: 4
Size: 320293 Color: 1

Bin 630: 1270 of cap free
Amount of items: 2
Items: 
Size: 767133 Color: 0
Size: 231598 Color: 1

Bin 631: 1280 of cap free
Amount of items: 2
Items: 
Size: 654242 Color: 3
Size: 344479 Color: 1

Bin 632: 1284 of cap free
Amount of items: 2
Items: 
Size: 548321 Color: 2
Size: 450396 Color: 1

Bin 633: 1286 of cap free
Amount of items: 2
Items: 
Size: 569398 Color: 1
Size: 429317 Color: 2

Bin 634: 1293 of cap free
Amount of items: 2
Items: 
Size: 668626 Color: 4
Size: 330082 Color: 2

Bin 635: 1313 of cap free
Amount of items: 2
Items: 
Size: 632780 Color: 3
Size: 365908 Color: 4

Bin 636: 1316 of cap free
Amount of items: 3
Items: 
Size: 727504 Color: 0
Size: 149064 Color: 2
Size: 122117 Color: 1

Bin 637: 1317 of cap free
Amount of items: 2
Items: 
Size: 735761 Color: 4
Size: 262923 Color: 1

Bin 638: 1331 of cap free
Amount of items: 2
Items: 
Size: 611077 Color: 4
Size: 387593 Color: 1

Bin 639: 1336 of cap free
Amount of items: 3
Items: 
Size: 599828 Color: 0
Size: 214770 Color: 2
Size: 184067 Color: 3

Bin 640: 1338 of cap free
Amount of items: 2
Items: 
Size: 552956 Color: 0
Size: 445707 Color: 4

Bin 641: 1340 of cap free
Amount of items: 2
Items: 
Size: 530685 Color: 3
Size: 467976 Color: 4

Bin 642: 1343 of cap free
Amount of items: 3
Items: 
Size: 377574 Color: 4
Size: 313361 Color: 3
Size: 307723 Color: 3

Bin 643: 1355 of cap free
Amount of items: 2
Items: 
Size: 539043 Color: 4
Size: 459603 Color: 3

Bin 644: 1377 of cap free
Amount of items: 2
Items: 
Size: 523968 Color: 4
Size: 474656 Color: 0

Bin 645: 1380 of cap free
Amount of items: 2
Items: 
Size: 638602 Color: 3
Size: 360019 Color: 0

Bin 646: 1381 of cap free
Amount of items: 2
Items: 
Size: 534427 Color: 0
Size: 464193 Color: 4

Bin 647: 1399 of cap free
Amount of items: 3
Items: 
Size: 656736 Color: 1
Size: 171112 Color: 0
Size: 170754 Color: 3

Bin 648: 1408 of cap free
Amount of items: 2
Items: 
Size: 696598 Color: 3
Size: 301995 Color: 1

Bin 649: 1410 of cap free
Amount of items: 2
Items: 
Size: 649950 Color: 1
Size: 348641 Color: 2

Bin 650: 1431 of cap free
Amount of items: 2
Items: 
Size: 790996 Color: 4
Size: 207574 Color: 0

Bin 651: 1436 of cap free
Amount of items: 3
Items: 
Size: 501614 Color: 0
Size: 250238 Color: 1
Size: 246713 Color: 2

Bin 652: 1437 of cap free
Amount of items: 2
Items: 
Size: 585228 Color: 4
Size: 413336 Color: 3

Bin 653: 1453 of cap free
Amount of items: 2
Items: 
Size: 506402 Color: 4
Size: 492146 Color: 2

Bin 654: 1461 of cap free
Amount of items: 2
Items: 
Size: 748003 Color: 4
Size: 250537 Color: 1

Bin 655: 1487 of cap free
Amount of items: 2
Items: 
Size: 577570 Color: 0
Size: 420944 Color: 2

Bin 656: 1488 of cap free
Amount of items: 2
Items: 
Size: 619461 Color: 0
Size: 379052 Color: 3

Bin 657: 1489 of cap free
Amount of items: 2
Items: 
Size: 531973 Color: 2
Size: 466539 Color: 1

Bin 658: 1503 of cap free
Amount of items: 2
Items: 
Size: 554735 Color: 4
Size: 443763 Color: 3

Bin 659: 1509 of cap free
Amount of items: 2
Items: 
Size: 591429 Color: 4
Size: 407063 Color: 3

Bin 660: 1525 of cap free
Amount of items: 2
Items: 
Size: 714996 Color: 0
Size: 283480 Color: 1

Bin 661: 1549 of cap free
Amount of items: 2
Items: 
Size: 703895 Color: 3
Size: 294557 Color: 2

Bin 662: 1554 of cap free
Amount of items: 2
Items: 
Size: 678410 Color: 4
Size: 320037 Color: 3

Bin 663: 1556 of cap free
Amount of items: 2
Items: 
Size: 574547 Color: 4
Size: 423898 Color: 2

Bin 664: 1559 of cap free
Amount of items: 2
Items: 
Size: 499571 Color: 4
Size: 498871 Color: 0

Bin 665: 1565 of cap free
Amount of items: 2
Items: 
Size: 757137 Color: 3
Size: 241299 Color: 0

Bin 666: 1570 of cap free
Amount of items: 2
Items: 
Size: 571164 Color: 0
Size: 427267 Color: 4

Bin 667: 1573 of cap free
Amount of items: 3
Items: 
Size: 504754 Color: 2
Size: 246989 Color: 4
Size: 246685 Color: 0

Bin 668: 1581 of cap free
Amount of items: 2
Items: 
Size: 534237 Color: 1
Size: 464183 Color: 2

Bin 669: 1589 of cap free
Amount of items: 2
Items: 
Size: 714871 Color: 1
Size: 283541 Color: 0

Bin 670: 1596 of cap free
Amount of items: 2
Items: 
Size: 501060 Color: 3
Size: 497345 Color: 4

Bin 671: 1611 of cap free
Amount of items: 2
Items: 
Size: 679993 Color: 4
Size: 318397 Color: 3

Bin 672: 1621 of cap free
Amount of items: 2
Items: 
Size: 607386 Color: 4
Size: 390994 Color: 0

Bin 673: 1642 of cap free
Amount of items: 3
Items: 
Size: 637738 Color: 1
Size: 184313 Color: 2
Size: 176308 Color: 0

Bin 674: 1656 of cap free
Amount of items: 2
Items: 
Size: 690267 Color: 0
Size: 308078 Color: 3

Bin 675: 1679 of cap free
Amount of items: 2
Items: 
Size: 761472 Color: 3
Size: 236850 Color: 4

Bin 676: 1687 of cap free
Amount of items: 3
Items: 
Size: 715150 Color: 4
Size: 152216 Color: 2
Size: 130948 Color: 0

Bin 677: 1711 of cap free
Amount of items: 3
Items: 
Size: 494293 Color: 4
Size: 254324 Color: 1
Size: 249673 Color: 3

Bin 678: 1726 of cap free
Amount of items: 3
Items: 
Size: 607475 Color: 3
Size: 209747 Color: 2
Size: 181053 Color: 0

Bin 679: 1735 of cap free
Amount of items: 2
Items: 
Size: 774978 Color: 3
Size: 223288 Color: 1

Bin 680: 1744 of cap free
Amount of items: 2
Items: 
Size: 549093 Color: 1
Size: 449164 Color: 4

Bin 681: 1784 of cap free
Amount of items: 2
Items: 
Size: 720395 Color: 0
Size: 277822 Color: 3

Bin 682: 1848 of cap free
Amount of items: 2
Items: 
Size: 636584 Color: 3
Size: 361569 Color: 2

Bin 683: 1851 of cap free
Amount of items: 3
Items: 
Size: 426849 Color: 2
Size: 300182 Color: 4
Size: 271119 Color: 1

Bin 684: 1853 of cap free
Amount of items: 3
Items: 
Size: 401456 Color: 0
Size: 298499 Color: 1
Size: 298193 Color: 3

Bin 685: 1854 of cap free
Amount of items: 2
Items: 
Size: 634728 Color: 4
Size: 363419 Color: 2

Bin 686: 1895 of cap free
Amount of items: 2
Items: 
Size: 538575 Color: 2
Size: 459531 Color: 0

Bin 687: 1904 of cap free
Amount of items: 2
Items: 
Size: 687579 Color: 1
Size: 310518 Color: 2

Bin 688: 1913 of cap free
Amount of items: 3
Items: 
Size: 352942 Color: 1
Size: 324846 Color: 3
Size: 320300 Color: 4

Bin 689: 1915 of cap free
Amount of items: 2
Items: 
Size: 718146 Color: 1
Size: 279940 Color: 4

Bin 690: 1916 of cap free
Amount of items: 2
Items: 
Size: 661621 Color: 4
Size: 336464 Color: 2

Bin 691: 1963 of cap free
Amount of items: 2
Items: 
Size: 607340 Color: 4
Size: 390698 Color: 3

Bin 692: 1973 of cap free
Amount of items: 2
Items: 
Size: 571308 Color: 4
Size: 426720 Color: 0

Bin 693: 1981 of cap free
Amount of items: 2
Items: 
Size: 543601 Color: 2
Size: 454419 Color: 4

Bin 694: 2041 of cap free
Amount of items: 2
Items: 
Size: 600379 Color: 4
Size: 397581 Color: 0

Bin 695: 2048 of cap free
Amount of items: 2
Items: 
Size: 643351 Color: 3
Size: 354602 Color: 1

Bin 696: 2073 of cap free
Amount of items: 2
Items: 
Size: 799898 Color: 3
Size: 198030 Color: 1

Bin 697: 2086 of cap free
Amount of items: 2
Items: 
Size: 772231 Color: 3
Size: 225684 Color: 4

Bin 698: 2091 of cap free
Amount of items: 2
Items: 
Size: 606077 Color: 0
Size: 391833 Color: 2

Bin 699: 2097 of cap free
Amount of items: 2
Items: 
Size: 776928 Color: 3
Size: 220976 Color: 2

Bin 700: 2099 of cap free
Amount of items: 2
Items: 
Size: 661767 Color: 3
Size: 336135 Color: 0

Bin 701: 2128 of cap free
Amount of items: 2
Items: 
Size: 790131 Color: 0
Size: 207742 Color: 4

Bin 702: 2157 of cap free
Amount of items: 2
Items: 
Size: 604070 Color: 3
Size: 393774 Color: 4

Bin 703: 2170 of cap free
Amount of items: 2
Items: 
Size: 796996 Color: 4
Size: 200835 Color: 0

Bin 704: 2208 of cap free
Amount of items: 2
Items: 
Size: 556626 Color: 3
Size: 441167 Color: 2

Bin 705: 2215 of cap free
Amount of items: 3
Items: 
Size: 433294 Color: 0
Size: 292973 Color: 0
Size: 271519 Color: 2

Bin 706: 2226 of cap free
Amount of items: 2
Items: 
Size: 528685 Color: 1
Size: 469090 Color: 3

Bin 707: 2278 of cap free
Amount of items: 2
Items: 
Size: 722017 Color: 0
Size: 275706 Color: 2

Bin 708: 2288 of cap free
Amount of items: 3
Items: 
Size: 551855 Color: 1
Size: 225233 Color: 0
Size: 220625 Color: 2

Bin 709: 2316 of cap free
Amount of items: 2
Items: 
Size: 747343 Color: 0
Size: 250342 Color: 4

Bin 710: 2323 of cap free
Amount of items: 2
Items: 
Size: 772735 Color: 0
Size: 224943 Color: 2

Bin 711: 2344 of cap free
Amount of items: 2
Items: 
Size: 653187 Color: 2
Size: 344470 Color: 0

Bin 712: 2349 of cap free
Amount of items: 3
Items: 
Size: 342973 Color: 0
Size: 327633 Color: 4
Size: 327046 Color: 1

Bin 713: 2386 of cap free
Amount of items: 2
Items: 
Size: 543014 Color: 2
Size: 454601 Color: 0

Bin 714: 2403 of cap free
Amount of items: 3
Items: 
Size: 411750 Color: 2
Size: 300044 Color: 4
Size: 285804 Color: 1

Bin 715: 2419 of cap free
Amount of items: 2
Items: 
Size: 580912 Color: 4
Size: 416670 Color: 0

Bin 716: 2488 of cap free
Amount of items: 2
Items: 
Size: 633887 Color: 4
Size: 363626 Color: 0

Bin 717: 2520 of cap free
Amount of items: 2
Items: 
Size: 618909 Color: 1
Size: 378572 Color: 4

Bin 718: 2547 of cap free
Amount of items: 2
Items: 
Size: 534215 Color: 1
Size: 463239 Color: 0

Bin 719: 2558 of cap free
Amount of items: 2
Items: 
Size: 661710 Color: 0
Size: 335733 Color: 3

Bin 720: 2569 of cap free
Amount of items: 3
Items: 
Size: 486650 Color: 2
Size: 257699 Color: 0
Size: 253083 Color: 3

Bin 721: 2623 of cap free
Amount of items: 2
Items: 
Size: 649010 Color: 1
Size: 348368 Color: 2

Bin 722: 2630 of cap free
Amount of items: 2
Items: 
Size: 584366 Color: 1
Size: 413005 Color: 3

Bin 723: 2658 of cap free
Amount of items: 2
Items: 
Size: 747072 Color: 4
Size: 250271 Color: 2

Bin 724: 2661 of cap free
Amount of items: 3
Items: 
Size: 404476 Color: 0
Size: 299935 Color: 4
Size: 292929 Color: 0

Bin 725: 2673 of cap free
Amount of items: 2
Items: 
Size: 519574 Color: 2
Size: 477754 Color: 0

Bin 726: 2676 of cap free
Amount of items: 2
Items: 
Size: 507510 Color: 1
Size: 489815 Color: 2

Bin 727: 2681 of cap free
Amount of items: 2
Items: 
Size: 720141 Color: 3
Size: 277179 Color: 0

Bin 728: 2744 of cap free
Amount of items: 2
Items: 
Size: 599558 Color: 0
Size: 397699 Color: 4

Bin 729: 2781 of cap free
Amount of items: 2
Items: 
Size: 533696 Color: 2
Size: 463524 Color: 0

Bin 730: 2782 of cap free
Amount of items: 2
Items: 
Size: 649438 Color: 0
Size: 347781 Color: 1

Bin 731: 2786 of cap free
Amount of items: 2
Items: 
Size: 562318 Color: 3
Size: 434897 Color: 1

Bin 732: 2877 of cap free
Amount of items: 2
Items: 
Size: 775295 Color: 1
Size: 221829 Color: 4

Bin 733: 2886 of cap free
Amount of items: 2
Items: 
Size: 606471 Color: 4
Size: 390644 Color: 3

Bin 734: 2890 of cap free
Amount of items: 2
Items: 
Size: 695038 Color: 1
Size: 302073 Color: 2

Bin 735: 2917 of cap free
Amount of items: 2
Items: 
Size: 583024 Color: 4
Size: 414060 Color: 2

Bin 736: 2922 of cap free
Amount of items: 2
Items: 
Size: 758459 Color: 4
Size: 238620 Color: 2

Bin 737: 2961 of cap free
Amount of items: 2
Items: 
Size: 652891 Color: 2
Size: 344149 Color: 3

Bin 738: 2976 of cap free
Amount of items: 2
Items: 
Size: 799691 Color: 0
Size: 197334 Color: 2

Bin 739: 3008 of cap free
Amount of items: 2
Items: 
Size: 579940 Color: 1
Size: 417053 Color: 2

Bin 740: 3018 of cap free
Amount of items: 2
Items: 
Size: 557533 Color: 4
Size: 439450 Color: 1

Bin 741: 3047 of cap free
Amount of items: 2
Items: 
Size: 778680 Color: 0
Size: 218274 Color: 1

Bin 742: 3052 of cap free
Amount of items: 2
Items: 
Size: 555251 Color: 2
Size: 441698 Color: 4

Bin 743: 3081 of cap free
Amount of items: 2
Items: 
Size: 642610 Color: 0
Size: 354310 Color: 4

Bin 744: 3120 of cap free
Amount of items: 3
Items: 
Size: 370445 Color: 0
Size: 316182 Color: 3
Size: 310254 Color: 1

Bin 745: 3132 of cap free
Amount of items: 3
Items: 
Size: 489662 Color: 0
Size: 258073 Color: 1
Size: 249134 Color: 2

Bin 746: 3132 of cap free
Amount of items: 3
Items: 
Size: 592365 Color: 0
Size: 214140 Color: 2
Size: 190364 Color: 3

Bin 747: 3142 of cap free
Amount of items: 2
Items: 
Size: 561480 Color: 3
Size: 435379 Color: 2

Bin 748: 3142 of cap free
Amount of items: 2
Items: 
Size: 790121 Color: 2
Size: 206738 Color: 3

Bin 749: 3148 of cap free
Amount of items: 2
Items: 
Size: 510887 Color: 2
Size: 485966 Color: 1

Bin 750: 3150 of cap free
Amount of items: 2
Items: 
Size: 703212 Color: 4
Size: 293639 Color: 0

Bin 751: 3170 of cap free
Amount of items: 2
Items: 
Size: 570238 Color: 2
Size: 426593 Color: 1

Bin 752: 3254 of cap free
Amount of items: 2
Items: 
Size: 654368 Color: 4
Size: 342379 Color: 2

Bin 753: 3281 of cap free
Amount of items: 3
Items: 
Size: 373127 Color: 3
Size: 316688 Color: 0
Size: 306905 Color: 4

Bin 754: 3287 of cap free
Amount of items: 2
Items: 
Size: 543072 Color: 1
Size: 453642 Color: 4

Bin 755: 3287 of cap free
Amount of items: 2
Items: 
Size: 795752 Color: 0
Size: 200962 Color: 2

Bin 756: 3293 of cap free
Amount of items: 2
Items: 
Size: 500764 Color: 2
Size: 495944 Color: 4

Bin 757: 3367 of cap free
Amount of items: 3
Items: 
Size: 458633 Color: 1
Size: 281878 Color: 0
Size: 256123 Color: 3

Bin 758: 3381 of cap free
Amount of items: 2
Items: 
Size: 574332 Color: 2
Size: 422288 Color: 3

Bin 759: 3387 of cap free
Amount of items: 3
Items: 
Size: 352293 Color: 0
Size: 322179 Color: 3
Size: 322142 Color: 3

Bin 760: 3390 of cap free
Amount of items: 2
Items: 
Size: 708275 Color: 3
Size: 288336 Color: 0

Bin 761: 3470 of cap free
Amount of items: 2
Items: 
Size: 538513 Color: 1
Size: 458018 Color: 2

Bin 762: 3503 of cap free
Amount of items: 2
Items: 
Size: 523779 Color: 4
Size: 472719 Color: 3

Bin 763: 3506 of cap free
Amount of items: 2
Items: 
Size: 778256 Color: 1
Size: 218239 Color: 4

Bin 764: 3583 of cap free
Amount of items: 2
Items: 
Size: 580045 Color: 3
Size: 416373 Color: 0

Bin 765: 3610 of cap free
Amount of items: 2
Items: 
Size: 613519 Color: 3
Size: 382872 Color: 4

Bin 766: 3617 of cap free
Amount of items: 3
Items: 
Size: 437510 Color: 3
Size: 286472 Color: 4
Size: 272402 Color: 2

Bin 767: 3689 of cap free
Amount of items: 2
Items: 
Size: 598638 Color: 3
Size: 397674 Color: 2

Bin 768: 3737 of cap free
Amount of items: 2
Items: 
Size: 733790 Color: 4
Size: 262474 Color: 1

Bin 769: 3802 of cap free
Amount of items: 2
Items: 
Size: 680701 Color: 4
Size: 315498 Color: 2

Bin 770: 3803 of cap free
Amount of items: 2
Items: 
Size: 591269 Color: 4
Size: 404929 Color: 2

Bin 771: 3838 of cap free
Amount of items: 2
Items: 
Size: 649029 Color: 4
Size: 347134 Color: 3

Bin 772: 3867 of cap free
Amount of items: 2
Items: 
Size: 506618 Color: 1
Size: 489516 Color: 3

Bin 773: 3872 of cap free
Amount of items: 2
Items: 
Size: 707623 Color: 0
Size: 288506 Color: 3

Bin 774: 3949 of cap free
Amount of items: 2
Items: 
Size: 503097 Color: 0
Size: 492955 Color: 1

Bin 775: 4015 of cap free
Amount of items: 2
Items: 
Size: 759457 Color: 0
Size: 236529 Color: 1

Bin 776: 4034 of cap free
Amount of items: 2
Items: 
Size: 766541 Color: 4
Size: 229426 Color: 3

Bin 777: 4049 of cap free
Amount of items: 2
Items: 
Size: 632613 Color: 3
Size: 363339 Color: 1

Bin 778: 4156 of cap free
Amount of items: 2
Items: 
Size: 520791 Color: 0
Size: 475054 Color: 4

Bin 779: 4204 of cap free
Amount of items: 2
Items: 
Size: 509940 Color: 0
Size: 485857 Color: 1

Bin 780: 4274 of cap free
Amount of items: 2
Items: 
Size: 541990 Color: 2
Size: 453737 Color: 0

Bin 781: 4289 of cap free
Amount of items: 2
Items: 
Size: 561999 Color: 3
Size: 433713 Color: 1

Bin 782: 4426 of cap free
Amount of items: 3
Items: 
Size: 457232 Color: 1
Size: 276450 Color: 4
Size: 261893 Color: 2

Bin 783: 4614 of cap free
Amount of items: 2
Items: 
Size: 708031 Color: 3
Size: 287356 Color: 2

Bin 784: 4626 of cap free
Amount of items: 2
Items: 
Size: 687491 Color: 3
Size: 307884 Color: 4

Bin 785: 4774 of cap free
Amount of items: 2
Items: 
Size: 632239 Color: 1
Size: 362988 Color: 4

Bin 786: 4882 of cap free
Amount of items: 3
Items: 
Size: 480518 Color: 3
Size: 261714 Color: 0
Size: 252887 Color: 0

Bin 787: 4897 of cap free
Amount of items: 2
Items: 
Size: 574027 Color: 3
Size: 421077 Color: 0

Bin 788: 4953 of cap free
Amount of items: 2
Items: 
Size: 566882 Color: 4
Size: 428166 Color: 2

Bin 789: 4960 of cap free
Amount of items: 2
Items: 
Size: 755533 Color: 1
Size: 239508 Color: 0

Bin 790: 5069 of cap free
Amount of items: 2
Items: 
Size: 573889 Color: 4
Size: 421043 Color: 1

Bin 791: 5100 of cap free
Amount of items: 2
Items: 
Size: 560469 Color: 3
Size: 434432 Color: 2

Bin 792: 5307 of cap free
Amount of items: 2
Items: 
Size: 797133 Color: 2
Size: 197561 Color: 4

Bin 793: 5347 of cap free
Amount of items: 2
Items: 
Size: 583274 Color: 0
Size: 411380 Color: 1

Bin 794: 5366 of cap free
Amount of items: 2
Items: 
Size: 693225 Color: 3
Size: 301410 Color: 1

Bin 795: 5427 of cap free
Amount of items: 2
Items: 
Size: 537543 Color: 4
Size: 457031 Color: 2

Bin 796: 5456 of cap free
Amount of items: 2
Items: 
Size: 765335 Color: 1
Size: 229210 Color: 0

Bin 797: 5459 of cap free
Amount of items: 2
Items: 
Size: 797073 Color: 2
Size: 197469 Color: 1

Bin 798: 5576 of cap free
Amount of items: 2
Items: 
Size: 530353 Color: 2
Size: 464072 Color: 1

Bin 799: 5767 of cap free
Amount of items: 2
Items: 
Size: 498142 Color: 0
Size: 496092 Color: 2

Bin 800: 5796 of cap free
Amount of items: 2
Items: 
Size: 542473 Color: 1
Size: 451732 Color: 4

Bin 801: 5799 of cap free
Amount of items: 2
Items: 
Size: 604363 Color: 4
Size: 389839 Color: 3

Bin 802: 5829 of cap free
Amount of items: 2
Items: 
Size: 566613 Color: 1
Size: 427559 Color: 2

Bin 803: 5911 of cap free
Amount of items: 2
Items: 
Size: 604043 Color: 3
Size: 390047 Color: 2

Bin 804: 5981 of cap free
Amount of items: 2
Items: 
Size: 555011 Color: 2
Size: 439009 Color: 0

Bin 805: 6578 of cap free
Amount of items: 2
Items: 
Size: 691848 Color: 1
Size: 301575 Color: 2

Bin 806: 6933 of cap free
Amount of items: 2
Items: 
Size: 579430 Color: 4
Size: 413638 Color: 2

Bin 807: 7221 of cap free
Amount of items: 2
Items: 
Size: 730989 Color: 4
Size: 261791 Color: 1

Bin 808: 7298 of cap free
Amount of items: 2
Items: 
Size: 755763 Color: 1
Size: 236940 Color: 2

Bin 809: 7532 of cap free
Amount of items: 2
Items: 
Size: 795020 Color: 3
Size: 197449 Color: 0

Bin 810: 7542 of cap free
Amount of items: 2
Items: 
Size: 648933 Color: 4
Size: 343526 Color: 0

Bin 811: 7554 of cap free
Amount of items: 3
Items: 
Size: 332064 Color: 3
Size: 330772 Color: 1
Size: 329611 Color: 4

Bin 812: 7607 of cap free
Amount of items: 2
Items: 
Size: 500080 Color: 0
Size: 492314 Color: 1

Bin 813: 7680 of cap free
Amount of items: 2
Items: 
Size: 755614 Color: 1
Size: 236707 Color: 2

Bin 814: 7864 of cap free
Amount of items: 2
Items: 
Size: 705752 Color: 3
Size: 286385 Color: 2

Bin 815: 7895 of cap free
Amount of items: 2
Items: 
Size: 676912 Color: 4
Size: 315194 Color: 2

Bin 816: 8101 of cap free
Amount of items: 2
Items: 
Size: 536997 Color: 4
Size: 454903 Color: 2

Bin 817: 8249 of cap free
Amount of items: 2
Items: 
Size: 755052 Color: 1
Size: 236700 Color: 2

Bin 818: 8675 of cap free
Amount of items: 2
Items: 
Size: 496452 Color: 0
Size: 494874 Color: 4

Bin 819: 8968 of cap free
Amount of items: 2
Items: 
Size: 537531 Color: 1
Size: 453502 Color: 2

Bin 820: 9235 of cap free
Amount of items: 2
Items: 
Size: 630958 Color: 1
Size: 359808 Color: 2

Bin 821: 9283 of cap free
Amount of items: 2
Items: 
Size: 753931 Color: 4
Size: 236787 Color: 0

Bin 822: 9763 of cap free
Amount of items: 2
Items: 
Size: 702803 Color: 0
Size: 287435 Color: 3

Bin 823: 9839 of cap free
Amount of items: 2
Items: 
Size: 602425 Color: 3
Size: 387737 Color: 2

Bin 824: 9861 of cap free
Amount of items: 2
Items: 
Size: 691115 Color: 3
Size: 299025 Color: 2

Bin 825: 10439 of cap free
Amount of items: 2
Items: 
Size: 525948 Color: 3
Size: 463614 Color: 1

Bin 826: 10509 of cap free
Amount of items: 2
Items: 
Size: 753923 Color: 1
Size: 235569 Color: 2

Bin 827: 10723 of cap free
Amount of items: 2
Items: 
Size: 642839 Color: 4
Size: 346439 Color: 2

Bin 828: 12624 of cap free
Amount of items: 2
Items: 
Size: 573988 Color: 1
Size: 413389 Color: 2

Bin 829: 12812 of cap free
Amount of items: 2
Items: 
Size: 573829 Color: 3
Size: 413360 Color: 2

Bin 830: 13337 of cap free
Amount of items: 2
Items: 
Size: 533166 Color: 3
Size: 453498 Color: 0

Bin 831: 13658 of cap free
Amount of items: 2
Items: 
Size: 579813 Color: 1
Size: 406530 Color: 3

Bin 832: 14723 of cap free
Amount of items: 2
Items: 
Size: 703755 Color: 3
Size: 281523 Color: 2

Bin 833: 15057 of cap free
Amount of items: 2
Items: 
Size: 533596 Color: 1
Size: 451348 Color: 4

Bin 834: 15768 of cap free
Amount of items: 2
Items: 
Size: 494727 Color: 2
Size: 489506 Color: 3

Bin 835: 16375 of cap free
Amount of items: 2
Items: 
Size: 702518 Color: 4
Size: 281108 Color: 2

Bin 836: 16818 of cap free
Amount of items: 2
Items: 
Size: 685680 Color: 3
Size: 297503 Color: 2

Bin 837: 19422 of cap free
Amount of items: 2
Items: 
Size: 499198 Color: 2
Size: 481381 Color: 1

Bin 838: 21150 of cap free
Amount of items: 2
Items: 
Size: 498050 Color: 0
Size: 480801 Color: 1

Bin 839: 23497 of cap free
Amount of items: 2
Items: 
Size: 523763 Color: 4
Size: 452741 Color: 2

Bin 840: 26347 of cap free
Amount of items: 2
Items: 
Size: 494695 Color: 0
Size: 478959 Color: 1

Bin 841: 26810 of cap free
Amount of items: 2
Items: 
Size: 520455 Color: 1
Size: 452736 Color: 0

Bin 842: 34348 of cap free
Amount of items: 2
Items: 
Size: 487139 Color: 2
Size: 478514 Color: 1

Bin 843: 42275 of cap free
Amount of items: 2
Items: 
Size: 483008 Color: 0
Size: 474718 Color: 1

Bin 844: 55695 of cap free
Amount of items: 2
Items: 
Size: 495689 Color: 2
Size: 448617 Color: 4

Bin 845: 56132 of cap free
Amount of items: 2
Items: 
Size: 480321 Color: 2
Size: 463548 Color: 1

Bin 846: 73678 of cap free
Amount of items: 2
Items: 
Size: 494724 Color: 2
Size: 431599 Color: 4

Bin 847: 100056 of cap free
Amount of items: 2
Items: 
Size: 452486 Color: 0
Size: 447459 Color: 3

Bin 848: 119145 of cap free
Amount of items: 2
Items: 
Size: 449463 Color: 1
Size: 431393 Color: 4

Bin 849: 129858 of cap free
Amount of items: 2
Items: 
Size: 449202 Color: 2
Size: 420941 Color: 3

Bin 850: 147102 of cap free
Amount of items: 2
Items: 
Size: 447827 Color: 1
Size: 405072 Color: 3

Bin 851: 150638 of cap free
Amount of items: 2
Items: 
Size: 447349 Color: 2
Size: 402014 Color: 3

Bin 852: 151820 of cap free
Amount of items: 2
Items: 
Size: 445558 Color: 1
Size: 402623 Color: 4

Bin 853: 152479 of cap free
Amount of items: 2
Items: 
Size: 445386 Color: 2
Size: 402136 Color: 4

Bin 854: 153290 of cap free
Amount of items: 2
Items: 
Size: 445318 Color: 0
Size: 401393 Color: 3

Bin 855: 154718 of cap free
Amount of items: 2
Items: 
Size: 443525 Color: 1
Size: 401758 Color: 4

Bin 856: 159521 of cap free
Amount of items: 2
Items: 
Size: 444179 Color: 2
Size: 396301 Color: 3

Bin 857: 160225 of cap free
Amount of items: 2
Items: 
Size: 438500 Color: 1
Size: 401276 Color: 4

Bin 858: 171365 of cap free
Amount of items: 2
Items: 
Size: 438945 Color: 1
Size: 389691 Color: 3

Bin 859: 174011 of cap free
Amount of items: 2
Items: 
Size: 433139 Color: 1
Size: 392851 Color: 4

Bin 860: 182439 of cap free
Amount of items: 2
Items: 
Size: 432848 Color: 0
Size: 384714 Color: 3

Bin 861: 184006 of cap free
Amount of items: 2
Items: 
Size: 432993 Color: 1
Size: 383002 Color: 3

Bin 862: 190611 of cap free
Amount of items: 2
Items: 
Size: 426554 Color: 2
Size: 382836 Color: 4

Bin 863: 195946 of cap free
Amount of items: 2
Items: 
Size: 426629 Color: 2
Size: 377426 Color: 3

Bin 864: 210939 of cap free
Amount of items: 2
Items: 
Size: 411660 Color: 2
Size: 377402 Color: 3

Bin 865: 215651 of cap free
Amount of items: 2
Items: 
Size: 410202 Color: 1
Size: 374148 Color: 4

Bin 866: 216905 of cap free
Amount of items: 2
Items: 
Size: 406239 Color: 1
Size: 376857 Color: 3

Bin 867: 221687 of cap free
Amount of items: 2
Items: 
Size: 389301 Color: 1
Size: 389013 Color: 0

Bin 868: 229360 of cap free
Amount of items: 2
Items: 
Size: 401764 Color: 1
Size: 368877 Color: 3

Bin 869: 256149 of cap free
Amount of items: 2
Items: 
Size: 373482 Color: 1
Size: 370370 Color: 0

Bin 870: 256692 of cap free
Amount of items: 2
Items: 
Size: 383426 Color: 2
Size: 359883 Color: 3

Bin 871: 256999 of cap free
Amount of items: 2
Items: 
Size: 373207 Color: 1
Size: 369795 Color: 0

Bin 872: 257580 of cap free
Amount of items: 2
Items: 
Size: 389666 Color: 1
Size: 352755 Color: 4

Bin 873: 261945 of cap free
Amount of items: 2
Items: 
Size: 386645 Color: 1
Size: 351411 Color: 4

Bin 874: 262061 of cap free
Amount of items: 2
Items: 
Size: 378221 Color: 0
Size: 359719 Color: 3

Bin 875: 269479 of cap free
Amount of items: 2
Items: 
Size: 386381 Color: 2
Size: 344141 Color: 4

Bin 876: 269741 of cap free
Amount of items: 2
Items: 
Size: 375716 Color: 2
Size: 354544 Color: 0

Bin 877: 275687 of cap free
Amount of items: 2
Items: 
Size: 381332 Color: 2
Size: 342982 Color: 4

Bin 878: 277696 of cap free
Amount of items: 2
Items: 
Size: 380602 Color: 2
Size: 341703 Color: 4

Bin 879: 283060 of cap free
Amount of items: 2
Items: 
Size: 378616 Color: 2
Size: 338325 Color: 4

Bin 880: 287560 of cap free
Amount of items: 2
Items: 
Size: 378415 Color: 2
Size: 334026 Color: 4

Bin 881: 289212 of cap free
Amount of items: 2
Items: 
Size: 376799 Color: 2
Size: 333990 Color: 4

Bin 882: 292716 of cap free
Amount of items: 2
Items: 
Size: 368943 Color: 1
Size: 338342 Color: 3

Bin 883: 297790 of cap free
Amount of items: 2
Items: 
Size: 367620 Color: 1
Size: 334591 Color: 3

Bin 884: 313995 of cap free
Amount of items: 2
Items: 
Size: 351266 Color: 0
Size: 334740 Color: 1

Total size: 874417442
Total free space: 9583442

