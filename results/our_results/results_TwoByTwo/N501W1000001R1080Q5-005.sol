Capicity Bin: 1000001
Lower Bound: 220

Bins used: 223
Amount of Colors: 5

Bin 1: 7 of cap free
Amount of items: 3
Items: 
Size: 680038 Color: 1
Size: 174122 Color: 2
Size: 145834 Color: 2

Bin 2: 46 of cap free
Amount of items: 3
Items: 
Size: 736209 Color: 4
Size: 134931 Color: 1
Size: 128815 Color: 0

Bin 3: 47 of cap free
Amount of items: 2
Items: 
Size: 574876 Color: 4
Size: 425078 Color: 2

Bin 4: 48 of cap free
Amount of items: 2
Items: 
Size: 797549 Color: 1
Size: 202404 Color: 3

Bin 5: 82 of cap free
Amount of items: 3
Items: 
Size: 493345 Color: 2
Size: 253370 Color: 3
Size: 253204 Color: 0

Bin 6: 95 of cap free
Amount of items: 3
Items: 
Size: 604307 Color: 1
Size: 202762 Color: 3
Size: 192837 Color: 4

Bin 7: 98 of cap free
Amount of items: 2
Items: 
Size: 651896 Color: 4
Size: 348007 Color: 2

Bin 8: 103 of cap free
Amount of items: 3
Items: 
Size: 762999 Color: 1
Size: 126088 Color: 0
Size: 110811 Color: 0

Bin 9: 115 of cap free
Amount of items: 2
Items: 
Size: 538795 Color: 4
Size: 461091 Color: 1

Bin 10: 116 of cap free
Amount of items: 2
Items: 
Size: 729031 Color: 0
Size: 270854 Color: 3

Bin 11: 117 of cap free
Amount of items: 3
Items: 
Size: 755119 Color: 1
Size: 130301 Color: 2
Size: 114464 Color: 4

Bin 12: 125 of cap free
Amount of items: 2
Items: 
Size: 584266 Color: 2
Size: 415610 Color: 3

Bin 13: 132 of cap free
Amount of items: 2
Items: 
Size: 731395 Color: 1
Size: 268474 Color: 3

Bin 14: 155 of cap free
Amount of items: 3
Items: 
Size: 702591 Color: 4
Size: 153753 Color: 1
Size: 143502 Color: 3

Bin 15: 163 of cap free
Amount of items: 3
Items: 
Size: 706968 Color: 2
Size: 148213 Color: 0
Size: 144657 Color: 1

Bin 16: 193 of cap free
Amount of items: 3
Items: 
Size: 752643 Color: 1
Size: 128038 Color: 0
Size: 119127 Color: 0

Bin 17: 194 of cap free
Amount of items: 3
Items: 
Size: 662107 Color: 3
Size: 180293 Color: 1
Size: 157407 Color: 4

Bin 18: 197 of cap free
Amount of items: 2
Items: 
Size: 797465 Color: 1
Size: 202339 Color: 3

Bin 19: 204 of cap free
Amount of items: 3
Items: 
Size: 662716 Color: 1
Size: 173553 Color: 4
Size: 163528 Color: 4

Bin 20: 207 of cap free
Amount of items: 3
Items: 
Size: 765060 Color: 1
Size: 125649 Color: 3
Size: 109085 Color: 4

Bin 21: 210 of cap free
Amount of items: 3
Items: 
Size: 767321 Color: 1
Size: 124122 Color: 2
Size: 108348 Color: 2

Bin 22: 230 of cap free
Amount of items: 2
Items: 
Size: 778597 Color: 3
Size: 221174 Color: 2

Bin 23: 231 of cap free
Amount of items: 3
Items: 
Size: 715794 Color: 3
Size: 142851 Color: 1
Size: 141125 Color: 4

Bin 24: 232 of cap free
Amount of items: 3
Items: 
Size: 642294 Color: 1
Size: 190748 Color: 3
Size: 166727 Color: 2

Bin 25: 234 of cap free
Amount of items: 2
Items: 
Size: 690684 Color: 4
Size: 309083 Color: 1

Bin 26: 236 of cap free
Amount of items: 2
Items: 
Size: 780022 Color: 2
Size: 219743 Color: 3

Bin 27: 248 of cap free
Amount of items: 3
Items: 
Size: 794145 Color: 1
Size: 102819 Color: 0
Size: 102789 Color: 4

Bin 28: 252 of cap free
Amount of items: 2
Items: 
Size: 574019 Color: 3
Size: 425730 Color: 1

Bin 29: 253 of cap free
Amount of items: 2
Items: 
Size: 719723 Color: 2
Size: 280025 Color: 1

Bin 30: 309 of cap free
Amount of items: 3
Items: 
Size: 617114 Color: 1
Size: 226231 Color: 4
Size: 156347 Color: 3

Bin 31: 314 of cap free
Amount of items: 2
Items: 
Size: 759838 Color: 3
Size: 239849 Color: 4

Bin 32: 339 of cap free
Amount of items: 2
Items: 
Size: 645307 Color: 2
Size: 354355 Color: 4

Bin 33: 353 of cap free
Amount of items: 2
Items: 
Size: 791611 Color: 3
Size: 208037 Color: 2

Bin 34: 355 of cap free
Amount of items: 2
Items: 
Size: 661164 Color: 0
Size: 338482 Color: 3

Bin 35: 364 of cap free
Amount of items: 2
Items: 
Size: 794827 Color: 4
Size: 204810 Color: 2

Bin 36: 365 of cap free
Amount of items: 2
Items: 
Size: 771554 Color: 3
Size: 228082 Color: 4

Bin 37: 379 of cap free
Amount of items: 2
Items: 
Size: 608179 Color: 2
Size: 391443 Color: 1

Bin 38: 390 of cap free
Amount of items: 3
Items: 
Size: 791829 Color: 0
Size: 105674 Color: 0
Size: 102108 Color: 3

Bin 39: 398 of cap free
Amount of items: 2
Items: 
Size: 560011 Color: 2
Size: 439592 Color: 4

Bin 40: 399 of cap free
Amount of items: 3
Items: 
Size: 573338 Color: 1
Size: 224688 Color: 4
Size: 201576 Color: 0

Bin 41: 399 of cap free
Amount of items: 2
Items: 
Size: 721380 Color: 0
Size: 278222 Color: 2

Bin 42: 402 of cap free
Amount of items: 2
Items: 
Size: 519306 Color: 4
Size: 480293 Color: 1

Bin 43: 406 of cap free
Amount of items: 2
Items: 
Size: 612384 Color: 3
Size: 387211 Color: 4

Bin 44: 422 of cap free
Amount of items: 3
Items: 
Size: 594479 Color: 1
Size: 230212 Color: 0
Size: 174888 Color: 4

Bin 45: 445 of cap free
Amount of items: 2
Items: 
Size: 582593 Color: 0
Size: 416963 Color: 1

Bin 46: 452 of cap free
Amount of items: 2
Items: 
Size: 572275 Color: 3
Size: 427274 Color: 0

Bin 47: 457 of cap free
Amount of items: 3
Items: 
Size: 706282 Color: 0
Size: 148226 Color: 2
Size: 145036 Color: 1

Bin 48: 460 of cap free
Amount of items: 2
Items: 
Size: 761631 Color: 3
Size: 237910 Color: 2

Bin 49: 493 of cap free
Amount of items: 2
Items: 
Size: 536215 Color: 0
Size: 463293 Color: 1

Bin 50: 511 of cap free
Amount of items: 2
Items: 
Size: 551620 Color: 2
Size: 447870 Color: 4

Bin 51: 515 of cap free
Amount of items: 2
Items: 
Size: 778121 Color: 1
Size: 221365 Color: 0

Bin 52: 531 of cap free
Amount of items: 3
Items: 
Size: 658585 Color: 1
Size: 171136 Color: 0
Size: 169749 Color: 4

Bin 53: 550 of cap free
Amount of items: 3
Items: 
Size: 713617 Color: 3
Size: 143618 Color: 1
Size: 142216 Color: 2

Bin 54: 563 of cap free
Amount of items: 2
Items: 
Size: 721014 Color: 1
Size: 278424 Color: 3

Bin 55: 568 of cap free
Amount of items: 3
Items: 
Size: 699727 Color: 1
Size: 154941 Color: 2
Size: 144765 Color: 0

Bin 56: 613 of cap free
Amount of items: 3
Items: 
Size: 685168 Color: 1
Size: 161777 Color: 0
Size: 152443 Color: 3

Bin 57: 625 of cap free
Amount of items: 3
Items: 
Size: 701077 Color: 1
Size: 150758 Color: 3
Size: 147541 Color: 4

Bin 58: 648 of cap free
Amount of items: 3
Items: 
Size: 727459 Color: 0
Size: 136276 Color: 1
Size: 135618 Color: 2

Bin 59: 649 of cap free
Amount of items: 2
Items: 
Size: 679409 Color: 0
Size: 319943 Color: 3

Bin 60: 678 of cap free
Amount of items: 2
Items: 
Size: 667007 Color: 1
Size: 332316 Color: 0

Bin 61: 691 of cap free
Amount of items: 2
Items: 
Size: 590542 Color: 4
Size: 408768 Color: 0

Bin 62: 712 of cap free
Amount of items: 2
Items: 
Size: 585298 Color: 2
Size: 413991 Color: 0

Bin 63: 722 of cap free
Amount of items: 3
Items: 
Size: 585744 Color: 1
Size: 235075 Color: 0
Size: 178460 Color: 2

Bin 64: 729 of cap free
Amount of items: 2
Items: 
Size: 798811 Color: 2
Size: 200461 Color: 4

Bin 65: 739 of cap free
Amount of items: 2
Items: 
Size: 718132 Color: 3
Size: 281130 Color: 4

Bin 66: 750 of cap free
Amount of items: 2
Items: 
Size: 720481 Color: 3
Size: 278770 Color: 1

Bin 67: 757 of cap free
Amount of items: 2
Items: 
Size: 694840 Color: 4
Size: 304404 Color: 3

Bin 68: 785 of cap free
Amount of items: 3
Items: 
Size: 541201 Color: 4
Size: 233552 Color: 0
Size: 224463 Color: 3

Bin 69: 787 of cap free
Amount of items: 3
Items: 
Size: 338744 Color: 0
Size: 336297 Color: 1
Size: 324173 Color: 3

Bin 70: 802 of cap free
Amount of items: 3
Items: 
Size: 561222 Color: 1
Size: 236409 Color: 3
Size: 201568 Color: 2

Bin 71: 827 of cap free
Amount of items: 3
Items: 
Size: 636979 Color: 1
Size: 186284 Color: 3
Size: 175911 Color: 2

Bin 72: 844 of cap free
Amount of items: 2
Items: 
Size: 534725 Color: 1
Size: 464432 Color: 3

Bin 73: 855 of cap free
Amount of items: 2
Items: 
Size: 759196 Color: 4
Size: 239950 Color: 1

Bin 74: 860 of cap free
Amount of items: 2
Items: 
Size: 733580 Color: 4
Size: 265561 Color: 0

Bin 75: 893 of cap free
Amount of items: 2
Items: 
Size: 755944 Color: 0
Size: 243164 Color: 4

Bin 76: 959 of cap free
Amount of items: 2
Items: 
Size: 731391 Color: 1
Size: 267651 Color: 3

Bin 77: 1004 of cap free
Amount of items: 3
Items: 
Size: 656201 Color: 1
Size: 186327 Color: 4
Size: 156469 Color: 0

Bin 78: 1010 of cap free
Amount of items: 2
Items: 
Size: 758358 Color: 0
Size: 240633 Color: 4

Bin 79: 1030 of cap free
Amount of items: 2
Items: 
Size: 575751 Color: 0
Size: 423220 Color: 4

Bin 80: 1058 of cap free
Amount of items: 3
Items: 
Size: 593193 Color: 1
Size: 223214 Color: 0
Size: 182536 Color: 2

Bin 81: 1059 of cap free
Amount of items: 2
Items: 
Size: 766456 Color: 0
Size: 232486 Color: 2

Bin 82: 1094 of cap free
Amount of items: 2
Items: 
Size: 777467 Color: 2
Size: 221440 Color: 1

Bin 83: 1119 of cap free
Amount of items: 2
Items: 
Size: 526754 Color: 0
Size: 472128 Color: 3

Bin 84: 1176 of cap free
Amount of items: 2
Items: 
Size: 600708 Color: 3
Size: 398117 Color: 2

Bin 85: 1194 of cap free
Amount of items: 2
Items: 
Size: 627986 Color: 1
Size: 370821 Color: 0

Bin 86: 1196 of cap free
Amount of items: 2
Items: 
Size: 530194 Color: 0
Size: 468611 Color: 3

Bin 87: 1239 of cap free
Amount of items: 2
Items: 
Size: 598684 Color: 2
Size: 400078 Color: 1

Bin 88: 1247 of cap free
Amount of items: 3
Items: 
Size: 377914 Color: 0
Size: 349929 Color: 0
Size: 270911 Color: 1

Bin 89: 1274 of cap free
Amount of items: 2
Items: 
Size: 713094 Color: 3
Size: 285633 Color: 0

Bin 90: 1284 of cap free
Amount of items: 2
Items: 
Size: 568819 Color: 2
Size: 429898 Color: 0

Bin 91: 1306 of cap free
Amount of items: 3
Items: 
Size: 530602 Color: 1
Size: 245905 Color: 0
Size: 222188 Color: 4

Bin 92: 1306 of cap free
Amount of items: 3
Items: 
Size: 693680 Color: 4
Size: 164394 Color: 1
Size: 140621 Color: 0

Bin 93: 1330 of cap free
Amount of items: 2
Items: 
Size: 596692 Color: 0
Size: 401979 Color: 3

Bin 94: 1413 of cap free
Amount of items: 3
Items: 
Size: 589550 Color: 3
Size: 233157 Color: 1
Size: 175881 Color: 4

Bin 95: 1419 of cap free
Amount of items: 2
Items: 
Size: 575261 Color: 3
Size: 423321 Color: 0

Bin 96: 1495 of cap free
Amount of items: 3
Items: 
Size: 741940 Color: 1
Size: 137574 Color: 0
Size: 118992 Color: 2

Bin 97: 1497 of cap free
Amount of items: 2
Items: 
Size: 574171 Color: 4
Size: 424333 Color: 3

Bin 98: 1542 of cap free
Amount of items: 3
Items: 
Size: 767760 Color: 3
Size: 125128 Color: 1
Size: 105571 Color: 4

Bin 99: 1581 of cap free
Amount of items: 2
Items: 
Size: 734794 Color: 2
Size: 263626 Color: 4

Bin 100: 1615 of cap free
Amount of items: 2
Items: 
Size: 593352 Color: 1
Size: 405034 Color: 4

Bin 101: 1642 of cap free
Amount of items: 2
Items: 
Size: 649862 Color: 3
Size: 348497 Color: 1

Bin 102: 1648 of cap free
Amount of items: 2
Items: 
Size: 516054 Color: 1
Size: 482299 Color: 4

Bin 103: 1666 of cap free
Amount of items: 2
Items: 
Size: 518728 Color: 3
Size: 479607 Color: 1

Bin 104: 1725 of cap free
Amount of items: 2
Items: 
Size: 595451 Color: 4
Size: 402825 Color: 0

Bin 105: 1763 of cap free
Amount of items: 2
Items: 
Size: 766629 Color: 2
Size: 231609 Color: 4

Bin 106: 1765 of cap free
Amount of items: 2
Items: 
Size: 692569 Color: 0
Size: 305667 Color: 4

Bin 107: 1818 of cap free
Amount of items: 3
Items: 
Size: 554285 Color: 1
Size: 234097 Color: 2
Size: 209801 Color: 4

Bin 108: 1868 of cap free
Amount of items: 2
Items: 
Size: 597495 Color: 3
Size: 400638 Color: 2

Bin 109: 1880 of cap free
Amount of items: 2
Items: 
Size: 605993 Color: 2
Size: 392128 Color: 3

Bin 110: 1894 of cap free
Amount of items: 2
Items: 
Size: 724338 Color: 4
Size: 273769 Color: 2

Bin 111: 2014 of cap free
Amount of items: 2
Items: 
Size: 644599 Color: 2
Size: 353388 Color: 3

Bin 112: 2038 of cap free
Amount of items: 2
Items: 
Size: 749806 Color: 3
Size: 248157 Color: 2

Bin 113: 2041 of cap free
Amount of items: 2
Items: 
Size: 688593 Color: 3
Size: 309367 Color: 4

Bin 114: 2042 of cap free
Amount of items: 2
Items: 
Size: 532389 Color: 4
Size: 465570 Color: 0

Bin 115: 2083 of cap free
Amount of items: 2
Items: 
Size: 557592 Color: 4
Size: 440326 Color: 3

Bin 116: 2087 of cap free
Amount of items: 2
Items: 
Size: 616696 Color: 0
Size: 381218 Color: 2

Bin 117: 2132 of cap free
Amount of items: 2
Items: 
Size: 524836 Color: 0
Size: 473033 Color: 4

Bin 118: 2142 of cap free
Amount of items: 2
Items: 
Size: 706876 Color: 4
Size: 290983 Color: 3

Bin 119: 2166 of cap free
Amount of items: 3
Items: 
Size: 754919 Color: 4
Size: 132619 Color: 1
Size: 110297 Color: 2

Bin 120: 2319 of cap free
Amount of items: 2
Items: 
Size: 532544 Color: 4
Size: 465138 Color: 1

Bin 121: 2362 of cap free
Amount of items: 2
Items: 
Size: 514904 Color: 4
Size: 482735 Color: 1

Bin 122: 2371 of cap free
Amount of items: 2
Items: 
Size: 704220 Color: 4
Size: 293410 Color: 2

Bin 123: 2467 of cap free
Amount of items: 2
Items: 
Size: 783690 Color: 3
Size: 213844 Color: 4

Bin 124: 2497 of cap free
Amount of items: 2
Items: 
Size: 685799 Color: 4
Size: 311705 Color: 2

Bin 125: 2502 of cap free
Amount of items: 2
Items: 
Size: 540761 Color: 3
Size: 456738 Color: 2

Bin 126: 2603 of cap free
Amount of items: 2
Items: 
Size: 790661 Color: 4
Size: 206737 Color: 0

Bin 127: 2642 of cap free
Amount of items: 2
Items: 
Size: 736529 Color: 1
Size: 260830 Color: 2

Bin 128: 2653 of cap free
Amount of items: 3
Items: 
Size: 379764 Color: 2
Size: 329551 Color: 3
Size: 288033 Color: 3

Bin 129: 2663 of cap free
Amount of items: 2
Items: 
Size: 701281 Color: 0
Size: 296057 Color: 3

Bin 130: 2670 of cap free
Amount of items: 2
Items: 
Size: 645012 Color: 4
Size: 352319 Color: 1

Bin 131: 2761 of cap free
Amount of items: 2
Items: 
Size: 655606 Color: 2
Size: 341634 Color: 4

Bin 132: 2768 of cap free
Amount of items: 2
Items: 
Size: 678917 Color: 4
Size: 318316 Color: 1

Bin 133: 2812 of cap free
Amount of items: 2
Items: 
Size: 662916 Color: 3
Size: 334273 Color: 0

Bin 134: 2839 of cap free
Amount of items: 2
Items: 
Size: 623947 Color: 4
Size: 373215 Color: 3

Bin 135: 2882 of cap free
Amount of items: 3
Items: 
Size: 738515 Color: 1
Size: 135814 Color: 3
Size: 122790 Color: 4

Bin 136: 2887 of cap free
Amount of items: 2
Items: 
Size: 503941 Color: 2
Size: 493173 Color: 1

Bin 137: 2951 of cap free
Amount of items: 2
Items: 
Size: 550318 Color: 1
Size: 446732 Color: 3

Bin 138: 2959 of cap free
Amount of items: 2
Items: 
Size: 678991 Color: 0
Size: 318051 Color: 3

Bin 139: 2984 of cap free
Amount of items: 3
Items: 
Size: 454780 Color: 4
Size: 272607 Color: 2
Size: 269630 Color: 3

Bin 140: 3019 of cap free
Amount of items: 2
Items: 
Size: 576757 Color: 2
Size: 420225 Color: 1

Bin 141: 3025 of cap free
Amount of items: 2
Items: 
Size: 618542 Color: 2
Size: 378434 Color: 1

Bin 142: 3051 of cap free
Amount of items: 3
Items: 
Size: 581336 Color: 4
Size: 224533 Color: 0
Size: 191081 Color: 4

Bin 143: 3217 of cap free
Amount of items: 2
Items: 
Size: 701080 Color: 2
Size: 295704 Color: 4

Bin 144: 3329 of cap free
Amount of items: 3
Items: 
Size: 576655 Color: 1
Size: 218163 Color: 2
Size: 201854 Color: 3

Bin 145: 3346 of cap free
Amount of items: 2
Items: 
Size: 528037 Color: 3
Size: 468618 Color: 1

Bin 146: 3388 of cap free
Amount of items: 2
Items: 
Size: 693017 Color: 4
Size: 303596 Color: 3

Bin 147: 3465 of cap free
Amount of items: 3
Items: 
Size: 347640 Color: 4
Size: 331073 Color: 0
Size: 317823 Color: 3

Bin 148: 3536 of cap free
Amount of items: 2
Items: 
Size: 503480 Color: 4
Size: 492985 Color: 0

Bin 149: 3650 of cap free
Amount of items: 2
Items: 
Size: 720082 Color: 3
Size: 276269 Color: 2

Bin 150: 3708 of cap free
Amount of items: 2
Items: 
Size: 798506 Color: 1
Size: 197787 Color: 3

Bin 151: 3780 of cap free
Amount of items: 2
Items: 
Size: 733766 Color: 0
Size: 262455 Color: 2

Bin 152: 3839 of cap free
Amount of items: 3
Items: 
Size: 625218 Color: 4
Size: 221404 Color: 1
Size: 149540 Color: 0

Bin 153: 3928 of cap free
Amount of items: 2
Items: 
Size: 564208 Color: 1
Size: 431865 Color: 2

Bin 154: 4239 of cap free
Amount of items: 2
Items: 
Size: 601124 Color: 2
Size: 394638 Color: 4

Bin 155: 4282 of cap free
Amount of items: 2
Items: 
Size: 752575 Color: 0
Size: 243144 Color: 2

Bin 156: 4297 of cap free
Amount of items: 2
Items: 
Size: 737832 Color: 2
Size: 257872 Color: 4

Bin 157: 4311 of cap free
Amount of items: 3
Items: 
Size: 488558 Color: 1
Size: 253653 Color: 0
Size: 253479 Color: 2

Bin 158: 4631 of cap free
Amount of items: 2
Items: 
Size: 790667 Color: 3
Size: 204703 Color: 1

Bin 159: 4679 of cap free
Amount of items: 2
Items: 
Size: 533281 Color: 1
Size: 462041 Color: 4

Bin 160: 4768 of cap free
Amount of items: 2
Items: 
Size: 781875 Color: 3
Size: 213358 Color: 0

Bin 161: 5372 of cap free
Amount of items: 2
Items: 
Size: 678910 Color: 3
Size: 315719 Color: 2

Bin 162: 5563 of cap free
Amount of items: 2
Items: 
Size: 550012 Color: 2
Size: 444426 Color: 1

Bin 163: 5638 of cap free
Amount of items: 2
Items: 
Size: 791308 Color: 0
Size: 203055 Color: 3

Bin 164: 5945 of cap free
Amount of items: 3
Items: 
Size: 377144 Color: 2
Size: 347835 Color: 0
Size: 269077 Color: 1

Bin 165: 6047 of cap free
Amount of items: 2
Items: 
Size: 638563 Color: 0
Size: 355391 Color: 2

Bin 166: 6263 of cap free
Amount of items: 2
Items: 
Size: 570740 Color: 2
Size: 422998 Color: 4

Bin 167: 6661 of cap free
Amount of items: 2
Items: 
Size: 664277 Color: 0
Size: 329063 Color: 1

Bin 168: 6814 of cap free
Amount of items: 2
Items: 
Size: 608423 Color: 3
Size: 384764 Color: 2

Bin 169: 6827 of cap free
Amount of items: 2
Items: 
Size: 504255 Color: 4
Size: 488919 Color: 3

Bin 170: 6879 of cap free
Amount of items: 2
Items: 
Size: 566297 Color: 0
Size: 426825 Color: 2

Bin 171: 7153 of cap free
Amount of items: 2
Items: 
Size: 758072 Color: 3
Size: 234776 Color: 1

Bin 172: 7442 of cap free
Amount of items: 2
Items: 
Size: 732983 Color: 0
Size: 259576 Color: 2

Bin 173: 7535 of cap free
Amount of items: 2
Items: 
Size: 574493 Color: 2
Size: 417973 Color: 1

Bin 174: 7611 of cap free
Amount of items: 2
Items: 
Size: 594157 Color: 3
Size: 398233 Color: 1

Bin 175: 7788 of cap free
Amount of items: 2
Items: 
Size: 500644 Color: 1
Size: 491569 Color: 0

Bin 176: 8051 of cap free
Amount of items: 2
Items: 
Size: 514580 Color: 4
Size: 477370 Color: 2

Bin 177: 8379 of cap free
Amount of items: 2
Items: 
Size: 661033 Color: 0
Size: 330589 Color: 2

Bin 178: 8971 of cap free
Amount of items: 2
Items: 
Size: 638374 Color: 2
Size: 352656 Color: 0

Bin 179: 9468 of cap free
Amount of items: 2
Items: 
Size: 732914 Color: 2
Size: 257619 Color: 4

Bin 180: 9501 of cap free
Amount of items: 2
Items: 
Size: 502529 Color: 2
Size: 487971 Color: 3

Bin 181: 9625 of cap free
Amount of items: 2
Items: 
Size: 637219 Color: 4
Size: 353157 Color: 2

Bin 182: 9839 of cap free
Amount of items: 2
Items: 
Size: 678186 Color: 3
Size: 311976 Color: 4

Bin 183: 10589 of cap free
Amount of items: 2
Items: 
Size: 748978 Color: 0
Size: 240434 Color: 3

Bin 184: 11170 of cap free
Amount of items: 2
Items: 
Size: 642966 Color: 0
Size: 345865 Color: 1

Bin 185: 11442 of cap free
Amount of items: 2
Items: 
Size: 717920 Color: 2
Size: 270639 Color: 3

Bin 186: 11755 of cap free
Amount of items: 2
Items: 
Size: 612001 Color: 2
Size: 376245 Color: 1

Bin 187: 11767 of cap free
Amount of items: 3
Items: 
Size: 553495 Color: 3
Size: 256835 Color: 1
Size: 177904 Color: 2

Bin 188: 13554 of cap free
Amount of items: 2
Items: 
Size: 524696 Color: 0
Size: 461751 Color: 2

Bin 189: 14551 of cap free
Amount of items: 2
Items: 
Size: 785112 Color: 0
Size: 200338 Color: 1

Bin 190: 14974 of cap free
Amount of items: 2
Items: 
Size: 696739 Color: 4
Size: 288288 Color: 1

Bin 191: 15685 of cap free
Amount of items: 2
Items: 
Size: 593070 Color: 4
Size: 391246 Color: 3

Bin 192: 16409 of cap free
Amount of items: 2
Items: 
Size: 631592 Color: 3
Size: 352000 Color: 2

Bin 193: 16529 of cap free
Amount of items: 2
Items: 
Size: 587000 Color: 4
Size: 396472 Color: 1

Bin 194: 16752 of cap free
Amount of items: 2
Items: 
Size: 748779 Color: 3
Size: 234470 Color: 1

Bin 195: 17411 of cap free
Amount of items: 2
Items: 
Size: 630716 Color: 3
Size: 351874 Color: 2

Bin 196: 19104 of cap free
Amount of items: 2
Items: 
Size: 529409 Color: 2
Size: 451488 Color: 3

Bin 197: 19318 of cap free
Amount of items: 2
Items: 
Size: 629656 Color: 4
Size: 351027 Color: 0

Bin 198: 21037 of cap free
Amount of items: 2
Items: 
Size: 595118 Color: 3
Size: 383846 Color: 0

Bin 199: 21067 of cap free
Amount of items: 2
Items: 
Size: 493659 Color: 2
Size: 485275 Color: 3

Bin 200: 22891 of cap free
Amount of items: 2
Items: 
Size: 489373 Color: 1
Size: 487737 Color: 4

Bin 201: 22950 of cap free
Amount of items: 2
Items: 
Size: 605138 Color: 3
Size: 371913 Color: 1

Bin 202: 23916 of cap free
Amount of items: 2
Items: 
Size: 587052 Color: 0
Size: 389033 Color: 3

Bin 203: 26236 of cap free
Amount of items: 3
Items: 
Size: 325639 Color: 4
Size: 324143 Color: 1
Size: 323983 Color: 0

Bin 204: 26982 of cap free
Amount of items: 2
Items: 
Size: 635788 Color: 0
Size: 337231 Color: 1

Bin 205: 30945 of cap free
Amount of items: 2
Items: 
Size: 779711 Color: 4
Size: 189345 Color: 1

Bin 206: 32911 of cap free
Amount of items: 2
Items: 
Size: 484643 Color: 4
Size: 482447 Color: 1

Bin 207: 33671 of cap free
Amount of items: 2
Items: 
Size: 522404 Color: 0
Size: 443926 Color: 3

Bin 208: 38270 of cap free
Amount of items: 2
Items: 
Size: 682000 Color: 4
Size: 279731 Color: 1

Bin 209: 50628 of cap free
Amount of items: 3
Items: 
Size: 323926 Color: 1
Size: 321879 Color: 3
Size: 303568 Color: 2

Bin 210: 61827 of cap free
Amount of items: 2
Items: 
Size: 567106 Color: 0
Size: 371068 Color: 1

Bin 211: 62405 of cap free
Amount of items: 3
Items: 
Size: 323007 Color: 4
Size: 314281 Color: 3
Size: 300308 Color: 2

Bin 212: 64266 of cap free
Amount of items: 2
Items: 
Size: 565779 Color: 4
Size: 369956 Color: 1

Bin 213: 72028 of cap free
Amount of items: 2
Items: 
Size: 467771 Color: 0
Size: 460202 Color: 2

Bin 214: 72716 of cap free
Amount of items: 2
Items: 
Size: 463997 Color: 1
Size: 463288 Color: 0

Bin 215: 75052 of cap free
Amount of items: 2
Items: 
Size: 486539 Color: 0
Size: 438410 Color: 3

Bin 216: 93706 of cap free
Amount of items: 3
Items: 
Size: 321820 Color: 4
Size: 308757 Color: 3
Size: 275718 Color: 1

Bin 217: 95495 of cap free
Amount of items: 2
Items: 
Size: 460303 Color: 0
Size: 444203 Color: 2

Bin 218: 105487 of cap free
Amount of items: 2
Items: 
Size: 451795 Color: 4
Size: 442719 Color: 2

Bin 219: 125762 of cap free
Amount of items: 2
Items: 
Size: 439647 Color: 2
Size: 434592 Color: 1

Bin 220: 180355 of cap free
Amount of items: 2
Items: 
Size: 469749 Color: 0
Size: 349897 Color: 3

Bin 221: 191717 of cap free
Amount of items: 2
Items: 
Size: 461554 Color: 4
Size: 346730 Color: 3

Bin 222: 202377 of cap free
Amount of items: 2
Items: 
Size: 451853 Color: 4
Size: 345771 Color: 3

Bin 223: 710704 of cap free
Amount of items: 1
Items: 
Size: 289297 Color: 2

Total size: 219918824
Total free space: 3081399

