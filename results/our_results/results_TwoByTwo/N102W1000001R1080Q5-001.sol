Capicity Bin: 1000001
Lower Bound: 46

Bins used: 47
Amount of Colors: 5

Bin 1: 1275 of cap free
Amount of items: 2
Items: 
Size: 571301 Color: 0
Size: 427425 Color: 2

Bin 2: 1408 of cap free
Amount of items: 2
Items: 
Size: 531845 Color: 3
Size: 466748 Color: 2

Bin 3: 4097 of cap free
Amount of items: 2
Items: 
Size: 650219 Color: 4
Size: 345685 Color: 1

Bin 4: 4478 of cap free
Amount of items: 2
Items: 
Size: 784327 Color: 3
Size: 211196 Color: 4

Bin 5: 4948 of cap free
Amount of items: 3
Items: 
Size: 554672 Color: 2
Size: 245323 Color: 0
Size: 195058 Color: 3

Bin 6: 5122 of cap free
Amount of items: 3
Items: 
Size: 380208 Color: 4
Size: 375097 Color: 4
Size: 239574 Color: 2

Bin 7: 5428 of cap free
Amount of items: 2
Items: 
Size: 595915 Color: 4
Size: 398658 Color: 3

Bin 8: 5484 of cap free
Amount of items: 2
Items: 
Size: 502270 Color: 2
Size: 492247 Color: 0

Bin 9: 5500 of cap free
Amount of items: 2
Items: 
Size: 734031 Color: 1
Size: 260470 Color: 0

Bin 10: 5657 of cap free
Amount of items: 2
Items: 
Size: 676395 Color: 1
Size: 317949 Color: 4

Bin 11: 6484 of cap free
Amount of items: 3
Items: 
Size: 691973 Color: 4
Size: 187903 Color: 3
Size: 113641 Color: 2

Bin 12: 6721 of cap free
Amount of items: 3
Items: 
Size: 729667 Color: 0
Size: 159775 Color: 3
Size: 103838 Color: 2

Bin 13: 6828 of cap free
Amount of items: 2
Items: 
Size: 604333 Color: 2
Size: 388840 Color: 0

Bin 14: 6929 of cap free
Amount of items: 2
Items: 
Size: 702385 Color: 1
Size: 290687 Color: 2

Bin 15: 8057 of cap free
Amount of items: 3
Items: 
Size: 786464 Color: 2
Size: 102843 Color: 3
Size: 102637 Color: 0

Bin 16: 8100 of cap free
Amount of items: 2
Items: 
Size: 769156 Color: 1
Size: 222745 Color: 2

Bin 17: 8705 of cap free
Amount of items: 2
Items: 
Size: 547454 Color: 2
Size: 443842 Color: 4

Bin 18: 11325 of cap free
Amount of items: 3
Items: 
Size: 606278 Color: 4
Size: 203025 Color: 0
Size: 179373 Color: 2

Bin 19: 11948 of cap free
Amount of items: 2
Items: 
Size: 546367 Color: 1
Size: 441686 Color: 4

Bin 20: 13091 of cap free
Amount of items: 2
Items: 
Size: 525585 Color: 1
Size: 461325 Color: 3

Bin 21: 14598 of cap free
Amount of items: 2
Items: 
Size: 715277 Color: 4
Size: 270126 Color: 3

Bin 22: 15267 of cap free
Amount of items: 2
Items: 
Size: 620791 Color: 4
Size: 363943 Color: 3

Bin 23: 16312 of cap free
Amount of items: 2
Items: 
Size: 545956 Color: 0
Size: 437733 Color: 3

Bin 24: 17239 of cap free
Amount of items: 3
Items: 
Size: 361514 Color: 1
Size: 314084 Color: 3
Size: 307164 Color: 4

Bin 25: 17385 of cap free
Amount of items: 2
Items: 
Size: 632707 Color: 1
Size: 349909 Color: 0

Bin 26: 17854 of cap free
Amount of items: 2
Items: 
Size: 577060 Color: 3
Size: 405087 Color: 2

Bin 27: 19941 of cap free
Amount of items: 2
Items: 
Size: 654486 Color: 2
Size: 325574 Color: 3

Bin 28: 23601 of cap free
Amount of items: 2
Items: 
Size: 519903 Color: 4
Size: 456497 Color: 0

Bin 29: 26853 of cap free
Amount of items: 2
Items: 
Size: 571472 Color: 3
Size: 401676 Color: 4

Bin 30: 27090 of cap free
Amount of items: 2
Items: 
Size: 709329 Color: 4
Size: 263582 Color: 1

Bin 31: 33334 of cap free
Amount of items: 2
Items: 
Size: 660337 Color: 1
Size: 306330 Color: 2

Bin 32: 34014 of cap free
Amount of items: 2
Items: 
Size: 694935 Color: 2
Size: 271052 Color: 4

Bin 33: 36850 of cap free
Amount of items: 2
Items: 
Size: 794527 Color: 0
Size: 168624 Color: 4

Bin 34: 38087 of cap free
Amount of items: 2
Items: 
Size: 740526 Color: 0
Size: 221388 Color: 4

Bin 35: 39832 of cap free
Amount of items: 2
Items: 
Size: 764380 Color: 0
Size: 195789 Color: 1

Bin 36: 41285 of cap free
Amount of items: 2
Items: 
Size: 563283 Color: 3
Size: 395433 Color: 4

Bin 37: 47031 of cap free
Amount of items: 3
Items: 
Size: 711777 Color: 0
Size: 124326 Color: 0
Size: 116867 Color: 2

Bin 38: 59197 of cap free
Amount of items: 2
Items: 
Size: 694877 Color: 0
Size: 245927 Color: 1

Bin 39: 64973 of cap free
Amount of items: 2
Items: 
Size: 507025 Color: 2
Size: 428003 Color: 1

Bin 40: 65712 of cap free
Amount of items: 2
Items: 
Size: 692395 Color: 1
Size: 241894 Color: 3

Bin 41: 68558 of cap free
Amount of items: 2
Items: 
Size: 598544 Color: 1
Size: 332899 Color: 0

Bin 42: 71205 of cap free
Amount of items: 2
Items: 
Size: 537872 Color: 0
Size: 390924 Color: 4

Bin 43: 73778 of cap free
Amount of items: 2
Items: 
Size: 504029 Color: 0
Size: 422194 Color: 3

Bin 44: 120724 of cap free
Amount of items: 2
Items: 
Size: 481067 Color: 0
Size: 398210 Color: 3

Bin 45: 190981 of cap free
Amount of items: 2
Items: 
Size: 416314 Color: 3
Size: 392706 Color: 2

Bin 46: 308525 of cap free
Amount of items: 2
Items: 
Size: 350259 Color: 2
Size: 341217 Color: 1

Bin 47: 320433 of cap free
Amount of items: 2
Items: 
Size: 369329 Color: 4
Size: 310239 Color: 0

Total size: 45057803
Total free space: 1942244

