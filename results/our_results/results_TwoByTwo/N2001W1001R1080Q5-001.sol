Capicity Bin: 1001
Lower Bound: 908

Bins used: 913
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 3
Size: 334 Color: 4
Size: 322 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 3
Size: 337 Color: 1
Size: 322 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 0
Size: 329 Color: 4
Size: 322 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 1
Size: 332 Color: 4
Size: 329 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 0
Size: 344 Color: 4
Size: 304 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 3
Size: 279 Color: 2
Size: 279 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 280 Color: 1
Size: 275 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 2
Size: 276 Color: 3
Size: 250 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 2
Size: 273 Color: 1
Size: 250 Color: 4

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 4
Size: 500 Color: 0

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 2
Size: 500 Color: 4

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 4
Size: 500 Color: 3

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 3
Size: 499 Color: 0

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 498 Color: 4

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 4
Size: 498 Color: 2

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 4
Size: 496 Color: 2

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 2
Size: 496 Color: 0

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 496 Color: 3

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 2
Size: 495 Color: 1

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 495 Color: 3

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 0
Size: 493 Color: 4

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 3
Size: 493 Color: 4

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 1
Size: 493 Color: 2

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 4
Size: 494 Color: 3

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 3
Size: 492 Color: 0

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 1
Size: 492 Color: 4

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 1

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 491 Color: 0

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 4
Size: 489 Color: 2

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 4
Size: 487 Color: 2

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 485 Color: 2

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 2
Size: 487 Color: 1

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 2
Size: 486 Color: 4

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 4
Size: 485 Color: 0

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 4
Size: 485 Color: 1

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 485 Color: 1

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 484 Color: 0

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 4
Size: 483 Color: 2

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 3
Size: 482 Color: 1

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 2
Size: 482 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 521 Color: 0
Size: 241 Color: 1
Size: 239 Color: 4

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 2
Size: 481 Color: 3

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 4
Size: 240 Color: 1
Size: 239 Color: 3

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 0
Size: 477 Color: 1

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 477 Color: 2

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 2
Size: 476 Color: 0

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 1
Size: 476 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 4
Size: 239 Color: 0
Size: 237 Color: 2

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 3
Size: 476 Color: 2

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 474 Color: 2

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 1
Size: 475 Color: 2

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 0

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 4
Size: 472 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 2
Size: 238 Color: 2
Size: 238 Color: 1

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 0
Size: 471 Color: 1

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 3
Size: 470 Color: 2

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 2
Size: 470 Color: 3

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 2
Size: 470 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 533 Color: 4
Size: 236 Color: 0
Size: 232 Color: 3

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 2
Size: 469 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 533 Color: 4
Size: 237 Color: 1
Size: 231 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 3
Size: 235 Color: 3
Size: 232 Color: 0

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 2
Size: 468 Color: 4

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 2
Size: 468 Color: 3

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 4
Size: 465 Color: 0

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 0
Size: 463 Color: 1

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 0
Size: 462 Color: 2

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 462 Color: 1

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 464 Color: 3

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 4
Size: 461 Color: 0

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 0
Size: 460 Color: 3

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 2
Size: 459 Color: 3

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 0
Size: 456 Color: 4

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 3
Size: 456 Color: 1

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 4
Size: 456 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 545 Color: 4
Size: 231 Color: 3
Size: 225 Color: 1

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 0
Size: 455 Color: 4

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 1
Size: 452 Color: 4

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 4
Size: 452 Color: 0

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 3
Size: 452 Color: 1

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 2
Size: 451 Color: 0

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 1
Size: 450 Color: 4

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 4
Size: 452 Color: 3

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 0
Size: 448 Color: 2

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 3
Size: 448 Color: 4

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 0
Size: 447 Color: 4

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 2
Size: 447 Color: 0

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 4
Size: 449 Color: 3

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 445 Color: 3

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 0
Size: 445 Color: 4

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 3
Size: 444 Color: 0

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 3
Size: 443 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 3
Size: 224 Color: 0
Size: 217 Color: 1

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 440 Color: 4

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 440 Color: 4

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 4
Size: 446 Color: 3

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 3

Bin 99: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 2
Size: 439 Color: 3

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 4

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 3
Size: 438 Color: 2

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 4
Size: 444 Color: 3

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 437 Color: 4

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 4
Size: 440 Color: 2

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 2
Size: 434 Color: 0

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 0
Size: 434 Color: 3

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 0

Bin 108: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 0

Bin 109: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 1
Size: 433 Color: 3

Bin 110: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 4
Size: 434 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 571 Color: 0
Size: 216 Color: 1
Size: 214 Color: 3

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 3
Size: 431 Color: 1

Bin 113: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 1
Size: 429 Color: 0

Bin 114: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 3
Size: 429 Color: 2

Bin 115: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 3
Size: 428 Color: 1

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 3

Bin 117: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 3
Size: 426 Color: 2

Bin 118: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 2
Size: 422 Color: 4

Bin 119: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 2
Size: 422 Color: 4

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 4
Size: 422 Color: 1

Bin 121: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 3
Size: 421 Color: 0

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 2
Size: 421 Color: 4

Bin 123: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 419 Color: 3

Bin 124: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 419 Color: 4

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 2
Size: 419 Color: 3

Bin 126: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 2
Size: 418 Color: 1

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 2
Size: 418 Color: 4

Bin 128: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 2
Size: 417 Color: 1

Bin 129: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 3
Size: 416 Color: 4

Bin 130: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 414 Color: 2

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 0
Size: 216 Color: 4
Size: 196 Color: 1

Bin 132: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 3
Size: 412 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 590 Color: 0
Size: 215 Color: 3
Size: 196 Color: 1

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 3
Size: 411 Color: 1

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 0

Bin 136: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 2
Size: 411 Color: 3

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 2
Size: 409 Color: 3

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 1
Size: 409 Color: 0

Bin 139: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 4
Size: 408 Color: 3

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 1
Size: 407 Color: 3

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 4
Size: 407 Color: 3

Bin 142: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 2
Size: 407 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 2
Size: 209 Color: 3
Size: 197 Color: 3

Bin 144: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 406 Color: 0

Bin 145: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 2
Size: 405 Color: 4

Bin 146: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 2
Size: 405 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 0
Size: 208 Color: 4
Size: 196 Color: 2

Bin 148: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 3
Size: 404 Color: 4

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 599 Color: 0
Size: 207 Color: 1
Size: 195 Color: 2

Bin 150: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 3
Size: 402 Color: 4

Bin 151: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 600 Color: 2
Size: 206 Color: 0
Size: 195 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 601 Color: 0
Size: 206 Color: 1
Size: 194 Color: 2

Bin 154: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 2
Size: 400 Color: 3

Bin 155: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 4
Size: 398 Color: 1

Bin 156: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 2
Size: 398 Color: 3

Bin 157: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 397 Color: 0

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 0

Bin 159: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 4
Size: 395 Color: 1

Bin 160: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 394 Color: 3

Bin 161: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 1
Size: 394 Color: 2

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 607 Color: 0
Size: 197 Color: 4
Size: 197 Color: 3

Bin 163: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 394 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 607 Color: 0
Size: 199 Color: 4
Size: 195 Color: 3

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 608 Color: 4
Size: 198 Color: 2
Size: 195 Color: 3

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 4
Size: 393 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 608 Color: 0
Size: 198 Color: 4
Size: 195 Color: 3

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 608 Color: 1
Size: 199 Color: 0
Size: 194 Color: 2

Bin 169: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 2

Bin 170: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 2
Size: 391 Color: 0

Bin 171: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 3
Size: 391 Color: 2

Bin 172: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 1
Size: 391 Color: 3

Bin 173: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 3
Size: 391 Color: 0

Bin 174: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 4

Bin 175: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 2
Size: 389 Color: 1

Bin 176: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 2
Size: 389 Color: 3

Bin 177: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 1
Size: 388 Color: 4

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 0
Size: 194 Color: 1
Size: 193 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 615 Color: 2
Size: 194 Color: 0
Size: 192 Color: 1

Bin 180: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 3
Size: 386 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 615 Color: 2
Size: 196 Color: 4
Size: 190 Color: 0

Bin 182: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 3
Size: 386 Color: 4

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 616 Color: 2
Size: 193 Color: 0
Size: 192 Color: 3

Bin 184: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 3
Size: 383 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 4
Size: 193 Color: 1
Size: 190 Color: 0

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 2
Size: 382 Color: 1

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 2
Size: 382 Color: 1

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 3
Size: 382 Color: 1

Bin 189: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 2
Size: 381 Color: 4

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 4
Size: 191 Color: 1
Size: 189 Color: 0

Bin 191: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 3
Size: 379 Color: 4

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 3
Size: 190 Color: 4
Size: 189 Color: 2

Bin 193: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 3
Size: 378 Color: 2

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 3
Size: 189 Color: 3
Size: 188 Color: 1

Bin 195: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 3
Size: 376 Color: 2

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 625 Color: 4
Size: 188 Color: 2
Size: 188 Color: 1

Bin 197: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 4
Size: 375 Color: 3

Bin 198: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 2
Size: 375 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 629 Color: 0
Size: 186 Color: 1
Size: 186 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 4
Size: 187 Color: 0
Size: 186 Color: 1

Bin 201: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 1
Size: 372 Color: 2

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 4
Size: 371 Color: 3

Bin 203: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 4
Size: 371 Color: 3

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 0
Size: 370 Color: 2

Bin 205: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 0
Size: 370 Color: 2

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 1
Size: 371 Color: 2

Bin 207: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 2
Size: 370 Color: 3

Bin 208: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 2
Size: 370 Color: 4

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 0
Size: 187 Color: 2
Size: 182 Color: 1

Bin 210: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 4
Size: 369 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 0
Size: 186 Color: 1
Size: 182 Color: 1

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 3
Size: 368 Color: 2

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 4
Size: 365 Color: 2

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 637 Color: 0
Size: 182 Color: 4
Size: 182 Color: 2

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 4
Size: 364 Color: 3

Bin 216: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 2

Bin 217: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 2
Size: 364 Color: 0

Bin 218: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 4
Size: 363 Color: 1

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 363 Color: 4

Bin 220: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 2

Bin 221: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 1
Size: 362 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 2
Size: 184 Color: 0
Size: 178 Color: 4

Bin 223: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 4
Size: 362 Color: 2

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 0
Size: 181 Color: 4
Size: 180 Color: 3

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 2
Size: 362 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 0
Size: 181 Color: 2
Size: 179 Color: 3

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 3
Size: 360 Color: 2

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 642 Color: 1
Size: 180 Color: 3
Size: 179 Color: 0

Bin 229: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 2

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 643 Color: 3
Size: 179 Color: 2
Size: 179 Color: 0

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 4
Size: 357 Color: 3

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 1
Size: 356 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 3
Size: 178 Color: 4
Size: 178 Color: 1

Bin 234: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 4
Size: 356 Color: 1

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 646 Color: 3
Size: 178 Color: 0
Size: 177 Color: 1

Bin 236: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 2
Size: 355 Color: 3

Bin 237: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 0
Size: 353 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 648 Color: 0
Size: 178 Color: 1
Size: 175 Color: 3

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 648 Color: 0
Size: 178 Color: 4
Size: 175 Color: 1

Bin 240: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 2
Size: 354 Color: 3

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 0
Size: 177 Color: 2
Size: 175 Color: 1

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 2
Size: 353 Color: 4

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 2
Size: 177 Color: 4
Size: 175 Color: 0

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 3
Size: 352 Color: 2

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 1
Size: 176 Color: 0
Size: 175 Color: 2

Bin 246: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 3
Size: 351 Color: 1

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 2
Size: 176 Color: 1
Size: 175 Color: 0

Bin 248: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 2
Size: 348 Color: 3

Bin 249: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 3
Size: 347 Color: 0

Bin 250: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 2
Size: 347 Color: 4

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 2

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 4
Size: 345 Color: 1

Bin 253: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 4
Size: 345 Color: 3

Bin 254: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 3
Size: 345 Color: 4

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 4
Size: 172 Color: 2
Size: 172 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 3
Size: 174 Color: 0
Size: 170 Color: 1

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 344 Color: 2

Bin 258: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 343 Color: 4

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 0
Size: 341 Color: 1

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 4
Size: 342 Color: 1

Bin 261: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 3
Size: 339 Color: 1

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 3
Size: 338 Color: 1

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 4
Size: 169 Color: 0
Size: 168 Color: 3

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 336 Color: 2

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 3
Size: 170 Color: 0
Size: 166 Color: 2

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 0
Size: 169 Color: 4
Size: 167 Color: 1

Bin 267: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 2
Size: 336 Color: 3

Bin 268: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 2
Size: 335 Color: 1

Bin 269: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 3
Size: 333 Color: 1

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 4
Size: 333 Color: 0

Bin 271: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 1
Size: 333 Color: 2

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 669 Color: 0
Size: 167 Color: 1
Size: 165 Color: 1

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 2
Size: 331 Color: 0

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 1
Size: 331 Color: 3

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 4
Size: 167 Color: 0
Size: 163 Color: 2

Bin 276: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 330 Color: 3

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 4
Size: 167 Color: 3
Size: 163 Color: 2

Bin 278: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 3
Size: 330 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 4
Size: 165 Color: 4
Size: 164 Color: 3

Bin 280: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 4
Size: 328 Color: 3

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 3
Size: 328 Color: 4

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 327 Color: 1

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 2
Size: 326 Color: 4

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 327 Color: 1

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 2
Size: 326 Color: 1

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 3
Size: 327 Color: 4

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 327 Color: 3

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 2
Size: 165 Color: 4
Size: 160 Color: 4

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 2
Size: 163 Color: 0
Size: 162 Color: 4

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 1
Size: 326 Color: 4

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 2
Size: 163 Color: 1
Size: 162 Color: 4

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 4
Size: 326 Color: 3

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 4
Size: 165 Color: 2
Size: 160 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 2
Size: 163 Color: 3
Size: 162 Color: 0

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 3
Size: 325 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 4
Size: 164 Color: 2
Size: 160 Color: 0

Bin 297: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 3
Size: 323 Color: 0

Bin 298: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 0
Size: 321 Color: 4

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 3
Size: 321 Color: 1

Bin 300: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 4
Size: 321 Color: 2

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 321 Color: 4

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 680 Color: 1
Size: 161 Color: 3
Size: 160 Color: 2

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 2
Size: 160 Color: 3
Size: 159 Color: 1

Bin 304: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 1
Size: 319 Color: 4

Bin 305: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 4
Size: 319 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 3
Size: 161 Color: 4
Size: 158 Color: 2

Bin 307: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 1
Size: 319 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 0
Size: 159 Color: 4
Size: 159 Color: 1

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 317 Color: 0

Bin 310: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 3
Size: 316 Color: 2

Bin 311: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 3
Size: 316 Color: 1

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 0
Size: 315 Color: 1

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 1
Size: 315 Color: 2

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 3
Size: 315 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 2
Size: 158 Color: 4
Size: 156 Color: 1

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 3
Size: 314 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 2
Size: 156 Color: 3
Size: 156 Color: 3

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 1
Size: 312 Color: 4

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 4
Size: 312 Color: 2

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 691 Color: 0
Size: 156 Color: 3
Size: 154 Color: 2

Bin 321: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 2
Size: 310 Color: 3

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 4
Size: 309 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 1
Size: 155 Color: 1
Size: 153 Color: 4

Bin 324: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 0
Size: 308 Color: 1

Bin 325: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 0
Size: 307 Color: 3

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 2
Size: 307 Color: 0

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 306 Color: 3

Bin 328: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 3
Size: 306 Color: 0

Bin 329: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 3
Size: 304 Color: 4

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 0

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 4
Size: 304 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 4
Size: 152 Color: 3
Size: 151 Color: 0

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 1
Size: 303 Color: 2

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 1
Size: 151 Color: 2
Size: 151 Color: 1

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 1
Size: 302 Color: 4

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 0
Size: 152 Color: 2
Size: 150 Color: 4

Bin 337: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 3
Size: 300 Color: 4

Bin 338: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 4
Size: 299 Color: 1

Bin 339: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 0
Size: 298 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 0
Size: 149 Color: 2
Size: 149 Color: 1

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 0
Size: 298 Color: 3

Bin 342: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 297 Color: 2

Bin 343: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 297 Color: 4

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 2
Size: 149 Color: 1
Size: 148 Color: 3

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 4
Size: 296 Color: 1

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 296 Color: 0

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 0
Size: 295 Color: 4

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 3
Size: 294 Color: 1

Bin 349: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 1
Size: 294 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 4
Size: 149 Color: 1
Size: 145 Color: 2

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 4
Size: 293 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 0
Size: 147 Color: 1
Size: 146 Color: 2

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 4
Size: 146 Color: 3
Size: 146 Color: 3

Bin 354: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 0
Size: 291 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 1
Size: 146 Color: 2
Size: 145 Color: 3

Bin 356: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 3
Size: 290 Color: 4

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 4
Size: 290 Color: 3

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 3
Size: 289 Color: 0

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 0
Size: 289 Color: 2

Bin 360: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 3
Size: 288 Color: 2

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 3
Size: 288 Color: 4

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 3
Size: 147 Color: 3
Size: 141 Color: 2

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 1
Size: 288 Color: 4

Bin 364: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 1
Size: 288 Color: 4

Bin 365: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 4

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 2
Size: 286 Color: 0

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 4
Size: 287 Color: 3

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 3
Size: 146 Color: 1
Size: 140 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 2
Size: 145 Color: 1
Size: 139 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 2
Size: 142 Color: 1
Size: 142 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 3
Size: 143 Color: 4
Size: 141 Color: 2

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 283 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 0
Size: 142 Color: 2
Size: 139 Color: 0

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 1
Size: 281 Color: 4

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 2
Size: 140 Color: 3
Size: 139 Color: 4

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 0
Size: 280 Color: 3

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 3
Size: 140 Color: 2
Size: 139 Color: 1

Bin 378: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 4
Size: 279 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 2
Size: 139 Color: 4
Size: 139 Color: 4

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 4
Size: 278 Color: 3

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 4
Size: 278 Color: 2

Bin 382: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 4
Size: 276 Color: 1

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 3
Size: 138 Color: 3
Size: 138 Color: 2

Bin 384: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 275 Color: 0

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 275 Color: 1

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 1
Size: 274 Color: 2

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 2
Size: 274 Color: 3

Bin 388: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 3
Size: 274 Color: 1

Bin 389: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 0
Size: 274 Color: 4

Bin 390: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 2
Size: 272 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 1
Size: 136 Color: 4
Size: 136 Color: 4

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 3
Size: 271 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 730 Color: 4
Size: 136 Color: 2
Size: 135 Color: 4

Bin 394: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 0
Size: 271 Color: 4

Bin 395: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 1
Size: 270 Color: 2

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 731 Color: 1
Size: 136 Color: 2
Size: 134 Color: 4

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 2
Size: 270 Color: 0

Bin 398: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 1
Size: 269 Color: 2

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 0
Size: 268 Color: 4

Bin 400: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 1
Size: 268 Color: 4

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 4
Size: 135 Color: 2
Size: 133 Color: 3

Bin 402: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 0
Size: 267 Color: 3

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 3
Size: 267 Color: 4

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 0
Size: 267 Color: 2

Bin 405: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 1
Size: 267 Color: 4

Bin 406: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 0
Size: 266 Color: 1

Bin 407: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 4
Size: 266 Color: 1

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 2
Size: 266 Color: 3

Bin 409: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 4

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 3

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 2
Size: 265 Color: 0

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 3
Size: 266 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 736 Color: 2
Size: 133 Color: 1
Size: 132 Color: 4

Bin 414: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 3
Size: 266 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 736 Color: 1
Size: 134 Color: 2
Size: 131 Color: 0

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 1
Size: 264 Color: 3

Bin 417: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 264 Color: 2

Bin 418: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 3
Size: 263 Color: 1

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 4
Size: 263 Color: 2

Bin 420: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 3
Size: 263 Color: 1

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 1
Size: 262 Color: 0

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 4
Size: 262 Color: 1

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 261 Color: 4

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 740 Color: 3
Size: 131 Color: 1
Size: 130 Color: 2

Bin 425: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 4
Size: 261 Color: 3

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 740 Color: 0
Size: 131 Color: 3
Size: 130 Color: 2

Bin 427: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 0
Size: 260 Color: 4

Bin 428: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 3
Size: 259 Color: 0

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 3
Size: 259 Color: 2

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 2
Size: 258 Color: 4

Bin 431: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 4
Size: 258 Color: 1

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 1
Size: 130 Color: 2
Size: 128 Color: 1

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 1
Size: 258 Color: 0

Bin 434: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 1
Size: 258 Color: 4

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 2
Size: 257 Color: 3

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 4

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 4
Size: 256 Color: 1

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 0
Size: 129 Color: 2
Size: 127 Color: 1

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 256 Color: 4

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 4
Size: 255 Color: 2

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 4
Size: 255 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 2
Size: 128 Color: 3
Size: 127 Color: 3

Bin 443: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 3
Size: 255 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 2
Size: 128 Color: 4
Size: 127 Color: 1

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 0
Size: 255 Color: 3

Bin 446: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 1
Size: 254 Color: 2

Bin 447: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 4
Size: 254 Color: 1

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 3
Size: 254 Color: 2

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 1
Size: 254 Color: 3

Bin 450: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 0
Size: 254 Color: 2

Bin 451: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 0
Size: 253 Color: 4

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 2
Size: 127 Color: 4
Size: 126 Color: 0

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 252 Color: 1

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 2
Size: 126 Color: 4
Size: 126 Color: 1

Bin 455: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 2
Size: 252 Color: 1

Bin 456: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 1
Size: 251 Color: 3

Bin 457: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 250 Color: 0

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 250 Color: 2

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 2
Size: 126 Color: 1
Size: 123 Color: 1

Bin 460: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 2
Size: 249 Color: 4

Bin 461: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 4
Size: 249 Color: 3

Bin 462: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 4
Size: 249 Color: 0

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 2
Size: 248 Color: 1

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 246 Color: 0

Bin 465: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 3
Size: 246 Color: 1

Bin 466: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 246 Color: 1

Bin 467: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 4
Size: 244 Color: 3

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 0
Size: 125 Color: 2
Size: 119 Color: 4

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 3
Size: 243 Color: 4

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 759 Color: 2
Size: 121 Color: 1
Size: 121 Color: 1

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 3
Size: 125 Color: 0
Size: 118 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 2
Size: 121 Color: 3
Size: 119 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 0
Size: 123 Color: 4
Size: 118 Color: 1

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 2
Size: 122 Color: 1
Size: 117 Color: 4

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 2
Size: 120 Color: 1
Size: 119 Color: 1

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 1
Size: 240 Color: 4

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 4
Size: 239 Color: 0

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 0
Size: 237 Color: 2

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 1
Size: 237 Color: 3

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 4
Size: 237 Color: 2

Bin 481: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 0
Size: 234 Color: 4

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 3
Size: 233 Color: 4

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 0
Size: 233 Color: 3

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 3
Size: 119 Color: 2
Size: 114 Color: 4

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 3
Size: 232 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 770 Color: 2
Size: 118 Color: 3
Size: 113 Color: 3

Bin 487: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 1
Size: 232 Color: 4

Bin 488: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 4
Size: 231 Color: 2

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 230 Color: 3

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 3
Size: 230 Color: 1

Bin 491: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 4
Size: 230 Color: 2

Bin 492: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 0
Size: 229 Color: 3

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 0
Size: 228 Color: 4

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 4
Size: 114 Color: 2
Size: 114 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 4
Size: 115 Color: 1
Size: 113 Color: 2

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 3
Size: 228 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 3
Size: 115 Color: 2
Size: 113 Color: 1

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 227 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 774 Color: 1
Size: 114 Color: 2
Size: 113 Color: 3

Bin 500: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 0
Size: 227 Color: 3

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 775 Color: 4
Size: 113 Color: 3
Size: 113 Color: 2

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 1
Size: 225 Color: 2

Bin 503: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 0
Size: 224 Color: 4

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 0
Size: 224 Color: 2

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 3
Size: 223 Color: 0

Bin 506: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 2
Size: 224 Color: 4

Bin 507: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 3
Size: 222 Color: 1

Bin 508: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 4
Size: 222 Color: 1

Bin 509: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 3

Bin 510: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 4
Size: 221 Color: 2

Bin 511: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 4
Size: 221 Color: 3

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 2
Size: 112 Color: 3
Size: 109 Color: 0

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 1
Size: 220 Color: 3

Bin 514: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 219 Color: 4

Bin 515: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 2
Size: 218 Color: 1

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 217 Color: 2

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 3
Size: 217 Color: 4

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 785 Color: 2
Size: 109 Color: 3
Size: 107 Color: 3

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 4
Size: 216 Color: 3

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 785 Color: 4
Size: 112 Color: 2
Size: 104 Color: 1

Bin 521: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 1
Size: 216 Color: 4

Bin 522: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 3
Size: 216 Color: 1

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 2
Size: 109 Color: 1
Size: 106 Color: 0

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 3
Size: 216 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 2
Size: 111 Color: 0
Size: 104 Color: 4

Bin 526: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 4
Size: 215 Color: 1

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 2
Size: 108 Color: 0
Size: 106 Color: 4

Bin 528: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 215 Color: 4

Bin 529: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 4
Size: 213 Color: 0

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 2

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 0
Size: 212 Color: 4

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 791 Color: 2
Size: 107 Color: 0
Size: 103 Color: 3

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 3
Size: 210 Color: 1

Bin 534: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 1
Size: 209 Color: 2

Bin 535: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 4

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 1
Size: 109 Color: 2
Size: 100 Color: 0

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 0
Size: 208 Color: 1

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 0

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 794 Color: 0
Size: 107 Color: 2
Size: 100 Color: 3

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 795 Color: 3
Size: 106 Color: 2
Size: 100 Color: 4

Bin 541: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 4
Size: 204 Color: 2

Bin 542: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 204 Color: 4

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 797 Color: 2
Size: 104 Color: 3
Size: 100 Color: 4

Bin 544: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 204 Color: 0

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 204 Color: 3

Bin 546: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 4
Size: 203 Color: 2

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 799 Color: 2
Size: 102 Color: 4
Size: 100 Color: 3

Bin 548: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 3
Size: 202 Color: 4

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 800 Color: 0
Size: 101 Color: 3
Size: 100 Color: 2

Bin 550: 1 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 280 Color: 1
Size: 273 Color: 1

Bin 551: 1 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 0
Size: 494 Color: 2

Bin 552: 1 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 490 Color: 0

Bin 553: 1 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 490 Color: 1

Bin 554: 1 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 490 Color: 1

Bin 555: 1 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 0
Size: 491 Color: 3

Bin 556: 1 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 3
Size: 487 Color: 2

Bin 557: 1 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 1
Size: 486 Color: 0

Bin 558: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 3
Size: 483 Color: 4

Bin 559: 1 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 4
Size: 483 Color: 2

Bin 560: 1 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 1
Size: 481 Color: 2

Bin 561: 1 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 1
Size: 481 Color: 2

Bin 562: 1 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 1
Size: 481 Color: 3

Bin 563: 1 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 0
Size: 476 Color: 3

Bin 564: 1 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 0
Size: 467 Color: 1

Bin 565: 1 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 4
Size: 460 Color: 1

Bin 566: 1 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 4
Size: 231 Color: 1
Size: 229 Color: 0

Bin 567: 1 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 4
Size: 458 Color: 1

Bin 568: 1 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 2
Size: 450 Color: 4

Bin 569: 1 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 449 Color: 1

Bin 570: 1 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 449 Color: 1

Bin 571: 1 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 0
Size: 447 Color: 1

Bin 572: 1 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 444 Color: 0

Bin 573: 1 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 1
Size: 443 Color: 2

Bin 574: 1 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 1
Size: 443 Color: 4

Bin 575: 1 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 4
Size: 227 Color: 0
Size: 218 Color: 3

Bin 576: 1 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 435 Color: 3

Bin 577: 1 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 430 Color: 2

Bin 578: 1 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 1
Size: 431 Color: 2

Bin 579: 1 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 2
Size: 430 Color: 3

Bin 580: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 4
Size: 427 Color: 0

Bin 581: 1 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 4
Size: 421 Color: 0

Bin 582: 1 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 2
Size: 419 Color: 3

Bin 583: 1 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 0
Size: 417 Color: 3

Bin 584: 1 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 1
Size: 417 Color: 3

Bin 585: 1 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 0
Size: 417 Color: 2

Bin 586: 1 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 4
Size: 415 Color: 0

Bin 587: 1 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 3
Size: 412 Color: 2

Bin 588: 1 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 2
Size: 409 Color: 0

Bin 589: 1 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 0
Size: 409 Color: 3

Bin 590: 1 of cap free
Amount of items: 3
Items: 
Size: 594 Color: 0
Size: 209 Color: 3
Size: 197 Color: 3

Bin 591: 1 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 3
Size: 401 Color: 0

Bin 592: 1 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 4
Size: 397 Color: 0

Bin 593: 1 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 396 Color: 4

Bin 594: 1 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 4
Size: 396 Color: 1

Bin 595: 1 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 2
Size: 396 Color: 4

Bin 596: 1 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 1
Size: 393 Color: 0

Bin 597: 1 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 2
Size: 391 Color: 0

Bin 598: 1 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 2
Size: 389 Color: 1

Bin 599: 1 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 2
Size: 385 Color: 1

Bin 600: 1 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 0
Size: 380 Color: 1

Bin 601: 1 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 0
Size: 376 Color: 3

Bin 602: 1 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 3
Size: 370 Color: 4

Bin 603: 1 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 368 Color: 2

Bin 604: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 3
Size: 367 Color: 0

Bin 605: 1 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 3
Size: 365 Color: 1

Bin 606: 1 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 4
Size: 365 Color: 0

Bin 607: 1 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 2
Size: 361 Color: 0

Bin 608: 1 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 1
Size: 361 Color: 3

Bin 609: 1 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 2
Size: 361 Color: 3

Bin 610: 1 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 1
Size: 353 Color: 4

Bin 611: 1 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 3
Size: 173 Color: 1
Size: 172 Color: 0

Bin 612: 1 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 2
Size: 173 Color: 1
Size: 172 Color: 0

Bin 613: 1 of cap free
Amount of items: 3
Items: 
Size: 658 Color: 1
Size: 172 Color: 2
Size: 170 Color: 3

Bin 614: 1 of cap free
Amount of items: 3
Items: 
Size: 658 Color: 1
Size: 172 Color: 0
Size: 170 Color: 4

Bin 615: 1 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 4
Size: 342 Color: 2

Bin 616: 1 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 3
Size: 338 Color: 2

Bin 617: 1 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 0
Size: 169 Color: 2
Size: 168 Color: 4

Bin 618: 1 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 337 Color: 1

Bin 619: 1 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 4
Size: 337 Color: 0

Bin 620: 1 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 4
Size: 334 Color: 2

Bin 621: 1 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 4
Size: 332 Color: 0

Bin 622: 1 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 4
Size: 165 Color: 0
Size: 163 Color: 2

Bin 623: 1 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 2
Size: 315 Color: 1

Bin 624: 1 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 3
Size: 314 Color: 2

Bin 625: 1 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 1
Size: 312 Color: 0

Bin 626: 1 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 3
Size: 312 Color: 2

Bin 627: 1 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 308 Color: 3

Bin 628: 1 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 1
Size: 307 Color: 2

Bin 629: 1 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 2
Size: 306 Color: 3

Bin 630: 1 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 3
Size: 305 Color: 2

Bin 631: 1 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 3
Size: 302 Color: 4

Bin 632: 1 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 0
Size: 302 Color: 3

Bin 633: 1 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 295 Color: 2

Bin 634: 1 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 2
Size: 293 Color: 4

Bin 635: 1 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 2
Size: 142 Color: 0
Size: 141 Color: 1

Bin 636: 1 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 0
Size: 284 Color: 1

Bin 637: 1 of cap free
Amount of items: 3
Items: 
Size: 728 Color: 2
Size: 136 Color: 1
Size: 136 Color: 0

Bin 638: 1 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 2
Size: 268 Color: 0

Bin 639: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 4
Size: 264 Color: 2

Bin 640: 1 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 3
Size: 261 Color: 2

Bin 641: 1 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 1
Size: 261 Color: 2

Bin 642: 1 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 0
Size: 259 Color: 3

Bin 643: 1 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 2
Size: 259 Color: 0

Bin 644: 1 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 3
Size: 256 Color: 1

Bin 645: 1 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 3
Size: 253 Color: 1

Bin 646: 1 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 251 Color: 3

Bin 647: 1 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 251 Color: 0

Bin 648: 1 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 0
Size: 248 Color: 4

Bin 649: 1 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 246 Color: 0

Bin 650: 1 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 2
Size: 246 Color: 3

Bin 651: 1 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 3
Size: 244 Color: 4

Bin 652: 1 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 238 Color: 2

Bin 653: 1 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 4
Size: 121 Color: 2
Size: 114 Color: 1

Bin 654: 1 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 1
Size: 233 Color: 4

Bin 655: 1 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 4
Size: 233 Color: 3

Bin 656: 1 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 229 Color: 2

Bin 657: 1 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 3
Size: 225 Color: 4

Bin 658: 1 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 4
Size: 221 Color: 2

Bin 659: 1 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 1
Size: 216 Color: 4

Bin 660: 1 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 2
Size: 110 Color: 4
Size: 104 Color: 0

Bin 661: 1 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 0
Size: 210 Color: 3

Bin 662: 1 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 207 Color: 3

Bin 663: 1 of cap free
Amount of items: 3
Items: 
Size: 793 Color: 4
Size: 106 Color: 2
Size: 101 Color: 0

Bin 664: 1 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 0
Size: 207 Color: 2

Bin 665: 1 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 202 Color: 3

Bin 666: 2 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 1
Size: 336 Color: 3
Size: 323 Color: 4

Bin 667: 2 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 2
Size: 273 Color: 2
Size: 251 Color: 1

Bin 668: 2 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 3
Size: 499 Color: 1

Bin 669: 2 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 4
Size: 493 Color: 0

Bin 670: 2 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 2
Size: 481 Color: 0

Bin 671: 2 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 475 Color: 4

Bin 672: 2 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 475 Color: 1

Bin 673: 2 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 4
Size: 475 Color: 0

Bin 674: 2 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 4
Size: 468 Color: 0

Bin 675: 2 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 2
Size: 467 Color: 4

Bin 676: 2 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 1
Size: 465 Color: 2

Bin 677: 2 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 1
Size: 460 Color: 0

Bin 678: 2 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 2
Size: 461 Color: 4

Bin 679: 2 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 4
Size: 459 Color: 2

Bin 680: 2 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 0
Size: 454 Color: 1

Bin 681: 2 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 2
Size: 452 Color: 3

Bin 682: 2 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 2
Size: 449 Color: 4

Bin 683: 2 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 3
Size: 440 Color: 4

Bin 684: 2 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 1
Size: 438 Color: 0

Bin 685: 2 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 1
Size: 431 Color: 0

Bin 686: 2 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 2
Size: 429 Color: 3

Bin 687: 2 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 429 Color: 4

Bin 688: 2 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 3
Size: 422 Color: 4

Bin 689: 2 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 2
Size: 419 Color: 1

Bin 690: 2 of cap free
Amount of items: 3
Items: 
Size: 583 Color: 0
Size: 209 Color: 4
Size: 207 Color: 3

Bin 691: 2 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 0
Size: 214 Color: 4
Size: 197 Color: 2

Bin 692: 2 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 3
Size: 408 Color: 2

Bin 693: 2 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 2
Size: 403 Color: 0

Bin 694: 2 of cap free
Amount of items: 3
Items: 
Size: 601 Color: 0
Size: 201 Color: 2
Size: 197 Color: 1

Bin 695: 2 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 385 Color: 2

Bin 696: 2 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 1
Size: 370 Color: 0

Bin 697: 2 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 3
Size: 365 Color: 1

Bin 698: 2 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 361 Color: 0

Bin 699: 2 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 0
Size: 352 Color: 2

Bin 700: 2 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 2
Size: 352 Color: 3

Bin 701: 2 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 1
Size: 352 Color: 4

Bin 702: 2 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 1
Size: 337 Color: 4

Bin 703: 2 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 3
Size: 337 Color: 0

Bin 704: 2 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 2
Size: 324 Color: 1

Bin 705: 2 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 2
Size: 315 Color: 3

Bin 706: 2 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 4
Size: 315 Color: 1

Bin 707: 2 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 315 Color: 0

Bin 708: 2 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 2
Size: 305 Color: 0

Bin 709: 2 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 1
Size: 301 Color: 4

Bin 710: 2 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 2
Size: 296 Color: 4

Bin 711: 2 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 2
Size: 292 Color: 1

Bin 712: 2 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 4
Size: 292 Color: 3

Bin 713: 2 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 0
Size: 287 Color: 3

Bin 714: 2 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 244 Color: 2

Bin 715: 2 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 2
Size: 243 Color: 1

Bin 716: 2 of cap free
Amount of items: 3
Items: 
Size: 764 Color: 0
Size: 121 Color: 2
Size: 114 Color: 4

Bin 717: 2 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 4
Size: 220 Color: 3

Bin 718: 2 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 3
Size: 220 Color: 4

Bin 719: 2 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 2
Size: 110 Color: 3
Size: 110 Color: 1

Bin 720: 2 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 3
Size: 203 Color: 1

Bin 721: 3 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 2
Size: 493 Color: 3

Bin 722: 3 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 1
Size: 487 Color: 4

Bin 723: 3 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 2
Size: 486 Color: 0

Bin 724: 3 of cap free
Amount of items: 3
Items: 
Size: 519 Color: 0
Size: 240 Color: 1
Size: 239 Color: 1

Bin 725: 3 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 474 Color: 0

Bin 726: 3 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 1
Size: 400 Color: 2

Bin 727: 3 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 4
Size: 389 Color: 2

Bin 728: 3 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 384 Color: 1

Bin 729: 3 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 1
Size: 378 Color: 0

Bin 730: 3 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 1
Size: 368 Color: 3

Bin 731: 3 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 0
Size: 368 Color: 4

Bin 732: 3 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 2
Size: 346 Color: 3

Bin 733: 3 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 337 Color: 0

Bin 734: 3 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 1
Size: 331 Color: 0

Bin 735: 3 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 4
Size: 329 Color: 1

Bin 736: 3 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 4
Size: 329 Color: 3

Bin 737: 3 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 324 Color: 3

Bin 738: 3 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 0
Size: 310 Color: 1

Bin 739: 3 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 4
Size: 300 Color: 2

Bin 740: 3 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 4
Size: 300 Color: 2

Bin 741: 3 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 296 Color: 4

Bin 742: 3 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 1
Size: 296 Color: 0

Bin 743: 3 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 1
Size: 296 Color: 4

Bin 744: 3 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 1
Size: 295 Color: 2

Bin 745: 3 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 3
Size: 292 Color: 0

Bin 746: 3 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 2
Size: 283 Color: 4

Bin 747: 3 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 1
Size: 266 Color: 3

Bin 748: 3 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 244 Color: 2

Bin 749: 3 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 0
Size: 124 Color: 2
Size: 117 Color: 4

Bin 750: 3 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 219 Color: 3

Bin 751: 4 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 0
Size: 346 Color: 2
Size: 301 Color: 4

Bin 752: 4 of cap free
Amount of items: 2
Items: 
Size: 499 Color: 4
Size: 498 Color: 1

Bin 753: 4 of cap free
Amount of items: 2
Items: 
Size: 499 Color: 2
Size: 498 Color: 0

Bin 754: 4 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 473 Color: 4

Bin 755: 4 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 4
Size: 459 Color: 0

Bin 756: 4 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 3
Size: 459 Color: 4

Bin 757: 4 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 0
Size: 459 Color: 2

Bin 758: 4 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 3
Size: 429 Color: 0

Bin 759: 4 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 2
Size: 428 Color: 1

Bin 760: 4 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 420 Color: 2

Bin 761: 4 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 4
Size: 419 Color: 0

Bin 762: 4 of cap free
Amount of items: 3
Items: 
Size: 600 Color: 0
Size: 200 Color: 1
Size: 197 Color: 2

Bin 763: 4 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 2
Size: 385 Color: 0

Bin 764: 4 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 383 Color: 4

Bin 765: 4 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 383 Color: 3

Bin 766: 4 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 4
Size: 383 Color: 2

Bin 767: 4 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 323 Color: 3

Bin 768: 4 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 3
Size: 324 Color: 1

Bin 769: 4 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 3
Size: 305 Color: 1

Bin 770: 4 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 4
Size: 290 Color: 2

Bin 771: 4 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 0
Size: 261 Color: 3

Bin 772: 4 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 4
Size: 252 Color: 2

Bin 773: 4 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 3
Size: 243 Color: 0

Bin 774: 4 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 243 Color: 2

Bin 775: 4 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 243 Color: 0

Bin 776: 4 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 3
Size: 218 Color: 2

Bin 777: 4 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 3
Size: 201 Color: 2

Bin 778: 5 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 0
Size: 323 Color: 2
Size: 323 Color: 2

Bin 779: 5 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 277 Color: 4
Size: 277 Color: 4

Bin 780: 5 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 0
Size: 487 Color: 3

Bin 781: 5 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 3
Size: 458 Color: 4

Bin 782: 5 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 1
Size: 449 Color: 0

Bin 783: 5 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 3
Size: 449 Color: 0

Bin 784: 5 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 0
Size: 427 Color: 1

Bin 785: 5 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 3
Size: 253 Color: 4

Bin 786: 5 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 252 Color: 2

Bin 787: 5 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 0
Size: 242 Color: 3

Bin 788: 5 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 1
Size: 218 Color: 4

Bin 789: 5 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 1
Size: 200 Color: 3

Bin 790: 6 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 277 Color: 0
Size: 276 Color: 2

Bin 791: 6 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 3
Size: 486 Color: 1

Bin 792: 6 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 0
Size: 451 Color: 1

Bin 793: 6 of cap free
Amount of items: 3
Items: 
Size: 559 Color: 2
Size: 218 Color: 3
Size: 218 Color: 0

Bin 794: 6 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 3
Size: 429 Color: 0

Bin 795: 6 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 0
Size: 427 Color: 1

Bin 796: 6 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 400 Color: 0

Bin 797: 6 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 400 Color: 0

Bin 798: 6 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 2
Size: 396 Color: 1

Bin 799: 6 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 2
Size: 384 Color: 0

Bin 800: 6 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 1
Size: 343 Color: 0

Bin 801: 6 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 1
Size: 305 Color: 3

Bin 802: 6 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 260 Color: 2

Bin 803: 6 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 1
Size: 199 Color: 0

Bin 804: 7 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 3
Size: 490 Color: 0

Bin 805: 7 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 4
Size: 399 Color: 2

Bin 806: 7 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 0
Size: 364 Color: 4

Bin 807: 7 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 1
Size: 359 Color: 0

Bin 808: 7 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 0
Size: 348 Color: 2

Bin 809: 7 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 2
Size: 282 Color: 0

Bin 810: 7 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 3
Size: 262 Color: 0

Bin 811: 7 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 0
Size: 207 Color: 2

Bin 812: 8 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 2
Size: 261 Color: 1
Size: 253 Color: 3

Bin 813: 8 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 4
Size: 384 Color: 0

Bin 814: 8 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 2
Size: 281 Color: 4

Bin 815: 8 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 4
Size: 262 Color: 0

Bin 816: 8 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 4
Size: 241 Color: 0

Bin 817: 9 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 487 Color: 3

Bin 818: 9 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 0
Size: 483 Color: 4

Bin 819: 9 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 4
Size: 447 Color: 3

Bin 820: 9 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 2
Size: 426 Color: 1

Bin 821: 9 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 295 Color: 2

Bin 822: 9 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 2
Size: 280 Color: 3

Bin 823: 9 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 3
Size: 225 Color: 2

Bin 824: 10 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 0
Size: 337 Color: 0
Size: 285 Color: 2

Bin 825: 10 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 305 Color: 1
Size: 287 Color: 2

Bin 826: 10 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 486 Color: 2

Bin 827: 10 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 4
Size: 458 Color: 3

Bin 828: 10 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 425 Color: 2

Bin 829: 10 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 2
Size: 425 Color: 3

Bin 830: 10 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 2
Size: 396 Color: 4

Bin 831: 10 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 1
Size: 382 Color: 4

Bin 832: 10 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 4
Size: 382 Color: 1

Bin 833: 10 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 0
Size: 363 Color: 2

Bin 834: 10 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 280 Color: 0

Bin 835: 10 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 280 Color: 0

Bin 836: 10 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 2
Size: 261 Color: 1

Bin 837: 10 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 241 Color: 2

Bin 838: 11 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 0
Size: 425 Color: 1

Bin 839: 11 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 425 Color: 2

Bin 840: 11 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 4
Size: 413 Color: 0

Bin 841: 11 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 383 Color: 0

Bin 842: 11 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 382 Color: 4

Bin 843: 11 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 4
Size: 311 Color: 2

Bin 844: 11 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 2
Size: 280 Color: 1

Bin 845: 12 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 4
Size: 292 Color: 3

Bin 846: 13 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 2
Size: 395 Color: 4

Bin 847: 13 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 380 Color: 3

Bin 848: 13 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 2
Size: 360 Color: 3

Bin 849: 13 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 2
Size: 305 Color: 0

Bin 850: 14 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 1
Size: 483 Color: 3

Bin 851: 14 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 3
Size: 482 Color: 1

Bin 852: 15 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 0
Size: 480 Color: 4

Bin 853: 15 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 3
Size: 411 Color: 0

Bin 854: 15 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 0
Size: 359 Color: 1

Bin 855: 15 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 0
Size: 359 Color: 2

Bin 856: 15 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 329 Color: 2

Bin 857: 16 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 1
Size: 481 Color: 2

Bin 858: 16 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 0
Size: 480 Color: 4

Bin 859: 16 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 415 Color: 4

Bin 860: 16 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 359 Color: 3

Bin 861: 17 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 480 Color: 4

Bin 862: 17 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 4
Size: 447 Color: 1

Bin 863: 17 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 1
Size: 441 Color: 2

Bin 864: 17 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 4
Size: 441 Color: 2

Bin 865: 18 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 479 Color: 4

Bin 866: 18 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 1
Size: 253 Color: 0

Bin 867: 19 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 0
Size: 394 Color: 4

Bin 868: 20 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 4
Size: 445 Color: 1

Bin 869: 20 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 0
Size: 358 Color: 3

Bin 870: 20 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 1
Size: 325 Color: 2

Bin 871: 21 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 3
Size: 444 Color: 1

Bin 872: 21 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 416 Color: 3

Bin 873: 21 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 4
Size: 358 Color: 3

Bin 874: 22 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 3
Size: 441 Color: 2

Bin 875: 22 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 3
Size: 201 Color: 2

Bin 876: 23 of cap free
Amount of items: 2
Items: 
Size: 498 Color: 4
Size: 480 Color: 1

Bin 877: 23 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 1
Size: 440 Color: 2

Bin 878: 24 of cap free
Amount of items: 2
Items: 
Size: 498 Color: 4
Size: 479 Color: 0

Bin 879: 24 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 413 Color: 3

Bin 880: 24 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 4
Size: 375 Color: 0

Bin 881: 25 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 0
Size: 443 Color: 1

Bin 882: 26 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 4
Size: 438 Color: 2

Bin 883: 26 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 411 Color: 0

Bin 884: 27 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 0
Size: 443 Color: 1

Bin 885: 33 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 3
Size: 375 Color: 0

Bin 886: 34 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 439 Color: 1

Bin 887: 39 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 2
Size: 438 Color: 1

Bin 888: 39 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 1
Size: 394 Color: 4

Bin 889: 44 of cap free
Amount of items: 2
Items: 
Size: 479 Color: 3
Size: 478 Color: 2

Bin 890: 49 of cap free
Amount of items: 2
Items: 
Size: 480 Color: 1
Size: 472 Color: 0

Bin 891: 50 of cap free
Amount of items: 2
Items: 
Size: 478 Color: 3
Size: 473 Color: 2

Bin 892: 50 of cap free
Amount of items: 2
Items: 
Size: 479 Color: 4
Size: 472 Color: 0

Bin 893: 51 of cap free
Amount of items: 2
Items: 
Size: 478 Color: 3
Size: 472 Color: 0

Bin 894: 55 of cap free
Amount of items: 2
Items: 
Size: 478 Color: 4
Size: 468 Color: 0

Bin 895: 55 of cap free
Amount of items: 2
Items: 
Size: 478 Color: 3
Size: 468 Color: 0

Bin 896: 61 of cap free
Amount of items: 2
Items: 
Size: 470 Color: 4
Size: 470 Color: 3

Bin 897: 65 of cap free
Amount of items: 2
Items: 
Size: 471 Color: 4
Size: 465 Color: 0

Bin 898: 66 of cap free
Amount of items: 2
Items: 
Size: 469 Color: 2
Size: 466 Color: 4

Bin 899: 67 of cap free
Amount of items: 2
Items: 
Size: 469 Color: 2
Size: 465 Color: 0

Bin 900: 77 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 1
Size: 358 Color: 4

Bin 901: 78 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 0
Size: 358 Color: 4

Bin 902: 79 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 357 Color: 4

Bin 903: 85 of cap free
Amount of items: 2
Items: 
Size: 458 Color: 3
Size: 458 Color: 0

Bin 904: 86 of cap free
Amount of items: 2
Items: 
Size: 458 Color: 3
Size: 457 Color: 0

Bin 905: 153 of cap free
Amount of items: 2
Items: 
Size: 441 Color: 3
Size: 407 Color: 2

Bin 906: 175 of cap free
Amount of items: 2
Items: 
Size: 469 Color: 2
Size: 357 Color: 1

Bin 907: 189 of cap free
Amount of items: 2
Items: 
Size: 455 Color: 4
Size: 357 Color: 1

Bin 908: 217 of cap free
Amount of items: 2
Items: 
Size: 442 Color: 3
Size: 342 Color: 1

Bin 909: 255 of cap free
Amount of items: 2
Items: 
Size: 394 Color: 3
Size: 352 Color: 0

Bin 910: 261 of cap free
Amount of items: 2
Items: 
Size: 398 Color: 2
Size: 342 Color: 1

Bin 911: 264 of cap free
Amount of items: 2
Items: 
Size: 395 Color: 2
Size: 342 Color: 1

Bin 912: 314 of cap free
Amount of items: 2
Items: 
Size: 350 Color: 0
Size: 337 Color: 1

Bin 913: 331 of cap free
Amount of items: 2
Items: 
Size: 336 Color: 0
Size: 334 Color: 1

Total size: 908904
Total free space: 5009

