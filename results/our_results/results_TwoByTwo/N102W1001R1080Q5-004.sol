Capicity Bin: 1001
Lower Bound: 46

Bins used: 47
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 0
Size: 402 Color: 4
Size: 149 Color: 1

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 435 Color: 4

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 3
Size: 285 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 750 Color: 1
Size: 132 Color: 0
Size: 119 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 1
Size: 131 Color: 0
Size: 112 Color: 2

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 1
Size: 212 Color: 0

Bin 7: 1 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 3
Size: 422 Color: 1

Bin 8: 1 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 2
Size: 404 Color: 1

Bin 9: 1 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 4
Size: 234 Color: 3

Bin 10: 2 of cap free
Amount of items: 3
Items: 
Size: 629 Color: 4
Size: 230 Color: 3
Size: 140 Color: 3

Bin 11: 3 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 0
Size: 215 Color: 4
Size: 198 Color: 2

Bin 12: 3 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 4
Size: 219 Color: 1

Bin 13: 4 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 415 Color: 3
Size: 153 Color: 2

Bin 14: 4 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 465 Color: 1

Bin 15: 4 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 2
Size: 430 Color: 3

Bin 16: 4 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 391 Color: 0

Bin 17: 5 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 4
Size: 312 Color: 3

Bin 18: 5 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 3
Size: 134 Color: 0
Size: 129 Color: 1

Bin 19: 6 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 3
Size: 446 Color: 4

Bin 20: 6 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 409 Color: 0

Bin 21: 6 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 3
Size: 149 Color: 0
Size: 124 Color: 2

Bin 22: 7 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 1
Size: 449 Color: 3

Bin 23: 7 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 0
Size: 385 Color: 3

Bin 24: 7 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 333 Color: 0

Bin 25: 8 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 2
Size: 467 Color: 0

Bin 26: 8 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 3
Size: 267 Color: 2

Bin 27: 9 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 318 Color: 4

Bin 28: 12 of cap free
Amount of items: 3
Items: 
Size: 582 Color: 0
Size: 227 Color: 4
Size: 180 Color: 1

Bin 29: 12 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 4
Size: 347 Color: 0

Bin 30: 14 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 3
Size: 311 Color: 1

Bin 31: 15 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 0
Size: 261 Color: 2

Bin 32: 18 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 3
Size: 286 Color: 2

Bin 33: 20 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 232 Color: 1

Bin 34: 22 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 0
Size: 379 Color: 3

Bin 35: 22 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 2
Size: 252 Color: 0

Bin 36: 28 of cap free
Amount of items: 2
Items: 
Size: 497 Color: 2
Size: 476 Color: 1

Bin 37: 28 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 2
Size: 186 Color: 1

Bin 38: 32 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 1
Size: 391 Color: 0

Bin 39: 32 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 337 Color: 2

Bin 40: 53 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 1
Size: 173 Color: 0

Bin 41: 58 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 0
Size: 428 Color: 4

Bin 42: 58 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 1
Size: 308 Color: 2

Bin 43: 76 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 4
Size: 166 Color: 0

Bin 44: 84 of cap free
Amount of items: 2
Items: 
Size: 491 Color: 0
Size: 426 Color: 4

Bin 45: 103 of cap free
Amount of items: 2
Items: 
Size: 457 Color: 3
Size: 441 Color: 2

Bin 46: 299 of cap free
Amount of items: 2
Items: 
Size: 448 Color: 0
Size: 254 Color: 3

Bin 47: 630 of cap free
Amount of items: 1
Items: 
Size: 371 Color: 4

Total size: 45330
Total free space: 1717

