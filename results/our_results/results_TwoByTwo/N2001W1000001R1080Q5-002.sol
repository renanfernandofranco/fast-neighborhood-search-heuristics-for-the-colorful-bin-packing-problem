Capicity Bin: 1000001
Lower Bound: 902

Bins used: 908
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 561794 Color: 0
Size: 438207 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 628194 Color: 1
Size: 188812 Color: 4
Size: 182995 Color: 3

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 663716 Color: 4
Size: 336285 Color: 2

Bin 4: 1 of cap free
Amount of items: 3
Items: 
Size: 627720 Color: 1
Size: 188170 Color: 2
Size: 184110 Color: 2

Bin 5: 1 of cap free
Amount of items: 3
Items: 
Size: 646151 Color: 2
Size: 177651 Color: 1
Size: 176198 Color: 2

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 738003 Color: 2
Size: 261997 Color: 0

Bin 7: 2 of cap free
Amount of items: 3
Items: 
Size: 675204 Color: 3
Size: 165295 Color: 2
Size: 159500 Color: 1

Bin 8: 2 of cap free
Amount of items: 2
Items: 
Size: 688126 Color: 4
Size: 311873 Color: 2

Bin 9: 2 of cap free
Amount of items: 3
Items: 
Size: 771677 Color: 1
Size: 114831 Color: 3
Size: 113491 Color: 4

Bin 10: 3 of cap free
Amount of items: 3
Items: 
Size: 788985 Color: 0
Size: 105841 Color: 0
Size: 105172 Color: 1

Bin 11: 4 of cap free
Amount of items: 2
Items: 
Size: 583772 Color: 3
Size: 416225 Color: 2

Bin 12: 4 of cap free
Amount of items: 3
Items: 
Size: 657909 Color: 1
Size: 172119 Color: 3
Size: 169969 Color: 4

Bin 13: 4 of cap free
Amount of items: 2
Items: 
Size: 657947 Color: 2
Size: 342050 Color: 3

Bin 14: 4 of cap free
Amount of items: 3
Items: 
Size: 679636 Color: 3
Size: 161015 Color: 0
Size: 159346 Color: 1

Bin 15: 5 of cap free
Amount of items: 3
Items: 
Size: 492647 Color: 2
Size: 254901 Color: 1
Size: 252448 Color: 1

Bin 16: 5 of cap free
Amount of items: 3
Items: 
Size: 763221 Color: 1
Size: 120275 Color: 0
Size: 116500 Color: 3

Bin 17: 6 of cap free
Amount of items: 2
Items: 
Size: 525373 Color: 3
Size: 474622 Color: 4

Bin 18: 8 of cap free
Amount of items: 2
Items: 
Size: 561898 Color: 0
Size: 438095 Color: 2

Bin 19: 8 of cap free
Amount of items: 3
Items: 
Size: 629259 Color: 1
Size: 185386 Color: 0
Size: 185348 Color: 0

Bin 20: 8 of cap free
Amount of items: 2
Items: 
Size: 731703 Color: 3
Size: 268290 Color: 2

Bin 21: 8 of cap free
Amount of items: 3
Items: 
Size: 756854 Color: 2
Size: 126177 Color: 1
Size: 116962 Color: 4

Bin 22: 10 of cap free
Amount of items: 2
Items: 
Size: 624240 Color: 3
Size: 375751 Color: 4

Bin 23: 11 of cap free
Amount of items: 3
Items: 
Size: 766412 Color: 1
Size: 121148 Color: 3
Size: 112430 Color: 3

Bin 24: 12 of cap free
Amount of items: 2
Items: 
Size: 511097 Color: 1
Size: 488892 Color: 2

Bin 25: 14 of cap free
Amount of items: 2
Items: 
Size: 745777 Color: 1
Size: 254210 Color: 2

Bin 26: 15 of cap free
Amount of items: 2
Items: 
Size: 710127 Color: 2
Size: 289859 Color: 3

Bin 27: 16 of cap free
Amount of items: 3
Items: 
Size: 704410 Color: 1
Size: 148213 Color: 0
Size: 147362 Color: 4

Bin 28: 17 of cap free
Amount of items: 2
Items: 
Size: 558607 Color: 0
Size: 441377 Color: 3

Bin 29: 18 of cap free
Amount of items: 3
Items: 
Size: 660494 Color: 3
Size: 171284 Color: 3
Size: 168205 Color: 1

Bin 30: 18 of cap free
Amount of items: 2
Items: 
Size: 797692 Color: 0
Size: 202291 Color: 2

Bin 31: 19 of cap free
Amount of items: 2
Items: 
Size: 594565 Color: 3
Size: 405417 Color: 4

Bin 32: 19 of cap free
Amount of items: 2
Items: 
Size: 611821 Color: 0
Size: 388161 Color: 4

Bin 33: 19 of cap free
Amount of items: 3
Items: 
Size: 657899 Color: 1
Size: 171360 Color: 3
Size: 170723 Color: 4

Bin 34: 20 of cap free
Amount of items: 3
Items: 
Size: 473540 Color: 0
Size: 265048 Color: 3
Size: 261393 Color: 0

Bin 35: 20 of cap free
Amount of items: 2
Items: 
Size: 694378 Color: 2
Size: 305603 Color: 0

Bin 36: 21 of cap free
Amount of items: 2
Items: 
Size: 544220 Color: 1
Size: 455760 Color: 3

Bin 37: 22 of cap free
Amount of items: 2
Items: 
Size: 648020 Color: 2
Size: 351959 Color: 0

Bin 38: 23 of cap free
Amount of items: 3
Items: 
Size: 696998 Color: 4
Size: 152055 Color: 3
Size: 150925 Color: 1

Bin 39: 24 of cap free
Amount of items: 3
Items: 
Size: 461262 Color: 2
Size: 269530 Color: 3
Size: 269185 Color: 4

Bin 40: 27 of cap free
Amount of items: 2
Items: 
Size: 721110 Color: 3
Size: 278864 Color: 2

Bin 41: 28 of cap free
Amount of items: 3
Items: 
Size: 674436 Color: 1
Size: 162803 Color: 2
Size: 162734 Color: 4

Bin 42: 28 of cap free
Amount of items: 2
Items: 
Size: 699675 Color: 3
Size: 300298 Color: 4

Bin 43: 29 of cap free
Amount of items: 2
Items: 
Size: 587081 Color: 1
Size: 412891 Color: 2

Bin 44: 29 of cap free
Amount of items: 3
Items: 
Size: 651387 Color: 1
Size: 174300 Color: 0
Size: 174285 Color: 4

Bin 45: 30 of cap free
Amount of items: 2
Items: 
Size: 573446 Color: 1
Size: 426525 Color: 2

Bin 46: 30 of cap free
Amount of items: 2
Items: 
Size: 679612 Color: 1
Size: 320359 Color: 0

Bin 47: 31 of cap free
Amount of items: 2
Items: 
Size: 774576 Color: 4
Size: 225394 Color: 3

Bin 48: 31 of cap free
Amount of items: 2
Items: 
Size: 799114 Color: 4
Size: 200856 Color: 3

Bin 49: 32 of cap free
Amount of items: 3
Items: 
Size: 641320 Color: 1
Size: 179786 Color: 4
Size: 178863 Color: 1

Bin 50: 32 of cap free
Amount of items: 3
Items: 
Size: 649619 Color: 0
Size: 176367 Color: 1
Size: 173983 Color: 3

Bin 51: 32 of cap free
Amount of items: 3
Items: 
Size: 765208 Color: 1
Size: 122182 Color: 3
Size: 112579 Color: 4

Bin 52: 33 of cap free
Amount of items: 3
Items: 
Size: 633325 Color: 1
Size: 183431 Color: 0
Size: 183212 Color: 0

Bin 53: 33 of cap free
Amount of items: 3
Items: 
Size: 674515 Color: 2
Size: 162923 Color: 3
Size: 162530 Color: 1

Bin 54: 33 of cap free
Amount of items: 3
Items: 
Size: 721355 Color: 1
Size: 139577 Color: 2
Size: 139036 Color: 3

Bin 55: 35 of cap free
Amount of items: 3
Items: 
Size: 689412 Color: 1
Size: 156460 Color: 0
Size: 154094 Color: 0

Bin 56: 36 of cap free
Amount of items: 2
Items: 
Size: 641900 Color: 3
Size: 358065 Color: 4

Bin 57: 36 of cap free
Amount of items: 2
Items: 
Size: 680501 Color: 4
Size: 319464 Color: 2

Bin 58: 37 of cap free
Amount of items: 2
Items: 
Size: 592926 Color: 2
Size: 407038 Color: 1

Bin 59: 38 of cap free
Amount of items: 2
Items: 
Size: 515440 Color: 2
Size: 484523 Color: 1

Bin 60: 38 of cap free
Amount of items: 2
Items: 
Size: 566756 Color: 4
Size: 433207 Color: 0

Bin 61: 38 of cap free
Amount of items: 3
Items: 
Size: 659576 Color: 1
Size: 171587 Color: 4
Size: 168800 Color: 2

Bin 62: 38 of cap free
Amount of items: 2
Items: 
Size: 694163 Color: 2
Size: 305800 Color: 4

Bin 63: 38 of cap free
Amount of items: 2
Items: 
Size: 780316 Color: 0
Size: 219647 Color: 4

Bin 64: 38 of cap free
Amount of items: 3
Items: 
Size: 786069 Color: 1
Size: 107762 Color: 0
Size: 106132 Color: 4

Bin 65: 40 of cap free
Amount of items: 2
Items: 
Size: 586627 Color: 0
Size: 413334 Color: 2

Bin 66: 40 of cap free
Amount of items: 3
Items: 
Size: 781950 Color: 1
Size: 109858 Color: 0
Size: 108153 Color: 3

Bin 67: 42 of cap free
Amount of items: 3
Items: 
Size: 558176 Color: 0
Size: 221001 Color: 2
Size: 220782 Color: 4

Bin 68: 43 of cap free
Amount of items: 2
Items: 
Size: 519725 Color: 1
Size: 480233 Color: 4

Bin 69: 44 of cap free
Amount of items: 2
Items: 
Size: 622453 Color: 0
Size: 377504 Color: 4

Bin 70: 44 of cap free
Amount of items: 2
Items: 
Size: 757943 Color: 2
Size: 242014 Color: 4

Bin 71: 45 of cap free
Amount of items: 2
Items: 
Size: 534608 Color: 4
Size: 465348 Color: 2

Bin 72: 45 of cap free
Amount of items: 3
Items: 
Size: 774129 Color: 4
Size: 117591 Color: 1
Size: 108236 Color: 2

Bin 73: 46 of cap free
Amount of items: 3
Items: 
Size: 676674 Color: 2
Size: 164093 Color: 0
Size: 159188 Color: 1

Bin 74: 46 of cap free
Amount of items: 2
Items: 
Size: 781641 Color: 3
Size: 218314 Color: 1

Bin 75: 46 of cap free
Amount of items: 3
Items: 
Size: 790988 Color: 1
Size: 105675 Color: 0
Size: 103292 Color: 0

Bin 76: 47 of cap free
Amount of items: 2
Items: 
Size: 695248 Color: 4
Size: 304706 Color: 2

Bin 77: 48 of cap free
Amount of items: 3
Items: 
Size: 708038 Color: 1
Size: 147626 Color: 3
Size: 144289 Color: 4

Bin 78: 48 of cap free
Amount of items: 3
Items: 
Size: 765820 Color: 1
Size: 122053 Color: 3
Size: 112080 Color: 4

Bin 79: 49 of cap free
Amount of items: 2
Items: 
Size: 628822 Color: 4
Size: 371130 Color: 3

Bin 80: 49 of cap free
Amount of items: 3
Items: 
Size: 630445 Color: 4
Size: 186565 Color: 1
Size: 182942 Color: 0

Bin 81: 51 of cap free
Amount of items: 2
Items: 
Size: 582380 Color: 1
Size: 417570 Color: 3

Bin 82: 51 of cap free
Amount of items: 3
Items: 
Size: 628198 Color: 1
Size: 188371 Color: 3
Size: 183381 Color: 4

Bin 83: 51 of cap free
Amount of items: 2
Items: 
Size: 797979 Color: 0
Size: 201971 Color: 4

Bin 84: 53 of cap free
Amount of items: 3
Items: 
Size: 623332 Color: 0
Size: 192330 Color: 1
Size: 184286 Color: 0

Bin 85: 53 of cap free
Amount of items: 3
Items: 
Size: 787993 Color: 4
Size: 107521 Color: 1
Size: 104434 Color: 3

Bin 86: 55 of cap free
Amount of items: 3
Items: 
Size: 624964 Color: 1
Size: 192198 Color: 3
Size: 182784 Color: 3

Bin 87: 56 of cap free
Amount of items: 2
Items: 
Size: 633243 Color: 2
Size: 366702 Color: 0

Bin 88: 56 of cap free
Amount of items: 2
Items: 
Size: 656231 Color: 0
Size: 343714 Color: 4

Bin 89: 57 of cap free
Amount of items: 3
Items: 
Size: 685611 Color: 0
Size: 157222 Color: 2
Size: 157111 Color: 4

Bin 90: 57 of cap free
Amount of items: 3
Items: 
Size: 692993 Color: 1
Size: 153824 Color: 4
Size: 153127 Color: 3

Bin 91: 57 of cap free
Amount of items: 2
Items: 
Size: 724623 Color: 4
Size: 275321 Color: 3

Bin 92: 58 of cap free
Amount of items: 2
Items: 
Size: 512909 Color: 1
Size: 487034 Color: 3

Bin 93: 58 of cap free
Amount of items: 2
Items: 
Size: 651443 Color: 3
Size: 348500 Color: 2

Bin 94: 58 of cap free
Amount of items: 2
Items: 
Size: 703390 Color: 2
Size: 296553 Color: 4

Bin 95: 58 of cap free
Amount of items: 2
Items: 
Size: 720055 Color: 2
Size: 279888 Color: 4

Bin 96: 59 of cap free
Amount of items: 3
Items: 
Size: 712446 Color: 4
Size: 145432 Color: 1
Size: 142064 Color: 2

Bin 97: 59 of cap free
Amount of items: 2
Items: 
Size: 784455 Color: 2
Size: 215487 Color: 3

Bin 98: 62 of cap free
Amount of items: 2
Items: 
Size: 797190 Color: 2
Size: 202749 Color: 0

Bin 99: 63 of cap free
Amount of items: 2
Items: 
Size: 771488 Color: 3
Size: 228450 Color: 4

Bin 100: 64 of cap free
Amount of items: 2
Items: 
Size: 500599 Color: 2
Size: 499338 Color: 3

Bin 101: 64 of cap free
Amount of items: 2
Items: 
Size: 605404 Color: 4
Size: 394533 Color: 2

Bin 102: 64 of cap free
Amount of items: 2
Items: 
Size: 615701 Color: 3
Size: 384236 Color: 1

Bin 103: 65 of cap free
Amount of items: 2
Items: 
Size: 604121 Color: 3
Size: 395815 Color: 0

Bin 104: 69 of cap free
Amount of items: 2
Items: 
Size: 591233 Color: 3
Size: 408699 Color: 1

Bin 105: 70 of cap free
Amount of items: 2
Items: 
Size: 525328 Color: 3
Size: 474603 Color: 4

Bin 106: 71 of cap free
Amount of items: 2
Items: 
Size: 646975 Color: 3
Size: 352955 Color: 2

Bin 107: 71 of cap free
Amount of items: 3
Items: 
Size: 782187 Color: 0
Size: 109933 Color: 1
Size: 107810 Color: 4

Bin 108: 73 of cap free
Amount of items: 3
Items: 
Size: 726828 Color: 1
Size: 136986 Color: 2
Size: 136114 Color: 2

Bin 109: 74 of cap free
Amount of items: 3
Items: 
Size: 629503 Color: 4
Size: 189267 Color: 1
Size: 181157 Color: 2

Bin 110: 76 of cap free
Amount of items: 2
Items: 
Size: 690688 Color: 3
Size: 309237 Color: 0

Bin 111: 77 of cap free
Amount of items: 3
Items: 
Size: 626310 Color: 1
Size: 187056 Color: 3
Size: 186558 Color: 3

Bin 112: 77 of cap free
Amount of items: 2
Items: 
Size: 684965 Color: 3
Size: 314959 Color: 1

Bin 113: 78 of cap free
Amount of items: 2
Items: 
Size: 578729 Color: 1
Size: 421194 Color: 0

Bin 114: 78 of cap free
Amount of items: 2
Items: 
Size: 664565 Color: 3
Size: 335358 Color: 0

Bin 115: 80 of cap free
Amount of items: 2
Items: 
Size: 732138 Color: 0
Size: 267783 Color: 2

Bin 116: 82 of cap free
Amount of items: 2
Items: 
Size: 718097 Color: 2
Size: 281822 Color: 4

Bin 117: 82 of cap free
Amount of items: 2
Items: 
Size: 735226 Color: 2
Size: 264693 Color: 4

Bin 118: 83 of cap free
Amount of items: 3
Items: 
Size: 642489 Color: 4
Size: 179109 Color: 3
Size: 178320 Color: 4

Bin 119: 84 of cap free
Amount of items: 2
Items: 
Size: 622340 Color: 4
Size: 377577 Color: 3

Bin 120: 84 of cap free
Amount of items: 3
Items: 
Size: 675558 Color: 1
Size: 162623 Color: 2
Size: 161736 Color: 0

Bin 121: 84 of cap free
Amount of items: 2
Items: 
Size: 715789 Color: 4
Size: 284128 Color: 0

Bin 122: 84 of cap free
Amount of items: 2
Items: 
Size: 748358 Color: 1
Size: 251559 Color: 3

Bin 123: 86 of cap free
Amount of items: 3
Items: 
Size: 768069 Color: 0
Size: 119670 Color: 2
Size: 112176 Color: 0

Bin 124: 87 of cap free
Amount of items: 3
Items: 
Size: 702351 Color: 1
Size: 150736 Color: 0
Size: 146827 Color: 2

Bin 125: 87 of cap free
Amount of items: 3
Items: 
Size: 722538 Color: 2
Size: 138809 Color: 1
Size: 138567 Color: 2

Bin 126: 88 of cap free
Amount of items: 2
Items: 
Size: 559557 Color: 0
Size: 440356 Color: 2

Bin 127: 90 of cap free
Amount of items: 3
Items: 
Size: 621438 Color: 1
Size: 193597 Color: 2
Size: 184876 Color: 2

Bin 128: 90 of cap free
Amount of items: 2
Items: 
Size: 729730 Color: 2
Size: 270181 Color: 0

Bin 129: 91 of cap free
Amount of items: 3
Items: 
Size: 746926 Color: 0
Size: 127005 Color: 0
Size: 125979 Color: 2

Bin 130: 95 of cap free
Amount of items: 2
Items: 
Size: 782872 Color: 2
Size: 217034 Color: 3

Bin 131: 96 of cap free
Amount of items: 2
Items: 
Size: 560191 Color: 2
Size: 439714 Color: 0

Bin 132: 97 of cap free
Amount of items: 2
Items: 
Size: 676669 Color: 2
Size: 323235 Color: 0

Bin 133: 98 of cap free
Amount of items: 3
Items: 
Size: 651353 Color: 4
Size: 174827 Color: 1
Size: 173723 Color: 2

Bin 134: 99 of cap free
Amount of items: 3
Items: 
Size: 456847 Color: 2
Size: 273439 Color: 4
Size: 269616 Color: 3

Bin 135: 99 of cap free
Amount of items: 2
Items: 
Size: 626792 Color: 0
Size: 373110 Color: 4

Bin 136: 100 of cap free
Amount of items: 3
Items: 
Size: 794127 Color: 3
Size: 103357 Color: 1
Size: 102417 Color: 0

Bin 137: 101 of cap free
Amount of items: 3
Items: 
Size: 790481 Color: 2
Size: 105282 Color: 1
Size: 104137 Color: 3

Bin 138: 102 of cap free
Amount of items: 2
Items: 
Size: 753502 Color: 0
Size: 246397 Color: 4

Bin 139: 102 of cap free
Amount of items: 3
Items: 
Size: 784350 Color: 3
Size: 108046 Color: 3
Size: 107503 Color: 1

Bin 140: 103 of cap free
Amount of items: 2
Items: 
Size: 590877 Color: 2
Size: 409021 Color: 4

Bin 141: 104 of cap free
Amount of items: 2
Items: 
Size: 658944 Color: 0
Size: 340953 Color: 4

Bin 142: 104 of cap free
Amount of items: 2
Items: 
Size: 750839 Color: 1
Size: 249058 Color: 4

Bin 143: 105 of cap free
Amount of items: 3
Items: 
Size: 639241 Color: 0
Size: 180518 Color: 4
Size: 180137 Color: 4

Bin 144: 105 of cap free
Amount of items: 2
Items: 
Size: 644872 Color: 3
Size: 355024 Color: 2

Bin 145: 106 of cap free
Amount of items: 2
Items: 
Size: 609955 Color: 0
Size: 389940 Color: 1

Bin 146: 106 of cap free
Amount of items: 2
Items: 
Size: 707395 Color: 0
Size: 292500 Color: 4

Bin 147: 106 of cap free
Amount of items: 2
Items: 
Size: 791498 Color: 3
Size: 208397 Color: 2

Bin 148: 107 of cap free
Amount of items: 3
Items: 
Size: 728439 Color: 1
Size: 136284 Color: 0
Size: 135171 Color: 0

Bin 149: 108 of cap free
Amount of items: 3
Items: 
Size: 522658 Color: 0
Size: 248115 Color: 1
Size: 229120 Color: 4

Bin 150: 108 of cap free
Amount of items: 2
Items: 
Size: 564424 Color: 2
Size: 435469 Color: 4

Bin 151: 109 of cap free
Amount of items: 2
Items: 
Size: 734433 Color: 4
Size: 265459 Color: 0

Bin 152: 109 of cap free
Amount of items: 2
Items: 
Size: 737597 Color: 1
Size: 262295 Color: 0

Bin 153: 110 of cap free
Amount of items: 3
Items: 
Size: 620718 Color: 1
Size: 190585 Color: 3
Size: 188588 Color: 4

Bin 154: 111 of cap free
Amount of items: 2
Items: 
Size: 591417 Color: 2
Size: 408473 Color: 3

Bin 155: 111 of cap free
Amount of items: 3
Items: 
Size: 708984 Color: 1
Size: 145659 Color: 0
Size: 145247 Color: 0

Bin 156: 112 of cap free
Amount of items: 2
Items: 
Size: 639604 Color: 0
Size: 360285 Color: 2

Bin 157: 113 of cap free
Amount of items: 2
Items: 
Size: 744629 Color: 1
Size: 255259 Color: 3

Bin 158: 114 of cap free
Amount of items: 2
Items: 
Size: 611899 Color: 2
Size: 387988 Color: 0

Bin 159: 116 of cap free
Amount of items: 2
Items: 
Size: 566186 Color: 2
Size: 433699 Color: 4

Bin 160: 117 of cap free
Amount of items: 2
Items: 
Size: 592166 Color: 3
Size: 407718 Color: 2

Bin 161: 118 of cap free
Amount of items: 2
Items: 
Size: 612658 Color: 4
Size: 387225 Color: 2

Bin 162: 119 of cap free
Amount of items: 3
Items: 
Size: 555328 Color: 0
Size: 223129 Color: 2
Size: 221425 Color: 4

Bin 163: 120 of cap free
Amount of items: 3
Items: 
Size: 461605 Color: 2
Size: 270540 Color: 3
Size: 267736 Color: 4

Bin 164: 120 of cap free
Amount of items: 3
Items: 
Size: 548538 Color: 3
Size: 229242 Color: 1
Size: 222101 Color: 2

Bin 165: 120 of cap free
Amount of items: 2
Items: 
Size: 605760 Color: 4
Size: 394121 Color: 1

Bin 166: 120 of cap free
Amount of items: 2
Items: 
Size: 619038 Color: 0
Size: 380843 Color: 2

Bin 167: 121 of cap free
Amount of items: 2
Items: 
Size: 662356 Color: 1
Size: 337524 Color: 4

Bin 168: 122 of cap free
Amount of items: 2
Items: 
Size: 693871 Color: 4
Size: 306008 Color: 3

Bin 169: 122 of cap free
Amount of items: 2
Items: 
Size: 700180 Color: 2
Size: 299699 Color: 0

Bin 170: 123 of cap free
Amount of items: 2
Items: 
Size: 518351 Color: 1
Size: 481527 Color: 3

Bin 171: 123 of cap free
Amount of items: 2
Items: 
Size: 651920 Color: 3
Size: 347958 Color: 4

Bin 172: 123 of cap free
Amount of items: 2
Items: 
Size: 794480 Color: 4
Size: 205398 Color: 2

Bin 173: 124 of cap free
Amount of items: 2
Items: 
Size: 548239 Color: 4
Size: 451638 Color: 0

Bin 174: 124 of cap free
Amount of items: 2
Items: 
Size: 648873 Color: 3
Size: 351004 Color: 1

Bin 175: 126 of cap free
Amount of items: 2
Items: 
Size: 791904 Color: 3
Size: 207971 Color: 4

Bin 176: 127 of cap free
Amount of items: 2
Items: 
Size: 574021 Color: 0
Size: 425853 Color: 1

Bin 177: 128 of cap free
Amount of items: 3
Items: 
Size: 494146 Color: 1
Size: 254175 Color: 2
Size: 251552 Color: 1

Bin 178: 128 of cap free
Amount of items: 2
Items: 
Size: 667153 Color: 0
Size: 332720 Color: 3

Bin 179: 128 of cap free
Amount of items: 3
Items: 
Size: 798238 Color: 1
Size: 100862 Color: 3
Size: 100773 Color: 4

Bin 180: 129 of cap free
Amount of items: 2
Items: 
Size: 613416 Color: 4
Size: 386456 Color: 2

Bin 181: 130 of cap free
Amount of items: 2
Items: 
Size: 616115 Color: 4
Size: 383756 Color: 0

Bin 182: 131 of cap free
Amount of items: 2
Items: 
Size: 669043 Color: 0
Size: 330827 Color: 3

Bin 183: 132 of cap free
Amount of items: 2
Items: 
Size: 635364 Color: 1
Size: 364505 Color: 4

Bin 184: 132 of cap free
Amount of items: 3
Items: 
Size: 780749 Color: 1
Size: 109698 Color: 2
Size: 109422 Color: 0

Bin 185: 133 of cap free
Amount of items: 3
Items: 
Size: 637169 Color: 1
Size: 181709 Color: 0
Size: 180990 Color: 4

Bin 186: 133 of cap free
Amount of items: 2
Items: 
Size: 662717 Color: 1
Size: 337151 Color: 4

Bin 187: 134 of cap free
Amount of items: 2
Items: 
Size: 634863 Color: 2
Size: 365004 Color: 0

Bin 188: 136 of cap free
Amount of items: 3
Items: 
Size: 703168 Color: 2
Size: 150376 Color: 1
Size: 146321 Color: 4

Bin 189: 138 of cap free
Amount of items: 3
Items: 
Size: 727859 Color: 1
Size: 137265 Color: 2
Size: 134739 Color: 0

Bin 190: 139 of cap free
Amount of items: 2
Items: 
Size: 597221 Color: 3
Size: 402641 Color: 1

Bin 191: 139 of cap free
Amount of items: 2
Items: 
Size: 649140 Color: 2
Size: 350722 Color: 1

Bin 192: 140 of cap free
Amount of items: 2
Items: 
Size: 651240 Color: 2
Size: 348621 Color: 0

Bin 193: 141 of cap free
Amount of items: 2
Items: 
Size: 640829 Color: 3
Size: 359031 Color: 0

Bin 194: 141 of cap free
Amount of items: 3
Items: 
Size: 644881 Color: 1
Size: 178779 Color: 3
Size: 176200 Color: 3

Bin 195: 141 of cap free
Amount of items: 3
Items: 
Size: 792671 Color: 1
Size: 104270 Color: 3
Size: 102919 Color: 3

Bin 196: 143 of cap free
Amount of items: 3
Items: 
Size: 690053 Color: 0
Size: 156699 Color: 1
Size: 153106 Color: 3

Bin 197: 144 of cap free
Amount of items: 3
Items: 
Size: 743355 Color: 2
Size: 129700 Color: 2
Size: 126802 Color: 1

Bin 198: 147 of cap free
Amount of items: 3
Items: 
Size: 634921 Color: 0
Size: 184117 Color: 1
Size: 180816 Color: 2

Bin 199: 147 of cap free
Amount of items: 3
Items: 
Size: 711029 Color: 1
Size: 144900 Color: 2
Size: 143925 Color: 3

Bin 200: 152 of cap free
Amount of items: 2
Items: 
Size: 764778 Color: 4
Size: 235071 Color: 2

Bin 201: 156 of cap free
Amount of items: 2
Items: 
Size: 564816 Color: 4
Size: 435029 Color: 0

Bin 202: 157 of cap free
Amount of items: 2
Items: 
Size: 790661 Color: 4
Size: 209183 Color: 0

Bin 203: 158 of cap free
Amount of items: 2
Items: 
Size: 689760 Color: 3
Size: 310083 Color: 0

Bin 204: 160 of cap free
Amount of items: 3
Items: 
Size: 546642 Color: 3
Size: 229364 Color: 4
Size: 223835 Color: 1

Bin 205: 160 of cap free
Amount of items: 2
Items: 
Size: 691382 Color: 2
Size: 308459 Color: 0

Bin 206: 160 of cap free
Amount of items: 3
Items: 
Size: 779943 Color: 4
Size: 111893 Color: 2
Size: 108005 Color: 2

Bin 207: 162 of cap free
Amount of items: 2
Items: 
Size: 515090 Color: 1
Size: 484749 Color: 3

Bin 208: 162 of cap free
Amount of items: 2
Items: 
Size: 786353 Color: 0
Size: 213486 Color: 2

Bin 209: 163 of cap free
Amount of items: 2
Items: 
Size: 582640 Color: 4
Size: 417198 Color: 0

Bin 210: 166 of cap free
Amount of items: 2
Items: 
Size: 711583 Color: 0
Size: 288252 Color: 2

Bin 211: 168 of cap free
Amount of items: 2
Items: 
Size: 746397 Color: 4
Size: 253436 Color: 0

Bin 212: 169 of cap free
Amount of items: 2
Items: 
Size: 796041 Color: 0
Size: 203791 Color: 4

Bin 213: 171 of cap free
Amount of items: 2
Items: 
Size: 593115 Color: 3
Size: 406715 Color: 2

Bin 214: 174 of cap free
Amount of items: 3
Items: 
Size: 716356 Color: 4
Size: 142530 Color: 0
Size: 140941 Color: 2

Bin 215: 176 of cap free
Amount of items: 2
Items: 
Size: 659367 Color: 0
Size: 340458 Color: 1

Bin 216: 177 of cap free
Amount of items: 2
Items: 
Size: 610866 Color: 1
Size: 388958 Color: 3

Bin 217: 177 of cap free
Amount of items: 2
Items: 
Size: 667756 Color: 3
Size: 332068 Color: 4

Bin 218: 178 of cap free
Amount of items: 3
Items: 
Size: 710563 Color: 1
Size: 145852 Color: 4
Size: 143408 Color: 2

Bin 219: 180 of cap free
Amount of items: 3
Items: 
Size: 760642 Color: 1
Size: 122169 Color: 0
Size: 117010 Color: 0

Bin 220: 184 of cap free
Amount of items: 2
Items: 
Size: 570514 Color: 1
Size: 429303 Color: 2

Bin 221: 185 of cap free
Amount of items: 2
Items: 
Size: 712863 Color: 4
Size: 286953 Color: 2

Bin 222: 186 of cap free
Amount of items: 2
Items: 
Size: 568840 Color: 1
Size: 430975 Color: 2

Bin 223: 187 of cap free
Amount of items: 2
Items: 
Size: 645400 Color: 4
Size: 354414 Color: 3

Bin 224: 187 of cap free
Amount of items: 2
Items: 
Size: 754699 Color: 1
Size: 245115 Color: 4

Bin 225: 189 of cap free
Amount of items: 2
Items: 
Size: 565623 Color: 1
Size: 434189 Color: 4

Bin 226: 189 of cap free
Amount of items: 3
Items: 
Size: 684707 Color: 3
Size: 158183 Color: 2
Size: 156922 Color: 4

Bin 227: 191 of cap free
Amount of items: 2
Items: 
Size: 568875 Color: 2
Size: 430935 Color: 4

Bin 228: 192 of cap free
Amount of items: 3
Items: 
Size: 485915 Color: 0
Size: 263849 Color: 3
Size: 250045 Color: 1

Bin 229: 192 of cap free
Amount of items: 2
Items: 
Size: 555321 Color: 1
Size: 444488 Color: 4

Bin 230: 193 of cap free
Amount of items: 2
Items: 
Size: 500424 Color: 3
Size: 499384 Color: 2

Bin 231: 197 of cap free
Amount of items: 2
Items: 
Size: 754923 Color: 4
Size: 244881 Color: 1

Bin 232: 198 of cap free
Amount of items: 2
Items: 
Size: 694130 Color: 3
Size: 305673 Color: 1

Bin 233: 199 of cap free
Amount of items: 2
Items: 
Size: 572598 Color: 1
Size: 427204 Color: 4

Bin 234: 204 of cap free
Amount of items: 2
Items: 
Size: 721360 Color: 0
Size: 278437 Color: 2

Bin 235: 205 of cap free
Amount of items: 2
Items: 
Size: 601502 Color: 3
Size: 398294 Color: 2

Bin 236: 205 of cap free
Amount of items: 2
Items: 
Size: 612906 Color: 1
Size: 386890 Color: 2

Bin 237: 206 of cap free
Amount of items: 2
Items: 
Size: 587236 Color: 4
Size: 412559 Color: 1

Bin 238: 207 of cap free
Amount of items: 2
Items: 
Size: 587751 Color: 0
Size: 412043 Color: 2

Bin 239: 207 of cap free
Amount of items: 3
Items: 
Size: 687237 Color: 1
Size: 157644 Color: 0
Size: 154913 Color: 0

Bin 240: 208 of cap free
Amount of items: 2
Items: 
Size: 519463 Color: 0
Size: 480330 Color: 2

Bin 241: 208 of cap free
Amount of items: 2
Items: 
Size: 722409 Color: 4
Size: 277384 Color: 3

Bin 242: 208 of cap free
Amount of items: 2
Items: 
Size: 755090 Color: 3
Size: 244703 Color: 4

Bin 243: 209 of cap free
Amount of items: 2
Items: 
Size: 774005 Color: 4
Size: 225787 Color: 2

Bin 244: 212 of cap free
Amount of items: 2
Items: 
Size: 683216 Color: 0
Size: 316573 Color: 2

Bin 245: 212 of cap free
Amount of items: 2
Items: 
Size: 740285 Color: 2
Size: 259504 Color: 3

Bin 246: 215 of cap free
Amount of items: 2
Items: 
Size: 584625 Color: 4
Size: 415161 Color: 2

Bin 247: 215 of cap free
Amount of items: 2
Items: 
Size: 779416 Color: 1
Size: 220370 Color: 0

Bin 248: 216 of cap free
Amount of items: 2
Items: 
Size: 700440 Color: 1
Size: 299345 Color: 3

Bin 249: 217 of cap free
Amount of items: 2
Items: 
Size: 696557 Color: 2
Size: 303227 Color: 4

Bin 250: 218 of cap free
Amount of items: 3
Items: 
Size: 789892 Color: 4
Size: 106449 Color: 1
Size: 103442 Color: 2

Bin 251: 220 of cap free
Amount of items: 2
Items: 
Size: 592373 Color: 3
Size: 407408 Color: 2

Bin 252: 221 of cap free
Amount of items: 2
Items: 
Size: 514380 Color: 3
Size: 485400 Color: 1

Bin 253: 221 of cap free
Amount of items: 2
Items: 
Size: 688266 Color: 3
Size: 311514 Color: 1

Bin 254: 223 of cap free
Amount of items: 2
Items: 
Size: 575253 Color: 4
Size: 424525 Color: 1

Bin 255: 225 of cap free
Amount of items: 2
Items: 
Size: 638387 Color: 2
Size: 361389 Color: 3

Bin 256: 225 of cap free
Amount of items: 2
Items: 
Size: 705742 Color: 4
Size: 294034 Color: 1

Bin 257: 226 of cap free
Amount of items: 2
Items: 
Size: 714708 Color: 3
Size: 285067 Color: 2

Bin 258: 227 of cap free
Amount of items: 2
Items: 
Size: 621882 Color: 4
Size: 377892 Color: 2

Bin 259: 227 of cap free
Amount of items: 3
Items: 
Size: 690111 Color: 1
Size: 156031 Color: 3
Size: 153632 Color: 0

Bin 260: 228 of cap free
Amount of items: 2
Items: 
Size: 565085 Color: 4
Size: 434688 Color: 3

Bin 261: 228 of cap free
Amount of items: 2
Items: 
Size: 661239 Color: 4
Size: 338534 Color: 0

Bin 262: 234 of cap free
Amount of items: 2
Items: 
Size: 541429 Color: 2
Size: 458338 Color: 0

Bin 263: 235 of cap free
Amount of items: 2
Items: 
Size: 681393 Color: 3
Size: 318373 Color: 0

Bin 264: 235 of cap free
Amount of items: 3
Items: 
Size: 723133 Color: 1
Size: 141106 Color: 4
Size: 135527 Color: 2

Bin 265: 237 of cap free
Amount of items: 2
Items: 
Size: 633156 Color: 2
Size: 366608 Color: 4

Bin 266: 238 of cap free
Amount of items: 2
Items: 
Size: 547187 Color: 3
Size: 452576 Color: 4

Bin 267: 238 of cap free
Amount of items: 3
Items: 
Size: 689449 Color: 3
Size: 157225 Color: 1
Size: 153089 Color: 2

Bin 268: 241 of cap free
Amount of items: 2
Items: 
Size: 553130 Color: 3
Size: 446630 Color: 2

Bin 269: 242 of cap free
Amount of items: 2
Items: 
Size: 558961 Color: 4
Size: 440798 Color: 3

Bin 270: 243 of cap free
Amount of items: 2
Items: 
Size: 688463 Color: 3
Size: 311295 Color: 4

Bin 271: 245 of cap free
Amount of items: 3
Items: 
Size: 666793 Color: 3
Size: 168936 Color: 3
Size: 164027 Color: 1

Bin 272: 246 of cap free
Amount of items: 2
Items: 
Size: 731992 Color: 0
Size: 267763 Color: 3

Bin 273: 249 of cap free
Amount of items: 3
Items: 
Size: 517565 Color: 0
Size: 246504 Color: 1
Size: 235683 Color: 4

Bin 274: 249 of cap free
Amount of items: 3
Items: 
Size: 622844 Color: 0
Size: 193396 Color: 1
Size: 183512 Color: 4

Bin 275: 250 of cap free
Amount of items: 2
Items: 
Size: 654296 Color: 0
Size: 345455 Color: 3

Bin 276: 252 of cap free
Amount of items: 2
Items: 
Size: 558460 Color: 0
Size: 441289 Color: 3

Bin 277: 254 of cap free
Amount of items: 2
Items: 
Size: 522817 Color: 4
Size: 476930 Color: 3

Bin 278: 254 of cap free
Amount of items: 3
Items: 
Size: 649024 Color: 2
Size: 176471 Color: 2
Size: 174252 Color: 4

Bin 279: 257 of cap free
Amount of items: 3
Items: 
Size: 723938 Color: 3
Size: 139552 Color: 1
Size: 136254 Color: 2

Bin 280: 258 of cap free
Amount of items: 3
Items: 
Size: 538923 Color: 4
Size: 230985 Color: 3
Size: 229835 Color: 3

Bin 281: 258 of cap free
Amount of items: 3
Items: 
Size: 715457 Color: 4
Size: 143346 Color: 1
Size: 140940 Color: 2

Bin 282: 259 of cap free
Amount of items: 2
Items: 
Size: 552589 Color: 4
Size: 447153 Color: 1

Bin 283: 259 of cap free
Amount of items: 3
Items: 
Size: 726913 Color: 1
Size: 140485 Color: 3
Size: 132344 Color: 4

Bin 284: 261 of cap free
Amount of items: 2
Items: 
Size: 609943 Color: 2
Size: 389797 Color: 4

Bin 285: 262 of cap free
Amount of items: 3
Items: 
Size: 620590 Color: 1
Size: 193831 Color: 3
Size: 185318 Color: 3

Bin 286: 262 of cap free
Amount of items: 3
Items: 
Size: 657112 Color: 1
Size: 173156 Color: 0
Size: 169471 Color: 3

Bin 287: 263 of cap free
Amount of items: 2
Items: 
Size: 570565 Color: 0
Size: 429173 Color: 2

Bin 288: 264 of cap free
Amount of items: 2
Items: 
Size: 566374 Color: 3
Size: 433363 Color: 1

Bin 289: 264 of cap free
Amount of items: 2
Items: 
Size: 781595 Color: 0
Size: 218142 Color: 4

Bin 290: 266 of cap free
Amount of items: 2
Items: 
Size: 626230 Color: 4
Size: 373505 Color: 2

Bin 291: 266 of cap free
Amount of items: 3
Items: 
Size: 715394 Color: 1
Size: 142988 Color: 0
Size: 141353 Color: 2

Bin 292: 267 of cap free
Amount of items: 3
Items: 
Size: 483292 Color: 0
Size: 265756 Color: 3
Size: 250686 Color: 1

Bin 293: 269 of cap free
Amount of items: 2
Items: 
Size: 573628 Color: 2
Size: 426104 Color: 1

Bin 294: 269 of cap free
Amount of items: 3
Items: 
Size: 664843 Color: 1
Size: 168197 Color: 3
Size: 166692 Color: 0

Bin 295: 269 of cap free
Amount of items: 2
Items: 
Size: 767613 Color: 4
Size: 232119 Color: 2

Bin 296: 271 of cap free
Amount of items: 3
Items: 
Size: 754848 Color: 1
Size: 123018 Color: 4
Size: 121864 Color: 3

Bin 297: 273 of cap free
Amount of items: 2
Items: 
Size: 752763 Color: 4
Size: 246965 Color: 3

Bin 298: 274 of cap free
Amount of items: 3
Items: 
Size: 463007 Color: 4
Size: 269787 Color: 3
Size: 266933 Color: 0

Bin 299: 274 of cap free
Amount of items: 3
Items: 
Size: 465853 Color: 0
Size: 272872 Color: 3
Size: 261002 Color: 0

Bin 300: 277 of cap free
Amount of items: 3
Items: 
Size: 796253 Color: 0
Size: 103033 Color: 1
Size: 100438 Color: 4

Bin 301: 281 of cap free
Amount of items: 3
Items: 
Size: 735778 Color: 1
Size: 133788 Color: 2
Size: 130154 Color: 2

Bin 302: 282 of cap free
Amount of items: 2
Items: 
Size: 705631 Color: 4
Size: 294088 Color: 3

Bin 303: 287 of cap free
Amount of items: 3
Items: 
Size: 648958 Color: 1
Size: 175404 Color: 2
Size: 175352 Color: 0

Bin 304: 287 of cap free
Amount of items: 2
Items: 
Size: 686446 Color: 4
Size: 313268 Color: 3

Bin 305: 287 of cap free
Amount of items: 3
Items: 
Size: 771665 Color: 2
Size: 116523 Color: 1
Size: 111526 Color: 2

Bin 306: 288 of cap free
Amount of items: 2
Items: 
Size: 509284 Color: 4
Size: 490429 Color: 3

Bin 307: 288 of cap free
Amount of items: 2
Items: 
Size: 598309 Color: 3
Size: 401404 Color: 0

Bin 308: 289 of cap free
Amount of items: 3
Items: 
Size: 629461 Color: 4
Size: 188182 Color: 1
Size: 182069 Color: 2

Bin 309: 290 of cap free
Amount of items: 3
Items: 
Size: 769559 Color: 1
Size: 116167 Color: 0
Size: 113985 Color: 3

Bin 310: 290 of cap free
Amount of items: 3
Items: 
Size: 789584 Color: 2
Size: 105412 Color: 1
Size: 104715 Color: 4

Bin 311: 293 of cap free
Amount of items: 2
Items: 
Size: 691653 Color: 3
Size: 308055 Color: 4

Bin 312: 294 of cap free
Amount of items: 3
Items: 
Size: 663432 Color: 3
Size: 168393 Color: 1
Size: 167882 Color: 3

Bin 313: 302 of cap free
Amount of items: 2
Items: 
Size: 594594 Color: 1
Size: 405105 Color: 3

Bin 314: 303 of cap free
Amount of items: 3
Items: 
Size: 650905 Color: 3
Size: 175035 Color: 2
Size: 173758 Color: 1

Bin 315: 303 of cap free
Amount of items: 2
Items: 
Size: 691501 Color: 3
Size: 308197 Color: 1

Bin 316: 303 of cap free
Amount of items: 2
Items: 
Size: 738170 Color: 0
Size: 261528 Color: 1

Bin 317: 309 of cap free
Amount of items: 2
Items: 
Size: 656375 Color: 3
Size: 343317 Color: 4

Bin 318: 309 of cap free
Amount of items: 2
Items: 
Size: 674586 Color: 2
Size: 325106 Color: 3

Bin 319: 309 of cap free
Amount of items: 2
Items: 
Size: 772766 Color: 3
Size: 226926 Color: 4

Bin 320: 312 of cap free
Amount of items: 3
Items: 
Size: 405606 Color: 2
Size: 321624 Color: 1
Size: 272459 Color: 0

Bin 321: 313 of cap free
Amount of items: 2
Items: 
Size: 670204 Color: 3
Size: 329484 Color: 4

Bin 322: 315 of cap free
Amount of items: 2
Items: 
Size: 530366 Color: 4
Size: 469320 Color: 1

Bin 323: 315 of cap free
Amount of items: 2
Items: 
Size: 609535 Color: 1
Size: 390151 Color: 3

Bin 324: 316 of cap free
Amount of items: 2
Items: 
Size: 506110 Color: 2
Size: 493575 Color: 4

Bin 325: 316 of cap free
Amount of items: 3
Items: 
Size: 571226 Color: 0
Size: 221312 Color: 0
Size: 207147 Color: 4

Bin 326: 317 of cap free
Amount of items: 3
Items: 
Size: 741023 Color: 3
Size: 130520 Color: 1
Size: 128141 Color: 2

Bin 327: 321 of cap free
Amount of items: 3
Items: 
Size: 531059 Color: 0
Size: 245899 Color: 1
Size: 222722 Color: 2

Bin 328: 322 of cap free
Amount of items: 3
Items: 
Size: 668186 Color: 0
Size: 165966 Color: 2
Size: 165527 Color: 0

Bin 329: 322 of cap free
Amount of items: 2
Items: 
Size: 726169 Color: 0
Size: 273510 Color: 2

Bin 330: 323 of cap free
Amount of items: 2
Items: 
Size: 785139 Color: 1
Size: 214539 Color: 0

Bin 331: 325 of cap free
Amount of items: 2
Items: 
Size: 634072 Color: 4
Size: 365604 Color: 2

Bin 332: 326 of cap free
Amount of items: 3
Items: 
Size: 672671 Color: 4
Size: 163606 Color: 3
Size: 163398 Color: 1

Bin 333: 326 of cap free
Amount of items: 2
Items: 
Size: 714312 Color: 3
Size: 285363 Color: 2

Bin 334: 327 of cap free
Amount of items: 3
Items: 
Size: 617664 Color: 4
Size: 192483 Color: 2
Size: 189527 Color: 2

Bin 335: 327 of cap free
Amount of items: 3
Items: 
Size: 673988 Color: 2
Size: 163959 Color: 3
Size: 161727 Color: 1

Bin 336: 327 of cap free
Amount of items: 2
Items: 
Size: 756704 Color: 3
Size: 242970 Color: 4

Bin 337: 328 of cap free
Amount of items: 2
Items: 
Size: 581008 Color: 2
Size: 418665 Color: 4

Bin 338: 329 of cap free
Amount of items: 3
Items: 
Size: 739180 Color: 2
Size: 130964 Color: 4
Size: 129528 Color: 4

Bin 339: 330 of cap free
Amount of items: 2
Items: 
Size: 643731 Color: 2
Size: 355940 Color: 3

Bin 340: 330 of cap free
Amount of items: 2
Items: 
Size: 725154 Color: 4
Size: 274517 Color: 0

Bin 341: 332 of cap free
Amount of items: 2
Items: 
Size: 657251 Color: 2
Size: 342418 Color: 3

Bin 342: 333 of cap free
Amount of items: 3
Items: 
Size: 608608 Color: 1
Size: 195777 Color: 4
Size: 195283 Color: 0

Bin 343: 334 of cap free
Amount of items: 2
Items: 
Size: 710035 Color: 0
Size: 289632 Color: 2

Bin 344: 335 of cap free
Amount of items: 2
Items: 
Size: 627294 Color: 4
Size: 372372 Color: 3

Bin 345: 336 of cap free
Amount of items: 2
Items: 
Size: 575560 Color: 1
Size: 424105 Color: 0

Bin 346: 337 of cap free
Amount of items: 3
Items: 
Size: 526761 Color: 3
Size: 236547 Color: 0
Size: 236356 Color: 4

Bin 347: 343 of cap free
Amount of items: 2
Items: 
Size: 728235 Color: 3
Size: 271423 Color: 4

Bin 348: 347 of cap free
Amount of items: 2
Items: 
Size: 674201 Color: 0
Size: 325453 Color: 3

Bin 349: 348 of cap free
Amount of items: 2
Items: 
Size: 699689 Color: 4
Size: 299964 Color: 2

Bin 350: 348 of cap free
Amount of items: 2
Items: 
Size: 788505 Color: 4
Size: 211148 Color: 0

Bin 351: 351 of cap free
Amount of items: 2
Items: 
Size: 689194 Color: 1
Size: 310456 Color: 4

Bin 352: 352 of cap free
Amount of items: 2
Items: 
Size: 545014 Color: 4
Size: 454635 Color: 0

Bin 353: 353 of cap free
Amount of items: 2
Items: 
Size: 521062 Color: 1
Size: 478586 Color: 2

Bin 354: 358 of cap free
Amount of items: 2
Items: 
Size: 738226 Color: 4
Size: 261417 Color: 3

Bin 355: 359 of cap free
Amount of items: 3
Items: 
Size: 628783 Color: 0
Size: 189362 Color: 1
Size: 181497 Color: 3

Bin 356: 359 of cap free
Amount of items: 3
Items: 
Size: 726991 Color: 2
Size: 137997 Color: 1
Size: 134654 Color: 0

Bin 357: 360 of cap free
Amount of items: 2
Items: 
Size: 693537 Color: 4
Size: 306104 Color: 1

Bin 358: 360 of cap free
Amount of items: 2
Items: 
Size: 701350 Color: 1
Size: 298291 Color: 2

Bin 359: 361 of cap free
Amount of items: 2
Items: 
Size: 760553 Color: 4
Size: 239087 Color: 0

Bin 360: 367 of cap free
Amount of items: 3
Items: 
Size: 775254 Color: 2
Size: 116127 Color: 1
Size: 108253 Color: 3

Bin 361: 368 of cap free
Amount of items: 2
Items: 
Size: 777508 Color: 1
Size: 222125 Color: 0

Bin 362: 369 of cap free
Amount of items: 2
Items: 
Size: 684009 Color: 1
Size: 315623 Color: 2

Bin 363: 373 of cap free
Amount of items: 2
Items: 
Size: 798694 Color: 0
Size: 200934 Color: 4

Bin 364: 379 of cap free
Amount of items: 2
Items: 
Size: 700368 Color: 1
Size: 299254 Color: 3

Bin 365: 380 of cap free
Amount of items: 2
Items: 
Size: 798388 Color: 4
Size: 201233 Color: 0

Bin 366: 388 of cap free
Amount of items: 2
Items: 
Size: 714948 Color: 3
Size: 284665 Color: 2

Bin 367: 390 of cap free
Amount of items: 3
Items: 
Size: 762757 Color: 1
Size: 125355 Color: 3
Size: 111499 Color: 3

Bin 368: 391 of cap free
Amount of items: 2
Items: 
Size: 642523 Color: 0
Size: 357087 Color: 3

Bin 369: 391 of cap free
Amount of items: 2
Items: 
Size: 659290 Color: 4
Size: 340320 Color: 2

Bin 370: 393 of cap free
Amount of items: 2
Items: 
Size: 510232 Color: 4
Size: 489376 Color: 0

Bin 371: 395 of cap free
Amount of items: 2
Items: 
Size: 650145 Color: 2
Size: 349461 Color: 4

Bin 372: 396 of cap free
Amount of items: 2
Items: 
Size: 614702 Color: 4
Size: 384903 Color: 3

Bin 373: 398 of cap free
Amount of items: 2
Items: 
Size: 716356 Color: 0
Size: 283247 Color: 4

Bin 374: 400 of cap free
Amount of items: 2
Items: 
Size: 585577 Color: 4
Size: 414024 Color: 3

Bin 375: 403 of cap free
Amount of items: 2
Items: 
Size: 765481 Color: 1
Size: 234117 Color: 3

Bin 376: 406 of cap free
Amount of items: 2
Items: 
Size: 559497 Color: 1
Size: 440098 Color: 0

Bin 377: 408 of cap free
Amount of items: 2
Items: 
Size: 593388 Color: 1
Size: 406205 Color: 0

Bin 378: 410 of cap free
Amount of items: 2
Items: 
Size: 585271 Color: 2
Size: 414320 Color: 0

Bin 379: 410 of cap free
Amount of items: 2
Items: 
Size: 598943 Color: 4
Size: 400648 Color: 0

Bin 380: 413 of cap free
Amount of items: 2
Items: 
Size: 581458 Color: 3
Size: 418130 Color: 4

Bin 381: 415 of cap free
Amount of items: 2
Items: 
Size: 665618 Color: 3
Size: 333968 Color: 1

Bin 382: 420 of cap free
Amount of items: 2
Items: 
Size: 623315 Color: 4
Size: 376266 Color: 0

Bin 383: 422 of cap free
Amount of items: 3
Items: 
Size: 623690 Color: 0
Size: 189227 Color: 1
Size: 186662 Color: 2

Bin 384: 423 of cap free
Amount of items: 2
Items: 
Size: 551818 Color: 4
Size: 447760 Color: 0

Bin 385: 424 of cap free
Amount of items: 3
Items: 
Size: 537257 Color: 2
Size: 237535 Color: 0
Size: 224785 Color: 2

Bin 386: 425 of cap free
Amount of items: 2
Items: 
Size: 710633 Color: 4
Size: 288943 Color: 3

Bin 387: 428 of cap free
Amount of items: 2
Items: 
Size: 736442 Color: 4
Size: 263131 Color: 0

Bin 388: 429 of cap free
Amount of items: 2
Items: 
Size: 513704 Color: 2
Size: 485868 Color: 3

Bin 389: 429 of cap free
Amount of items: 2
Items: 
Size: 662918 Color: 0
Size: 336654 Color: 2

Bin 390: 434 of cap free
Amount of items: 2
Items: 
Size: 608445 Color: 2
Size: 391122 Color: 0

Bin 391: 434 of cap free
Amount of items: 2
Items: 
Size: 677326 Color: 2
Size: 322241 Color: 3

Bin 392: 434 of cap free
Amount of items: 2
Items: 
Size: 785529 Color: 2
Size: 214038 Color: 3

Bin 393: 435 of cap free
Amount of items: 2
Items: 
Size: 558202 Color: 3
Size: 441364 Color: 2

Bin 394: 436 of cap free
Amount of items: 2
Items: 
Size: 727166 Color: 0
Size: 272399 Color: 2

Bin 395: 440 of cap free
Amount of items: 2
Items: 
Size: 735621 Color: 1
Size: 263940 Color: 4

Bin 396: 442 of cap free
Amount of items: 2
Items: 
Size: 753983 Color: 2
Size: 245576 Color: 4

Bin 397: 444 of cap free
Amount of items: 3
Items: 
Size: 602378 Color: 1
Size: 204877 Color: 4
Size: 192302 Color: 0

Bin 398: 445 of cap free
Amount of items: 2
Items: 
Size: 555282 Color: 1
Size: 444274 Color: 3

Bin 399: 446 of cap free
Amount of items: 2
Items: 
Size: 742786 Color: 2
Size: 256769 Color: 4

Bin 400: 451 of cap free
Amount of items: 2
Items: 
Size: 757963 Color: 4
Size: 241587 Color: 1

Bin 401: 451 of cap free
Amount of items: 3
Items: 
Size: 786858 Color: 3
Size: 109463 Color: 1
Size: 103229 Color: 0

Bin 402: 452 of cap free
Amount of items: 2
Items: 
Size: 554129 Color: 2
Size: 445420 Color: 4

Bin 403: 456 of cap free
Amount of items: 3
Items: 
Size: 619731 Color: 1
Size: 192566 Color: 4
Size: 187248 Color: 4

Bin 404: 457 of cap free
Amount of items: 3
Items: 
Size: 533465 Color: 2
Size: 235542 Color: 0
Size: 230537 Color: 4

Bin 405: 457 of cap free
Amount of items: 2
Items: 
Size: 636742 Color: 4
Size: 362802 Color: 3

Bin 406: 458 of cap free
Amount of items: 2
Items: 
Size: 552405 Color: 2
Size: 447138 Color: 4

Bin 407: 459 of cap free
Amount of items: 2
Items: 
Size: 683143 Color: 4
Size: 316399 Color: 2

Bin 408: 460 of cap free
Amount of items: 3
Items: 
Size: 776413 Color: 0
Size: 112757 Color: 1
Size: 110371 Color: 3

Bin 409: 462 of cap free
Amount of items: 3
Items: 
Size: 530358 Color: 0
Size: 245702 Color: 1
Size: 223479 Color: 3

Bin 410: 464 of cap free
Amount of items: 3
Items: 
Size: 744091 Color: 3
Size: 128534 Color: 4
Size: 126912 Color: 4

Bin 411: 467 of cap free
Amount of items: 3
Items: 
Size: 529145 Color: 0
Size: 247071 Color: 1
Size: 223318 Color: 3

Bin 412: 467 of cap free
Amount of items: 2
Items: 
Size: 596077 Color: 2
Size: 403457 Color: 3

Bin 413: 467 of cap free
Amount of items: 3
Items: 
Size: 614681 Color: 2
Size: 195606 Color: 1
Size: 189247 Color: 0

Bin 414: 470 of cap free
Amount of items: 3
Items: 
Size: 657634 Color: 1
Size: 172251 Color: 2
Size: 169646 Color: 3

Bin 415: 470 of cap free
Amount of items: 2
Items: 
Size: 669173 Color: 3
Size: 330358 Color: 4

Bin 416: 472 of cap free
Amount of items: 3
Items: 
Size: 546987 Color: 3
Size: 228695 Color: 0
Size: 223847 Color: 4

Bin 417: 472 of cap free
Amount of items: 2
Items: 
Size: 587426 Color: 1
Size: 412103 Color: 3

Bin 418: 476 of cap free
Amount of items: 2
Items: 
Size: 503982 Color: 2
Size: 495543 Color: 3

Bin 419: 478 of cap free
Amount of items: 2
Items: 
Size: 746331 Color: 2
Size: 253192 Color: 4

Bin 420: 481 of cap free
Amount of items: 3
Items: 
Size: 732257 Color: 2
Size: 134605 Color: 1
Size: 132658 Color: 0

Bin 421: 484 of cap free
Amount of items: 3
Items: 
Size: 644712 Color: 1
Size: 178447 Color: 3
Size: 176358 Color: 3

Bin 422: 487 of cap free
Amount of items: 2
Items: 
Size: 591774 Color: 1
Size: 407740 Color: 3

Bin 423: 489 of cap free
Amount of items: 2
Items: 
Size: 543051 Color: 2
Size: 456461 Color: 4

Bin 424: 493 of cap free
Amount of items: 2
Items: 
Size: 698700 Color: 2
Size: 300808 Color: 4

Bin 425: 493 of cap free
Amount of items: 2
Items: 
Size: 779666 Color: 2
Size: 219842 Color: 4

Bin 426: 498 of cap free
Amount of items: 2
Items: 
Size: 794610 Color: 3
Size: 204893 Color: 0

Bin 427: 499 of cap free
Amount of items: 2
Items: 
Size: 652640 Color: 0
Size: 346862 Color: 1

Bin 428: 503 of cap free
Amount of items: 2
Items: 
Size: 718988 Color: 3
Size: 280510 Color: 4

Bin 429: 503 of cap free
Amount of items: 2
Items: 
Size: 740502 Color: 4
Size: 258996 Color: 3

Bin 430: 507 of cap free
Amount of items: 2
Items: 
Size: 568071 Color: 3
Size: 431423 Color: 1

Bin 431: 507 of cap free
Amount of items: 2
Items: 
Size: 653342 Color: 4
Size: 346152 Color: 3

Bin 432: 510 of cap free
Amount of items: 2
Items: 
Size: 690177 Color: 0
Size: 309314 Color: 2

Bin 433: 510 of cap free
Amount of items: 2
Items: 
Size: 739102 Color: 2
Size: 260389 Color: 0

Bin 434: 512 of cap free
Amount of items: 2
Items: 
Size: 736566 Color: 2
Size: 262923 Color: 1

Bin 435: 515 of cap free
Amount of items: 3
Items: 
Size: 729816 Color: 0
Size: 137161 Color: 1
Size: 132509 Color: 2

Bin 436: 516 of cap free
Amount of items: 3
Items: 
Size: 618949 Color: 1
Size: 193372 Color: 2
Size: 187164 Color: 4

Bin 437: 519 of cap free
Amount of items: 2
Items: 
Size: 711714 Color: 4
Size: 287768 Color: 1

Bin 438: 519 of cap free
Amount of items: 2
Items: 
Size: 718053 Color: 0
Size: 281429 Color: 4

Bin 439: 521 of cap free
Amount of items: 2
Items: 
Size: 582147 Color: 2
Size: 417333 Color: 4

Bin 440: 522 of cap free
Amount of items: 2
Items: 
Size: 589656 Color: 2
Size: 409823 Color: 0

Bin 441: 523 of cap free
Amount of items: 2
Items: 
Size: 671943 Color: 3
Size: 327535 Color: 4

Bin 442: 526 of cap free
Amount of items: 3
Items: 
Size: 604119 Color: 1
Size: 197913 Color: 0
Size: 197443 Color: 2

Bin 443: 526 of cap free
Amount of items: 2
Items: 
Size: 633132 Color: 4
Size: 366343 Color: 2

Bin 444: 526 of cap free
Amount of items: 2
Items: 
Size: 751895 Color: 3
Size: 247580 Color: 0

Bin 445: 529 of cap free
Amount of items: 2
Items: 
Size: 517091 Color: 1
Size: 482381 Color: 3

Bin 446: 532 of cap free
Amount of items: 3
Items: 
Size: 468632 Color: 0
Size: 270876 Color: 1
Size: 259961 Color: 2

Bin 447: 536 of cap free
Amount of items: 2
Items: 
Size: 664193 Color: 0
Size: 335272 Color: 2

Bin 448: 538 of cap free
Amount of items: 2
Items: 
Size: 700251 Color: 1
Size: 299212 Color: 4

Bin 449: 539 of cap free
Amount of items: 2
Items: 
Size: 591630 Color: 3
Size: 407832 Color: 1

Bin 450: 540 of cap free
Amount of items: 2
Items: 
Size: 754092 Color: 1
Size: 245369 Color: 4

Bin 451: 541 of cap free
Amount of items: 2
Items: 
Size: 635037 Color: 4
Size: 364423 Color: 3

Bin 452: 542 of cap free
Amount of items: 2
Items: 
Size: 740495 Color: 1
Size: 258964 Color: 2

Bin 453: 542 of cap free
Amount of items: 2
Items: 
Size: 784777 Color: 4
Size: 214682 Color: 2

Bin 454: 543 of cap free
Amount of items: 2
Items: 
Size: 576601 Color: 1
Size: 422857 Color: 2

Bin 455: 543 of cap free
Amount of items: 2
Items: 
Size: 758777 Color: 4
Size: 240681 Color: 2

Bin 456: 551 of cap free
Amount of items: 3
Items: 
Size: 681340 Color: 3
Size: 159728 Color: 1
Size: 158382 Color: 2

Bin 457: 560 of cap free
Amount of items: 2
Items: 
Size: 621449 Color: 0
Size: 377992 Color: 4

Bin 458: 564 of cap free
Amount of items: 2
Items: 
Size: 511507 Color: 1
Size: 487930 Color: 3

Bin 459: 565 of cap free
Amount of items: 2
Items: 
Size: 707132 Color: 2
Size: 292304 Color: 0

Bin 460: 566 of cap free
Amount of items: 2
Items: 
Size: 756339 Color: 2
Size: 243096 Color: 1

Bin 461: 571 of cap free
Amount of items: 2
Items: 
Size: 721816 Color: 3
Size: 277614 Color: 2

Bin 462: 575 of cap free
Amount of items: 2
Items: 
Size: 535988 Color: 0
Size: 463438 Color: 4

Bin 463: 578 of cap free
Amount of items: 2
Items: 
Size: 567730 Color: 1
Size: 431693 Color: 3

Bin 464: 581 of cap free
Amount of items: 3
Items: 
Size: 596358 Color: 4
Size: 208137 Color: 1
Size: 194925 Color: 2

Bin 465: 588 of cap free
Amount of items: 2
Items: 
Size: 632516 Color: 2
Size: 366897 Color: 1

Bin 466: 594 of cap free
Amount of items: 2
Items: 
Size: 587346 Color: 1
Size: 412061 Color: 3

Bin 467: 608 of cap free
Amount of items: 2
Items: 
Size: 612964 Color: 4
Size: 386429 Color: 2

Bin 468: 615 of cap free
Amount of items: 2
Items: 
Size: 771862 Color: 3
Size: 227524 Color: 2

Bin 469: 616 of cap free
Amount of items: 2
Items: 
Size: 681183 Color: 3
Size: 318202 Color: 0

Bin 470: 631 of cap free
Amount of items: 2
Items: 
Size: 532476 Color: 1
Size: 466894 Color: 4

Bin 471: 637 of cap free
Amount of items: 2
Items: 
Size: 571707 Color: 1
Size: 427657 Color: 2

Bin 472: 645 of cap free
Amount of items: 2
Items: 
Size: 656980 Color: 2
Size: 342376 Color: 0

Bin 473: 647 of cap free
Amount of items: 2
Items: 
Size: 683780 Color: 3
Size: 315574 Color: 2

Bin 474: 649 of cap free
Amount of items: 2
Items: 
Size: 712083 Color: 4
Size: 287269 Color: 0

Bin 475: 650 of cap free
Amount of items: 2
Items: 
Size: 515621 Color: 4
Size: 483730 Color: 0

Bin 476: 655 of cap free
Amount of items: 2
Items: 
Size: 781988 Color: 4
Size: 217358 Color: 3

Bin 477: 656 of cap free
Amount of items: 2
Items: 
Size: 529484 Color: 1
Size: 469861 Color: 3

Bin 478: 656 of cap free
Amount of items: 2
Items: 
Size: 559385 Color: 3
Size: 439960 Color: 4

Bin 479: 659 of cap free
Amount of items: 2
Items: 
Size: 545926 Color: 1
Size: 453416 Color: 3

Bin 480: 667 of cap free
Amount of items: 2
Items: 
Size: 588400 Color: 4
Size: 410934 Color: 0

Bin 481: 668 of cap free
Amount of items: 3
Items: 
Size: 409528 Color: 3
Size: 329638 Color: 1
Size: 260167 Color: 0

Bin 482: 679 of cap free
Amount of items: 2
Items: 
Size: 702359 Color: 0
Size: 296963 Color: 2

Bin 483: 679 of cap free
Amount of items: 3
Items: 
Size: 757011 Color: 3
Size: 123848 Color: 1
Size: 118463 Color: 3

Bin 484: 681 of cap free
Amount of items: 2
Items: 
Size: 546984 Color: 3
Size: 452336 Color: 0

Bin 485: 684 of cap free
Amount of items: 2
Items: 
Size: 658166 Color: 0
Size: 341151 Color: 1

Bin 486: 687 of cap free
Amount of items: 2
Items: 
Size: 529805 Color: 2
Size: 469509 Color: 0

Bin 487: 693 of cap free
Amount of items: 2
Items: 
Size: 524458 Color: 3
Size: 474850 Color: 1

Bin 488: 703 of cap free
Amount of items: 2
Items: 
Size: 599250 Color: 4
Size: 400048 Color: 1

Bin 489: 706 of cap free
Amount of items: 2
Items: 
Size: 721151 Color: 4
Size: 278144 Color: 1

Bin 490: 709 of cap free
Amount of items: 2
Items: 
Size: 629664 Color: 3
Size: 369628 Color: 2

Bin 491: 711 of cap free
Amount of items: 2
Items: 
Size: 557427 Color: 2
Size: 441863 Color: 3

Bin 492: 712 of cap free
Amount of items: 3
Items: 
Size: 718592 Color: 4
Size: 142244 Color: 1
Size: 138453 Color: 3

Bin 493: 721 of cap free
Amount of items: 3
Items: 
Size: 477225 Color: 0
Size: 266752 Color: 3
Size: 255303 Color: 1

Bin 494: 721 of cap free
Amount of items: 2
Items: 
Size: 585427 Color: 3
Size: 413853 Color: 4

Bin 495: 721 of cap free
Amount of items: 2
Items: 
Size: 731286 Color: 4
Size: 267994 Color: 1

Bin 496: 724 of cap free
Amount of items: 2
Items: 
Size: 772785 Color: 4
Size: 226492 Color: 2

Bin 497: 725 of cap free
Amount of items: 2
Items: 
Size: 716308 Color: 0
Size: 282968 Color: 4

Bin 498: 727 of cap free
Amount of items: 2
Items: 
Size: 518280 Color: 3
Size: 480994 Color: 1

Bin 499: 728 of cap free
Amount of items: 2
Items: 
Size: 735250 Color: 4
Size: 264023 Color: 0

Bin 500: 728 of cap free
Amount of items: 2
Items: 
Size: 741114 Color: 2
Size: 258159 Color: 0

Bin 501: 731 of cap free
Amount of items: 2
Items: 
Size: 585082 Color: 2
Size: 414188 Color: 0

Bin 502: 733 of cap free
Amount of items: 2
Items: 
Size: 541473 Color: 4
Size: 457795 Color: 3

Bin 503: 733 of cap free
Amount of items: 2
Items: 
Size: 755793 Color: 4
Size: 243475 Color: 2

Bin 504: 735 of cap free
Amount of items: 2
Items: 
Size: 584427 Color: 4
Size: 414839 Color: 3

Bin 505: 736 of cap free
Amount of items: 2
Items: 
Size: 638274 Color: 1
Size: 360991 Color: 2

Bin 506: 739 of cap free
Amount of items: 2
Items: 
Size: 601504 Color: 2
Size: 397758 Color: 1

Bin 507: 741 of cap free
Amount of items: 2
Items: 
Size: 578134 Color: 1
Size: 421126 Color: 0

Bin 508: 742 of cap free
Amount of items: 2
Items: 
Size: 786770 Color: 2
Size: 212489 Color: 3

Bin 509: 753 of cap free
Amount of items: 2
Items: 
Size: 534262 Color: 1
Size: 464986 Color: 3

Bin 510: 758 of cap free
Amount of items: 2
Items: 
Size: 595438 Color: 4
Size: 403805 Color: 2

Bin 511: 765 of cap free
Amount of items: 2
Items: 
Size: 561611 Color: 2
Size: 437625 Color: 0

Bin 512: 765 of cap free
Amount of items: 2
Items: 
Size: 595624 Color: 0
Size: 403612 Color: 1

Bin 513: 770 of cap free
Amount of items: 2
Items: 
Size: 755511 Color: 2
Size: 243720 Color: 4

Bin 514: 777 of cap free
Amount of items: 2
Items: 
Size: 678506 Color: 0
Size: 320718 Color: 3

Bin 515: 777 of cap free
Amount of items: 2
Items: 
Size: 696309 Color: 3
Size: 302915 Color: 1

Bin 516: 777 of cap free
Amount of items: 2
Items: 
Size: 699783 Color: 2
Size: 299441 Color: 3

Bin 517: 777 of cap free
Amount of items: 2
Items: 
Size: 705625 Color: 2
Size: 293599 Color: 3

Bin 518: 781 of cap free
Amount of items: 2
Items: 
Size: 619655 Color: 4
Size: 379565 Color: 3

Bin 519: 781 of cap free
Amount of items: 2
Items: 
Size: 663958 Color: 0
Size: 335262 Color: 2

Bin 520: 790 of cap free
Amount of items: 3
Items: 
Size: 549293 Color: 0
Size: 227289 Color: 4
Size: 222629 Color: 1

Bin 521: 791 of cap free
Amount of items: 2
Items: 
Size: 524386 Color: 0
Size: 474824 Color: 1

Bin 522: 796 of cap free
Amount of items: 3
Items: 
Size: 554705 Color: 1
Size: 223272 Color: 0
Size: 221228 Color: 2

Bin 523: 797 of cap free
Amount of items: 3
Items: 
Size: 695311 Color: 0
Size: 152339 Color: 1
Size: 151554 Color: 4

Bin 524: 803 of cap free
Amount of items: 3
Items: 
Size: 618741 Color: 1
Size: 193318 Color: 4
Size: 187139 Color: 2

Bin 525: 803 of cap free
Amount of items: 2
Items: 
Size: 649874 Color: 2
Size: 349324 Color: 3

Bin 526: 810 of cap free
Amount of items: 2
Items: 
Size: 772036 Color: 2
Size: 227155 Color: 1

Bin 527: 829 of cap free
Amount of items: 2
Items: 
Size: 762657 Color: 0
Size: 236515 Color: 3

Bin 528: 836 of cap free
Amount of items: 2
Items: 
Size: 549504 Color: 2
Size: 449661 Color: 1

Bin 529: 836 of cap free
Amount of items: 2
Items: 
Size: 786600 Color: 2
Size: 212565 Color: 1

Bin 530: 842 of cap free
Amount of items: 2
Items: 
Size: 628722 Color: 0
Size: 370437 Color: 4

Bin 531: 846 of cap free
Amount of items: 2
Items: 
Size: 695416 Color: 3
Size: 303739 Color: 2

Bin 532: 848 of cap free
Amount of items: 2
Items: 
Size: 757513 Color: 0
Size: 241640 Color: 4

Bin 533: 851 of cap free
Amount of items: 2
Items: 
Size: 530220 Color: 2
Size: 468930 Color: 4

Bin 534: 851 of cap free
Amount of items: 2
Items: 
Size: 736382 Color: 3
Size: 262768 Color: 1

Bin 535: 863 of cap free
Amount of items: 2
Items: 
Size: 728188 Color: 4
Size: 270950 Color: 1

Bin 536: 868 of cap free
Amount of items: 2
Items: 
Size: 655075 Color: 4
Size: 344058 Color: 3

Bin 537: 874 of cap free
Amount of items: 2
Items: 
Size: 708222 Color: 3
Size: 290905 Color: 4

Bin 538: 876 of cap free
Amount of items: 3
Items: 
Size: 521473 Color: 0
Size: 248537 Color: 1
Size: 229115 Color: 4

Bin 539: 877 of cap free
Amount of items: 2
Items: 
Size: 545929 Color: 0
Size: 453195 Color: 1

Bin 540: 878 of cap free
Amount of items: 2
Items: 
Size: 666849 Color: 0
Size: 332274 Color: 2

Bin 541: 898 of cap free
Amount of items: 2
Items: 
Size: 723247 Color: 2
Size: 275856 Color: 3

Bin 542: 899 of cap free
Amount of items: 3
Items: 
Size: 759580 Color: 4
Size: 123957 Color: 1
Size: 115565 Color: 0

Bin 543: 907 of cap free
Amount of items: 2
Items: 
Size: 526227 Color: 3
Size: 472867 Color: 4

Bin 544: 910 of cap free
Amount of items: 2
Items: 
Size: 551113 Color: 3
Size: 447978 Color: 1

Bin 545: 910 of cap free
Amount of items: 2
Items: 
Size: 593063 Color: 2
Size: 406028 Color: 4

Bin 546: 914 of cap free
Amount of items: 2
Items: 
Size: 737169 Color: 0
Size: 261918 Color: 2

Bin 547: 915 of cap free
Amount of items: 2
Items: 
Size: 647355 Color: 4
Size: 351731 Color: 3

Bin 548: 916 of cap free
Amount of items: 2
Items: 
Size: 611587 Color: 0
Size: 387498 Color: 4

Bin 549: 919 of cap free
Amount of items: 2
Items: 
Size: 660869 Color: 2
Size: 338213 Color: 3

Bin 550: 922 of cap free
Amount of items: 2
Items: 
Size: 782171 Color: 3
Size: 216908 Color: 0

Bin 551: 925 of cap free
Amount of items: 2
Items: 
Size: 679378 Color: 0
Size: 319698 Color: 2

Bin 552: 946 of cap free
Amount of items: 3
Items: 
Size: 749403 Color: 1
Size: 124831 Color: 4
Size: 124821 Color: 2

Bin 553: 962 of cap free
Amount of items: 2
Items: 
Size: 703080 Color: 0
Size: 295959 Color: 4

Bin 554: 965 of cap free
Amount of items: 2
Items: 
Size: 656361 Color: 3
Size: 342675 Color: 2

Bin 555: 978 of cap free
Amount of items: 2
Items: 
Size: 547824 Color: 4
Size: 451199 Color: 3

Bin 556: 978 of cap free
Amount of items: 2
Items: 
Size: 632150 Color: 0
Size: 366873 Color: 3

Bin 557: 997 of cap free
Amount of items: 2
Items: 
Size: 669157 Color: 3
Size: 329847 Color: 4

Bin 558: 997 of cap free
Amount of items: 2
Items: 
Size: 786590 Color: 2
Size: 212414 Color: 4

Bin 559: 999 of cap free
Amount of items: 2
Items: 
Size: 692617 Color: 3
Size: 306385 Color: 0

Bin 560: 1000 of cap free
Amount of items: 2
Items: 
Size: 619570 Color: 0
Size: 379431 Color: 1

Bin 561: 1028 of cap free
Amount of items: 2
Items: 
Size: 516140 Color: 0
Size: 482833 Color: 1

Bin 562: 1030 of cap free
Amount of items: 2
Items: 
Size: 743961 Color: 4
Size: 255010 Color: 3

Bin 563: 1038 of cap free
Amount of items: 2
Items: 
Size: 797555 Color: 2
Size: 201408 Color: 1

Bin 564: 1041 of cap free
Amount of items: 2
Items: 
Size: 553551 Color: 1
Size: 445409 Color: 3

Bin 565: 1041 of cap free
Amount of items: 3
Items: 
Size: 785373 Color: 4
Size: 107625 Color: 1
Size: 105962 Color: 2

Bin 566: 1043 of cap free
Amount of items: 2
Items: 
Size: 551328 Color: 0
Size: 447630 Color: 1

Bin 567: 1050 of cap free
Amount of items: 2
Items: 
Size: 777358 Color: 2
Size: 221593 Color: 4

Bin 568: 1057 of cap free
Amount of items: 2
Items: 
Size: 522302 Color: 3
Size: 476642 Color: 1

Bin 569: 1060 of cap free
Amount of items: 2
Items: 
Size: 718956 Color: 1
Size: 279985 Color: 4

Bin 570: 1078 of cap free
Amount of items: 2
Items: 
Size: 505695 Color: 1
Size: 493228 Color: 4

Bin 571: 1080 of cap free
Amount of items: 2
Items: 
Size: 686448 Color: 1
Size: 312473 Color: 4

Bin 572: 1088 of cap free
Amount of items: 2
Items: 
Size: 535244 Color: 4
Size: 463669 Color: 0

Bin 573: 1091 of cap free
Amount of items: 2
Items: 
Size: 786553 Color: 0
Size: 212357 Color: 1

Bin 574: 1098 of cap free
Amount of items: 2
Items: 
Size: 766217 Color: 4
Size: 232686 Color: 2

Bin 575: 1099 of cap free
Amount of items: 2
Items: 
Size: 793372 Color: 4
Size: 205530 Color: 3

Bin 576: 1107 of cap free
Amount of items: 2
Items: 
Size: 510913 Color: 4
Size: 487981 Color: 1

Bin 577: 1113 of cap free
Amount of items: 2
Items: 
Size: 767126 Color: 2
Size: 231762 Color: 3

Bin 578: 1119 of cap free
Amount of items: 2
Items: 
Size: 521090 Color: 2
Size: 477792 Color: 0

Bin 579: 1125 of cap free
Amount of items: 2
Items: 
Size: 749382 Color: 4
Size: 249494 Color: 2

Bin 580: 1157 of cap free
Amount of items: 2
Items: 
Size: 585265 Color: 4
Size: 413579 Color: 1

Bin 581: 1158 of cap free
Amount of items: 2
Items: 
Size: 635349 Color: 4
Size: 363494 Color: 1

Bin 582: 1159 of cap free
Amount of items: 2
Items: 
Size: 508190 Color: 1
Size: 490652 Color: 4

Bin 583: 1186 of cap free
Amount of items: 2
Items: 
Size: 501852 Color: 3
Size: 496963 Color: 2

Bin 584: 1193 of cap free
Amount of items: 2
Items: 
Size: 689709 Color: 4
Size: 309099 Color: 2

Bin 585: 1233 of cap free
Amount of items: 3
Items: 
Size: 645150 Color: 0
Size: 176819 Color: 2
Size: 176799 Color: 1

Bin 586: 1234 of cap free
Amount of items: 2
Items: 
Size: 795508 Color: 0
Size: 203259 Color: 4

Bin 587: 1238 of cap free
Amount of items: 2
Items: 
Size: 649613 Color: 0
Size: 349150 Color: 4

Bin 588: 1262 of cap free
Amount of items: 2
Items: 
Size: 742370 Color: 0
Size: 256369 Color: 1

Bin 589: 1271 of cap free
Amount of items: 2
Items: 
Size: 592904 Color: 1
Size: 405826 Color: 2

Bin 590: 1271 of cap free
Amount of items: 2
Items: 
Size: 711061 Color: 3
Size: 287669 Color: 1

Bin 591: 1276 of cap free
Amount of items: 2
Items: 
Size: 513603 Color: 2
Size: 485122 Color: 4

Bin 592: 1276 of cap free
Amount of items: 2
Items: 
Size: 518089 Color: 1
Size: 480636 Color: 2

Bin 593: 1277 of cap free
Amount of items: 2
Items: 
Size: 613845 Color: 3
Size: 384879 Color: 2

Bin 594: 1288 of cap free
Amount of items: 2
Items: 
Size: 679184 Color: 1
Size: 319529 Color: 4

Bin 595: 1292 of cap free
Amount of items: 2
Items: 
Size: 621621 Color: 4
Size: 377088 Color: 2

Bin 596: 1296 of cap free
Amount of items: 2
Items: 
Size: 524675 Color: 4
Size: 474030 Color: 0

Bin 597: 1324 of cap free
Amount of items: 2
Items: 
Size: 502381 Color: 4
Size: 496296 Color: 1

Bin 598: 1324 of cap free
Amount of items: 2
Items: 
Size: 697567 Color: 0
Size: 301110 Color: 1

Bin 599: 1342 of cap free
Amount of items: 2
Items: 
Size: 703538 Color: 4
Size: 295121 Color: 2

Bin 600: 1353 of cap free
Amount of items: 2
Items: 
Size: 551741 Color: 0
Size: 446907 Color: 3

Bin 601: 1357 of cap free
Amount of items: 2
Items: 
Size: 789204 Color: 0
Size: 209440 Color: 4

Bin 602: 1360 of cap free
Amount of items: 2
Items: 
Size: 675765 Color: 1
Size: 322876 Color: 0

Bin 603: 1371 of cap free
Amount of items: 2
Items: 
Size: 715382 Color: 3
Size: 283248 Color: 0

Bin 604: 1377 of cap free
Amount of items: 2
Items: 
Size: 664406 Color: 1
Size: 334218 Color: 3

Bin 605: 1379 of cap free
Amount of items: 2
Items: 
Size: 713938 Color: 0
Size: 284684 Color: 1

Bin 606: 1393 of cap free
Amount of items: 2
Items: 
Size: 681148 Color: 3
Size: 317460 Color: 1

Bin 607: 1405 of cap free
Amount of items: 2
Items: 
Size: 554841 Color: 2
Size: 443755 Color: 4

Bin 608: 1409 of cap free
Amount of items: 2
Items: 
Size: 644276 Color: 1
Size: 354316 Color: 3

Bin 609: 1421 of cap free
Amount of items: 2
Items: 
Size: 787747 Color: 4
Size: 210833 Color: 0

Bin 610: 1428 of cap free
Amount of items: 3
Items: 
Size: 465110 Color: 0
Size: 273260 Color: 2
Size: 260203 Color: 4

Bin 611: 1428 of cap free
Amount of items: 2
Items: 
Size: 764580 Color: 2
Size: 233993 Color: 3

Bin 612: 1453 of cap free
Amount of items: 2
Items: 
Size: 619190 Color: 4
Size: 379358 Color: 2

Bin 613: 1454 of cap free
Amount of items: 2
Items: 
Size: 613524 Color: 4
Size: 385023 Color: 1

Bin 614: 1470 of cap free
Amount of items: 2
Items: 
Size: 557211 Color: 3
Size: 441320 Color: 2

Bin 615: 1478 of cap free
Amount of items: 2
Items: 
Size: 606775 Color: 2
Size: 391748 Color: 3

Bin 616: 1483 of cap free
Amount of items: 2
Items: 
Size: 548804 Color: 0
Size: 449714 Color: 4

Bin 617: 1507 of cap free
Amount of items: 2
Items: 
Size: 743851 Color: 2
Size: 254643 Color: 0

Bin 618: 1516 of cap free
Amount of items: 2
Items: 
Size: 502046 Color: 0
Size: 496439 Color: 3

Bin 619: 1517 of cap free
Amount of items: 2
Items: 
Size: 709825 Color: 2
Size: 288659 Color: 0

Bin 620: 1541 of cap free
Amount of items: 2
Items: 
Size: 741878 Color: 4
Size: 256582 Color: 3

Bin 621: 1557 of cap free
Amount of items: 2
Items: 
Size: 673778 Color: 2
Size: 324666 Color: 1

Bin 622: 1558 of cap free
Amount of items: 3
Items: 
Size: 570882 Color: 3
Size: 220772 Color: 0
Size: 206789 Color: 4

Bin 623: 1564 of cap free
Amount of items: 2
Items: 
Size: 799323 Color: 4
Size: 199114 Color: 2

Bin 624: 1569 of cap free
Amount of items: 2
Items: 
Size: 718458 Color: 1
Size: 279974 Color: 0

Bin 625: 1572 of cap free
Amount of items: 2
Items: 
Size: 715898 Color: 0
Size: 282531 Color: 3

Bin 626: 1578 of cap free
Amount of items: 2
Items: 
Size: 512200 Color: 2
Size: 486223 Color: 0

Bin 627: 1579 of cap free
Amount of items: 2
Items: 
Size: 669026 Color: 4
Size: 329396 Color: 3

Bin 628: 1579 of cap free
Amount of items: 2
Items: 
Size: 721311 Color: 1
Size: 277111 Color: 0

Bin 629: 1585 of cap free
Amount of items: 2
Items: 
Size: 628256 Color: 3
Size: 370160 Color: 1

Bin 630: 1588 of cap free
Amount of items: 2
Items: 
Size: 737087 Color: 0
Size: 261326 Color: 3

Bin 631: 1594 of cap free
Amount of items: 2
Items: 
Size: 680951 Color: 0
Size: 317456 Color: 3

Bin 632: 1607 of cap free
Amount of items: 2
Items: 
Size: 549948 Color: 4
Size: 448446 Color: 0

Bin 633: 1611 of cap free
Amount of items: 2
Items: 
Size: 617927 Color: 3
Size: 380463 Color: 4

Bin 634: 1636 of cap free
Amount of items: 2
Items: 
Size: 553434 Color: 3
Size: 444931 Color: 0

Bin 635: 1642 of cap free
Amount of items: 2
Items: 
Size: 781460 Color: 0
Size: 216899 Color: 1

Bin 636: 1651 of cap free
Amount of items: 2
Items: 
Size: 638292 Color: 2
Size: 360058 Color: 0

Bin 637: 1663 of cap free
Amount of items: 2
Items: 
Size: 559363 Color: 1
Size: 438975 Color: 0

Bin 638: 1680 of cap free
Amount of items: 3
Items: 
Size: 702150 Color: 1
Size: 149434 Color: 3
Size: 146737 Color: 0

Bin 639: 1703 of cap free
Amount of items: 3
Items: 
Size: 748950 Color: 1
Size: 124689 Color: 2
Size: 124659 Color: 4

Bin 640: 1721 of cap free
Amount of items: 2
Items: 
Size: 687920 Color: 3
Size: 310360 Color: 1

Bin 641: 1724 of cap free
Amount of items: 2
Items: 
Size: 642424 Color: 3
Size: 355853 Color: 1

Bin 642: 1727 of cap free
Amount of items: 2
Items: 
Size: 517190 Color: 3
Size: 481084 Color: 0

Bin 643: 1742 of cap free
Amount of items: 3
Items: 
Size: 657430 Color: 1
Size: 170554 Color: 2
Size: 170275 Color: 0

Bin 644: 1747 of cap free
Amount of items: 3
Items: 
Size: 720742 Color: 1
Size: 139008 Color: 0
Size: 138504 Color: 4

Bin 645: 1759 of cap free
Amount of items: 2
Items: 
Size: 799331 Color: 1
Size: 198911 Color: 2

Bin 646: 1770 of cap free
Amount of items: 2
Items: 
Size: 682925 Color: 4
Size: 315306 Color: 0

Bin 647: 1777 of cap free
Amount of items: 2
Items: 
Size: 586090 Color: 3
Size: 412134 Color: 0

Bin 648: 1780 of cap free
Amount of items: 2
Items: 
Size: 653377 Color: 4
Size: 344844 Color: 1

Bin 649: 1782 of cap free
Amount of items: 3
Items: 
Size: 481035 Color: 0
Size: 262692 Color: 1
Size: 254492 Color: 2

Bin 650: 1811 of cap free
Amount of items: 2
Items: 
Size: 558798 Color: 2
Size: 439392 Color: 3

Bin 651: 1818 of cap free
Amount of items: 2
Items: 
Size: 799307 Color: 1
Size: 198876 Color: 4

Bin 652: 1824 of cap free
Amount of items: 2
Items: 
Size: 773766 Color: 3
Size: 224411 Color: 1

Bin 653: 1825 of cap free
Amount of items: 2
Items: 
Size: 602493 Color: 4
Size: 395683 Color: 1

Bin 654: 1830 of cap free
Amount of items: 2
Items: 
Size: 649601 Color: 0
Size: 348570 Color: 2

Bin 655: 1833 of cap free
Amount of items: 3
Items: 
Size: 535243 Color: 3
Size: 237566 Color: 1
Size: 225359 Color: 4

Bin 656: 1840 of cap free
Amount of items: 2
Items: 
Size: 702858 Color: 0
Size: 295303 Color: 1

Bin 657: 1857 of cap free
Amount of items: 2
Items: 
Size: 690452 Color: 2
Size: 307692 Color: 1

Bin 658: 1879 of cap free
Amount of items: 2
Items: 
Size: 592281 Color: 2
Size: 405841 Color: 4

Bin 659: 1882 of cap free
Amount of items: 2
Items: 
Size: 644258 Color: 1
Size: 353861 Color: 2

Bin 660: 1891 of cap free
Amount of items: 2
Items: 
Size: 678426 Color: 3
Size: 319684 Color: 4

Bin 661: 1892 of cap free
Amount of items: 2
Items: 
Size: 602962 Color: 3
Size: 395147 Color: 2

Bin 662: 1902 of cap free
Amount of items: 2
Items: 
Size: 643532 Color: 0
Size: 354567 Color: 4

Bin 663: 1906 of cap free
Amount of items: 2
Items: 
Size: 584714 Color: 4
Size: 413381 Color: 1

Bin 664: 1929 of cap free
Amount of items: 2
Items: 
Size: 675199 Color: 4
Size: 322873 Color: 3

Bin 665: 1934 of cap free
Amount of items: 2
Items: 
Size: 513324 Color: 4
Size: 484743 Color: 1

Bin 666: 1942 of cap free
Amount of items: 2
Items: 
Size: 649960 Color: 4
Size: 348099 Color: 1

Bin 667: 1956 of cap free
Amount of items: 2
Items: 
Size: 660050 Color: 4
Size: 337995 Color: 3

Bin 668: 1976 of cap free
Amount of items: 2
Items: 
Size: 516031 Color: 0
Size: 481994 Color: 3

Bin 669: 1976 of cap free
Amount of items: 2
Items: 
Size: 548852 Color: 2
Size: 449173 Color: 3

Bin 670: 2053 of cap free
Amount of items: 2
Items: 
Size: 504355 Color: 2
Size: 493593 Color: 0

Bin 671: 2066 of cap free
Amount of items: 2
Items: 
Size: 571065 Color: 0
Size: 426870 Color: 1

Bin 672: 2109 of cap free
Amount of items: 2
Items: 
Size: 531646 Color: 2
Size: 466246 Color: 0

Bin 673: 2109 of cap free
Amount of items: 2
Items: 
Size: 535605 Color: 4
Size: 462287 Color: 3

Bin 674: 2120 of cap free
Amount of items: 2
Items: 
Size: 637818 Color: 3
Size: 360063 Color: 2

Bin 675: 2139 of cap free
Amount of items: 2
Items: 
Size: 759840 Color: 3
Size: 238022 Color: 4

Bin 676: 2148 of cap free
Amount of items: 2
Items: 
Size: 741192 Color: 1
Size: 256661 Color: 4

Bin 677: 2152 of cap free
Amount of items: 2
Items: 
Size: 608869 Color: 2
Size: 388980 Color: 1

Bin 678: 2158 of cap free
Amount of items: 2
Items: 
Size: 730885 Color: 4
Size: 266958 Color: 0

Bin 679: 2165 of cap free
Amount of items: 2
Items: 
Size: 581647 Color: 4
Size: 416189 Color: 2

Bin 680: 2171 of cap free
Amount of items: 2
Items: 
Size: 601950 Color: 0
Size: 395880 Color: 3

Bin 681: 2172 of cap free
Amount of items: 2
Items: 
Size: 709839 Color: 1
Size: 287990 Color: 4

Bin 682: 2175 of cap free
Amount of items: 2
Items: 
Size: 531666 Color: 3
Size: 466160 Color: 1

Bin 683: 2178 of cap free
Amount of items: 2
Items: 
Size: 695100 Color: 0
Size: 302723 Color: 1

Bin 684: 2220 of cap free
Amount of items: 2
Items: 
Size: 505031 Color: 0
Size: 492750 Color: 2

Bin 685: 2221 of cap free
Amount of items: 2
Items: 
Size: 575181 Color: 2
Size: 422599 Color: 0

Bin 686: 2228 of cap free
Amount of items: 2
Items: 
Size: 668529 Color: 0
Size: 329244 Color: 4

Bin 687: 2231 of cap free
Amount of items: 2
Items: 
Size: 545333 Color: 2
Size: 452437 Color: 3

Bin 688: 2242 of cap free
Amount of items: 2
Items: 
Size: 597527 Color: 3
Size: 400232 Color: 4

Bin 689: 2268 of cap free
Amount of items: 2
Items: 
Size: 500895 Color: 3
Size: 496838 Color: 1

Bin 690: 2287 of cap free
Amount of items: 2
Items: 
Size: 655745 Color: 1
Size: 341969 Color: 2

Bin 691: 2289 of cap free
Amount of items: 2
Items: 
Size: 753117 Color: 1
Size: 244595 Color: 0

Bin 692: 2313 of cap free
Amount of items: 2
Items: 
Size: 673446 Color: 4
Size: 324242 Color: 1

Bin 693: 2335 of cap free
Amount of items: 2
Items: 
Size: 784281 Color: 3
Size: 213385 Color: 2

Bin 694: 2339 of cap free
Amount of items: 2
Items: 
Size: 567731 Color: 3
Size: 429931 Color: 0

Bin 695: 2378 of cap free
Amount of items: 2
Items: 
Size: 620645 Color: 4
Size: 376978 Color: 0

Bin 696: 2403 of cap free
Amount of items: 3
Items: 
Size: 476410 Color: 0
Size: 261002 Color: 4
Size: 260186 Color: 2

Bin 697: 2405 of cap free
Amount of items: 2
Items: 
Size: 555953 Color: 4
Size: 441643 Color: 0

Bin 698: 2483 of cap free
Amount of items: 2
Items: 
Size: 697150 Color: 0
Size: 300368 Color: 4

Bin 699: 2510 of cap free
Amount of items: 2
Items: 
Size: 666007 Color: 4
Size: 331484 Color: 2

Bin 700: 2512 of cap free
Amount of items: 2
Items: 
Size: 763604 Color: 3
Size: 233885 Color: 1

Bin 701: 2529 of cap free
Amount of items: 2
Items: 
Size: 699404 Color: 4
Size: 298068 Color: 0

Bin 702: 2542 of cap free
Amount of items: 2
Items: 
Size: 781387 Color: 0
Size: 216072 Color: 2

Bin 703: 2556 of cap free
Amount of items: 2
Items: 
Size: 575174 Color: 4
Size: 422271 Color: 3

Bin 704: 2556 of cap free
Amount of items: 2
Items: 
Size: 730510 Color: 4
Size: 266935 Color: 2

Bin 705: 2557 of cap free
Amount of items: 2
Items: 
Size: 798838 Color: 1
Size: 198606 Color: 4

Bin 706: 2577 of cap free
Amount of items: 2
Items: 
Size: 709522 Color: 3
Size: 287902 Color: 4

Bin 707: 2586 of cap free
Amount of items: 2
Items: 
Size: 675303 Color: 3
Size: 322112 Color: 2

Bin 708: 2600 of cap free
Amount of items: 2
Items: 
Size: 741487 Color: 4
Size: 255914 Color: 1

Bin 709: 2620 of cap free
Amount of items: 2
Items: 
Size: 797439 Color: 0
Size: 199942 Color: 1

Bin 710: 2676 of cap free
Amount of items: 2
Items: 
Size: 527123 Color: 4
Size: 470202 Color: 0

Bin 711: 2705 of cap free
Amount of items: 2
Items: 
Size: 721581 Color: 0
Size: 275715 Color: 2

Bin 712: 2710 of cap free
Amount of items: 2
Items: 
Size: 767407 Color: 0
Size: 229884 Color: 1

Bin 713: 2715 of cap free
Amount of items: 2
Items: 
Size: 722068 Color: 3
Size: 275218 Color: 1

Bin 714: 2740 of cap free
Amount of items: 2
Items: 
Size: 709646 Color: 1
Size: 287615 Color: 4

Bin 715: 2753 of cap free
Amount of items: 2
Items: 
Size: 566040 Color: 1
Size: 431208 Color: 3

Bin 716: 2754 of cap free
Amount of items: 2
Items: 
Size: 643375 Color: 3
Size: 353872 Color: 0

Bin 717: 2822 of cap free
Amount of items: 2
Items: 
Size: 727863 Color: 3
Size: 269316 Color: 4

Bin 718: 2849 of cap free
Amount of items: 2
Items: 
Size: 721353 Color: 4
Size: 275799 Color: 0

Bin 719: 2866 of cap free
Amount of items: 2
Items: 
Size: 580673 Color: 2
Size: 416462 Color: 4

Bin 720: 2872 of cap free
Amount of items: 2
Items: 
Size: 617701 Color: 4
Size: 379428 Color: 1

Bin 721: 2877 of cap free
Amount of items: 2
Items: 
Size: 732050 Color: 3
Size: 265074 Color: 1

Bin 722: 2881 of cap free
Amount of items: 3
Items: 
Size: 455632 Color: 2
Size: 285049 Color: 3
Size: 256439 Color: 4

Bin 723: 2889 of cap free
Amount of items: 2
Items: 
Size: 765366 Color: 3
Size: 231746 Color: 0

Bin 724: 2916 of cap free
Amount of items: 2
Items: 
Size: 541957 Color: 0
Size: 455128 Color: 1

Bin 725: 2963 of cap free
Amount of items: 2
Items: 
Size: 637727 Color: 4
Size: 359311 Color: 3

Bin 726: 3002 of cap free
Amount of items: 2
Items: 
Size: 655632 Color: 1
Size: 341367 Color: 3

Bin 727: 3020 of cap free
Amount of items: 2
Items: 
Size: 530614 Color: 1
Size: 466367 Color: 3

Bin 728: 3030 of cap free
Amount of items: 2
Items: 
Size: 716210 Color: 3
Size: 280761 Color: 1

Bin 729: 3125 of cap free
Amount of items: 2
Items: 
Size: 501267 Color: 0
Size: 495609 Color: 2

Bin 730: 3176 of cap free
Amount of items: 2
Items: 
Size: 740316 Color: 1
Size: 256509 Color: 0

Bin 731: 3197 of cap free
Amount of items: 2
Items: 
Size: 688433 Color: 4
Size: 308371 Color: 2

Bin 732: 3206 of cap free
Amount of items: 2
Items: 
Size: 524954 Color: 1
Size: 471841 Color: 4

Bin 733: 3221 of cap free
Amount of items: 2
Items: 
Size: 634097 Color: 4
Size: 362683 Color: 1

Bin 734: 3231 of cap free
Amount of items: 2
Items: 
Size: 780380 Color: 3
Size: 216390 Color: 0

Bin 735: 3275 of cap free
Amount of items: 2
Items: 
Size: 628581 Color: 0
Size: 368145 Color: 3

Bin 736: 3309 of cap free
Amount of items: 2
Items: 
Size: 705396 Color: 3
Size: 291296 Color: 1

Bin 737: 3310 of cap free
Amount of items: 2
Items: 
Size: 602464 Color: 3
Size: 394227 Color: 0

Bin 738: 3310 of cap free
Amount of items: 2
Items: 
Size: 771459 Color: 4
Size: 225232 Color: 3

Bin 739: 3314 of cap free
Amount of items: 2
Items: 
Size: 512628 Color: 1
Size: 484059 Color: 3

Bin 740: 3415 of cap free
Amount of items: 2
Items: 
Size: 584220 Color: 0
Size: 412366 Color: 1

Bin 741: 3418 of cap free
Amount of items: 2
Items: 
Size: 701799 Color: 0
Size: 294784 Color: 2

Bin 742: 3517 of cap free
Amount of items: 2
Items: 
Size: 753026 Color: 1
Size: 243458 Color: 2

Bin 743: 3684 of cap free
Amount of items: 2
Items: 
Size: 544973 Color: 2
Size: 451344 Color: 0

Bin 744: 3836 of cap free
Amount of items: 2
Items: 
Size: 648760 Color: 4
Size: 347405 Color: 2

Bin 745: 3842 of cap free
Amount of items: 2
Items: 
Size: 557370 Color: 2
Size: 438789 Color: 1

Bin 746: 3844 of cap free
Amount of items: 2
Items: 
Size: 798090 Color: 1
Size: 198067 Color: 4

Bin 747: 3850 of cap free
Amount of items: 2
Items: 
Size: 674484 Color: 3
Size: 321667 Color: 4

Bin 748: 3860 of cap free
Amount of items: 2
Items: 
Size: 617496 Color: 0
Size: 378645 Color: 4

Bin 749: 3884 of cap free
Amount of items: 3
Items: 
Size: 462155 Color: 4
Size: 270200 Color: 1
Size: 263762 Color: 0

Bin 750: 3902 of cap free
Amount of items: 2
Items: 
Size: 586369 Color: 1
Size: 409730 Color: 3

Bin 751: 3934 of cap free
Amount of items: 2
Items: 
Size: 709288 Color: 3
Size: 286779 Color: 4

Bin 752: 3952 of cap free
Amount of items: 2
Items: 
Size: 500812 Color: 0
Size: 495237 Color: 2

Bin 753: 4084 of cap free
Amount of items: 2
Items: 
Size: 597198 Color: 0
Size: 398719 Color: 3

Bin 754: 4129 of cap free
Amount of items: 2
Items: 
Size: 540797 Color: 4
Size: 455075 Color: 1

Bin 755: 4152 of cap free
Amount of items: 2
Items: 
Size: 746297 Color: 3
Size: 249552 Color: 4

Bin 756: 4155 of cap free
Amount of items: 2
Items: 
Size: 783809 Color: 4
Size: 212037 Color: 1

Bin 757: 4191 of cap free
Amount of items: 2
Items: 
Size: 575241 Color: 3
Size: 420569 Color: 1

Bin 758: 4207 of cap free
Amount of items: 3
Items: 
Size: 535493 Color: 2
Size: 236679 Color: 0
Size: 223622 Color: 3

Bin 759: 4222 of cap free
Amount of items: 2
Items: 
Size: 715830 Color: 3
Size: 279949 Color: 1

Bin 760: 4249 of cap free
Amount of items: 2
Items: 
Size: 627834 Color: 0
Size: 367918 Color: 2

Bin 761: 4300 of cap free
Amount of items: 2
Items: 
Size: 797638 Color: 2
Size: 198063 Color: 4

Bin 762: 4571 of cap free
Amount of items: 2
Items: 
Size: 529269 Color: 1
Size: 466161 Color: 3

Bin 763: 4718 of cap free
Amount of items: 2
Items: 
Size: 557349 Color: 4
Size: 437934 Color: 1

Bin 764: 4738 of cap free
Amount of items: 2
Items: 
Size: 549346 Color: 2
Size: 445917 Color: 1

Bin 765: 4747 of cap free
Amount of items: 2
Items: 
Size: 771033 Color: 0
Size: 224221 Color: 1

Bin 766: 4776 of cap free
Amount of items: 2
Items: 
Size: 730547 Color: 0
Size: 264678 Color: 1

Bin 767: 4849 of cap free
Amount of items: 2
Items: 
Size: 601381 Color: 0
Size: 393771 Color: 2

Bin 768: 4863 of cap free
Amount of items: 2
Items: 
Size: 635858 Color: 3
Size: 359280 Color: 0

Bin 769: 4908 of cap free
Amount of items: 2
Items: 
Size: 797476 Color: 0
Size: 197617 Color: 3

Bin 770: 4935 of cap free
Amount of items: 2
Items: 
Size: 715320 Color: 0
Size: 279746 Color: 1

Bin 771: 4976 of cap free
Amount of items: 2
Items: 
Size: 553982 Color: 2
Size: 441043 Color: 0

Bin 772: 5042 of cap free
Amount of items: 2
Items: 
Size: 564122 Color: 2
Size: 430837 Color: 4

Bin 773: 5085 of cap free
Amount of items: 2
Items: 
Size: 654001 Color: 3
Size: 340915 Color: 0

Bin 774: 5203 of cap free
Amount of items: 2
Items: 
Size: 653952 Color: 3
Size: 340846 Color: 0

Bin 775: 5235 of cap free
Amount of items: 2
Items: 
Size: 600592 Color: 3
Size: 394174 Color: 0

Bin 776: 5259 of cap free
Amount of items: 2
Items: 
Size: 553863 Color: 4
Size: 440879 Color: 0

Bin 777: 5325 of cap free
Amount of items: 3
Items: 
Size: 374607 Color: 2
Size: 365560 Color: 3
Size: 254509 Color: 4

Bin 778: 5468 of cap free
Amount of items: 2
Items: 
Size: 655718 Color: 2
Size: 338815 Color: 1

Bin 779: 5490 of cap free
Amount of items: 2
Items: 
Size: 512401 Color: 2
Size: 482110 Color: 1

Bin 780: 5578 of cap free
Amount of items: 2
Items: 
Size: 617824 Color: 1
Size: 376599 Color: 4

Bin 781: 5623 of cap free
Amount of items: 2
Items: 
Size: 557770 Color: 0
Size: 436608 Color: 1

Bin 782: 5865 of cap free
Amount of items: 2
Items: 
Size: 685768 Color: 4
Size: 308368 Color: 3

Bin 783: 5871 of cap free
Amount of items: 2
Items: 
Size: 688361 Color: 3
Size: 305769 Color: 2

Bin 784: 6082 of cap free
Amount of items: 3
Items: 
Size: 603767 Color: 1
Size: 195084 Color: 3
Size: 195068 Color: 0

Bin 785: 6139 of cap free
Amount of items: 2
Items: 
Size: 525515 Color: 0
Size: 468347 Color: 1

Bin 786: 6198 of cap free
Amount of items: 2
Items: 
Size: 500583 Color: 3
Size: 493220 Color: 0

Bin 787: 6205 of cap free
Amount of items: 2
Items: 
Size: 685484 Color: 1
Size: 308312 Color: 3

Bin 788: 6295 of cap free
Amount of items: 2
Items: 
Size: 707876 Color: 1
Size: 285830 Color: 4

Bin 789: 6341 of cap free
Amount of items: 2
Items: 
Size: 617184 Color: 2
Size: 376476 Color: 4

Bin 790: 6375 of cap free
Amount of items: 2
Items: 
Size: 633321 Color: 0
Size: 360305 Color: 1

Bin 791: 6407 of cap free
Amount of items: 2
Items: 
Size: 720779 Color: 0
Size: 272815 Color: 1

Bin 792: 6411 of cap free
Amount of items: 2
Items: 
Size: 790259 Color: 4
Size: 203331 Color: 0

Bin 793: 6512 of cap free
Amount of items: 3
Items: 
Size: 527179 Color: 1
Size: 235870 Color: 4
Size: 230440 Color: 3

Bin 794: 6545 of cap free
Amount of items: 2
Items: 
Size: 707680 Color: 1
Size: 285776 Color: 2

Bin 795: 6563 of cap free
Amount of items: 2
Items: 
Size: 556946 Color: 4
Size: 436492 Color: 3

Bin 796: 6595 of cap free
Amount of items: 2
Items: 
Size: 685309 Color: 1
Size: 308097 Color: 3

Bin 797: 6641 of cap free
Amount of items: 2
Items: 
Size: 544761 Color: 1
Size: 448599 Color: 3

Bin 798: 6648 of cap free
Amount of items: 2
Items: 
Size: 513544 Color: 0
Size: 479809 Color: 3

Bin 799: 6669 of cap free
Amount of items: 2
Items: 
Size: 717852 Color: 1
Size: 275480 Color: 2

Bin 800: 6922 of cap free
Amount of items: 2
Items: 
Size: 795364 Color: 0
Size: 197715 Color: 1

Bin 801: 6956 of cap free
Amount of items: 2
Items: 
Size: 784371 Color: 2
Size: 208674 Color: 4

Bin 802: 6971 of cap free
Amount of items: 2
Items: 
Size: 542811 Color: 4
Size: 450219 Color: 0

Bin 803: 7001 of cap free
Amount of items: 2
Items: 
Size: 755415 Color: 0
Size: 237585 Color: 1

Bin 804: 7312 of cap free
Amount of items: 2
Items: 
Size: 521845 Color: 3
Size: 470844 Color: 2

Bin 805: 7861 of cap free
Amount of items: 2
Items: 
Size: 582316 Color: 0
Size: 409824 Color: 1

Bin 806: 7868 of cap free
Amount of items: 2
Items: 
Size: 625695 Color: 2
Size: 366438 Color: 4

Bin 807: 7902 of cap free
Amount of items: 2
Items: 
Size: 706410 Color: 2
Size: 285689 Color: 3

Bin 808: 8037 of cap free
Amount of items: 2
Items: 
Size: 565431 Color: 4
Size: 426533 Color: 1

Bin 809: 8173 of cap free
Amount of items: 2
Items: 
Size: 647460 Color: 2
Size: 344368 Color: 1

Bin 810: 8271 of cap free
Amount of items: 2
Items: 
Size: 616047 Color: 4
Size: 375683 Color: 3

Bin 811: 8348 of cap free
Amount of items: 3
Items: 
Size: 601590 Color: 1
Size: 195038 Color: 3
Size: 195025 Color: 4

Bin 812: 8490 of cap free
Amount of items: 2
Items: 
Size: 622355 Color: 0
Size: 369156 Color: 1

Bin 813: 8529 of cap free
Amount of items: 2
Items: 
Size: 599891 Color: 3
Size: 391581 Color: 2

Bin 814: 8542 of cap free
Amount of items: 2
Items: 
Size: 794568 Color: 2
Size: 196891 Color: 1

Bin 815: 8758 of cap free
Amount of items: 2
Items: 
Size: 525358 Color: 0
Size: 465885 Color: 1

Bin 816: 8811 of cap free
Amount of items: 2
Items: 
Size: 684700 Color: 2
Size: 306490 Color: 3

Bin 817: 8890 of cap free
Amount of items: 2
Items: 
Size: 581605 Color: 0
Size: 409506 Color: 1

Bin 818: 8978 of cap free
Amount of items: 3
Items: 
Size: 455371 Color: 2
Size: 273508 Color: 3
Size: 262144 Color: 0

Bin 819: 9261 of cap free
Amount of items: 2
Items: 
Size: 614653 Color: 2
Size: 376087 Color: 4

Bin 820: 9561 of cap free
Amount of items: 2
Items: 
Size: 512081 Color: 3
Size: 478359 Color: 4

Bin 821: 9562 of cap free
Amount of items: 2
Items: 
Size: 779849 Color: 4
Size: 210590 Color: 1

Bin 822: 9651 of cap free
Amount of items: 2
Items: 
Size: 581374 Color: 0
Size: 408976 Color: 1

Bin 823: 9674 of cap free
Amount of items: 2
Items: 
Size: 673980 Color: 0
Size: 316347 Color: 4

Bin 824: 9958 of cap free
Amount of items: 2
Items: 
Size: 522210 Color: 2
Size: 467833 Color: 4

Bin 825: 10155 of cap free
Amount of items: 2
Items: 
Size: 596377 Color: 1
Size: 393469 Color: 3

Bin 826: 10430 of cap free
Amount of items: 2
Items: 
Size: 621543 Color: 3
Size: 368028 Color: 1

Bin 827: 10473 of cap free
Amount of items: 2
Items: 
Size: 579923 Color: 2
Size: 409605 Color: 3

Bin 828: 10622 of cap free
Amount of items: 2
Items: 
Size: 751794 Color: 2
Size: 237585 Color: 4

Bin 829: 11011 of cap free
Amount of items: 2
Items: 
Size: 793213 Color: 4
Size: 195777 Color: 1

Bin 830: 11134 of cap free
Amount of items: 2
Items: 
Size: 600154 Color: 2
Size: 388713 Color: 1

Bin 831: 11206 of cap free
Amount of items: 2
Items: 
Size: 684573 Color: 0
Size: 304222 Color: 3

Bin 832: 11427 of cap free
Amount of items: 2
Items: 
Size: 683435 Color: 4
Size: 305139 Color: 1

Bin 833: 11478 of cap free
Amount of items: 2
Items: 
Size: 750959 Color: 4
Size: 237564 Color: 3

Bin 834: 11842 of cap free
Amount of items: 2
Items: 
Size: 612883 Color: 2
Size: 375276 Color: 3

Bin 835: 11849 of cap free
Amount of items: 2
Items: 
Size: 683098 Color: 2
Size: 305054 Color: 1

Bin 836: 12429 of cap free
Amount of items: 2
Items: 
Size: 685135 Color: 3
Size: 302437 Color: 0

Bin 837: 12771 of cap free
Amount of items: 2
Items: 
Size: 525642 Color: 1
Size: 461588 Color: 3

Bin 838: 12846 of cap free
Amount of items: 2
Items: 
Size: 517319 Color: 0
Size: 469836 Color: 2

Bin 839: 12895 of cap free
Amount of items: 2
Items: 
Size: 598400 Color: 2
Size: 388706 Color: 1

Bin 840: 13486 of cap free
Amount of items: 2
Items: 
Size: 611467 Color: 0
Size: 375048 Color: 2

Bin 841: 13697 of cap free
Amount of items: 2
Items: 
Size: 494025 Color: 1
Size: 492279 Color: 0

Bin 842: 14987 of cap free
Amount of items: 2
Items: 
Size: 617029 Color: 2
Size: 367985 Color: 1

Bin 843: 15280 of cap free
Amount of items: 2
Items: 
Size: 647288 Color: 2
Size: 337433 Color: 1

Bin 844: 15285 of cap free
Amount of items: 2
Items: 
Size: 595926 Color: 2
Size: 388790 Color: 3

Bin 845: 16550 of cap free
Amount of items: 2
Items: 
Size: 514105 Color: 0
Size: 469346 Color: 2

Bin 846: 16756 of cap free
Amount of items: 2
Items: 
Size: 682820 Color: 0
Size: 300425 Color: 1

Bin 847: 16915 of cap free
Amount of items: 2
Items: 
Size: 643066 Color: 2
Size: 340020 Color: 4

Bin 848: 18234 of cap free
Amount of items: 2
Items: 
Size: 522447 Color: 0
Size: 459320 Color: 3

Bin 849: 18903 of cap free
Amount of items: 2
Items: 
Size: 490949 Color: 1
Size: 490149 Color: 0

Bin 850: 19530 of cap free
Amount of items: 2
Items: 
Size: 643902 Color: 1
Size: 336569 Color: 2

Bin 851: 20660 of cap free
Amount of items: 2
Items: 
Size: 643460 Color: 1
Size: 335881 Color: 0

Bin 852: 20703 of cap free
Amount of items: 2
Items: 
Size: 680452 Color: 0
Size: 298846 Color: 1

Bin 853: 20720 of cap free
Amount of items: 2
Items: 
Size: 642901 Color: 3
Size: 336380 Color: 0

Bin 854: 20972 of cap free
Amount of items: 2
Items: 
Size: 591319 Color: 3
Size: 387710 Color: 0

Bin 855: 20989 of cap free
Amount of items: 2
Items: 
Size: 521608 Color: 0
Size: 457404 Color: 3

Bin 856: 21178 of cap free
Amount of items: 2
Items: 
Size: 513728 Color: 0
Size: 465095 Color: 2

Bin 857: 21371 of cap free
Amount of items: 2
Items: 
Size: 512519 Color: 0
Size: 466111 Color: 4

Bin 858: 21430 of cap free
Amount of items: 2
Items: 
Size: 642762 Color: 1
Size: 335809 Color: 0

Bin 859: 21650 of cap free
Amount of items: 2
Items: 
Size: 642552 Color: 1
Size: 335799 Color: 0

Bin 860: 21959 of cap free
Amount of items: 2
Items: 
Size: 489516 Color: 1
Size: 488526 Color: 4

Bin 861: 23712 of cap free
Amount of items: 2
Items: 
Size: 672793 Color: 0
Size: 303496 Color: 3

Bin 862: 24854 of cap free
Amount of items: 2
Items: 
Size: 672803 Color: 1
Size: 302344 Color: 0

Bin 863: 25209 of cap free
Amount of items: 2
Items: 
Size: 616003 Color: 3
Size: 358789 Color: 1

Bin 864: 25479 of cap free
Amount of items: 2
Items: 
Size: 672382 Color: 1
Size: 302140 Color: 0

Bin 865: 26353 of cap free
Amount of items: 2
Items: 
Size: 671342 Color: 2
Size: 302306 Color: 0

Bin 866: 26502 of cap free
Amount of items: 2
Items: 
Size: 510327 Color: 0
Size: 463172 Color: 2

Bin 867: 26978 of cap free
Amount of items: 2
Items: 
Size: 671510 Color: 1
Size: 301513 Color: 0

Bin 868: 27143 of cap free
Amount of items: 2
Items: 
Size: 510018 Color: 0
Size: 462840 Color: 2

Bin 869: 28672 of cap free
Amount of items: 2
Items: 
Size: 612775 Color: 2
Size: 358554 Color: 1

Bin 870: 31753 of cap free
Amount of items: 2
Items: 
Size: 612625 Color: 4
Size: 355623 Color: 1

Bin 871: 32629 of cap free
Amount of items: 2
Items: 
Size: 670876 Color: 1
Size: 296496 Color: 0

Bin 872: 34071 of cap free
Amount of items: 2
Items: 
Size: 509956 Color: 2
Size: 455974 Color: 3

Bin 873: 34447 of cap free
Amount of items: 2
Items: 
Size: 590852 Color: 4
Size: 374702 Color: 0

Bin 874: 35272 of cap free
Amount of items: 2
Items: 
Size: 670833 Color: 1
Size: 293896 Color: 2

Bin 875: 35475 of cap free
Amount of items: 2
Items: 
Size: 670798 Color: 1
Size: 293728 Color: 2

Bin 876: 35597 of cap free
Amount of items: 2
Items: 
Size: 670731 Color: 1
Size: 293673 Color: 2

Bin 877: 41362 of cap free
Amount of items: 3
Items: 
Size: 374898 Color: 3
Size: 323868 Color: 1
Size: 259873 Color: 4

Bin 878: 44157 of cap free
Amount of items: 2
Items: 
Size: 670536 Color: 1
Size: 285308 Color: 0

Bin 879: 44569 of cap free
Amount of items: 2
Items: 
Size: 580801 Color: 1
Size: 374631 Color: 0

Bin 880: 45065 of cap free
Amount of items: 2
Items: 
Size: 508113 Color: 4
Size: 446823 Color: 3

Bin 881: 45097 of cap free
Amount of items: 2
Items: 
Size: 547911 Color: 4
Size: 406993 Color: 1

Bin 882: 49869 of cap free
Amount of items: 2
Items: 
Size: 507004 Color: 0
Size: 443128 Color: 3

Bin 883: 50497 of cap free
Amount of items: 2
Items: 
Size: 485051 Color: 0
Size: 464453 Color: 4

Bin 884: 56820 of cap free
Amount of items: 2
Items: 
Size: 537036 Color: 0
Size: 406145 Color: 1

Bin 885: 58534 of cap free
Amount of items: 2
Items: 
Size: 535674 Color: 0
Size: 405793 Color: 1

Bin 886: 78919 of cap free
Amount of items: 2
Items: 
Size: 461131 Color: 4
Size: 459951 Color: 0

Bin 887: 81505 of cap free
Amount of items: 2
Items: 
Size: 460332 Color: 4
Size: 458164 Color: 0

Bin 888: 88272 of cap free
Amount of items: 2
Items: 
Size: 489612 Color: 4
Size: 422117 Color: 3

Bin 889: 106457 of cap free
Amount of items: 2
Items: 
Size: 446937 Color: 4
Size: 446607 Color: 0

Bin 890: 123756 of cap free
Amount of items: 2
Items: 
Size: 455176 Color: 2
Size: 421069 Color: 3

Bin 891: 125424 of cap free
Amount of items: 2
Items: 
Size: 453823 Color: 2
Size: 420754 Color: 3

Bin 892: 135036 of cap free
Amount of items: 2
Items: 
Size: 462126 Color: 0
Size: 402839 Color: 1

Bin 893: 136537 of cap free
Amount of items: 2
Items: 
Size: 461766 Color: 0
Size: 401698 Color: 1

Bin 894: 145674 of cap free
Amount of items: 2
Items: 
Size: 445396 Color: 2
Size: 408931 Color: 3

Bin 895: 171481 of cap free
Amount of items: 2
Items: 
Size: 420238 Color: 2
Size: 408282 Color: 3

Bin 896: 172434 of cap free
Amount of items: 2
Items: 
Size: 419287 Color: 2
Size: 408280 Color: 3

Bin 897: 205595 of cap free
Amount of items: 2
Items: 
Size: 405782 Color: 2
Size: 388624 Color: 3

Bin 898: 207093 of cap free
Amount of items: 2
Items: 
Size: 455740 Color: 4
Size: 337168 Color: 1

Bin 899: 211863 of cap free
Amount of items: 2
Items: 
Size: 453536 Color: 2
Size: 334602 Color: 1

Bin 900: 223470 of cap free
Amount of items: 2
Items: 
Size: 443173 Color: 4
Size: 333358 Color: 1

Bin 901: 235067 of cap free
Amount of items: 2
Items: 
Size: 436292 Color: 4
Size: 328642 Color: 1

Bin 902: 250134 of cap free
Amount of items: 2
Items: 
Size: 425672 Color: 4
Size: 324195 Color: 1

Bin 903: 269682 of cap free
Amount of items: 2
Items: 
Size: 365981 Color: 2
Size: 364338 Color: 3

Bin 904: 271672 of cap free
Amount of items: 2
Items: 
Size: 404953 Color: 4
Size: 323376 Color: 1

Bin 905: 276357 of cap free
Amount of items: 2
Items: 
Size: 364356 Color: 2
Size: 359288 Color: 3

Bin 906: 276541 of cap free
Amount of items: 2
Items: 
Size: 364328 Color: 2
Size: 359132 Color: 3

Bin 907: 310804 of cap free
Amount of items: 2
Items: 
Size: 366309 Color: 0
Size: 322888 Color: 1

Bin 908: 314071 of cap free
Amount of items: 2
Items: 
Size: 365749 Color: 4
Size: 320181 Color: 1

Total size: 901073537
Total free space: 6927371

