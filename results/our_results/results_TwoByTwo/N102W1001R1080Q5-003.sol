Capicity Bin: 1001
Lower Bound: 50

Bins used: 54
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 0
Size: 401 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 741 Color: 1
Size: 134 Color: 0
Size: 126 Color: 3

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 3
Size: 240 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 778 Color: 0
Size: 114 Color: 4
Size: 109 Color: 0

Bin 5: 1 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 2
Size: 392 Color: 1

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 4
Size: 365 Color: 3

Bin 7: 2 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 4
Size: 359 Color: 0

Bin 8: 2 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 4
Size: 226 Color: 2

Bin 9: 3 of cap free
Amount of items: 3
Items: 
Size: 754 Color: 4
Size: 137 Color: 0
Size: 107 Color: 0

Bin 10: 3 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 4
Size: 207 Color: 3

Bin 11: 4 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 0
Size: 318 Color: 2

Bin 12: 5 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 403 Color: 4

Bin 13: 5 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 4
Size: 310 Color: 1

Bin 14: 5 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 0
Size: 265 Color: 1

Bin 15: 6 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 4
Size: 419 Color: 3

Bin 16: 7 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 383 Color: 0

Bin 17: 8 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 3
Size: 423 Color: 4

Bin 18: 8 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 214 Color: 3

Bin 19: 9 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 2
Size: 236 Color: 4

Bin 20: 11 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 376 Color: 4

Bin 21: 11 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 0
Size: 341 Color: 4

Bin 22: 13 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 2
Size: 344 Color: 0

Bin 23: 14 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 4
Size: 229 Color: 2

Bin 24: 16 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 3
Size: 464 Color: 2

Bin 25: 20 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 3
Size: 457 Color: 4

Bin 26: 21 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 4
Size: 319 Color: 0

Bin 27: 23 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 2
Size: 457 Color: 4

Bin 28: 25 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 2
Size: 199 Color: 3

Bin 29: 28 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 1
Size: 341 Color: 3

Bin 30: 32 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 195 Color: 2

Bin 31: 38 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 0
Size: 378 Color: 3

Bin 32: 44 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 0
Size: 191 Color: 3

Bin 33: 48 of cap free
Amount of items: 2
Items: 
Size: 492 Color: 4
Size: 461 Color: 3

Bin 34: 50 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 445 Color: 2

Bin 35: 50 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 2
Size: 441 Color: 4

Bin 36: 57 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 2
Size: 184 Color: 3

Bin 37: 66 of cap free
Amount of items: 2
Items: 
Size: 498 Color: 0
Size: 437 Color: 4

Bin 38: 68 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 180 Color: 4

Bin 39: 85 of cap free
Amount of items: 2
Items: 
Size: 486 Color: 4
Size: 430 Color: 3

Bin 40: 90 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 174 Color: 4

Bin 41: 96 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 0
Size: 156 Color: 3

Bin 42: 107 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 4
Size: 155 Color: 3

Bin 43: 116 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 1
Size: 155 Color: 0

Bin 44: 140 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 4
Size: 146 Color: 1

Bin 45: 145 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 2
Size: 143 Color: 3

Bin 46: 290 of cap free
Amount of items: 1
Items: 
Size: 711 Color: 3

Bin 47: 299 of cap free
Amount of items: 1
Items: 
Size: 702 Color: 0

Bin 48: 310 of cap free
Amount of items: 1
Items: 
Size: 691 Color: 2

Bin 49: 311 of cap free
Amount of items: 1
Items: 
Size: 690 Color: 1

Bin 50: 313 of cap free
Amount of items: 1
Items: 
Size: 688 Color: 2

Bin 51: 328 of cap free
Amount of items: 1
Items: 
Size: 673 Color: 2

Bin 52: 329 of cap free
Amount of items: 1
Items: 
Size: 672 Color: 2

Bin 53: 371 of cap free
Amount of items: 1
Items: 
Size: 630 Color: 2

Bin 54: 416 of cap free
Amount of items: 1
Items: 
Size: 585 Color: 3

Total size: 49604
Total free space: 4450

