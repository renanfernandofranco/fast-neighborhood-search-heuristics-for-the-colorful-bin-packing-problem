Capicity Bin: 1000001
Lower Bound: 45

Bins used: 46
Amount of Colors: 5

Bin 1: 21 of cap free
Amount of items: 2
Items: 
Size: 699659 Color: 1
Size: 300321 Color: 2

Bin 2: 657 of cap free
Amount of items: 2
Items: 
Size: 538903 Color: 1
Size: 460441 Color: 0

Bin 3: 693 of cap free
Amount of items: 2
Items: 
Size: 705130 Color: 0
Size: 294178 Color: 2

Bin 4: 795 of cap free
Amount of items: 3
Items: 
Size: 578287 Color: 3
Size: 244430 Color: 1
Size: 176489 Color: 0

Bin 5: 861 of cap free
Amount of items: 3
Items: 
Size: 743798 Color: 0
Size: 145613 Color: 2
Size: 109729 Color: 0

Bin 6: 972 of cap free
Amount of items: 2
Items: 
Size: 702610 Color: 1
Size: 296419 Color: 0

Bin 7: 1474 of cap free
Amount of items: 2
Items: 
Size: 652734 Color: 3
Size: 345793 Color: 2

Bin 8: 1753 of cap free
Amount of items: 3
Items: 
Size: 668819 Color: 2
Size: 167749 Color: 3
Size: 161680 Color: 1

Bin 9: 2107 of cap free
Amount of items: 2
Items: 
Size: 561816 Color: 2
Size: 436078 Color: 3

Bin 10: 2404 of cap free
Amount of items: 3
Items: 
Size: 756218 Color: 1
Size: 136047 Color: 4
Size: 105332 Color: 2

Bin 11: 2599 of cap free
Amount of items: 3
Items: 
Size: 630287 Color: 2
Size: 197022 Color: 3
Size: 170093 Color: 0

Bin 12: 2681 of cap free
Amount of items: 2
Items: 
Size: 742882 Color: 2
Size: 254438 Color: 4

Bin 13: 2690 of cap free
Amount of items: 2
Items: 
Size: 612740 Color: 3
Size: 384571 Color: 2

Bin 14: 2826 of cap free
Amount of items: 2
Items: 
Size: 742672 Color: 4
Size: 254503 Color: 0

Bin 15: 3341 of cap free
Amount of items: 2
Items: 
Size: 585486 Color: 1
Size: 411174 Color: 4

Bin 16: 3473 of cap free
Amount of items: 2
Items: 
Size: 794565 Color: 3
Size: 201963 Color: 4

Bin 17: 4153 of cap free
Amount of items: 2
Items: 
Size: 594287 Color: 3
Size: 401561 Color: 2

Bin 18: 4174 of cap free
Amount of items: 2
Items: 
Size: 555241 Color: 0
Size: 440586 Color: 2

Bin 19: 4718 of cap free
Amount of items: 3
Items: 
Size: 602658 Color: 2
Size: 222973 Color: 3
Size: 169652 Color: 1

Bin 20: 5592 of cap free
Amount of items: 2
Items: 
Size: 507242 Color: 2
Size: 487167 Color: 3

Bin 21: 6570 of cap free
Amount of items: 2
Items: 
Size: 566072 Color: 0
Size: 427359 Color: 2

Bin 22: 6871 of cap free
Amount of items: 3
Items: 
Size: 754790 Color: 2
Size: 135815 Color: 0
Size: 102525 Color: 4

Bin 23: 7275 of cap free
Amount of items: 3
Items: 
Size: 600740 Color: 2
Size: 215458 Color: 1
Size: 176528 Color: 1

Bin 24: 7750 of cap free
Amount of items: 2
Items: 
Size: 642109 Color: 4
Size: 350142 Color: 0

Bin 25: 8565 of cap free
Amount of items: 2
Items: 
Size: 517481 Color: 3
Size: 473955 Color: 0

Bin 26: 10331 of cap free
Amount of items: 2
Items: 
Size: 522279 Color: 2
Size: 467391 Color: 1

Bin 27: 10580 of cap free
Amount of items: 2
Items: 
Size: 509052 Color: 4
Size: 480369 Color: 2

Bin 28: 10866 of cap free
Amount of items: 2
Items: 
Size: 711858 Color: 0
Size: 277277 Color: 3

Bin 29: 12346 of cap free
Amount of items: 2
Items: 
Size: 582035 Color: 0
Size: 405620 Color: 3

Bin 30: 15777 of cap free
Amount of items: 2
Items: 
Size: 774118 Color: 4
Size: 210106 Color: 2

Bin 31: 16496 of cap free
Amount of items: 2
Items: 
Size: 551363 Color: 4
Size: 432142 Color: 0

Bin 32: 21681 of cap free
Amount of items: 2
Items: 
Size: 602306 Color: 3
Size: 376014 Color: 0

Bin 33: 22101 of cap free
Amount of items: 2
Items: 
Size: 672860 Color: 0
Size: 305040 Color: 3

Bin 34: 24694 of cap free
Amount of items: 2
Items: 
Size: 709800 Color: 2
Size: 265507 Color: 0

Bin 35: 25404 of cap free
Amount of items: 2
Items: 
Size: 570667 Color: 4
Size: 403930 Color: 0

Bin 36: 29574 of cap free
Amount of items: 3
Items: 
Size: 741369 Color: 4
Size: 117940 Color: 3
Size: 111118 Color: 2

Bin 37: 31332 of cap free
Amount of items: 2
Items: 
Size: 709552 Color: 0
Size: 259117 Color: 3

Bin 38: 32473 of cap free
Amount of items: 3
Items: 
Size: 597025 Color: 2
Size: 185600 Color: 1
Size: 184903 Color: 0

Bin 39: 35711 of cap free
Amount of items: 2
Items: 
Size: 545417 Color: 4
Size: 418873 Color: 1

Bin 40: 47098 of cap free
Amount of items: 2
Items: 
Size: 583003 Color: 3
Size: 369900 Color: 2

Bin 41: 106887 of cap free
Amount of items: 2
Items: 
Size: 533747 Color: 4
Size: 359367 Color: 0

Bin 42: 116913 of cap free
Amount of items: 2
Items: 
Size: 550157 Color: 1
Size: 332931 Color: 2

Bin 43: 193694 of cap free
Amount of items: 2
Items: 
Size: 508596 Color: 3
Size: 297711 Color: 2

Bin 44: 197876 of cap free
Amount of items: 2
Items: 
Size: 498822 Color: 2
Size: 303303 Color: 4

Bin 45: 231055 of cap free
Amount of items: 2
Items: 
Size: 471009 Color: 0
Size: 297937 Color: 2

Bin 46: 242115 of cap free
Amount of items: 2
Items: 
Size: 508014 Color: 4
Size: 249872 Color: 0

Total size: 44480027
Total free space: 1520019

