#ifndef ShakerNeighborhood4_H
#define ShakerNeighborhood4_H

#include "neighborhood.h"
using namespace std;

class ShakerMovement4 {
  public:
    Bin* from1, *from2;
    int cur1, cur2;
    ShakerMovement4(){}
    ShakerMovement4(Bin* from1, Bin* from2, int cur1, int cur2):
      from1(from1), from2(from2), cur1(cur1), cur2(cur2){}
};

class ShakerNeighborhood4 : public Neighborhood {
  vector<ShakerMovement4> neighbors;
  vector<Bin*> bins;
  vector<int> its;
  int szIts, numBinsRepack, numMoves;
  ShakerMovement4 toDo;

  public:
    ShakerNeighborhood4(int idNeighbor, int timeLimit, ofstream& graphicStream,
     int numBinsRepack, int numMoves);

    void updateSolution(Solution* sol) override;
    void populate(int i1);
    void populate() override;
    void makeMove() override;
    bool search(Solution* bestSol) override;
    bool shake(Solution* bestSol);
};

#endif
