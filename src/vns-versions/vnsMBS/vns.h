#ifndef VNS_H
#define VNS_H

#include "solution.h"
#include "neighborhood.h"

#include "MoveItemNeighborhood.h"
#include "SwapItemsNeighborhood.h"
#include "ShakerNeighborhood4.h"
#include "MoveTwoToOneNeighborhood.h"
#include "SwapAndMoveToInNeighborhood.h"

using namespace std;

class Vns{
  private:
    Solution *bestSol;
    vector<Neighborhood*> neighborhoods;
    int timeInitial, diffToLowerBound;
    int targetValue, kmax;
    bool useTargetValue, tleOnInitial;
    unsigned int seed;
    random_device rd;
    chrono::steady_clock::time_point begin, now;
    int timeLastImprovement,  iteraLastImprovement,  iteraCurrent, timeCurrent;
    int iteraLastImprovementObject, beginSolValue, lastPrint;
    bool hitTarget;
    vector<int> timesPerformed;
    string sep;
    int sizeBests;

  public:
    ofstream reportStream, graphicStream;
    set<Solution*, GreaterSol> setBests;
    int timeLimit, lowerBound, timeLastImprovementObject, timeLast;
    vector<int> solsRelink;
    
    Vns(Status& status);

    void configure(chrono::steady_clock::time_point begin);
    void setInitialConfigurationToRun(Solution* sol, chrono::steady_clock::time_point begin);

    Solution* run(Solution* sol, chrono::steady_clock::time_point begin);
    Solution* bestImprovement();

    void printReport(bool result, int finalValue, int diffVnsToRelink);
    void printStatus(bool reset = true);

    ~Vns();
};

#endif
