#ifndef MOVEITEMNEIGHBORHOOD_H
#define MOVEITEMNEIGHBORHOOD_H

#include "BestImprovementNeighborhood.h"
using namespace std;

struct MoveItemMovement {
  Bin* from, *to;
  int cur, capTo, capI1, capI2, type;
  MoveItemMovement() : capTo(0), type(1){}
  MoveItemMovement(Bin* from, Bin* to, int cur):
    from(from), to(to), cur(cur){
      capTo = to->cap + weights[cur];
      capI1 = from->cap;
      capI2 = to->cap;
      if(capI1 < capI2) swap(capI1, capI2);
      type = (from->cap - weights[cur] == 0) ? 2 : capTo > from->cap;
    }

  bool operator < (const MoveItemMovement& m2) const{
    return type != m2.type ? type > m2.type :
      (capTo != m2.capTo ? capTo > m2.capTo :
      (capI1 != m2.capI1 ? capI1 < m2.capI1 : capI2 < m2.capI2));
  }
};


class MoveItemNeighborhood : public BestImprovementNeighborhood {
  int numBins;
  int *residues, *prevDiff, *forbColor;
  vector<MoveItemMovement> moves;
  MoveItemMovement toDo;

  public:
    MoveItemNeighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream);
    ~MoveItemNeighborhood();

    void populate() override;
    void makeMove() override;
};

#endif
