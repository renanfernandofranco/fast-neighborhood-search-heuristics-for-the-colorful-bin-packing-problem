#include "BestImprovementNeighborhood.h"

BestImprovementNeighborhood::BestImprovementNeighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream):
  Neighborhood(idNeighbor, timeLimit, graphicStream){}

bool BestImprovementNeighborhood::bestImprovement(Solution* bestSol){
  bool improved = false;
  while (sol->trueObject() > targetValue && timeCurrent < timeLimit &&
    this->gotoBestImprovingNeighbor(bestSol))
      improved = true;
  return improved;
}

bool BestImprovementNeighborhood::gotoBestImprovingNeighbor(Solution*& bestSol){
  populate();
  runs++;
  if(hasGood){
    improvements++;
    makeMove();
    printIfImproved(bestSol);
  }
  return hasGood;
}

bool BestImprovementNeighborhood::search(Solution* bestSol){
  return bestImprovement(bestSol);
}

void BestImprovementNeighborhood::updateSolution(Solution* sol){
  this->sol = sol;
  binIt = sol->binIt;
}
