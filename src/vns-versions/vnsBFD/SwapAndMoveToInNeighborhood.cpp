#include "SwapAndMoveToInNeighborhood.h"

SwapAndMoveToInNeighborhood::SwapAndMoveToInNeighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream):
  BestImprovementNeighborhood(idNeighbor, timeLimit, graphicStream){
    prevDiff = new int[numItems];
    itsGood = new int[numItems];
}

SwapAndMoveToInNeighborhood::~SwapAndMoveToInNeighborhood(){
  delete[] prevDiff;
  delete[] itsGood;
}

void SwapAndMoveToInNeighborhood::removeEquals(int& szGood){
  int skip = 0;
  FOR(i, szGood - 1){
    int cur = itsGood[i - skip];
    int nxt = itsGood[i + 1];
    if(weights[cur] == weights[nxt] && colors[cur] == colors[nxt]){
      if(binIt[cur]->cap > binIt[nxt]->cap)
        itsGood[i - skip] = nxt;
      skip++;
    }else
      itsGood[i - skip + 1] = nxt;
  }
  szGood -= skip;
}

void SwapAndMoveToInNeighborhood::populate() {
  vector<int>& its = sol->nonFullIts;
  int szIts = its.size();

  hasGood = false;
  toDo = SwapAndMoveToInMovement();
  moves.clear();

  int szGood = 0;
  for(int it : its)
    if(binIt[it]->canRemoveColor(colors[it]))
      itsGood[szGood++] = it;

  if(!szGood) return;

  removeEquals(szGood);

  prevDiff[0] = -1;
  for (int i = 1; i < szGood; i++)
    prevDiff[i] = colors[itsGood[i]] == colors[itsGood[i- 1]] ? prevDiff[i - 1] : i - 1;

  for(int j = 0, nxt = 0; j < szIts; j++){
    int it1 = its[j];
    UNPACK1(it1);
    while(nxt < szIts && weights[its[nxt]] <= weight1)
      nxt++;
    if(nxt == szIts)
      break;
    for (int i = nxt; i < szIts; i++){
      int it2 = its[i];
      UNPACK2(it2);
      if(weight1 + from1->residue() < weight2)
        break;
      if(from1 == from2)
        continue;

      #define MOV(f1, f2, i1, i2, w1, w2, c1, c2){\
        int tightColor2 = f2->forbiddenColor();\
        int cap2 = f2->residue() + w2 - w1;\
        if(cap2 >= itsGood[0] &&\
          (c1 == c2 || (!cannotReplace(f1, c2) && c1 != tightColor2))){\
          int forbColor = cannotAddInc(f2, c1, c1 != c2) ? c1 : \
            (c2 != tightColor2 ? tightColor2 : -1);\
          int id = upper_bound(itsGood, itsGood + szGood, cap2, \
          [weights = weights](int val, int i){ return val < weights[i];}) - itsGood - 1;\
          bool changed = true;\
          while(id != -1 && changed){\
            changed = false;\
            if (colors[itsGood[id]] == forbColor)\
              id = prevDiff[id], changed = true;\
            if (id != -1 && (binIt[itsGood[id]] == f1 || binIt[itsGood[id]] == f2))\
              id--, changed = true;\
          }\
          if (id != -1){\
            int i3 = itsGood[id];\
            SwapAndMoveToInMovement curMove(f1, f2, binIt[i3], i1, i2, i3);\
            if(!(toDo < curMove)){\
              if(curMove < toDo){\
                moves.clear();\
                toDo = curMove;\
              }\
              moves.push_back(curMove);\
            }\
          }\
        }\
      }
      MOV(from1, from2, it1, it2, weight1, weight2, color1, color2);
      MOV(from2, from1, it2, it1, weight2, weight1, color2, color1);
    }
  }

  if(!moves.empty()){
    uniform_int_distribution<int> choose_move(0, SZ(moves) - 1);
    toDo = moves[choose_move(rng)];
    hasGood = true;
  }
}

void SwapAndMoveToInNeighborhood::makeMove(){
  // sol->checkMap();
  sol->swapItem(toDo.b1, toDo.i1, toDo.i2);
  sol->swapItem(toDo.b2, toDo.i2, toDo.i1);
  sol->removeItem(toDo.b3, toDo.i3);
  sol->addItem(toDo.b2, toDo.i3);
  // sol->checkMap();
}
