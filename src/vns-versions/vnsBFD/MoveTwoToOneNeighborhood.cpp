#include "MoveTwoToOneNeighborhood.h"

MoveTwoToOneNeighborhood::MoveTwoToOneNeighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream):
  BestImprovementNeighborhood(idNeighbor, timeLimit, graphicStream){

  residues = new int[numItems + 1];
  residuesBK = new int[numItems + 1];
  bins = new Bin*[numItems];
  prevDiff = new int[numItems];
  forbColor = new int[numItems];
  forbColorBK = new int[numItems];
}

MoveTwoToOneNeighborhood::~MoveTwoToOneNeighborhood(){
  delete[] residues;
  delete[] residuesBK;
  delete[] bins;
  delete[] prevDiff;
  delete[] forbColor;
  delete[] forbColorBK;
}

void MoveTwoToOneNeighborhood::populate(){
  hasGood = false;
  toDo = MoveTwoToOneMovement();
  moves.clear();

  vector<Bin*>& binsBK = sol->nonFullBins;
  residuesBK[numBins = binsBK.size()] = 0;

  FOR(i, numBins){
    residuesBK[i] = binsBK[i]->residue();
    forbColorBK[i] = binsBK[i]->forbiddenColor();
  }

  vector<vector<int>*> pt = {&(sol->fullIts), &(sol->nonFullIts),
    &(sol->nonFullIts), &(sol->its)};

  FOR(k, 2){
    vector<int>& its = *pt[2 * k];
    vector<int>& its2 = *pt[2 * k + 1];
    int szIts = its.size();
    if(its2.empty())
      continue;
    int minw = weights[its2[0]];

    REV(i, szIts){
      int it1 = its[i];
      UNPACK1(it1);
      if (!from1->canRemoveColor(color1))
        continue;

      int nBins = 0;
      FOR(j, numBins){
        residues[nBins] = residuesBK[j] - weight1;
        if(residues[nBins] < minw)
          break;
        if(forbColorBK[j] == color1 || binsBK[j] == from1)
          continue;
        forbColor[nBins] = (forbColorBK[j] != -1 || canAddTwo(binsBK[j], color1))? -1 : color1;
        bins[nBins] = binsBK[j];
        nBins++;
      }
      residues[nBins] = 0;

      int end = upper_bound(all(its2), residues[0],
        [weights = weights](int val, int i){ return val < weights[i];}) - its2.begin();
      if(end == 0)
        continue;

      prevDiff[0] = -1;
      for (int j = 1; j < nBins; j++)
        prevDiff[j] = (forbColor[j] != -1 && forbColor[j] == forbColor[j - 1]) ? prevDiff[j - 1] : j - 1;

      int szHull = 0;

      REV(j, end){
        int it2 = its2[j];
        UNPACK2(it2);
        if (from2 == from1 || !from2->canRemoveColor(color2))
          continue;
        while (residues[szHull] >= weight2)
          szHull++;

        int id = szHull - 1;

        if (forbColor[id] == color2)
          id = prevDiff[id];
        if (id != -1 && bins[id] == from2){
          id--;
          if (id != -1 && forbColor[id] == color2)
            id = prevDiff[id];
        }

        if (id != -1){
          MoveTwoToOneMovement curMove(from1, from2, bins[id], it1, it2);
          if(!(toDo < curMove)){
            if(curMove < toDo){
              moves.clear();
              toDo = curMove;
            }
            moves.push_back(curMove);
          }
        }
      }
    }
  }

  if(!moves.empty()){
    uniform_int_distribution<int> choose_move(0, SZ(moves) - 1);
    toDo = moves[choose_move(rng)];
    hasGood = true;
  }
}

void MoveTwoToOneNeighborhood::makeMove(){
  // sol->checkMap();
  sol->removeItem(toDo.from1, toDo.cur1);
  sol->removeItem(toDo.from2, toDo.cur2);
  sol->addItem(toDo.to, toDo.cur1, toDo.cur2);
  // sol->checkMap();
}
