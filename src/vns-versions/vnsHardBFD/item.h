#ifndef ITEM_H
#define ITEM_H
#include <bits/stdc++.h>

using namespace std;
using ii = pair<int, int>;
using ll = long long;
#define all(a) a.begin(), a.end()
#define rall(a) a.rbegin(), a.rend()
#define SZ(v) ((int)v.size())
#define max(a, b) ((a) > (b) ? (a) : (b))
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define REV(i, b) for(int i = (b) - 1; i >= 0; i--)
extern int* weights;
extern int* colors;
extern int numItems;
extern int binCapacity;
extern int nColors;

class Item{
  public:
    int weight, color;
    Item(){};
    Item(int weight, int color): weight(weight), color(color){};
    bool operator < (Item const& i2) const{
      return this->weight != i2.weight ? this->weight < i2.weight : this->color < i2.color;
    }

    bool operator == (Item const& i2)const{
      return this->weight == i2.weight && this->color == i2.color;
    }
};

#endif
