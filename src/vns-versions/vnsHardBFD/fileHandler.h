#ifndef FILEHANDLE_H
#define FILEHANDLER_H

#include "bits/stdc++.h"
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

struct Status{
  bool useTarget = false;
  string pathInstance, checkSol, prefix;
  int repacksShake = 2, movesShake = 20, seed = 0;
};

string getFileName(string path);

string outdirName(string currentDir);

void createDir(string& dir);

Status readArguments(int argc, char* argv[]);

#endif
