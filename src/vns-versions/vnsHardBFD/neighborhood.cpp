#include "neighborhood.h"

Neighborhood::~Neighborhood(){
}

Neighborhood::Neighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream):graphicStream(graphicStream){
  this->sol = NULL;
  this->idNeighbor = idNeighbor;
  this->timeCurrent = 0;
  this->timeLimit = timeLimit;
  this->improvements = 0;
  this->runs = 0;
}

void Neighborhood::setSetting(int seed, int targetValue, chrono::steady_clock::time_point begin){
  this->seed = seed;
  this->rng = mt19937(seed);
  this->targetValue = targetValue;
  this->begin = begin;
}

void Neighborhood::updateTime(){
  now = chrono::steady_clock::now();
  timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();
}

void Neighborhood::printIfImproved(Solution*& bestSol){
  updateTime();
  if(*sol < *bestSol){
    bestSol = sol;
    graphicStream << sol->trueObject() << " " << timeCurrent << " " << idNeighbor << '\n';
  }
}
