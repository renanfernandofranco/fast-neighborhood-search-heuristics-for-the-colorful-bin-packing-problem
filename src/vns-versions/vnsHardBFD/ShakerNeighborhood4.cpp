#include "ShakerNeighborhood4.h"

ShakerNeighborhood4::ShakerNeighborhood4(int idNeighbor, int timeLimit,
 ofstream& graphicStream, int numBinsRepack, int numMoves):
  Neighborhood(idNeighbor, timeLimit, graphicStream), numBinsRepack(numBinsRepack),
  numMoves(numMoves){}

void ShakerNeighborhood4::populate(){}

void ShakerNeighborhood4::populate(int it1){
  neighbors.clear();

  UNPACK1(it1);

  if(from1->canRemoveColor(color1))
    for (Bin* bin : bins){
      if (bin->residue() < weight1)
        break;
      if(from1 != bin && bin->canPackColor(color1))
        neighbors.push_back(ShakerMovement4(from1, bin, it1, it1));
    }

  FOR(j, szIts){
    int it2 = its[j];
    UNPACK2(it2);
    if(weight1 + from1->residue() < weight2)
      break;

    if(from1 == from2 || weight2 + from2->residue() < weight1)
      continue;

    if(color1 != color2){
      if (cannotReplace(from1, color2) || cannotReplace(from2, color1))
        continue;
    }else{
      if(weight1 == weight2)
        continue;
    }

    neighbors.push_back(ShakerMovement4(from1, from2, it1, it2));
  }
}

bool ShakerNeighborhood4::shake(Solution* bestSol) {
  std::uniform_real_distribution<double> choose_uniform(0, 1);
  sol->getBinsReverse(bins);
  if(choose_uniform(rng) >= 0.5){
    for(Bin* bin : bins)
      bin->changed = false;

    its = sol->its;
    szIts = SZ(its);

    //int rem = min<int>(numMoves, ceil(0.05 * numItems));
    int rem = 20;
    while(rem > 0 && szIts > 0){
      uniform_int_distribution<int> choose_item(0, szIts - 1);
      int idIt = choose_item(rng);
      populate(its[idIt]);

      if (!neighbors.empty()){
        uniform_int_distribution<int> choose_move(0, neighbors.size() - 1UL);
        toDo = neighbors[choose_move(rng)];
        toDo.from1->changed = toDo.from2->changed = true;

        int skip = 0;
        FOR(i, szIts){
          if (binIt[its[i]]->changed)
            skip++;
          else
            its[i - skip] = its[i];
        }
        szIts -= skip;

        bins.erase(find(all(bins), toDo.from1));
        bins.erase(find(all(bins), toDo.from2));

        makeMove();
        rem--;
      }else{
        int skip = 0;
        int it = its[idIt];
        FOR(i, szIts){
          int it2 = its[i];
          if (weights[it] == weights[it2] &&
            colors[it] == colors[it2] &&
            binIt[it]->cap == binIt[it2]->cap)
              skip++;
          else
            its[i - skip] = its[i];
        }
        szIts -= skip;
      }
    }
  }else{
    vector<int> items;
    FOR(i, numBinsRepack){
      uniform_int_distribution<int> choose_uniform(i, SZ(bins) - 1);
      int choosen = choose_uniform(rng);
      swap(bins[i], bins[choosen]);
      items.insert(items.end(), all(bins[i]->items));
      sol->eraseBin(bins[i]);
    }

    shuffle(all(items), rng);
    sol->vectorPackingBFD(items);
  }
  runs++;
  printIfImproved(bestSol);
  if (sol == bestSol) // Wrong way to do this
    improvements++;
  //sol->checkMap();
  return sol == bestSol;
}

void ShakerNeighborhood4::updateSolution(Solution* sol){
  this->sol = sol;
  binIt = sol->binIt;
}

bool ShakerNeighborhood4::search(Solution* bestSol){
  return shake(bestSol);
}

void ShakerNeighborhood4::makeMove(){
  // sol->checkMap();
  if (toDo.cur1 == toDo.cur2){
    sol->removeItem(toDo.from1, toDo.cur1);
    sol->addItem(toDo.from2, toDo.cur1);
  }
  else{
    sol->swapItem(toDo.from1, toDo.cur1, toDo.cur2);
    sol->swapItem(toDo.from2, toDo.cur2, toDo.cur1);
  }
  // sol->checkMap();
}