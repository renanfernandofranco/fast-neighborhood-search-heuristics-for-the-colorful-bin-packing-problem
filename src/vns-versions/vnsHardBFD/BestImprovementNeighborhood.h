#ifndef BESTIMPROVEMENTNEIGHBORHOOD_H
#define BESTIMPROVEMENTNEIGHBORHOOD_H
#include "neighborhood.h"

class BestImprovementNeighborhood : public Neighborhood{
  public:
  BestImprovementNeighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream);

  virtual bool bestImprovement(Solution* bestSol);
  virtual bool gotoBestImprovingNeighbor(Solution*& bestSol);
  bool search(Solution* bestSol) override;
  void updateSolution(Solution* sol) override;
};

#endif