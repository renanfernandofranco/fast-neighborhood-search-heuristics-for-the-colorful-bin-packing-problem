#ifndef INSTANCE_H
#define INSTANCE_H
#include "bin.h"
#include "fileHandler.h"

class Instance{
  public:
    string pathInstance, pathOutSol, pathCheckSol, prefix;
    bool resultCheckSol = false;
    int size;
    Item* items;
    int *weights, *colors;
    Instance(int numItems);
    void add(int weight, int color);
    void sort();
    static Instance* readInstance(Status& status);

    ~Instance(){
      delete[] items;
      delete[] weights;
      delete[] colors;
      delete[] Bin::seenColor;
    }
};

#endif
