#include "instance.h"

int* weights;
int* colors;
int numItems;
int binCapacity;
int nColors;

Instance::Instance(int numItems){
  this->size = 0;
  this->items = new Item[numItems];
  ::weights = this->weights = new int[numItems];
  ::colors = this->colors = new int[numItems];
  Bin::seenColor = new bool[nColors];
}

void Instance::add(int weight, int color){
  this->items[size] = Item(weight, color);
  this->weights[size] = weight;
  this->colors[size++] = color;
}

void Instance::sort(){
  std::sort(items, items + size);
  for(int i = 0; i < size; i++){
    weights[i] = items[i].weight;
    colors[i] = items[i].color;
  }
}

Instance* Instance::readInstance(Status& status){
  Instance* instance;
  int size, color;
  char s[1001];
  bool isBPP = false;
  ifstream instanceStream;

  instanceStream.open(status.pathInstance);

  if (!instanceStream.good()){
    throw new invalid_argument("Not found instance file");
  }

  instanceStream.getline(s, 1000);
  int ret = sscanf(s, "%d %d %d", &numItems, &nColors, &binCapacity);
  if (ret != 3){
    isBPP = true;
    if (ret == 2)
      binCapacity = nColors;
    else
      instanceStream >> binCapacity;
    nColors = numItems;
  }

  instance = new Instance(numItems);

  for(int i = 0; i < numItems; i++){
    instanceStream >> size;
    if (isBPP)
      color = i;
    else
      instanceStream >> color;
    instance->add(size, color);
  }

  instanceStream.close();
  instance->sort();

  instance->pathInstance = status.pathInstance;
  instance->prefix = status.prefix;
  instance->pathOutSol = instance->prefix + ".sol";
  instance->pathCheckSol = status.checkSol;

  return instance;
}
