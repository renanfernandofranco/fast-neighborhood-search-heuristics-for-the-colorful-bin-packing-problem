#include "fileHandler.h"

string getFileName(string path){
  int begin = 0;
  size_t found = path.find("/");
  while(found != string::npos){
    begin = found + 1;
    found = path.find("/", begin);
  }
  return path.substr(begin);
}

string outdirName(string currentDir){
  string outdir = currentDir;

  while(outdir.back() != '/')
    outdir.pop_back();

  if(outdir == (string)"./")
    outdir.pop_back(), outdir.pop_back();

  outdir += "out";
  return outdir;
}

void createDir(string& dir){
  struct stat buffer;
  if (stat(dir.c_str(), &buffer) != 0){
    string command = "mkdir " + dir;
    assert(system(command.c_str()) == 0);
  }
}

Status readArguments(int argc, char* argv[]){
  stringstream ss;
  Status status;
  string outDir = outdirName(argv[0]);

  for(int i = 1; i < argc; i++)
    ss << argv[i] << " ";

  string tp, val;
  while(ss >> tp){
    if(tp[0] == '-'){
      tp.erase(remove(tp.begin(), tp.end(), '-'), tp.end());
      if(tp.empty() && (ss >> tp))
        throw invalid_argument("Invalid input arguments");
      if(ss >> val){
        if(tp == "seed")
          status.seed = stoi(val);
        else if(tp == "i")
          status.pathInstance = val;
        else if(tp == "target")
          status.useTarget = val == "True";
        else if(tp == "v")
          status.checkSol = val;
        else if(tp == "repacks") //repack shake
          status.repacksShake = stoi(val);
        else if(tp == "moves") //moves shake
          status.movesShake = stoi(val);
      }else
        throw invalid_argument("Invalid input arguments");
    }
    else
      throw invalid_argument("Invalid input arguments");
  }
  if(status.checkSol == "" || status.pathInstance == "")
    throw invalid_argument("Insufficient Arguments");

  createDir(outDir);
  assert(access(status.checkSol.c_str(), F_OK ) != -1);
  assert(access(status.pathInstance.c_str(), F_OK ) != -1);
  status.prefix = outDir + "/" + getFileName(status.pathInstance);

  return status;
}
