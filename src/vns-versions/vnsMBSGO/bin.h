#ifndef BIN_H
#define BIN_H
#include "item.h"
#define UNPACK(it)  int color = colors[it];\
                    int weight = weights[it];\
                    Bin* from = binIt[it];
#define UNPACK1(it1)    int color1 = colors[it1];\
                        int weight1 = weights[it1];\
                        Bin* from1 = binIt[it1];
#define UNPACK2(it2)    int color2 = colors[it2];\
                        int weight2 = weights[it2];\
                        Bin* from2 = binIt[it2];

/**
 * If inc = 0, then check if cannot insert zero item of a color without change number of items
 * If inc = 1, then check if cannot insert one item of a color without change number of items
 * If inc = 2, then check if cannot insert two items of a color without change number of items
 *
 * x' = x + 1, x" = x + 2
 *
 * Inc = 0, then 2x - 1 > n that is equal to 2x > n + 1,
 * that is the inverse of 2x <= n + 1
 *
 * Inc = 1, then 2(x + 1) - 1 > n that is equal to 2(x + 1) > n + 1,
 * that is the inverse of 2(x + 1) <= n + 1 that is equal to 2x' <= n + 1
 *
 * Inc = 2, then 2(x + 2) - 1 > n that is equal to 2(x + 2) > n + 1
 * that is the inverse of 2(x + 2) <= n + 1 that is equal to 2x" <= n + 1
 */
#define checkReplaceInc2(idBin, idColor, inc) (((bins[idBin]->colorCounter[idColor] + (inc)) << 1 | 1) > bins[idBin]->size)

/**
 * If inc = 0, then check if cannot insert zero item of a color without change number of items
 * If inc = 1, then check if cannot insert one item of a color without change number of items
 * If inc = 2, then check if cannot insert two items of a color without change number of items
 *
 * x' = x + 1, x" = x + 2
 *
 * Inc = 0, then 2x - 1 > n that is equal to 2x > n + 1,
 * that is the inverse of 2x <= n + 1
 *
 * Inc = 1, then 2(x + 1) - 1 > n that is equal to 2(x + 1) > n + 1,
 * that is the inverse of 2(x + 1) <= n + 1 that is equal to 2x' <= n + 1
 *
 * Inc = 2, then 2(x + 2) - 1 > n that is equal to 2(x + 2) > n + 1
 * that is the inverse of 2(x + 2) <= n + 1 that is equal to 2x" <= n + 1
 */
#define cannotReplaceInc(idBin, idColor, inc) (((bins[idBin]->colorCounter[idColor] + (inc)) << 1) - 1 > bins[idBin]->size)

/**
 *  Check if cannot remove one item of bin without change the number of a color
 *
 *  2x > n that is inverse of 2x <= n
 */
#define cannotItemLess(bin, idColor) ((bin->colorCounter[idColor] << 1 ) > bin->size)

/**
 *  Check if cannot remove one item of bin without change the number of a color
 *
 *  2x > n - 2 that is inverse of 2x <= n
 */
#define cannotTwoItemLess(idBin, idColor) ((bins[idBin]->colorCounter[idColor] << 1 ) > bins[idBin]->size - 2)

/**
 *  Check if cannot add one item of a color
 *
 *  x' = x + 1, n' = n + 1
 *
 *  2x > n that is equal to 2(x+1) > n + 2,
 *  that is inverse of 2(x+1) <= n + 2 that is equal to 2x' <= n' + 1
 */
#define cannotAddOne(bin, idColor) ((bin->colorCounter[idColor] << 1) > bin->size)

/**
 *  Check if cannot add one item of a color
 *
 *  x' = x + 2, n' = n + 2
 *
 *  2x' <= n' + 1 that is equal to 2(x + 2) <= n + 3,
 *  that is equal to 2x + 1 <= n
 */
#define canAddTwo(bin, idColor) ((bin->colorCounter[idColor] << 1) + 1 <= bin->size)

/**
 * Check if cannot insert one item of a color without change number of items
 *
 * x' = x + 1
 *
 * 2x + 1 > n that is equal to 2(x+1) > n + 1,
 * that is inverse of 2(x + 1) <= n + 1 that is equal to 2x' <= n + 1
 */
#define cannotReplace(bin, idColor) (((bin->colorCounter[idColor]) << 1 | 1) > bin->size)

/**
 * If inc = 0, then check if cannot insert one item of a color incrementing one in the number of items
 * If inc = 1, then check if cannot insert two item of a color incrementing one in the number of items
 *
 * x' = x + 1, x" = x + 2, n' = n + 1, n" = n + 2
 *
 * Inc = 0, then 2x > n that is equal to 2(x + 1) > n + 2,
 * that is the inverse of 2(x + 1) <= n + 2 that is equal to 2x' <= n' + 1
 *
 * Inc = 1, then 2(x + 1) > n that is equal to 2(x + 2) > n + 2
 * that is the inverse of 2(x + 2) <= n + 2 that is equal to 2x" <= n"
 */

#define cannotAddInc(bin, idColor, inc) (((bin->colorCounter[idColor] + (inc) ) << 1) > bin->size)
/**
 *  Check if cannot add one item of a color if other two of other color are removed
 *
 *  x' = x + 1, n* = n - 1
 *
 *  2x > n - 2 that is equal to 2(x+1) > n,
 *  that is inverse of 2(x+1) <= n that is equal to 2x' <= n* + 1
 */
#define cannotAddRmTwo(bin, idColor) ((bin->colorCounter[idColor] << 1 ) > bin->size - 2)

/**
 *  Check if a color is Valid
 */
#define colorIsValid(bin, idColor) ((bin->colorCounter[idColor] << 1 ) - 1 <= bin->size)

class Bin{
  private:

  inline static ll lastId = -1;
  bool needUpdate;
  int moreFreq, qtColors;
  ll binId;

  public:

  inline static bool* seenColor = NULL;
  vector<int> items;
  int* colorCounter;
  bool changed;
  int cap, size;

  inline Bin(int cur = -1){
    this->cap = this->size = this->moreFreq = 0;
    this->needUpdate = false;
    this->binId = ++lastId;
    this->colorCounter = (int*)calloc(nColors, sizeof(*colorCounter));
    if (cur != -1)
      add(cur);
  }

  inline Bin(const Bin& b2){
    this->cap = b2.cap;
    this->size = b2.size;
    this->needUpdate = b2.needUpdate;
    this->moreFreq = b2.moreFreq;
    this->binId = ++lastId;
    this->items = b2.items;
    this->colorCounter = (int*)malloc(nColors * sizeof(*colorCounter));
    memcpy(this->colorCounter, b2.colorCounter, nColors * sizeof(*colorCounter));
  }

  inline ~Bin(){
    free(this->colorCounter);
  }

  inline void add(int cur){
    this->items.push_back(cur);
    this->cap += weights[cur];
    this->colorCounter[colors[cur]]++;
    this->size++;
    this->needUpdate = true;
  }

  inline const vector<int>& getItems(){
    update();
    return items;
  }

  inline void update(){
    if(needUpdate){
      needUpdate = false;
      sort(rall(items));
      moreFreq = 0;

      if(nColors < 3 * size){
        qtColors = colorCounter[0] != 0;
        for(int i = 1; i < nColors; i++){
          if(colorCounter[i] > colorCounter[moreFreq])
            moreFreq = i;
          qtColors += colorCounter[i] != 0;
        }
      }else{
        qtColors = 0;
        for(int it : items)
          seenColor[colors[it]] = false;
        for(int it : items){
          int color = colors[it];
          if(!seenColor[color]){
            seenColor[color] = true;
            qtColors++;
            if(colorCounter[color] > colorCounter[moreFreq])
              moreFreq = color;
          }
        }
      }
    }
  }

  inline int getMoreFreq(){
    update();
    return moreFreq;
  }

  inline bool canPackColor(int color){
    return (colorCounter[color] << 1) <= size;
  }

  inline bool canPack(int weight, int color){
    return (weight <= binCapacity - cap) && ((colorCounter[color] << 1) <= size);
  }

  inline void swapItem(int cur, int nxt){
    this->items[getPos(cur)] = nxt;
    this->cap -= weights[cur];
    this->cap += weights[nxt];
    this->colorCounter[colors[cur]]--;
    this->colorCounter[colors[nxt]]++;
    needUpdate = true;
  }

  inline void swapPos(int cur1, int cur2){
    swap(items[getPos(cur1)], items[getPos(cur2)]);
    needUpdate = true;
  }

  inline void justRemove(int cur){
    this->cap -= weights[cur];
    this->colorCounter[colors[cur]]--;
    swap(items.back(), items[getPos(cur)]);
    this->items.pop_back();
    this->size--;
    needUpdate = true;
  }

  inline int residue() const{
    return binCapacity - cap;
  }

  inline int isFull() const{
    return binCapacity == cap;
  }

  inline bool canRemoveColor(int color){
    update();
    return color == moreFreq || 2 * colorCounter[moreFreq] <= size;
  }

  inline bool canRemoveTwoColor(int color1, int color2){
    int forb = forbiddenColor();
    if(color1 != moreFreq)
      swap(color1, color2);
    return color1 == forb || (color1 != moreFreq ?
      2 * colorCounter[moreFreq] <= size - 1 : qtColors != 2);
  }

  inline int forbiddenColor(){
    update();
    return 2 * colorCounter[moreFreq] <= size ? -1 : moreFreq;
  }

  inline bool isValid(int cur, int& joker){
    if(canRemoveColor(colors[cur]))
      return true;

    for(int i = 0; i < size; i++){
      if(colors[items[i]] == moreFreq){
        joker = items[i];
        return false;
      }
    }
    throw invalid_argument("Not found color more freq in isValid");
  }

  inline Bin* remove(int cur){
    int joker = -1;

    this->isValid(cur, joker);
    this->justRemove(cur);

    if(joker == -1)
      return NULL;

    Bin* remainderBin = new Bin(joker);
    this->justRemove(joker);

    return remainderBin;
  }

  inline int getPos(int cur){
    for(int i = 0; i < size; i++)
      if (items[i] == cur)
        return i;
    throw invalid_argument("Not found item in getPos");
  }

  inline static void colorMoreFreq(vector<int>& itemsToPack, int& qtMF, int& moreFreq){
    qtMF = moreFreq = 0;
    int colorCounter[nColors] = {0};

    for(int cur : itemsToPack){
      colorCounter[colors[cur]]++;
      if(colorCounter[colors[cur]] > qtMF){
        moreFreq = colors[cur];
        qtMF = colorCounter[colors[cur]];
      }
    }
  }

  bool isLess(Bin& b2){
    if (cap != b2.cap) return cap > b2.cap;
    if (size != b2.size) return size < b2.size;
    update();
    b2.update();
    int i = 0;
    for(int it : items){
      if(weights[it] != weights[b2[i]]) return weights[it] > weights[b2[i]];
      if(colors[it] != colors[b2[i]]) return colors[it] > colors[b2[i]];
      i++;
    }
    return false;
  }

  bool operator < (const Bin& b2)const{
    return cap != b2.cap ? cap < b2.cap : binId < b2.binId;
  }

  bool operator > (const Bin& b2) const{
    return cap != b2.cap ? cap > b2.cap : binId > b2.binId;
  }

  int& operator [] (int i){
    return items[i];
  }
};

struct lessBin{
  bool operator () (const Bin* b1, const Bin* b2)const{
    return *b1 < *b2;
  }
};

struct greaterBin{
  bool operator () (const Bin* b1, const Bin* b2) const{
    return *b1 > *b2;
  }
};

struct lessCap{
  bool operator () (const Bin* b1, const Bin* b2)const{
    return b1->cap < b2->cap;
  }
};

struct greaterCap{
  bool operator () (const Bin* b1, const Bin* b2)const{
    return b1->cap > b2->cap;
  }
};

struct lessBin2{
  bool operator () (Bin* b1, Bin* b2){
    return b1->isLess(*b2);
  }
};

#endif
