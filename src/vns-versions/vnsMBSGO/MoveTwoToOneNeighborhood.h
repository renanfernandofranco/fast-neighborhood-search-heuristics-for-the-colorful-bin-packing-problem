#ifndef MOVETWOTOONENEIGHBORHOOD_H
#define MOVETWOTOONENEIGHBORHOOD_H

#include "BestImprovementNeighborhood.h"
using namespace std;

struct MoveTwoToOneMovement {
  Bin* from1, *from2, *to;
  int cur1, cur2, capTo, capI, type;
  MoveTwoToOneMovement(): capTo(0), type(1){}
  MoveTwoToOneMovement(Bin* from1, Bin* from2, Bin* to, int cur1, int cur2):
    from1(from1), from2(from2), to(to), cur1(cur1), cur2(cur2){
      capTo = to->cap + weights[cur1] + weights[cur2];
      int capI1 = from1->cap;
      int capI2 = from2->cap;
      int capFrom1 = capI1 - weights[cur1];
      int capFrom2 = capI2 - weights[cur2];
      if(capI1 < capI2) swap(capI1, capI2);
      if(capFrom1 < capFrom2) swap(capFrom1, capFrom2);
      type = capFrom1 == 0 ? 3 :
        (capFrom2 == 0 ? 2 :
        (capTo > capI1 || (capTo == capI1 && capFrom1 > capI2)));
      capI = max(capI1, to->cap);
    }

  bool operator < (const MoveTwoToOneMovement& m2) const{
    return type != m2.type ? type > m2.type :
        (capTo != m2.capTo ? capTo > m2.capTo : capI < m2.capI);
  }
};

class MoveTwoToOneNeighborhood : public BestImprovementNeighborhood {
  int numBins;
  int* residues, *residuesBK, * prevDiff, *forbColor, *forbColorBK;
  MoveTwoToOneMovement toDo;
  vector<MoveTwoToOneMovement> moves;
  Bin** bins;
  public:
    MoveTwoToOneNeighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream);
    ~MoveTwoToOneNeighborhood();

    void populate() override;
    void makeMove();
};

#endif
