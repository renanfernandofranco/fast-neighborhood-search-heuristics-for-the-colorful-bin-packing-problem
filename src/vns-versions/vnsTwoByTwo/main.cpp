#include "bin.h"
#include "solution.h"
#include "instance.h"
#include "vns.h"

using namespace std;

int main(int argc, char* argv[]){
  Status status = readArguments(argc, argv);
  Instance* instance = Instance::readInstance(status);

  Vns* vns = new Vns(status);
  chrono::steady_clock::time_point begin = chrono::steady_clock::now();
  Solution* bestSol = new Solution(instance);
  bestSol->TwoByTwo2();
  bestSol = vns->run(bestSol, begin);

  bestSol->externalChecker();
  int valueSolVns = bestSol->trueObject();
  // PathRelinking::relinkAll(bestSol, vns, begin);
  // bestSol->externalChecker();
  vns->printReport(instance->resultCheckSol, bestSol->trueObject(), valueSolVns - bestSol->trueObject());
  int timeCur = vns->timeLast > 58000 ? 59999: vns->timeLast;
  cout << (bestSol->trueObject() * 60000 + timeCur) << " " << timeCur << endl;
  delete bestSol;
  delete Solution::jk;
  delete instance;
  delete vns;

  return 0;
}
