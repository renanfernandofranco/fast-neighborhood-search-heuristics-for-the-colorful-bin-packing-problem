#ifndef SWAPITEMSNEIGHBORHOOD_H
#define SWAPITEMSNEIGHBORHOOD_H

#include "BestImprovementNeighborhood.h"

struct SwapItemsMovement {
  Bin* from1, *from2;
  int cur1, cur2, capTo, capI1, capI2;
  bool type;
  SwapItemsMovement() : capTo(0), type(true){}
  SwapItemsMovement(Bin* from1, Bin* from2, int cur1, int cur2):
    from1(from1), from2(from2), cur1(cur1), cur2(cur2){
      capTo = max(from1->cap - weights[cur1] + weights[cur2],
               from2->cap + weights[cur1] - weights[cur2]);
      capI1 = from1->cap;
      capI2 = from2->cap;
      if(capI1 < capI2) swap(capI1, capI2);
      type = capTo > capI1;
    }

  bool operator < (const SwapItemsMovement& m2){
    return type != m2.type ? type :
      (capTo != m2.capTo ? capTo > m2.capTo :
      (capI1 != m2.capI1 ? capI1 < m2.capI1 : capI2 < m2.capI2));
  }
};

class SwapItemsNeighborhood : public BestImprovementNeighborhood {
  private:
    vector<SwapItemsMovement> moves;
    SwapItemsMovement toDo;

  public:
    SwapItemsNeighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream);
    void populate() override;
    void makeMove() override;
};

#endif
