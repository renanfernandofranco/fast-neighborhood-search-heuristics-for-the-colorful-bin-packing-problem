#include "MoveItemNeighborhood.h"

MoveItemNeighborhood::MoveItemNeighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream):
  BestImprovementNeighborhood(idNeighbor, timeLimit, graphicStream){

  residues = new int[numItems + 1];
  prevDiff = new int[numItems];
  forbColor = new int[numItems];
}

MoveItemNeighborhood::~MoveItemNeighborhood(){
  delete[] residues;
  delete[] prevDiff;
  delete[] forbColor;
}

void MoveItemNeighborhood::populate(){
  hasGood = false;
  toDo = MoveItemMovement();
  moves.clear();

  vector<Bin*>& bins = sol->nonFullBins;
  residues[numBins = bins.size()] = 0;

  FOR(i, numBins){
    residues[i] = bins[i]->residue();
    forbColor[i] = bins[i]->forbiddenColor();
  }

  prevDiff[0] = -1;
  for (int i = 1; i < numBins; i++)
    prevDiff[i] = (forbColor[i] != -1 && forbColor[i] == forbColor[i - 1]) ? prevDiff[i - 1] : i - 1;

  vector<int>& its = sol->nonFullIts;
  int end = upper_bound(all(its), residues[0],
    [weights = weights](int val, int i){ return val < weights[i];}) - its.begin();

  int szHull = 0;
  REV(i, end){
    int it = its[i];
    UNPACK(it);
    if (!from->canRemoveColor(color))
      continue;
    while (residues[szHull] >= weight)
      szHull++;

    if (szHull > 0){
      int id = szHull - 1;

      if (forbColor[id] == color)
        id = prevDiff[id];
      if (id != -1 && bins[id] == from){
        id--;
        if (id != -1 && forbColor[id] == color)
          id = prevDiff[id];
      }

      if (id != -1){
        MoveItemMovement curMove(from, bins[id], it);
        if(!(toDo < curMove)){
          if(curMove < toDo){
            moves.clear();
            toDo = curMove;
          }
          moves.push_back(curMove);
        }
      }
    }
  }

  if(!moves.empty()){
    uniform_int_distribution<int> choose_move(0, SZ(moves) - 1);
    toDo = moves[choose_move(rng)];
    hasGood = true;
  }
}

void MoveItemNeighborhood::makeMove(){
  // sol->checkMap();
  sol->removeItem(toDo.from, toDo.cur);
  sol->addItem(toDo.to, toDo.cur);
  // sol->checkMap();
}
