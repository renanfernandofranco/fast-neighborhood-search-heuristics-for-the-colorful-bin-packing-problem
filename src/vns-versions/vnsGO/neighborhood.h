#ifndef NEIGHBORHOOD_H
#define NEIGHBORHOOD_H

#include <random>
#include "solution.h"

struct TwoItem{
  int i1, i2, weight, color;
  TwoItem(){}
  TwoItem(int i1, int i2, int weight, int color): i1(i1), i2(i2), weight(weight), color(color){}
  TwoItem(int i1, int i2, int weight): i1(i1), i2(i2), weight(weight){}

  bool operator < (TwoItem const& p2) const{
    return weight < p2.weight;
  }
};

class Neighborhood {
  public:
    ofstream& graphicStream;
    mt19937 rng;
    Solution* sol;
    int targetValue;
    Bin** binIt;
    int *itsNonFull;
    unsigned seed;
    int idNeighbor, timeLimit, timeCurrent, improvements, runs;
    chrono::steady_clock::time_point begin, now;
    bool hasGood;

    Neighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream);

    virtual void updateTime();

    virtual void printIfImproved(Solution*& bestSol);

    virtual void setSetting(int seed, int targetValue, chrono::steady_clock::time_point begin);

    virtual ~Neighborhood();
    /* ---
    * pure virtual methods, must be implemented by children */
    virtual bool search(Solution* bestSol) = 0;
    virtual void updateSolution(Solution* sol) = 0;
    virtual void populate() = 0;
    virtual void makeMove() = 0;

    void inline calcTwoItemsPartial(vector<Bin*>& bins, vector<TwoItem>& twoItems){
      twoItems.clear();
      for(Bin* bin : bins){
        bin->update(); // sort items
        FOR(i, bin->size){
          int it1 = bin->items[i];
          int weight1 = weights[it1];
          for(int j = i + 1; j < bin->size; j++){
            int it2 = bin->items[j];
            if(bin->canRemoveColor(colors[it1]) || bin->canRemoveColor(colors[it2]))
              twoItems.push_back({it1, it2, weight1 + weights[it2]});
          }
        }
      }
      sort(all(twoItems));
    }

    void inline calcTwoItemsNone(vector<Bin*>& bins, vector<TwoItem>& twoItems){
      twoItems.clear();
      for(Bin* bin : bins){
        FOR(i, bin->size){
          int it1 = bin->items[i];
          int weight1 = weights[it1];
          for(int j = i + 1; j < bin->size; j++){
            int it2 = bin->items[j];
            twoItems.push_back({it1, it2, weight1 + weights[it2]});
          }
        }
      }
      sort(all(twoItems));
    }
};
#endif
