#include "SwapItemsNeighborhood.h"

SwapItemsNeighborhood::SwapItemsNeighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream):
  BestImprovementNeighborhood(idNeighbor, timeLimit, graphicStream){}

void SwapItemsNeighborhood::populate() {
  vector<int>& its = sol->nonFullIts;
  int szIts = its.size();
  //assert(szIts <= sol->numItems);

  hasGood = false;
  toDo = SwapItemsMovement();
  moves.clear();

  #define Search(vl) (upper_bound(its.begin() + j + 1, its.end(), vl,\
      [weights = weights](int val, int i){ return val < weights[i];}) - its.begin())

  FOR(j, szIts - 1){
    int it1 = its[j];
    UNPACK1(it1);
    int last = Search(binIt[it1]->residue() + weight1);
    int lb = weight1 + max(toDo.capTo - binIt[it1]->cap, 1);
    REV(i, last){
      int it2 = its[i];
      UNPACK2(it2);
      if(weight2 < lb)
        break;
      if(from1 == from2)
        continue;
      if(color1 != color2 && (cannotReplace(from1, color2) || cannotReplace(from2, color1)))
        continue;
      SwapItemsMovement curMove(from1, from2, it1, it2);
      if(!(toDo < curMove)){
        if(curMove < toDo){
          moves.clear();
          toDo = curMove;
        }
        moves.push_back(curMove);
        // lb = weight2;
        break;
      }
    }
  }
  // for(int j = 0, nxt = 0; j < szIts; j++){
  //    int it1 = its[j];
  //    UNPACK1(it1);
  //    while(nxt < szIts && weights[its[nxt]] <= weight1)
  //       nxt++;
  //    if(nxt == szIts)
  //       break;
  //    for (int i = nxt; i < szIts; i++){
  //       int it2 = its[i];
  //       UNPACK2(it2);
  //       if(weight1 + from1->residue() < weight2)
  //          break;
  //       if(from1 == from2)
  //          continue;
  //       if(color1 != color2 && (cannotReplace(from1, color2) || cannotReplace(from2, color1)))
  //          continue;
  //       SwapItemsMovement curMove(from1, from2, it1, it2);
  //       if(!(toDo < curMove))
  //          if(curMove < toDo)
  //             assert(false);
  //    }
  // }


  if(!moves.empty()){
    uniform_int_distribution<int> choose_move(0, SZ(moves) - 1);
    toDo = moves[choose_move(rng)];
    hasGood = true;
  }
}

void SwapItemsNeighborhood::makeMove(){
  // sol->checkMap();
  sol->swapItem(toDo.from1, toDo.cur1, toDo.cur2);
  sol->swapItem(toDo.from2, toDo.cur2, toDo.cur1);
  // sol->checkMap();
}
