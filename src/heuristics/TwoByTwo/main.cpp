#include "bin.h"
#include "solution.h"
#include "instance.h"

using namespace std;

int main(int argc, char* argv[]){
  Status status = readArguments(argc, argv);
  Instance* instance = Instance::readInstance(status);

  chrono::steady_clock::time_point begin = chrono::steady_clock::now();
  Solution* sol = new Solution(instance);
  
  sol->TwoByTwo2();

  sol->externalChecker();

	chrono::steady_clock::time_point now = chrono::steady_clock::now();
	int timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();

	string pathReport = status.prefix + ".log";
	ofstream reportStream(pathReport, ofstream::app);
	reportStream << sol->trueObject() << "," << timeCurrent << endl;

  delete sol;
  delete Solution::jk;
  delete instance;

  return 0;
}
