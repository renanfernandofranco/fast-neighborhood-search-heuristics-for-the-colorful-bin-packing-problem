#ifndef FILEHANDLE_H
#define FILEHANDLER_H

#include "bits/stdc++.h"
#include <sys/stat.h>
#include <unistd.h>

using namespace std;

struct Status{
  bool useTarget = false;
  string pathInstance, checkSol, prefix;
  int mask = 0, repacksShake = 2, movesShake = 20;
  int tlGrasp = 1000, intervalGrasp = 5000, numPL = 1;
  unsigned int seed = 0, maxCol = 60000;
  double alphaGrasp = 0.3, endGrasp = 1;
};

string getFileName(string path);

string outdirName(string currentDir);

void createDir(string& dir);

Status readArguments(int argc, char* argv[]);

#endif
