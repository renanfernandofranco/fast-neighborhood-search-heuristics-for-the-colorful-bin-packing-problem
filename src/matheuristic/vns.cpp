#include "vns.h"

using namespace std;

Vns::Vns(Status& status): status(status){
  timeLimit = 60000;
  sizeBests = 10;
  sep = ",";
  string pathReport = status.prefix + ".log";
  string pathGraphic = status.prefix + ".gph";
  string pathPL = status.prefix + ".pl";

  reportStream.open(pathReport, ofstream::app);
  graphicStream.open(pathGraphic);
  plStream.open(pathPL);

  neighborhoods.push_back(new ShakerNeighborhood4(1, timeLimit, graphicStream,
   status.repacksShake, status.movesShake));

  neighborhoods.push_back(new MoveItemNeighborhood(2, timeLimit, graphicStream));
  neighborhoods.push_back(new SwapItemsNeighborhood(3, timeLimit, graphicStream));
  neighborhoods.push_back(new SwapAndMoveToInNeighborhood(4, timeLimit, graphicStream));
  neighborhoods.push_back(new MoveTwoToOneNeighborhood(5, timeLimit, graphicStream));
  kmax = neighborhoods.size();

  // sort by mask
  vector<int> val(kmax);
  FOR(i, kmax)
    val[i] = i;
  FOR(i, status.mask)
    next_permutation(val.begin() + 1, val.end());
  FOR(i, SZ(val))
    FOR(j, SZ(val) - 1)
      if(val[j] > val[j + 1]){
        swap(val[j], val[j + 1]);
        swap(neighborhoods[j], neighborhoods[j + 1]);
      }
}

void Vns::configure(chrono::steady_clock::time_point begin){
  lowerBound = bestSol->getLowerBound();
  if(status.useTarget){
    targetValue = lowerBound + 1;
  }else
    targetValue = lowerBound;

  for(size_t i = 0; i < neighborhoods.size(); i++){
    neighborhoods[i]->setSetting(status.seed, targetValue, begin);
  }
}

void Vns::setInitialConfigurationToRun(Solution* sol, chrono::steady_clock::time_point begin) {
  this->bestSol = sol;
  this->begin = begin;
  this->solver = new Solver(sol->instance, status.seed);
  this->heuristic = new Heuristic(solver->patterns, solver->its, sol->copy(),
     status.tlGrasp, status.alphaGrasp, status.seed);
  this->skipPL = false;
  configure(begin);

  beginSolValue = sol->trueObject();
  timesPerformed.assign(neighborhoods.size(), 0);

  now = chrono::steady_clock::now();
  timeInitial = timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();
  timeLastImprovementObject = timeLastImprovement = timeCurrent;

  iteraLastImprovement  = iteraLastImprovementObject = iteraCurrent = 0;
  hitTarget = sol->trueObject() <= targetValue;
  tleOnInitial = timeInitial >= timeLimit;
  sumSize = sol->getSumSize();
  maxAccept = sol->maxAccepted(sumSize);
  storePatterns(sol);

  graphicStream << sol->trueObject() << " " << timeCurrent  << " " << 0 << '\n';
}

Solution* Vns::run(Solution* sol, chrono::steady_clock::time_point begin){
  setInitialConfigurationToRun(sol, begin);
  setBests.insert(sol->copy());
  return bestImprovement();
}

void Vns::printStatus(bool reset){
  if(reset) lastPrint = -10001;
  if(timeCurrent - lastPrint > 10000){
    lastPrint = timeCurrent;
    ll iterations = 0;
    FOR(i, kmax)
      iterations += neighborhoods[i]->runs;
    cout << bestSol->trueObject() << "\t" << bestSol->trueObject() - lowerBound << "\t"<< iterations << "\t\t" << timeCurrent / 1000 << endl;
    graphicStream.flush();
  }
}

Solution* Vns::bestImprovement(){
  Solution* sol =  bestSol->copy();
  bool skipFirst = true;
  int last = 0;

  // cout << "Value\tDiff\tIterations\tTime(s)" << endl;

  while(bestSol->trueObject() > targetValue && timeCurrent < timeLimit){
    iteraCurrent++;

    // printStatus(skipFirst);

    for(int k = skipFirst ? 1 : 0; k < kmax && timeCurrent < timeLimit && bestSol->trueObject() > targetValue;){
      timesPerformed[k]++;
      neighborhoods[k]->updateSolution(sol);
      bool improved = neighborhoods[k]->search(bestSol);

      now = chrono::steady_clock::now();
      timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();

      if(improved || k == 0){
        k = k == 1 ? 2 : 1;
        if (*sol < *bestSol){
          if(sol->trueObject() < bestSol->trueObject()){
            iteraLastImprovementObject = iteraCurrent;
            timeLastImprovementObject = timeCurrent;
          }
          iteraLastImprovement = iteraCurrent;
          timeLastImprovement = timeCurrent;
        }
      }
      else{
        k++;
      }
    }
    // if(!setBests.count(sol)){
    //   if(SZ(setBests) < sizeBests)
    //     setBests.insert(new Solution(*sol));
    //   else if(*sol < **setBests.begin()){
    //     Solution* toAdd = *setBests.begin();
    //     setBests.erase(setBests.begin());
    //     toAdd->reUse(sol);
    //     setBests.insert(toAdd);
    //   }
    // }
    if(*sol < *bestSol)
      bestSol->reUse(sol);
    storePatterns(sol);

    if(timeCurrent - last > status.intervalGrasp){
      FOR(h, status.numPL){
        if(timeCurrent < timeLimit && bestSol->trueObject() > lowerBound && runPL())
          sol->reUse(bestSol);
        now = chrono::steady_clock::now();
        timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();
      }
      last = timeCurrent;
    }
    skipFirst = false;
  }
  delete sol;

  // storePatterns(bestSol);
  hitTarget = bestSol->trueObject() <= targetValue;
  diffToLowerBound = bestSol->trueObject() - lowerBound;
  timeLast = timeCurrent;

  cout << "Final Sol: " << bestSol->trueObject() << endl;

  // printStatus(true);

  return bestSol;
}

void Vns::storePatterns(Solution* sol){
  static int bestADD = INT_MAX;
  if((timeCurrent >= (status.endGrasp * timeLimit) || patterns.size() > status.maxCol)
     && bestADD <= sol->trueObject())
    return;
  bestADD = min(sol->trueObject(), bestADD);
  for(Bin* bin : sol->fullBins){
    bin->update();
    if(!patterns.count(bin)){
      Pattern* pat = new Pattern(*bin);
      patterns.insert(pat);
      solver->AddPattern(pat);
    }
  }
  for(Bin* bin : sol->nonFullBins){
    bin->update();
    if(!patterns.count(bin)){
      Pattern* pat = new Pattern(*bin);
      patterns.insert(pat);
      solver->AddPattern(pat);
    }
  }
}

bool Vns::runPL(){
  if(skipPL)
    return false;

  now = chrono::steady_clock::now();
  timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();
  
  vd simplexSol = solver->Solve(skipPL, max(0, timeLimit - timeCurrent) / 1000);
  if(skipPL)
    return false;

  now = chrono::steady_clock::now();
  timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();
  if(timeCurrent > timeLimit)
    return false;
  
  cout << "Best Sol: " << bestSol->trueObject() << endl;
  plStream << "Best Sol: " << bestSol->trueObject() << endl;
  
  heuristic->TimeLimit = min(heuristic->TimeLimit, timeLimit - timeCurrent);
  Solution* heuristicSol = heuristic->Solve(simplexSol, plStream, lowerBound);

  storePatterns(heuristicSol);

  bool improved = heuristicSol->trueObject() < bestSol->trueObject();

  if(improved){
    swap(heuristicSol, bestSol);
    now = chrono::steady_clock::now();
    timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();
    graphicStream << bestSol->trueObject() << " " << timeCurrent << " " << (kmax + 1) << '\n';
  }

  delete heuristicSol;

  // vector<int> basis;
  // bestSol->getBinsReverse(bins);
  // for(Bin* bin : bins){
  //    auto pat = patterns.find(bin);
  //    assert(pat != patterns.end());
  //    basis.push_back((*pat)->id);
  // }
  // solver->setBasis(basis);

  return improved;
}

void Vns::printReport(bool result, int finalValue, int diffFinal){
  reportStream << diffToLowerBound << sep << diffFinal << sep << beginSolValue - lowerBound << sep;
  reportStream << finalValue << sep << beginSolValue << sep;
  reportStream << timeInitial << sep << timeLastImprovementObject << sep << timeLastImprovement << sep;
  reportStream << timeCurrent << sep << timeLast << sep << iteraLastImprovementObject << sep;
  reportStream << iteraLastImprovement << sep << iteraCurrent  << sep << hitTarget << sep << tleOnInitial << sep;

  graphicStream << "times" << endl;
  for(size_t i = 0; i < neighborhoods.size(); i++){
    graphicStream << neighborhoods[i]->improvements << " " << timesPerformed[i] << " " << neighborhoods[i]->runs << endl;
    reportStream << neighborhoods[i]->improvements << sep << timesPerformed[i] << sep;
  }

  reportStream << result << endl;
  reportStream << "Seed: " <<  status.seed << " - Mask: " << status.mask << endl;
  reportStream << "Sequence Neighboorhood: ";
  FOR(i, kmax)
    reportStream << neighborhoods[i]->idNeighbor << " \n"[i == kmax - 1];
  
  reportStream << fixed << setprecision(4);
  reportStream << "tlGrasp: " << status.tlGrasp;
  reportStream << " - intervalGrasp: " << status.intervalGrasp;
  reportStream << " - numPL: " << status.numPL << '\n';
  
  reportStream << "maxCol: " << status.maxCol;
  reportStream << " - alphaGrasp: " << status.alphaGrasp;
  reportStream << " - endGrasp: " << status.endGrasp << '\n';
  

  int cnt = 0;
  for(int vl : solsRelink)
    reportStream << vl << " " << cnt++ << endl;

  for(Pattern* pat : patterns)
    delete pat;
  delete solver;
  delete heuristic;
}

Vns::~Vns(){
  for(size_t i = 0; i < neighborhoods.size(); i++)
    delete neighborhoods[i];
  reportStream.close();
  graphicStream.close();
  plStream.close();
  for(Solution* sol : setBests){
    delete sol;
  }
}
