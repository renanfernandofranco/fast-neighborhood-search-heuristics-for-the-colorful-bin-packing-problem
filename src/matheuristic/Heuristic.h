#ifndef HEURISTIC_H
#define HEURISTIC_H
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
#include "Vnd.h"

#define FOR(i, b) for(int i = 0; i < (b); i++)
#define EPS 1e-4
using di = pair<double, int>;

using namespace __gnu_pbds;
template<typename T>
using ordered_set = tree<T, null_type, greater<T>,
    rb_tree_tag, tree_order_statistics_node_update>;

class Heuristic{
  vector<Pattern*>& patterns;
  vector<vector<int>>& its;
  int N, rem, integer;
  Solution* sample;
  Solution* integerSol, *fractionalSol;
  ordered_set<di> q;
  vector<char> usedIts;
  const double Alpha;
  mt19937 rng;
  
  public:
  int TimeLimit;
  
  Heuristic(vector<Pattern*>& patterns, vector<vector<int>>& its, Solution* sample, int timeLimit, double alpha, unsigned int seed):
  patterns(patterns), its(its), N(its.size()), sample(sample), Alpha(alpha), rng(seed), TimeLimit(timeLimit){
    sample->clear();
  }

  ~Heuristic(){
    delete sample;
  }

  void divide(vector<double>& simplexSol){
    q.clear();
    usedIts.assign(N, false);
    integerSol = new Solution(*sample, true);

    int nvar = patterns.size();
    FOR(i, nvar){
      double val = simplexSol[i];
      if(val >= 1 - EPS){
        integerSol->insertBin(new Bin(*patterns[i]));
        for(int it : patterns[i]->items)
          usedIts[it] = true;
      }else if(val > EPS){
        q.insert({val, i});
      }
    }

    fractionalSol = new Solution(*sample, true);
    fractionalSol->its.clear();
    FOR(i, N)
      if(!usedIts[i])
        fractionalSol->its.push_back(i);
    rem = fractionalSol->its.size();
    integer = integerSol->trueObject();
  }

  Solution* Grasp(ordered_set<di> q, vector<char> usedIts, int rem, double alpha, int& initialFractional, int& unpackIts){
    Solution* sol = new Solution(*fractionalSol, true);

    while(rem && !q.empty()){    
      int high = min<int>(q.size() - 1, max<int>(0, (ceil(alpha * q.size()) - 1)));

      std::uniform_int_distribution<> distrib(0, high);
      int ch = distrib(rng);

      auto p = q.find_by_order(ch);
      int j = p->second;
      q.erase(p);

      bool any = false;

      Pattern* pat = patterns[j];
      for(int it : pat->items)
        any |= usedIts[it];

      if(!any){
        for(int it : pat->items){
          rem--;
          usedIts[it] = true;
        }
        sol->insertBin(new Bin(*pat));
      }
    }

    vector<int> remIts;
    if(rem)
      REV(i, N)
        if(!usedIts[i])
          remIts.push_back(i);

    Solution* sol2 = new Solution(*sol, true);
    sol2->its = remIts;
    unpackIts = remIts.size();
    
    sol2->TwoByTwoGrasp(alpha, remIts);
    
    initialFractional = sol->trueObject() + sol2->trueObject();

    Vnd vnd(sol2->instance->prefix);
    vnd.run(sol2);
    vector<Bin*> bins;
    sol2->getBinsReverse(bins);
    for(Bin* bin : bins){
      sol->insertBin(new Bin(*bin));
    }
    delete sol2;
    
    return sol;
  }

  Solution* Solve(vector<double>& simplexSol, ofstream& plStream, int lowerBound){
    chrono::steady_clock::time_point begin, now;
    begin = chrono::steady_clock::now();
    divide(simplexSol);

    int initialFractional, unpackIts;

    int cntGrasp = 0;
    Solution* sol = Grasp(q, usedIts, rem, 0.0, initialFractional, unpackIts);
    
    plStream << "Iter. " << cntGrasp << " Grasp - unPackIts: " << unpackIts << " - Value: "<<  sol->trueObject() + integerSol->trueObject() << '\n';
    cntGrasp++;

    now = chrono::steady_clock::now();
    int timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();
    while(timeCurrent < TimeLimit && sol->trueObject() + integer > lowerBound){
      int initialFractional2, unpackIts2;

      Solution* sol2 = Grasp(q, usedIts, rem, Alpha, initialFractional2, unpackIts2);
      
      plStream << "Iter. " << cntGrasp << " Grasp - unPackIts: " << unpackIts2 << " - Value: "<<  sol2->trueObject() + integerSol->trueObject() << '\n';
      cntGrasp++;

      if(*sol2 < *sol){
        swap(sol2, sol);
        swap(initialFractional2, initialFractional);
      }
      delete sol2;

      now = chrono::steady_clock::now();
      timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();
    }

    delete fractionalSol;

    int initialPL = initialFractional + integer;
    cout << "Integer Bins : " << integer << '\n';
    plStream << "Integer Bins : " << integer << '\n';
    plStream << "Sol Initial PL: " << initialPL << '\n';
   
    vector<Bin*> bins;
    sol->getBinsReverse(bins);
    for(Bin* bin : bins)
      integerSol->insertBin(new Bin(*bin));
    delete sol;

    cout << "Sol PL: " << integerSol->trueObject() << '\n';
    plStream << "Sol PL: " << integerSol->trueObject() << '\n' << '\n';

    integerSol->externalChecker();
    return integerSol;
  }
};

#endif
