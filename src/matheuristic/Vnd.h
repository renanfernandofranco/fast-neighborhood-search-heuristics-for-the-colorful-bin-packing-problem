#ifndef VND_H
#define VND_H

#include "solution.h"
#include "neighborhood.h"

#include "MoveItemNeighborhood.h"
#include "SwapItemsNeighborhood.h"
#include "ShakerNeighborhood4.h"
#include "MoveTwoToOneNeighborhood.h"
#include "SwapAndMoveToInNeighborhood.h"

using namespace std;

class Vnd{
  private:
    vector<Neighborhood*> neighborhoods;
    int kmax, targetValue;
     ofstream graphicStream;

  public:
    Vnd(string prefix);
    void run(Solution* sol);
    void bestImprovement(Solution* sol);
    ~Vnd();
};

#endif
