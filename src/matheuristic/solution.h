#ifndef SOL_H
#define SOL_H
#include <bits/stdc++.h>
#include "instance.h"

class Solution{
   public:
      Instance* instance;
      Bin** binIt;
      vector<int> its, fullIts, nonFullIts;
      vector<Bin*> fullBins, nonFullBins;
      static inline Bin* jk = new Bin();

      Bin* bin, *bestBin;
      ll cntMBS, maxCntMBS;
      vector<int> itemsMBS;
      vector<int> suffixMBS;
      vector<int> minMBS;
      int itemsMBSsize;

      Solution(Instance* instance);
      Solution(const Solution& sol, bool empty = false);
      ~Solution();
      Solution* copy();
      void copyBins(const Solution& sol);
      void reUse(Solution* sol);
      void clear(bool resetBinIt = false);
      int maxAccepted(ll sumSize);
      ll getSumSize();
      int getLowerBound();
      void getBins(vector<Bin*>& bins);
      void getBinsReverse(vector<Bin*>& bins);
      void getBinsReverseLB(vector<Bin*>& bins, int lb);
      void static sortBinsByResidue(vector<Bin*>& bins);
      void static sortBinsByResidueReverse(vector<Bin*>& bins);
      void checkMap()const;
      void printSolution(string pathOutSol);
      void repacking(vector<int>& ids);
      static Solution* getBestInitial(Instance* instance);
      void TwoByTwo();
      void TwoByTwo2(vector<int> items = vector<int>());
      void TwoByTwoGrasp(const double alpha, vector<int> items = vector<int>());
      void HardBFD();
      void BFD();
      void buildGoodOrdering(vector<int>& order, bool insertBegin);
      bool MBSOnePacking(int pos);
      void MBS();
      void GoodOrdering();
      void externalChecker();

      void printSolution(const char* s){
         printSolution(string(s));
      }

      #define comp [weights = weights, binIt = binIt](int it1, int it2){\
         return weights[it1] != weights[it2] ?\
            weights[it1] < weights[it2] : \
            (binIt[it1]->cap != binIt[it2]->cap ? binIt[it1]->cap > binIt[it2]->cap : it1 < it2);\
      }

      inline void removeAllItems(Bin* bin){
         for(int it : bin->items){
            if(binIt[it] == bin){
               if(bin->isFull())
                  fullIts.erase(lower_bound(all(fullIts), it, comp));
               else
                  nonFullIts.erase(lower_bound(all(nonFullIts), it, comp));
               binIt[it] = NULL;
            }
         }
      }

      inline void removeBin(Bin* bin){
         removeAllItems(bin);
         if(bin->isFull())
            fullBins.erase(lower_bound(all(fullBins), bin, lessBin()));
         else
            nonFullBins.erase(lower_bound(all(nonFullBins), bin, lessBin()));
      }

      inline void insertItem(Bin* bin ,int it){
         if(binIt[it] != NULL){
            if(binIt[it]->isFull())
               fullIts.erase(lower_bound(all(fullIts), it, comp));
            else
               nonFullIts.erase(lower_bound(all(nonFullIts), it, comp));
         }
         binIt[it] = bin;
         if(bin->isFull())
            fullIts.insert(upper_bound(all(fullIts), it, comp), it);
         else
            nonFullIts.insert(upper_bound(all(nonFullIts), it, comp), it);
      }

      inline void insertBin(Bin* bin){
         for(int it : bin->items)
            insertItem(bin, it);
         if(bin->isFull())
            fullBins.insert(upper_bound(all(fullBins), bin, lessBin()), bin);
         else
            nonFullBins.insert(upper_bound(all(nonFullBins), bin, lessBin()), bin);
      }

      inline void eraseBin(Bin* bin){
         removeBin(bin);
         delete bin;
      }

      inline void swapItem(Bin* bin, int cur1, int nxt1){
         removeBin(bin);
         bin->swapItem(cur1, nxt1);
         insertBin(bin);
      }

      inline void swap2By1(Bin* bin, int cur1, int cur2, int nxt1){
         removeBin(bin);
         bin->swapItem(cur1, nxt1);
         bin->justRemove(cur2);
         insertBin(bin);
      }

      inline void swap1By2(Bin* bin, int cur1, int nxt1, int nxt2){
         removeBin(bin);
         bin->swapItem(cur1, nxt1);
         bin->add(nxt2);
         insertBin(bin);
      }

      inline void swap2By2(Bin* bin, int cur1, int cur2, int nxt1, int nxt2){
         removeBin(bin);
         bin->swapItem(cur1, nxt1);
         bin->swapItem(cur2, nxt2);
         insertBin(bin);
      }

      inline void addItem(Bin* bin, int nxt1, int nxt2 = -1){
         removeBin(bin);
         bin->add(nxt1);
         if(nxt2 != -1)
            bin->add(nxt2);
         insertBin(bin);
      }

      inline void removeItem(Bin* bin, int cur1, int cur2 = -1){
         if(bin->size == 1 + (cur2 != -1)){
            eraseBin(bin);
         }else {
            removeBin(bin);
            bin->justRemove(cur1);
            if(cur2 != -1)
               bin->justRemove(cur2);
            insertBin(bin);
         }
      }

      inline void fix(Bin* bin){
         int mf = bin->getMoreFreq();
         while(!colorIsValid(bin, mf))
            for(int it : bin->items)
               if(colors[it] == mf){
                  removeItem(bin, it);
                  packingBFD(it);
                  break;
               }
      }

      inline void vectorPackingBFD(vector<int>& items){
         for(int it : items)
            packingBFD(it);
      }

      inline bool tryPackingBFD(int cur){
         jk->cap = binCapacity - weights[cur];
         
         for(auto it = lower_bound(rall(nonFullBins), jk, greaterBin()); it != nonFullBins.rend(); it++){
            Bin* bin = *it;
            if (bin->canPackColor(colors[cur])){
               addItem(bin, cur);
               return true;
            }
         }
         return false;
      }

      inline void packingBFD(int cur){
         if(!tryPackingBFD(cur))
            insertBin(new Bin(cur));
      }

      int trueObject() const{
         return SZ(fullBins) + SZ(nonFullBins);
      }

      bool operator < (const Solution& solB) const{
         if(trueObject() != solB.trueObject())
            return trueObject() < solB.trueObject();

         if(SZ(fullBins) != SZ(solB.fullBins))	
            return SZ(fullBins) < SZ(solB.fullBins);
         
         auto it = solB.nonFullBins.begin();
         for(Bin* bin : nonFullBins){
            if(bin->cap != (*it)->cap)
               return bin->cap > (*it)->cap;
            it++;
         }
         return false;
      }
};

struct GreaterSol{
   bool operator () (const Solution* sol1, const Solution* sol2) const{
      return *sol2 < *sol1;
   }
};

#endif
