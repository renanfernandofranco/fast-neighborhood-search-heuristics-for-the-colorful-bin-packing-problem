#ifndef PL_H
#define PL_H
#include "gurobi_c++.h"
#include "bin.h"
#include "instance.h"
#define FOR(i, b) for(int i = 0; i < (b); i++)
#define EPS 1e-4
using vd = vector<double>;

class Solver{
  public:
	Instance* instance;
  GRBModel* model;
  vector<Pattern*> patterns;
  int numCols, N;
  vector<vector<int>> its;
  GRBConstr* constrs;
  vector<GRBVar> vars;
  mt19937 rng;
  bool needUpdate;

	Solver(Instance* instance, unsigned int seed);
  void AddPattern(Pattern* pat);
  void InitModel();
  void setBasis(vector<int> basis);

  ~Solver(){
    delete[] model->getVars();
    delete[] constrs;
    delete model;
  }

	vd Solve(bool& abort, double remainingTime);
};

#endif
