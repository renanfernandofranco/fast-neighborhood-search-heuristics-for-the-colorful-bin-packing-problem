#ifndef SwapAndMoveToInNEIGHBORHOOD_H
#define SwapAndMoveToInNEIGHBORHOOD_H

#include "BestImprovementNeighborhood.h"

struct SwapAndMoveToInMovement {
  Bin* b1, *b2, *b3;
  int i1, i2, i3, v0, v1, v2, v3, v4, v5, type;
  #define S (1 << 30)
  #define E(vl) (vl | S)
  #define G(vl) if(vl & S) vl = -(vl ^ S)
  #define SW(v1, v2) if(E(v1) < E(v2)) swap(v1, v2)

  SwapAndMoveToInMovement(): v0(1), v1(0), v2(0), v3(0), v4(0), v5(0), type(1){}
  SwapAndMoveToInMovement(Bin* b1, Bin* b2, Bin* b3, int i1, int i2, int i3):
    b1(b1), b2(b2), b3(b3), i1(i1), i2(i2), i3(i3){
      v0 = b1->cap - weights[i1] + weights[i2];
      v1 = b2->cap + weights[i1] - weights[i2] + weights[i3];
      v2 = b3->cap - weights[i3];
      v3 = b1->cap | S;
      v4 = b2->cap | S;
      v5 = b3->cap | S;
      SW(v1, v2); SW(v4, v5); SW(v0, v2);
      SW(v3, v5); SW(v0, v1); SW(v3, v4);
      SW(v2, v5); SW(v0, v3); SW(v1, v4);
      SW(v2, v4); SW(v1, v3); SW(v2, v3);
      type = v4 == 0 ? 3 : (v5 == 0 ? 2 : (v0 & S) ==  0);

      if(E(v5) == E(v4) && v5 != v4)
        v5 = v4 = 0;

      else if(E(v4) == E(v3) && v4 != v3)
        v3 = v5, v5 = v4 = 0;

      if (E(v3) == E(v2) && v3 != v2)
        v2 = v4, v3 = v5, v5 = v4 = 0;

      if (E(v2) == E(v1) && v2 != v1)
        v1 = v3, v2 = v4, v3 = v5, v5 = v4 = 0;

      if (E(v1) == E(v0) && v1 != v0)
        v0 = v2, v1 = v3, v2 = v4, v3 = v5, v5 = v4 = 0;

      G(v0); G(v1); G(v2); G(v3); G(v4); G(v5);
    }

  bool operator < (const SwapAndMoveToInMovement& m2) const{
    return type != m2.type ? type > m2.type :
          (v0 != m2.v0 ? v0 > m2.v0 :
          (v1 != m2.v1 ? v1 > m2.v1 :
          (v2 != m2.v2 ? v2 > m2.v2 :
          (v3 != m2.v3 ? v3 > m2.v3 :
          (v4 != m2.v4 ? v4 > m2.v4 : v5 > m2.v5)))));
  }

};

class SwapAndMoveToInNeighborhood : public BestImprovementNeighborhood {
  private:
  int *prevDiff, *itsGood;
  vector<SwapAndMoveToInMovement> moves;
  SwapAndMoveToInMovement toDo;

  public:
  SwapAndMoveToInNeighborhood(int idNeighbor, int timeLimit, ofstream& graphicStream);
  ~SwapAndMoveToInNeighborhood();

  void populate() override;
  void makeMove() override;
  void removeEquals(int& szGood);
};

#endif
