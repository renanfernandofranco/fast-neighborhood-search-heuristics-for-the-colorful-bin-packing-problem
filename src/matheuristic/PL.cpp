#include "PL.h"

using namespace std;

Solver::Solver(Instance* instance, unsigned int seed): instance(instance), rng(seed){
  string logPath = instance->prefix + "-PLI.log";
  // Create an environment
  GRBEnv env = GRBEnv(true);
  env.set("LogFile", logPath);
  env.set(GRB_IntParam_Threads, 1);
  env.set(GRB_DoubleParam_TimeLimit, 60);
  env.set(GRB_IntParam_Presolve, 0);
  env.set(GRB_IntParam_Method, 2);
  env.set(GRB_IntParam_OutputFlag, 0);
  env.start();

  // Create an empty model
  model = new GRBModel(env);
  numCols = 0;
  N = instance->size;
  its.resize(N);
  InitModel();
}

void Solver::InitModel(){
  // set the model objective to a minimization problem
  model->set(GRB_IntAttr_ModelSense, GRB_MINIMIZE);

  // Add constraints of uniqueness
  FOR(i, N){
    GRBLinExpr linExp = 0.0;
    model->addConstr(linExp, '=', 1.0, "u" + to_string(i));
  }

  model->update();
  constrs = model->getConstrs();
}

void Solver::AddPattern(Pattern* pat){
  pat->id = numCols++;
  GRBColumn col;
  
  for(int it : pat->items){
    its[it].push_back(pat->id);
    col.addTerm(1.0, constrs[it]);
  }

  string varname = "y[" + to_string(pat->id) + "]";
  vars.push_back(model->addVar(0.0, GRB_INFINITY, 1.0, GRB_CONTINUOUS, col, varname));
  patterns.push_back(pat);
}


void Solver::setBasis(vector<int> basis){
  model->update();
  FOR(i, numCols)
    vars[i].set(GRB_IntAttr_VBasis, -1);
  for(int b : basis)
    vars[b].set(GRB_IntAttr_VBasis, 0);
  FOR(i, N - SZ(basis))
    constrs[i].set(GRB_IntAttr_CBasis, 0);
  FOR(i, SZ(basis))
    constrs[N - SZ(basis) + i].set(GRB_IntAttr_CBasis, -1);
  model->update();
}

vd Solver::Solve(bool& abort, double remainingTime){
  try {
    model->set(GRB_IntParam_Seed, rng() % INT_MAX);
    model->set(GRB_DoubleParam_TimeLimit, remainingTime);
    model->update();
    model->optimize();

    if(model->get(GRB_IntAttr_Status) == GRB_TIME_LIMIT){
      abort = true;
      return vd();
    }

    int iterations = model->get(GRB_DoubleAttr_IterCount);
    vd solSimplex(numCols);
    FOR(i, numCols)
      solSimplex[i] = vars[i].get(GRB_DoubleAttr_X);

    if(iterations == 0){
      vector<int> candidates;
      FOR(i, numCols)
        if(solSimplex[i] > EPS && solSimplex[i] < 1 - EPS){
          if(solSimplex[i] > 0.5 + EPS){
            candidates.push_back(i);
          }
        }

      vector<GRBConstr> cuts;
      int rem = 2;
      while(rem-- && !candidates.empty()){
        int ch = rng() % candidates.size();
        GRBLinExpr linExp = vars[candidates[ch]];
        candidates.erase(candidates.begin() + ch);
        cuts.push_back(model->addConstr(linExp, '>', 1.0, "cut[" + to_string(cuts.size()) + "]"));
      }

      model->set(GRB_DoubleParam_TimeLimit, remainingTime);
      model->update();
      model->optimize();

      if(model->get(GRB_IntAttr_Status) == GRB_TIME_LIMIT){
        abort = true;
      }

      FOR(i, numCols)
        solSimplex[i] = vars[i].get(GRB_DoubleAttr_X);

      for(auto c: cuts)
        model->remove(c);
      model->update();
    }

    return solSimplex;
  } catch(GRBException e) {
    cout << "Error code = " << e.getErrorCode() << endl;
    cout << e.getMessage() << endl;
  } catch(...) {
    cout << "Exception during optimization" << endl;
  }
  exit(1);
  return vd();
}