#ifndef VNS_H
#define VNS_H

#include "solution.h"
#include "neighborhood.h"

#include "MoveItemNeighborhood.h"
#include "SwapItemsNeighborhood.h"
#include "ShakerNeighborhood4.h"
#include "MoveTwoToOneNeighborhood.h"
#include "SwapAndMoveToInNeighborhood.h"
#include "PL.h"
#include "Heuristic.h"

using namespace std;

class Vns{
  private:
    Status status;
    Solution *bestSol;
    vector<Neighborhood*> neighborhoods;
    int timeInitial, diffToLowerBound;
    int targetValue, kmax;
    bool tleOnInitial;
    random_device rd;
    chrono::steady_clock::time_point begin, now;
    int timeLastImprovement,  iteraLastImprovement,  iteraCurrent, timeCurrent;
    int iteraLastImprovementObject, beginSolValue, lastPrint;
    bool hitTarget;
    vector<int> timesPerformed;
    string sep;
    int sizeBests;
    ll sumSize;
    int maxAccept;
    Solver* solver;
    Heuristic* heuristic;
    bool skipPL;

  public:
    ofstream reportStream, graphicStream, plStream;
    set<Solution*, GreaterSol> setBests;
    int timeLimit, lowerBound, timeLastImprovementObject, timeLast;
    vector<int> solsRelink;
    set<Pattern*, lessPattern> patterns;

    Vns(Status& status);

    void configure(chrono::steady_clock::time_point begin);
    void setInitialConfigurationToRun(Solution* sol, chrono::steady_clock::time_point begin);

    Solution* run(Solution* sol, chrono::steady_clock::time_point begin);
    Solution* bestImprovement();
    void storePatterns(Solution* sol);

    bool runPL();

    void printReport(bool result, int finalValue, int diffVnsToRelink);
    void printStatus(bool reset = true);

    ~Vns();
};

#endif
