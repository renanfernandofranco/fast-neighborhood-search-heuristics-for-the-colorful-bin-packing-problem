#include "Vnd.h"

Vnd::Vnd(string prefix){
  int timeLimit = 1000;
  graphicStream.open(prefix + "-relink.gph", ofstream::app);
  neighborhoods.push_back(new MoveItemNeighborhood(2, timeLimit, graphicStream));
  neighborhoods.push_back(new SwapItemsNeighborhood(3, timeLimit, graphicStream));
  neighborhoods.push_back(new SwapAndMoveToInNeighborhood(4, timeLimit, graphicStream));
  neighborhoods.push_back(new MoveTwoToOneNeighborhood(5, timeLimit, graphicStream));
  kmax = neighborhoods.size();
  chrono::steady_clock::time_point begin = chrono::steady_clock::now();
  for(size_t i = 0; i < neighborhoods.size(); i++)
    neighborhoods[i]->setSetting(0, 0, begin);
}

void Vnd::run(Solution* sol){
  targetValue = sol->getLowerBound();
  bestImprovement(sol);
}

void Vnd::bestImprovement(Solution* sol){
  for(int k = 0; k < kmax && sol->trueObject() > targetValue;){
    neighborhoods[k]->updateSolution(sol);
    bool improved = neighborhoods[k]->search(sol);
    if(improved)
      k = 0;
    else
      k++;
  }
}

Vnd::~Vnd(){
  FOR(i, SZ(neighborhoods))
    delete neighborhoods[i];
  graphicStream.close();
}
