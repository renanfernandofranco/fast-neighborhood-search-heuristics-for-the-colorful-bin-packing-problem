#include "vns.h"

using namespace std;

Vns::Vns(Status& status){
  this->seed = status.seed;
  this->useTargetValue = status.useTarget;

  timeLimit = 60000;
  sizeBests = 10;
  sep = ",";
  string pathReport = status.prefix + ".log";
  string pathGraphic = status.prefix + ".gph";

  reportStream.open(pathReport, ofstream::app);
  graphicStream.open(pathGraphic);

  neighborhoods.push_back(new ShakerNeighborhood4(1, timeLimit, graphicStream,
   status.repacksShake, status.movesShake));

  neighborhoods.push_back(new MoveItemNeighborhood(2, timeLimit, graphicStream));
  neighborhoods.push_back(new SwapItemsNeighborhood(3, timeLimit, graphicStream));
  neighborhoods.push_back(new SwapAndMoveToInNeighborhood(4, timeLimit, graphicStream));
  neighborhoods.push_back(new MoveTwoToOneNeighborhood(5, timeLimit, graphicStream));
  kmax = neighborhoods.size();
}

void Vns::configure(chrono::steady_clock::time_point begin){
  lowerBound = bestSol->getLowerBound();
  if(useTargetValue){
    targetValue = lowerBound + 1;
  }else
    targetValue = lowerBound;

  for(size_t i = 0; i < neighborhoods.size(); i++){
    neighborhoods[i]->setSetting(seed, targetValue, begin);
  }
}

void Vns::setInitialConfigurationToRun(Solution* sol, chrono::steady_clock::time_point begin) {
  this->bestSol = sol;
  this->begin = begin;
  configure(begin);

  beginSolValue = sol->trueObject();
  timesPerformed.assign(neighborhoods.size(), 0);

  now = chrono::steady_clock::now();
  timeInitial = timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();
  timeLastImprovementObject = timeLastImprovement = timeCurrent;

  iteraLastImprovement  = iteraLastImprovementObject = iteraCurrent = 0;
  hitTarget = sol->trueObject() <= targetValue;
  tleOnInitial = timeInitial >= timeLimit;

  graphicStream << sol->trueObject() << " " << timeCurrent  << " " << 0 << '\n';
}

Solution* Vns::run(Solution* sol, chrono::steady_clock::time_point begin){
  setInitialConfigurationToRun(sol, begin);
  setBests.insert(sol->copy());
  return bestImprovement();
}

void Vns::printStatus(bool reset){
  if(reset) lastPrint = -10001;
  if(timeCurrent - lastPrint > 10000){
    lastPrint = timeCurrent;
    ll iterations = 0;
    FOR(i, kmax)
      iterations += neighborhoods[i]->runs;
    cout << bestSol->trueObject() << "\t" << bestSol->trueObject() - lowerBound << "\t"<< iterations << "\t\t" << timeCurrent / 1000 << endl;
    graphicStream.flush();
  }
}

Solution* Vns::bestImprovement(){
  Solution* sol =  bestSol->copy();
  bool skipFirst = true;

  // cout << "Value\tDiff\tIterations\tTime(s)" << endl;

  while(bestSol->trueObject() > targetValue && timeCurrent < timeLimit){
    iteraCurrent++;

    // printStatus(skipFirst);

    for(int k = skipFirst ? 1 : 0; k < kmax && timeCurrent < timeLimit && bestSol->trueObject() > targetValue;){
      timesPerformed[k]++;
      neighborhoods[k]->updateSolution(sol);
      bool improved = neighborhoods[k]->search(bestSol);

      now = chrono::steady_clock::now();
      timeCurrent = chrono::duration_cast<chrono::milliseconds>(now - begin).count();

      if(improved || k == 0){
        k = k == 1 ? 2 : 1;
        if (*sol < *bestSol){
          if(sol->trueObject() < bestSol->trueObject()){
            iteraLastImprovementObject = iteraCurrent;
            timeLastImprovementObject = timeCurrent;
          }
          iteraLastImprovement = iteraCurrent;
          timeLastImprovement = timeCurrent;
        }
      }
      else{
        k++;
      }
    }
    // if(!setBests.count(sol)){
    //   if(SZ(setBests) < sizeBests)
    //     setBests.insert(new Solution(*sol));
    //   else if(*sol < **setBests.begin()){
    //     Solution* toAdd = *setBests.begin();
    //     setBests.erase(setBests.begin());
    //     toAdd->reUse(sol);
    //     setBests.insert(toAdd);
    //   }
    // }
    if(*sol < *bestSol)
      bestSol->reUse(sol);
    skipFirst = false;
  }
  delete sol;

  hitTarget = bestSol->trueObject() <= targetValue;
  diffToLowerBound = bestSol->trueObject() - lowerBound;
  timeLast = timeCurrent;

  // printStatus(true);

  return bestSol;
}

void Vns::printReport(bool result, int finalValue, int diffFinal){
  reportStream << diffToLowerBound << sep << diffFinal << sep << beginSolValue - lowerBound << sep;
  reportStream << finalValue << sep << beginSolValue << sep;
  reportStream << timeInitial << sep << timeLastImprovementObject << sep << timeLastImprovement << sep;
  reportStream << timeCurrent << sep << timeLast << sep << iteraLastImprovementObject << sep;
  reportStream << iteraLastImprovement << sep << iteraCurrent  << sep << hitTarget << sep << tleOnInitial << sep;

  graphicStream << "times" << endl;
  for(size_t i = 0; i < neighborhoods.size(); i++){
    graphicStream << neighborhoods[i]->improvements << " " << timesPerformed[i] << " " << neighborhoods[i]->runs << endl;
    reportStream << neighborhoods[i]->improvements << sep << timesPerformed[i] << sep;
  }

  reportStream << result << endl;
  reportStream << "Seed: " <<  seed << endl;
  reportStream << "Sequence Neighboorhood: ";
  FOR(i, kmax)
    reportStream << neighborhoods[i]->idNeighbor << " \n"[i == kmax - 1];

  int cnt = 0;
  for(int vl : solsRelink)
    reportStream << vl << " " << cnt++ << endl;
}

Vns::~Vns(){
  for(size_t i = 0; i < neighborhoods.size(); i++)
    delete neighborhoods[i];
  reportStream.close();
  graphicStream.close();
  for(Solution* sol : setBests){
    delete sol;
  }
}
