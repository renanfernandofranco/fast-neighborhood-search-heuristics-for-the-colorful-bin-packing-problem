#include "solution.h"

Solution::Solution(Instance* instance){
  this->instance = instance;
  its.resize(instance->size);
  FOR(i, instance->size) its[i] = i;
  binIt = new Bin*[instance->size];
  memset(binIt, 0, sizeof(*binIt) * numItems);
}

Solution::Solution(const Solution& sol, bool empty){
  //sol.checkMap();
  instance = sol.instance;
  its = sol.its;
  binIt = new Bin*[instance->size];

  if(empty)
    memset(binIt, 0, sizeof(*binIt) * numItems);
  else
    copyBins(sol);
  //checkMap();
}

void Solution::reUse(Solution* sol){
  assert(instance == sol->instance);
  assert(its == sol->its);

  clear();
  copyBins(*sol);
}

void Solution::copyBins(const Solution& sol){
  for(Bin* bin : sol.fullBins){
    Bin* newBin = new Bin(*bin);
    fullBins.push_back(newBin);
    for(int it : newBin->items){
      binIt[it] = newBin;
      fullIts.push_back(it);
    }
  }
  for(Bin* bin : sol.nonFullBins){
    Bin* newBin = new Bin(*bin);
    nonFullBins.push_back(newBin);
    for(int it : bin->items){
      binIt[it] = newBin;
      nonFullIts.push_back(it);
    }
  }
  sort(all(fullBins), lessBin());
  sort(all(nonFullBins), lessBin());
  sort(all(fullIts), comp);
  sort(all(nonFullIts), comp);
  // checkMap();
}

void Solution::clear(){
  for(Bin* bin: fullBins)
    delete bin;
  for(Bin* bin: nonFullBins)
    delete bin;

  memset(binIt, 0, sizeof(*binIt) * numItems);
  fullIts.clear();
  nonFullIts.clear();
  fullBins.clear();
  nonFullBins.clear();
}

Solution::~Solution(){
  clear();
  delete[] binIt;
}

Solution* Solution::copy(){
  return new Solution(*this);
}

int Solution::getLowerBound(){
  ll sumSize = 0;
  for(int it : its)
    sumSize += weights[it];
  return ceil(1.0L * sumSize / binCapacity);
}

void Solution::sortBinsByResidue(vector<Bin*>& bins){
  sort(rall(bins), lessBin());
}

void Solution::sortBinsByResidueReverse(vector<Bin*>& bins){
  sort(all(bins), lessBin());
}

void Solution::checkMap()const{
  vector<int> cnt(numItems, 0);
  #define CHECK(bins)\
  for(Bin* bin : bins){\
    int residue = binCapacity;\
    for(int it : bin->items){\
      assert(2 * bin->colorCounter[colors[it]] - 1 <= bin->size);\
      cnt[it]++;\
      assert(cnt[it] == 1);\
      assert(binIt[it] == bin);\
      residue -= weights[it];\
    }\
    assert(residue ==  bin->residue() && residue >= 0);\
  }
  CHECK(fullBins);
  CHECK(nonFullBins);
  for(int it : its)
    assert(binary_search(all(fullIts), it, comp) +
      (int)binary_search(all(nonFullIts), it, comp) == 1);
  assert(accumulate(all(cnt), 0) == SZ(its));
  {
  vector<int> fits(numItems, 0), nfIts(numItems, 0);
    for(int it : fullIts)
      fits[it]++;
    for(int it : nonFullIts)
      nfIts[it]++;
    for(Bin* bin : nonFullBins)
      for(int it : bin->items)
        assert(nfIts[it] == 1);
    for(Bin* bin : fullBins)
      for(int it : bin->items)
        assert(fits[it] == 1);
  }
}

void Solution::printSolution(string pathOutSol){
  ofstream outSolStream;
  outSolStream.open(pathOutSol);
  ll sumSize = 0, totalFreeSpace = 0;
  int lowerBound = getLowerBound();
  outSolStream <<  "Capicity Bin: " << binCapacity << endl;
  outSolStream << "Lower Bound: " << lowerBound << endl;

  outSolStream << endl <<  "Bins used: " << trueObject() << endl;
  outSolStream << "Amount of Colors: " << nColors << endl << endl;
  int i = 0;

  vector<Bin*> bins;
  getBins(bins);
  for(Bin* bin: bins){
    totalFreeSpace += bin->residue();
    outSolStream << "Bin " << ++i << ": " << bin->residue() << " of cap free"<< endl;
    outSolStream << "Amount of items: " << bin->size << endl;
    outSolStream << "Items: " << endl;
    for(int id : bin->items){
      outSolStream << "Size: " << weights[id] << " Color: " << colors[id] << endl;
      sumSize += weights[id];
    }
    outSolStream << endl;
  }
  outSolStream << "Total size: " << sumSize << endl;
  outSolStream << "Total free space: " << totalFreeSpace << endl << endl;
  outSolStream.close();
}


void Solution::getBins(vector<Bin*>& bins){
  bins.clear();
  bins.insert(bins.end(), rall(fullBins));
  bins.insert(bins.end(), rall(nonFullBins));
}

void Solution::getBinsReverse(vector<Bin*>& bins){
  bins.clear();
  bins.insert(bins.end(), all(nonFullBins));
  bins.insert(bins.end(), all(fullBins));
}

void Solution::getBinsReverseLB(vector<Bin*>& bins, int lb){
  bins.clear();
  jk->cap = binCapacity - lb;
  auto it = lower_bound(all(nonFullBins), jk, lessCap());
  bins.insert(bins.end(), nonFullBins.begin(), it);
}

void Solution::repacking(vector<int>& items){
  if(items.size() > 0){
    sort(items.begin(), items.end());
    Bin* bin = new Bin();
    int moreFreq, qtMF;

    Bin::colorMoreFreq(items, qtMF, moreFreq);
    int binSolo = max(2 * qtMF - 1 - SZ(items), 0);

    for (int it: items){
      if(binSolo > 0 && colors[it] == moreFreq){
        binSolo--;
        insertBin(new Bin(it));
      }else
        bin->add(it);
    }
    insertBin(bin);
  }
}

Solution* Solution::getBestInitial(Instance* instance){
	Solution* sol1 = new Solution(instance);
	Solution* sol2 = new Solution(instance);
	sol1->TwoByTwo2();
	sol2->HardBFD();
	if(*sol2 < *sol1) swap(sol1, sol2);
	delete sol2;
	return sol1;
}

void Solution::TwoByTwo(){
  vector<int> order(all(its));
  reverse(all(order));
  vector<ll> residues(numItems + 1);
  vector<int> itsSel(numItems + 1), forbColor(numItems + 1), prevDiff(numItems + 1);
  int seed = 0;
  mt19937 rng(seed);
  vector<ii> moves;
  vector<int> colorCounter(nColors, 0);

  struct Move{
    int value;
    int cntMf;
    int i1, i2;
    bool operator < (const Move& m2){
      return value != m2.value ? value < m2.value : cntMf > m2.cntMf;
    }
  };

  FOR(i, numItems)
    colorCounter[colors[i]]++;

  #define EVAL(it1, it2){\
    int cntMf = (colors[it1] == mf) + (it2 != -1 && colors[it2] == mf);\
    int qtMov = (it2 != -1) + 1;\
    if(cntMf == qtMov || 3 * (colorCounter[mf] - cntMf) <= 2 * (SZ(order) - qtMov)){\
      Move curMove = {curResidue, cntMf, it1, it2};\
      if(curMove < bestMove)\
        moves.clear();\
      if(!(bestMove < curMove)){\
        bestMove = curMove;\
        moves.emplace_back(it1, it2);\
      }\
    }\
  }

  while(!order.empty()){
    int it = order[0];
    int mf = max_element(all(colorCounter)) - colorCounter.begin();

    if(3 * colorCounter[mf] > 2 * (SZ(order) - 1))
      for(int it2 : order)
        if(colors[it2] == mf){
          it = it2;
          break;
        }

    #define ADDERASE(it){\
      to->add(it);\
      order.erase(find(all(order), it));\
      colorCounter[colors[it]]--;\
    }
    Bin* to = new Bin();
    ADDERASE(it);

    while(true)	{
      int residueTo = to->residue();
      int forbColorTo = to->forbiddenColor();
      int nBins = 0;
      int mf = max_element(all(colorCounter)) - colorCounter.begin();
      moves.clear();

      Move bestMove = {residueTo, 0, -1, -1};

      REV(j, SZ(order)){
        int it = order[j];
        int weight = weights[it];
        int color = colors[it];

        if(to->canPack(weight, color)){
          int curResidue = residues[nBins] = residueTo - weight;
          itsSel[nBins] = it;
          forbColor[nBins] = (forbColorTo != -1 || canAddTwo(to, color))? -1 : color;
          EVAL(it, -1);
          nBins++;
        }
      }

      if(!nBins)
        break;

      residues[nBins] = 0;
      prevDiff[0] = -1;
      for (int j = 1; j < nBins; j++)
        prevDiff[j] = (forbColor[j] != -1 && forbColor[j] == forbColor[j - 1]) ? prevDiff[j - 1] : j - 1;

      int szHull = 0;
      FOR(j, SZ(order)){
        int it = order[j];
        int weight = weights[it];
        int color = colors[it];
        while (residues[szHull] >= weight)
          szHull++;

        //assert(szHull);
        int id = szHull - 1;

        if (id != - 1 && forbColor[id] == color)
          id = prevDiff[id];
        if (id != -1 && it == itsSel[id]){
          id--;
          if (id != -1 && forbColor[id] == color)
            id = prevDiff[id];
        }

        if (id != -1){
          int curResidue = residues[id] - weight;
          EVAL(itsSel[id], it);
        }
      }

      if(bestMove.i1 == -1)
        break;

      std::uniform_int_distribution<int> choose_uniform(0, SZ(moves) - 1);

      ii chosen = moves[choose_uniform(rng)];

      ADDERASE(chosen.first);
      if(chosen.second != -1)
        ADDERASE(chosen.second);
    }
    insertBin(to);
  }
  checkMap();
}

void Solution::externalChecker(){
  checkMap();
  printSolution(instance->pathOutSol);
  string commandSystem = instance->pathCheckSol + " " + instance->pathOutSol + " " + instance->pathInstance;
  instance->resultCheckSol |= system(commandSystem.c_str());
  assert(!instance->resultCheckSol);
}

void Solution::TwoByTwo2(vector<int> items){
  vector<int> order = items.empty() ? vector<int>(rall(its)) : items;
  vector<ll> residues(numItems + 1);
  vector<int> itsSel(numItems + 1), forbColor(numItems + 1), prevDiff(numItems + 1);
  int seed = 0;
  mt19937 rng(seed);
  vector<ii> moves;
  vector<int> colorCounter(nColors, 0);

  struct Move{
    double value;
    int i1, i2;
    bool operator < (const Move& m2){
      return value < m2.value;
    }
  };

  for(int it : order)
    colorCounter[colors[it]]++;

  int mf = max_element(all(colorCounter)) - colorCounter.begin();
  double ratio = colorCounter[mf] / (0.0 + SZ(order));

  #define EVAL2(it1, it2){\
    int cntMf = (colors[it1] == mf) + (it2 != -1 && colors[it2] == mf);\
    int qtMov = (it2 != -1) + 1;\
    if(cntMf == qtMov || 3 * (colorCounter[mf] - cntMf) <= 2 * (SZ(order) - qtMov)){\
      double diffRatio = pow(ratio - (colorCounter[mf] - cntMf)/(.0 + SZ(order) - qtMov), 2);\
      double resRatio = pow(curResidue/(0. + binCapacity), 2);\
      Move curMove = {resRatio + SZ(order) * diffRatio, it1, it2};\
      if(curMove < bestMove)\
        moves.clear();\
      if(!(bestMove < curMove)){\
        bestMove = curMove;\
        moves.emplace_back(it1, it2);\
      }\
    }\
  }

  while(!order.empty()){
    int mf = max_element(all(colorCounter)) - colorCounter.begin();
    Move bestMove = {DBL_MAX, -1, -1};
    moves.clear();

    FOR(i, SZ(order)){
      ll curResidue = binCapacity - weights[order[i]];
      EVAL2(order[i], -1);
    }

    assert(!moves.empty());

    std::uniform_int_distribution<int> choose_uniform(0, SZ(moves) - 1);
    int it = moves[choose_uniform(rng)].first;

    #define ADDERASE(it){\
      to->add(it);\
      order.erase(find(all(order), it));\
      colorCounter[colors[it]]--;\
    }
    Bin* to = new Bin();
    ADDERASE(it);

    while(true)	{
      ll residueTo = to->residue();
      int forbColorTo = to->forbiddenColor();
      int nBins = 0;
      int mf = max_element(all(colorCounter)) - colorCounter.begin();
      moves.clear();

      bestMove = {DBL_MAX, -1, -1};

      FOR(z, 2){
        REV(j, SZ(order)){
          int it = order[j];
          ll weight = weights[it];
          int color = colors[it];

          if(to->canPack(weight, color)){
            ll curResidue = residues[nBins] = residueTo - weight;
            itsSel[nBins] = it;
            forbColor[nBins] = (forbColorTo != -1 || canAddTwo(to, color))? -1 : color;
            if(z ^ (forbColor[nBins] == mf))
              continue;
            EVAL2(it, -1);
            nBins++;
          }
        }

        if(!nBins)
          break;

        residues[nBins] = 0;
        prevDiff[0] = -1;
        for (int j = 1; j < nBins; j++)
          prevDiff[j] = (forbColor[j] != -1 && forbColor[j] == forbColor[j - 1]) ? prevDiff[j - 1] : j - 1;

        int szHull = 0;
        FOR(j, SZ(order)){
          int it = order[j];
          ll weight = weights[it];
          int color = colors[it];
          while (residues[szHull] >= weight)
            szHull++;

          //assert(szHull);
          int id = szHull - 1;

          if (id != - 1 && forbColor[id] == color)
            id = prevDiff[id];
          if (id != -1 && it == itsSel[id]){
            id--;
            if (id != -1 && forbColor[id] == color)
              id = prevDiff[id];
          }

          if (id != -1){
            ll curResidue = residues[id] - weight;
            EVAL2(itsSel[id], it);
          }
        }
      }

      if(bestMove.i1 == -1)
        break;

      std::uniform_int_distribution<int> choose_uniform(0, SZ(moves) - 1);

      ii chosen = moves[choose_uniform(rng)];

      ADDERASE(chosen.first);
      if(chosen.second != -1)
        ADDERASE(chosen.second);
    }
    insertBin(to);
  }
  if(items.empty()) checkMap();
}


void Solution::HardBFD(){
  vector<int> order = its, withoutOneColor(numItems);
  vector<Bin*> bins;
  vector<ll> residues(numItems + 1);

  REV(i, SZ(order)){
    int it1 = order[i];
    ll weight1 = weights[it1];
    int color1 = colors[it1];
    order.pop_back();

    if(!tryPackingBFD(it1)){
      ll bestValue = binCapacity;
      int it2Best = -1;
      int szHull = 0;
      Bin* binBest = NULL;
      ll minIt = weights[0];

      getBinsReverseLB(bins, minIt + weight1 - 1);

      FOR(i, SZ(bins))
        residues[i] = bins[i]->residue() - weight1;
      residues[SZ(bins)] = 0;

      if(residues[0] <= 0)
        goto bad;

      REV(j, i){
        int it2 = order[j];
        ll weight2 = weights[it2];
        int color2 = colors[it2];
        if(color1 == color2)
          continue;
        while (residues[szHull] >= weight2)
          szHull++;

        int id = szHull - 1;
        if(id >= 0 && residues[id] - weight2 < bestValue){
          bestValue = residues[id] - weight2;
          it2Best = it2;
          binBest = bins[id];
        }
      }
      bad:
      if(binBest != NULL){
        addItem(binBest, it1, it2Best);
        order.erase(find(all(order), it2Best));
        i--;
      }else{
        insertBin(new Bin(it1));
      }
    }
  }
  checkMap();
}

void Solution::MBS(){
  int i, j, k;

  buildGoodOrdering(itemsMBS, false);
  minMBS.resize(itemsMBS.size() + 1);
  suffixMBS.resize(itemsMBS.size());

  while (!itemsMBS.empty()) {
    bin = new Bin(itemsMBS[0]);
    bestBin = new Bin(itemsMBS[0]);
    itemsMBSsize = itemsMBS.size();

    maxCntMBS = 1LL * itemsMBSsize * sqrt(itemsMBSsize);
    cntMBS = 0;
    ll acum = 0;
    minMBS[itemsMBSsize] = INT_MAX;
    for(i = itemsMBSsize - 1; i >= 0; i--){
      acum += weights[itemsMBS[i]];
      suffixMBS[i] = acum;
      minMBS[i] = min(weights[itemsMBS[i]], minMBS[i + 1]);
    }

    MBSOnePacking(1);

    j = 0; k = 0;
    for (i = 0; i < bestBin->size; i++){
      while(itemsMBS[k] != bestBin->items[i]){
        itemsMBS[j] = itemsMBS[k];
        k++; j++;
      }
      k++;
    }
    while(k < itemsMBSsize){
      itemsMBS[j] = itemsMBS[k];
      j++; k++;
    }
    itemsMBS.resize(itemsMBSsize - bestBin->size);
    insertBin(bestBin);
    delete bin;
  }
  checkMap();
}

bool Solution::MBSOnePacking(int pos){
  if (minMBS[pos] > bin->residue())
    return false;
  int cur;
  for(; pos < itemsMBSsize; pos++){
    cntMBS++;
    if (cntMBS > maxCntMBS)
      return true;
    if (suffixMBS[pos] + bin->residue() <= bestBin->residue())
      return false;

    cur = itemsMBS[pos];
    if (bin->canPack(weights[cur], colors[cur])){
      bin->add(cur);
      if (bin->residue() < bestBin->residue()){
        delete bestBin;
        bestBin = new Bin(*bin);
        if (bestBin->residue() == 0)
          return true;
      }
      if (MBSOnePacking(pos + 1))
        return true;
      bin->justRemove(cur);
    }
  }
  return false;
}

void Solution::buildGoodOrdering(vector<int>& order, bool insertBegin){
  vector<vector<int>> its(nColors);
  vector<int> remainder;

  for(int i = 0; i < numItems; i++)
    its[colors[i]].push_back(i);

  int mf = 0, cl, gt;
  for(int i = 1; i < nColors; i++)
    if(its[mf].size() < its[i].size())
      mf = i;

  if (insertBegin){
    while(2 * SZ(its[mf]) - 1 > numItems - SZ(order)){
      order.push_back(its[mf].back());
      its[mf].pop_back();
    }
  }else{
    std::reverse(its[mf].begin(), its[mf].end());
    while(2 * SZ(its[mf]) - 1 > numItems - SZ(order)){
      remainder.push_back(its[mf].back());
      its[mf].pop_back();
    }
    std::reverse(its[mf].begin(), its[mf].end());
    std::reverse(remainder.begin(), remainder.end());
  }

  for(int sz = numItems - order.size() - remainder.size(); sz >= 1; sz--){
    mf = 0;
    for(int i = 1; i < nColors; i++)
      if(its[mf].size() < its[i].size())
        mf = i;
    if (2 * SZ(its[mf]) - 1 < sz){
      cl = -1;
      gt = -1;
      for(int i = 0; i < nColors; i++){
        if (!order.empty() && colors[order.back()] == i)
          continue;
        if(!its[i].empty() && gt < its[i].back()){
          cl = i;
          gt = its[i].back();
        }
      }
      mf = cl;
    }
    order.push_back(its[mf].back());
    its[mf].pop_back();
  }
  for (int cur : remainder)
    order.push_back(cur);
}

void Solution::GoodOrdering(){
  vector<int> order;
  buildGoodOrdering(order, true);
  vectorPackingBFD(order);
}

void Solution::BFD(){
  for(int i = numItems - 1; i >= 0; i--)
    packingBFD(i);
}