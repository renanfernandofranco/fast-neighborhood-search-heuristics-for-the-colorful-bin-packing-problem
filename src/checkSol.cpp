#include <bits/stdc++.h>

using namespace std;
using ll = long long;

int main(int argc, char* argv[]){
    ifstream solIn, instIn;
    solIn.open(argv[1]);
    instIn.open(argv[2]);
    ll capBinSol, capBinInst;
    int contador = 0;
    int binsUsed, nColorsSol, nColorsInst;
    int nInst, lowerBound;
    int color, nItems;
    ll size, capfree, capAcum;
    ll totalsize;
    string l;
    vector<int> colorCount;

    instIn >> nInst >> nColorsInst >> capBinInst;
    
    vector<pair<ll, int>> itemsIn(nInst), itemsSol;
    for(int i = 0; i < nInst; i++)
        instIn >> itemsIn[i].first >> itemsIn[i].second;

    sort(itemsIn.begin(),itemsIn.end());

    solIn >> l >> l >> capBinSol;
    solIn >> l >> l >> lowerBound;
    solIn >> l >> l >> binsUsed;
    solIn >> l >> l >> l >> nColorsSol;

    if(solIn.eof()){
        throw runtime_error("Not found solution");
    }
    
    ll totalSizeSol = 0;

    for(int i = 0; i < binsUsed; i++){
        colorCount.assign(nColorsSol, 0);
        solIn >> l >> l >> capfree;
        getline(solIn, l);
        solIn >> l >> l >> l >> nItems;
        solIn >> l;
        capAcum = 0;
        for(int j = 0; j < nItems; j++){
            solIn >> l >> size >> l >> color;
            itemsSol.push_back({size,color});
            if(size < 0){
                cout << "Item com cap negativa na bin: " << i << endl;
                exit(1);
            }
            capAcum += size;
            if(color >= nColorsSol){
                cout << "Cor invalida na bin: " << i << endl;
                exit(1);
            }
            colorCount[color]++;
        }
        if(capAcum > capBinSol || capBinSol - capAcum != capfree){
            cout << "Cap incorreta na bin: " << i << endl;
            exit(1);
        }
        for(int j = 0; j < nColorsSol; j++){
            if(2 * colorCount[j] - 1 > nItems){
                cout << "Violacao de cor na bin: " << i << endl;
                exit(1);
            }
        }
        totalSizeSol += capAcum; 
    }
    string bin;
    solIn >> bin >> l >> totalsize;
    solIn >> l >> l >> l >> l;

    if (bin == "Bin"){
        cout << "Solucao mentiu ao dizer o numero de bins" << endl;
        exit(1);
    }

    ll totalSizeIn = 0;

    for (auto p : itemsIn)
        totalSizeIn += p.first;

    
    sort(itemsSol.begin(), itemsSol.end());

    if (itemsSol.size() < itemsIn.size()){
        cout << "Ta faltando " << itemsIn.size()- itemsSol.size() << " item(s) na solucao" << endl;
        exit(1);
    }

    if (itemsSol.size() > itemsIn.size()){
        cout << "Ta sobrando " << itemsSol.size()- itemsIn.size() << " item(s) na solucao" << endl;
        exit(1);
    }

    if(totalSizeIn != totalSizeSol){
        cout << "Total size invalido" << endl;
        cout << "A instancia tem soma igual a: " << totalSizeIn << endl;
        cout << "A solucao tem soma igual a: " << totalSizeSol << endl;
        exit(1);
    }

    for(int i = 0; i < itemsSol.size(); i++){
        if(itemsIn[i] != itemsSol[i]){    
            cout << "Os item nao bateu" << endl;
            cout << "Na instancia o item " << i + 1 << " tem tamanho " << itemsIn[i].first << " e cor" << itemsIn[i].second  << endl;
            cout << "Na solucao o item " << i + 1 << " tem tamanho " << itemsSol[i].first << " e cor" << itemsSol[i].second  << endl;
            exit(1);
        }
    }
    if(nColorsInst != nColorsSol || binsUsed < lowerBound || capBinInst != capBinSol || totalsize != totalSizeSol){
        cout << "Provavelmente acesando memoria invalida !!!" << endl;
        exit(1);
    }
    cout << "Solution ok" << endl;
    solIn.close();
    instIn.close();
    return 0;
}